/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:43 PM
**        * FROM NATURAL PROGRAM : Iaap999
************************************************************
**        * FILE NAME            : Iaap999.java
**        * CLASS NAME           : Iaap999
**        * INSTANCE NAME        : Iaap999
************************************************************
************************************************************************
* PROGRAM : IAAP999
*
* DATE    : 11/03/2008
*
* PURPOSE : ADHOC TO PERFORM DATA FIXES, ETC.
*
* 12/22/10 : ADDED A STEP TO DETERMINE IF A PROGRAM TO BE FETCHED EXISTS
*
* 12/03/15 : FIXED CNTRCT-PYMNT-MTHD FOR SELECTED CONTRACTS
* 06/06/16 : TERMINATE JOB IF NO FETCHED PROGRAM EXISTS.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap999 extends BLNatBase
{
    // Data Areas
    private LdaIaal999 ldaIaal999;
    private LdaIaal205a ldaIaal205a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_cpr2;
    private DbsField cpr2_Cpr_Id_Nbr;
    private DbsField cpr2_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr2_Cntrct_Part_Payee_Cde;
    private DbsField cpr2_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr2_Cntrct_Actvty_Cde;

    private DataAccessProgramView vw_dc;
    private DbsField dc_Cntrct_Id_Nbr;
    private DbsField dc_Cntrct_Tax_Id_Nbr;
    private DbsField dc_Cntrct_Process_Type;
    private DbsField dc_Cntrct_Req_Seq_Nbr;
    private DbsField dc_Cntrct_Timestamp;
    private DbsField dc_Cntrct_Ppcn_Nbr;
    private DbsField dc_Cntrct_Payee_Cde;
    private DbsField dc_Cntrct_Status_Cde;
    private DbsField dc_Cntrct_Annt_Type;
    private DbsField dc_Cntrct_Optn_Cde;
    private DbsField dc_Cntrct_Issue_Dte;
    private DbsGroup dc_Cntrct_Sttlmnt_Info_CdeMuGroup;
    private DbsField dc_Cntrct_Sttlmnt_Info_Cde;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key;

    private DbsGroup pnd_Pin_Taxid_Seq_Cntrct_Key__R_Field_1;
    private DbsField pnd_Pin_Taxid_Seq_Cntrct_Key_Pnd_Cntrct_Id_Nbr;

    private DataAccessProgramView vw_settl;
    private DbsField settl_Sttlmnt_Id_Nbr;
    private DbsField settl_Sttlmnt_Tax_Id_Nbr;
    private DbsField settl_Sttlmnt_Req_Seq_Nbr;
    private DbsField settl_Sttlmnt_Process_Type;
    private DbsField settl_Sttlmnt_Decedent_Type;
    private DbsField settl_Sttlmnt_Dod_Dte;
    private DbsField settl_Sttlmnt_2nd_Dod_Dte;
    private DbsField settl_Sttlmnt_Timestamp;
    private DbsField settl_Sttlmnt_Status_Cde;
    private DbsField settl_Sttlmnt_Status_Timestamp;
    private DbsField pnd_Pin_Taxid_Seq_Key;

    private DbsGroup pnd_Pin_Taxid_Seq_Key__R_Field_2;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_3;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte;

    private DataAccessProgramView vw_xfr;
    private DbsField xfr_Xfr_Work_Prcss_Id;
    private DbsField xfr_Xfr_Stts_Cde;
    private DbsField xfr_Rqst_Xfr_Type;
    private DbsField xfr_Xfr_Rjctn_Cde;
    private DbsField xfr_Rqst_Entry_Dte;
    private DbsField xfr_Rqst_Effctv_Dte;
    private DbsField xfr_Rqst_Entry_User_Id;
    private DbsField xfr_Rcrd_Type_Cde;
    private DbsField xfr_Ia_Frm_Cntrct;
    private DbsField xfr_Ia_Frm_Payee;

    private DbsGroup xfr_Xfr_Frm_Acct_Dta;
    private DbsField xfr_Xfr_Frm_Acct_Cde;
    private DbsField xfr_Xfr_Frm_Unit_Typ;
    private DbsField xfr_Xfr_Frm_Typ;
    private DbsField xfr_Xfr_Frm_Qty;
    private DbsField xfr_Xfr_Frm_Asset_Amt;

    private DbsGroup xfr_Xfr_To_Acct_Dta;
    private DbsField xfr_Xfr_To_Acct_Cde;
    private DbsField xfr_Xfr_To_Unit_Typ;
    private DbsField xfr_Xfr_To_Typ;
    private DbsField xfr_Xfr_To_Qty;
    private DbsField xfr_Xfr_To_Asset_Amt;
    private DbsField xfr_Ia_To_Cntrct;
    private DbsField xfr_Ia_To_Payee;
    private DbsField pnd_Ia_Super_De_01;

    private DbsGroup pnd_Ia_Super_De_01__R_Field_4;
    private DbsField pnd_Ia_Super_De_01_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Ia_Super_De_01_Filler;
    private DbsField pnd_D;
    private DbsField pnd_U;
    private DbsField pnd_Old_D;
    private DbsField pnd_Old_Py;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_5;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_6;
    private DbsField pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Cnt;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cpr_Cnt;
    private DbsField pnd_Cpr_Cnt1;
    private DbsField pnd_Old_Pin;
    private DbsField pnd_Pend_Dte;
    private DbsField pnd_Old_Pend;
    private DbsField pnd_Old_Pend_Dte;
    private DbsField pnd_First;
    private DbsField pnd_Norec;
    private DbsField pnd_Updt;
    private DbsField pnd_C_Isn;
    private DbsField pnd_Trans_Cmbne_Cde;

    private DbsGroup pnd_Trans_Cmbne_Cde__R_Field_7;
    private DbsField pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_I_Ssn;
    private DbsField pnd_Input_Pnd_Fill1;
    private DbsField pnd_Input_Pnd_I_Cntrct;
    private DbsField pnd_W_Ssn;
    private DbsField pnd_Pin_Cpr_Key;

    private DbsGroup pnd_Pin_Cpr_Key__R_Field_8;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Pin;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Cntrct;
    private DbsField pnd_Pin_Cpr_Key_Pnd_P_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_9;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Cpr_Bfre_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_10;
    private DbsField pnd_Cpr_Bfre_Key__Filler1;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cb_Key;

    private DbsGroup pnd_Cpr_Bfre_Key__R_Field_11;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cb_Cntrct;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cb_Pyee;
    private DbsField pnd_Cpr_Bfre_Key_Pnd_Cb_Trans_Dte;
    private DbsField pnd_Cpr_Updt;
    private DbsField pnd_Fund_Updt;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_2;

    private DbsGroup pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_2__Filler2;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Cntrct;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Pyee;
    private DbsField pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Trans_Dte;
    private DbsField pnd_Old_Prev_Dist_Cde;
    private DbsField pnd_Old_Curr_Dist_Cde;
    private DbsField pnd_Old_Res_Cde;
    private DbsField pnd_Pgm;
    private DbsField pnd_Rc;
    private DbsField pnd_Parm_Invrse_Dte;
    private DbsField pnd_Parm_Trans_Dte;
    private DbsField pnd_Parm_Check_Dte;
    private DbsField pnd_Parm_Todays_Dte;
    private DbsField pnd_Parm_Sub_Cde;
    private DbsField pnd_Parm_Trans_Cde;
    private DbsField pnd_New_Orgn;
    private DbsField pnd_Old_Orgn;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Updated_Cnt;
    private DbsField pnd_Contract_Not_Found;
    private DbsField pnd_Invalid_Opt_Code;
    private DbsField pnd_Ssn_Exists;
    private DbsField pnd_Cntrct;
    private DbsField pnd_C_Cntrct;
    private DbsField pnd_2nd_Xref;
    private DbsField pnd_C_Fund;
    private DbsField pnd_C_Rate;
    private DbsField pnd_C_Pins;
    private DbsField pnd_Cp;

    private DbsGroup pnd_Cp__R_Field_13;
    private DbsField pnd_Cp_Pnd_Cp_Cntrct;
    private DbsField pnd_Cp_Pnd_Cp_Payee;
    private DbsField pnd_H_Cp;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_14;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_O_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_O_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde;
    private DbsField pnd_Cntrct_Pymnt_Mthd;
    private DbsField pnd_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField pls_S_Invrse_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_cpr2 = new DataAccessProgramView(new NameInfo("vw_cpr2", "CPR2"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr2_Cpr_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr2_Cntrct_Part_Ppcn_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr2_Cntrct_Part_Payee_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr2_Prtcpnt_Tax_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr2_Cntrct_Actvty_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_cpr2);

        vw_dc = new DataAccessProgramView(new NameInfo("vw_dc", "DC"), "IAA_DC_CNTRCT", "IA_DEATH_CLAIMS");
        dc_Cntrct_Id_Nbr = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Id_Nbr", "CNTRCT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_ID_NBR");
        dc_Cntrct_Tax_Id_Nbr = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Tax_Id_Nbr", "CNTRCT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "CNTRCT_TAX_ID_NBR");
        dc_Cntrct_Process_Type = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Process_Type", "CNTRCT-PROCESS-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PROCESS_TYPE");
        dc_Cntrct_Req_Seq_Nbr = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Req_Seq_Nbr", "CNTRCT-REQ-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_REQ_SEQ_NBR");
        dc_Cntrct_Timestamp = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Timestamp", "CNTRCT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_TIMESTAMP");
        dc_Cntrct_Ppcn_Nbr = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        dc_Cntrct_Payee_Cde = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        dc_Cntrct_Status_Cde = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Status_Cde", "CNTRCT-STATUS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_STATUS_CDE");
        dc_Cntrct_Annt_Type = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Annt_Type", "CNTRCT-ANNT-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_ANNT_TYPE");
        dc_Cntrct_Optn_Cde = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        dc_Cntrct_Issue_Dte = vw_dc.getRecord().newFieldInGroup("dc_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        dc_Cntrct_Sttlmnt_Info_CdeMuGroup = vw_dc.getRecord().newGroupInGroup("DC_CNTRCT_STTLMNT_INFO_CDEMuGroup", "CNTRCT_STTLMNT_INFO_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "IA_DEATH_CLAIMS_CNTRCT_STTLMNT_INFO_CDE");
        dc_Cntrct_Sttlmnt_Info_Cde = dc_Cntrct_Sttlmnt_Info_CdeMuGroup.newFieldArrayInGroup("dc_Cntrct_Sttlmnt_Info_Cde", "CNTRCT-STTLMNT-INFO-CDE", FieldType.STRING, 
            4, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_STTLMNT_INFO_CDE");
        registerRecord(vw_dc);

        pnd_Pin_Taxid_Seq_Cntrct_Key = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key", "#PIN-TAXID-SEQ-CNTRCT-KEY", FieldType.STRING, 
            30);

        pnd_Pin_Taxid_Seq_Cntrct_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Cntrct_Key__R_Field_1", "REDEFINE", pnd_Pin_Taxid_Seq_Cntrct_Key);
        pnd_Pin_Taxid_Seq_Cntrct_Key_Pnd_Cntrct_Id_Nbr = pnd_Pin_Taxid_Seq_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Pin_Taxid_Seq_Cntrct_Key_Pnd_Cntrct_Id_Nbr", 
            "#CNTRCT-ID-NBR", FieldType.NUMERIC, 7);

        vw_settl = new DataAccessProgramView(new NameInfo("vw_settl", "SETTL"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        settl_Sttlmnt_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "STTLMNT_ID_NBR");
        settl_Sttlmnt_Tax_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "STTLMNT_TAX_ID_NBR");
        settl_Sttlmnt_Req_Seq_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "STTLMNT_REQ_SEQ_NBR");
        settl_Sttlmnt_Process_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STTLMNT_PROCESS_TYPE");
        settl_Sttlmnt_Decedent_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        settl_Sttlmnt_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_DOD_DTE");
        settl_Sttlmnt_2nd_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_2ND_DOD_DTE");
        settl_Sttlmnt_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STTLMNT_TIMESTAMP");
        settl_Sttlmnt_Status_Cde = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "STTLMNT_STATUS_CDE");
        settl_Sttlmnt_Status_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Timestamp", "STTLMNT-STATUS-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        registerRecord(vw_settl);

        pnd_Pin_Taxid_Seq_Key = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Key", "#PIN-TAXID-SEQ-KEY", FieldType.BINARY, 19);

        pnd_Pin_Taxid_Seq_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Key__R_Field_2", "REDEFINE", pnd_Pin_Taxid_Seq_Key);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_2.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr", "#STTLMNT-ID-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_2.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr", 
            "#STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_2.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr", 
            "#STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_3", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde", "#TRANS-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Chck_Dte_Key__R_Field_3.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte", 
            "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);

        vw_xfr = new DataAccessProgramView(new NameInfo("vw_xfr", "XFR"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        xfr_Xfr_Work_Prcss_Id = vw_xfr.getRecord().newFieldInGroup("xfr_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "XFR_WORK_PRCSS_ID");
        xfr_Xfr_Stts_Cde = vw_xfr.getRecord().newFieldInGroup("xfr_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        xfr_Rqst_Xfr_Type = vw_xfr.getRecord().newFieldInGroup("xfr_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_XFR_TYPE");
        xfr_Xfr_Rjctn_Cde = vw_xfr.getRecord().newFieldInGroup("xfr_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_RJCTN_CDE");
        xfr_Rqst_Entry_Dte = vw_xfr.getRecord().newFieldInGroup("xfr_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        xfr_Rqst_Effctv_Dte = vw_xfr.getRecord().newFieldInGroup("xfr_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        xfr_Rqst_Entry_User_Id = vw_xfr.getRecord().newFieldInGroup("xfr_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_USER_ID");
        xfr_Rcrd_Type_Cde = vw_xfr.getRecord().newFieldInGroup("xfr_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        xfr_Ia_Frm_Cntrct = vw_xfr.getRecord().newFieldInGroup("xfr_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        xfr_Ia_Frm_Payee = vw_xfr.getRecord().newFieldInGroup("xfr_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");

        xfr_Xfr_Frm_Acct_Dta = vw_xfr.getRecord().newGroupArrayInGroup("xfr_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        xfr_Xfr_Frm_Acct_Cde = xfr_Xfr_Frm_Acct_Dta.newFieldInGroup("xfr_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        xfr_Xfr_Frm_Unit_Typ = xfr_Xfr_Frm_Acct_Dta.newFieldInGroup("xfr_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        xfr_Xfr_Frm_Typ = xfr_Xfr_Frm_Acct_Dta.newFieldInGroup("xfr_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        xfr_Xfr_Frm_Qty = xfr_Xfr_Frm_Acct_Dta.newFieldInGroup("xfr_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        xfr_Xfr_Frm_Asset_Amt = xfr_Xfr_Frm_Acct_Dta.newFieldInGroup("xfr_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        xfr_Xfr_To_Acct_Dta = vw_xfr.getRecord().newGroupArrayInGroup("xfr_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Xfr_To_Acct_Cde = xfr_Xfr_To_Acct_Dta.newFieldInGroup("xfr_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Xfr_To_Unit_Typ = xfr_Xfr_To_Acct_Dta.newFieldInGroup("xfr_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Xfr_To_Typ = xfr_Xfr_To_Acct_Dta.newFieldInGroup("xfr_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Xfr_To_Qty = xfr_Xfr_To_Acct_Dta.newFieldInGroup("xfr_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Xfr_To_Asset_Amt = xfr_Xfr_To_Acct_Dta.newFieldInGroup("xfr_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        xfr_Ia_To_Cntrct = vw_xfr.getRecord().newFieldInGroup("xfr_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        xfr_Ia_To_Payee = vw_xfr.getRecord().newFieldInGroup("xfr_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        registerRecord(vw_xfr);

        pnd_Ia_Super_De_01 = localVariables.newFieldInRecord("pnd_Ia_Super_De_01", "#IA-SUPER-DE-01", FieldType.STRING, 17);

        pnd_Ia_Super_De_01__R_Field_4 = localVariables.newGroupInRecord("pnd_Ia_Super_De_01__R_Field_4", "REDEFINE", pnd_Ia_Super_De_01);
        pnd_Ia_Super_De_01_Pnd_Rqst_Effctv_Dte = pnd_Ia_Super_De_01__R_Field_4.newFieldInGroup("pnd_Ia_Super_De_01_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", 
            FieldType.DATE);
        pnd_Ia_Super_De_01_Filler = pnd_Ia_Super_De_01__R_Field_4.newFieldInGroup("pnd_Ia_Super_De_01_Filler", "FILLER", FieldType.STRING, 11);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.DATE);
        pnd_Old_D = localVariables.newFieldInRecord("pnd_Old_D", "#OLD-D", FieldType.DATE);
        pnd_Old_Py = localVariables.newFieldInRecord("pnd_Old_Py", "#OLD-PY", FieldType.NUMERIC, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Date__R_Field_5", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_5.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 6);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_5.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_6", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cpr_Key__R_Field_6.newFieldInGroup("pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cpr_Key__R_Field_6.newFieldInGroup("pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.PACKED_DECIMAL, 7);
        pnd_Cpr_Cnt = localVariables.newFieldInRecord("pnd_Cpr_Cnt", "#CPR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cpr_Cnt1 = localVariables.newFieldInRecord("pnd_Cpr_Cnt1", "#CPR-CNT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Pin = localVariables.newFieldInRecord("pnd_Old_Pin", "#OLD-PIN", FieldType.NUMERIC, 7);
        pnd_Pend_Dte = localVariables.newFieldInRecord("pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 6);
        pnd_Old_Pend = localVariables.newFieldInRecord("pnd_Old_Pend", "#OLD-PEND", FieldType.STRING, 1);
        pnd_Old_Pend_Dte = localVariables.newFieldInRecord("pnd_Old_Pend_Dte", "#OLD-PEND-DTE", FieldType.NUMERIC, 6);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Norec = localVariables.newFieldInRecord("pnd_Norec", "#NOREC", FieldType.BOOLEAN, 1);
        pnd_Updt = localVariables.newFieldInRecord("pnd_Updt", "#UPDT", FieldType.BOOLEAN, 1);
        pnd_C_Isn = localVariables.newFieldInRecord("pnd_C_Isn", "#C-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Trans_Cmbne_Cde = localVariables.newFieldInRecord("pnd_Trans_Cmbne_Cde", "#TRANS-CMBNE-CDE", FieldType.STRING, 12);

        pnd_Trans_Cmbne_Cde__R_Field_7 = localVariables.newGroupInRecord("pnd_Trans_Cmbne_Cde__R_Field_7", "REDEFINE", pnd_Trans_Cmbne_Cde);
        pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Seq_Nbr = pnd_Trans_Cmbne_Cde__R_Field_7.newFieldInGroup("pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Cde = pnd_Trans_Cmbne_Cde__R_Field_7.newFieldInGroup("pnd_Trans_Cmbne_Cde_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_I_Ssn = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ssn", "#I-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_Fill1 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        pnd_Input_Pnd_I_Cntrct = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Cntrct", "#I-CNTRCT", FieldType.STRING, 10);
        pnd_W_Ssn = localVariables.newFieldInRecord("pnd_W_Ssn", "#W-SSN", FieldType.STRING, 9);
        pnd_Pin_Cpr_Key = localVariables.newFieldInRecord("pnd_Pin_Cpr_Key", "#PIN-CPR-KEY", FieldType.STRING, 19);

        pnd_Pin_Cpr_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Pin_Cpr_Key__R_Field_8", "REDEFINE", pnd_Pin_Cpr_Key);
        pnd_Pin_Cpr_Key_Pnd_P_Pin = pnd_Pin_Cpr_Key__R_Field_8.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Pin", "#P-PIN", FieldType.NUMERIC, 7);
        pnd_Pin_Cpr_Key_Pnd_P_Cntrct = pnd_Pin_Cpr_Key__R_Field_8.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Cntrct", "#P-CNTRCT", FieldType.STRING, 10);
        pnd_Pin_Cpr_Key_Pnd_P_Payee = pnd_Pin_Cpr_Key__R_Field_8.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_P_Payee", "#P-PAYEE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_9", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr = pnd_Tiaa_Cntrct_Fund_Key__R_Field_9.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr", 
            "#TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_9.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde", 
            "#TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde = pnd_Tiaa_Cntrct_Fund_Key__R_Field_9.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde", 
            "#TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3);
        pnd_Cpr_Bfre_Key = localVariables.newFieldInRecord("pnd_Cpr_Bfre_Key", "#CPR-BFRE-KEY", FieldType.STRING, 20);

        pnd_Cpr_Bfre_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Cpr_Bfre_Key__R_Field_10", "REDEFINE", pnd_Cpr_Bfre_Key);
        pnd_Cpr_Bfre_Key__Filler1 = pnd_Cpr_Bfre_Key__R_Field_10.newFieldInGroup("pnd_Cpr_Bfre_Key__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Cpr_Bfre_Key_Pnd_Cb_Key = pnd_Cpr_Bfre_Key__R_Field_10.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cb_Key", "#CB-KEY", FieldType.STRING, 12);

        pnd_Cpr_Bfre_Key__R_Field_11 = pnd_Cpr_Bfre_Key__R_Field_10.newGroupInGroup("pnd_Cpr_Bfre_Key__R_Field_11", "REDEFINE", pnd_Cpr_Bfre_Key_Pnd_Cb_Key);
        pnd_Cpr_Bfre_Key_Pnd_Cb_Cntrct = pnd_Cpr_Bfre_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cb_Cntrct", "#CB-CNTRCT", FieldType.STRING, 
            10);
        pnd_Cpr_Bfre_Key_Pnd_Cb_Pyee = pnd_Cpr_Bfre_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cb_Pyee", "#CB-PYEE", FieldType.NUMERIC, 2);
        pnd_Cpr_Bfre_Key_Pnd_Cb_Trans_Dte = pnd_Cpr_Bfre_Key__R_Field_10.newFieldInGroup("pnd_Cpr_Bfre_Key_Pnd_Cb_Trans_Dte", "#CB-TRANS-DTE", FieldType.TIME);
        pnd_Cpr_Updt = localVariables.newFieldInRecord("pnd_Cpr_Updt", "#CPR-UPDT", FieldType.BOOLEAN, 1);
        pnd_Fund_Updt = localVariables.newFieldInRecord("pnd_Fund_Updt", "#FUND-UPDT", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Fund_Bfre_Key_2 = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Bfre_Key_2", "#TIAA-FUND-BFRE-KEY-2", FieldType.STRING, 23);

        pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12 = localVariables.newGroupInRecord("pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12", "REDEFINE", pnd_Tiaa_Fund_Bfre_Key_2);
        pnd_Tiaa_Fund_Bfre_Key_2__Filler2 = pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_2__Filler2", "_FILLER2", FieldType.STRING, 
            1);
        pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Cntrct = pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Cntrct", "#FB-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Pyee = pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Pyee", "#FB-PYEE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Trans_Dte = pnd_Tiaa_Fund_Bfre_Key_2__R_Field_12.newFieldInGroup("pnd_Tiaa_Fund_Bfre_Key_2_Pnd_Fb_Trans_Dte", 
            "#FB-TRANS-DTE", FieldType.TIME);
        pnd_Old_Prev_Dist_Cde = localVariables.newFieldInRecord("pnd_Old_Prev_Dist_Cde", "#OLD-PREV-DIST-CDE", FieldType.STRING, 4);
        pnd_Old_Curr_Dist_Cde = localVariables.newFieldInRecord("pnd_Old_Curr_Dist_Cde", "#OLD-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Old_Res_Cde = localVariables.newFieldInRecord("pnd_Old_Res_Cde", "#OLD-RES-CDE", FieldType.STRING, 3);
        pnd_Pgm = localVariables.newFieldInRecord("pnd_Pgm", "#PGM", FieldType.STRING, 8);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Parm_Invrse_Dte = localVariables.newFieldInRecord("pnd_Parm_Invrse_Dte", "#PARM-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Parm_Trans_Dte = localVariables.newFieldInRecord("pnd_Parm_Trans_Dte", "#PARM-TRANS-DTE", FieldType.TIME);
        pnd_Parm_Check_Dte = localVariables.newFieldInRecord("pnd_Parm_Check_Dte", "#PARM-CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Todays_Dte = localVariables.newFieldInRecord("pnd_Parm_Todays_Dte", "#PARM-TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Parm_Sub_Cde = localVariables.newFieldInRecord("pnd_Parm_Sub_Cde", "#PARM-SUB-CDE", FieldType.STRING, 3);
        pnd_Parm_Trans_Cde = localVariables.newFieldInRecord("pnd_Parm_Trans_Cde", "#PARM-TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_New_Orgn = localVariables.newFieldInRecord("pnd_New_Orgn", "#NEW-ORGN", FieldType.NUMERIC, 2);
        pnd_Old_Orgn = localVariables.newFieldInRecord("pnd_Old_Orgn", "#OLD-ORGN", FieldType.NUMERIC, 2);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 7);
        pnd_Updated_Cnt = localVariables.newFieldInRecord("pnd_Updated_Cnt", "#UPDATED-CNT", FieldType.NUMERIC, 7);
        pnd_Contract_Not_Found = localVariables.newFieldInRecord("pnd_Contract_Not_Found", "#CONTRACT-NOT-FOUND", FieldType.NUMERIC, 7);
        pnd_Invalid_Opt_Code = localVariables.newFieldInRecord("pnd_Invalid_Opt_Code", "#INVALID-OPT-CODE", FieldType.NUMERIC, 7);
        pnd_Ssn_Exists = localVariables.newFieldInRecord("pnd_Ssn_Exists", "#SSN-EXISTS", FieldType.NUMERIC, 7);
        pnd_Cntrct = localVariables.newFieldArrayInRecord("pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 4));
        pnd_C_Cntrct = localVariables.newFieldArrayInRecord("pnd_C_Cntrct", "#C-CNTRCT", FieldType.STRING, 10, new DbsArrayController(1, 9));
        pnd_2nd_Xref = localVariables.newFieldArrayInRecord("pnd_2nd_Xref", "#2ND-XREF", FieldType.STRING, 9, new DbsArrayController(1, 9));
        pnd_C_Fund = localVariables.newFieldArrayInRecord("pnd_C_Fund", "#C-FUND", FieldType.STRING, 3, new DbsArrayController(1, 10));
        pnd_C_Rate = localVariables.newFieldArrayInRecord("pnd_C_Rate", "#C-RATE", FieldType.STRING, 2, new DbsArrayController(1, 10));
        pnd_C_Pins = localVariables.newFieldArrayInRecord("pnd_C_Pins", "#C-PINS", FieldType.NUMERIC, 7, new DbsArrayController(1, 10));
        pnd_Cp = localVariables.newFieldInRecord("pnd_Cp", "#CP", FieldType.STRING, 12);

        pnd_Cp__R_Field_13 = localVariables.newGroupInRecord("pnd_Cp__R_Field_13", "REDEFINE", pnd_Cp);
        pnd_Cp_Pnd_Cp_Cntrct = pnd_Cp__R_Field_13.newFieldInGroup("pnd_Cp_Pnd_Cp_Cntrct", "#CP-CNTRCT", FieldType.STRING, 10);
        pnd_Cp_Pnd_Cp_Payee = pnd_Cp__R_Field_13.newFieldInGroup("pnd_Cp_Pnd_Cp_Payee", "#CP-PAYEE", FieldType.NUMERIC, 2);
        pnd_H_Cp = localVariables.newFieldInRecord("pnd_H_Cp", "#H-CP", FieldType.STRING, 12);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_14", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_O_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_14.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_O_Ppcn_Nbr", "#O-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_O_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_14.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_O_Payee", "#O-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_14.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Invrse_Lst_Pd_Dte", 
            "#FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_14.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", 
            FieldType.STRING, 3);
        pnd_Cntrct_Pymnt_Mthd = localVariables.newFieldInRecord("pnd_Cntrct_Pymnt_Mthd", "#CNTRCT-PYMNT-MTHD", FieldType.STRING, 1);
        pnd_Cntrct_Scnd_Annt_Xref_Ind = localVariables.newFieldInRecord("pnd_Cntrct_Scnd_Annt_Xref_Ind", "#CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 
            9);
        pls_S_Invrse_Dte = WsIndependent.getInstance().newFieldInRecord("pls_S_Invrse_Dte", "+S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_cpr2.reset();
        vw_dc.reset();
        vw_settl.reset();
        vw_xfr.reset();

        ldaIaal999.initializeValues();
        ldaIaal205a.initializeValues();

        localVariables.reset();
        pnd_Pin_Taxid_Seq_Cntrct_Key.setInitialValue("1337783");
        pnd_Pin_Taxid_Seq_Key.setInitialValue("1337783");
        pnd_Cpr_Bfre_Key.setInitialValue("1");
        pnd_Tiaa_Fund_Bfre_Key_2.setInitialValue("1");
        pnd_Parm_Sub_Cde.setInitialValue("U");
        pnd_Parm_Trans_Cde.setInitialValue(304);
        pnd_New_Orgn.setInitialValue(50);
        pnd_Cntrct.getValue(1).setInitialValue("IG671556");
        pnd_Cntrct.getValue(2).setInitialValue("IG716849");
        pnd_Cntrct.getValue(3).setInitialValue("W0588209");
        pnd_Cntrct.getValue(4).setInitialValue("W0671526");
        pnd_C_Cntrct.getValue(1).setInitialValue("Z2537158");
        pnd_C_Cntrct.getValue(2).setInitialValue("Z2101856");
        pnd_C_Cntrct.getValue(3).setInitialValue("Z2101831");
        pnd_C_Cntrct.getValue(4).setInitialValue("Z2089150");
        pnd_C_Cntrct.getValue(5).setInitialValue("Y2537150");
        pnd_C_Cntrct.getValue(6).setInitialValue("Y2101833");
        pnd_C_Cntrct.getValue(7).setInitialValue("Y2089152");
        pnd_C_Cntrct.getValue(8).setInitialValue("Y2030149");
        pnd_C_Cntrct.getValue(9).setInitialValue("Z2030147");
        pnd_2nd_Xref.getValue(1).setInitialValue("OCONNORWJ");
        pnd_2nd_Xref.getValue(2).setInitialValue("OCONNORME");
        pnd_2nd_Xref.getValue(3).setInitialValue("OCONNORME");
        pnd_2nd_Xref.getValue(4).setInitialValue("OCONNORME");
        pnd_2nd_Xref.getValue(5).setInitialValue("OCONNORWJ");
        pnd_2nd_Xref.getValue(6).setInitialValue("OCONNORME");
        pnd_2nd_Xref.getValue(7).setInitialValue("OCONNORME");
        pnd_2nd_Xref.getValue(8).setInitialValue("OCONNORWJ");
        pnd_2nd_Xref.getValue(9).setInitialValue("OCONNORWJ");
        pnd_C_Fund.getValue(1).setInitialValue("208");
        pnd_C_Fund.getValue(2).setInitialValue("202");
        pnd_C_Fund.getValue(3).setInitialValue("206");
        pnd_C_Fund.getValue(4).setInitialValue("208");
        pnd_C_Fund.getValue(5).setInitialValue("207");
        pnd_C_Fund.getValue(6).setInitialValue("204");
        pnd_C_Fund.getValue(7).setInitialValue("204");
        pnd_C_Fund.getValue(8).setInitialValue("204");
        pnd_C_Fund.getValue(9).setInitialValue("204");
        pnd_C_Fund.getValue(10).setInitialValue("204");
        pnd_C_Rate.getValue(1).setInitialValue("16");
        pnd_C_Rate.getValue(2).setInitialValue("38");
        pnd_C_Rate.getValue(3).setInitialValue("17");
        pnd_C_Rate.getValue(4).setInitialValue("16");
        pnd_C_Rate.getValue(5).setInitialValue("15");
        pnd_C_Rate.getValue(6).setInitialValue("18");
        pnd_C_Rate.getValue(7).setInitialValue("18");
        pnd_C_Rate.getValue(8).setInitialValue("18");
        pnd_C_Rate.getValue(9).setInitialValue("18");
        pnd_C_Rate.getValue(10).setInitialValue("18");
        pnd_C_Pins.getValue(1).setInitialValue(1709714);
        pnd_C_Pins.getValue(2).setInitialValue(1709714);
        pnd_C_Pins.getValue(3).setInitialValue(2333530);
        pnd_C_Pins.getValue(4).setInitialValue(1604720);
        pnd_C_Pins.getValue(5).setInitialValue(1604720);
        pnd_C_Pins.getValue(6).setInitialValue(1494491);
        pnd_C_Pins.getValue(7).setInitialValue(1689845);
        pnd_C_Pins.getValue(8).setInitialValue(1354759);
        pnd_C_Pins.getValue(9).setInitialValue(1488736);
        pnd_C_Pins.getValue(10).setInitialValue(1715374);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap999() throws Exception
    {
        super("Iaap999");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP999", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133 ZP = ON;//Natural: FORMAT ( 2 ) PS = 0 LS = 133 ZP = ON
        //*  DETERMINE IF THERE's a program to be fetched
        DbsUtil.callnat(Iaan999r.class , getCurrentProcessState(), pnd_Pgm, pnd_Rc);                                                                                      //Natural: CALLNAT 'IAAN999R' #PGM #RC
        if (condition(Global.isEscape())) return;
        //*  THERE's a program to be fetched
        if (condition(pnd_Rc.equals(getZero())))                                                                                                                          //Natural: IF #RC = 0
        {
            Global.setFetchProgram(DbsUtil.getBlType(pnd_Pgm));                                                                                                           //Natural: FETCH #PGM
            if (condition(Global.isEscape())) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  06/06/16 - SO THE PROGRAM WILL NOT EXECUTE THE CODE BELOW.
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //*  IF NO PROGRAM TO BE FETCHED, CONTINUE WITH THIS MODULE
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Parm_Check_Dte.compute(new ComputeParameters(false, pnd_Parm_Check_Dte), pnd_Date.val());                                                                 //Natural: ASSIGN #PARM-CHECK-DTE := VAL ( #DATE )
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Parm_Todays_Dte.compute(new ComputeParameters(false, pnd_Parm_Todays_Dte), pnd_Date.val());                                                               //Natural: ASSIGN #PARM-TODAYS-DTE := VAL ( #DATE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Parm_Trans_Cde.setValue(50);                                                                                                                                  //Natural: ASSIGN #PARM-TRANS-CDE := 50
        pnd_Parm_Sub_Cde.reset();                                                                                                                                         //Natural: RESET #PARM-SUB-CDE #TRANS-CMBNE-CDE
        pnd_Trans_Cmbne_Cde.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 4
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT ( #I )
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct.getValue(pnd_I), WcType.WITH) },
            1
            );
            FIND01:
            while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FIND01")))
            {
                ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                pnd_Cntrct_Pymnt_Mthd.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Pymnt_Mthd());                                                                             //Natural: ASSIGN #CNTRCT-PYMNT-MTHD := CNTRCT-PYMNT-MTHD
                ldaIaal999.getIaa_Cntrct_Cntrct_Pymnt_Mthd().reset();                                                                                                     //Natural: RESET CNTRCT-PYMNT-MTHD
                ldaIaal999.getVw_iaa_Cntrct().updateDBRow("FIND01");                                                                                                      //Natural: UPDATE
                pnd_Cntrct_Scnd_Annt_Xref_Ind.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                                             //Natural: ASSIGN #CNTRCT-SCND-ANNT-XREF-IND := CNTRCT-SCND-ANNT-XREF-IND
                                                                                                                                                                          //Natural: PERFORM DISPLAY-IT
                sub_Display_It();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 9
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #C-CNTRCT ( #I )
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_C_Cntrct.getValue(pnd_I), WcType.WITH) },
            1
            );
            FIND02:
            while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FIND02")))
            {
                ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
                pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                                //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := CNTRCT-PPCN-NBR
                pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                        //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 01
                                                                                                                                                                          //Natural: PERFORM CREATE-BEFORE-IMAGE
                sub_Create_Before_Image();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cntrct_Scnd_Annt_Xref_Ind.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());                                                             //Natural: ASSIGN #CNTRCT-SCND-ANNT-XREF-IND := CNTRCT-SCND-ANNT-XREF-IND
                ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind().setValue(pnd_2nd_Xref.getValue(pnd_I));                                                              //Natural: ASSIGN CNTRCT-SCND-ANNT-XREF-IND := #2ND-XREF ( #I )
                ldaIaal999.getVw_iaa_Cntrct().updateDBRow("FIND02");                                                                                                      //Natural: UPDATE
                pnd_Cntrct_Pymnt_Mthd.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Pymnt_Mthd());                                                                             //Natural: ASSIGN #CNTRCT-PYMNT-MTHD := CNTRCT-PYMNT-MTHD
                                                                                                                                                                          //Natural: PERFORM DISPLAY-IT
                sub_Display_It();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.callnat(Iaan060y.class , getCurrentProcessState(), pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr, pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde,                   //Natural: CALLNAT 'IAAN060Y' #CNTRCT-PART-PPCN-NBR #CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE
                    pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte);
                if (condition(Global.isEscape())) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        vw_dc.startDatabaseRead                                                                                                                                           //Natural: READ DC BY PIN-TAXID-SEQ-CNTRCT-KEY
        (
        "READ02",
        new Oc[] { new Oc("PIN_TAXID_SEQ_CNTRCT_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_dc.readNextRow("READ02")))
        {
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  STARTING FROM #PIN-TAXID-SEQ-CNTRCT-KEY
            if (condition(dc_Cntrct_Id_Nbr.notEquals(pnd_Pin_Taxid_Seq_Cntrct_Key_Pnd_Cntrct_Id_Nbr)))                                                                    //Natural: IF CNTRCT-ID-NBR NE #CNTRCT-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            vw_dc.deleteDBRow("READ02");                                                                                                                                  //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            getReports().write(1, "DC RECORD:",dc_Cntrct_Id_Nbr,"purged successfully");                                                                                   //Natural: WRITE ( 1 ) 'DC RECORD:' CNTRCT-ID-NBR 'purged successfully'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_settl.startDatabaseRead                                                                                                                                        //Natural: READ SETTL BY PIN-TAXID-SEQ-KEY
        (
        "READ03",
        new Oc[] { new Oc("PIN_TAXID_SEQ_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_settl.readNextRow("READ03")))
        {
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  STARTING FROM #PIN-TAXID-SEQ-KEY
            if (condition(settl_Sttlmnt_Id_Nbr.notEquals(pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr)))                                                                      //Natural: IF STTLMNT-ID-NBR NE #STTLMNT-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            vw_settl.deleteDBRow("READ03");                                                                                                                               //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            getReports().write(1, "SETTL RECORD:",settl_Sttlmnt_Id_Nbr,"purged successfully");                                                                            //Natural: WRITE ( 1 ) 'SETTL RECORD:' STTLMNT-ID-NBR 'purged successfully'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *STOP
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ04",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ04")))
        {
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Parm_Check_Dte.compute(new ComputeParameters(false, pnd_Parm_Check_Dte), pnd_Date.val());                                                                 //Natural: ASSIGN #PARM-CHECK-DTE := VAL ( #DATE )
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Parm_Todays_Dte.compute(new ComputeParameters(false, pnd_Parm_Todays_Dte), pnd_Date.val());                                                               //Natural: ASSIGN #PARM-TODAYS-DTE := VAL ( #DATE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Parm_Trans_Cde.setValue(50);                                                                                                                                  //Natural: ASSIGN #PARM-TRANS-CDE := 50
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            pnd_Cp.setValue(pnd_C_Cntrct.getValue(pnd_I));                                                                                                                //Natural: ASSIGN #CP := #C-CNTRCT ( #I )
            //* *#CP-CNTRCT := 'Z0007402'
            //* *#CP-PAYEE := 01
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTRACT-CPR
            sub_Update_Contract_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_H_Cp.notEquals(pnd_Cp)))                                                                                                                    //Natural: IF #H-CP NE #CP
            {
                pnd_H_Cp.setValue(pnd_Cp);                                                                                                                                //Natural: ASSIGN #H-CP := #CP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        pnd_Ia_Super_De_01_Pnd_Rqst_Effctv_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),"20120331");                                                                 //Natural: MOVE EDITED '20120331' TO #RQST-EFFCTV-DTE ( EM = YYYYMMDD )
        pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),"20120331");                                                                                                  //Natural: MOVE EDITED '20120331' TO #D ( EM = YYYYMMDD )
        pnd_U.setValueEdited(new ReportEditMask("YYYYMMDD"),"20120330");                                                                                                  //Natural: MOVE EDITED '20120330' TO #U ( EM = YYYYMMDD )
        //* *PERFORM GET-XFR
        //* *STOP
        //* *#TRANS-CHECK-DTE := 20111101
        ldaIaal999.getVw_iaa_Trans_Rcrd().startDatabaseRead                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "READ05",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ05:
        while (condition(ldaIaal999.getVw_iaa_Trans_Rcrd().readNextRow("READ05")))
        {
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            if (condition(ldaIaal999.getIaa_Trans_Rcrd_Trans_Check_Dte().greater(pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte)))                                            //Natural: IF TRANS-CHECK-DTE GT #TRANS-CHECK-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde().equals(66) && ldaIaal999.getIaa_Trans_Rcrd_Trans_Todays_Dte().equals(20110923) &&                    //Natural: ACCEPT IF TRANS-CDE = 66 AND TRANS-TODAYS-DTE = 20110923 AND TRANS-USER-ID = 'BERWYN'
                ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Id().equals("BERWYN"))))
            {
                continue;
            }
            vw_dc.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) DC BY CNTRCT-PPCN-NBR STARTING FROM TRANS-PPCN-NBR
            (
            "R0",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
            1
            );
            R0:
            while (condition(vw_dc.readNextRow("R0")))
            {
                if (condition(dc_Cntrct_Ppcn_Nbr.notEquals(ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr())))                                                               //Natural: IF CNTRCT-PPCN-NBR NE TRANS-PPCN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(dc_Cntrct_Payee_Cde.equals(0)))                                                                                                             //Natural: IF CNTRCT-PAYEE-CDE = 00
                {
                    pnd_Old_Py.setValue(dc_Cntrct_Payee_Cde);                                                                                                             //Natural: ASSIGN #OLD-PY := CNTRCT-PAYEE-CDE
                    dc_Cntrct_Payee_Cde.setValue(1);                                                                                                                      //Natural: ASSIGN CNTRCT-PAYEE-CDE := 01
                    vw_dc.updateDBRow("R0");                                                                                                                              //Natural: UPDATE ( R0. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    getReports().display(0, "Contract",                                                                                                                   //Natural: DISPLAY ( 0 ) 'Contract' TRANS-PPCN-NBR 'Py' TRANS-PAYEE-CDE 'Trans' TRANS-CDE 'Todays Dte' TRANS-TODAYS-DTE 'User' TRANS-USER-ID 'User Area' TRANS-USER-AREA 'Old Py' #OLD-PY 'New Py' CNTRCT-PAYEE-CDE
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(),"Py",
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_Payee_Cde(),"Trans",
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde(),"Todays Dte",
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_Todays_Dte(),"User",
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Id(),"User Area",
                    		ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Area(),"Old Py",
                    		pnd_Old_Py,"New Py",
                    		dc_Cntrct_Payee_Cde);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R0"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R0"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *STOP
        //* *READ WORK FILE 1 #INPUT
        //*   WRITE '---------------------------------------------------'
        //*   ADD 1 TO #READ-CNT
        //*   PERFORM UPDATE-CONTRACT-CPR
        //* *END-WORK
        //* *WRITE (2) // '='  #READ-CNT /
        //*   '=' #UPDATED-CNT  /
        //*   '=' #INVALID-OPT-CODE  /
        //*   '=' #SSN-EXISTS  /
        //*   '=' #CONTRACT-NOT-FOUND
        //* *#CP-CNTRCT := 'S0128781'
        //* *#CP-PAYEE := 01
        //* *PERFORM UPDATE-CONTRACT-CPR
        //* *FOR #I 1 TO 4
        //* *#CP := #CNTRCT(#I)
        pnd_Cp.setValue("Y0553233");                                                                                                                                      //Natural: ASSIGN #CP := 'Y0553233'
        pnd_Cp_Pnd_Cp_Payee.setValue(1);                                                                                                                                  //Natural: ASSIGN #CP-PAYEE := 01
        pnd_Input_Pnd_I_Cntrct.setValue(pnd_Cp_Pnd_Cp_Cntrct);                                                                                                            //Natural: ASSIGN #I-CNTRCT := #CP-CNTRCT
        //*  #TRANS-CMBNE-CDE := #C-CNTRCT(#I)
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTRACT-CPR
        sub_Update_Contract_Cpr();
        if (condition(Global.isEscape())) {return;}
        //* *END-FOR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTRACT-CPR
        //* ***********************************************************************
        //*  WRITE '=' #I-CNTRCT ' BEFORE IMAGE CPR'
        //*  F1.
        //*  FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-PART-PPCN-NBR
        //*    IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE := 201008
        //*    IF NO RECORD FOUND
        //*      WRITE (1) #I-CNTRCT '01' #I-SSN ' CONTRACT NOT FOUND'
        //*      ADD 1 TO #CONTRACT-NOT-FOUND
        //*      BACKOUT TRANSACTION
        //*      ESCAPE ROUTINE IMMEDIATE
        //*    END-NOREC
        //*    IF CNTRCT-OPTN-CDE = 01 OR = 02 OR = 05 OR =06 OR = 09
        //*        OR = 21 OR = 22 OR = 23 OR =25 OR = 27 OR = 28 OR = 30
        //*        OR = 32 OR = 50
        //*      WRITE (1)  CPR.CNTRCT-PART-PPCN-NBR(AL=8)
        //*        CPR.CNTRCT-PART-PAYEE-CDE(EM=99)
        //*        'OPTION' CNTRCT-OPTN-CDE(EM=99)
        //*        'REASON' 'SINGLE LIFE CONTRACT'
        //*      ADD 1 TO #INVALID-OPT-CODE
        //*      BACKOUT TRANSACTION
        //*      ESCAPE ROUTINE IMMEDIATE
        //*    END-IF
        //*    IF CNTRCT-SCND-ANNT-SSN = #I-SSN
        //*      WRITE  (1)
        //*        CPR.CNTRCT-PART-PPCN-NBR(AL=8)
        //*        CPR.CNTRCT-PART-PAYEE-CDE(EM=99)
        //*        'OLD/SSN2' CNTRCT-SCND-ANNT-SSN (EM=999999999)
        //*        'NEW/SSN2' #I-SSN   (EM=999999999)   'SSN ALREADY EXISTS'
        //*      ADD 1 TO #SSN-EXISTS
        //*      BACKOUT TRANSACTION
        //*      ESCAPE ROUTINE IMMEDIATE
        //*    END-IF
        //*    WRITE '=' #I-CNTRCT ' FIND 2 + UPDATE'
        //*    DISPLAY(2)
        //*      '/CNTRCT' CPR.CNTRCT-PART-PPCN-NBR(AL=8)
        //*      '/PY'  CPR.CNTRCT-PART-PAYEE-CDE(EM=99)
        //*      'OLD/SSN2' CNTRCT-SCND-ANNT-SSN
        //*      'NEW/SSN2' #I-SSN   (EM=999999999)
        //*    CNTRCT-SCND-ANNT-SSN := #I-SSN
        //*    UPDATE(F1.)
        //*    ADD 1 TO #UPDATED-CNT
        //*  END-FIND
        //*  BACKOUT TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BEFORE-IMAGE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-XFR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RCRD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUNDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-IT
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Update_Contract_Cpr() throws Exception                                                                                                               //Natural: UPDATE-CONTRACT-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Cp_Pnd_Cp_Cntrct);                                                                                              //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := #CP-CNTRCT
        pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(pnd_Cp_Pnd_Cp_Payee);                                                                                              //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := #CP-PAYEE
        //* *WRITE '=' #I-CNTRCT ' READ'
        ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FC",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) }
        );
        FC:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("FC", true)))
        {
            ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal999.getVw_cpr().getAstCOUNTER().equals(0)))                                                                                              //Natural: IF NO RECORD FOUND
            {
                //*    WRITE (1) #I-CNTRCT '01' #I-SSN (EM=999999999) ' CONTRACT NOT FOUND'
                //*    ADD 1 TO #CONTRACT-NOT-FOUND
                getReports().write(1, pnd_Cp_Pnd_Cp_Cntrct,pnd_Cp_Pnd_Cp_Payee," CONTRACT NOT FOUND");                                                                    //Natural: WRITE ( 1 ) #CP-CNTRCT #CP-PAYEE ' CONTRACT NOT FOUND'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  WRITE '=' #I-CNTRCT ' FIND 1'
            //*  FIND CPR2 WITH CNTRCT-PAYEE-KEY = #CPR-KEY
            //*  END-FIND
            //*  CPR.CNTRCT-PART-PAYEE-CDE := #CP-PAYEE
            //*  MOVE EDITED CPR.CPR-ID-NBR(EM=9999999) TO
            //*    IAA-TRANS-RCRD.TRANS-VERIFY-ID
            //*  CPR.PRTCPNT-TAX-ID-NBR := CPR2.PRTCPNT-TAX-ID-NBR
            //*  CPR.CNTRCT-ACTVTY-CDE := 1
            //*  STORE CPR
            //*  #PARM-TRANS-DTE := *TIMX
            //*  COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
            //*  IF #PARM-INVRSE-DTE = +S-INVRSE-DTE
            //*    REPEAT
            //*      #PARM-TRANS-DTE := *TIMX
            //*      COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
            //*      IF #PARM-INVRSE-DTE NE +S-INVRSE-DTE
            //*        ESCAPE BOTTOM
            //*      END-IF
            //*    END-REPEAT
            //*  END-IF
            //*  +S-INVRSE-DTE := #PARM-INVRSE-DTE
            //*  IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
            //*  IAA-TRANS-RCRD.TRANS-DTE := #PARM-TRANS-DTE
            //*  IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
            //*  IAA-TRANS-RCRD.TRANS-PPCN-NBR := #CNTRCT-PART-PPCN-NBR
            //*  IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #CNTRCT-PART-PAYEE-CDE
            //*  IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #CP-PAYEE
            //*  IAA-TRANS-RCRD.TRANS-CHECK-DTE := #PARM-CHECK-DTE
            //*  IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
            //*  IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #PARM-TODAYS-DTE
            //*  IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
            //*  IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP999'
            //*  IAA-TRANS-RCRD.TRANS-CDE := #PARM-TRANS-CDE
            //*  IAA-TRANS-RCRD.TRANS-SUB-CDE := #PARM-SUB-CDE
            //*  IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #TRANS-CMBNE-CDE
            //*  STORE IAA-TRANS-RCRD
            if (condition(pnd_H_Cp.notEquals(pnd_Cp)))                                                                                                                    //Natural: IF #H-CP NE #CP
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-BEFORE-IMAGE
                sub_Create_Before_Image();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal999.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal999.getVw_cpr());                                                                                 //Natural: MOVE BY NAME CPR TO IAA-CPR-TRANS
                ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                             //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
                ldaIaal999.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := '1'
                ldaIaal999.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := ' '
                ldaIaal999.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                               //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := #PARM-CHECK-DTE
                ldaIaal999.getIaa_Cpr_Trans_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                               //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
                ldaIaal999.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                           //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
                ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CPR-TRANS
                ldaIaal999.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := ' '
                ldaIaal999.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := '2'
                ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                           //Natural: STORE IAA-CPR-TRANS
                ldaIaal999.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue(" ");                                                                                              //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := ' '
                ldaIaal999.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                              //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := '2'
                ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                        //Natural: STORE IAA-CNTRCT-TRANS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(pnd_Cp_Pnd_Cp_Cntrct);                                                                             //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := #CP-CNTRCT
            pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(pnd_Cp_Pnd_Cp_Payee);                                                                             //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := #CP-PAYEE
            pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde.setValue(pnd_C_Fund.getValue(pnd_I));                                                                        //Natural: ASSIGN #TIAA-CMPNY-FUND-CDE := #C-FUND ( #I )
            ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().startDatabaseFind                                                                                                       //Natural: FIND IAA-CREF-FUND-RCRD WITH CREF-CNTRCT-FUND-KEY = #TIAA-CNTRCT-FUND-KEY
            (
            "R1",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", pnd_Tiaa_Cntrct_Fund_Key, WcType.WITH) }
            );
            R1:
            while (condition(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().readNextRow("R1")))
            {
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().setIfNotFoundControlFlag(false);
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());                                                            //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Trans_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                       //Natural: ASSIGN IAA-CREF-FUND-TRANS.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID := '1'
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue(" ");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID := ' '
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-CHECK-DTE := #PARM-CHECK-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                         //Natural: ASSIGN IAA-CREF-FUND-TRANS.TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
                ldaIaal999.getIaa_Cref_Fund_Trans_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                     //Natural: ASSIGN IAA-CREF-FUND-TRANS.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1).setValue(pnd_C_Rate.getValue(pnd_I));                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 ) := #C-RATE ( #I )
                ldaIaal999.getVw_iaa_Cref_Fund_Rcrd().updateDBRow("R1");                                                                                                  //Natural: UPDATE ( R1. )
                ldaIaal999.getIaa_Cref_Fund_Trans_Bfre_Imge_Id().setValue(" ");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.BFRE-IMGE-ID := ' '
                ldaIaal999.getIaa_Cref_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                           //Natural: ASSIGN IAA-CREF-FUND-TRANS.AFTR-IMGE-ID := '2'
                ldaIaal999.getIaa_Cref_Fund_Trans_Cref_Rate_Cde().getValue(1).setValue(pnd_C_Rate.getValue(pnd_I));                                                       //Natural: ASSIGN IAA-CREF-FUND-TRANS.CREF-RATE-CDE ( 1 ) := #C-RATE ( #I )
                ldaIaal999.getVw_iaa_Cref_Fund_Trans().insertDBRow();                                                                                                     //Natural: STORE IAA-CREF-FUND-TRANS
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().display(4, "/CNTRCT",                                                                                                                        //Natural: DISPLAY ( 4 ) '/CNTRCT' CREF-CNTRCT-PPCN-NBR ( AL = 8 ) '/PY' CREF-CNTRCT-PAYEE-CDE ( EM = 99 ) '/FUND' IAA-CREF-FUND-RCRD.CREF-CMPNY-FUND-CDE '/RATE' IAA-CREF-FUND-RCRD.CREF-RATE-CDE ( 1 )
                		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"/PY",
                		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"/FUND",
                		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde(),"/RATE",
                		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde().getValue(1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      'NEW/SSN2' #I-SSN   (EM=999999999)
                //*      STARTING FROM #TIAA-CNTRCT-FUND-KEY
                //*  IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT-PPCN-NBR OR
                //*      IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #TIAA-CNTRCT-PAYEE-CDE
                //*      ESCAPE BOTTOM
                //*    END-IF
                //*    IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := #CP-PAYEE
                //*    STORE IAA-TIAA-FUND-RCRD
                //*    ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FC"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  #UPDT := TRUE
            //*  CPR.CNTRCT-PEND-CDE := '0'
            //*  CPR.CNTRCT-PEND-DTE := 0
            //*  CPR.CNTRCT-FINAL-PER-PAY-DTE := 999999
            //*  CPR.CNTRCT-FINAL-PAY-DTE := 99999999
            //*  IF CPR.CPR-ID-NBR NE 1598135
            //*  CPR.CPR-ID-NBR := 1312534
            //*  END-IF
            //*  IF CPR.PRTCPNT-TAX-ID-NBR NE 574096809
            //*  CPR.PRTCPNT-TAX-ID-NBR := 376362373
            //*  END-IF
            //*  CPR.CNTRCT-ACTVTY-CDE := 9
            //*  CPR.CPR-ID-NBR := #C-PINS(#I)
            //*  CPR.CNTRCT-PEND-CDE := 'A'
            ldaIaal999.getCpr_Cntrct_Cmbne_Cde().reset();                                                                                                                 //Natural: RESET CPR.CNTRCT-CMBNE-CDE
            ldaIaal999.getVw_cpr().updateDBRow("FC");                                                                                                                     //Natural: UPDATE ( FC. )
            //*  #CNTRCT-PART-PAYEE-CDE := #CP-PAYEE
            //*  IF #CP-PAYEE = 01
            //*    GXX.
            //*    GET IAA-CNTRCT #C-ISN
            //*    IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE :=
            //*      IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN := 555091684
            //*    RESET IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            //*    UPDATE(GXX.)
            //*  END-IF
            //*  CALLNAT 'IAAN060Y'
            //*    #CNTRCT-PART-PPCN-NBR
            //*    #CNTRCT-PART-PAYEE-CDE
            //*    #PARM-INVRSE-DTE
            //*    #PARM-TRANS-DTE
            //*    #PARM-CHECK-DTE
            //*  WRITE '=' #I-CNTRCT ' AFTER IMAGE'
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            getReports().write(1, "Contract Payee:",pnd_Cp_Pnd_Cp_Cntrct,pnd_Cp_Pnd_Cp_Payee, new ReportEditMask ("99"),"Combine Code",ldaIaal999.getCpr_Cntrct_Cmbne_Cde(), //Natural: WRITE ( 1 ) 'Contract Payee:' #CP-CNTRCT #CP-PAYEE ( EM = 99 ) 'Combine Code' CPR.CNTRCT-CMBNE-CDE 'modified successfully'
                "modified successfully");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FC"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FC"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DISPLAY(2)
            //*    '/CNTRCT' CPR.CNTRCT-PART-PPCN-NBR(AL=8)
            //*    '/PY'  CPR.CNTRCT-PART-PAYEE-CDE(EM=99)
            //*    'OLD/SSN2' CPR2.PRTCPNT-TAX-ID-NBR
            //*    'NEW/SSN2' CPR.PRTCPNT-TAX-ID-NBR
            //*    'New/SSN' CPR.PRTCPNT-RSDNCY-CDE
            //*    'Old/Prev' #OLD-PREV-DIST-CDE
            //*    'Old/Curr' #OLD-CURR-DIST-CDE
            //*    'Prev/Dest' CPR.CNTRCT-PREV-DIST-CDE
            //*    'Curr/Dest' CPR.CNTRCT-CURR-DIST-CDE
            //*    '/DOD' IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE
            //*  ADD 1 TO #CPR-CNT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CREATE 03, 04, 05 PAYEES
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().setValue(3);                                                                                                            //Natural: ASSIGN CPR.CNTRCT-PART-PAYEE-CDE := 03
        ldaIaal999.getCpr_Cntrct_Actvty_Cde().setValue(1);                                                                                                                //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE := 1
        ldaIaal999.getCpr_Cntrct_Pend_Cde().setValue("A");                                                                                                                //Natural: ASSIGN CPR.CNTRCT-PEND-CDE := 'A'
        ldaIaal999.getCpr_Cntrct_Pend_Dte().setValue(201212);                                                                                                             //Natural: ASSIGN CPR.CNTRCT-PEND-DTE := 201212
        ldaIaal999.getVw_cpr().insertDBRow();                                                                                                                             //Natural: STORE CPR
        pnd_Parm_Trans_Cde.setValue(33);                                                                                                                                  //Natural: ASSIGN #PARM-TRANS-CDE := 033
        //* *#PARM-SUB-CDE := '066'
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
        sub_Create_Trans_Rcrd();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-FUNDS
        sub_Create_Funds();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Iaan060y.class , getCurrentProcessState(), ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),                   //Natural: CALLNAT 'IAAN060Y' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE
            pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte);
        if (condition(Global.isEscape())) return;
        getReports().write(1, "CPR/Fund for:",ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),"created successfully");                 //Natural: WRITE ( 1 ) 'CPR/Fund for:' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE 'created successfully'
        if (Global.isEscape()) return;
        ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().setValue(4);                                                                                                            //Natural: ASSIGN CPR.CNTRCT-PART-PAYEE-CDE := 04
        ldaIaal999.getVw_cpr().insertDBRow();                                                                                                                             //Natural: STORE CPR
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
        sub_Create_Trans_Rcrd();
        if (condition(Global.isEscape())) {return;}
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(4);                                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 04
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TIAA-FUND-RCRD
        DbsUtil.callnat(Iaan060y.class , getCurrentProcessState(), ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),                   //Natural: CALLNAT 'IAAN060Y' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE
            pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte);
        if (condition(Global.isEscape())) return;
        getReports().write(1, "CPR/Fund for:",ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),"created successfully");                 //Natural: WRITE ( 1 ) 'CPR/Fund for:' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE 'created successfully'
        if (Global.isEscape()) return;
        ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().setValue(5);                                                                                                            //Natural: ASSIGN CPR.CNTRCT-PART-PAYEE-CDE := 05
        ldaIaal999.getVw_cpr().insertDBRow();                                                                                                                             //Natural: STORE CPR
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
        sub_Create_Trans_Rcrd();
        if (condition(Global.isEscape())) {return;}
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(5);                                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 05
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(3).nadd(0);                                                                                          //Natural: ADD .01 TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 3 )
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * )
            DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*")));
        ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * )
            DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*")));
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TIAA-FUND-RCRD
        DbsUtil.callnat(Iaan060y.class , getCurrentProcessState(), ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(), ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),                   //Natural: CALLNAT 'IAAN060Y' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE #PARM-INVRSE-DTE #PARM-TRANS-DTE #PARM-CHECK-DTE
            pnd_Parm_Invrse_Dte, pnd_Parm_Trans_Dte, pnd_Parm_Check_Dte);
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, "CPR/Fund for:",ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),"created successfully");                 //Natural: WRITE ( 1 ) 'CPR/Fund for:' CPR.CNTRCT-PART-PPCN-NBR CPR.CNTRCT-PART-PAYEE-CDE 'created successfully'
        if (Global.isEscape()) return;
    }
    private void sub_Create_Before_Image() throws Exception                                                                                                               //Natural: CREATE-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Trans_Dte.setValue(Global.getTIMX());                                                                                                                    //Natural: ASSIGN #PARM-TRANS-DTE := *TIMX
        pnd_Parm_Invrse_Dte.compute(new ComputeParameters(false, pnd_Parm_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Parm_Trans_Dte));                     //Natural: COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
        if (condition(pnd_Parm_Invrse_Dte.equals(pls_S_Invrse_Dte)))                                                                                                      //Natural: IF #PARM-INVRSE-DTE = +S-INVRSE-DTE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Parm_Trans_Dte.setValue(Global.getTIMX());                                                                                                            //Natural: ASSIGN #PARM-TRANS-DTE := *TIMX
                pnd_Parm_Invrse_Dte.compute(new ComputeParameters(false, pnd_Parm_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Parm_Trans_Dte));             //Natural: COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
                if (condition(pnd_Parm_Invrse_Dte.notEquals(pls_S_Invrse_Dte)))                                                                                           //Natural: IF #PARM-INVRSE-DTE NE +S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pls_S_Invrse_Dte.setValue(pnd_Parm_Invrse_Dte);                                                                                                                   //Natural: ASSIGN +S-INVRSE-DTE := #PARM-INVRSE-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Parm_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #PARM-TRANS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr);                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #CNTRCT-PART-PPCN-NBR
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde);                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #CNTRCT-PART-PAYEE-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #PARM-CHECK-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Parm_Todays_Dte);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #PARM-TODAYS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Area().setValue("BATCH");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Id().setValue("IAAP999");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP999'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde().setValue(pnd_Parm_Trans_Cde);                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := #PARM-TRANS-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue(pnd_Parm_Sub_Cde);                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := #PARM-SUB-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Cmbne_Cde().setValue(pnd_Trans_Cmbne_Cde);                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #TRANS-CMBNE-CDE
        ldaIaal999.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
        ldaIaal999.getVw_iaa_Cntrct_Trans().setValuesByName(ldaIaal999.getVw_iaa_Cntrct());                                                                               //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaIaal999.getIaa_Cntrct_Trans_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                                  //Natural: ASSIGN IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
        ldaIaal999.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := '1'
        ldaIaal999.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := ' '
        ldaIaal999.getIaa_Cntrct_Trans_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE := #PARM-CHECK-DTE
        ldaIaal999.getIaa_Cntrct_Trans_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal999.getIaa_Cntrct_Trans_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal999.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-CNTRCT-TRANS
        ldaIaal999.getVw_cpr().startDatabaseFind                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("FIND03")))
        {
            ldaIaal999.getVw_cpr().setIfNotFoundControlFlag(false);
            ldaIaal999.getVw_iaa_Cpr_Trans().setValuesByName(ldaIaal999.getVw_cpr());                                                                                     //Natural: MOVE BY NAME CPR TO IAA-CPR-TRANS
            ldaIaal999.getIaa_Cpr_Trans_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                                 //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
            ldaIaal999.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := '1'
            ldaIaal999.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                     //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := ' '
            ldaIaal999.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := #PARM-CHECK-DTE
            ldaIaal999.getIaa_Cpr_Trans_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                                   //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
            ldaIaal999.getIaa_Cpr_Trans_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                               //Natural: ASSIGN IAA-CPR-TRANS.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
            ldaIaal999.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                               //Natural: STORE IAA-CPR-TRANS
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Tiaa_Cntrct_Fund_Key.setValue(pnd_Cpr_Key);                                                                                                                   //Natural: ASSIGN #TIAA-CNTRCT-FUND-KEY := #CPR-KEY
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde.setValue(" ");                                                                                                   //Natural: ASSIGN #TIAA-CMPNY-FUND-CDE := ' '
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #TIAA-CNTRCT-FUND-KEY
        (
        "L1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Tiaa_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        L1:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("L1")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().notEquals(pnd_Cpr_Key_Pnd_Cntrct_Part_Ppcn_Nbr) || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().notEquals(pnd_Cpr_Key_Pnd_Cntrct_Part_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #CNTRCT-PART-PPCN-NBR OR IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #CNTRCT-PART-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());                                                                //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
            ldaIaal999.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                           //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
            ldaIaal999.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := '1'
            ldaIaal999.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue(" ");                                                                                               //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := ' '
            ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                             //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := #PARM-CHECK-DTE
            ldaIaal999.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                             //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
            ldaIaal999.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                         //Natural: STORE IAA-TIAA-FUND-TRANS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Xfr() throws Exception                                                                                                                           //Natural: GET-XFR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_xfr.startDatabaseRead                                                                                                                                          //Natural: READ XFR BY IA-SUPER-DE-01 STARTING FROM #IA-SUPER-DE-01
        (
        "RXX",
        new Wc[] { new Wc("IA_SUPER_DE_01", ">=", pnd_Ia_Super_De_01.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_01", "ASC") }
        );
        RXX:
        while (condition(vw_xfr.readNextRow("RXX")))
        {
            if (condition(xfr_Rqst_Effctv_Dte.greater(pnd_D)))                                                                                                            //Natural: IF RQST-EFFCTV-DTE GT #D
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(xfr_Xfr_Stts_Cde,"'F'")))                                                                                                   //Natural: IF XFR-STTS-CDE = MASK ( 'F' )
            {
                pnd_Old_D.setValue(xfr_Rqst_Effctv_Dte);                                                                                                                  //Natural: ASSIGN #OLD-D := RQST-EFFCTV-DTE
                GXFR:                                                                                                                                                     //Natural: GET XFR *ISN ( RXX. )
                vw_xfr.readByID(vw_xfr.getAstISN("RXX"), "GXFR");
                xfr_Rqst_Effctv_Dte.setValue(pnd_U);                                                                                                                      //Natural: ASSIGN RQST-EFFCTV-DTE := #U
                vw_xfr.updateDBRow("GXFR");                                                                                                                               //Natural: UPDATE ( GXFR. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().display(3, "From Cntrct",                                                                                                                    //Natural: DISPLAY ( 3 ) 'From Cntrct' IA-FRM-CNTRCT 'From Py' IA-FRM-PAYEE 'To Cntrct' IA-TO-CNTRCT 'To Py' IA-TO-PAYEE 'Stts' XFR-STTS-CDE 'Type' RCRD-TYPE-CDE 'Old Eff Dte' #OLD-D ( EM = YYYYMMDD ) 'New Eff Dte' RQST-EFFCTV-DTE ( EM = YYYYMMDD )
                		xfr_Ia_Frm_Cntrct,"From Py",
                		xfr_Ia_Frm_Payee,"To Cntrct",
                		xfr_Ia_To_Cntrct,"To Py",
                		xfr_Ia_To_Payee,"Stts",
                		xfr_Xfr_Stts_Cde,"Type",
                		xfr_Rcrd_Type_Cde,"Old Eff Dte",
                		pnd_Old_D, new ReportEditMask ("YYYYMMDD"),"New Eff Dte",
                		xfr_Rqst_Effctv_Dte, new ReportEditMask ("YYYYMMDD"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RXX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RXX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_Trans_Rcrd() throws Exception                                                                                                                 //Natural: CREATE-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Trans_Dte.setValue(Global.getTIMX());                                                                                                                    //Natural: ASSIGN #PARM-TRANS-DTE := *TIMX
        pnd_Parm_Invrse_Dte.compute(new ComputeParameters(false, pnd_Parm_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Parm_Trans_Dte));                     //Natural: COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
        if (condition(pnd_Parm_Invrse_Dte.equals(pls_S_Invrse_Dte)))                                                                                                      //Natural: IF #PARM-INVRSE-DTE = +S-INVRSE-DTE
        {
            REPEAT02:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Parm_Trans_Dte.setValue(Global.getTIMX());                                                                                                            //Natural: ASSIGN #PARM-TRANS-DTE := *TIMX
                pnd_Parm_Invrse_Dte.compute(new ComputeParameters(false, pnd_Parm_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Parm_Trans_Dte));             //Natural: COMPUTE #PARM-INVRSE-DTE = 1000000000000 - #PARM-TRANS-DTE
                if (condition(pnd_Parm_Invrse_Dte.notEquals(pls_S_Invrse_Dte)))                                                                                           //Natural: IF #PARM-INVRSE-DTE NE +S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pls_S_Invrse_Dte.setValue(pnd_Parm_Invrse_Dte);                                                                                                                   //Natural: ASSIGN +S-INVRSE-DTE := #PARM-INVRSE-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Parm_Invrse_Dte);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #PARM-INVRSE-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Parm_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #PARM-TRANS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(ldaIaal999.getIaa_Trans_Rcrd_Trans_Dte());                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr());                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := CPR.CNTRCT-PART-PPCN-NBR
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(ldaIaal999.getCpr_Cntrct_Part_Payee_Cde());                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := CPR.CNTRCT-PART-PAYEE-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Parm_Check_Dte);                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #PARM-CHECK-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Parm_Todays_Dte);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #PARM-TODAYS-DTE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Area().setValue("BATCH");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Id().setValue("IAAP999");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP999'
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde().setValue(pnd_Parm_Trans_Cde);                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := #PARM-TRANS-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue(pnd_Parm_Sub_Cde);                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := #PARM-SUB-CDE
        ldaIaal999.getIaa_Trans_Rcrd_Trans_Cmbne_Cde().setValue(pnd_Trans_Cmbne_Cde);                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #TRANS-CMBNE-CDE
        ldaIaal999.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
        getReports().write(1, "Trans record",ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde(),ldaIaal999.getIaa_Trans_Rcrd_Trans_Sub_Cde(),"for:",ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), //Natural: WRITE ( 1 ) 'Trans record' IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-SUB-CDE 'for:' IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE 'created successfully'
            ldaIaal999.getIaa_Trans_Rcrd_Trans_Payee_Cde(),"created successfully");
        if (Global.isEscape()) return;
    }
    private void sub_Create_Funds() throws Exception                                                                                                                      //Natural: CREATE-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr.setValue(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr());                                                             //Natural: ASSIGN #TIAA-CNTRCT-PPCN-NBR := CPR.CNTRCT-PART-PPCN-NBR
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde.setValue(2);                                                                                                   //Natural: ASSIGN #TIAA-CNTRCT-PAYEE-CDE := 02
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cmpny_Fund_Cde.setValue(" ");                                                                                                   //Natural: ASSIGN #TIAA-CMPNY-FUND-CDE := ' '
        ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #TIAA-CNTRCT-FUND-KEY
        (
        "READ06",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Tiaa_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ06:
        while (condition(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("READ06")))
        {
            if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Ppcn_Nbr) || ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().notEquals(pnd_Tiaa_Cntrct_Fund_Key_Pnd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #TIAA-CNTRCT-PPCN-NBR OR IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(3);                                                                                         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 03
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO C*TIAA-RATE-DATA-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); pnd_I.nadd(1))
            {
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).compute(new ComputeParameters(true, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED TIAA-PER-PAY-AMT ( #I ) = TIAA-PER-PAY-AMT ( #I ) * .3333
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).multiply(new DbsDecimal(".3333")));
                ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).compute(new ComputeParameters(true, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)),  //Natural: COMPUTE ROUNDED TIAA-PER-DIV-AMT ( #I ) = TIAA-PER-DIV-AMT ( #I ) * .3333
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).multiply(new DbsDecimal(".3333")));
                if (condition(ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals("63")))                                                             //Natural: IF TIAA-RATE-CDE ( #I ) = '63'
                {
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nsubtract(0);                                                                     //Natural: SUBTRACT .01 FROM TIAA-PER-PAY-AMT ( #I )
                    ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(0);                                                                          //Natural: ADD .01 TO TIAA-PER-DIV-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                //Natural: ASSIGN TIAA-TOT-PER-AMT := 0 + TIAA-PER-PAY-AMT ( * )
                DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*")));
            ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                //Natural: ASSIGN TIAA-TOT-DIV-AMT := 0 + TIAA-PER-DIV-AMT ( * )
                DbsField.add(getZero(),ldaIaal999.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*")));
            ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                          //Natural: STORE IAA-TIAA-FUND-RCRD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Display_It() throws Exception                                                                                                                        //Natural: DISPLAY-IT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Contract",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Contract' IAA-CNTRCT.CNTRCT-PPCN-NBR 'Old Contract/Pymnt Mthd' #CNTRCT-PYMNT-MTHD 'New Contract/Pymnt Mthd' IAA-CNTRCT.CNTRCT-PYMNT-MTHD 'Old 2nd Annt/Xref Ind' #CNTRCT-SCND-ANNT-XREF-IND 'New 2nd Annt/Xref Ind' IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND
        		ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr(),"Old Contract/Pymnt Mthd",
        		pnd_Cntrct_Pymnt_Mthd,"New Contract/Pymnt Mthd",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Pymnt_Mthd(),"Old 2nd Annt/Xref Ind",
        		pnd_Cntrct_Scnd_Annt_Xref_Ind,"New 2nd Annt/Xref Ind",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, "IA UPDATE REPORT");                                                                                                            //Natural: WRITE ( 1 ) 'IA UPDATE REPORT'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, "IA EXCEPTION REPORT");                                                                                                         //Natural: WRITE ( 2 ) 'IA EXCEPTION REPORT'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "=",ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr(),"=",ldaIaal999.getCpr_Cntrct_Part_Payee_Cde(),"=",pnd_Parm_Invrse_Dte,NEWLINE,                 //Natural: WRITE '=' CPR.CNTRCT-PART-PPCN-NBR '=' CPR.CNTRCT-PART-PAYEE-CDE '=' #PARM-INVRSE-DTE / '=' #PARM-TRANS-DTE '=' #PARM-CHECK-DTE
            "=",pnd_Parm_Trans_Dte,"=",pnd_Parm_Check_Dte);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133 ZP=ON");
        Global.format(2, "PS=0 LS=133 ZP=ON");

        getReports().setDisplayColumns(0, "Contract",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(),"Py",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_Payee_Cde(),"Trans",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_Cde(),"Todays Dte",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_Todays_Dte(),"User",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Id(),"User Area",
        		ldaIaal999.getIaa_Trans_Rcrd_Trans_User_Area(),"Old Py",
        		pnd_Old_Py,"New Py",
        		dc_Cntrct_Payee_Cde);
        getReports().setDisplayColumns(4, "/CNTRCT",
        		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),"/PY",
        		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde(), new ReportEditMask ("99"),"/FUND",
        		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde(),"/RATE",
        		ldaIaal999.getIaa_Cref_Fund_Rcrd_Cref_Rate_Cde());
        getReports().setDisplayColumns(3, "From Cntrct",
        		xfr_Ia_Frm_Cntrct,"From Py",
        		xfr_Ia_Frm_Payee,"To Cntrct",
        		xfr_Ia_To_Cntrct,"To Py",
        		xfr_Ia_To_Payee,"Stts",
        		xfr_Xfr_Stts_Cde,"Type",
        		xfr_Rcrd_Type_Cde,"Old Eff Dte",
        		pnd_Old_D, new ReportEditMask ("YYYYMMDD"),"New Eff Dte",
        		xfr_Rqst_Effctv_Dte, new ReportEditMask ("YYYYMMDD"));
        getReports().setDisplayColumns(1, "Contract",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Ppcn_Nbr(),"Old Contract/Pymnt Mthd",
        		pnd_Cntrct_Pymnt_Mthd,"New Contract/Pymnt Mthd",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Pymnt_Mthd(),"Old 2nd Annt/Xref Ind",
        		pnd_Cntrct_Scnd_Annt_Xref_Ind,"New 2nd Annt/Xref Ind",
        		ldaIaal999.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind());
    }
}
