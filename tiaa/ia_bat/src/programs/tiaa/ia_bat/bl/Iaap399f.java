/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:27:13 PM
**        * FROM NATURAL PROGRAM : Iaap399f
************************************************************
**        * FILE NAME            : Iaap399f.java
**        * CLASS NAME           : Iaap399f
**        * INSTANCE NAME        : Iaap399f
************************************************************
************************************************************************
* PROGRAM  : IAAP399F
*
* PURPOSE  : CREATES DATA RECTYPE30 EXTENSION
*
* DATE     : 3/17/2017
*
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap399f extends BLNatBase
{
    // Data Areas
    private LdaIaal050 ldaIaal050;
    private PdaIaaa051z pdaIaaa051z;
    private LdaIaal399f ldaIaal399f;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField cntrl_Cntrl_Rcrd_Key;
    private DbsField pnd_W_Price;

    private DbsGroup pnd_W_Price__R_Field_1;
    private DbsField pnd_W_Price_Pnd_W_Auv;
    private DbsField pnd_I;
    private DbsField pnd_S00;
    private DbsField pnd_Ga;
    private DbsField pnd_Gw;
    private DbsField pnd_Ia;
    private DbsField pnd_Ib;
    private DbsField pnd_Ic;
    private DbsField pnd_Id;
    private DbsField pnd_Ie;
    private DbsField pnd_If;
    private DbsField pnd_Ig;
    private DbsField pnd_Ih;
    private DbsField pnd_0a;
    private DbsField pnd_6a;
    private DbsField pnd_W0;
    private DbsField pnd_W3;
    private DbsField pnd_W7;
    private DbsField pnd_Y00;
    private DbsField pnd_Y01;
    private DbsField pnd_Y02;
    private DbsField pnd_Y03;
    private DbsField pnd_Y04;
    private DbsField pnd_Y05;
    private DbsField pnd_Y06;
    private DbsField pnd_Y07;
    private DbsField pnd_Y08;
    private DbsField pnd_Y09;
    private DbsField pnd_Y10;
    private DbsField pnd_Y99;
    private DbsField pnd_Z00;
    private DbsField pnd_Z01;
    private DbsField pnd_Z02;
    private DbsField pnd_Z03;
    private DbsField pnd_Z04;
    private DbsField pnd_Z05;
    private DbsField pnd_Z06;
    private DbsField pnd_Z07;
    private DbsField pnd_Z08;
    private DbsField pnd_Z09;
    private DbsField pnd_Z10;
    private DbsField pnd_Z99;
    private DbsField pnd_Test;
    private DbsField pnd_Todays_Dte;

    private DbsGroup pnd_Todays_Dte__R_Field_2;
    private DbsField pnd_Todays_Dte_Pnd_Tyyyy_Mm;
    private DbsField pnd_Todays_Dte_Pnd_Tdd;
    private DbsField pnd_Next_Bus_Dte;

    private DbsGroup pnd_Next_Bus_Dte__R_Field_3;
    private DbsField pnd_Next_Bus_Dte_Pnd_Nyyyy_Mm;
    private DbsField pnd_Next_Bus_Dte_Pnd_Ndd;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Ts;

    private DbsGroup pnd_Ts__R_Field_4;
    private DbsField pnd_Ts_Pnd_T_Dte;
    private DbsField pnd_Ts_Pnd_T_Hhii;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);
        ldaIaal399f = new LdaIaal399f();
        registerRecord(ldaIaal399f);
        registerRecord(ldaIaal399f.getVw_iaa_Tiaa_Fund_Rcrd());

        // Local Variables

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        cntrl_Cntrl_Rcrd_Key = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRL_RCRD_KEY");
        cntrl_Cntrl_Rcrd_Key.setSuperDescriptor(true);
        registerRecord(vw_cntrl);

        pnd_W_Price = localVariables.newFieldInRecord("pnd_W_Price", "#W-PRICE", FieldType.NUMERIC, 9, 2);

        pnd_W_Price__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Price__R_Field_1", "REDEFINE", pnd_W_Price);
        pnd_W_Price_Pnd_W_Auv = pnd_W_Price__R_Field_1.newFieldInGroup("pnd_W_Price_Pnd_W_Auv", "#W-AUV", FieldType.NUMERIC, 9, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_S00 = localVariables.newFieldInRecord("pnd_S00", "#S00", FieldType.PACKED_DECIMAL, 7);
        pnd_Ga = localVariables.newFieldInRecord("pnd_Ga", "#GA", FieldType.PACKED_DECIMAL, 7);
        pnd_Gw = localVariables.newFieldInRecord("pnd_Gw", "#GW", FieldType.PACKED_DECIMAL, 7);
        pnd_Ia = localVariables.newFieldInRecord("pnd_Ia", "#IA", FieldType.PACKED_DECIMAL, 7);
        pnd_Ib = localVariables.newFieldInRecord("pnd_Ib", "#IB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ic = localVariables.newFieldInRecord("pnd_Ic", "#IC", FieldType.PACKED_DECIMAL, 7);
        pnd_Id = localVariables.newFieldInRecord("pnd_Id", "#ID", FieldType.PACKED_DECIMAL, 7);
        pnd_Ie = localVariables.newFieldInRecord("pnd_Ie", "#IE", FieldType.PACKED_DECIMAL, 7);
        pnd_If = localVariables.newFieldInRecord("pnd_If", "#IF", FieldType.PACKED_DECIMAL, 7);
        pnd_Ig = localVariables.newFieldInRecord("pnd_Ig", "#IG", FieldType.PACKED_DECIMAL, 7);
        pnd_Ih = localVariables.newFieldInRecord("pnd_Ih", "#IH", FieldType.PACKED_DECIMAL, 7);
        pnd_0a = localVariables.newFieldInRecord("pnd_0a", "#0A", FieldType.PACKED_DECIMAL, 7);
        pnd_6a = localVariables.newFieldInRecord("pnd_6a", "#6A", FieldType.PACKED_DECIMAL, 7);
        pnd_W0 = localVariables.newFieldInRecord("pnd_W0", "#W0", FieldType.PACKED_DECIMAL, 7);
        pnd_W3 = localVariables.newFieldInRecord("pnd_W3", "#W3", FieldType.PACKED_DECIMAL, 7);
        pnd_W7 = localVariables.newFieldInRecord("pnd_W7", "#W7", FieldType.PACKED_DECIMAL, 7);
        pnd_Y00 = localVariables.newFieldInRecord("pnd_Y00", "#Y00", FieldType.PACKED_DECIMAL, 7);
        pnd_Y01 = localVariables.newFieldInRecord("pnd_Y01", "#Y01", FieldType.PACKED_DECIMAL, 7);
        pnd_Y02 = localVariables.newFieldInRecord("pnd_Y02", "#Y02", FieldType.PACKED_DECIMAL, 7);
        pnd_Y03 = localVariables.newFieldInRecord("pnd_Y03", "#Y03", FieldType.PACKED_DECIMAL, 7);
        pnd_Y04 = localVariables.newFieldInRecord("pnd_Y04", "#Y04", FieldType.PACKED_DECIMAL, 7);
        pnd_Y05 = localVariables.newFieldInRecord("pnd_Y05", "#Y05", FieldType.PACKED_DECIMAL, 7);
        pnd_Y06 = localVariables.newFieldInRecord("pnd_Y06", "#Y06", FieldType.PACKED_DECIMAL, 7);
        pnd_Y07 = localVariables.newFieldInRecord("pnd_Y07", "#Y07", FieldType.PACKED_DECIMAL, 7);
        pnd_Y08 = localVariables.newFieldInRecord("pnd_Y08", "#Y08", FieldType.PACKED_DECIMAL, 7);
        pnd_Y09 = localVariables.newFieldInRecord("pnd_Y09", "#Y09", FieldType.PACKED_DECIMAL, 7);
        pnd_Y10 = localVariables.newFieldInRecord("pnd_Y10", "#Y10", FieldType.PACKED_DECIMAL, 7);
        pnd_Y99 = localVariables.newFieldInRecord("pnd_Y99", "#Y99", FieldType.PACKED_DECIMAL, 7);
        pnd_Z00 = localVariables.newFieldInRecord("pnd_Z00", "#Z00", FieldType.PACKED_DECIMAL, 7);
        pnd_Z01 = localVariables.newFieldInRecord("pnd_Z01", "#Z01", FieldType.PACKED_DECIMAL, 7);
        pnd_Z02 = localVariables.newFieldInRecord("pnd_Z02", "#Z02", FieldType.PACKED_DECIMAL, 7);
        pnd_Z03 = localVariables.newFieldInRecord("pnd_Z03", "#Z03", FieldType.PACKED_DECIMAL, 7);
        pnd_Z04 = localVariables.newFieldInRecord("pnd_Z04", "#Z04", FieldType.PACKED_DECIMAL, 7);
        pnd_Z05 = localVariables.newFieldInRecord("pnd_Z05", "#Z05", FieldType.PACKED_DECIMAL, 7);
        pnd_Z06 = localVariables.newFieldInRecord("pnd_Z06", "#Z06", FieldType.PACKED_DECIMAL, 7);
        pnd_Z07 = localVariables.newFieldInRecord("pnd_Z07", "#Z07", FieldType.PACKED_DECIMAL, 7);
        pnd_Z08 = localVariables.newFieldInRecord("pnd_Z08", "#Z08", FieldType.PACKED_DECIMAL, 7);
        pnd_Z09 = localVariables.newFieldInRecord("pnd_Z09", "#Z09", FieldType.PACKED_DECIMAL, 7);
        pnd_Z10 = localVariables.newFieldInRecord("pnd_Z10", "#Z10", FieldType.PACKED_DECIMAL, 7);
        pnd_Z99 = localVariables.newFieldInRecord("pnd_Z99", "#Z99", FieldType.PACKED_DECIMAL, 7);
        pnd_Test = localVariables.newFieldInRecord("pnd_Test", "#TEST", FieldType.BOOLEAN, 1);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);

        pnd_Todays_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Todays_Dte__R_Field_2", "REDEFINE", pnd_Todays_Dte);
        pnd_Todays_Dte_Pnd_Tyyyy_Mm = pnd_Todays_Dte__R_Field_2.newFieldInGroup("pnd_Todays_Dte_Pnd_Tyyyy_Mm", "#TYYYY-MM", FieldType.NUMERIC, 6);
        pnd_Todays_Dte_Pnd_Tdd = pnd_Todays_Dte__R_Field_2.newFieldInGroup("pnd_Todays_Dte_Pnd_Tdd", "#TDD", FieldType.NUMERIC, 2);
        pnd_Next_Bus_Dte = localVariables.newFieldInRecord("pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.STRING, 8);

        pnd_Next_Bus_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Next_Bus_Dte__R_Field_3", "REDEFINE", pnd_Next_Bus_Dte);
        pnd_Next_Bus_Dte_Pnd_Nyyyy_Mm = pnd_Next_Bus_Dte__R_Field_3.newFieldInGroup("pnd_Next_Bus_Dte_Pnd_Nyyyy_Mm", "#NYYYY-MM", FieldType.NUMERIC, 6);
        pnd_Next_Bus_Dte_Pnd_Ndd = pnd_Next_Bus_Dte__R_Field_3.newFieldInGroup("pnd_Next_Bus_Dte_Pnd_Ndd", "#NDD", FieldType.NUMERIC, 2);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ts = localVariables.newFieldInRecord("pnd_Ts", "#TS", FieldType.STRING, 12);

        pnd_Ts__R_Field_4 = localVariables.newGroupInRecord("pnd_Ts__R_Field_4", "REDEFINE", pnd_Ts);
        pnd_Ts_Pnd_T_Dte = pnd_Ts__R_Field_4.newFieldInGroup("pnd_Ts_Pnd_T_Dte", "#T-DTE", FieldType.STRING, 8);
        pnd_Ts_Pnd_T_Hhii = pnd_Ts__R_Field_4.newFieldInGroup("pnd_Ts_Pnd_T_Hhii", "#T-HHII", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaIaal050.initializeValues();
        ldaIaal399f.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap399f() throws Exception
    {
        super("Iaap399f");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT LS = 163 PS = 0
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date_A().setValueEdited(cntrl_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO IA-CHECK-DATE-A
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date_A().setValue(ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date_A());                                              //Natural: ASSIGN IA-ISSUE-DATE-A := IA-CHECK-DATE-A
            pnd_Todays_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DTE
            pnd_Next_Bus_Dte.setValueEdited(cntrl_Cntrl_Next_Bus_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-NEXT-BUS-DTE ( EM = YYYYMMDD ) TO #NEXT-BUS-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                                  //Natural: ASSIGN IA-REVAL-METHD := 'M'
        ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue("L");                                                                                                    //Natural: ASSIGN IA-CALL-TYPE := 'L'
        ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                              //Natural: ASSIGN IA-CHECK-DATE := 99999999
        //* *IF #TDD LE 20 AND #NDD GE 21
        //*   IA-CALL-TYPE := 'P'
        //*   MOVE EDITED *TIMX(EM=YYYYMMDDHHII) TO #TS
        //*   IF #T-DTE = #TODAYS-DTE
        //*     IF #T-HHII LE 2000
        //*       IA-CALL-TYPE := 'L'
        //*       IA-CHECK-DATE := 99999999
        //*     END-IF
        //*   END-IF
        //* *ELSE
        //*   IA-CHECK-DATE := 99999999
        //* *END-IF
        ldaIaal399f.getPnd_Work_Record_Out_Pnd_W2_Lst_Pd_Dte().reset();                                                                                                   //Natural: RESET #W2-LST-PD-DTE
        ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Record_Code().setValue(30);                                                                                             //Natural: ASSIGN #W1-RECORD-CODE := 30
        ldaIaal399f.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD PHYSICAL
        (
        "READ02",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ02:
        while (condition(ldaIaal399f.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("READ02")))
        {
            if (condition(pnd_Test.getBoolean()))                                                                                                                         //Natural: IF #TEST
            {
                short decideConditionsMet258 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE TIAA-CNTRCT-PPCN-NBR;//Natural: VALUE '0L000000':'0U999999'
                if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("0L000000' : '0U999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_0a.greater(250)))                                                                                                                   //Natural: IF #0A GT 250
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_0a.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #0A
                }                                                                                                                                                         //Natural: VALUE '6L000000':'6N999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("6L000000' : '6N999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_6a.greater(250)))                                                                                                                   //Natural: IF #6A GT 250
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_6a.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #6A
                }                                                                                                                                                         //Natural: VALUE 'IA000000':'IA999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IA000000' : 'IA999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ia.greater(100)))                                                                                                                   //Natural: IF #IA GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ia.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IA
                }                                                                                                                                                         //Natural: VALUE 'IB000000':'IB999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IB000000' : 'IB999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ib.greater(100)))                                                                                                                   //Natural: IF #IB GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ib.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IB
                }                                                                                                                                                         //Natural: VALUE 'IC000000':'IC999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IC000000' : 'IC999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ic.greater(100)))                                                                                                                   //Natural: IF #IC GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ic.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IC
                }                                                                                                                                                         //Natural: VALUE 'ID000000':'ID999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("ID000000' : 'ID999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Id.greater(100)))                                                                                                                   //Natural: IF #ID GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Id.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ID
                }                                                                                                                                                         //Natural: VALUE 'IE000000':'IE999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IE000000' : 'IE999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ie.greater(100)))                                                                                                                   //Natural: IF #IE GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ie.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IE
                }                                                                                                                                                         //Natural: VALUE 'IF000000':'IF999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IF000000' : 'IF999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_If.greater(100)))                                                                                                                   //Natural: IF #IF GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_If.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IF
                }                                                                                                                                                         //Natural: VALUE 'IG000000':'IG999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IG000000' : 'IG999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ig.greater(100)))                                                                                                                   //Natural: IF #IG GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ig.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IG
                }                                                                                                                                                         //Natural: VALUE 'IH000000':'IH999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("IH000000' : 'IH999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ih.greater(100)))                                                                                                                   //Natural: IF #IH GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ih.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IH
                }                                                                                                                                                         //Natural: VALUE 'GA000000':'GA999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("GA000000' : 'GA999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Ga.greater(200)))                                                                                                                   //Natural: IF #GA GT 200
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ga.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #GA
                }                                                                                                                                                         //Natural: VALUE 'GW000000':'GW999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("GW000000' : 'GW999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Gw.greater(200)))                                                                                                                   //Natural: IF #GW GT 200
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Gw.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #GW
                }                                                                                                                                                         //Natural: VALUE 'S0000000':'S9999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("S0000000' : 'S9999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_S00.greater(200)))                                                                                                                  //Natural: IF #S00 GT 200
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_S00.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #S00
                }                                                                                                                                                         //Natural: VALUE 'W0000000':'W0249999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("W0000000' : 'W0249999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_W0.greater(100)))                                                                                                                   //Natural: IF #W0 GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W0.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #W0
                }                                                                                                                                                         //Natural: VALUE 'W0300000':'W0699999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("W0300000' : 'W0699999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_W3.greater(100)))                                                                                                                   //Natural: IF #W3 GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W3.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #W3
                }                                                                                                                                                         //Natural: VALUE 'W0700000':'W0899999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("W0700000' : 'W0899999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_W7.greater(100)))                                                                                                                   //Natural: IF #W7 GT 100
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W7.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #W7
                }                                                                                                                                                         //Natural: VALUE 'Y0000000':'Y0029999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0000000' : 'Y0029999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y00.greater(300)))                                                                                                                  //Natural: IF #Y00 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y00.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y00
                }                                                                                                                                                         //Natural: VALUE 'Y0100000':'Y0199999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0100000' : 'Y0199999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y01.greater(300)))                                                                                                                  //Natural: IF #Y01 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y01.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y01
                }                                                                                                                                                         //Natural: VALUE 'Y0200000':'Y0299999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0200000' : 'Y0299999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y02.greater(300)))                                                                                                                  //Natural: IF #Y02 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y02.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y02
                }                                                                                                                                                         //Natural: VALUE 'Y0300000':'Y0399999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0300000' : 'Y0399999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y03.greater(300)))                                                                                                                  //Natural: IF #Y03 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y03.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y03
                }                                                                                                                                                         //Natural: VALUE 'Y0400000':'Y0499999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0400000' : 'Y0499999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y04.greater(300)))                                                                                                                  //Natural: IF #Y04 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y04.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y04
                }                                                                                                                                                         //Natural: VALUE 'Y0500000':'Y0599999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0500000' : 'Y0599999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y05.greater(300)))                                                                                                                  //Natural: IF #Y05 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y05.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y05
                }                                                                                                                                                         //Natural: VALUE 'Y0600000':'Y0699999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0600000' : 'Y0699999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y06.greater(300)))                                                                                                                  //Natural: IF #Y06 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y06.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y06
                }                                                                                                                                                         //Natural: VALUE 'Y0700000':'Y0799999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0700000' : 'Y0799999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y07.greater(300)))                                                                                                                  //Natural: IF #Y07 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y07.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y07
                }                                                                                                                                                         //Natural: VALUE 'Y0800000':'Y0899999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0800000' : 'Y0899999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y08.greater(300)))                                                                                                                  //Natural: IF #Y08 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y08.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y08
                }                                                                                                                                                         //Natural: VALUE 'Y0900000':'Y0999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y0900000' : 'Y0999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y09.greater(300)))                                                                                                                  //Natural: IF #Y09 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y09.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y09
                }                                                                                                                                                         //Natural: VALUE 'Y1000000':'Y8999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y1000000' : 'Y8999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y10.greater(300)))                                                                                                                  //Natural: IF #Y10 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y10.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y10
                }                                                                                                                                                         //Natural: VALUE 'Y9000000':'Y9999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Y9000000' : 'Y9999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y99.greater(300)))                                                                                                                  //Natural: IF #Y99 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Y99.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Y99
                }                                                                                                                                                         //Natural: VALUE 'Z0000000':'Z0029999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0000000' : 'Z0029999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z00.greater(300)))                                                                                                                  //Natural: IF #Z00 GT 300
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z00.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z00
                }                                                                                                                                                         //Natural: VALUE 'Z0100000':'Y0199999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0100000' : 'Y0199999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Y01.greater(400)))                                                                                                                  //Natural: IF #Y01 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z01.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z01
                }                                                                                                                                                         //Natural: VALUE 'Z0200000':'Z0299999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0200000' : 'Z0299999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z02.greater(400)))                                                                                                                  //Natural: IF #Z02 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z02.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z02
                }                                                                                                                                                         //Natural: VALUE 'Z0300000':'Z0399999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0300000' : 'Z0399999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z03.greater(400)))                                                                                                                  //Natural: IF #Z03 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z03.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z03
                }                                                                                                                                                         //Natural: VALUE 'Z0400000':'Z0499999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0400000' : 'Z0499999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z04.greater(400)))                                                                                                                  //Natural: IF #Z04 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z04.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z04
                }                                                                                                                                                         //Natural: VALUE 'Z0500000':'Z0599999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0500000' : 'Z0599999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z05.greater(400)))                                                                                                                  //Natural: IF #Z05 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z05.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z05
                }                                                                                                                                                         //Natural: VALUE 'Z0600000':'Z0699999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0600000' : 'Z0699999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z06.greater(400)))                                                                                                                  //Natural: IF #Z06 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z06.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z06
                }                                                                                                                                                         //Natural: VALUE 'Z0700000':'Z0799999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0700000' : 'Z0799999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z07.greater(400)))                                                                                                                  //Natural: IF #Z07 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z07.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z07
                }                                                                                                                                                         //Natural: VALUE 'Z0800000':'Z0899999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0800000' : 'Z0899999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z08.greater(400)))                                                                                                                  //Natural: IF #Z08 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z08.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z08
                }                                                                                                                                                         //Natural: VALUE 'Z0900000':'Z0999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z0900000' : 'Z0999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z09.greater(400)))                                                                                                                  //Natural: IF #Z09 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z09.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z09
                }                                                                                                                                                         //Natural: VALUE 'Z1000000':'Z8999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z1000000' : 'Z8999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z10.greater(400)))                                                                                                                  //Natural: IF #Z10 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z10.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z10
                }                                                                                                                                                         //Natural: VALUE 'Z9000000':'Z9999999'
                else if (condition((ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals("Z9000000' : 'Z9999999"))))
                {
                    decideConditionsMet258++;
                    if (condition(pnd_Z99.greater(400)))                                                                                                                  //Natural: IF #Z99 GT 400
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Z99.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #Z99
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr());                               //Natural: ASSIGN #W1-CNTRCT-PPCN-NBR := TIAA-CNTRCT-PPCN-NBR
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde());                             //Natural: ASSIGN #W1-CNTRCT-PAYEE-CDE := TIAA-CNTRCT-PAYEE-CDE
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cmpny_Cde().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                      //Natural: ASSIGN #W1-CMPNY-CDE := TIAA-CMPNY-FUND-CDE
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Count().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp());                            //Natural: ASSIGN #W1-RATE-COUNT := C*TIAA-RATE-DATA-GRP
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Ivc_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt());                                       //Natural: ASSIGN #W1-PER-IVC-AMT := TIAA-PER-IVC-AMT
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rtb_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt());                                               //Natural: ASSIGN #W1-RTB-AMT := TIAA-RTB-AMT
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte().reset();                                                                                            //Natural: RESET #W1-LST-TRANS-DTE #W1-XFR-ISS-DTE #W1-UNITS-CNT #W1-LST-XFR-OUT-DTE #W1-LST-XFR-IN-DTE #W1-RATE-DTE #W1-RATE-CDE #W1-PER-PAY-AMT #W1-PER-DIV-AMT #W1-RATE-FINAL-PAY-AMT #W1-RATE-FINAL-DIV-AMT
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Units_Cnt().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Dte().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Cde().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Div_Amt().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt().reset();
            ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt().reset();
            if (condition(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().greater(getZero())))                                                                          //Natural: IF LST-TRANS-DTE GT 0
            {
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Trans_Dte().setValueEdited(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED LST-TRANS-DTE ( EM = YYYYMMDD ) TO #W1-LST-TRANS-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte().greater(getZero())))                                                                       //Natural: IF TIAA-XFR-ISS-DTE GT 0
            {
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Xfr_Iss_Dte().setValueEdited(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED TIAA-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #W1-XFR-ISS-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte().greater(getZero())))                                                                    //Natural: IF TIAA-LST-XFR-IN-DTE GT 0
            {
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_In_Dte().setValueEdited(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte(),new                     //Natural: MOVE EDITED TIAA-LST-XFR-IN-DTE ( EM = YYYYMMDD ) TO #W1-LST-XFR-IN-DTE
                    ReportEditMask("YYYYMMDD"));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte().greater(getZero())))                                                                   //Natural: IF TIAA-LST-XFR-OUT-DTE GT 0
            {
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Lst_Xfr_Out_Dte().setValueEdited(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte(),new                   //Natural: MOVE EDITED TIAA-LST-XFR-OUT-DTE ( EM = YYYYMMDD ) TO #W1-LST-XFR-OUT-DTE
                    ReportEditMask("YYYYMMDD"));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Fund_Type().equals("T")))                                                                             //Natural: IF #W1-FUND-TYPE = 'T'
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO #W1-RATE-COUNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Count())); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).greater(getZero())))                                                  //Natural: IF TIAA-RATE-DTE ( #I ) GT 0
                    {
                        ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Dte().setValueEdited(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I),new         //Natural: MOVE EDITED TIAA-RATE-DTE ( #I ) ( EM = YYYYMMDD ) TO #W1-RATE-DTE
                            ReportEditMask("YYYYMMDD"));
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Cde().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I));                     //Natural: ASSIGN #W1-RATE-CDE := TIAA-RATE-CDE ( #I )
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I));               //Natural: ASSIGN #W1-PER-PAY-AMT := TIAA-PER-PAY-AMT ( #I )
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Div_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I));               //Natural: ASSIGN #W1-PER-DIV-AMT := TIAA-PER-DIV-AMT ( #I )
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Pay_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN #W1-RATE-FINAL-PAY-AMT := TIAA-RATE-FINAL-PAY-AMT ( #I )
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Final_Div_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN #W1-RATE-FINAL-DIV-AMT := TIAA-RATE-FINAL-DIV-AMT ( #I )
                    getWorkFiles().write(1, false, ldaIaal399f.getPnd_Work_Record_Out());                                                                                 //Natural: WRITE WORK FILE 1 #WORK-RECORD-OUT
                    pnd_Rec_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-CNT
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Rate_Cde().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1));                             //Natural: ASSIGN #W1-RATE-CDE := TIAA-RATE-CDE ( 1 )
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Units_Cnt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1));                           //Natural: ASSIGN #W1-UNITS-CNT := TIAA-UNITS-CNT ( 1 )
                //*  MONTHLY REVALUED
                if (condition(ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Fund_Type().equals("W") || ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Fund_Type().equals("4")))    //Natural: IF #W1-FUND-TYPE = 'W' OR = '4'
                {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                    sub_Get_Auv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Pay_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1));                       //Natural: ASSIGN #W1-PER-PAY-AMT := TIAA-PER-PAY-AMT ( 1 )
                ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Per_Div_Amt().setValue(ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1));                       //Natural: ASSIGN #W1-PER-DIV-AMT := TIAA-PER-DIV-AMT ( 1 )
                getWorkFiles().write(1, false, ldaIaal399f.getPnd_Work_Record_Out());                                                                                     //Natural: WRITE WORK FILE 1 #WORK-RECORD-OUT
                pnd_Rec_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL RECORDS WRITTEN:",pnd_Rec_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                                  //Natural: WRITE 'TOTAL RECORDS WRITTEN:' #REC-CNT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* *====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* *====================================================================
        DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Fund_Cde(),  //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL #W1-FUND-CDE GIVING INDEX #I
            true), new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                                      //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
            ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
            ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                      //Natural: ASSIGN IA-FUND-CODE := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
            DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
            if (condition(Global.isEscape())) return;
            if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().equals(getZero())))                                                                              //Natural: IF RETURN-CODE = 0
            {
                ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1).compute(new ComputeParameters(true, ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1)),  //Natural: COMPUTE ROUNDED TIAA-PER-PAY-AMT ( 1 ) = #W1-UNITS-CNT * AUV-RETURNED
                    ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Units_Cnt().multiply(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned()));
                pnd_W_Price_Pnd_W_Auv.setValue(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                                          //Natural: ASSIGN #W-AUV := AUV-RETURNED
                ldaIaal399f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1).setValue(pnd_W_Price);                                                                   //Natural: ASSIGN TIAA-PER-DIV-AMT ( 1 ) := #W-PRICE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "AIAN026 RETURN CODE NE 0 FOR: ",ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr(),ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde(), //Natural: WRITE 'AIAN026 RETURN CODE NE 0 FOR: ' #W1-CNTRCT-PPCN-NBR #W1-CNTRCT-PAYEE-CDE / '=' IA-FUND-CODE / '=' IA-CALL-TYPE / '=' IA-REVAL-METHD / '=' IA-CHECK-DATE / '=' RETURN-CODE
                    NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type(),NEWLINE,"=",
                    ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date(),NEWLINE,"=",ldaIaal050.getIa_Aian026_Linkage_Return_Code());
                if (Global.isEscape()) return;
                DbsUtil.terminate(64);  if (true) return;                                                                                                                 //Natural: TERMINATE 64
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR ERROR ERROR -- INVALID FUND FOR: ",ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Ppcn_Nbr(),ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cntrct_Payee_Cde(), //Natural: WRITE 'ERROR ERROR ERROR -- INVALID FUND FOR: ' #W1-CNTRCT-PPCN-NBR #W1-CNTRCT-PAYEE-CDE / '=' #W1-CMPNY-CDE
                NEWLINE,"=",ldaIaal399f.getPnd_Work_Record_Out_Pnd_W1_Cmpny_Cde());
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        //*  #AIAN026-CALL
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=163 PS=0");
    }
}
