/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:35 PM
**        * FROM NATURAL PROGRAM : Iaap798
************************************************************
**        * FILE NAME            : Iaap798.java
**        * CLASS NAME           : Iaap798
**        * INSTANCE NAME        : Iaap798
************************************************************
************************************************************************
* PROGRAM:   IAAP798
* DATE   :   09/28/2005
* FUNCTION:  READS FILE 200 ON DB03 AND EXTRACTS ALL CANADIAN CONVERTED
*            CONTRACTS. THESE ARE IDENTIFIED BY PPG CODE = Y1650. THE
*            REPORT IS GENERATED EVERY MONTH AND ONLY PICKS UP CONTRACTS
*            ISSUED FOR THE MONTH IT IS RUN. THE JOB RUNS EVERY MONTHEND
* HISTORY:
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap798 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Actvty_Cde;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField pnd_Todays_Dte;

    private DbsGroup pnd_Todays_Dte__R_Field_1;
    private DbsField pnd_Todays_Dte_Pnd_Todays_Dte_N;
    private DbsField pnd_Issue_Dte;

    private DbsGroup pnd_Issue_Dte__R_Field_2;
    private DbsField pnd_Issue_Dte_Pnd_Issue_Yyyymm;
    private DbsField pnd_Issue_Dte_Pnd_Issue_Dd;
    private DbsField pnd_Datd;
    private DbsField pnd_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        registerRecord(vw_cpr);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 6);

        pnd_Todays_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Todays_Dte__R_Field_1", "REDEFINE", pnd_Todays_Dte);
        pnd_Todays_Dte_Pnd_Todays_Dte_N = pnd_Todays_Dte__R_Field_1.newFieldInGroup("pnd_Todays_Dte_Pnd_Todays_Dte_N", "#TODAYS-DTE-N", FieldType.NUMERIC, 
            6);
        pnd_Issue_Dte = localVariables.newFieldInRecord("pnd_Issue_Dte", "#ISSUE-DTE", FieldType.STRING, 8);

        pnd_Issue_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Issue_Dte__R_Field_2", "REDEFINE", pnd_Issue_Dte);
        pnd_Issue_Dte_Pnd_Issue_Yyyymm = pnd_Issue_Dte__R_Field_2.newFieldInGroup("pnd_Issue_Dte_Pnd_Issue_Yyyymm", "#ISSUE-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Issue_Dte_Pnd_Issue_Dd = pnd_Issue_Dte__R_Field_2.newFieldInGroup("pnd_Issue_Dte_Pnd_Issue_Dd", "#ISSUE-DD", FieldType.NUMERIC, 2);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap798() throws Exception
    {
        super("Iaap798");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Todays_Dte.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMM"));                                                                //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMM ) TO #TODAYS-DTE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM ' '
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
        {
            if (condition(!(iaa_Cntrct_Cntrct_Issue_Dte.greaterOrEqual(pnd_Todays_Dte_Pnd_Todays_Dte_N) && iaa_Cntrct_Cntrct_Inst_Iss_Cde.equals("Y1650"))))              //Natural: ACCEPT IF CNTRCT-ISSUE-DTE GE #TODAYS-DTE-N AND CNTRCT-INST-ISS-CDE = 'Y1650'
            {
                continue;
            }
            pnd_Issue_Dte_Pnd_Issue_Yyyymm.setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                                         //Natural: ASSIGN #ISSUE-YYYYMM := CNTRCT-ISSUE-DTE
            pnd_Issue_Dte_Pnd_Issue_Dd.setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                                                          //Natural: ASSIGN #ISSUE-DD := CNTRCT-ISSUE-DTE-DD
            if (condition(pnd_Issue_Dte_Pnd_Issue_Dd.equals(getZero())))                                                                                                  //Natural: IF #ISSUE-DD = 0
            {
                pnd_Issue_Dte_Pnd_Issue_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN #ISSUE-DD := 01
            }                                                                                                                                                             //Natural: END-IF
            pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Issue_Dte);                                                                                        //Natural: MOVE EDITED #ISSUE-DTE TO #DATD ( EM = YYYYMMDD )
            vw_cpr.startDatabaseRead                                                                                                                                      //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM CNTRCT-PPCN-NBR
            (
            "READ03",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", iaa_Cntrct_Cntrct_Ppcn_Nbr, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
            );
            READ03:
            while (condition(vw_cpr.readNextRow("READ03")))
            {
                if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cntrct_Cntrct_Ppcn_Nbr)))                                                                            //Natural: IF CNTRCT-PART-PPCN-NBR NE CNTRCT-PPCN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(cpr_Cntrct_Actvty_Cde.notEquals(9))))                                                                                                     //Natural: ACCEPT IF CNTRCT-ACTVTY-CDE NE 9
                {
                    continue;
                }
                getReports().display(1, ReportOption.NOTITLE,"/Contract",                                                                                                 //Natural: DISPLAY ( 1 ) NOTITLE '/Contract' CNTRCT-PPCN-NBR ( AL = 8 ) '/Payee' CNTRCT-PART-PAYEE-CDE ( EM = 99 ) '/Mode' CNTRCT-MODE-IND ( EM = 999 ) 'Optn/Code' CNTRCT-OPTN-CDE ( EM = 99 ) 'PPG/Code' CNTRCT-INST-ISS-CDE 'Issue/Date' #DATD ( EM = MM/DD/YYYY )
                		iaa_Cntrct_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"/Payee",
                		cpr_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"/Mode",
                		cpr_Cntrct_Mode_Ind, new ReportEditMask ("999"),"Optn/Code",
                		iaa_Cntrct_Cntrct_Optn_Cde, new ReportEditMask ("99"),"PPG/Code",
                		iaa_Cntrct_Cntrct_Inst_Iss_Cde,"Issue/Date",
                		pnd_Datd, new ReportEditMask ("MM/DD/YYYY"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"Total Canadian Converted Contracts for Month of",iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte, new ReportEditMask                     //Natural: WRITE ( 1 ) // 'Total Canadian Converted Contracts for Month of' CNTRL-TODAYS-DTE ( EM = LLL' 'YYYY ) ':' #TOTAL ( EM = ZZ9 )
            ("LLL' 'YYYY"),":",pnd_Total, new ReportEditMask ("ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, "Canadian Converted Contracts Report",new ColumnSpacing(5),"Run Date:",Global.getDATX(),NEWLINE,"     IA Administration System"); //Natural: WRITE ( 1 ) 'Canadian Converted Contracts Report' 5X 'Run Date:' *DATX / '     IA Administration System'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"/Contract",
        		iaa_Cntrct_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"/Payee",
        		cpr_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"/Mode",
        		cpr_Cntrct_Mode_Ind, new ReportEditMask ("999"),"Optn/Code",
        		iaa_Cntrct_Cntrct_Optn_Cde, new ReportEditMask ("99"),"PPG/Code",
        		iaa_Cntrct_Cntrct_Inst_Iss_Cde,"Issue/Date",
        		pnd_Datd, new ReportEditMask ("MM/DD/YYYY"));
    }
}
