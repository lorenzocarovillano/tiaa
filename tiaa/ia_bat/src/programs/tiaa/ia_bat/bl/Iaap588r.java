/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:23 PM
**        * FROM NATURAL PROGRAM : Iaap588r
************************************************************
**        * FILE NAME            : Iaap588r.java
**        * CLASS NAME           : Iaap588r
**        * INSTANCE NAME        : Iaap588r
************************************************************
*
*   PROGRAM    : IAAP588R
*   DATE       : 10/2000
*   AUTHOR     : T. DIAZ
*   DESCRIPTION: DEATH CLAIMS PROCESSING REPORT (LUMP SUM)
*                CLONED SOME PROCESSING FROM IAAP587. IT READS A
*                WORKFILE INSTEAD OF THE AUDIT-FILE & MUST BE RAN AFTER
*                IAAP587
*
*   MAINTENANCE HISTORY :
*
*   PGMR        DATE             DESCRIPTION
*
*   T. DIAZ   05/07/02   RESTOWED FOR CHANGES IN IAAN051A
*                            IAAN051F & IAAN0511
*   T. DIAZ   07/31/01   FIXED LOGIC WHEN INSTALLMENT IS ONLY 1
*   T. DIAZ   10/02/01   FIXED INDEX PROBLEM IN LINE 2040
*   O. SOTTO  04/05/12   RATE BASE CHANGES. SC 040512.
* JUN 2017 J BREMER       PIN EXPANSION RE-STOW ONLY
* 04/2017  O SOTTO   RE-STOWED FOR IAAG588R, IAAA420, AND IAAL200B.
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap588r extends BLNatBase
{
    // Data Areas
    private GdaIaag588r gdaIaag588r;
    private LdaIaal588r ldaIaal588r;
    private PdaIaaa420 pdaIaaa420;
    private PdaAnta001 pdaAnta001;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal205a ldaIaal205a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;

    private DbsGroup pnd_Iaan425x;
    private DbsField pnd_Iaan425x_Pnd_Iaan425x_Ppcn_Nbr;
    private DbsField pnd_Iaan425x_Pnd_Iaan425x_Pyee_Cde;
    private DbsField pnd_Iaan425x_Pnd_Iaan425x_Parm_Dte;
    private DbsField pnd_Iaan425x_Pnd_Iaan425x_Master_Or_Hist;
    private DbsField pnd_Iaan425x_Pnd_Iaan425x_Inverse_Dte;

    private DbsGroup pnd_Iaan427d;
    private DbsField pnd_Iaan427d_Pnd_Iaan427d_Fund_File;
    private DbsField pnd_Iaan427d_Pnd_Iaan427d_Inverse_Date;
    private DbsField pnd_Iaan427d_Pnd_Iaan427d_Fund;
    private DbsField pnd_Iaan427d_Pnd_Iaan427d_Com_Val_Amt;
    private DbsField pnd_T_Fund_Code_1_3;

    private DbsGroup pnd_T_Fund_Code_1_3__R_Field_1;
    private DbsField pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1;
    private DbsField pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3;
    private DbsField pnd_T_Fund_Code_Hold;

    private DbsGroup pnd_T_Fund_Code_Hold__R_Field_2;
    private DbsField pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1;
    private DbsField pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_W_Fund_Code_1_3;

    private DbsGroup pnd_W_Fund_Code_1_3__R_Field_3;
    private DbsField pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_T_Fund_Code_M;
    private DbsField pnd_W_Fund_Code;

    private DbsGroup pnd_W_Fund_Code__R_Field_4;
    private DbsField pnd_W_Fund_Code_Pnd_Fund_Code_1;
    private DbsField pnd_W_Fund_Code_Pnd_Fund_Code_2;
    private DbsField pnd_Fund_Code_Monthly;

    private DbsGroup pnd_Fund_Code_Monthly__R_Field_5;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1;

    private DbsGroup pnd_Fund_Code_Monthly__R_Field_6;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N;
    private DbsField pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2;
    private DbsField pnd_Fund_Desc;

    private DbsGroup pnd_Fund_Desc__R_Field_7;
    private DbsField pnd_Fund_Desc_Pnd_Fund_Desc_14;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_W_Fund_Dte;

    private DbsGroup pnd_W_Fund_Dte__R_Field_8;
    private DbsField pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A;
    private DbsField pnd_W_Issue_Date;
    private DbsField pnd_Xfr_Issue_Date_A;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_9;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_10;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_11;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm;

    private DbsGroup pnd_Xfr_Issue_Date_A__R_Field_12;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm;
    private DbsField pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_13;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date_A__R_Field_14;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A__R_Field_15;
    private DbsField pnd_Issue_Date_A_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Issue_Date_Mm;
    private DbsField pnd_Tab_Inverse_Date;
    private DbsField pnd_Tab_Install_Date;

    private DbsGroup pnd_Tab_Install_Date__R_Field_16;
    private DbsField pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N;
    private DbsField pnd_Bottom_Date;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alpha;

    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_17;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yyyymmdd_N;

    private DbsGroup pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Cc;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yy;
    private DbsField pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Mm;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_19;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_20;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_21;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code;
    private DbsField pnd_Table_Comp_Desc;
    private DbsField pnd_Table_Fund_Desc;
    private DbsField pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_File;
    private DbsField pnd_Table_Cref_Com_Val_Amt;
    private DbsField pnd_Tab_Gross_Total;
    private DbsField pnd_Tab_Check_Total;
    private DbsField pnd_Tab_Eft_Total;
    private DbsField pnd_Dci_Rem;
    private DbsField pnd_Dci;
    private DbsField pnd_Total_Payment;
    private DbsField pnd_Total_Dci;
    private DbsField pnd_Total_Rem;
    private DbsField pnd_Payment;
    private DbsField pnd_W_Contract;
    private DbsField pnd_W_Payee;
    private DbsField pnd_Pay_Field;
    private DbsField pnd_Ia_Rpt_Nm_Cd;
    private DbsField pnd_Ia_Rpt_Cmpny_Cd_A;
    private DbsField pnd_Sub;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_G;
    private DbsField pnd_Gg;
    private DbsField pnd_L;
    private DbsField pnd_U;
    private DbsField pnd_V;
    private DbsField pnd_W;
    private DbsField pnd_X;
    private DbsField pnd_Z;
    private DbsField pnd_Nmb;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_Len;
    private DbsField pnd_Num_Of_Instlmnts;
    private DbsField pnd_Last_Dci_Rec;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Val_Meth;
    private DbsField pnd_W_Amt;
    private DbsField pnd_Total;
    private DbsField pnd_Check_Total_Dci;
    private DbsField pnd_Save_Dci;
    private DbsField pnd_Last_Fund_Dci;
    private DbsField pnd_Save_Total_Dci;

    private DbsGroup pnd_A26_Fields;
    private DbsField pnd_A26_Fields_Pnd_A26_Call_Type;
    private DbsField pnd_A26_Fields_Pnd_A26_Fnd_Cde;
    private DbsField pnd_A26_Fields_Pnd_A26_Reval_Meth;
    private DbsField pnd_A26_Fields_Pnd_A26_Req_Dte;
    private DbsField pnd_A26_Fields_Pnd_A26_Prtc_Dte;
    private DbsField pnd_A26_Fields_Pnd_Return_Code;

    private DbsGroup pnd_A26_Fields__R_Field_22;
    private DbsField pnd_A26_Fields_Pnd_Rc_Pgm;
    private DbsField pnd_A26_Fields_Pnd_Rc;
    private DbsField pnd_A26_Fields_Pnd_Auv;
    private DbsField pnd_A26_Fields_Pnd_Auv_Ret_Dte;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Request_Month;
    private DbsField pnd_A26_Fields_Pnd_Days_In_Particip_Month;
    private DbsField pnd_Y2_Yy_N;

    private DbsGroup pnd_Y2_Yy_N__R_Field_23;
    private DbsField pnd_Y2_Yy_N_Pnd_Y2_Yy_A;
    private DbsField pnd_Get_Commuted_Value;
    private DbsField pnd_Check_Fund;
    private DbsField pnd_Table_File;
    private DbsField pnd_C_Fund_Cde;

    private DbsGroup pnd_C_Fund_Cde__R_Field_24;
    private DbsField pnd_C_Fund_Cde_Pnd_C_Fund_1;
    private DbsField pnd_C_Fund_Cde_Pnd_C_Fund_2;
    private DbsField pnd_Cref_Fund;

    private DbsGroup pnd_Cref_Fund__R_Field_25;
    private DbsField pnd_Cref_Fund_Pnd_Cref_Fund_1;
    private DbsField pnd_Cref_Fund_Pnd_Cref_Fund_2;

    private DbsGroup pnd_Cref_Fund__R_Field_26;
    private DbsField pnd_Cref_Fund_Pnd_Cref_Fund_2_N;
    private DbsField pnd_Old_Fund_Cde;
    private DbsField pnd_Read_Work2;
    private DbsField pnd_Time_Selects;
    private DbsField pnd_Rec;
    private DbsField pnd_Num_Of_Cref_Funds;
    private DbsField pnd_Report_Type;

    private DbsGroup pnd_Work_Rec_3;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Num_Payments;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Gross_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Check_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Eft_Total;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Check_Count;
    private DbsField pnd_Work_Rec_3_Pnd_Pp_Eft_Count;
    private DbsField pnd_Ls_Num_Payments;
    private DbsField pnd_Ls_Gross_Total;
    private DbsField pnd_Ls_Check_Total;
    private DbsField pnd_Ls_Eft_Total;
    private DbsField pnd_Ls_Check_Count;
    private DbsField pnd_Ls_Eft_Count;
    private DbsField pnd_Grand_Total_Num_Payments;
    private DbsField pnd_Grand_Total_Gross;
    private DbsField pnd_Grand_Total_Check;
    private DbsField pnd_Grand_Total_Eft;
    private DbsField pnd_Grand_Total_Check_Count;
    private DbsField pnd_Grand_Total_Eft_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaIaag588r = GdaIaag588r.getInstance(getCallnatLevel());
        registerRecord(gdaIaag588r);
        if (gdaOnly) return;

        ldaIaal588r = new LdaIaal588r();
        registerRecord(ldaIaal588r);
        localVariables = new DbsRecord();
        pdaIaaa420 = new PdaIaaa420(localVariables);
        pdaAnta001 = new PdaAnta001(localVariables);
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal205a = new LdaIaal205a();
        registerRecord(ldaIaal205a);
        registerRecord(ldaIaal205a.getVw_old_Tiaa_Rates());

        // Local Variables

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Iaan425x = localVariables.newGroupInRecord("pnd_Iaan425x", "#IAAN425X");
        pnd_Iaan425x_Pnd_Iaan425x_Ppcn_Nbr = pnd_Iaan425x.newFieldInGroup("pnd_Iaan425x_Pnd_Iaan425x_Ppcn_Nbr", "#IAAN425X-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Iaan425x_Pnd_Iaan425x_Pyee_Cde = pnd_Iaan425x.newFieldInGroup("pnd_Iaan425x_Pnd_Iaan425x_Pyee_Cde", "#IAAN425X-PYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Iaan425x_Pnd_Iaan425x_Parm_Dte = pnd_Iaan425x.newFieldInGroup("pnd_Iaan425x_Pnd_Iaan425x_Parm_Dte", "#IAAN425X-PARM-DTE", FieldType.NUMERIC, 
            8);
        pnd_Iaan425x_Pnd_Iaan425x_Master_Or_Hist = pnd_Iaan425x.newFieldInGroup("pnd_Iaan425x_Pnd_Iaan425x_Master_Or_Hist", "#IAAN425X-MASTER-OR-HIST", 
            FieldType.STRING, 1);
        pnd_Iaan425x_Pnd_Iaan425x_Inverse_Dte = pnd_Iaan425x.newFieldInGroup("pnd_Iaan425x_Pnd_Iaan425x_Inverse_Dte", "#IAAN425X-INVERSE-DTE", FieldType.NUMERIC, 
            8);

        pnd_Iaan427d = localVariables.newGroupInRecord("pnd_Iaan427d", "#IAAN427D");
        pnd_Iaan427d_Pnd_Iaan427d_Fund_File = pnd_Iaan427d.newFieldInGroup("pnd_Iaan427d_Pnd_Iaan427d_Fund_File", "#IAAN427D-FUND-FILE", FieldType.STRING, 
            1);
        pnd_Iaan427d_Pnd_Iaan427d_Inverse_Date = pnd_Iaan427d.newFieldInGroup("pnd_Iaan427d_Pnd_Iaan427d_Inverse_Date", "#IAAN427D-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        pnd_Iaan427d_Pnd_Iaan427d_Fund = pnd_Iaan427d.newFieldArrayInGroup("pnd_Iaan427d_Pnd_Iaan427d_Fund", "#IAAN427D-FUND", FieldType.STRING, 3, new 
            DbsArrayController(1, 40));
        pnd_Iaan427d_Pnd_Iaan427d_Com_Val_Amt = pnd_Iaan427d.newFieldArrayInGroup("pnd_Iaan427d_Pnd_Iaan427d_Com_Val_Amt", "#IAAN427D-COM-VAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 40));
        pnd_T_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_T_Fund_Code_1_3", "#T-FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_T_Fund_Code_1_3__R_Field_1 = localVariables.newGroupInRecord("pnd_T_Fund_Code_1_3__R_Field_1", "REDEFINE", pnd_T_Fund_Code_1_3);
        pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1 = pnd_T_Fund_Code_1_3__R_Field_1.newFieldInGroup("pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1", "#T-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3 = pnd_T_Fund_Code_1_3__R_Field_1.newFieldInGroup("pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3", "#T-FUND-CODE-2-3", 
            FieldType.STRING, 2);
        pnd_T_Fund_Code_Hold = localVariables.newFieldInRecord("pnd_T_Fund_Code_Hold", "#T-FUND-CODE-HOLD", FieldType.STRING, 3);

        pnd_T_Fund_Code_Hold__R_Field_2 = localVariables.newGroupInRecord("pnd_T_Fund_Code_Hold__R_Field_2", "REDEFINE", pnd_T_Fund_Code_Hold);
        pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1 = pnd_T_Fund_Code_Hold__R_Field_2.newFieldInGroup("pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1", "#T-FUND-CODE-HOLD-1", 
            FieldType.STRING, 1);
        pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3 = pnd_T_Fund_Code_Hold__R_Field_2.newFieldInGroup("pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_2_3", 
            "#T-FUND-CODE-HOLD-2-3", FieldType.STRING, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_W_Fund_Code_1_3 = localVariables.newFieldInRecord("pnd_W_Fund_Code_1_3", "#W-FUND-CODE-1-3", FieldType.STRING, 3);

        pnd_W_Fund_Code_1_3__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Fund_Code_1_3__R_Field_3", "REDEFINE", pnd_W_Fund_Code_1_3);
        pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1 = pnd_W_Fund_Code_1_3__R_Field_3.newFieldInGroup("pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1", "#W-FUND-CODE-1", 
            FieldType.STRING, 1);
        pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3 = pnd_W_Fund_Code_1_3__R_Field_3.newFieldInGroup("pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3", "#W-FUND-CODE-2-3", 
            FieldType.STRING, 2);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 3, new DbsArrayController(1, 60));
        pnd_T_Fund_Code_M = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code_M", "#T-FUND-CODE-M", FieldType.STRING, 3, new DbsArrayController(1, 
            60));
        pnd_W_Fund_Code = localVariables.newFieldInRecord("pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 3);

        pnd_W_Fund_Code__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Fund_Code__R_Field_4", "REDEFINE", pnd_W_Fund_Code);
        pnd_W_Fund_Code_Pnd_Fund_Code_1 = pnd_W_Fund_Code__R_Field_4.newFieldInGroup("pnd_W_Fund_Code_Pnd_Fund_Code_1", "#FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_W_Fund_Code_Pnd_Fund_Code_2 = pnd_W_Fund_Code__R_Field_4.newFieldInGroup("pnd_W_Fund_Code_Pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 
            2);
        pnd_Fund_Code_Monthly = localVariables.newFieldInRecord("pnd_Fund_Code_Monthly", "#FUND-CODE-MONTHLY", FieldType.STRING, 3);

        pnd_Fund_Code_Monthly__R_Field_5 = localVariables.newGroupInRecord("pnd_Fund_Code_Monthly__R_Field_5", "REDEFINE", pnd_Fund_Code_Monthly);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1 = pnd_Fund_Code_Monthly__R_Field_5.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1", 
            "#FUND-CODE-MONTHLY-1", FieldType.STRING, 1);

        pnd_Fund_Code_Monthly__R_Field_6 = pnd_Fund_Code_Monthly__R_Field_5.newGroupInGroup("pnd_Fund_Code_Monthly__R_Field_6", "REDEFINE", pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N = pnd_Fund_Code_Monthly__R_Field_6.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N", 
            "#FUND-CODE-MONTHLY-1-N", FieldType.NUMERIC, 1);
        pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2 = pnd_Fund_Code_Monthly__R_Field_5.newFieldInGroup("pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_2", 
            "#FUND-CODE-MONTHLY-2", FieldType.STRING, 2);
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);

        pnd_Fund_Desc__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Desc__R_Field_7", "REDEFINE", pnd_Fund_Desc);
        pnd_Fund_Desc_Pnd_Fund_Desc_14 = pnd_Fund_Desc__R_Field_7.newFieldInGroup("pnd_Fund_Desc_Pnd_Fund_Desc_14", "#FUND-DESC-14", FieldType.STRING, 
            14);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_W_Fund_Dte = localVariables.newFieldInRecord("pnd_W_Fund_Dte", "#W-FUND-DTE", FieldType.NUMERIC, 8);

        pnd_W_Fund_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_W_Fund_Dte__R_Field_8", "REDEFINE", pnd_W_Fund_Dte);
        pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A = pnd_W_Fund_Dte__R_Field_8.newFieldInGroup("pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A", "#W-FUND-DTE-A", FieldType.STRING, 
            8);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_Xfr_Issue_Date_A = localVariables.newFieldInRecord("pnd_Xfr_Issue_Date_A", "#XFR-ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Xfr_Issue_Date_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_9", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N = pnd_Xfr_Issue_Date_A__R_Field_9.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N", "#XFR-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Xfr_Issue_Date_A__R_Field_10 = pnd_Xfr_Issue_Date_A__R_Field_9.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_10", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm = pnd_Xfr_Issue_Date_A__R_Field_10.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyymm", 
            "#XFR-ISSUE-DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd = pnd_Xfr_Issue_Date_A__R_Field_10.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Dd", "#XFR-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_11 = pnd_Xfr_Issue_Date_A__R_Field_9.newGroupInGroup("pnd_Xfr_Issue_Date_A__R_Field_11", "REDEFINE", pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy = pnd_Xfr_Issue_Date_A__R_Field_11.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Yyyy", 
            "#XFR-ISSUE-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm = pnd_Xfr_Issue_Date_A__R_Field_11.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_Mm", "#XFR-ISSUE-DATE-MM", 
            FieldType.NUMERIC, 2);

        pnd_Xfr_Issue_Date_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Xfr_Issue_Date_A__R_Field_12", "REDEFINE", pnd_Xfr_Issue_Date_A);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy = pnd_Xfr_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Ccyy", 
            "#XFR-ISSUE-DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm = pnd_Xfr_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Mm", 
            "#XFR-ISSUE-DATE-A8-MM", FieldType.STRING, 2);
        pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd = pnd_Xfr_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_A8_Dd", 
            "#XFR-ISSUE-DATE-A8-DD", FieldType.STRING, 2);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_13", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N = pnd_Issue_Date_A__R_Field_13.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_14 = pnd_Issue_Date_A__R_Field_13.newGroupInGroup("pnd_Issue_Date_A__R_Field_14", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A_Pnd_Issue_Date_Dd = pnd_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A__R_Field_15 = pnd_Issue_Date_A__R_Field_13.newGroupInGroup("pnd_Issue_Date_A__R_Field_15", "REDEFINE", pnd_Issue_Date_A_Pnd_Issue_Date_N);
        pnd_Issue_Date_A_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Issue_Date_A_Issue_Date_Yyyy", "ISSUE-DATE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_A_Issue_Date_Mm = pnd_Issue_Date_A__R_Field_15.newFieldInGroup("pnd_Issue_Date_A_Issue_Date_Mm", "ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Tab_Inverse_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Inverse_Date", "#TAB-INVERSE-DATE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            120));
        pnd_Tab_Install_Date = localVariables.newFieldArrayInRecord("pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", FieldType.STRING, 8, new DbsArrayController(1, 
            120));

        pnd_Tab_Install_Date__R_Field_16 = localVariables.newGroupInRecord("pnd_Tab_Install_Date__R_Field_16", "REDEFINE", pnd_Tab_Install_Date);
        pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N = pnd_Tab_Install_Date__R_Field_16.newFieldArrayInGroup("pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N", 
            "#TAB-INSTALL-DATE-N", FieldType.NUMERIC, 8, new DbsArrayController(1, 120));
        pnd_Bottom_Date = localVariables.newFieldInRecord("pnd_Bottom_Date", "#BOTTOM-DATE", FieldType.NUMERIC, 8);
        pnd_Fl_Date_Yyyymmdd_Alpha = localVariables.newFieldInRecord("pnd_Fl_Date_Yyyymmdd_Alpha", "#FL-DATE-YYYYMMDD-ALPHA", FieldType.STRING, 8);

        pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_17 = localVariables.newGroupInRecord("pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_17", "REDEFINE", pnd_Fl_Date_Yyyymmdd_Alpha);
        pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yyyymmdd_N = pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_17.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yyyymmdd_N", 
            "#FL-DATE-YYYYMMDD-N", FieldType.NUMERIC, 8);

        pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18 = pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_17.newGroupInGroup("pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18", "REDEFINE", 
            pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yyyymmdd_N);
        pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Cc = pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Cc", 
            "#FL-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yy = pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yy", 
            "#FL-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Mm = pnd_Fl_Date_Yyyymmdd_Alpha__R_Field_18.newFieldInGroup("pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Mm", 
            "#FL-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_19", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_19.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_19.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_20", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_20.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_20.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code = pnd_Cntrct_Fund_Key__R_Field_20.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code", "#W-FND-CODE", FieldType.STRING, 
            3);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_21", "REDEFINE", pnd_Cntrct_Py_Dte_Key);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key__R_Field_21.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr", 
            "#OLD-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee = pnd_Cntrct_Py_Dte_Key__R_Field_21.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee", "#OLD-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte = pnd_Cntrct_Py_Dte_Key__R_Field_21.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte", 
            "#OLD-INVERSE-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code = pnd_Cntrct_Py_Dte_Key__R_Field_21.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code", "#OLD-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Table_Comp_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Comp_Desc", "#TABLE-COMP-DESC", FieldType.STRING, 4, new DbsArrayController(1, 
            60));
        pnd_Table_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Fund_Desc", "#TABLE-FUND-DESC", FieldType.STRING, 8, new DbsArrayController(1, 
            60));
        pnd_Tab_Per_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Per_Amt", "#TAB-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_Div_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Div_Amt", "#TAB-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            120));
        pnd_Tab_File = localVariables.newFieldArrayInRecord("pnd_Tab_File", "#TAB-FILE", FieldType.STRING, 1, new DbsArrayController(1, 120));
        pnd_Table_Cref_Com_Val_Amt = localVariables.newFieldArrayInRecord("pnd_Table_Cref_Com_Val_Amt", "#TABLE-CREF-COM-VAL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2, 1, 60));
        pnd_Tab_Gross_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Gross_Total", "#TAB-GROSS-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Tab_Check_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Check_Total", "#TAB-CHECK-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Tab_Eft_Total = localVariables.newFieldArrayInRecord("pnd_Tab_Eft_Total", "#TAB-EFT-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Dci_Rem = localVariables.newFieldInRecord("pnd_Dci_Rem", "#DCI-REM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dci = localVariables.newFieldInRecord("pnd_Dci", "#DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Payment = localVariables.newFieldInRecord("pnd_Total_Payment", "#TOTAL-PAYMENT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Dci = localVariables.newFieldInRecord("pnd_Total_Dci", "#TOTAL-DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Rem = localVariables.newFieldInRecord("pnd_Total_Rem", "#TOTAL-REM", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Payment = localVariables.newFieldInRecord("pnd_Payment", "#PAYMENT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Contract = localVariables.newFieldInRecord("pnd_W_Contract", "#W-CONTRACT", FieldType.STRING, 10);
        pnd_W_Payee = localVariables.newFieldInRecord("pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 2);
        pnd_Pay_Field = localVariables.newFieldArrayInRecord("pnd_Pay_Field", "#PAY-FIELD", FieldType.STRING, 7, new DbsArrayController(1, 60));
        pnd_Ia_Rpt_Nm_Cd = localVariables.newFieldArrayInRecord("pnd_Ia_Rpt_Nm_Cd", "#IA-RPT-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 80));
        pnd_Ia_Rpt_Cmpny_Cd_A = localVariables.newFieldArrayInRecord("pnd_Ia_Rpt_Cmpny_Cd_A", "#IA-RPT-CMPNY-CD-A", FieldType.STRING, 1, new DbsArrayController(1, 
            80));
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.STRING, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 5);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 5);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 5);
        pnd_Gg = localVariables.newFieldInRecord("pnd_Gg", "#GG", FieldType.NUMERIC, 5);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 5);
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.NUMERIC, 5);
        pnd_V = localVariables.newFieldInRecord("pnd_V", "#V", FieldType.NUMERIC, 5);
        pnd_W = localVariables.newFieldInRecord("pnd_W", "#W", FieldType.NUMERIC, 5);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 5);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 5);
        pnd_Nmb = localVariables.newFieldInRecord("pnd_Nmb", "#NMB", FieldType.NUMERIC, 3);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Num_Of_Instlmnts = localVariables.newFieldInRecord("pnd_Num_Of_Instlmnts", "#NUM-OF-INSTLMNTS", FieldType.NUMERIC, 3);
        pnd_Last_Dci_Rec = localVariables.newFieldInRecord("pnd_Last_Dci_Rec", "#LAST-DCI-REC", FieldType.NUMERIC, 2);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);
        pnd_Val_Meth = localVariables.newFieldInRecord("pnd_Val_Meth", "#VAL-METH", FieldType.STRING, 1);
        pnd_W_Amt = localVariables.newFieldInRecord("pnd_W_Amt", "#W-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Check_Total_Dci = localVariables.newFieldInRecord("pnd_Check_Total_Dci", "#CHECK-TOTAL-DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Save_Dci = localVariables.newFieldArrayInRecord("pnd_Save_Dci", "#SAVE-DCI", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 60));
        pnd_Last_Fund_Dci = localVariables.newFieldInRecord("pnd_Last_Fund_Dci", "#LAST-FUND-DCI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Save_Total_Dci = localVariables.newFieldInRecord("pnd_Save_Total_Dci", "#SAVE-TOTAL-DCI", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_A26_Fields = localVariables.newGroupInRecord("pnd_A26_Fields", "#A26-FIELDS");
        pnd_A26_Fields_Pnd_A26_Call_Type = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Call_Type", "#A26-CALL-TYPE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Fnd_Cde = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Fnd_Cde", "#A26-FND-CDE", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Reval_Meth = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Reval_Meth", "#A26-REVAL-METH", FieldType.STRING, 1);
        pnd_A26_Fields_Pnd_A26_Req_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Req_Dte", "#A26-REQ-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_A26_Prtc_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_A26_Prtc_Dte", "#A26-PRTC-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Return_Code = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 11);

        pnd_A26_Fields__R_Field_22 = pnd_A26_Fields.newGroupInGroup("pnd_A26_Fields__R_Field_22", "REDEFINE", pnd_A26_Fields_Pnd_Return_Code);
        pnd_A26_Fields_Pnd_Rc_Pgm = pnd_A26_Fields__R_Field_22.newFieldInGroup("pnd_A26_Fields_Pnd_Rc_Pgm", "#RC-PGM", FieldType.STRING, 8);
        pnd_A26_Fields_Pnd_Rc = pnd_A26_Fields__R_Field_22.newFieldInGroup("pnd_A26_Fields_Pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_A26_Fields_Pnd_Auv = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_A26_Fields_Pnd_Auv_Ret_Dte = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_A26_Fields_Pnd_Days_In_Request_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_A26_Fields_Pnd_Days_In_Particip_Month = pnd_A26_Fields.newFieldInGroup("pnd_A26_Fields_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Yy_N = localVariables.newFieldInRecord("pnd_Y2_Yy_N", "#Y2-YY-N", FieldType.NUMERIC, 2);

        pnd_Y2_Yy_N__R_Field_23 = localVariables.newGroupInRecord("pnd_Y2_Yy_N__R_Field_23", "REDEFINE", pnd_Y2_Yy_N);
        pnd_Y2_Yy_N_Pnd_Y2_Yy_A = pnd_Y2_Yy_N__R_Field_23.newFieldInGroup("pnd_Y2_Yy_N_Pnd_Y2_Yy_A", "#Y2-YY-A", FieldType.STRING, 2);
        pnd_Get_Commuted_Value = localVariables.newFieldInRecord("pnd_Get_Commuted_Value", "#GET-COMMUTED-VALUE", FieldType.BOOLEAN, 1);
        pnd_Check_Fund = localVariables.newFieldInRecord("pnd_Check_Fund", "#CHECK-FUND", FieldType.BOOLEAN, 1);
        pnd_Table_File = localVariables.newFieldInRecord("pnd_Table_File", "#TABLE-FILE", FieldType.STRING, 1);
        pnd_C_Fund_Cde = localVariables.newFieldInRecord("pnd_C_Fund_Cde", "#C-FUND-CDE", FieldType.STRING, 3);

        pnd_C_Fund_Cde__R_Field_24 = localVariables.newGroupInRecord("pnd_C_Fund_Cde__R_Field_24", "REDEFINE", pnd_C_Fund_Cde);
        pnd_C_Fund_Cde_Pnd_C_Fund_1 = pnd_C_Fund_Cde__R_Field_24.newFieldInGroup("pnd_C_Fund_Cde_Pnd_C_Fund_1", "#C-FUND-1", FieldType.STRING, 1);
        pnd_C_Fund_Cde_Pnd_C_Fund_2 = pnd_C_Fund_Cde__R_Field_24.newFieldInGroup("pnd_C_Fund_Cde_Pnd_C_Fund_2", "#C-FUND-2", FieldType.STRING, 2);
        pnd_Cref_Fund = localVariables.newFieldInRecord("pnd_Cref_Fund", "#CREF-FUND", FieldType.STRING, 3);

        pnd_Cref_Fund__R_Field_25 = localVariables.newGroupInRecord("pnd_Cref_Fund__R_Field_25", "REDEFINE", pnd_Cref_Fund);
        pnd_Cref_Fund_Pnd_Cref_Fund_1 = pnd_Cref_Fund__R_Field_25.newFieldInGroup("pnd_Cref_Fund_Pnd_Cref_Fund_1", "#CREF-FUND-1", FieldType.STRING, 1);
        pnd_Cref_Fund_Pnd_Cref_Fund_2 = pnd_Cref_Fund__R_Field_25.newFieldInGroup("pnd_Cref_Fund_Pnd_Cref_Fund_2", "#CREF-FUND-2", FieldType.STRING, 2);

        pnd_Cref_Fund__R_Field_26 = pnd_Cref_Fund__R_Field_25.newGroupInGroup("pnd_Cref_Fund__R_Field_26", "REDEFINE", pnd_Cref_Fund_Pnd_Cref_Fund_2);
        pnd_Cref_Fund_Pnd_Cref_Fund_2_N = pnd_Cref_Fund__R_Field_26.newFieldInGroup("pnd_Cref_Fund_Pnd_Cref_Fund_2_N", "#CREF-FUND-2-N", FieldType.NUMERIC, 
            2);
        pnd_Old_Fund_Cde = localVariables.newFieldInRecord("pnd_Old_Fund_Cde", "#OLD-FUND-CDE", FieldType.STRING, 5);
        pnd_Read_Work2 = localVariables.newFieldInRecord("pnd_Read_Work2", "#READ-WORK2", FieldType.NUMERIC, 8);
        pnd_Time_Selects = localVariables.newFieldInRecord("pnd_Time_Selects", "#TIME-SELECTS", FieldType.NUMERIC, 8);
        pnd_Rec = localVariables.newFieldInRecord("pnd_Rec", "#REC", FieldType.NUMERIC, 8);
        pnd_Num_Of_Cref_Funds = localVariables.newFieldInRecord("pnd_Num_Of_Cref_Funds", "#NUM-OF-CREF-FUNDS", FieldType.NUMERIC, 8);
        pnd_Report_Type = localVariables.newFieldInRecord("pnd_Report_Type", "#REPORT-TYPE", FieldType.STRING, 15);

        pnd_Work_Rec_3 = localVariables.newGroupInRecord("pnd_Work_Rec_3", "#WORK-REC-3");
        pnd_Work_Rec_3_Pnd_Pp_Num_Payments = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Num_Payments", "#PP-NUM-PAYMENTS", FieldType.NUMERIC, 
            7);
        pnd_Work_Rec_3_Pnd_Pp_Gross_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Gross_Total", "#PP-GROSS-TOTAL", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Check_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Check_Total", "#PP-CHECK-TOTAL", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Eft_Total = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Eft_Total", "#PP-EFT-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Work_Rec_3_Pnd_Pp_Check_Count = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Check_Count", "#PP-CHECK-COUNT", FieldType.NUMERIC, 
            7);
        pnd_Work_Rec_3_Pnd_Pp_Eft_Count = pnd_Work_Rec_3.newFieldInGroup("pnd_Work_Rec_3_Pnd_Pp_Eft_Count", "#PP-EFT-COUNT", FieldType.NUMERIC, 7);
        pnd_Ls_Num_Payments = localVariables.newFieldInRecord("pnd_Ls_Num_Payments", "#LS-NUM-PAYMENTS", FieldType.NUMERIC, 7);
        pnd_Ls_Gross_Total = localVariables.newFieldInRecord("pnd_Ls_Gross_Total", "#LS-GROSS-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Ls_Check_Total = localVariables.newFieldInRecord("pnd_Ls_Check_Total", "#LS-CHECK-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Ls_Eft_Total = localVariables.newFieldInRecord("pnd_Ls_Eft_Total", "#LS-EFT-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Ls_Check_Count = localVariables.newFieldInRecord("pnd_Ls_Check_Count", "#LS-CHECK-COUNT", FieldType.NUMERIC, 7);
        pnd_Ls_Eft_Count = localVariables.newFieldInRecord("pnd_Ls_Eft_Count", "#LS-EFT-COUNT", FieldType.NUMERIC, 7);
        pnd_Grand_Total_Num_Payments = localVariables.newFieldInRecord("pnd_Grand_Total_Num_Payments", "#GRAND-TOTAL-NUM-PAYMENTS", FieldType.NUMERIC, 
            7);
        pnd_Grand_Total_Gross = localVariables.newFieldInRecord("pnd_Grand_Total_Gross", "#GRAND-TOTAL-GROSS", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Total_Check = localVariables.newFieldInRecord("pnd_Grand_Total_Check", "#GRAND-TOTAL-CHECK", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Total_Eft = localVariables.newFieldInRecord("pnd_Grand_Total_Eft", "#GRAND-TOTAL-EFT", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Total_Check_Count = localVariables.newFieldInRecord("pnd_Grand_Total_Check_Count", "#GRAND-TOTAL-CHECK-COUNT", FieldType.NUMERIC, 7);
        pnd_Grand_Total_Eft_Count = localVariables.newFieldInRecord("pnd_Grand_Total_Eft_Count", "#GRAND-TOTAL-EFT-COUNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();

        ldaIaal588r.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal205a.initializeValues();

        localVariables.reset();
        pnd_Len.setInitialValue(10);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap588r() throws Exception
    {
        super("Iaap588r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP588R", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 200 PS = 56;//Natural: FORMAT ( 2 ) LS = 200 PS = 56
        //* ***********************************************************************
        //*                           START OF PROGRAM
        //* ***********************************************************************
        //* *
        //*  WRITE '*** START OF PROGRAM IAAP588R ***'
        //* ***********************************************************************
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, ldaIaal588r.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Iaan051f.class , getCurrentProcessState(), pnd_Ia_Rpt_Nm_Cd.getValue("*"), pnd_Ia_Rpt_Cmpny_Cd_A.getValue("*"));                                  //Natural: CALLNAT 'IAAN051F' #IA-RPT-NM-CD ( * ) #IA-RPT-CMPNY-CD-A ( * )
        if (condition(Global.isEscape())) return;
        FA:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Gg.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #GG
            pnd_Sub.setValue(pnd_Ia_Rpt_Cmpny_Cd_A.getValue(pnd_G));                                                                                                      //Natural: MOVE #IA-RPT-CMPNY-CD-A ( #G ) TO #SUB
            if (condition(pnd_Sub.equals(" ")))                                                                                                                           //Natural: IF #SUB = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_T_Fund_Code.getValue(pnd_Gg).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ia_Rpt_Cmpny_Cd_A.getValue(pnd_G), pnd_Ia_Rpt_Nm_Cd.getValue(pnd_G))); //Natural: COMPRESS #IA-RPT-CMPNY-CD-A ( #G ) #IA-RPT-NM-CD ( #G ) INTO #T-FUND-CODE ( #GG ) LEAVING NO
            if (condition(pnd_T_Fund_Code.getValue(pnd_Gg).equals("U09") || pnd_T_Fund_Code.getValue(pnd_Gg).equals("U11") || DbsUtil.maskMatches(pnd_T_Fund_Code.getValue(pnd_Gg), //Natural: IF #T-FUND-CODE ( #GG ) = 'U09' OR = 'U11' OR #T-FUND-CODE ( #GG ) = MASK ( 'U4' )
                "'U4'")))
            {
                pnd_Gg.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #GG
                pnd_T_Fund_Code.getValue(pnd_Gg).setValue(pnd_T_Fund_Code.getValue(pnd_Gg.getDec().subtract(1)));                                                         //Natural: MOVE #T-FUND-CODE ( #GG -1 ) TO #T-FUND-CODE ( #GG )
                DbsUtil.examine(new ExamineSource(pnd_T_Fund_Code.getValue(pnd_Gg),true), new ExamineSearch("U"), new ExamineReplace("W"));                               //Natural: EXAMINE FULL #T-FUND-CODE ( #GG ) FOR 'U' REPLACE WITH 'W'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_B.reset();                                                                                                                                                    //Natural: RESET #B
        FB:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Fund_Code_Monthly.setValue(pnd_T_Fund_Code.getValue(pnd_G));                                                                                              //Natural: MOVE #T-FUND-CODE ( #G ) TO #FUND-CODE-MONTHLY
            if (condition(pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1.equals("2")))                                                                                     //Natural: IF #FUND-CODE-MONTHLY-1 = '2'
            {
                pnd_Fund_Code_Monthly_Pnd_Fund_Code_Monthly_1_N.nadd(2);                                                                                                  //Natural: ADD 2 TO #FUND-CODE-MONTHLY-1-N
                pnd_B.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #B
                pnd_T_Fund_Code_M.getValue(pnd_B).setValue(pnd_Fund_Code_Monthly);                                                                                        //Natural: MOVE #FUND-CODE-MONTHLY TO #T-FUND-CODE-M ( #B )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FC:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            if (condition(pnd_T_Fund_Code.getValue(pnd_G).equals(" ")))                                                                                                   //Natural: IF #T-FUND-CODE ( #G ) = ' '
            {
                pnd_W.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #W
                if (condition(pnd_T_Fund_Code_M.getValue(pnd_W).notEquals(" ")))                                                                                          //Natural: IF #T-FUND-CODE-M ( #W ) NE ' '
                {
                    pnd_T_Fund_Code.getValue(pnd_G).setValue(pnd_T_Fund_Code_M.getValue(pnd_W));                                                                          //Natural: MOVE #T-FUND-CODE-M ( #W ) TO #T-FUND-CODE ( #G )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FD:                                                                                                                                                               //Natural: FOR #G = 1 TO 60
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(60)); pnd_G.nadd(1))
        {
            pnd_Fund_Desc.reset();                                                                                                                                        //Natural: RESET #FUND-DESC #COMP-DESC
            pnd_Comp_Desc.reset();
            if (condition(pnd_T_Fund_Code.getValue(pnd_G).equals(" ")))                                                                                                   //Natural: IF #T-FUND-CODE ( #G ) = ' '
            {
                if (true) break FD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FD. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Fund_Code.setValue(pnd_T_Fund_Code.getValue(pnd_G));                                                                                                    //Natural: MOVE #T-FUND-CODE ( #G ) TO #W-FUND-CODE
            pnd_Len.setValue(6);                                                                                                                                          //Natural: MOVE 06 TO #LEN
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_W_Fund_Code_Pnd_Fund_Code_2, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2 #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            pnd_Table_Fund_Desc.getValue(pnd_G).setValue(pnd_Fund_Desc);                                                                                                  //Natural: MOVE #FUND-DESC TO #TABLE-FUND-DESC ( #G )
            pnd_Table_Comp_Desc.getValue(pnd_G).setValue(pnd_Comp_Desc);                                                                                                  //Natural: MOVE #COMP-DESC TO #TABLE-COMP-DESC ( #G )
            if (condition(pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("2") || pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("U")))                                                    //Natural: IF #FUND-CODE-1 = '2' OR = 'U'
            {
                pnd_Pay_Field.getValue(pnd_G).setValue("ANNUAL");                                                                                                         //Natural: MOVE 'ANNUAL' TO #PAY-FIELD ( #G )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("4") || pnd_W_Fund_Code_Pnd_Fund_Code_1.equals("W")))                                                //Natural: IF #FUND-CODE-1 = '4' OR = 'W'
                {
                    pnd_Pay_Field.getValue(pnd_G).setValue("MONTHLY");                                                                                                    //Natural: MOVE 'MONTHLY' TO #PAY-FIELD ( #G )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pay_Field.getValue(pnd_G).setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #PAY-FIELD ( #G )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 #WORK-RECORD2
        while (condition(getWorkFiles().read(2, gdaIaag588r.getPnd_Work_Record2())))
        {
            pnd_Read_Work2.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #READ-WORK2
            //*  WRITE '='  #READ-WORK2
            pnd_Fl_Date_Yyyymmdd_Alpha.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Status_Timestamp());                                                              //Natural: MOVE #WK2-STATUS-TIMESTAMP TO #FL-DATE-YYYYMMDD-ALPHA
            pnd_Get_Commuted_Value.setValue(true);                                                                                                                        //Natural: ASSIGN #GET-COMMUTED-VALUE := TRUE
            pnd_Check_Fund.setValue(true);                                                                                                                                //Natural: ASSIGN #CHECK-FUND := TRUE
            pnd_X.reset();                                                                                                                                                //Natural: RESET #X #TOTAL #CHECK-TOTAL-DCI #NUM-OF-CREF-FUNDS
            pnd_Total.reset();
            pnd_Check_Total_Dci.reset();
            pnd_Num_Of_Cref_Funds.reset();
            if (condition(!(pnd_Fl_Date_Yyyymmdd_Alpha_Pnd_Fl_Date_Yyyymmdd_N.equals(ldaIaal588r.getIaa_Parm_Card_Pnd_Parm_Date_N()))))                                   //Natural: ACCEPT IF #FL-DATE-YYYYMMDD-N EQ #PARM-DATE-N
            {
                continue;
            }
            pnd_Time_Selects.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TIME-SELECTS
            ldaIaal588r.getPnd_Num().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnts());                                                                      //Natural: MOVE #WK2-INSTLLMNTS TO #NUM
            if (condition(!(ldaIaal588r.getPnd_Num().greater(getZero()))))                                                                                                //Natural: ACCEPT IF #NUM > 0
            {
                continue;
            }
            ldaIaal588r.getPnd_Payment_Records().nadd(1);                                                                                                                 //Natural: ADD 1 TO #PAYMENT-RECORDS
            if (condition(ldaIaal588r.getPnd_Diff_Accounting_Date().notEquals("Y")))                                                                                      //Natural: IF #DIFF-ACCOUNTING-DATE NE 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM #FILL-UP-HEADERS
                sub_Pnd_Fill_Up_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte().equals(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte())))                               //Natural: IF #WK2-ACCTG-DTE EQ #WK2-PYMNT-DTE
            {
                ldaIaal588r.getPnd_M().setValue(1);                                                                                                                       //Natural: MOVE 1 TO #M
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal588r.getPnd_M().setValue(2);                                                                                                                       //Natural: MOVE 2 TO #M
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal588r.getPnd_Tab_Total_Num_Of_Pay().getValue(ldaIaal588r.getPnd_M()).nadd(1);                                                                           //Natural: ADD 1 TO #TAB-TOTAL-NUM-OF-PAY ( #M )
            ldaIaal588r.getPnd_Check().reset();                                                                                                                           //Natural: RESET #CHECK #EFT #EFT-HOLD
            ldaIaal588r.getPnd_Eft().reset();
            ldaIaal588r.getPnd_Eft_Hold().reset();
            short decideConditionsMet1029 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK2-TYPE-REQ-IND = 1
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Type_Req_Ind().equals(1)))
            {
                decideConditionsMet1029++;
                ldaIaal588r.getPnd_Check().setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #CHECK
            }                                                                                                                                                             //Natural: WHEN #WK2-TYPE-REQ-IND = 2
            else if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Type_Req_Ind().equals(2)))
            {
                decideConditionsMet1029++;
                ldaIaal588r.getPnd_Eft().setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #EFT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, NEWLINE,"PAUDIT-TYPE-REQ-IND IS NOT 1 OR 2  PIN ==>",gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Id_Nbr());                             //Natural: WRITE / 'PAUDIT-TYPE-REQ-IND IS NOT 1 OR 2  PIN ==>' #WK2-ID-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_W_Contract.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr());                                                                           //Natural: ASSIGN #W-CONTRACT := #WK2-CNTRCT-PPCN-NBR
            pnd_W_Payee.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Payee_Cde());                                                                                    //Natural: ASSIGN #W-PAYEE := #WK2-PAYEE-CDE
            pnd_Iaan425x_Pnd_Iaan425x_Ppcn_Nbr.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr());                                                       //Natural: ASSIGN #IAAN425X-PPCN-NBR := #WK2-CNTRCT-PPCN-NBR
            pnd_Iaan425x_Pnd_Iaan425x_Pyee_Cde.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Payee_Cde());                                                             //Natural: ASSIGN #IAAN425X-PYEE-CDE := #WK2-PAYEE-CDE
            pnd_Iaan425x_Pnd_Iaan425x_Parm_Dte.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Proof_Dte());                                                             //Natural: ASSIGN #IAAN425X-PARM-DTE := #WK2-PROOF-DTE
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CNTRCT
            sub_Pnd_Process_Cntrct();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CPR
            sub_Pnd_Process_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Nmb.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnts());                                                                                       //Natural: MOVE #WK2-INSTLLMNTS TO #NMB
            pnd_Bottom_Date.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Lst_Pymnt_Dte());                                                                            //Natural: ASSIGN #BOTTOM-DATE := #WK2-LST-PYMNT-DTE
            DbsUtil.callnat(Iaan460.class , getCurrentProcessState(), pnd_W_Contract, pnd_W_Payee, pnd_Tab_Install_Date.getValue(1,":",120), pnd_Tab_Per_Amt.getValue(1,":",120),  //Natural: CALLNAT 'IAAN460' #W-CONTRACT #W-PAYEE #TAB-INSTALL-DATE ( 1:120 ) #TAB-PER-AMT ( 1:120 ) #TAB-DIV-AMT ( 1:120 ) #TAB-FILE ( 1:120 ) #TAB-INVERSE-DATE ( 1:120 ) #NUM-OF-INSTLMNTS #BOTTOM-DATE #RET-CDE
                pnd_Tab_Div_Amt.getValue(1,":",120), pnd_Tab_File.getValue(1,":",120), pnd_Tab_Inverse_Date.getValue(1,":",120), pnd_Num_Of_Instlmnts, pnd_Bottom_Date, 
                pnd_Ret_Cde);
            if (condition(Global.isEscape())) return;
            short decideConditionsMet1054 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #EFT = 'Y'
            if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))
            {
                decideConditionsMet1054++;
                ldaIaal588r.getPnd_Tab_Num_Eft().getValue(ldaIaal588r.getPnd_M()).nadd(1);                                                                                //Natural: ADD 1 TO #TAB-NUM-EFT ( #M )
            }                                                                                                                                                             //Natural: WHEN #CHECK = 'Y'
            else if (condition(ldaIaal588r.getPnd_Check().equals("Y")))
            {
                decideConditionsMet1054++;
                ldaIaal588r.getPnd_Tab_Num_Check().getValue(ldaIaal588r.getPnd_M()).nadd(1);                                                                              //Natural: ADD 1 TO #TAB-NUM-CHECK ( #M )
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1054 > 0))
            {
                ldaIaal588r.getPnd_Tab_Num_Paym().getValue(ldaIaal588r.getPnd_M()).nadd(1);                                                                               //Natural: ADD 1 TO #TAB-NUM-PAYM ( #M )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #NUM
            for (ldaIaal588r.getPnd_I().setValue(1); condition(ldaIaal588r.getPnd_I().lessOrEqual(ldaIaal588r.getPnd_Num())); ldaIaal588r.getPnd_I().nadd(1))
            {
                FE:                                                                                                                                                       //Natural: FOR #A = 1 TO #NUM-OF-INSTLMNTS
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Num_Of_Instlmnts)); pnd_A.nadd(1))
                {
                    if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dte().getValue(ldaIaal588r.getPnd_I()).equals(pnd_Tab_Install_Date_Pnd_Tab_Install_Date_N.getValue(pnd_A)))) //Natural: IF #WK2-INSTLLMNT-DTE ( #I ) = #TAB-INSTALL-DATE-N ( #A )
                    {
                        if (true) break FE;                                                                                                                               //Natural: ESCAPE BOTTOM ( FE. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_A.equals(pnd_Num_Of_Instlmnts)))                                                                                                    //Natural: IF #A = #NUM-OF-INSTLMNTS
                    {
                        if (true) break FE;                                                                                                                               //Natural: ESCAPE BOTTOM ( FE. )
                        //*  10/01/2001
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* **
                //*  07/30/2001
                if (condition(pnd_Num_Of_Instlmnts.equals(1)))                                                                                                            //Natural: IF #NUM-OF-INSTLMNTS = 1
                {
                    pnd_A.setValue(1);                                                                                                                                    //Natural: ASSIGN #A := 1
                }                                                                                                                                                         //Natural: END-IF
                //* **
                pnd_Dci_Rem.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci().getValue(ldaIaal588r.getPnd_I()));                                           //Natural: ASSIGN #DCI-REM := #WK2-INSTLLMNT-DCI ( #I )
                pnd_Total_Dci.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci().getValue(ldaIaal588r.getPnd_I()));                                         //Natural: ASSIGN #TOTAL-DCI := #WK2-INSTLLMNT-DCI ( #I )
                pnd_Total_Rem.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross().getValue(ldaIaal588r.getPnd_I()));                                       //Natural: ASSIGN #TOTAL-REM := #WK2-INSTLLMNT-GROSS ( #I )
                pnd_Total_Payment.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross().getValue(ldaIaal588r.getPnd_I()));                                   //Natural: ASSIGN #TOTAL-PAYMENT := #WK2-INSTLLMNT-GROSS ( #I )
                if (condition(pnd_Tab_File.getValue(pnd_A).equals("F")))                                                                                                  //Natural: IF #TAB-FILE ( #A ) = 'F'
                {
                                                                                                                                                                          //Natural: PERFORM #READ-FUND
                    sub_Pnd_Read_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      MOVE TRUE TO #TAB-FILE-MATCH
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Tab_File.getValue(pnd_A).equals("H")))                                                                                              //Natural: IF #TAB-FILE ( #A ) = 'H'
                    {
                                                                                                                                                                          //Natural: PERFORM #READ-HISTORY
                        sub_Pnd_Read_History();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        MOVE TRUE TO #TAB-FILE-MATCH
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*        MOVE FALSE TO #TAB-FILE-MATCH
                        getReports().write(0, "ERROR IN IAAN460 TABLE-FILE FIELD",pnd_Tab_File.getValue(pnd_A),NEWLINE,"FOR: ",pnd_W_Contract,pnd_W_Payee,                //Natural: WRITE 'ERROR IN IAAN460 TABLE-FILE FIELD' #TAB-FILE ( #A ) / 'FOR: ' #W-CONTRACT #W-PAYEE '=' #I '=' #NUM '=' #A
                            "=",ldaIaal588r.getPnd_I(),"=",ldaIaal588r.getPnd_Num(),"=",pnd_A);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rec.reset();                                                                                                                                              //Natural: RESET #REC
            DbsUtil.callnat(Iaan425x.class , getCurrentProcessState(), pnd_Iaan425x);                                                                                     //Natural: CALLNAT 'IAAN425X' #IAAN425X
            if (condition(Global.isEscape())) return;
            pnd_Table_File.setValue(pnd_Iaan425x_Pnd_Iaan425x_Master_Or_Hist);                                                                                            //Natural: MOVE #IAAN425X-MASTER-OR-HIST TO #TABLE-FILE
            DbsUtil.callnat(Iaan400x.class , getCurrentProcessState(), pnd_Todays_Dte);                                                                                   //Natural: CALLNAT 'IAAN400X' #TODAYS-DTE
            if (condition(Global.isEscape())) return;
            pdaIaaa420.getIaaa420_Pnd_For_Date().setValue(pnd_Todays_Dte);                                                                                                //Natural: ASSIGN IAAA420.#FOR-DATE := #TODAYS-DTE
            //*  IF #TAB-FILE-MATCH
                                                                                                                                                                          //Natural: PERFORM GET-COMMUTED-VALUE
            sub_Get_Commuted_Value();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END-IF
            pnd_Report_Type.setValue("(LUMP SUM) ");                                                                                                                      //Natural: MOVE '(LUMP SUM) ' TO #REPORT-TYPE
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        //*  *********  START OF PRINTING REPORT *********
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM WRITE-SUB-HEADING
        sub_Write_Sub_Heading();
        if (condition(Global.isEscape())) {return;}
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CNTRCT
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CPR
        //*  -------------------------- *
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-UP-HEADERS
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-FUND
        //* *
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-HISTORY
        //* *Y2NCTS
        //* *
        //* *
        //* *
        //*       OLD-TIAA-RATES.CMPNY-FUND-CDE := '243'
        //*        COMPUTE #W-FUND-DTE = 100000000 - FUND-INVRSE-LST-PD-DTE
        //* *Y2NCTS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-SINGLE-BYTE-FUND
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-UNIT-VALUE
        //* * #RC
        //* *
        //* *
        //* ***********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUB-HEADING
        //*  -------------------- *
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CREF-COMM-VAL
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-COMM-VAL
        //*  ----------------------- *
        //*  WRITE ' COM VAL**'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-LAST-FUND-DCI
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DCI-PARA
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMMUTED-VALUE
        //*  ---------------------- *
        //*  WRITE ' READ FUND RECORD ***  ' '=' #REC
        //* *************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-GRAND-TOTAL
        //* ************************  O N   E R R O R  ****************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Pnd_Process_Cntrct() throws Exception                                                                                                                //Natural: #PROCESS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------- *
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #W-CONTRACT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_W_Contract, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pnd_W_Contract);                                                                                           //Natural: WRITE 'NO CONTRACT FOUND FOR ' #W-CONTRACT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD = 0
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(1);                                                                                                           //Natural: ASSIGN #ISSUE-DATE-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Issue_Date_A_Pnd_Issue_Date_Dd.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                             //Natural: ASSIGN #ISSUE-DATE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Issue_Date_A_Pnd_Issue_Date_Yyyymm.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte());                                                                //Natural: ASSIGN #ISSUE-DATE-YYYYMM := IAA-CNTRCT.CNTRCT-ISSUE-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Process_Cpr() throws Exception                                                                                                                   //Natural: #PROCESS-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                                //Natural: ASSIGN #CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_W_Payee);                                                                                                  //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #W-PAYEE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND02", true)))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CPR RECORD FOUND FOR ",pnd_W_Contract,pnd_W_Payee);                                                                             //Natural: WRITE 'NO CPR RECORD FOUND FOR ' #W-CONTRACT #W-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Xfr_Issue_Date_A.setValueEdited(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #XFR-ISSUE-DATE-A
            if (condition(pnd_Xfr_Issue_Date_A.equals(" ")))                                                                                                              //Natural: IF #XFR-ISSUE-DATE-A = ' '
            {
                pnd_W_Issue_Date.setValue(pnd_Issue_Date_A_Pnd_Issue_Date_N);                                                                                             //Natural: ASSIGN #W-ISSUE-DATE := #ISSUE-DATE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Issue_Date.setValue(pnd_Xfr_Issue_Date_A_Pnd_Xfr_Issue_Date_N);                                                                                     //Natural: ASSIGN #W-ISSUE-DATE := #XFR-ISSUE-DATE-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Fill_Up_Headers() throws Exception                                                                                                               //Natural: #FILL-UP-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------ *
        if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte().greater(getZero()) && gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte().greater(getZero())))  //Natural: IF #WK2-ACCTG-DTE > 0 AND #WK2-PYMNT-DTE > 0
        {
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte().notEquals(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte())))                            //Natural: IF #WK2-ACCTG-DTE NE #WK2-PYMNT-DTE
            {
                ldaIaal588r.getPnd_Title_Variable2().getValue(2).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Acctg_Dte());                                           //Natural: MOVE #WK2-ACCTG-DTE TO #TITLE-VARIABLE2 ( 2 )
                ldaIaal588r.getPnd_Title_Variable1().getValue(1).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte());                                           //Natural: MOVE #WK2-PYMNT-DTE TO #TITLE-VARIABLE1 ( 1 ) #TITLE-VARIABLE2 ( 1 ) #TITLE-VARIABLE1 ( 2 )
                ldaIaal588r.getPnd_Title_Variable2().getValue(1).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte());
                ldaIaal588r.getPnd_Title_Variable1().getValue(2).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte());
                ldaIaal588r.getPnd_Diff_Accounting_Date().setValue("Y");                                                                                                  //Natural: MOVE 'Y' TO #DIFF-ACCOUNTING-DATE
                getReports().write(0, "NOTE !!! REPORT HAS 2 DIFFERENT ACCOUNTING DATES");                                                                                //Natural: WRITE 'NOTE !!! REPORT HAS 2 DIFFERENT ACCOUNTING DATES'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal588r.getPnd_Title_Variable1().getValue(1).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte());                                           //Natural: MOVE #WK2-PYMNT-DTE TO #TITLE-VARIABLE1 ( 1 ) #TITLE-VARIABLE2 ( 1 )
                ldaIaal588r.getPnd_Title_Variable2().getValue(1).setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pymnt_Dte());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Read_Fund() throws Exception                                                                                                                     //Natural: #READ-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------ *
        //*  WRITE 'READ FUND'
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                               //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_W_Payee);                                                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE
        pnd_Cntrct_Fund_Key_Pnd_W_Fnd_Code.setValue(" ");                                                                                                                 //Natural: ASSIGN #W-FND-CODE := ' '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "RQ",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        RQ:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("RQ")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                FP:                                                                                                                                                       //Natural: FOR #N = 1 TO 60
                for (ldaIaal588r.getPnd_N().setValue(1); condition(ldaIaal588r.getPnd_N().lessOrEqual(60)); ldaIaal588r.getPnd_N().nadd(1))
                {
                    if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals(pnd_T_Fund_Code.getValue(ldaIaal588r.getPnd_N()))))                      //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = #T-FUND-CODE ( #N )
                    {
                        if (condition(pnd_Check_Fund.getBoolean()))                                                                                                       //Natural: IF #CHECK-FUND
                        {
                            if (condition(pnd_Table_Fund_Desc.getValue(ldaIaal588r.getPnd_N()).equals("REA") || DbsUtil.maskMatches(pnd_Table_Fund_Desc.getValue(ldaIaal588r.getPnd_N()), //Natural: IF #TABLE-FUND-DESC ( #N ) = 'REA' OR #TABLE-FUND-DESC ( #N ) = MASK ( 'LRI' )
                                "'LRI'")))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Num_Of_Cref_Funds.nadd(1);                                                                                                            //Natural: ADD 1 TO #NUM-OF-CREF-FUNDS
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (true) break FP;                                                                                                                               //Natural: ESCAPE BOTTOM ( FP. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RQ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W_Fund_Code_1_3.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde());                                                                    //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE TO #W-FUND-CODE-1-3
                pnd_L.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L
                if (condition(pnd_L.equals(1)))                                                                                                                           //Natural: IF #L = 1
                {
                    gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt().getValue(ldaIaal588r.getPnd_I())); //Natural: ADD #WK2-INSTLLMNT-OVRPYMNT ( #I ) TO #TABLE-OVRPY ( #M,#N )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal588r.getPnd_I().equals(ldaIaal588r.getPnd_Num())))                                                                                   //Natural: IF #I = #NUM
                {
                    if (true) break RQ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( RQ. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("T")))                                                                                         //Natural: IF #W-FUND-CODE-1 = 'T'
                {
                    gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());   //Natural: ADD TIAA-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                    gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt());   //Natural: ADD TIAA-TOT-DIV-AMT TO #TABLE-DIVD ( #M,#N )
                    if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                       //Natural: IF #TOTAL-DCI = 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Payment.reset();                                                                                                                              //Natural: RESET #PAYMENT
                        pnd_Payment.compute(new ComputeParameters(false, pnd_Payment), pnd_Payment.add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt())); //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                        sub_Pnd_Dci_Para();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                             //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M())),      //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-GROSS-TOTAL ( #M )
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                    if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                                  //Natural: IF #EFT = 'Y'
                    {
                        pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M())),      //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-EFT-TOTAL ( #M )
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                            //Natural: IF #CHECK = 'Y'
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M())),  //Natural: ADD TIAA-TOT-PER-AMT TIAA-TOT-DIV-AMT TO #TAB-CHECK-TOTAL ( #M )
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()).add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A.setValue(pnd_Tab_Install_Date.getValue(pnd_A));                                                                       //Natural: ASSIGN #W-FUND-DTE-A := #TAB-INSTALL-DATE ( #A )
                    if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                                //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                    {
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()); //Natural: ADD TIAA-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                                                   //Natural: MOVE TIAA-TOT-PER-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RQ"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                         //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                  //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                              //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                                //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                        //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt());                          //Natural: ADD TIAA-TOT-PER-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3);                                                                                     //Natural: ASSIGN #FUND-2 := #W-FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                            //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RQ"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-UNITS-CNT ( 1 ) * #AUV
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_W_Amt);                                          //Natural: ADD #W-AMT TO #TABLE-GUAR ( #M,#N )
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(pnd_W_Amt);                                                                                                              //Natural: MOVE #W-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RQ"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RQ"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                         //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                        }                                                                                                                                                 //Natural: END-IF
                        //* *
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                             //Natural: ADD #W-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                              //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                           //Natural: ADD #W-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                        //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                     //Natural: ADD #W-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Check_Fund.setValue(false);                                                                                                                           //Natural: ASSIGN #CHECK-FUND := FALSE
                if (true) break RQ;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RQ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_History() throws Exception                                                                                                                  //Natural: #READ-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------- *
        //*  WRITE 'READ HISTORY'
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                           //Natural: ASSIGN #OLD-CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee.setValue(pnd_W_Payee);                                                                                                 //Natural: ASSIGN #OLD-CNTRCT-PAYEE := #W-PAYEE
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte.setValue(pnd_Tab_Inverse_Date.getValue(pnd_A));                                                                      //Natural: ASSIGN #OLD-INVERSE-PD-DTE := #TAB-INVERSE-DATE ( #A )
        pnd_Cntrct_Py_Dte_Key_Pnd_Old_Fund_Code.setValue(" ");                                                                                                            //Natural: ASSIGN #OLD-FUND-CODE := ' '
        ldaIaal205a.getVw_old_Tiaa_Rates().startDatabaseRead                                                                                                              //Natural: READ OLD-TIAA-RATES BY CNTRCT-PY-DTE-KEY STARTING FROM #CNTRCT-PY-DTE-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", ">=", pnd_Cntrct_Py_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PY_DTE_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal205a.getVw_old_Tiaa_Rates().readNextRow("R3")))
        {
            if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Ppcn_Nbr) && ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Payee_Cde().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Cntrct_Payee)  //Natural: IF OLD-TIAA-RATES.CNTRCT-PPCN-NBR = #OLD-CNTRCT-PPCN-NBR AND OLD-TIAA-RATES.CNTRCT-PAYEE-CDE = #OLD-CNTRCT-PAYEE AND OLD-TIAA-RATES.FUND-INVRSE-LST-PD-DTE = #OLD-INVERSE-PD-DTE
                && ldaIaal205a.getOld_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte().equals(pnd_Cntrct_Py_Dte_Key_Pnd_Old_Inverse_Pd_Dte)))
            {
                FY:                                                                                                                                                       //Natural: FOR #N = 1 TO 60
                for (ldaIaal588r.getPnd_N().setValue(1); condition(ldaIaal588r.getPnd_N().lessOrEqual(60)); ldaIaal588r.getPnd_N().nadd(1))
                {
                    pnd_Total.reset();                                                                                                                                    //Natural: RESET #TOTAL
                    if (condition(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde().equals(pnd_T_Fund_Code.getValue(ldaIaal588r.getPnd_N()))))                               //Natural: IF OLD-TIAA-RATES.CMPNY-FUND-CDE = #T-FUND-CODE ( #N )
                    {
                        if (condition(pnd_Check_Fund.getBoolean()))                                                                                                       //Natural: IF #CHECK-FUND
                        {
                            if (condition(pnd_Table_Fund_Desc.getValue(ldaIaal588r.getPnd_N()).equals("REA") || DbsUtil.maskMatches(pnd_Table_Fund_Desc.getValue(ldaIaal588r.getPnd_N()), //Natural: IF #TABLE-FUND-DESC ( #N ) = 'REA' OR #TABLE-FUND-DESC ( #N ) = MASK ( 'LRI' )
                                "'LRI'")))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Num_Of_Cref_Funds.nadd(1);                                                                                                            //Natural: ADD 1 TO #NUM-OF-CREF-FUNDS
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (true) break FY;                                                                                                                               //Natural: ESCAPE BOTTOM ( FY. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_W_Fund_Code_1_3.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cmpny_Fund_Cde());                                                                             //Natural: MOVE OLD-TIAA-RATES.CMPNY-FUND-CDE TO #W-FUND-CODE-1-3
                pnd_L.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L
                if (condition(pnd_L.equals(1)))                                                                                                                           //Natural: IF #L = 1
                {
                    //*      ADD PAUDIT-INSTLLMNT-OVRPYMNT(#I) TO #TABLE-OVRPY(#M,#N)
                    gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Ovrpymnt().getValue(ldaIaal588r.getPnd_I())); //Natural: ADD #WK2-INSTLLMNT-OVRPYMNT ( #I ) TO #TABLE-OVRPY ( #M,#N )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal588r.getPnd_I().equals(ldaIaal588r.getPnd_Num())))                                                                                   //Natural: IF #I = #NUM
                {
                    if (true) break R3;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R3. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("T")))                                                                                         //Natural: IF #W-FUND-CODE-1 = 'T'
                {
                    gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());     //Natural: ADD CNTRCT-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                    gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt());     //Natural: ADD CNTRCT-TOT-DIV-AMT TO #TABLE-DIVD ( #M,#N )
                    if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                       //Natural: IF #TOTAL-DCI = 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Payment.reset();                                                                                                                              //Natural: RESET #PAYMENT
                        pnd_Payment.compute(new ComputeParameters(false, pnd_Payment), pnd_Payment.add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt())); //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                        sub_Pnd_Dci_Para();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                             //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M())),      //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-GROSS-TOTAL ( #M )
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                    if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                                  //Natural: IF #EFT = 'Y'
                    {
                        pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M())),      //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-EFT-TOTAL ( #M )
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                            //Natural: IF #CHECK = 'Y'
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).compute(new ComputeParameters(false, pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M())),  //Natural: ADD CNTRCT-TOT-PER-AMT CNTRCT-TOT-DIV-AMT TO #TAB-CHECK-TOTAL ( #M )
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()).add(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Div_Amt()));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Fund_Dte_Pnd_W_Fund_Dte_A.setValue(pnd_Tab_Install_Date.getValue(pnd_A));                                                                       //Natural: ASSIGN #W-FUND-DTE-A := #TAB-INSTALL-DATE ( #A )
                    if (condition(((pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")) && pnd_W_Fund_Dte.greater(19970401)))) //Natural: IF ( #W-FUND-CODE-1 = 'U' OR = '2' ) AND #W-FUND-DTE > 19970401
                    {
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt()); //Natural: ADD CNTRCT-TOT-PER-AMT TO #TABLE-GUAR ( #M,#N )
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                                                     //Natural: MOVE CNTRCT-TOT-PER-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                         //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                    //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                              //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                                  //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                        //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(ldaIaal205a.getOld_Tiaa_Rates_Cntrct_Tot_Per_Amt());                            //Natural: ADD CNTRCT-TOT-PER-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Fund_2.setValue(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_2_3);                                                                                     //Natural: ASSIGN #FUND-2 := #W-FUND-CODE-2-3
                                                                                                                                                                          //Natural: PERFORM #GET-SINGLE-BYTE-FUND
                        sub_Pnd_Get_Single_Byte_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("U") || pnd_W_Fund_Code_1_3_Pnd_W_Fund_Code_1.equals("2")))                            //Natural: IF #W-FUND-CODE-1 = 'U' OR = '2'
                        {
                            pnd_Val_Meth.setValue("A");                                                                                                                   //Natural: MOVE 'A' TO #VAL-METH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Val_Meth.setValue("M");                                                                                                                   //Natural: MOVE 'M' TO #VAL-METH
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #GET-UNIT-VALUE
                        sub_Pnd_Get_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Amt.compute(new ComputeParameters(true, pnd_W_Amt), ldaIaal205a.getOld_Tiaa_Rates_Tiaa_No_Units().getValue(1).multiply(pnd_A26_Fields_Pnd_Auv)); //Natural: COMPUTE ROUNDED #W-AMT = TIAA-NO-UNITS ( 1 ) * #AUV
                        //* *
                        if (condition(pnd_Total_Dci.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-DCI = 0
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Payment.reset();                                                                                                                          //Natural: RESET #PAYMENT
                            pnd_Payment.setValue(pnd_W_Amt);                                                                                                              //Natural: MOVE #W-AMT TO #PAYMENT
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                            sub_Pnd_Dci_Para();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_Dci);                                         //Natural: ADD #DCI TO #TABLE-DCI ( #M,#N )
                        }                                                                                                                                                 //Natural: END-IF
                        //* *
                        gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(pnd_W_Amt);                                          //Natural: ADD #W-AMT TO #TABLE-GUAR ( #M,#N )
                        pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                             //Natural: ADD #W-AMT TO #TAB-GROSS-TOTAL ( #M )
                        if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                              //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                           //Natural: ADD #W-AMT TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                        //Natural: IF #CHECK = 'Y'
                            {
                                pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(pnd_W_Amt);                                                                     //Natural: ADD #W-AMT TO #TAB-CHECK-TOTAL ( #M )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Single_Byte_Fund() throws Exception                                                                                                          //Natural: #GET-SINGLE-BYTE-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------- *
        pnd_Fund_1.reset();                                                                                                                                               //Natural: RESET #FUND-1
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtn_Cde);                                                                  //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTN-CDE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Get_Unit_Value() throws Exception                                                                                                                //Natural: #GET-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------- *
        //*  WRITE ' INSIDE #GET-UNIT-VALUE'
        //* *
        //*  040512
        pnd_A26_Fields_Pnd_Auv.reset();                                                                                                                                   //Natural: RESET #AUV #AUV-RET-DTE #RETURN-CODE #A26-PRTC-DTE
        pnd_A26_Fields_Pnd_Auv_Ret_Dte.reset();
        pnd_A26_Fields_Pnd_Return_Code.reset();
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.reset();
        pnd_A26_Fields_Pnd_A26_Call_Type.setValue("P");                                                                                                                   //Natural: ASSIGN #A26-CALL-TYPE := 'P'
        pnd_A26_Fields_Pnd_A26_Fnd_Cde.setValue(pnd_Fund_1);                                                                                                              //Natural: ASSIGN #A26-FND-CDE := #FUND-1
        pnd_A26_Fields_Pnd_A26_Reval_Meth.setValue(pnd_Val_Meth);                                                                                                         //Natural: ASSIGN #A26-REVAL-METH := #VAL-METH
        pnd_A26_Fields_Pnd_A26_Req_Dte.setValue(pnd_W_Fund_Dte);                                                                                                          //Natural: ASSIGN #A26-REQ-DTE := #W-FUND-DTE
        pnd_A26_Fields_Pnd_A26_Prtc_Dte.setValue(pnd_W_Issue_Date);                                                                                                       //Natural: ASSIGN #A26-PRTC-DTE := #W-ISSUE-DATE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pnd_A26_Fields);                                                                                        //Natural: CALLNAT 'AIAN026' #A26-FIELDS
        if (condition(Global.isEscape())) return;
        //*  #RC                                  /* 040512
        //*  #RETURN-CODE                         /* 040512
        //*  #AUV #AUV-RET-DTE                    /* 040512
        //*  #DAYS-IN-REQUEST-MONTH #DAYS-IN-PARTICIP-MONTH /* 040512
        if (condition(pnd_A26_Fields_Pnd_Rc.greater(getZero())))                                                                                                          //Natural: IF #RC > 0
        {
            //*  040512
            getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Rc);                                                           //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RC
            if (Global.isEscape()) return;
            //*  040512
            getReports().write(0, NEWLINE,"ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ",pnd_A26_Fields_Pnd_Return_Code);                                                  //Natural: WRITE / 'ERROR IN LINKAGE AIAN026 - ERROR CODE ===> ' #RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "CONTRACT NUMBER =>",pnd_W_Contract,pnd_W_Payee);                                                                                       //Natural: WRITE 'CONTRACT NUMBER =>' #W-CONTRACT #W-PAYEE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE #AUV TO #W-AUV  /* 0401512
    }
    private void sub_Write_Sub_Heading() throws Exception                                                                                                                 //Natural: WRITE-SUB-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #J = 1 TO 2
        for (ldaIaal588r.getPnd_J().setValue(1); condition(ldaIaal588r.getPnd_J().lessOrEqual(2)); ldaIaal588r.getPnd_J().nadd(1))
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(ldaIaal588r.getPnd_Title_Variable1().getValue(ldaIaal588r.getPnd_J()).notEquals(getZero())))                                                    //Natural: IF #TITLE-VARIABLE1 ( #J ) NE 0
            {
                ldaIaal588r.getPnd_Date_Ccyymmdd().setValue(ldaIaal588r.getPnd_Title_Variable1().getValue(ldaIaal588r.getPnd_J()));                                       //Natural: MOVE #TITLE-VARIABLE1 ( #J ) TO #DATE-CCYYMMDD
                pnd_Y2_Yy_N.setValue(ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Yy());                                                                                     //Natural: MOVE #DATE-YY TO #Y2-YY-N
                ldaIaal588r.getPnd_W_Title_Variable1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),           //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #Y2-YY-A INTO #W-TITLE-VARIABLE1 LEAVING NO
                    "/", ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", pnd_Y2_Yy_N_Pnd_Y2_Yy_A));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal588r.getPnd_W_Title_Variable1().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #W-TITLE-VARIABLE1
            }                                                                                                                                                             //Natural: END-IF
            //* *
            if (condition(ldaIaal588r.getPnd_Title_Variable2().getValue(ldaIaal588r.getPnd_J()).notEquals(getZero())))                                                    //Natural: IF #TITLE-VARIABLE2 ( #J ) NE 0
            {
                ldaIaal588r.getPnd_Date_Ccyymmdd().setValue(ldaIaal588r.getPnd_Title_Variable2().getValue(ldaIaal588r.getPnd_J()));                                       //Natural: MOVE #TITLE-VARIABLE2 ( #J ) TO #DATE-CCYYMMDD
                pnd_Y2_Yy_N.setValue(ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Yy());                                                                                     //Natural: MOVE #DATE-YY TO #Y2-YY-N
                ldaIaal588r.getPnd_W_Title_Variable2().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),           //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #Y2-YY-A INTO #W-TITLE-VARIABLE2 LEAVING NO
                    "/", ldaIaal588r.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", pnd_Y2_Yy_N_Pnd_Y2_Yy_A));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal588r.getPnd_W_Title_Variable2().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #W-TITLE-VARIABLE2
            }                                                                                                                                                             //Natural: END-IF
            //* *
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(24),"CHECK DATE: ",ldaIaal588r.getPnd_W_Title_Variable1(),new TabSetting(72),               //Natural: WRITE ( 1 ) / 24T 'CHECK DATE: ' #W-TITLE-VARIABLE1 72T 'ACCOUNTING DATE: ' #W-TITLE-VARIABLE2
                "ACCOUNTING DATE: ",ldaIaal588r.getPnd_W_Title_Variable2());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(28),"COMMUTED AMT",new TabSetting(52),"GROSS GTD AMT",new TabSetting(75),"GROSS DIV AMT",new        //Natural: WRITE ( 1 ) 028T 'COMMUTED AMT' 052T 'GROSS GTD AMT' 075T 'GROSS DIV AMT' 100T 'DCI AMT' 115T 'OVERPAY AMT'
                TabSetting(100),"DCI AMT",new TabSetting(115),"OVERPAY AMT");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(29),"------------",new TabSetting(52),"-------------",new TabSetting(75),"-------------",new        //Natural: WRITE ( 1 ) 029T '------------' 052T '-------------' 075T '-------------' 100T '-------' 115T '-----------'
                TabSetting(100),"-------",new TabSetting(115),"-----------");
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            FZ:                                                                                                                                                           //Natural: FOR #W = 2 TO 60
            for (pnd_W.setValue(2); condition(pnd_W.lessOrEqual(60)); pnd_W.nadd(1))
            {
                if (condition(pnd_T_Fund_Code.getValue(pnd_W).equals(" ")))                                                                                               //Natural: IF #T-FUND-CODE ( #W ) = ' '
                {
                    if (true) break FZ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FZ. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_T_Fund_Code_1_3.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                                        //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-1-3
                    if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_2_3.greater("40")))                                                                                 //Natural: IF #T-FUND-CODE-2-3 > '40'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_T_Fund_Code_1_3_Pnd_T_Fund_Code_1.notEquals(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1)))                                      //Natural: IF #T-FUND-CODE-1 NE #T-FUND-CODE-HOLD-1
                        {
                            pnd_T_Fund_Code_Hold.setValue(pnd_T_Fund_Code.getValue(pnd_W));                                                                               //Natural: MOVE #T-FUND-CODE ( #W ) TO #T-FUND-CODE-HOLD
                            if (condition(pnd_T_Fund_Code_Hold_Pnd_T_Fund_Code_Hold_1.equals("W")))                                                                       //Natural: IF #T-FUND-CODE-HOLD-1 = 'W'
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().skip(1, 1);                                                                                                                  //Natural: SKIP ( 1 ) 1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Table_Comp_Desc.getValue(pnd_W),new TabSetting(8),pnd_Table_Fund_Desc.getValue(pnd_W),new  //Natural: WRITE ( 1 ) 001T #TABLE-COMP-DESC ( #W ) 008T #TABLE-FUND-DESC ( #W ) 017T #PAY-FIELD ( #W ) 027T #TABLE-CREF-COM-VAL-AMT ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 050T #TABLE-GUAR ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 073T #TABLE-DIVD ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 93T #TABLE-DCI ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 112T #TABLE-OVRPY ( #J,#W ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                        TabSetting(17),pnd_Pay_Field.getValue(pnd_W),new TabSetting(27),pnd_Table_Cref_Com_Val_Amt.getValue(ldaIaal588r.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),gdaIaag588r.getPnd_Table_Guar().getValue(ldaIaal588r.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(73),gdaIaag588r.getPnd_Table_Divd().getValue(ldaIaal588r.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(93),gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),gdaIaag588r.getPnd_Table_Ovrpy().getValue(ldaIaal588r.getPnd_J(),pnd_W), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FZ"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(1),"GROSS TOTAL",pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_J()),                   //Natural: WRITE ( 1 ) / 1X 'GROSS TOTAL' #TAB-GROSS-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'CHECK TOTAL' #TAB-CHECK-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'EFT   TOTAL' #TAB-EFT-TOTAL ( #J ) ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'CHECK COUNT' 9X #TAB-NUM-CHECK ( #J ) ( EM = ZZ9 ) / 1X 'EFT   COUNT' 9X #TAB-NUM-EFT ( #J ) ( EM = ZZ9 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(1),"CHECK TOTAL",pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_J()), new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(1),"EFT   TOTAL",pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_J()), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(1),"CHECK COUNT",new ColumnSpacing(9),ldaIaal588r.getPnd_Tab_Num_Check().getValue(ldaIaal588r.getPnd_J()), 
                new ReportEditMask ("ZZ9"),NEWLINE,new ColumnSpacing(1),"EFT   COUNT",new ColumnSpacing(9),ldaIaal588r.getPnd_Tab_Num_Eft().getValue(ldaIaal588r.getPnd_J()), 
                new ReportEditMask ("ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            if (condition(ldaIaal588r.getPnd_J().equals(1)))                                                                                                              //Natural: IF #J = 1
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_J()).notEquals(getZero())))                                                                     //Natural: IF #TAB-GROSS-TOTAL ( #J ) NE 0
            {
                pnd_Ls_Gross_Total.setValue(pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_J()));                                                                        //Natural: MOVE #TAB-GROSS-TOTAL ( #J ) TO #LS-GROSS-TOTAL
                pnd_Ls_Check_Total.setValue(pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_J()));                                                                        //Natural: MOVE #TAB-CHECK-TOTAL ( #J ) TO #LS-CHECK-TOTAL
                pnd_Ls_Eft_Total.setValue(pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_J()));                                                                            //Natural: MOVE #TAB-EFT-TOTAL ( #J ) TO #LS-EFT-TOTAL
                pnd_Ls_Check_Count.setValue(ldaIaal588r.getPnd_Tab_Num_Check().getValue(ldaIaal588r.getPnd_J()));                                                         //Natural: MOVE #TAB-NUM-CHECK ( #J ) TO #LS-CHECK-COUNT
                pnd_Ls_Eft_Count.setValue(ldaIaal588r.getPnd_Tab_Num_Eft().getValue(ldaIaal588r.getPnd_J()));                                                             //Natural: MOVE #TAB-NUM-EFT ( #J ) TO #LS-EFT-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-GRAND-TOTAL
        sub_Print_Grand_Total();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE '*** END OF PROGRAM IAAP588R  ***'
    }
    private void sub_Move_Cref_Comm_Val() throws Exception                                                                                                                //Natural: MOVE-CREF-COMM-VAL
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------- *
        //*  WRITE ' GET CREF-VALUE '
        pnd_X.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #X
        XY:                                                                                                                                                               //Natural: FOR #X = #X TO 40
        for (pnd_X.setValue(pnd_X); condition(pnd_X.lessOrEqual(40)); pnd_X.nadd(1))
        {
            //*  CHECK FUND CODE BASED ON RATE TABLE
            //*  IF RATE IS LE 20 IT IS MONTHLY
            //*  IF RATE IS GT 20 IT IS ANNUAL
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).greater(getZero())))                                                       //Natural: IF #WK2-RATE-BASIS ( #X ) GT 0
            {
                //*  ANNUAL RE
                if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).equals(9) || gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).equals(11))) //Natural: IF #WK2-RATE-BASIS ( #X ) EQ 09 OR = 11
                {
                    pnd_Cref_Fund_Pnd_Cref_Fund_1.setValue("U");                                                                                                          //Natural: MOVE 'U' TO #CREF-FUND-1
                    pnd_Cref_Fund_Pnd_Cref_Fund_2_N.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X));                                       //Natural: MOVE #WK2-RATE-BASIS ( #X ) TO #CREF-FUND-2-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  MONTHLY RE
                    if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).equals(29) || gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).equals(31))) //Natural: IF #WK2-RATE-BASIS ( #X ) EQ 29 OR = 31
                    {
                        pnd_Cref_Fund_Pnd_Cref_Fund_1.setValue("W");                                                                                                      //Natural: MOVE 'W' TO #CREF-FUND-1
                        pnd_Cref_Fund_Pnd_Cref_Fund_2_N.compute(new ComputeParameters(false, pnd_Cref_Fund_Pnd_Cref_Fund_2_N), gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).subtract(20)); //Natural: ASSIGN #CREF-FUND-2-N := #WK2-RATE-BASIS ( #X ) - 20
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).lessOrEqual(20)))                                              //Natural: IF #WK2-RATE-BASIS ( #X ) LE 20
                        {
                            pnd_Cref_Fund_Pnd_Cref_Fund_1.setValue("2");                                                                                                  //Natural: MOVE '2' TO #CREF-FUND-1
                            pnd_Cref_Fund_Pnd_Cref_Fund_2_N.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X));                               //Natural: MOVE #WK2-RATE-BASIS ( #X ) TO #CREF-FUND-2-N
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Cref_Fund_Pnd_Cref_Fund_1.setValue("4");                                                                                                  //Natural: MOVE '4' TO #CREF-FUND-1
                            pnd_Cref_Fund_Pnd_Cref_Fund_2_N.compute(new ComputeParameters(false, pnd_Cref_Fund_Pnd_Cref_Fund_2_N), gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rate_Basis().getValue(pnd_X).subtract(20)); //Natural: ASSIGN #CREF-FUND-2-N := #WK2-RATE-BASIS ( #X ) - 20
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  CHECK FUND CODE AGAINST COMPANY FUND AND IF = PER-PYMNT AMOUNT TO
                //*  THE TABLE FIELD
                if (condition(pnd_Cref_Fund.equals(pnd_T_Fund_Code.getValue(pnd_U))))                                                                                     //Natural: IF #CREF-FUND = #T-FUND-CODE ( #U )
                {
                    pnd_Table_Cref_Com_Val_Amt.getValue(ldaIaal588r.getPnd_M(),pnd_U).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));          //Natural: ADD #WK2-PER-PYMNT ( #X ) TO #TABLE-CREF-COM-VAL-AMT ( #M,#U )
                    pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));                       //Natural: ADD #WK2-PER-PYMNT ( #X ) TO #TAB-GROSS-TOTAL ( #M )
                    pnd_Total.nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));                                                                  //Natural: ADD #WK2-PER-PYMNT ( #X ) TO #TOTAL
                    pnd_Total_Payment.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Gross().getValue(ldaIaal588r.getPnd_Num()));                             //Natural: ASSIGN #TOTAL-PAYMENT := #WK2-INSTLLMNT-GROSS ( #NUM )
                    pnd_Payment.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));                                                            //Natural: ASSIGN #PAYMENT := #WK2-PER-PYMNT ( #X )
                    pnd_Total_Dci.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci().getValue(ldaIaal588r.getPnd_Num()));                                   //Natural: ASSIGN #TOTAL-DCI := #WK2-INSTLLMNT-DCI ( #NUM )
                    if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                                //Natural: IF #CHECK = 'Y'
                    {
                        pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));                   //Natural: ADD #WK2-PER-PYMNT ( #X ) TO #TAB-CHECK-TOTAL ( #M )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                              //Natural: IF #EFT = 'Y'
                        {
                            pnd_Tab_Eft_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_X));                 //Natural: ADD #WK2-PER-PYMNT ( #X ) TO #TAB-EFT-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Num_Of_Cref_Funds.equals(1)))                                                                                                       //Natural: IF #NUM-OF-CREF-FUNDS = 1
                    {
                        pnd_Dci.setValue(pnd_Total_Dci);                                                                                                                  //Natural: ASSIGN #DCI := #TOTAL-DCI
                        gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),pnd_U).nadd(pnd_Dci);                                                              //Natural: ADD #DCI TO #TABLE-DCI ( #M,#U )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM #DCI-PARA
                        sub_Pnd_Dci_Para();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("XY"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("XY"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_X.equals(pnd_Num_Of_Cref_Funds)))                                                                                               //Natural: IF #X = #NUM-OF-CREF-FUNDS
                        {
                                                                                                                                                                          //Natural: PERFORM CHECK-LAST-FUND-DCI
                            sub_Check_Last_Fund_Dci();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("XY"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("XY"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),pnd_U).nadd(pnd_Dci);                                                          //Natural: ADD #DCI TO #TABLE-DCI ( #M,#U )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),pnd_U).nadd(pnd_Dci);                                                          //Natural: ADD #DCI TO #TABLE-DCI ( #M,#U )
                            pnd_Check_Total_Dci.nadd(pnd_Dci);                                                                                                            //Natural: ADD #DCI TO #CHECK-TOTAL-DCI
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (true) break XY;                                                                                                                                   //Natural: ESCAPE BOTTOM ( XY. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Move_Tiaa_Comm_Val() throws Exception                                                                                                                //Natural: MOVE-TIAA-COMM-VAL
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #V = 1 TO 40
        for (pnd_V.setValue(1); condition(pnd_V.lessOrEqual(40)); pnd_V.nadd(1))
        {
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V).greater(getZero()) || gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue(pnd_V).greater(getZero()))) //Natural: IF #WK2-PER-PYMNT ( #V ) GT 0 OR #WK2-PER-DVDND ( #V ) GT 0
            {
                if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V).notEquals(getZero())))                                                  //Natural: IF #WK2-PER-PYMNT ( #V ) NE 0
                {
                    pnd_Table_Cref_Com_Val_Amt.getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V)); //Natural: ADD #WK2-PER-PYMNT ( #V ) TO #TABLE-CREF-COM-VAL-AMT ( #M,#N )
                    pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V));                       //Natural: ADD #WK2-PER-PYMNT ( #V ) TO #TAB-GROSS-TOTAL ( #M )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Table_Cref_Com_Val_Amt.getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue(pnd_V)); //Natural: ADD #WK2-PER-DVDND ( #V ) TO #TABLE-CREF-COM-VAL-AMT ( #M,#N )
                    pnd_Tab_Gross_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue(pnd_V));                       //Natural: ADD #WK2-PER-DVDND ( #V ) TO #TAB-GROSS-TOTAL ( #M )
                }                                                                                                                                                         //Natural: END-IF
                gdaIaag588r.getPnd_Table_Dci().getValue(ldaIaal588r.getPnd_M(),ldaIaal588r.getPnd_N()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Instllmnt_Dci().getValue(ldaIaal588r.getPnd_Num())); //Natural: ADD #WK2-INSTLLMNT-DCI ( #NUM ) TO #TABLE-DCI ( #M,#N )
                if (condition(ldaIaal588r.getPnd_Check().equals("Y")))                                                                                                    //Natural: IF #CHECK = 'Y'
                {
                    if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V).notEquals(getZero())))                                              //Natural: IF #WK2-PER-PYMNT ( #V ) NE 0
                    {
                        pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V));                   //Natural: ADD #WK2-PER-PYMNT ( #V ) TO #TAB-CHECK-TOTAL ( #M )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue(pnd_V));                   //Natural: ADD #WK2-PER-DVDND ( #V ) TO #TAB-CHECK-TOTAL ( #M )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal588r.getPnd_Eft().equals("Y")))                                                                                                  //Natural: IF #EFT = 'Y'
                    {
                        if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V).notEquals(getZero())))                                          //Natural: IF #WK2-PER-PYMNT ( #V ) NE 0
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Pymnt().getValue(pnd_V));               //Natural: ADD #WK2-PER-PYMNT ( #V ) TO #TAB-CHECK-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tab_Check_Total.getValue(ldaIaal588r.getPnd_M()).nadd(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Per_Dvdnd().getValue(pnd_V));               //Natural: ADD #WK2-PER-DVDND ( #V ) TO #TAB-CHECK-TOTAL ( #M )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Last_Fund_Dci() throws Exception                                                                                                               //Natural: CHECK-LAST-FUND-DCI
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------- *
        if (condition(pnd_Total_Dci.equals(pnd_Check_Total_Dci)))                                                                                                         //Natural: IF #TOTAL-DCI = #CHECK-TOTAL-DCI
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_Fund_Dci.compute(new ComputeParameters(false, pnd_Last_Fund_Dci), pnd_Total_Dci.subtract(pnd_Check_Total_Dci));                                      //Natural: ASSIGN #LAST-FUND-DCI := #TOTAL-DCI - #CHECK-TOTAL-DCI
            pnd_Dci.setValue(pnd_Last_Fund_Dci);                                                                                                                          //Natural: ASSIGN #DCI := #LAST-FUND-DCI
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Dci_Para() throws Exception                                                                                                                      //Natural: #DCI-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------- *
        DbsUtil.callnat(Iaan587b.class , getCurrentProcessState(), pnd_Payment, pnd_Total_Payment, pnd_Total_Dci, pnd_Dci, pnd_Total_Rem, pnd_Dci_Rem);                   //Natural: CALLNAT 'IAAN587B' #PAYMENT #TOTAL-PAYMENT #TOTAL-DCI #DCI #TOTAL-REM #DCI-REM
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------ *
        //* *
        if (condition(DbsUtil.maskMatches(ldaIaal588r.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal588r.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Commuted_Value() throws Exception                                                                                                                //Natural: GET-COMMUTED-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_W_Contract);                                                                                               //Natural: ASSIGN #W-CNTRCT-PPCN-NBR := #W-CONTRACT
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_W_Payee);                                                                                                     //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-PAYEE
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "XX",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        XX:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("XX")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE
            {
                pnd_Rec.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC
                XZ:                                                                                                                                                       //Natural: FOR #U = 1 TO 60
                for (pnd_U.setValue(1); condition(pnd_U.lessOrEqual(60)); pnd_U.nadd(1))
                {
                    if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals(pnd_T_Fund_Code.getValue(pnd_U))))                                       //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = #T-FUND-CODE ( #U )
                    {
                        if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde().equals("T") || ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde().equals("U")    //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE = 'T' OR = 'U' OR = 'W'
                            || ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde().equals("W")))
                        {
                            pdaIaaa420.getIaaa420_Pnd_Fund_Ind().setValue("T");                                                                                           //Natural: ASSIGN IAAA420.#FUND-IND := 'T'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde().equals("2") || ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde().equals("4")))  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-CDE = '2' OR = '4'
                        {
                            pdaIaaa420.getIaaa420_Pnd_Fund_Ind().setValue("C");                                                                                           //Natural: ASSIGN IAAA420.#FUND-IND := 'C'
                        }                                                                                                                                                 //Natural: END-IF
                        if (true) break XZ;                                                                                                                               //Natural: ESCAPE BOTTOM ( XZ. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("XX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("XX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break XX;                                                                                                                                       //Natural: ESCAPE BOTTOM ( XX. )
            }                                                                                                                                                             //Natural: END-IF
            vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY
            (
            "READ01",
            new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
            1
            );
            READ01:
            while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
            {
                pdaIaaa420.getIaaa420_Pnd_Check_Date_A().setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO IAAA420.#CHECK-DATE-A
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("XX"))) break;
                else if (condition(Global.isEscapeBottomImmediate("XX"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaIaaa420.getIaaa420_Pnd_Payment_Method().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Type_Ind());                                                      //Natural: MOVE #WK2-TYPE-IND TO #PAYMENT-METHOD
            ldaIaal588r.getIaal588r_Pnd_Parm_Cntrct().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr());                                                //Natural: MOVE #WK2-CNTRCT-PPCN-NBR TO #PARM-CNTRCT IAAA420.#PPCN
            pdaIaaa420.getIaaa420_Pnd_Ppcn().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr());
            ldaIaal588r.getIaal588r_Pnd_Parm_Payee().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Payee_Cde());                                                       //Natural: MOVE #WK2-PAYEE-CDE TO #PARM-PAYEE IAAA420.#PAYEE
            pdaIaaa420.getIaaa420_Pnd_Payee().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Payee_Cde());
            pnd_Iaan425x_Pnd_Iaan425x_Parm_Dte.setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Proof_Dte());                                                             //Natural: MOVE #WK2-PROOF-DTE TO #IAAN425X-PARM-DTE IAAA420.#EFF-DATE
            pdaIaaa420.getIaaa420_Pnd_Eff_Date().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Proof_Dte());
            ldaIaal588r.getPnd_Iaan425b_Pnd_Iaan425b_Date_Of_Death().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Dod_Dte());                                         //Natural: MOVE #WK2-DOD-DTE TO #IAAN425B-DATE-OF-DEATH IAAA420.#DECEDENT-DOD
            pdaIaaa420.getIaaa420_Pnd_Decedent_Dod().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Dod_Dte());
            pdaIaaa420.getIaaa420_Pnd_Decedent_Tax_Id_Nbr().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Pyee_Tax_Id_Nbr());                                          //Natural: MOVE #WK2-PYEE-TAX-ID-NBR TO IAAA420.#DECEDENT-TAX-ID-NBR
            pdaIaaa420.getIaaa420_Pnd_Payee_Residency().setValue(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Rsdncy_Cde());                                                   //Natural: MOVE #WK2-RSDNCY-CDE TO IAAA420.#PAYEE-RESIDENCY
            ldaIaal588r.getIaal588r_Pnd_Parm_Com_Val_Amt().getValue("*").reset();                                                                                         //Natural: RESET #PARM-COM-VAL-AMT ( * )
            DbsUtil.callnat(Iaan425x.class , getCurrentProcessState(), pnd_Iaan425x);                                                                                     //Natural: CALLNAT 'IAAN425X' #IAAN425X
            if (condition(Global.isEscape())) return;
            pnd_Iaan427d_Pnd_Iaan427d_Fund_File.setValue(pnd_Iaan425x_Pnd_Iaan425x_Master_Or_Hist);                                                                       //Natural: ASSIGN #IAAN427D-FUND-FILE := #IAAN425X-MASTER-OR-HIST
            pnd_Iaan427d_Pnd_Iaan427d_Inverse_Date.setValue(pnd_Iaan425x_Pnd_Iaan425x_Inverse_Dte);                                                                       //Natural: ASSIGN #IAAN427D-INVERSE-DATE := #IAAN425X-INVERSE-DTE
            short decideConditionsMet1798 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK2-CNTRCT-PPCN-NBR GE 'Z'
            if (condition(gdaIaag588r.getPnd_Work_Record2_Pnd_Wk2_Cntrct_Ppcn_Nbr().greaterOrEqual("Z")))
            {
                decideConditionsMet1798++;
                                                                                                                                                                          //Natural: PERFORM MOVE-CREF-COMM-VAL
                sub_Move_Cref_Comm_Val();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("XX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("XX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #TABLE-COMP-DESC ( #U ) = 'TIAA'
            else if (condition(pnd_Table_Comp_Desc.getValue(pnd_U).equals("TIAA")))
            {
                decideConditionsMet1798++;
                if (condition(pnd_Get_Commuted_Value.getBoolean()))                                                                                                       //Natural: IF #GET-COMMUTED-VALUE
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-COMM-VAL
                    sub_Move_Tiaa_Comm_Val();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("XX"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("XX"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Print_Grand_Total() throws Exception                                                                                                                 //Natural: PRINT-GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Report_Type.setValue("GRAND TOTAL ");                                                                                                                         //Natural: MOVE 'GRAND TOTAL ' TO #REPORT-TYPE
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #WORK-REC-3
        while (condition(getWorkFiles().read(3, pnd_Work_Rec_3)))
        {
            pnd_Grand_Total_Gross.compute(new ComputeParameters(false, pnd_Grand_Total_Gross), pnd_Work_Rec_3_Pnd_Pp_Gross_Total.add(pnd_Ls_Gross_Total));                //Natural: ASSIGN #GRAND-TOTAL-GROSS := #PP-GROSS-TOTAL + #LS-GROSS-TOTAL
            pnd_Grand_Total_Eft_Count.compute(new ComputeParameters(false, pnd_Grand_Total_Eft_Count), pnd_Work_Rec_3_Pnd_Pp_Eft_Count.add(pnd_Ls_Eft_Count));            //Natural: ASSIGN #GRAND-TOTAL-EFT-COUNT := #PP-EFT-COUNT + #LS-EFT-COUNT
            pnd_Grand_Total_Eft.compute(new ComputeParameters(false, pnd_Grand_Total_Eft), pnd_Work_Rec_3_Pnd_Pp_Eft_Total.add(pnd_Ls_Eft_Total));                        //Natural: ASSIGN #GRAND-TOTAL-EFT := #PP-EFT-TOTAL + #LS-EFT-TOTAL
            pnd_Grand_Total_Check_Count.compute(new ComputeParameters(false, pnd_Grand_Total_Check_Count), pnd_Work_Rec_3_Pnd_Pp_Check_Count.add(pnd_Ls_Check_Count));    //Natural: ASSIGN #GRAND-TOTAL-CHECK-COUNT := #PP-CHECK-COUNT + #LS-CHECK-COUNT
            pnd_Grand_Total_Check.compute(new ComputeParameters(false, pnd_Grand_Total_Check), pnd_Work_Rec_3_Pnd_Pp_Check_Total.add(pnd_Ls_Check_Total));                //Natural: ASSIGN #GRAND-TOTAL-CHECK := #PP-CHECK-TOTAL + #LS-CHECK-TOTAL
            pnd_Grand_Total_Num_Payments.compute(new ComputeParameters(false, pnd_Grand_Total_Num_Payments), pnd_Work_Rec_3_Pnd_Pp_Num_Payments.add(pnd_Ls_Num_Payments)); //Natural: ASSIGN #GRAND-TOTAL-NUM-PAYMENTS := #PP-NUM-PAYMENTS + #LS-NUM-PAYMENTS
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(1),"GROSS TOTAL CONTINUED ",new ColumnSpacing(4),pnd_Work_Rec_3_Pnd_Pp_Gross_Total,  //Natural: WRITE ( 1 ) /// 1X 'GROSS TOTAL CONTINUED ' 4X #PP-GROSS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'GROSS TOTAL LUMP SUM  ' 4X #LS-GROSS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 4X 'GRAND TOTAL GROSS     ' #GRAND-TOTAL-GROSS ( EM = ZZZ,ZZZ,ZZ9.99 ) // 1X 'CHECK TOTAL CONTINUED ' 4X #PP-CHECK-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'CHECK TOTAL LUMP SUM  ' 4X #LS-CHECK-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 4X 'GRAND TOTAL CHECK     ' #GRAND-TOTAL-CHECK ( EM = ZZZ,ZZZ,ZZ9.99 ) // 1X 'EFT   TOTAL CONTINUED ' 4X #PP-EFT-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 1X 'EFT   TOTAL LUMP SUM  ' 4X #LS-EFT-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 4X 'GRAND TOTAL EFT       ' #GRAND-TOTAL-EFT ( EM = ZZZ,ZZZ,ZZ9.99 ) // 1X 'CHECK COUNT CONTINUED ' 5X #PP-CHECK-COUNT / 1X 'CHECK COUNT LUMP SUM  ' 5X #LS-CHECK-COUNT / 4X 'GRAND TOTAL CHECK COUNT' #GRAND-TOTAL-CHECK-COUNT // 1X 'EFT   COUNT CONTINUED ' 5X #PP-EFT-COUNT / 1X 'EFT   COUNT LUMP SUM  ' 5X #LS-EFT-COUNT / 4X 'GRAND TOTAL EFT COUNT  ' #GRAND-TOTAL-EFT-COUNT
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(1),"GROSS TOTAL LUMP SUM  ",new ColumnSpacing(4),pnd_Ls_Gross_Total, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(4),"GRAND TOTAL GROSS     ",pnd_Grand_Total_Gross, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
                ColumnSpacing(1),"CHECK TOTAL CONTINUED ",new ColumnSpacing(4),pnd_Work_Rec_3_Pnd_Pp_Check_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                ColumnSpacing(1),"CHECK TOTAL LUMP SUM  ",new ColumnSpacing(4),pnd_Ls_Check_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(4),"GRAND TOTAL CHECK     ",pnd_Grand_Total_Check, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new ColumnSpacing(1),"EFT   TOTAL CONTINUED ",new ColumnSpacing(4),pnd_Work_Rec_3_Pnd_Pp_Eft_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(1),"EFT   TOTAL LUMP SUM  ",new ColumnSpacing(4),pnd_Ls_Eft_Total, new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(4),"GRAND TOTAL EFT       ",pnd_Grand_Total_Eft, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
                ColumnSpacing(1),"CHECK COUNT CONTINUED ",new ColumnSpacing(5),pnd_Work_Rec_3_Pnd_Pp_Check_Count,NEWLINE,new ColumnSpacing(1),"CHECK COUNT LUMP SUM  ",new 
                ColumnSpacing(5),pnd_Ls_Check_Count,NEWLINE,new ColumnSpacing(4),"GRAND TOTAL CHECK COUNT",pnd_Grand_Total_Check_Count,NEWLINE,NEWLINE,new 
                ColumnSpacing(1),"EFT   COUNT CONTINUED ",new ColumnSpacing(5),pnd_Work_Rec_3_Pnd_Pp_Eft_Count,NEWLINE,new ColumnSpacing(1),"EFT   COUNT LUMP SUM  ",new 
                ColumnSpacing(5),pnd_Ls_Eft_Count,NEWLINE,new ColumnSpacing(4),"GRAND TOTAL EFT COUNT  ",pnd_Grand_Total_Eft_Count);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 46T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 113T 'DATE ' *DATU / 47T 'DAILY PAYMENT REPORT ' #REPORT-TYPE
                        TabSetting(113),"DATE ",Global.getDATU(),NEWLINE,new TabSetting(47),"DAILY PAYMENT REPORT ",pnd_Report_Type);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=200 PS=56");
        Global.format(2, "LS=200 PS=56");
    }
}
