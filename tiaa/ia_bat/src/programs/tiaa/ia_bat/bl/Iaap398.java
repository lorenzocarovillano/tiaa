/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:27:02 PM
**        * FROM NATURAL PROGRAM : Iaap398
************************************************************
**        * FILE NAME            : Iaap398.java
**        * CLASS NAME           : Iaap398
**        * INSTANCE NAME        : Iaap398
************************************************************
************************************************************************
************************************************************************
* PROGRAM  : IAAP398
* SYSTEM   : IAD
* TITLE    : SPECIAL FUNCTION TIAA DIVIDEND CHANGE FOR APRIL PMNTS
* CREATED  : JAN 17, 97
* FUNCTION : APPLY DIVIDEND CHANGES TO TIAA-FUND-RCRDS FROM TRAN
* TITLE    : INPUT FILE CREATED BY ANOTHER JOB OPTION 21 CONTRACTS
*            ISSUED DATE AFTER 3/91
*
* HISTORY
*
* DATE     PROGRAMMER    DESCRIPTION
* 01/2005                CHANGED FOR ACCUMULATION OF NUMERIC & ALPHA
*                        RATES IN TABLE
*                        DO SCAN ON 1/05
*
* 09/2003                INCREASED FUND OCCURS FROM 60 TO 90 & ALPHA RTE
*                        DO SCAN ON 9/03
*
* 01/2001  TCD           ADDED OPTION CODES 22,25,27,28,29,30 & 31
*                        TPA, IPRO & P/I CONVERSION TO IAIQ
* 04/2008                INCREASED FUND OCCURS FROM 90 TO 99
*
* 03/2012                INCREASED FUND OCCURS FROM 99 TO 250
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap398 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_I3;
    private DbsField pnd_I4;
    private DbsField pnd_Rtex;
    private DbsField pnd_K;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_1;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_2;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;

    private DbsGroup pnd_Trn_Fund_Rec;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_3;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_4;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_5;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code;

    private DbsGroup pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_6;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf;

    private DbsGroup pnd_Trn_Fund_Rec__R_Field_7;
    private DbsField pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf;

    private DbsGroup pnd_Trailer_Fund_Rec;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt;

    private DbsGroup pnd_Trailer_Fund_Rec__R_Field_8;
    private DbsField pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf;

    private DbsGroup pnd_Total_Prog_Accumulations;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt;

    private DbsGroup pnd_Total_Prog_Accumulations__R_Field_9;
    private DbsField pnd_Total_Prog_Accumulations_Pnd_Total_Prog_Accumulations_Redf;

    private DbsGroup pnd_Tot_Accumulators;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt;
    private DbsField pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt;
    private DbsField pnd_Wrk_Dte_Ccyymmdd;

    private DbsGroup pnd_Wrk_Dte_Ccyymmdd__R_Field_10;
    private DbsField pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd;
    private DbsField pnd_Last_Cntrct_Ppcn_Nbr_Payee;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_Wrk_Tot_Fnd_Rec_Changed;
    private DbsField pnd_Wrk_Chg_Tiaa_Payees;
    private DbsField pnd_Wrk_Chg_Fund_Payees;
    private DbsField pnd_Wrk_Chg_Per_Pay_Amt;
    private DbsField pnd_Wrk_Chg_Per_Div_Amt;
    private DbsField pnd_Wrk_Chg_Final_Pay_Amt;
    private DbsField pnd_Wrk_Chg_Final_Div_Amt;
    private DbsField pnd_Wrk_Per_Pay_Amt;
    private DbsField pnd_Wrk_Per_Div_Amt;
    private DbsField pnd_Wrk_Final_Pay_Amt;
    private DbsField pnd_Wrk_Final_Div_Amt;
    private DbsField pnd_Idx_Rate_Cde;
    private DbsField pnd_Gtot_Rate_Div;
    private DbsField pnd_Gtot_Rate_Final_Div;
    private DbsField pnd_Last_Updte_Dte;
    private DbsField pnd_Next_Updte_Dte;
    private DbsField pnd_Datd;
    private DbsField pnd_Zero;
    private DbsField pnd_Date8;

    private DbsGroup pnd_Date8__R_Field_11;
    private DbsField pnd_Date8_Pnd_Next_Updt_Ccyymm;
    private DbsField pnd_Date8_Pnd_Next_Updt_Dd;

    private DbsGroup pnd_Date8__R_Field_12;
    private DbsField pnd_Date8_Pnd_Next_Date_N;
    private DbsField pnd_Year;

    private DbsGroup pnd_Year__R_Field_13;
    private DbsField pnd_Year_Pnd_Year_N;

    private DbsGroup pnd_Hd_Txt1;
    private DbsField pnd_Hd_Txt1_Hd_Txt1;
    private DbsField pnd_Hd_Txt1_Hd_Txt2;
    private DbsField pnd_Hd_Txt1_Hd_Txt3;

    private DbsGroup pnd_Hd_Txt1__R_Field_14;
    private DbsField pnd_Hd_Txt1_Pnd_Hd_Txt1_R;

    private DbsGroup pnd_Hd2_Txt1;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt1;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt2;
    private DbsField pnd_Hd2_Txt1_Hd2_Txt3;

    private DbsGroup pnd_Hd2_Txt1__R_Field_15;
    private DbsField pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R;
    private DbsField pnd_Inactive_Payees;
    private DbsField pnd_Tot_Rate_Cde;
    private DbsField pnd_Tot_Rate_Div;
    private DbsField pnd_Tot_Rate_Final_Div;
    private DbsField pnd_Work_Rate_Cde;

    private DbsGroup pnd_Work_Rate_Cde__R_Field_16;
    private DbsField pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_17;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Pay;
    private DbsField pnd_Tpa_Codes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.INTEGER, 4);
        pnd_I4 = localVariables.newFieldInRecord("pnd_I4", "#I4", FieldType.INTEGER, 4);
        pnd_Rtex = localVariables.newFieldInRecord("pnd_Rtex", "#RTEX", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_1", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_1.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_1.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_1.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_2 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_2", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        pnd_Trn_Fund_Rec = localVariables.newGroupInRecord("pnd_Trn_Fund_Rec", "#TRN-FUND-REC");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde", "#TRN-REC-CDE", FieldType.STRING, 3);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type", "#TRN-FUND-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate", "#TRN-BLANK-RATE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type", "#TRN-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee", "#TRN-CNTRCT-PPCN-NBR-PAYEE", 
            FieldType.STRING, 12);

        pnd_Trn_Fund_Rec__R_Field_3 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_3", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr = pnd_Trn_Fund_Rec__R_Field_3.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr", "#TRN-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde = pnd_Trn_Fund_Rec__R_Field_3.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde", "#TRN-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde", "#TRN-CMPNY-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Trn_Fund_Rec__R_Field_4 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_4", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde);
        pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde = pnd_Trn_Fund_Rec__R_Field_4.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Cde", "#TRN-CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde = pnd_Trn_Fund_Rec__R_Field_4.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Cde", "#TRN-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt", "#TRN-RATE-CNT", FieldType.NUMERIC, 
            3);

        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data", "#TRN-RATE-DATA");
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde", "#TRN-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic", "#TRN-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt", "#TRN-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt", "#TRN-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt", "#TRN-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Data.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt", "#TRN-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250));
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt", "#TRN-OLD-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt", "#TRN-OLD-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Per_Amt", "#TRN-NEW-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt", "#TRN-NEW-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Trn_Fund_Rec__R_Field_5 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_5", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_New_Tot_Div_Amt);
        pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees = pnd_Trn_Fund_Rec__R_Field_5.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees", "#TRL-REA-PAYEES", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code = pnd_Trn_Fund_Rec.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code", "#TRN-OPTION-CODE", FieldType.NUMERIC, 
            2);

        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info", "#TRN-TRL-INFO");
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tiaa_Payees", "#TRN-OLD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Fund_Payees", "#TRN-OLD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Pay_Amt", "#TRN-OLD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Per_Div_Amt", "#TRN-OLD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Pay_Amt", "#TRN-OLD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Old_Final_Div_Amt", "#TRN-OLD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Tiaa_Payees", "#TRN-NEW-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Fund_Payees", "#TRN-NEW-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Pay_Amt", "#TRN-NEW-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Per_Div_Amt", "#TRN-NEW-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Pay_Amt", "#TRN-NEW-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt = pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info.newFieldInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_New_Final_Div_Amt", "#TRN-NEW-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trn_Fund_Rec__R_Field_6 = pnd_Trn_Fund_Rec.newGroupInGroup("pnd_Trn_Fund_Rec__R_Field_6", "REDEFINE", pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info);
        pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf = pnd_Trn_Fund_Rec__R_Field_6.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf", "#TRN-TRL-INFO-REDF", 
            FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Trn_Fund_Rec__R_Field_7 = localVariables.newGroupInRecord("pnd_Trn_Fund_Rec__R_Field_7", "REDEFINE", pnd_Trn_Fund_Rec);
        pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf = pnd_Trn_Fund_Rec__R_Field_7.newFieldArrayInGroup("pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Rec_Redf", "#TRN-FUND-REC-REDF", 
            FieldType.STRING, 1, new DbsArrayController(1, 1000));

        pnd_Trailer_Fund_Rec = localVariables.newGroupInRecord("pnd_Trailer_Fund_Rec", "#TRAILER-FUND-REC");
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees", "#TRL-OLD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Fund_Payees", "#TRL-OLD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Pay_Amt", "#TRL-OLD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Per_Div_Amt", "#TRL-OLD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Pay_Amt", "#TRL-OLD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Final_Div_Amt", "#TRL-OLD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees", "#TRL-NEW-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees", "#TRL-NEW-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt", "#TRL-NEW-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt", "#TRL-NEW-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt", "#TRL-NEW-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt = pnd_Trailer_Fund_Rec.newFieldInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt", "#TRL-NEW-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trailer_Fund_Rec__R_Field_8 = localVariables.newGroupInRecord("pnd_Trailer_Fund_Rec__R_Field_8", "REDEFINE", pnd_Trailer_Fund_Rec);
        pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf = pnd_Trailer_Fund_Rec__R_Field_8.newFieldArrayInGroup("pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf", 
            "#TRL-FUND-REC-REDF", FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Total_Prog_Accumulations = localVariables.newGroupInRecord("pnd_Total_Prog_Accumulations", "#TOTAL-PROG-ACCUMULATIONS");
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees", 
            "#TOT-OLD-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees", 
            "#TOT-OLD-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt", 
            "#TOT-OLD-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt", 
            "#TOT-OLD-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt", 
            "#TOT-OLD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt", 
            "#TOT-OLD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees", 
            "#TOT-NEW-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees", 
            "#TOT-NEW-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt", 
            "#TOT-NEW-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt", 
            "#TOT-NEW-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt", 
            "#TOT-NEW-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt = pnd_Total_Prog_Accumulations.newFieldInGroup("pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt", 
            "#TOT-NEW-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Total_Prog_Accumulations__R_Field_9 = localVariables.newGroupInRecord("pnd_Total_Prog_Accumulations__R_Field_9", "REDEFINE", pnd_Total_Prog_Accumulations);
        pnd_Total_Prog_Accumulations_Pnd_Total_Prog_Accumulations_Redf = pnd_Total_Prog_Accumulations__R_Field_9.newFieldArrayInGroup("pnd_Total_Prog_Accumulations_Pnd_Total_Prog_Accumulations_Redf", 
            "#TOTAL-PROG-ACCUMULATIONS-REDF", FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Tot_Accumulators = localVariables.newGroupInRecord("pnd_Tot_Accumulators", "#TOT-ACCUMULATORS");
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Payees", "#OTOT-GRD-STD-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees", 
            "#OTOT-GRD-STD-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt", 
            "#OTOT-GRD-STD-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt", 
            "#OTOT-GRD-STD-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt", 
            "#OTOT-GRD-STD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt", 
            "#OTOT-GRD-STD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Tiaa_Payees", 
            "#NTOT-GRD-STD-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees", 
            "#NTOT-GRD-STD-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt", 
            "#NTOT-GRD-STD-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt", 
            "#NTOT-GRD-STD-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt", 
            "#NTOT-GRD-STD-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt", 
            "#NTOT-GRD-STD-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Payees", "#OTOT-STD-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees", "#OTOT-STD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt", "#OTOT-STD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt", "#OTOT-STD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt", "#OTOT-STD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt", "#OTOT-STD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tiaa_Payees", "#NTOT-STD-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees", "#NTOT-STD-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt", "#NTOT-STD-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt", "#NTOT-STD-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt", "#NTOT-STD-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt", "#NTOT-STD-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Payees", "#OTOT-GRD-OTH-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees", 
            "#OTOT-GRD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt", 
            "#OTOT-GRD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt", 
            "#OTOT-GRD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt", 
            "#OTOT-GRD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt", 
            "#OTOT-GRD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Tiaa_Payees", 
            "#NTOT-GRD-OTH-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees", 
            "#NTOT-GRD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt", 
            "#NTOT-GRD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt", 
            "#NTOT-GRD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt", 
            "#NTOT-GRD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt", 
            "#NTOT-GRD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Payees", "#OTOT-STD-OTH-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees", 
            "#OTOT-STD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt", 
            "#OTOT-STD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt", 
            "#OTOT-STD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt", 
            "#OTOT-STD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt", 
            "#OTOT-STD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Tiaa_Payees", 
            "#NTOT-STD-OTH-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees", 
            "#NTOT-STD-OTH-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt", 
            "#NTOT-STD-OTH-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt", 
            "#NTOT-STD-OTH-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt", 
            "#NTOT-STD-OTH-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt", 
            "#NTOT-STD-OTH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Payees", "#OTOT-STD-TPA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees", 
            "#OTOT-STD-TPA-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt", 
            "#OTOT-STD-TPA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt", 
            "#OTOT-STD-TPA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt", 
            "#OTOT-STD-TPA-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt", 
            "#OTOT-STD-TPA-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Tiaa_Payees", 
            "#NTOT-STD-TPA-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees", 
            "#NTOT-STD-TPA-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt", 
            "#NTOT-STD-TPA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt", 
            "#NTOT-STD-TPA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt", 
            "#NTOT-STD-TPA-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt", 
            "#NTOT-STD-TPA-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Payees", "#OTOT-STD-IPRO-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees", 
            "#OTOT-STD-IPRO-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt", 
            "#OTOT-STD-IPRO-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt", 
            "#OTOT-STD-IPRO-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt", 
            "#OTOT-STD-IPRO-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt", 
            "#OTOT-STD-IPRO-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Tiaa_Payees", 
            "#NTOT-STD-IPRO-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees", 
            "#NTOT-STD-IPRO-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt", 
            "#NTOT-STD-IPRO-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt", 
            "#NTOT-STD-IPRO-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt", 
            "#NTOT-STD-IPRO-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt", 
            "#NTOT-STD-IPRO-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Payees", "#OTOT-STD-PI-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees", "#OTOT-STD-PI-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt", "#OTOT-STD-PI-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt", "#OTOT-STD-PI-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt", 
            "#OTOT-STD-PI-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt", 
            "#OTOT-STD-PI-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Tiaa_Payees", "#NTOT-STD-PI-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees", "#NTOT-STD-PI-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt", "#NTOT-STD-PI-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt", "#NTOT-STD-PI-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt", 
            "#NTOT-STD-PI-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt = pnd_Tot_Accumulators.newFieldInGroup("pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt", 
            "#NTOT-STD-PI-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Dte_Ccyymmdd = localVariables.newFieldInRecord("pnd_Wrk_Dte_Ccyymmdd", "#WRK-DTE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Wrk_Dte_Ccyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Wrk_Dte_Ccyymmdd__R_Field_10", "REDEFINE", pnd_Wrk_Dte_Ccyymmdd);
        pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd = pnd_Wrk_Dte_Ccyymmdd__R_Field_10.newFieldInGroup("pnd_Wrk_Dte_Ccyymmdd_Pnd_Wrk_Dte_N_Ccyymmdd", 
            "#WRK-DTE-N-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Last_Cntrct_Ppcn_Nbr_Payee = localVariables.newFieldInRecord("pnd_Last_Cntrct_Ppcn_Nbr_Payee", "#LAST-CNTRCT-PPCN-NBR-PAYEE", FieldType.STRING, 
            12);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_Wrk_Tot_Fnd_Rec_Changed = localVariables.newFieldInRecord("pnd_Wrk_Tot_Fnd_Rec_Changed", "#WRK-TOT-FND-REC-CHANGED", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Wrk_Chg_Tiaa_Payees = localVariables.newFieldInRecord("pnd_Wrk_Chg_Tiaa_Payees", "#WRK-CHG-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Wrk_Chg_Fund_Payees = localVariables.newFieldInRecord("pnd_Wrk_Chg_Fund_Payees", "#WRK-CHG-FUND-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Wrk_Chg_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Per_Pay_Amt", "#WRK-CHG-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Chg_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Per_Div_Amt", "#WRK-CHG-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Chg_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Final_Pay_Amt", "#WRK-CHG-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_Chg_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Chg_Final_Div_Amt", "#WRK-CHG-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Wrk_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Per_Pay_Amt", "#WRK-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Wrk_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Per_Div_Amt", "#WRK-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Wrk_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Wrk_Final_Pay_Amt", "#WRK-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Wrk_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Wrk_Final_Div_Amt", "#WRK-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Idx_Rate_Cde = localVariables.newFieldInRecord("pnd_Idx_Rate_Cde", "#IDX-RATE-CDE", FieldType.NUMERIC, 2);
        pnd_Gtot_Rate_Div = localVariables.newFieldInRecord("pnd_Gtot_Rate_Div", "#GTOT-RATE-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Gtot_Rate_Final_Div = localVariables.newFieldInRecord("pnd_Gtot_Rate_Final_Div", "#GTOT-RATE-FINAL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Last_Updte_Dte = localVariables.newFieldInRecord("pnd_Last_Updte_Dte", "#LAST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Next_Updte_Dte = localVariables.newFieldInRecord("pnd_Next_Updte_Dte", "#NEXT-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Zero = localVariables.newFieldInRecord("pnd_Zero", "#ZERO", FieldType.DATE);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);

        pnd_Date8__R_Field_11 = localVariables.newGroupInRecord("pnd_Date8__R_Field_11", "REDEFINE", pnd_Date8);
        pnd_Date8_Pnd_Next_Updt_Ccyymm = pnd_Date8__R_Field_11.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Ccyymm", "#NEXT-UPDT-CCYYMM", FieldType.NUMERIC, 
            6);
        pnd_Date8_Pnd_Next_Updt_Dd = pnd_Date8__R_Field_11.newFieldInGroup("pnd_Date8_Pnd_Next_Updt_Dd", "#NEXT-UPDT-DD", FieldType.STRING, 2);

        pnd_Date8__R_Field_12 = localVariables.newGroupInRecord("pnd_Date8__R_Field_12", "REDEFINE", pnd_Date8);
        pnd_Date8_Pnd_Next_Date_N = pnd_Date8__R_Field_12.newFieldInGroup("pnd_Date8_Pnd_Next_Date_N", "#NEXT-DATE-N", FieldType.NUMERIC, 8);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);

        pnd_Year__R_Field_13 = localVariables.newGroupInRecord("pnd_Year__R_Field_13", "REDEFINE", pnd_Year);
        pnd_Year_Pnd_Year_N = pnd_Year__R_Field_13.newFieldInGroup("pnd_Year_Pnd_Year_N", "#YEAR-N", FieldType.NUMERIC, 4);

        pnd_Hd_Txt1 = localVariables.newGroupInRecord("pnd_Hd_Txt1", "#HD-TXT1");
        pnd_Hd_Txt1_Hd_Txt1 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt1", "HD-TXT1", FieldType.STRING, 31);
        pnd_Hd_Txt1_Hd_Txt2 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt2", "HD-TXT2", FieldType.STRING, 57);
        pnd_Hd_Txt1_Hd_Txt3 = pnd_Hd_Txt1.newFieldInGroup("pnd_Hd_Txt1_Hd_Txt3", "HD-TXT3", FieldType.STRING, 43);

        pnd_Hd_Txt1__R_Field_14 = localVariables.newGroupInRecord("pnd_Hd_Txt1__R_Field_14", "REDEFINE", pnd_Hd_Txt1);
        pnd_Hd_Txt1_Pnd_Hd_Txt1_R = pnd_Hd_Txt1__R_Field_14.newFieldInGroup("pnd_Hd_Txt1_Pnd_Hd_Txt1_R", "#HD-TXT1-R", FieldType.STRING, 131);

        pnd_Hd2_Txt1 = localVariables.newGroupInRecord("pnd_Hd2_Txt1", "#HD2-TXT1");
        pnd_Hd2_Txt1_Hd2_Txt1 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt1", "HD2-TXT1", FieldType.STRING, 31);
        pnd_Hd2_Txt1_Hd2_Txt2 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt2", "HD2-TXT2", FieldType.STRING, 57);
        pnd_Hd2_Txt1_Hd2_Txt3 = pnd_Hd2_Txt1.newFieldInGroup("pnd_Hd2_Txt1_Hd2_Txt3", "HD2-TXT3", FieldType.STRING, 43);

        pnd_Hd2_Txt1__R_Field_15 = localVariables.newGroupInRecord("pnd_Hd2_Txt1__R_Field_15", "REDEFINE", pnd_Hd2_Txt1);
        pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R = pnd_Hd2_Txt1__R_Field_15.newFieldInGroup("pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R", "#HD2-TXT1-R", FieldType.STRING, 131);
        pnd_Inactive_Payees = localVariables.newFieldInRecord("pnd_Inactive_Payees", "#INACTIVE-PAYEES", FieldType.NUMERIC, 7);
        pnd_Tot_Rate_Cde = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Cde", "#TOT-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 201));
        pnd_Tot_Rate_Div = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Div", "#TOT-RATE-DIV", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            201));
        pnd_Tot_Rate_Final_Div = localVariables.newFieldArrayInRecord("pnd_Tot_Rate_Final_Div", "#TOT-RATE-FINAL-DIV", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            201));
        pnd_Work_Rate_Cde = localVariables.newFieldInRecord("pnd_Work_Rate_Cde", "#WORK-RATE-CDE", FieldType.STRING, 2);

        pnd_Work_Rate_Cde__R_Field_16 = localVariables.newGroupInRecord("pnd_Work_Rate_Cde__R_Field_16", "REDEFINE", pnd_Work_Rate_Cde);
        pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N = pnd_Work_Rate_Cde__R_Field_16.newFieldInGroup("pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N", "#WORK-RATE-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_17", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Nbr = pnd_Cntrct_Payee_Key__R_Field_17.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Nbr", "#IAA-CPR-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Pay = pnd_Cntrct_Payee_Key__R_Field_17.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Pay", "#IAA-CPR-PAY", 
            FieldType.NUMERIC, 2);
        pnd_Tpa_Codes = localVariables.newFieldArrayInRecord("pnd_Tpa_Codes", "#TPA-CODES", FieldType.NUMERIC, 2, new DbsArrayController(1, 10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Tiaa_Fund_Rcrd.reset();

        localVariables.reset();
        pnd_Hd_Txt1_Hd_Txt2.setInitialValue("         INPUT          NET CHANGE                OUTPUT ");
        pnd_Hd_Txt1_Hd_Txt3.setInitialValue("      IN TRAILER REC    CALC OUT DIFFERENT");
        pnd_Hd2_Txt1_Hd2_Txt2.setInitialValue("    ---------------- --------------------    ------------");
        pnd_Hd2_Txt1_Hd2_Txt3.setInitialValue("---  -----------------  ------------------");
        pnd_Tpa_Codes.getValue(1).setInitialValue(28);
        pnd_Tpa_Codes.getValue(2).setInitialValue(29);
        pnd_Tpa_Codes.getValue(3).setInitialValue(30);
        pnd_Tpa_Codes.getValue(4).setInitialValue(31);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap398() throws Exception
    {
        super("Iaap398");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Updte_Dte, pnd_Next_Updte_Dte);                                                               //Natural: CALLNAT 'IAAN0020' #LAST-UPDTE-DTE #NEXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        //*  FOR TESTING ONLY
        //*  #LAST-UPDTE-DTE := '03/01/2001'
        //*  #NEXT-UPDTE-DTE := '04/01/2001'
        if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"MM'/'DD'/'YYYY")))                                                                                          //Natural: IF #LAST-UPDTE-DTE = MASK ( MM'/'DD'/'YYYY )
        {
            pnd_Datd.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Next_Updte_Dte);                                                                                 //Natural: MOVE EDITED #NEXT-UPDTE-DTE TO #DATD ( EM = MM/DD/YYYY )
            pnd_Date8.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE8
            pnd_Year.setValueEdited(pnd_Datd,new ReportEditMask("YYYY"));                                                                                                 //Natural: MOVE EDITED #DATD ( EM = YYYY ) TO #YEAR
            if (condition(DbsUtil.maskMatches(pnd_Last_Updte_Dte,"'01'")))                                                                                                //Natural: IF #LAST-UPDTE-DTE = MASK ( '01' )
            {
                pnd_Year_Pnd_Year_N.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #YEAR-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 4
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
            {
                getReports().write(0, "****************************",NEWLINE,"ERROR   ERROR   ERROR  ERROR",NEWLINE,"NO CONTROL RECORD ON FILE",NEWLINE,                  //Natural: WRITE '****************************' / 'ERROR   ERROR   ERROR  ERROR' / 'NO CONTROL RECORD ON FILE' / 'NO CONTROL RECORD DATES RETURNED ' / ' FROM CALL TO IAAN0020' / 'PROCESSING WILL NOT CONTINUE'
                    "NO CONTROL RECORD DATES RETURNED ",NEWLINE," FROM CALL TO IAAN0020",NEWLINE,"PROCESSING WILL NOT CONTINUE");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"****************************",NEWLINE,"ERROR   ERROR   ERROR  ERROR",NEWLINE,"NO CONTROL RECORD ON FILE",     //Natural: WRITE ( 1 ) '****************************' / 'ERROR   ERROR   ERROR  ERROR' / 'NO CONTROL RECORD ON FILE' / 'NO CONTROL RECORD DATES RETURNED ' / ' FROM CALL TO IAAN0020' / 'PROCESSING WILL NOT CONTINUE'
                    NEWLINE,"NO CONTROL RECORD DATES RETURNED ",NEWLINE," FROM CALL TO IAAN0020",NEWLINE,"PROCESSING WILL NOT CONTINUE");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
        R1:                                                                                                                                                               //Natural: READ WORK 1 #TRN-FUND-REC
        while (condition(getWorkFiles().read(1, pnd_Trn_Fund_Rec)))
        {
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Rec_Cde.equals("TRL")))                                                                                                //Natural: IF #TRN-REC-CDE = 'TRL'
            {
                pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf.getValue("*").setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Trl_Info_Redf.getValue("*"));                                  //Natural: ASSIGN #TRL-FUND-REC-REDF ( * ) := #TRN-TRL-INFO-REDF ( * )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee.notEquals(pnd_Last_Cntrct_Ppcn_Nbr_Payee)))                                                      //Natural: IF #TRN-CNTRCT-PPCN-NBR-PAYEE NE #LAST-CNTRCT-PPCN-NBR-PAYEE
            {
                pnd_Last_Cntrct_Ppcn_Nbr_Payee.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr_Payee);                                                                  //Natural: ASSIGN #LAST-CNTRCT-PPCN-NBR-PAYEE := #TRN-CNTRCT-PPCN-NBR-PAYEE
                pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees.nadd(1);                                                                                             //Natural: ADD 1 TO #TOT-OLD-TIAA-PAYEES
                pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees.nadd(1);                                                                                             //Natural: ADD 1 TO #TOT-NEW-TIAA-PAYEES
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Ppcn_Nbr);                                                                                //Natural: ASSIGN #FUND-PPCN := #TRN-CNTRCT-PPCN-NBR
            pnd_Fund_Key_Pnd_Fund_Paye.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Payee_Cde);                                                                               //Natural: ASSIGN #FUND-PAYE := #TRN-CNTRCT-PAYEE-CDE
            pnd_Fund_Key_Pnd_Fund_Cde.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Cmpny_Fund_Cde);                                                                                  //Natural: ASSIGN #FUND-CDE := #TRN-CMPNY-FUND-CDE
                                                                                                                                                                          //Natural: PERFORM READ-PROCESS-FUND-RECORD
            sub_Read_Process_Fund_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        //*  PERFORM READ-IAA-CNTRL-RCRD-1
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-TOTALS
        sub_Print_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RATE-TOTALS
        sub_Print_Rate_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  ---------------------------------------------------------------
        //*   PROCESS TIAA FUND RECORDS
        //*   APPLY RATE CHANGES FROM TRAN IN TO FUND RECORD
        //*  ---------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PROCESS-FUND-RECORD
        //*  END OF CHANGE 9/03
        //*  FOR TESTING ONLY DO NOT UPDATE
        //*  --------------------------------------------------
        //*   ACCUMULATE CURR PAYMENT AMOUNTS
        //*  --------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CURR-PAYMENT-AMTS
        //*  --------------------------------------------------
        //*   ACCUMULATE NEW  PAYMENT AMOUNTS
        //*   INTO TRAN RECORD
        //*  --------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-NEW-PAYMENT-AMTS
        //*  --------------------------------------------
        //*   PRINT SUMMARY REPORT TIAA FUNDS
        //*  --------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-TOTALS
        //*  WRITE(1) 'TIAA TRADTIONAL FUND RECORDS    '
        //*   41T #TRL-OLD-FUND-PAYEES  (EM=ZZZ,ZZZ,ZZ9-)
        //*   60T #WRK-CHG-FUND-PAYEES  (EM=ZZZ,ZZZ,ZZ9-)
        //*   81T #TRL-NEW-FUND-PAYEES  (EM=ZZZ,ZZZ,ZZ9-)
        //*  102T CNTRL-TIAA-PAYEES    (EM=ZZZ,ZZZ,ZZ9-)
        //*  WRITE(1)  'TIAA TRADTIONAL PER PAYMENT     '
        //*   35T #TRL-OLD-PER-PAY-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*   54T #WRK-CHG-PER-PAY-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99+)
        //*   75T #TRL-NEW-PER-PAY-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  96T CNTRL-PER-PAY-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  WRITE(1)  'TIAA TRADTIONAL PER DIDVIDEND   '
        //*   35T #TRL-OLD-PER-DIV-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*   54T #WRK-CHG-PER-DIV-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99+)
        //*   75T #TRL-NEW-PER-DIV-AMT  (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  96T CNTRL-PER-DIV-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  WRITE(1)  'TIAA TRADTIONAL FINAL PAYMENT   '
        //*   35T#TRL-OLD-FINAL-PAY-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*   54T#WRK-CHG-FINAL-PAY-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99+)
        //*   75T#TRL-NEW-FINAL-PAY-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  96T CNTRL-FINAL-PAY-AMT   (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  WRITE(1)  'TIAA TRADTIONAL FINAL DIDVIDEND '
        //*   35T#TRL-OLD-FINAL-DIV-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*   54T#WRK-CHG-FINAL-DIV-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99+)
        //*   75T#TRL-NEW-FINAL-DIV-AMT (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  96T CNTRL-FINAL-DIV-AMT   (EM=ZZ,ZZZ,ZZZ,ZZZ.99-)
        //*  102T CNTRL-ACTVE-TIAA-PYS  (EM=ZZZ,ZZZ,ZZ9-)
        //*  WRITE(1) / 'TOTAL FUND RECORDS CHANGED......'
        //*   60T #WRK-TOT-FND-REC-CHANGED (EM=ZZZ,ZZZ,ZZ9-)
        //*   PRINT GRADED OTHER TOTALS FOR OPTION 21 AFTER 199103,
        //*         (ORIGIN 3 AFTER 199312) & (PA CONTRACTS ORIGIN 17 & 18)
        //*   PRINT STANDARD OTHER TOTALS FOR OPTION 21 AFTER 199103,
        //*         (ORIGIN 3 AFTER 199312) & (PA CONTRACTS ORIGIN 17 & 18)
        //*   PRINT STANDARD TPA TOTALS FOR OPTION 28,29,30 & 31
        //*         ORIGIN CODE 02,08 FOR OPTION 28 & 29
        //*         ORIGIN CODE 01,08 FOR OPTION 30 & 31
        //*   PRINT STANDARD IPRO TOTALS FOR OPTION 25 &27
        //*         ORIGIN CODE 02,05 FOR OPTION 25
        //*         ORIGIN CODE 01,05 FOR OPTION 27
        //*   PRINT STANDARD P/I TOTALS FOR OPTION 22
        //*         ORIGIN CODE 01,02,19,(17->PA-SELECT)
        //*  ---------------------------------------------------
        //*  PRINT SUMMARY TOTALS OF NEW DIVIDENDS BY RATE
        //*  ---------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-RATE-TOTALS
        //*      15T #I  (EM=99)
        //*  CHANGED ABOVE TO FOLLOWING 9/03
        //*  ------------------------------------------------------
        //*  ERROR IN TRAILER INPUT TOTALS & NEW TOTALS CALCULATED
        //*  ------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //*    102T CNTRL-TIAA-PAYEES    (EM=ZZZ,ZZZ,ZZ9-)
    }
    private void sub_Read_Process_Fund_Record() throws Exception                                                                                                          //Natural: READ-PROCESS-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
        (
        "R2",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") },
        1
        );
        R2:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("R2")))
        {
            if (condition(pnd_Fund_Key_Pnd_Fund_Ppcn.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr) || pnd_Fund_Key_Pnd_Fund_Paye.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde)  //Natural: IF #FUND-PPCN NE TIAA-CNTRCT-PPCN-NBR OR #FUND-PAYE NE TIAA-CNTRCT-PAYEE-CDE OR #FUND-CDE NE TIAA-CMPNY-FUND-CDE
                || pnd_Fund_Key_Pnd_Fund_Cde.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  0 + TIAA-PER-PAY-AMT(*)
            //*  0 + TIAA-PER-DIV-AMT(*)
                                                                                                                                                                          //Natural: PERFORM ACCUM-CURR-PAYMENT-AMTS
            sub_Accum_Curr_Payment_Amts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Per_Amt);                                                                       //Natural: ASSIGN TIAA-OLD-PER-AMT := #TRN-OLD-TOT-PER-AMT
            iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt.setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Old_Tot_Div_Amt);                                                                       //Natural: ASSIGN TIAA-OLD-DIV-AMT := #TRN-OLD-TOT-DIV-AMT
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Blank_Rate.equals("Y")))                                                                                               //Natural: IF #TRN-BLANK-RATE = 'Y'
            {
                iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue("*").setValue("  ");                                                                                            //Natural: ASSIGN TIAA-RATE-CDE ( * ) := '  '
                iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*").setValue(0);                                                                                            //Natural: ASSIGN TIAA-PER-PAY-AMT ( * ) := 0
                iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*").setValue(0);                                                                                            //Natural: ASSIGN TIAA-PER-DIV-AMT ( * ) := 0
                iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*").setValue(0);                                                                                     //Natural: ASSIGN TIAA-RATE-FINAL-PAY-AMT ( * ) := 0
                iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*").setValue(0);                                                                                     //Natural: ASSIGN TIAA-RATE-FINAL-DIV-AMT ( * ) := 0
            }                                                                                                                                                             //Natural: END-IF
            //*  C*TIAA-RATE-DATA-GRP
            pnd_Wrk_Per_Pay_Amt.reset();                                                                                                                                  //Natural: RESET #WRK-PER-PAY-AMT #WRK-PER-DIV-AMT #WRK-FINAL-PAY-AMT #WRK-FINAL-DIV-AMT
            pnd_Wrk_Per_Div_Amt.reset();
            pnd_Wrk_Final_Pay_Amt.reset();
            pnd_Wrk_Final_Div_Amt.reset();
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO #TRN-RATE-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cnt)); pnd_I.nadd(1))
            {
                //*     IF #TRN-RATE-CDE (#I) NE  '0'
                //*  CHANGED ABOVE COMMENTED TO FOLLOWING 9/03
                //*  3/12
                if (condition(! (pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).equals("00") || pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).equals("  "))))      //Natural: IF NOT ( #TRN-RATE-CDE ( #I ) = '00' OR = '  ' )
                {
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte.getValue(pnd_I).setValue(pnd_Zero);                                                                                  //Natural: ASSIGN TIAA-RATE-DTE ( #I ) := #ZERO
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I));                                         //Natural: ASSIGN TIAA-RATE-CDE ( #I ) := #TRN-RATE-CDE ( #I )
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Gic.getValue(pnd_I));                                         //Natural: ASSIGN TIAA-RATE-GIC ( #I ) := #TRN-RATE-GIC ( #I )
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Pay_Amt.getValue(pnd_I));                                   //Natural: ASSIGN TIAA-PER-PAY-AMT ( #I ) := #TRN-PER-PAY-AMT ( #I )
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Per_Div_Amt.getValue(pnd_I));                                   //Natural: ASSIGN TIAA-PER-DIV-AMT ( #I ) := #TRN-PER-DIV-AMT ( #I )
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Final_Pay_Amt.getValue(pnd_I));                          //Natural: ASSIGN TIAA-RATE-FINAL-PAY-AMT ( #I ) := #TRN-FINAL-PAY-AMT ( #I )
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Final_Div_Amt.getValue(pnd_I));                          //Natural: ASSIGN TIAA-RATE-FINAL-DIV-AMT ( #I ) := #TRN-FINAL-DIV-AMT ( #I )
                    pnd_Wrk_Per_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I));                                                                        //Natural: ADD TIAA-PER-PAY-AMT ( #I ) TO #WRK-PER-PAY-AMT
                    pnd_Wrk_Per_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I));                                                                        //Natural: ADD TIAA-PER-DIV-AMT ( #I ) TO #WRK-PER-DIV-AMT
                    pnd_Wrk_Final_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                                               //Natural: ADD TIAA-RATE-FINAL-PAY-AMT ( #I ) TO #WRK-FINAL-PAY-AMT
                    pnd_Wrk_Final_Div_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I));                                                               //Natural: ADD TIAA-RATE-FINAL-DIV-AMT ( #I ) TO #WRK-FINAL-DIV-AMT
                    //*  ACCUM DIV AMTS BY RATE FOR REPORT
                    //*      #RTEX := #TRN-RATE-CDE (#I)
                    //*  CHANGED ABOVE TO FOLLOWING 9/03
                    if (condition(DbsUtil.is(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).getText(),"N2")))                                                          //Natural: IF #TRN-RATE-CDE ( #I ) IS ( N2 )
                    {
                        pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N.compute(new ComputeParameters(false, pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N), pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I).val()); //Natural: ASSIGN #WORK-RATE-CDE-N := VAL ( #TRN-RATE-CDE ( #I ) )
                        pnd_Rtex.setValue(pnd_Work_Rate_Cde_Pnd_Work_Rate_Cde_N);                                                                                         //Natural: ASSIGN #RTEX := #WORK-RATE-CDE-N
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Tot_Rate_Cde.getValue("*"),true), new ExamineSearch(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I)),     //Natural: EXAMINE FULL #TOT-RATE-CDE ( * ) FOR #TRN-RATE-CDE ( #I ) GIVING INDEX #I3
                            new ExamineGivingIndex(pnd_I3));
                        //*  ADDED   01/05 /* 3/12
                        if (condition(pnd_I3.equals(getZero())))                                                                                                          //Natural: IF #I3 = 0
                        {
                            FOR03:                                                                                                                                        //Natural: FOR #I4 = 100 TO 250
                            for (pnd_I4.setValue(100); condition(pnd_I4.lessOrEqual(250)); pnd_I4.nadd(1))
                            {
                                if (condition(pnd_Tot_Rate_Cde.getValue(pnd_I4).equals("  ")))                                                                            //Natural: IF #TOT-RATE-CDE ( #I4 ) = '  '
                                {
                                    pnd_Rtex.setValue(pnd_I4);                                                                                                            //Natural: ASSIGN #RTEX := #I4
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_I4.setValue(pnd_I3);                                                                                                                      //Natural: ASSIGN #I4 := #I3
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Rtex.setValue(pnd_I4);                                                                                                                        //Natural: ASSIGN #RTEX := #I4
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Tot_Rate_Cde.getValue(pnd_Rtex).setValue(pnd_Trn_Fund_Rec_Pnd_Trn_Rate_Cde.getValue(pnd_I));                                                      //Natural: ASSIGN #TOT-RATE-CDE ( #RTEX ) := #TRN-RATE-CDE ( #I )
                    pnd_Tot_Rate_Div.getValue(pnd_Rtex).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I));                                                        //Natural: ASSIGN #TOT-RATE-DIV ( #RTEX ) := #TOT-RATE-DIV ( #RTEX ) + TIAA-PER-DIV-AMT ( #I )
                    pnd_Tot_Rate_Final_Div.getValue(pnd_Rtex).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue(pnd_I));                                           //Natural: ASSIGN #TOT-RATE-FINAL-DIV ( #RTEX ) := #TOT-RATE-FINAL-DIV ( #RTEX ) + TIAA-RATE-FINAL-DIV-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
                //*  ACCUM NEW TOT AMTS
                //*  IN TIAA-FUND REC
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(false, iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"))); //Natural: ASSIGN TIAA-TOT-PER-AMT := 0 + TIAA-PER-PAY-AMT ( * )
            iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt.compute(new ComputeParameters(false, iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"))); //Natural: ASSIGN TIAA-TOT-DIV-AMT := 0 + TIAA-PER-DIV-AMT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-NEW-PAYMENT-AMTS
            sub_Accum_New_Payment_Amts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Nbr.setValue(pnd_Fund_Key_Pnd_Fund_Ppcn);                                                                                    //Natural: ASSIGN #IAA-CPR-NBR := #FUND-PPCN
            pnd_Cntrct_Payee_Key_Pnd_Iaa_Cpr_Pay.setValue(pnd_Fund_Key_Pnd_Fund_Paye);                                                                                    //Natural: ASSIGN #IAA-CPR-PAY := #FUND-PAYE
            vw_iaa_Tiaa_Fund_Rcrd.updateDBRow("R2");                                                                                                                      //Natural: UPDATE ( R2. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Curr_Payment_Amts() throws Exception                                                                                                           //Natural: ACCUM-CURR-PAYMENT-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wrk_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Per_Pay_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*")));        //Natural: ASSIGN #WRK-PER-PAY-AMT := 0 + TIAA-PER-PAY-AMT ( * )
        pnd_Wrk_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Per_Div_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*")));        //Natural: ASSIGN #WRK-PER-DIV-AMT := 0 + TIAA-PER-DIV-AMT ( * )
        pnd_Wrk_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Final_Pay_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"))); //Natural: ASSIGN #WRK-FINAL-PAY-AMT := 0 + TIAA-RATE-FINAL-PAY-AMT ( * )
        pnd_Wrk_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Final_Div_Amt), DbsField.add(getZero(),iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt.getValue("*"))); //Natural: ASSIGN #WRK-FINAL-DIV-AMT := 0 + TIAA-RATE-FINAL-DIV-AMT ( * )
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees.nadd(1);                                                                                                     //Natural: ADD 1 TO #TOT-OLD-FUND-PAYEES
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                   //Natural: ASSIGN #TOT-OLD-PER-PAY-AMT := #TOT-OLD-PER-PAY-AMT + #WRK-PER-PAY-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                   //Natural: ASSIGN #TOT-OLD-PER-DIV-AMT := #TOT-OLD-PER-DIV-AMT + #WRK-PER-DIV-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                               //Natural: ASSIGN #TOT-OLD-FINAL-PAY-AMT := #TOT-OLD-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                               //Natural: ASSIGN #TOT-OLD-FINAL-DIV-AMT := #TOT-OLD-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        //*  ADDED 1/01
        if (condition(! (pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(25) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(27) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(28)  //Natural: IF NOT ( #TRN-OPTION-CODE = 25 OR = 27 OR = 28 OR = 30 )
            || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(30))))
        {
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("P") || pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("A") || pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("I"))) //Natural: IF ( #TRN-CNTRCT-TYPE = 'P' OR = 'A' OR = 'I' )
            {
                if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.equals("G")))                                                                                            //Natural: IF #TRN-FUND-TYPE = 'G'
                {
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #OTOT-GRD-OTH-FUND-PAYEES := #OTOT-GRD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                          //Natural: ASSIGN #OTOT-GRD-OTH-PER-PAY-AMT := #OTOT-GRD-OTH-PER-PAY-AMT + #WRK-PER-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                          //Natural: ASSIGN #OTOT-GRD-OTH-PER-DIV-AMT := #OTOT-GRD-OTH-PER-DIV-AMT + #WRK-PER-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                      //Natural: ASSIGN #OTOT-GRD-OTH-FINAL-PAY-AMT := #OTOT-GRD-OTH-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                      //Natural: ASSIGN #OTOT-GRD-OTH-FINAL-DIV-AMT := #OTOT-GRD-OTH-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #OTOT-STD-OTH-FUND-PAYEES := #OTOT-STD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                          //Natural: ASSIGN #OTOT-STD-OTH-PER-PAY-AMT := #OTOT-STD-OTH-PER-PAY-AMT + #WRK-PER-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                          //Natural: ASSIGN #OTOT-STD-OTH-PER-DIV-AMT := #OTOT-STD-OTH-PER-DIV-AMT + #WRK-PER-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                      //Natural: ASSIGN #OTOT-STD-OTH-FINAL-PAY-AMT := #OTOT-STD-OTH-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                      //Natural: ASSIGN #OTOT-STD-OTH-FINAL-DIV-AMT := #OTOT-STD-OTH-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING FOR TPA & IPRO 1/01
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(pnd_Tpa_Codes.getValue("*"))))                                                                          //Natural: IF #TRN-OPTION-CODE = #TPA-CODES ( * )
        {
            pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #OTOT-STD-TPA-FUND-PAYEES := #OTOT-STD-TPA-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                  //Natural: ASSIGN #OTOT-STD-TPA-PER-PAY-AMT := #OTOT-STD-TPA-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                  //Natural: ASSIGN #OTOT-STD-TPA-PER-DIV-AMT := #OTOT-STD-TPA-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                              //Natural: ASSIGN #OTOT-STD-TPA-FINAL-PAY-AMT := #OTOT-STD-TPA-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                              //Natural: ASSIGN #OTOT-STD-TPA-FINAL-DIV-AMT := #OTOT-STD-TPA-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(25) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(27)))                                                //Natural: IF #TRN-OPTION-CODE = 25 OR = 27
        {
            pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees.nadd(1);                                                                                                   //Natural: ASSIGN #OTOT-STD-IPRO-FUND-PAYEES := #OTOT-STD-IPRO-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                 //Natural: ASSIGN #OTOT-STD-IPRO-PER-PAY-AMT := #OTOT-STD-IPRO-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                 //Natural: ASSIGN #OTOT-STD-IPRO-PER-DIV-AMT := #OTOT-STD-IPRO-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                             //Natural: ASSIGN #OTOT-STD-IPRO-FINAL-PAY-AMT := #OTOT-STD-IPRO-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                             //Natural: ASSIGN #OTOT-STD-IPRO-FINAL-DIV-AMT := #OTOT-STD-IPRO-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(22)))                                                                                                   //Natural: IF #TRN-OPTION-CODE = 22
        {
            pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees.nadd(1);                                                                                                     //Natural: ASSIGN #OTOT-STD-PI-FUND-PAYEES := #OTOT-STD-PI-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                   //Natural: ASSIGN #OTOT-STD-PI-PER-PAY-AMT := #OTOT-STD-PI-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                   //Natural: ASSIGN #OTOT-STD-PI-PER-DIV-AMT := #OTOT-STD-PI-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                               //Natural: ASSIGN #OTOT-STD-PI-FINAL-PAY-AMT := #OTOT-STD-PI-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                               //Natural: ASSIGN #OTOT-STD-PI-FINAL-DIV-AMT := #OTOT-STD-PI-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 1/01
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.equals("G")))                                                                                                    //Natural: IF #TRN-FUND-TYPE = 'G'
        {
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #OTOT-GRD-STD-FUND-PAYEES := #OTOT-GRD-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                  //Natural: ASSIGN #OTOT-GRD-STD-PER-PAY-AMT := #OTOT-GRD-STD-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                  //Natural: ASSIGN #OTOT-GRD-STD-PER-DIV-AMT := #OTOT-GRD-STD-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                              //Natural: ASSIGN #OTOT-GRD-STD-FINAL-PAY-AMT := #OTOT-GRD-STD-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                              //Natural: ASSIGN #OTOT-GRD-STD-FINAL-DIV-AMT := #OTOT-GRD-STD-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees.nadd(1);                                                                                                        //Natural: ASSIGN #OTOT-STD-FUND-PAYEES := #OTOT-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                      //Natural: ASSIGN #OTOT-STD-PER-PAY-AMT := #OTOT-STD-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                      //Natural: ASSIGN #OTOT-STD-PER-DIV-AMT := #OTOT-STD-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                                  //Natural: ASSIGN #OTOT-STD-FINAL-PAY-AMT := #OTOT-STD-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                                  //Natural: ASSIGN #OTOT-STD-FINAL-DIV-AMT := #OTOT-STD-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_New_Payment_Amts() throws Exception                                                                                                            //Natural: ACCUM-NEW-PAYMENT-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees.nadd(1);                                                                                                     //Natural: ADD 1 TO #TOT-NEW-FUND-PAYEES
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                   //Natural: ASSIGN #TOT-NEW-PER-PAY-AMT := #TOT-NEW-PER-PAY-AMT + #WRK-PER-PAY-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                   //Natural: ASSIGN #TOT-NEW-PER-DIV-AMT := #TOT-NEW-PER-DIV-AMT + #WRK-PER-DIV-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                               //Natural: ASSIGN #TOT-NEW-FINAL-PAY-AMT := #TOT-NEW-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                               //Natural: ASSIGN #TOT-NEW-FINAL-DIV-AMT := #TOT-NEW-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        //*  ADDED 1/01
        if (condition(! (pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(25) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(27) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(28)  //Natural: IF NOT ( #TRN-OPTION-CODE = 25 OR = 27 OR = 28 OR = 30 )
            || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(30))))
        {
            if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("P") || pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("A") || pnd_Trn_Fund_Rec_Pnd_Trn_Cntrct_Type.equals("I"))) //Natural: IF ( #TRN-CNTRCT-TYPE = 'P' OR = 'A' OR = 'I' )
            {
                if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.equals("G")))                                                                                            //Natural: IF #TRN-FUND-TYPE = 'G'
                {
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #NTOT-GRD-OTH-FUND-PAYEES := #NTOT-GRD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                          //Natural: ASSIGN #NTOT-GRD-OTH-PER-PAY-AMT := #NTOT-GRD-OTH-PER-PAY-AMT + #WRK-PER-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                          //Natural: ASSIGN #NTOT-GRD-OTH-PER-DIV-AMT := #NTOT-GRD-OTH-PER-DIV-AMT + #WRK-PER-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                      //Natural: ASSIGN #NTOT-GRD-OTH-FINAL-PAY-AMT := #NTOT-GRD-OTH-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                      //Natural: ASSIGN #NTOT-GRD-OTH-FINAL-DIV-AMT := #NTOT-GRD-OTH-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees.nadd(1);                                                                                            //Natural: ASSIGN #NTOT-STD-OTH-FUND-PAYEES := #NTOT-STD-OTH-FUND-PAYEES + 1
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                          //Natural: ASSIGN #NTOT-STD-OTH-PER-PAY-AMT := #NTOT-STD-OTH-PER-PAY-AMT + #WRK-PER-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                          //Natural: ASSIGN #NTOT-STD-OTH-PER-DIV-AMT := #NTOT-STD-OTH-PER-DIV-AMT + #WRK-PER-DIV-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                      //Natural: ASSIGN #NTOT-STD-OTH-FINAL-PAY-AMT := #NTOT-STD-OTH-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
                    pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                      //Natural: ASSIGN #NTOT-STD-OTH-FINAL-DIV-AMT := #NTOT-STD-OTH-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING FOR TPA & IPRO 1/01
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(pnd_Tpa_Codes.getValue("*"))))                                                                          //Natural: IF #TRN-OPTION-CODE = #TPA-CODES ( * )
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #NTOT-STD-TPA-FUND-PAYEES := #NTOT-STD-TPA-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                  //Natural: ASSIGN #NTOT-STD-TPA-PER-PAY-AMT := #NTOT-STD-TPA-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                  //Natural: ASSIGN #NTOT-STD-TPA-PER-DIV-AMT := #NTOT-STD-TPA-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                              //Natural: ASSIGN #NTOT-STD-TPA-FINAL-PAY-AMT := #NTOT-STD-TPA-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                              //Natural: ASSIGN #NTOT-STD-TPA-FINAL-DIV-AMT := #NTOT-STD-TPA-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(25) || pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(27)))                                                //Natural: IF #TRN-OPTION-CODE = 25 OR = 27
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees.nadd(1);                                                                                                   //Natural: ASSIGN #NTOT-STD-IPRO-FUND-PAYEES := #NTOT-STD-IPRO-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                 //Natural: ASSIGN #NTOT-STD-IPRO-PER-PAY-AMT := #NTOT-STD-IPRO-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                 //Natural: ASSIGN #NTOT-STD-IPRO-PER-DIV-AMT := #NTOT-STD-IPRO-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                             //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-PAY-AMT := #NTOT-STD-IPRO-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                             //Natural: ASSIGN #NTOT-STD-IPRO-FINAL-DIV-AMT := #NTOT-STD-IPRO-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Option_Code.equals(22)))                                                                                                   //Natural: IF #TRN-OPTION-CODE = 22
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees.nadd(1);                                                                                                     //Natural: ASSIGN #NTOT-STD-PI-FUND-PAYEES := #NTOT-STD-PI-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                   //Natural: ASSIGN #NTOT-STD-PI-PER-PAY-AMT := #NTOT-STD-PI-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                   //Natural: ASSIGN #NTOT-STD-PI-PER-DIV-AMT := #NTOT-STD-PI-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                               //Natural: ASSIGN #NTOT-STD-PI-FINAL-PAY-AMT := #NTOT-STD-PI-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                               //Natural: ASSIGN #NTOT-STD-PI-FINAL-DIV-AMT := #NTOT-STD-PI-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 1/01
        if (condition(pnd_Trn_Fund_Rec_Pnd_Trn_Fund_Type.equals("G")))                                                                                                    //Natural: IF #TRN-FUND-TYPE = 'G'
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees.nadd(1);                                                                                                    //Natural: ASSIGN #NTOT-GRD-STD-FUND-PAYEES := #NTOT-GRD-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                  //Natural: ASSIGN #NTOT-GRD-STD-PER-PAY-AMT := #NTOT-GRD-STD-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                  //Natural: ASSIGN #NTOT-GRD-STD-PER-DIV-AMT := #NTOT-GRD-STD-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                              //Natural: ASSIGN #NTOT-GRD-STD-FINAL-PAY-AMT := #NTOT-GRD-STD-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                              //Natural: ASSIGN #NTOT-GRD-STD-FINAL-DIV-AMT := #NTOT-GRD-STD-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees.nadd(1);                                                                                                        //Natural: ASSIGN #NTOT-STD-FUND-PAYEES := #NTOT-STD-FUND-PAYEES + 1
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt.nadd(pnd_Wrk_Per_Pay_Amt);                                                                                      //Natural: ASSIGN #NTOT-STD-PER-PAY-AMT := #NTOT-STD-PER-PAY-AMT + #WRK-PER-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt.nadd(pnd_Wrk_Per_Div_Amt);                                                                                      //Natural: ASSIGN #NTOT-STD-PER-DIV-AMT := #NTOT-STD-PER-DIV-AMT + #WRK-PER-DIV-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt.nadd(pnd_Wrk_Final_Pay_Amt);                                                                                  //Natural: ASSIGN #NTOT-STD-FINAL-PAY-AMT := #NTOT-STD-FINAL-PAY-AMT + #WRK-FINAL-PAY-AMT
            pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt.nadd(pnd_Wrk_Final_Div_Amt);                                                                                  //Natural: ASSIGN #NTOT-STD-FINAL-DIV-AMT := #NTOT-STD-FINAL-DIV-AMT + #WRK-FINAL-DIV-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Control_Totals() throws Exception                                                                                                              //Natural: PRINT-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA SPECIAL FUNCTION TIAA DIVIDEND CHANGE FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Date8_Pnd_Next_Updt_Ccyymm,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X'IA SPECIAL FUNCTION TIAA DIVIDEND CHANGE FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
            new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,new ColumnSpacing(23),"----APRIL DIVIDEND CHANGE----",NEWLINE,NEWLINE);                      //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT 23X '----APRIL DIVIDEND CHANGE----' //
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Hd_Txt1_Pnd_Hd_Txt1_R);                                                                                            //Natural: WRITE ( 1 ) #HD-TXT1-R
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Hd2_Txt1_Pnd_Hd2_Txt1_R);                                                                                          //Natural: WRITE ( 1 ) #HD2-TXT1-R
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Tiaa_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Tiaa_Payees), pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees)); //Natural: ASSIGN #WRK-CHG-TIAA-PAYEES := #TOT-OLD-TIAA-PAYEES - #TOT-NEW-TIAA-PAYEES
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #TOT-OLD-FUND-PAYEES - #TOT-NEW-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #TOT-NEW-PER-PAY-AMT - #TOT-OLD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #TOT-NEW-PER-DIV-AMT - #TOT-OLD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #TOT-NEW-FINAL-PAY-AMT - #TOT-OLD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt.subtract(pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #TOT-NEW-FINAL-DIV-AMT - #TOT-OLD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA ACTIVE PAYEES            ",new TabSetting(41),pnd_Trailer_Fund_Rec_Pnd_Trl_Old_Tiaa_Payees,                    //Natural: WRITE ( 1 ) '  TIAA ACTIVE PAYEES            ' 41T #TRL-OLD-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #TRL-NEW-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Tiaa_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-GRD-STD-FUND-PAYEES - #OTOT-GRD-STD-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-GRD-STD-PER-PAY-AMT - #OTOT-GRD-STD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-GRD-STD-PER-DIV-AMT - #OTOT-GRD-STD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-GRD-STD-FINAL-PAY-AMT - #OTOT-GRD-STD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-GRD-STD-FINAL-DIV-AMT - #OTOT-GRD-STD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA GRADED   FUND RECORDS    ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA GRADED   FUND RECORDS    ' 41T #OTOT-GRD-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-GRD-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   PER PAYMENT     ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       GRADED   PER PAYMENT     ' 35T #OTOT-GRD-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   PER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Per_Div_Amt,                //Natural: WRITE ( 1 ) '       GRADED   PER DIDVIDEND  ' 35T #OTOT-GRD-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Pay_Amt,              //Natural: WRITE ( 1 ) '       GRADED   FINAL PAYMENT  ' 35T#OTOT-GRD-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED   FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Std_Final_Div_Amt,              //Natural: WRITE ( 1 ) '       GRADED   FINAL DIDVIDEND' 35T#OTOT-GRD-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Std_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-FUND-PAYEES - #OTOT-STD-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-PER-PAY-AMT - #OTOT-STD-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-PER-DIV-AMT - #OTOT-STD-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-FINAL-PAY-AMT - #OTOT-STD-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-FINAL-DIV-AMT - #OTOT-STD-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STANDARD FUND RECORDS    ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Fund_Payees,                   //Natural: WRITE ( 1 ) '  TIAA STANDARD FUND RECORDS    ' 41T #OTOT-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD PER PAYMENT     ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Pay_Amt,                   //Natural: WRITE ( 1 ) '       STANDARD PER PAYMENT     ' 35T #OTOT-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD PER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Per_Div_Amt,                    //Natural: WRITE ( 1 ) '       STANDARD PER DIDVIDEND  ' 35T #OTOT-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Pay_Amt,                  //Natural: WRITE ( 1 ) '       STANDARD FINAL PAYMENT  ' 35T#OTOT-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STANDARD FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Final_Div_Amt,                  //Natural: WRITE ( 1 ) '       STANDARD FINAL DIDVIDEND' 35T#OTOT-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-GRD-OTH-FUND-PAYEES - #OTOT-GRD-OTH-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-GRD-OTH-PER-PAY-AMT - #OTOT-GRD-OTH-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-GRD-OTH-PER-DIV-AMT - #OTOT-GRD-OTH-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-GRD-OTH-FINAL-PAY-AMT - #OTOT-GRD-OTH-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-GRD-OTH-FINAL-DIV-AMT - #OTOT-GRD-OTH-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA GRADED OTH FUND RECORDS  ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA GRADED OTH FUND RECORDS  ' 41T #OTOT-GRD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-GRD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH PER PAYMENT   ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       GRADED OTH PER PAYMENT   ' 35T #OTOT-GRD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH PER DIDVIDEND ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Per_Div_Amt,               //Natural: WRITE ( 1 ) '       GRADED OTH PER DIDVIDEND ' 35T #OTOT-GRD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-GRD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Pay_Amt,             //Natural: WRITE ( 1 ) '       GRADED OTH FINAL PAYMENT ' 35T#OTOT-GRD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-GRD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Grd_Oth_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       GRADED OTH FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Grd_Oth_Final_Div_Amt,            //Natural: WRITE ( 1 ) '       GRADED OTH FINAL DIDVIDEND' 35T#OTOT-GRD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-OTH-FUND-PAYEES - #OTOT-STD-OTH-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-OTH-PER-PAY-AMT - #OTOT-STD-OTH-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-OTH-PER-DIV-AMT - #OTOT-STD-OTH-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-OTH-FINAL-PAY-AMT - #OTOT-STD-OTH-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-OTH-FINAL-DIV-AMT - #OTOT-STD-OTH-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD OTH FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Fund_Payees,              //Natural: WRITE ( 1 ) '  TIAA STNDRD OTH FUND RECORDS   ' 41T #OTOT-STD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-OTH-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH PER PAYMENT    ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Pay_Amt,              //Natural: WRITE ( 1 ) '       STNDRD OTH PER PAYMENT    ' 35T #OTOT-STD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-OTH-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH PER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Per_Div_Amt,              //Natural: WRITE ( 1 ) '       STNDRD OTH PER DIDVIDEND  ' 35T #OTOT-STD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-OTH-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Pay_Amt,            //Natural: WRITE ( 1 ) '       STNDRD OTH FINAL PAYMENT  ' 35T#OTOT-STD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-OTH-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD OTH FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Oth_Final_Div_Amt,            //Natural: WRITE ( 1 ) '       STNDRD OTH FINAL DIDVIDEND' 35T#OTOT-STD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-OTH-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Oth_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-TPA-FUND-PAYEES - #OTOT-STD-TPA-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-TPA-PER-PAY-AMT - #OTOT-STD-TPA-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-TPA-PER-DIV-AMT - #OTOT-STD-TPA-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-TPA-FINAL-PAY-AMT - #OTOT-STD-TPA-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-TPA-FINAL-DIV-AMT - #OTOT-STD-TPA-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD TPA FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Fund_Payees,              //Natural: WRITE ( 1 ) '  TIAA STNDRD TPA FUND RECORDS   ' 41T #OTOT-STD-TPA-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-TPA-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA PER PAYMENT    ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Pay_Amt,              //Natural: WRITE ( 1 ) '       STNDRD TPA PER PAYMENT    ' 35T #OTOT-STD-TPA-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-TPA-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA PER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Per_Div_Amt,              //Natural: WRITE ( 1 ) '       STNDRD TPA PER DIDVIDEND  ' 35T #OTOT-STD-TPA-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-TPA-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Pay_Amt,            //Natural: WRITE ( 1 ) '       STNDRD TPA FINAL PAYMENT  ' 35T#OTOT-STD-TPA-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-TPA-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD TPA FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Tpa_Final_Div_Amt,            //Natural: WRITE ( 1 ) '       STNDRD TPA FINAL DIDVIDEND' 35T#OTOT-STD-TPA-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-TPA-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-IPRO-FUND-PAYEES - #OTOT-STD-IPRO-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-IPRO-PER-PAY-AMT - #OTOT-STD-IPRO-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-IPRO-PER-DIV-AMT - #OTOT-STD-IPRO-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-IPRO-FINAL-PAY-AMT - #OTOT-STD-IPRO-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-IPRO-FINAL-DIV-AMT - #OTOT-STD-IPRO-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD IPRO FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Fund_Payees,            //Natural: WRITE ( 1 ) '  TIAA STNDRD IPRO FUND RECORDS   ' 41T #OTOT-STD-IPRO-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-IPRO-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO PER PAYMENT   ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Pay_Amt,             //Natural: WRITE ( 1 ) '       STNDRD IPRO PER PAYMENT   ' 35T #OTOT-STD-IPRO-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-IPRO-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPROPER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Per_Div_Amt,             //Natural: WRITE ( 1 ) '       STNDRD IPROPER DIDVIDEND  ' 35T #OTOT-STD-IPRO-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-IPRO-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD IPRO FINAL PAYMENT ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Pay_Amt,           //Natural: WRITE ( 1 ) '       STNDRD IPRO FINAL PAYMENT ' 35T#OTOT-STD-IPRO-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-IPRO-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Ipro_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"      STNDRD IPRO FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Ipro_Final_Div_Amt,           //Natural: WRITE ( 1 ) '      STNDRD IPRO FINAL DIDVIDEND' 35T#OTOT-STD-IPRO-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-TPA-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Tpa_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #NTOT-STD-PI-FUND-PAYEES - #OTOT-STD-PI-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #NTOT-STD-PI-PER-PAY-AMT - #OTOT-STD-PI-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #NTOT-STD-PI-PER-DIV-AMT - #OTOT-STD-PI-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #NTOT-STD-PI-FINAL-PAY-AMT - #OTOT-STD-PI-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt.subtract(pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #NTOT-STD-PI-FINAL-DIV-AMT - #OTOT-STD-PI-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,"  TIAA STNDRD P/I FUND RECORDS   ",new TabSetting(41),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Fund_Payees,               //Natural: WRITE ( 1 ) '  TIAA STNDRD P/I FUND RECORDS   ' 41T #OTOT-STD-PI-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #NTOT-STD-PI-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I PER PAYMENT    ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Pay_Amt,               //Natural: WRITE ( 1 ) '       STNDRD P/I PER PAYMENT    ' 35T #OTOT-STD-PI-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PI-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I PER DIDVIDEND  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Per_Div_Amt,               //Natural: WRITE ( 1 ) '       STNDRD P/I PER DIDVIDEND  ' 35T #OTOT-STD-PI-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #NTOT-STD-PI-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I FINAL PAYMENT  ",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Pay_Amt,             //Natural: WRITE ( 1 ) '       STNDRD P/I FINAL PAYMENT  ' 35T#OTOT-STD-PI-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-PI-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"       STNDRD P/I FINAL DIDVIDEND",new TabSetting(35),pnd_Tot_Accumulators_Pnd_Otot_Std_Pi_Final_Div_Amt,             //Natural: WRITE ( 1 ) '       STNDRD P/I FINAL DIDVIDEND' 35T#OTOT-STD-PI-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#NTOT-STD-PI-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Tot_Accumulators_Pnd_Ntot_Std_Pi_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees.nadd(pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees);                                                                   //Natural: ASSIGN #TOT-OLD-TIAA-PAYEES := #TOT-OLD-TIAA-PAYEES + #TRL-REA-PAYEES
        pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees.nadd(pnd_Trn_Fund_Rec_Pnd_Trl_Rea_Payees);                                                                   //Natural: ASSIGN #TOT-NEW-TIAA-PAYEES := #TOT-NEW-TIAA-PAYEES + #TRL-REA-PAYEES
        if (condition(pnd_Trailer_Fund_Rec_Pnd_Trl_Fund_Rec_Redf.getValue("*").notEquals(pnd_Total_Prog_Accumulations_Pnd_Total_Prog_Accumulations_Redf.getValue("*"))))  //Natural: IF #TRL-FUND-REC-REDF ( * ) NE #TOTAL-PROG-ACCUMULATIONS-REDF ( * )
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Rate_Totals() throws Exception                                                                                                                 //Natural: PRINT-RATE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA SPECIAL FUNCTION TOTALS BY RATE FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Date8_Pnd_Next_Updt_Ccyymm,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X'IA SPECIAL FUNCTION TOTALS BY RATE FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-UPDT-CCYYMM ( EM = 9999/99 ) 4X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
            new ReportEditMask ("9999/99"),new ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,NEWLINE,NEWLINE);                                                                            //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT //
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(7),"     RATE               DIVIDENDS        FINAL DIVIDENDS    ");                                  //Natural: WRITE ( 1 ) 7X'     RATE               DIVIDENDS        FINAL DIVIDENDS    '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(7),"    ------           ---------------     ---------------    ");                                  //Natural: WRITE ( 1 ) 7X'    ------           ---------------     ---------------    '
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 201
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(201)); pnd_I.nadd(1))
        {
            //*  IF #TOT-RATE-DIV (#I) NE 0
            //*  CHANGED ABOVE TO FOLLOWING 9/03
            if (condition(! (pnd_Tot_Rate_Cde.getValue(pnd_I).equals("00") || pnd_Tot_Rate_Cde.getValue(pnd_I).equals("  "))))                                            //Natural: IF NOT ( #TOT-RATE-CDE ( #I ) = '00' OR = '  ' )
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),pnd_Tot_Rate_Cde.getValue(pnd_I), new ReportEditMask ("XX"),new TabSetting(30),pnd_Tot_Rate_Div.getValue(pnd_I),  //Natural: WRITE ( 1 ) 15T #TOT-RATE-CDE ( #I ) ( EM = XX ) 30T#TOT-RATE-DIV ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 50T#TOT-RATE-FINAL-DIV ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new TabSetting(50),pnd_Tot_Rate_Final_Div.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Gtot_Rate_Div.nadd(pnd_Tot_Rate_Div.getValue(pnd_I));                                                                                                 //Natural: ASSIGN #GTOT-RATE-DIV := #GTOT-RATE-DIV + #TOT-RATE-DIV ( #I )
                pnd_Gtot_Rate_Final_Div.nadd(pnd_Tot_Rate_Final_Div.getValue(pnd_I));                                                                                     //Natural: ASSIGN #GTOT-RATE-FINAL-DIV := #GTOT-RATE-FINAL-DIV + #TOT-RATE-FINAL-DIV ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"GRAND TOTAL....",new TabSetting(27),pnd_Gtot_Rate_Div,new  //Natural: WRITE ( 1 ) / 2T'GRAND TOTAL....' 27T#GTOT-RATE-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 47T#GTOT-RATE-FINAL-DIV ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            TabSetting(47),pnd_Gtot_Rate_Final_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wrk_Chg_Tiaa_Payees.setValue(0);                                                                                                                              //Natural: ASSIGN #WRK-CHG-TIAA-PAYEES := 0
        pnd_Wrk_Chg_Fund_Payees.setValue(0);                                                                                                                              //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := 0
        pnd_Wrk_Chg_Per_Pay_Amt.setValue(0);                                                                                                                              //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := 0
        pnd_Wrk_Chg_Per_Div_Amt.setValue(0);                                                                                                                              //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := 0
        pnd_Wrk_Chg_Final_Pay_Amt.setValue(0);                                                                                                                            //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := 0
        pnd_Wrk_Chg_Final_Div_Amt.setValue(0);                                                                                                                            //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := 0
        pnd_Wrk_Chg_Tiaa_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Tiaa_Payees), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees)); //Natural: ASSIGN #WRK-CHG-TIAA-PAYEES := #TOT-NEW-TIAA-PAYEES - #TRL-NEW-TIAA-PAYEES
        pnd_Wrk_Chg_Fund_Payees.compute(new ComputeParameters(false, pnd_Wrk_Chg_Fund_Payees), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees)); //Natural: ASSIGN #WRK-CHG-FUND-PAYEES := #TOT-NEW-FUND-PAYEES - #TRL-NEW-FUND-PAYEES
        pnd_Wrk_Chg_Per_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Pay_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-PER-PAY-AMT := #TOT-NEW-PER-PAY-AMT - #TRL-NEW-PER-PAY-AMT
        pnd_Wrk_Chg_Per_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Per_Div_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Div_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt)); //Natural: ASSIGN #WRK-CHG-PER-DIV-AMT := #TOT-NEW-PER-DIV-AMT - #TRL-NEW-PER-DIV-AMT
        pnd_Wrk_Chg_Final_Pay_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Pay_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-PAY-AMT := #TOT-NEW-FINAL-PAY-AMT - #TRL-NEW-FINAL-PAY-AMT
        pnd_Wrk_Chg_Final_Div_Amt.compute(new ComputeParameters(false, pnd_Wrk_Chg_Final_Div_Amt), pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt.subtract(pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt)); //Natural: ASSIGN #WRK-CHG-FINAL-DIV-AMT := #TOT-NEW-FINAL-DIV-AMT - #TRL-NEW-FINAL-DIV-AMT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** DIFFERENCE IN CALCULATIONS BETWEEN ABOVE INPUT TRAILER ","AMOUNTS & PROGRAM OUTPUT AMOUNTS BELOW"); //Natural: WRITE ( 1 ) /// '*** DIFFERENCE IN CALCULATIONS BETWEEN ABOVE INPUT TRAILER ' 'AMOUNTS & PROGRAM OUTPUT AMOUNTS BELOW'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FUND RECORDS    ",new TabSetting(41),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Fund_Payees,            //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FUND RECORDS    ' 41T #TOT-OLD-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #TOT-NEW-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 102T #TRL-NEW-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 121T #WRK-CHG-FUND-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Total_Prog_Accumulations_Pnd_Tot_New_Fund_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(102),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new 
            TabSetting(121),pnd_Wrk_Chg_Fund_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL PER PAYMENT     ",new TabSetting(35),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Pay_Amt,            //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL PER PAYMENT     ' 35T #TOT-OLD-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #TOT-NEW-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T #TRL-NEW-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 115T #WRK-CHG-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Total_Prog_Accumulations_Pnd_Tot_New_Per_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            TabSetting(115),pnd_Wrk_Chg_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL PER DIDVIDEND   ",new TabSetting(35),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Per_Div_Amt,            //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL PER DIDVIDEND   ' 35T #TOT-OLD-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T #TRL-NEW-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T #TRL-NEW-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 115T #WRK-CHG-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            TabSetting(115),pnd_Wrk_Chg_Per_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL PAYMENT   ",new TabSetting(35),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Pay_Amt,          //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FINAL PAYMENT   ' 35T#TOT-OLD-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#TOT-NEW-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T#TRL-NEW-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 115T#WRK-CHG-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Pay_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            TabSetting(115),pnd_Wrk_Chg_Final_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADTIONAL FINAL DIDVIDEND ",new TabSetting(35),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Final_Div_Amt,          //Natural: WRITE ( 1 ) 'TIAA TRADTIONAL FINAL DIDVIDEND ' 35T#TOT-OLD-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 54T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 75T#TOT-NEW-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 96T#TRL-NEW-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- ) 115T#WRK-CHG-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99- )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(54),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(75),pnd_Total_Prog_Accumulations_Pnd_Tot_New_Final_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new TabSetting(96),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new 
            TabSetting(115),pnd_Wrk_Chg_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA ACTIVE PAYEES              ",new TabSetting(41),pnd_Total_Prog_Accumulations_Pnd_Tot_Old_Tiaa_Payees,            //Natural: WRITE ( 1 ) 'TIAA ACTIVE PAYEES              ' 41T #TOT-OLD-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 60T #WRK-CHG-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 81T #TOT-NEW-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 102T #TRL-NEW-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- ) 121T #WRK-CHG-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(60),pnd_Wrk_Chg_Tiaa_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(81),pnd_Total_Prog_Accumulations_Pnd_Tot_New_Tiaa_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new TabSetting(102),pnd_Trailer_Fund_Rec_Pnd_Trl_New_Tiaa_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new 
            TabSetting(121),pnd_Wrk_Chg_Tiaa_Payees, new ReportEditMask ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL FUND RECORDS CHANGED......",new TabSetting(60),pnd_Wrk_Tot_Fnd_Rec_Changed, new ReportEditMask                  //Natural: WRITE ( 1 ) 'TOTAL FUND RECORDS CHANGED......' 60T #WRK-TOT-FND-REC-CHANGED ( EM = ZZZ,ZZZ,ZZ9- )
            ("ZZZ,ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
