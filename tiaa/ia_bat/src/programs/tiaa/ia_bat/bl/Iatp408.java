/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:32 PM
**        * FROM NATURAL PROGRAM : Iatp408
************************************************************
**        * FILE NAME            : Iatp408.java
**        * CLASS NAME           : Iatp408
**        * INSTANCE NAME        : Iatp408
************************************************************
************************************************************************
* PROGRAM: IATP408
* DATE   : 05/08/99
* AUTHOR : ILYA KIZHNERMAN
* MODE   : BATCH
* DESC   : THIS PROGRAM CREATE DATA ELEMENTS FOR THE IA TRANFER CREF
*        : CONTRACT AND/OR DIRECTORY CIS INTERFACE.
*        : THIS PROGRAM READ THE IAA-XFR-AUDIT FILE TO FIND ALL NEW
*        : ISSUES. THIS MODULE PROCESSES TIAA TO CREF ONLY
*          DETERMINED BY RQST-XFR-TYPE NE ' '. VALUE OF '0' MEAN SINGLE
*          AND GREATER THAN '0' MEANS REPETITIVE TRANSFER.
* HISTORY:
*
*     02/28/2002 ADDED LOGIC TO CREATE REPORT #2 EVEN THOUGH THERE ARE
*                NO ERRORS ON THIS RUN.
*     01/28/2009 OS RECOMPILED TO GET LATEST IATL406.
*                   SOME OTHER CHANGES. SC 012809.
*     07/20/2010 OS RECOMPILED TO GET LATEST IATL406.
*     02/27/2014 OS CREF REA CHANGES MARKED REA0614.
*     10/07/2014 JT COR/NAAD DECOMMISSION CHANGES MARKED 10/2014
*     04/2017    OS PIN EXPANSION - SC 082017 FOR CHANGES.
*     08/2017    OS MINOR CHANGE RELATED TO PIN EXPANSION MARKED OS08.
*     07/2018    JT RESET #I-TIAA-CREF-IND BEFORE CALLING MDM
*                   SC 07/2018.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp408 extends BLNatBase
{
    // Data Areas
    private LdaNazl6004 ldaNazl6004;
    private PdaIaaa299a pdaIaaa299a;
    private PdaIaaa202h pdaIaaa202h;
    private PdaIaapda_M pdaIaapda_M;
    private LdaIatl406 ldaIatl406;
    private LdaIaal200b ldaIaal200b;
    private LdaIaal200a ldaIaal200a;
    private LdaIaal204a ldaIaal204a;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw;
    private DbsField iaa_Trnsfr_Sw_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Ia_Frm_Payee;

    private DataAccessProgramView vw_cis_Updt;
    private DbsField cis_Updt_Cis_Rqst_Id_Key;

    private DbsGroup cis_Updt_Cis_Sg_Fund_Identifier;
    private DbsField cis_Updt_Cis_Sg_Fund_Ticker;
    private DbsField pnd_Total_Sw;
    private DbsField pnd_Dod_1_Sw;
    private DbsField pnd_Dod_2_Sw;
    private DbsField pnd_Count_Pe;
    private DbsField pnd_Zip;
    private DbsField cntrct_Issue_Dte_D;

    private DbsGroup cntrct_Issue_Dte_D__R_Field_1;
    private DbsField cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date;
    private DbsField cntrct_Dte_D;

    private DbsGroup cntrct_Dte_D__R_Field_2;
    private DbsField cntrct_Dte_D_Cntrct_Dte_Date;
    private DbsField compress_Dd;
    private DbsField compress_Yymm;
    private DbsField pnd_Count_Pe_Save;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_3;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_V_Corr_Cntrct_Payee_Key;
    private DbsField pnd_Functions;

    private DbsGroup pnd_Functions__R_Field_4;
    private DbsField pnd_Functions_Pnd_Funct_Bene;
    private DbsField pnd_Functions_Pnd_Funct_Bene_Upd;
    private DbsField pnd_Functions_Pnd_Funct_Ilog;
    private DbsField pnd_Functions_Pnd_Funct_Mit;
    private DbsField pnd_Functions_Pnd_Funct_Efm;
    private DbsField pnd_Functions_Pnd_Funct_Cor;
    private DbsField pnd_Functions_Pnd_Funct_Aloc;
    private DbsField pnd_Functions_Pnd_Funct_Nonp;
    private DbsField pnd_Functions_Pnd_Funct_Naad;
    private DbsField pnd_Functions_Pnd_Funct_State;
    private DbsField pnd_Functions_Pnd_Funct_Filler;
    private DbsField pnd_Funds;

    private DbsGroup pnd_Funds__R_Field_5;
    private DbsField pnd_Funds_Pnd_Fund_A;
    private DbsField pnd_Ws_Fund;
    private DbsField pnd_I;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_New_Issue_Tiaa;
    private DbsField pnd_L_Pnd_Vacation_Address;
    private DbsField pnd_L_Pnd_Error;
    private DbsField pnd_Curr_Proc_Dte;

    private DbsGroup pnd_Curr_Proc_Dte__R_Field_6;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Yyyy;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Mm;
    private DbsField pnd_Curr_Proc_Dte_Pnd_Dd;

    private DbsGroup pnd_Count;
    private DbsField pnd_Count_Pnd_Err_Cnt;
    private DbsField pnd_Count_Pnd_Comp_Cnt;
    private DbsField pnd_Count_Pnd_Rec_Cnt;
    private DbsField pnd_Iaxfr_Units_A;
    private DbsField pnd_Iaxfr_Units_M;
    private DbsField pnd_Reason1;
    private DbsField pnd_Reason2;
    private DbsField pnd_Found;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_A;
    private DbsField pnd_M;
    private DbsField pnd_Cis_No_Found_Sw;
    private DbsField pnd_Index;
    private DbsField pnd_Date_D;
    private DbsField cntrct_Issue_Dte_Dd_1;
    private DbsField pnd_Audit_Cntrct;
    private DbsField pnd_Audit_Payee;
    private DbsField pnd_Cis_From_Iss_Date;

    private DbsGroup pnd_Cis_From_Iss_Date__R_Field_7;
    private DbsField pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Mm;
    private DbsField pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_8;
    private DbsField pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Dd;
    private DbsField pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_9;
    private DbsField pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Ccyy;
    private DbsField pnd_Compress_Date_1;
    private DbsField pnd_Compress_Date_2;
    private DbsField pnd_Cis_Issue_Date;
    private DbsField pnd_Cis_Log_Date;
    private DbsField pnd_Cis_Frst_Annt_Ssn;

    private DbsGroup pnd_Cis_Frst_Annt_Ssn__R_Field_8;
    private DbsField pnd_Cis_Frst_Annt_Ssn_Pnd_Cis_Frst_Annt_Ssn_A;
    private DbsField pnd_Cis_Frst_Annt_Dob;

    private DbsGroup pnd_Cis_Frst_Annt_Dob__R_Field_9;
    private DbsField pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Cc;
    private DbsField pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Yy;
    private DbsField pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Mmdd;
    private DbsField pnd_Cis_Scnd_Annt_Dob;

    private DbsGroup pnd_Cis_Scnd_Annt_Dob__R_Field_10;
    private DbsField pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Cc;
    private DbsField pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Yy;
    private DbsField pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Mmdd;
    private DbsField pnd_Cis_Scnd_Annt_Ssn;

    private DbsGroup pnd_Cis_Scnd_Annt_Ssn__R_Field_11;
    private DbsField pnd_Cis_Scnd_Annt_Ssn_Pnd_Cis_Scnd_Annt_Ssn_A;
    private DbsField pnd_Date_A8;

    private DbsGroup pnd_Date_A8__R_Field_12;
    private DbsField pnd_Date_A8_Pnd_Date_N8;

    private DbsGroup pnd_Date_A8__R_Field_13;
    private DbsField pnd_Date_A8_Pnd_Date_Yyyymm;
    private DbsField pnd_Date_A8_Pnd_Date_Dd;

    private DbsGroup pnd_Date_A8__R_Field_14;
    private DbsField pnd_Date_A8_Pnd_Date_Yyyy;
    private DbsField pnd_Date_A8_Pnd_Date_Mm;

    private DbsGroup pnd_Date_A8__R_Field_15;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Ccyy;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Mm;
    private DbsField pnd_Date_A8_Pnd_Date_A8_Dd;

    private DbsGroup pnd_Date_A8__R_Field_16;
    private DbsField pnd_Date_A8_Fill;

    private DbsGroup pnd_Date_A8_Date_P;
    private DbsField pnd_Date_A8_Pnd_Date_A_Yy;
    private DbsField pnd_Date_A8_Pnd_Date_A_Mm;
    private DbsField pnd_Date_A8_Pnd_Date_A_Dd;
    private DbsField pnd_Orig_Issue;
    private DbsField pnd_Orig_Issue_Re;
    private DbsField pnd_Issue_Date_D;
    private DbsField pnd_Issue_Date_A8;

    private DbsGroup pnd_Issue_Date_A8__R_Field_17;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_N8;

    private DbsGroup pnd_Issue_Date_A8__R_Field_18;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Dd;

    private DbsGroup pnd_Issue_Date_A8__R_Field_19;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A8_Pnd_Issue_Date_Mm;

    private DbsGroup pnd_Cis_Frst_Dob;
    private DbsField pnd_Cis_Frst_Dob_Pnd_Cis_Frst_Yy_1;
    private DbsField pnd_Cis_Frst_Dob_Pnd_Cis_Rrs_Mmdd_1;

    private DbsGroup pnd_Cis_Frst_Dob__R_Field_20;
    private DbsField pnd_Cis_Frst_Dob_Pnd_Cis_Dob_F;

    private DbsGroup pnd_Cis_Scnd_Dob;
    private DbsField pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Yy_1;
    private DbsField pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Mmdd_1;

    private DbsGroup pnd_Cis_Scnd_Dob__R_Field_21;
    private DbsField pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Dob_1;

    private DbsGroup print_Line;
    private DbsField print_Line_Ppcn;
    private DbsField print_Line_Filler;
    private DbsField print_Line_Payee_Cd;
    private DbsField print_Line_Fillr;
    private DbsField print_Line_Mode_Code;
    private DbsField print_Line_Filler1;
    private DbsField print_Line_Fund;
    private DbsField print_Line_Filler2;
    private DbsField print_Line_Ia_Units;
    private DbsField print_Line_Filler3;
    private DbsField print_Line_Cis_Units;
    private DbsField print_Line_Filler4;
    private DbsField print_Line_Cis_Option;
    private DbsField print_Line_Filler5;
    private DbsField print_Line_Cis_Gr_Years;
    private DbsField print_Line_Filler6;
    private DbsField print_Line_Cis_Gr_Days;

    private DbsGroup print_Total_Line_Annual;
    private DbsField print_Total_Line_Annual_Fund_Annual;
    private DbsField print_Total_Line_Annual_Ia_Units_Annual_Tot;
    private DbsField print_Total_Line_Annual_Cis_Units_Annual_Tot;
    private DbsField print_Total_Line_Annual_Ia_Units_Annual_Tot_Bypass;

    private DbsGroup print_Total_Line_Monthly;
    private DbsField print_Total_Line_Monthly_Fund_Mnthly;
    private DbsField print_Total_Line_Monthly_Ia_Units_Mnthly_Tot;
    private DbsField print_Total_Line_Monthly_Cis_Units_Mnthly_Tot;
    private DbsField print_Total_Line_Monthly_Ia_Units_Mnthly_Tot_Bypass;

    private DbsGroup print_Total_Line_Annual_C;
    private DbsField print_Total_Line_Annual_C_Fund_Annual_C;
    private DbsField print_Total_Line_Annual_C_Ia_Units_Annual_Tot_C;
    private DbsField print_Total_Line_Annual_C_Cis_Units_Annual_Tot_C;
    private DbsField print_Total_Line_Annual_C_Ia_Units_Annual_Tot_Bypass_C;

    private DbsGroup print_Total_Line_Monthly_C;
    private DbsField print_Total_Line_Monthly_C_Fund_Mnthly_C;
    private DbsField print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_C;
    private DbsField print_Total_Line_Monthly_C_Cis_Units_Mnthly_Tot_C;
    private DbsField print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_Bypass_C;
    private DbsField pnd_Iaxfr_Unit;
    private DbsField pnd_Cis_Unit_A;
    private DbsField pnd_Cis_Unit_M;
    private DbsField pnd_Cor_Unique_Id;
    private DbsField pnd_Numeric_Ste;
    private DbsField pnd_Alpha_Ste;
    private DbsField pnd_Ii;
    private DbsField pnd_Todays_Date;
    private DbsField pnd_Address_Array;
    private DbsField pnd_Err_Reason;
    private DbsField pnd_Comp_Reason;
    private DbsField pnd_To_Company;
    private DbsField pnd_Fr_Company;
    private DbsField pnd_From_Cref;
    private DbsField pnd_Process_Date_A;

    private DbsGroup pnd_Process_Date_A__R_Field_22;
    private DbsField pnd_Process_Date_A_Pnd_Process_Date_N;
    private DbsField pnd_Isn;
    private DbsField pnd_Rqst_Id;
    private DbsField pnd_Nbr_Acct;
    private DbsField pnd_Acct_Std_Alpha_Cde;
    private DbsField pnd_Acct_Nme_5;
    private DbsField pnd_Acct_Tckr;
    private DbsField pnd_Acct_Effctve_Dte;
    private DbsField pnd_Acct_Unit_Rte_Ind;
    private DbsField pnd_Acct_Std_Nm_Cde;
    private DbsField pnd_Acct_Rpt_Seq;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaNazl6004 = new LdaNazl6004();
        registerRecord(ldaNazl6004);
        localVariables = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(localVariables);
        pdaIaaa202h = new PdaIaaa202h(localVariables);
        pdaIaapda_M = new PdaIaapda_M(localVariables);
        ldaIatl406 = new LdaIatl406();
        registerRecord(ldaIatl406);
        registerRecord(ldaIatl406.getVw_cis_Prtcpnt_File());
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaIaal204a = new LdaIaal204a();
        registerRecord(ldaIaal204a);
        registerRecord(ldaIaal204a.getVw_iaa_Xfr_Audit_View());
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        vw_iaa_Trnsfr_Sw = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw", "IAA-TRNSFR-SW"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        iaa_Trnsfr_Sw_Rqst_Id = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Trnsfr_Sw_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Ia_Unique_Id = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_FRM_PAYEE");
        registerRecord(vw_iaa_Trnsfr_Sw);

        vw_cis_Updt = new DataAccessProgramView(new NameInfo("vw_cis_Updt", "CIS-UPDT"), "CIS_PRTCPNT_FILE", "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE"));
        cis_Updt_Cis_Rqst_Id_Key = vw_cis_Updt.getRecord().newFieldInGroup("cis_Updt_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "CIS_RQST_ID_KEY");

        cis_Updt_Cis_Sg_Fund_Identifier = vw_cis_Updt.getRecord().newGroupInGroup("cis_Updt_Cis_Sg_Fund_Identifier", "CIS-SG-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Updt_Cis_Sg_Fund_Ticker = cis_Updt_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Updt_Cis_Sg_Fund_Ticker", "CIS-SG-FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_TICKER", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        registerRecord(vw_cis_Updt);

        pnd_Total_Sw = localVariables.newFieldInRecord("pnd_Total_Sw", "#TOTAL-SW", FieldType.STRING, 1);
        pnd_Dod_1_Sw = localVariables.newFieldInRecord("pnd_Dod_1_Sw", "#DOD-1-SW", FieldType.STRING, 1);
        pnd_Dod_2_Sw = localVariables.newFieldInRecord("pnd_Dod_2_Sw", "#DOD-2-SW", FieldType.STRING, 1);
        pnd_Count_Pe = localVariables.newFieldInRecord("pnd_Count_Pe", "#COUNT-PE", FieldType.NUMERIC, 3);
        pnd_Zip = localVariables.newFieldInRecord("pnd_Zip", "#ZIP", FieldType.STRING, 5);
        cntrct_Issue_Dte_D = localVariables.newFieldInRecord("cntrct_Issue_Dte_D", "CNTRCT-ISSUE-DTE-D", FieldType.STRING, 8);

        cntrct_Issue_Dte_D__R_Field_1 = localVariables.newGroupInRecord("cntrct_Issue_Dte_D__R_Field_1", "REDEFINE", cntrct_Issue_Dte_D);
        cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date = cntrct_Issue_Dte_D__R_Field_1.newFieldInGroup("cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date", "CNTRCT-ISSUE-DTE-DATE", 
            FieldType.NUMERIC, 8);
        cntrct_Dte_D = localVariables.newFieldInRecord("cntrct_Dte_D", "CNTRCT-DTE-D", FieldType.STRING, 8);

        cntrct_Dte_D__R_Field_2 = localVariables.newGroupInRecord("cntrct_Dte_D__R_Field_2", "REDEFINE", cntrct_Dte_D);
        cntrct_Dte_D_Cntrct_Dte_Date = cntrct_Dte_D__R_Field_2.newFieldInGroup("cntrct_Dte_D_Cntrct_Dte_Date", "CNTRCT-DTE-DATE", FieldType.NUMERIC, 6);
        compress_Dd = localVariables.newFieldInRecord("compress_Dd", "COMPRESS-DD", FieldType.STRING, 2);
        compress_Yymm = localVariables.newFieldInRecord("compress_Yymm", "COMPRESS-YYMM", FieldType.STRING, 6);
        pnd_Count_Pe_Save = localVariables.newFieldInRecord("pnd_Count_Pe_Save", "#COUNT-PE-SAVE", FieldType.NUMERIC, 3);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_V_Corr_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_V_Corr_Cntrct_Payee_Key", "#V-CORR-CNTRCT-PAYEE-KEY", FieldType.STRING, 13);
        pnd_Functions = localVariables.newFieldInRecord("pnd_Functions", "#FUNCTIONS", FieldType.STRING, 40);

        pnd_Functions__R_Field_4 = localVariables.newGroupInRecord("pnd_Functions__R_Field_4", "REDEFINE", pnd_Functions);
        pnd_Functions_Pnd_Funct_Bene = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Bene", "#FUNCT-BENE", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Bene_Upd = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Bene_Upd", "#FUNCT-BENE-UPD", FieldType.STRING, 
            2);
        pnd_Functions_Pnd_Funct_Ilog = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Ilog", "#FUNCT-ILOG", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Mit = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Mit", "#FUNCT-MIT", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Efm = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Efm", "#FUNCT-EFM", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Cor = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Cor", "#FUNCT-COR", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Aloc = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Aloc", "#FUNCT-ALOC", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Nonp = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Nonp", "#FUNCT-NONP", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Naad = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Naad", "#FUNCT-NAAD", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_State = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_State", "#FUNCT-STATE", FieldType.STRING, 2);
        pnd_Functions_Pnd_Funct_Filler = pnd_Functions__R_Field_4.newFieldInGroup("pnd_Functions_Pnd_Funct_Filler", "#FUNCT-FILLER", FieldType.STRING, 
            20);
        pnd_Funds = localVariables.newFieldInRecord("pnd_Funds", "#FUNDS", FieldType.STRING, 5);

        pnd_Funds__R_Field_5 = localVariables.newGroupInRecord("pnd_Funds__R_Field_5", "REDEFINE", pnd_Funds);
        pnd_Funds_Pnd_Fund_A = pnd_Funds__R_Field_5.newFieldArrayInGroup("pnd_Funds_Pnd_Fund_A", "#FUND-A", FieldType.STRING, 1, new DbsArrayController(1, 
            5));
        pnd_Ws_Fund = localVariables.newFieldInRecord("pnd_Ws_Fund", "#WS-FUND", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_New_Issue_Tiaa = pnd_L.newFieldInGroup("pnd_L_Pnd_New_Issue_Tiaa", "#NEW-ISSUE-TIAA", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Vacation_Address = pnd_L.newFieldInGroup("pnd_L_Pnd_Vacation_Address", "#VACATION-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_L_Pnd_Error = pnd_L.newFieldInGroup("pnd_L_Pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Curr_Proc_Dte = localVariables.newFieldInRecord("pnd_Curr_Proc_Dte", "#CURR-PROC-DTE", FieldType.STRING, 8);

        pnd_Curr_Proc_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Proc_Dte__R_Field_6", "REDEFINE", pnd_Curr_Proc_Dte);
        pnd_Curr_Proc_Dte_Pnd_Yyyy = pnd_Curr_Proc_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Curr_Proc_Dte_Pnd_Mm = pnd_Curr_Proc_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Curr_Proc_Dte_Pnd_Dd = pnd_Curr_Proc_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Proc_Dte_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Count = localVariables.newGroupInRecord("pnd_Count", "#COUNT");
        pnd_Count_Pnd_Err_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Err_Cnt", "#ERR-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Comp_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Comp_Cnt", "#COMP-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Count_Pnd_Rec_Cnt = pnd_Count.newFieldInGroup("pnd_Count_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Iaxfr_Units_A = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Units_A", "#IAXFR-UNITS-A", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            20));
        pnd_Iaxfr_Units_M = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Units_M", "#IAXFR-UNITS-M", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            20));
        pnd_Reason1 = localVariables.newFieldInRecord("pnd_Reason1", "#REASON1", FieldType.STRING, 18);
        pnd_Reason2 = localVariables.newFieldInRecord("pnd_Reason2", "#REASON2", FieldType.STRING, 18);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 2);
        pnd_Cis_No_Found_Sw = localVariables.newFieldInRecord("pnd_Cis_No_Found_Sw", "#CIS-NO-FOUND-SW", FieldType.STRING, 1);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 2);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        cntrct_Issue_Dte_Dd_1 = localVariables.newFieldInRecord("cntrct_Issue_Dte_Dd_1", "CNTRCT-ISSUE-DTE-DD-1", FieldType.STRING, 2);
        pnd_Audit_Cntrct = localVariables.newFieldInRecord("pnd_Audit_Cntrct", "#AUDIT-CNTRCT", FieldType.STRING, 10);
        pnd_Audit_Payee = localVariables.newFieldInRecord("pnd_Audit_Payee", "#AUDIT-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cis_From_Iss_Date = localVariables.newFieldInRecord("pnd_Cis_From_Iss_Date", "#CIS-FROM-ISS-DATE", FieldType.STRING, 10);

        pnd_Cis_From_Iss_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Cis_From_Iss_Date__R_Field_7", "REDEFINE", pnd_Cis_From_Iss_Date);
        pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Mm = pnd_Cis_From_Iss_Date__R_Field_7.newFieldInGroup("pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Mm", "#CIS-FROM-ISS-MM", 
            FieldType.STRING, 2);
        pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_8 = pnd_Cis_From_Iss_Date__R_Field_7.newFieldInGroup("pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_8", "#CIS-FILLER-8", 
            FieldType.STRING, 1);
        pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Dd = pnd_Cis_From_Iss_Date__R_Field_7.newFieldInGroup("pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Dd", "#CIS-FROM-ISS-DD", 
            FieldType.STRING, 2);
        pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_9 = pnd_Cis_From_Iss_Date__R_Field_7.newFieldInGroup("pnd_Cis_From_Iss_Date_Pnd_Cis_Filler_9", "#CIS-FILLER-9", 
            FieldType.STRING, 1);
        pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Ccyy = pnd_Cis_From_Iss_Date__R_Field_7.newFieldInGroup("pnd_Cis_From_Iss_Date_Pnd_Cis_From_Iss_Ccyy", 
            "#CIS-FROM-ISS-CCYY", FieldType.STRING, 4);
        pnd_Compress_Date_1 = localVariables.newFieldInRecord("pnd_Compress_Date_1", "#COMPRESS-DATE-1", FieldType.STRING, 8);
        pnd_Compress_Date_2 = localVariables.newFieldInRecord("pnd_Compress_Date_2", "#COMPRESS-DATE-2", FieldType.STRING, 8);
        pnd_Cis_Issue_Date = localVariables.newFieldInRecord("pnd_Cis_Issue_Date", "#CIS-ISSUE-DATE", FieldType.DATE);
        pnd_Cis_Log_Date = localVariables.newFieldInRecord("pnd_Cis_Log_Date", "#CIS-LOG-DATE", FieldType.STRING, 10);
        pnd_Cis_Frst_Annt_Ssn = localVariables.newFieldInRecord("pnd_Cis_Frst_Annt_Ssn", "#CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 9);

        pnd_Cis_Frst_Annt_Ssn__R_Field_8 = localVariables.newGroupInRecord("pnd_Cis_Frst_Annt_Ssn__R_Field_8", "REDEFINE", pnd_Cis_Frst_Annt_Ssn);
        pnd_Cis_Frst_Annt_Ssn_Pnd_Cis_Frst_Annt_Ssn_A = pnd_Cis_Frst_Annt_Ssn__R_Field_8.newFieldInGroup("pnd_Cis_Frst_Annt_Ssn_Pnd_Cis_Frst_Annt_Ssn_A", 
            "#CIS-FRST-ANNT-SSN-A", FieldType.STRING, 9);
        pnd_Cis_Frst_Annt_Dob = localVariables.newFieldInRecord("pnd_Cis_Frst_Annt_Dob", "#CIS-FRST-ANNT-DOB", FieldType.STRING, 8);

        pnd_Cis_Frst_Annt_Dob__R_Field_9 = localVariables.newGroupInRecord("pnd_Cis_Frst_Annt_Dob__R_Field_9", "REDEFINE", pnd_Cis_Frst_Annt_Dob);
        pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Cc = pnd_Cis_Frst_Annt_Dob__R_Field_9.newFieldInGroup("pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Cc", 
            "#CIS-FRST-ANNT-DOB-CC", FieldType.STRING, 2);
        pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Yy = pnd_Cis_Frst_Annt_Dob__R_Field_9.newFieldInGroup("pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Annt_Dob_Yy", 
            "#CIS-FRST-ANNT-DOB-YY", FieldType.STRING, 2);
        pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Mmdd = pnd_Cis_Frst_Annt_Dob__R_Field_9.newFieldInGroup("pnd_Cis_Frst_Annt_Dob_Pnd_Cis_Frst_Mmdd", "#CIS-FRST-MMDD", 
            FieldType.STRING, 4);
        pnd_Cis_Scnd_Annt_Dob = localVariables.newFieldInRecord("pnd_Cis_Scnd_Annt_Dob", "#CIS-SCND-ANNT-DOB", FieldType.STRING, 8);

        pnd_Cis_Scnd_Annt_Dob__R_Field_10 = localVariables.newGroupInRecord("pnd_Cis_Scnd_Annt_Dob__R_Field_10", "REDEFINE", pnd_Cis_Scnd_Annt_Dob);
        pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Cc = pnd_Cis_Scnd_Annt_Dob__R_Field_10.newFieldInGroup("pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Cc", 
            "#CIS-SCND-ANNT-DOB-CC", FieldType.STRING, 2);
        pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Yy = pnd_Cis_Scnd_Annt_Dob__R_Field_10.newFieldInGroup("pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Annt_Dob_Yy", 
            "#CIS-SCND-ANNT-DOB-YY", FieldType.STRING, 2);
        pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Mmdd = pnd_Cis_Scnd_Annt_Dob__R_Field_10.newFieldInGroup("pnd_Cis_Scnd_Annt_Dob_Pnd_Cis_Scnd_Mmdd", "#CIS-SCND-MMDD", 
            FieldType.STRING, 4);
        pnd_Cis_Scnd_Annt_Ssn = localVariables.newFieldInRecord("pnd_Cis_Scnd_Annt_Ssn", "#CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 9);

        pnd_Cis_Scnd_Annt_Ssn__R_Field_11 = localVariables.newGroupInRecord("pnd_Cis_Scnd_Annt_Ssn__R_Field_11", "REDEFINE", pnd_Cis_Scnd_Annt_Ssn);
        pnd_Cis_Scnd_Annt_Ssn_Pnd_Cis_Scnd_Annt_Ssn_A = pnd_Cis_Scnd_Annt_Ssn__R_Field_11.newFieldInGroup("pnd_Cis_Scnd_Annt_Ssn_Pnd_Cis_Scnd_Annt_Ssn_A", 
            "#CIS-SCND-ANNT-SSN-A", FieldType.STRING, 9);
        pnd_Date_A8 = localVariables.newFieldInRecord("pnd_Date_A8", "#DATE-A8", FieldType.STRING, 8);

        pnd_Date_A8__R_Field_12 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_12", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_N8 = pnd_Date_A8__R_Field_12.newFieldInGroup("pnd_Date_A8_Pnd_Date_N8", "#DATE-N8", FieldType.NUMERIC, 8);

        pnd_Date_A8__R_Field_13 = pnd_Date_A8__R_Field_12.newGroupInGroup("pnd_Date_A8__R_Field_13", "REDEFINE", pnd_Date_A8_Pnd_Date_N8);
        pnd_Date_A8_Pnd_Date_Yyyymm = pnd_Date_A8__R_Field_13.newFieldInGroup("pnd_Date_A8_Pnd_Date_Yyyymm", "#DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Date_A8_Pnd_Date_Dd = pnd_Date_A8__R_Field_13.newFieldInGroup("pnd_Date_A8_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        pnd_Date_A8__R_Field_14 = pnd_Date_A8__R_Field_12.newGroupInGroup("pnd_Date_A8__R_Field_14", "REDEFINE", pnd_Date_A8_Pnd_Date_N8);
        pnd_Date_A8_Pnd_Date_Yyyy = pnd_Date_A8__R_Field_14.newFieldInGroup("pnd_Date_A8_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Date_A8_Pnd_Date_Mm = pnd_Date_A8__R_Field_14.newFieldInGroup("pnd_Date_A8_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);

        pnd_Date_A8__R_Field_15 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_15", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_A8_Ccyy = pnd_Date_A8__R_Field_15.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Ccyy", "#DATE-A8-CCYY", FieldType.STRING, 4);
        pnd_Date_A8_Pnd_Date_A8_Mm = pnd_Date_A8__R_Field_15.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Mm", "#DATE-A8-MM", FieldType.STRING, 2);
        pnd_Date_A8_Pnd_Date_A8_Dd = pnd_Date_A8__R_Field_15.newFieldInGroup("pnd_Date_A8_Pnd_Date_A8_Dd", "#DATE-A8-DD", FieldType.NUMERIC, 2);

        pnd_Date_A8__R_Field_16 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_16", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Fill = pnd_Date_A8__R_Field_16.newFieldInGroup("pnd_Date_A8_Fill", "FILL", FieldType.STRING, 2);

        pnd_Date_A8_Date_P = pnd_Date_A8__R_Field_16.newGroupInGroup("pnd_Date_A8_Date_P", "DATE-P");
        pnd_Date_A8_Pnd_Date_A_Yy = pnd_Date_A8_Date_P.newFieldInGroup("pnd_Date_A8_Pnd_Date_A_Yy", "#DATE-A-YY", FieldType.STRING, 2);
        pnd_Date_A8_Pnd_Date_A_Mm = pnd_Date_A8_Date_P.newFieldInGroup("pnd_Date_A8_Pnd_Date_A_Mm", "#DATE-A-MM", FieldType.STRING, 2);
        pnd_Date_A8_Pnd_Date_A_Dd = pnd_Date_A8_Date_P.newFieldInGroup("pnd_Date_A8_Pnd_Date_A_Dd", "#DATE-A-DD", FieldType.NUMERIC, 2);
        pnd_Orig_Issue = localVariables.newFieldInRecord("pnd_Orig_Issue", "#ORIG-ISSUE", FieldType.STRING, 3);
        pnd_Orig_Issue_Re = localVariables.newFieldInRecord("pnd_Orig_Issue_Re", "#ORIG-ISSUE-RE", FieldType.STRING, 3);
        pnd_Issue_Date_D = localVariables.newFieldInRecord("pnd_Issue_Date_D", "#ISSUE-DATE-D", FieldType.DATE);
        pnd_Issue_Date_A8 = localVariables.newFieldInRecord("pnd_Issue_Date_A8", "#ISSUE-DATE-A8", FieldType.STRING, 8);

        pnd_Issue_Date_A8__R_Field_17 = localVariables.newGroupInRecord("pnd_Issue_Date_A8__R_Field_17", "REDEFINE", pnd_Issue_Date_A8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_N8 = pnd_Issue_Date_A8__R_Field_17.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_N8", "#ISSUE-DATE-N8", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A8__R_Field_18 = pnd_Issue_Date_A8__R_Field_17.newGroupInGroup("pnd_Issue_Date_A8__R_Field_18", "REDEFINE", pnd_Issue_Date_A8_Pnd_Issue_Date_N8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm = pnd_Issue_Date_A8__R_Field_18.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyymm", "#ISSUE-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Dd = pnd_Issue_Date_A8__R_Field_18.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Issue_Date_A8__R_Field_19 = pnd_Issue_Date_A8__R_Field_17.newGroupInGroup("pnd_Issue_Date_A8__R_Field_19", "REDEFINE", pnd_Issue_Date_A8_Pnd_Issue_Date_N8);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A8__R_Field_19.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A8_Pnd_Issue_Date_Mm = pnd_Issue_Date_A8__R_Field_19.newFieldInGroup("pnd_Issue_Date_A8_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Cis_Frst_Dob = localVariables.newGroupInRecord("pnd_Cis_Frst_Dob", "#CIS-FRST-DOB");
        pnd_Cis_Frst_Dob_Pnd_Cis_Frst_Yy_1 = pnd_Cis_Frst_Dob.newFieldInGroup("pnd_Cis_Frst_Dob_Pnd_Cis_Frst_Yy_1", "#CIS-FRST-YY-1", FieldType.STRING, 
            2);
        pnd_Cis_Frst_Dob_Pnd_Cis_Rrs_Mmdd_1 = pnd_Cis_Frst_Dob.newFieldInGroup("pnd_Cis_Frst_Dob_Pnd_Cis_Rrs_Mmdd_1", "#CIS-RRS-MMDD-1", FieldType.STRING, 
            4);

        pnd_Cis_Frst_Dob__R_Field_20 = localVariables.newGroupInRecord("pnd_Cis_Frst_Dob__R_Field_20", "REDEFINE", pnd_Cis_Frst_Dob);
        pnd_Cis_Frst_Dob_Pnd_Cis_Dob_F = pnd_Cis_Frst_Dob__R_Field_20.newFieldInGroup("pnd_Cis_Frst_Dob_Pnd_Cis_Dob_F", "#CIS-DOB-F", FieldType.STRING, 
            6);

        pnd_Cis_Scnd_Dob = localVariables.newGroupInRecord("pnd_Cis_Scnd_Dob", "#CIS-SCND-DOB");
        pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Yy_1 = pnd_Cis_Scnd_Dob.newFieldInGroup("pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Yy_1", "#CIS-SCND-YY-1", FieldType.STRING, 
            2);
        pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Mmdd_1 = pnd_Cis_Scnd_Dob.newFieldInGroup("pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Mmdd_1", "#CIS-SCND-MMDD-1", FieldType.STRING, 
            4);

        pnd_Cis_Scnd_Dob__R_Field_21 = localVariables.newGroupInRecord("pnd_Cis_Scnd_Dob__R_Field_21", "REDEFINE", pnd_Cis_Scnd_Dob);
        pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Dob_1 = pnd_Cis_Scnd_Dob__R_Field_21.newFieldInGroup("pnd_Cis_Scnd_Dob_Pnd_Cis_Scnd_Dob_1", "#CIS-SCND-DOB-1", FieldType.STRING, 
            6);

        print_Line = localVariables.newGroupInRecord("print_Line", "PRINT-LINE");
        print_Line_Ppcn = print_Line.newFieldInGroup("print_Line_Ppcn", "PPCN", FieldType.STRING, 8);
        print_Line_Filler = print_Line.newFieldInGroup("print_Line_Filler", "FILLER", FieldType.STRING, 5);
        print_Line_Payee_Cd = print_Line.newFieldInGroup("print_Line_Payee_Cd", "PAYEE-CD", FieldType.STRING, 2);
        print_Line_Fillr = print_Line.newFieldInGroup("print_Line_Fillr", "FILLR", FieldType.STRING, 4);
        print_Line_Mode_Code = print_Line.newFieldInGroup("print_Line_Mode_Code", "MODE-CODE", FieldType.STRING, 4);
        print_Line_Filler1 = print_Line.newFieldInGroup("print_Line_Filler1", "FILLER1", FieldType.STRING, 5);
        print_Line_Fund = print_Line.newFieldInGroup("print_Line_Fund", "FUND", FieldType.STRING, 1);
        print_Line_Filler2 = print_Line.newFieldInGroup("print_Line_Filler2", "FILLER2", FieldType.STRING, 1);
        print_Line_Ia_Units = print_Line.newFieldInGroup("print_Line_Ia_Units", "IA-UNITS", FieldType.NUMERIC, 11, 3);
        print_Line_Filler3 = print_Line.newFieldInGroup("print_Line_Filler3", "FILLER3", FieldType.STRING, 3);
        print_Line_Cis_Units = print_Line.newFieldInGroup("print_Line_Cis_Units", "CIS-UNITS", FieldType.NUMERIC, 11, 3);
        print_Line_Filler4 = print_Line.newFieldInGroup("print_Line_Filler4", "FILLER4", FieldType.STRING, 5);
        print_Line_Cis_Option = print_Line.newFieldInGroup("print_Line_Cis_Option", "CIS-OPTION", FieldType.STRING, 4);
        print_Line_Filler5 = print_Line.newFieldInGroup("print_Line_Filler5", "FILLER5", FieldType.STRING, 10);
        print_Line_Cis_Gr_Years = print_Line.newFieldInGroup("print_Line_Cis_Gr_Years", "CIS-GR-YEARS", FieldType.STRING, 4);
        print_Line_Filler6 = print_Line.newFieldInGroup("print_Line_Filler6", "FILLER6", FieldType.STRING, 14);
        print_Line_Cis_Gr_Days = print_Line.newFieldInGroup("print_Line_Cis_Gr_Days", "CIS-GR-DAYS", FieldType.STRING, 4);

        print_Total_Line_Annual = localVariables.newGroupInRecord("print_Total_Line_Annual", "PRINT-TOTAL-LINE-ANNUAL");
        print_Total_Line_Annual_Fund_Annual = print_Total_Line_Annual.newFieldArrayInGroup("print_Total_Line_Annual_Fund_Annual", "FUND-ANNUAL", FieldType.STRING, 
            8, new DbsArrayController(1, 5));
        print_Total_Line_Annual_Ia_Units_Annual_Tot = print_Total_Line_Annual.newFieldArrayInGroup("print_Total_Line_Annual_Ia_Units_Annual_Tot", "IA-UNITS-ANNUAL-TOT", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Annual_Cis_Units_Annual_Tot = print_Total_Line_Annual.newFieldArrayInGroup("print_Total_Line_Annual_Cis_Units_Annual_Tot", "CIS-UNITS-ANNUAL-TOT", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Annual_Ia_Units_Annual_Tot_Bypass = print_Total_Line_Annual.newFieldArrayInGroup("print_Total_Line_Annual_Ia_Units_Annual_Tot_Bypass", 
            "IA-UNITS-ANNUAL-TOT-BYPASS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));

        print_Total_Line_Monthly = localVariables.newGroupInRecord("print_Total_Line_Monthly", "PRINT-TOTAL-LINE-MONTHLY");
        print_Total_Line_Monthly_Fund_Mnthly = print_Total_Line_Monthly.newFieldArrayInGroup("print_Total_Line_Monthly_Fund_Mnthly", "FUND-MNTHLY", FieldType.STRING, 
            8, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_Ia_Units_Mnthly_Tot = print_Total_Line_Monthly.newFieldArrayInGroup("print_Total_Line_Monthly_Ia_Units_Mnthly_Tot", "IA-UNITS-MNTHLY-TOT", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_Cis_Units_Mnthly_Tot = print_Total_Line_Monthly.newFieldArrayInGroup("print_Total_Line_Monthly_Cis_Units_Mnthly_Tot", 
            "CIS-UNITS-MNTHLY-TOT", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_Ia_Units_Mnthly_Tot_Bypass = print_Total_Line_Monthly.newFieldArrayInGroup("print_Total_Line_Monthly_Ia_Units_Mnthly_Tot_Bypass", 
            "IA-UNITS-MNTHLY-TOT-BYPASS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));

        print_Total_Line_Annual_C = localVariables.newGroupInRecord("print_Total_Line_Annual_C", "PRINT-TOTAL-LINE-ANNUAL-C");
        print_Total_Line_Annual_C_Fund_Annual_C = print_Total_Line_Annual_C.newFieldArrayInGroup("print_Total_Line_Annual_C_Fund_Annual_C", "FUND-ANNUAL-C", 
            FieldType.STRING, 8, new DbsArrayController(1, 5));
        print_Total_Line_Annual_C_Ia_Units_Annual_Tot_C = print_Total_Line_Annual_C.newFieldArrayInGroup("print_Total_Line_Annual_C_Ia_Units_Annual_Tot_C", 
            "IA-UNITS-ANNUAL-TOT-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Annual_C_Cis_Units_Annual_Tot_C = print_Total_Line_Annual_C.newFieldArrayInGroup("print_Total_Line_Annual_C_Cis_Units_Annual_Tot_C", 
            "CIS-UNITS-ANNUAL-TOT-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Annual_C_Ia_Units_Annual_Tot_Bypass_C = print_Total_Line_Annual_C.newFieldArrayInGroup("print_Total_Line_Annual_C_Ia_Units_Annual_Tot_Bypass_C", 
            "IA-UNITS-ANNUAL-TOT-BYPASS-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));

        print_Total_Line_Monthly_C = localVariables.newGroupInRecord("print_Total_Line_Monthly_C", "PRINT-TOTAL-LINE-MONTHLY-C");
        print_Total_Line_Monthly_C_Fund_Mnthly_C = print_Total_Line_Monthly_C.newFieldArrayInGroup("print_Total_Line_Monthly_C_Fund_Mnthly_C", "FUND-MNTHLY-C", 
            FieldType.STRING, 8, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_C = print_Total_Line_Monthly_C.newFieldArrayInGroup("print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_C", 
            "IA-UNITS-MNTHLY-TOT-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_C_Cis_Units_Mnthly_Tot_C = print_Total_Line_Monthly_C.newFieldArrayInGroup("print_Total_Line_Monthly_C_Cis_Units_Mnthly_Tot_C", 
            "CIS-UNITS-MNTHLY-TOT-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_Bypass_C = print_Total_Line_Monthly_C.newFieldArrayInGroup("print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_Bypass_C", 
            "IA-UNITS-MNTHLY-TOT-BYPASS-C", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_Iaxfr_Unit = localVariables.newFieldArrayInRecord("pnd_Iaxfr_Unit", "#IAXFR-UNIT", FieldType.NUMERIC, 8, 3, new DbsArrayController(1, 20));
        pnd_Cis_Unit_A = localVariables.newFieldArrayInRecord("pnd_Cis_Unit_A", "#CIS-UNIT-A", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20));
        pnd_Cis_Unit_M = localVariables.newFieldArrayInRecord("pnd_Cis_Unit_M", "#CIS-UNIT-M", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20));
        pnd_Cor_Unique_Id = localVariables.newFieldInRecord("pnd_Cor_Unique_Id", "#COR-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Numeric_Ste = localVariables.newFieldInRecord("pnd_Numeric_Ste", "#NUMERIC-STE", FieldType.STRING, 2);
        pnd_Alpha_Ste = localVariables.newFieldInRecord("pnd_Alpha_Ste", "#ALPHA-STE", FieldType.STRING, 2);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.NUMERIC, 2);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.DATE);
        pnd_Address_Array = localVariables.newFieldInRecord("pnd_Address_Array", "#ADDRESS-ARRAY", FieldType.NUMERIC, 1);
        pnd_Err_Reason = localVariables.newFieldInRecord("pnd_Err_Reason", "#ERR-REASON", FieldType.STRING, 60);
        pnd_Comp_Reason = localVariables.newFieldInRecord("pnd_Comp_Reason", "#COMP-REASON", FieldType.STRING, 60);
        pnd_To_Company = localVariables.newFieldArrayInRecord("pnd_To_Company", "#TO-COMPANY", FieldType.STRING, 4, new DbsArrayController(1, 2));
        pnd_Fr_Company = localVariables.newFieldInRecord("pnd_Fr_Company", "#FR-COMPANY", FieldType.STRING, 4);
        pnd_From_Cref = localVariables.newFieldInRecord("pnd_From_Cref", "#FROM-CREF", FieldType.STRING, 1);
        pnd_Process_Date_A = localVariables.newFieldInRecord("pnd_Process_Date_A", "#PROCESS-DATE-A", FieldType.STRING, 8);

        pnd_Process_Date_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Process_Date_A__R_Field_22", "REDEFINE", pnd_Process_Date_A);
        pnd_Process_Date_A_Pnd_Process_Date_N = pnd_Process_Date_A__R_Field_22.newFieldInGroup("pnd_Process_Date_A_Pnd_Process_Date_N", "#PROCESS-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 34);
        pnd_Nbr_Acct = localVariables.newFieldInRecord("pnd_Nbr_Acct", "#NBR-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Acct_Std_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Alpha_Cde", "#ACCT-STD-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Acct_Nme_5 = localVariables.newFieldArrayInRecord("pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 6, new DbsArrayController(1, 20));
        pnd_Acct_Tckr = localVariables.newFieldArrayInRecord("pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Acct_Effctve_Dte = localVariables.newFieldArrayInRecord("pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            20));
        pnd_Acct_Unit_Rte_Ind = localVariables.newFieldArrayInRecord("pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 
            20));
        pnd_Acct_Std_Nm_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Nm_Cde", "#ACCT-STD-NM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pnd_Acct_Rpt_Seq = localVariables.newFieldArrayInRecord("pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw.reset();
        vw_cis_Updt.reset();

        ldaNazl6004.initializeValues();
        ldaIatl406.initializeValues();
        ldaIaal200b.initializeValues();
        ldaIaal200a.initializeValues();
        ldaIaal204a.initializeValues();

        localVariables.reset();
        pnd_Total_Sw.setInitialValue("Y");
        pnd_Dod_1_Sw.setInitialValue("N");
        pnd_Dod_2_Sw.setInitialValue("N");
        pnd_Funds.setInitialValue("CWSLE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp408() throws Exception
    {
        super("Iatp408");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        //* *======================= AT TOP OF PAGE ===============================
        //*                          START OF PROGRAM                                                                                                                     //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        //* *======================================================================
        //*  01/2014 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATES
        sub_Get_Control_Dates();
        if (condition(Global.isEscape())) {return;}
        //*  REA0614
                                                                                                                                                                          //Natural: PERFORM GET-FUND-INFO
        sub_Get_Fund_Info();
        if (condition(Global.isEscape())) {return;}
        vw_iaa_Trnsfr_Sw.startDatabaseRead                                                                                                                                //Natural: READ IAA-TRNSFR-SW BY IA-SUPER-DE-02 STARTING FROM '1M1 '
        (
        "RD1",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", "1M1 ", WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        RD1:
        while (condition(vw_iaa_Trnsfr_Sw.readNextRow("RD1")))
        {
            //*  ACCEPT IF IA-FRM-CNTRCT = 'Y4154966'
            pnd_Rqst_Id.setValue(iaa_Trnsfr_Sw_Rqst_Id);                                                                                                                  //Natural: ASSIGN #RQST-ID := IAA-TRNSFR-SW.RQST-ID
            if (condition(iaa_Trnsfr_Sw_Xfr_Stts_Cde.notEquals("M1") || iaa_Trnsfr_Sw_Rcrd_Type_Cde.notEquals("1")))                                                      //Natural: IF XFR-STTS-CDE NE 'M1' OR RCRD-TYPE-CDE NE '1'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal204a.getVw_iaa_Xfr_Audit_View().startDatabaseFind                                                                                                      //Natural: FIND IAA-XFR-AUDIT-VIEW WITH RQST-ID = #RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", pnd_Rqst_Id, WcType.WITH) }
            );
            FIND01:
            while (condition(ldaIaal204a.getVw_iaa_Xfr_Audit_View().readNextRow("FIND01", true)))
            {
                ldaIaal204a.getVw_iaa_Xfr_Audit_View().setIfNotFoundControlFlag(false);
                if (condition(ldaIaal204a.getVw_iaa_Xfr_Audit_View().getAstCOUNTER().equals(0)))                                                                          //Natural: IF NO RECORD FOUND
                {
                    pnd_Err_Reason.setValue("COULD NOT FIND AUDIT REC");                                                                                                  //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND AUDIT REC'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                    sub_Pnd_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Found.reset();                                                                                                                                    //Natural: RESET #FOUND
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*   ACCEPT IF  RQST-ID = '10950478000899819990721085308'
                pnd_Count_Pe.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Count_Castiaxfr_Calc_To_Acct_Data());                                                             //Natural: ASSIGN #COUNT-PE := C*IAXFR-CALC-TO-ACCT-DATA
                if (condition(!(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_New_Issue_Ind().equals("C") && ldaIaal204a.getIaa_Xfr_Audit_View_Rqst_Xfr_Type().notEquals(" ")))) //Natural: ACCEPT IF IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND EQ 'C' AND RQST-XFR-TYPE NE ' '
                {
                    continue;
                }
                pnd_Process_Date_A.setValueEdited(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Cycle_Dte(),new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED IAXFR-CYCLE-DTE ( EM = YYYYMMDD ) TO #PROCESS-DATE-A
                //*  012809
                pnd_L_Pnd_Error.reset();                                                                                                                                  //Natural: RESET #ERROR
                                                                                                                                                                          //Natural: PERFORM #GET-CIS-REC
                sub_Pnd_Get_Cis_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Cis_No_Found_Sw.equals("Y")))                                                                                                           //Natural: IF #CIS-NO-FOUND-SW = 'Y'
                {
                    pnd_Cis_No_Found_Sw.reset();                                                                                                                          //Natural: RESET #CIS-NO-FOUND-SW
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                G1:                                                                                                                                                       //Natural: GET CIS-PRTCPNT-FILE #ISN
                ldaIatl406.getVw_cis_Prtcpnt_File().readByID(pnd_Isn.getLong(), "G1");
                G2:                                                                                                                                                       //Natural: GET IAA-TRNSFR-SW *ISN ( RD1. )
                vw_iaa_Trnsfr_Sw.readByID(vw_iaa_Trnsfr_Sw.getAstISN("Find01"), "G2");
                                                                                                                                                                          //Natural: PERFORM PROCESS-COR-PAYEE-01
                sub_Process_Cor_Payee_01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-IAA-CNTRCT
                sub_Process_Iaa_Cntrct();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                //*  7/07
                if (condition(((((((((((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4))                 //Natural: IF CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR = 10 OR = 11 OR = 12 OR = 13 OR = 14 OR = 15 OR = 16 OR = 17 OR CNTRCT-OPTN-CDE = 54 THRU 57
                    || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) 
                    || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13)) 
                    || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16)) 
                    || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))))
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COR-PAYEE-02
                    sub_Process_Cor_Payee_02();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();                                                                                      //Natural: RESET CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB CIS-SCND-ANNT-SSN
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-CIS-PRTCPNT-FILE
                sub_Fill_Cis_Prtcpnt_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-PRTCPNT-ROLE
                sub_Process_Prtcpnt_Role();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-GRD-STD-GRNTED-AMT
                sub_Fill_Grd_Std_Grnted_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COMPUTE-ANNTY-START-DATE
                sub_Compute_Annty_Start_Date();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Curr_Proc_Dte);                                    //Natural: MOVE EDITED #CURR-PROC-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
                                                                                                                                                                          //Natural: PERFORM FILL-ADDRESS-INFO
                sub_Fill_Address_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  012809 START
                if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                              //Natural: IF #ERROR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  012809 END
                }                                                                                                                                                         //Natural: END-IF
                ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Rqst_Id());                                                   //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY := IAA-XFR-AUDIT-VIEW.RQST-ID
                //*  MOVE THE UPDATE TO CIS/XFR BEFORE CALLING THE CIS ROUTINE
                ldaIatl406.getVw_cis_Prtcpnt_File().updateDBRow("G1");                                                                                                    //Natural: UPDATE ( G1. )
                iaa_Trnsfr_Sw_Xfr_Stts_Cde.setValue("M2");                                                                                                                //Natural: ASSIGN XFR-STTS-CDE := 'M2'
                vw_iaa_Trnsfr_Sw.updateDBRow("G2");                                                                                                                       //Natural: UPDATE ( G2. )
                                                                                                                                                                          //Natural: PERFORM UPDATE-CIS-PRTCPNT-FILE
                sub_Update_Cis_Prtcpnt_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   UPDATE (G1.) /*
                //*    XFR-STTS-CDE := 'M2'   /*THIS CODE MOVED UP - SEE COMMENTS
                //*    UPDATE (G2.)           /*
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   END-REPEAT /* RPT1.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  012809
            if (condition(pnd_L_Pnd_Error.getBoolean()))                                                                                                                  //Natural: IF #ERROR
            {
                //*  012809
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                //*  012809
            }                                                                                                                                                             //Natural: END-IF
            //*  IAA-XFR-AUDIT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  01/2014 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS-C
        sub_Write_Totals_C();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Count_Pnd_Err_Cnt.equals(getZero())))                                                                                                           //Natural: IF #ERR-CNT = 0
        {
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"****************************************************** ");                                                        //Natural: WRITE ( 2 ) '****************************************************** '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 2 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 2 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"*        NO ERRORS DETECTED IN THIS RUN              * ");                                                        //Natural: WRITE ( 2 ) '*        NO ERRORS DETECTED IN THIS RUN              * '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 2 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"*                                                    * ");                                                        //Natural: WRITE ( 2 ) '*                                                    * '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"****************************************************** ");                                                        //Natural: WRITE ( 2 ) '****************************************************** '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *======================================================================
        //*                          START OF SUBROUTINES
        //* *======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATES
        //* ***********************************************************************
        //*  GET TODAY's date
        //* ***************************************************REA0614*************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-INFO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CONTROL-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-CIS-REC
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-GRD-STD-GRNTED-AMT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COR-PAYEE-01
        //* ***********************************************************************
        //*  GET THE COR UNIQUE ID FOR PAYEE 01
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-PAYEE-CDE := '01'
        //*  10/2014 - START
        //*  #COR-SUPER-PIN-RCDTYPE.#PH-UNIQUE-ID-NBR  := #COR-UNIQUE-ID
        //*  IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 2
        //*    #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 1
        //*  ELSE IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 4
        //*      #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 3
        //*    END-IF
        //*  END-IF
        //*  #I-PIN := #COR-UNIQUE-ID
        //*  10/2014 - START
        //*    CIS-FRST-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
        //*    CIS-FRST-ANNT-PRFX     := PH-PRFX-NME
        //*    CIS-FRST-ANNT-FRST-NME := PH-FIRST-NME
        //*    CIS-FRST-ANNT-MID-NME  := PH-MDDLE-NME
        //*    CIS-FRST-ANNT-LST-NME  := PH-LAST-NME
        //*    CIS-FRST-ANNT-SFFX     := PH-SFFX-NME
        //*    CIS-FRST-ANNT-SSN      := #MDMA100.#O-SSN
        //*  10/2014 - END
        //*    #DATE-N8 := PH-DOB-DTE
        //*    CIS-FRST-ANNT-SEX-CDE  := PH-SEX-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-ERROR-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* ***********************************************************************
        //*  CIS-PRTCPNT-FILE-VIEW
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* ***********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS-C
        //* ***********************************************************************
        //*  #TOTAL-SW := 'N'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COR-PAYEE-02
        //* ***********************************************************************
        //*  GET THE COR UNIQUE ID FOR PAYEE 02
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-PAYEE-CDE := '02'
        //*  10/2014 - START
        //*  #COR-SUPER-PIN-RCDTYPE.#PH-UNIQUE-ID-NBR  := #COR-UNIQUE-ID
        //*  IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 2
        //*    #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 1
        //*  ELSE IF COR-XREF-CNTRCT.PH-RCD-TYPE-CDE = 4
        //*      #COR-SUPER-PIN-RCDTYPE.#PH-RCD-TYPE-CDE  := 3
        //*    END-IF
        //*  END-IF
        //*  #I-PIN := #COR-UNIQUE-ID
        //*  10/2014 - START
        //*    CIS-SCND-ANNT-SSN      := PH-SOCIAL-SECURITY-NO
        //*    CIS-SCND-ANNT-PRFX     := PH-PRFX-NME
        //*    CIS-SCND-ANNT-FRST-NME := PH-FIRST-NME
        //*    CIS-SCND-ANNT-MID-NME  := PH-MDDLE-NME
        //*    CIS-SCND-ANNT-LST-NME  := PH-LAST-NME
        //*    CIS-SCND-ANNT-SFFX     := PH-SFFX-NME
        //*    CIS-SCND-ANNT-SSN      := #MDMA100.#O-SSN
        //*  10/2014 - END
        //*    #DATE-N8 := PH-DOB-DTE
        //*    CIS-SCND-ANNT-SEX-CDE  := PH-SEX-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        //*  10/2014 - START
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#CNTRCT-NBR       := IAXFR-FROM-PPCN-NBR
        //* *#COR-SUPER-CNTRCT-PAYEE-PIN.#PH-UNIQUE-ID-NBR := 0
        //* *1#COR-SUPER-CNTRCT-PAYEE-PIN.#PH-RCD-TYPE-CDE  := 0
        //* *READ(1) COR-XREF-CNTRCT BY COR-SUPER-CNTRCT-PAYEE-PIN
        //*     STARTING FROM #COR-SUPER-CNTRCT-PAYEE-PIN
        //*   IF CNTRCT-PAYEE-CDE EQ #CNTRCT-PAYEE-CDE AND
        //*       CNTRCT-NBR EQ #CNTRCT-NBR
        //*     #COR-UNIQUE-ID := COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR
        //*   ELSE
        //*     #COR-UNIQUE-ID := 0
        //*   END-IF
        //* *END-READ
        //* *#COR-UNIQUE-ID := #MDMA210.#O-PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COR-XREF-PH
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IAA-CNTRCT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-CIS-PRTCPNT-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PRTCPNT-ROLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-ADDRESS-INFO
        //* ***********************************************************************
        //*  ==============================================
        //*  FILL PARTICIPANT CORRESPONDENCE ADDRESS POS(1)
        //*  ==============================================
        //*  10/2014 - START
        //* *PERFORM GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* *IF NAS-NAME-ADDRESS.PERMANENT-ADDRSS-IND = 'P'
        //*   RESET #VACATION-ADDRESS
        //* *ELSE
        //*   PERFORM GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //*   #VACATION-ADDRESS  := TRUE
        //* *END-IF
        //* *#I-PIN := #MDMA210.#O-PIN
        //*  10/2014 - END
        //*  =============================================
        //*  FILL PARTICIPANT CHECK MAILING ADDRESS POS(2)
        //*  =============================================
        //*  10/2014 - START
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *#CAD-CNTRCT-TYPE-CDE           := 'I'
        //* *#CAD-CORRESPONDENCE-ADDRSS-IND := 'O'
        //* *#CAD-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#CAD-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-NAME-ADDRESS WITH
        //*     CNTRCT-TYPE-CORR-CNTRCT-KEY = #CNTRCT-TYPE-CORR-CNTRCT-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for correspondence'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*    PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-NAME-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *#MAD-CNTRCT-TYPE-CDE           := 'I'
        //* *#MAD-CHECK-MAILING-ADDRSS-IND  := 'K'
        //* *#MAD-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#MAD-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-NAME-ADDRESS WITH
        //*     CNTRCT-TYPE-CHECK-CNTRCT-KEY = #CNTRCT-TYPE-CHECK-CNTRCT-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for mailing check'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*    PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *#CAD-V-CORRESPONDENCE-ADDRSS-IND :=  'O'
        //* *#CAD-V-CNTRCT-NMBR               := IAXFR-FROM-PPCN-NBR
        //* *#CAD-V-CNTRCT-PAYEE-CDE          := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-VACATION-ADDRESS WITH
        //*     V-CORR-CNTRCT-PAYEE-KEY = #V-CORR-CNTRCT-PAYEE-KEY
        //*   IF NO RECORDS FOUND
        //*     MOVE  'Could not find NAS-VACATION-ADDRESS for correspondence.'
        //*       TO #ERR-REASON
        //*     PERFORM #WRITE-ERROR-REPORT
        //*    PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-VACATION-ADDRESS-CORRESPONDENCE
        //* ***********************************************************************
        //* *DEFINE SUBROUTINE GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //* ***********************************************************************
        //* *#MAD-V-CHECK-MAILING-ADDRSS-IND := 'K'
        //* *#MAD-V-CNTRCT-NMBR              := IAXFR-FROM-PPCN-NBR
        //* *#MAD-V-CNTRCT-PAYEE-CDE         := IAXFR-FROM-PAYEE-CDE
        //* *FIND (1) NAS-VACATION-ADDRESS WITH
        //*     V-CHECK-CNTRCT-PAYEE-KEY = #V-CHECK-CNTRCT-PAYEE-KEY
        //*   IF NO RECORDS FOUND
        //*     #ERR-REASON := 'Could not find NAS-VACATION-ADDRESS to mail check.'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*    PERFORM PROCESS-ERROR
        //*   END-NOREC
        //* *END-FIND
        //* *END-SUBROUTINE /* GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //*  10/2014 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        //*  CIS-ADDRESS-DEST-NAME(#I) :=
        //*    NAS-VACATION-ADDRESS.V-CNTRCT-NAME-FREE
        //*  CIS-CHECKING-SAVING-CD(#I) :=
        //*    NAS-VACATION-ADDRESS.V-CHECKING-SAVING-CDE
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-LNE-1
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-LNE-2
        //*  CIS-ADDRESS-TXT(#I,3) :=
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-LNE-3
        //*  CIS-ADDRESS-TXT(#I,4) :=
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-LNE-4
        //*  CIS-ADDRESS-TXT(#I,5) :=
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-LNE-5
        //*    NAS-VACATION-ADDRESS.V-ADDRSS-ZIP-PLUS-4
        //*  CIS-BANK-PYMNT-ACCT-NMBR(#I) :=
        //*    NAS-VACATION-ADDRESS.V-PH-BANK-PYMNT-ACCT-NMBR
        //*  CIS-BANK-ABA-ACCT-NMBR(#I) :=
        //*    NAS-VACATION-ADDRESS.V-BANK-ABA-ACCT-NMBR
        //*  #ZIP :=NAS-VACATION-ADDRESS.V-ADDRSS-ZIP-PLUS-4
        //*    NAS-NAME-ADDRESS.CHECKING-SAVING-CDE
        //*  CIS-ADDRESS-DEST-NAME(#I) :=
        //*    NAS-NAME-ADDRESS.CNTRCT-NAME-FREE
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-1
        //*  CIS-ADDRESS-TXT(#I,2) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-2
        //*  CIS-ADDRESS-TXT(#I,3) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-3
        //*  CIS-ADDRESS-TXT(#I,4) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-4
        //*  CIS-ADDRESS-TXT(#I,5) :=
        //*    NAS-NAME-ADDRESS.ADDRSS-LNE-5
        //*    NAS-NAME-ADDRESS.ADDRSS-ZIP-PLUS-4
        //*    NAS-NAME-ADDRESS.PH-BANK-PYMNT-ACCT-NMBR
        //*    NAS-NAME-ADDRESS.BANK-ABA-ACCT-NMBR
        //*  #ZIP :=NAS-NAME-ADDRESS.ADDRSS-ZIP-PLUS-4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-100
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-601-603
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-701-706
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MODE-801-812
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CIS-PRTCPNT-FILE
        //* ***********************************************************************
        //* ==============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASS-CONTRACT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASS-CONTRACT-DIFFERENT-DATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DIFFERENT-IAXFR-CYCLE-DTE
        //* =================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-ANNTY-START-DATE
        //*  07/2018 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PARM-VALUES
        //*  07/2018 - END
    }
    private void sub_Get_Control_Dates() throws Exception                                                                                                                 //Natural: GET-CONTROL-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde().setValue("DC");                                                                                                        //Natural: ASSIGN #IAAN299A-IN.CNTRL-CDE := 'DC'
                                                                                                                                                                          //Natural: PERFORM #GET-CONTROL-REC
        sub_Pnd_Get_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte().equals(getZero()) || pdaIaaa299a.getPnd_Iaan299a_Out_Pnd_Return_Code().equals("E")))         //Natural: IF #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE = 0 OR #IAAN299A-OUT.#RETURN-CODE = 'E'
        {
            getReports().write(2, ReportOption.NOTITLE,"CONTROL RECORD 'AA' RETURNED AN INVALID DATE");                                                                   //Natural: WRITE ( 2 ) 'CONTROL RECORD "AA" RETURNED AN INVALID DATE'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"TODAYS DATE   = ",pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                        //Natural: WRITE ( 2 ) 'TODAYS DATE   = ' #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"FATAL ERROR PROGRAM TERMINATED");                                                                                 //Natural: WRITE ( 2 ) 'FATAL ERROR PROGRAM TERMINATED'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Todays_Date.setValue(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                                                             //Natural: ASSIGN #TODAYS-DATE := #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTROL-DATES
    }
    private void sub_Get_Fund_Info() throws Exception                                                                                                                     //Natural: GET-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), pnd_Nbr_Acct, pnd_Acct_Std_Alpha_Cde.getValue("*"), pnd_Acct_Nme_5.getValue("*"), pnd_Acct_Tckr.getValue("*"),  //Natural: CALLNAT 'IATN216' #NBR-ACCT #ACCT-STD-ALPHA-CDE ( * ) #ACCT-NME-5 ( * ) #ACCT-TCKR ( * ) #ACCT-EFFCTVE-DTE ( * ) #ACCT-UNIT-RTE-IND ( * ) #ACCT-STD-NM-CDE ( * ) #ACCT-RPT-SEQ ( * )
            pnd_Acct_Effctve_Dte.getValue("*"), pnd_Acct_Unit_Rte_Ind.getValue("*"), pnd_Acct_Std_Nm_Cde.getValue("*"), pnd_Acct_Rpt_Seq.getValue("*"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Pnd_Get_Control_Rec() throws Exception                                                                                                               //Natural: #GET-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FETCH LATEST CONTROL DATE
        DbsUtil.callnat(Iaan299a.class , getCurrentProcessState(), pdaIaaa299a.getPnd_Iaan299a_In(), pdaIaaa299a.getPnd_Iaan299a_Out(), pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1()); //Natural: CALLNAT 'IAAN299A' #IAAN299A-IN #IAAN299A-OUT #IAA-CNTRL-RCRD-1
        if (condition(Global.isEscape())) return;
        //*  #GET-CONTROL-REC
    }
    private void sub_Pnd_Get_Cis_Rec() throws Exception                                                                                                                   //Natural: #GET-CIS-REC
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl406.getVw_cis_Prtcpnt_File().startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) CIS-PRTCPNT-FILE WITH CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY = IAA-XFR-AUDIT-VIEW.RQST-ID
        (
        "CIS",
        new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", ldaIaal204a.getIaa_Xfr_Audit_View_Rqst_Id(), WcType.WITH) },
        1
        );
        CIS:
        while (condition(ldaIatl406.getVw_cis_Prtcpnt_File().readNextRow("CIS", true)))
        {
            ldaIatl406.getVw_cis_Prtcpnt_File().setIfNotFoundControlFlag(false);
            if (condition(ldaIatl406.getVw_cis_Prtcpnt_File().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("COULD NOT FIND CIS REC");                                                                                                        //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND CIS REC'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Found.reset();                                                                                                                                        //Natural: RESET #FOUND
                pnd_Cis_No_Found_Sw.setValue("Y");                                                                                                                        //Natural: ASSIGN #CIS-NO-FOUND-SW := 'Y'
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Isn.setValue(ldaIatl406.getVw_cis_Prtcpnt_File().getAstISN("CIS"));                                                                                       //Natural: ASSIGN #ISN := *ISN ( CIS. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  #GET-CONTROL-REC
    }
    private void sub_Fill_Grd_Std_Grnted_Amt() throws Exception                                                                                                           //Natural: FILL-GRD-STD-GRNTED-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        cntrct_Dte_D.setValueEdited(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMM"));                                                 //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMM ) TO CNTRCT-DTE-D
        if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().equals(getZero()) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().lessOrEqual(cntrct_Dte_D_Cntrct_Dte_Date))) //Natural: IF CNTRCT-FINAL-PER-PAY-DTE = 0 OR CNTRCT-FINAL-PER-PAY-DTE LE CNTRCT-DTE-DATE
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_End_Dte().reset();                                                                                                   //Natural: RESET CIS-ANNTY-END-DTE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A8_Pnd_Date_Dd.setValue(1);                                                                                                                              //Natural: ASSIGN #DATE-DD := 01
        short decideConditionsMet1937 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100))))
        {
            decideConditionsMet1937++;
                                                                                                                                                                          //Natural: PERFORM MODE-100
            sub_Mode_100();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(601) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(603)))))
        {
            decideConditionsMet1937++;
                                                                                                                                                                          //Natural: PERFORM MODE-601-603
            sub_Mode_601_603();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(701) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(706)))))
        {
            decideConditionsMet1937++;
                                                                                                                                                                          //Natural: PERFORM MODE-701-706
            sub_Mode_701_706();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(801) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(812)))))
        {
            decideConditionsMet1937++;
                                                                                                                                                                          //Natural: PERFORM MODE-801-812
            sub_Mode_801_812();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Issue_Date_A8.setValueEdited(ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi(),new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED CIS-CREF-DOI ( EM = YYYYMMDD ) TO #ISSUE-DATE-A8
        //*  ISSUE DTE < PERIOD END DTE           **
        if (condition(pnd_Issue_Date_A8_Pnd_Issue_Date_N8.less(pnd_Date_A8_Pnd_Date_N8)))                                                                                 //Natural: IF #ISSUE-DATE-N8 LT #DATE-N8
        {
            if (condition(pnd_Date_A8_Pnd_Date_Mm.less(pnd_Issue_Date_A8_Pnd_Issue_Date_Mm)))                                                                             //Natural: IF #DATE-MM LT #ISSUE-DATE-MM
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),      //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = ( #DATE-YYYY - 1 ) - #ISSUE-DATE-YYYY
                    (pnd_Date_A8_Pnd_Date_Yyyy.subtract(1)).subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Date_A8_Pnd_Date_Mm.equals(pnd_Issue_Date_A8_Pnd_Issue_Date_Mm) && pnd_Date_A8_Pnd_Date_A8_Dd.less(pnd_Issue_Date_A8_Pnd_Issue_Date_Dd))) //Natural: IF #DATE-MM = #ISSUE-DATE-MM AND #DATE-A8-DD LT #ISSUE-DATE-DD
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = ( #DATE-YYYY - 1 ) - #ISSUE-DATE-YYYY
                        (pnd_Date_A8_Pnd_Date_Yyyy.subtract(1)).subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs()),  //Natural: COMPUTE CIS-GRNTED-PERIOD-YRS = #DATE-YYYY - #ISSUE-DATE-YYYY
                        pnd_Date_A8_Pnd_Date_Yyyy.subtract(pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Issue_Date_A8_Pnd_Issue_Date_Yyyy.nadd(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs());                                                               //Natural: ADD CIS-GRNTED-PERIOD-YRS TO #ISSUE-DATE-YYYY
        //* *3/5/2000
        if (condition(! (DbsUtil.maskMatches(pnd_Issue_Date_A8,"YYYYMMDD"))))                                                                                             //Natural: IF #ISSUE-DATE-A8 NE MASK ( YYYYMMDD )
        {
            pnd_Issue_Date_A8_Pnd_Issue_Date_Dd.setValue(28);                                                                                                             //Natural: ASSIGN #ISSUE-DATE-DD := 28
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Effctve_Dte());                                                //Natural: ASSIGN CIS-CREF-DOI := IAXFR-EFFCTVE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISS DTE
        pnd_Issue_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Issue_Date_A8);                                                                                //Natural: MOVE EDITED #ISSUE-DATE-A8 TO #ISSUE-DATE-D ( EM = YYYYMMDD )
        //*  GRNTD END DTE
        pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                                                                            //Natural: MOVE EDITED #DATE-A8 TO #DATE-D ( EM = YYYYMMDD )
        //*  CALC THE NUMBER OF DAYS
        ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys()),              //Natural: COMPUTE CIS-GRNTED-PERIOD-DYS = #DATE-D - #ISSUE-DATE-D
            pnd_Date_D.subtract(pnd_Issue_Date_D));
        //*  FILL-GRD-STD-GRNTED-AMT
    }
    //*  10/2014
    private void sub_Process_Cor_Payee_01() throws Exception                                                                                                              //Natural: PROCESS-COR-PAYEE-01
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(1);                                                                                                         //Natural: ASSIGN #I-PAYEE-CODE := 01
                                                                                                                                                                          //Natural: PERFORM READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        sub_Read_Cor_Xref_Cntrct_For_Unique_Id();
        if (condition(Global.isEscape())) {return;}
        pnd_Reason1.reset();                                                                                                                                              //Natural: RESET #REASON1
        //*  #COR-UNIQUE-ID:=  0 /*REMOVE
        if (condition(pnd_Cor_Unique_Id.equals(getZero())))                                                                                                               //Natural: IF #COR-UNIQUE-ID EQ 0
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().reset();                                                                                                   //Natural: RESET CIS-FRST-ANNT-SSN CIS-FRST-ANNT-PRFX CIS-FRST-ANNT-FRST-NME CIS-FRST-ANNT-MID-NME CIS-FRST-ANNT-LST-NME CIS-FRST-ANNT-SFFX CIS-FRST-ANNT-DOB
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().reset();
            pnd_Reason1.setValue("DATA NOT AVAILABLE");                                                                                                                   //Natural: ASSIGN #REASON1 := 'DATA NOT AVAILABLE'
            pnd_Err_Reason.setValue("COULD NOT FIND COR UNIQUE ID. (PAYEE 01)");                                                                                          //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND COR UNIQUE ID. (PAYEE 01)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BYPASS-CONTRACT
            sub_Bypass_Contract();
            if (condition(Global.isEscape())) {return;}
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
            //*  082017
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Cor_Unique_Id);                                                                                        //Natural: ASSIGN #I-PIN-N12 := #COR-UNIQUE-ID
            //*  10/2014 - END
                                                                                                                                                                          //Natural: PERFORM GET-COR-XREF-PH
            sub_Get_Cor_Xref_Ph();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Found.notEquals("Y")))                                                                                                                      //Natural: IF #FOUND NOT = 'Y'
            {
                pnd_Cis_Frst_Annt_Ssn.reset();                                                                                                                            //Natural: RESET #CIS-FRST-ANNT-SSN CIS-FRST-ANNT-PRFX CIS-FRST-ANNT-FRST-NME CIS-FRST-ANNT-MID-NME CIS-FRST-ANNT-LST-NME CIS-FRST-ANNT-SFFX CIS-FRST-ANNT-DOB
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().reset();
                //*  082017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Ssn());                                                       //Natural: ASSIGN CIS-FRST-ANNT-SSN := #MDMA101.#O-SSN
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Prfx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix());                                                   //Natural: ASSIGN CIS-FRST-ANNT-PRFX := #O-PREFIX
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                           //Natural: ASSIGN CIS-FRST-ANNT-FRST-NME := #O-FIRST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Mid_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                           //Natural: ASSIGN CIS-FRST-ANNT-MID-NME := #O-MIDDLE-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                             //Natural: ASSIGN CIS-FRST-ANNT-LST-NME := #O-LAST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sffx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix());                                                   //Natural: ASSIGN CIS-FRST-ANNT-SFFX := #O-SUFFIX
                //*    IF   PH-DOD-DTE > 0
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero()) && ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde().greater(1))) //Natural: IF #O-DATE-OF-DEATH > 0 AND IAXFR-FROM-PAYEE-CDE GT 01
                {
                    pnd_Dod_1_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #DOD-1-SW := 'Y'
                    //*  10/2014
                }                                                                                                                                                         //Natural: END-IF
                pnd_Date_A8_Pnd_Date_N8.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                        //Natural: ASSIGN #DATE-N8 := #O-DATE-OF-BIRTH
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-FRST-ANNT-DOB ( EM = YYYYMMDD )
                //*  10/2014
                pnd_Date_A8_Pnd_Date_N8.reset();                                                                                                                          //Natural: RESET #DATE-N8
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Sex_Cde().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Gender_Code());                                           //Natural: ASSIGN CIS-FRST-ANNT-SEX-CDE := #O-GENDER-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-COR-PAYEE-01
    }
    private void sub_Pnd_Write_Error_Report() throws Exception                                                                                                            //Natural: #WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Count_Pnd_Err_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ERR-CNT
        //* *WRITE(2) IAXFR-CALC-UNIQUE-ID                  /* 012809 START
        //* *  IAXFR-FROM-PPCN-NBR
        //* *  IAXFR-FROM-PAYEE-CDE
        //* *  #ERR-REASON
        //*  OS08
        //*  012809 END
        getReports().write(2, ReportOption.NOTITLE,iaa_Trnsfr_Sw_Ia_Unique_Id, new ReportEditMask ("999999999999"),iaa_Trnsfr_Sw_Ia_Frm_Cntrct,iaa_Trnsfr_Sw_Ia_Frm_Payee, //Natural: WRITE ( 2 ) IAA-TRNSFR-SW.IA-UNIQUE-ID ( EM = 9 ( 12 ) ) IAA-TRNSFR-SW.IA-FRM-CNTRCT IAA-TRNSFR-SW.IA-FRM-PAYEE #ERR-REASON
            pnd_Err_Reason);
        if (Global.isEscape()) return;
        pnd_L_Pnd_Error.setValue(true);                                                                                                                                   //Natural: ASSIGN #ERROR := TRUE
        pnd_Err_Reason.reset();                                                                                                                                           //Natural: RESET #ERR-REASON
        //*  #WRITE-ERROR-REPORT
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        print_Line_Ppcn.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Cert_Nbr());                                                                                          //Natural: ASSIGN PPCN := CIS-CERT-NBR
        print_Line_Mode_Code.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode());                                                                                   //Natural: ASSIGN MODE-CODE := CIS-PYMNT-MODE
        print_Line_Payee_Cd.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde());                                                                           //Natural: ASSIGN PAYEE-CD := IAXFR-FROM-PAYEE-CDE
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs().greater(getZero())))                                                                         //Natural: IF CIS-GRNTED-PERIOD-YRS GT 0
        {
            print_Line_Cis_Gr_Years.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Yrs());                                                                     //Natural: ASSIGN CIS-GR-YEARS := CIS-GRNTED-PERIOD-YRS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys().greater(getZero())))                                                                         //Natural: IF CIS-GRNTED-PERIOD-DYS GT 0
        {
            print_Line_Cis_Gr_Days.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Grnted_Period_Dys());                                                                      //Natural: ASSIGN CIS-GR-DAYS := CIS-GRNTED-PERIOD-DYS
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            print_Line_Cis_Option.setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option());                                                                            //Natural: ASSIGN CIS-OPTION := CIS-ANNTY-OPTION
            if (condition(pnd_Iaxfr_Units_A.getValue(pnd_I).greater(getZero())))                                                                                          //Natural: IF #IAXFR-UNITS-A ( #I ) GT 0
            {
                print_Line_Ia_Units.setValue(pnd_Iaxfr_Units_A.getValue(pnd_I));                                                                                          //Natural: ASSIGN IA-UNITS := #IAXFR-UNITS-A ( #I )
                print_Line_Fund.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I));                                                           //Natural: ASSIGN FUND := IAXFR-TO-ACCT-CD ( #I )
                print_Line_Mode_Code.setValue("A");                                                                                                                       //Natural: ASSIGN MODE-CODE := 'A'
                print_Line_Cis_Units.setValue(pnd_Cis_Unit_A.getValue(pnd_I));                                                                                            //Natural: ASSIGN CIS-UNITS := #CIS-UNIT-A ( #I )
                getReports().write(1, ReportOption.NOTITLE,print_Line_Ppcn,print_Line_Filler,print_Line_Payee_Cd,print_Line_Fillr,print_Line_Mode_Code,                   //Natural: WRITE ( 1 ) PRINT-LINE
                    print_Line_Filler1,print_Line_Fund,print_Line_Filler2,print_Line_Ia_Units,print_Line_Filler3,print_Line_Cis_Units,print_Line_Filler4,
                    print_Line_Cis_Option,print_Line_Filler5,print_Line_Cis_Gr_Years,print_Line_Filler6,print_Line_Cis_Gr_Days);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Iaxfr_Units_M.getValue(pnd_I).greater(getZero())))                                                                                          //Natural: IF #IAXFR-UNITS-M ( #I ) GT 0
            {
                print_Line_Fund.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I));                                                           //Natural: ASSIGN FUND := IAXFR-TO-ACCT-CD ( #I )
                print_Line_Ia_Units.setValue(pnd_Iaxfr_Units_M.getValue(pnd_I));                                                                                          //Natural: ASSIGN IA-UNITS := #IAXFR-UNITS-M ( #I )
                print_Line_Mode_Code.setValue("M");                                                                                                                       //Natural: ASSIGN MODE-CODE := 'M'
                print_Line_Cis_Units.setValue(pnd_Cis_Unit_M.getValue(pnd_I));                                                                                            //Natural: ASSIGN CIS-UNITS := #CIS-UNIT-M ( #I )
                getReports().write(1, ReportOption.NOTITLE,print_Line_Ppcn,print_Line_Filler,print_Line_Payee_Cd,print_Line_Fillr,print_Line_Mode_Code,                   //Natural: WRITE ( 1 ) PRINT-LINE
                    print_Line_Filler1,print_Line_Fund,print_Line_Filler2,print_Line_Ia_Units,print_Line_Filler3,print_Line_Cis_Units,print_Line_Filler4,
                    print_Line_Cis_Option,print_Line_Filler5,print_Line_Cis_Gr_Years,print_Line_Filler6,print_Line_Cis_Gr_Days);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        print_Line.reset();                                                                                                                                               //Natural: RESET PRINT-LINE #IAXFR-UNITS-A ( * ) #IAXFR-UNITS-M ( * ) #CIS-UNIT-A ( * ) #CIS-UNIT-M ( * )
        pnd_Iaxfr_Units_A.getValue("*").reset();
        pnd_Iaxfr_Units_M.getValue("*").reset();
        pnd_Cis_Unit_A.getValue("*").reset();
        pnd_Cis_Unit_M.getValue("*").reset();
        //*  WRITE-REPORT
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Total_Sw.setValue("N");                                                                                                                                       //Natural: ASSIGN #TOTAL-SW := 'N'
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"                     TOTAL  UNITS TRANSFERED       ");                                                                //Natural: WRITE ( 1 ) '                     TOTAL  UNITS TRANSFERED       '
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"ANNUAL UNITS  ",new ColumnSpacing(3)," IA UNITS ",new ColumnSpacing(8),"IA BYPASS",new ColumnSpacing(8),              //Natural: WRITE ( 1 ) 'ANNUAL UNITS  ' 3X ' IA UNITS ' 8X 'IA BYPASS' 8X ' CIS UNITS'
            " CIS UNITS");
        if (Global.isEscape()) return;
        print_Total_Line_Annual_Fund_Annual.getValue(1).setValue("STOCK");                                                                                                //Natural: ASSIGN FUND-ANNUAL ( 1 ) := 'STOCK'
        print_Total_Line_Annual_Fund_Annual.getValue(2).setValue("GLOBAL");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 2 ) := 'GLOBAL'
        print_Total_Line_Annual_Fund_Annual.getValue(3).setValue("SOCIAL");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 3 ) := 'SOCIAL'
        print_Total_Line_Annual_Fund_Annual.getValue(4).setValue("GROWTH");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 4 ) := 'GROWTH'
        print_Total_Line_Annual_Fund_Annual.getValue(5).setValue("EQUITY");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 5 ) := 'EQUITY'
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,print_Total_Line_Annual_Fund_Annual.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Annual_Ia_Units_Annual_Tot.getValue(pnd_I),new  //Natural: WRITE ( 1 ) FUND-ANNUAL ( #I ) 5X IA-UNITS-ANNUAL-TOT ( #I ) 5X IA-UNITS-ANNUAL-TOT-BYPASS ( #I ) 5X CIS-UNITS-ANNUAL-TOT ( #I )
                ColumnSpacing(5),print_Total_Line_Annual_Ia_Units_Annual_Tot_Bypass.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Annual_Cis_Units_Annual_Tot.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"MONTHLY UNITS  ",new ColumnSpacing(3)," IA UNITS ",new ColumnSpacing(7),"IA  BYPASS",new ColumnSpacing(8),            //Natural: WRITE ( 1 ) 'MONTHLY UNITS  ' 3X ' IA UNITS ' 7X 'IA  BYPASS' 8X ' CIS UNITS'
            " CIS UNITS");
        if (Global.isEscape()) return;
        print_Total_Line_Monthly_Fund_Mnthly.getValue(1).setValue("STOCK");                                                                                               //Natural: ASSIGN FUND-MNTHLY ( 1 ) := 'STOCK'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(2).setValue("GLOBAL");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 2 ) := 'GLOBAL'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(3).setValue("SOCIAL");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 3 ) := 'SOCIAL'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(4).setValue("GROWTH");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 4 ) := 'GROWTH'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(5).setValue("EQUITY");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 5 ) := 'EQUITY'
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,print_Total_Line_Monthly_Fund_Mnthly.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Monthly_Ia_Units_Mnthly_Tot.getValue(pnd_I),new  //Natural: WRITE ( 1 ) FUND-MNTHLY ( #I ) 5X IA-UNITS-MNTHLY-TOT ( #I ) 5X IA-UNITS-MNTHLY-TOT-BYPASS ( #I ) 5X CIS-UNITS-MNTHLY-TOT ( #I )
                ColumnSpacing(5),print_Total_Line_Monthly_Ia_Units_Mnthly_Tot_Bypass.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Monthly_Cis_Units_Mnthly_Tot.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-ERROR-TOTALS
    }
    private void sub_Write_Totals_C() throws Exception                                                                                                                    //Natural: WRITE-TOTALS-C
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"                     TOTAL  UNITS TRANSFERED       ",NEWLINE,"                     TODAY DATE NOT EQUAL CYCLE DATE"); //Natural: WRITE ( 1 ) '                     TOTAL  UNITS TRANSFERED       ' /'                     TODAY DATE NOT EQUAL CYCLE DATE'
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"ANNUAL UNITS  ",new ColumnSpacing(3)," IA UNITS ",new ColumnSpacing(8),"IA BYPASS",new ColumnSpacing(8),              //Natural: WRITE ( 1 ) 'ANNUAL UNITS  ' 3X ' IA UNITS ' 8X 'IA BYPASS' 8X ' CIS UNITS'
            " CIS UNITS");
        if (Global.isEscape()) return;
        print_Total_Line_Annual_Fund_Annual.getValue(1).setValue("STOCK");                                                                                                //Natural: ASSIGN FUND-ANNUAL ( 1 ) := 'STOCK'
        print_Total_Line_Annual_Fund_Annual.getValue(2).setValue("GLOBAL");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 2 ) := 'GLOBAL'
        print_Total_Line_Annual_Fund_Annual.getValue(3).setValue("SOCIAL");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 3 ) := 'SOCIAL'
        print_Total_Line_Annual_Fund_Annual.getValue(4).setValue("GROWTH");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 4 ) := 'GROWTH'
        print_Total_Line_Annual_Fund_Annual.getValue(5).setValue("EQUITY");                                                                                               //Natural: ASSIGN FUND-ANNUAL ( 5 ) := 'EQUITY'
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,print_Total_Line_Annual_Fund_Annual.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Annual_C_Ia_Units_Annual_Tot_C.getValue(pnd_I),new  //Natural: WRITE ( 1 ) FUND-ANNUAL ( #I ) 5X IA-UNITS-ANNUAL-TOT-C ( #I ) 5X IA-UNITS-ANNUAL-TOT-BYPASS-C ( #I ) 5X CIS-UNITS-ANNUAL-TOT-C ( #I )
                ColumnSpacing(5),print_Total_Line_Annual_C_Ia_Units_Annual_Tot_Bypass_C.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Annual_C_Cis_Units_Annual_Tot_C.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"MONTHLY UNITS  ",new ColumnSpacing(3)," IA UNITS ",new ColumnSpacing(7),"IA  BYPASS",new ColumnSpacing(8),            //Natural: WRITE ( 1 ) 'MONTHLY UNITS  ' 3X ' IA UNITS ' 7X 'IA  BYPASS' 8X ' CIS UNITS'
            " CIS UNITS");
        if (Global.isEscape()) return;
        print_Total_Line_Monthly_Fund_Mnthly.getValue(1).setValue("STOCK");                                                                                               //Natural: ASSIGN FUND-MNTHLY ( 1 ) := 'STOCK'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(2).setValue("GLOBAL");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 2 ) := 'GLOBAL'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(3).setValue("SOCIAL");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 3 ) := 'SOCIAL'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(4).setValue("GROWTH");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 4 ) := 'GROWTH'
        print_Total_Line_Monthly_Fund_Mnthly.getValue(5).setValue("EQUITY");                                                                                              //Natural: ASSIGN FUND-MNTHLY ( 5 ) := 'EQUITY'
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,print_Total_Line_Monthly_Fund_Mnthly.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_C.getValue(pnd_I),new  //Natural: WRITE ( 1 ) FUND-MNTHLY ( #I ) 5X IA-UNITS-MNTHLY-TOT-C ( #I ) 5X IA-UNITS-MNTHLY-TOT-BYPASS-C ( #I ) 5X CIS-UNITS-MNTHLY-TOT-C ( #I )
                ColumnSpacing(5),print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_Bypass_C.getValue(pnd_I),new ColumnSpacing(5),print_Total_Line_Monthly_C_Cis_Units_Mnthly_Tot_C.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-ERROR-TOTALS
    }
    //*  10/2014
    private void sub_Process_Cor_Payee_02() throws Exception                                                                                                              //Natural: PROCESS-COR-PAYEE-02
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(2);                                                                                                         //Natural: ASSIGN #I-PAYEE-CODE := 02
                                                                                                                                                                          //Natural: PERFORM READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
        sub_Read_Cor_Xref_Cntrct_For_Unique_Id();
        if (condition(Global.isEscape())) {return;}
        pnd_Reason2.reset();                                                                                                                                              //Natural: RESET #REASON2
        if (condition(pnd_Cor_Unique_Id.equals(getZero())))                                                                                                               //Natural: IF #COR-UNIQUE-ID EQ 0
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();                                                                                                   //Natural: RESET CIS-SCND-ANNT-SSN CIS-SCND-ANNT-PRFX CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
            ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
            pnd_Reason2.setValue("DATA NOT AVAILABLE");                                                                                                                   //Natural: ASSIGN #REASON2 := 'DATA NOT AVAILABLE'
            pnd_Err_Reason.setValue("COULD NOT FIND COR UNIQUE ID. (PAYEE 02)");                                                                                          //Natural: ASSIGN #ERR-REASON := 'COULD NOT FIND COR UNIQUE ID. (PAYEE 02)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BYPASS-CONTRACT
            sub_Bypass_Contract();
            if (condition(Global.isEscape())) {return;}
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
            //*  082017
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Cor_Unique_Id);                                                                                        //Natural: ASSIGN #I-PIN-N12 := #COR-UNIQUE-ID
            //*  10/2014 - END
                                                                                                                                                                          //Natural: PERFORM GET-COR-XREF-PH
            sub_Get_Cor_Xref_Ph();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Found.notEquals("Y")))                                                                                                                      //Natural: IF #FOUND NOT = 'Y'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().reset();                                                                                               //Natural: RESET CIS-SCND-ANNT-SSN CIS-SCND-ANNT-PRFX CIS-SCND-ANNT-FRST-NME CIS-SCND-ANNT-MID-NME CIS-SCND-ANNT-LST-NME CIS-SCND-ANNT-SFFX CIS-SCND-ANNT-DOB
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().reset();
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().reset();
                //*  082017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Ssn().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Ssn());                                                       //Natural: ASSIGN CIS-SCND-ANNT-SSN := #MDMA101.#O-SSN
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Prfx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix());                                                   //Natural: ASSIGN CIS-SCND-ANNT-PRFX := #O-PREFIX
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Frst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                           //Natural: ASSIGN CIS-SCND-ANNT-FRST-NME := #O-FIRST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Mid_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                           //Natural: ASSIGN CIS-SCND-ANNT-MID-NME := #O-MIDDLE-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Lst_Nme().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                             //Natural: ASSIGN CIS-SCND-ANNT-LST-NME := #O-LAST-NAME
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sffx().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix());                                                   //Natural: ASSIGN CIS-SCND-ANNT-SFFX := #O-SUFFIX
                //*    IF   PH-DOD-DTE > 0
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero()) && ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde().greater(1))) //Natural: IF #O-DATE-OF-DEATH > 0 AND IAXFR-FROM-PAYEE-CDE GT 01
                {
                    pnd_Dod_2_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #DOD-2-SW := 'Y'
                    //*  10/2014
                }                                                                                                                                                         //Natural: END-IF
                pnd_Date_A8_Pnd_Date_N8.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                        //Natural: ASSIGN #DATE-N8 := #O-DATE-OF-BIRTH
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-SCND-ANNT-DOB ( EM = YYYYMMDD )
                //*  10/2014
                pnd_Date_A8.reset();                                                                                                                                      //Natural: RESET #DATE-A8
                ldaIatl406.getCis_Prtcpnt_File_Cis_Scnd_Annt_Sex_Cde().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Gender_Code());                                           //Natural: ASSIGN CIS-SCND-ANNT-SEX-CDE := #O-GENDER-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-COR-PAYEE-02
    }
    private void sub_Read_Cor_Xref_Cntrct_For_Unique_Id() throws Exception                                                                                                //Natural: READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cor_Unique_Id.reset();                                                                                                                                        //Natural: RESET #COR-UNIQUE-ID
        pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                    //Natural: ASSIGN #I-TIAA-CREF-IND := 'T'
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr());                                              //Natural: ASSIGN #I-CONTRACT-NUMBER := IAXFR-FROM-PPCN-NBR
        //* *LLNAT 'MDMN210A' #MDMA210                      /* 082017 START
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //* * #MDMA210.#O-RETURN-CODE EQ '0000'
        //*  082017 END
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
        {
            pnd_Cor_Unique_Id.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12());                                                                                        //Natural: ASSIGN #COR-UNIQUE-ID := #MDMA211.#O-PIN-N12
            //*  07/2018
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  07/2018
                                                                                                                                                                          //Natural: PERFORM WRITE-PARM-VALUES
            sub_Write_Parm_Values();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  10/2014 - END
        //*  READ-COR-XREF-CNTRCT-FOR-UNIQUE-ID
    }
    private void sub_Get_Cor_Xref_Ph() throws Exception                                                                                                                   //Natural: GET-COR-XREF-PH
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Found.setValue("Y");                                                                                                                                          //Natural: ASSIGN #FOUND := 'Y'
        //*  10/2014 - START
        //* *FIND(1) COR-XREF-PH
        //*     WITH  COR-SUPER-PIN-RCDTYPE = #COR-SUPER-PIN-RCDTYPE
        //*   IF NO RECORD FOUND
        //*     #ERR-REASON := 'Could not find COR unique id. (COR-XREF-PH)'
        //*     PERFORM #WRITE-ERROR-REPORT
        //*     RESET #FOUND
        //*   END-NOREC
        //* *END-FIND
        //* *LLNAT 'MDMN100A' #MDMA100                      /* 082017 START
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //* * #MDMA100.#O-RETURN-CODE EQ '0000'
        //*  082017 END
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Err_Reason.setValue("Could not find COR unique id. (COR-XREF-PH)");                                                                                       //Natural: ASSIGN #ERR-REASON := 'Could not find COR unique id. (COR-XREF-PH)'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Found.reset();                                                                                                                                            //Natural: RESET #FOUND
        }                                                                                                                                                                 //Natural: END-IF
        //*  10/2014 - END
        //*  GET-COR-XREF-PH
    }
    private void sub_Process_Iaa_Cntrct() throws Exception                                                                                                                //Natural: PROCESS-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Audit_Cntrct.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr());                                                                               //Natural: ASSIGN #AUDIT-CNTRCT := IAXFR-FROM-PPCN-NBR
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #AUDIT-CNTRCT
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Audit_Cntrct, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND02", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("Could not find IAA-CNTRCT.");                                                                                                    //Natural: ASSIGN #ERR-REASON := 'Could not find IAA-CNTRCT.'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM PROCESS-ERROR
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(DbsUtil.maskMatches(ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde(),"NNN") && ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().greater("000")  //Natural: IF CNTRCT-RSDNCY-AT-ISSUE-CDE = MASK ( NNN ) AND ( CNTRCT-RSDNCY-AT-ISSUE-CDE GT '000' AND CNTRCT-RSDNCY-AT-ISSUE-CDE LT '070' )
                && ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().less("070")))
            {
                pnd_Numeric_Ste.setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().getSubstring(2,2));                                                       //Natural: ASSIGN #NUMERIC-STE := SUBSTRING ( CNTRCT-RSDNCY-AT-ISSUE-CDE,2,2 )
                pnd_Alpha_Ste.reset();                                                                                                                                    //Natural: RESET #ALPHA-STE
                //*  CONVERT NUMERIC RSDNCY CDE TO ALPHA RSDNCY CDE
                DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Alpha_Ste, pnd_Numeric_Ste);                                 //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
                if (condition(Global.isEscape())) return;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Orig_Issue_State().setValue(pnd_Alpha_Ste);                                                                            //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := #ALPHA-STE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Orig_Issue_State().setValue("NY");                                                                                     //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := 'NY'
            }                                                                                                                                                             //Natural: END-IF
            //*  FILL THE CIS-ANNTY-OPTION CODE
            ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().reset();                                                                                                    //Natural: RESET CIS-ANNTY-OPTION
            short decideConditionsMet2239 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE OF IAA-CNTRCT.CNTRCT-OPTN-CDE;//Natural: VALUE 01, 05, 06, 09
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9))))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("OL");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'OL'
            }                                                                                                                                                             //Natural: VALUE 02
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(2)))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("IR");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'IR'
            }                                                                                                                                                             //Natural: VALUE 04, 11, 14, 16
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16))))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LSF");                                                                                        //Natural: ASSIGN CIS-ANNTY-OPTION := 'LSF'
            }                                                                                                                                                             //Natural: VALUE 07, 08, 10, 13
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13))))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("JS");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'JS'
            }                                                                                                                                                             //Natural: VALUE 03, 12, 15, 17
            if (condition((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) 
                || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17))))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LS");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'LS'
            }                                                                                                                                                             //Natural: VALUE 54:57
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))
            {
                decideConditionsMet2239++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_Option().setValue("LST");                                                                                        //Natural: ASSIGN CIS-ANNTY-OPTION := 'LST'
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet2239 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                             //Natural: IF CNTRCT-ISSUE-DTE-DD = 0
            {
                cntrct_Issue_Dte_Dd_1.setValue("01");                                                                                                                     //Natural: ASSIGN CNTRCT-ISSUE-DTE-DD-1 := '01'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cntrct_Issue_Dte_Dd_1.setValueEdited(ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte_Dd(),new ReportEditMask("99"));                                           //Natural: MOVE EDITED CNTRCT-ISSUE-DTE-DD ( EM = 99 ) TO CNTRCT-ISSUE-DTE-DD-1
            }                                                                                                                                                             //Natural: END-IF
            cntrct_Issue_Dte_D.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte(), cntrct_Issue_Dte_Dd_1));            //Natural: COMPRESS CNTRCT-ISSUE-DTE CNTRCT-ISSUE-DTE-DD-1 INTO CNTRCT-ISSUE-DTE-D LEAVING NO
            short decideConditionsMet2261 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-OPTN-CDE = 01 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 01 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(1) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 02
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(2)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("IR00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'IR00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 05 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 06 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 09 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("SL15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'SL15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 05 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(5) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 06 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(6) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 09 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(9) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("OL15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'OL15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 03 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 04 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 07 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 03 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 04 AND CNTRCT-ISSUE-DTE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4) && ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte().less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 07 AND CNTRCT-ISSUE-DTE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) && ldaIaal200a.getIaa_Cntrct_Cntrct_Issue_Dte().less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 00'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 08 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 10 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 13 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("J 15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'J 15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 08 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 10 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 13 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TJ15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TJ15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 11 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 14 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 16 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LF10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LF10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 11 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 14 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 16 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TF10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TF10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 12 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 15 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 17 AND CNTRCT-ISSUE-DTE-DATE LT 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.less(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LH10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LH10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 12 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 15 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 17 AND CNTRCT-ISSUE-DTE-DATE GE 19971001
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17) && cntrct_Issue_Dte_D_Cntrct_Issue_Dte_Date.greaterOrEqual(19971001)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("TH10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'TH10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 54
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(54)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT10");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT10'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 55
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(55)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT15");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT15'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 56
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(56)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT20");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT20'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-OPTN-CDE = 57
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(57)))
            {
                decideConditionsMet2261++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_To_Company().setValue("LT00");                                                                                   //Natural: ASSIGN CIS-TRNSF-TO-COMPANY := 'LT00'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-IAA-CNTRCT
    }
    private void sub_Fill_Cis_Prtcpnt_File() throws Exception                                                                                                             //Natural: FILL-CIS-PRTCPNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte().setValue(pnd_Todays_Date);                                                                                  //Natural: ASSIGN CIS-CNTRCT-PRINT-DTE := #TODAYS-DATE
        if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Ia_New_Iss_Prt_Pull().equals("Y")))                                                                               //Natural: IF IA-NEW-ISS-PRT-PULL = 'Y'
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("SPEC");                                                                                              //Natural: ASSIGN CIS-PULL-CODE := 'SPEC'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("    ");                                                                                              //Natural: ASSIGN CIS-PULL-CODE := '    '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().reset();                                                                                                        //Natural: RESET CIS-OWNERSHIP-CD
        if (condition(((((((((((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF CNTRCT-OPTN-CDE = 3 OR = 4 OR = 7 OR = 8 OR = 10 OR = 11 OR = 12 OR = 13 OR = 14 OR = 15 OR = 16 OR = 17 OR CNTRCT-OPTN-CDE = 54 THRU 57
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(11)) 
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(14)) 
            || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(16)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17)) 
            || (ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))))
        {
            if (condition(pnd_Dod_1_Sw.equals("Y")))                                                                                                                      //Natural: IF #DOD-1-SW = 'Y'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().setValue(2);                                                                                            //Natural: ASSIGN CIS-OWNERSHIP-CD := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Dod_2_Sw.equals("Y")))                                                                                                                  //Natural: IF #DOD-2-SW = 'Y'
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Ownership_Cd().setValue(1);                                                                                        //Natural: ASSIGN CIS-OWNERSHIP-CD := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dod_1_Sw.reset();                                                                                                                                             //Natural: RESET #DOD-1-SW #DOD-2-SW CIS-TRNSF-FROM-COMPANY CIS-TIAA-DOI CIS-CREF-DOI CIS-PIN-NBR CIS-RQST-ID CIS-CNTRCT-TYPE CIS-OPN-CLSD-IND CIS-STATUS-CD CIS-DA-CERT-NBR ( 1 ) CIS-DA-CREF-PROCEEDS-AMT ( 1 ) CIS-TRNSF-FLAG CIS-TIAA-CNTRCT-TYPE CIS-CREF-CNTRCT-TYPE CIS-REA-CNTRCT-TYPE CIS-CORP-SYNC-IND CIS-ADDR-SYN-IND CIS-ADDR-PROCESS-ENV CIS-COR-SYNC-IND CIS-COR-PROCESS-ENV
        pnd_Dod_2_Sw.reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Doi().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Pin_Nbr().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Status_Cd().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cert_Nbr().getValue(1).reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt().getValue(1).reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Flag().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Rea_Cntrct_Type().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Process_Env().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Process_Env().reset();
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_From_Company().setValue("TIAA");                                                                                         //Natural: ASSIGN CIS-TRNSF-FROM-COMPANY := 'TIAA'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi().compute(new ComputeParameters(false, ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Doi()), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Effctve_Dte().add(1)); //Natural: ASSIGN CIS-CREF-DOI := IAXFR-EFFCTVE-DTE + 1
        ldaIatl406.getCis_Prtcpnt_File_Cis_Pin_Nbr().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Calc_Unique_Id());                                                  //Natural: ASSIGN CIS-PIN-NBR := IAXFR-CALC-UNIQUE-ID
        ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id().setValue("IA");                                                                                                      //Natural: ASSIGN CIS-RQST-ID := 'IA'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cntrct_Type().setValue("C");                                                                                                   //Natural: ASSIGN CIS-CNTRCT-TYPE := 'C'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind().setValue("O");                                                                                                  //Natural: ASSIGN CIS-OPN-CLSD-IND := 'O'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Status_Cd().setValue("I");                                                                                                     //Natural: ASSIGN CIS-STATUS-CD := 'I'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cert_Nbr().getValue(1).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr());                                   //Natural: ASSIGN CIS-DA-CERT-NBR ( 1 ) := IAXFR-FROM-PPCN-NBR
        ldaIatl406.getCis_Prtcpnt_File_Cis_Da_Cref_Proceeds_Amt().getValue(1).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Rqstd_Xfr_Guar().getValue(1));        //Natural: ASSIGN CIS-DA-CREF-PROCEEDS-AMT ( 1 ) := IAXFR-FROM-RQSTD-XFR-GUAR ( 1 )
        ldaIatl406.getCis_Prtcpnt_File_Cis_Trnsf_Flag().setValue("Y");                                                                                                    //Natural: ASSIGN CIS-TRNSF-FLAG := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Cntrct_Type().setValue("T");                                                                                              //Natural: ASSIGN CIS-CREF-CNTRCT-TYPE := 'T'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN CIS-CORP-SYNC-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().setValue("Y");                                                                                                  //Natural: ASSIGN CIS-ADDR-SYN-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Process_Env().setValue("B");                                                                                              //Natural: ASSIGN CIS-ADDR-PROCESS-ENV := 'B'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().setValue("Y");                                                                                                  //Natural: ASSIGN CIS-COR-SYNC-IND := 'Y'
        ldaIatl406.getCis_Prtcpnt_File_Cis_Cor_Process_Env().setValue("B");                                                                                               //Natural: ASSIGN CIS-COR-PROCESS-ENV := 'B'
        pnd_Dod_1_Sw.reset();                                                                                                                                             //Natural: RESET #DOD-1-SW #DOD-2-SW
        pnd_Dod_2_Sw.reset();
    }
    private void sub_Process_Prtcpnt_Role() throws Exception                                                                                                              //Natural: PROCESS-PRTCPNT-ROLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  012809 START
        //*  012809 END
        pnd_Iaxfr_Units_A.getValue("*").reset();                                                                                                                          //Natural: RESET #IAXFR-UNITS-A ( * ) #IAXFR-UNITS-M ( * ) #CIS-UNIT-A ( * ) #CIS-UNIT-M ( * )
        pnd_Iaxfr_Units_M.getValue("*").reset();
        pnd_Cis_Unit_A.getValue("*").reset();
        pnd_Cis_Unit_M.getValue("*").reset();
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Ppcn_Nbr());                                               //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-CALC-TO-PPCN-NBR
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Calc_To_Payee_Cde());                                             //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-CALC-TO-PAYEE-CDE
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                     //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("FIND03", true)))
        {
            ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORD FOUND
            {
                pnd_Err_Reason.setValue("Could not find IAA-CNTRCT-PRTCPNT-ROLE");                                                                                        //Natural: ASSIGN #ERR-REASON := 'Could not find IAA-CNTRCT-PRTCPNT-ROLE'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
                sub_Pnd_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM PROCESS-ERROR
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition((DbsUtil.maskMatches(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde(),"NNN") && (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().greater("000")  //Natural: IF PRTCPNT-RSDNCY-CDE = MASK ( NNN ) AND ( PRTCPNT-RSDNCY-CDE GT '000' OR PRTCPNT-RSDNCY-CDE LT '070' )
                || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().less("070")))))
            {
                pnd_Numeric_Ste.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().getSubstring(2,2));                                                  //Natural: ASSIGN #NUMERIC-STE := SUBSTRING ( PRTCPNT-RSDNCY-CDE,2,2 )
                pnd_Alpha_Ste.reset();                                                                                                                                    //Natural: RESET #ALPHA-STE
                //*  CONVERT NUMERIC RSDNCY CDE TO ALPHA RSDNCY CDE
                DbsUtil.callnat(Nazn031.class , getCurrentProcessState(), pdaIaapda_M.getMsg_Info_Sub(), pnd_Alpha_Ste, pnd_Numeric_Ste);                                 //Natural: CALLNAT 'NAZN031' MSG-INFO-SUB #ALPHA-STE #NUMERIC-STE
                if (condition(Global.isEscape())) return;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Alpha_Ste);                                                                         //Natural: ASSIGN CIS-FRST-ANNT-RSDNC-CDE := #ALPHA-STE
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue(pnd_Alpha_Ste);                                                                              //Natural: ASSIGN CIS-ISSUE-STATE-CD := #ALPHA-STE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue("NY");                                                                                  //Natural: ASSIGN CIS-FRST-ANNT-RSDNC-CDE := 'NY'
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue("NY");                                                                                       //Natural: ASSIGN CIS-ISSUE-STATE-CD := 'NY'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().greater("58")))                                                                     //Natural: IF PRTCPNT-RSDNCY-CDE GT '58'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue("NY");                                                                                       //Natural: ASSIGN CIS-ISSUE-STATE-CD := 'NY'
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet2411 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PRTCPNT-CTZNSHP-CDE;//Natural: VALUE 1
            if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().equals(1))))
            {
                decideConditionsMet2411++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue("US");                                                                                 //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := 'US'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().equals(2))))
            {
                decideConditionsMet2411++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue("CA");                                                                                 //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := 'CA'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde());                             //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := CIS-FRST-ANNT-RSDNC-CDE
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet2419 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-MODE-IND = 100
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100)))
            {
                decideConditionsMet2419++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("M");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'M'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 701 AND CNTRCT-MODE-IND LE 706
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(701) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(706)))
            {
                decideConditionsMet2419++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("S");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'S'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 601 AND CNTRCT-MODE-IND LE 603
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(601) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(603)))
            {
                decideConditionsMet2419++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("Q");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'Q'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-MODE-IND GE 801 AND CNTRCT-MODE-IND LE 812
            else if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().greaterOrEqual(801) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().lessOrEqual(812)))
            {
                decideConditionsMet2419++;
                ldaIatl406.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue("A");                                                                                            //Natural: ASSIGN CIS-PYMNT-MODE := 'A'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet2431 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT.CNTRCT-OPTN-CDE = 03 OR = 12 OR = 15 OR = 17 AND CNTRCT-FIRST-ANNT-DOD-DTE > 0
            if (condition(((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(12)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(15)) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(17)) && ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()))))
            {
                decideConditionsMet2431++;
                FOR06:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(2)); //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 2
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(2)); //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 07 OR = 08 OR = 10 OR = 13 ) AND ( CNTRCT-FIRST-ANNT-DOD-DTE > 0 OR CNTRCT-SCND-ANNT-DOD-DTE > 0 )
            else if (condition(((((ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || 
                ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(10)) || ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().equals(13)) && (ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero()) 
                || ldaIaal200a.getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte().greater(getZero())))))
            {
                decideConditionsMet2431++;
                FOR07:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.5
                            DbsDecimal("1.5")));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.5
                            DbsDecimal("1.5")));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN IAA-CNTRCT.CNTRCT-OPTN-CDE = 54 THRU 57 AND CNTRCT-FIRST-ANNT-DOD-DTE > 0
            else if (condition(ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaIaal200a.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57) 
                && ldaIaal200a.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero())))
            {
                decideConditionsMet2431++;
                FOR08:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_A.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-A ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.333333
                            DbsDecimal("1.333333")));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Cis_Unit_M.getValue(pnd_I)), ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I).multiply(new  //Natural: COMPUTE #CIS-UNIT-M ( #I ) = IAXFR-TO-AFTR-XFR-UNITS ( #I ) * 1.333333
                            DbsDecimal("1.333333")));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                FOR09:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        pnd_Cis_Unit_A.getValue(pnd_I).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                             //Natural: ASSIGN #CIS-UNIT-A ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Unit_M.getValue(pnd_I).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                             //Natural: ASSIGN #CIS-UNIT-M ( #I ) := IAXFR-TO-AFTR-XFR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ************
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue("*").reset();                                                                             //Natural: RESET CIS-CREF-ANNUAL-NBR-UNITS ( * )
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units().getValue("*").reset();                                                                             //Natural: RESET CIS-CREF-MNTHLY-NBR-UNITS ( * )
            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue("*").reset();                                                                                     //Natural: RESET CIS-CREF-ACCT-CDE ( * )
            pnd_Ws_Fund.reset();                                                                                                                                          //Natural: RESET #WS-FUND #K
            pnd_K.reset();
            if (condition(pnd_Todays_Date.greater(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Cycle_Dte())))                                                                  //Natural: IF #TODAYS-DATE GT IAXFR-CYCLE-DTE
            {
                                                                                                                                                                          //Natural: PERFORM DIFFERENT-IAXFR-CYCLE-DTE
                sub_Different_Iaxfr_Cycle_Dte();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR10:                                                                                                                                                    //Natural: FOR #J = 1 TO 5
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
                {
                    FOR11:                                                                                                                                                //Natural: FOR #I = 1 TO #COUNT-PE
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                    {
                        if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I).equals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                 //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = #FUND-A ( #J )
                        {
                            if (condition(pnd_Ws_Fund.notEquals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                                                                   //Natural: IF #WS-FUND NE #FUND-A ( #J )
                            {
                                pnd_K.nadd(1);                                                                                                                            //Natural: ADD 1 TO #K
                                pnd_Ws_Fund.setValue(pnd_Funds_Pnd_Fund_A.getValue(pnd_J));                                                                               //Natural: ASSIGN #WS-FUND := #FUND-A ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue(pnd_K).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I)); //Natural: ASSIGN CIS-CREF-ACCT-CDE ( #K ) := IAXFR-TO-ACCT-CD ( #I )
                            if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                             //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                            {
                                ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                          //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-CREF-ANNUAL-NBR-UNITS ( #K )
                                print_Total_Line_Annual_Cis_Units_Annual_Tot.getValue(pnd_J).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                        //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-UNITS-ANNUAL-TOT ( #J )
                                pnd_Iaxfr_Units_A.getValue(pnd_I).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                      //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO #IAXFR-UNITS-A ( #I )
                                print_Total_Line_Annual_Ia_Units_Annual_Tot.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-ANNUAL-TOT ( #J )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                          //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-CREF-MNTHLY-NBR-UNITS ( #K )
                                print_Total_Line_Monthly_Cis_Units_Mnthly_Tot.getValue(pnd_J).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                       //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-UNITS-MNTHLY-TOT ( #J )
                                pnd_Iaxfr_Units_M.getValue(pnd_I).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                      //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO #IAXFR-UNITS-M ( #I )
                                print_Total_Line_Monthly_Ia_Units_Mnthly_Tot.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-MNTHLY-TOT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* ****
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().notEquals(getZero())))                                                        //Natural: IF CNTRCT-FINAL-PER-PAY-DTE NE 0
            {
                pnd_Date_A8_Pnd_Date_Yyyymm.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte());                                                  //Natural: ASSIGN #DATE-YYYYMM := CNTRCT-FINAL-PER-PAY-DTE
                pnd_Date_A8_Pnd_Date_Dd.setValue(1);                                                                                                                      //Natural: ASSIGN #DATE-DD := 01
                ldaIatl406.getCis_Prtcpnt_File_Cis_Annty_End_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                            //Natural: MOVE EDITED #DATE-A8 TO CIS-ANNTY-END-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PROCESS-PRTCPNT-ROLE
    }
    private void sub_Fill_Address_Info() throws Exception                                                                                                                 //Natural: FILL-ADDRESS-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Address_Array.setValue(1);                                                                                                                                    //Natural: ASSIGN #ADDRESS-ARRAY := 1
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Ppcn_Nbr());                                              //Natural: ASSIGN #I-CONTRACT-NUMBER := IAXFR-FROM-PPCN-NBR
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_From_Payee_Cde());                                                  //Natural: ASSIGN #I-PAYEE-CODE := IAXFR-FROM-PAYEE-CDE
        //* *LLNAT 'MDMN210A' #MDMA210                      /* 082017 START
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //* * #MDMA210.#O-RETURN-CODE EQ '0000'
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
        {
            //* *RESET #MDMA100
            pdaMdma101.getPnd_Mdma101().reset();                                                                                                                          //Natural: RESET #MDMA101
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Pin_N12());                                                                //Natural: ASSIGN #I-PIN-N12 := #MDMA211.#O-PIN-N12
            //* *CALLNAT 'MDMN100A' #MDMA100
            //*  082017 END
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Err_Reason.setValue("Could not find NAS-NAME-ADDRESS for correspondence");                                                                                //Natural: ASSIGN #ERR-REASON := 'Could not find NAS-NAME-ADDRESS for correspondence'
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        sub_Fill_One_Occur_Of_Address_Array();
        if (condition(Global.isEscape())) {return;}
        pnd_Address_Array.setValue(2);                                                                                                                                    //Natural: ASSIGN #ADDRESS-ARRAY := 2
        //* *PERFORM GET-NAS-NAME-ADDRESS-CHECK-MAILING
        //* *IF NAS-NAME-ADDRESS.PERMANENT-ADDRSS-IND = 'P'  /* CHECK MAIL PERM
        //*   RESET #VACATION-ADDRESS
        //* *ELSE
        //*   PERFORM GET-NAS-VACATION-ADDRESS-CHECK-MAILING
        //*   #VACATION-ADDRESS  := TRUE
        //* *END-IF
        //*  10/2014 - END
                                                                                                                                                                          //Natural: PERFORM FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
        sub_Fill_One_Occur_Of_Address_Array();
        if (condition(Global.isEscape())) {return;}
        //*  FILL-ADDRESS-INFO
    }
    private void sub_Fill_One_Occur_Of_Address_Array() throws Exception                                                                                                   //Natural: FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Address_Array.less(1) || pnd_Address_Array.greater(3)))                                                                                         //Natural: IF #ADDRESS-ARRAY LT 1 OR #ADDRESS-ARRAY GT 3
        {
            pnd_Err_Reason.setValue(DbsUtil.compress(" ADDRESS ARRAY = ", pnd_Address_Array, "IT MUST BE 1,2 OR 3."));                                                    //Natural: COMPRESS ' ADDRESS ARRAY = ' #ADDRESS-ARRAY 'IT MUST BE 1,2 OR 3.' INTO #ERR-REASON
                                                                                                                                                                          //Natural: PERFORM #WRITE-ERROR-REPORT
            sub_Pnd_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            //*  PERFORM PROCESS-ERROR
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.setValue(pnd_Address_Array);                                                                                                                                //Natural: ASSIGN #I := #ADDRESS-ARRAY
        //*  10/2014 - START
        //* *IF  #VACATION-ADDRESS
        //*  CORRESPONDENCE
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),                 //Natural: COMPRESS #O-LAST-NAME #O-PREFIX #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO CIS-ADDRESS-DEST-NAME ( #I )
                pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), 
                pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,1).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_1());                             //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,1 ) := #O-CO-ADDRESS-LINE-1
            pnd_Index.setValue(2);                                                                                                                                        //Natural: ASSIGN #INDEX := 2
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_2().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-2 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_2());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,2 ) := #O-CO-ADDRESS-LINE-2
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_3().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-3 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_3());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,3 ) := #O-CO-ADDRESS-LINE-3
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_4().notEquals(" ")))                                                                            //Natural: IF #O-CO-ADDRESS-LINE-4 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_4());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,4 ) := #O-CO-ADDRESS-LINE-4
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(DbsUtil.compress(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_City(),       //Natural: COMPRESS #O-CO-ADDRESS-CITY #O-CO-ADDRESS-ST-PROV TO CIS-ADDRESS-TXT ( #I,#INDEX )
                pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_St_Prov()));
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Type_Code().equals("C") || pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Type_Code().equals("F")))      //Natural: IF #O-CO-ADDRESS-TYPE-CODE = 'C' OR = 'F'
            {
                if (condition(pnd_Index.less(5)))                                                                                                                         //Natural: IF #INDEX LT 5
                {
                    pnd_Index.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #INDEX
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Country());            //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#INDEX ) := #O-CO-ADDRESS-COUNTRY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(DbsUtil.compress(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index),  //Natural: COMPRESS CIS-ADDRESS-TXT ( #I,#INDEX ) #O-CO-ADDRESS-COUNTRY INTO CIS-ADDRESS-TXT ( #I,#INDEX )
                        pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Country()));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Zip_Code());                                //Natural: ASSIGN CIS-ZIP-CODE ( #I ) := #O-CO-ADDRESS-ZIP-CODE
            pnd_Zip.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Zip_Code());                                                                                      //Natural: ASSIGN #ZIP := #O-CO-ADDRESS-ZIP-CODE
            //*  10/2014 - END
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,2 ) FOR #ZIP AND DELETE
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,3 ) FOR #ZIP AND DELETE
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,4 ) FOR #ZIP AND DELETE
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,5)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,5 ) FOR #ZIP AND DELETE
            ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).setValue(pnd_I);                                                                         //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #I ) := #I
            if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).equals("1")))                                                              //Natural: IF CIS-ADDR-USAGE-CODE ( #I ) = '1'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(" ");                                                                     //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #I ) := ' '
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2014 - START
            //*  IF CIS-ADDR-USAGE-CODE(#I) = '2'
            //*    CIS-ADDRESS-DEST-NAME(#I):=  CIS-ADDRESS-TXT(#I,1)
            //*    CIS-ADDRESS-TXT(#I,1) := ' '
            //*    CIS-ADDRESS-TXT(#I,5) := ' '
            //*  END-IF
            //*  10/2014 - END
            //*  NORMAL ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  10/2014 - START
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(),                 //Natural: COMPRESS #O-LAST-NAME #O-PREFIX #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO CIS-ADDRESS-DEST-NAME ( #I )
                pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), 
                pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            ldaIatl406.getCis_Prtcpnt_File_Cis_Checking_Saving_Cd().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Check_Saving_Ind());                      //Natural: ASSIGN CIS-CHECKING-SAVING-CD ( #I ) := #O-CM-CHECK-SAVING-IND
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,1).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_1());                             //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,1 ) := #O-CM-ADDRESS-LINE-1
            pnd_Index.setValue(2);                                                                                                                                        //Natural: ASSIGN #INDEX := 2
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_2().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-2 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_2());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,2 ) := #O-CM-ADDRESS-LINE-2
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_3().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-3 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_3());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,3 ) := #O-CM-ADDRESS-LINE-3
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_4().notEquals(" ")))                                                                            //Natural: IF #O-CM-ADDRESS-LINE-4 NE ' '
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Line_4());                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,4 ) := #O-CM-ADDRESS-LINE-4
                pnd_Index.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INDEX
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(DbsUtil.compress(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_City(),       //Natural: COMPRESS #O-CM-ADDRESS-CITY #O-CM-ADDRESS-ST-PROV TO CIS-ADDRESS-TXT ( #I,#INDEX )
                pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_St_Prov()));
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Type_Code().equals("C") || pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Type_Code().equals("F")))      //Natural: IF #O-CM-ADDRESS-TYPE-CODE = 'C' OR = 'F'
            {
                if (condition(pnd_Index.less(5)))                                                                                                                         //Natural: IF #INDEX LT 5
                {
                    pnd_Index.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #INDEX
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Country());            //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#INDEX ) := #O-CM-ADDRESS-COUNTRY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index).setValue(DbsUtil.compress(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,pnd_Index),  //Natural: COMPRESS CIS-ADDRESS-TXT ( #I,#INDEX ) #O-CM-ADDRESS-COUNTRY INTO CIS-ADDRESS-TXT ( #I,#INDEX )
                        pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Country()));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaIatl406.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Zip_Code());                                //Natural: ASSIGN CIS-ZIP-CODE ( #I ) := #O-CM-ADDRESS-ZIP-CODE
            ldaIatl406.getCis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Account_Number());                 //Natural: ASSIGN CIS-BANK-PYMNT-ACCT-NMBR ( #I ) := #O-CM-BANK-ACCOUNT-NUMBER
            ldaIatl406.getCis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr().getValue(pnd_I).setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Aba_Eft_Number());                   //Natural: ASSIGN CIS-BANK-ABA-ACCT-NMBR ( #I ) := #O-CM-BANK-ABA-EFT-NUMBER
            ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).setValue(pnd_I);                                                                         //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #I ) := #I
            //*  IF CIS-ADDR-USAGE-CODE(#I) = '1'
            //*    CIS-ADDRESS-TXT(#I,5) := ' '
            //*    CIS-ADDRESS-DEST-NAME(#I) := ' '
            //*  END-IF
            if (condition(ldaIatl406.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_I).equals("2")))                                                              //Natural: IF CIS-ADDR-USAGE-CODE ( #I ) = '2'
            {
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_I).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,          //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #I ) := CIS-ADDRESS-TXT ( #I,1 )
                    1));
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,1).setValue(" ");                                                                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,1 ) := ' '
                ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,5).setValue(" ");                                                                         //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,5 ) := ' '
            }                                                                                                                                                             //Natural: END-IF
            pnd_Zip.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Address_Zip_Code());                                                                                      //Natural: ASSIGN #ZIP := #O-CM-ADDRESS-ZIP-CODE
            //*  10/2014 - END
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,2)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,2 ) FOR #ZIP AND DELETE
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,3)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,3 ) FOR #ZIP AND DELETE
            DbsUtil.examine(new ExamineSource(ldaIatl406.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_I,4)), new ExamineSearch(pnd_Zip), new ExamineDelete());      //Natural: EXAMINE CIS-ADDRESS-TXT ( #I,4 ) FOR #ZIP AND DELETE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FILL-ONE-OCCUR-OF-ADDRESS-ARRAY
    }
    private void sub_Mode_100() throws Exception                                                                                                                          //Natural: MODE-100
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Date_A8_Pnd_Date_Mm.equals(12)))                                                                                                                //Natural: IF #DATE-MM = 12
        {
            pnd_Date_A8_Pnd_Date_Mm.setValue(1);                                                                                                                          //Natural: ASSIGN #DATE-MM := 1
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Date_A8_Pnd_Date_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_601_603() throws Exception                                                                                                                      //Natural: MODE-601-603
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Mm.nadd(3);                                                                                                                                  //Natural: ADD 3 TO #DATE-MM
        if (condition(pnd_Date_A8_Pnd_Date_Mm.greater(12)))                                                                                                               //Natural: IF #DATE-MM > 12
        {
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
            pnd_Date_A8_Pnd_Date_Mm.nsubtract(12);                                                                                                                        //Natural: SUBTRACT 12 FROM #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_701_706() throws Exception                                                                                                                      //Natural: MODE-701-706
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Mm.nadd(6);                                                                                                                                  //Natural: ADD 6 TO #DATE-MM
        if (condition(pnd_Date_A8_Pnd_Date_Mm.greater(12)))                                                                                                               //Natural: IF #DATE-MM > 12
        {
            pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DATE-YYYY
            pnd_Date_A8_Pnd_Date_Mm.nsubtract(12);                                                                                                                        //Natural: SUBTRACT 12 FROM #DATE-MM
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mode_801_812() throws Exception                                                                                                                      //Natural: MODE-801-812
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Date_A8_Pnd_Date_Yyyy.nadd(1);                                                                                                                                //Natural: ADD 1 TO #DATE-YYYY
    }
    private void sub_Update_Cis_Prtcpnt_File() throws Exception                                                                                                           //Natural: UPDATE-CIS-PRTCPNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key().setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Rqst_Id());                                                           //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY := IAA-XFR-AUDIT-VIEW.RQST-ID
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Entry_Date_Time_Key().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key());                                             //Natural: MOVE CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY TO #CIS-ENTRY-DATE-TIME-KEY
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Requestor().setValue("IA");                                                                                                   //Natural: MOVE 'IA' TO #CIS-REQUESTOR
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Pin_Number().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Pin_Nbr());                                                          //Natural: MOVE CIS-PIN-NBR TO #CIS-PIN-NUMBER
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Ssn().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn());                                                           //Natural: MOVE CIS-FRST-ANNT-SSN TO #CIS-SSN
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Dob().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob());                                                           //Natural: MOVE CIS-FRST-ANNT-DOB TO #CIS-DOB
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Tiaa_No().getValue(1).setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Tiaa_Nbr());                                                //Natural: MOVE CIS-TIAA-NBR TO #CIS-TIAA-NO ( 1 )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Cref_No().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Cert_Nbr());                                                            //Natural: MOVE CIS-CERT-NBR TO #CIS-CREF-NO
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Rqst_Log_Dte_Tme().setValue(ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme());                                   //Natural: MOVE CIS-RQST-SYS-MIT-LOG-DTE-TME TO #CIS-RQST-LOG-DTE-TME
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("I");                                                                                      //Natural: MOVE 'I' TO #CIS-ASSIGN-ISSUE-CNTRCT-IND
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Product_Cde().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #CIS-PRODUCT-CDE
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Cntrct_Type().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #CIS-CNTRCT-TYPE
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Msg_Text().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #CIS-MSG-TEXT
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Msg_Number().getValue("*").setValue(" ");                                                                                     //Natural: MOVE ' ' TO #CIS-MSG-NUMBER ( * )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Functions().getValue(4).setValue("YB");                                                                                       //Natural: MOVE 'YB' TO #CIS-FUNCTIONS ( 4 )
        ldaNazl6004.getPnd_Nazl6004_Pnd_Cis_Mit_Function_Code().setValue("MO");                                                                                           //Natural: MOVE 'MO' TO #CIS-MIT-FUNCTION-CODE
        DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), ldaNazl6004.getPnd_Nazl6004());                                                                        //Natural: CALLNAT 'CISN1000' USING #NAZL6004
        if (condition(Global.isEscape())) return;
        //* *SET CONTROL 'WB'
        //*         WRITE 'MESSAGE FROM CISN1000 '   #CIS-MSG-TEXT
        //*  REA0614 START
        vw_cis_Updt.startDatabaseFind                                                                                                                                     //Natural: FIND CIS-UPDT WITH CIS-UPDT.CIS-RQST-ID-KEY = CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY
        (
        "FIND04",
        new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", ldaIatl406.getCis_Prtcpnt_File_Cis_Rqst_Id_Key(), WcType.WITH) }
        );
        FIND04:
        while (condition(vw_cis_Updt.readNextRow("FIND04")))
        {
            vw_cis_Updt.setIfNotFoundControlFlag(false);
            FOR12:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I).equals(" ")))                                                          //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I)),  //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR IAXFR-TO-ACCT-CD ( #I ) GIVING INDEX #A
                    new ExamineGivingIndex(pnd_A));
                if (condition(pnd_A.greater(getZero())))                                                                                                                  //Natural: IF #A GT 0
                {
                    cis_Updt_Cis_Sg_Fund_Ticker.getValue(pnd_I).setValue(pnd_Acct_Tckr.getValue(pnd_A));                                                                  //Natural: ASSIGN CIS-UPDT.CIS-SG-FUND-TICKER ( #I ) := #ACCT-TCKR ( #A )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            vw_cis_Updt.updateDBRow("FIND04");                                                                                                                            //Natural: UPDATE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  REA0614 END
        pnd_Count_Pnd_Rec_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-CNT
    }
    private void sub_Bypass_Contract() throws Exception                                                                                                                   //Natural: BYPASS-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Todays_Date.greater(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_Cycle_Dte())))                                                                      //Natural: IF #TODAYS-DATE GT IAXFR-CYCLE-DTE
        {
                                                                                                                                                                          //Natural: PERFORM BYPASS-CONTRACT-DIFFERENT-DATE
            sub_Bypass_Contract_Different_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR13:                                                                                                                                                        //Natural: FOR #J = 1 TO 5
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
            {
                FOR14:                                                                                                                                                    //Natural: FOR #I = 1 TO #COUNT-PE
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I).equals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                     //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = #FUND-A ( #J )
                    {
                        if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                 //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                        {
                            print_Total_Line_Annual_Ia_Units_Annual_Tot_Bypass.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-ANNUAL-TOT-BYPASS ( #J )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            print_Total_Line_Monthly_Ia_Units_Mnthly_Tot_Bypass.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-MNTHLY-TOT-BYPASS ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypass_Contract_Different_Date() throws Exception                                                                                                    //Natural: BYPASS-CONTRACT-DIFFERENT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        FOR15:                                                                                                                                                            //Natural: FOR #J = 1 TO 5
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
        {
            FOR16:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I).equals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                         //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = #FUND-A ( #J )
                {
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        print_Total_Line_Annual_C_Ia_Units_Annual_Tot_Bypass_C.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-ANNUAL-TOT-BYPASS-C ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_Bypass_C.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-MNTHLY-TOT-BYPASS-C ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ==============================
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
    }
    private void sub_Different_Iaxfr_Cycle_Dte() throws Exception                                                                                                         //Natural: DIFFERENT-IAXFR-CYCLE-DTE
    {
        if (BLNatReinput.isReinput()) return;

        FOR17:                                                                                                                                                            //Natural: FOR #J = 1 TO 5
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
        {
            FOR18:                                                                                                                                                        //Natural: FOR #I = 1 TO #COUNT-PE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Count_Pe)); pnd_I.nadd(1))
            {
                if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I).equals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                         //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = #FUND-A ( #J )
                {
                    if (condition(pnd_Ws_Fund.notEquals(pnd_Funds_Pnd_Fund_A.getValue(pnd_J))))                                                                           //Natural: IF #WS-FUND NE #FUND-A ( #J )
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        pnd_Ws_Fund.setValue(pnd_Funds_Pnd_Fund_A.getValue(pnd_J));                                                                                       //Natural: ASSIGN #WS-FUND := #FUND-A ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue(pnd_K).setValue(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Acct_Cd().getValue(pnd_I));    //Natural: ASSIGN CIS-CREF-ACCT-CDE ( #K ) := IAXFR-TO-ACCT-CD ( #I )
                    if (condition(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Unit_Typ().getValue(pnd_I).equals("A")))                                                     //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                    {
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-CREF-ANNUAL-NBR-UNITS ( #K )
                        print_Total_Line_Annual_C_Cis_Units_Annual_Tot_C.getValue(pnd_J).nadd(pnd_Cis_Unit_A.getValue(pnd_I));                                            //Natural: ADD #CIS-UNIT-A ( #I ) TO CIS-UNITS-ANNUAL-TOT-C ( #J )
                        pnd_Iaxfr_Units_A.getValue(pnd_I).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                              //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO #IAXFR-UNITS-A ( #I )
                        print_Total_Line_Annual_C_Ia_Units_Annual_Tot_C.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-ANNUAL-TOT-C ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIatl406.getCis_Prtcpnt_File_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_K).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                  //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-CREF-MNTHLY-NBR-UNITS ( #K )
                        print_Total_Line_Monthly_C_Cis_Units_Mnthly_Tot_C.getValue(pnd_J).nadd(pnd_Cis_Unit_M.getValue(pnd_I));                                           //Natural: ADD #CIS-UNIT-M ( #I ) TO CIS-UNITS-MNTHLY-TOT-C ( #J )
                        pnd_Iaxfr_Units_M.getValue(pnd_I).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I));                              //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO #IAXFR-UNITS-M ( #I )
                        print_Total_Line_Monthly_C_Ia_Units_Mnthly_Tot_C.getValue(pnd_J).nadd(ldaIaal204a.getIaa_Xfr_Audit_View_Iaxfr_To_Aftr_Xfr_Units().getValue(pnd_I)); //Natural: ADD IAXFR-TO-AFTR-XFR-UNITS ( #I ) TO IA-UNITS-MNTHLY-TOT-C ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Compute_Annty_Start_Date() throws Exception                                                                                                          //Natural: COMPUTE-ANNTY-START-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Curr_Proc_Dte.setValueEdited(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CURR-PROC-DTE
        pnd_Curr_Proc_Dte_Pnd_Dd.setValue(1);                                                                                                                             //Natural: ASSIGN #DD := 01
        if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(100)))                                                                              //Natural: IF CNTRCT-MODE-IND = 100
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR19:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet2796 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #MM;//Natural: VALUE 01
            if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(1))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(701)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 701 OR = 801
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(801)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 02
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(2))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(702)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 702 OR = 802
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(802)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 03
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(3))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(703)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 703 OR = 803
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(803)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 04
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(4))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(704)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 704 OR = 804
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(804)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 05
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(5))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(705)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 705 OR = 805
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(805)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(6))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(706)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 706 OR = 806
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(806)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 07
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(7))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(701)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 701 OR = 807
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(807)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 08
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(8))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(702)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 702 OR = 808
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(808)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(9))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(703)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 703 OR = 809
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(809)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(10))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(601) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(704)  //Natural: IF CNTRCT-MODE-IND = 601 OR = 704 OR = 810
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(810)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(11))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(602) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(705)  //Natural: IF CNTRCT-MODE-IND = 602 OR = 705 OR = 811
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(811)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Curr_Proc_Dte_Pnd_Mm.equals(12))))
            {
                decideConditionsMet2796++;
                if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(603) || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(706)  //Natural: IF CNTRCT-MODE-IND = 603 OR = 706 OR = 812
                    || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().equals(812)))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Curr_Proc_Dte_Pnd_Mm.nadd(1);                                                                                                                             //Natural: ADD 1 TO #MM
            if (condition(pnd_Curr_Proc_Dte_Pnd_Mm.greater(12)))                                                                                                          //Natural: IF #MM GT 12
            {
                pnd_Curr_Proc_Dte_Pnd_Mm.setValue(1);                                                                                                                     //Natural: ASSIGN #MM := 01
                pnd_Curr_Proc_Dte_Pnd_Yyyy.nadd(1);                                                                                                                       //Natural: ADD 1 TO #YYYY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Parm_Values() throws Exception                                                                                                                 //Natural: WRITE-PARM-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "CALLING MDMN211A",NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number(), //Natural: WRITE 'CALLING MDMN211A' / '=' #I-TIAA-CREF-IND / '=' #I-CONTRACT-NUMBER / '=' #I-PAYEE-CODE / '=' #MDMA211.#O-RETURN-CODE / '=' #MDMA211.#O-RETURN-TEXT
            NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code(),NEWLINE,"=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATU(),new ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATU 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM' 12X 'PAGE       :  ' *PAGE-NUMBER ( 1 ) 01X 'RUN TIME : ' *TIMX 33X 'CIS NEW ISSUE REPORT' 29X 'PROGRAM ID : ' *PROGRAM /
                        ColumnSpacing(12),"PAGE       :  ",getReports().getPageNumberDbs(1),new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(33),"CIS NEW ISSUE REPORT",new 
                        ColumnSpacing(29),"PROGRAM ID : ",Global.getPROGRAM(),NEWLINE);
                    if (condition(pnd_Total_Sw.equals("Y")))                                                                                                              //Natural: IF #TOTAL-SW = 'Y'
                    {
                        getReports().write(1, ReportOption.NOTITLE,"CERT-NBR",new ColumnSpacing(3),"PAYEE CD ",new ColumnSpacing(1),"UNIT TYPE ",new ColumnSpacing(1)," FUND",new  //Natural: WRITE ( 1 ) 'CERT-NBR' 3X 'PAYEE CD ' 1X 'UNIT TYPE ' 1X ' FUND' 4X '  IA UNITS ' 4X '    CIS UNITS  ' 1X 'INCOME OPTION ' 1X ' GUARANTEED YEARS  ' 1X ' GUARANTEED DAYS'
                            ColumnSpacing(4),"  IA UNITS ",new ColumnSpacing(4),"    CIS UNITS  ",new ColumnSpacing(1),"INCOME OPTION ",new ColumnSpacing(1)," GUARANTEED YEARS  ",new 
                            ColumnSpacing(1)," GUARANTEED DAYS");
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ERROR REPORT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATX(), new ReportEditMask                 //Natural: WRITE ( 2 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATX ( EM = MM/DD/YYYY ) 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM' 12X 'PAGE       :  ' *PAGE-NUMBER ( 2 ) 01X 'RUN TIME : ' *TIMX 33X 'CIS NEW ISSUE ERROR REPORT FOR' 20X 'PROGRAM ID : ' *PROGRAM /
                        ("MM/DD/YYYY"),new ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM",new ColumnSpacing(12),"PAGE       :  ",getReports().getPageNumberDbs(2),new 
                        ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new ColumnSpacing(33),"CIS NEW ISSUE ERROR REPORT FOR",new ColumnSpacing(20),"PROGRAM ID : ",
                        Global.getPROGRAM(),NEWLINE);
                    //*    27X 'PROGRAM ID : ' *PROGRAM /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
