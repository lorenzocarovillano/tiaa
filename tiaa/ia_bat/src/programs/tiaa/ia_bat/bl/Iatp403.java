/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:12 PM
**        * FROM NATURAL PROGRAM : Iatp403
************************************************************
**        * FILE NAME            : Iatp403.java
**        * CLASS NAME           : Iatp403
**        * INSTANCE NAME        : Iatp403
************************************************************
************************************************************************
*
*   PROGRAM    : IATP403 (CLONED FROM IATN421V)
*   SYSTEM     : IA
*
*   FUNCTION   : CREATE THE FILE FOR INPUT TO CTLS.  THIS REPLACES
*                IATN421V AS PART OF CALM RETIREMENT.
*
*   MAINTENANCE:
*
* NAME          DATE       DESCRIPTION
* ----          ----       -----------
* O. SOTTO     03/25/15    INITIAL CREATION.
* J. BREMER    08/26/16    RESTOW FOR IAALCTLS PIN EXPANSION
* O. SOTTO     04/2017     PIN EXPANSION - SC 082017 FOR CHANGES.
* J. TINIO     03/2020     FIX LEDGERS FOR MANY TO ONE AND MANY TO
*                          MANY TRANSFERS - SC 032020 FOR CHANGES.
* J. TINIO     05/2020     RESET STANDARD TO GRADED FLAG AFTER EVERY
*                          REQUEST PROCESSED - SC 052020 FOR CHANGES.
* J. TINIO     02/2021     ALIGN THE AUDIT AND TRANSFER RECORDS IF
*                          PROCESSING DUPLICATE RQST-ID - SC 022021
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp403 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;
    private PdaIaaactls pdaIaaactls;
    private LdaIaalctls ldaIaalctls;
    private PdaIatl400p pdaIatl400p;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Payee;

    private DataAccessProgramView vw_audit;
    private DbsField audit_Rcrd_Type_Cde;
    private DbsField audit_Rqst_Id;

    private DbsGroup audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField audit_Iaxfr_Frm_Acct_Cd;
    private DbsField audit_Iaxfr_From_Asset_Xfr_Amt;

    private DbsGroup audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField audit_Iaxfr_To_Acct_Cd;
    private DbsField audit_Iaxfr_To_Asset_Amt;
    private DbsField audit_Iaxfr_To_Qty;

    private DataAccessProgramView vw_pnd_Ext_Fund;
    private DbsField pnd_Ext_Fund_Nec_Table_Cde;
    private DbsField pnd_Ext_Fund_Nec_Ticker_Symbol;
    private DbsField pnd_Ext_Fund_Nec_Act_Class_Cde;
    private DbsField pnd_Ext_Fund_Nec_Act_Invesment_Typ;
    private DbsField pnd_Ext_Fund_Nec_Investment_Grouping_Id;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol;
    private DbsField pnd_Pin_Cntrct_Payee_Key;

    private DbsGroup pnd_Pin_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Switch;
    private DbsField pnd_Debug;
    private DbsField pnd_End_Of_Month;
    private DbsField pnd_Grd_To_Std;
    private DbsField pnd_Fr_Tiaa;
    private DbsField pnd_To_Tiaa;
    private DbsField pnd_First_Call;
    private DbsField pnd_Annty_Certain;
    private DbsField pnd_Deposit_Acctg;
    private DbsField pnd_Leap_Year;
    private DbsField pnd_Tot_Trans_Amt;
    private DbsField pnd_Actl_Amt;
    private DbsField pnd_Dx;
    private DbsField pnd_Fund_Cnt;
    private DbsField pnd_Stts_Cde;
    private DbsField pnd_Res_Cde;
    private DbsField pnd_Payee_Code;
    private DbsField pnd_Settl_Ind;
    private DbsField pnd_Work_Date_X;
    private DbsField pnd_Work_Date;

    private DbsGroup pnd_Work_Date__R_Field_3;
    private DbsField pnd_Work_Date_Pnd_W_Yyyy;
    private DbsField pnd_Work_Date_Pnd_W_Mm;
    private DbsField pnd_Work_Date_Pnd_W_Dd;

    private DbsGroup pnd_Work_Date__R_Field_4;
    private DbsField pnd_Work_Date_Pnd_W_Yy;
    private DbsField pnd_Work_Date_Pnd_W_Mmdd;
    private DbsField pnd_Dte_Alpha;

    private DbsGroup pnd_Dte_Alpha__R_Field_5;
    private DbsField pnd_Dte_Alpha_Pnd_Dte;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Day;
    private DbsField pnd_Fr_Ticker;
    private DbsField pnd_To_Ticker;
    private DbsField pnd_Ws_Ticker;
    private DbsField pnd_Contract_Cert;
    private DbsField pnd_Fr_Contract_Pair;
    private DbsField pnd_To_Contract_Pair;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Tckr;

    private DbsGroup pnd_Stable_Value_Funds;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_403b;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_401k;
    private DbsField pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Rc;
    private DbsField pnd_Stable_Value_Funds_Pnd_Rafslash_Gra;

    private DbsGroup pnd_Stable_Value_Funds__R_Field_6;
    private DbsField pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes;
    private DbsField pnd_Grdd_Sub_Lob;
    private DbsField pnd_Stdd_Sub_Lob;
    private DbsField pnd_Tiaa_And_Access;
    private DbsField pnd_Tiaa_Fnd_Cdes;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_7;
    private DbsField pnd_W_Date_Pnd_W_Date_A;
    private DbsField pnd_Datd;
    private DbsField pnd_Naz_Tbl_Super1;

    private DbsGroup pnd_Naz_Tbl_Super1__R_Field_8;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField pnd_From_Tckr;
    private DbsField pnd_To_Acct;
    private DbsField pnd_Detail_Record;

    private DbsGroup pnd_Detail_Record__R_Field_9;
    private DbsField pnd_Detail_Record_Pnd_Fill;
    private DbsField pnd_Detail_Record_Pnd_Dtl_End;

    private DbsGroup pnd_Detail_Record__R_Field_10;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rcrd_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Sqnce;
    private DbsField pnd_Detail_Record_Pnd_Trans_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Typ_Rcrd;
    private DbsField pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rte_Basis;
    private DbsField pnd_Detail_Record_Pnd_Trans_Undldgr_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde;
    private DbsField pnd_Detail_Record_Pnd_Trans_Dept_Id;
    private DbsField pnd_Detail_Record_Pnd_Trans_Allctn_Ndx;
    private DbsField pnd_Detail_Record_Pnd_Trans_Vltn_Basis;
    private DbsField pnd_Detail_Record_Pnd_Trans_Amt_Sign;
    private DbsField pnd_Detail_Record_Pnd_Trans_Dtl_Amt;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Tkr;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Sub_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Tiaa_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Cref_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Num_Units;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Tkr;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Sub_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Tiaa_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Cref_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Num_Units;
    private DbsField pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ldgr_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ind2;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Tax_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte;
    private DbsField pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Subplan;
    private DbsField pnd_Input;
    private DbsField pnd_Ctls_Seq_Num;
    private DbsField pnd_Begin_Timn;

    private DbsGroup pnd_Begin_Timn__R_Field_11;
    private DbsField pnd_Begin_Timn_Pnd_Begin_Time;
    private DbsField pnd_End_Timn;

    private DbsGroup pnd_End_Timn__R_Field_12;
    private DbsField pnd_End_Timn_Pnd_End_Time;
    private DbsField pnd_Dup_Cnt;
    private DbsField pnd_Aud_Cnt;
    private DbsField pnd_Rqst_Ids;
    private DbsField pnd_R_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaIaaactls = new PdaIaaactls(localVariables);
        ldaIaalctls = new LdaIaalctls();
        registerRecord(ldaIaalctls);
        pdaIatl400p = new PdaIatl400p(localVariables);

        // Local Variables

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        vw_audit = new DataAccessProgramView(new NameInfo("vw_audit", "AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        audit_Rcrd_Type_Cde = vw_audit.getRecord().newFieldInGroup("audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        audit_Rqst_Id = vw_audit.getRecord().newFieldInGroup("audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, "RQST_ID");

        audit_Iaxfr_Calc_From_Acct_Data = vw_audit.getRecord().newGroupInGroup("audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        audit_Iaxfr_Frm_Acct_Cd = audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        audit_Iaxfr_From_Asset_Xfr_Amt = audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("audit_Iaxfr_From_Asset_Xfr_Amt", "IAXFR-FROM-ASSET-XFR-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_ASSET_XFR_AMT", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        audit_Iaxfr_Calc_To_Acct_Data = vw_audit.getRecord().newGroupInGroup("audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        audit_Iaxfr_To_Acct_Cd = audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", FieldType.STRING, 1, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        audit_Iaxfr_To_Asset_Amt = audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        audit_Iaxfr_To_Qty = audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        registerRecord(vw_audit);

        vw_pnd_Ext_Fund = new DataAccessProgramView(new NameInfo("vw_pnd_Ext_Fund", "#EXT-FUND"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        pnd_Ext_Fund_Nec_Table_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        pnd_Ext_Fund_Nec_Ticker_Symbol = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        pnd_Ext_Fund_Nec_Act_Class_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Act_Class_Cde", "NEC-ACT-CLASS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_ACT_CLASS_CDE");
        pnd_Ext_Fund_Nec_Act_Invesment_Typ = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Act_Invesment_Typ", "NEC-ACT-INVESMENT-TYP", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ACT_INVESMENT_TYP");
        pnd_Ext_Fund_Nec_Investment_Grouping_Id = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_pnd_Ext_Fund);

        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_1", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol = pnd_Nec_Fnd_Super1__R_Field_1.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol", "#NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10);
        pnd_Pin_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Payee_Key", "#PIN-CNTRCT-PAYEE-KEY", FieldType.STRING, 24);

        pnd_Pin_Cntrct_Payee_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Pin_Cntrct_Payee_Key);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr", "#PIN-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Pin_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Pin_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Switch = localVariables.newFieldInRecord("pnd_Switch", "#SWITCH", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_End_Of_Month = localVariables.newFieldInRecord("pnd_End_Of_Month", "#END-OF-MONTH", FieldType.BOOLEAN, 1);
        pnd_Grd_To_Std = localVariables.newFieldInRecord("pnd_Grd_To_Std", "#GRD-TO-STD", FieldType.BOOLEAN, 1);
        pnd_Fr_Tiaa = localVariables.newFieldInRecord("pnd_Fr_Tiaa", "#FR-TIAA", FieldType.BOOLEAN, 1);
        pnd_To_Tiaa = localVariables.newFieldInRecord("pnd_To_Tiaa", "#TO-TIAA", FieldType.BOOLEAN, 1);
        pnd_First_Call = localVariables.newFieldInRecord("pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);
        pnd_Annty_Certain = localVariables.newFieldInRecord("pnd_Annty_Certain", "#ANNTY-CERTAIN", FieldType.BOOLEAN, 1);
        pnd_Deposit_Acctg = localVariables.newFieldInRecord("pnd_Deposit_Acctg", "#DEPOSIT-ACCTG", FieldType.BOOLEAN, 1);
        pnd_Leap_Year = localVariables.newFieldInRecord("pnd_Leap_Year", "#LEAP-YEAR", FieldType.BOOLEAN, 1);
        pnd_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt", "#TOT-TRANS-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Actl_Amt = localVariables.newFieldInRecord("pnd_Actl_Amt", "#ACTL-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Dx = localVariables.newFieldInRecord("pnd_Dx", "#DX", FieldType.INTEGER, 4);
        pnd_Fund_Cnt = localVariables.newFieldInRecord("pnd_Fund_Cnt", "#FUND-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Stts_Cde = localVariables.newFieldInRecord("pnd_Stts_Cde", "#STTS-CDE", FieldType.STRING, 3);
        pnd_Res_Cde = localVariables.newFieldInRecord("pnd_Res_Cde", "#RES-CDE", FieldType.STRING, 2);
        pnd_Payee_Code = localVariables.newFieldInRecord("pnd_Payee_Code", "#PAYEE-CODE", FieldType.NUMERIC, 2);
        pnd_Settl_Ind = localVariables.newFieldInRecord("pnd_Settl_Ind", "#SETTL-IND", FieldType.STRING, 1);
        pnd_Work_Date_X = localVariables.newFieldInRecord("pnd_Work_Date_X", "#WORK-DATE-X", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);

        pnd_Work_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_3", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yyyy = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mm = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_Work_Date_Pnd_W_Dd = pnd_Work_Date__R_Field_3.newFieldInGroup("pnd_Work_Date_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);

        pnd_Work_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_4", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yy = pnd_Work_Date__R_Field_4.newFieldInGroup("pnd_Work_Date_Pnd_W_Yy", "#W-YY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mmdd = pnd_Work_Date__R_Field_4.newFieldInGroup("pnd_Work_Date_Pnd_W_Mmdd", "#W-MMDD", FieldType.NUMERIC, 4);
        pnd_Dte_Alpha = localVariables.newFieldInRecord("pnd_Dte_Alpha", "#DTE-ALPHA", FieldType.STRING, 8);

        pnd_Dte_Alpha__R_Field_5 = localVariables.newGroupInRecord("pnd_Dte_Alpha__R_Field_5", "REDEFINE", pnd_Dte_Alpha);
        pnd_Dte_Alpha_Pnd_Dte = pnd_Dte_Alpha__R_Field_5.newFieldInGroup("pnd_Dte_Alpha_Pnd_Dte", "#DTE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 4);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Fr_Ticker = localVariables.newFieldInRecord("pnd_Fr_Ticker", "#FR-TICKER", FieldType.STRING, 10);
        pnd_To_Ticker = localVariables.newFieldInRecord("pnd_To_Ticker", "#TO-TICKER", FieldType.STRING, 10);
        pnd_Ws_Ticker = localVariables.newFieldInRecord("pnd_Ws_Ticker", "#WS-TICKER", FieldType.STRING, 10);
        pnd_Contract_Cert = localVariables.newFieldInRecord("pnd_Contract_Cert", "#CONTRACT-CERT", FieldType.STRING, 10);
        pnd_Fr_Contract_Pair = localVariables.newFieldInRecord("pnd_Fr_Contract_Pair", "#FR-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_To_Contract_Pair = localVariables.newFieldInRecord("pnd_To_Contract_Pair", "#TO-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Tckr = localVariables.newFieldInRecord("pnd_Tckr", "#TCKR", FieldType.STRING, 10);

        pnd_Stable_Value_Funds = localVariables.newGroupInRecord("pnd_Stable_Value_Funds", "#STABLE-VALUE-FUNDS");
        pnd_Stable_Value_Funds_Pnd_Roth_403b = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_403b", "#ROTH-403B", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc", "#ROTH-403B-RC", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_401k = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_401k", "#ROTH-401K", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc", "#ROTH-401K-RC", FieldType.NUMERIC, 
            2);
        pnd_Stable_Value_Funds_Pnd_Rc = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Stable_Value_Funds_Pnd_Rafslash_Gra = pnd_Stable_Value_Funds.newFieldInGroup("pnd_Stable_Value_Funds_Pnd_Rafslash_Gra", "#RA/GRA", FieldType.NUMERIC, 
            2);

        pnd_Stable_Value_Funds__R_Field_6 = localVariables.newGroupInRecord("pnd_Stable_Value_Funds__R_Field_6", "REDEFINE", pnd_Stable_Value_Funds);
        pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes = pnd_Stable_Value_Funds__R_Field_6.newFieldArrayInGroup("pnd_Stable_Value_Funds_Pnd_Stable_Org_Cdes", 
            "#STABLE-ORG-CDES", FieldType.NUMERIC, 2, new DbsArrayController(1, 6));
        pnd_Grdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Grdd_Sub_Lob", "#GRDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Stdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Stdd_Sub_Lob", "#STDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Tiaa_And_Access = localVariables.newFieldArrayInRecord("pnd_Tiaa_And_Access", "#TIAA-AND-ACCESS", FieldType.STRING, 1, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Fnd_Cdes = localVariables.newFieldArrayInRecord("pnd_Tiaa_Fnd_Cdes", "#TIAA-FND-CDES", FieldType.STRING, 1, new DbsArrayController(1, 
            4));

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);

        pnd_W_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_7", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Date_A = pnd_W_Date__R_Field_7.newFieldInGroup("pnd_W_Date_Pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Naz_Tbl_Super1 = localVariables.newFieldInRecord("pnd_Naz_Tbl_Super1", "#NAZ-TBL-SUPER1", FieldType.STRING, 29);

        pnd_Naz_Tbl_Super1__R_Field_8 = localVariables.newGroupInRecord("pnd_Naz_Tbl_Super1__R_Field_8", "REDEFINE", pnd_Naz_Tbl_Super1);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", "#NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", "#NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Tbl_Super1__R_Field_8.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", "#NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_From_Tckr = localVariables.newFieldInRecord("pnd_From_Tckr", "#FROM-TCKR", FieldType.STRING, 10);
        pnd_To_Acct = localVariables.newFieldArrayInRecord("pnd_To_Acct", "#TO-ACCT", FieldType.STRING, 1, new DbsArrayController(1, 200));
        pnd_Detail_Record = localVariables.newFieldInRecord("pnd_Detail_Record", "#DETAIL-RECORD", FieldType.STRING, 250);

        pnd_Detail_Record__R_Field_9 = localVariables.newGroupInRecord("pnd_Detail_Record__R_Field_9", "REDEFINE", pnd_Detail_Record);
        pnd_Detail_Record_Pnd_Fill = pnd_Detail_Record__R_Field_9.newFieldInGroup("pnd_Detail_Record_Pnd_Fill", "#FILL", FieldType.STRING, 249);
        pnd_Detail_Record_Pnd_Dtl_End = pnd_Detail_Record__R_Field_9.newFieldInGroup("pnd_Detail_Record_Pnd_Dtl_End", "#DTL-END", FieldType.STRING, 1);

        pnd_Detail_Record__R_Field_10 = localVariables.newGroupInRecord("pnd_Detail_Record__R_Field_10", "REDEFINE", pnd_Detail_Record);
        pnd_Detail_Record_Pnd_Trans_Rcrd_Typ = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rcrd_Typ", "#TRANS-RCRD-TYP", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Sqnce = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Sqnce", "#TRANS-SQNCE", FieldType.NUMERIC, 
            9);
        pnd_Detail_Record_Pnd_Trans_Typ = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Typ", "#TRANS-TYP", FieldType.STRING, 
            2);
        pnd_Detail_Record_Pnd_Trans_Typ_Rcrd = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Typ_Rcrd", "#TRANS-TYP-RCRD", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr", "#TRANS-OCCRNCE-NBR", 
            FieldType.NUMERIC, 4);
        pnd_Detail_Record_Pnd_Trans_Rte_Basis = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rte_Basis", "#TRANS-RTE-BASIS", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Undldgr_Ind = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Undldgr_Ind", "#TRANS-UNDLDGR-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde", "#TRANS-RVRSL-CDE", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Dept_Id = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Dept_Id", "#TRANS-DEPT-ID", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_Allctn_Ndx = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Allctn_Ndx", "#TRANS-ALLCTN-NDX", 
            FieldType.STRING, 6);
        pnd_Detail_Record_Pnd_Trans_Vltn_Basis = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Vltn_Basis", "#TRANS-VLTN-BASIS", 
            FieldType.STRING, 5);
        pnd_Detail_Record_Pnd_Trans_Amt_Sign = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Amt_Sign", "#TRANS-AMT-SIGN", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Dtl_Amt = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Dtl_Amt", "#TRANS-DTL-AMT", FieldType.NUMERIC, 
            16, 2);
        pnd_Detail_Record_Pnd_Trans_From_Tkr = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Tkr", "#TRANS-FROM-TKR", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Lob = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Lob", "#TRANS-FROM-LOB", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Sub_Lob = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Sub_Lob", "#TRANS-FROM-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Detail_Record_Pnd_Trans_From_Tiaa_No = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Tiaa_No", "#TRANS-FROM-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Cref_No = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Cref_No", "#TRANS-FROM-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Num_Units = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Num_Units", "#TRANS-FROM-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Detail_Record_Pnd_Trans_To_Tkr = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Tkr", "#TRANS-TO-TKR", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_To_Lob = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Lob", "#TRANS-TO-LOB", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_To_Sub_Lob = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Sub_Lob", "#TRANS-TO-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Detail_Record_Pnd_Trans_To_Tiaa_No = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Tiaa_No", "#TRANS-TO-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_To_Cref_No = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Cref_No", "#TRANS-TO-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_To_Num_Units = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Num_Units", "#TRANS-TO-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id", "#TRANS-PITS-RQST-ID", 
            FieldType.STRING, 29);
        pnd_Detail_Record_Pnd_Trans_Ldgr_Typ = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ldgr_Typ", "#TRANS-LDGR-TYP", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw", "#TRANS-CNCL-RDRW", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Ind2 = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ind2", "#TRANS-IND2", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Ind = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ind", "#TRANS-IND", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Tax_Ind = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Tax_Ind", "#TRANS-TAX-IND", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind", "#TRANS-NET-CASH-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte", "#TRANS-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind", "#TRANS-1ST-YR-RNWL-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Subplan = pnd_Detail_Record__R_Field_10.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Subplan", "#TRANS-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Input = localVariables.newFieldInRecord("pnd_Input", "#INPUT", FieldType.STRING, 34);
        pnd_Ctls_Seq_Num = localVariables.newFieldInRecord("pnd_Ctls_Seq_Num", "#CTLS-SEQ-NUM", FieldType.NUMERIC, 9);
        pnd_Begin_Timn = localVariables.newFieldInRecord("pnd_Begin_Timn", "#BEGIN-TIMN", FieldType.NUMERIC, 7);

        pnd_Begin_Timn__R_Field_11 = localVariables.newGroupInRecord("pnd_Begin_Timn__R_Field_11", "REDEFINE", pnd_Begin_Timn);
        pnd_Begin_Timn_Pnd_Begin_Time = pnd_Begin_Timn__R_Field_11.newFieldInGroup("pnd_Begin_Timn_Pnd_Begin_Time", "#BEGIN-TIME", FieldType.NUMERIC, 
            6);
        pnd_End_Timn = localVariables.newFieldInRecord("pnd_End_Timn", "#END-TIMN", FieldType.NUMERIC, 7);

        pnd_End_Timn__R_Field_12 = localVariables.newGroupInRecord("pnd_End_Timn__R_Field_12", "REDEFINE", pnd_End_Timn);
        pnd_End_Timn_Pnd_End_Time = pnd_End_Timn__R_Field_12.newFieldInGroup("pnd_End_Timn_Pnd_End_Time", "#END-TIME", FieldType.NUMERIC, 6);
        pnd_Dup_Cnt = localVariables.newFieldInRecord("pnd_Dup_Cnt", "#DUP-CNT", FieldType.INTEGER, 2);
        pnd_Aud_Cnt = localVariables.newFieldInRecord("pnd_Aud_Cnt", "#AUD-CNT", FieldType.INTEGER, 2);
        pnd_Rqst_Ids = localVariables.newFieldArrayInRecord("pnd_Rqst_Ids", "#RQST-IDS", FieldType.STRING, 34, new DbsArrayController(1, 5000));
        pnd_R_Cnt = localVariables.newFieldInRecord("pnd_R_Cnt", "#R-CNT", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Trnsfr_Sw_Rqst.reset();
        vw_audit.reset();
        vw_pnd_Ext_Fund.reset();
        vw_naz_Table_Ddm.reset();

        ldaIaalctls.initializeValues();

        localVariables.reset();
        pnd_First_Call.setInitialValue(true);
        pnd_Dx.setInitialValue(1);
        pnd_Fund_Cnt.setInitialValue(20);
        pnd_Stable_Value_Funds_Pnd_Roth_403b.setInitialValue(24);
        pnd_Stable_Value_Funds_Pnd_Roth_403b_Rc.setInitialValue(25);
        pnd_Stable_Value_Funds_Pnd_Roth_401k.setInitialValue(26);
        pnd_Stable_Value_Funds_Pnd_Roth_401k_Rc.setInitialValue(27);
        pnd_Stable_Value_Funds_Pnd_Rc.setInitialValue(28);
        pnd_Stable_Value_Funds_Pnd_Rafslash_Gra.setInitialValue(29);
        pnd_Grdd_Sub_Lob.setInitialValue("GBPM-IA");
        pnd_Stdd_Sub_Lob.setInitialValue("IA");
        pnd_Tiaa_And_Access.getValue(1).setInitialValue("T");
        pnd_Tiaa_And_Access.getValue(2).setInitialValue("G");
        pnd_Tiaa_And_Access.getValue(3).setInitialValue("D");
        pnd_Tiaa_Fnd_Cdes.getValue(1).setInitialValue("T");
        pnd_Tiaa_Fnd_Cdes.getValue(2).setInitialValue("G");
        pnd_Tiaa_Fnd_Cdes.getValue(3).setInitialValue("R");
        pnd_Tiaa_Fnd_Cdes.getValue(4).setInitialValue("D");
        pnd_Ctls_Seq_Num.setInitialValue(200000000);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp403() throws Exception
    {
        super("Iatp403");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Debug.reset();                                                                                                                                                //Natural: RESET #DEBUG
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = 'NAZ021UTLUTL3'
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", "NAZ021UTLUTL3", WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.equals("TEST")))                                                                                         //Natural: IF NAZ-TBL-RCRD-DSCRPTN-TXT = 'TEST'
            {
                pnd_Debug.setValue(true);                                                                                                                                 //Natural: ASSIGN #DEBUG := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Debug.setValue(false);                                                                                                                                //Natural: ASSIGN #DEBUG := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATES
        sub_Get_Control_Dates();
        if (condition(Global.isEscape())) {return;}
        pnd_Begin_Timn.setValue(Global.getTIMN());                                                                                                                        //Natural: ASSIGN #BEGIN-TIMN := *TIMN
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            //*  052020
            pnd_Grd_To_Std.reset();                                                                                                                                       //Natural: RESET #GRD-TO-STD
            //*  022021 TO ALIGN AUDIT AND TRNSF IF DUPS
            pnd_Dup_Cnt.reset();                                                                                                                                          //Natural: RESET #DUP-CNT #AUD-CNT
            pnd_Aud_Cnt.reset();
            //*  022021 - START
            //*  TO ALIGN AUDIT AND TRNSF IF DUPLICATE
            pnd_Dup_Cnt.reset();                                                                                                                                          //Natural: RESET #DUP-CNT
            //*  DUPLICATE - ALREADY PROCESSED
            if (condition(pnd_Input.equals(pnd_Rqst_Ids.getValue("*"))))                                                                                                  //Natural: IF #INPUT = #RQST-IDS ( * )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_R_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #R-CNT
                pnd_Rqst_Ids.getValue(pnd_R_Cnt).setValue(pnd_Input);                                                                                                     //Natural: ASSIGN #RQST-IDS ( #R-CNT ) := #INPUT
            }                                                                                                                                                             //Natural: END-IF
            //*  022021 - END
            vw_iaa_Trnsfr_Sw_Rqst.startDatabaseFind                                                                                                                       //Natural: FIND IAA-TRNSFR-SW-RQST WITH IAA-TRNSFR-SW-RQST.RQST-ID = #INPUT
            (
            "FIND02",
            new Wc[] { new Wc("RQST_ID", "=", pnd_Input, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("FIND02", true)))
            {
                vw_iaa_Trnsfr_Sw_Rqst.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Trnsfr_Sw_Rqst.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "*** RQST-ID ",pnd_Input," not found !!***");                                                                                   //Natural: WRITE '*** RQST-ID ' #INPUT ' not found !!***'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals("2")))                                                                                              //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE = '2'
                {
                    pnd_Switch.setValue(true);                                                                                                                            //Natural: ASSIGN #SWITCH := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Switch.setValue(false);                                                                                                                           //Natural: ASSIGN #SWITCH := FALSE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Annty_Certain.reset();                                                                                                                                //Natural: RESET #ANNTY-CERTAIN #RES-CDE #CTLS-HDR-RECORD #CTLS-TRANS-RECORD ( * ) #CTLS-TRANS-CNT #TOT-TRANS-AMT #FR-CONTRACT-PAIR #TO-CONTRACT-PAIR
                pnd_Res_Cde.reset();
                ldaIaalctls.getPnd_Ctls_Hdr_Record().reset();
                ldaIaalctls.getPnd_Ctls_Trans_Record().getValue("*").reset();
                ldaIaalctls.getPnd_Ctls_Trans_Cnt().reset();
                pnd_Tot_Trans_Amt.reset();
                pnd_Fr_Contract_Pair.reset();
                pnd_To_Contract_Pair.reset();
                vw_iaa_Cntrct.startDatabaseFind                                                                                                                           //Natural: FIND IAA-CNTRCT WITH IAA-CNTRCT.CNTRCT-PPCN-NBR = IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
                (
                "FIND03",
                new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct, WcType.WITH) }
                );
                FIND03:
                while (condition(vw_iaa_Cntrct.readNextRow("FIND03")))
                {
                    vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
                    if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(21)))                                                                                                 //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
                    {
                        pnd_Annty_Certain.setValue(true);                                                                                                                 //Natural: ASSIGN #ANNTY-CERTAIN := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Call.setValue(true);                                                                                                                            //Natural: ASSIGN #FIRST-CALL := TRUE
                pnd_Contract_Cert.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                                             //Natural: ASSIGN #CONTRACT-CERT := IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-OR-CERT
                sub_Get_Contract_Or_Cert();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Call.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-CALL := FALSE
                pnd_Contract_Cert.setValue(iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct);                                                                                              //Natural: ASSIGN #CONTRACT-CERT := IAA-TRNSFR-SW-RQST.IA-TO-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-OR-CERT
                sub_Get_Contract_Or_Cert();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  022021 TO MATCH AUDIT/TRNSF RECORDS IF DUPS
                pnd_Dup_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DUP-CNT
                pnd_Aud_Cnt.reset();                                                                                                                                      //Natural: RESET #AUD-CNT
                vw_audit.startDatabaseFind                                                                                                                                //Natural: FIND AUDIT WITH AUDIT.RQST-ID = #INPUT
                (
                "FIND04",
                new Wc[] { new Wc("RQST_ID", "=", pnd_Input, WcType.WITH) }
                );
                FIND04:
                while (condition(vw_audit.readNextRow("FIND04", true)))
                {
                    vw_audit.setIfNotFoundControlFlag(false);
                    if (condition(vw_audit.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, "*** RQST-ID ",pnd_Input," audit not found !!***");                                                                         //Natural: WRITE '*** RQST-ID ' #INPUT ' audit not found !!***'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*  022021 - START
                    pnd_Aud_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #AUD-CNT
                    //*  ENSURE THE AUDIT AND TRNSF ALIGN
                    if (condition(pnd_Aud_Cnt.equals(pnd_Dup_Cnt)))                                                                                                       //Natural: IF #AUD-CNT = #DUP-CNT
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  022021 - END
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ctls_Seq_Num.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTLS-SEQ-NUM
                if (condition(! (pnd_Switch.getBoolean())))                                                                                                               //Natural: IF NOT #SWITCH
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 C*XFR-FRM-ACCT-DTA
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta)); pnd_I.nadd(1))
                    {
                        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" ")))                                                                   //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Fund_Cde.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I));                                                                       //Natural: ASSIGN #FUND-CDE := IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
                        sub_Get_Ticker_Symbol();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Tckr.notEquals(" ")))                                                                                                           //Natural: IF #TCKR NE ' '
                        {
                            pnd_From_Tckr.setValue(pnd_Tckr);                                                                                                             //Natural: ASSIGN #FROM-TCKR := #TCKR
                            pnd_Fr_Ticker.setValue(pnd_Tckr);                                                                                                             //Natural: ASSIGN #FR-TICKER := #TCKR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "INVALID FROM ACCT CODE",iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I));                                          //Natural: WRITE 'INVALID FROM ACCT CODE' IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                       //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = #TIAA-FND-CDES ( * )
                        {
                            pnd_Fr_Tiaa.setValue(true);                                                                                                                   //Natural: ASSIGN #FR-TIAA := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fr_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #FR-TIAA := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        FOR02:                                                                                                                                            //Natural: FOR #X = 1 TO C*XFR-TO-ACCT-DTA
                        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta)); pnd_X.nadd(1))
                        {
                            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals(" ")))                                                                //Natural: IF IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = ' '
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                    //Natural: IF IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = #TIAA-FND-CDES ( * )
                            {
                                pnd_To_Tiaa.setValue(true);                                                                                                               //Natural: ASSIGN #TO-TIAA := TRUE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_To_Tiaa.setValue(false);                                                                                                              //Natural: ASSIGN #TO-TIAA := FALSE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Annty_Certain.getBoolean()))                                                                                                //Natural: IF #ANNTY-CERTAIN
                            {
                                if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals(pnd_Tiaa_And_Access.getValue("*"))))                              //Natural: IF IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = #TIAA-AND-ACCESS ( * )
                                {
                                    pnd_Deposit_Acctg.setValue(true);                                                                                                     //Natural: ASSIGN #DEPOSIT-ACCTG := TRUE
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Deposit_Acctg.setValue(false);                                                                                                    //Natural: ASSIGN #DEPOSIT-ACCTG := FALSE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Fund_Cde.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X));                                                                    //Natural: ASSIGN #FUND-CDE := IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X )
                            pnd_Tckr.reset();                                                                                                                             //Natural: RESET #TCKR #SETTL-IND
                            pnd_Settl_Ind.reset();
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
                            sub_Get_Ticker_Symbol();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(pnd_Tckr.notEquals(" ")))                                                                                                       //Natural: IF #TCKR NE ' '
                            {
                                pnd_To_Ticker.setValue(pnd_Tckr);                                                                                                         //Natural: ASSIGN #TO-TICKER := #TCKR
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(0, Global.getPROGRAM());                                                                                               //Natural: WRITE *PROGRAM
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "INVALID TO ACCT CODE",iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X),pnd_X);                                   //Natural: WRITE 'INVALID TO ACCT CODE' IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) #X
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                DbsUtil.terminate(99);  if (true) return;                                                                                                 //Natural: TERMINATE 99
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals("G") && iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals("T"))) //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = 'G' AND IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = 'T'
                            {
                                pnd_Grd_To_Std.setValue(true);                                                                                                            //Natural: ASSIGN #GRD-TO-STD := TRUE
                                pnd_Settl_Ind.setValue(" ");                                                                                                              //Natural: ASSIGN #SETTL-IND := ' '
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                short decideConditionsMet1423 = 0;                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = 'T' OR = 'G'
                                if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals("T") || iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals("G")))
                                {
                                    decideConditionsMet1423++;
                                    pnd_Settl_Ind.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ.getValue(pnd_I));                                                          //Natural: ASSIGN #SETTL-IND := IAA-TRNSFR-SW-RQST.XFR-FRM-UNIT-TYP ( #I )
                                }                                                                                                                                         //Natural: WHEN NONE
                                else if (condition())
                                {
                                    pnd_Settl_Ind.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ.getValue(pnd_X));                                                           //Natural: ASSIGN #SETTL-IND := IAA-TRNSFR-SW-RQST.XFR-TO-UNIT-TYP ( #X )
                                }                                                                                                                                         //Natural: END-DECIDE
                            }                                                                                                                                             //Natural: END-IF
                            //*  032020 START
                            //*          #ACTL-AMT := AUDIT.IAXFR-TO-ASSET-AMT (#X)
                            if (condition(iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta.equals(1)))                                                                       //Natural: IF C*XFR-FRM-ACCT-DTA = 1
                            {
                                pnd_Actl_Amt.setValue(audit_Iaxfr_To_Asset_Amt.getValue(pnd_X));                                                                          //Natural: ASSIGN #ACTL-AMT := AUDIT.IAXFR-TO-ASSET-AMT ( #X )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Actl_Amt.compute(new ComputeParameters(true, pnd_Actl_Amt), (audit_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I).multiply(audit_Iaxfr_To_Qty.getValue(pnd_X))).divide(100)); //Natural: COMPUTE ROUNDED #ACTL-AMT = ( IAXFR-FROM-ASSET-XFR-AMT ( #I ) * IAXFR-TO-QTY ( #X ) ) / 100
                            }                                                                                                                                             //Natural: END-IF
                            //*  032020 END
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
                            sub_Move_Detail_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR03:                                                                                                                                                //Natural: FOR #I 1 C*XFR-FRM-ACCT-DTA
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta)); pnd_I.nadd(1))
                    {
                        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(" ")))                                                                   //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Fund_Cde.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I));                                                                       //Natural: ASSIGN #FUND-CDE := IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
                        sub_Get_Ticker_Symbol();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Tckr.notEquals(" ")))                                                                                                           //Natural: IF #TCKR NE ' '
                        {
                            pnd_From_Tckr.setValue(pnd_Tckr);                                                                                                             //Natural: ASSIGN #FROM-TCKR := #TCKR
                            pnd_Fr_Ticker.setValue(pnd_Tckr);                                                                                                             //Natural: ASSIGN #FR-TICKER := #TCKR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "INVALID FROM ACCT CODE",iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I));                                          //Natural: WRITE 'INVALID FROM ACCT CODE' IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                       //Natural: IF IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = #TIAA-FND-CDES ( * )
                        {
                            pnd_Fr_Tiaa.setValue(true);                                                                                                                   //Natural: ASSIGN #FR-TIAA := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fr_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #FR-TIAA := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        DbsUtil.examine(new ExamineSource(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_X)); //Natural: EXAMINE IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( * ) FOR #FUND-CDE GIVING INDEX #X
                        if (condition(pnd_X.equals(getZero())))                                                                                                           //Natural: IF #X = 0
                        {
                            getReports().write(0, "* INVALID SWITCH REQUEST FOR ID",pnd_Input);                                                                           //Natural: WRITE '* INVALID SWITCH REQUEST FOR ID' #INPUT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Fund_Cde.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X));                                                                        //Natural: ASSIGN #FUND-CDE := IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X )
                        pnd_Tckr.reset();                                                                                                                                 //Natural: RESET #TCKR #SETTL-IND
                        pnd_Settl_Ind.reset();
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
                        sub_Get_Ticker_Symbol();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Tckr.notEquals(" ")))                                                                                                           //Natural: IF #TCKR NE ' '
                        {
                            pnd_To_Ticker.setValue(pnd_Tckr);                                                                                                             //Natural: ASSIGN #TO-TICKER := #TCKR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "INVALID TO ACCT CODE",iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X),pnd_X);                                       //Natural: WRITE 'INVALID TO ACCT CODE' IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) #X
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Settl_Ind.setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ.getValue(pnd_I));                                                                      //Natural: ASSIGN #SETTL-IND := IAA-TRNSFR-SW-RQST.XFR-FRM-UNIT-TYP ( #I )
                        pnd_Actl_Amt.setValue(audit_Iaxfr_To_Asset_Amt.getValue(pnd_X));                                                                                  //Natural: ASSIGN #ACTL-AMT := AUDIT.IAXFR-TO-ASSET-AMT ( #X )
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
                        sub_Move_Detail_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-DATA
                sub_Write_Hdr_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-CTLS-TRANSACTIONS
                sub_Write_Ctls_Transactions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Rcrd_Cnt().greater(getZero())))                                                               //Natural: IF #CTLS-TRLR-HDR-RCRD-CNT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAILER
            sub_Write_Trailer();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EXT-MODULE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATES
        //* ***********************************************************************
        //*  GET TODAY's date and Next Business date
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT-OR-CERT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INVSTMNT-ID
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TICKER-SYMBOL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DETAIL-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HDR-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CTLS-TRANSACTIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAILER
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TRANS-FIELDS
    }
    //*  GET PRODUCT/CONTRACT TYPE
    //*  BY ACCT
    private void sub_Call_Ext_Module() throws Exception                                                                                                                   //Natural: CALL-EXT-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Control_Dates() throws Exception                                                                                                                 //Natural: GET-CONTROL-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pdaIatl400p.getPnd_Iatn400_In_Cntrl_Cde().setValue("DC");                                                                                                         //Natural: ASSIGN #IATN400-IN.CNTRL-CDE := 'DC'
        //*  FETCH THE LATEST CONTROL RECORD FOR CNTRL CDE DC
        DbsUtil.callnat(Iatn400.class , getCurrentProcessState(), pdaIatl400p.getPnd_Iatn400_In(), pdaIatl400p.getPnd_Iatn400_Out());                                     //Natural: CALLNAT 'IATN400' #IATN400-IN #IATN400-OUT
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Contract_Or_Cert() throws Exception                                                                                                              //Natural: GET-CONTRACT-OR-CERT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr.setValue(iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id);                                                                                   //Natural: ASSIGN #PIN-NBR := IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #PIN-CNTRCT-PAYEE-KEY
        (
        "READ02",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Pin_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ02")))
        {
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr.notEquals(pnd_Pin_Cntrct_Payee_Key_Pnd_Pin_Nbr)))                                                            //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR NE #PIN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1545 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR EQ #CONTRACT-CERT
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.equals(pnd_Contract_Cert)))
            {
                decideConditionsMet1545++;
                //*  060809
                if (condition(pnd_Res_Cde.equals(" ")))                                                                                                                   //Natural: IF #RES-CDE = ' '
                {
                    //*  060809
                    pnd_Res_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT PRTCPNT-RSDNCY-CDE TO #RES-CDE
                    //*  060809
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_First_Call.getBoolean()))                                                                                                               //Natural: IF #FIRST-CALL
                {
                    //*  TIAA
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).notEquals(" ")))                                                                  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        pnd_Fr_Tiaa.setValue(true);                                                                                                                       //Natural: ASSIGN #FR-TIAA := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CREF
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(2).notEquals(" ")))                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 2 ) NE ' '
                        {
                            pnd_Fr_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #FR-TIAA := FALSE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "NO COMPANY CODE FOR",iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                    //Natural: WRITE 'NO COMPANY CODE FOR' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SECOND CALL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TIAA
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).notEquals(" ")))                                                                  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) NE ' '
                    {
                        pnd_To_Tiaa.setValue(true);                                                                                                                       //Natural: ASSIGN #TO-TIAA := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CREF
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(2).notEquals(" ")))                                                              //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 2 ) NE ' '
                        {
                            pnd_To_Tiaa.setValue(false);                                                                                                                  //Natural: ASSIGN #TO-TIAA := FALSE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM());                                                                                                   //Natural: WRITE *PROGRAM
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "NO COMPANY CODE FOR",iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                    //Natural: WRITE 'NO COMPANY CODE FOR' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR,2,6 ) EQ SUBSTR ( #CONTRACT-CERT,2,6 )
            else if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.getSubstring(2,6).equals(pnd_Contract_Cert.getSubstring(2,6))))
            {
                decideConditionsMet1545++;
                //*  060809
                if (condition(pnd_Res_Cde.equals(" ")))                                                                                                                   //Natural: IF #RES-CDE = ' '
                {
                    //*  060809
                    pnd_Res_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT PRTCPNT-RSDNCY-CDE TO #RES-CDE
                    //*  060809
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_First_Call.getBoolean()))                                                                                                               //Natural: IF #FIRST-CALL
                {
                    pnd_Fr_Contract_Pair.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                          //Natural: ASSIGN #FR-CONTRACT-PAIR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_To_Contract_Pair.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                          //Natural: ASSIGN #TO-CONTRACT-PAIR := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Invstmnt_Id() throws Exception                                                                                                                   //Natural: GET-INVSTMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol.setValue(pnd_Ws_Ticker);                                                                                                 //Natural: ASSIGN #NEC-TICKER-SYMBOL := #WS-TICKER
        vw_pnd_Ext_Fund.startDatabaseFind                                                                                                                                 //Natural: FIND #EXT-FUND WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
        (
        "FIND05",
        new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
        );
        FIND05:
        while (condition(vw_pnd_Ext_Fund.readNextRow("FIND05")))
        {
            vw_pnd_Ext_Fund.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(pnd_Ext_Fund_Nec_Investment_Grouping_Id);                                                                                              //Natural: ASSIGN #WS-TICKER := NEC-INVESTMENT-GROUPING-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ084");                                                                                                   //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ084'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("ADS");                                                                                                      //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'ADS'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Ws_Ticker);                                                                                              //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #WS-TICKER
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TBL-SUPER1
        (
        "FIND06",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Tbl_Super1, WcType.WITH) },
        1
        );
        FIND06:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND06")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                               //Natural: ASSIGN #WS-TICKER := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Ticker_Symbol() throws Exception                                                                                                                 //Natural: GET-TICKER-SYMBOL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan021x.class , getCurrentProcessState(), pnd_Fund_Cde, pnd_Tckr);                                                                               //Natural: CALLNAT 'IAAN021X' #FUND-CDE #TCKR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Move_Detail_Data() throws Exception                                                                                                                  //Natural: MOVE-DETAIL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tot_Trans_Amt.nadd(pnd_Actl_Amt);                                                                                                                             //Natural: ADD #ACTL-AMT TO #TOT-TRANS-AMT
        ldaIaalctls.getPnd_Ctls_Trans_Cnt().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CTLS-TRANS-CNT
        if (condition(pnd_Fr_Ticker.notEquals("TIAA#")))                                                                                                                  //Natural: IF #FR-TICKER NE 'TIAA#'
        {
            pnd_Ws_Ticker.setValue(pnd_Fr_Ticker);                                                                                                                        //Natural: ASSIGN #WS-TICKER := #FR-TICKER
                                                                                                                                                                          //Natural: PERFORM GET-INVSTMNT-ID
            sub_Get_Invstmnt_Id();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Ws_Ticker.notEquals(" ")))                                                                                                                  //Natural: IF #WS-TICKER NE ' '
            {
                pnd_Fr_Ticker.setValue(pnd_Ws_Ticker);                                                                                                                    //Natural: ASSIGN #FR-TICKER := #WS-TICKER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_To_Ticker.notEquals("TIAA#")))                                                                                                                  //Natural: IF #TO-TICKER NE 'TIAA#'
        {
            pnd_Ws_Ticker.setValue(pnd_To_Ticker);                                                                                                                        //Natural: ASSIGN #WS-TICKER := #TO-TICKER
                                                                                                                                                                          //Natural: PERFORM GET-INVSTMNT-ID
            sub_Get_Invstmnt_Id();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Ws_Ticker.notEquals(" ")))                                                                                                                  //Natural: IF #WS-TICKER NE ' '
            {
                pnd_To_Ticker.setValue(pnd_Ws_Ticker);                                                                                                                    //Natural: ASSIGN #TO-TICKER := #WS-TICKER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Eor().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("X");                                            //Natural: ASSIGN #CTLS-TRANS-EOR ( #CTLS-TRANS-CNT ) := 'X'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("T");                                       //Natural: ASSIGN #CTLS-TRANS-RCRD-TYP ( #CTLS-TRANS-CNT ) := 'T'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Ctls_Seq_Num);                             //Natural: ASSIGN #CTLS-TRANS-SQNCE ( #CTLS-TRANS-CNT ) := #CTLS-SEQ-NUM
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("PS");                                           //Natural: ASSIGN #CTLS-TRANS-TYP ( #CTLS-TRANS-CNT ) := 'PS'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                       //Natural: ASSIGN #CTLS-TRANS-TYP-RCRD ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt());    //Natural: ASSIGN #CTLS-TRANS-OCCRNCE-NBR ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-CNT
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                      //Natural: ASSIGN #CTLS-TRANS-RTE-BASIS ( #CTLS-TRANS-CNT ) := ' '
        if (condition(pnd_Grd_To_Std.getBoolean()))                                                                                                                       //Natural: IF #GRD-TO-STD
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("Y");                                //Natural: ASSIGN #CTLS-TRANS-UNDLDGR-IND ( #CTLS-TRANS-CNT ) := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("N");                                //Natural: ASSIGN #CTLS-TRANS-UNDLDGR-IND ( #CTLS-TRANS-CNT ) := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("N");                                      //Natural: ASSIGN #CTLS-TRANS-RVRSL-CDE ( #CTLS-TRANS-CNT ) := 'N'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-DEPT-ID ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                     //Natural: ASSIGN #CTLS-TRANS-ALLCTN-NDX ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                     //Natural: ASSIGN #CTLS-TRANS-VLTN-BASIS ( #CTLS-TRANS-CNT ) := ' '
        if (condition(pnd_Actl_Amt.less(getZero())))                                                                                                                      //Natural: IF #ACTL-AMT < 0
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("-");                                   //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Actl_Amt);                                   //Natural: ASSIGN #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) := #ACTL-AMT
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Fr_Ticker);                             //Natural: ASSIGN #CTLS-TRANS-FROM-TKR ( #CTLS-TRANS-CNT ) := #FR-TICKER
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                          //Natural: IF #FR-TIAA
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                       //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT ) := IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Fr_Contract_Pair);              //Natural: ASSIGN #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT ) := #FR-CONTRACT-PAIR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Fr_Contract_Pair);              //Natural: ASSIGN #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT ) := #FR-CONTRACT-PAIR
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                       //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT ) := IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
        sub_Call_Ext_Module();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                             //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: ASSIGN #CTLS-TRANS-FROM-LOB ( #CTLS-TRANS-CNT ) := PRD-PRODUCT-CDE ( 1 )
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)); //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT ) := PRD-SUB-PRODUCT-CDE ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                          //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                                       //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1678 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = 'G'
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals("G")))
        {
            decideConditionsMet1678++;
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Grdd_Sub_Lob);                  //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT ) := #GRDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-FRM-ACCT-CDE ( #I ) = 'T'
        else if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde.getValue(pnd_I).equals("T")))
        {
            decideConditionsMet1678++;
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Stdd_Sub_Lob);                  //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT ) := #STDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                   //Natural: ASSIGN #CTLS-TRANS-FROM-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_To_Ticker);                               //Natural: ASSIGN #CTLS-TRANS-TO-TKR ( #CTLS-TRANS-CNT ) := #TO-TICKER
        if (condition(pnd_To_Tiaa.getBoolean()))                                                                                                                          //Natural: IF #TO-TIAA
        {
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := IAA-TRNSFR-SW-RQST.IA-TO-CNTRCT
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_To_Contract_Pair);            //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := #TO-CONTRACT-PAIR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Fr_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #FR-TIAA
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_To_Contract_Pair);            //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := #TO-CONTRACT-PAIR
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := IAA-TRNSFR-SW-RQST.IA-TO-CNTRCT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        if (condition(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).equals(" ")))                        //Natural: IF #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) = ' '
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct);                                                                        //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := IAA-TRNSFR-SW-RQST.IA-TO-CNTRCT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().equals(" ")))                                                                                           //Natural: IF NECA4000.PRD-KEY4-ACCT-NBR = ' '
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-LOB ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-LOB ( #CTLS-TRANS-CNT )
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT )
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT )
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_To_Tiaa.getBoolean()))                                                                                                                      //Natural: IF #TO-TIAA
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).notEquals(" ")))             //Natural: IF #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) NE ' '
                {
                    pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
            sub_Call_Ext_Module();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                         //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: ASSIGN #CTLS-TRANS-TO-LOB ( #CTLS-TRANS-CNT ) := PRD-PRODUCT-CDE ( 1 )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)); //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := PRD-SUB-PRODUCT-CDE ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, Global.getPROGRAM());                                                                                                               //Natural: WRITE *PROGRAM
                if (Global.isEscape()) return;
                getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                      //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
                if (Global.isEscape()) return;
                getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                                   //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
                if (Global.isEscape()) return;
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1737 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = 'G'
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals("G")))
        {
            decideConditionsMet1737++;
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Grdd_Sub_Lob);                    //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := #GRDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN IAA-TRNSFR-SW-RQST.XFR-TO-ACCT-CDE ( #X ) = 'T'
        else if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde.getValue(pnd_X).equals("T")))
        {
            decideConditionsMet1737++;
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Stdd_Sub_Lob);                    //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := #STDD-SUB-LOB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                     //Natural: ASSIGN #CTLS-TRANS-TO-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-PITS-RQST-ID ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                       //Natural: ASSIGN #CTLS-TRANS-LDGR-TYP ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                      //Natural: ASSIGN #CTLS-TRANS-CNCL-RDRW ( #CTLS-TRANS-CNT ) := ' '
        if (condition(pnd_Switch.getBoolean()))                                                                                                                           //Natural: IF #SWITCH
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("S");                                       //Natural: ASSIGN #CTLS-TRANS-IND2 ( #CTLS-TRANS-CNT ) := 'S'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                       //Natural: ASSIGN #CTLS-TRANS-IND2 ( #CTLS-TRANS-CNT ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Deposit_Acctg.getBoolean() && ! (pnd_Switch.getBoolean())))                                                                                     //Natural: IF #DEPOSIT-ACCTG AND NOT #SWITCH
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("D");                                       //Natural: ASSIGN #CTLS-TRANS-IND2 ( #CTLS-TRANS-CNT ) := 'D'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Settl_Ind);                                  //Natural: ASSIGN #CTLS-TRANS-IND ( #CTLS-TRANS-CNT ) := #SETTL-IND
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-TAX-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-NET-CASH-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                    //Natural: ASSIGN #CTLS-TRANS-INSTLLMNT-DTE ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                //Natural: ASSIGN #CTLS-TRANS-1ST-YR-RNWL-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-SUBPLAN ( #CTLS-TRANS-CNT ) := ' '
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Amt_Ttl().nadd(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ADD #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) TO #CTLS-TRLR-DTL-AMT-TTL
    }
    private void sub_Write_Hdr_Data() throws Exception                                                                                                                    //Natural: WRITE-HDR-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Dte_Alpha.setValueEdited(pdaIatl400p.getPnd_Iatn400_Out_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #IATN400-OUT.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Jrnl_Dte().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                       //Natural: ASSIGN #CTLS-HDR-JRNL-DTE := #DTE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Eor().setValue("X");                                                                                              //Natural: ASSIGN #CTLS-HDR-EOR := 'X'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().setValue(pnd_Tot_Trans_Amt);                                                                       //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT := #TOT-TRANS-AMT
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Typ().setValue("H");                                                                                         //Natural: ASSIGN #CTLS-HDR-RCRD-TYP := 'H'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sqnce().setValue(pnd_Ctls_Seq_Num);                                                                               //Natural: ASSIGN #CTLS-HDR-SQNCE := #CTLS-SEQ-NUM
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Orgn_Sys().setValue("PST");                                                                                       //Natural: ASSIGN #CTLS-HDR-ORGN-SYS := 'PST'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Flow_Stts().setValue("O");                                                                                   //Natural: ASSIGN #CTLS-HDR-RCRD-FLOW-STTS := 'O'
        pnd_Dte_Alpha.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Part_Dte().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                       //Natural: ASSIGN #CTLS-HDR-PART-DTE := #DTE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Dte_Stmp().setValue(Global.getDATN());                                                                 //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-DTE-STMP := #CTLS-HDR-PRCSS-DTE-STMP := #CTLS-HDR-CRTE-DTE-STMP := #CTLS-HDR-RCVD-DTE-STMP := *DATN
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Dte_Stmp().setValue(Global.getDATN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Dte_Stmp().setValue(Global.getDATN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Dte_Stmp().setValue(Global.getDATN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Tme_Stmp().setValue(Global.getTIMN());                                                                 //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-TME-STMP := #CTLS-HDR-PRCSS-TME-STMP := #CTLS-HDR-CRTE-TME-STMP := #CTLS-HDR-RCVD-TME-STMP := *TIMN
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Tme_Stmp().setValue(Global.getTIMN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Tme_Stmp().setValue(Global.getTIMN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Tme_Stmp().setValue(Global.getTIMN());
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Intrfce_Dte().setValue(0);                                                                                        //Natural: ASSIGN #CTLS-HDR-INTRFCE-DTE := 0
        if (condition(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().less(getZero())))                                                                    //Natural: IF #CTLS-HDR-TTL-TRNS-AMT < 0
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue("-");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue(" ");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE EDITED IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID (EM=9999999)
        //*   PIN EXP 06/2017
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Num().setValueEdited(iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id,new ReportEditMask("999999999999"));                      //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID ( EM = 999999999999 ) TO #CTLS-HDR-PH-NUM
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_St_Cde().setValue(pnd_Res_Cde);                                                                                   //Natural: ASSIGN #CTLS-HDR-ST-CDE := #RES-CDE
        getWorkFiles().write(2, false, ldaIaalctls.getPnd_Ctls_Hdr_Record());                                                                                             //Natural: WRITE WORK FILE 2 #CTLS-HDR-RECORD
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Rcrd_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #CTLS-TRLR-HDR-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #CTLS-TRLR-TTL-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Amt_Ttl().nadd(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt());                             //Natural: ADD #CTLS-HDR-TTL-TRNS-AMT TO #CTLS-TRLR-HDR-AMT-TTL
    }
    private void sub_Write_Ctls_Transactions() throws Exception                                                                                                           //Natural: WRITE-CTLS-TRANSACTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, NEWLINE,"DETAIL RECORD FIELDS");                                                                                                        //Natural: WRITE / 'DETAIL RECORD FIELDS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #Z 1 #CTLS-TRANS-CNT
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); pnd_Z.nadd(1))
        {
            pnd_Detail_Record_Pnd_Trans_Rcrd_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-RCRD-TYP := #CTLS-TRANS-RCRD-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Sqnce.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_Z));                                      //Natural: ASSIGN #TRANS-SQNCE := #CTLS-TRANS-SQNCE ( #Z )
            pnd_Detail_Record_Pnd_Trans_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Z));                                          //Natural: ASSIGN #TRANS-TYP := #CTLS-TRANS-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Typ_Rcrd.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-TYP-RCRD := #CTLS-TRANS-TYP-RCRD ( #Z )
            pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_Z));                          //Natural: ASSIGN #TRANS-OCCRNCE-NBR := #CTLS-TRANS-OCCRNCE-NBR ( #Z )
            pnd_Detail_Record_Pnd_Trans_Rte_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-RTE-BASIS := #CTLS-TRANS-RTE-BASIS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Undldgr_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Z));                          //Natural: ASSIGN #TRANS-UNDLDGR-IND := #CTLS-TRANS-UNDLDGR-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-RVRSL-CDE := #CTLS-TRANS-RVRSL-CDE ( #Z )
            pnd_Detail_Record_Pnd_Trans_Dept_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-DEPT-ID := #CTLS-TRANS-DEPT-ID ( #Z )
            pnd_Detail_Record_Pnd_Trans_Allctn_Ndx.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-ALLCTN-NDX := #CTLS-TRANS-ALLCTN-NDX ( #Z )
            pnd_Detail_Record_Pnd_Trans_Vltn_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-VLTN-BASIS := #CTLS-TRANS-VLTN-BASIS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Amt_Sign.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-AMT-SIGN := #CTLS-TRANS-AMT-SIGN ( #Z )
            pnd_Detail_Record_Pnd_Trans_Dtl_Amt.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Z));                                      //Natural: ASSIGN #TRANS-DTL-AMT := #CTLS-TRANS-AMT ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-FROM-TKR := #CTLS-TRANS-FROM-TKR ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-FROM-LOB := #CTLS-TRANS-FROM-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-SUB-LOB := #CTLS-TRANS-FROM-SUB-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-TIAA-NO := #CTLS-TRANS-FROM-TIAA-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-CREF-NO := #CTLS-TRANS-FROM-CREF-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_Z));                    //Natural: ASSIGN #TRANS-FROM-NUM-UNITS := #CTLS-TRANS-FROM-NUM-UNITS ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Z));                                    //Natural: ASSIGN #TRANS-TO-TKR := #CTLS-TRANS-TO-TKR ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_Z));                                    //Natural: ASSIGN #TRANS-TO-LOB := #CTLS-TRANS-TO-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-SUB-LOB := #CTLS-TRANS-TO-SUB-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-TIAA-NO := #CTLS-TRANS-TO-TIAA-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-CREF-NO := #CTLS-TRANS-TO-CREF-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-TO-NUM-UNITS := #CTLS-TRANS-TO-NUM-UNITS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-PITS-RQST-ID := #CTLS-TRANS-PITS-RQST-ID ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ldgr_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-LDGR-TYP := #CTLS-TRANS-LDGR-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-CNCL-RDRW := #CTLS-TRANS-CNCL-RDRW ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ind2.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Z));                                        //Natural: ASSIGN #TRANS-IND2 := #CTLS-TRANS-IND2 ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Z));                                          //Natural: ASSIGN #TRANS-IND := #CTLS-TRANS-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Tax_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-TAX-IND := #CTLS-TRANS-TAX-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-NET-CASH-IND := #CTLS-TRANS-NET-CASH-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_Z));                      //Natural: ASSIGN #TRANS-INSTLLMNT-DTE := #CTLS-TRANS-INSTLLMNT-DTE ( #Z )
            pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_Z));                  //Natural: ASSIGN #TRANS-1ST-YR-RNWL-IND := #CTLS-TRANS-1ST-YR-RNWL-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Subplan.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-SUBPLAN := #CTLS-TRANS-SUBPLAN ( #Z )
            pnd_Detail_Record_Pnd_Dtl_End.setValue("X");                                                                                                                  //Natural: ASSIGN #DTL-END := 'X'
            getWorkFiles().write(2, false, pnd_Detail_Record);                                                                                                            //Natural: WRITE WORK FILE 2 #DETAIL-RECORD
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #CTLS-TRLR-TTL-RCRD-CNT
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Rcrd_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #CTLS-TRLR-DTL-RCRD-CNT
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRANS-FIELDS
                sub_Display_Trans_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Trailer() throws Exception                                                                                                                     //Natural: WRITE-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Eor().setValue("X");                                                                                            //Natural: ASSIGN #CTLS-TRLR-EOR := 'X'
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Rcrd_Typ().setValue("C");                                                                                       //Natural: ASSIGN #CTLS-TRLR-RCRD-TYP := 'C'
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Run_Dte().setValue(Global.getDATN());                                                                           //Natural: ASSIGN #CTLS-TRLR-RUN-DTE := *DATN
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Begin_Run_Tme().setValue(pnd_Begin_Timn_Pnd_Begin_Time);                                                        //Natural: ASSIGN #CTLS-TRLR-BEGIN-RUN-TME := #BEGIN-TIME
        pnd_End_Timn.setValue(Global.getTIMN());                                                                                                                          //Natural: ASSIGN #END-TIMN := *TIMN
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_End_Run_Tme().setValue(pnd_End_Timn_Pnd_End_Time);                                                              //Natural: ASSIGN #CTLS-TRLR-END-RUN-TME := #END-TIME
        getWorkFiles().write(2, false, pdaIaaactls.getPnd_Ctls_Trlr_Record());                                                                                            //Natural: WRITE WORK FILE 2 #CTLS-TRLR-RECORD
    }
    private void sub_Display_Trans_Fields() throws Exception                                                                                                              //Natural: DISPLAY-TRANS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_Z), //Natural: WRITE / '=' #CTLS-TRANS-RCRD-TYP ( #Z ) / '=' #CTLS-TRANS-SQNCE ( #Z ) / '=' #CTLS-TRANS-TYP ( #Z ) / '=' #CTLS-TRANS-TYP-RCRD ( #Z ) / '=' #CTLS-TRANS-OCCRNCE-NBR ( #Z ) / '=' #CTLS-TRANS-RTE-BASIS ( #Z ) / '=' #CTLS-TRANS-UNDLDGR-IND ( #Z ) / '=' #CTLS-TRANS-RVRSL-CDE ( #Z ) / '=' #CTLS-TRANS-DEPT-ID ( #Z ) / '=' #CTLS-TRANS-ALLCTN-NDX ( #Z ) / '=' #CTLS-TRANS-VLTN-BASIS ( #Z ) / '=' #CTLS-TRANS-AMT-SIGN ( #Z ) / '=' #CTLS-TRANS-AMT ( #Z ) / '=' #CTLS-TRANS-FROM-TKR ( #Z ) / '=' #CTLS-TRANS-FROM-LOB ( #Z ) / '=' #CTLS-TRANS-FROM-SUB-LOB ( #Z ) / '=' #CTLS-TRANS-FROM-TIAA-NO ( #Z ) / '=' #CTLS-TRANS-FROM-CREF-NO ( #Z ) / '=' #CTLS-TRANS-FROM-NUM-UNITS ( #Z ) / '=' #CTLS-TRANS-TO-TKR ( #Z )
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Z));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, "=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_Z), //Natural: WRITE '=' #CTLS-TRANS-TO-LOB ( #Z ) / '=' #CTLS-TRANS-TO-SUB-LOB ( #Z ) / '=' #CTLS-TRANS-TO-TIAA-NO ( #Z ) / '=' #CTLS-TRANS-TO-CREF-NO ( #Z ) / '=' #CTLS-TRANS-TO-NUM-UNITS ( #Z ) / '=' #CTLS-TRANS-PITS-RQST-ID ( #Z ) / '=' #CTLS-TRANS-LDGR-TYP ( #Z ) / '=' #CTLS-TRANS-CNCL-RDRW ( #Z ) / '=' #CTLS-TRANS-IND2 ( #Z ) / '=' #CTLS-TRANS-IND ( #Z ) / '=' #CTLS-TRANS-TAX-IND ( #Z ) / '=' #CTLS-TRANS-NET-CASH-IND ( #Z ) / '=' #CTLS-TRANS-INSTLLMNT-DTE ( #Z ) / '=' #CTLS-TRANS-1ST-YR-RNWL-IND ( #Z ) / '=' #CTLS-TRANS-SUBPLAN ( #Z )
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_Z));
        if (Global.isEscape()) return;
    }

    //
}
