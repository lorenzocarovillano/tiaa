/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:47 PM
**        * FROM NATURAL PROGRAM : Iaap301
************************************************************
**        * FILE NAME            : Iaap301.java
**        * CLASS NAME           : Iaap301
**        * INSTANCE NAME        : Iaap301
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM   -  IAAP301        THIS PROGRAM WILL READ ADABASE FILE    *
*      DATE   -  08/95          200 AND CREATE 1 FLAT EXTRACT FILE     *
*    AUTHOR   -  ARI G.         CONTAING CPR, CONTRACT AND DEDUCTION   *
*                               RECORDS. THIS FILE WILL CONTAIN HEADER *
*                               AND TRAILER RECORDS.                   *
*                                                                      *
*  HISTORY :- 11/97 LEN B ADDED FIELDS TO #WORK-RECORD-1 AND TO THE    *
*                         IAA-MASTER-FILE-VIEW                         *
*                                                                      *
*             04/03 RM    ADDED FIELDS TO #WORK-RECORD-1 AND TO THE    *
*                         IAA-MASTER-FILE-VIEW FOR EGTTRA ROLLOVER     *
*                         IN LOCAL IAAL301                             *
*             06/06 JFT   POPULATED CPR-XFR-ISS-DTE AND MOVED 01 TO    *
*                         DAY FIELDS WHEN THEY ARE 0.                  *
*             07/06 JFT   CONVERTED CPR-XFR-ISS-DTE TO A YYYYMMDD      *
*                         FIELD TO MAKE IT READABLE BY COBOL PROGRAM   *
*
*             04/08 JFT   ADDED ROTH 403(B)/401(K) FIELDS              *
*             01/10 JFT   ADDED ORIGINAL TPA ISSUE DATE STORED IN      *
*                         CNTRCT-SSNNG-DTE. FOR ORIGIN 54,62,74 ONLY   *
*             01/12 JFT   ADDED SUNY/CUNY FIELDS                       *
*             04/17 JFT   PIN EXPANSION - RECATALOG                    *
*             04/17 OS  ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap301 extends BLNatBase
{
    // Data Areas
    private LdaIaal301 ldaIaal301;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Wk_Dte_A8;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal301 = new LdaIaal301();
        registerRecord(ldaIaal301);
        registerRecord(ldaIaal301.getVw_iaa_Master_File_View());
        registerRecord(ldaIaal301.getVw_iaa_Cntrl_Rcrd_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Wk_Dte_A8 = localVariables.newFieldInRecord("pnd_Wk_Dte_A8", "#WK-DTE-A8", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal301.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap301() throws Exception
    {
        super("Iaap301");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        getReports().write(0, "*** START OF PROGRAM IAAP301 ***");                                                                                                        //Natural: WRITE '*** START OF PROGRAM IAAP301 ***'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        ldaIaal301.getVw_iaa_Cntrl_Rcrd_View().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) IAA-CNTRL-RCRD-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "RC",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        RC:
        while (condition(ldaIaal301.getVw_iaa_Cntrl_Rcrd_View().readNextRow("RC")))
        {
            ldaIaal301.getPnd_Check_Date_Ccyymmdd().setValueEdited(ldaIaal301.getIaa_Cntrl_Rcrd_View_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Ppcn_Nbr().setValue("   CHEADER");                                                                             //Natural: MOVE '   CHEADER' TO #WH-CNTRCT-PPCN-NBR
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Cntrct_Payee_Cde().setValue(0);                                                                                       //Natural: MOVE 0 TO #WH-CNTRCT-PAYEE-CDE
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Record_Code().setValue(0);                                                                                            //Natural: MOVE 0 TO #WH-RECORD-CODE
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Check_Date().setValue(ldaIaal301.getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N());                             //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WH-CHECK-DATE
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Process_Date().setValue(Global.getDATN());                                                                            //Natural: MOVE *DATN TO #WH-PROCESS-DATE
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Filler_1().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-1
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Filler_2().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-2
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Filler_3().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-3
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Filler_4().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-4
        //*  082017
        ldaIaal301.getPnd_Work_Record_Header_Pnd_Wh_Filler_5().setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WH-FILLER-5
        getWorkFiles().write(1, false, ldaIaal301.getPnd_Work_Record_Header());                                                                                           //Natural: WRITE WORK FILE 1 #WORK-RECORD-HEADER
        ldaIaal301.getPnd_Count().reset();                                                                                                                                //Natural: RESET #COUNT #COUNT2
        ldaIaal301.getPnd_Count2().reset();
        ldaIaal301.getVw_iaa_Master_File_View().startDatabaseRead                                                                                                         //Natural: READ IAA-MASTER-FILE-VIEW PHYSICAL
        (
        "R2",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        R2:
        while (condition(ldaIaal301.getVw_iaa_Master_File_View().readNextRow("R2")))
        {
            ldaIaal301.getPnd_Count_Master_File_Reads().nadd(1);                                                                                                          //Natural: ADD 1 TO #COUNT-MASTER-FILE-READS
            ldaIaal301.getPnd_Count().nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT
            ldaIaal301.getPnd_Count2().nadd(1);                                                                                                                           //Natural: ADD 1 TO #COUNT2
            if (condition(ldaIaal301.getPnd_Count().greater(4999)))                                                                                                       //Natural: IF #COUNT > 4999
            {
                getReports().write(0, "NUMBER OF RECORDS READ",ldaIaal301.getPnd_Count2(),Global.getTIMX());                                                              //Natural: WRITE 'NUMBER OF RECORDS READ' #COUNT2 *TIMX
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaIaal301.getPnd_Count().setValue(0);                                                                                                                    //Natural: MOVE 0 TO #COUNT
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal301.getPnd_All_Ok().reset();                                                                                                                           //Natural: RESET #ALL-OK
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            short decideConditionsMet447 = 0;                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = ' ' AND CNTRCT-PART-PPCN-NBR = ' ' AND DDCTN-PPCN-NBR = ' '
            if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr().equals(" ") && ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr().equals(" ") 
                && ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr().equals(" ")))
            {
                decideConditionsMet447++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr(),new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN 'ALL CNTR. FIELDS ARE BLANK'
                    ColumnSpacing(7),Global.getAstISN(),"ALL CNTR. FIELDS ARE BLANK");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR NOT = ' ' AND CNTRCT-PART-PPCN-NBR NOT = ' '
            else if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr().notEquals(" ") && ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet447++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr(),new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR NOT = ' ' AND DDCTN-PPCN-NBR NOT = ' '
            else if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr().notEquals(" ") && ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet447++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr(),new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PART-PPCN-NBR NOT = ' ' AND DDCTN-PPCN-NBR NOT = ' '
            else if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr().notEquals(" ") && ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet447++;
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr(),"=",ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr(),new  //Natural: WRITE ( 2 ) / '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PPCN-NBR '=' DDCTN-PPCN-NBR 7X *ISN '  MULTIPLE CNT FLD VALUES'
                    ColumnSpacing(7),Global.getAstISN(),"  MULTIPLE CNT FLD VALUES");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaIaal301.getPnd_All_Ok().setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #ALL-OK
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaIaal301.getPnd_All_Ok().notEquals("Y")))                                                                                                     //Natural: IF #ALL-OK NOT = 'Y'
            {
                ldaIaal301.getPnd_Count_Master_Record_Error().nadd(1);                                                                                                    //Natural: ADD 1 TO #COUNT-MASTER-RECORD-ERROR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaIaal301.getPnd_All_Ok().equals("Y"))))                                                                                                     //Natural: ACCEPT IF #ALL-OK = 'Y'
            {
                continue;
            }
            short decideConditionsMet470 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-PPCN-NBR NOT = ' '
            if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet470++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-CONTRACT-EXTRACT
                sub_Pnd_Write_Contract_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PART-PPCN-NBR NOT = ' '
            else if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet470++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-CPR-EXTRACT
                sub_Pnd_Write_Cpr_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN DDCTN-PPCN-NBR NOT = ' '
            else if (condition(ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr().notEquals(" ")))
            {
                decideConditionsMet470++;
                                                                                                                                                                          //Natural: PERFORM #WRITE-DEDUCTION-EXTRACT
                sub_Pnd_Write_Deduction_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Ppcn_Nbr().setValue("99CTRAILER");                                                                            //Natural: MOVE '99CTRAILER' TO #WT-CNTRCT-PPCN-NBR
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Cntrct_Payee_Cde().setValue(0);                                                                                      //Natural: MOVE 0 TO #WT-CNTRCT-PAYEE-CDE
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Record_Code().setValue(0);                                                                                           //Natural: MOVE 0 TO #WT-RECORD-CODE
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Check_Date().setValue(ldaIaal301.getPnd_Check_Date_Ccyymmdd_Pnd_Check_Date_Ccyymmdd_N());                            //Natural: MOVE #CHECK-DATE-CCYYMMDD-N TO #WT-CHECK-DATE
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Process_Date().setValue(Global.getDATN());                                                                           //Natural: MOVE *DATN TO #WT-PROCESS-DATE
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Cnt_Records().setValue(ldaIaal301.getPnd_Count_Contract_Extract());                                              //Natural: MOVE #COUNT-CONTRACT-EXTRACT TO #WT-TOT-CNT-RECORDS
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Cpr_Records().setValue(ldaIaal301.getPnd_Count_Cpr_Extract());                                                   //Natural: MOVE #COUNT-CPR-EXTRACT TO #WT-TOT-CPR-RECORDS
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Tot_Ded_Records().setValue(ldaIaal301.getPnd_Count_Deduction_Extract());                                             //Natural: MOVE #COUNT-DEDUCTION-EXTRACT TO #WT-TOT-DED-RECORDS
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_1().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-1
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_2().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-2
        //*  7/06
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_3().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-3
        //*  7/06
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_4().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-4
        //*  082017
        ldaIaal301.getPnd_Work_Record_Trailer_Pnd_Wt_Filler_5().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #WT-FILLER-5
        getWorkFiles().write(1, false, ldaIaal301.getPnd_Work_Record_Trailer());                                                                                          //Natural: WRITE WORK FILE 1 #WORK-RECORD-TRAILER
        getReports().write(1, ReportOption.NOTITLE,"          CPR EXTRACT ==>",ldaIaal301.getPnd_Count_Cpr_Extract(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));                 //Natural: WRITE ( 1 ) '          CPR EXTRACT ==>' #COUNT-CPR-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"     CONTRACT EXTRACT ==>",ldaIaal301.getPnd_Count_Contract_Extract(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));            //Natural: WRITE ( 1 ) '     CONTRACT EXTRACT ==>' #COUNT-CONTRACT-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"    DEDUCTION EXTRACT ==>",ldaIaal301.getPnd_Count_Deduction_Extract(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));           //Natural: WRITE ( 1 ) '    DEDUCTION EXTRACT ==>' #COUNT-DEDUCTION-EXTRACT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"  TOTAL ACTIVE PAYEES ==>",ldaIaal301.getPnd_Count_Active_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));       //Natural: WRITE ( 1 ) / '  TOTAL ACTIVE PAYEES ==>' #COUNT-ACTIVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL INACTIVE PAYEES ==>",ldaIaal301.getPnd_Count_Inactive_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9"));             //Natural: WRITE ( 1 ) 'TOTAL INACTIVE PAYEES ==>' #COUNT-INACTIVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        if (condition(ldaIaal301.getPnd_Count_Master_Record_Error().greater(getZero())))                                                                                  //Natural: IF #COUNT-MASTER-RECORD-ERROR > 0
        {
            getReports().skip(2, 2);                                                                                                                                      //Natural: SKIP ( 2 ) 2
            getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));              //Natural: WRITE ( 2 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, NEWLINE,"*** END OF PROGRAM IAAP301 ***");                                                                                                  //Natural: WRITE / '*** END OF PROGRAM IAAP301 ***'
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CONTRACT-EXTRACT
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-DEDUCTION-EXTRACT
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CPR-EXTRACT
        //* ***************************************************************
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *  WRITE '===================================================='
        getReports().write(0, NEWLINE,"  MASTER FILE READS =========> ",ldaIaal301.getPnd_Count_Master_File_Reads());                                                     //Natural: WRITE / '  MASTER FILE READS =========> ' #COUNT-MASTER-FILE-READS
        if (Global.isEscape()) return;
        getReports().write(0, "  MASTER FILE RECORD ERRORS => ",ldaIaal301.getPnd_Count_Master_Record_Error());                                                           //Natural: WRITE '  MASTER FILE RECORD ERRORS => ' #COUNT-MASTER-RECORD-ERROR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Write_Contract_Extract() throws Exception                                                                                                        //Natural: #WRITE-CONTRACT-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Ppcn_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Ppcn_Nbr());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PPCN-NBR TO #W1-PPCN-NBR
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Payee().setValue(0);                                                                                                       //Natural: MOVE 0 TO #W1-PAYEE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Record_Code().setValue(10);                                                                                                //Natural: MOVE 10 TO #W1-RECORD-CODE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Optn_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Optn_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-OPTN-CDE TO #W1-OPTN-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgn_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Orgn_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ORGN-CDE TO #W1-ORGN-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Acctng_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Acctng_Cde());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ACCTNG-CDE TO #W1-ACCTNG-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Issue_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Issue_Dte());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ISSUE-DTE TO #W1-ISSUE-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte());                           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE TO #W1-FIRST-PYMNT-DUE-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-PYMNT-PD-DTE TO #W1-FIRST-PYMNT-PD-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Crrncy_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Crrncy_Cde());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-CRRNCY-CDE TO #W1-CRRNCY-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Type_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Type_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-TYPE-CDE TO #W1-TYPE-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Pymnt_Mthd().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Pymnt_Mthd());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PYMNT-MTHD TO #W1-PYMNT-MTHD
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Pnsn_Pln_Cde());                                         //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PNSN-PLN-CDE TO #W1-PNSN-PLN-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Joint_Cnvrt_Rcrd_Ind());                        //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-JOINT-CNVRT-RCRD-IND TO #W1-JOINT-CNVRT-RCRCD-IND
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Orig_Da_Cntrct_Nbr());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ORIG-DA-CNTRCT-NBR TO #W1-ORIG-DA-CNTRCT-NBR
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rsdncy_At_Issue_Cde());                           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RSDNCY-AT-ISSUE-CDE TO #W1-RSDNCY-AT-ISSUE-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Xref_Ind());                           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-XREF-IND TO #W1-FIRST-ANNT-XREF-IND
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Dob_Dte());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-DOB-DTE TO #W1-FIRST-ANNT-DOB-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Mrtlty_Yob_Dte());               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE TO #W1-FIRST-ANNT-MRTLTY-YOB-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Sex_Cde());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-SEX-CDE TO #W1-FIRST-ANNT-SEX-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Lfe_Cnt());                            //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-LFE-CNT TO #W1-FIRST-ANNT-LIFE-CNT
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Annt_Dod_Dte());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-ANNT-DOD-DTE TO #W1-FIRST-ANNT-DOD-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Xref_Ind());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-XREF-IND TO #W1-SCND-ANNT-XREF-IND
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Dob_Dte());                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-DOB-DTE TO #W1-SCND-ANNT-DOB-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte());                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE TO #W1-SCND-ANNT-MRTLTY-YOB-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Sex_Cde());                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-SEX-CDE TO #W1-SCND-ANNT-SEX-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Life_Cnt());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-LIFE-CNT TO #W1-SCND-ANNT-LIFE-CNT
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Dod_Dte());                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-DOD-DTE TO #W1-SCND-ANNT-DOD-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Scnd_Annt_Ssn());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SCND-ANNT-SSN TO #W1-SCND-ANNT-SSN
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Div_Payee_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Div_Payee_Cde());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-DIV-PAYEE-CDE TO #W1-DIV-PAYEE-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Div_Coll_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Div_Coll_Cde());                                         //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-DIV-COLL-CDE TO #W1-DIV-COLL-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Inst_Iss_Cde());                                         //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-INST-ISS-CDE TO #W1-INST-ISS-CDE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Lst_Trans_Dte());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.LST-TRANS-DTE TO #W1-LST-TRANS-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Type().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Type());                                                  //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-TYPE TO #W1-CNTRCT-TYPE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rsdncy_At_Iss_Re());                          //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RSDNCY-AT-ISS-RE TO #W1-CNTRCT-RSDNCY-AT-ISS-RE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte().getValue("*").setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Fnl_Prm_Dte().getValue("*"));        //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FNL-PRM-DTE ( * ) TO #W1-CNTRCT-FNL-PRM-DTE ( * )
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Mtch_Ppcn());                                        //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-MTCH-PPCN TO #W1-CNTRCT-MTCH-PPCN
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Annty_Strt_Dte());                              //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ANNTY-STRT-DTE TO #W1-CNTRCT-ANNTY-STRT-DTE
        //*  DEFAULT TO 01 WHERE APPLICABLE  /* 06/06
        if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Issue_Dte_Dd().equals(getZero())))                                                                        //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-ISSUE-DTE-DD = 0
        {
            ldaIaal301.getIaa_Master_File_View_Cntrct_Issue_Dte_Dd().setValue(1);                                                                                         //Natural: ASSIGN IAA-MASTER-FILE-VIEW.CNTRCT-ISSUE-DTE-DD := 01
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Pymnt_Due_Dte().greater(getZero())))                                                                //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-PYMNT-DUE-DTE GT 0
        {
            if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd().equals(getZero())))                                                                   //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-FP-DUE-DTE-DD = 0
            {
                ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd().setValue(1);                                                                                    //Natural: ASSIGN IAA-MASTER-FILE-VIEW.CNTRCT-FP-DUE-DTE-DD := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_First_Pymnt_Pd_Dte().greater(getZero())))                                                                 //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-FIRST-PYMNT-PD-DTE GT 0
        {
            if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd().equals(getZero())))                                                                    //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-FP-PD-DTE-DD = 0
            {
                ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd().setValue(1);                                                                                     //Natural: ASSIGN IAA-MASTER-FILE-VIEW.CNTRCT-FP-PD-DTE-DD := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF 06/06 CODE CHANGES
        //*  LB 11/97
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Issue_Dte_Dd());                                  //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ISSUE-DTE-DD TO #W1-CNTRCT-ISSUE-DTE-DD
        //*  LB 11/97
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Due_Dte_Dd());                                //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FP-DUE-DTE-DD TO #W1-CNTRCT-FP-DUE-DTE-DD
        //*  LB 11/97
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Fp_Pd_Dte_Dd());                                  //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FP-PD-DTE-DD TO #W1-CNTRCT-FP-PD-DTE-DD
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Filler().setValue(" ");                                                                                                    //Natural: MOVE ' ' TO #W1-FILLER
        //*  082017
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Filler2().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #W1-FILLER2
        //*  04/08 NEW FOR ROTH 403(B)/401(K)
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte().reset();                                                                                             //Natural: RESET #W1-ROTH-FRST-CNTRB-DTE #W1-ROTH-SSNNG-DTE #W1-CNTRCT-SSNNG-DTE
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte().reset();
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte().reset();
        if (condition(ldaIaal301.getIaa_Master_File_View_Roth_Frst_Cntrb_Dte().greater(getZero())))                                                                       //Natural: IF IAA-MASTER-FILE-VIEW.ROTH-FRST-CNTRB-DTE GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Roth_Frst_Cntrb_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED IAA-MASTER-FILE-VIEW.ROTH-FRST-CNTRB-DTE ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Frst_Cntrb_Dte()),  //Natural: ASSIGN #W1-ROTH-FRST-CNTRB-DTE := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal301.getIaa_Master_File_View_Roth_Ssnng_Dte().greater(getZero())))                                                                            //Natural: IF IAA-MASTER-FILE-VIEW.ROTH-SSNNG-DTE GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Roth_Ssnng_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED IAA-MASTER-FILE-VIEW.ROTH-SSNNG-DTE ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Roth_Ssnng_Dte()),        //Natural: ASSIGN #W1-ROTH-SSNNG-DTE := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        //*  01/10 FOR ORIGINAL TPA ISSUE DATE
        //*  IPRO FROM TPA
        if (condition(ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgn_Cde().equals(54) || ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgn_Cde().equals(62) ||                     //Natural: IF #W1-ORGN-CDE = 54 OR = 62 OR = 74
            ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgn_Cde().equals(74)))
        {
            //*  TPA ORIGINAL ISSUE DATE
            if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Ssnng_Dte().greater(getZero())))                                                                      //Natural: IF CNTRCT-SSNNG-DTE GT 0
            {
                pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Cntrct_Ssnng_Dte(),new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED CNTRCT-SSNNG-DTE ( EM = YYYYMMDD ) TO #WK-DTE-A8
                ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Cntrct_Ssnng_Dte()),  //Natural: ASSIGN #W1-CNTRCT-SSNNG-DTE := VAL ( #WK-DTE-A8 )
                    pnd_Wk_Dte_A8.val());
            }                                                                                                                                                             //Natural: END-IF
            //*  01/12 SUNY/CUNY
            //*  01/12 SUNY/CUNY
            //*  01/12 SUNY/CUNY
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Plan_Nmbr().setValue(ldaIaal301.getIaa_Master_File_View_Plan_Nmbr());                                                      //Natural: ASSIGN #W1-PLAN-NMBR := PLAN-NMBR
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Tax_Exmpt_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Tax_Exmpt_Ind());                                              //Natural: ASSIGN #W1-TAX-EXMPT-IND := TAX-EXMPT-IND
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Sub_Plan_Nmbr().setValue(ldaIaal301.getIaa_Master_File_View_Sub_Plan_Nmbr());                                              //Natural: ASSIGN #W1-SUB-PLAN-NMBR := SUB-PLAN-NMBR
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgntng_Sub_Plan_Nmbr().setValue(ldaIaal301.getIaa_Master_File_View_Orgntng_Sub_Plan_Nmbr());                              //Natural: ASSIGN #W1-ORGNTNG-SUB-PLAN-NMBR := ORGNTNG-SUB-PLAN-NMBR
        ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orgntng_Cntrct_Nmbr().setValue(ldaIaal301.getIaa_Master_File_View_Orgntng_Cntrct_Nmbr());                                  //Natural: ASSIGN #W1-ORGNTNG-CNTRCT-NMBR := ORGNTNG-CNTRCT-NMBR
        if (condition(ldaIaal301.getIaa_Master_File_View_Orig_Ownr_Dob().greater(getZero())))                                                                             //Natural: IF ORIG-OWNR-DOB GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Orig_Ownr_Dob(),new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED ORIG-OWNR-DOB ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dob()),          //Natural: ASSIGN #W1-ORIG-OWNR-DOB := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal301.getIaa_Master_File_View_Orig_Ownr_Dod().greater(getZero())))                                                                             //Natural: IF ORIG-OWNR-DOD GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Orig_Ownr_Dod(),new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED ORIG-OWNR-DOD ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_1_Pnd_W1_Orig_Ownr_Dod()),          //Natural: ASSIGN #W1-ORIG-OWNR-DOD := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        //*  END 01/10
        getWorkFiles().write(1, false, ldaIaal301.getPnd_Work_Record_1());                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
        ldaIaal301.getPnd_Count_Contract_Extract().nadd(1);                                                                                                               //Natural: ADD 1 TO #COUNT-CONTRACT-EXTRACT
    }
    private void sub_Pnd_Write_Deduction_Extract() throws Exception                                                                                                       //Natural: #WRITE-DEDUCTION-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Ppcn_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Ppcn_Nbr());                                            //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-PPCN-NBR TO #W3-DDCTN-PPCN-NBR
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Payee_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Payee_Cde());                                          //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-PAYEE-CDE TO #W3-DDCTN-PAYEE-CDE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Record_Cde().setValue(40);                                                                                           //Natural: MOVE 40 TO #W3-DDCTN-RECORD-CDE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Id_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Id_Nbr());                                                //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-ID-NBR TO #W3-DDCTN-ID-NBR
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Cde());                                                      //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-CDE TO #W3-DDCTN-CDE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Seq_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Seq_Nbr());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-SEQ-NBR TO #W3-DDCTN-SEQ-NBR
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Payee().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Payee());                                                  //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-PAYEE TO #W3-DDCTN-PAYEE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Per_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Per_Amt());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-PER-AMT TO #W3-DDCTN-PER-AMT
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Ytd_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Ytd_Amt());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-YTD-AMT TO #W3-DDCTN-YTD-AMT
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Pd_To_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Pd_To_Dte());                                          //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-PD-TO-DTE TO #W3-DDCTN-PD-TO-DTE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Tot_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Tot_Amt());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-TOT-AMT TO #W3-DDCTN-TOT-AMT
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Intent_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Intent_Cde());                                        //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-INTENT-CDE TO #W3-DDCTN-INTENT-CDE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Strt_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Strt_Dte());                                            //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-STRT-DTE TO #W3-DDCTN-STRT-DTE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Stp_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Stp_Dte());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-STP-DTE TO #W3-DDCTN-STP-DTE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Ddctn_Final_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Ddctn_Final_Dte());                                          //Natural: MOVE IAA-MASTER-FILE-VIEW.DDCTN-FINAL-DTE TO #W3-DDCTN-FINAL-DTE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Lst_Trans_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Lst_Trans_Dte());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.LST-TRANS-DTE TO #W3-LST-TRANS-DTE
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Filler().setValue(" ");                                                                                                    //Natural: MOVE ' ' TO #W3-FILLER
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Filler2().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #W3-FILLER2
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Filler3().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #W3-FILLER3
        ldaIaal301.getPnd_Work_Record_3_Pnd_W3_Filler4().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #W3-FILLER4
        getWorkFiles().write(1, false, ldaIaal301.getPnd_Work_Record_3());                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-3
        ldaIaal301.getPnd_Count_Deduction_Extract().nadd(1);                                                                                                              //Natural: ADD 1 TO #COUNT-DEDUCTION-EXTRACT
    }
    private void sub_Pnd_Write_Cpr_Extract() throws Exception                                                                                                             //Natural: #WRITE-CPR-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Ppcn_Nbr());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PART-PPCN-NBR TO #W2-PART-PPCN-NBR
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Part_Payee_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Part_Payee_Cde());                                     //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PART-PAYEE-CDE TO #W2-PART-PAYEE-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Record_Cde().setValue(20);                                                                                                 //Natural: MOVE 20 TO #W2-RECORD-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Cpr_Id_Nbr());                                                    //Natural: MOVE IAA-MASTER-FILE-VIEW.CPR-ID-NBR TO #W2-CPR-ID-NBR
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Lst_Trans_Dte());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.LST-TRANS-DTE TO #W2-LST-TRANS-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Prtcpnt_Ctznshp_Cde());                                  //Natural: MOVE IAA-MASTER-FILE-VIEW.PRTCPNT-CTZNSHP-CDE TO #W2-PRTCPNT-CTZNSHP-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Prtcpnt_Rsdncy_Cde());                                    //Natural: MOVE IAA-MASTER-FILE-VIEW.PRTCPNT-RSDNCY-CDE TO #W2-PRTCPNT-RSDNCY-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw().setValue(ldaIaal301.getIaa_Master_File_View_Prtcpnt_Rsdncy_Sw());                                      //Natural: MOVE IAA-MASTER-FILE-VIEW.PRTCPNT-RSDNCY-SW TO #W2-PRTCPNT-RSDNCY-SW
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Prtcpnt_Tax_Id_Nbr());                                    //Natural: MOVE IAA-MASTER-FILE-VIEW.PRTCPNT-TAX-ID-NBR TO #W2-PRTCPNT-TAX-ID-NBR
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ().setValue(ldaIaal301.getIaa_Master_File_View_Prtcpnt_Tax_Id_Typ());                                    //Natural: MOVE IAA-MASTER-FILE-VIEW.PRTCPNT-TAX-ID-TYP TO #W2-PRTCPNT-TAX-ID-TYP
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Actvty_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Actvty_Cde());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-ACTVTY-CDE TO #W2-ACTVTY-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Trmnte_Rsn().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Trmnte_Rsn());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-TRMNTE-RSN TO #W2-TRMNTE-RSN
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rwrttn_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rwrttn_Ind());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RWRTTN-IND TO #W2-RWRTTN-IND
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cash_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Cash_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-CASH-CDE TO #W2-CASH-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Emplymnt_Trmnt_Cde());                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-EMPLYMNT-TRMNT-CDE TO #W2-EMPLYMNT-TRMNT-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Company_Cd().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Company_Cd().getValue(1,                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-COMPANY-CD ( 1:5 ) TO #W2-COMPANY-CD ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rcvry_Type_Ind().getValue(1,         //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RCVRY-TYPE-IND ( 1:5 ) TO #W2-RCVRY-TYPE-IND ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Per_Ivc_Amt().getValue(1,               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PER-IVC-AMT ( 1:5 ) TO #W2-PER-IVC-AMT ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Resdl_Ivc_Amt().getValue(1,           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RESDL-IVC-AMT ( 1:5 ) TO #W2-RESDL-IVC-AMT ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Ivc_Amt().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Ivc_Amt().getValue(1,":",                   //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-IVC-AMT ( 1:5 ) TO #W2-IVC-AMT ( 1:5 )
            5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Ivc_Used_Amt().getValue(1,             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-IVC-USED-AMT ( 1:5 ) TO #W2-IVC-USED-AMT ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rtb_Amt().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rtb_Amt().getValue(1,":",                   //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RTB-AMT ( 1:5 ) TO #W2-RTB-AMT ( 1:5 )
            5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rtb_Percent().getValue(1,":",5).setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Rtb_Percent().getValue(1,               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-RTB-PERCENT ( 1:5 ) TO #W2-RTB-PERCENT ( 1:5 )
            ":",5));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Mode_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Mode_Ind());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-MODE-IND TO #W2-MODE-IND
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Wthdrwl_Dte());                                           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-WTHDRWL-DTE TO #W2-WTHDRWL-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Final_Per_Pay_Dte());                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FINAL-PER-PAY-DTE TO #W2-FINAL-PER-PAY-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Final_Pay_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Final_Pay_Dte());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FINAL-PAY-DTE TO #W2-FINAL-PAY-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Bnfcry_Xref_Ind());                                          //Natural: MOVE IAA-MASTER-FILE-VIEW.BNFCRY-XREF-IND TO #W2-BNFCRY-XREF-IND
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Bnfcry_Dod_Dte());                                            //Natural: MOVE IAA-MASTER-FILE-VIEW.BNFCRY-DOD-DTE TO #W2-BNFCRY-DOD-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Pend_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Pend_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PEND-CDE TO #W2-PEND-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Hold_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Hold_Cde());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-HOLD-CDE TO #W2-HOLD-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Pend_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Pend_Dte());                                                 //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PEND-DTE TO #W2-PEND-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Prev_Dist_Cde());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-PREV-DIST-CDE TO #W2-PREV-DIST-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Curr_Dist_Cde());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-CURR-DIST-CDE TO #W2-CURR-DIST-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cmbne_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Cmbne_Cde());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-CMBNE-CDE TO #W2-CMBNE-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Spirt_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Spirt_Cde());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SPIRT-CDE TO #W2-SPIRT-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Spirt_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Spirt_Amt());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SPIRT-AMT TO #W2-SPIRT-AMT
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Spirt_Srce().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Spirt_Srce());                                             //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SPIRT-SRCE TO #W2-SPIRT-SRCE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Spirt_Arr_Dte());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SPIRT-ARR-DTE TO #W2-SPIRT-ARR-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Spirt_Prcss_Dte());                                   //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-SPIRT-PRCSS-DTE TO #W2-SPIRT-PRCSS-DTE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Fed_Tax_Amt());                                           //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-FED-TAX-AMT TO #W2-FED-TAX-AMT
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_State_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_State_Cde());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-STATE-CDE TO #W2-STATE-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_State_Tax_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_State_Tax_Amt());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-STATE-TAX-AMT TO #W2-STATE-TAX-AMT
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Local_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Local_Cde());                                               //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-LOCAL-CDE TO #W2-LOCAL-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Local_Tax_Amt().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Local_Tax_Amt());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-LOCAL-TAX-AMT TO #W2-LOCAL-TAX-AMT
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cntrct_Lst_Chnge_Dte());                                       //Natural: MOVE IAA-MASTER-FILE-VIEW.CNTRCT-LST-CHNGE-DTE TO #W2-LST-CHNGE-DTE
        //*  ADDED FOLLOWING 4/03 FOR EGTTRA
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr().setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Cntrct_Nbr());                                        //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-CNTRCT-NBR TO #W2-RLLVR-CNTRCT-NBR
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Ivc_Ind());                                              //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-IVC-IND TO #W2-RLLVR-IVC-IND
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Elgble_Ind());                                        //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-ELGBLE-IND TO #W2-RLLVR-ELGBLE-IND
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde().getValue(1,":",4).setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Dstrbtng_Irc_Cde().getValue(1, //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-DSTRBTNG-IRC-CDE ( 1:4 ) TO #W2-RLLVR-DSTRBTNG-IRC-CDE ( 1:4 )
            ":",4));
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde().setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Accptng_Irc_Cde());                              //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-ACCPTNG-IRC-CDE TO #W2-RLLVR-ACCPTNG-IRC-CDE
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind().setValue(ldaIaal301.getIaa_Master_File_View_Rllvr_Pln_Admn_Ind());                                    //Natural: MOVE IAA-MASTER-FILE-VIEW.RLLVR-PLN-ADMN-IND TO #W2-RLLVR-PLN-ADMN-IND
        //*  END OF ADD 4/03
        //*  POPULATE CPR-XFR-ISS-DTE  /* 06/06
        //*  7/06
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8().reset();                                                                                              //Natural: RESET #W2-CPR-XFR-ISS-DTE-N8
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte().setValue(ldaIaal301.getIaa_Master_File_View_Cpr_Xfr_Iss_Dte());                                          //Natural: MOVE IAA-MASTER-FILE-VIEW.CPR-XFR-ISS-DTE TO #W2-CPR-XFR-ISS-DTE
        //*  ADDED 7/06
        if (condition(ldaIaal301.getIaa_Master_File_View_Cpr_Xfr_Iss_Dte().greater(getZero())))                                                                           //Natural: IF IAA-MASTER-FILE-VIEW.CPR-XFR-ISS-DTE GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Cpr_Xfr_Iss_Dte(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED IAA-MASTER-FILE-VIEW.CPR-XFR-ISS-DTE ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8()),  //Natural: ASSIGN #W2-CPR-XFR-ISS-DTE-N8 := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        //*  04/08 NEW FOR ROTH 403(B)/401(K)
        ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte().reset();                                                                                                 //Natural: RESET #W2-ROTH-DSBLTY-DTE
        if (condition(ldaIaal301.getIaa_Master_File_View_Roth_Dsblty_Dte().greater(getZero())))                                                                           //Natural: IF IAA-MASTER-FILE-VIEW.ROTH-DSBLTY-DTE GT 0
        {
            pnd_Wk_Dte_A8.setValueEdited(ldaIaal301.getIaa_Master_File_View_Roth_Dsblty_Dte(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED IAA-MASTER-FILE-VIEW.ROTH-DSBLTY-DTE ( EM = YYYYMMDD ) TO #WK-DTE-A8
            ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte().compute(new ComputeParameters(false, ldaIaal301.getPnd_Work_Record_2_Pnd_W2_Roth_Dsblty_Dte()),      //Natural: ASSIGN #W2-ROTH-DSBLTY-DTE := VAL ( #WK-DTE-A8 )
                pnd_Wk_Dte_A8.val());
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(1, false, ldaIaal301.getPnd_Work_Record_2());                                                                                                //Natural: WRITE WORK FILE 1 #WORK-RECORD-2
        ldaIaal301.getPnd_Count_Cpr_Extract().nadd(1);                                                                                                                    //Natural: ADD 1 TO #COUNT-CPR-EXTRACT
        if (condition(ldaIaal301.getIaa_Master_File_View_Cntrct_Actvty_Cde().equals(9)))                                                                                  //Natural: IF IAA-MASTER-FILE-VIEW.CNTRCT-ACTVTY-CDE = 9
        {
            ldaIaal301.getPnd_Count_Inactive_Payees().nadd(1);                                                                                                            //Natural: ADD 1 TO #COUNT-INACTIVE-PAYEES
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal301.getPnd_Count_Active_Payees().nadd(1);                                                                                                              //Natural: ADD 1 TO #COUNT-ACTIVE-PAYEES
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"TOTAL RECORDS");                                                                    //Natural: WRITE ( 1 ) 29X 'TOTAL RECORDS'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(29),"  EXTRACTED");                                                                      //Natural: WRITE ( 1 ) 29X '  EXTRACTED'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    //*  ADD 1 TO #PAGE-CTR
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(48),"IA ADMINISTRATION FILE EXTRACT",new                     //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 48T 'IA ADMINISTRATION FILE EXTRACT' 119T 'DATE ' *DATU
                        TabSetting(119),"DATE ",Global.getDATU());
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(55),"EXCEPTION REPORT");                                                                    //Natural: WRITE ( 2 ) 55T 'EXCEPTION REPORT'
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(33),"ERROR FIELDS","-",new RepeatItem(40),new TabSetting(89),"------ ISN ------",new    //Natural: WRITE ( 2 ) '-' ( 33 ) 'ERROR FIELDS' '-' ( 40 ) 89T '------ ISN ------' 107T '----- ERROR MESSAGE ------'
                        TabSetting(107),"----- ERROR MESSAGE ------");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
