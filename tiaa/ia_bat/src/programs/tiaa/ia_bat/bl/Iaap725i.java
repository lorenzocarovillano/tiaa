/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:17 PM
**        * FROM NATURAL PROGRAM : Iaap725i
************************************************************
**        * FILE NAME            : Iaap725i.java
**        * CLASS NAME           : Iaap725i
**        * INSTANCE NAME        : Iaap725i
************************************************************
************************************************************************
* PROGRAM  : IAAP725I
* PURPOSE  : TO GENERATE A RATE BY RATE REPORT FOR IPROS BY
*            MODE. THIS WILL BE USED IN THE TPA CONVERSION
*            RECONCILIATION
* DATE     : 06/29/2005
* AUTHOR   : JUN TINIO
*
*  3/12    : RATE BASE EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap725i extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Todays_Dte;

    private DbsGroup pnd_Output_Rec;
    private DbsField pnd_Output_Rec_Pnd_Mode;
    private DbsField pnd_Output_Rec_Pnd_Rate_Cde;
    private DbsField pnd_Output_Rec_Pnd_Guar_Amt;
    private DbsField pnd_Output_Rec_Pnd_Div_Amt;
    private DbsField pnd_Output_Rec_Pnd_Final_Pay;
    private DbsField pnd_Tot_Guar;
    private DbsField pnd_Tot_Div;
    private DbsField pnd_Tot_Final;
    private DbsField pnd_Tot_Pay;
    private DbsField pnd_M_Tot_Guar;
    private DbsField pnd_M_Tot_Div;
    private DbsField pnd_M_Tot_Final;
    private DbsField pnd_M_Tot_Pay;
    private DbsField pnd_G_Tot_Guar;
    private DbsField pnd_G_Tot_Div;
    private DbsField pnd_G_Tot_Final;
    private DbsField pnd_G_Tot_Pay;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Old_Mode;
    private DbsField pnd_Old_Rate;
    private DbsField pnd_First;
    private DbsField pnd_Rte_Cnt;
    private DbsField pnd_Rte_Cdes;
    private DbsField pnd_R_Guar;
    private DbsField pnd_R_Div;
    private DbsField pnd_R_Tot;
    private DbsField pnd_R_Final;
    private DbsField pnd_S_Guar_Amt;
    private DbsField pnd_S_Div_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        pnd_Output_Rec = localVariables.newGroupInRecord("pnd_Output_Rec", "#OUTPUT-REC");
        pnd_Output_Rec_Pnd_Mode = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Output_Rec_Pnd_Rate_Cde = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Rate_Cde", "#RATE-CDE", FieldType.STRING, 2);
        pnd_Output_Rec_Pnd_Guar_Amt = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Guar_Amt", "#GUAR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Output_Rec_Pnd_Div_Amt = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Div_Amt", "#DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Output_Rec_Pnd_Final_Pay = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Final_Pay", "#FINAL-PAY", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Guar = localVariables.newFieldInRecord("pnd_Tot_Guar", "#TOT-GUAR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Div = localVariables.newFieldInRecord("pnd_Tot_Div", "#TOT-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Final = localVariables.newFieldInRecord("pnd_Tot_Final", "#TOT-FINAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Pay = localVariables.newFieldInRecord("pnd_Tot_Pay", "#TOT-PAY", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Tot_Guar = localVariables.newFieldInRecord("pnd_M_Tot_Guar", "#M-TOT-GUAR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Tot_Div = localVariables.newFieldInRecord("pnd_M_Tot_Div", "#M-TOT-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Tot_Final = localVariables.newFieldInRecord("pnd_M_Tot_Final", "#M-TOT-FINAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Tot_Pay = localVariables.newFieldInRecord("pnd_M_Tot_Pay", "#M-TOT-PAY", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_G_Tot_Guar = localVariables.newFieldInRecord("pnd_G_Tot_Guar", "#G-TOT-GUAR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_G_Tot_Div = localVariables.newFieldInRecord("pnd_G_Tot_Div", "#G-TOT-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_G_Tot_Final = localVariables.newFieldInRecord("pnd_G_Tot_Final", "#G-TOT-FINAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_G_Tot_Pay = localVariables.newFieldInRecord("pnd_G_Tot_Pay", "#G-TOT-PAY", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Old_Mode = localVariables.newFieldInRecord("pnd_Old_Mode", "#OLD-MODE", FieldType.NUMERIC, 3);
        pnd_Old_Rate = localVariables.newFieldInRecord("pnd_Old_Rate", "#OLD-RATE", FieldType.STRING, 2);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Rte_Cnt = localVariables.newFieldInRecord("pnd_Rte_Cnt", "#RTE-CNT", FieldType.INTEGER, 2);
        pnd_Rte_Cdes = localVariables.newFieldArrayInRecord("pnd_Rte_Cdes", "#RTE-CDES", FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_R_Guar = localVariables.newFieldArrayInRecord("pnd_R_Guar", "#R-GUAR", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 250));
        pnd_R_Div = localVariables.newFieldArrayInRecord("pnd_R_Div", "#R-DIV", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 250));
        pnd_R_Tot = localVariables.newFieldArrayInRecord("pnd_R_Tot", "#R-TOT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 250));
        pnd_R_Final = localVariables.newFieldArrayInRecord("pnd_R_Final", "#R-FINAL", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 250));
        pnd_S_Guar_Amt = localVariables.newFieldInRecord("pnd_S_Guar_Amt", "#S-GUAR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Div_Amt = localVariables.newFieldInRecord("pnd_S_Div_Amt", "#S-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        localVariables.reset();
        pnd_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap725i() throws Exception
    {
        super("Iaap725i");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133;//Natural: FORMAT ( 2 ) PS = 66 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #OUTPUT-REC
        while (condition(getWorkFiles().read(1, pnd_Output_Rec)))
        {
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Old_Mode.setValue(pnd_Output_Rec_Pnd_Mode);                                                                                                           //Natural: ASSIGN #OLD-MODE := #MODE
                pnd_Old_Rate.setValue(pnd_Output_Rec_Pnd_Rate_Cde);                                                                                                       //Natural: ASSIGN #OLD-RATE := #RATE-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Output_Rec_Pnd_Rate_Cde.notEquals(pnd_Old_Rate)))                                                                                           //Natural: IF #RATE-CDE NE #OLD-RATE
            {
                DbsUtil.examine(new ExamineSource(pnd_Rte_Cdes.getValue("*"),true), new ExamineSearch(pnd_Old_Rate, true), new ExamineGivingIndex(pnd_I));                //Natural: EXAMINE FULL #RTE-CDES ( * ) FOR FULL #OLD-RATE GIVING INDEX #I
                if (condition(pnd_I.equals(getZero())))                                                                                                                   //Natural: IF #I = 0
                {
                    pnd_Rte_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #RTE-CNT
                    pnd_Rte_Cdes.getValue(pnd_Rte_Cnt).setValue(pnd_Old_Rate);                                                                                            //Natural: ASSIGN #RTE-CDES ( #RTE-CNT ) := #OLD-RATE
                    pnd_R_Guar.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Guar);                                                                                              //Natural: ASSIGN #R-GUAR ( #RTE-CNT ) := #TOT-GUAR
                    pnd_R_Div.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Div);                                                                                                //Natural: ASSIGN #R-DIV ( #RTE-CNT ) := #TOT-DIV
                    pnd_R_Tot.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Pay);                                                                                                //Natural: ASSIGN #R-TOT ( #RTE-CNT ) := #TOT-PAY
                    pnd_R_Final.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Final);                                                                                            //Natural: ASSIGN #R-FINAL ( #RTE-CNT ) := #TOT-FINAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_R_Guar.getValue(pnd_I).nadd(pnd_Tot_Guar);                                                                                                        //Natural: ADD #TOT-GUAR TO #R-GUAR ( #I )
                    pnd_R_Div.getValue(pnd_I).nadd(pnd_Tot_Div);                                                                                                          //Natural: ADD #TOT-DIV TO #R-DIV ( #I )
                    pnd_R_Tot.getValue(pnd_I).nadd(pnd_Tot_Pay);                                                                                                          //Natural: ADD #TOT-PAY TO #R-TOT ( #I )
                    pnd_R_Final.getValue(pnd_I).nadd(pnd_Tot_Final);                                                                                                      //Natural: ADD #TOT-FINAL TO #R-FINAL ( #I )
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(1, new TabSetting(2),"/MODE",                                                                                                        //Natural: DISPLAY ( 1 ) 2T '/MODE' #OLD-MODE ( IS = ON ) 8T '/RATE' #OLD-RATE 21T 'GUAR/AMT' #TOT-GUAR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T 'DIV/AMT' #TOT-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T 'PAY/AMT' #TOT-PAY ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T 'FINAL/PAY' #TOT-FINAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                		pnd_Old_Mode, new IdenticalSuppress(true),new TabSetting(8),"/RATE",
                		pnd_Old_Rate,new TabSetting(21),"GUAR/AMT",
                		pnd_Tot_Guar, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),"DIV/AMT",
                		pnd_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),"PAY/AMT",
                		pnd_Tot_Pay, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),"FINAL/PAY",
                		pnd_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Tot_Guar.reset();                                                                                                                                     //Natural: RESET #TOT-GUAR #TOT-DIV #TOT-PAY #TOT-FINAL
                pnd_Tot_Div.reset();
                pnd_Tot_Pay.reset();
                pnd_Tot_Final.reset();
                pnd_Old_Rate.setValue(pnd_Output_Rec_Pnd_Rate_Cde);                                                                                                       //Natural: ASSIGN #OLD-RATE := #RATE-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Output_Rec_Pnd_Mode.notEquals(pnd_Old_Mode)))                                                                                               //Natural: IF #MODE NE #OLD-MODE
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"MODE",pnd_Old_Mode,"TOTAL",new TabSetting(21),pnd_M_Tot_Guar, new ReportEditMask            //Natural: WRITE ( 1 ) 2T 'MODE' #OLD-MODE 'TOTAL' 21T #M-TOT-GUAR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T #M-TOT-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T #M-TOT-PAY ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T #M-TOT-FINAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),pnd_M_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),pnd_M_Tot_Pay, new 
                    ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_M_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_M_Tot_Guar.reset();                                                                                                                                   //Natural: RESET #M-TOT-GUAR #M-TOT-DIV #TOT-GUAR #TOT-DIV #M-TOT-FINAL #TOT-FINAL #M-TOT-PAY #M-TOT-DIV
                pnd_M_Tot_Div.reset();
                pnd_Tot_Guar.reset();
                pnd_Tot_Div.reset();
                pnd_M_Tot_Final.reset();
                pnd_Tot_Final.reset();
                pnd_M_Tot_Pay.reset();
                pnd_M_Tot_Div.reset();
                pnd_Old_Mode.setValue(pnd_Old_Mode);                                                                                                                      //Natural: ASSIGN #OLD-MODE := #OLD-MODE
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Guar.nadd(pnd_Output_Rec_Pnd_Guar_Amt);                                                                                                               //Natural: ADD #GUAR-AMT TO #TOT-GUAR
            pnd_Tot_Div.nadd(pnd_Output_Rec_Pnd_Div_Amt);                                                                                                                 //Natural: ADD #DIV-AMT TO #TOT-DIV
            pnd_Tot_Pay.compute(new ComputeParameters(false, pnd_Tot_Pay), pnd_Tot_Pay.add(pnd_Output_Rec_Pnd_Guar_Amt).add(pnd_Output_Rec_Pnd_Div_Amt));                 //Natural: COMPUTE #TOT-PAY = #TOT-PAY + #GUAR-AMT + #DIV-AMT
            pnd_Tot_Final.nadd(pnd_Output_Rec_Pnd_Final_Pay);                                                                                                             //Natural: ADD #FINAL-PAY TO #TOT-FINAL
            pnd_M_Tot_Guar.nadd(pnd_Output_Rec_Pnd_Guar_Amt);                                                                                                             //Natural: ADD #GUAR-AMT TO #M-TOT-GUAR
            pnd_G_Tot_Guar.nadd(pnd_Output_Rec_Pnd_Guar_Amt);                                                                                                             //Natural: ADD #GUAR-AMT TO #G-TOT-GUAR
            pnd_M_Tot_Div.nadd(pnd_Output_Rec_Pnd_Div_Amt);                                                                                                               //Natural: ADD #DIV-AMT TO #M-TOT-DIV
            pnd_G_Tot_Div.nadd(pnd_Output_Rec_Pnd_Div_Amt);                                                                                                               //Natural: ADD #DIV-AMT TO #G-TOT-DIV
            pnd_M_Tot_Pay.compute(new ComputeParameters(false, pnd_M_Tot_Pay), pnd_M_Tot_Pay.add(pnd_Output_Rec_Pnd_Guar_Amt).add(pnd_Output_Rec_Pnd_Div_Amt));           //Natural: COMPUTE #M-TOT-PAY = #M-TOT-PAY + #GUAR-AMT + #DIV-AMT
            pnd_G_Tot_Pay.compute(new ComputeParameters(false, pnd_G_Tot_Pay), pnd_G_Tot_Pay.add(pnd_Output_Rec_Pnd_Guar_Amt).add(pnd_Output_Rec_Pnd_Div_Amt));           //Natural: COMPUTE #G-TOT-PAY = #G-TOT-PAY + #GUAR-AMT + #DIV-AMT
            pnd_M_Tot_Final.nadd(pnd_Output_Rec_Pnd_Final_Pay);                                                                                                           //Natural: ADD #FINAL-PAY TO #M-TOT-FINAL
            pnd_G_Tot_Final.nadd(pnd_Output_Rec_Pnd_Final_Pay);                                                                                                           //Natural: ADD #FINAL-PAY TO #G-TOT-FINAL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(8),pnd_Old_Rate,new TabSetting(21),pnd_Tot_Guar, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new           //Natural: WRITE ( 1 ) 8T #OLD-RATE 21T #TOT-GUAR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T #TOT-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T #TOT-PAY ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T #TOT-FINAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            TabSetting(40),pnd_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),pnd_Tot_Pay, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(76),pnd_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Rte_Cdes.getValue("*"),true), new ExamineSearch(pnd_Old_Rate, true), new ExamineGivingIndex(pnd_I));                        //Natural: EXAMINE FULL #RTE-CDES ( * ) FOR FULL #OLD-RATE GIVING INDEX #I
        if (condition(pnd_I.equals(getZero())))                                                                                                                           //Natural: IF #I = 0
        {
            pnd_Rte_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #RTE-CNT
            pnd_Rte_Cdes.getValue(pnd_Rte_Cnt).setValue(pnd_Old_Rate);                                                                                                    //Natural: ASSIGN #RTE-CDES ( #RTE-CNT ) := #OLD-RATE
            pnd_R_Guar.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Guar);                                                                                                      //Natural: ASSIGN #R-GUAR ( #RTE-CNT ) := #TOT-GUAR
            pnd_R_Div.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Div);                                                                                                        //Natural: ASSIGN #R-DIV ( #RTE-CNT ) := #TOT-DIV
            pnd_R_Tot.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Pay);                                                                                                        //Natural: ASSIGN #R-TOT ( #RTE-CNT ) := #TOT-PAY
            pnd_R_Final.getValue(pnd_Rte_Cnt).setValue(pnd_Tot_Final);                                                                                                    //Natural: ASSIGN #R-FINAL ( #RTE-CNT ) := #TOT-FINAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_R_Guar.getValue(pnd_I).nadd(pnd_Tot_Guar);                                                                                                                //Natural: ADD #TOT-GUAR TO #R-GUAR ( #I )
            pnd_R_Div.getValue(pnd_I).nadd(pnd_Tot_Div);                                                                                                                  //Natural: ADD #TOT-DIV TO #R-DIV ( #I )
            pnd_R_Tot.getValue(pnd_I).nadd(pnd_Tot_Pay);                                                                                                                  //Natural: ADD #TOT-PAY TO #R-TOT ( #I )
            pnd_R_Final.getValue(pnd_I).nadd(pnd_Tot_Final);                                                                                                              //Natural: ADD #TOT-FINAL TO #R-FINAL ( #I )
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SORT-ARRAY
        sub_Sort_Array();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"MODE",pnd_Old_Mode,"TOTAL",new TabSetting(21),pnd_M_Tot_Guar, new ReportEditMask                    //Natural: WRITE ( 1 ) 2T 'MODE' #OLD-MODE 'TOTAL' 21T #M-TOT-GUAR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T #M-TOT-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T #M-TOT-PAY ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T #M-TOT-FINAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),pnd_M_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),pnd_M_Tot_Pay, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_M_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #RTE-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rte_Cnt)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"TOTAL ",pnd_Rte_Cdes.getValue(pnd_I),new TabSetting(21),pnd_R_Guar.getValue(pnd_I),             //Natural: WRITE ( 1 ) 2T 'TOTAL ' #RTE-CDES ( #I ) 21T #R-GUAR ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T #R-DIV ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T #R-TOT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T #R-FINAL ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),pnd_R_Div.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),pnd_R_Tot.getValue(pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_R_Final.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(2),"GRAND TOTAL:",new TabSetting(21),pnd_G_Tot_Guar, new ReportEditMask                 //Natural: WRITE ( 1 ) // 2T 'GRAND TOTAL:' 21T #G-TOT-GUAR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 40T #G-TOT-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 58T #G-TOT-PAY ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 76T #G-TOT-FINAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),pnd_G_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),pnd_G_Tot_Pay, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),pnd_G_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SORT-ARRAY
        //* ***********************************************************************
    }
    private void sub_Sort_Array() throws Exception                                                                                                                        //Natural: SORT-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #RTE-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Rte_Cnt)); pnd_I.nadd(1))
        {
            pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_I.add(1));                                                                                             //Natural: ASSIGN #K := #I + 1
            if (condition(pnd_K.greater(pnd_Rte_Cnt)))                                                                                                                    //Natural: IF #K GT #RTE-CNT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #J #K TO #RTE-CNT
            for (pnd_J.setValue(pnd_K); condition(pnd_J.lessOrEqual(pnd_Rte_Cnt)); pnd_J.nadd(1))
            {
                if (condition(pnd_Rte_Cdes.getValue(pnd_J).less(pnd_Rte_Cdes.getValue(pnd_I))))                                                                           //Natural: IF #RTE-CDES ( #J ) LT #RTE-CDES ( #I )
                {
                    pnd_Output_Rec_Pnd_Rate_Cde.setValue(pnd_Rte_Cdes.getValue(pnd_I));                                                                                   //Natural: ASSIGN #RATE-CDE := #RTE-CDES ( #I )
                    pnd_S_Guar_Amt.setValue(pnd_R_Guar.getValue(pnd_I));                                                                                                  //Natural: ASSIGN #S-GUAR-AMT := #R-GUAR ( #I )
                    pnd_S_Div_Amt.setValue(pnd_R_Div.getValue(pnd_I));                                                                                                    //Natural: ASSIGN #S-DIV-AMT := #R-DIV ( #I )
                    pnd_Tot_Pay.setValue(pnd_R_Tot.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #TOT-PAY := #R-TOT ( #I )
                    pnd_Tot_Final.setValue(pnd_R_Final.getValue(pnd_I));                                                                                                  //Natural: ASSIGN #TOT-FINAL := #R-FINAL ( #I )
                    pnd_Rte_Cdes.getValue(pnd_I).setValue(pnd_Rte_Cdes.getValue(pnd_J));                                                                                  //Natural: ASSIGN #RTE-CDES ( #I ) := #RTE-CDES ( #J )
                    pnd_R_Guar.getValue(pnd_I).setValue(pnd_R_Guar.getValue(pnd_J));                                                                                      //Natural: ASSIGN #R-GUAR ( #I ) := #R-GUAR ( #J )
                    pnd_R_Div.getValue(pnd_I).setValue(pnd_R_Div.getValue(pnd_J));                                                                                        //Natural: ASSIGN #R-DIV ( #I ) := #R-DIV ( #J )
                    pnd_R_Tot.getValue(pnd_I).setValue(pnd_R_Tot.getValue(pnd_J));                                                                                        //Natural: ASSIGN #R-TOT ( #I ) := #R-TOT ( #J )
                    pnd_R_Final.getValue(pnd_I).setValue(pnd_R_Final.getValue(pnd_J));                                                                                    //Natural: ASSIGN #R-FINAL ( #I ) := #R-FINAL ( #J )
                    pnd_Rte_Cdes.getValue(pnd_J).setValue(pnd_Output_Rec_Pnd_Rate_Cde);                                                                                   //Natural: ASSIGN #RTE-CDES ( #J ) := #RATE-CDE
                    pnd_R_Guar.getValue(pnd_J).setValue(pnd_S_Guar_Amt);                                                                                                  //Natural: ASSIGN #R-GUAR ( #J ) := #S-GUAR-AMT
                    pnd_R_Div.getValue(pnd_J).setValue(pnd_S_Div_Amt);                                                                                                    //Natural: ASSIGN #R-DIV ( #J ) := #S-DIV-AMT
                    pnd_R_Tot.getValue(pnd_J).setValue(pnd_Tot_Pay);                                                                                                      //Natural: ASSIGN #R-TOT ( #J ) := #TOT-PAY
                    pnd_R_Final.getValue(pnd_J).setValue(pnd_Tot_Final);                                                                                                  //Natural: ASSIGN #R-FINAL ( #J ) := #TOT-FINAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(20),"IPRO RATE BY RATE BY MODE REPORT",new        //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 20X 'IPRO RATE BY RATE BY MODE REPORT' 20X CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY )
                        ColumnSpacing(20),cntrl_Cntrl_Todays_Dte, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(30),"TPA/IPRO CONVERSION PROJECT",new ColumnSpacing(20),"PAGE",getReports().getPageNumberDbs(1), //Natural: WRITE ( 1 ) 30X 'TPA/IPRO CONVERSION PROJECT' 20X 'PAGE' *PAGE-NUMBER ( 1 ) //
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133");
        Global.format(2, "PS=66 LS=133");

        getReports().setDisplayColumns(1, new TabSetting(2),"/MODE",
        		pnd_Old_Mode, new IdenticalSuppress(true),new TabSetting(8),"/RATE",
        		pnd_Old_Rate,new TabSetting(21),"GUAR/AMT",
        		pnd_Tot_Guar, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(40),"DIV/AMT",
        		pnd_Tot_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),"PAY/AMT",
        		pnd_Tot_Pay, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(76),"FINAL/PAY",
        		pnd_Tot_Final, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
    }
}
