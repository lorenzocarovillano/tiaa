/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:37:46 PM
**        * FROM NATURAL PROGRAM : Iatp360
************************************************************
**        * FILE NAME            : Iatp360.java
**        * CLASS NAME           : Iatp360
**        * INSTANCE NAME        : Iatp360
************************************************************
************************************************************************
* PROGRAM  : IATP360
* SYSTEM   : IAS
* TITLE    : IA POST RETIREMENT FLEXIBILITIES
* GENERATED: SEP. 3, 1997
* FUNCTION : THIS PROGRAM CREATES WORK FILES TO BE SORTED AND USED TO
*            REPORT DAILY AND CUMULATIVE IA TRANSFERS AND SWITCHES.
*            THIS MODULE EXTRACTS THE IA TRANSFER AND SWITCH
*            REQUESTS USING THE IA TRANSFER/SWITCH KDO FILE.
*
*    MAPS USED ARE IATM361H - HEADINGS, PAGE COUNT, DATE AND TIME
*
* HISTORY :-
* 04/2016  RC :  COR NAAD DECOMM - CHANGED TO CALL MDMN100
* 08/2015  RC :  COR NAAD DECOMM - SCAN ON 08/2015
* 05/99 RM.   :- ADDED CHANGED VIEW IAA-TRNSFR-SW-RQST
* 11/97 LEN B :- ADDED READ TO WORK FILE 5 TO RETRIEVE REPORT DATE
*
*       WORK FILE 1 = DAILY TRANSFER RECORDS
*       WORK FILE 2 = CUMULATIVE TRANSFER RECORDS
*       WORK FILE 3 = DAILY SWITCH RECORDS
*       WORK FILE 4 = CUMULATIVE SWITCH RECORDS
*       WORK FILE 5 = RUN DATE OF REPORT
*       WORK FILE 6 = TRANSFERS DELETED TODAY BUT ADDED PRIOR TO TODAY
*       WORK FILE 7 = SWITCHES DELETED TODAY BUT ADDED PRIOR TO TODAY
*
* JUN 2017 J BREMER       PIN EXPANSION STOW 06/2017
* 04/2017  O SOTTO  ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
*********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp360 extends BLNatBase
{
    // Data Areas
    private PdaMdma101 pdaMdma101;
    private LdaIatl361 ldaIatl361;
    private LdaIatl365 ldaIatl365;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Srce;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Ssn;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta;

    private DbsGroup iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_To_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Issue;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Stat_Deleted;
    private DbsField pnd_Report_Date_A8;

    private DbsGroup pnd_Report_Date_A8__R_Field_1;
    private DbsField pnd_Report_Date_A8_Pnd_Report_Date_N8;
    private DbsField pnd_Current_Dte;

    private DbsGroup pnd_Current_Dte__R_Field_2;
    private DbsField pnd_Current_Dte_Pnd_Current_Dte_A;

    private DbsGroup pnd_Current_Dte__R_Field_3;
    private DbsField pnd_Current_Dte_Pnd_Current_Dte_Cy;
    private DbsField pnd_Current_Dte_Pnd_Current_Dte_Mm;
    private DbsField pnd_Current_Dte_Pnd_Current_Dte_Dd;
    private DbsField pnd_Compare_Dte;

    private DbsGroup pnd_Compare_Dte__R_Field_4;
    private DbsField pnd_Compare_Dte_Pnd_Compare_Dte_N;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_5;
    private DbsField pnd_Start_Date_Pnd_Start_Date_A;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_6;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_Del_Date;

    private DbsGroup pnd_Del_Date__R_Field_7;
    private DbsField pnd_Del_Date_Pnd_Del_Date_N;
    private DbsField pnd_Stat_1_2;

    private DbsGroup pnd_Stat_1_2__R_Field_8;
    private DbsField pnd_Stat_1_2_Pnd_Stat_1;
    private DbsField pnd_Stat_1_2_Pnd_Stat_2;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_9;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Write_Debug;
    private DbsField pnd_Write_Limit;
    private DbsField pnd_Fn;
    private DbsField pnd_Fnp;
    private DbsField pnd_Mn;
    private DbsField pnd_Mnp;
    private DbsField pnd_Rec_Read_Ctr;
    private DbsField pnd_Rec_Ctr1;
    private DbsField pnd_Rec_Ctr2;
    private DbsField pnd_Rec_Ctr3;
    private DbsField pnd_Rec_Ctr4;
    private DbsField pnd_Rec_Ctr6;
    private DbsField pnd_Rec_Ctr7;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M361h_Busns_Dte;

    private DbsGroup pnd_M361h_Busns_Dte__R_Field_10;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy;
    private DbsField pnd_M361h_Page;
    private DbsField pnd_Hold_Prtcpnt_Nme;
    private DbsField pnd_Program;
    private DbsField pnd_I;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        ldaIatl361 = new LdaIatl361();
        registerRecord(ldaIatl361);
        ldaIatl365 = new LdaIatl365();
        registerRecord(ldaIatl365);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_ENTRY_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "RQST_RCVD_TME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_RCVD_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RQST_ENTRY_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_CNTCT_MDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Srce = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_SRCE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_STTMNT_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_OPN_CLSD_IND");
        iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RQST_RCPRCL_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Ssn = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "RQST_SSN");
        iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "XFR_WORK_PRCSS_ID");
        iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "XFR_MIT_LOG_DTE_TME");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_In_Progress_Ind", "XFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "XFR_IN_PROGRESS_IND");
        iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Retry_Cnt", "XFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "XFR_RETRY_CNT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "IA_HOLD_CDE");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_Frm_Acct_Dta", 
            "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Asset_Amt", "XFR-FRM-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ASSET_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Count_Castxfr_To_Acct_Dta", 
            "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newGroupInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt = iaa_Trnsfr_Sw_Rqst_Xfr_To_Acct_Dta.newFieldArrayInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_To_Asset_Amt", "XFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ASSET_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_To_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IA_APPL_RCVD_DTE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Issue = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_RSN_FOR_OVRRDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_OVRRDE_USER_ID");
        iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_GNRL_PEND_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Xfr_Type", "RQST-XFR-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RQST_XFR_TYPE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Unit_Cde", "RQST-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RQST_UNIT_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_New_Iss_Prt_Pull", "IA-NEW-ISS-PRT-PULL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISS_PRT_PULL");
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Stat_Deleted = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Stat_Deleted", "#STAT-DELETED", FieldType.STRING, 1);
        pnd_Report_Date_A8 = localVariables.newFieldInRecord("pnd_Report_Date_A8", "#REPORT-DATE-A8", FieldType.STRING, 8);

        pnd_Report_Date_A8__R_Field_1 = localVariables.newGroupInRecord("pnd_Report_Date_A8__R_Field_1", "REDEFINE", pnd_Report_Date_A8);
        pnd_Report_Date_A8_Pnd_Report_Date_N8 = pnd_Report_Date_A8__R_Field_1.newFieldInGroup("pnd_Report_Date_A8_Pnd_Report_Date_N8", "#REPORT-DATE-N8", 
            FieldType.NUMERIC, 8);
        pnd_Current_Dte = localVariables.newFieldInRecord("pnd_Current_Dte", "#CURRENT-DTE", FieldType.NUMERIC, 8);

        pnd_Current_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Current_Dte__R_Field_2", "REDEFINE", pnd_Current_Dte);
        pnd_Current_Dte_Pnd_Current_Dte_A = pnd_Current_Dte__R_Field_2.newFieldInGroup("pnd_Current_Dte_Pnd_Current_Dte_A", "#CURRENT-DTE-A", FieldType.STRING, 
            8);

        pnd_Current_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Current_Dte__R_Field_3", "REDEFINE", pnd_Current_Dte);
        pnd_Current_Dte_Pnd_Current_Dte_Cy = pnd_Current_Dte__R_Field_3.newFieldInGroup("pnd_Current_Dte_Pnd_Current_Dte_Cy", "#CURRENT-DTE-CY", FieldType.STRING, 
            4);
        pnd_Current_Dte_Pnd_Current_Dte_Mm = pnd_Current_Dte__R_Field_3.newFieldInGroup("pnd_Current_Dte_Pnd_Current_Dte_Mm", "#CURRENT-DTE-MM", FieldType.STRING, 
            2);
        pnd_Current_Dte_Pnd_Current_Dte_Dd = pnd_Current_Dte__R_Field_3.newFieldInGroup("pnd_Current_Dte_Pnd_Current_Dte_Dd", "#CURRENT-DTE-DD", FieldType.STRING, 
            2);
        pnd_Compare_Dte = localVariables.newFieldInRecord("pnd_Compare_Dte", "#COMPARE-DTE", FieldType.STRING, 8);

        pnd_Compare_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Compare_Dte__R_Field_4", "REDEFINE", pnd_Compare_Dte);
        pnd_Compare_Dte_Pnd_Compare_Dte_N = pnd_Compare_Dte__R_Field_4.newFieldInGroup("pnd_Compare_Dte_Pnd_Compare_Dte_N", "#COMPARE-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);

        pnd_Start_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_5", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_A = pnd_Start_Date__R_Field_5.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 
            8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Date__R_Field_6", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Del_Date = localVariables.newFieldInRecord("pnd_Del_Date", "#DEL-DATE", FieldType.STRING, 8);

        pnd_Del_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Del_Date__R_Field_7", "REDEFINE", pnd_Del_Date);
        pnd_Del_Date_Pnd_Del_Date_N = pnd_Del_Date__R_Field_7.newFieldInGroup("pnd_Del_Date_Pnd_Del_Date_N", "#DEL-DATE-N", FieldType.NUMERIC, 8);
        pnd_Stat_1_2 = localVariables.newFieldInRecord("pnd_Stat_1_2", "#STAT-1-2", FieldType.STRING, 2);

        pnd_Stat_1_2__R_Field_8 = localVariables.newGroupInRecord("pnd_Stat_1_2__R_Field_8", "REDEFINE", pnd_Stat_1_2);
        pnd_Stat_1_2_Pnd_Stat_1 = pnd_Stat_1_2__R_Field_8.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_1", "#STAT-1", FieldType.STRING, 1);
        pnd_Stat_1_2_Pnd_Stat_2 = pnd_Stat_1_2__R_Field_8.newFieldInGroup("pnd_Stat_1_2_Pnd_Stat_2", "#STAT-2", FieldType.STRING, 1);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_9", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_9.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Write_Debug = localVariables.newFieldInRecord("pnd_Write_Debug", "#WRITE-DEBUG", FieldType.BOOLEAN, 1);
        pnd_Write_Limit = localVariables.newFieldInRecord("pnd_Write_Limit", "#WRITE-LIMIT", FieldType.NUMERIC, 3);
        pnd_Fn = localVariables.newFieldInRecord("pnd_Fn", "#FN", FieldType.STRING, 1);
        pnd_Fnp = localVariables.newFieldInRecord("pnd_Fnp", "#FNP", FieldType.STRING, 2);
        pnd_Mn = localVariables.newFieldInRecord("pnd_Mn", "#MN", FieldType.STRING, 1);
        pnd_Mnp = localVariables.newFieldInRecord("pnd_Mnp", "#MNP", FieldType.STRING, 2);
        pnd_Rec_Read_Ctr = localVariables.newFieldInRecord("pnd_Rec_Read_Ctr", "#REC-READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr1 = localVariables.newFieldInRecord("pnd_Rec_Ctr1", "#REC-CTR1", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr2 = localVariables.newFieldInRecord("pnd_Rec_Ctr2", "#REC-CTR2", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr3 = localVariables.newFieldInRecord("pnd_Rec_Ctr3", "#REC-CTR3", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr4 = localVariables.newFieldInRecord("pnd_Rec_Ctr4", "#REC-CTR4", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr6 = localVariables.newFieldInRecord("pnd_Rec_Ctr6", "#REC-CTR6", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ctr7 = localVariables.newFieldInRecord("pnd_Rec_Ctr7", "#REC-CTR7", FieldType.PACKED_DECIMAL, 9);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M361h_Busns_Dte = localVariables.newFieldInRecord("pnd_M361h_Busns_Dte", "#M361H-BUSNS-DTE", FieldType.NUMERIC, 8);

        pnd_M361h_Busns_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_M361h_Busns_Dte__R_Field_10", "REDEFINE", pnd_M361h_Busns_Dte);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm", "#M361H-BUSNS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd", "#M361H-BUSNS-DTE-DD", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy = pnd_M361h_Busns_Dte__R_Field_10.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy", "#M361H-BUSNS-DTE-CY", 
            FieldType.STRING, 4);
        pnd_M361h_Page = localVariables.newFieldInRecord("pnd_M361h_Page", "#M361H-PAGE", FieldType.NUMERIC, 5);
        pnd_Hold_Prtcpnt_Nme = localVariables.newFieldInRecord("pnd_Hold_Prtcpnt_Nme", "#HOLD-PRTCPNT-NME", FieldType.STRING, 25);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();

        ldaIatl361.initializeValues();
        ldaIatl365.initializeValues();

        localVariables.reset();
        pnd_Const_Pnd_Stat_Deleted.setInitialValue("N");
        pnd_Write_Limit.setInitialValue(3);
        pnd_Header1.setInitialValue("            POST SETTLEMENT FLEXIBILITIES ");
        pnd_Header2.setInitialValue("          Transfer/Switch Work file Report");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp360() throws Exception
    {
        super("Iatp360");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        //* ======================================================================
        //*                      START OF PROGRAM
        //* =====================================================================
        //*  OPEN MQ 04/2016
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        //*   #WRITE-DEBUG := TRUE  /* TESTING ONLY
        //*  LB 11/97
                                                                                                                                                                          //Natural: PERFORM SET-REPORT-DATE
        sub_Set_Report_Date();
        if (condition(Global.isEscape())) {return;}
        //*  READ, IAA TRNSFR SW DATABASE AND
        //*  SELECT AND WRITE RECORDS TO DIFFERENT WORK FILES BASED ON
        //*  REQUEST TYPE AND DATE CRITERIA
        vw_iaa_Trnsfr_Sw_Rqst.startDatabaseRead                                                                                                                           //Natural: READ IAA-TRNSFR-SW-RQST LOGICAL BY IA-SUPER-DE-04
        (
        "RD1",
        new Oc[] { new Oc("IA_SUPER_DE_04", "ASC") }
        );
        RD1:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("RD1")))
        {
            //*  CHECK THAT THIS RECORD IS NOT A RECORD THAT WAS DELETED DUE TO A
            //*  CHANGE ACTION
            getReports().write(0, ReportOption.NOTITLE," READT TEST ",iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde,"=",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                              //Natural: WRITE ' READT TEST ' IAA-TRNSFR-SW-RQST.XFR-STTS-CDE '=' IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde.equals("N8")))                                                                                                  //Natural: IF IAA-TRNSFR-SW-RQST.XFR-STTS-CDE = 'N8'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, ReportOption.NOTITLE,"ACCEPTED ",iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                     //Natural: WRITE 'ACCEPTED ' IAA-TRNSFR-SW-RQST.IA-FRM-CNTRCT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rec_Read_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #REC-READ-CTR
            //*  PERFORM GET-NAME
            //*  MOVE EDITED NAD-MTRTY-NON-PRM-DTE (EM=YYYYMMDD) TO #DATE
            pnd_Compare_Dte.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Entry_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-ENTRY-DTE ( EM = YYYYMMDD ) TO #COMPARE-DTE
            if (condition(pnd_Write_Debug.getBoolean()))                                                                                                                  //Natural: IF #WRITE-DEBUG
            {
                if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                             //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                {
                    getReports().write(0, ReportOption.NOTITLE,"*");                                                                                                      //Natural: WRITE '*'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*","NEXT RECORD");                                                                                        //Natural: WRITE '*' 'NEXT RECORD'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"*");                                                                                                      //Natural: WRITE '*'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"=",iaa_Trnsfr_Sw_Rqst_Rqst_Rcvd_Dte);                                                                     //Natural: WRITE '=' IAA-TRNSFR-SW-RQST.RQST-RCVD-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"=",pnd_Current_Dte);                                                                                      //Natural: WRITE '=' #CURRENT-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"=",iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde);                                                                     //Natural: WRITE '=' IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            short decideConditionsMet454 = 0;                                                                                                                             //Natural: WHEN #COMPARE-DTE-N EQ #CURRENT-DTE
            if (condition(pnd_Compare_Dte_Pnd_Compare_Dte_N.equals(pnd_Current_Dte)))
            {
                decideConditionsMet454++;
                if (condition(pnd_Write_Debug.getBoolean()))                                                                                                              //Natural: IF #WRITE-DEBUG
                {
                    getReports().write(0, ReportOption.NOTITLE," RECEIVED DATE EQ CURRENT DATE");                                                                         //Natural: WRITE ' RECEIVED DATE EQ CURRENT DATE'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  SELECT TRANSFERS (DAILY)
                                                                                                                                                                          //Natural: PERFORM GET-NAME
                sub_Get_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  1= TRANSFER 2=SWITCH
                if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals("1")))                                                                                              //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE = '1'
                {
                    ldaIatl361.getIat_Trnsfr_Rpt().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IAT-TRNSFR-RPT
                    ldaIatl361.getIat_Trnsfr_Rpt_Prtcpnt_Nme().setValue(pnd_Hold_Prtcpnt_Nme);                                                                            //Natural: ASSIGN IAT-TRNSFR-RPT.PRTCPNT-NME := #HOLD-PRTCPNT-NME
                    ldaIatl361.getIat_Trnsfr_Rpt_Reval_Mthd().setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ.getValue(1));                                                  //Natural: ASSIGN IAT-TRNSFR-RPT.REVAL-MTHD := IAA-TRNSFR-SW-RQST.XFR-FRM-UNIT-TYP ( 1 )
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE," Case is for daily transfer report ");                                                                //Natural: WRITE ' Case is for daily transfer report '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 1","DAILY TRANSFER");                                                      //Natural: WRITE ' BEFORE WRITE TO WORK FILE 1' 'DAILY TRANSFER'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                     //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                        {
                            getReports().write(0, ReportOption.NOTITLE," ");                                                                                              //Natural: WRITE ' '
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"TRANSFER RECORD");                                                                                //Natural: WRITE 'TRANSFER RECORD'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Reval_Mthd());                                                    //Natural: WRITE '=' IAT-TRNSFR-RPT.REVAL-MTHD
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-CNTCT-MDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rcrd_Type_Cde());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RCRD-TYPE-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Id());                                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Effctv_Dte());                                               //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-EFFCTV-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Entry_Dte());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ENTRY-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Entry_Tme());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ENTRY-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Rcvd_Dte());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-RCVD-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Rcvd_Tme());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-RCVD-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind());                                             //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-OPN-CLSD-IND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-STTS-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Rjctn_Cde());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-RJCTN-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Frm_Payee());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-FRM-PAYEE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-UNIQUE-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Frm_Cntrct());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-FRM-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(1));                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ().getValue(1));                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue(1));                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Qty().getValue(1));                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_To_Cntrct());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-TO-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(1));                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Unit_Typ().getValue(1));                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Typ().getValue(1));                                        //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Qty().getValue(1));                                        //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Prtcpnt_Nme());                                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.PRTCPNT-NME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  #WRITE-DEBUG
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE WORK FILE 1
                    getWorkFiles().write(1, false, ldaIatl361.getIat_Trnsfr_Rpt());                                                                                       //Natural: WRITE WORK FILE 1 IAT-TRNSFR-RPT
                    pnd_Rec_Ctr1.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR1
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," Case is for cumul transfer report ");                                                                //Natural: WRITE ' Case is for cumul transfer report '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 2"," CUMU TRANSFER");                                                      //Natural: WRITE ' BEFORE WRITE TO WORK FILE 2' ' CUMU TRANSFER'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE TO THE CUMMULATIVE REPORT WORK FILE
                    //*  WRITE WORK FILE 2
                    getWorkFiles().write(2, false, ldaIatl361.getIat_Trnsfr_Rpt());                                                                                       //Natural: WRITE WORK FILE 2 IAT-TRNSFR-RPT
                    pnd_Rec_Ctr2.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR2
                    //*  SWITHCES (DAILY)
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl365.getIat_Switch_Rpt().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IAT-SWITCH-RPT
                    ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme().setValue(pnd_Hold_Prtcpnt_Nme);                                                                            //Natural: ASSIGN IAT-SWITCH-RPT.PRTCPNT-NME := #HOLD-PRTCPNT-NME
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 3"," DAILY SWITCH");                                                       //Natural: WRITE ' BEFORE WRITE TO WORK FILE 3' ' DAILY SWITCH'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                     //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                        {
                            getReports().write(0, ReportOption.NOTITLE," ");                                                                                              //Natural: WRITE ' '
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"SWITCH RECORD");                                                                                  //Natural: WRITE 'SWITCH RECORD'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-CNTCT-MDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rcrd_Type_Cde());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RCRD-TYPE-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Id());                                                       //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Effctv_Dte());                                               //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-EFFCTV-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Dte());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ENTRY-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Tme());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ENTRY-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Dte());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-RCVD-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Tme());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-RCVD-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Opn_Clsd_Ind());                                             //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-OPN-CLSD-IND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-STTS-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Rjctn_Cde());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-RJCTN-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                  //Natural: WRITE '=' IAT-SWITCH-RPT.IA-UNIQUE-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.IA-FRM-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Payee());                                                  //Natural: WRITE '=' IAT-SWITCH-RPT.IA-FRM-PAYEE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(1));                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(1));                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(1));                                       //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(1));                                       //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme());                                                   //Natural: WRITE '=' IAT-SWITCH-RPT.PRTCPNT-NME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE WORK FILE 3
                    getWorkFiles().write(3, false, ldaIatl365.getIat_Switch_Rpt());                                                                                       //Natural: WRITE WORK FILE 3 IAT-SWITCH-RPT
                    pnd_Rec_Ctr3.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR3
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 4","CUMU SWITCH");                                                         //Natural: WRITE ' BEFORE WRITE TO WORK FILE 4' 'CUMU SWITCH'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE WORK FILE 4
                    getWorkFiles().write(4, false, ldaIatl365.getIat_Switch_Rpt());                                                                                       //Natural: WRITE WORK FILE 4 IAT-SWITCH-RPT
                    pnd_Rec_Ctr4.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR4
                    //*  LB 11/97
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #COMPARE-DTE-N LT #CURRENT-DTE
            else if (condition(pnd_Compare_Dte_Pnd_Compare_Dte_N.less(pnd_Current_Dte)))
            {
                decideConditionsMet454++;
                if (condition(pnd_Write_Debug.getBoolean()))                                                                                                              //Natural: IF #WRITE-DEBUG
                {
                    if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                         //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                    {
                        getReports().write(0, ReportOption.NOTITLE,"RECEIVED DATE NOT = CURRENT DATE","CUMU FILES");                                                      //Natural: WRITE 'RECEIVED DATE NOT = CURRENT DATE' 'CUMU FILES'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  SELECT TRANSFERS FOR CUMULATIVE FILE
                if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals("1")))                                                                                              //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE = '1'
                {
                    ldaIatl361.getIat_Trnsfr_Rpt().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IAT-TRNSFR-RPT
                    ldaIatl361.getIat_Trnsfr_Rpt_Prtcpnt_Nme().setValue(pnd_Hold_Prtcpnt_Nme);                                                                            //Natural: ASSIGN IAT-TRNSFR-RPT.PRTCPNT-NME := #HOLD-PRTCPNT-NME
                    ldaIatl361.getIat_Trnsfr_Rpt_Reval_Mthd().setValue(iaa_Trnsfr_Sw_Rqst_Xfr_Frm_Unit_Typ.getValue(1));                                                  //Natural: ASSIGN IAT-TRNSFR-RPT.REVAL-MTHD := IAA-TRNSFR-SW-RQST.XFR-FRM-UNIT-TYP ( 1 )
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE,"  ");                                                                                                 //Natural: WRITE '  '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 2"," CUMU TRANSFER");                                                      //Natural: WRITE ' BEFORE WRITE TO WORK FILE 2' ' CUMU TRANSFER'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                     //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                        {
                            getReports().write(0, ReportOption.NOTITLE,"  ");                                                                                             //Natural: WRITE '  '
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"TRANSFER RECORD");                                                                                //Natural: WRITE 'TRANSFER RECORD'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Reval_Mthd());                                                    //Natural: WRITE '=' IAT-TRNSFR-RPT.REVAL-MTHD
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Cntct_Mde());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-CNTCT-MDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rcrd_Type_Cde());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RCRD-TYPE-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Id());                                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Effctv_Dte());                                               //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-EFFCTV-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Entry_Dte());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ENTRY-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Entry_Tme());                                                //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-ENTRY-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Rcvd_Dte());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-RCVD-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Rcvd_Tme());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-RCVD-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Rqst_Opn_Clsd_Ind());                                             //Natural: WRITE '=' IAT-TRNSFR-RPT.RQST-OPN-CLSD-IND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-STTS-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Rjctn_Cde());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-RJCTN-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Frm_Payee());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-FRM-PAYEE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Unique_Id());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-UNIQUE-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_Frm_Cntrct());                                                 //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-FRM-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Acct_Cde().getValue(1));                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Unit_Typ().getValue(1));                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Typ().getValue(1));                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Frm_Qty().getValue(1));                                       //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-FRM-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Ia_To_Cntrct());                                                  //Natural: WRITE '=' IAT-TRNSFR-RPT.IA-TO-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Acct_Cde().getValue(1));                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Unit_Typ().getValue(1));                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Typ().getValue(1));                                        //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Xfr_To_Qty().getValue(1));                                        //Natural: WRITE '=' IAT-TRNSFR-RPT.XFR-TO-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl361.getIat_Trnsfr_Rpt_Prtcpnt_Nme());                                                   //Natural: WRITE '=' IAT-TRNSFR-RPT.PRTCPNT-NME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE WORK FILE 2
                    getWorkFiles().write(2, false, ldaIatl361.getIat_Trnsfr_Rpt());                                                                                       //Natural: WRITE WORK FILE 2 IAT-TRNSFR-RPT
                    pnd_Rec_Ctr2.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR2
                    pnd_Stat_1_2.setValue(ldaIatl361.getIat_Trnsfr_Rpt_Xfr_Stts_Cde());                                                                                   //Natural: ASSIGN #STAT-1-2 := IAT-TRNSFR-RPT.XFR-STTS-CDE
                    pnd_Del_Date.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DEL-DATE
                    if (condition(pnd_Stat_1_2_Pnd_Stat_1.equals(pnd_Const_Pnd_Stat_Deleted) && pnd_Del_Date_Pnd_Del_Date_N.equals(pnd_Current_Dte)))                     //Natural: IF #STAT-1 = #STAT-DELETED AND #DEL-DATE-N = #CURRENT-DTE
                    {
                        pnd_Rec_Ctr6.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REC-CTR6
                        //*  WRITE WORK FILE 6
                        getWorkFiles().write(6, false, ldaIatl361.getIat_Trnsfr_Rpt());                                                                                   //Natural: WRITE WORK FILE 6 IAT-TRNSFR-RPT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SWITCH REQUEST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatl365.getIat_Switch_Rpt().setValuesByName(vw_iaa_Trnsfr_Sw_Rqst);                                                                                //Natural: MOVE BY NAME IAA-TRNSFR-SW-RQST TO IAT-SWITCH-RPT
                    ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme().setValue(pnd_Hold_Prtcpnt_Nme);                                                                            //Natural: ASSIGN IAT-SWITCH-RPT.PRTCPNT-NME := #HOLD-PRTCPNT-NME
                    if (condition(pnd_Write_Debug.getBoolean()))                                                                                                          //Natural: IF #WRITE-DEBUG
                    {
                        getReports().write(0, ReportOption.NOTITLE," ");                                                                                                  //Natural: WRITE ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOTITLE," BEFORE WRITE TO WORK FILE 4","CUMM SWITCH");                                                         //Natural: WRITE ' BEFORE WRITE TO WORK FILE 4' 'CUMM SWITCH'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Rec_Read_Ctr.lessOrEqual(pnd_Write_Limit)))                                                                                     //Natural: IF #REC-READ-CTR LE #WRITE-LIMIT
                        {
                            getReports().write(0, ReportOption.NOTITLE," ");                                                                                              //Natural: WRITE ' '
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"SWITCH RECORD");                                                                                  //Natural: WRITE 'SWITCH RECORD'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-CNTCT-MDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rcrd_Type_Cde());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RCRD-TYPE-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Id());                                                       //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Effctv_Dte());                                               //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-EFFCTV-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Dte());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ENTRY-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Tme());                                                //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-ENTRY-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Dte());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-RCVD-DTE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Tme());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-RCVD-TME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Rqst_Opn_Clsd_Ind());                                             //Natural: WRITE '=' IAT-SWITCH-RPT.RQST-OPN-CLSD-IND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-STTS-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Rjctn_Cde());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-RJCTN-CDE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                  //Natural: WRITE '=' IAT-SWITCH-RPT.IA-UNIQUE-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct());                                                 //Natural: WRITE '=' IAT-SWITCH-RPT.IA-FRM-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(1));                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-ACCT-CDE ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(1));                                  //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-UNIT-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(1));                                       //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-TYP ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(1));                                       //Natural: WRITE '=' IAT-SWITCH-RPT.XFR-FRM-QTY ( 1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, ReportOption.NOTITLE,"=",ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme());                                                   //Natural: WRITE '=' IAT-SWITCH-RPT.PRTCPNT-NME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WRITE WORK FILE 4
                    getWorkFiles().write(4, false, ldaIatl365.getIat_Switch_Rpt());                                                                                       //Natural: WRITE WORK FILE 4 IAT-SWITCH-RPT
                    pnd_Rec_Ctr4.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CTR4
                    pnd_Stat_1_2.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                                   //Natural: ASSIGN #STAT-1-2 := IAT-SWITCH-RPT.XFR-STTS-CDE
                    pnd_Del_Date.setValueEdited(iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED IAA-TRNSFR-SW-RQST.RQST-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #DEL-DATE
                    if (condition(pnd_Stat_1_2_Pnd_Stat_1.equals(pnd_Const_Pnd_Stat_Deleted) && pnd_Del_Date_Pnd_Del_Date_N.equals(pnd_Current_Dte)))                     //Natural: IF #STAT-1 = #STAT-DELETED AND #DEL-DATE-N = #CURRENT-DTE
                    {
                        pnd_Rec_Ctr7.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REC-CTR7
                        //*  WRITE WORK FILE 7
                        getWorkFiles().write(7, false, ldaIatl365.getIat_Switch_Rpt());                                                                                   //Natural: WRITE WORK FILE 7 IAT-SWITCH-RPT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  CLOSE MQ 04/2016
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"Transfers");                                                                                                  //Natural: WRITE / 'Transfers'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Daily transfer report records output      -",pnd_Rec_Ctr1);                                             //Natural: WRITE / 'TOTAL Daily transfer report records output      -' #REC-CTR1
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Cumulative transfer report records output -",pnd_Rec_Ctr2);                                             //Natural: WRITE / 'TOTAL Cumulative transfer report records output -' #REC-CTR2
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Prior Transfer Deletions                  -",pnd_Rec_Ctr6);                                             //Natural: WRITE / 'TOTAL Prior Transfer Deletions                  -' #REC-CTR6
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Switches");                                                                                           //Natural: WRITE / / 'Switches'
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Daily switch report records output        -",pnd_Rec_Ctr3);                                             //Natural: WRITE / 'TOTAL Daily switch report records output        -' #REC-CTR3
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Cumulative switch report records output   -",pnd_Rec_Ctr4);                                             //Natural: WRITE / 'TOTAL Cumulative switch report records output   -' #REC-CTR4
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"TOTAL Prior Switch Deletions                    -",pnd_Rec_Ctr7);                                             //Natural: WRITE / 'TOTAL Prior Switch Deletions                    -' #REC-CTR7
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,new ColumnSpacing(45)," END REPORT  ","IATP360");                                                                      //Natural: WRITE 45X ' END REPORT  ' 'IATP360'
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        //*  ======================================================================
        //*             PROGRAM SUBROUTINES
        //*  ======================================================================
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-REPORT-DATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* * 08/2015 - END
        //*     IATP360   /*
    }
    private void sub_Set_Report_Date() throws Exception                                                                                                                   //Natural: SET-REPORT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INITIAL DETAIL SETUP                                       /* LB 11/97
        getWorkFiles().read(5, pnd_Report_Date_A8);                                                                                                                       //Natural: READ WORK FILE 5 ONCE #REPORT-DATE-A8
        if (condition(pnd_Report_Date_A8.equals("99999999")))                                                                                                             //Natural: IF #REPORT-DATE-A8 = '99999999'
        {
            pnd_Current_Dte.setValue(Global.getDATN());                                                                                                                   //Natural: ASSIGN #CURRENT-DTE := *DATN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CHECK DATE FORMAT
            if (condition(DbsUtil.maskMatches(pnd_Report_Date_A8,"YYYYMMDD")))                                                                                            //Natural: IF #REPORT-DATE-A8 EQ MASK ( YYYYMMDD )
            {
                pnd_Current_Dte.setValue(pnd_Report_Date_A8_Pnd_Report_Date_N8);                                                                                          //Natural: MOVE #REPORT-DATE-N8 TO #CURRENT-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, ReportOption.NOTITLE,"**************************************************");                                                         //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"*                                                *");                                                         //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"* ================ ERROR ======================= *");                                                         //Natural: WRITE '* ================ ERROR ======================= *'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"* The Requested Date for the Report is           *");                                                         //Natural: WRITE '* The Requested Date for the Report is           *'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"* not in the format YYYYMMDD.                    *");                                                         //Natural: WRITE '* not in the format YYYYMMDD.                    *'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"* Request Date = ",pnd_Report_Date_A8);                                                                       //Natural: WRITE '* Request Date = ' #REPORT-DATE-A8
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"*                                                *");                                                         //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"**************************************************");                                                         //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy.setValue(pnd_Current_Dte_Pnd_Current_Dte_Cy);                                                                          //Natural: ASSIGN #M361H-BUSNS-DTE-CY := #CURRENT-DTE-CY
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm.setValue(pnd_Current_Dte_Pnd_Current_Dte_Mm);                                                                          //Natural: ASSIGN #M361H-BUSNS-DTE-MM := #CURRENT-DTE-MM
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd.setValue(pnd_Current_Dte_Pnd_Current_Dte_Dd);                                                                          //Natural: ASSIGN #M361H-BUSNS-DTE-DD := #CURRENT-DTE-DD
        //*  SET-REPORT-DATE
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  08/2015 - START
        //*  CHANGED TO USE MDMN100A                        /* 04/2016
        //*  -----------------------------------------------------------------
        //*  RESET #MDMA100                                 /* 082017
        //*  082017
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(iaa_Trnsfr_Sw_Rqst_Rqst_Ssn);                                                                                      //Natural: ASSIGN #I-SSN := IAA-TRNSFR-SW-RQST.RQST-SSN
        //*  MOVE EDITED IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID (EM=9999999)
        //*       TO #I-PIN-A7
        //*  CALLNAT 'MDMN100A' #MDMA100                    /* 082017
        //*  082017
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*   WRITE ' AFTER MDMN100 ' #I-SSN
        //*         ' PIN '  IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID
        //*         ' RC '  #O-RETURN-CODE
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #O-RETURN-CODE NE '0000'
        {
            pnd_Hold_Prtcpnt_Nme.setValue("PIN NOT FOUND IN MDMN101A");                                                                                                   //Natural: ASSIGN #HOLD-PRTCPNT-NME := 'PIN NOT FOUND IN MDMN101A'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Fnp.reset();                                                                                                                                                  //Natural: RESET #FNP #MNP
        pnd_Mnp.reset();
        pnd_Fn.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name().getSubstring(1,1));                                                                                  //Natural: ASSIGN #FN := SUBSTRING ( #O-FIRST-NAME,1,1 )
        pnd_Fnp.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Fn, "."));                                                                                   //Natural: COMPRESS #FN '.' INTO #FNP LEAVING NO
        pnd_Mn.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name().getSubstring(1,1));                                                                                 //Natural: ASSIGN #MN := SUBSTRING ( #O-MIDDLE-NAME,1,1 )
        if (condition(pnd_Mn.notEquals(" ")))                                                                                                                             //Natural: IF #MN NE ' '
        {
            pnd_Mnp.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Mn, "."));                                                                               //Natural: COMPRESS #MN '.' INTO #MNP LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Prtcpnt_Nme.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), " ", pnd_Fnp, " ", pnd_Mnp));                                         //Natural: COMPRESS #O-LAST-NAME ' ' #FNP ' ' #MNP INTO #HOLD-PRTCPNT-NME
        //*  READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE
        //*     STARTING FROM #COR-SUPER-PIN-RCDTYPE
        //*   IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE
        //*       IAA-TRNSFR-SW-RQST.IA-UNIQUE-ID  AND
        //*       COR-XREF-PH.PH-RCD-TYPE-CDE NE 01
        //*     #HOLD-PRTCPNT-NME := 'NAME NOT ON COR FILE'
        //*   ELSE
        //*     RESET #FNP #MNP
        //*     #FN := SUBSTRING (COR-XREF-PH.PH-FIRST-NME,1,1)
        //*     COMPRESS  #FN '.' INTO #FNP LEAVING NO
        //*     #MN := SUBSTRING (COR-XREF-PH.PH-MDDLE-NME,1,1)
        //*     IF #MN NE ' '
        //*       COMPRESS #MN '.'  INTO #MNP LEAVING NO
        //*     END-IF
        //*     COMPRESS
        //*       COR-XREF-PH.PH-LAST-NME ' ' #FNP ' ' #MNP INTO #HOLD-PRTCPNT-NME
        //*   END-IF
        //*  END-READ
        //*  GET-NAME
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_M361h_Page.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361H-PAGE
                    getReports().write(0, ReportOption.NOTITLE, writeMapToStringOutput(Iatm361h.class));                                                                  //Natural: WRITE NOTITLE USING FORM 'IATM361H'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
}
