/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:25 PM
**        * FROM NATURAL PROGRAM : Iaap608b
************************************************************
**        * FILE NAME            : Iaap608b.java
**        * CLASS NAME           : Iaap608b
**        * INSTANCE NAME        : Iaap608b
************************************************************
***********************************************************************
*                                                                     *
*  PROGRAM      -           : REPLACES COBOL PROGRAM PIA6080          *
*
*  HISTORY:
*
*  5/2015       - COR/NAAD DECOMMISSION.
*
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap608b extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Out_Low_Seq_Rec;

    private DbsGroup pnd_Out_Low_Seq_Rec__R_Field_1;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls;

    private DbsGroup pnd_Out_Low_Seq_Rec__R_Field_2;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls;

    private DbsGroup pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls;
    private DbsField pnd_Low_Seq_No_Saved;
    private DbsField pnd_Total_Seq;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Out_Low_Seq_Rec = localVariables.newFieldInRecord("pnd_Out_Low_Seq_Rec", "#OUT-LOW-SEQ-REC", FieldType.STRING, 334);

        pnd_Out_Low_Seq_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Out_Low_Seq_Rec__R_Field_1", "REDEFINE", pnd_Out_Low_Seq_Rec);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls", "#OUT-LOW-SEQ-NO-LS", 
            FieldType.STRING, 8);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls", "#OUT-PAYEE-1-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls", "#OUT-CONTRACT-NO-LS", 
            FieldType.STRING, 8);

        pnd_Out_Low_Seq_Rec__R_Field_2 = pnd_Out_Low_Seq_Rec__R_Field_1.newGroupInGroup("pnd_Out_Low_Seq_Rec__R_Field_2", "REDEFINE", pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls = pnd_Out_Low_Seq_Rec__R_Field_2.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls", "#OUT-CON-PRE-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls = pnd_Out_Low_Seq_Rec__R_Field_2.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls", "#OUT-CON-NUM-LS", 
            FieldType.STRING, 6);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls", "#OUT-PAYEE-2-LS", 
            FieldType.STRING, 2);

        pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newGroupInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls", "#OUT-NAME-ADDR-LS");
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1", "#OUT-ADDR-LINE1", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2", "#OUT-ADDR-LINE2", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3", "#OUT-ADDR-LINE3", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4", "#OUT-ADDR-LINE4", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5", "#OUT-ADDR-LINE5", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6", "#OUT-ADDR-LINE6", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7", "#OUT-ADDR-LINE7", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8", "#OUT-ADDR-LINE8", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls", "#OUT-CURRENCY-LS", 
            FieldType.STRING, 1);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls", "#OUT-SEX-LS", FieldType.STRING, 
            1);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls", "#OUT-DOB-LS", FieldType.NUMERIC, 
            8);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls", "#OUT-MODE-LS", FieldType.STRING, 
            3);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls", "#OUT-SOC-SEC-NO-LS", 
            FieldType.STRING, 9);
        pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls", "#OUT-X-REF-LS", 
            FieldType.STRING, 9);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls", "#OUT-OPTION-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls = pnd_Out_Low_Seq_Rec__R_Field_1.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls", "#OUT-J-C-CODE-LS", 
            FieldType.STRING, 1);
        pnd_Low_Seq_No_Saved = localVariables.newFieldInRecord("pnd_Low_Seq_No_Saved", "#LOW-SEQ-NO-SAVED", FieldType.STRING, 8);
        pnd_Total_Seq = localVariables.newFieldInRecord("pnd_Total_Seq", "#TOTAL-SEQ", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap608b() throws Exception
    {
        super("Iaap608b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().write(0, "INSIDE IAAP608B");                                                                                                                         //Natural: WRITE 'INSIDE IAAP608B'
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #OUT-LOW-SEQ-REC
        while (condition(getWorkFiles().read(1, pnd_Out_Low_Seq_Rec)))
        {
            if (condition(pnd_Low_Seq_No_Saved.notEquals(pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls)))                                                                     //Natural: IF #LOW-SEQ-NO-SAVED NE #OUT-LOW-SEQ-NO-LS
            {
                pnd_Low_Seq_No_Saved.setValue(pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls);                                                                                 //Natural: MOVE #OUT-LOW-SEQ-NO-LS TO #LOW-SEQ-NO-SAVED
                pnd_Total_Seq.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-SEQ
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "END OF IAAP608B");                                                                                                                         //Natural: WRITE 'END OF IAAP608B'
        if (Global.isEscape()) return;
        getReports().display(1, "OUTPUT LOWSEQ COUNT",                                                                                                                    //Natural: DISPLAY ( 1 ) 'OUTPUT LOWSEQ COUNT' #TOTAL-SEQ
        		pnd_Total_Seq);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(1, "OUTPUT LOWSEQ COUNT",
        		pnd_Total_Seq);
    }
}
