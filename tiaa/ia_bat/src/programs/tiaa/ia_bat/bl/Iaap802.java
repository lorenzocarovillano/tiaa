/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:46 PM
**        * FROM NATURAL PROGRAM : Iaap802
************************************************************
**        * FILE NAME            : Iaap802.java
**        * CLASS NAME           : Iaap802
**        * INSTANCE NAME        : Iaap802
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP802  :  READS NAME CHECK FLAT FILE            *
*      DATE     -  10/95       READS NAME CORRESPONDENCE FLAT FILE   *
*                              CREATES NAME CHECK/CORRESPONDENCE FLAT*
*                              FILE                                  *
*  05/2017 OS PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap802 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Name_Check;
    private DbsField pnd_Name_Check_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Check_Cntrct_Payee;

    private DbsGroup pnd_Name_Check__R_Field_1;
    private DbsField pnd_Name_Check_Cntrct_Nmbr;
    private DbsField pnd_Name_Check_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Check_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Check__R_Field_2;
    private DbsField pnd_Name_Check_Cntrct_Name_Free;
    private DbsField pnd_Name_Check_Addrss_Lne_1;
    private DbsField pnd_Name_Check_Addrss_Lne_2;
    private DbsField pnd_Name_Check_Addrss_Lne_3;
    private DbsField pnd_Name_Check_Addrss_Lne_4;
    private DbsField pnd_Name_Check_Addrss_Lne_5;
    private DbsField pnd_Name_Check_Addrss_Lne_6;
    private DbsField pnd_Name_Check_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Check__R_Field_3;
    private DbsField pnd_Name_Check_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Check_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Check_Addrss_Walk_Rte;
    private DbsField pnd_Name_Check_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Check_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Check__R_Field_4;
    private DbsField pnd_Name_Check_Addrss_Type_Cde;
    private DbsField pnd_Name_Check_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Check_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Check_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Check_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Check_Eft_Status_Ind;
    private DbsField pnd_Name_Check_Checking_Saving_Cde;
    private DbsField pnd_Name_Check_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Check_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Check_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Check_Mailing_Addrss_Ind;
    private DbsField pnd_Name_Check_Addrss_Geographic_Cde;
    private DbsField pnd_Name_Check_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Check_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Check_Intl_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup pnd_Name_Corr;
    private DbsField pnd_Name_Corr_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Corr_Cntrct_Payee;

    private DbsGroup pnd_Name_Corr__R_Field_5;
    private DbsField pnd_Name_Corr_Cntrct_Nmbr;
    private DbsField pnd_Name_Corr_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Corr_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Corr__R_Field_6;
    private DbsField pnd_Name_Corr_Cntrct_Name_Free;
    private DbsField pnd_Name_Corr_Addrss_Lne_1;
    private DbsField pnd_Name_Corr_Addrss_Lne_2;
    private DbsField pnd_Name_Corr_Addrss_Lne_3;
    private DbsField pnd_Name_Corr_Addrss_Lne_4;
    private DbsField pnd_Name_Corr_Addrss_Lne_5;
    private DbsField pnd_Name_Corr_Addrss_Lne_6;
    private DbsField pnd_Name_Corr_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Corr__R_Field_7;
    private DbsField pnd_Name_Corr_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Corr_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Corr_Addrss_Walk_Rte;
    private DbsField pnd_Name_Corr_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Corr_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Corr__R_Field_8;
    private DbsField pnd_Name_Corr_Addrss_Type_Cde;
    private DbsField pnd_Name_Corr_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Corr_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Corr_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Corr_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Eft_Status_Ind;
    private DbsField pnd_Name_Corr_Checking_Saving_Cde;
    private DbsField pnd_Name_Corr_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Corr_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Correspondence_Addrss_Ind;
    private DbsField pnd_Name_Corr_Addrss_Geographic_Cde;

    private DbsGroup pnd_Name_Check_Out;
    private DbsField pnd_Name_Check_Out_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Check_Out_Cntrct_Payee;

    private DbsGroup pnd_Name_Check_Out__R_Field_9;
    private DbsField pnd_Name_Check_Out_Cntrct_Nmbr;
    private DbsField pnd_Name_Check_Out_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Check_Out_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Check_Out__R_Field_10;
    private DbsField pnd_Name_Check_Out_Cntrct_Name_Free;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_1;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_2;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_3;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_4;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_5;
    private DbsField pnd_Name_Check_Out_Addrss_Lne_6;
    private DbsField pnd_Name_Check_Out_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Check_Out__R_Field_11;
    private DbsField pnd_Name_Check_Out_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Check_Out_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Check_Out_Addrss_Walk_Rte;
    private DbsField pnd_Name_Check_Out_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Check_Out_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Check_Out__R_Field_12;
    private DbsField pnd_Name_Check_Out_Addrss_Type_Cde;
    private DbsField pnd_Name_Check_Out_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Check_Out_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Check_Out_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Check_Out_Eft_Status_Ind;
    private DbsField pnd_Name_Check_Out_Checking_Saving_Cde;
    private DbsField pnd_Name_Check_Out_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Check_Out_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Check_Out_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Check_Mailing_Addrss_Ind;
    private DbsField pnd_Name_Check_Out_Addrss_Geographic_Cde;
    private DbsField pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup pnd_Name_Corr_Out;
    private DbsField pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Corr_Out__R_Field_13;
    private DbsField pnd_Name_Corr_Out_Cntrct_Name_Free;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_1;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_2;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_3;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_4;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_5;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_6;
    private DbsField pnd_Name_Corr_Out_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Corr_Out__R_Field_14;
    private DbsField pnd_Name_Corr_Out_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Corr_Out_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Corr_Out_Addrss_Walk_Rte;
    private DbsField pnd_Name_Corr_Out_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Corr_Out_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Corr_Out__R_Field_15;
    private DbsField pnd_Name_Corr_Out_Addrss_Type_Cde;
    private DbsField pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Corr_Out_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Out_Eft_Status_Ind;
    private DbsField pnd_Name_Corr_Out_Checking_Saving_Cde;
    private DbsField pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Correspondence_Addrss_Ind;
    private DbsField pnd_Name_Corr_Out_Addrss_Geographic_Cde;

    private DbsGroup miscellaneous_Variables;
    private DbsField miscellaneous_Variables_Pnd_Blank_Addrss;
    private DbsField miscellaneous_Variables_Pnd_Blank_Postal;
    private DbsField miscellaneous_Variables_Pnd_Blank_Rest;
    private DbsField miscellaneous_Variables_Pnd_Name_Corr_Eof;
    private DbsField miscellaneous_Variables_Pnd_Name_Check_Eof;
    private DbsField miscellaneous_Variables_Pnd_Check_Ctr;
    private DbsField miscellaneous_Variables_Pnd_Check_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_Corr_Ctr;
    private DbsField miscellaneous_Variables_Pnd_Corr_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_Both_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Name_Check = localVariables.newGroupInRecord("pnd_Name_Check", "#NAME-CHECK");
        pnd_Name_Check_Ph_Unque_Id_Nmbr = pnd_Name_Check.newFieldInGroup("pnd_Name_Check_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Check_Cntrct_Payee = pnd_Name_Check.newFieldInGroup("pnd_Name_Check_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name_Check__R_Field_1 = pnd_Name_Check.newGroupInGroup("pnd_Name_Check__R_Field_1", "REDEFINE", pnd_Name_Check_Cntrct_Payee);
        pnd_Name_Check_Cntrct_Nmbr = pnd_Name_Check__R_Field_1.newFieldInGroup("pnd_Name_Check_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Check_Cntrct_Payee_Cde = pnd_Name_Check__R_Field_1.newFieldInGroup("pnd_Name_Check_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Check_Pnd_Cntrct_Name_Add = pnd_Name_Check.newFieldInGroup("pnd_Name_Check_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);

        pnd_Name_Check__R_Field_2 = pnd_Name_Check.newGroupInGroup("pnd_Name_Check__R_Field_2", "REDEFINE", pnd_Name_Check_Pnd_Cntrct_Name_Add);
        pnd_Name_Check_Cntrct_Name_Free = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Name_Check_Addrss_Lne_1 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Lne_2 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Lne_3 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Lne_4 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Lne_5 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Lne_6 = pnd_Name_Check__R_Field_2.newFieldInGroup("pnd_Name_Check_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Name_Check_Addrss_Postal_Data = pnd_Name_Check.newFieldInGroup("pnd_Name_Check_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Name_Check__R_Field_3 = pnd_Name_Check.newGroupInGroup("pnd_Name_Check__R_Field_3", "REDEFINE", pnd_Name_Check_Addrss_Postal_Data);
        pnd_Name_Check_Addrss_Zip_Plus_4 = pnd_Name_Check__R_Field_3.newFieldInGroup("pnd_Name_Check_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", FieldType.STRING, 
            9);
        pnd_Name_Check_Addrss_Carrier_Rte = pnd_Name_Check__R_Field_3.newFieldInGroup("pnd_Name_Check_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", FieldType.STRING, 
            4);
        pnd_Name_Check_Addrss_Walk_Rte = pnd_Name_Check__R_Field_3.newFieldInGroup("pnd_Name_Check_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 
            4);
        pnd_Name_Check_Addrss_Usps_Future_Use = pnd_Name_Check__R_Field_3.newFieldInGroup("pnd_Name_Check_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Name_Check_Pnd_Rest_Of_Record = pnd_Name_Check.newFieldInGroup("pnd_Name_Check_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 148);

        pnd_Name_Check__R_Field_4 = pnd_Name_Check.newGroupInGroup("pnd_Name_Check__R_Field_4", "REDEFINE", pnd_Name_Check_Pnd_Rest_Of_Record);
        pnd_Name_Check_Addrss_Type_Cde = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Name_Check_Addrss_Last_Chnge_Dte = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Addrss_Last_Chnge_Time = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Name_Check_Permanent_Addrss_Ind = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Bank_Aba_Acct_Nmbr = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", FieldType.STRING, 
            9);
        pnd_Name_Check_Eft_Status_Ind = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Name_Check_Checking_Saving_Cde = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Checking_Saving_Cde", "CHECKING-SAVING-CDE", FieldType.STRING, 
            1);
        pnd_Name_Check_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Name_Check_Pending_Addrss_Chnge_Dte = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Pending_Addrss_Restore_Dte = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Pending_Addrss_Restore_Dte", "PENDING-ADDRSS-RESTORE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Pending_Perm_Addrss_Chnge_Dte", "PENDING-PERM-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Check_Mailing_Addrss_Ind = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Check_Mailing_Addrss_Ind", "CHECK-MAILING-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Addrss_Geographic_Cde = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Intl_Eft_Pay_Type_Cde = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Check_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check__R_Field_4.newFieldInGroup("pnd_Name_Check_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);

        pnd_Name_Corr = localVariables.newGroupInRecord("pnd_Name_Corr", "#NAME-CORR");
        pnd_Name_Corr_Ph_Unque_Id_Nmbr = pnd_Name_Corr.newFieldInGroup("pnd_Name_Corr_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Corr_Cntrct_Payee = pnd_Name_Corr.newFieldInGroup("pnd_Name_Corr_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name_Corr__R_Field_5 = pnd_Name_Corr.newGroupInGroup("pnd_Name_Corr__R_Field_5", "REDEFINE", pnd_Name_Corr_Cntrct_Payee);
        pnd_Name_Corr_Cntrct_Nmbr = pnd_Name_Corr__R_Field_5.newFieldInGroup("pnd_Name_Corr_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Corr_Cntrct_Payee_Cde = pnd_Name_Corr__R_Field_5.newFieldInGroup("pnd_Name_Corr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Corr_Pnd_Cntrct_Name_Add = pnd_Name_Corr.newFieldInGroup("pnd_Name_Corr_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 245);

        pnd_Name_Corr__R_Field_6 = pnd_Name_Corr.newGroupInGroup("pnd_Name_Corr__R_Field_6", "REDEFINE", pnd_Name_Corr_Pnd_Cntrct_Name_Add);
        pnd_Name_Corr_Cntrct_Name_Free = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Name_Corr_Addrss_Lne_1 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Lne_2 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Lne_3 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Lne_4 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Lne_5 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Lne_6 = pnd_Name_Corr__R_Field_6.newFieldInGroup("pnd_Name_Corr_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 35);
        pnd_Name_Corr_Addrss_Postal_Data = pnd_Name_Corr.newFieldInGroup("pnd_Name_Corr_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 32);

        pnd_Name_Corr__R_Field_7 = pnd_Name_Corr.newGroupInGroup("pnd_Name_Corr__R_Field_7", "REDEFINE", pnd_Name_Corr_Addrss_Postal_Data);
        pnd_Name_Corr_Addrss_Zip_Plus_4 = pnd_Name_Corr__R_Field_7.newFieldInGroup("pnd_Name_Corr_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", FieldType.STRING, 
            9);
        pnd_Name_Corr_Addrss_Carrier_Rte = pnd_Name_Corr__R_Field_7.newFieldInGroup("pnd_Name_Corr_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", FieldType.STRING, 
            4);
        pnd_Name_Corr_Addrss_Walk_Rte = pnd_Name_Corr__R_Field_7.newFieldInGroup("pnd_Name_Corr_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 
            4);
        pnd_Name_Corr_Addrss_Usps_Future_Use = pnd_Name_Corr__R_Field_7.newFieldInGroup("pnd_Name_Corr_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Name_Corr_Pnd_Rest_Of_Record = pnd_Name_Corr.newFieldInGroup("pnd_Name_Corr_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 76);

        pnd_Name_Corr__R_Field_8 = pnd_Name_Corr.newGroupInGroup("pnd_Name_Corr__R_Field_8", "REDEFINE", pnd_Name_Corr_Pnd_Rest_Of_Record);
        pnd_Name_Corr_Addrss_Type_Cde = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Name_Corr_Addrss_Last_Chnge_Dte = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Addrss_Last_Chnge_Time = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Name_Corr_Permanent_Addrss_Ind = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", FieldType.STRING, 
            1);
        pnd_Name_Corr_Bank_Aba_Acct_Nmbr = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", FieldType.STRING, 
            9);
        pnd_Name_Corr_Eft_Status_Ind = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 1);
        pnd_Name_Corr_Checking_Saving_Cde = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Checking_Saving_Cde", "CHECKING-SAVING-CDE", FieldType.STRING, 
            1);
        pnd_Name_Corr_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Name_Corr_Pending_Addrss_Chnge_Dte = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Pending_Addrss_Restore_Dte = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Pending_Addrss_Restore_Dte", "PENDING-ADDRSS-RESTORE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Pending_Perm_Addrss_Chnge_Dte", "PENDING-PERM-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Correspondence_Addrss_Ind = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Correspondence_Addrss_Ind", "CORRESPONDENCE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Addrss_Geographic_Cde = pnd_Name_Corr__R_Field_8.newFieldInGroup("pnd_Name_Corr_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);

        pnd_Name_Check_Out = localVariables.newGroupInRecord("pnd_Name_Check_Out", "#NAME-CHECK-OUT");
        pnd_Name_Check_Out_Ph_Unque_Id_Nmbr = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 
            12);
        pnd_Name_Check_Out_Cntrct_Payee = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name_Check_Out__R_Field_9 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_9", "REDEFINE", pnd_Name_Check_Out_Cntrct_Payee);
        pnd_Name_Check_Out_Cntrct_Nmbr = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 
            10);
        pnd_Name_Check_Out_Cntrct_Payee_Cde = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Name_Check_Out_Pnd_Cntrct_Name_Add = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);

        pnd_Name_Check_Out__R_Field_10 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_10", "REDEFINE", pnd_Name_Check_Out_Pnd_Cntrct_Name_Add);
        pnd_Name_Check_Out_Cntrct_Name_Free = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Cntrct_Name_Free", "CNTRCT-NAME-FREE", 
            FieldType.STRING, 35);
        pnd_Name_Check_Out_Addrss_Lne_1 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Lne_2 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Lne_3 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Lne_4 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Lne_5 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Lne_6 = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Addrss_Postal_Data = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Name_Check_Out__R_Field_11 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_11", "REDEFINE", pnd_Name_Check_Out_Addrss_Postal_Data);
        pnd_Name_Check_Out_Addrss_Zip_Plus_4 = pnd_Name_Check_Out__R_Field_11.newFieldInGroup("pnd_Name_Check_Out_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pnd_Name_Check_Out_Addrss_Carrier_Rte = pnd_Name_Check_Out__R_Field_11.newFieldInGroup("pnd_Name_Check_Out_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", 
            FieldType.STRING, 4);
        pnd_Name_Check_Out_Addrss_Walk_Rte = pnd_Name_Check_Out__R_Field_11.newFieldInGroup("pnd_Name_Check_Out_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 
            4);
        pnd_Name_Check_Out_Addrss_Usps_Future_Use = pnd_Name_Check_Out__R_Field_11.newFieldInGroup("pnd_Name_Check_Out_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Name_Check_Out_Pnd_Rest_Of_Record = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            148);

        pnd_Name_Check_Out__R_Field_12 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_12", "REDEFINE", pnd_Name_Check_Out_Pnd_Rest_Of_Record);
        pnd_Name_Check_Out_Addrss_Type_Cde = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Name_Check_Out_Addrss_Last_Chnge_Dte = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Addrss_Last_Chnge_Time = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Name_Check_Out_Permanent_Addrss_Ind = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Bank_Aba_Acct_Nmbr = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Name_Check_Out_Eft_Status_Ind = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Name_Check_Out_Checking_Saving_Cde = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Checking_Saving_Cde", "CHECKING-SAVING-CDE", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Name_Check_Out_Pending_Addrss_Chnge_Dte = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pending_Addrss_Restore_Dte = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Pending_Addrss_Restore_Dte", 
            "PENDING-ADDRSS-RESTORE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Pending_Perm_Addrss_Chnge_Dte", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Check_Mailing_Addrss_Ind = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Check_Mailing_Addrss_Ind", "CHECK-MAILING-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Addrss_Geographic_Cde = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check_Out__R_Field_12.newFieldInGroup("pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr", 
            "INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 35);

        pnd_Name_Corr_Out = localVariables.newGroupInRecord("pnd_Name_Corr_Out", "#NAME-CORR-OUT");
        pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);

        pnd_Name_Corr_Out__R_Field_13 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_13", "REDEFINE", pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add);
        pnd_Name_Corr_Out_Cntrct_Name_Free = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_1 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_2 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_3 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_4 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_5 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_6 = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Postal_Data = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Name_Corr_Out__R_Field_14 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_14", "REDEFINE", pnd_Name_Corr_Out_Addrss_Postal_Data);
        pnd_Name_Corr_Out_Addrss_Zip_Plus_4 = pnd_Name_Corr_Out__R_Field_14.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pnd_Name_Corr_Out_Addrss_Carrier_Rte = pnd_Name_Corr_Out__R_Field_14.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", 
            FieldType.STRING, 4);
        pnd_Name_Corr_Out_Addrss_Walk_Rte = pnd_Name_Corr_Out__R_Field_14.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 
            4);
        pnd_Name_Corr_Out_Addrss_Usps_Future_Use = pnd_Name_Corr_Out__R_Field_14.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Name_Corr_Out_Pnd_Rest_Of_Record = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            76);

        pnd_Name_Corr_Out__R_Field_15 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_15", "REDEFINE", pnd_Name_Corr_Out_Pnd_Rest_Of_Record);
        pnd_Name_Corr_Out_Addrss_Type_Cde = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Addrss_Last_Chnge_Time = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Name_Corr_Out_Permanent_Addrss_Ind = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Name_Corr_Out_Eft_Status_Ind = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Name_Corr_Out_Checking_Saving_Cde = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Checking_Saving_Cde", "CHECKING-SAVING-CDE", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte", "PENDING-ADDRSS-RESTORE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Correspondence_Addrss_Ind = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Correspondence_Addrss_Ind", "CORRESPONDENCE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Addrss_Geographic_Cde = pnd_Name_Corr_Out__R_Field_15.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);

        miscellaneous_Variables = localVariables.newGroupInRecord("miscellaneous_Variables", "MISCELLANEOUS-VARIABLES");
        miscellaneous_Variables_Pnd_Blank_Addrss = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Blank_Addrss", "#BLANK-ADDRSS", 
            FieldType.STRING, 245);
        miscellaneous_Variables_Pnd_Blank_Postal = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Blank_Postal", "#BLANK-POSTAL", 
            FieldType.STRING, 32);
        miscellaneous_Variables_Pnd_Blank_Rest = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Blank_Rest", "#BLANK-REST", FieldType.STRING, 
            76);
        miscellaneous_Variables_Pnd_Name_Corr_Eof = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Name_Corr_Eof", "#NAME-CORR-EOF", 
            FieldType.BOOLEAN, 1);
        miscellaneous_Variables_Pnd_Name_Check_Eof = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Name_Check_Eof", "#NAME-CHECK-EOF", 
            FieldType.BOOLEAN, 1);
        miscellaneous_Variables_Pnd_Check_Ctr = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Check_Ctr", "#CHECK-CTR", FieldType.PACKED_DECIMAL, 
            9);
        miscellaneous_Variables_Pnd_Check_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Check_Ctr_Out", "#CHECK-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Corr_Ctr = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Corr_Ctr", "#CORR-CTR", FieldType.PACKED_DECIMAL, 
            9);
        miscellaneous_Variables_Pnd_Corr_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Corr_Ctr_Out", "#CORR-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Both_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Both_Ctr_Out", "#BOTH-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_X = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap802() throws Exception
    {
        super("Iaap802");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP802", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CORR
        sub_Read_Name_Corr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CHECK
        sub_Read_Name_Check();
        if (condition(Global.isEscape())) {return;}
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(miscellaneous_Variables_Pnd_Name_Corr_Eof.getBoolean() && miscellaneous_Variables_Pnd_Name_Check_Eof.getBoolean()))                             //Natural: IF #NAME-CORR-EOF AND #NAME-CHECK-EOF
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Name_Corr_Eof.getBoolean()))                                                                                        //Natural: IF #NAME-CORR-EOF
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CHECK
                sub_Write_Name_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Name_Check_Eof.getBoolean()))                                                                                       //Natural: IF #NAME-CHECK-EOF
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CORR
                sub_Write_Name_Corr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Corr_Cntrct_Payee.greater(pnd_Name_Check_Cntrct_Payee)))                                                                               //Natural: IF #NAME-CORR.CNTRCT-PAYEE GT #NAME-CHECK.CNTRCT-PAYEE
            {
                                                                                                                                                                          //Natural: PERFORM NAME-CORR-GT-NAME-CHECK
                sub_Name_Corr_Gt_Name_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Name_Corr_Cntrct_Payee.less(pnd_Name_Check_Cntrct_Payee)))                                                                              //Natural: IF #NAME-CORR.CNTRCT-PAYEE LT #NAME-CHECK.CNTRCT-PAYEE
                {
                                                                                                                                                                          //Natural: PERFORM NAME-CORR-LT-NAME-CHECK
                    sub_Name_Corr_Lt_Name_Check();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH-NAME
                    sub_Write_Both_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(0, "=",miscellaneous_Variables_Pnd_Check_Ctr,"=",miscellaneous_Variables_Pnd_Check_Ctr_Out,"=",miscellaneous_Variables_Pnd_Corr_Ctr,           //Natural: WRITE '=' #CHECK-CTR '=' #CHECK-CTR-OUT '=' #CORR-CTR '=' #CORR-CTR-OUT '=' #BOTH-CTR-OUT
            "=",miscellaneous_Variables_Pnd_Corr_Ctr_Out,"=",miscellaneous_Variables_Pnd_Both_Ctr_Out);
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-CORR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-CHECK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-CORR-GT-NAME-CHECK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-CORR-LT-NAME-CHECK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NAME-CORR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NAME-CHECK
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BOTH-NAME
        //*  WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        //*  WRITE 'TOTAL NAME ADDRESS READ           : ' #TOT-NAME-CTR
        //*  WRITE 'TOTAL IA NAME ADDRESS READ        : ' #TOT-NAME-CTR-I
        //*  WRITE 'TOTAL NAME CHECK MAILING READ     : ' #CHECK-CTR-NAME-NAME
        //*  WRITE 'TOTAL VAC CHECK MAILING READ      : ' #CHECK-CTR-NAME-VAC
        //*  WRITE 'TOTAL NAME CORR READ              : ' #CORR-CTR-NAME-NAME
        //*  WRITE 'TOTAL VAC CORR READ               : ' #CORR-CTR-NAME-VAC
        //*  WRITE 'TOTAL NAME CHECK MAILING OUT      : ' #CHECK-CTR-NAME-NAME-OUT
        //*  WRITE 'TOTAL VAC CHECK MAILING OUT       : ' #CHECK-CTR-NAME-VAC-OUT
        //*  WRITE 'TOTAL NAME CORR MAILING OUT       : ' #CORR-CTR-NAME-NAME-OUT
        //*  WRITE 'TOTAL VAC CORR MAILING OUT        : ' #CORR-CTR-NAME-VAC-OUT
        //*  WRITE 'TOTAL RECORDS WRITTEN             : ' #TOT-CTR-OUT
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Read_Name_Corr() throws Exception                                                                                                                    //Natural: READ-NAME-CORR
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(miscellaneous_Variables_Pnd_Name_Corr_Eof.getBoolean()))                                                                                            //Natural: IF #NAME-CORR-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(1, pnd_Name_Corr);                                                                                                                            //Natural: READ WORK 1 ONCE #NAME-CORR
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            miscellaneous_Variables_Pnd_Name_Corr_Eof.setValue(true);                                                                                                     //Natural: MOVE TRUE TO #NAME-CORR-EOF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        miscellaneous_Variables_Pnd_Corr_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CORR-CTR
    }
    private void sub_Read_Name_Check() throws Exception                                                                                                                   //Natural: READ-NAME-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(miscellaneous_Variables_Pnd_Name_Check_Eof.getBoolean()))                                                                                           //Natural: IF #NAME-CHECK-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(2, pnd_Name_Check);                                                                                                                           //Natural: READ WORK 2 ONCE #NAME-CHECK
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            miscellaneous_Variables_Pnd_Name_Check_Eof.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #NAME-CHECK-EOF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        miscellaneous_Variables_Pnd_Check_Ctr.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CHECK-CTR
    }
    private void sub_Name_Corr_Gt_Name_Check() throws Exception                                                                                                           //Natural: NAME-CORR-GT-NAME-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Name_Check_Cntrct_Payee.greaterOrEqual(pnd_Name_Corr_Cntrct_Payee)))                                                                        //Natural: IF #NAME-CHECK.CNTRCT-PAYEE GE #NAME-CORR.CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CHECK
                sub_Write_Name_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Name_Check_Cntrct_Payee.equals(pnd_Name_Corr_Cntrct_Payee)))                                                                                    //Natural: IF #NAME-CHECK.CNTRCT-PAYEE = #NAME-CORR.CNTRCT-PAYEE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH-NAME
            sub_Write_Both_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CORR
            sub_Write_Name_Corr();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Name_Corr_Lt_Name_Check() throws Exception                                                                                                           //Natural: NAME-CORR-LT-NAME-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Name_Corr_Cntrct_Payee.greaterOrEqual(pnd_Name_Check_Cntrct_Payee)))                                                                        //Natural: IF #NAME-CORR.CNTRCT-PAYEE GE #NAME-CHECK.CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CORR
                sub_Write_Name_Corr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Name_Check_Cntrct_Payee.equals(pnd_Name_Corr_Cntrct_Payee)))                                                                                    //Natural: IF #NAME-CHECK.CNTRCT-PAYEE = #NAME-CORR.CNTRCT-PAYEE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH-NAME
            sub_Write_Both_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-CHECK
            sub_Write_Name_Check();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Name_Corr() throws Exception                                                                                                                   //Natural: WRITE-NAME-CORR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name_Check_Out_Ph_Unque_Id_Nmbr.setValue(pnd_Name_Corr_Ph_Unque_Id_Nmbr);                                                                                     //Natural: ASSIGN #NAME-CHECK-OUT.PH-UNQUE-ID-NMBR := #NAME-CORR.PH-UNQUE-ID-NMBR
        pnd_Name_Check_Out_Cntrct_Payee.setValue(pnd_Name_Corr_Cntrct_Payee);                                                                                             //Natural: ASSIGN #NAME-CHECK-OUT.CNTRCT-PAYEE := #NAME-CORR.CNTRCT-PAYEE
        pnd_Name_Check_Out_Pnd_Cntrct_Name_Add.setValue(miscellaneous_Variables_Pnd_Blank_Addrss);                                                                        //Natural: ASSIGN #NAME-CHECK-OUT.#CNTRCT-NAME-ADD := #BLANK-ADDRSS
        pnd_Name_Check_Out_Addrss_Postal_Data.setValue(miscellaneous_Variables_Pnd_Blank_Postal);                                                                         //Natural: ASSIGN #NAME-CHECK-OUT.ADDRSS-POSTAL-DATA := #BLANK-POSTAL
        pnd_Name_Check_Out_Pnd_Rest_Of_Record.setValue(miscellaneous_Variables_Pnd_Blank_Rest);                                                                           //Natural: ASSIGN #NAME-CHECK-OUT.#REST-OF-RECORD := #BLANK-REST
        pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add.setValue(pnd_Name_Corr_Pnd_Cntrct_Name_Add);                                                                                //Natural: ASSIGN #NAME-CORR-OUT.#CNTRCT-NAME-ADD := #NAME-CORR.#CNTRCT-NAME-ADD
        pnd_Name_Corr_Out_Pnd_Rest_Of_Record.setValue(pnd_Name_Corr_Pnd_Rest_Of_Record);                                                                                  //Natural: ASSIGN #NAME-CORR-OUT.#REST-OF-RECORD := #NAME-CORR.#REST-OF-RECORD
        getWorkFiles().write(5, false, pnd_Name_Check_Out, pnd_Name_Corr_Out);                                                                                            //Natural: WRITE WORK FILE 5 #NAME-CHECK-OUT #NAME-CORR-OUT
        miscellaneous_Variables_Pnd_Corr_Ctr_Out.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CORR-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CORR
        sub_Read_Name_Corr();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Name_Check() throws Exception                                                                                                                  //Natural: WRITE-NAME-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name_Check_Out_Ph_Unque_Id_Nmbr.setValue(pnd_Name_Check_Ph_Unque_Id_Nmbr);                                                                                    //Natural: ASSIGN #NAME-CHECK-OUT.PH-UNQUE-ID-NMBR := #NAME-CHECK.PH-UNQUE-ID-NMBR
        pnd_Name_Check_Out_Cntrct_Payee.setValue(pnd_Name_Check_Cntrct_Payee);                                                                                            //Natural: ASSIGN #NAME-CHECK-OUT.CNTRCT-PAYEE := #NAME-CHECK.CNTRCT-PAYEE
        pnd_Name_Check_Out_Pnd_Cntrct_Name_Add.setValue(pnd_Name_Check_Pnd_Cntrct_Name_Add);                                                                              //Natural: ASSIGN #NAME-CHECK-OUT.#CNTRCT-NAME-ADD := #NAME-CHECK.#CNTRCT-NAME-ADD
        pnd_Name_Check_Out_Addrss_Postal_Data.setValue(pnd_Name_Check_Addrss_Postal_Data);                                                                                //Natural: ASSIGN #NAME-CHECK-OUT.ADDRSS-POSTAL-DATA := #NAME-CHECK.ADDRSS-POSTAL-DATA
        pnd_Name_Check_Out_Pnd_Rest_Of_Record.setValue(pnd_Name_Check_Pnd_Rest_Of_Record);                                                                                //Natural: ASSIGN #NAME-CHECK-OUT.#REST-OF-RECORD := #NAME-CHECK.#REST-OF-RECORD
        pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add.setValue(miscellaneous_Variables_Pnd_Blank_Addrss);                                                                         //Natural: ASSIGN #NAME-CORR-OUT.#CNTRCT-NAME-ADD := #BLANK-ADDRSS
        pnd_Name_Corr_Out_Addrss_Postal_Data.setValue(miscellaneous_Variables_Pnd_Blank_Postal);                                                                          //Natural: ASSIGN #NAME-CORR-OUT.ADDRSS-POSTAL-DATA := #BLANK-POSTAL
        pnd_Name_Corr_Out_Pnd_Rest_Of_Record.setValue(miscellaneous_Variables_Pnd_Blank_Rest);                                                                            //Natural: ASSIGN #NAME-CORR-OUT.#REST-OF-RECORD := #BLANK-REST
        getWorkFiles().write(5, false, pnd_Name_Check_Out, pnd_Name_Corr_Out);                                                                                            //Natural: WRITE WORK FILE 5 #NAME-CHECK-OUT #NAME-CORR-OUT
        miscellaneous_Variables_Pnd_Check_Ctr_Out.nadd(1);                                                                                                                //Natural: ADD 1 TO #CHECK-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CHECK
        sub_Read_Name_Check();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Both_Name() throws Exception                                                                                                                   //Natural: WRITE-BOTH-NAME
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(5, false, pnd_Name_Check, pnd_Name_Corr_Pnd_Cntrct_Name_Add, pnd_Name_Corr_Addrss_Postal_Data, pnd_Name_Corr_Pnd_Rest_Of_Record);            //Natural: WRITE WORK FILE 5 #NAME-CHECK #NAME-CORR.#CNTRCT-NAME-ADD #NAME-CORR.ADDRSS-POSTAL-DATA #NAME-CORR.#REST-OF-RECORD
        miscellaneous_Variables_Pnd_Both_Ctr_Out.nadd(1);                                                                                                                 //Natural: ADD 1 TO #BOTH-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CHECK
        sub_Read_Name_Check();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-NAME-CORR
        sub_Read_Name_Corr();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM());                                                                                                   //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM
        getReports().write(0, "LINE NUMBER: ",Global.getERROR_LINE());                                                                                                    //Natural: WRITE 'LINE NUMBER: ' *ERROR-LINE
        getReports().write(0, "ERROR NUMBER: ",Global.getERROR_NR());                                                                                                     //Natural: WRITE 'ERROR NUMBER: ' *ERROR-NR
        getReports().write(0, "=",miscellaneous_Variables_Pnd_Check_Ctr,"=",miscellaneous_Variables_Pnd_Check_Ctr_Out,"=",miscellaneous_Variables_Pnd_Corr_Ctr,           //Natural: WRITE '=' #CHECK-CTR '=' #CHECK-CTR-OUT '=' #CORR-CTR '=' #CORR-CTR-OUT '=' #BOTH-CTR-OUT
            "=",miscellaneous_Variables_Pnd_Corr_Ctr_Out,"=",miscellaneous_Variables_Pnd_Both_Ctr_Out);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
