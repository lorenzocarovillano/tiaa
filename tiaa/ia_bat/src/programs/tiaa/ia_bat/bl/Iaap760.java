/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:50 PM
**        * FROM NATURAL PROGRAM : Iaap760
************************************************************
**        * FILE NAME            : Iaap760.java
**        * CLASS NAME           : Iaap760
**        * INSTANCE NAME        : Iaap760
************************************************************
************************************************************************
* PROGRAM : IAAP760
* DATE    : 2/13/2017
* PURPOSE : LOAD OPT-OUT ACCELERATED ANNUITIZED CONTRACTS IN IA LEGACY
* CMWKF01   INPUT : DSN=PIA.ANN.IA.ITD.INFILE(0) FROM AUTOSYS.
* CMWKF02   OUTPUT: DSN=PIA.ANN.IA.STATUS.UPDT(+1)TO CTH FOR STATUS UPDT
* CMWKF03   OUTPUT: DSN=PIA.ANN.IA.STTLMNT(+1) TO FAH/FSDF FOR TRADE
* CMWKF04   OUTPUT: DSN=PPDD.ANN.IAITD.CTL TO FAH/FSDF FOR TRIGGER
*
*
* HISTORY :
* ORIGINAL CODE : JUN TINIO
* 11/17/17      : ADDED ORIG TIAA/CREF DA NUMBER TO LAYOUT AND POPULATED
*                 ALL CONTRACT NUMBER FIELDS SENT TO ESP. SCAN 112017
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap760 extends BLNatBase
{
    // Data Areas
    private LdaIaal760 ldaIaal760;
    private LdaIaal760b ldaIaal760b;
    private LdaIaal999f ldaIaal999f;
    private PdaNeca4000 pdaNeca4000;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_tiaa_Fund;
    private DbsField tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField tiaa_Fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField tiaa_Fund_Tiaa_Cmpny_Fund_Cde;
    private DbsField pnd_Pmt_Mode;
    private DbsField pnd_No_Guar;
    private DbsField pnd_Mode_Wrk;

    private DbsGroup pnd_Mode_Wrk__R_Field_1;
    private DbsField pnd_Mode_Wrk_Pnd_Mode_Pos1;
    private DbsField pnd_Mode_Wrk_Pnd_Mode_Pos2;
    private DbsField pnd_Pend_Dte;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Check_Dte;

    private DbsGroup pnd_Check_Dte__R_Field_2;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_A;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Date;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_3;
    private DbsField pnd_W_Date_Pnd_W_Yyyy;
    private DbsField pnd_W_Date_Pnd_W_Mm;
    private DbsField pnd_W_Date_Pnd_W_Dd;
    private DbsField pnd_S_Invrse_Dte;
    private DbsField pnd_Cntrct_Cnt;
    private DbsField pnd_Cpr_Cnt;
    private DbsField pnd_Fnd_Cnt;
    private DbsField pnd_Annl_Cnt;
    private DbsField pnd_Mnth_Cnt;
    private DbsField pnd_W_Tot;
    private DbsField pnd_Tot_Per_Amt;
    private DbsField pnd_Tot_Div_Amt;
    private DbsField pnd_Completed;
    private DbsField pnd_Rejected;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Proc_Cnt;
    private DbsField pnd_Rej_Cnt;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_4;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key;

    private DbsGroup pnd_Tiaa_Cntrct_Fund_Key__R_Field_5;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee;
    private DbsField pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund;
    private DbsField pnd_Tot_Annl_Units;
    private DbsField pnd_Tot_Annl_Pmts;
    private DbsField pnd_Tot_Mnth_Units;
    private DbsField pnd_Tot_Mnth_Pmts;
    private DbsField pnd_Price_Div;

    private DbsGroup pnd_Price_Div__R_Field_6;
    private DbsField pnd_Price_Div_Pnd_U_Val;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_F;
    private DbsField pnd_C_Fnd;

    private DbsGroup pnd_C_Fnd__R_Field_7;
    private DbsField pnd_C_Fnd_Pnd_Cmpny;
    private DbsField pnd_C_Fnd_Pnd_Fund;
    private DbsField pnd_Per_Units_Cnt;
    private DbsField pnd_Per_Pmt_Amt;
    private DbsField pnd_Temp_Fnd_Cnt;
    private DbsField pnd_Temp_Annl_Cnt;
    private DbsField pnd_Temp_Mnth_Cnt;
    private DbsField pnd_Temp_Tot_Per_Amt;
    private DbsField pnd_Temp_Tot_Div_Amt;
    private DbsField pnd_Temp_Tot_Mnth_Pmts;
    private DbsField pnd_Temp_Tot_Annl_Pmts;
    private DbsField pnd_Temp_Tot_Annl_Units;
    private DbsField pnd_Temp_Tot_Mnth_Units;

    private DbsGroup pnd_Out_File;
    private DbsField pnd_Out_File_Pnd_O_Orch_Id;
    private DbsField pnd_Out_File_Pnd_O_Status;
    private DbsField pnd_Out_File_Pnd_O_Msg;
    private DbsField pnd_Out_File_Pnd_O_Delimiter;
    private DbsField pnd_O_Shrt_Nme;
    private DbsField pnd_Rc;
    private DbsField pnd_Rc_Msg;

    private DbsGroup pnd_Save_Data;
    private DbsField pnd_Save_Data_Pnd_Inv_Shrt_Nme;
    private DbsField pnd_Save_Data_Pnd_Frm_Fnd_Tckr;
    private DbsField pnd_Save_Data_Pnd_To_Fnd_Tckr;
    private DbsField pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt;
    private DbsField pnd_Save_Data_Pnd_Rvl_Mthd;
    private DbsField pnd_Trigger;
    private DbsField pnd_Fund_Details;

    private DbsGroup pnd_Fund_Details__R_Field_8;
    private DbsField pnd_Fund_Details_Pnd_Cref_Fund_Cde;
    private DbsField pnd_Fund_Details_Pnd_Valuation_Mthd;
    private DbsField pnd_Fund_Details_Pnd_Cref_Rate_Cde;
    private DbsField pnd_Fund_Details_Pnd_Cref_Units_Cnt;
    private DbsField pnd_Fund_Details_Pnd_Cref_Unit_Val;
    private DbsField pnd_Fund_Details_Pnd_Cref_Pmt_Amt;
    private DbsField pnd_Fund_Details_Pnd_Cref_Sttlmnt_Amt;
    private DbsField pnd_Fund_Details_Pnd_Cref_Sttlmnt_Units;
    private DbsField pnd_Fund_Details_Pnd_Frm_Omni_Tckr;
    private DbsField pnd_Fund_Details_Pnd_Omni_Fnd_Shrt_Nme;
    private DbsField pnd_Fund_Details_Pnd_To_Cref_Fnd_Tckr;
    private DbsField pnd_Fd1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal760 = new LdaIaal760();
        registerRecord(ldaIaal760);
        ldaIaal760b = new LdaIaal760b();
        registerRecord(ldaIaal760b);
        ldaIaal999f = new LdaIaal999f();
        registerRecord(ldaIaal999f);
        registerRecord(ldaIaal999f.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999f.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999f.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999f.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999f.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999f.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999f.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999f.getVw_cpr());
        registerRecord(ldaIaal999f.getVw_iaa_Cpr_Trans());
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_tiaa_Fund = new DataAccessProgramView(new NameInfo("vw_tiaa_Fund", "TIAA-FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr = vw_tiaa_Fund.getRecord().newFieldInGroup("TIAA_FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        tiaa_Fund_Tiaa_Cntrct_Payee_Cde = vw_tiaa_Fund.getRecord().newFieldInGroup("tiaa_Fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        tiaa_Fund_Tiaa_Cmpny_Fund_Cde = vw_tiaa_Fund.getRecord().newFieldInGroup("tiaa_Fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        registerRecord(vw_tiaa_Fund);

        pnd_Pmt_Mode = localVariables.newFieldArrayInRecord("pnd_Pmt_Mode", "#PMT-MODE", FieldType.NUMERIC, 3, new DbsArrayController(1, 22));
        pnd_No_Guar = localVariables.newFieldArrayInRecord("pnd_No_Guar", "#NO-GUAR", FieldType.NUMERIC, 2, new DbsArrayController(1, 8));
        pnd_Mode_Wrk = localVariables.newFieldInRecord("pnd_Mode_Wrk", "#MODE-WRK", FieldType.STRING, 3);

        pnd_Mode_Wrk__R_Field_1 = localVariables.newGroupInRecord("pnd_Mode_Wrk__R_Field_1", "REDEFINE", pnd_Mode_Wrk);
        pnd_Mode_Wrk_Pnd_Mode_Pos1 = pnd_Mode_Wrk__R_Field_1.newFieldInGroup("pnd_Mode_Wrk_Pnd_Mode_Pos1", "#MODE-POS1", FieldType.NUMERIC, 1);
        pnd_Mode_Wrk_Pnd_Mode_Pos2 = pnd_Mode_Wrk__R_Field_1.newFieldInGroup("pnd_Mode_Wrk_Pnd_Mode_Pos2", "#MODE-POS2", FieldType.NUMERIC, 2);
        pnd_Pend_Dte = localVariables.newFieldInRecord("pnd_Pend_Dte", "#PEND-DTE", FieldType.STRING, 6);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Check_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Dte__R_Field_2", "REDEFINE", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_A = pnd_Check_Dte__R_Field_2.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_A", "#CHECK-DTE-A", FieldType.STRING, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.STRING, 8);

        pnd_W_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_3", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Yyyy = pnd_W_Date__R_Field_3.newFieldInGroup("pnd_W_Date_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Date_Pnd_W_Mm = pnd_W_Date__R_Field_3.newFieldInGroup("pnd_W_Date_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_W_Date_Pnd_W_Dd = pnd_W_Date__R_Field_3.newFieldInGroup("pnd_W_Date_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);
        pnd_S_Invrse_Dte = localVariables.newFieldInRecord("pnd_S_Invrse_Dte", "#S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cpr_Cnt = localVariables.newFieldInRecord("pnd_Cpr_Cnt", "#CPR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Fnd_Cnt", "#FND-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Annl_Cnt = localVariables.newFieldInRecord("pnd_Annl_Cnt", "#ANNL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Mnth_Cnt = localVariables.newFieldInRecord("pnd_Mnth_Cnt", "#MNTH-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_W_Tot = localVariables.newFieldInRecord("pnd_W_Tot", "#W-TOT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Div_Amt = localVariables.newFieldInRecord("pnd_Tot_Div_Amt", "#TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Completed = localVariables.newFieldInRecord("pnd_Completed", "#COMPLETED", FieldType.STRING, 19);
        pnd_Rejected = localVariables.newFieldInRecord("pnd_Rejected", "#REJECTED", FieldType.STRING, 19);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Proc_Cnt = localVariables.newFieldInRecord("pnd_Proc_Cnt", "#PROC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rej_Cnt = localVariables.newFieldInRecord("pnd_Rej_Cnt", "#REJ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_4", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cpr_Cntrct = pnd_Cpr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Cntrct", "#CPR-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cpr_Payee = pnd_Cpr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Payee", "#CPR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Fund_Key", "#TIAA-CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Tiaa_Cntrct_Fund_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Tiaa_Cntrct_Fund_Key__R_Field_5", "REDEFINE", pnd_Tiaa_Cntrct_Fund_Key);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct = pnd_Tiaa_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct", "#FUND-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee = pnd_Tiaa_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee", "#FUND-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund = pnd_Tiaa_Cntrct_Fund_Key__R_Field_5.newFieldInGroup("pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund", "#CMPNY-FUND", 
            FieldType.STRING, 3);
        pnd_Tot_Annl_Units = localVariables.newFieldInRecord("pnd_Tot_Annl_Units", "#TOT-ANNL-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Tot_Annl_Pmts = localVariables.newFieldInRecord("pnd_Tot_Annl_Pmts", "#TOT-ANNL-PMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Mnth_Units = localVariables.newFieldInRecord("pnd_Tot_Mnth_Units", "#TOT-MNTH-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Tot_Mnth_Pmts = localVariables.newFieldInRecord("pnd_Tot_Mnth_Pmts", "#TOT-MNTH-PMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Price_Div = localVariables.newFieldInRecord("pnd_Price_Div", "#PRICE-DIV", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Price_Div__R_Field_6 = localVariables.newGroupInRecord("pnd_Price_Div__R_Field_6", "REDEFINE", pnd_Price_Div);
        pnd_Price_Div_Pnd_U_Val = pnd_Price_Div__R_Field_6.newFieldInGroup("pnd_Price_Div_Pnd_U_Val", "#U-VAL", FieldType.PACKED_DECIMAL, 9, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_C_Fnd = localVariables.newFieldInRecord("pnd_C_Fnd", "#C-FND", FieldType.STRING, 3);

        pnd_C_Fnd__R_Field_7 = localVariables.newGroupInRecord("pnd_C_Fnd__R_Field_7", "REDEFINE", pnd_C_Fnd);
        pnd_C_Fnd_Pnd_Cmpny = pnd_C_Fnd__R_Field_7.newFieldInGroup("pnd_C_Fnd_Pnd_Cmpny", "#CMPNY", FieldType.STRING, 1);
        pnd_C_Fnd_Pnd_Fund = pnd_C_Fnd__R_Field_7.newFieldInGroup("pnd_C_Fnd_Pnd_Fund", "#FUND", FieldType.STRING, 2);
        pnd_Per_Units_Cnt = localVariables.newFieldInRecord("pnd_Per_Units_Cnt", "#PER-UNITS-CNT", FieldType.NUMERIC, 9, 3);
        pnd_Per_Pmt_Amt = localVariables.newFieldInRecord("pnd_Per_Pmt_Amt", "#PER-PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Temp_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Temp_Fnd_Cnt", "#TEMP-FND-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Temp_Annl_Cnt = localVariables.newFieldInRecord("pnd_Temp_Annl_Cnt", "#TEMP-ANNL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Temp_Mnth_Cnt = localVariables.newFieldInRecord("pnd_Temp_Mnth_Cnt", "#TEMP-MNTH-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Temp_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_Temp_Tot_Per_Amt", "#TEMP-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Tot_Div_Amt = localVariables.newFieldInRecord("pnd_Temp_Tot_Div_Amt", "#TEMP-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Tot_Mnth_Pmts = localVariables.newFieldInRecord("pnd_Temp_Tot_Mnth_Pmts", "#TEMP-TOT-MNTH-PMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Tot_Annl_Pmts = localVariables.newFieldInRecord("pnd_Temp_Tot_Annl_Pmts", "#TEMP-TOT-ANNL-PMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Tot_Annl_Units = localVariables.newFieldInRecord("pnd_Temp_Tot_Annl_Units", "#TEMP-TOT-ANNL-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Temp_Tot_Mnth_Units = localVariables.newFieldInRecord("pnd_Temp_Tot_Mnth_Units", "#TEMP-TOT-MNTH-UNITS", FieldType.PACKED_DECIMAL, 10, 3);

        pnd_Out_File = localVariables.newGroupInRecord("pnd_Out_File", "#OUT-FILE");
        pnd_Out_File_Pnd_O_Orch_Id = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_O_Orch_Id", "#O-ORCH-ID", FieldType.STRING, 9);
        pnd_Out_File_Pnd_O_Status = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_O_Status", "#O-STATUS", FieldType.STRING, 19);
        pnd_Out_File_Pnd_O_Msg = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_O_Msg", "#O-MSG", FieldType.STRING, 99);
        pnd_Out_File_Pnd_O_Delimiter = pnd_Out_File.newFieldInGroup("pnd_Out_File_Pnd_O_Delimiter", "#O-DELIMITER", FieldType.STRING, 1);
        pnd_O_Shrt_Nme = localVariables.newFieldInRecord("pnd_O_Shrt_Nme", "#O-SHRT-NME", FieldType.STRING, 5);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Rc_Msg = localVariables.newFieldInRecord("pnd_Rc_Msg", "#RC-MSG", FieldType.STRING, 79);

        pnd_Save_Data = localVariables.newGroupArrayInRecord("pnd_Save_Data", "#SAVE-DATA", new DbsArrayController(1, 20));
        pnd_Save_Data_Pnd_Inv_Shrt_Nme = pnd_Save_Data.newFieldInGroup("pnd_Save_Data_Pnd_Inv_Shrt_Nme", "#INV-SHRT-NME", FieldType.STRING, 5);
        pnd_Save_Data_Pnd_Frm_Fnd_Tckr = pnd_Save_Data.newFieldInGroup("pnd_Save_Data_Pnd_Frm_Fnd_Tckr", "#FRM-FND-TCKR", FieldType.STRING, 10);
        pnd_Save_Data_Pnd_To_Fnd_Tckr = pnd_Save_Data.newFieldInGroup("pnd_Save_Data_Pnd_To_Fnd_Tckr", "#TO-FND-TCKR", FieldType.STRING, 10);
        pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt = pnd_Save_Data.newFieldInGroup("pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt", "#FND-STTLMNT-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Save_Data_Pnd_Rvl_Mthd = pnd_Save_Data.newFieldInGroup("pnd_Save_Data_Pnd_Rvl_Mthd", "#RVL-MTHD", FieldType.STRING, 1);
        pnd_Trigger = localVariables.newFieldInRecord("pnd_Trigger", "#TRIGGER", FieldType.STRING, 1);
        pnd_Fund_Details = localVariables.newFieldInRecord("pnd_Fund_Details", "#FUND-DETAILS", FieldType.STRING, 80);

        pnd_Fund_Details__R_Field_8 = localVariables.newGroupInRecord("pnd_Fund_Details__R_Field_8", "REDEFINE", pnd_Fund_Details);
        pnd_Fund_Details_Pnd_Cref_Fund_Cde = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Fund_Cde", "#CREF-FUND-CDE", FieldType.STRING, 
            2);
        pnd_Fund_Details_Pnd_Valuation_Mthd = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Valuation_Mthd", "#VALUATION-MTHD", FieldType.STRING, 
            1);
        pnd_Fund_Details_Pnd_Cref_Rate_Cde = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Rate_Cde", "#CREF-RATE-CDE", FieldType.STRING, 
            2);
        pnd_Fund_Details_Pnd_Cref_Units_Cnt = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Units_Cnt", "#CREF-UNITS-CNT", FieldType.NUMERIC, 
            9, 3);
        pnd_Fund_Details_Pnd_Cref_Unit_Val = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Unit_Val", "#CREF-UNIT-VAL", FieldType.NUMERIC, 
            9, 4);
        pnd_Fund_Details_Pnd_Cref_Pmt_Amt = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Pmt_Amt", "#CREF-PMT-AMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Fund_Details_Pnd_Cref_Sttlmnt_Amt = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Sttlmnt_Amt", "#CREF-STTLMNT-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Fund_Details_Pnd_Cref_Sttlmnt_Units = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Cref_Sttlmnt_Units", "#CREF-STTLMNT-UNITS", 
            FieldType.NUMERIC, 12, 3);
        pnd_Fund_Details_Pnd_Frm_Omni_Tckr = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Frm_Omni_Tckr", "#FRM-OMNI-TCKR", FieldType.STRING, 
            10);
        pnd_Fund_Details_Pnd_Omni_Fnd_Shrt_Nme = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_Omni_Fnd_Shrt_Nme", "#OMNI-FND-SHRT-NME", 
            FieldType.STRING, 5);
        pnd_Fund_Details_Pnd_To_Cref_Fnd_Tckr = pnd_Fund_Details__R_Field_8.newFieldInGroup("pnd_Fund_Details_Pnd_To_Cref_Fnd_Tckr", "#TO-CREF-FND-TCKR", 
            FieldType.STRING, 10);
        pnd_Fd1 = localVariables.newFieldInRecord("pnd_Fd1", "#FD1", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_tiaa_Fund.reset();

        ldaIaal760.initializeValues();
        ldaIaal760b.initializeValues();
        ldaIaal999f.initializeValues();

        localVariables.reset();
        pnd_Pmt_Mode.getValue(1).setInitialValue(100);
        pnd_Pmt_Mode.getValue(2).setInitialValue(601);
        pnd_Pmt_Mode.getValue(3).setInitialValue(602);
        pnd_Pmt_Mode.getValue(4).setInitialValue(603);
        pnd_Pmt_Mode.getValue(5).setInitialValue(701);
        pnd_Pmt_Mode.getValue(6).setInitialValue(702);
        pnd_Pmt_Mode.getValue(7).setInitialValue(703);
        pnd_Pmt_Mode.getValue(8).setInitialValue(704);
        pnd_Pmt_Mode.getValue(9).setInitialValue(705);
        pnd_Pmt_Mode.getValue(10).setInitialValue(706);
        pnd_Pmt_Mode.getValue(11).setInitialValue(801);
        pnd_Pmt_Mode.getValue(12).setInitialValue(802);
        pnd_Pmt_Mode.getValue(13).setInitialValue(803);
        pnd_Pmt_Mode.getValue(14).setInitialValue(804);
        pnd_Pmt_Mode.getValue(15).setInitialValue(805);
        pnd_Pmt_Mode.getValue(16).setInitialValue(806);
        pnd_Pmt_Mode.getValue(17).setInitialValue(807);
        pnd_Pmt_Mode.getValue(18).setInitialValue(808);
        pnd_Pmt_Mode.getValue(19).setInitialValue(809);
        pnd_Pmt_Mode.getValue(20).setInitialValue(810);
        pnd_Pmt_Mode.getValue(21).setInitialValue(811);
        pnd_Pmt_Mode.getValue(22).setInitialValue(812);
        pnd_No_Guar.getValue(1).setInitialValue(1);
        pnd_No_Guar.getValue(2).setInitialValue(3);
        pnd_No_Guar.getValue(3).setInitialValue(4);
        pnd_No_Guar.getValue(4).setInitialValue(7);
        pnd_No_Guar.getValue(5).setInitialValue(18);
        pnd_No_Guar.getValue(6).setInitialValue(19);
        pnd_No_Guar.getValue(7).setInitialValue(20);
        pnd_No_Guar.getValue(8).setInitialValue(57);
        pnd_Mode_Wrk.setInitialValue(" ");
        pnd_Completed.setInitialValue("Platform Complete");
        pnd_Rejected.setInitialValue("Platform Rejected");
        pnd_Trigger.setInitialValue("H'00'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap760() throws Exception
    {
        super("Iaap760");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 250;//Natural: FORMAT ( 2 ) PS = 0 LS = 250
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
        sub_Initialize_Fields();
        if (condition(Global.isEscape())) {return;}
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Check_Dte.compute(new ComputeParameters(false, pnd_Check_Dte), pnd_Date.val());                                                                           //Natural: ASSIGN #CHECK-DTE := VAL ( #DATE )
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Todays_Dte.compute(new ComputeParameters(false, pnd_Todays_Dte), pnd_Date.val());                                                                         //Natural: ASSIGN #TODAYS-DTE := VAL ( #DATE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #IN-OPT-OUT
        while (condition(getWorkFiles().read(1, ldaIaal760.getPnd_In_Opt_Out())))
        {
            ldaIaal999f.getVw_iaa_Cntrct().reset();                                                                                                                       //Natural: RESET IAA-CNTRCT CPR IAA-TIAA-FUND-RCRD #TOT-PER-AMT #TOT-DIV-AMT #TEMP-FND-CNT #TEMP-ANNL-CNT #TEMP-MNTH-CNT #TEMP-TOT-PER-AMT #TEMP-TOT-DIV-AMT #TEMP-TOT-MNTH-PMTS #TEMP-TOT-ANNL-PMTS #TEMP-TOT-ANNL-UNITS #TEMP-TOT-MNTH-UNITS #O-ORCH-ID #O-STATUS #O-MSG #INV-SHRT-NME ( * ) #FRM-FND-TCKR ( * ) #TO-FND-TCKR ( * ) #FND-STTLMNT-AMT ( * ) #RVL-MTHD ( * ) #PER-UNITS-CNT #PER-PMT-AMT
            ldaIaal999f.getVw_cpr().reset();
            ldaIaal999f.getVw_iaa_Tiaa_Fund_Rcrd().reset();
            pnd_Tot_Per_Amt.reset();
            pnd_Tot_Div_Amt.reset();
            pnd_Temp_Fnd_Cnt.reset();
            pnd_Temp_Annl_Cnt.reset();
            pnd_Temp_Mnth_Cnt.reset();
            pnd_Temp_Tot_Per_Amt.reset();
            pnd_Temp_Tot_Div_Amt.reset();
            pnd_Temp_Tot_Mnth_Pmts.reset();
            pnd_Temp_Tot_Annl_Pmts.reset();
            pnd_Temp_Tot_Annl_Units.reset();
            pnd_Temp_Tot_Mnth_Units.reset();
            pnd_Out_File_Pnd_O_Orch_Id.reset();
            pnd_Out_File_Pnd_O_Status.reset();
            pnd_Out_File_Pnd_O_Msg.reset();
            pnd_Save_Data_Pnd_Inv_Shrt_Nme.getValue("*").reset();
            pnd_Save_Data_Pnd_Frm_Fnd_Tckr.getValue("*").reset();
            pnd_Save_Data_Pnd_To_Fnd_Tckr.getValue("*").reset();
            pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt.getValue("*").reset();
            pnd_Save_Data_Pnd_Rvl_Mthd.getValue("*").reset();
            pnd_Per_Units_Cnt.reset();
            pnd_Per_Pmt_Amt.reset();
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            pnd_Out_File_Pnd_O_Orch_Id.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orchstrtn_Id());                                                                         //Natural: ASSIGN #O-ORCH-ID := #ORCHSTRTN-ID
            if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code().notEquals("NI")))                                                                               //Natural: IF #PROCESS-CODE NE 'NI'
            {
                pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                         //Natural: ASSIGN #O-STATUS := #REJECTED
                pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde(),             //Natural: COMPRESS #CNTRCT-NBR #PAYEE-CDE #PROCESS-CODE ' - Invalid process code' INTO #O-MSG
                    ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code(), " - Invalid process code"));
                pnd_Rej_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REJ-CNT
                getWorkFiles().write(2, false, pnd_Out_File);                                                                                                             //Natural: WRITE WORK FILE 2 #OUT-FILE
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-CONTRACT-RECORD
                sub_Create_Contract_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-RECORD
                sub_Create_Cpr_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-FUND-RECORD
                sub_Create_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                     //Natural: IF #O-MSG NE ' '
                {
                    pnd_Rej_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REJ-CNT
                    getWorkFiles().write(2, false, pnd_Out_File);                                                                                                         //Natural: WRITE WORK FILE 2 #OUT-FILE
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                    sub_Write_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-ESP-TRANS-FILE
            sub_Create_Esp_Trans_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RECORD
            sub_Create_Trans_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                         //Natural: IF #O-MSG NE ' '
            {
                pnd_Rej_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REJ-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
                sub_Write_Error_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Proc_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #PROC-CNT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Out_File_Pnd_O_Status.equals(" ")))                                                                                                         //Natural: IF #O-STATUS = ' '
            {
                pnd_Out_File_Pnd_O_Status.setValue(pnd_Completed);                                                                                                        //Natural: ASSIGN #O-STATUS := #COMPLETED
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, pnd_Out_File);                                                                                                                 //Natural: WRITE WORK FILE 2 #OUT-FILE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //* *IF #REC-CNT = 0  /* WRITE OUT EMPTY FILES
        //*   RESET #OUT-FILE #ESP-DATA
        //*   WRITE WORK 2 #OUT-FILE
        //*   WRITE WORK 3 #ESP-DATA
        //* *END-IF
        getWorkFiles().write(4, false, pnd_Trigger);                                                                                                                      //Natural: WRITE WORK FILE 4 #TRIGGER
        getReports().write(1, ReportOption.NOTITLE,"TOTAL INPUT READ-------->",pnd_Rec_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                           //Natural: WRITE ( 1 ) 'TOTAL INPUT READ-------->' #REC-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL INPUT PROCESSED--->",pnd_Proc_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                          //Natural: WRITE ( 1 ) 'TOTAL INPUT PROCESSED--->' #PROC-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL INPUT REJECTED---->",pnd_Rej_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                           //Natural: WRITE ( 1 ) 'TOTAL INPUT REJECTED---->' #REJ-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL CONTRACTS ADDED--->",pnd_Cntrct_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                        //Natural: WRITE ( 1 ) 'TOTAL CONTRACTS ADDED--->' #CNTRCT-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL CPR       ADDED--->",pnd_Cpr_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                           //Natural: WRITE ( 1 ) 'TOTAL CPR       ADDED--->' #CPR-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL FUNDS     ADDED--->",pnd_Fnd_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                           //Natural: WRITE ( 1 ) 'TOTAL FUNDS     ADDED--->' #FND-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL MONTHLY FUNDS----->",pnd_Mnth_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                          //Natural: WRITE ( 1 ) 'TOTAL MONTHLY FUNDS----->' #MNTH-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL ANNUAL FUNDS------>",pnd_Annl_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                          //Natural: WRITE ( 1 ) 'TOTAL ANNUAL FUNDS------>' #ANNL-CNT ( EM = ZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* *WRITE(1) 'TOTAL FIXED PMTS-------->' #TOT-PER-AMT   (EM=ZZ,ZZZ,ZZ9.99)
        //* *WRITE(1) 'TOTAL DIVIDEND  PMTS---->' #TOT-DIV-AMT   (EM=ZZ,ZZZ,ZZ9.99)
        getReports().write(1, ReportOption.NOTITLE,"TOTAL MONTHLY PMTS------>",pnd_Tot_Mnth_Pmts, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"));                                  //Natural: WRITE ( 1 ) 'TOTAL MONTHLY PMTS------>' #TOT-MNTH-PMTS ( EM = ZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL ANNUAL PMTS------->",pnd_Tot_Annl_Pmts, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"));                                  //Natural: WRITE ( 1 ) 'TOTAL ANNUAL PMTS------->' #TOT-ANNL-PMTS ( EM = ZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL ANNUAL UNITS------>",pnd_Tot_Annl_Units, new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));                                //Natural: WRITE ( 1 ) 'TOTAL ANNUAL UNITS------>' #TOT-ANNL-UNITS ( EM = ZZ,ZZZ,ZZ9.999 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL MNTHLY UNITS------>",pnd_Tot_Mnth_Units, new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));                                //Natural: WRITE ( 1 ) 'TOTAL MNTHLY UNITS------>' #TOT-MNTH-UNITS ( EM = ZZ,ZZZ,ZZ9.999 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CONTRACT-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPR-RECORD
        //* *PERFORM EDIT-ISSUE-DATE-MODE
        //* *IF #O-MSG NE ' '
        //* *  ESCAPE ROUTINE
        //* *END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUND-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RECORD
        //* *IF #PROCESS-CODE = 'RV'
        //* *  IAA-TRANS-RCRD.TRANS-CDE := 020
        //* *ELSE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-ISSUE-DATE-MODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ESP-TRANS-FILE
        //*  112017 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SHORT-NAME
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SUB-LOB
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //* ***********************************************************************
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fa().setValue("`");                                                                                                               //Natural: MOVE '`' TO #FA #FB #FC #FD #FE #FF #FG #FH #FI #FJ #FK #FL #FM #FN #FO #FP #FQ #FR #FS #FT #FU #FV #FW #FX #FY #FZ #F1 #F2 #F3 #F4 #F5 #F6 #F7 #F8 #F9
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fb().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fc().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fd().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fe().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Ff().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fg().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fh().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fi().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fj().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fk().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fl().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fm().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fn().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fo().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fp().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fq().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fr().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fs().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Ft().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fu().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fv().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fw().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fx().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fy().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_Fz().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F1().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F2().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F3().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F4().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F5().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F6().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F7().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F8().setValue("`");
        ldaIaal760b.getPnd_Esp_Data_Pnd_F9().setValue("`");
        pnd_Out_File_Pnd_O_Delimiter.setValue(",");                                                                                                                       //Natural: ASSIGN #O-DELIMITER := ','
        ldaIaal760b.getPnd_Esp_Data_Pnd_Extract_Timestamp().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISS"));                                        //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISS ) TO #EXTRACT-TIMESTAMP
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
    }
    private void sub_Create_Contract_Record() throws Exception                                                                                                            //Natural: CREATE-CONTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal999f.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), WcType.WITH) }
        );
        FIND01:
        while (condition(ldaIaal999f.getVw_iaa_Cntrct().readNextRow("FIND01")))
        {
            ldaIaal999f.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Contract already exists");                                                                                                   //Natural: ASSIGN #O-MSG := 'Contract already exists'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                         //Natural: ASSIGN #TRANS-DTE := *TIMX
        //*  DO NOT CREATE ANOTHER CONTRACT RECORD WHEN THE PAYEE IS NOT 01
        //*  - WILL CAUSE UNIQUENESS PROBLEM.
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().notEquals("01")))                                                                                      //Natural: IF #PAYEE-CDE NE '01'
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid payee code");                                                                                                        //Natural: ASSIGN #O-MSG := 'Invalid payee code'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde()));                                      //Natural: COMPRESS #O-MSG #PAYEE-CDE INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde(),"NN"))))                                                                         //Natural: IF #ORGN-CDE NE MASK ( NN )
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid origin code");                                                                                                       //Natural: ASSIGN #O-MSG := 'Invalid origin code'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde()));                                       //Natural: COMPRESS #O-MSG #ORGN-CDE INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde(),"NN"))))                                                                         //Natural: IF #OPTN-CDE NE MASK ( NN )
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid option code");                                                                                                       //Natural: ASSIGN #O-MSG := 'Invalid option code'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde()));                                       //Natural: COMPRESS #O-MSG #OPTN-CDE INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte());                                                                                                //Natural: ASSIGN #W-DATE := #ISSUE-DTE
        if (condition(DbsUtil.maskMatches(pnd_W_Date,"YYYYMMDD")))                                                                                                        //Natural: IF #W-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Issue Date not in YYYYMMDD format");                                                                                         //Natural: ASSIGN #O-MSG := 'Issue Date not in YYYYMMDD format'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte());                                                                                          //Natural: ASSIGN #W-DATE := #1ST-PMT-DUE-DTE
        if (condition(DbsUtil.maskMatches(pnd_W_Date,"YYYYMMDD")))                                                                                                        //Natural: IF #W-DATE = MASK ( YYYYMMDD )
        {
            if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte().greater(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte())))                                      //Natural: IF #ISSUE-DTE GT #1ST-PMT-DUE-DTE
            {
                pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                         //Natural: ASSIGN #O-STATUS := #REJECTED
                pnd_Out_File_Pnd_O_Msg.setValue("First Payment Due Date before Issue Date");                                                                              //Natural: ASSIGN #O-MSG := 'First Payment Due Date before Issue Date'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("First Payment Due Date not in YYYYMMDD format");                                                                             //Natural: ASSIGN #O-MSG := 'First Payment Due Date not in YYYYMMDD format'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte().notEquals(" ")))                                                                                  //Natural: IF #1ST-PMT-PD-DTE NE ' '
        {
            pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte());                                                                                       //Natural: ASSIGN #W-DATE := #1ST-PMT-PD-DTE
            if (condition(DbsUtil.maskMatches(pnd_W_Date,"YYYYMMDD")))                                                                                                    //Natural: IF #W-DATE = MASK ( YYYYMMDD )
            {
                if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte().greater(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dte())))                                   //Natural: IF #ISSUE-DTE GT #1ST-PMT-PD-DTE
                {
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue("First Payment Paid Date before Issue Date");                                                                         //Natural: ASSIGN #O-MSG := 'First Payment Paid Date before Issue Date'
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                         //Natural: ASSIGN #O-STATUS := #REJECTED
                pnd_Out_File_Pnd_O_Msg.setValue("First Payment Paid Date not in YYYYMMDD format");                                                                        //Natural: ASSIGN #O-MSG := 'First Payment Paid Date not in YYYYMMDD format'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("First Payment Paid Date is required");                                                                                       //Natural: ASSIGN #O-MSG := 'First Payment Paid Date is required'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Dob(),"YYYYMMDD"))))                                                               //Natural: IF #1ST-ANNT-DOB NE MASK ( YYYYMMDD )
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("First Annt DOB not in YYYYMMDD format");                                                                                     //Natural: ASSIGN #O-MSG := 'First Annt DOB not in YYYYMMDD format'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob().notEquals(" ") && ! (DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob(),"YYYYMMDD"))  //Natural: IF #2ND-ANNT-DOB NE ' ' AND #2ND-ANNT-DOB NE MASK ( YYYYMMDD ) AND #2ND-ANNT-DOB NE '00000000'
            && ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob().notEquals("00000000")))
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("2nd Annt DOB not in YYYYMMDD format");                                                                                       //Natural: ASSIGN #O-MSG := '2nd Annt DOB not in YYYYMMDD format'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde().equals(" ") || ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde().equals("0")))          //Natural: IF #PRTCPNT-CTZNSHP-CDE = ' ' OR = '0'
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid citizenship code");                                                                                                  //Natural: ASSIGN #O-MSG := 'Invalid citizenship code'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde()));                            //Natural: COMPRESS #O-MSG #PRTCPNT-CTZNSHP-CDE INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde().equals(" ") || ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde().equals("0")))                //Natural: IF #1ST-ANNT-SEX-CDE = ' ' OR = '0'
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid sex code");                                                                                                          //Natural: ASSIGN #O-MSG := 'Invalid sex code'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde()));                               //Natural: COMPRESS #O-MSG #1ST-ANNT-SEX-CDE INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Cntrct_Cntrct_Ppcn_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-PPCN-NBR := #CNTRCT-NBR
        ldaIaal999f.getIaa_Cntrct_Cntrct_Optn_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Optn_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde().val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-OPTN-CDE := VAL ( #OPTN-CDE )
        ldaIaal999f.getIaa_Cntrct_Cntrct_Orgn_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Orgn_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde().val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := VAL ( #ORGN-CDE )
        ldaIaal999f.getIaa_Cntrct_Cntrct_Issue_Dte().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte_N6());                                                           //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE := #ISSUE-DTE-N6
        ldaIaal999f.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte_Dd());                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD := #ISSUE-DTE-DD
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1s_Pmt_Due_N6());                                                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE := #1S-PMT-DUE-N6
        ldaIaal999f.getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dd());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-DUE-DTE-DD := #1ST-PMT-DUE-DD
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_N6());                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE := #1ST-PMT-PD-N6
        ldaIaal999f.getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Pd_Dd());                                                       //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-PD-DTE-DD := #1ST-PMT-PD-DD
        ldaIaal999f.getIaa_Cntrct_Cntrct_Crrncy_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Crrncy_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Crrncy_Cde().val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-CRRNCY-CDE := VAL ( #CRRNCY-CDE )
        ldaIaal999f.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Stte(), MoveOption.RightJustified);                        //Natural: MOVE RIGHT #ISSUE-STTE TO IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Xref());                                                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND := #1ST-ANNT-XREF
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte()),                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE := VAL ( #1ST-ANNT-DOB )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Dob().val());
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Yyyy());                                          //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE := #1ST-ANNT-YYYY
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde()),                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := VAL ( #1ST-ANNT-SEX-CDE )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde().val());
        ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt().setValue(1);                                                                                                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := 1
        if (condition(ldaIaal999f.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21) || ldaIaal999f.getIaa_Cntrct_Cntrct_Optn_Cde().equals(22)))                                  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21 OR = 22
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt().setValue(0);                                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob(),"YYYYMMDD")))                                                                   //Natural: IF #2ND-ANNT-DOB = MASK ( YYYYMMDD )
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte()),              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE := VAL ( #2ND-ANNT-DOB )
                ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob().val());
            ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Yyyy());                                       //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE := #2ND-ANNT-YYYY
            ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt().setValue(1);                                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Xref());                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND := #2ND-ANNT-XREF
        if (condition(DbsUtil.is(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde().getText(),"N1")))                                                                    //Natural: IF #2ND-ANNT-SEX-CDE IS ( N1 )
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde()),              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := VAL ( #2ND-ANNT-SEX-CDE )
                ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Sex_Cde().val());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Ssn().getText(),"N9")))                                                                        //Natural: IF #2ND-ANNT-SSN IS ( N9 )
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn()), ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Ssn().val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN := VAL ( #2ND-ANNT-SSN )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TRANS-DTE
        if (condition(! (DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Annty_Strt_Dte(),"YYYYMMDD"))))                                                             //Natural: IF #ANNTY-STRT-DTE NE MASK ( YYYYMMDD )
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Annuity start date not in YYYYMMDD format");                                                                                 //Natural: ASSIGN #O-MSG := 'Annuity start date not in YYYYMMDD format'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Annty_Strt_Dte());                                                                                           //Natural: ASSIGN #W-DATE := #ANNTY-STRT-DTE
        ldaIaal999f.getIaa_Cntrct_Cntrct_Annty_Strt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date);                                                      //Natural: MOVE EDITED #W-DATE TO IAA-CNTRCT.CNTRCT-ANNTY-STRT-DTE ( EM = YYYYMMDD )
        //*  112017 - START
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde().equals("T")))                                                                                   //Natural: IF #CNTRCT-CMPNY-CDE = 'T'
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr());                                          //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR := #ORIG-TIAA-DA-NBR
            ldaIaal999f.getIaa_Cntrct_Orgntng_Cntrct_Nmbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr());                                                //Natural: ASSIGN IAA-CNTRCT.ORGNTNG-CNTRCT-NMBR := #ORIG-TIAA-DA-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal999f.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr());                                          //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR := #ORIG-CREF-DA-NBR
            ldaIaal999f.getIaa_Cntrct_Orgntng_Cntrct_Nmbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr());                                                //Natural: ASSIGN IAA-CNTRCT.ORGNTNG-CNTRCT-NMBR := #ORIG-CREF-DA-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //*  112017 - END
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Prem_Dtes().getValue(1),"YYYYMMDD")))                                                     //Natural: IF #FINL-PREM-DTES ( 1 ) = MASK ( YYYYMMDD )
        {
            pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Prem_Dtes().getValue(1));                                                                           //Natural: ASSIGN #W-DATE := #FINL-PREM-DTES ( 1 )
            ldaIaal999f.getIaa_Cntrct_Cntrct_Fnl_Prm_Dte().getValue(1).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date);                                         //Natural: MOVE EDITED #W-DATE TO IAA-CNTRCT.CNTRCT-FNL-PRM-DTE ( 1 ) ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Prem_Dtes().getValue(2),"YYYYMMDD")))                                                     //Natural: IF #FINL-PREM-DTES ( 2 ) = MASK ( YYYYMMDD )
        {
            pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Prem_Dtes().getValue(2));                                                                           //Natural: ASSIGN #W-DATE := #FINL-PREM-DTES ( 2 )
            ldaIaal999f.getIaa_Cntrct_Cntrct_Fnl_Prm_Dte().getValue(2).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date);                                         //Natural: MOVE EDITED #W-DATE TO IAA-CNTRCT.CNTRCT-FNL-PRM-DTE ( 2 ) ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Cntrct_Cntrct_Mtch_Ppcn().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr());                                                      //Natural: ASSIGN IAA-CNTRCT.CNTRCT-MTCH-PPCN := #MTCHNG-CNTRCT-NBR
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte(),"YYYYMMDD")))                                                             //Natural: IF #ROTH-1ST-CNTRB-DTE = MASK ( YYYYMMDD )
        {
            pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Roth_1st_Cntrb_Dte());                                                                                   //Natural: ASSIGN #W-DATE := #ROTH-1ST-CNTRB-DTE
            ldaIaal999f.getIaa_Cntrct_Roth_Frst_Cntrb_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date);                                                    //Natural: MOVE EDITED #W-DATE TO IAA-CNTRCT.ROTH-FRST-CNTRB-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaIaal760.getPnd_In_Opt_Out_Pnd_Tax_Exempt()), new ExamineTranslate(TranslateOption.Upper));                                   //Natural: EXAMINE #TAX-EXEMPT AND TRANSLATE INTO UPPER CASE
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Tax_Exempt().equals("TRUE")))                                                                                      //Natural: IF #TAX-EXEMPT = 'TRUE'
        {
            ldaIaal999f.getIaa_Cntrct_Tax_Exmpt_Ind().setValue("Y");                                                                                                      //Natural: ASSIGN IAA-CNTRCT.TAX-EXMPT-IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal999f.getIaa_Cntrct_Tax_Exmpt_Ind().setValue("N");                                                                                                      //Natural: ASSIGN IAA-CNTRCT.TAX-EXMPT-IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Cntrct_Sub_Plan_Nmbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr());                                                               //Natural: ASSIGN IAA-CNTRCT.SUB-PLAN-NMBR := #SUB-PLN-NBR
        ldaIaal999f.getIaa_Cntrct_Plan_Nmbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Pln_Nbr());                                                                       //Natural: ASSIGN IAA-CNTRCT.PLAN-NMBR := #PLN-NBR
        ldaIaal999f.getIaa_Cntrct_For_Future_Use().setValue("OPTOUT");                                                                                                    //Natural: ASSIGN IAA-CNTRCT.FOR-FUTURE-USE := 'OPTOUT'
        ldaIaal999f.getIaa_Cntrct_Cntrct_Type().setValue("A");                                                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE := 'A'
        ldaIaal999f.getVw_iaa_Cntrct().insertDBRow();                                                                                                                     //Natural: STORE IAA-CNTRCT
    }
    private void sub_Create_Cpr_Record() throws Exception                                                                                                                 //Natural: CREATE-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                             //Natural: IF #O-MSG NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cpr_Key_Pnd_Cpr_Cntrct.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                                               //Natural: ASSIGN #CPR-CNTRCT := #CNTRCT-NBR
        pnd_Cpr_Key_Pnd_Cpr_Payee.compute(new ComputeParameters(false, pnd_Cpr_Key_Pnd_Cpr_Payee), ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val());                   //Natural: ASSIGN #CPR-PAYEE := VAL ( #PAYEE-CDE )
        ldaIaal999f.getVw_cpr().startDatabaseFind                                                                                                                         //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(ldaIaal999f.getVw_cpr().readNextRow("FIND02")))
        {
            ldaIaal999f.getVw_cpr().setIfNotFoundControlFlag(false);
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Contract Payee already exists");                                                                                             //Natural: ASSIGN #O-MSG := 'Contract Payee already exists'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind().equals(pnd_Pmt_Mode.getValue("*"))))                                                             //Natural: IF #CNTRCT-MODE-IND = #PMT-MODE ( * )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Invalid Payment Mode");                                                                                                      //Natural: ASSIGN #O-MSG := 'Invalid Payment Mode'
            pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind()));                                //Natural: COMPRESS #O-MSG #CNTRCT-MODE-IND INTO #O-MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte(),"YYYYMM") && ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde().equals(pnd_No_Guar.getValue("*")))) //Natural: IF #FINL-PER-PAY-DTE = MASK ( YYYYMM ) AND VAL ( #OPTN-CDE ) = #NO-GUAR ( * )
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("Final per pay date not valid for annuity option");                                                                           //Natural: ASSIGN #O-MSG := 'Final per pay date not valid for annuity option'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mode_Wrk.setValueEdited(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind(),new ReportEditMask("999"));                                                        //Natural: MOVE EDITED #CNTRCT-MODE-IND ( EM = 999 ) TO #MODE-WRK
        pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte());                                                                                                //Natural: ASSIGN #W-DATE := #ISSUE-DTE
        ldaIaal999f.getCpr_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                                //Natural: ASSIGN CPR.CNTRCT-PART-PPCN-NBR := #CNTRCT-NBR
        ldaIaal999f.getCpr_Cntrct_Part_Payee_Cde().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Cntrct_Part_Payee_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val()); //Natural: ASSIGN CPR.CNTRCT-PART-PAYEE-CDE := VAL ( #PAYEE-CDE )
        if (condition(DbsUtil.is(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr().getText(),"N12")))                                                                         //Natural: IF #CPR-ID-NBR IS ( N12 )
        {
            ldaIaal999f.getCpr_Cpr_Id_Nbr().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Cpr_Id_Nbr()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr().val());  //Natural: ASSIGN CPR.CPR-ID-NBR := VAL ( #CPR-ID-NBR )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Cntrct_Actvty_Cde().setValue(1);                                                                                                               //Natural: ASSIGN CPR.CNTRCT-ACTVTY-CDE := 1
        ldaIaal999f.getCpr_Cntrct_Company_Cd().getValue("*").reset();                                                                                                     //Natural: RESET CPR.CNTRCT-COMPANY-CD ( * ) CPR.CNTRCT-RCVRY-TYPE-IND ( * ) CPR.CNTRCT-IVC-AMT ( * ) CPR.CNTRCT-PER-IVC-AMT ( * ) CPR.CNTRCT-IVC-USED-AMT ( * ) CPR.CNTRCT-RESDL-IVC-AMT ( * ) CPR.CNTRCT-RTB-AMT ( * ) CPR.CNTRCT-RTB-PERCENT ( * )
        ldaIaal999f.getCpr_Cntrct_Rcvry_Type_Ind().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Ivc_Amt().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Per_Ivc_Amt().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Ivc_Used_Amt().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Resdl_Ivc_Amt().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Rtb_Amt().getValue("*").reset();
        ldaIaal999f.getCpr_Cntrct_Rtb_Percent().getValue("*").reset();
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde().equals("T")))                                                                                   //Natural: IF #CNTRCT-CMPNY-CDE = 'T'
        {
            ldaIaal999f.getCpr_Cntrct_Company_Cd().getValue(1).setValue("T");                                                                                             //Natural: ASSIGN CPR.CNTRCT-COMPANY-CD ( 1 ) := 'T'
            ldaIaal999f.getCpr_Cntrct_Ivc_Amt().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt());                                                  //Natural: ASSIGN CPR.CNTRCT-IVC-AMT ( 1 ) := #CNTRCT-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Per_Ivc_Amt().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt());                                          //Natural: ASSIGN CPR.CNTRCT-PER-IVC-AMT ( 1 ) := #CNTRCT-PER-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Ivc_Used_Amt().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt());                                        //Natural: ASSIGN CPR.CNTRCT-IVC-USED-AMT ( 1 ) := #CNTRCT-IVC-USED-AMT
            ldaIaal999f.getCpr_Cntrct_Resdl_Ivc_Amt().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt());                                       //Natural: ASSIGN CPR.CNTRCT-RESDL-IVC-AMT ( 1 ) := #CNTRCT-RSDL-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Rtb_Amt().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt());                                                  //Natural: ASSIGN CPR.CNTRCT-RTB-AMT ( 1 ) := #CNTRCT-RTB-AMT
            ldaIaal999f.getCpr_Cntrct_Rtb_Percent().getValue(1).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct());                                              //Natural: ASSIGN CPR.CNTRCT-RTB-PERCENT ( 1 ) := #CNTRCT-RTB-PCT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal999f.getCpr_Cntrct_Company_Cd().getValue(2).setValue("C");                                                                                             //Natural: ASSIGN CPR.CNTRCT-COMPANY-CD ( 2 ) := 'C'
            ldaIaal999f.getCpr_Cntrct_Ivc_Amt().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt());                                                  //Natural: ASSIGN CPR.CNTRCT-IVC-AMT ( 2 ) := #CNTRCT-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Per_Ivc_Amt().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt());                                          //Natural: ASSIGN CPR.CNTRCT-PER-IVC-AMT ( 2 ) := #CNTRCT-PER-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Ivc_Used_Amt().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Used_Amt());                                        //Natural: ASSIGN CPR.CNTRCT-IVC-USED-AMT ( 2 ) := #CNTRCT-IVC-USED-AMT
            ldaIaal999f.getCpr_Cntrct_Resdl_Ivc_Amt().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rsdl_Ivc_Amt());                                       //Natural: ASSIGN CPR.CNTRCT-RESDL-IVC-AMT ( 2 ) := #CNTRCT-RSDL-IVC-AMT
            ldaIaal999f.getCpr_Cntrct_Rtb_Amt().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Amt());                                                  //Natural: ASSIGN CPR.CNTRCT-RTB-AMT ( 2 ) := #CNTRCT-RTB-AMT
            ldaIaal999f.getCpr_Cntrct_Rtb_Percent().getValue(2).setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Rtb_Pct());                                              //Natural: ASSIGN CPR.CNTRCT-RTB-PERCENT ( 2 ) := #CNTRCT-RTB-PCT
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                       //Natural: ASSIGN CPR.LST-TRANS-DTE := #TRANS-DTE
        ldaIaal999f.getCpr_Cntrct_Mode_Ind().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind());                                                                //Natural: ASSIGN CPR.CNTRCT-MODE-IND := #CNTRCT-MODE-IND
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte(),"YYYYMM")))                                                                 //Natural: IF #FINL-PER-PAY-DTE = MASK ( YYYYMM )
        {
            ldaIaal999f.getCpr_Cntrct_Final_Per_Pay_Dte().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Cntrct_Final_Per_Pay_Dte()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte().val()); //Natural: ASSIGN CPR.CNTRCT-FINAL-PER-PAY-DTE := VAL ( #FINL-PER-PAY-DTE )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte(),"YYYYMMDD")))                                                                 //Natural: IF #FINL-PYMNT-DTE = MASK ( YYYYMMDD )
        {
            ldaIaal999f.getCpr_Cntrct_Final_Pay_Dte().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Cntrct_Final_Pay_Dte()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Pymnt_Dte().val()); //Natural: ASSIGN CPR.CNTRCT-FINAL-PAY-DTE := VAL ( #FINL-PYMNT-DTE )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Prtcpnt_Ctznshp_Cde().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Prtcpnt_Ctznshp_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Ctznshp_Cde().val()); //Natural: ASSIGN CPR.PRTCPNT-CTZNSHP-CDE := VAL ( #PRTCPNT-CTZNSHP-CDE )
        ldaIaal999f.getCpr_Prtcpnt_Rsdncy_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde(), MoveOption.RightJustified);                               //Natural: MOVE RIGHT #PRTCPNT-RSDNCY-CDE TO CPR.PRTCPNT-RSDNCY-CDE
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde().equals("03E")))                                                                               //Natural: IF #PRTCPNT-RSDNCY-CDE = '03E'
        {
            ldaIaal999f.getCpr_Prtcpnt_Rsdncy_Cde().setValue("035");                                                                                                      //Natural: ASSIGN CPR.PRTCPNT-RSDNCY-CDE := '035'
            ldaIaal999f.getCpr_Cntrct_Local_Cde().setValue("03E");                                                                                                        //Natural: ASSIGN CPR.CNTRCT-LOCAL-CDE := '03E'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.is(ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr().getText(),"N9")))                                                                  //Natural: IF #PRTCPNT-TAX-ID-NBR IS ( N9 )
        {
            ldaIaal999f.getCpr_Prtcpnt_Tax_Id_Nbr().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Prtcpnt_Tax_Id_Nbr()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr().val()); //Natural: ASSIGN CPR.PRTCPNT-TAX-ID-NBR := VAL ( #PRTCPNT-TAX-ID-NBR )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Cntrct_Emplymnt_Trmnt_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Emplymnt_Term_Cde());                                             //Natural: ASSIGN CPR.CNTRCT-EMPLYMNT-TRMNT-CDE := #CNTRCT-EMPLYMNT-TERM-CDE
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde().equals(" ")))                                                                                    //Natural: IF #CNTRCT-PEND-CDE = ' '
        {
            ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde().setValue("0");                                                                                             //Natural: ASSIGN #CNTRCT-PEND-CDE := '0'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Cntrct_Pend_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde());                                                                //Natural: ASSIGN CPR.CNTRCT-PEND-CDE := #CNTRCT-PEND-CDE
        ldaIaal999f.getCpr_Cntrct_Hold_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Hold_Cde());                                                                //Natural: ASSIGN CPR.CNTRCT-HOLD-CDE := #CNTRCT-HOLD-CDE
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde().notEquals("0")))                                                                                 //Natural: IF #CNTRCT-PEND-CDE NE '0'
        {
            if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte(),"YYYYMMDD")))                                                            //Natural: IF #CNTRCT-PEND-DTE = MASK ( YYYYMMDD )
            {
                pnd_Pend_Dte.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte().getSubstring(1,6));                                                              //Natural: ASSIGN #PEND-DTE := SUBSTR ( #CNTRCT-PEND-DTE,1,6 )
                ldaIaal999f.getCpr_Cntrct_Pend_Dte().compute(new ComputeParameters(false, ldaIaal999f.getCpr_Cntrct_Pend_Dte()), pnd_Pend_Dte.val());                     //Natural: ASSIGN CPR.CNTRCT-PEND-DTE := VAL ( #PEND-DTE )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                         //Natural: ASSIGN #O-STATUS := #REJECTED
                pnd_Out_File_Pnd_O_Msg.setValue("Invalid Pend Date");                                                                                                     //Natural: ASSIGN #O-MSG := 'Invalid Pend Date'
                pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Cde(), ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Pend_Dte())); //Natural: COMPRESS #O-MSG #CNTRCT-PEND-CDE #CNTRCT-PEND-DTE INTO #O-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getCpr_Cntrct_Curr_Dist_Cde().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Dest_Cde());                                                           //Natural: ASSIGN CPR.CNTRCT-CURR-DIST-CDE := #CNTRCT-DEST-CDE
        ldaIaal999f.getCpr_Rllvr_Cntrct_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Rllvr_Cntrct_Nbr());                                                              //Natural: ASSIGN CPR.RLLVR-CNTRCT-NBR := #RLLVR-CNTRCT-NBR
        if (condition(DbsUtil.maskMatches(ldaIaal760.getPnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte(),"YYYYMMDD")))                                                                //Natural: IF #ROTH-DSBLTY-DTE = MASK ( YYYYMMDD )
        {
            pnd_W_Date.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Roth_Dsblty_Dte());                                                                                      //Natural: ASSIGN #W-DATE := #ROTH-DSBLTY-DTE
            ldaIaal999f.getCpr_Roth_Dsblty_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date);                                                               //Natural: MOVE EDITED #W-DATE TO CPR.ROTH-DSBLTY-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getVw_cpr().insertDBRow();                                                                                                                            //Natural: STORE CPR
    }
    private void sub_Create_Fund_Record() throws Exception                                                                                                                //Natural: CREATE-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                             //Natural: IF #O-MSG NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                 //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := #CNTRCT-NBR
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde()),        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := VAL ( #PAYEE-CDE )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val());
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().reset();                                                                                                     //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT #J IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * ) IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * ) IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( * )
        pnd_J.reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*").reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*").reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*").reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue("*").reset();
        ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue("*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            pnd_Fund_Details.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Variable_Funds().getValue(pnd_I));                                                                 //Natural: ASSIGN #FUND-DETAILS := #VARIABLE-FUNDS ( #I )
            if (condition(pnd_Fund_Details_Pnd_Cref_Fund_Cde.notEquals(" ")))                                                                                             //Natural: IF #CREF-FUND-CDE NE ' '
            {
                pnd_Fd1.setValue(pnd_Fund_Details_Pnd_Cref_Fund_Cde);                                                                                                     //Natural: ASSIGN #FD1 := #CREF-FUND-CDE
                DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue("*")), new ExamineSearch(pnd_Fd1), new ExamineGivingIndex(pnd_F)); //Natural: EXAMINE IAAA051Z.#IA-STD-ALPHA-CD ( * ) FOR #FD1 GIVING INDEX #F
                if (condition(pnd_F.greater(getZero())))                                                                                                                  //Natural: IF #F GT 0
                {
                    pnd_C_Fnd_Pnd_Fund.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_F));                                                              //Natural: ASSIGN #FUND := IAAA051Z.#IA-STD-NM-CD ( #F )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue("Invalid fund code");                                                                                                 //Natural: ASSIGN #O-MSG := 'Invalid fund code'
                    pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, pnd_Fund_Details_Pnd_Cref_Fund_Cde));                                        //Natural: COMPRESS #O-MSG #CREF-FUND-CDE INTO #O-MSG
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Details_Pnd_Cref_Sttlmnt_Amt.equals(getZero())))                                                                                   //Natural: IF #CREF-STTLMNT-AMT = 0
                {
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue("Settlement amount cannot be zero:");                                                                                 //Natural: ASSIGN #O-MSG := 'Settlement amount cannot be zero:'
                    pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), pnd_Fund_Details_Pnd_Cref_Fund_Cde)); //Natural: COMPRESS #O-MSG #CNTRCT-NBR #CREF-FUND-CDE INTO #O-MSG
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Details_Pnd_Cref_Units_Cnt.equals(getZero())))                                                                                     //Natural: IF #CREF-UNITS-CNT = 0
                {
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue("Units count cannot be zero:");                                                                                       //Natural: ASSIGN #O-MSG := 'Units count cannot be zero:'
                    pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), pnd_Fund_Details_Pnd_Cref_Fund_Cde)); //Natural: COMPRESS #O-MSG #CNTRCT-NBR #CREF-FUND-CDE INTO #O-MSG
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A")))                                                                                           //Natural: IF #VALUATION-MTHD = 'A'
                {
                    if (condition(pnd_Fund_Details_Pnd_Cref_Pmt_Amt.equals(getZero())))                                                                                   //Natural: IF #CREF-PMT-AMT = 0
                    {
                        pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                 //Natural: ASSIGN #O-STATUS := #REJECTED
                        pnd_Out_File_Pnd_O_Msg.setValue("Payment amount cannot be zero:");                                                                                //Natural: ASSIGN #O-MSG := 'Payment amount cannot be zero:'
                        pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), pnd_Fund_Details_Pnd_Cref_Fund_Cde)); //Natural: COMPRESS #O-MSG #CNTRCT-NBR #CREF-FUND-CDE INTO #O-MSG
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A") || pnd_Fund_Details_Pnd_Valuation_Mthd.equals("M")))                                        //Natural: IF #VALUATION-MTHD = 'A' OR = 'M'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue("Invalid valuation method");                                                                                          //Natural: ASSIGN #O-MSG := 'Invalid valuation method'
                    pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(pnd_Out_File_Pnd_O_Msg, pnd_Fund_Details_Pnd_Valuation_Mthd, "for variable fund:",                   //Natural: COMPRESS #O-MSG #VALUATION-MTHD 'for variable fund:' #CNTRCT-NBR #CREF-FUND-CDE INTO #O-MSG
                        ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), pnd_Fund_Details_Pnd_Cref_Fund_Cde));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A") || pnd_Fund_Details_Pnd_Valuation_Mthd.equals("M")))                                        //Natural: IF #VALUATION-MTHD = 'A' OR = 'M'
                {
                    if (condition(pnd_C_Fnd_Pnd_Fund.equals("09") || pnd_C_Fnd_Pnd_Fund.equals("11")))                                                                    //Natural: IF #FUND = '09' OR = '11'
                    {
                        if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A")))                                                                                   //Natural: IF #VALUATION-MTHD = 'A'
                        {
                            pnd_C_Fnd_Pnd_Cmpny.setValue("U");                                                                                                            //Natural: ASSIGN #CMPNY := 'U'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_C_Fnd_Pnd_Cmpny.setValue("W");                                                                                                            //Natural: ASSIGN #CMPNY := 'W'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A")))                                                                                   //Natural: IF #VALUATION-MTHD = 'A'
                        {
                            pnd_C_Fnd_Pnd_Cmpny.setValue("2");                                                                                                            //Natural: ASSIGN #CMPNY := '2'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_C_Fnd_Pnd_Cmpny.setValue("4");                                                                                                            //Natural: ASSIGN #CMPNY := '4'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Cntrct.setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                         //Natural: ASSIGN #FUND-CNTRCT := #CNTRCT-NBR
                pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee.compute(new ComputeParameters(false, pnd_Tiaa_Cntrct_Fund_Key_Pnd_Fund_Payee), ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val()); //Natural: ASSIGN #FUND-PAYEE := VAL ( #PAYEE-CDE )
                pnd_Tiaa_Cntrct_Fund_Key_Pnd_Cmpny_Fund.setValue(pnd_C_Fnd);                                                                                              //Natural: ASSIGN #CMPNY-FUND := #C-FND
                vw_tiaa_Fund.startDatabaseFind                                                                                                                            //Natural: FIND TIAA-FUND WITH TIAA-CNTRCT-FUND-KEY = #TIAA-CNTRCT-FUND-KEY
                (
                "FIND03",
                new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", pnd_Tiaa_Cntrct_Fund_Key, WcType.WITH) }
                );
                FIND03:
                while (condition(vw_tiaa_Fund.readNextRow("FIND03")))
                {
                    vw_tiaa_Fund.setIfNotFoundControlFlag(false);
                    pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                     //Natural: ASSIGN #O-STATUS := #REJECTED
                    pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress("Duplicate fund:", pnd_Fund_Details_Pnd_Valuation_Mthd, pnd_Fund_Details_Pnd_Cref_Fund_Cde));        //Natural: COMPRESS 'Duplicate fund:' #VALUATION-MTHD #CREF-FUND-CDE INTO #O-MSG
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Price_Div_Pnd_U_Val.reset();                                                                                                                          //Natural: RESET #U-VAL
                if (condition(pnd_C_Fnd_Pnd_Cmpny.equals("U") || pnd_C_Fnd_Pnd_Cmpny.equals("2")))                                                                        //Natural: IF #CMPNY = 'U' OR = '2'
                {
                    pnd_Price_Div_Pnd_U_Val.setValue(pnd_Fund_Details_Pnd_Cref_Unit_Val);                                                                                 //Natural: ASSIGN #U-VAL := #CREF-UNIT-VAL
                }                                                                                                                                                         //Natural: END-IF
                ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue(pnd_C_Fnd);                                                                              //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := #C-FND
                ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().setValue(pnd_Fund_Details_Pnd_Cref_Pmt_Amt);                                                         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #CREF-PMT-AMT
                ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().setValue(pnd_Price_Div);                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := #PRICE-DIV
                ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1).setValue(pnd_Fund_Details_Pnd_Cref_Rate_Cde);                                               //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1 ) := #CREF-RATE-CDE
                ldaIaal999f.getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt().getValue(1).setValue(pnd_Fund_Details_Pnd_Cref_Units_Cnt);                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 ) := #CREF-UNITS-CNT
                ldaIaal999f.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                     //Natural: STORE IAA-TIAA-FUND-RCRD
                pnd_Temp_Fnd_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TEMP-FND-CNT
                if (condition(pnd_Fund_Details_Pnd_Valuation_Mthd.equals("A")))                                                                                           //Natural: IF #VALUATION-MTHD = 'A'
                {
                    pnd_Temp_Annl_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TEMP-ANNL-CNT
                    pnd_Temp_Tot_Annl_Pmts.nadd(pnd_Fund_Details_Pnd_Cref_Pmt_Amt);                                                                                       //Natural: ADD #CREF-PMT-AMT TO #TEMP-TOT-ANNL-PMTS
                    pnd_Temp_Tot_Annl_Units.nadd(pnd_Fund_Details_Pnd_Cref_Units_Cnt);                                                                                    //Natural: ADD #CREF-UNITS-CNT TO #TEMP-TOT-ANNL-UNITS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Temp_Mnth_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TEMP-MNTH-CNT
                    pnd_Temp_Tot_Mnth_Pmts.nadd(pnd_Fund_Details_Pnd_Cref_Pmt_Amt);                                                                                       //Natural: ADD #CREF-PMT-AMT TO #TEMP-TOT-MNTH-PMTS
                    pnd_Temp_Tot_Mnth_Units.nadd(pnd_Fund_Details_Pnd_Cref_Units_Cnt);                                                                                    //Natural: ADD #CREF-UNITS-CNT TO #TEMP-TOT-MNTH-UNITS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Per_Pmt_Amt.nadd(pnd_Fund_Details_Pnd_Cref_Pmt_Amt);                                                                                                  //Natural: ADD #CREF-PMT-AMT TO #PER-PMT-AMT
                pnd_Per_Units_Cnt.nadd(pnd_Fund_Details_Pnd_Cref_Units_Cnt);                                                                                              //Natural: ADD #CREF-UNITS-CNT TO #PER-UNITS-CNT
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                pnd_Save_Data_Pnd_Frm_Fnd_Tckr.getValue(pnd_J).setValue(pnd_Fund_Details_Pnd_Frm_Omni_Tckr);                                                              //Natural: ASSIGN #FRM-FND-TCKR ( #J ) := #FRM-OMNI-TCKR
                if (condition(DbsUtil.maskMatches(pnd_Fund_Details_Pnd_Frm_Omni_Tckr,"'TR'") || DbsUtil.maskMatches(pnd_Fund_Details_Pnd_Frm_Omni_Tckr,                   //Natural: IF #FRM-OMNI-TCKR = MASK ( 'TR' ) OR = MASK ( 'W' )
                    "'W'")))
                {
                    pnd_Save_Data_Pnd_To_Fnd_Tckr.getValue(pnd_J).setValue(pnd_Fund_Details_Pnd_Frm_Omni_Tckr);                                                           //Natural: ASSIGN #TO-FND-TCKR ( #J ) := #FRM-OMNI-TCKR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.callnat(Iaan760.class , getCurrentProcessState(), pnd_Fund_Details_Pnd_To_Cref_Fnd_Tckr, pnd_O_Shrt_Nme, pnd_Rc, pnd_Rc_Msg);                 //Natural: CALLNAT 'IAAN760' #TO-CREF-FND-TCKR #O-SHRT-NME #RC #RC-MSG
                    if (condition(Global.isEscape())) return;
                    if (condition(pnd_Rc.equals(getZero())))                                                                                                              //Natural: IF #RC = 0
                    {
                        pnd_Save_Data_Pnd_To_Fnd_Tckr.getValue(pnd_J).setValue(pnd_O_Shrt_Nme);                                                                           //Natural: ASSIGN #TO-FND-TCKR ( #J ) := #O-SHRT-NME
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                 //Natural: ASSIGN #O-STATUS := #REJECTED
                        pnd_Out_File_Pnd_O_Msg.setValue(DbsUtil.compress(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(), pnd_Rc_Msg));                                     //Natural: COMPRESS #CNTRCT-NBR #RC-MSG INTO #O-MSG
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_Data_Pnd_Rvl_Mthd.getValue(pnd_J).setValue(pnd_Fund_Details_Pnd_Valuation_Mthd);                                                                 //Natural: ASSIGN #RVL-MTHD ( #J ) := #VALUATION-MTHD
                pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt.getValue(pnd_J).setValue(pnd_Fund_Details_Pnd_Cref_Sttlmnt_Amt);                                                        //Natural: ASSIGN #FND-STTLMNT-AMT ( #J ) := #CREF-STTLMNT-AMT
                pnd_Save_Data_Pnd_Inv_Shrt_Nme.getValue(pnd_J).setValue(pnd_Fund_Details_Pnd_Omni_Fnd_Shrt_Nme);                                                          //Natural: ASSIGN #INV-SHRT-NME ( #J ) := #OMNI-FND-SHRT-NME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Create_Trans_Record() throws Exception                                                                                                               //Natural: CREATE-TRANS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                             //Natural: IF #O-MSG NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                                    //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
        if (condition(pnd_Invrse_Dte.equals(pnd_S_Invrse_Dte)))                                                                                                           //Natural: IF #INVRSE-DTE = #S-INVRSE-DTE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                 //Natural: ASSIGN #TRANS-DTE := *TIMX
                pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                            //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
                if (condition(pnd_Invrse_Dte.notEquals(pnd_S_Invrse_Dte)))                                                                                                //Natural: IF #INVRSE-DTE NE #S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Invrse_Dte.setValue(pnd_Invrse_Dte);                                                                                                                        //Natural: ASSIGN #S-INVRSE-DTE := #INVRSE-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Invrse_Dte);                                                                                        //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(ldaIaal999f.getIaa_Trans_Rcrd_Trans_Dte());                                                                //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #CNTRCT-NBR
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Payee_Cde().compute(new ComputeParameters(false, ldaIaal999f.getIaa_Trans_Rcrd_Trans_Payee_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val()); //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := VAL ( #PAYEE-CDE )
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Check_Dte);                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #CHECK-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Todays_Dte);                                                                                        //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DTE
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_User_Area().setValue("BATCH");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_User_Id().setValue("PIA2250D");                                                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'PIA2250D'
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Cde().setValue(33);                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 033
        ldaIaal999f.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue("ACC");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := 'ACC'
        //* *END-IF
        ldaIaal999f.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                 //Natural: STORE IAA-TRANS-RCRD
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt().greater(getZero())))                                                                              //Natural: IF #CNTRCT-IVC-AMT GT 0
        {
            if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code().equals("NI")))                                                                                  //Natural: IF #PROCESS-CODE = 'NI'
            {
                ldaIaal999f.getIaa_Trans_Rcrd_Trans_Cde().setValue(724);                                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 724
                ldaIaal999f.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                         //Natural: STORE IAA-TRANS-RCRD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE AFTER IMAGES
        DbsUtil.callnat(Iaan060h.class , getCurrentProcessState(), ldaIaal999f.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr(), ldaIaal999f.getIaa_Trans_Rcrd_Trans_Payee_Cde(),       //Natural: CALLNAT 'IAAN060H' IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE #INVRSE-DTE #TRANS-DTE #CHECK-DTE
            pnd_Invrse_Dte, pnd_Trans_Dte, pnd_Check_Dte);
        if (condition(Global.isEscape())) return;
        pnd_Cntrct_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #CNTRCT-CNT
        pnd_Cpr_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CPR-CNT
        pnd_Fnd_Cnt.nadd(pnd_Temp_Fnd_Cnt);                                                                                                                               //Natural: ADD #TEMP-FND-CNT TO #FND-CNT
        pnd_Tot_Per_Amt.nadd(pnd_Temp_Tot_Per_Amt);                                                                                                                       //Natural: ADD #TEMP-TOT-PER-AMT TO #TOT-PER-AMT
        pnd_Tot_Div_Amt.nadd(pnd_Temp_Tot_Div_Amt);                                                                                                                       //Natural: ADD #TEMP-TOT-DIV-AMT TO #TOT-DIV-AMT
        pnd_Annl_Cnt.nadd(pnd_Temp_Annl_Cnt);                                                                                                                             //Natural: ADD #TEMP-ANNL-CNT TO #ANNL-CNT
        pnd_Tot_Annl_Pmts.nadd(pnd_Temp_Tot_Annl_Pmts);                                                                                                                   //Natural: ADD #TEMP-TOT-ANNL-PMTS TO #TOT-ANNL-PMTS
        pnd_Tot_Annl_Units.nadd(pnd_Temp_Tot_Annl_Units);                                                                                                                 //Natural: ADD #TEMP-TOT-ANNL-UNITS TO #TOT-ANNL-UNITS
        pnd_Mnth_Cnt.nadd(pnd_Temp_Mnth_Cnt);                                                                                                                             //Natural: ADD #TEMP-MNTH-CNT TO #MNTH-CNT
        pnd_Tot_Mnth_Pmts.nadd(pnd_Temp_Tot_Mnth_Pmts);                                                                                                                   //Natural: ADD #TEMP-TOT-MNTH-PMTS TO #TOT-MNTH-PMTS
        pnd_Tot_Mnth_Units.nadd(pnd_Temp_Tot_Mnth_Units);                                                                                                                 //Natural: ADD #TEMP-TOT-MNTH-UNITS TO #TOT-MNTH-UNITS
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                             //Natural: IF #O-MSG NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "Orchstrtn/Id",                                                                                                                           //Natural: DISPLAY ( 1 ) 'Orchstrtn/Id' #ORCHSTRTN-ID 'Pro/Cde' #PROCESS-CODE '/PIN' #CPR-ID-NBR '/Contract' #CNTRCT-NBR '/Py' #PAYEE-CDE '/1st SSN' #PRTCPNT-TAX-ID-NBR '/2nd SSN' #2ND-ANNT-SSN '/1st DOB' #1ST-ANNT-DOB '/2nd DOB' #2ND-ANNT-DOB 'Matching/Contract' #MTCHNG-CNTRCT-NBR 'Orig TIAA/DA Nbr'#ORIG-TIAA-DA-NBR 'Orig CREF/DA Nbr'#ORIG-CREF-DA-NBR 'Issue/Date' #ISSUE-DTE 'Iss/St' #ISSUE-STTE 'Res/Cde' #PRTCPNT-RSDNCY-CDE '/Optn' #OPTN-CDE '/Orgn' #ORGN-CDE '/Mode' #CNTRCT-MODE-IND '/Plan' #PLN-NBR 'Sub/Plan' #SUB-PLN-NBR 'Fr/LOB' #FROM-LOB 'To/LOB' #ESP-DATA.#TO-LOB 'To/SUB LOB' #ESP-DATA.#TO-SUB-LOB 'Final Per/Pay Date' #FINL-PER-PAY-DTE 'Cntrct/IVC Amt' #CNTRCT-IVC-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'Cntrct/Per IVC Amt' #CNTRCT-PER-IVC-AMT ( EM = ZZZ,ZZ9.99 ) '/Per Amt' #PER-PMT-AMT ( EM = ZZZ,ZZ9.99 ) '/Per Units' #PER-UNITS-CNT ( EM = ZZZZZ9.999 )
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orchstrtn_Id(),"Pro/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code(),"/PIN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr(),"/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(),"/Py",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde(),"/1st SSN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr(),"/2nd SSN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Ssn(),"/1st DOB",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Dob(),"/2nd DOB",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob(),"Matching/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr(),"Orig TIAA/DA Nbr",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr(),"Orig CREF/DA Nbr",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr(),"Issue/Date",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte(),"Iss/St",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Stte(),"Res/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde(),"/Optn",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde(),"/Orgn",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde(),"/Mode",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind(),"/Plan",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Pln_Nbr(),"Sub/Plan",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr(),"Fr/LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_From_Lob(),"To/LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_To_Lob(),"To/SUB LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_To_Sub_Lob(),"Final Per/Pay Date",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte(),"Cntrct/IVC Amt",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Cntrct/Per IVC Amt",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"/Per Amt",
        		pnd_Per_Pmt_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"/Per Units",
        		pnd_Per_Units_Cnt, new ReportEditMask ("ZZZZZ9.999"));
        if (Global.isEscape()) return;
    }
    private void sub_Edit_Issue_Date_Mode() throws Exception                                                                                                              //Natural: EDIT-ISSUE-DATE-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MONTHLY
        if (condition(pnd_Mode_Wrk_Pnd_Mode_Pos1.equals(1)))                                                                                                              //Natural: IF #MODE-POS1 = 1
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  QUARTERLY
        if (condition(pnd_Mode_Wrk_Pnd_Mode_Pos1.equals(6)))                                                                                                              //Natural: IF #MODE-POS1 = 6
        {
            if (condition(pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2) || pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2.add(3)) || pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2.add(6))  //Natural: IF #W-MM = #MODE-POS2 OR = #MODE-POS2 + 3 OR = #MODE-POS2 + 6 OR = #MODE-POS2 + 9
                || pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2.add(9))))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  SEMI-ANNUALLY
        if (condition(pnd_Mode_Wrk_Pnd_Mode_Pos1.equals(7)))                                                                                                              //Natural: IF #MODE-POS1 = 7
        {
            if (condition(pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2) || pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2.add(6))))                       //Natural: IF #W-MM = #MODE-POS2 OR = #MODE-POS2 + 6
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUALLY
        if (condition(pnd_Mode_Wrk_Pnd_Mode_Pos1.equals(8)))                                                                                                              //Natural: IF #MODE-POS1 = 8
        {
            if (condition(pnd_W_Date_Pnd_W_Mm.equals(pnd_Mode_Wrk_Pnd_Mode_Pos2)))                                                                                        //Natural: IF #W-MM = #MODE-POS2
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                                 //Natural: ASSIGN #O-STATUS := #REJECTED
        pnd_Out_File_Pnd_O_Msg.setValue("Error in Issue Date Mode combination");                                                                                          //Natural: ASSIGN #O-MSG := 'Error in Issue Date Mode combination'
    }
    private void sub_Create_Esp_Trans_File() throws Exception                                                                                                             //Natural: CREATE-ESP-TRANS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM GET-SUB-LOB
        sub_Get_Sub_Lob();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Out_File_Pnd_O_Msg.notEquals(" ")))                                                                                                             //Natural: IF #O-MSG NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal760b.getPnd_Esp_Data().setValuesByName(ldaIaal760.getPnd_In_Opt_Out());                                                                                    //Natural: MOVE BY NAME #IN-OPT-OUT TO #ESP-DATA
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                   //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := #CNTRCT-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Part_Payee_Cde()),            //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := VAL ( #PAYEE-CDE )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde().val());
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Optn_Cde().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Optn_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde().val()); //Natural: ASSIGN #CNTRCT-OPTN-CDE := VAL ( #OPTN-CDE )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Orgn_Cde().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Orgn_Cde()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde().val()); //Natural: ASSIGN #CNTRCT-ORGN-CDE := VAL ( #ORGN-CDE )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Issue_Dte().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Issue_Dte()), ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte().val()); //Natural: ASSIGN #CNTRCT-ISSUE-DTE := VAL ( #ISSUE-DTE )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Actvty_Cde().setValue(1);                                                                                                  //Natural: ASSIGN #CNTRCT-ACTVTY-CDE := 1
        if (condition(DbsUtil.is(ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte().getText(),"N8")))                                                                     //Natural: IF #1ST-PMT-DUE-DTE IS ( N8 )
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Pymnt_Due_Dte()),                //Natural: ASSIGN #1ST-PYMNT-DUE-DTE := VAL ( #1ST-PMT-DUE-DTE )
                ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Pmt_Due_Dte().val());
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_Cntrct_Crrncy_Cde()),                    //Natural: ASSIGN #CNTRCT-CRRNCY-CDE := VAL ( #CRRNCY-CDE )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_Crrncy_Cde().val());
        ldaIaal760b.getPnd_Esp_Data_Pnd_Orig_Cref_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr());                                                    //Natural: ASSIGN #ORIG-CREF-NBR := #ORIG-CREF-DA-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_Orig_Tiaa_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr());                                                    //Natural: ASSIGN #ORIG-TIAA-NBR := #ORIG-TIAA-DA-NBR
        //*  112017 - START
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Cmpny_Cde().equals("T")))                                                                                   //Natural: IF #CNTRCT-CMPNY-CDE = 'T'
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                       //Natural: ASSIGN #IAA-TIAA-NBR := #CNTRCT-NBR
            ldaIaal760b.getPnd_Esp_Data_Pnd_Iaa_Cref_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr());                                                //Natural: ASSIGN #IAA-CREF-NBR := #MTCHNG-CNTRCT-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Iaa_Cref_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                       //Natural: ASSIGN #IAA-CREF-NBR := #CNTRCT-NBR
            ldaIaal760b.getPnd_Esp_Data_Pnd_Iaa_Tiaa_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr());                                                //Natural: ASSIGN #IAA-TIAA-NBR := #MTCHNG-CNTRCT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Ssn().setValue(ldaIaal999f.getCpr_Prtcpnt_Tax_Id_Nbr());                                                                 //Natural: ASSIGN #1ST-ANNT-SSN := CPR.PRTCPNT-TAX-ID-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_Cpr_Pin_Nbr().setValue(ldaIaal999f.getCpr_Cpr_Id_Nbr());                                                                          //Natural: ASSIGN #CPR-PIN-NBR := CPR.CPR-ID-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Dob_Dte().setValue(ldaIaal999f.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte());                                               //Natural: ASSIGN #1ST-ANNT-DOB-DTE := IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Sex_Code().compute(new ComputeParameters(false, ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Sex_Code()),                    //Natural: ASSIGN #1ST-ANNT-SEX-CODE := VAL ( #1ST-ANNT-SEX-CDE )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Sex_Cde().val());
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Fname().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Frst_Annt_Frst_Nme());                                                 //Natural: ASSIGN #1ST-ANNT-FNAME := #FRST-ANNT-FRST-NME
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Mname().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Frst_Annt_Mddle_Nme());                                                //Natural: ASSIGN #1ST-ANNT-MNAME := #FRST-ANNT-MDDLE-NME
        ldaIaal760b.getPnd_Esp_Data_Pnd_1st_Annt_Lname().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Frst_Annt_Lst_Nme());                                                  //Natural: ASSIGN #1ST-ANNT-LNAME := #FRST-ANNT-LST-NME
        ldaIaal760b.getPnd_Esp_Data_Pnd_Omni_Plan().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Pln_Nbr());                                                                 //Natural: ASSIGN #OMNI-PLAN := #PLN-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_Omni_Sub_Plan().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr());                                                         //Natural: ASSIGN #OMNI-SUB-PLAN := #SUB-PLN-NBR
        ldaIaal760b.getPnd_Esp_Data_Pnd_From_Lob().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr().getSubstring(1,3));                                            //Natural: ASSIGN #FROM-LOB := SUBSTR ( #SUB-PLN-NBR,1,3 )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Effective_Date().setValue(pnd_Todays_Dte);                                                                                        //Natural: ASSIGN #EFFECTIVE-DATE := #TODAYS-DTE
        ldaIaal760b.getPnd_Esp_Data_Pnd_Issue_State().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Stte().getSubstring(2,2));                                          //Natural: ASSIGN #ISSUE-STATE := SUBSTR ( #ISSUE-STTE,2,2 )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Res_State().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde().getSubstring(2,2));                                    //Natural: ASSIGN #RES-STATE := SUBSTR ( #PRTCPNT-RSDNCY-CDE,2,2 )
        ldaIaal760b.getPnd_Esp_Data_Pnd_Pymnt_Frequency().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind());                                                   //Natural: ASSIGN #PYMNT-FREQUENCY := #CNTRCT-MODE-IND
        ldaIaal760b.getPnd_Esp_Data_Pnd_Acct_Type().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Sttlmnt_Ind());                                                             //Natural: ASSIGN #ACCT-TYPE := #STTLMNT-IND
        if (condition(ldaIaal760b.getPnd_Esp_Data_Pnd_Acct_Type().equals("1") || ldaIaal760b.getPnd_Esp_Data_Pnd_Acct_Type().equals("2") || ldaIaal760b.getPnd_Esp_Data_Pnd_Acct_Type().equals("3"))) //Natural: IF #ACCT-TYPE = '1' OR = '2' OR = '3'
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Acct_Type().setValue("S");                                                                                                    //Natural: ASSIGN #ACCT-TYPE := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code().equals("NI")))                                                                                      //Natural: IF #PROCESS-CODE = 'NI'
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Reversal_Code().setValue("N");                                                                                                //Natural: ASSIGN #REVERSAL-CODE := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Reversal_Code().setValue("Y");                                                                                                //Natural: ASSIGN #REVERSAL-CODE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #J
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_J)); pnd_I.nadd(1))
        {
            ldaIaal760b.getPnd_Esp_Data_Pnd_Reval_Method().setValue(pnd_Save_Data_Pnd_Rvl_Mthd.getValue(pnd_I));                                                          //Natural: ASSIGN #REVAL-METHOD := #RVL-MTHD ( #I )
            ldaIaal760b.getPnd_Esp_Data_Pnd_Inv_Short_Name().setValue(pnd_Save_Data_Pnd_Inv_Shrt_Nme.getValue(pnd_I));                                                    //Natural: ASSIGN #INV-SHORT-NAME := #INV-SHRT-NME ( #I )
            ldaIaal760b.getPnd_Esp_Data_Pnd_From_Fund_Ticker().setValue(pnd_Save_Data_Pnd_Frm_Fnd_Tckr.getValue(pnd_I));                                                  //Natural: ASSIGN #FROM-FUND-TICKER := #FRM-FND-TCKR ( #I )
            ldaIaal760b.getPnd_Esp_Data_Pnd_To_Fund_Ticker().setValue(pnd_Save_Data_Pnd_To_Fnd_Tckr.getValue(pnd_I));                                                     //Natural: ASSIGN #TO-FUND-TICKER := #TO-FND-TCKR ( #I )
            ldaIaal760b.getPnd_Esp_Data_Pnd_Fund_Sttlmnt_Amt().setValueEdited(pnd_Save_Data_Pnd_Fnd_Sttlmnt_Amt.getValue(pnd_I),new ReportEditMask("ZZZZZZZZ9.99"));      //Natural: MOVE EDITED #FND-STTLMNT-AMT ( #I ) ( EM = ZZZZZZZZ9.99 ) TO #FUND-STTLMNT-AMT
            getWorkFiles().write(3, false, ldaIaal760b.getPnd_Esp_Data());                                                                                                //Natural: WRITE WORK FILE 3 #ESP-DATA
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Short_Name() throws Exception                                                                                                                    //Natural: GET-SHORT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan760.class , getCurrentProcessState(), pnd_Fund_Details_Pnd_Frm_Omni_Tckr, pnd_O_Shrt_Nme, pnd_Rc, pnd_Rc_Msg);                                //Natural: CALLNAT 'IAAN760' #FRM-OMNI-TCKR #O-SHRT-NME #RC #RC-MSG
        if (condition(Global.isEscape())) return;
    }
    //*  GET PRODUCT/CONTRACT TYPE
    //*  BY ACCT
    private void sub_Get_Sub_Lob() throws Exception                                                                                                                       //Natural: GET-SUB-LOB
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr());                                                              //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #CNTRCT-NBR
        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                             //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
        {
            ldaIaal760.getPnd_In_Opt_Out_Pnd_To_Lob().setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                                    //Natural: ASSIGN #IN-OPT-OUT.#TO-LOB := PRD-PRODUCT-CDE ( 1 )
            ldaIaal760.getPnd_In_Opt_Out_Pnd_To_Sub_Lob().setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                                            //Natural: ASSIGN #IN-OPT-OUT.#TO-SUB-LOB := PRD-SUB-PRODUCT-CDE ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Out_File_Pnd_O_Status.setValue(pnd_Rejected);                                                                                                             //Natural: ASSIGN #O-STATUS := #REJECTED
            pnd_Out_File_Pnd_O_Msg.setValue("LOB/SUB-LOB error");                                                                                                         //Natural: ASSIGN #O-MSG := 'LOB/SUB-LOB error'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "Orchstrtn/Id",                                                                                                                           //Natural: DISPLAY ( 2 ) 'Orchstrtn/Id' #ORCHSTRTN-ID 'Pro/Cde' #PROCESS-CODE '/PIN' #CPR-ID-NBR '/Contract' #CNTRCT-NBR '/Py' #PAYEE-CDE '/Message' #O-MSG ( AL = 80 )
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orchstrtn_Id(),"Pro/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code(),"/PIN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr(),"/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(),"/Py",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde(),"/Message",
        		pnd_Out_File_Pnd_O_Msg, new AlphanumericLength (80));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(40),"IA CONTRACT LOAD REPORT",new                 //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 40X 'IA CONTRACT LOAD REPORT' 40X / *DATX ( EM = MM/DD/YYYY ) 106X 'Page' *PAGE-NUMBER ( 1 )
                        ColumnSpacing(40),NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(106),"Page",getReports().getPageNumberDbs(1));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(38),"IA CONTRACT LOAD ERROR REPORT",new           //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM 38X 'IA CONTRACT LOAD ERROR REPORT' 40X / *DATX ( EM = MM/DD/YYYY ) 103X 'Page' *PAGE-NUMBER ( 2 )
                        ColumnSpacing(40),NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(103),"Page",getReports().getPageNumberDbs(2));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=250");
        Global.format(2, "PS=0 LS=250");

        getReports().setDisplayColumns(1, "Orchstrtn/Id",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orchstrtn_Id(),"Pro/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code(),"/PIN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr(),"/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(),"/Py",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde(),"/1st SSN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Tax_Id_Nbr(),"/2nd SSN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Ssn(),"/1st DOB",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_1st_Annt_Dob(),"/2nd DOB",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_2nd_Annt_Dob(),"Matching/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Mtchng_Cntrct_Nbr(),"Orig TIAA/DA Nbr",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Tiaa_Da_Nbr(),"Orig CREF/DA Nbr",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orig_Cref_Da_Nbr(),"Issue/Date",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Dte(),"Iss/St",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Issue_Stte(),"Res/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Prtcpnt_Rsdncy_Cde(),"/Optn",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Optn_Cde(),"/Orgn",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orgn_Cde(),"/Mode",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Mode_Ind(),"/Plan",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Pln_Nbr(),"Sub/Plan",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Sub_Pln_Nbr(),"Fr/LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_From_Lob(),"To/LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_To_Lob(),"To/SUB LOB",
        		ldaIaal760b.getPnd_Esp_Data_Pnd_To_Sub_Lob(),"Final Per/Pay Date",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Finl_Per_Pay_Dte(),"Cntrct/IVC Amt",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Ivc_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Cntrct/Per IVC Amt",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Per_Ivc_Amt(), new ReportEditMask ("ZZZ,ZZ9.99"),"/Per Amt",
        		pnd_Per_Pmt_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),"/Per Units",
        		pnd_Per_Units_Cnt, new ReportEditMask ("ZZZZZ9.999"));
        getReports().setDisplayColumns(2, "Orchstrtn/Id",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Orchstrtn_Id(),"Pro/Cde",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Process_Code(),"/PIN",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cpr_Id_Nbr(),"/Contract",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Cntrct_Nbr(),"/Py",
        		ldaIaal760.getPnd_In_Opt_Out_Pnd_Payee_Cde(),"/Message",
        		pnd_Out_File_Pnd_O_Msg, new AlphanumericLength (80));
    }
}
