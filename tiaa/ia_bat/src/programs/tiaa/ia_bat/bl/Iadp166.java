/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:07 PM
**        * FROM NATURAL PROGRAM : Iadp166
************************************************************
**        * FILE NAME            : Iadp166.java
**        * CLASS NAME           : Iadp166
**        * INSTANCE NAME        : Iadp166
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IADP166    READS IAIQ EXTRACT FLAT FILE           *
*      DATE     -  4/01       & CREATES OLD IA MASTER RECORD FORMAT  *
*                             LEVEL 10, 20, 50 70 RECORDS            *
*                             READS IAIQ FUND FILE FOR RATE INFO     *
*                             INPUT TO QTRLY PROCESSING              *
*                                                                    *
*                             READ IAIQ TRANSACTION EXTRACT          *
*                             CREATED BY (IADP165) & MATCH           *
*                             TO EXTRACTED MASTER RECORDS TO         *
*                             DETERMINE NEW-ISSUE CONTRACTS          *
*   HISTORY                                                          *
*                                                                    *
*   DO SCAN ON 7/01 FOR CODE ADDED TO SET NEWISSU FLAG               *
*              THIS CODE CAN BE REMOVED AFTER CONVERSION DONE        *
*   DO SCAN ON 12/01 COMMNETD MOVE OF FINAL DIVIDEND AMTS            *
*                    NO FINAL DIVIDENDS FOR TPA,IPRO OR P&I          *
*                                                                    *
*   DO SCAN ON 01/02 FOR CHANGES TO CAPTURE CORRECT OLD-DIV & PMT-AMT*
*                    AND CAPTURE ISSUE-DATE DAY FOR ACTUARY CALL     *
*                    IADP170 FOR TPA OPENING VALUE                   *
*                                                                    *
*                    ADDED LOGIC TO INCLUDE RATE-DATE IF PRESENT     *
*                     ON RATE-RECORD FINAL-PREMIUM SWEEP             *
*                     FOR CALL TO ACTUARY FOR OPENING VALUE FOR      *
*                     NEW RATES                                      *
*                     DO SCAN ON 1/02                                *
*                                                                    *
*   DO SCAN ON 04/02 ADDED CHANGES TO PICK UP TRANS 60 (RESETTLEMENT)*
*                    NEW PREMIUMS IN CONTRACTS TO GIVE QTRLY A       *
*                    TRANSACTION TO REFLECT THE NEW MONEY            *
*                    DO SCAN ON 4/02                                 *
*   DO SCAN ON 05/02 INCREASE FIELD SIZE FOR PER-PMT & PER-DIV IN    *
*                    OUTPUT LVL 50 RATE RECORD                       *
*                                                                    *
*                    DO SCAN ON 5/02                                 *
*   DO SCAN ON 04/03 CHANGE TO ACCEPT ALPHA RATES                    *
*                    AND 90 RATES                                    *
*                    DO SCAN ON 4/03                                 *
*   DO SCAN ON 03/05 CHANGE TO MOVE CNTRCT-TYPE INTO OUTPUT MASTER   *
*                    FOR TPA CONVERSION TO DETERMINE FULL WITHDRAWAL *
*                    DO SCAN ON 3/05                                 *
*   DO SCAN ON 08/05 IDENTIFY AND FLAG MANUAL NEW ISSUES.
*   DO SCAN ON 02/06 GET TRANSACTION DATES FOR 40 TRANSACTIONS FROM
*                    TPA/IPRO CONVERSION TO OMNI.
*   DO SCAN ON 03/06 GET TRANSACTION DATES FOR 50 TRANSACTIONS FROM
*                    TPA/IPRO CONVERSION TO OMNI.
*   DO SCAN ON 04/06 GET FUND INFO FROM HISTORY FOR PARTIAL DRAWDOWNS
*                    FOR CONTRACTS DUE FOR PAYMENT ONLY.
*   DO SCAN ON 05/06 IDENTIFY MANUAL FULL DRAWDOWNS USING SUB CDE =
*                    OMN
*   04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp166 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Record_Cde;
    private DbsField pnd_Input_Record_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_W_Check_Date;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Optn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orgin_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Due_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Pd_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill1;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Orig_Da;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issu_St_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill3;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_1st_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Xref;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dob;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fill5;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Div_Payee_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Div_Coll_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde;
    private DbsField pnd_Input_Record_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Type;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Lst_Trans_Dte;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_Filler_3a;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Cash_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Term_Cde;

    private DbsGroup pnd_Input_Record_Pnd_Cpr_Ivc_Info;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Company_Cd;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Rtb_Percent;

    private DbsGroup pnd_Input_Record__R_Field_5;
    private DbsField pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Xref;
    private DbsField pnd_Input_Record_Pnd_Bnfcry_Dod;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Hold_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Pend_Date;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Filler;
    private DbsField pnd_Input_Record_Pnd_Cntrct_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Filler2;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Id_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Cde;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Ytd_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Tot_Amt;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Intent_Code;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Stp_Dte;
    private DbsField pnd_Input_Record_Pnd_Ddctn_Final_Dte;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_7;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_8;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Cde;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Lst_Trans_Dte;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Check_Dte;
    private DbsField trans_Trans_Todays_Dte;
    private DbsField trans_Trans_User_Area;

    private DataAccessProgramView vw_fund_Rcrd;
    private DbsField fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup fund_Rcrd__R_Field_9;
    private DbsField fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField fund_Rcrd_Tiaa_Rate_Cde;

    private DbsGroup fund_Rcrd__R_Field_10;
    private DbsField fund_Rcrd_Tiaa_Rate_Cde_N;
    private DbsField fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField fund_Rcrd_Lst_Trans_Dte;
    private DbsField fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_fund_Trans;
    private DbsField fund_Trans_Bfre_Imge_Id;
    private DbsField fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Trans_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup fund_Trans__R_Field_11;
    private DbsField fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField fund_Trans_Tiaa_Fund_Cde;
    private DbsField fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField fund_Trans_Tiaa_Rate_Cde;

    private DbsGroup fund_Trans__R_Field_12;
    private DbsField fund_Trans_Tiaa_Rate_Cde_N;
    private DbsField fund_Trans_Tiaa_Rate_Dte;
    private DbsField fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField fund_Trans_Tiaa_Units_Cnt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_13;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_14;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2;
    private DbsField pnd_Next_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_15;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_16;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Prev_Check_Date_Ccyymmdd__R_Field_17;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Ccyy;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Dd;

    private DbsGroup pnd_Prev_Check_Date_Ccyymmdd__R_Field_18;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Ccyymm;
    private DbsField pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Dd2;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Due_Mode;

    private DbsGroup pnd_Due_Mode__R_Field_19;
    private DbsField pnd_Due_Mode_Pnd_Fllr;
    private DbsField pnd_Due_Mode_Pnd_Due_Mm;
    private DbsField pnd_Term_Dte;
    private DbsField pnd_Drwdwn_Dte;
    private DbsField pnd_Sve_Mode;
    private DbsField pnd_Master_Work_Rec;

    private DbsGroup pnd_Master_Work_Rec__R_Field_20;
    private DbsField pnd_Master_Work_Rec_Contract_No_M;
    private DbsField pnd_Master_Work_Rec_Payee_M;
    private DbsField pnd_Master_Work_Rec_Rec_Type_M;

    private DbsGroup pnd_Master_Work_Rec_Rec_Type_10;
    private DbsField pnd_Master_Work_Rec_Filler_1;
    private DbsField pnd_Master_Work_Rec_Product_M;
    private DbsField pnd_Master_Work_Rec_Currency_M;
    private DbsField pnd_Master_Work_Rec_Mode_M;
    private DbsField pnd_Master_Work_Rec_Pend_Cde;
    private DbsField pnd_Master_Work_Rec_Hold_Cde;
    private DbsField pnd_Master_Work_Rec_Pend_Date;
    private DbsField pnd_Master_Work_Rec_Option_M;
    private DbsField pnd_Master_Work_Rec_Iss_Origin;
    private DbsField pnd_Master_Work_Rec_Iss_Iss_Date;
    private DbsField pnd_Master_Work_Rec_Iss_1st_Pay_Due_Date;
    private DbsField pnd_Master_Work_Rec_Iss_1st_Pay_Paid_Date;
    private DbsField pnd_Master_Work_Rec_Iss_Final_Per_Pay_Date;
    private DbsField pnd_Master_Work_Rec_Iss_Final_Pay_Date;
    private DbsField pnd_Master_Work_Rec_Iss_Withdraw_Date;
    private DbsField pnd_Master_Work_Rec_Iss_Citizenship;
    private DbsField pnd_Master_Work_Rec_Iss_State_At_Issue_Time;
    private DbsField pnd_Master_Work_Rec_Iss_Current_State;
    private DbsField pnd_Master_Work_Rec_Iss_College_Code;
    private DbsField pnd_Master_Work_Rec_Iss_Rewrite;
    private DbsField pnd_Master_Work_Rec_Iss_Delete;

    private DbsGroup pnd_Master_Work_Rec__R_Field_21;
    private DbsField pnd_Master_Work_Rec_Iss_Delete_R;
    private DbsField pnd_Master_Work_Rec_Iss_Rtb_Amt;
    private DbsField pnd_Master_Work_Rec_Iss_Last_Trans_Date;
    private DbsField pnd_Master_Work_Rec_J_C_Code_M;
    private DbsField pnd_Master_Work_Rec_Iss_Temp_Hold_Code;
    private DbsField pnd_Master_Work_Rec_Iss_Spirt_Code;
    private DbsField pnd_Master_Work_Rec_Iss_Pens_Plan_Cd;
    private DbsField pnd_Master_Work_Rec_Iss_Contr_Type_Cd;
    private DbsField pnd_Master_Work_Rec_Iss_New_Issue_Flag;
    private DbsField pnd_Master_Work_Rec_Iss_Dte_Dd;
    private DbsField pnd_Master_Work_Rec_Iss_Tran_Cde;

    private DbsGroup pnd_Master_Work_Rec__R_Field_22;
    private DbsField pnd_Master_Work_Rec_Filler_4;
    private DbsField pnd_Master_Work_Rec_X_Ref_1_M;
    private DbsField pnd_Master_Work_Rec_Sex_1_M;
    private DbsField pnd_Master_Work_Rec_Dob_1_M;
    private DbsField pnd_Master_Work_Rec_Dod_1_M;
    private DbsField pnd_Master_Work_Rec_Filler_5;
    private DbsField pnd_Master_Work_Rec_X_Ref_2_M;
    private DbsField pnd_Master_Work_Rec_Sex2_M;
    private DbsField pnd_Master_Work_Rec_Dob_2_M;
    private DbsField pnd_Master_Work_Rec_Dod_2_M;
    private DbsField pnd_Master_Work_Rec_Sec_Mort_Yob_M2;
    private DbsField pnd_Master_Work_Rec_Sec_Life_Cnt_M2;
    private DbsField pnd_Master_Work_Rec_Ben_Cross_M2;
    private DbsField pnd_Master_Work_Rec_Ben_Dod_M2;
    private DbsField pnd_Master_Work_Rec_Da_Contract;
    private DbsField pnd_Master_Work_Rec_Cash_Code_M2;
    private DbsField pnd_Master_Work_Rec_Employ_Code_M2;
    private DbsField pnd_Master_Work_Rec_Prev_Dist_Cde;
    private DbsField pnd_Master_Work_Rec_Curr_Dist_Cde;

    private DbsGroup pnd_Master_Work_Rec__R_Field_23;
    private DbsField pnd_Master_Work_Rec_Rate_Filler;
    private DbsField pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div;

    private DbsGroup pnd_Master_Work_Rec_Lvl_5_Grp;
    private DbsField pnd_Master_Work_Rec_Rate_Rate;
    private DbsField pnd_Master_Work_Rec_Rate_Per_Pay;
    private DbsField pnd_Master_Work_Rec_Rate_Per_Div;
    private DbsField pnd_Master_Work_Rec_Rate_Final_Pay;
    private DbsField pnd_Master_Work_Rec_Rate_Date;
    private DbsField pnd_Master_Work_Rec_Rate_Trans_Code;
    private DbsField pnd_Master_Work_Rec_Rate_Trans_Date;
    private DbsField pnd_Master_Work_Rec_Filler_1a;
    private DbsField pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt;

    private DbsGroup pnd_Master_Work_Rec__R_Field_24;
    private DbsField pnd_Master_Work_Rec_Us_Seq_Nbr;
    private DbsField pnd_Master_Work_Rec_Us_Tax_Auth;
    private DbsField pnd_Master_Work_Rec_Us_Active_Code;
    private DbsField pnd_Master_Work_Rec_Us_Per_Tax_Ded;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Tax_Ded;
    private DbsField pnd_Master_Work_Rec_Us_Recov_Type;
    private DbsField pnd_Master_Work_Rec_Us_Per_Tax_Free_Amt;
    private DbsField pnd_Master_Work_Rec_Us_Res_Tax_Free_Amt;
    private DbsField pnd_Master_Work_Rec_Us_Cont_To_Date_Inv_Used;
    private DbsField pnd_Master_Work_Rec_Us_Invest_In_Contr;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax_Free;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Div_Pay;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Int_Pay;
    private DbsField pnd_Master_Work_Rec_Us_Ytd_Int_Div_Pay;
    private DbsField pnd_Master_Work_Rec_Soc_Sec_No_M;
    private DbsField pnd_Master_Work_Rec_Us_Start_Date;
    private DbsField pnd_Master_Work_Rec_Us_Stop_Date;
    private DbsField pnd_Master_Work_Rec_Us_Sprt_Lump_Sum;
    private DbsField pnd_Master_Work_Rec_Us_Spirt_Arrival_Date;
    private DbsField pnd_Master_Work_Rec_Us_Spirt_Arrival_Source;
    private DbsField pnd_Master_Work_Rec_Us_Spirt_Process_Date;
    private DbsField pnd_Master_Work_Rec_Us_Filler_1b;
    private DbsField pnd_Master_Trl_Rec;

    private DbsGroup pnd_Master_Trl_Rec__R_Field_25;
    private DbsField pnd_Master_Trl_Rec_High_Value;
    private DbsField pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr;
    private DbsField pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr;
    private DbsField pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr;
    private DbsField pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr;
    private DbsField pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr;
    private DbsField pnd_Master_Trl_Rec_Tpa_Per_Amt;
    private DbsField pnd_Master_Trl_Rec_Tpa_Div_Amt;
    private DbsField pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr;
    private DbsField pnd_Master_Trl_Rec_Ipro_Per_Amt;
    private DbsField pnd_Master_Trl_Rec_Ipro_Per_Div;
    private DbsField pnd_Master_Trl_Rec_Ipro_Fin_Pmt;
    private DbsField pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr;
    private DbsField pnd_Master_Trl_Rec_P_I_Per_Amt;
    private DbsField pnd_Master_Trl_Rec_P_I_Per_Div;
    private DbsField pnd_Master_Trl_Rec_P_I_Fin_Pmt;
    private DbsField pnd_Master_Trl_Rec_Inactive_Payees_Mitr;
    private DbsField pnd_W_Hex_High_Value;

    private DbsGroup pnd_Sve_10_Rec_Data;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Opt;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Origin;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dd;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Due_Date;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Paid_Date;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Coll_Code;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_State_At_Issue;
    private DbsField pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type;
    private DbsField pnd_Sve_Delete_Cde;
    private DbsField pnd_Sve_Orig_Da_Nbr;
    private DbsField pnd_Sve_Ssn_Nbr;
    private DbsField pnd_Sve_Cntrct_Payee_Nbr;

    private DbsGroup pnd_Sve_Cntrct_Payee_Nbr__R_Field_26;
    private DbsField pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr;
    private DbsField pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde;
    private DbsField pnd_End_Of_Newissus_File;
    private DbsField pnd_New_Issus_In;
    private DbsField pnd_Matched_New_Issu_Rec_In;

    private DbsGroup pnd_Iadp165_Selct_Trans_In;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr;

    private DbsGroup pnd_Iadp165_Selct_Trans_In__R_Field_27;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Payee_Cde;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Opt_Cde;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Issu_Dte;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cnt;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Data_Grp;

    private DbsGroup pnd_Iadp165_Selct_Trans_In__R_Field_28;

    private DbsGroup pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Dte;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte;

    private DbsGroup pnd_Iadp165_Selct_Trans_In__R_Field_29;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Ccyymm;
    private DbsField pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Dd;
    private DbsField pnd_Inactive_Cntrct;
    private DbsField pnd_Wrte_700_Sw;
    private DbsField pnd_Gen_700_Trn_Sw;
    private DbsField pnd_Gen_006_Trn_Sw;
    private DbsField pnd_Tot_Trans_Out;
    private DbsField pnd_Tot_Tran_10;
    private DbsField pnd_Tot_Tran_20;
    private DbsField pnd_Tot_Tran_50;
    private DbsField pnd_Tot_Tran_70;
    private DbsField pnd_Cntrcts_Not_Selct;
    private DbsField pnd_Fst_Tme;
    private DbsField pnd_Sel_Record;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_Rte_Lvl_Nbr;
    private DbsField pnd_Found_Fnd_Sw;
    private DbsField pnd_Found_Iadp165_Trn_Sw;
    private DbsField pnd_End_Of_Iadp165_File;
    private DbsField pnd_Matched_Iadp165_Rec_In;
    private DbsField pnd_Iadp165_Rec_In;
    private DbsField pnd_Cntrct_Lst_Trans_Dte_A;

    private DbsGroup pnd_Cntrct_Lst_Trans_Dte_A__R_Field_30;
    private DbsField pnd_Cntrct_Lst_Trans_Dte_A_Pnd_Cntrct_Lst_Trans_Dte_N;
    private DbsField pnd_Blank_Rate;
    private DbsField pnd_Have_Cntrct_Rec_Sw;
    private DbsField pnd_Rate_Date_A8;

    private DbsGroup pnd_Rate_Date_A8__R_Field_31;
    private DbsField pnd_Rate_Date_A8_Pnd_Rate_Date_N6;
    private DbsField pnd_Rate_Date_A8_Pnd_Rate_Date_Dd;
    private DbsField pnd_Save_Isn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_Input_Record__R_Field_1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_1", "#CNTRCT-PPCN-NBR-1", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr_9", "#CNTRCT-PPCN-NBR-9", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Record_Pnd_Record_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_Record_Cde", "#RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Rest_Of_Record = pnd_Input_Record.newFieldArrayInGroup("pnd_Input_Record_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            1, new DbsArrayController(1, 315));

        pnd_Input_Record__R_Field_2 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_W_Check_Date = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_W_Check_Date", "#W-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cntrct_Optn_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Optn_Cde", "#CNTRCT-OPTN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Orgin_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orgin_Cde", "#CNTRCT-ORGIN-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte", "#CNTRCT-ISSUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_1st_Due_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Due_Dte", "#CNTRCT-1ST-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_1st_Pd_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Pd_Dte", "#CNTRCT-1ST-PD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde", "#CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_Fill1 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill1", "#CNTRCT-FILL1", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I", 
            "#CNTRCT-JOINT-CNVRT-RCRCD-I", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Orig_Da = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Orig_Da", "#CNTRCT-ORIG-DA", FieldType.STRING, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Issu_St_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issu_St_Cde", "#CNTRCT-ISSU-ST-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_1st_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Xref", "#CNTRCT-1ST-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_1st_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dob", "#CNTRCT-1ST-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Fill3 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill3", "#CNTRCT-FILL3", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex", "#CNTRCT-1ST-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Annt_Life_Cnt", "#CNTRCT-1ST-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_1st_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_1st_Dod", "#CNTRCT-1ST-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Xref = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Xref", "#CNTRCT-2ND-XREF", 
            FieldType.STRING, 9);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dob = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dob", "#CNTRCT-2ND-DOB", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Cntrct_Fill5 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fill5", "#CNTRCT-FILL5", FieldType.STRING, 
            4);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex", "#CNTRCT-2ND-ANNT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Dod = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Dod", "#CNTRCT-2ND-DOD", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Life_Cnt", "#CNTRCT-2ND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Ssn", "#CNTRCT-2ND-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_Cntrct_Div_Payee_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Div_Payee_Cde", "#CNTRCT-DIV-PAYEE-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Div_Coll_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Div_Coll_Cde", "#CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5);
        pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde", "#CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Input_Record_Pnd_Lst_Trans_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Record_Pnd_Cntrct_Type = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Cntrct_Rsdncy_At_Iss_Re = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Rsdncy_At_Iss_Re", "#CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_Fnl_Prm_Dte = pnd_Input_Record__R_Field_3.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Fnl_Prm_Dte", "#CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Mtch_Ppcn = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", 
            FieldType.STRING, 10);
        pnd_Input_Record_Pnd_Cntrct_Annty_Strt_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Annty_Strt_Dte", "#CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE);
        pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Fp_Due_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fp_Due_Dte_Dd", "#CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Fp_Pd_Dte_Dd = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Fp_Pd_Dte_Dd", "#CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", 
            FieldType.DATE);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Cpr_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Record_Pnd_Cntrct_Lst_Trans_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Lst_Trans_Dte", "#CNTRCT-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Ctznshp_Cde", "#PRTCPNT-CTZNSHP-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Cde", "#PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Rsdncy_Sw", "#PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr", "#PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Typ", "#PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Actvty_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Actvty_Cde", "#CNTRCT-ACTVTY-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Filler_3a = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3a", "#FILLER-3A", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde", "#CNTRCT-RWRTTN-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Cash_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Cash_Cde", "#CNTRCT-CASH-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Term_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Term_Cde", "#CNTRCT-TERM-CDE", 
            FieldType.STRING, 1);

        pnd_Input_Record_Pnd_Cpr_Ivc_Info = pnd_Input_Record__R_Field_4.newGroupInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info", "#CPR-IVC-INFO");
        pnd_Input_Record_Pnd_Cntrct_Company_Cd = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Company_Cd", "#CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rcvry_Type_Ind", 
            "#CNTRCT-RCVRY-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Resdl_Ivc_Amt", 
            "#CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Ivc_Used_Amt", 
            "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Amt = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Record_Pnd_Cntrct_Rtb_Percent = pnd_Input_Record_Pnd_Cpr_Ivc_Info.newFieldArrayInGroup("pnd_Input_Record_Pnd_Cntrct_Rtb_Percent", "#CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5));

        pnd_Input_Record__R_Field_5 = pnd_Input_Record__R_Field_4.newGroupInGroup("pnd_Input_Record__R_Field_5", "REDEFINE", pnd_Input_Record_Pnd_Cpr_Ivc_Info);
        pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha", "#CPR-IVC-INFO-ALPHA", 
            FieldType.STRING, 155);
        pnd_Input_Record_Pnd_Cntrct_Mode_Ind = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Mode_Ind", "#CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3);
        pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte", "#CNTRCT-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte", "#CNTRCT-FINAL-PER-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte", "#CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Bnfcry_Xref = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Xref", "#BNFCRY-XREF", FieldType.STRING, 
            9);
        pnd_Input_Record_Pnd_Bnfcry_Dod = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Bnfcry_Dod", "#BNFCRY-DOD", FieldType.NUMERIC, 
            6);
        pnd_Input_Record_Pnd_Cntrct_Pend_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Cde", "#CNTRCT-PEND-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Hold_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Cntrct_Pend_Date = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Pend_Date", "#CNTRCT-PEND-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde", "#CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde", "#CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Cntrct_Filler = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Filler", "#CNTRCT-FILLER", FieldType.STRING, 
            33);
        pnd_Input_Record_Pnd_Cntrct_State_Cde = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_State_Cde", "#CNTRCT-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Cntrct_Filler2 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Filler2", "#CNTRCT-FILLER2", FieldType.STRING, 
            13);
        pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte", "#CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6);

        pnd_Input_Record__R_Field_6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record_Pnd_Rest_Of_Record);
        pnd_Input_Record_Pnd_Ddctn_Id_Nbr = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Id_Nbr", "#DDCTN-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Input_Record_Pnd_Ddctn_Cde = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);
        pnd_Input_Record_Pnd_Ddctn_Seq_Nbr = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Input_Record_Pnd_Ddctn_Payee = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Ddctn_Per_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Input_Record_Pnd_Ddctn_Ytd_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Ytd_Amt", "#DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Pd_To_Dte", "#DDCTN-PD-TO-DTE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_Ddctn_Tot_Amt = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Tot_Amt", "#DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_Ddctn_Intent_Code = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Intent_Code", "#DDCTN-INTENT-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Input_Record_Pnd_Ddctn_Strt_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Stp_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Ddctn_Final_Dte = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Ddctn_Final_Dte", "#DDCTN-FINAL-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_7", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_7.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_7.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_7.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);
        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_8", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_8.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_8.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_8.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde = pnd_Trans_Cntrct_Key__R_Field_8.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Lst_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Check_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        trans_Trans_Todays_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_TODAYS_DTE");
        trans_Trans_User_Area = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TRANS_USER_AREA");
        registerRecord(vw_trans);

        vw_fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_fund_Rcrd", "FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        fund_Rcrd__R_Field_9 = vw_fund_Rcrd.getRecord().newGroupInGroup("fund_Rcrd__R_Field_9", "REDEFINE", fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        fund_Rcrd_Tiaa_Cmpny_Cde = fund_Rcrd__R_Field_9.newFieldInGroup("fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        fund_Rcrd_Tiaa_Fund_Cde = fund_Rcrd__R_Field_9.newFieldInGroup("fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 2);
        fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        fund_Rcrd_Tiaa_Rtb_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_RTB_AMT");
        fund_Rcrd_Tiaa_Tot_Per_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        fund_Rcrd_Tiaa_Tot_Div_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        fund_Rcrd_Tiaa_Old_Per_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "AJ");
        fund_Rcrd_Tiaa_Old_Div_Amt = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        fund_Rcrd_Tiaa_Rate_Data_Grp = vw_fund_Rcrd.getRecord().newGroupInGroup("fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Rate_Cde = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        fund_Rcrd__R_Field_10 = fund_Rcrd_Tiaa_Rate_Data_Grp.newGroupInGroup("fund_Rcrd__R_Field_10", "REDEFINE", fund_Rcrd_Tiaa_Rate_Cde);
        fund_Rcrd_Tiaa_Rate_Cde_N = fund_Rcrd__R_Field_10.newFieldArrayInGroup("fund_Rcrd_Tiaa_Rate_Cde_N", "TIAA-RATE-CDE-N", FieldType.NUMERIC, 2, new 
            DbsArrayController(1, 90));
        fund_Rcrd_Tiaa_Rate_Dte = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 
            90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Per_Pay_Amt = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Per_Div_Amt = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Units_Cnt = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Tiaa_Rate_Final_Div_Amt = fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Rcrd_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Rcrd_Lst_Trans_Dte = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_fund_Rcrd.getRecord().newFieldInGroup("fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIAA_XFR_ISS_DTE");
        fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_fund_Rcrd.getRecord().newFieldInGroup("FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_fund_Rcrd);

        vw_fund_Trans = new DataAccessProgramView(new NameInfo("vw_fund_Trans", "FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        fund_Trans_Bfre_Imge_Id = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        fund_Trans__R_Field_11 = vw_fund_Trans.getRecord().newGroupInGroup("fund_Trans__R_Field_11", "REDEFINE", fund_Trans_Tiaa_Cmpny_Fund_Cde);
        fund_Trans_Tiaa_Cmpny_Cde = fund_Trans__R_Field_11.newFieldInGroup("fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        fund_Trans_Tiaa_Fund_Cde = fund_Trans__R_Field_11.newFieldInGroup("fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 2);
        fund_Trans_Tiaa_Tot_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        fund_Trans_Tiaa_Tot_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        fund_Trans_Tiaa_Old_Per_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        fund_Trans_Tiaa_Old_Div_Amt = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newFieldInGroup("fund_Trans_Count_Casttiaa_Rate_Data_Grp", "C*TIAA-RATE-DATA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        fund_Trans_Tiaa_Rate_Data_Grp = vw_fund_Trans.getRecord().newGroupInGroup("fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Cde = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", FieldType.STRING, 2, 
            new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        fund_Trans__R_Field_12 = fund_Trans_Tiaa_Rate_Data_Grp.newGroupInGroup("fund_Trans__R_Field_12", "REDEFINE", fund_Trans_Tiaa_Rate_Cde);
        fund_Trans_Tiaa_Rate_Cde_N = fund_Trans__R_Field_12.newFieldArrayInGroup("fund_Trans_Tiaa_Rate_Cde_N", "TIAA-RATE-CDE-N", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 90));
        fund_Trans_Tiaa_Rate_Dte = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, new 
            DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Per_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Units_Cnt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Pay_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        fund_Trans_Tiaa_Rate_Final_Div_Amt = fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Trans_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund_Trans);

        pnd_Curr_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Curr_Check_Date_Ccyymmdd", "#CURR-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_13 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_13", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy = pnd_Curr_Check_Date_Ccyymmdd__R_Field_13.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy", 
            "#CURR-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_13.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm", 
            "#CURR-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd = pnd_Curr_Check_Date_Ccyymmdd__R_Field_13.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd", 
            "#CURR-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_14 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_14", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_14.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm", 
            "#CURR-CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2 = pnd_Curr_Check_Date_Ccyymmdd__R_Field_14.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2", 
            "#CURR-CHECK-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Next_Check_Date_Ccyymmdd", "#NEXT-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_15 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_15", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy = pnd_Next_Check_Date_Ccyymmdd__R_Field_15.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy", 
            "#NEXT-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm = pnd_Next_Check_Date_Ccyymmdd__R_Field_15.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm", 
            "#NEXT-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd = pnd_Next_Check_Date_Ccyymmdd__R_Field_15.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd", 
            "#NEXT-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_16 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_16", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm = pnd_Next_Check_Date_Ccyymmdd__R_Field_16.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm", 
            "#NEXT-CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2 = pnd_Next_Check_Date_Ccyymmdd__R_Field_16.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2", 
            "#NEXT-CHECK-DD2", FieldType.NUMERIC, 2);
        pnd_Prev_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Prev_Check_Date_Ccyymmdd", "#PREV-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Prev_Check_Date_Ccyymmdd__R_Field_17 = localVariables.newGroupInRecord("pnd_Prev_Check_Date_Ccyymmdd__R_Field_17", "REDEFINE", pnd_Prev_Check_Date_Ccyymmdd);
        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Ccyy = pnd_Prev_Check_Date_Ccyymmdd__R_Field_17.newFieldInGroup("pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Ccyy", 
            "#PREV-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm = pnd_Prev_Check_Date_Ccyymmdd__R_Field_17.newFieldInGroup("pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm", 
            "#PREV-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Dd = pnd_Prev_Check_Date_Ccyymmdd__R_Field_17.newFieldInGroup("pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Dd", 
            "#PREV-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Prev_Check_Date_Ccyymmdd__R_Field_18 = localVariables.newGroupInRecord("pnd_Prev_Check_Date_Ccyymmdd__R_Field_18", "REDEFINE", pnd_Prev_Check_Date_Ccyymmdd);
        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Ccyymm = pnd_Prev_Check_Date_Ccyymmdd__R_Field_18.newFieldInGroup("pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Ccyymm", 
            "#PREV-CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Dd2 = pnd_Prev_Check_Date_Ccyymmdd__R_Field_18.newFieldInGroup("pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Date_Dd2", 
            "#PREV-CHECK-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 5);
        pnd_Due_Mode = localVariables.newFieldInRecord("pnd_Due_Mode", "#DUE-MODE", FieldType.NUMERIC, 3);

        pnd_Due_Mode__R_Field_19 = localVariables.newGroupInRecord("pnd_Due_Mode__R_Field_19", "REDEFINE", pnd_Due_Mode);
        pnd_Due_Mode_Pnd_Fllr = pnd_Due_Mode__R_Field_19.newFieldInGroup("pnd_Due_Mode_Pnd_Fllr", "#FLLR", FieldType.STRING, 1);
        pnd_Due_Mode_Pnd_Due_Mm = pnd_Due_Mode__R_Field_19.newFieldInGroup("pnd_Due_Mode_Pnd_Due_Mm", "#DUE-MM", FieldType.NUMERIC, 2);
        pnd_Term_Dte = localVariables.newFieldInRecord("pnd_Term_Dte", "#TERM-DTE", FieldType.NUMERIC, 8);
        pnd_Drwdwn_Dte = localVariables.newFieldInRecord("pnd_Drwdwn_Dte", "#DRWDWN-DTE", FieldType.NUMERIC, 8);
        pnd_Sve_Mode = localVariables.newFieldInRecord("pnd_Sve_Mode", "#SVE-MODE", FieldType.NUMERIC, 3);
        pnd_Master_Work_Rec = localVariables.newFieldInRecord("pnd_Master_Work_Rec", "#MASTER-WORK-REC", FieldType.STRING, 100);

        pnd_Master_Work_Rec__R_Field_20 = localVariables.newGroupInRecord("pnd_Master_Work_Rec__R_Field_20", "REDEFINE", pnd_Master_Work_Rec);
        pnd_Master_Work_Rec_Contract_No_M = pnd_Master_Work_Rec__R_Field_20.newFieldInGroup("pnd_Master_Work_Rec_Contract_No_M", "CONTRACT-NO-M", FieldType.STRING, 
            8);
        pnd_Master_Work_Rec_Payee_M = pnd_Master_Work_Rec__R_Field_20.newFieldInGroup("pnd_Master_Work_Rec_Payee_M", "PAYEE-M", FieldType.NUMERIC, 2);
        pnd_Master_Work_Rec_Rec_Type_M = pnd_Master_Work_Rec__R_Field_20.newFieldInGroup("pnd_Master_Work_Rec_Rec_Type_M", "REC-TYPE-M", FieldType.NUMERIC, 
            2);

        pnd_Master_Work_Rec_Rec_Type_10 = pnd_Master_Work_Rec__R_Field_20.newGroupInGroup("pnd_Master_Work_Rec_Rec_Type_10", "REC-TYPE-10");
        pnd_Master_Work_Rec_Filler_1 = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_1", "FILLER-1", FieldType.STRING, 3);
        pnd_Master_Work_Rec_Product_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Product_M", "PRODUCT-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Currency_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Currency_M", "CURRENCY-M", FieldType.NUMERIC, 
            1);
        pnd_Master_Work_Rec_Mode_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Mode_M", "MODE-M", FieldType.NUMERIC, 3);
        pnd_Master_Work_Rec_Pend_Cde = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pend_Cde", "PEND-CDE", FieldType.STRING, 1);
        pnd_Master_Work_Rec_Hold_Cde = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Hold_Cde", "HOLD-CDE", FieldType.STRING, 1);
        pnd_Master_Work_Rec_Pend_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pend_Date", "PEND-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Option_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Option_M", "OPTION-M", FieldType.NUMERIC, 
            2);
        pnd_Master_Work_Rec_Iss_Origin = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Origin", "ISS-ORIGIN", FieldType.NUMERIC, 
            2);
        pnd_Master_Work_Rec_Iss_Iss_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Iss_Date", "ISS-ISS-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Iss_1st_Pay_Due_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_1st_Pay_Due_Date", "ISS-1ST-PAY-DUE-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Iss_1st_Pay_Paid_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_1st_Pay_Paid_Date", "ISS-1ST-PAY-PAID-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Iss_Final_Per_Pay_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Final_Per_Pay_Date", "ISS-FINAL-PER-PAY-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Iss_Final_Pay_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Final_Pay_Date", "ISS-FINAL-PAY-DATE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Master_Work_Rec_Iss_Withdraw_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Withdraw_Date", "ISS-WITHDRAW-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Iss_Citizenship = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Citizenship", "ISS-CITIZENSHIP", 
            FieldType.NUMERIC, 3);
        pnd_Master_Work_Rec_Iss_State_At_Issue_Time = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_State_At_Issue_Time", "ISS-STATE-AT-ISSUE-TIME", 
            FieldType.STRING, 3);
        pnd_Master_Work_Rec_Iss_Current_State = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Current_State", "ISS-CURRENT-STATE", 
            FieldType.STRING, 3);
        pnd_Master_Work_Rec_Iss_College_Code = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_College_Code", "ISS-COLLEGE-CODE", 
            FieldType.STRING, 5);
        pnd_Master_Work_Rec_Iss_Rewrite = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Rewrite", "ISS-REWRITE", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Iss_Delete = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Delete", "ISS-DELETE", FieldType.NUMERIC, 
            1);

        pnd_Master_Work_Rec__R_Field_21 = pnd_Master_Work_Rec_Rec_Type_10.newGroupInGroup("pnd_Master_Work_Rec__R_Field_21", "REDEFINE", pnd_Master_Work_Rec_Iss_Delete);
        pnd_Master_Work_Rec_Iss_Delete_R = pnd_Master_Work_Rec__R_Field_21.newFieldInGroup("pnd_Master_Work_Rec_Iss_Delete_R", "ISS-DELETE-R", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Iss_Rtb_Amt = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Rtb_Amt", "ISS-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Master_Work_Rec_Iss_Last_Trans_Date = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Last_Trans_Date", "ISS-LAST-TRANS-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_J_C_Code_M = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_J_C_Code_M", "J-C-CODE-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Iss_Temp_Hold_Code = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Temp_Hold_Code", "ISS-TEMP-HOLD-CODE", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Iss_Spirt_Code = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Spirt_Code", "ISS-SPIRT-CODE", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Iss_Pens_Plan_Cd = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Pens_Plan_Cd", "ISS-PENS-PLAN-CD", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Iss_Contr_Type_Cd = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Contr_Type_Cd", "ISS-CONTR-TYPE-CD", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Iss_New_Issue_Flag = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_New_Issue_Flag", "ISS-NEW-ISSUE-FLAG", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Iss_Dte_Dd = pnd_Master_Work_Rec_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Iss_Dte_Dd", "ISS-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Master_Work_Rec_Iss_Tran_Cde = pnd_Master_Work_Rec_Rec_Type_10.newFieldArrayInGroup("pnd_Master_Work_Rec_Iss_Tran_Cde", "ISS-TRAN-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 4));

        pnd_Master_Work_Rec__R_Field_22 = pnd_Master_Work_Rec__R_Field_20.newGroupInGroup("pnd_Master_Work_Rec__R_Field_22", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_Filler_4 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Filler_4", "FILLER-4", FieldType.STRING, 3);
        pnd_Master_Work_Rec_X_Ref_1_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_X_Ref_1_M", "X-REF-1-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Sex_1_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Sex_1_M", "SEX-1-M", FieldType.NUMERIC, 1);
        pnd_Master_Work_Rec_Dob_1_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Dob_1_M", "DOB-1-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Dod_1_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Dod_1_M", "DOD-1-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Filler_5 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Filler_5", "FILLER-5", FieldType.STRING, 10);
        pnd_Master_Work_Rec_X_Ref_2_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_X_Ref_2_M", "X-REF-2-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Sex2_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Sex2_M", "SEX2-M", FieldType.NUMERIC, 1);
        pnd_Master_Work_Rec_Dob_2_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Dob_2_M", "DOB-2-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Dod_2_M = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Dod_2_M", "DOD-2-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Sec_Mort_Yob_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Sec_Mort_Yob_M2", "SEC-MORT-YOB-M2", 
            FieldType.PACKED_DECIMAL, 4);
        pnd_Master_Work_Rec_Sec_Life_Cnt_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Sec_Life_Cnt_M2", "SEC-LIFE-CNT-M2", 
            FieldType.NUMERIC, 1);
        pnd_Master_Work_Rec_Ben_Cross_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Ben_Cross_M2", "BEN-CROSS-M2", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Ben_Dod_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Ben_Dod_M2", "BEN-DOD-M2", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Da_Contract = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Da_Contract", "DA-CONTRACT", FieldType.STRING, 
            8);
        pnd_Master_Work_Rec_Cash_Code_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Cash_Code_M2", "CASH-CODE-M2", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Employ_Code_M2 = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Employ_Code_M2", "EMPLOY-CODE-M2", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Prev_Dist_Cde = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Prev_Dist_Cde", "PREV-DIST-CDE", FieldType.STRING, 
            4);
        pnd_Master_Work_Rec_Curr_Dist_Cde = pnd_Master_Work_Rec__R_Field_22.newFieldInGroup("pnd_Master_Work_Rec_Curr_Dist_Cde", "CURR-DIST-CDE", FieldType.STRING, 
            4);

        pnd_Master_Work_Rec__R_Field_23 = pnd_Master_Work_Rec__R_Field_20.newGroupInGroup("pnd_Master_Work_Rec__R_Field_23", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_Rate_Filler = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Rate_Filler", "RATE-FILLER", FieldType.STRING, 
            3);
        pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div", "RATE-TOT-OLD-TIAA-DIV", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Master_Work_Rec_Lvl_5_Grp = pnd_Master_Work_Rec__R_Field_23.newGroupArrayInGroup("pnd_Master_Work_Rec_Lvl_5_Grp", "LVL-5-GRP", new DbsArrayController(1, 
            3));
        pnd_Master_Work_Rec_Rate_Rate = pnd_Master_Work_Rec_Lvl_5_Grp.newFieldInGroup("pnd_Master_Work_Rec_Rate_Rate", "RATE-RATE", FieldType.STRING, 
            2);
        pnd_Master_Work_Rec_Rate_Per_Pay = pnd_Master_Work_Rec_Lvl_5_Grp.newFieldInGroup("pnd_Master_Work_Rec_Rate_Per_Pay", "RATE-PER-PAY", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Master_Work_Rec_Rate_Per_Div = pnd_Master_Work_Rec_Lvl_5_Grp.newFieldInGroup("pnd_Master_Work_Rec_Rate_Per_Div", "RATE-PER-DIV", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Master_Work_Rec_Rate_Final_Pay = pnd_Master_Work_Rec_Lvl_5_Grp.newFieldInGroup("pnd_Master_Work_Rec_Rate_Final_Pay", "RATE-FINAL-PAY", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Master_Work_Rec_Rate_Date = pnd_Master_Work_Rec_Lvl_5_Grp.newFieldInGroup("pnd_Master_Work_Rec_Rate_Date", "RATE-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Rate_Trans_Code = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Rate_Trans_Code", "RATE-TRANS-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Master_Work_Rec_Rate_Trans_Date = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Rate_Trans_Date", "RATE-TRANS-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Filler_1a = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Filler_1a", "FILLER-1A", FieldType.STRING, 
            6);
        pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt = pnd_Master_Work_Rec__R_Field_23.newFieldInGroup("pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt", "RATE-TOT-OLD-TIAA-PMT", 
            FieldType.BINARY, 4);

        pnd_Master_Work_Rec__R_Field_24 = pnd_Master_Work_Rec__R_Field_20.newGroupInGroup("pnd_Master_Work_Rec__R_Field_24", "REDEFINE", pnd_Master_Work_Rec_Rec_Type_10);
        pnd_Master_Work_Rec_Us_Seq_Nbr = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Seq_Nbr", "US-SEQ-NBR", FieldType.STRING, 
            3);
        pnd_Master_Work_Rec_Us_Tax_Auth = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Tax_Auth", "US-TAX-AUTH", FieldType.STRING, 
            3);
        pnd_Master_Work_Rec_Us_Active_Code = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Active_Code", "US-ACTIVE-CODE", FieldType.NUMERIC, 
            1);
        pnd_Master_Work_Rec_Us_Per_Tax_Ded = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Per_Tax_Ded", "US-PER-TAX-DED", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Master_Work_Rec_Us_Ytd_Tax_Ded = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Tax_Ded", "US-YTD-TAX-DED", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Master_Work_Rec_Us_Recov_Type = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Recov_Type", "US-RECOV-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Master_Work_Rec_Us_Per_Tax_Free_Amt = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Per_Tax_Free_Amt", "US-PER-TAX-FREE-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Master_Work_Rec_Us_Res_Tax_Free_Amt = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Res_Tax_Free_Amt", "US-RES-TAX-FREE-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Master_Work_Rec_Us_Cont_To_Date_Inv_Used = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Cont_To_Date_Inv_Used", 
            "US-CONT-TO-DATE-INV-USED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Master_Work_Rec_Us_Invest_In_Contr = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Invest_In_Contr", "US-INVEST-IN-CONTR", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax", "US-YTD-ANN-PAY-TAX", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax_Free = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Ann_Pay_Tax_Free", "US-YTD-ANN-PAY-TAX-FREE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Master_Work_Rec_Us_Ytd_Div_Pay = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Div_Pay", "US-YTD-DIV-PAY", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Master_Work_Rec_Us_Ytd_Int_Pay = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Int_Pay", "US-YTD-INT-PAY", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Master_Work_Rec_Us_Ytd_Int_Div_Pay = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Ytd_Int_Div_Pay", "US-YTD-INT-DIV-PAY", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Master_Work_Rec_Soc_Sec_No_M = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Soc_Sec_No_M", "SOC-SEC-NO-M", FieldType.NUMERIC, 
            9);
        pnd_Master_Work_Rec_Us_Start_Date = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Start_Date", "US-START-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Us_Stop_Date = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Stop_Date", "US-STOP-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Us_Sprt_Lump_Sum = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Sprt_Lump_Sum", "US-SPRT-LUMP-SUM", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Master_Work_Rec_Us_Spirt_Arrival_Date = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Spirt_Arrival_Date", "US-SPIRT-ARRIVAL-DATE", 
            FieldType.PACKED_DECIMAL, 4);
        pnd_Master_Work_Rec_Us_Spirt_Arrival_Source = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Spirt_Arrival_Source", "US-SPIRT-ARRIVAL-SOURCE", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Us_Spirt_Process_Date = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Spirt_Process_Date", "US-SPIRT-PROCESS-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Master_Work_Rec_Us_Filler_1b = pnd_Master_Work_Rec__R_Field_24.newFieldInGroup("pnd_Master_Work_Rec_Us_Filler_1b", "US-FILLER-1B", FieldType.STRING, 
            2);
        pnd_Master_Trl_Rec = localVariables.newFieldInRecord("pnd_Master_Trl_Rec", "#MASTER-TRL-REC", FieldType.STRING, 100);

        pnd_Master_Trl_Rec__R_Field_25 = localVariables.newGroupInRecord("pnd_Master_Trl_Rec__R_Field_25", "REDEFINE", pnd_Master_Trl_Rec);
        pnd_Master_Trl_Rec_High_Value = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_High_Value", "HIGH-VALUE", FieldType.STRING, 
            2);
        pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr", "TIAA-ACTIVE-PAYEE-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr", "TIAA-PERIODIC-CONT-MITR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr", "TIAA-PERIODIC-DIV-MITR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr", "TIAA-FINAL-PMT-MITR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr", "TPA-ACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Master_Trl_Rec_Tpa_Per_Amt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tpa_Per_Amt", "TPA-PER-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_Tpa_Div_Amt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Tpa_Div_Amt", "TPA-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr", "IPRO-ACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Master_Trl_Rec_Ipro_Per_Amt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Ipro_Per_Amt", "IPRO-PER-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_Ipro_Per_Div = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Ipro_Per_Div", "IPRO-PER-DIV", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_Ipro_Fin_Pmt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Ipro_Fin_Pmt", "IPRO-FIN-PMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr", "P-I-ACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Master_Trl_Rec_P_I_Per_Amt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_P_I_Per_Amt", "P-I-PER-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_P_I_Per_Div = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_P_I_Per_Div", "P-I-PER-DIV", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_P_I_Fin_Pmt = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_P_I_Fin_Pmt", "P-I-FIN-PMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Master_Trl_Rec_Inactive_Payees_Mitr = pnd_Master_Trl_Rec__R_Field_25.newFieldInGroup("pnd_Master_Trl_Rec_Inactive_Payees_Mitr", "INACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_W_Hex_High_Value = localVariables.newFieldInRecord("pnd_W_Hex_High_Value", "#W-HEX-HIGH-VALUE", FieldType.STRING, 2);

        pnd_Sve_10_Rec_Data = localVariables.newGroupInRecord("pnd_Sve_10_Rec_Data", "#SVE-10-REC-DATA");
        pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde", "#SVE-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Opt = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Opt", "#SVE-OPT", FieldType.NUMERIC, 2);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Origin = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Origin", "#SVE-ORIGIN", FieldType.NUMERIC, 
            2);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte", "#SVE-ISSU-DTE", FieldType.NUMERIC, 
            6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dd = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dd", "#SVE-ISSU-DD", FieldType.NUMERIC, 
            2);
        pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Due_Date = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Due_Date", "#SVE-1ST-PAY-DUE-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Paid_Date = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Paid_Date", "#SVE-1ST-PAY-PAID-DATE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M", "#SVE-J-C-CODE-M", FieldType.STRING, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M", "#SVE-X-REF-1-M", FieldType.STRING, 
            9);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M", "#SVE-SEX-1-M", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M", "#SVE-DOB-1-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M", "#SVE-DOD-1-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M", "#SVE-X-REF-2-M", FieldType.STRING, 
            9);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M", "#SVE-SEX2-M", FieldType.NUMERIC, 
            1);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M", "#SVE-DOB-2-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M", "#SVE-DOD-2-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Coll_Code = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Coll_Code", "#SVE-ISSU-COLL-CODE", 
            FieldType.STRING, 5);
        pnd_Sve_10_Rec_Data_Pnd_Sve_State_At_Issue = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_State_At_Issue", "#SVE-STATE-AT-ISSUE", 
            FieldType.STRING, 3);
        pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type = pnd_Sve_10_Rec_Data.newFieldInGroup("pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type", "#SVE-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Sve_Delete_Cde = localVariables.newFieldInRecord("pnd_Sve_Delete_Cde", "#SVE-DELETE-CDE", FieldType.NUMERIC, 1);
        pnd_Sve_Orig_Da_Nbr = localVariables.newFieldInRecord("pnd_Sve_Orig_Da_Nbr", "#SVE-ORIG-DA-NBR", FieldType.STRING, 10);
        pnd_Sve_Ssn_Nbr = localVariables.newFieldInRecord("pnd_Sve_Ssn_Nbr", "#SVE-SSN-NBR", FieldType.NUMERIC, 9);
        pnd_Sve_Cntrct_Payee_Nbr = localVariables.newFieldInRecord("pnd_Sve_Cntrct_Payee_Nbr", "#SVE-CNTRCT-PAYEE-NBR", FieldType.STRING, 12);

        pnd_Sve_Cntrct_Payee_Nbr__R_Field_26 = localVariables.newGroupInRecord("pnd_Sve_Cntrct_Payee_Nbr__R_Field_26", "REDEFINE", pnd_Sve_Cntrct_Payee_Nbr);
        pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr = pnd_Sve_Cntrct_Payee_Nbr__R_Field_26.newFieldInGroup("pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr", 
            "#SVE-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde = pnd_Sve_Cntrct_Payee_Nbr__R_Field_26.newFieldInGroup("pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde", 
            "#SVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_End_Of_Newissus_File = localVariables.newFieldInRecord("pnd_End_Of_Newissus_File", "#END-OF-NEWISSUS-FILE", FieldType.STRING, 1);
        pnd_New_Issus_In = localVariables.newFieldInRecord("pnd_New_Issus_In", "#NEW-ISSUS-IN", FieldType.NUMERIC, 5);
        pnd_Matched_New_Issu_Rec_In = localVariables.newFieldInRecord("pnd_Matched_New_Issu_Rec_In", "#MATCHED-NEW-ISSU-REC-IN", FieldType.NUMERIC, 5);

        pnd_Iadp165_Selct_Trans_In = localVariables.newGroupInRecord("pnd_Iadp165_Selct_Trans_In", "#IADP165-SELCT-TRANS-IN");
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr = pnd_Iadp165_Selct_Trans_In.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr", 
            "#IADP165-CNTRCT-PAYEE-NBR", FieldType.STRING, 12);

        pnd_Iadp165_Selct_Trans_In__R_Field_27 = pnd_Iadp165_Selct_Trans_In.newGroupInGroup("pnd_Iadp165_Selct_Trans_In__R_Field_27", "REDEFINE", pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr = pnd_Iadp165_Selct_Trans_In__R_Field_27.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr", 
            "#IADP165-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Payee_Cde = pnd_Iadp165_Selct_Trans_In__R_Field_27.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Payee_Cde", 
            "#IADP165-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Opt_Cde = pnd_Iadp165_Selct_Trans_In.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Opt_Cde", 
            "#IADP165-OPT-CDE", FieldType.NUMERIC, 2);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Issu_Dte = pnd_Iadp165_Selct_Trans_In.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Issu_Dte", 
            "#IADP165-ISSU-DTE", FieldType.NUMERIC, 6);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cnt = pnd_Iadp165_Selct_Trans_In.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cnt", 
            "#IADP165-TRN-CNT", FieldType.NUMERIC, 2);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Data_Grp = pnd_Iadp165_Selct_Trans_In.newFieldArrayInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Data_Grp", 
            "#IADP165-TRAN-CDE-DATA-GRP", FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Iadp165_Selct_Trans_In__R_Field_28 = pnd_Iadp165_Selct_Trans_In.newGroupInGroup("pnd_Iadp165_Selct_Trans_In__R_Field_28", "REDEFINE", pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Data_Grp);

        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp = pnd_Iadp165_Selct_Trans_In__R_Field_28.newGroupArrayInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp", 
            "#IADP165-TRAN-CDE-GRP", new DbsArrayController(1, 4));
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde = pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde", 
            "#IADP165-TRN-CDE", FieldType.NUMERIC, 3);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Dte = pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Dte", 
            "#IADP165-TRN-DTE", FieldType.NUMERIC, 8);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte = pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte", 
            "#IADP165-TRN-CK-DTE", FieldType.NUMERIC, 8);

        pnd_Iadp165_Selct_Trans_In__R_Field_29 = pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Tran_Cde_Grp.newGroupInGroup("pnd_Iadp165_Selct_Trans_In__R_Field_29", 
            "REDEFINE", pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Ccyymm = pnd_Iadp165_Selct_Trans_In__R_Field_29.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Ccyymm", 
            "#IADP165-TRN-CK-DTE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Dd = pnd_Iadp165_Selct_Trans_In__R_Field_29.newFieldInGroup("pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Dd", 
            "#IADP165-TRN-CK-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Inactive_Cntrct = localVariables.newFieldInRecord("pnd_Inactive_Cntrct", "#INACTIVE-CNTRCT", FieldType.STRING, 1);
        pnd_Wrte_700_Sw = localVariables.newFieldInRecord("pnd_Wrte_700_Sw", "#WRTE-700-SW", FieldType.STRING, 1);
        pnd_Gen_700_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_700_Trn_Sw", "#GEN-700-TRN-SW", FieldType.STRING, 1);
        pnd_Gen_006_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_006_Trn_Sw", "#GEN-006-TRN-SW", FieldType.STRING, 1);
        pnd_Tot_Trans_Out = localVariables.newFieldInRecord("pnd_Tot_Trans_Out", "#TOT-TRANS-OUT", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_10 = localVariables.newFieldInRecord("pnd_Tot_Tran_10", "#TOT-TRAN-10", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_20 = localVariables.newFieldInRecord("pnd_Tot_Tran_20", "#TOT-TRAN-20", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_50 = localVariables.newFieldInRecord("pnd_Tot_Tran_50", "#TOT-TRAN-50", FieldType.NUMERIC, 9);
        pnd_Tot_Tran_70 = localVariables.newFieldInRecord("pnd_Tot_Tran_70", "#TOT-TRAN-70", FieldType.NUMERIC, 9);
        pnd_Cntrcts_Not_Selct = localVariables.newFieldInRecord("pnd_Cntrcts_Not_Selct", "#CNTRCTS-NOT-SELCT", FieldType.NUMERIC, 5);
        pnd_Fst_Tme = localVariables.newFieldInRecord("pnd_Fst_Tme", "#FST-TME", FieldType.STRING, 1);
        pnd_Sel_Record = localVariables.newFieldInRecord("pnd_Sel_Record", "#SEL-RECORD", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_Rte_Lvl_Nbr = localVariables.newFieldInRecord("pnd_Rte_Lvl_Nbr", "#RTE-LVL-NBR", FieldType.NUMERIC, 2);
        pnd_Found_Fnd_Sw = localVariables.newFieldInRecord("pnd_Found_Fnd_Sw", "#FOUND-FND-SW", FieldType.STRING, 1);
        pnd_Found_Iadp165_Trn_Sw = localVariables.newFieldInRecord("pnd_Found_Iadp165_Trn_Sw", "#FOUND-IADP165-TRN-SW", FieldType.STRING, 1);
        pnd_End_Of_Iadp165_File = localVariables.newFieldInRecord("pnd_End_Of_Iadp165_File", "#END-OF-IADP165-FILE", FieldType.STRING, 1);
        pnd_Matched_Iadp165_Rec_In = localVariables.newFieldInRecord("pnd_Matched_Iadp165_Rec_In", "#MATCHED-IADP165-REC-IN", FieldType.NUMERIC, 5);
        pnd_Iadp165_Rec_In = localVariables.newFieldInRecord("pnd_Iadp165_Rec_In", "#IADP165-REC-IN", FieldType.NUMERIC, 5);
        pnd_Cntrct_Lst_Trans_Dte_A = localVariables.newFieldInRecord("pnd_Cntrct_Lst_Trans_Dte_A", "#CNTRCT-LST-TRANS-DTE-A", FieldType.STRING, 6);

        pnd_Cntrct_Lst_Trans_Dte_A__R_Field_30 = localVariables.newGroupInRecord("pnd_Cntrct_Lst_Trans_Dte_A__R_Field_30", "REDEFINE", pnd_Cntrct_Lst_Trans_Dte_A);
        pnd_Cntrct_Lst_Trans_Dte_A_Pnd_Cntrct_Lst_Trans_Dte_N = pnd_Cntrct_Lst_Trans_Dte_A__R_Field_30.newFieldInGroup("pnd_Cntrct_Lst_Trans_Dte_A_Pnd_Cntrct_Lst_Trans_Dte_N", 
            "#CNTRCT-LST-TRANS-DTE-N", FieldType.NUMERIC, 6);
        pnd_Blank_Rate = localVariables.newFieldInRecord("pnd_Blank_Rate", "#BLANK-RATE", FieldType.STRING, 1);
        pnd_Have_Cntrct_Rec_Sw = localVariables.newFieldInRecord("pnd_Have_Cntrct_Rec_Sw", "#HAVE-CNTRCT-REC-SW", FieldType.STRING, 1);
        pnd_Rate_Date_A8 = localVariables.newFieldInRecord("pnd_Rate_Date_A8", "#RATE-DATE-A8", FieldType.STRING, 8);

        pnd_Rate_Date_A8__R_Field_31 = localVariables.newGroupInRecord("pnd_Rate_Date_A8__R_Field_31", "REDEFINE", pnd_Rate_Date_A8);
        pnd_Rate_Date_A8_Pnd_Rate_Date_N6 = pnd_Rate_Date_A8__R_Field_31.newFieldInGroup("pnd_Rate_Date_A8_Pnd_Rate_Date_N6", "#RATE-DATE-N6", FieldType.NUMERIC, 
            6);
        pnd_Rate_Date_A8_Pnd_Rate_Date_Dd = pnd_Rate_Date_A8__R_Field_31.newFieldInGroup("pnd_Rate_Date_A8_Pnd_Rate_Date_Dd", "#RATE-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Save_Isn = localVariables.newFieldInRecord("pnd_Save_Isn", "#SAVE-ISN", FieldType.PACKED_DECIMAL, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trans.reset();
        vw_fund_Rcrd.reset();
        vw_fund_Trans.reset();

        localVariables.reset();
        pnd_Due_Mode.setInitialValue(800);
        pnd_W_Hex_High_Value.setInitialValue("H'FFFF'");
        pnd_Fst_Tme.setInitialValue("Y");
        pnd_Sel_Record.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iadp166() throws Exception
    {
        super("Iadp166");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IADP166", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  WRITE 'BEGING OF IADP166 *******************'
        pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr.reset();                                                                                                                //Natural: RESET TIAA-ACTIVE-PAYEE-MITR TIAA-ACTIVE-PAYEE-MITR TIAA-PERIODIC-CONT-MITR TIAA-PERIODIC-DIV-MITR TIAA-FINAL-PMT-MITR TPA-ACTIVE-PAYEES-MITR TPA-PER-AMT TPA-DIV-AMT IPRO-ACTIVE-PAYEES-MITR IPRO-PER-AMT IPRO-PER-DIV IPRO-FIN-PMT P-I-ACTIVE-PAYEES-MITR P-I-PER-AMT P-I-PER-DIV P-I-FIN-PMT INACTIVE-PAYEES-MITR #IADP165-CNTRCT-NBR RATE-RATE ( * ) RATE-PER-PAY ( * ) RATE-PER-DIV ( * ) RATE-FINAL-PAY ( * ) RATE-DATE ( * )
        pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr.reset();
        pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr.reset();
        pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr.reset();
        pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr.reset();
        pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr.reset();
        pnd_Master_Trl_Rec_Tpa_Per_Amt.reset();
        pnd_Master_Trl_Rec_Tpa_Div_Amt.reset();
        pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr.reset();
        pnd_Master_Trl_Rec_Ipro_Per_Amt.reset();
        pnd_Master_Trl_Rec_Ipro_Per_Div.reset();
        pnd_Master_Trl_Rec_Ipro_Fin_Pmt.reset();
        pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr.reset();
        pnd_Master_Trl_Rec_P_I_Per_Amt.reset();
        pnd_Master_Trl_Rec_P_I_Per_Div.reset();
        pnd_Master_Trl_Rec_P_I_Fin_Pmt.reset();
        pnd_Master_Trl_Rec_Inactive_Payees_Mitr.reset();
        pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr.reset();
        pnd_Master_Work_Rec_Rate_Rate.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Per_Pay.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Per_Div.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Final_Pay.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Date.getValue("*").reset();
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr.notEquals(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr)))                                                   //Natural: IF #SVE-CNTRCT-NBR NE #CNTRCT-PPCN-NBR
            {
                pnd_Have_Cntrct_Rec_Sw.reset();                                                                                                                           //Natural: RESET #HAVE-CNTRCT-REC-SW #SEL-RECORD
                pnd_Sel_Record.reset();
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet582 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RECORD-CDE;//Natural: VALUE 00
            if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(0))))
            {
                decideConditionsMet582++;
                if (condition(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr.equals("   CHEADER")))                                                                                 //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER'
                {
                    pnd_Curr_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #CURR-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #NEXT-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.nadd(1);                                                                                               //Natural: ADD 1 TO #NEXT-CHECK-MM
                    if (condition(pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.greater(12)))                                                                            //Natural: IF #NEXT-CHECK-MM GT 12
                    {
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.setValue(1);                                                                                       //Natural: MOVE 1 TO #NEXT-CHECK-MM
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy.nadd(1);                                                                                         //Natural: ADD 1 TO #NEXT-CHECK-CCYY
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FOR PARTIAL DRAWDOWN 03/06
                    pnd_Prev_Check_Date_Ccyymmdd.setValue(pnd_Input_Record_Pnd_W_Check_Date);                                                                             //Natural: ASSIGN #PREV-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm.nsubtract(1);                                                                                          //Natural: SUBTRACT 1 FROM #PREV-CHECK-MM
                    if (condition(pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm.lessOrEqual(getZero())))                                                                 //Natural: IF #PREV-CHECK-MM LE 0
                    {
                        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm.setValue(12);                                                                                      //Natural: MOVE 12 TO #PREV-CHECK-MM
                        pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Ccyy.nsubtract(1);                                                                                    //Natural: SUBTRACT 1 FROM #PREV-CHECK-CCYY
                        //*  04/06 DETERMINE MODE DUE FOR
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Due_Mode_Pnd_Due_Mm.compute(new ComputeParameters(false, pnd_Due_Mode_Pnd_Due_Mm), pnd_Prev_Check_Date_Ccyymmdd_Pnd_Prev_Check_Mm.subtract(1));   //Natural: ASSIGN #DUE-MM := #PREV-CHECK-MM - 1
                    //*  PAYMENT
                    if (condition(pnd_Due_Mode_Pnd_Due_Mm.lessOrEqual(getZero())))                                                                                        //Natural: IF #DUE-MM LE 0
                    {
                        pnd_Due_Mode_Pnd_Due_Mm.setValue(12);                                                                                                             //Natural: MOVE 12 TO #DUE-MM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(10))))
            {
                decideConditionsMet582++;
                pnd_Have_Cntrct_Rec_Sw.setValue("Y");                                                                                                                     //Natural: ASSIGN #HAVE-CNTRCT-REC-SW := 'Y'
                pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                               //Natural: ASSIGN #SVE-CNTRCT-NBR := #CNTRCT-PPCN-NBR
                if (condition(pnd_Input_Record_Pnd_Cntrct_Optn_Cde.equals(22) || pnd_Input_Record_Pnd_Cntrct_Optn_Cde.equals(25) || pnd_Input_Record_Pnd_Cntrct_Optn_Cde.equals(27)  //Natural: IF #CNTRCT-OPTN-CDE = 22 OR = 25 OR = 27 OR = 28 OR = 30
                    || pnd_Input_Record_Pnd_Cntrct_Optn_Cde.equals(28) || pnd_Input_Record_Pnd_Cntrct_Optn_Cde.equals(30)))
                {
                    pnd_Sel_Record.setValue("Y");                                                                                                                         //Natural: ASSIGN #SEL-RECORD := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sel_Record.setValue("N");                                                                                                                         //Natural: ASSIGN #SEL-RECORD := 'N'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  ADDED 3/05
                    //*  04/06 SAVE MODE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Crrncy_Cde);                                                                  //Natural: ASSIGN #SVE-CRRNCY-CDE := #CNTRCT-CRRNCY-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.setValue(pnd_Input_Record_Pnd_Cntrct_Optn_Cde);                                                                           //Natural: ASSIGN #SVE-OPT := #CNTRCT-OPTN-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Origin.setValue(pnd_Input_Record_Pnd_Cntrct_Orgin_Cde);                                                                       //Natural: ASSIGN #SVE-ORIGIN := #CNTRCT-ORGIN-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte);                                                                     //Natural: ASSIGN #SVE-ISSU-DTE := #CNTRCT-ISSUE-DTE
                //*  ADDED FOLLOWING 1/02
                pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dd.setValue(pnd_Input_Record_Pnd_Cntrct_Issue_Dte_Dd);                                                                   //Natural: ASSIGN #SVE-ISSU-DD := #CNTRCT-ISSUE-DTE-DD
                pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M.setValue(pnd_Input_Record_Pnd_Cntrct_Joint_Cnvrt_Rcrcd_I);                                                         //Natural: ASSIGN #SVE-J-C-CODE-M := #CNTRCT-JOINT-CNVRT-RCRCD-I
                pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Due_Date.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Due_Dte);                                                           //Natural: ASSIGN #SVE-1ST-PAY-DUE-DATE := #CNTRCT-1ST-DUE-DTE
                pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Paid_Date.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Pd_Dte);                                                           //Natural: ASSIGN #SVE-1ST-PAY-PAID-DATE := #CNTRCT-1ST-PD-DTE
                pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Coll_Code.setValue(pnd_Input_Record_Pnd_Cntrct_Inst_Iss_Cde);                                                            //Natural: ASSIGN #SVE-ISSU-COLL-CODE := #CNTRCT-INST-ISS-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_State_At_Issue.setValue(pnd_Input_Record_Pnd_Cntrct_Issu_St_Cde);                                                             //Natural: ASSIGN #SVE-STATE-AT-ISSUE := #CNTRCT-ISSU-ST-CDE
                pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Xref);                                                                     //Natural: ASSIGN #SVE-X-REF-1-M := #CNTRCT-1ST-XREF
                pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Annt_Sex);                                                                   //Natural: ASSIGN #SVE-SEX-1-M := #CNTRCT-1ST-ANNT-SEX
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dob);                                                                        //Natural: ASSIGN #SVE-DOB-1-M := #CNTRCT-1ST-DOB
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M.setValue(pnd_Input_Record_Pnd_Cntrct_1st_Dod);                                                                        //Natural: ASSIGN #SVE-DOD-1-M := #CNTRCT-1ST-DOD
                pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Xref);                                                                     //Natural: ASSIGN #SVE-X-REF-2-M := #CNTRCT-2ND-XREF
                pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Annt_Sex);                                                                    //Natural: ASSIGN #SVE-SEX2-M := #CNTRCT-2ND-ANNT-SEX
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dob);                                                                        //Natural: ASSIGN #SVE-DOB-2-M := #CNTRCT-2ND-DOB
                pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M.setValue(pnd_Input_Record_Pnd_Cntrct_2nd_Dod);                                                                        //Natural: ASSIGN #SVE-DOD-2-M := #CNTRCT-2ND-DOD
                pnd_Sve_Orig_Da_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Orig_Da);                                                                                        //Natural: ASSIGN #SVE-ORIG-DA-NBR := #CNTRCT-ORIG-DA
                pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.setValue(pnd_Input_Record_Pnd_Cntrct_Type);                                                                       //Natural: ASSIGN #SVE-CNTRCT-TYPE := #CNTRCT-TYPE
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Record_Pnd_Record_Cde.equals(20))))
            {
                decideConditionsMet582++;
                pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                               //Natural: ASSIGN #SVE-CNTRCT-NBR := #CNTRCT-PPCN-NBR
                pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                               //Natural: ASSIGN #SVE-PAYEE-CDE := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_Iss_Delete.setValue(pnd_Input_Record_Pnd_Cntrct_Actvty_Cde);                                                                          //Natural: ASSIGN ISS-DELETE := #CNTRCT-ACTVTY-CDE
                pnd_Sve_Delete_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Actvty_Cde);                                                                                      //Natural: ASSIGN #SVE-DELETE-CDE := #CNTRCT-ACTVTY-CDE
                pnd_Master_Work_Rec_Pend_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Pend_Cde);                                                                              //Natural: ASSIGN PEND-CDE := #CNTRCT-PEND-CDE
                pnd_Master_Work_Rec_Hold_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Hold_Cde);                                                                              //Natural: ASSIGN HOLD-CDE := #CNTRCT-HOLD-CDE
                pnd_Master_Work_Rec_Pend_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Pend_Date);                                                                            //Natural: ASSIGN PEND-DATE := #CNTRCT-PEND-DATE
                pnd_Sve_Mode.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                              //Natural: ASSIGN #SVE-MODE := #CNTRCT-MODE-IND
                if (condition(pnd_Have_Cntrct_Rec_Sw.notEquals("Y")))                                                                                                     //Natural: IF #HAVE-CNTRCT-REC-SW NE 'Y'
                {
                    pnd_Cntrcts_Not_Selct.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CNTRCTS-NOT-SELCT
                    getReports().write(1, ReportOption.NOTITLE,pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,"<---*** NO CONTRACT RECORD FOUND FOR CONTRACT-NBR**");               //Natural: WRITE ( 1 ) #CNTRCT-PPCN-NBR '<---*** NO CONTRACT RECORD FOUND FOR CONTRACT-NBR**'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,"*** CONTRACT BYPASSED NOT SELECTED *************");                                                       //Natural: WRITE ( 1 ) '*** CONTRACT BYPASSED NOT SELECTED *************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                    if (condition(pnd_Input_Record_Pnd_Cntrct_Actvty_Cde.notEquals(9)))                                                                                   //Natural: IF #CNTRCT-ACTVTY-CDE NE 9
                    {
                        pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr.nadd(1);                                                                                                //Natural: ADD 1 TO TIAA-ACTIVE-PAYEE-MITR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Master_Trl_Rec_Inactive_Payees_Mitr.nadd(1);                                                                                                  //Natural: ADD 1 TO INACTIVE-PAYEES-MITR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec_Rec_Type_M.setValue(10);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 10
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_Mode_M.setValue(pnd_Input_Record_Pnd_Cntrct_Mode_Ind);                                                                                //Natural: ASSIGN MODE-M := #CNTRCT-MODE-IND
                pnd_Master_Work_Rec_Currency_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Crrncy_Cde);                                                                          //Natural: ASSIGN CURRENCY-M := #SVE-CRRNCY-CDE
                pnd_Master_Work_Rec_Option_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt);                                                                                   //Natural: ASSIGN OPTION-M := #SVE-OPT
                pnd_Master_Work_Rec_J_C_Code_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_J_C_Code_M);                                                                          //Natural: ASSIGN J-C-CODE-M := #SVE-J-C-CODE-M
                pnd_Master_Work_Rec_Iss_Origin.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Origin);                                                                              //Natural: ASSIGN ISS-ORIGIN := #SVE-ORIGIN
                pnd_Master_Work_Rec_Iss_Iss_Date.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dte);                                                                          //Natural: ASSIGN ISS-ISS-DATE := #SVE-ISSU-DTE
                //*  ADDED FOLLOWING 1/02 CAPTURE ISS-DAY FOR ACTUARY CALL TPA OPEN VALUE
                if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(28) || pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(30)))                                                  //Natural: IF #SVE-OPT = 28 OR = 30
                {
                    pnd_Master_Work_Rec_Iss_Dte_Dd.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Dd);                                                                         //Natural: ASSIGN ISS-DTE-DD := #SVE-ISSU-DD
                    if (condition(pnd_Master_Work_Rec_Iss_Dte_Dd.equals(getZero())))                                                                                      //Natural: IF ISS-DTE-DD = 0
                    {
                        pnd_Master_Work_Rec_Iss_Dte_Dd.setValue(1);                                                                                                       //Natural: ASSIGN ISS-DTE-DD := 01
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADDED 3/05
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD 1/02
                pnd_Master_Work_Rec_Iss_1st_Pay_Due_Date.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Due_Date);                                                          //Natural: ASSIGN ISS-1ST-PAY-DUE-DATE := #SVE-1ST-PAY-DUE-DATE
                pnd_Master_Work_Rec_Iss_1st_Pay_Paid_Date.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_1st_Pay_Paid_Date);                                                        //Natural: ASSIGN ISS-1ST-PAY-PAID-DATE := #SVE-1ST-PAY-PAID-DATE
                pnd_Master_Work_Rec_Iss_College_Code.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Issu_Coll_Code);                                                                //Natural: ASSIGN ISS-COLLEGE-CODE := #SVE-ISSU-COLL-CODE
                pnd_Master_Work_Rec_Iss_State_At_Issue_Time.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_State_At_Issue);                                                         //Natural: ASSIGN ISS-STATE-AT-ISSUE-TIME := #SVE-STATE-AT-ISSUE
                pnd_Master_Work_Rec_Iss_Final_Per_Pay_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Final_Per_Dte);                                                           //Natural: ASSIGN ISS-FINAL-PER-PAY-DATE := #CNTRCT-FINAL-PER-DTE
                pnd_Master_Work_Rec_Iss_Final_Pay_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte);                                                               //Natural: ASSIGN ISS-FINAL-PAY-DATE := #CNTRCT-FINAL-PAY-DTE
                pnd_Master_Work_Rec_Iss_Withdraw_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Wthdrwl_Dte);                                                                  //Natural: ASSIGN ISS-WITHDRAW-DATE := #CNTRCT-WTHDRWL-DTE
                pnd_Master_Work_Rec_Iss_Current_State.setValue(pnd_Input_Record_Pnd_Cntrct_State_Cde);                                                                    //Natural: ASSIGN ISS-CURRENT-STATE := #CNTRCT-STATE-CDE
                pnd_Master_Work_Rec_Iss_Rewrite.setValue(pnd_Input_Record_Pnd_Cntrct_Rwrttn_Cde);                                                                         //Natural: ASSIGN ISS-REWRITE := #CNTRCT-RWRTTN-CDE
                pnd_Master_Work_Rec_Iss_Rtb_Amt.setValue(pnd_Input_Record_Pnd_Cntrct_Rtb_Amt.getValue(1));                                                                //Natural: ASSIGN ISS-RTB-AMT := #CNTRCT-RTB-AMT ( 1 )
                pnd_Master_Work_Rec_Iss_Contr_Type_Cd.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type);                                                                  //Natural: ASSIGN ISS-CONTR-TYPE-CD := #SVE-CNTRCT-TYPE
                //*  FOR FULLY CONVERTED TPA/IPROS
                //*  AND PARTIAL 03/06
                if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.equals("F") || pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.equals("P")))                                //Natural: IF #SVE-CNTRCT-TYPE = 'F' OR = 'P'
                {
                    //*  02/06
                                                                                                                                                                          //Natural: PERFORM GET-LAST-TRANS-DTE
                    sub_Get_Last_Trans_Dte();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte.notEquals(getZero())))                                                                            //Natural: IF #CNTRCT-LST-CHNGE-DTE NE 0
                {
                    pnd_Master_Work_Rec_Iss_Last_Trans_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte);                                                          //Natural: ASSIGN ISS-LAST-TRANS-DATE := #CNTRCT-LST-CHNGE-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntrct_Lst_Trans_Dte_A.setValueEdited(pnd_Input_Record_Pnd_Cntrct_Lst_Trans_Dte,new ReportEditMask("YYYYMM"));                                    //Natural: MOVE EDITED #CNTRCT-LST-TRANS-DTE ( EM = YYYYMM ) TO #CNTRCT-LST-TRANS-DTE-A
                    pnd_Master_Work_Rec_Iss_Last_Trans_Date.setValue(pnd_Cntrct_Lst_Trans_Dte_A_Pnd_Cntrct_Lst_Trans_Dte_N);                                              //Natural: ASSIGN ISS-LAST-TRANS-DATE := #CNTRCT-LST-TRANS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec_Iss_New_Issue_Flag.setValue("O");                                                                                                     //Natural: ASSIGN ISS-NEW-ISSUE-FLAG := 'O'
                //*  ADDED 4/02
                pnd_Found_Iadp165_Trn_Sw.reset();                                                                                                                         //Natural: RESET #FOUND-IADP165-TRN-SW ISS-TRAN-CDE ( * )
                pnd_Master_Work_Rec_Iss_Tran_Cde.getValue("*").reset();
                if (condition(pnd_Sel_Record.equals("Y") && pnd_End_Of_Iadp165_File.notEquals("Y")))                                                                      //Natural: IF #SEL-RECORD = 'Y' AND #END-OF-IADP165-FILE NE 'Y'
                {
                    //* CHECK IF TRAN EXISTS
                                                                                                                                                                          //Natural: PERFORM READ-IAIQ-SELECTD-TRANS
                    sub_Read_Iaiq_Selectd_Trans();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*      IF  #FOUND-IADP165-TRN-SW = 'Y'
                //*        IF  #IADP165-TRN-CDE = 030 OR= 033
                //*          ISS-NEW-ISSUE-FLAG := 'N'
                //*          ISS-LAST-TRANS-DATE      := #IADP165-TRN-CK-DTE-CCYYMM
                //*        ELSE
                //*          ISS-LAST-TRANS-DATE      := #IADP165-TRN-CK-DTE-CCYYMM
                //*        END-IF
                //*      ELSE
                //*        ISS-LAST-TRANS-DATE      := #CNTRCT-LST-CHNGE-DTE
                //*      END-IF
                //*  REPLACED ABOVE COMMENTED WITH FOLLOWING  4/02
                if (condition(pnd_Found_Iadp165_Trn_Sw.equals("Y")))                                                                                                      //Natural: IF #FOUND-IADP165-TRN-SW = 'Y'
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 #IADP165-TRN-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cnt)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde.getValue(pnd_I).equals(30) || pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde.getValue(pnd_I).equals(33))) //Natural: IF #IADP165-TRN-CDE ( #I ) = 030 OR = 033
                        {
                            pnd_Master_Work_Rec_Iss_New_Issue_Flag.setValue("N");                                                                                         //Natural: ASSIGN ISS-NEW-ISSUE-FLAG := 'N'
                            //*  MANUAL NEW ISSUE 08/05
                            //*  08/05
                            if (condition(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde.getValue(pnd_I).equals(33)))                                                     //Natural: IF #IADP165-TRN-CDE ( #I ) = 033
                            {
                                pnd_Master_Work_Rec_Iss_New_Issue_Flag.setValue("M");                                                                                     //Natural: ASSIGN ISS-NEW-ISSUE-FLAG := 'M'
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Master_Work_Rec_Iss_Last_Trans_Date.setValue(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Ccyymm.getValue(pnd_I));                   //Natural: ASSIGN ISS-LAST-TRANS-DATE := #IADP165-TRN-CK-DTE-CCYYMM ( #I )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Master_Work_Rec_Iss_Tran_Cde.getValue(pnd_I).setValue(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Cde.getValue(pnd_I));                    //Natural: ASSIGN ISS-TRAN-CDE ( #I ) := #IADP165-TRN-CDE ( #I )
                            if (condition(pnd_Master_Work_Rec_Iss_New_Issue_Flag.notEquals("N") && pnd_Master_Work_Rec_Iss_New_Issue_Flag.notEquals("M")))                //Natural: IF ISS-NEW-ISSUE-FLAG NE 'N' AND ISS-NEW-ISSUE-FLAG NE 'M'
                            {
                                pnd_Master_Work_Rec_Iss_Last_Trans_Date.setValue(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Trn_Ck_Dte_Ccyymm.getValue(pnd_I));               //Natural: ASSIGN ISS-LAST-TRANS-DATE := #IADP165-TRN-CK-DTE-CCYYMM ( #I )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Master_Work_Rec_Iss_Last_Trans_Date.setValue(pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte);                                                          //Natural: ASSIGN ISS-LAST-TRANS-DATE := #CNTRCT-LST-CHNGE-DTE
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF REPLACED  4/02
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                    //*  WRITE LEVEL 10 RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
                    sub_Write_Ia_Master_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_10.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-10
                }                                                                                                                                                         //Natural: END-IF
                //*  SET UP LEVEL 20 RECORD
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
                pnd_Master_Work_Rec_Rec_Type_M.setValue(20);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 20
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_X_Ref_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_1_M);                                                                            //Natural: ASSIGN X-REF-1-M := #SVE-X-REF-1-M
                pnd_Master_Work_Rec_Sex_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Sex_1_M);                                                                                //Natural: ASSIGN SEX-1-M := #SVE-SEX-1-M
                pnd_Master_Work_Rec_Dob_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_1_M);                                                                                //Natural: ASSIGN DOB-1-M := #SVE-DOB-1-M
                pnd_Master_Work_Rec_Dod_1_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_1_M);                                                                                //Natural: ASSIGN DOD-1-M := #SVE-DOD-1-M
                pnd_Master_Work_Rec_X_Ref_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_X_Ref_2_M);                                                                            //Natural: ASSIGN X-REF-2-M := #SVE-X-REF-2-M
                pnd_Master_Work_Rec_Sex2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Sex2_M);                                                                                  //Natural: ASSIGN SEX2-M := #SVE-SEX2-M
                pnd_Master_Work_Rec_Dob_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dob_2_M);                                                                                //Natural: ASSIGN DOB-2-M := #SVE-DOB-2-M
                pnd_Master_Work_Rec_Dod_2_M.setValue(pnd_Sve_10_Rec_Data_Pnd_Sve_Dod_2_M);                                                                                //Natural: ASSIGN DOD-2-M := #SVE-DOD-2-M
                pnd_Master_Work_Rec_Ben_Cross_M2.setValue(pnd_Input_Record_Pnd_Bnfcry_Xref);                                                                              //Natural: ASSIGN BEN-CROSS-M2 := #BNFCRY-XREF
                pnd_Master_Work_Rec_Ben_Dod_M2.setValue(pnd_Input_Record_Pnd_Bnfcry_Dod);                                                                                 //Natural: ASSIGN BEN-DOD-M2 := #BNFCRY-DOD
                pnd_Master_Work_Rec_Da_Contract.setValue(pnd_Sve_Orig_Da_Nbr);                                                                                            //Natural: ASSIGN DA-CONTRACT := #SVE-ORIG-DA-NBR
                pnd_Master_Work_Rec_Cash_Code_M2.setValue(pnd_Input_Record_Pnd_Cntrct_Cash_Cde);                                                                          //Natural: ASSIGN CASH-CODE-M2 := #CNTRCT-CASH-CDE
                pnd_Master_Work_Rec_Employ_Code_M2.setValue(pnd_Input_Record_Pnd_Cntrct_Term_Cde);                                                                        //Natural: ASSIGN EMPLOY-CODE-M2 := #CNTRCT-TERM-CDE
                pnd_Master_Work_Rec_Prev_Dist_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Prev_Dist_Cde);                                                                    //Natural: ASSIGN PREV-DIST-CDE := #CNTRCT-PREV-DIST-CDE
                pnd_Master_Work_Rec_Curr_Dist_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde);                                                                    //Natural: ASSIGN CURR-DIST-CDE := #CNTRCT-CURR-DIST-CDE
                pnd_Sve_Ssn_Nbr.setValue(pnd_Input_Record_Pnd_Prtcpnt_Tax_Id_Nbr);                                                                                        //Natural: ASSIGN #SVE-SSN-NBR := #PRTCPNT-TAX-ID-NBR
                if (condition(pnd_Sel_Record.equals("Y")))                                                                                                                //Natural: IF #SEL-RECORD = 'Y'
                {
                    //*  WRITE LEVEL 20 RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
                    sub_Write_Ia_Master_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_20.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-20
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
                //*    VALUE 30
                if (condition(pnd_Sel_Record.notEquals("Y")))                                                                                                             //Natural: IF #SEL-RECORD NE 'Y'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*      SET UP LEVEL 50 RECORDS
                                                                                                                                                                          //Natural: PERFORM READ-PROCESS-FUND-RECORDS
                sub_Read_Process_Fund_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SET UP 70 LEVEL RECORD
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
                pnd_Master_Work_Rec_Us_Seq_Nbr.setValue("001");                                                                                                           //Natural: ASSIGN US-SEQ-NBR := '001'
                pnd_Master_Work_Rec_Us_Tax_Auth.setValue("000");                                                                                                          //Natural: ASSIGN US-TAX-AUTH := '000'
                pnd_Master_Work_Rec_Rec_Type_M.setValue(70);                                                                                                              //Natural: ASSIGN REC-TYPE-M := 70
                pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                              //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                pnd_Master_Work_Rec_Soc_Sec_No_M.setValue(pnd_Sve_Ssn_Nbr);                                                                                               //Natural: ASSIGN SOC-SEC-NO-M := #SVE-SSN-NBR
                pnd_Master_Work_Rec_Us_Stop_Date.setValue(0);                                                                                                             //Natural: ASSIGN US-STOP-DATE := 0
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
                sub_Write_Ia_Master_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Tot_Tran_70.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOT-TRAN-70
                pnd_Master_Work_Rec.reset();                                                                                                                              //Natural: RESET #MASTER-WORK-REC
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Master_Work_Rec.reset();                                                                                                                                      //Natural: RESET #MASTER-WORK-REC
        pnd_Master_Trl_Rec_High_Value.setValue(pnd_W_Hex_High_Value);                                                                                                     //Natural: ASSIGN HIGH-VALUE := #W-HEX-HIGH-VALUE
        //*  HIGH-VALUE  := -255
        pnd_Master_Work_Rec.setValue(pnd_Master_Trl_Rec);                                                                                                                 //Natural: ASSIGN #MASTER-WORK-REC := #MASTER-TRL-REC
        //*  IA MASTER TRAILER RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
        sub_Write_Ia_Master_Extract();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TRAILER RECORD TOTALS TPA, IPRO & P/I FOR CHECK DATE:",pnd_Curr_Check_Date_Ccyymmdd, new ReportEditMask       //Natural: WRITE ( 1 ) / 'TRAILER RECORD TOTALS TPA, IPRO & P/I FOR CHECK DATE:' #CURR-CHECK-DATE-CCYYMMDD ( EM = 9999/99/99 )
            ("9999/99/99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TPA,IPRO & P/I ACT PAYEES.:",new TabSetting(37),pnd_Master_Trl_Rec_Tiaa_Active_Payee_Mitr, new ReportEditMask         //Natural: WRITE ( 1 ) 'TPA,IPRO & P/I ACT PAYEES.:' 37T TIAA-ACTIVE-PAYEE-MITR ( EM = Z,ZZZ,ZZZ )
            ("Z,ZZZ,ZZZ"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA PERIODIC PAYMENT.....:",pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 1 ) 'TIAA PERIODIC PAYMENT.....:' TIAA-PERIODIC-CONT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA PERIODIC DIVIDEND....:",pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 1 ) 'TIAA PERIODIC DIVIDEND....:' TIAA-PERIODIC-DIV-MITR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT........:",pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT........:' TIAA-FINAL-PMT-MITR ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"---------------------------");                                                                                //Natural: WRITE ( 1 ) / '---------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TPA ACTIVE PAYEES.........:",new TabSetting(37),pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr, new ReportEditMask         //Natural: WRITE ( 1 ) 'TPA ACTIVE PAYEES.........:' 37T TPA-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZZ )
            ("Z,ZZZ,ZZZ"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TPA PERIODIC PAYMENT......:",pnd_Master_Trl_Rec_Tpa_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'TPA PERIODIC PAYMENT......:' TPA-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TPA DIVIDEND PAYMENT......:",pnd_Master_Trl_Rec_Tpa_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'TPA DIVIDEND PAYMENT......:' TPA-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"---------------------------");                                                                                //Natural: WRITE ( 1 ) / '---------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO ACTIVE PAYEES........:",new TabSetting(37),pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr, new ReportEditMask        //Natural: WRITE ( 1 ) 'IPRO ACTIVE PAYEES........:' 37T IPRO-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZZ )
            ("Z,ZZZ,ZZZ"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO PERIODIC PAYMENT.....:",pnd_Master_Trl_Rec_Ipro_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));              //Natural: WRITE ( 1 ) 'IPRO PERIODIC PAYMENT.....:' IPRO-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO DIVIDEND PAYMENT.....:",pnd_Master_Trl_Rec_Ipro_Per_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));              //Natural: WRITE ( 1 ) 'IPRO DIVIDEND PAYMENT.....:' IPRO-PER-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO FINAL PAYMENT........:",pnd_Master_Trl_Rec_Ipro_Fin_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));              //Natural: WRITE ( 1 ) 'IPRO FINAL PAYMENT........:' IPRO-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"---------------------------");                                                                                //Natural: WRITE ( 1 ) / '---------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"P&I  ACTIVE PAYEES........:",new TabSetting(37),pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr, new ReportEditMask         //Natural: WRITE ( 1 ) 'P&I  ACTIVE PAYEES........:' 37T P-I-ACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZZ )
            ("Z,ZZZ,ZZZ"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"IPRO PERIODIC PAYMENT.....:",pnd_Master_Trl_Rec_P_I_Per_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'IPRO PERIODIC PAYMENT.....:' P-I-PER-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"P&I  DIVIDEND PAYMENT.....:",pnd_Master_Trl_Rec_P_I_Per_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'P&I  DIVIDEND PAYMENT.....:' P-I-PER-DIV ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"P&I  FINAL PAYMENT........:",pnd_Master_Trl_Rec_P_I_Fin_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'P&I  FINAL PAYMENT........:' P-I-FIN-PMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"INACTIVE PAYEES...........:",new TabSetting(37),pnd_Master_Trl_Rec_Inactive_Payees_Mitr, new ReportEditMask           //Natural: WRITE ( 1 ) 'INACTIVE PAYEES...........:' 37T INACTIVE-PAYEES-MITR ( EM = Z,ZZZ,ZZZ ) ///
            ("Z,ZZZ,ZZZ"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 10 RECORDS ----------------->",pnd_Tot_Tran_10, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 10 RECORDS ----------------->' #TOT-TRAN-10 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 20 RECORDS ----------------->",pnd_Tot_Tran_20, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 20 RECORDS ----------------->' #TOT-TRAN-20 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 50 RECORDS ----------------->",pnd_Tot_Tran_50, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 50 RECORDS ----------------->' #TOT-TRAN-50 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL LEVEL 70 RECORDS ----------------->",pnd_Tot_Tran_70, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) ' TOTAL LEVEL 70 RECORDS ----------------->' #TOT-TRAN-70 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL MASTER RECORDS GENERATED --------->",pnd_Tot_Trans_Out, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' TOTAL MASTER RECORDS GENERATED --------->' #TOT-TRANS-OUT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," Total iaiq transaction records input---->",pnd_Iadp165_Rec_In, new ReportEditMask ("ZZ,ZZ9"));                       //Natural: WRITE ( 1 ) ' Total iaiq transaction records input---->' #IADP165-REC-IN ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," Total transaction records matched------->",pnd_Matched_Iadp165_Rec_In, new ReportEditMask ("ZZ,ZZ9"));               //Natural: WRITE ( 1 ) ' Total transaction records matched------->' #MATCHED-IADP165-REC-IN ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," Total iaiq contracts bypassed no IAA-CNTRCT record found",pnd_Cntrcts_Not_Selct, new ReportEditMask                  //Natural: WRITE ( 1 ) ' Total iaiq contracts bypassed no IAA-CNTRCT record found' #CNTRCTS-NOT-SELCT ( EM = ZZ,ZZ9 )
            ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE ( 1 ) / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        //*  REMOVE FOLLOWING WRITES AFTER CONVERSION 7/01
        getReports().write(1, ReportOption.NOTITLE," Total new iafl new-issu-contracts input---->",pnd_New_Issus_In, new ReportEditMask ("ZZ,ZZ9"));                      //Natural: WRITE ( 1 ) ' Total new iafl new-issu-contracts input---->' #NEW-ISSUS-IN ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," Total new iafl new-issu-contracts matched-->",pnd_Matched_New_Issu_Rec_In, new ReportEditMask ("ZZ,ZZ9"));           //Natural: WRITE ( 1 ) ' Total new iafl new-issu-contracts matched-->' #MATCHED-NEW-ISSU-REC-IN ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  END OF REMOVE FOLLOWING WRITES AFTER CONVERSION 7/01
        //* *****************************************************************                                                                                             //Natural: ON ERROR
        //*  READ TRANSACTION EXTRACT CREATED BY IADP165 FROM IAA-TRANS-RCRD
        //*  #SVE-CNTRCT-NBR =     INPUT MASTER EXTRACT CONTRACT-NBR
        //*  #IADP165-CNTRCT-NBR = INPUT IAIQ TRANSACTION EXTRACT CNTRCT-NBR
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAIQ-SELECTD-TRANS
        //* ****************************************************************
        //* *     PROCESS TIAA FUND RECORDS
        //* *
        //* *      WRITE RATE LVL 50 RECORDS
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PROCESS-FUND-RECORDS
        //*   #TIAA-FUND-BFRE-KEY
        //*   #TRANS-DTE
        //*      RATE-RATE     (#I2)  :=  TIAA-RATE-CDE-N (#I)
        //*  CHANGED ABOVE TO FOLLOWING TO HANDLE ALPHA RATES 4/03
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LAST-TRANS-DTE
        //*  ----------------------------------------------
        //*  WRITE IA MASTER EXTRACT OLD FORMAT
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IA-MASTER-EXTRACT
        //*  ----------------------------------------------
        //*  ABORT JOB MORE THAN 90 RATES INPUT MUST FIX
        //*   OLD RATE RECORD TO HANDLE MORE THAN 90 RATES
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ABORT-JOB
    }
    private void sub_Read_Iaiq_Selectd_Trans() throws Exception                                                                                                           //Natural: READ-IAIQ-SELECTD-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Found_Iadp165_Trn_Sw.reset();                                                                                                                                 //Natural: RESET #FOUND-IADP165-TRN-SW
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Sve_Cntrct_Payee_Nbr.less(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr)))                                                        //Natural: IF #SVE-CNTRCT-PAYEE-NBR LT #IADP165-CNTRCT-PAYEE-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Cntrct_Payee_Nbr.equals(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Payee_Nbr) && pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde.equals(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Payee_Cde))) //Natural: IF #SVE-CNTRCT-PAYEE-NBR = #IADP165-CNTRCT-PAYEE-NBR AND #SVE-PAYEE-CDE = #IADP165-PAYEE-CDE
            {
                pnd_Found_Iadp165_Trn_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #FOUND-IADP165-TRN-SW := 'Y'
                pnd_Matched_Iadp165_Rec_In.nadd(1);                                                                                                                       //Natural: ADD 1 TO #MATCHED-IADP165-REC-IN
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(4, pnd_Iadp165_Selct_Trans_In);                                                                                                           //Natural: READ WORK FILE 4 ONCE #IADP165-SELCT-TRANS-IN
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_End_Of_Iadp165_File.setValue("Y");                                                                                                                    //Natural: ASSIGN #END-OF-IADP165-FILE := 'Y'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-ENDFILE
            pnd_Iadp165_Rec_In.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #IADP165-REC-IN
            if (condition(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr.less(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr)))                                           //Natural: IF #SVE-CNTRCT-NBR LT #IADP165-CNTRCT-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr.equals(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Cntrct_Nbr) && pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde.equals(pnd_Iadp165_Selct_Trans_In_Pnd_Iadp165_Payee_Cde))) //Natural: IF #SVE-CNTRCT-NBR = #IADP165-CNTRCT-NBR AND #SVE-PAYEE-CDE = #IADP165-PAYEE-CDE
                {
                    pnd_Found_Iadp165_Trn_Sw.setValue("Y");                                                                                                               //Natural: ASSIGN #FOUND-IADP165-TRN-SW := 'Y'
                    pnd_Matched_Iadp165_Rec_In.nadd(1);                                                                                                                   //Natural: ADD 1 TO #MATCHED-IADP165-REC-IN
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  END-WORK
    }
    private void sub_Read_Process_Fund_Records() throws Exception                                                                                                         //Natural: READ-PROCESS-FUND-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr);                                                                                 //Natural: ASSIGN #FUND-PPCN := #SVE-CNTRCT-NBR
        pnd_Fund_Key_Pnd_Fund_Paye.setValue(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde);                                                                                  //Natural: ASSIGN #FUND-PAYE := #SVE-PAYEE-CDE
        pnd_Fund_Key_Pnd_Fund_Cde.setValue("   ");                                                                                                                        //Natural: ASSIGN #FUND-CDE := '   '
        pnd_Master_Work_Rec_Rec_Type_M.setValue(50);                                                                                                                      //Natural: ASSIGN REC-TYPE-M := 50
        pnd_Rte_Lvl_Nbr.setValue(50);                                                                                                                                     //Natural: ASSIGN #RTE-LVL-NBR := 50
        pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Cntrct_Nbr);                                                                          //Natural: ASSIGN CONTRACT-NO-M := #SVE-CNTRCT-NBR
        pnd_Master_Work_Rec_Payee_M.setValue(pnd_Sve_Cntrct_Payee_Nbr_Pnd_Sve_Payee_Cde);                                                                                 //Natural: ASSIGN PAYEE-M := #SVE-PAYEE-CDE
        pnd_Master_Work_Rec.reset();                                                                                                                                      //Natural: RESET #MASTER-WORK-REC
        pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div.reset();                                                                                                                //Natural: RESET RATE-TOT-OLD-TIAA-DIV RATE-TOT-OLD-TIAA-PMT RATE-RATE ( * ) RATE-PER-PAY ( * ) RATE-PER-DIV ( * ) RATE-FINAL-PAY ( * ) RATE-DATE ( * ) RATE-TRANS-CODE RATE-TRANS-DATE
        pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt.reset();
        pnd_Master_Work_Rec_Rate_Rate.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Per_Pay.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Per_Div.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Final_Pay.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Date.getValue("*").reset();
        pnd_Master_Work_Rec_Rate_Trans_Code.reset();
        pnd_Master_Work_Rec_Rate_Trans_Date.reset();
        pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                                 //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
        pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
        pnd_Master_Work_Rec_Rec_Type_M.setValue(pnd_I);                                                                                                                   //Natural: ASSIGN REC-TYPE-M := #I
        //*  RATE-TOT-OLD-TIAA-DIV := TIAA-OLD-DIV-AMT /* COMMENTED 1/15/02
        //*  RATE-TOT-OLD-TIAA-PMT := TIAA-OLD-PER-AMT /* COMMENTED 1/15/02
        vw_fund_Rcrd.startDatabaseRead                                                                                                                                    //Natural: READ FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
        (
        "READ02",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_fund_Rcrd.readNextRow("READ02")))
        {
            if (condition(pnd_Fund_Key_Pnd_Fund_Ppcn.notEquals(fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr) || pnd_Fund_Key_Pnd_Fund_Paye.notEquals(fund_Rcrd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF #FUND-PPCN NE FUND-RCRD.TIAA-CNTRCT-PPCN-NBR OR #FUND-PAYE NE FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  INSERTED HERE 5/02
            if (condition(fund_Rcrd_Tiaa_Cmpny_Cde.notEquals("T")))                                                                                                       //Natural: IF FUND-RCRD.TIAA-CMPNY-CDE NE 'T'
            {
                getReports().write(0, " TIAA-CMPNY-CDE NE TO T","=",fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr,"=",fund_Rcrd_Tiaa_Cmpny_Cde);                                         //Natural: WRITE ' TIAA-CMPNY-CDE NE TO T' '=' FUND-RCRD.TIAA-CNTRCT-PPCN-NBR '=' FUND-RCRD.TIAA-CMPNY-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF INSERTED HERE 5/02
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO FUND-RCRD.C*TIAA-RATE-DATA-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(fund_Rcrd_Count_Casttiaa_Rate_Data_Grp)); pnd_I.nadd(1))
            {
                if (condition(DbsUtil.maskMatches(fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I),"'  '") || DbsUtil.maskMatches(fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I),         //Natural: IF FUND-RCRD.TIAA-RATE-CDE ( #I ) = MASK ( '  ' ) OR = MASK ( ' ' )
                    "' '")))
                {
                    pnd_Blank_Rate.setValue("Y");                                                                                                                         //Natural: ASSIGN #BLANK-RATE := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  LVL-NBR 50 ALL RATE RECORDS 4/03
                }                                                                                                                                                         //Natural: END-IF
                pnd_Master_Work_Rec_Rec_Type_M.setValue(pnd_Rte_Lvl_Nbr);                                                                                                 //Natural: ASSIGN REC-TYPE-M := #RTE-LVL-NBR
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I2
                //*  ADD FOLLOWING 01/02
                //*  MOVE OLD-PMT-AMTS INTO FIRST RATE RECORD ONLY  50 REC-TYPE 4/03
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div.setValue(fund_Rcrd_Tiaa_Old_Div_Amt);                                                                       //Natural: ASSIGN RATE-TOT-OLD-TIAA-DIV := FUND-RCRD.TIAA-OLD-DIV-AMT
                    pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt.setValue(fund_Rcrd_Tiaa_Old_Per_Amt);                                                                       //Natural: ASSIGN RATE-TOT-OLD-TIAA-PMT := FUND-RCRD.TIAA-OLD-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD 01/02
                if (condition(! (DbsUtil.maskMatches(fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I),"'  '"))))                                                                   //Natural: IF FUND-RCRD.TIAA-RATE-CDE ( #I ) NE MASK ( '  ' )
                {
                    pnd_Master_Work_Rec_Rate_Rate.getValue(pnd_I2).setValue(fund_Rcrd_Tiaa_Rate_Cde.getValue(pnd_I));                                                     //Natural: ASSIGN RATE-RATE ( #I2 ) := FUND-RCRD.TIAA-RATE-CDE ( #I )
                    pnd_Master_Work_Rec_Rate_Per_Pay.getValue(pnd_I2).setValue(fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I));                                               //Natural: ASSIGN RATE-PER-PAY ( #I2 ) := FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                    pnd_Master_Work_Rec_Rate_Per_Div.getValue(pnd_I2).setValue(fund_Rcrd_Tiaa_Per_Div_Amt.getValue(pnd_I));                                               //Natural: ASSIGN RATE-PER-DIV ( #I2 ) := FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                    pnd_Master_Work_Rec_Rate_Final_Pay.getValue(pnd_I2).setValue(fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                      //Natural: ASSIGN RATE-FINAL-PAY ( #I2 ) := FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                    //* *
                    //* * ADDED FOLLOWING 1/02 TO MOVE IN RATE-DATE IF EXISTS FOR CALC
                    //* *                 OF TPA COMMUTED VALUE  BY ACTUARY
                    if (condition(fund_Rcrd_Tiaa_Rate_Dte.getValue(pnd_I).greater(getZero())))                                                                            //Natural: IF FUND-RCRD.TIAA-RATE-DTE ( #I ) GT 0
                    {
                        pnd_Rate_Date_A8.setValueEdited(fund_Rcrd_Tiaa_Rate_Dte.getValue(pnd_I),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED FUND-RCRD.TIAA-RATE-DTE ( #I ) ( EM = YYYYMMDD ) TO #RATE-DATE-A8
                        //*        MOVE TIAA-RATE-DTE (#I)   TO  RATE-DATE (#I2)
                        //*  CHANGED ABOVE TO FOLLOWING 4/03
                        pnd_Master_Work_Rec_Rate_Date.getValue(pnd_I2).setValue(pnd_Rate_Date_A8_Pnd_Rate_Date_N6);                                                       //Natural: MOVE #RATE-DATE-N6 TO RATE-DATE ( #I2 )
                        if (condition(pnd_Rate_Date_A8_Pnd_Rate_Date_N6.greaterOrEqual(pnd_Input_Record_Pnd_W_Check_Date)))                                               //Natural: IF #RATE-DATE-N6 GE #W-CHECK-DATE
                        {
                            pnd_Master_Work_Rec_Rate_Date.getValue(pnd_I2).reset();                                                                                       //Natural: RESET RATE-DATE ( #I2 )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //* * END OF ADD 1/02
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_I2.equals(3)))                                                                                                                          //Natural: IF #I2 = 3
                {
                    //*  WRITE LEVEL 50 RECORDS
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
                    sub_Write_Ia_Master_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tot_Tran_50.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOT-TRAN-50
                    pnd_I2.reset();                                                                                                                                       //Natural: RESET #I2
                    //*  ALL RATE-RECORDS LVL 50
                    pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Div.reset();                                                                                                    //Natural: RESET RATE-TOT-OLD-TIAA-DIV RATE-TOT-OLD-TIAA-PMT RATE-RATE ( * ) RATE-PER-PAY ( * ) RATE-PER-DIV ( * ) RATE-FINAL-PAY ( * ) RATE-DATE ( * ) RATE-TRANS-CODE RATE-TRANS-DATE
                    pnd_Master_Work_Rec_Rate_Tot_Old_Tiaa_Pmt.reset();
                    pnd_Master_Work_Rec_Rate_Rate.getValue("*").reset();
                    pnd_Master_Work_Rec_Rate_Per_Pay.getValue("*").reset();
                    pnd_Master_Work_Rec_Rate_Per_Div.getValue("*").reset();
                    pnd_Master_Work_Rec_Rate_Final_Pay.getValue("*").reset();
                    pnd_Master_Work_Rec_Rate_Date.getValue("*").reset();
                    pnd_Master_Work_Rec_Rate_Trans_Code.reset();
                    pnd_Master_Work_Rec_Rate_Trans_Date.reset();
                    pnd_Master_Work_Rec_Contract_No_M.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                     //Natural: ASSIGN CONTRACT-NO-M := #CNTRCT-PPCN-NBR
                    pnd_Master_Work_Rec_Payee_M.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                          //Natural: ASSIGN PAYEE-M := #CNTRCT-PAYEE-CDE
                    pnd_Master_Work_Rec_Rec_Type_M.setValue(pnd_Rte_Lvl_Nbr);                                                                                             //Natural: ASSIGN REC-TYPE-M := #RTE-LVL-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF  RATE-RATE (1) NE  0
            //*  CHANGED FROM NE 0 TO NE '  ' HANDLE ALPHA RATES 4/03
            if (condition(pnd_Master_Work_Rec_Rate_Rate.getValue(1).notEquals("  ")))                                                                                     //Natural: IF RATE-RATE ( 1 ) NE '  '
            {
                //*  WRITE LEVEL 50 RECORDS
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-MASTER-EXTRACT
                sub_Write_Ia_Master_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Tot_Tran_50.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOT-TRAN-50
            }                                                                                                                                                             //Natural: END-IF
            //*  CHANGED TO 90 4/03
            if (condition(pnd_I.greater(90)))                                                                                                                             //Natural: IF #I GT 90
            {
                                                                                                                                                                          //Natural: PERFORM ABORT-JOB
                sub_Abort_Job();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Delete_Cde.notEquals(9)))                                                                                                               //Natural: IF #SVE-DELETE-CDE NE 9
            {
                pnd_Master_Trl_Rec_Tiaa_Periodic_Cont_Mitr.nadd(fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                                                //Natural: ADD FUND-RCRD.TIAA-PER-PAY-AMT ( * ) TO TIAA-PERIODIC-CONT-MITR
                pnd_Master_Trl_Rec_Tiaa_Periodic_Div_Mitr.nadd(fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                                                 //Natural: ADD FUND-RCRD.TIAA-PER-DIV-AMT ( * ) TO TIAA-PERIODIC-DIV-MITR
                pnd_Master_Trl_Rec_Tiaa_Final_Pmt_Mitr.nadd(fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                             //Natural: ADD FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) TO TIAA-FINAL-PMT-MITR
                if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(28) || pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(30)))                                                  //Natural: IF #SVE-OPT = 28 OR = 30
                {
                    pnd_Master_Trl_Rec_Tpa_Active_Payees_Mitr.nadd(1);                                                                                                    //Natural: ADD 1 TO TPA-ACTIVE-PAYEES-MITR
                    pnd_Master_Trl_Rec_Tpa_Per_Amt.nadd(fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                                                        //Natural: ADD FUND-RCRD.TIAA-PER-PAY-AMT ( * ) TO TPA-PER-AMT
                    pnd_Master_Trl_Rec_Tpa_Div_Amt.nadd(fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                                                        //Natural: ADD FUND-RCRD.TIAA-PER-DIV-AMT ( * ) TO TPA-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(25) || pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(27)))                                              //Natural: IF #SVE-OPT = 25 OR = 27
                    {
                        pnd_Master_Trl_Rec_Ipro_Active_Payees_Mitr.nadd(1);                                                                                               //Natural: ADD 1 TO IPRO-ACTIVE-PAYEES-MITR
                        pnd_Master_Trl_Rec_Ipro_Per_Amt.nadd(fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                                                   //Natural: ADD FUND-RCRD.TIAA-PER-PAY-AMT ( * ) TO IPRO-PER-AMT
                        pnd_Master_Trl_Rec_Ipro_Per_Div.nadd(fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                                                   //Natural: ADD FUND-RCRD.TIAA-PER-DIV-AMT ( * ) TO IPRO-PER-DIV
                        pnd_Master_Trl_Rec_Ipro_Fin_Pmt.nadd(fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                            //Natural: ADD FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) TO IPRO-FIN-PMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Opt.equals(22)))                                                                                        //Natural: IF #SVE-OPT = 22
                        {
                            pnd_Master_Trl_Rec_P_I_Active_Payees_Mitr.nadd(1);                                                                                            //Natural: ADD 1 TO P-I-ACTIVE-PAYEES-MITR
                            pnd_Master_Trl_Rec_P_I_Per_Amt.nadd(fund_Rcrd_Tiaa_Per_Pay_Amt.getValue("*"));                                                                //Natural: ADD FUND-RCRD.TIAA-PER-PAY-AMT ( * ) TO P-I-PER-AMT
                            pnd_Master_Trl_Rec_P_I_Per_Div.nadd(fund_Rcrd_Tiaa_Per_Div_Amt.getValue("*"));                                                                //Natural: ADD FUND-RCRD.TIAA-PER-DIV-AMT ( * ) TO P-I-PER-DIV
                            pnd_Master_Trl_Rec_P_I_Fin_Pmt.nadd(fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                         //Natural: ADD FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) TO P-I-FIN-PMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Last_Trans_Dte() throws Exception                                                                                                                //Natural: GET-LAST-TRANS-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  04/06 FOR PARTIAL DRAWDOWN AND
        if (condition(pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.equals("P")))                                                                                               //Natural: IF #SVE-CNTRCT-TYPE = 'P'
        {
            //*  DUE FOR PAYMENT ONLY
            if (condition(pnd_Sve_Mode.equals(pnd_Due_Mode) || pnd_Sve_Mode.equals(100)))                                                                                 //Natural: IF #SVE-MODE = #DUE-MODE OR = 100
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.reset();                                                                                                          //Natural: RESET #SVE-CNTRCT-TYPE ISS-CONTR-TYPE-CD
                pnd_Master_Work_Rec_Iss_Contr_Type_Cd.reset();
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr);                                                                           //Natural: ASSIGN #TRANS-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(pnd_Input_Record_Pnd_Cntrct_Payee_Cde);                                                                         //Natural: ASSIGN #TRANS-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Rate_Date_A8.reset();                                                                                                                                         //Natural: RESET #RATE-DATE-A8
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_trans.readNextRow("READ03")))
        {
            if (condition(trans_Trans_Ppcn_Nbr.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr) || trans_Trans_Payee_Cde.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR NE #TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  04/06 ONLY IF DRAWDOWN
            //*  05/06 MANUAL DRAWDOWN
            //*  DURING CYCLE
            if (condition(!((trans_Trans_User_Area.equals("SNGD") || trans_Trans_Sub_Cde.equals("OMN")) && trans_Trans_Check_Dte.greaterOrEqual(pnd_Prev_Check_Date_Ccyymmdd)))) //Natural: ACCEPT IF ( TRANS-USER-AREA = 'SNGD' OR TRANS-SUB-CDE = 'OMN' ) AND TRANS-CHECK-DTE GE #PREV-CHECK-DATE-CCYYMMDD
            {
                continue;
            }
            pnd_Rate_Date_A8.setValueEdited(trans_Trans_Check_Dte,new ReportEditMask("99999999"));                                                                        //Natural: MOVE EDITED TRANS-CHECK-DTE ( EM = 99999999 ) TO #RATE-DATE-A8
            pnd_Input_Record_Pnd_Cntrct_Lst_Chnge_Dte.setValue(pnd_Rate_Date_A8_Pnd_Rate_Date_N6);                                                                        //Natural: ASSIGN #CNTRCT-LST-CHNGE-DTE := #RATE-DATE-N6
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NO DRAWDOWN DURING CYCLE
        if (condition(pnd_Rate_Date_A8.equals(" ")))                                                                                                                      //Natural: IF #RATE-DATE-A8 = ' '
        {
            //*  RESET DRAWDOWN FLAG
            pnd_Sve_10_Rec_Data_Pnd_Sve_Cntrct_Type.reset();                                                                                                              //Natural: RESET #SVE-CNTRCT-TYPE ISS-CONTR-TYPE-CD
            pnd_Master_Work_Rec_Iss_Contr_Type_Cd.reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Ia_Master_Extract() throws Exception                                                                                                           //Natural: WRITE-IA-MASTER-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(2, false, pnd_Master_Work_Rec);                                                                                                              //Natural: WRITE WORK FILE 2 #MASTER-WORK-REC
        pnd_Tot_Trans_Out.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-TRANS-OUT
    }
    private void sub_Abort_Job() throws Exception                                                                                                                         //Natural: ABORT-JOB
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "*** Program--->",Global.getPROGRAM(),NEWLINE,"*** Aborting job more than  90 rates input *************",NEWLINE,"*** For rate record ************************************", //Natural: WRITE '*** Program--->' *PROGRAM / '*** Aborting job more than  90 rates input *************' / '*** For rate record ************************************' / '*** Contract number-->' #CNTRCT-PPCN-NBR / '*** Payee-cde ------->' #CNTRCT-PAYEE-CDE /
            NEWLINE,"*** Contract number-->",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,NEWLINE,"*** Payee-cde ------->",pnd_Input_Record_Pnd_Cntrct_Payee_Cde,
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"*** Program--->",Global.getPROGRAM(),NEWLINE,"*** Aborting job more than  90 rates input *************",              //Natural: WRITE ( 1 ) '*** Program--->' *PROGRAM / '*** Aborting job more than  90 rates input *************' / '*** For rate record ************************************' / '*** Contract number-->' #CNTRCT-PPCN-NBR / '*** Payee-cde ------->' #CNTRCT-PAYEE-CDE /
            NEWLINE,"*** For rate record ************************************",NEWLINE,"*** Contract number-->",pnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr,NEWLINE,
            "*** Payee-cde ------->",pnd_Input_Record_Pnd_Cntrct_Payee_Cde,NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(16);  if (true) return;                                                                                                                         //Natural: TERMINATE 16
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IA ADMIN OLD IA MASTER RECORDS GENERATED FOR QTRLY REPORTING FOR CHECK DATE: ",pnd_Curr_Check_Date_Ccyymmdd,  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IA ADMIN OLD IA MASTER RECORDS GENERATED FOR QTRLY REPORTING FOR CHECK DATE: ' #CURR-CHECK-DATE-CCYYMMDD ( EM = 9999/99/99 ) 4X 'PAGE: ' #W-PAGE-CTR ( EM = Z9 )
                        new ReportEditMask ("9999/99/99"),new ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("Z9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,"                                                      ");                       //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT '                                                      '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                       //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
