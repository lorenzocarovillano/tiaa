/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:56 PM
**        * FROM NATURAL PROGRAM : Iaap3029
************************************************************
**        * FILE NAME            : Iaap3029.java
**        * CLASS NAME           : Iaap3029
**        * INSTANCE NAME        : Iaap3029
************************************************************
************************************************************************
*  PROGRAM NAME  :  IAAP3029                                           *
*  DESCRIPTION   :  NET CHANGE DATA EXTRACTION                         *
*  DATE          :  FEB 1998                                           *
*  DESCRIPTION   :  INPUT IS SORTED BY :                               *
*                       PIN, PAYEE-CODE AND CONTRACT NBR               *
*                   OUTPUT FILES                                       *
*                         CMWKF21   - 1 CONTRACT                       *
*                         CMWKF22   - 2 CONTRACTS                      *
*                         CMWKF23   - 3                                *
*                         CMWKF24   - 4                                *
*                         CMWKF25   - > 4                              *
*                         CMWKF26   - HELD RECORDS                     *
*                         CMWKF27   - FOREIGN CONTRACTS                *
*                         CMWKF28   - DOMESTIC LABELS                  *
*                         CMWKF29   - FOREIGN LABELS                   *
*                         CMWKF30   - FOREIGN LATS                     *
*                                                                      *
*  UPDATES       :  SORT HELD CONTRACTS BY HOLD-CODE                   *
*                :  COPIED FROM ISLE3029 - TRAILERS REMOVED AND RESTOWD*
*                                                                      *
*                                                                      *
************************************************************************
* HISTORY
* 05/02/01  MARIO         - CHANGE THE DESCRIPTION 'AFAPLINT FILES ARE
*                         CLOSED' to 'AFAPLINT FILES HAVE BEEN CLOSED
*                         SUCCESSFULLY'.
*                         - ADD THE DESCRIPTION 'AFAPLINT FILES HAVE
*                         BEEN OPENED SUCCESSFULLY.'
* 05/14/01  MARIO         - RESOLVE 0C7 PROBLEM IF TEST CASE INVOLVES
*                         MESSAGE # 05 OR 06 - CHANGE IAAA029A PER
*                         JULIANNE BOCKIUS' rec layout
* 03/05/09  J. OSTEEN     - CONVERTED TO REMOVE ALL AFAPLINT FILES AND
*                         CALLS AND WRITE NEW WORK FILES TO BE PRINTED
*                         USING ECS.
* 08/10/15  RAHUL         - RESTOW THIS MODULE FOR IAAA029B
* 04/21/17  SAI K         -RESTOW THIS MODULE FOR IAAA029B PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3029 extends BLNatBase
{
    // Data Areas
    private LdaIaaa029a ldaIaaa029a;
    private LdaIaaa029b ldaIaaa029b;
    private LdaIaaa029c ldaIaaa029c;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_C1;
    private DbsField pnd_Ones_Count;
    private DbsField pnd_Twos_Count;
    private DbsField pnd_Threes_Count;
    private DbsField pnd_Fours_Count;
    private DbsField pnd_Flatd_Count;
    private DbsField pnd_Flatf_Count;
    private DbsField pnd_Trif_Count;
    private DbsField pnd_Labeld_Count;
    private DbsField pnd_Labelf_Count;
    private DbsField pnd_Hold_Count;
    private DbsField pnd_Trail_Blank;
    private DbsField pnd_Ctrl_Record;

    private DbsGroup pnd_Ctrl_Record__R_Field_1;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Date;

    private DbsGroup pnd_Ctrl_Record__R_Field_2;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Yyyy;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Mm;
    private DbsField pnd_Ctrl_Record_Pnd_Cr_Run_Dd;

    private DbsGroup other_Variables;
    private DbsField other_Variables_Pnd_Ws_Date;

    private DbsGroup other_Variables__R_Field_3;
    private DbsField other_Variables_Pnd_Ws_Date_Mmddyyyy;
    private DbsField other_Variables_Pnd_Date;
    private DbsField other_Variables_Pnd_Ws_First_Pass;
    private DbsField other_Variables_Pnd_Ws_Save_Pin;
    private DbsField other_Variables_Pnd_Ws_Save_Payee_Code;
    private DbsField other_Variables_Pnd_Ws_Contract_Nbr;

    private DbsGroup other_Variables__R_Field_4;
    private DbsField other_Variables_Pnd_Ws_Contract_Nbr_7;
    private DbsField other_Variables_Pnd_Ws_Contract_Check_Digit;
    private DbsField other_Variables_Pnd_Ws_Payee_Cde_4;

    private DbsGroup other_Variables__R_Field_5;
    private DbsField other_Variables_Pnd_Ws_Payee_Cde;
    private DbsField other_Variables_Pnd_I;

    private DbsGroup constants;
    private DbsField constants_Pnd_Lo_Values;
    private DbsField constants_Pnd_Hi_Values;

    private DbsGroup pnd_Counter_Request_Id;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_For_Hld_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr;
    private DbsField pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaaa029a = new LdaIaaa029a();
        registerRecord(ldaIaaa029a);
        ldaIaaa029b = new LdaIaaa029b();
        registerRecord(ldaIaaa029b);
        ldaIaaa029c = new LdaIaaa029c();
        registerRecord(ldaIaaa029c);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_C1 = localVariables.newFieldInRecord("pnd_C1", "#C1", FieldType.NUMERIC, 10);
        pnd_Ones_Count = localVariables.newFieldInRecord("pnd_Ones_Count", "#ONES-COUNT", FieldType.NUMERIC, 7);
        pnd_Twos_Count = localVariables.newFieldInRecord("pnd_Twos_Count", "#TWOS-COUNT", FieldType.NUMERIC, 7);
        pnd_Threes_Count = localVariables.newFieldInRecord("pnd_Threes_Count", "#THREES-COUNT", FieldType.NUMERIC, 7);
        pnd_Fours_Count = localVariables.newFieldInRecord("pnd_Fours_Count", "#FOURS-COUNT", FieldType.NUMERIC, 7);
        pnd_Flatd_Count = localVariables.newFieldInRecord("pnd_Flatd_Count", "#FLATD-COUNT", FieldType.NUMERIC, 7);
        pnd_Flatf_Count = localVariables.newFieldInRecord("pnd_Flatf_Count", "#FLATF-COUNT", FieldType.NUMERIC, 7);
        pnd_Trif_Count = localVariables.newFieldInRecord("pnd_Trif_Count", "#TRIF-COUNT", FieldType.NUMERIC, 7);
        pnd_Labeld_Count = localVariables.newFieldInRecord("pnd_Labeld_Count", "#LABELD-COUNT", FieldType.NUMERIC, 7);
        pnd_Labelf_Count = localVariables.newFieldInRecord("pnd_Labelf_Count", "#LABELF-COUNT", FieldType.NUMERIC, 7);
        pnd_Hold_Count = localVariables.newFieldInRecord("pnd_Hold_Count", "#HOLD-COUNT", FieldType.NUMERIC, 7);
        pnd_Trail_Blank = localVariables.newFieldArrayInRecord("pnd_Trail_Blank", "#TRAIL-BLANK", FieldType.STRING, 1, new DbsArrayController(1, 388));
        pnd_Ctrl_Record = localVariables.newFieldInRecord("pnd_Ctrl_Record", "#CTRL-RECORD", FieldType.STRING, 8);

        pnd_Ctrl_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Ctrl_Record__R_Field_1", "REDEFINE", pnd_Ctrl_Record);
        pnd_Ctrl_Record_Pnd_Cr_Run_Date = pnd_Ctrl_Record__R_Field_1.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Date", "#CR-RUN-DATE", FieldType.NUMERIC, 
            8);

        pnd_Ctrl_Record__R_Field_2 = pnd_Ctrl_Record__R_Field_1.newGroupInGroup("pnd_Ctrl_Record__R_Field_2", "REDEFINE", pnd_Ctrl_Record_Pnd_Cr_Run_Date);
        pnd_Ctrl_Record_Pnd_Cr_Run_Yyyy = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Yyyy", "#CR-RUN-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Ctrl_Record_Pnd_Cr_Run_Mm = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Mm", "#CR-RUN-MM", FieldType.NUMERIC, 2);
        pnd_Ctrl_Record_Pnd_Cr_Run_Dd = pnd_Ctrl_Record__R_Field_2.newFieldInGroup("pnd_Ctrl_Record_Pnd_Cr_Run_Dd", "#CR-RUN-DD", FieldType.NUMERIC, 2);

        other_Variables = localVariables.newGroupInRecord("other_Variables", "OTHER-VARIABLES");
        other_Variables_Pnd_Ws_Date = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 8);

        other_Variables__R_Field_3 = other_Variables.newGroupInGroup("other_Variables__R_Field_3", "REDEFINE", other_Variables_Pnd_Ws_Date);
        other_Variables_Pnd_Ws_Date_Mmddyyyy = other_Variables__R_Field_3.newFieldInGroup("other_Variables_Pnd_Ws_Date_Mmddyyyy", "#WS-DATE-MMDDYYYY", 
            FieldType.NUMERIC, 8);
        other_Variables_Pnd_Date = other_Variables.newFieldInGroup("other_Variables_Pnd_Date", "#DATE", FieldType.DATE);
        other_Variables_Pnd_Ws_First_Pass = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_First_Pass", "#WS-FIRST-PASS", FieldType.BOOLEAN, 
            1);
        other_Variables_Pnd_Ws_Save_Pin = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_Save_Pin", "#WS-SAVE-PIN", FieldType.NUMERIC, 12);
        other_Variables_Pnd_Ws_Save_Payee_Code = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_Save_Payee_Code", "#WS-SAVE-PAYEE-CODE", FieldType.STRING, 
            4);
        other_Variables_Pnd_Ws_Contract_Nbr = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_Contract_Nbr", "#WS-CONTRACT-NBR", FieldType.STRING, 
            8);

        other_Variables__R_Field_4 = other_Variables.newGroupInGroup("other_Variables__R_Field_4", "REDEFINE", other_Variables_Pnd_Ws_Contract_Nbr);
        other_Variables_Pnd_Ws_Contract_Nbr_7 = other_Variables__R_Field_4.newFieldInGroup("other_Variables_Pnd_Ws_Contract_Nbr_7", "#WS-CONTRACT-NBR-7", 
            FieldType.STRING, 7);
        other_Variables_Pnd_Ws_Contract_Check_Digit = other_Variables__R_Field_4.newFieldInGroup("other_Variables_Pnd_Ws_Contract_Check_Digit", "#WS-CONTRACT-CHECK-DIGIT", 
            FieldType.STRING, 1);
        other_Variables_Pnd_Ws_Payee_Cde_4 = other_Variables.newFieldInGroup("other_Variables_Pnd_Ws_Payee_Cde_4", "#WS-PAYEE-CDE-4", FieldType.STRING, 
            4);

        other_Variables__R_Field_5 = other_Variables.newGroupInGroup("other_Variables__R_Field_5", "REDEFINE", other_Variables_Pnd_Ws_Payee_Cde_4);
        other_Variables_Pnd_Ws_Payee_Cde = other_Variables__R_Field_5.newFieldInGroup("other_Variables_Pnd_Ws_Payee_Cde", "#WS-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        other_Variables_Pnd_I = other_Variables.newFieldInGroup("other_Variables_Pnd_I", "#I", FieldType.INTEGER, 2);

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Lo_Values = constants.newFieldInGroup("constants_Pnd_Lo_Values", "#LO-VALUES", FieldType.STRING, 1);
        constants_Pnd_Hi_Values = constants.newFieldInGroup("constants_Pnd_Hi_Values", "#HI-VALUES", FieldType.STRING, 1);

        pnd_Counter_Request_Id = localVariables.newGroupInRecord("pnd_Counter_Request_Id", "#COUNTER-REQUEST-ID");
        pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr", "#WS-DOM-1-CTR", FieldType.NUMERIC, 
            6);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr", "#WS-DOM-2-CTR", FieldType.NUMERIC, 
            6);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr", "#WS-DOM-3-CTR", FieldType.NUMERIC, 
            6);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr", "#WS-DOM-4-CTR", FieldType.NUMERIC, 
            6);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr", "#WS-DOM-GT-4-CTR", 
            FieldType.NUMERIC, 6);
        pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr", "#WS-HLD-CTR", FieldType.NUMERIC, 
            6);
        pnd_Counter_Request_Id_Pnd_Ws_For_Hld_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_For_Hld_Ctr", "#WS-FOR-HLD-CTR", 
            FieldType.NUMERIC, 6);
        pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr", "#WS-FOR-TRI-CTR", 
            FieldType.NUMERIC, 6);
        pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr", "#WS-FOR-FLAT-CTR", 
            FieldType.NUMERIC, 6);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr", "#WS-DOM-LBL-CTR", 
            FieldType.NUMERIC, 6);
        pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr = pnd_Counter_Request_Id.newFieldInGroup("pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr", "#WS-FOR-LBL-CTR", 
            FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaaa029a.initializeValues();
        ldaIaaa029b.initializeValues();
        ldaIaaa029c.initializeValues();

        localVariables.reset();
        other_Variables_Pnd_Ws_First_Pass.setInitialValue(true);
        other_Variables_Pnd_Ws_Save_Pin.setInitialValue(0);
        other_Variables_Pnd_Ws_Save_Payee_Code.setInitialValue(" ");
        other_Variables_Pnd_Ws_Contract_Nbr.setInitialValue(0);
        constants_Pnd_Lo_Values.setInitialValue("H'00'");
        constants_Pnd_Hi_Values.setInitialValue("H'99'");
        pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_For_Hld_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr.setInitialValue(0);
        pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3029() throws Exception
    {
        super("Iaap3029");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap3029|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  PROCEDURE DIVISION STARTS HERE
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) LS = 151 PS = 65
                //* ***********************************************************************
                //*               >>>>>      GET PARAMETER CARD       <<<<<               *
                //* ***********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ctrl_Record);                                                                                      //Natural: INPUT #CTRL-RECORD
                if (condition(pnd_Ctrl_Record.greater(" ")))                                                                                                              //Natural: IF #CTRL-RECORD > ' '
                {
                    if (condition(DbsUtil.maskMatches(pnd_Ctrl_Record,"YYYYMMDD")))                                                                                       //Natural: IF #CTRL-RECORD = MASK ( YYYYMMDD )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "NOT A VALID DATE");                                                                                                        //Natural: WRITE 'NOT A VALID DATE'
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*               >>>>>     PROCEDURE STARTS HERE     <<<<<               *
                //* ***********************************************************************
                //* *PERFORM CSF-OPEN-FILE
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                sub_Initialize_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Array().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                   //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#SORT-KEY-ARRAY ( * )
                //*  COMPANY RECORD
                                                                                                                                                                          //Natural: PERFORM SETUP-HEADER-RECORD
                sub_Setup_Header_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 11 #NETCHANGE-RECORD
                while (condition(getWorkFiles().read(11, ldaIaaa029b.getPnd_Netchange_Record())))
                {
                    pnd_C1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #C1
                    //* *IF #NC-PIN-NBR LT  1241014
                    //* *OR #NC-PIN-NBR GT  1241020
                    //* *   ESCAPE TOP
                    //* *END-IF
                    //*  WRITE WORK FILE 12 #NETCHANGE-RECORD
                    //*  WRITE // '(1370) READ' 22T #C1 #NC-CONTRACT-NBR
                    //*           #SORT-KEY-LAYOUT.#KEY-L2-AREA #NC-ORIGIN
                    //*          #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
                    //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
                    if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().notEquals(other_Variables_Pnd_Ws_Save_Pin)))                                       //Natural: IF #NC-PIN-NBR NE #WS-SAVE-PIN
                    {
                        other_Variables_Pnd_Ws_Save_Pin.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr());                                                   //Natural: ASSIGN #WS-SAVE-PIN := #NC-PIN-NBR
                        other_Variables_Pnd_Ws_Save_Payee_Code.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code());                                         //Natural: ASSIGN #WS-SAVE-PAYEE-CODE := #NC-PAYEE-CODE
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-MAIL-ITEM-RECORD
                        sub_Setup_Mail_Item_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  WHEN BENEFIACIARIES ARE THE ANNUITANT RESULTING FROM AN IA DEATH
                        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code().notEquals(other_Variables_Pnd_Ws_Save_Payee_Code)))                         //Natural: IF #NC-PAYEE-CODE NE #WS-SAVE-PAYEE-CODE
                        {
                            other_Variables_Pnd_Ws_Save_Payee_Code.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code());                                     //Natural: ASSIGN #WS-SAVE-PAYEE-CODE := #NC-PAYEE-CODE
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                            sub_Initialize_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-MAIL-ITEM-RECORD
                            sub_Setup_Mail_Item_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                            sub_Csf_Write_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*   END OF CODE
                    //*  WRITE '(1580) READ-IF-ELSE' 22T #C1
                    //*   #NC-CONTRACT-NBR
                    //*           #SORT-KEY-LAYOUT.#KEY-L2-AREA
                    //*   #NC-ORIGIN #WS-FOR-LBL-CTR #WS-DOM-LBL-CTR
                    //*     #NC-RQST-ID-NBR
                    //* ***********************************************************************
                    //* ***        >>>>>        WRITE CONTRACT RECORD          <<<<<       ****
                    //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                    sub_Initialize_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-CONTRACT-RECORD
                    sub_Setup_Contract_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                    sub_Csf_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  WRITE '(1700) WRITE CONTRCT' 22T #C1
                    //*   #NC-CONTRACT-NBR
                    //*           #SORT-KEY-LAYOUT.#KEY-L2-AREA
                    //*   #NC-ORIGIN #WS-FOR-LBL-CTR #WS-DOM-LBL-CTR
                    //*     #NC-RQST-ID-NBR
                    //* ***********************************************************************
                    //*              >>>>>              FUND DATA               <<<<<         *
                    //* ***********************************************************************
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 #NC-FUND-COUNT
                    for (other_Variables_Pnd_I.setValue(1); condition(other_Variables_Pnd_I.lessOrEqual(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Count())); 
                        other_Variables_Pnd_I.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-FUND-RECORD
                        sub_Setup_Fund_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  LAST ACCOUNT RECORD
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                    sub_Initialize_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaIaaa029a.getFund_Account_Record_Fund_Prev_Total_Gross_Payment().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Gross_Amt().getValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Count())); //Natural: ASSIGN FUND-PREV-TOTAL-GROSS-PAYMENT := #NC-FUND-PREV-GROSS-AMT ( #NC-FUND-COUNT )
                    ldaIaaa029a.getFund_Account_Record_Fund_Curr_Total_Gross_Payment().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Gross_Amt().getValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Count())); //Natural: ASSIGN FUND-CURR-TOTAL-GROSS-PAYMENT := #NC-FUND-CURR-GROSS-AMT ( #NC-FUND-COUNT )
                    ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(999);                                                                              //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3 := 999
                    ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                             //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
                    ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getFund_Account_Record_Fund_Data_Length()).setValue(ldaIaaa029a.getFund_Account_Record_Fund_Data().getValue(1, //Natural: MOVE FUND-DATA ( 1:FUND-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:FUND-DATA-LENGTH )
                        ":",ldaIaaa029a.getFund_Account_Record_Fund_Data_Length()));
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                    sub_Csf_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  TRACE
                    //* ***********************************************************************
                    //*              >>>>>             DEDUCTIONS               <<<<<         *
                    //* ***********************************************************************
                    ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                 //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS ( * )
                    if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt().greater(getZero()) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt().greater(getZero()))) //Natural: IF #NC-PREV-FED-AMT > 0 OR #NC-CURR-FED-AMT > 0
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getDed_Record_Ded_Prev_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Fed_Amt());                                     //Natural: ASSIGN DED-RECORD.DED-PREV-AMT := #NC-PREV-FED-AMT
                        ldaIaaa029a.getDed_Record_Ded_Curr_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Fed_Amt());                                     //Natural: ASSIGN DED-RECORD.DED-CURR-AMT := #NC-CURR-FED-AMT
                        ldaIaaa029a.getDed_Record_Ded_Description().setValue("Federal Tax");                                                                              //Natural: MOVE 'Federal Tax' TO DED-RECORD.DED-DESCRIPTION
                                                                                                                                                                          //Natural: PERFORM SETUP-DEDUCTION-RECORD
                        sub_Setup_Deduction_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(1);                                                                            //Natural: MOVE 1 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_St_Amt().greater(getZero()) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_St_Amt().greater(getZero()))) //Natural: IF #NC-PREV-ST-AMT > 0 OR #NC-CURR-ST-AMT > 0
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getDed_Record_Ded_Prev_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_St_Amt());                                      //Natural: ASSIGN DED-RECORD.DED-PREV-AMT := #NC-PREV-ST-AMT
                        ldaIaaa029a.getDed_Record_Ded_Curr_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_St_Amt());                                      //Natural: ASSIGN DED-RECORD.DED-CURR-AMT := #NC-CURR-ST-AMT
                        ldaIaaa029a.getDed_Record_Ded_Description().setValue("State Tax");                                                                                //Natural: MOVE 'State Tax' TO DED-RECORD.DED-DESCRIPTION
                        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(2);                                                                            //Natural: MOVE 2 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                                                                                                                                                                          //Natural: PERFORM SETUP-DEDUCTION-RECORD
                        sub_Setup_Deduction_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt().greater(getZero()) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt().greater(getZero()))) //Natural: IF #NC-PREV-LOC-AMT > 0 OR #NC-CURR-LOC-AMT > 0
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getDed_Record_Ded_Prev_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Loc_Amt());                                     //Natural: ASSIGN DED-RECORD.DED-PREV-AMT := #NC-PREV-LOC-AMT
                        ldaIaaa029a.getDed_Record_Ded_Curr_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Loc_Amt());                                     //Natural: ASSIGN DED-RECORD.DED-CURR-AMT := #NC-CURR-LOC-AMT
                        ldaIaaa029a.getDed_Record_Ded_Description().setValue("Local Tax");                                                                                //Natural: MOVE 'Local Tax' TO DED-RECORD.DED-DESCRIPTION
                                                                                                                                                                          //Natural: PERFORM SETUP-DEDUCTION-RECORD
                        sub_Setup_Deduction_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(3);                                                                            //Natural: MOVE 3 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    FOR02:                                                                                                                                                //Natural: FOR #I 1 #NC-DEDUCTION-COUNT
                    for (other_Variables_Pnd_I.setValue(1); condition(other_Variables_Pnd_I.lessOrEqual(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Count())); 
                        other_Variables_Pnd_I.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getDed_Record_Ded_Description().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Deduction_Desc().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-DEDUCTION-DESC ( #I ) TO DED-RECORD.DED-DESCRIPTION
                        ldaIaaa029a.getDed_Record_Ded_Prev_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Ded_Amt().getValue(other_Variables_Pnd_I));     //Natural: ASSIGN DED-RECORD.DED-PREV-AMT := #NC-PREV-DED-AMT ( #I )
                        ldaIaaa029a.getDed_Record_Ded_Curr_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Ded_Amt().getValue(other_Variables_Pnd_I));     //Natural: ASSIGN DED-RECORD.DED-CURR-AMT := #NC-CURR-DED-AMT ( #I )
                                                                                                                                                                          //Natural: PERFORM SETUP-DEDUCTION-RECORD
                        sub_Setup_Deduction_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().compute(new ComputeParameters(false, ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3()),  //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3 := 3 + #I
                            DbsField.add(3,other_Variables_Pnd_I));
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  NET AMOUNT FROM FUNDS / LAST DEDUCTION RECORD
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                    sub_Initialize_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaIaaa029a.getDed_Record_Ded_Prev_Net_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Net_Amt());                                //Natural: ASSIGN DED-PREV-NET-AMT := #NC-FUND-PREV-NET-AMT
                    ldaIaaa029a.getDed_Record_Ded_Curr_Net_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Net_Amt());                                //Natural: ASSIGN DED-CURR-NET-AMT := #NC-FUND-CURR-NET-AMT
                    ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(999);                                                                              //Natural: MOVE 999 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                    ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak().setValue(2);                                                                                 //Natural: MOVE 2 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK
                    ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2().setValue(" ");                                                                              //Natural: MOVE ' ' TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK2
                    ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := DED-RECORD.DED-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
                        ldaIaaa029a.getDed_Record_Ded_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
                    ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getDed_Record_Ded_Record_Id());                                      //Natural: ASSIGN AFAPLINT-RECORD-ID := DED-RECORD.DED-RECORD-ID
                    ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                             //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
                    ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getDed_Record_Ded_Data_Length()).setValue(ldaIaaa029a.getDed_Record_Ded_Data().getValue(1, //Natural: MOVE DED-DATA ( 1:DED-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:DED-DATA-LENGTH )
                        ":",ldaIaaa029a.getDed_Record_Ded_Data_Length()));
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                    sub_Csf_Write_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* ***********************************************************************
                    //*              >>>>>          MESSAGE RECORDS              <<<<<        *
                    //* ***********************************************************************
                    FOR03:                                                                                                                                                //Natural: FOR #I 1 #NC-MESSAGES-CTR
                    for (other_Variables_Pnd_I.setValue(1); condition(other_Variables_Pnd_I.lessOrEqual(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Messages_Ctr())); 
                        other_Variables_Pnd_I.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-RECORD
                        sub_Initialize_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaIaaa029a.getMsg_Record_Msg_Date_1().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Date_1().getValue(other_Variables_Pnd_I));     //Natural: MOVE #NC-MESSAGE-DATE-1 ( #I ) TO MSG-DATE-1
                        ldaIaaa029a.getMsg_Record_Msg_Date_2().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Date_2().getValue(other_Variables_Pnd_I));     //Natural: MOVE #NC-MESSAGE-DATE-2 ( #I ) TO MSG-DATE-2
                        ldaIaaa029a.getMsg_Record_Msg_Increase_Percent().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Percent().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-MESSAGE-PERCENT ( #I ) TO MSG-INCREASE-PERCENT
                        ldaIaaa029a.getMsg_Record_Msg_Phone_Number().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Phone_Number().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-PHONE-NUMBER ( #I ) TO MSG-PHONE-NUMBER
                        ldaIaaa029a.getMsg_Record_Msg_Id().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number().getValue(other_Variables_Pnd_I));         //Natural: MOVE #NC-MESSAGE-NUMBER ( #I ) TO MSG-ID
                                                                                                                                                                          //Natural: PERFORM SETUP-MESSAGE-RECORD
                        sub_Setup_Message_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CSF-WRITE-RECORD
                        sub_Csf_Write_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //* *PERFORM CSF-CLOSE-FILE
                //*  WRITE CONTROL RECORD ON ALL ECS FILES       JWO 03/2009
                getWorkFiles().write(21, false, "99", pnd_Ones_Count, pnd_Trail_Blank.getValue("*"));                                                                     //Natural: WRITE WORK FILE 21 '99' #ONES-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(22, false, "99", pnd_Twos_Count, pnd_Trail_Blank.getValue("*"));                                                                     //Natural: WRITE WORK FILE 22 '99' #TWOS-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(23, false, "99", pnd_Threes_Count, pnd_Trail_Blank.getValue("*"));                                                                   //Natural: WRITE WORK FILE 23 '99' #THREES-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(24, false, "99", pnd_Fours_Count, pnd_Trail_Blank.getValue("*"));                                                                    //Natural: WRITE WORK FILE 24 '99' #FOURS-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(25, false, "99", pnd_Flatd_Count, pnd_Trail_Blank.getValue("*"));                                                                    //Natural: WRITE WORK FILE 25 '99' #FLATD-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(26, false, "99", pnd_Flatf_Count, pnd_Trail_Blank.getValue("*"));                                                                    //Natural: WRITE WORK FILE 26 '99' #FLATF-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(27, false, "99", pnd_Trif_Count, pnd_Trail_Blank.getValue("*"));                                                                     //Natural: WRITE WORK FILE 27 '99' #TRIF-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(28, false, "99", pnd_Labeld_Count, pnd_Trail_Blank.getValue("*"));                                                                   //Natural: WRITE WORK FILE 28 '99' #LABELD-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(29, false, "99", pnd_Labelf_Count, pnd_Trail_Blank.getValue("*"));                                                                   //Natural: WRITE WORK FILE 29 '99' #LABELF-COUNT #TRAIL-BLANK ( * )
                getWorkFiles().write(30, false, "99", pnd_Hold_Count, pnd_Trail_Blank.getValue("*"));                                                                     //Natural: WRITE WORK FILE 30 '99' #HOLD-COUNT #TRAIL-BLANK ( * )
                getReports().write(1, "Group Mail Item Count Report",NEWLINE,NEWLINE,"Group: One........:",pnd_Ones_Count,NEWLINE,"       Two........:",                  //Natural: WRITE ( 1 ) 'Group Mail Item Count Report' // 'Group: One........:' #ONES-COUNT / '       Two........:' #TWOS-COUNT / '       Three......:' #THREES-COUNT / '       Four.......:' #FOURS-COUNT / '       More 4(D)..:' #FLATD-COUNT / '       More 4(F)..:' #FLATF-COUNT / '       Tri-Foreing:' #TRIF-COUNT / '       Labels (D).:' #LABELD-COUNT / '       Labels (F).:' #LABELF-COUNT / '       Held.......:' #HOLD-COUNT
                    pnd_Twos_Count,NEWLINE,"       Three......:",pnd_Threes_Count,NEWLINE,"       Four.......:",pnd_Fours_Count,NEWLINE,"       More 4(D)..:",
                    pnd_Flatd_Count,NEWLINE,"       More 4(F)..:",pnd_Flatf_Count,NEWLINE,"       Tri-Foreing:",pnd_Trif_Count,NEWLINE,"       Labels (D).:",
                    pnd_Labeld_Count,NEWLINE,"       Labels (F).:",pnd_Labelf_Count,NEWLINE,"       Held.......:",pnd_Hold_Count);
                if (Global.isEscape()) return;
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-HEADER-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-MAIL-ITEM-RECORD
                //*         #SORT-KEY-LAYOUT.#KEY-L2-AREA := #NC-ZIP-CODE-3
                //*  WRITE '(3620)' 22T #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
                //*    #NC-ORIGIN
                //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
                //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
                //* *
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-CONTRACT-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-DEDUCTION-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FUND-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-MESSAGE-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CSF-OPEN-FILE
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-DDNAME
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CSF-WRITE-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CSF-CLOSE-FILE
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUEST-ID
                //*       #NC-ORIGIN  :=  'HLD'
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Setup_Header_Record() throws Exception                                                                                                               //Natural: SETUP-HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* * SET UP SORT KEY AND COMPANY RECORD  **
        //*   MOVE  ALL #LO-VALUES           TO MAIL-ITEM-RECORD.MAIL-DATA(*)
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Array().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                           //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#SORT-KEY-ARRAY ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Rsk_L1_Value().setValue("0A");                                                                                             //Natural: ASSIGN #SORT-KEY-LAYOUT.#RSK-L1-VALUE := '0A'
        if (condition(pnd_Ctrl_Record_Pnd_Cr_Run_Date.equals(getZero())))                                                                                                 //Natural: IF #CR-RUN-DATE = 0
        {
            other_Variables_Pnd_Ws_Date.setValue(Global.getDATN());                                                                                                       //Natural: MOVE *DATN TO #WS-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            other_Variables_Pnd_Ws_Date.setValue(pnd_Ctrl_Record_Pnd_Cr_Run_Date);                                                                                        //Natural: MOVE #CR-RUN-DATE TO #WS-DATE
        }                                                                                                                                                                 //Natural: END-IF
        other_Variables_Pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),other_Variables_Pnd_Ws_Date);                                                              //Natural: MOVE EDITED #WS-DATE TO #DATE ( EM = YYYYMMDD )
        other_Variables_Pnd_Ws_Date.setValueEdited(other_Variables_Pnd_Date,new ReportEditMask("MMDDYYYY"));                                                              //Natural: MOVE EDITED #DATE ( EM = MMDDYYYY ) TO #WS-DATE
        ldaIaaa029a.getCo_Header_Record_Co_Statement_Date().setValue(other_Variables_Pnd_Ws_Date_Mmddyyyy);                                                               //Natural: MOVE #WS-DATE-MMDDYYYY TO CO-HEADER-RECORD.CO-STATEMENT-DATE
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getCo_Header_Record_Co_Record_Length()).setValue(ldaIaaa029a.getCo_Header_Record_Co_Data().getValue(1, //Natural: MOVE CO-HEADER-RECORD.CO-DATA ( 1:CO-RECORD-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:CO-RECORD-LENGTH )
            ":",ldaIaaa029a.getCo_Header_Record_Co_Record_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := CO-HEADER-RECORD.CO-RECORD-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getCo_Header_Record_Co_Record_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getCo_Header_Record_Co_Record_Id());                                             //Natural: ASSIGN AFAPLINT-RECORD-ID := CO-HEADER-RECORD.CO-RECORD-ID
        //*  COMMENT OUT WRITE OF CSF FILE HEADER AND WRITE ECS FILE JWO 03/2009
        //*  HEADERS.                                                JWO 03/2009
        //* *FOR #I 1 10
        //* *  MOVE #I                         TO AFAPLINT-FILE-OCCURRENCE
        //* *  PERFORM CSF-WRITE-RECORD
        //* *END-FOR
        getWorkFiles().write(21, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 21 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(22, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 22 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(23, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 23 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(24, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 24 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(25, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 25 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(26, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 26 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(27, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 27 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(28, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 28 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(29, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 29 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        getWorkFiles().write(30, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*"));   //Natural: WRITE WORK FILE 30 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        //*  SETUP-HEADER-RECORD
    }
    //*  LEVEL 02
    private void sub_Setup_Mail_Item_Record() throws Exception                                                                                                            //Natural: SETUP-MAIL-ITEM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* * SET UP SORT KEY
        //*   MOVE ALL #LO-VALUES TO #CSF-DATA.#APPLICATION-DATA(*)
        //*   MOVE ALL #LO-VALUES TO MAIL-ITEM-RECORD.MAIL-DATA(*)
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                          //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L2_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L2-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L3_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L3-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS ( * )
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().equals("H")))                                                                           //Natural: IF #NC-LABEL-HOLD-IND = 'H'
        {
            ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Hold_2());                                           //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-AREA := #NC-HOLD-2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  WRITE '(3500) SETUP-MAIL' 22T #C1 #NC-CONTRACT-NBR
            //*   #SORT-KEY-LAYOUT.#KEY-L2-AREA
            //*   #NC-ORIGIN
            //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
            //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
            //*    10/22/98
            if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Zip_Code_5().equals("CANAD") || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Zip_Code_5().equals("FORGN"))) //Natural: IF #NC-ZIP-CODE-5 = 'CANAD' OR #NC-ZIP-CODE-5 = 'FORGN'
            {
                ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().setValue("FOR");                                                                                     //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-AREA := 'FOR'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().setValue("DOM");                                                                                     //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-AREA := 'DOM'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Nbr_Of_Contracts().setValue(0);                                                                                     //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-NBR-OF-CONTRACTS := 0
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Pin().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr());                                               //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-PIN := #NC-PIN-NBR
        other_Variables_Pnd_Ws_Payee_Cde_4.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Payee_Code());                                                             //Natural: ASSIGN #WS-PAYEE-CDE-4 := #NC-PAYEE-CODE
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Payee_Cde().setValue(other_Variables_Pnd_Ws_Payee_Cde);                                                             //Natural: ASSIGN #SORT-KEY-LAYOUT.#KEY-L2-PAYEE-CDE := #WS-PAYEE-CDE
        ldaIaaa029a.getMail_Item_Record_Mail_Package_Id().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr());                                                 //Natural: ASSIGN MAIL-PACKAGE-ID := #NC-PIN-NBR
        ldaIaaa029a.getMail_Item_Record_Mail_Pin_Number().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr());                                                 //Natural: ASSIGN MAIL-PIN-NUMBER := #NC-PIN-NBR
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Full_Name().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Full_Name());                                             //Natural: ASSIGN MAIL-PH-FULL-NAME := #NC-FULL-NAME
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address1().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(1));                                  //Natural: ASSIGN MAIL-PH-ADDRESS1 := #NC-ADDRESS-1 ( 1 )
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address2().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(2));                                  //Natural: ASSIGN MAIL-PH-ADDRESS2 := #NC-ADDRESS-1 ( 2 )
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address3().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(3));                                  //Natural: ASSIGN MAIL-PH-ADDRESS3 := #NC-ADDRESS-1 ( 3 )
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address4().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(4));                                  //Natural: ASSIGN MAIL-PH-ADDRESS4 := #NC-ADDRESS-1 ( 4 )
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address5().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(5));                                  //Natural: ASSIGN MAIL-PH-ADDRESS5 := #NC-ADDRESS-1 ( 5 )
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Address6().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Address_1().getValue(6));                                  //Natural: ASSIGN MAIL-PH-ADDRESS6 := #NC-ADDRESS-1 ( 6 )
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_2().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_2());                                              //Natural: ASSIGN MAIL-MACH-FUNC-2 := #NC-BIN-NBR-2
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_3().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_3());                                              //Natural: ASSIGN MAIL-MACH-FUNC-3 := #NC-BIN-NBR-3
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_4().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_4());                                              //Natural: ASSIGN MAIL-MACH-FUNC-4 := #NC-BIN-NBR-4
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_5().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Bin_Nbr_5());                                              //Natural: ASSIGN MAIL-MACH-FUNC-5 := #NC-BIN-NBR-5
        ldaIaaa029a.getMail_Item_Record_Mail_Postnet_Zip_Full().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Postnet_Zip());                                       //Natural: ASSIGN MAIL-POSTNET-ZIP-FULL := #NC-POSTNET-ZIP
        DbsUtil.examine(new ExamineSource(ldaIaaa029a.getMail_Item_Record_Mail_Postnet_Zip_Full()), new ExamineSearch("B"), new ExamineReplace("*"));                     //Natural: EXAMINE MAIL-POSTNET-ZIP-FULL FOR 'B' REPLACE WITH '*'
        DbsUtil.examine(new ExamineSource(ldaIaaa029a.getMail_Item_Record_Mail_Postnet_Zip_Full()), new ExamineSearch("E"), new ExamineReplace("*"));                     //Natural: EXAMINE MAIL-POSTNET-ZIP-FULL FOR 'E' REPLACE WITH '*'
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUEST-ID
        sub_Determine_Request_Id();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029a.getMail_Item_Record_Mail_Rqst_Id().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id());                                                    //Natural: MOVE #NC-RQST-ID TO MAIL-RQST-ID
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getMail_Item_Record_Mail_Data_Length()).setValue(ldaIaaa029a.getMail_Item_Record_Mail_Data().getValue(1, //Natural: MOVE MAIL-ITEM-RECORD.MAIL-DATA ( 1:MAIL-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:MAIL-DATA-LENGTH )
            ":",ldaIaaa029a.getMail_Item_Record_Mail_Data_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := MAIL-ITEM-RECORD.MAIL-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getMail_Item_Record_Mail_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getMail_Item_Record_Mail_Record_Id());                                           //Natural: ASSIGN AFAPLINT-RECORD-ID := MAIL-ITEM-RECORD.MAIL-RECORD-ID
        //*  SETUP-MAIL-ITEM-RECORD      /* LEVEL 02
    }
    //*   LEVEL 03
    private void sub_Setup_Contract_Record() throws Exception                                                                                                             //Natural: SETUP-CONTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *  SETUP SRT KEY    *
        //*    MOVE ALL #LO-VALUES   TO #CSF-DATA.#APPLICATION-DATA(*)
        //*    MOVE ALL #LO-VALUES   TO CONTRACT-RECORD.CONTRACT-DATA(*)
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L3_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L3-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L3_Tiebreak().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr());                                     //Natural: MOVE #NC-CONTRACT-NBR TO #SORT-KEY-LAYOUT.#KEY-L3-TIEBREAK
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DDNAME
        sub_Determine_Ddname();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date().notEquals(getZero())))                                                                 //Natural: IF #NC-PREV-CHECK-DATE NE 0
        {
            other_Variables_Pnd_Ws_Date.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Prev_Check_Date());                                                           //Natural: MOVE #NC-PREV-CHECK-DATE TO #WS-DATE
            other_Variables_Pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),other_Variables_Pnd_Ws_Date);                                                          //Natural: MOVE EDITED #WS-DATE TO #DATE ( EM = YYYYMMDD )
            other_Variables_Pnd_Ws_Date.setValueEdited(other_Variables_Pnd_Date,new ReportEditMask("MMDDYYYY"));                                                          //Natural: MOVE EDITED #DATE ( EM = MMDDYYYY ) TO #WS-DATE
            ldaIaaa029a.getContract_Record_Contract_Prev_Payment_Date().setValue(other_Variables_Pnd_Ws_Date_Mmddyyyy);                                                   //Natural: MOVE #WS-DATE-MMDDYYYY TO CONTRACT-PREV-PAYMENT-DATE
        }                                                                                                                                                                 //Natural: END-IF
        other_Variables_Pnd_Ws_Date.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Curr_Check_Date());                                                               //Natural: MOVE #NC-CURR-CHECK-DATE TO #WS-DATE
        other_Variables_Pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),other_Variables_Pnd_Ws_Date);                                                              //Natural: MOVE EDITED #WS-DATE TO #DATE ( EM = YYYYMMDD )
        other_Variables_Pnd_Ws_Date.setValueEdited(other_Variables_Pnd_Date,new ReportEditMask("MMDDYYYY"));                                                              //Natural: MOVE EDITED #DATE ( EM = MMDDYYYY ) TO #WS-DATE
        ldaIaaa029a.getContract_Record_Contract_Curr_Payment_Date().setValue(other_Variables_Pnd_Ws_Date_Mmddyyyy);                                                       //Natural: MOVE #WS-DATE-MMDDYYYY TO CONTRACT-CURR-PAYMENT-DATE
        ldaIaaa029a.getContract_Record_Contract_Number().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr());                                             //Natural: MOVE #NC-CONTRACT-NBR TO CONTRACT-NUMBER
        ldaIaaa029a.getContract_Record_Contract_Type().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Type());                                              //Natural: MOVE #NC-CONTRACT-TYPE TO CONTRACT-TYPE
        other_Variables_Pnd_Ws_Contract_Nbr.setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Contract_Nbr());                                                          //Natural: MOVE #NC-CONTRACT-NBR TO #WS-CONTRACT-NBR
        ldaIaaa029a.getContract_Record_Contract_Number().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, other_Variables_Pnd_Ws_Contract_Nbr_7,                  //Natural: COMPRESS #WS-CONTRACT-NBR-7 '-' #WS-CONTRACT-CHECK-DIGIT INTO CONTRACT-NUMBER LEAVING NO
            "-", other_Variables_Pnd_Ws_Contract_Check_Digit));
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getContract_Record_Contract_Data_Length()).setValue(ldaIaaa029a.getContract_Record_Contract_Data().getValue(1, //Natural: MOVE CONTRACT-DATA ( 1:CONTRACT-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:CONTRACT-DATA-LENGTH )
            ":",ldaIaaa029a.getContract_Record_Contract_Data_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := CONTRACT-RECORD.CONTRACT-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getContract_Record_Contract_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getContract_Record_Contract_Record_Id());                                        //Natural: ASSIGN AFAPLINT-RECORD-ID := CONTRACT-RECORD.CONTRACT-RECORD-ID
        //*  SETUP-CONTRACT-RECORD       /*  LEVEL 03
    }
    //*   LEVEL 04
    private void sub_Setup_Deduction_Record() throws Exception                                                                                                            //Natural: SETUP-DEDUCTION-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *  SETUP SRT KEY    *
        //*    MOVE ALL #LO-VALUES TO #CSF-DATA.#APPLICATION-DATA(*)
        //*    MOVE ALL #LO-VALUES TO DED-RECORD.DED-DATA(*)
        //*    MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS(*)
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak().setValue(2);                                                                                             //Natural: MOVE 2 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK2
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DDNAME
        sub_Determine_Ddname();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029a.getDed_Record_Ded_Prev_Net_Amt().setValue(0);                                                                                                         //Natural: MOVE 0 TO DED-PREV-NET-AMT DED-CURR-NET-AMT
        ldaIaaa029a.getDed_Record_Ded_Curr_Net_Amt().setValue(0);
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getDed_Record_Ded_Data_Length()).setValue(ldaIaaa029a.getDed_Record_Ded_Data().getValue(1, //Natural: MOVE DED-DATA ( 1:DED-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:DED-DATA-LENGTH )
            ":",ldaIaaa029a.getDed_Record_Ded_Data_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := DED-RECORD.DED-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getDed_Record_Ded_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getDed_Record_Ded_Record_Id());                                                  //Natural: ASSIGN AFAPLINT-RECORD-ID := DED-RECORD.DED-RECORD-ID
        //*  SETUP-DEDUCTION-RECORD      /*  LEVEL 04
    }
    //*   LEVEL 04
    private void sub_Setup_Fund_Record() throws Exception                                                                                                                 //Natural: SETUP-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *  SETUP SRT KEY    *
        //*    MOVE ALL #LO-VALUES           TO #CSF-DATA.#APPLICATION-DATA(*)
        //*    MOVE ALL #LO-VALUES           TO FUND-ACCOUNT-RECORD.FUND-DATA(*)
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak().setValue(1);                                                                                             //Natural: MOVE 1 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak2().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-VALUATION-PERIOD ( #I ) TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK2
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Code().getValue(other_Variables_Pnd_I));       //Natural: MOVE #NC-FUND-CODE ( #I ) TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DDNAME
        sub_Determine_Ddname();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029a.getFund_Account_Record_Fund_Account_Name().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Desc().getValue(other_Variables_Pnd_I));          //Natural: MOVE #NC-FUND-DESC ( #I ) TO FUND-ACCOUNT-NAME
        ldaIaaa029a.getFund_Account_Record_Fund_Account_Type().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Type().getValue(other_Variables_Pnd_I));          //Natural: MOVE #NC-FUND-TYPE ( #I ) TO FUND-ACCOUNT-TYPE
        ldaIaaa029a.getFund_Account_Record_Fund_Payment_Method().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Payment_Method().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-FUND-PAYMENT-METHOD ( #I ) TO FUND-PAYMENT-METHOD
        ldaIaaa029a.getFund_Account_Record_Fund_Change_Method().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Valuation_Period().getValue(other_Variables_Pnd_I));  //Natural: MOVE #NC-VALUATION-PERIOD ( #I ) TO FUND-CHANGE-METHOD
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Units().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Units().getValue(other_Variables_Pnd_I));      //Natural: MOVE #NC-FUND-PREV-UNITS ( #I ) TO FUND-PREV-UNITS
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero().getValue(other_Variables_Pnd_I).notEquals("*")))                                   //Natural: IF #NC-FUND-PREV-UNIT-ZERO ( #I ) NE '*'
        {
            ldaIaaa029a.getFund_Account_Record_Fund_Prev_Unit_Value_Ask().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Zero().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-PREV-UNIT-VALUE-ASK := #NC-FUND-PREV-UNIT-ZERO ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029a.getFund_Account_Record_Fund_Prev_Unit_Value().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Unit_Value().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-PREV-UNIT-VALUE := #NC-FUND-PREV-UNIT-VALUE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Contract_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Contract_Amt().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-PREV-CONTRACT-AMT := #NC-FUND-PREV-CONTRACT-AMT ( #I )
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Dividend().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Dividend().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-PREV-DIVIDEND := #NC-FUND-PREV-DIVIDEND ( #I )
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Total_Payment().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Prev_Total_Amt().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-PREV-TOTAL-PAYMENT := #NC-FUND-PREV-TOTAL-AMT ( #I )
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Units().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Units().getValue(other_Variables_Pnd_I));      //Natural: ASSIGN FUND-CURR-UNITS := #NC-FUND-CURR-UNITS ( #I )
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero().getValue(other_Variables_Pnd_I).notEquals("*")))                                   //Natural: IF #NC-FUND-CURR-UNIT-ZERO ( #I ) NE '*'
        {
            ldaIaaa029a.getFund_Account_Record_Fund_Curr_Unit_Value_Ask().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Zero().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-CURR-UNIT-VALUE-ASK := #NC-FUND-CURR-UNIT-ZERO ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaaa029a.getFund_Account_Record_Fund_Curr_Unit_Value().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Unit_Value().getValue(other_Variables_Pnd_I)); //Natural: ASSIGN FUND-CURR-UNIT-VALUE := #NC-FUND-CURR-UNIT-VALUE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Contract_Amt().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Contract_Amt().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-FUND-CURR-CONTRACT-AMT ( #I ) TO FUND-CURR-CONTRACT-AMT
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Dividend().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Dividend().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-FUND-CURR-DIVIDEND ( #I ) TO FUND-CURR-DIVIDEND
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Total_Payment().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Fund_Curr_Total_Amt().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-FUND-CURR-TOTAL-AMT ( #I ) TO FUND-CURR-TOTAL-PAYMENT
        //* * THESE FIELDS INITIALIZED TO ZERO
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Total_Gross_Payment().setValue(0);                                                                                   //Natural: MOVE 0 TO FUND-PREV-TOTAL-GROSS-PAYMENT FUND-CURR-TOTAL-GROSS-PAYMENT
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Total_Gross_Payment().setValue(0);
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getFund_Account_Record_Fund_Data_Length()).setValue(ldaIaaa029a.getFund_Account_Record_Fund_Data().getValue(1, //Natural: MOVE FUND-DATA ( 1:FUND-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:FUND-DATA-LENGTH )
            ":",ldaIaaa029a.getFund_Account_Record_Fund_Data_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := FUND-ACCOUNT-RECORD.FUND-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getFund_Account_Record_Fund_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getFund_Account_Record_Fund_Record_Id());                                        //Natural: ASSIGN AFAPLINT-RECORD-ID := FUND-ACCOUNT-RECORD.FUND-RECORD-ID
        //*  SETUP-FUND-RECORD            /*  LEVEL 04
    }
    //*   LEVEL 04
    private void sub_Setup_Message_Record() throws Exception                                                                                                              //Natural: SETUP-MESSAGE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *  SETUP SRT KEY    *
        //*    MOVE ALL #LO-VALUES           TO #CSF-DATA.#APPLICATION-DATA(*)
        //*    MOVE ALL #LO-VALUES           TO MSG-RECORD.MSG-DATA(*)
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_L4_Sort_Flds().getValue("*").moveAll(constants_Pnd_Lo_Values);                                                             //Natural: MOVE ALL #LO-VALUES TO #SORT-KEY-LAYOUT.#L4-SORT-FLDS ( * )
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak().setValue(3);                                                                                             //Natural: MOVE 3 TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK
        ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L4_Tiebreak3().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Message_Number_N().getValue(other_Variables_Pnd_I)); //Natural: MOVE #NC-MESSAGE-NUMBER-N ( #I ) TO #SORT-KEY-LAYOUT.#KEY-L4-TIEBREAK3
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DDNAME
        sub_Determine_Ddname();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*").reset();                                                                                         //Natural: RESET #CSF-DATA.#APPLICATION-DATA ( * )
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue(1,":",ldaIaaa029a.getMsg_Record_Msg_Data_Length()).setValue(ldaIaaa029a.getMsg_Record_Msg_Data().getValue(1, //Natural: MOVE MSG-DATA ( 1:MSG-DATA-LENGTH ) TO #CSF-DATA.#APPLICATION-DATA ( 1:MSG-DATA-LENGTH )
            ":",ldaIaaa029a.getMsg_Record_Msg_Data_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len().compute(new ComputeParameters(false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len()),  //Natural: ASSIGN AFAPLINT-RECORD-LEN := MSG-RECORD.MSG-DATA-LENGTH + #SORT-KEY-LAYOUT.#SORT-KEY-LENGTH
            ldaIaaa029a.getMsg_Record_Msg_Data_Length().add(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Length()));
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().setValue(ldaIaaa029a.getMsg_Record_Msg_Record_Id());                                                  //Natural: ASSIGN AFAPLINT-RECORD-ID := MSG-RECORD.MSG-RECORD-ID
        //*  SETUP-MESSAGE-RECORD         /*  LEVEL 04
    }
    private void sub_Csf_Open_File() throws Exception                                                                                                                     //Natural: CSF-OPEN-FILE
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 10
        for (other_Variables_Pnd_I.setValue(1); condition(other_Variables_Pnd_I.lessOrEqual(10)); other_Variables_Pnd_I.nadd(1))
        {
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                      //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Name().setValue("NET CHANGE");                                                                           //Natural: MOVE 'NET CHANGE' TO AFAPLINT-FILE-NAME
            if (condition(other_Variables_Pnd_I.equals(10)))                                                                                                              //Natural: IF #I = 10
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "CMWKF", other_Variables_Pnd_I));       //Natural: COMPRESS 'CMWKF' #I INTO AFAPLINT-DDNAME LEAVING NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "CMWKF0", other_Variables_Pnd_I));      //Natural: COMPRESS 'CMWKF0' #I INTO AFAPLINT-DDNAME LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(other_Variables_Pnd_I);                                                            //Natural: MOVE #I TO AFAPLINT-FILE-OCCURRENCE
            //* *  CALL 'AFAPLINT' AFAPLINT-OPEN AFAPLINT-CONTROL-FIELDS-ALPHA
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Return_Code().notEquals(0)))                                                                    //Natural: IF AFAPLINT-RETURN-CODE NE 00
            {
                getReports().write(0, "WORK FILE OPEN FAILED :",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname(),NEWLINE,"RECORD -ID            :",               //Natural: WRITE 'WORK FILE OPEN FAILED :' AFAPLINT-DDNAME / 'RECORD -ID            :' AFAPLINT-RECORD-ID / 'RET CODE              :' AFAPLINT-RETURN-CODE / 'FILE OCCURRENCE       :' AFAPLINT-FILE-OCCURRENCE
                    ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(),NEWLINE,"RET CODE              :",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Return_Code(),
                    NEWLINE,"FILE OCCURRENCE       :",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                          //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
        //*  MM 05/02/01
        getReports().write(0, "AFAPLINT FILES HAVE BEEN OPENED SUCCESSFULLY.");                                                                                           //Natural: WRITE 'AFAPLINT FILES HAVE BEEN OPENED SUCCESSFULLY.'
        if (Global.isEscape()) return;
        //*  CSF-OPEN-FILE
    }
    private void sub_Determine_Ddname() throws Exception                                                                                                                  //Natural: DETERMINE-DDNAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                          //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
        //*  WRITE '(5550) DETERMN-DDNM' 22T
        //*   #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
        //*   #NC-ORIGIN
        //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
        //*   #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
        short decideConditionsMet1021 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NC-LABEL-HOLD-IND = 'H'
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().equals("H")))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+10);                                                                              //Natural: MOVE +10 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-GTN-REPORT-CODE ( * ) = 1 OR #NC-GTN-REPORT-CODE ( * ) = 3 OR #NC-LABEL-HOLD-IND = 'L'
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").equals(1) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").equals(3) 
            || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().equals("L")))
        {
            decideConditionsMet1021++;
            //* **     IF #NC-ZIP-CODE-5 = 'CANAD' OR #NC-ZIP-CODE-5 = 'FORGN' /* IB
            //*  IB
            if (condition(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().equals("FOR")))                                                                            //Natural: IF #SORT-KEY-LAYOUT.#KEY-L2-AREA = 'FOR'
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+9);                                                                           //Natural: MOVE +9 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+8);                                                                           //Natural: MOVE +8 TO AFAPLINT-FILE-OCCURRENCE
                //*  IB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #SORT-KEY-LAYOUT.#KEY-L2-AREA = 'FOR'
        else if (condition(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().equals("FOR")))
        {
            decideConditionsMet1021++;
            //* *WHEN #NC-ZIP-CODE-5 = 'CANAD' OR = 'FORGN'              /* IB
            if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().greater(4)))                                                                       //Natural: IF #NC-NBR-OF-CONTRACT > 4
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+6);                                                                           //Natural: MOVE +6 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+7);                                                                           //Natural: MOVE +7 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT > 4
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().greater(4)))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+5);                                                                               //Natural: MOVE +5 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 4
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(4)))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+4);                                                                               //Natural: MOVE +4 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 3
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(3)))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+3);                                                                               //Natural: MOVE +3 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 2
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(2)))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+2);                                                                               //Natural: MOVE +2 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 1
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(1)))
        {
            decideConditionsMet1021++;
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+1);                                                                               //Natural: MOVE +1 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE '(5920)' 22T #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
        //*   #NC-ORIGIN #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
        //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
        //*  DETERMINE-DDNAME
    }
    private void sub_Csf_Write_Record() throws Exception                                                                                                                  //Natural: CSF-WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Name().setValue("NET CHANGE");                                                                               //Natural: MOVE 'NET CHANGE' TO AFAPLINT-FILE-NAME
        ldaIaaa029c.getPnd_Csf_Data_Pnd_Csf_Sort_Key().getValue("*").setValue(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Sort_Key_Array().getValue("*"));                     //Natural: MOVE #SORT-KEY-LAYOUT.#SORT-KEY-ARRAY ( * ) TO #CSF-DATA.#CSF-SORT-KEY ( * )
        //* * CALL 'AFAPLINT' AFAPLINT-WRITE AFAPLINT-CONTROL-FIELDS-ALPHA
        //* *              #CSF-DATA(1:2)
        if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Return_Code().notEquals(0)))                                                                        //Natural: IF AFAPLINT-RETURN-CODE NE 00
        {
            getReports().write(0, "AFAPLINT FAILED - RET CODE : ",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Return_Code(),NEWLINE,"WRITE WORK FILE ",               //Natural: WRITE 'AFAPLINT FAILED - RET CODE : ' AFAPLINT-RETURN-CODE / 'WRITE WORK FILE ' AFAPLINT-DDNAME / 'LENGTH          ' AFAPLINT-RECORD-LEN / 'RECORD ID       ' AFAPLINT-RECORD-ID
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname(),NEWLINE,"LENGTH          ",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Len(),
                NEWLINE,"RECORD ID       ",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id());
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1071 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE AFAPLINT-FILE-OCCURRENCE;//Natural: VALUE 1
        if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(1))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Ones_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ONES-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(21, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 21 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(2))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Twos_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TWOS-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(22, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 22 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(3))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Threes_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #THREES-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(23, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 23 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(4))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Fours_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FOURS-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(24, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 24 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(5))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Flatd_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FLATD-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(25, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 25 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(6))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Flatf_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FLATF-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(26, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 26 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(7))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Trif_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TRIF-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(27, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 27 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(8))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Labeld_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LABELD-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(28, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 28 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 9
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(9))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Labelf_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LABELF-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(29, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 29 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().equals(10))))
        {
            decideConditionsMet1071++;
            if (condition(ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id().equals("02")))                                                                      //Natural: IF AFAPLINT-RECORD-ID = '02'
            {
                pnd_Hold_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #HOLD-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(30, false, ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Record_Id(), ldaIaaa029c.getPnd_Csf_Data_Pnd_Application_Data().getValue("*")); //Natural: WRITE WORK FILE 30 AFAPLINT-RECORD-ID #APPLICATION-DATA ( * )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE '(6670)' 22T #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
        //*   #NC-ORIGIN
        //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
        //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
        //*  CSF-WRITE-RECORD
    }
    private void sub_Csf_Close_File() throws Exception                                                                                                                    //Natural: CSF-CLOSE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #I 1 10
        for (other_Variables_Pnd_I.setValue(1); condition(other_Variables_Pnd_I.lessOrEqual(10)); other_Variables_Pnd_I.nadd(1))
        {
            if (condition(other_Variables_Pnd_I.equals(10)))                                                                                                              //Natural: IF #I = 10
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "CMWKF", other_Variables_Pnd_I));       //Natural: COMPRESS 'CMWKF' #I INTO AFAPLINT-DDNAME LEAVING NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Ddname().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "CMWKF0", other_Variables_Pnd_I));      //Natural: COMPRESS 'CMWKF0' #I INTO AFAPLINT-DDNAME LEAVING NO
            }                                                                                                                                                             //Natural: END-IF
            //* *  CALL 'AFAPLINT' AFAPLINT-CLOSE AFAPLINT-CONTROL-FIELDS-ALPHA
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                          //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
        //*  MM 05/02/01
        getReports().write(0, "AFAPLINT FILES HAVE BEEN CLOSED SUCCESSFULLY.");                                                                                           //Natural: WRITE 'AFAPLINT FILES HAVE BEEN CLOSED SUCCESSFULLY.'
        if (Global.isEscape()) return;
        //* *WRITE 'AFAPLINT FILES ARE CLOSED '
        //*  CSF-CLOSE-FILE
    }
    private void sub_Initialize_Record() throws Exception                                                                                                                 //Natural: INITIALIZE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029a.getMail_Item_Record_Mail_Ph_Full_Name().setValue(" ");                                                                                                //Natural: MOVE ' ' TO MAIL-PH-FULL-NAME MAIL-WORK-PRCSS-ID MAIL-IMAGE-ID MAIL-RQST-ID MAIL-ENVIRONMENT MAIL-MACH-FUNC-2 MAIL-MACH-FUNC-3 MAIL-MACH-FUNC-4 MAIL-MACH-FUNC-5
        ldaIaaa029a.getMail_Item_Record_Mail_Work_Prcss_Id().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Image_Id().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Rqst_Id().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Environment().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_2().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_3().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_4().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Mach_Func_5().setValue(" ");
        ldaIaaa029a.getMail_Item_Record_Mail_Fill_Num().setValue(0);                                                                                                      //Natural: MOVE 0 TO MAIL-FILL-NUM MAIL-DOCUMENT-COUNT MAIL-PACKAGE-ID MAIL-SUMMARY-PREV-TOTAL MAIL-SUMMARY-CURR-TOTAL
        ldaIaaa029a.getMail_Item_Record_Mail_Document_Count().setValue(0);
        ldaIaaa029a.getMail_Item_Record_Mail_Package_Id().setValue(0);
        ldaIaaa029a.getMail_Item_Record_Mail_Summary_Prev_Total().setValue(0);
        ldaIaaa029a.getMail_Item_Record_Mail_Summary_Curr_Total().setValue(0);
        ldaIaaa029a.getContract_Record_Contract_Curr_Payment_Date().setValue(0);                                                                                          //Natural: MOVE 0 TO CONTRACT-CURR-PAYMENT-DATE CONTRACT-PREV-PAYMENT-DATE
        ldaIaaa029a.getContract_Record_Contract_Prev_Payment_Date().setValue(0);
        ldaIaaa029a.getContract_Record_Contract_Number().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO CONTRACT-NUMBER CONTRACT-TYPE
        ldaIaaa029a.getContract_Record_Contract_Type().setValue(" ");
        ldaIaaa029a.getContract_Record_Contract_Fill_Num().setValue(0);                                                                                                   //Natural: MOVE 0 TO CONTRACT-FILL-NUM
        ldaIaaa029a.getDed_Record_Ded_Description().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO DED-DESCRIPTION
        ldaIaaa029a.getDed_Record_Ded_Prev_Amt().setValue(0);                                                                                                             //Natural: MOVE 0 TO DED-PREV-AMT DED-CURR-AMT DED-PREV-NET-AMT DED-CURR-NET-AMT DED-FILL-NUM
        ldaIaaa029a.getDed_Record_Ded_Curr_Amt().setValue(0);
        ldaIaaa029a.getDed_Record_Ded_Prev_Net_Amt().setValue(0);
        ldaIaaa029a.getDed_Record_Ded_Curr_Net_Amt().setValue(0);
        ldaIaaa029a.getDed_Record_Ded_Fill_Num().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Account_Name().setValue(" ");                                                                                             //Natural: MOVE ' ' TO FUND-ACCOUNT-NAME FUND-ACCOUNT-TYPE FUND-PAYMENT-METHOD FUND-CHANGE-METHOD
        ldaIaaa029a.getFund_Account_Record_Fund_Account_Type().setValue(" ");
        ldaIaaa029a.getFund_Account_Record_Fund_Payment_Method().setValue(" ");
        ldaIaaa029a.getFund_Account_Record_Fund_Change_Method().setValue(" ");
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Contract_Amt().setValue(0);                                                                                          //Natural: MOVE 0 TO FUND-PREV-CONTRACT-AMT FUND-PREV-DIVIDEND FUND-PREV-UNITS FUND-PREV-UNIT-VALUE FUND-PREV-TOTAL-PAYMENT FUND-PREV-TOTAL-GROSS-PAYMENT FUND-CURR-CONTRACT-AMT FUND-CURR-DIVIDEND FUND-CURR-UNITS FUND-CURR-UNIT-VALUE FUND-CURR-TOTAL-PAYMENT FUND-CURR-TOTAL-GROSS-PAYMENT FUND-FILL-NUM
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Dividend().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Units().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Unit_Value().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Total_Payment().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Prev_Total_Gross_Payment().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Contract_Amt().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Dividend().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Units().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Unit_Value().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Total_Payment().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Curr_Total_Gross_Payment().setValue(0);
        ldaIaaa029a.getFund_Account_Record_Fund_Fill_Num().setValue(0);
        ldaIaaa029a.getMsg_Record_Msg_Phone_Number().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO MSG-PHONE-NUMBER MSG-ID
        ldaIaaa029a.getMsg_Record_Msg_Id().setValue(" ");
        ldaIaaa029a.getMsg_Record_Msg_Date_1().setValue(0);                                                                                                               //Natural: MOVE 0 TO MSG-DATE-1 MSG-DATE-2 MSG-INCREASE-PERCENT
        ldaIaaa029a.getMsg_Record_Msg_Date_2().setValue(0);
        ldaIaaa029a.getMsg_Record_Msg_Increase_Percent().setValue(0);
        //*  MM 05/14/01 BEGIN
        ldaIaaa029a.getMsg_Record_Msg_Numeric_1().setValue(0);                                                                                                            //Natural: MOVE 0 TO MSG-NUMERIC-1 MSG-NUMERIC-2 MSG-NUMERIC-3
        ldaIaaa029a.getMsg_Record_Msg_Numeric_2().setValue(0);
        ldaIaaa029a.getMsg_Record_Msg_Numeric_3().setValue(0);
        ldaIaaa029a.getMsg_Record_Msg_Char_1().setValue(" ");                                                                                                             //Natural: MOVE ' ' TO MSG-CHAR-1 MSG-CHAR-2 MSG-CHAR-3
        ldaIaaa029a.getMsg_Record_Msg_Char_2().setValue(" ");
        ldaIaaa029a.getMsg_Record_Msg_Char_3().setValue(" ");
        //*  MM 05/14/01 END
        //*  INITIALIZE-RECORD
    }
    private void sub_Determine_Request_Id() throws Exception                                                                                                              //Natural: DETERMINE-REQUEST-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_Control_Fields_Alpha().moveAll(constants_Pnd_Lo_Values);                                                          //Natural: MOVE ALL #LO-VALUES TO AFAPLINT-CONTROL-FIELDS-ALPHA
        //*  WRITE '(7380) DETRMN-REQ-ID' 22T
        //*  #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
        //*   #NC-ORIGIN
        //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
        //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
        short decideConditionsMet1179 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NC-LABEL-HOLD-IND = 'H'
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().equals("H")))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr), DbsField.add(1,pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr)); //Natural: ASSIGN #WS-HLD-CTR := 1 + #WS-HLD-CTR
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Hold_Code());                                         //Natural: ASSIGN #NC-ORIGIN := #NC-HOLD-CODE
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Hld_Ctr,new ReportEditMask("999999"));                  //Natural: MOVE EDITED #WS-HLD-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+10);                                                                              //Natural: MOVE +10 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-GTN-REPORT-CODE ( * ) = 1 OR #NC-GTN-REPORT-CODE ( * ) = 3 OR #NC-LABEL-HOLD-IND = 'L'
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").equals(1) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Gtn_Report_Code().getValue("*").equals(3) 
            || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Label_Hold_Ind().equals("L")))
        {
            decideConditionsMet1179++;
            //* **    IF #NC-ZIP-CODE-5 = 'CANAD' OR #NC-ZIP-CODE-5 = 'FORGN' /*
            //*  IB
            if (condition(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().equals("FOR")))                                                                            //Natural: IF #SORT-KEY-LAYOUT.#KEY-L2-AREA = 'FOR'
            {
                pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr), DbsField.add(1,                //Natural: ASSIGN #WS-FOR-LBL-CTR := 1 + #WS-FOR-LBL-CTR
                    pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("FOR-L");                                                                                    //Natural: ASSIGN #NC-ORIGIN := 'FOR-L'
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_For_Lbl_Ctr,new ReportEditMask("999999"));          //Natural: MOVE EDITED #WS-FOR-LBL-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+9);                                                                           //Natural: MOVE +9 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr), DbsField.add(1,                //Natural: ASSIGN #WS-DOM-LBL-CTR := 1 + #WS-DOM-LBL-CTR
                    pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-L");                                                                                    //Natural: ASSIGN #NC-ORIGIN := 'DOM-L'
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_Lbl_Ctr,new ReportEditMask("999999"));          //Natural: MOVE EDITED #WS-DOM-LBL-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+8);                                                                           //Natural: MOVE +8 TO AFAPLINT-FILE-OCCURRENCE
                //*  IB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #SORT-KEY-LAYOUT.#KEY-L2-AREA = 'FOR'
        else if (condition(ldaIaaa029a.getPnd_Sort_Key_Layout_Pnd_Key_L2_Area().equals("FOR")))
        {
            decideConditionsMet1179++;
            //* *WHEN #NC-ZIP-CODE-5 = 'CANAD' OR = 'FORGN'              /* IB
            if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().greater(4)))                                                                       //Natural: IF #NC-NBR-OF-CONTRACT > 4
            {
                pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr), DbsField.add(1,              //Natural: ASSIGN #WS-FOR-FLAT-CTR := 1 + #WS-FOR-FLAT-CTR
                    pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("FOR-M");                                                                                    //Natural: ASSIGN #NC-ORIGIN := 'FOR-M'
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_For_Flat_Ctr,new ReportEditMask("999999"));         //Natural: MOVE EDITED #WS-FOR-FLAT-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+6);                                                                           //Natural: MOVE +6 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr), DbsField.add(1,                //Natural: ASSIGN #WS-FOR-TRI-CTR := 1 + #WS-FOR-TRI-CTR
                    pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr));
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("FOR-S");                                                                                    //Natural: ASSIGN #NC-ORIGIN := 'FOR-S'
                ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_For_Tri_Ctr,new ReportEditMask("999999"));          //Natural: MOVE EDITED #WS-FOR-TRI-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
                ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+7);                                                                           //Natural: MOVE +7 TO AFAPLINT-FILE-OCCURRENCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT > 4
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().greater(4)))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr), DbsField.add(1,                  //Natural: ASSIGN #WS-DOM-GT-4-CTR := 1 + #WS-DOM-GT-4-CTR
                pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr));
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-M");                                                                                        //Natural: ASSIGN #NC-ORIGIN := 'DOM-M'
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_Gt_4_Ctr,new ReportEditMask("999999"));             //Natural: MOVE EDITED #WS-DOM-GT-4-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+5);                                                                               //Natural: MOVE +5 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 4
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(4)))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr), DbsField.add(1,pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr)); //Natural: ASSIGN #WS-DOM-4-CTR := 1 + #WS-DOM-4-CTR
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-Q");                                                                                        //Natural: ASSIGN #NC-ORIGIN := 'DOM-Q'
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_4_Ctr,new ReportEditMask("999999"));                //Natural: MOVE EDITED #WS-DOM-4-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+4);                                                                               //Natural: MOVE +4 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 3
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(3)))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr), DbsField.add(1,pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr)); //Natural: ASSIGN #WS-DOM-3-CTR := 1 + #WS-DOM-3-CTR
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-T");                                                                                        //Natural: ASSIGN #NC-ORIGIN := 'DOM-T'
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_3_Ctr,new ReportEditMask("999999"));                //Natural: MOVE EDITED #WS-DOM-3-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+3);                                                                               //Natural: MOVE +3 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 2
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(2)))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr), DbsField.add(1,pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr)); //Natural: ASSIGN #WS-DOM-2-CTR := 1 + #WS-DOM-2-CTR
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-D");                                                                                        //Natural: ASSIGN #NC-ORIGIN := 'DOM-D'
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_2_Ctr,new ReportEditMask("999999"));                //Natural: MOVE EDITED #WS-DOM-2-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+2);                                                                               //Natural: MOVE +2 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN #NC-NBR-OF-CONTRACT = 1
        else if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Nbr_Of_Contract().equals(1)))
        {
            decideConditionsMet1179++;
            pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr.compute(new ComputeParameters(false, pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr), DbsField.add(1,pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr)); //Natural: ASSIGN #WS-DOM-1-CTR := 1 + #WS-DOM-1-CTR
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Origin().setValue("DOM-S");                                                                                        //Natural: ASSIGN #NC-ORIGIN := 'DOM-S'
            ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr().setValueEdited(pnd_Counter_Request_Id_Pnd_Ws_Dom_1_Ctr,new ReportEditMask("999999"));                //Natural: MOVE EDITED #WS-DOM-1-CTR ( EM = 999999 ) TO #NC-RQST-ID-NBR
            ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence().setValue(+1);                                                                               //Natural: MOVE +1 TO AFAPLINT-FILE-OCCURRENCE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().equals(1566132) || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().equals(1987045)        //Natural: IF #NC-PIN-NBR = 1566132 OR = 1987045 OR = 1948617
            || ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr().equals(1948617)))
        {
            getReports().write(0, "=",ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Pin_Nbr(),"=",ldaIaaa029c.getAfaplint_Control_Fields_Afaplint_File_Occurrence(),         //Natural: WRITE '=' #NC-PIN-NBR '=' AFAPLINT-FILE-OCCURRENCE '=' #NC-RQST-ID-NBR
                "=",ldaIaaa029b.getPnd_Netchange_Record_Pnd_Nc_Rqst_Id_Nbr());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '(8100)' 22T
        //*  #C1 #NC-CONTRACT-NBR #SORT-KEY-LAYOUT.#KEY-L2-AREA
        //*   #NC-ORIGIN
        //*   #NC-PIN-NBR #WS-SAVE-PIN #NC-PAYEE-CODE
        //*        #WS-SAVE-PAYEE-CODE #NC-ZIP-CODE-5 #NC-LABEL-HOLD-IND
        //*  DETERMINE-REQUEST-ID
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=151 PS=65");
    }
}
