/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:11 PM
**        * FROM NATURAL PROGRAM : Iaap905
************************************************************
**        * FILE NAME            : Iaap905.java
**        * CLASS NAME           : Iaap905
**        * INSTANCE NAME        : Iaap905
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP905    FORMAT BATCH HEADER CARDS FOR IA TRANS *
*      DATE     -  9/94       OUTPUT FILE IS INPUT TO THE IA EDIT    *
*                                                                    *
**********************************************************************
* CHANGE HISTORY
* CHANGED ON APRIL 10, 1995 BY D ROBINSON
* >   ENHANCED TO PRODUCE PRODUCT CODE AND MODE REPORT
* CHANGED ON APRIL 19, 1995 BY D ROBINSON
* >   ENHANCED TO ALLEVIATE MODES FOR CC'S W/O RATE CHANGES.
* CHANGED ON AUGUST 4, 1995 BY D ROBINSON
* >   CHANGED TO START BATCH NUMBERS WITH 9000 AFTER 1499.
* CHANGED ON AUGUST 9, 1995 BY D ROBINSON
* >   ENLARGE ARITHMETIC FIELD.
* CHANGED ON AUGUST 15, 1995 BY D ROBINSON
* >   CORRECT ADDITION OF BATCH NUMBERS.  ADD SUMMARY REPORT.
* CHANGED ON AUGUST 15, 1995 BY D ROBINSON
* >   ENHANCED TO ACCOMODATE REAL ESTATE CONTRACTS.
* CHANGED ON NOVEMBER 10, 1995 BY D ROBINSON
* >   BYPASS BATCH #9143.
* CHANGED ON DECEMBER 4, 1995 BY D ROBINSON
* >   BYPASS BATCH #9388 AND 9412
* >
* >
**********************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap905 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField input_Rec;

    private DbsGroup input_Rec__R_Field_1;
    private DbsField input_Rec_Pnd_Batch_Number;
    private DbsField input_Rec_Pnd_Check_Date;
    private DbsField input_Rec_Pnd_Contract_Number;

    private DbsGroup input_Rec__R_Field_2;
    private DbsField input_Rec_Pnd_Contract_Prefix;
    private DbsField input_Rec_Pnd_Contract_End;
    private DbsField input_Rec_Pnd_Contract_Payee;
    private DbsField input_Rec_Pnd_Cross_Reference;
    private DbsField input_Rec_Pnd_Transaction_Num;
    private DbsField input_Rec_Pnd_Last_48;

    private DbsGroup input_Rec__R_Field_3;
    private DbsField input_Rec_Pnd_Record_Type;
    private DbsField input_Rec_Pnd_Record_Number;
    private DbsField input_Rec_Pnd_Rate;

    private DbsGroup input_Rec__R_Field_4;
    private DbsField input_Rec_Pnd_Product;
    private DbsField input_Rec_Pnd_Periodic_Pymt_Unit_P;

    private DbsGroup input_Rec__R_Field_5;
    private DbsField input_Rec_Pnd_Periodic_Pymt_Unit;

    private DbsGroup input_Rec__R_Field_6;
    private DbsField input_Rec_Pnd_Mode;

    private DbsGroup input_Rec__R_Field_7;
    private DbsField input_Rec_Pnd_Periodic_Pymt;

    private DbsGroup input_Rec__R_Field_8;
    private DbsField input_Rec_Pnd_Periodic_Unit;
    private DbsField input_Rec_Pnd_Periodic_Div;
    private DbsField input_Rec_Pnd_Final_Pymt;
    private DbsField input_Rec_Pnd_Old_Periodic_Div;
    private DbsField input_Rec_Pnd_Old_Periodic_Pymt_Unit;

    private DbsGroup input_Rec__R_Field_9;
    private DbsField input_Rec_Pnd_Old_Periodic_Pymt;

    private DbsGroup input_Rec__R_Field_10;
    private DbsField input_Rec_Pnd_Old_Periodic_Unit;
    private DbsField input_Rec_Pnd_Card_Filler;

    private DbsGroup input_Rec__R_Field_11;
    private DbsField input_Rec_Pnd_Intent_Code;
    private DbsField input_Rec_Pnd_Filler6;
    private DbsField input_Rec_Pnd_Hold_Check_Rd;
    private DbsField input_Rec_Pnd_Suspend_Rc;
    private DbsField input_Rec_Pnd_State_Residence;
    private DbsField input_Rec_Pnd_A1_Sex;
    private DbsField input_Rec_Pnd_A1_Date_Of_Birth;
    private DbsField input_Rec_Pnd_A1_Date_Of_Death;
    private DbsField input_Rec_Pnd_Filler2;
    private DbsField input_Rec_Pnd_A2_Sex;
    private DbsField input_Rec_Pnd_A2_Date_Of_Birth;
    private DbsField input_Rec_Pnd_A2_Date_Of_Death;
    private DbsField input_Rec_Pnd_Filler8;
    private DbsField input_Rec_Pnd_Eff_Dte_Chnge;

    private DbsGroup input_Rec__R_Field_12;
    private DbsField input_Rec_Pnd_Intent_Code_724;
    private DbsField input_Rec_Pnd_Sequence_Num;
    private DbsField input_Rec_Pnd_Tax_Authority;
    private DbsField input_Rec_Pnd_Active_Code;
    private DbsField input_Rec_Pnd_Periodic_Tax_With;
    private DbsField input_Rec_Pnd_Ytd_Tax_With;
    private DbsField input_Rec_Pnd_Filler7;
    private DbsField input_Rec_Pnd_Invest_In_Cntrct;
    private DbsField input_Rec_Pnd_Filler8_724;

    private DbsGroup input_Rec__R_Field_13;
    private DbsField input_Rec_Pnd_Intent_Code_902;
    private DbsField input_Rec_Pnd_Sequence_Num_902;
    private DbsField input_Rec_Pnd_Deduction_Cd;
    private DbsField input_Rec_Pnd_Deduction_Payee;
    private DbsField input_Rec_Pnd_Periodic_Ded_Amt;
    private DbsField input_Rec_Pnd_Total_Deduction;
    private DbsField input_Rec_Pnd_Final_Ded_Date;
    private DbsField input_Rec_Pnd_Filler16;

    private DbsGroup input_Rec__R_Field_14;
    private DbsField input_Rec_Pnd_Intent_Code_906;
    private DbsField input_Rec_Pnd_Sequence_Num_906;
    private DbsField input_Rec_Pnd_Deduction_Cd_906;
    private DbsField input_Rec_Pnd_Deduction_Payee_906;
    private DbsField input_Rec_Pnd_Filler07;
    private DbsField input_Rec_Pnd_Hist_Ded_Amt;
    private DbsField input_Rec_Pnd_Filler13;
    private DbsField input_Rec_Pnd_User_Area;
    private DbsField batch_Rec;

    private DbsGroup batch_Rec__R_Field_15;
    private DbsField batch_Rec_Pnd_Batch_Number;
    private DbsField batch_Rec_Pnd_Check_Date;
    private DbsField batch_Rec_Pnd_Field1;
    private DbsField batch_Rec_Pnd_Filler1;
    private DbsField batch_Rec_Pnd_Field2;
    private DbsField batch_Rec_Pnd_Card_Count;
    private DbsField batch_Rec_Pnd_Filler46;

    private DbsGroup batch_Rec__R_Field_16;
    private DbsField batch_Rec_Pnd_Field3;
    private DbsField batch_Rec_Pnd_Periodic_Pymt_Unit;

    private DbsGroup batch_Rec__R_Field_17;
    private DbsField batch_Rec_Pnd_Periodic_Pymt;

    private DbsGroup batch_Rec__R_Field_18;
    private DbsField batch_Rec_Pnd_Periodic_Unit;
    private DbsField batch_Rec_Pnd_Periodic_Div;
    private DbsField batch_Rec_Pnd_Final_Pymt;
    private DbsField batch_Rec_Pnd_Filler14;

    private DbsGroup batch_Rec__R_Field_19;
    private DbsField batch_Rec_Pnd_Transaction_Count;
    private DbsField batch_Rec_Pnd_Filler44;

    private DbsGroup batch_Rec__R_Field_20;
    private DbsField batch_Rec_Pnd_Transaction_Count_724;
    private DbsField batch_Rec_Pnd_Periodic_Tax_With;
    private DbsField batch_Rec_Pnd_Ytd_Tax_With;
    private DbsField batch_Rec_Pnd_Invest_In_Cntrct;
    private DbsField batch_Rec_Pnd_Filler14_724;

    private DbsGroup batch_Rec__R_Field_21;
    private DbsField batch_Rec_Pnd_Transaction_Count_902;
    private DbsField batch_Rec_Pnd_Periodic_Ded_Amt;
    private DbsField batch_Rec_Pnd_Filelr34;
    private DbsField output_Rec;

    private DbsGroup output_Rec__R_Field_22;
    private DbsField output_Rec_Output_Rec_Batch;
    private DbsField output_Rec_Output_Rec_76;

    private DbsGroup output_Rec__R_Field_23;
    private DbsField output_Rec_Pnd_Out_Data;
    private DbsField output_Rec__Filler1;
    private DbsField output_Rec_Pnd_Out_Product;
    private DbsField output_Rec__Filler2;

    private DbsGroup miscellaneous_Fields;
    private DbsField miscellaneous_Fields_Pnd_Next_Batch_Number;
    private DbsField miscellaneous_Fields_Pnd_W_Card_Count;
    private DbsField miscellaneous_Fields_Pnd_Sw_First;
    private DbsField miscellaneous_Fields_Pnd_W_Trans_Count;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Check_Date;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_User_Area;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr;

    private DbsGroup miscellaneous_Fields__R_Field_24;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Contract_Prefix;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Contract_End;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Contract_Payee;

    private DbsGroup miscellaneous_Fields__R_Field_25;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr8;
    private DbsField miscellaneous_Fields_Pnd_W_Display_Cntrct;

    private DbsGroup miscellaneous_Fields__R_Field_26;
    private DbsField miscellaneous_Fields_Pnd_W_Display_Cntrct_Nbr;
    private DbsField miscellaneous_Fields_Pnd_Filler;
    private DbsField miscellaneous_Fields_Pnd_W_Display_Cntrct_Payee;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Transaction_Num;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Sequence_Number;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Product;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Mode;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Record_Type;
    private DbsField miscellaneous_Fields_Pnd_W_Hold_Record_Number;
    private DbsField miscellaneous_Fields_Pnd_W_Periodic_Pymt;
    private DbsField miscellaneous_Fields_Pnd_W_Periodic_Unit;
    private DbsField miscellaneous_Fields_Pnd_W_Periodic_Div;
    private DbsField miscellaneous_Fields_Pnd_W_Final_Pymt;
    private DbsField miscellaneous_Fields_Pnd_W_Periodic_Tax_With;
    private DbsField miscellaneous_Fields_Pnd_W_Ytd_Tax_With;
    private DbsField miscellaneous_Fields_Pnd_W_Invest_In_Cntrct;
    private DbsField miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt;
    private DbsField miscellaneous_Fields_Pnd_W_Date_Out;
    private DbsField miscellaneous_Fields_Pnd_W_Page_Ctr;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Card_Count;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Trans_Count;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Periodic_Pymt;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Periodic_Unit;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Periodic_Div;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Final_Pymt;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Invest_In_Cntrct;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt;
    private DbsField miscellaneous_Fields_Pnd_W_Tot_Ded_Amt;
    private DbsField miscellaneous_Fields_Pnd_W_Indx;
    private DbsField miscellaneous_Fields_Pnd_W_C_Indx;
    private DbsField miscellaneous_Fields_Pnd_W_M_Indx;

    private DbsGroup mode_Report_Array;
    private DbsField mode_Report_Array_Pnd_W_Rpt_Mode;
    private DbsField mode_Report_Array_Pnd_W_T_Rpt_Amt;
    private DbsField mode_Report_Array_Pnd_W_C_Rpt_Units;

    private DbsGroup pnd_Work_Aread;
    private DbsField pnd_Work_Aread_Pnd_Periodic_Pymt_Unit_P;
    private DbsField pnd_Work_Aread_Pnd_Mode_A;
    private DbsField pnd_Work_Aread_Pnd_Periodic_Pymt_Conv;

    private DbsGroup pnd_Work_Aread__R_Field_27;
    private DbsField pnd_Work_Aread_Pnd_Periodic_Pymt_A;
    private DbsField pnd_Work_Aread_Pnd_Periodic_Unit_Conv;

    private DbsGroup pnd_Work_Aread__R_Field_28;
    private DbsField pnd_Work_Aread_Pnd_Periodic_Unit_A;
    private DbsField pnd_Total_Trans;
    private DbsField pnd_W_User_Area_Trans_Count;
    private DbsField pnd_W_Printing_User_Totals;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        input_Rec = localVariables.newFieldInRecord("input_Rec", "INPUT-REC", FieldType.STRING, 100);

        input_Rec__R_Field_1 = localVariables.newGroupInRecord("input_Rec__R_Field_1", "REDEFINE", input_Rec);
        input_Rec_Pnd_Batch_Number = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Batch_Number", "#BATCH-NUMBER", FieldType.PACKED_DECIMAL, 7);
        input_Rec_Pnd_Check_Date = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 6);
        input_Rec_Pnd_Contract_Number = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 10);

        input_Rec__R_Field_2 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_2", "REDEFINE", input_Rec_Pnd_Contract_Number);
        input_Rec_Pnd_Contract_Prefix = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Pnd_Contract_Prefix", "#CONTRACT-PREFIX", FieldType.STRING, 1);
        input_Rec_Pnd_Contract_End = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Pnd_Contract_End", "#CONTRACT-END", FieldType.STRING, 7);
        input_Rec_Pnd_Contract_Payee = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 2);
        input_Rec_Pnd_Cross_Reference = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Cross_Reference", "#CROSS-REFERENCE", FieldType.STRING, 9);
        input_Rec_Pnd_Transaction_Num = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Transaction_Num", "#TRANSACTION-NUM", FieldType.NUMERIC, 3);
        input_Rec_Pnd_Last_48 = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_Last_48", "#LAST-48", FieldType.STRING, 62);

        input_Rec__R_Field_3 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_3", "REDEFINE", input_Rec_Pnd_Last_48);
        input_Rec_Pnd_Record_Type = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 2);
        input_Rec_Pnd_Record_Number = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Record_Number", "#RECORD-NUMBER", FieldType.NUMERIC, 1);
        input_Rec_Pnd_Rate = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Rate", "#RATE", FieldType.STRING, 2);

        input_Rec__R_Field_4 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_4", "REDEFINE", input_Rec_Pnd_Rate);
        input_Rec_Pnd_Product = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Pnd_Product", "#PRODUCT", FieldType.STRING, 1);
        input_Rec_Pnd_Periodic_Pymt_Unit_P = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Periodic_Pymt_Unit_P", "#PERIODIC-PYMT-UNIT-P", FieldType.PACKED_DECIMAL, 
            13, 2);

        input_Rec__R_Field_5 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_5", "REDEFINE", input_Rec_Pnd_Periodic_Pymt_Unit_P);
        input_Rec_Pnd_Periodic_Pymt_Unit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Pnd_Periodic_Pymt_Unit", "#PERIODIC-PYMT-UNIT", FieldType.STRING, 
            7);

        input_Rec__R_Field_6 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_6", "REDEFINE", input_Rec_Pnd_Periodic_Pymt_Unit_P);
        input_Rec_Pnd_Mode = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Pnd_Mode", "#MODE", FieldType.STRING, 3);

        input_Rec__R_Field_7 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_7", "REDEFINE", input_Rec_Pnd_Periodic_Pymt_Unit_P);
        input_Rec_Pnd_Periodic_Pymt = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Pnd_Periodic_Pymt", "#PERIODIC-PYMT", FieldType.PACKED_DECIMAL, 9, 
            2);

        input_Rec__R_Field_8 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_8", "REDEFINE", input_Rec_Pnd_Periodic_Pymt_Unit_P);
        input_Rec_Pnd_Periodic_Unit = input_Rec__R_Field_8.newFieldInGroup("input_Rec_Pnd_Periodic_Unit", "#PERIODIC-UNIT", FieldType.PACKED_DECIMAL, 9, 
            3);
        input_Rec_Pnd_Periodic_Div = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Periodic_Div", "#PERIODIC-DIV", FieldType.NUMERIC, 9, 2);
        input_Rec_Pnd_Final_Pymt = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Final_Pymt", "#FINAL-PYMT", FieldType.NUMERIC, 9, 2);
        input_Rec_Pnd_Old_Periodic_Div = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Old_Periodic_Div", "#OLD-PERIODIC-DIV", FieldType.NUMERIC, 
            9, 2);
        input_Rec_Pnd_Old_Periodic_Pymt_Unit = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Old_Periodic_Pymt_Unit", "#OLD-PERIODIC-PYMT-UNIT", 
            FieldType.STRING, 9);

        input_Rec__R_Field_9 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_9", "REDEFINE", input_Rec_Pnd_Old_Periodic_Pymt_Unit);
        input_Rec_Pnd_Old_Periodic_Pymt = input_Rec__R_Field_9.newFieldInGroup("input_Rec_Pnd_Old_Periodic_Pymt", "#OLD-PERIODIC-PYMT", FieldType.NUMERIC, 
            9, 2);

        input_Rec__R_Field_10 = input_Rec__R_Field_3.newGroupInGroup("input_Rec__R_Field_10", "REDEFINE", input_Rec_Pnd_Old_Periodic_Pymt_Unit);
        input_Rec_Pnd_Old_Periodic_Unit = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Pnd_Old_Periodic_Unit", "#OLD-PERIODIC-UNIT", FieldType.NUMERIC, 
            9, 3);
        input_Rec_Pnd_Card_Filler = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Pnd_Card_Filler", "#CARD-FILLER", FieldType.STRING, 2);

        input_Rec__R_Field_11 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_11", "REDEFINE", input_Rec_Pnd_Last_48);
        input_Rec_Pnd_Intent_Code = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Intent_Code", "#INTENT-CODE", FieldType.STRING, 1);
        input_Rec_Pnd_Filler6 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Filler6", "#FILLER6", FieldType.STRING, 6);
        input_Rec_Pnd_Hold_Check_Rd = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Hold_Check_Rd", "#HOLD-CHECK-RD", FieldType.STRING, 1);
        input_Rec_Pnd_Suspend_Rc = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Suspend_Rc", "#SUSPEND-RC", FieldType.STRING, 1);
        input_Rec_Pnd_State_Residence = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_State_Residence", "#STATE-RESIDENCE", FieldType.STRING, 3);
        input_Rec_Pnd_A1_Sex = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A1_Sex", "#A1-SEX", FieldType.STRING, 1);
        input_Rec_Pnd_A1_Date_Of_Birth = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A1_Date_Of_Birth", "#A1-DATE-OF-BIRTH", FieldType.NUMERIC, 
            6);
        input_Rec_Pnd_A1_Date_Of_Death = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A1_Date_Of_Death", "#A1-DATE-OF-DEATH", FieldType.NUMERIC, 
            4);
        input_Rec_Pnd_Filler2 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Filler2", "#FILLER2", FieldType.STRING, 2);
        input_Rec_Pnd_A2_Sex = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A2_Sex", "#A2-SEX", FieldType.STRING, 1);
        input_Rec_Pnd_A2_Date_Of_Birth = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A2_Date_Of_Birth", "#A2-DATE-OF-BIRTH", FieldType.NUMERIC, 
            6);
        input_Rec_Pnd_A2_Date_Of_Death = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_A2_Date_Of_Death", "#A2-DATE-OF-DEATH", FieldType.NUMERIC, 
            4);
        input_Rec_Pnd_Filler8 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Filler8", "#FILLER8", FieldType.STRING, 8);
        input_Rec_Pnd_Eff_Dte_Chnge = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Pnd_Eff_Dte_Chnge", "#EFF-DTE-CHNGE", FieldType.NUMERIC, 4);

        input_Rec__R_Field_12 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_12", "REDEFINE", input_Rec_Pnd_Last_48);
        input_Rec_Pnd_Intent_Code_724 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Intent_Code_724", "#INTENT-CODE-724", FieldType.STRING, 1);
        input_Rec_Pnd_Sequence_Num = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Sequence_Num", "#SEQUENCE-NUM", FieldType.STRING, 3);
        input_Rec_Pnd_Tax_Authority = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Tax_Authority", "#TAX-AUTHORITY", FieldType.STRING, 3);
        input_Rec_Pnd_Active_Code = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Active_Code", "#ACTIVE-CODE", FieldType.NUMERIC, 1);
        input_Rec_Pnd_Periodic_Tax_With = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Periodic_Tax_With", "#PERIODIC-TAX-WITH", FieldType.NUMERIC, 
            7, 2);
        input_Rec_Pnd_Ytd_Tax_With = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Ytd_Tax_With", "#YTD-TAX-WITH", FieldType.NUMERIC, 9, 2);
        input_Rec_Pnd_Filler7 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Filler7", "#FILLER7", FieldType.STRING, 7);
        input_Rec_Pnd_Invest_In_Cntrct = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Invest_In_Cntrct", "#INVEST-IN-CNTRCT", FieldType.NUMERIC, 
            9, 2);
        input_Rec_Pnd_Filler8_724 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Pnd_Filler8_724", "#FILLER8-724", FieldType.STRING, 8);

        input_Rec__R_Field_13 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_13", "REDEFINE", input_Rec_Pnd_Last_48);
        input_Rec_Pnd_Intent_Code_902 = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Intent_Code_902", "#INTENT-CODE-902", FieldType.STRING, 1);
        input_Rec_Pnd_Sequence_Num_902 = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Sequence_Num_902", "#SEQUENCE-NUM-902", FieldType.STRING, 
            3);
        input_Rec_Pnd_Deduction_Cd = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Deduction_Cd", "#DEDUCTION-CD", FieldType.STRING, 3);
        input_Rec_Pnd_Deduction_Payee = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Deduction_Payee", "#DEDUCTION-PAYEE", FieldType.STRING, 5);
        input_Rec_Pnd_Periodic_Ded_Amt = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Periodic_Ded_Amt", "#PERIODIC-DED-AMT", FieldType.NUMERIC, 
            7, 2);
        input_Rec_Pnd_Total_Deduction = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Total_Deduction", "#TOTAL-DEDUCTION", FieldType.NUMERIC, 
            9, 2);
        input_Rec_Pnd_Final_Ded_Date = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Final_Ded_Date", "#FINAL-DED-DATE", FieldType.NUMERIC, 4);
        input_Rec_Pnd_Filler16 = input_Rec__R_Field_13.newFieldInGroup("input_Rec_Pnd_Filler16", "#FILLER16", FieldType.STRING, 16);

        input_Rec__R_Field_14 = input_Rec__R_Field_1.newGroupInGroup("input_Rec__R_Field_14", "REDEFINE", input_Rec_Pnd_Last_48);
        input_Rec_Pnd_Intent_Code_906 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Intent_Code_906", "#INTENT-CODE-906", FieldType.STRING, 1);
        input_Rec_Pnd_Sequence_Num_906 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Sequence_Num_906", "#SEQUENCE-NUM-906", FieldType.STRING, 
            3);
        input_Rec_Pnd_Deduction_Cd_906 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Deduction_Cd_906", "#DEDUCTION-CD-906", FieldType.STRING, 
            3);
        input_Rec_Pnd_Deduction_Payee_906 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Deduction_Payee_906", "#DEDUCTION-PAYEE-906", FieldType.STRING, 
            5);
        input_Rec_Pnd_Filler07 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Filler07", "#FILLER07", FieldType.STRING, 7);
        input_Rec_Pnd_Hist_Ded_Amt = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Hist_Ded_Amt", "#HIST-DED-AMT", FieldType.NUMERIC, 9, 2);
        input_Rec_Pnd_Filler13 = input_Rec__R_Field_14.newFieldInGroup("input_Rec_Pnd_Filler13", "#FILLER13", FieldType.STRING, 13);
        input_Rec_Pnd_User_Area = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Pnd_User_Area", "#USER-AREA", FieldType.STRING, 6);
        batch_Rec = localVariables.newFieldInRecord("batch_Rec", "BATCH-REC", FieldType.STRING, 80);

        batch_Rec__R_Field_15 = localVariables.newGroupInRecord("batch_Rec__R_Field_15", "REDEFINE", batch_Rec);
        batch_Rec_Pnd_Batch_Number = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Batch_Number", "#BATCH-NUMBER", FieldType.NUMERIC, 4);
        batch_Rec_Pnd_Check_Date = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 6);
        batch_Rec_Pnd_Field1 = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Field1", "#FIELD1", FieldType.NUMERIC, 1);
        batch_Rec_Pnd_Filler1 = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 18);
        batch_Rec_Pnd_Field2 = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Field2", "#FIELD2", FieldType.NUMERIC, 3);
        batch_Rec_Pnd_Card_Count = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Card_Count", "#CARD-COUNT", FieldType.NUMERIC, 2);
        batch_Rec_Pnd_Filler46 = batch_Rec__R_Field_15.newFieldInGroup("batch_Rec_Pnd_Filler46", "#FILLER46", FieldType.STRING, 46);

        batch_Rec__R_Field_16 = batch_Rec__R_Field_15.newGroupInGroup("batch_Rec__R_Field_16", "REDEFINE", batch_Rec_Pnd_Filler46);
        batch_Rec_Pnd_Field3 = batch_Rec__R_Field_16.newFieldInGroup("batch_Rec_Pnd_Field3", "#FIELD3", FieldType.NUMERIC, 2);
        batch_Rec_Pnd_Periodic_Pymt_Unit = batch_Rec__R_Field_16.newFieldInGroup("batch_Rec_Pnd_Periodic_Pymt_Unit", "#PERIODIC-PYMT-UNIT", FieldType.STRING, 
            10);

        batch_Rec__R_Field_17 = batch_Rec__R_Field_16.newGroupInGroup("batch_Rec__R_Field_17", "REDEFINE", batch_Rec_Pnd_Periodic_Pymt_Unit);
        batch_Rec_Pnd_Periodic_Pymt = batch_Rec__R_Field_17.newFieldInGroup("batch_Rec_Pnd_Periodic_Pymt", "#PERIODIC-PYMT", FieldType.NUMERIC, 10, 2);

        batch_Rec__R_Field_18 = batch_Rec__R_Field_16.newGroupInGroup("batch_Rec__R_Field_18", "REDEFINE", batch_Rec_Pnd_Periodic_Pymt_Unit);
        batch_Rec_Pnd_Periodic_Unit = batch_Rec__R_Field_18.newFieldInGroup("batch_Rec_Pnd_Periodic_Unit", "#PERIODIC-UNIT", FieldType.NUMERIC, 10, 3);
        batch_Rec_Pnd_Periodic_Div = batch_Rec__R_Field_16.newFieldInGroup("batch_Rec_Pnd_Periodic_Div", "#PERIODIC-DIV", FieldType.NUMERIC, 10, 2);
        batch_Rec_Pnd_Final_Pymt = batch_Rec__R_Field_16.newFieldInGroup("batch_Rec_Pnd_Final_Pymt", "#FINAL-PYMT", FieldType.NUMERIC, 13, 2);
        batch_Rec_Pnd_Filler14 = batch_Rec__R_Field_16.newFieldInGroup("batch_Rec_Pnd_Filler14", "#FILLER14", FieldType.STRING, 9);

        batch_Rec__R_Field_19 = batch_Rec__R_Field_15.newGroupInGroup("batch_Rec__R_Field_19", "REDEFINE", batch_Rec_Pnd_Filler46);
        batch_Rec_Pnd_Transaction_Count = batch_Rec__R_Field_19.newFieldInGroup("batch_Rec_Pnd_Transaction_Count", "#TRANSACTION-COUNT", FieldType.NUMERIC, 
            2);
        batch_Rec_Pnd_Filler44 = batch_Rec__R_Field_19.newFieldInGroup("batch_Rec_Pnd_Filler44", "#FILLER44", FieldType.STRING, 44);

        batch_Rec__R_Field_20 = batch_Rec__R_Field_15.newGroupInGroup("batch_Rec__R_Field_20", "REDEFINE", batch_Rec_Pnd_Filler46);
        batch_Rec_Pnd_Transaction_Count_724 = batch_Rec__R_Field_20.newFieldInGroup("batch_Rec_Pnd_Transaction_Count_724", "#TRANSACTION-COUNT-724", FieldType.NUMERIC, 
            2);
        batch_Rec_Pnd_Periodic_Tax_With = batch_Rec__R_Field_20.newFieldInGroup("batch_Rec_Pnd_Periodic_Tax_With", "#PERIODIC-TAX-WITH", FieldType.NUMERIC, 
            10, 2);
        batch_Rec_Pnd_Ytd_Tax_With = batch_Rec__R_Field_20.newFieldInGroup("batch_Rec_Pnd_Ytd_Tax_With", "#YTD-TAX-WITH", FieldType.NUMERIC, 10, 2);
        batch_Rec_Pnd_Invest_In_Cntrct = batch_Rec__R_Field_20.newFieldInGroup("batch_Rec_Pnd_Invest_In_Cntrct", "#INVEST-IN-CNTRCT", FieldType.NUMERIC, 
            10, 2);
        batch_Rec_Pnd_Filler14_724 = batch_Rec__R_Field_20.newFieldInGroup("batch_Rec_Pnd_Filler14_724", "#FILLER14-724", FieldType.STRING, 14);

        batch_Rec__R_Field_21 = batch_Rec__R_Field_15.newGroupInGroup("batch_Rec__R_Field_21", "REDEFINE", batch_Rec_Pnd_Filler46);
        batch_Rec_Pnd_Transaction_Count_902 = batch_Rec__R_Field_21.newFieldInGroup("batch_Rec_Pnd_Transaction_Count_902", "#TRANSACTION-COUNT-902", FieldType.NUMERIC, 
            2);
        batch_Rec_Pnd_Periodic_Ded_Amt = batch_Rec__R_Field_21.newFieldInGroup("batch_Rec_Pnd_Periodic_Ded_Amt", "#PERIODIC-DED-AMT", FieldType.NUMERIC, 
            10, 2);
        batch_Rec_Pnd_Filelr34 = batch_Rec__R_Field_21.newFieldInGroup("batch_Rec_Pnd_Filelr34", "#FILELR34", FieldType.STRING, 34);
        output_Rec = localVariables.newFieldInRecord("output_Rec", "OUTPUT-REC", FieldType.STRING, 80);

        output_Rec__R_Field_22 = localVariables.newGroupInRecord("output_Rec__R_Field_22", "REDEFINE", output_Rec);
        output_Rec_Output_Rec_Batch = output_Rec__R_Field_22.newFieldInGroup("output_Rec_Output_Rec_Batch", "OUTPUT-REC-BATCH", FieldType.NUMERIC, 4);
        output_Rec_Output_Rec_76 = output_Rec__R_Field_22.newFieldInGroup("output_Rec_Output_Rec_76", "OUTPUT-REC-76", FieldType.STRING, 76);

        output_Rec__R_Field_23 = output_Rec__R_Field_22.newGroupInGroup("output_Rec__R_Field_23", "REDEFINE", output_Rec_Output_Rec_76);
        output_Rec_Pnd_Out_Data = output_Rec__R_Field_23.newFieldInGroup("output_Rec_Pnd_Out_Data", "#OUT-DATA", FieldType.STRING, 30);
        output_Rec__Filler1 = output_Rec__R_Field_23.newFieldInGroup("output_Rec__Filler1", "_FILLER1", FieldType.STRING, 1);
        output_Rec_Pnd_Out_Product = output_Rec__R_Field_23.newFieldInGroup("output_Rec_Pnd_Out_Product", "#OUT-PRODUCT", FieldType.STRING, 1);
        output_Rec__Filler2 = output_Rec__R_Field_23.newFieldInGroup("output_Rec__Filler2", "_FILLER2", FieldType.STRING, 44);

        miscellaneous_Fields = localVariables.newGroupInRecord("miscellaneous_Fields", "MISCELLANEOUS-FIELDS");
        miscellaneous_Fields_Pnd_Next_Batch_Number = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_Next_Batch_Number", "#NEXT-BATCH-NUMBER", 
            FieldType.NUMERIC, 4);
        miscellaneous_Fields_Pnd_W_Card_Count = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Card_Count", "#W-CARD-COUNT", FieldType.PACKED_DECIMAL, 
            3);
        miscellaneous_Fields_Pnd_Sw_First = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_Sw_First", "#SW-FIRST", FieldType.STRING, 1);
        miscellaneous_Fields_Pnd_W_Trans_Count = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Trans_Count", "#W-TRANS-COUNT", FieldType.PACKED_DECIMAL, 
            3);
        miscellaneous_Fields_Pnd_W_Hold_Check_Date = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Check_Date", "#W-HOLD-CHECK-DATE", 
            FieldType.NUMERIC, 6);
        miscellaneous_Fields_Pnd_W_Hold_User_Area = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_User_Area", "#W-HOLD-USER-AREA", 
            FieldType.STRING, 6);
        miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr", "#W-HOLD-CNTRCT-NBR", 
            FieldType.STRING, 10);

        miscellaneous_Fields__R_Field_24 = miscellaneous_Fields.newGroupInGroup("miscellaneous_Fields__R_Field_24", "REDEFINE", miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr);
        miscellaneous_Fields_Pnd_W_Hold_Contract_Prefix = miscellaneous_Fields__R_Field_24.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Contract_Prefix", 
            "#W-HOLD-CONTRACT-PREFIX", FieldType.STRING, 1);
        miscellaneous_Fields_Pnd_W_Hold_Contract_End = miscellaneous_Fields__R_Field_24.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Contract_End", 
            "#W-HOLD-CONTRACT-END", FieldType.STRING, 7);
        miscellaneous_Fields_Pnd_W_Hold_Contract_Payee = miscellaneous_Fields__R_Field_24.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Contract_Payee", 
            "#W-HOLD-CONTRACT-PAYEE", FieldType.STRING, 2);

        miscellaneous_Fields__R_Field_25 = miscellaneous_Fields.newGroupInGroup("miscellaneous_Fields__R_Field_25", "REDEFINE", miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr);
        miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr8 = miscellaneous_Fields__R_Field_25.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr8", 
            "#W-HOLD-CNTRCT-NBR8", FieldType.STRING, 8);
        miscellaneous_Fields_Pnd_W_Display_Cntrct = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Display_Cntrct", "#W-DISPLAY-CNTRCT", 
            FieldType.STRING, 11);

        miscellaneous_Fields__R_Field_26 = miscellaneous_Fields.newGroupInGroup("miscellaneous_Fields__R_Field_26", "REDEFINE", miscellaneous_Fields_Pnd_W_Display_Cntrct);
        miscellaneous_Fields_Pnd_W_Display_Cntrct_Nbr = miscellaneous_Fields__R_Field_26.newFieldInGroup("miscellaneous_Fields_Pnd_W_Display_Cntrct_Nbr", 
            "#W-DISPLAY-CNTRCT-NBR", FieldType.STRING, 8);
        miscellaneous_Fields_Pnd_Filler = miscellaneous_Fields__R_Field_26.newFieldInGroup("miscellaneous_Fields_Pnd_Filler", "#FILLER", FieldType.STRING, 
            1);
        miscellaneous_Fields_Pnd_W_Display_Cntrct_Payee = miscellaneous_Fields__R_Field_26.newFieldInGroup("miscellaneous_Fields_Pnd_W_Display_Cntrct_Payee", 
            "#W-DISPLAY-CNTRCT-PAYEE", FieldType.STRING, 2);
        miscellaneous_Fields_Pnd_W_Hold_Transaction_Num = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Transaction_Num", "#W-HOLD-TRANSACTION-NUM", 
            FieldType.NUMERIC, 3);
        miscellaneous_Fields_Pnd_W_Hold_Sequence_Number = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Sequence_Number", "#W-HOLD-SEQUENCE-NUMBER", 
            FieldType.STRING, 3);
        miscellaneous_Fields_Pnd_W_Hold_Product = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Product", "#W-HOLD-PRODUCT", FieldType.STRING, 
            1);
        miscellaneous_Fields_Pnd_W_Hold_Mode = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Mode", "#W-HOLD-MODE", FieldType.STRING, 
            3);
        miscellaneous_Fields_Pnd_W_Hold_Record_Type = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Record_Type", "#W-HOLD-RECORD-TYPE", 
            FieldType.NUMERIC, 2);
        miscellaneous_Fields_Pnd_W_Hold_Record_Number = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Hold_Record_Number", "#W-HOLD-RECORD-NUMBER", 
            FieldType.NUMERIC, 1);
        miscellaneous_Fields_Pnd_W_Periodic_Pymt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Periodic_Pymt", "#W-PERIODIC-PYMT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Periodic_Unit = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Periodic_Unit", "#W-PERIODIC-UNIT", 
            FieldType.NUMERIC, 10, 3);
        miscellaneous_Fields_Pnd_W_Periodic_Div = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Periodic_Div", "#W-PERIODIC-DIV", FieldType.NUMERIC, 
            10, 2);
        miscellaneous_Fields_Pnd_W_Final_Pymt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Final_Pymt", "#W-FINAL-PYMT", FieldType.NUMERIC, 
            12, 2);
        miscellaneous_Fields_Pnd_W_Periodic_Tax_With = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Periodic_Tax_With", "#W-PERIODIC-TAX-WITH", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Ytd_Tax_With = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Ytd_Tax_With", "#W-YTD-TAX-WITH", FieldType.NUMERIC, 
            10, 2);
        miscellaneous_Fields_Pnd_W_Invest_In_Cntrct = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Invest_In_Cntrct", "#W-INVEST-IN-CNTRCT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt", "#W-PERIODIC-DED-AMT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Date_Out = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 
            8);
        miscellaneous_Fields_Pnd_W_Page_Ctr = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.PACKED_DECIMAL, 
            9);
        miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches", "#W-TOT-NBR-BATCHES", 
            FieldType.NUMERIC, 4);
        miscellaneous_Fields_Pnd_W_Tot_Card_Count = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Card_Count", "#W-TOT-CARD-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        miscellaneous_Fields_Pnd_W_Tot_Trans_Count = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Trans_Count", "#W-TOT-TRANS-COUNT", 
            FieldType.PACKED_DECIMAL, 7);
        miscellaneous_Fields_Pnd_W_Tot_Periodic_Pymt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Periodic_Pymt", "#W-TOT-PERIODIC-PYMT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Tot_Periodic_Unit = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Periodic_Unit", "#W-TOT-PERIODIC-UNIT", 
            FieldType.NUMERIC, 10, 3);
        miscellaneous_Fields_Pnd_W_Tot_Periodic_Div = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Periodic_Div", "#W-TOT-PERIODIC-DIV", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Tot_Final_Pymt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Final_Pymt", "#W-TOT-FINAL-PYMT", 
            FieldType.NUMERIC, 13, 2);
        miscellaneous_Fields_Pnd_W_Tot_Invest_In_Cntrct = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Invest_In_Cntrct", "#W-TOT-INVEST-IN-CNTRCT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt", "#W-TOT-PERIODIC-DED-AMT", 
            FieldType.NUMERIC, 10, 2);
        miscellaneous_Fields_Pnd_W_Tot_Ded_Amt = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Tot_Ded_Amt", "#W-TOT-DED-AMT", FieldType.NUMERIC, 
            9, 2);
        miscellaneous_Fields_Pnd_W_Indx = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_Indx", "#W-INDX", FieldType.PACKED_DECIMAL, 
            3);
        miscellaneous_Fields_Pnd_W_C_Indx = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_C_Indx", "#W-C-INDX", FieldType.PACKED_DECIMAL, 
            3);
        miscellaneous_Fields_Pnd_W_M_Indx = miscellaneous_Fields.newFieldInGroup("miscellaneous_Fields_Pnd_W_M_Indx", "#W-M-INDX", FieldType.PACKED_DECIMAL, 
            3);

        mode_Report_Array = localVariables.newGroupInRecord("mode_Report_Array", "MODE-REPORT-ARRAY");
        mode_Report_Array_Pnd_W_Rpt_Mode = mode_Report_Array.newFieldArrayInGroup("mode_Report_Array_Pnd_W_Rpt_Mode", "#W-RPT-MODE", FieldType.STRING, 
            3, new DbsArrayController(1, 22));
        mode_Report_Array_Pnd_W_T_Rpt_Amt = mode_Report_Array.newFieldArrayInGroup("mode_Report_Array_Pnd_W_T_Rpt_Amt", "#W-T-RPT-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2, 1, 22));
        mode_Report_Array_Pnd_W_C_Rpt_Units = mode_Report_Array.newFieldArrayInGroup("mode_Report_Array_Pnd_W_C_Rpt_Units", "#W-C-RPT-UNITS", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 7, 1, 22));

        pnd_Work_Aread = localVariables.newGroupInRecord("pnd_Work_Aread", "#WORK-AREAD");
        pnd_Work_Aread_Pnd_Periodic_Pymt_Unit_P = pnd_Work_Aread.newFieldInGroup("pnd_Work_Aread_Pnd_Periodic_Pymt_Unit_P", "#PERIODIC-PYMT-UNIT-P", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_Aread_Pnd_Mode_A = pnd_Work_Aread.newFieldInGroup("pnd_Work_Aread_Pnd_Mode_A", "#MODE-A", FieldType.STRING, 3);
        pnd_Work_Aread_Pnd_Periodic_Pymt_Conv = pnd_Work_Aread.newFieldInGroup("pnd_Work_Aread_Pnd_Periodic_Pymt_Conv", "#PERIODIC-PYMT-CONV", FieldType.STRING, 
            9);

        pnd_Work_Aread__R_Field_27 = pnd_Work_Aread.newGroupInGroup("pnd_Work_Aread__R_Field_27", "REDEFINE", pnd_Work_Aread_Pnd_Periodic_Pymt_Conv);
        pnd_Work_Aread_Pnd_Periodic_Pymt_A = pnd_Work_Aread__R_Field_27.newFieldInGroup("pnd_Work_Aread_Pnd_Periodic_Pymt_A", "#PERIODIC-PYMT-A", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_Aread_Pnd_Periodic_Unit_Conv = pnd_Work_Aread.newFieldInGroup("pnd_Work_Aread_Pnd_Periodic_Unit_Conv", "#PERIODIC-UNIT-CONV", FieldType.STRING, 
            9);

        pnd_Work_Aread__R_Field_28 = pnd_Work_Aread.newGroupInGroup("pnd_Work_Aread__R_Field_28", "REDEFINE", pnd_Work_Aread_Pnd_Periodic_Unit_Conv);
        pnd_Work_Aread_Pnd_Periodic_Unit_A = pnd_Work_Aread__R_Field_28.newFieldInGroup("pnd_Work_Aread_Pnd_Periodic_Unit_A", "#PERIODIC-UNIT-A", FieldType.NUMERIC, 
            9, 3);
        pnd_Total_Trans = localVariables.newFieldArrayInRecord("pnd_Total_Trans", "#TOTAL-TRANS", FieldType.NUMERIC, 6, new DbsArrayController(1, 17));
        pnd_W_User_Area_Trans_Count = localVariables.newFieldArrayInRecord("pnd_W_User_Area_Trans_Count", "#W-USER-AREA-TRANS-COUNT", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 17));
        pnd_W_Printing_User_Totals = localVariables.newFieldInRecord("pnd_W_Printing_User_Totals", "#W-PRINTING-USER-TOTALS", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        miscellaneous_Fields_Pnd_Next_Batch_Number.setInitialValue(250);
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(1).setInitialValue("100");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(2).setInitialValue("601");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(3).setInitialValue("602");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(4).setInitialValue("603");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(5).setInitialValue("701");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(6).setInitialValue("702");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(7).setInitialValue("703");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(8).setInitialValue("704");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(9).setInitialValue("705");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(10).setInitialValue("706");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(11).setInitialValue("801");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(12).setInitialValue("802");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(13).setInitialValue("803");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(14).setInitialValue("804");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(15).setInitialValue("805");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(16).setInitialValue("806");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(17).setInitialValue("807");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(18).setInitialValue("808");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(19).setInitialValue("809");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(20).setInitialValue("810");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(21).setInitialValue("811");
        mode_Report_Array_Pnd_W_Rpt_Mode.getValue(22).setInitialValue("812");
        pnd_Total_Trans.getValue(1).setInitialValue(0);
        pnd_W_User_Area_Trans_Count.getValue(1).setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap905() throws Exception
    {
        super("Iaap905");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 56;//Natural: FORMAT ( 01 ) LS = 133 PS = 56;//Natural: FORMAT ( 02 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        miscellaneous_Fields_Pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                   //Natural: ASSIGN #W-DATE-OUT := *DATU
        batch_Rec_Pnd_Field1.setValue(9);                                                                                                                                 //Natural: ASSIGN #FIELD1 := 9
        batch_Rec_Pnd_Field2.setValue(999);                                                                                                                               //Natural: ASSIGN #FIELD2 := 999
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 INPUT-REC
        while (condition(getWorkFiles().read(1, input_Rec)))
        {
            if (condition(miscellaneous_Fields_Pnd_Sw_First.equals("Y")))                                                                                                 //Natural: IF #SW-FIRST = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-BREAK
                sub_Check_For_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            miscellaneous_Fields_Pnd_Sw_First.setValue("Y");                                                                                                              //Natural: MOVE 'Y' TO #SW-FIRST
            //*  WRITING INPUT RECORD WITH BATCH NUMBER
            output_Rec_Output_Rec_Batch.setValue(miscellaneous_Fields_Pnd_Next_Batch_Number);                                                                             //Natural: MOVE #NEXT-BATCH-NUMBER TO OUTPUT-REC-BATCH
            output_Rec_Output_Rec_76.setValue(input_Rec.getSubstring(5,76));                                                                                              //Natural: ASSIGN OUTPUT-REC-76 := SUBSTR ( INPUT-REC,5,76 )
            getWorkFiles().write(2, false, output_Rec);                                                                                                                   //Natural: WRITE WORK FILE 2 OUTPUT-REC
            //*  SAVING INFO FOR LATER USE
            miscellaneous_Fields_Pnd_W_Hold_Check_Date.setValue(input_Rec_Pnd_Check_Date);                                                                                //Natural: MOVE INPUT-REC.#CHECK-DATE TO #W-HOLD-CHECK-DATE
            miscellaneous_Fields_Pnd_W_Hold_User_Area.setValue(input_Rec_Pnd_User_Area);                                                                                  //Natural: MOVE INPUT-REC.#USER-AREA TO #W-HOLD-USER-AREA
            miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.setValue(input_Rec_Pnd_Transaction_Num);                                                                      //Natural: MOVE INPUT-REC.#TRANSACTION-NUM TO #W-HOLD-TRANSACTION-NUM
            miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr.setValue(input_Rec_Pnd_Contract_Number);                                                                           //Natural: MOVE INPUT-REC.#CONTRACT-NUMBER TO #W-HOLD-CNTRCT-NBR
            miscellaneous_Fields_Pnd_W_Hold_Record_Type.setValue(input_Rec_Pnd_Record_Type);                                                                              //Natural: MOVE INPUT-REC.#RECORD-TYPE TO #W-HOLD-RECORD-TYPE
            miscellaneous_Fields_Pnd_W_Hold_Record_Number.setValue(input_Rec_Pnd_Record_Number);                                                                          //Natural: MOVE INPUT-REC.#RECORD-NUMBER TO #W-HOLD-RECORD-NUMBER
            if (condition(input_Rec_Pnd_Transaction_Num.equals(902)))                                                                                                     //Natural: IF INPUT-REC.#TRANSACTION-NUM = 902
            {
                miscellaneous_Fields_Pnd_W_Hold_Sequence_Number.setValue(input_Rec_Pnd_Sequence_Num_902);                                                                 //Natural: MOVE INPUT-REC.#SEQUENCE-NUM-902 TO #W-HOLD-SEQUENCE-NUMBER
            }                                                                                                                                                             //Natural: END-IF
            if (condition(input_Rec_Pnd_Transaction_Num.equals(906)))                                                                                                     //Natural: IF INPUT-REC.#TRANSACTION-NUM = 906
            {
                miscellaneous_Fields_Pnd_W_Hold_Sequence_Number.setValue(input_Rec_Pnd_Sequence_Num_906);                                                                 //Natural: MOVE INPUT-REC.#SEQUENCE-NUM-906 TO #W-HOLD-SEQUENCE-NUMBER
            }                                                                                                                                                             //Natural: END-IF
            miscellaneous_Fields_Pnd_W_Card_Count.nadd(1);                                                                                                                //Natural: ADD 1 TO #W-CARD-COUNT
            miscellaneous_Fields_Pnd_W_Tot_Card_Count.nadd(1);                                                                                                            //Natural: ADD 1 TO #W-TOT-CARD-COUNT
            if (condition(input_Rec_Pnd_Transaction_Num.equals(33) || input_Rec_Pnd_Transaction_Num.equals(35) || input_Rec_Pnd_Transaction_Num.equals(50)))              //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 33 OR = 35 OR = 50 )
            {
                if (condition((input_Rec_Pnd_Transaction_Num.equals(50))))                                                                                                //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 50 )
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PROD-CODE
                    sub_Determine_Prod_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition((input_Rec_Pnd_Transaction_Num.equals(50)) && input_Rec_Pnd_Record_Type.equals(10) && input_Rec_Pnd_Record_Number.equals(1)))               //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 50 ) AND INPUT-REC.#RECORD-TYPE = 10 AND INPUT-REC.#RECORD-NUMBER = 1
                {
                    miscellaneous_Fields_Pnd_W_Hold_Mode.setValue(input_Rec_Pnd_Mode);                                                                                    //Natural: MOVE INPUT-REC.#MODE TO #W-HOLD-MODE
                }                                                                                                                                                         //Natural: END-IF
                if (condition((((input_Rec_Pnd_Transaction_Num.equals(33) || input_Rec_Pnd_Transaction_Num.equals(35)) && input_Rec_Pnd_Record_Type.equals(10))           //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 33 OR = 35 ) AND INPUT-REC.#RECORD-TYPE = 10 AND INPUT-REC.#RECORD-NUMBER = 1
                    && input_Rec_Pnd_Record_Number.equals(1))))
                {
                    miscellaneous_Fields_Pnd_W_Hold_Product.setValue(input_Rec_Pnd_Product);                                                                              //Natural: MOVE INPUT-REC.#PRODUCT TO #W-HOLD-PRODUCT
                    miscellaneous_Fields_Pnd_W_Hold_Mode.setValue(input_Rec_Pnd_Mode);                                                                                    //Natural: MOVE INPUT-REC.#MODE TO #W-HOLD-MODE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(input_Rec_Pnd_Record_Type.greaterOrEqual(50) && input_Rec_Pnd_Record_Type.lessOrEqual(59)))                                                 //Natural: IF INPUT-REC.#RECORD-TYPE = 50 THRU 59
                {
                    miscellaneous_Fields_Pnd_W_Periodic_Div.nadd(input_Rec_Pnd_Periodic_Div);                                                                             //Natural: ADD INPUT-REC.#PERIODIC-DIV TO #W-PERIODIC-DIV
                    miscellaneous_Fields_Pnd_W_Tot_Periodic_Div.nadd(input_Rec_Pnd_Periodic_Div);                                                                         //Natural: ADD INPUT-REC.#PERIODIC-DIV TO #W-TOT-PERIODIC-DIV
                    miscellaneous_Fields_Pnd_W_Final_Pymt.nadd(input_Rec_Pnd_Final_Pymt);                                                                                 //Natural: ADD INPUT-REC.#FINAL-PYMT TO #W-FINAL-PYMT
                    miscellaneous_Fields_Pnd_W_Tot_Final_Pymt.nadd(input_Rec_Pnd_Final_Pymt);                                                                             //Natural: ADD INPUT-REC.#FINAL-PYMT TO #W-TOT-FINAL-PYMT
                    if (condition(input_Rec_Pnd_Contract_Prefix.equals("0") || input_Rec_Pnd_Contract_Prefix.equals("6")))                                                //Natural: IF #CONTRACT-PREFIX = '0' OR = '6'
                    {
                        pnd_Work_Aread_Pnd_Periodic_Unit_Conv.setValueEdited(input_Rec_Pnd_Periodic_Unit,new ReportEditMask("999999V999"));                               //Natural: MOVE EDITED INPUT-REC.#PERIODIC-UNIT ( EM = 999999V999 ) TO #PERIODIC-UNIT-CONV
                        //*             ADD INPUT-REC.#PERIODIC-UNIT TO #W-PERIODIC-UNIT
                        //*             ADD INPUT-REC.#PERIODIC-UNIT TO #W-TOT-PERIODIC-UNIT
                        miscellaneous_Fields_Pnd_W_Periodic_Unit.nadd(pnd_Work_Aread_Pnd_Periodic_Unit_A);                                                                //Natural: ADD #PERIODIC-UNIT-A TO #W-PERIODIC-UNIT
                        miscellaneous_Fields_Pnd_W_Tot_Periodic_Unit.nadd(pnd_Work_Aread_Pnd_Periodic_Unit_A);                                                            //Natural: ADD #PERIODIC-UNIT-A TO #W-TOT-PERIODIC-UNIT
                        FOR01:                                                                                                                                            //Natural: FOR #W-M-INDX 1 TO 22
                        for (miscellaneous_Fields_Pnd_W_M_Indx.setValue(1); condition(miscellaneous_Fields_Pnd_W_M_Indx.lessOrEqual(22)); miscellaneous_Fields_Pnd_W_M_Indx.nadd(1))
                        {
                            if (condition(miscellaneous_Fields_Pnd_W_Hold_Mode.equals(mode_Report_Array_Pnd_W_Rpt_Mode.getValue(miscellaneous_Fields_Pnd_W_M_Indx))))     //Natural: IF #W-HOLD-MODE = #W-RPT-MODE ( #W-M-INDX )
                            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PROD-INDX
                                sub_Determine_Prod_Indx();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*                 ADD INPUT-REC.#PERIODIC-UNIT TO
                                mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(miscellaneous_Fields_Pnd_W_C_Indx,miscellaneous_Fields_Pnd_W_M_Indx).nadd(pnd_Work_Aread_Pnd_Periodic_Unit_A); //Natural: ADD #PERIODIC-UNIT-A TO #W-C-RPT-UNITS ( #W-C-INDX,#W-M-INDX )
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  MOVE INPUT-REC.#PERIODIC-PYMT        TO #PERIODIC-PYMT-A
                        pnd_Work_Aread_Pnd_Periodic_Pymt_Conv.setValue(input_Rec_Pnd_Periodic_Pymt);                                                                      //Natural: MOVE INPUT-REC.#PERIODIC-PYMT TO #PERIODIC-PYMT-CONV
                        pnd_Work_Aread_Pnd_Periodic_Pymt_Conv.setValueEdited(input_Rec_Pnd_Periodic_Pymt,new ReportEditMask("9999999V99"));                               //Natural: MOVE EDITED INPUT-REC.#PERIODIC-PYMT ( EM = 9999999V99 ) TO #PERIODIC-PYMT-CONV
                        //*                 TO #PERIODIC-PYMT-A
                        //*               ADD INPUT-REC.#PERIODIC-PYMT TO #W-PERIODIC-PYMT
                        //*               ADD INPUT-REC.#PERIODIC-PYMT TO #W-TOT-PERIODIC-PYMT
                        miscellaneous_Fields_Pnd_W_Periodic_Pymt.nadd(pnd_Work_Aread_Pnd_Periodic_Pymt_A);                                                                //Natural: ADD #PERIODIC-PYMT-A TO #W-PERIODIC-PYMT
                        miscellaneous_Fields_Pnd_W_Tot_Periodic_Pymt.nadd(pnd_Work_Aread_Pnd_Periodic_Pymt_A);                                                            //Natural: ADD #PERIODIC-PYMT-A TO #W-TOT-PERIODIC-PYMT
                        FOR02:                                                                                                                                            //Natural: FOR #W-M-INDX 1 TO 22
                        for (miscellaneous_Fields_Pnd_W_M_Indx.setValue(1); condition(miscellaneous_Fields_Pnd_W_M_Indx.lessOrEqual(22)); miscellaneous_Fields_Pnd_W_M_Indx.nadd(1))
                        {
                            if (condition(miscellaneous_Fields_Pnd_W_Hold_Mode.equals(mode_Report_Array_Pnd_W_Rpt_Mode.getValue(miscellaneous_Fields_Pnd_W_M_Indx))))     //Natural: IF #W-HOLD-MODE = #W-RPT-MODE ( #W-M-INDX )
                            {
                                //*                  ADD INPUT-REC.#PERIODIC-PYMT TO
                                mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,miscellaneous_Fields_Pnd_W_M_Indx).nadd(pnd_Work_Aread_Pnd_Periodic_Pymt_A);                 //Natural: ADD #PERIODIC-PYMT-A TO #W-T-RPT-AMT ( 1,#W-M-INDX )
                                mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,miscellaneous_Fields_Pnd_W_M_Indx).nadd(input_Rec_Pnd_Periodic_Div);                         //Natural: ADD INPUT-REC.#PERIODIC-DIV TO #W-T-RPT-AMT ( 2,#W-M-INDX )
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition((input_Rec_Pnd_Transaction_Num.equals(724))))                                                                                                   //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 724 )
            {
                miscellaneous_Fields_Pnd_W_Periodic_Tax_With.nadd(input_Rec_Pnd_Periodic_Tax_With);                                                                       //Natural: ADD INPUT-REC.#PERIODIC-TAX-WITH TO #W-PERIODIC-TAX-WITH
                miscellaneous_Fields_Pnd_W_Ytd_Tax_With.nadd(input_Rec_Pnd_Ytd_Tax_With);                                                                                 //Natural: ADD INPUT-REC.#YTD-TAX-WITH TO #W-YTD-TAX-WITH
                miscellaneous_Fields_Pnd_W_Invest_In_Cntrct.nadd(input_Rec_Pnd_Invest_In_Cntrct);                                                                         //Natural: ADD INPUT-REC.#INVEST-IN-CNTRCT TO #W-INVEST-IN-CNTRCT
                miscellaneous_Fields_Pnd_W_Tot_Invest_In_Cntrct.nadd(input_Rec_Pnd_Invest_In_Cntrct);                                                                     //Natural: ADD INPUT-REC.#INVEST-IN-CNTRCT TO #W-TOT-INVEST-IN-CNTRCT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(input_Rec_Pnd_Transaction_Num.equals(902) || input_Rec_Pnd_Transaction_Num.equals(906)))                                                        //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 902 OR = 906 )
            {
                if (condition((input_Rec_Pnd_Transaction_Num.equals(902))))                                                                                               //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 902 )
                {
                    miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt.nadd(input_Rec_Pnd_Periodic_Ded_Amt);                                                                     //Natural: ADD INPUT-REC.#PERIODIC-DED-AMT TO #W-PERIODIC-DED-AMT
                    miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt.nadd(input_Rec_Pnd_Periodic_Ded_Amt);                                                                 //Natural: ADD INPUT-REC.#PERIODIC-DED-AMT TO #W-TOT-PERIODIC-DED-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition((input_Rec_Pnd_Transaction_Num.equals(906))))                                                                                               //Natural: IF ( INPUT-REC.#TRANSACTION-NUM = 906 )
                {
                    miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt.nadd(input_Rec_Pnd_Hist_Ded_Amt);                                                                         //Natural: ADD INPUT-REC.#HIST-DED-AMT TO #W-PERIODIC-DED-AMT
                    miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt.nadd(input_Rec_Pnd_Hist_Ded_Amt);                                                                     //Natural: ADD INPUT-REC.#HIST-DED-AMT TO #W-TOT-PERIODIC-DED-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        input_Rec_Pnd_Contract_Number.setValue("        ");                                                                                                               //Natural: MOVE '        ' TO INPUT-REC.#CONTRACT-NUMBER
        input_Rec_Pnd_Transaction_Num.setValue(0);                                                                                                                        //Natural: MOVE 0 TO INPUT-REC.#TRANSACTION-NUM
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-BREAK
        sub_Check_For_Break();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-USER-AREA-TOTALS
        sub_Print_User_Area_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
        sub_Print_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-MODE-REPORT
        sub_Write_Mode_Report();
        if (condition(Global.isEscape())) {return;}
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-BREAK
        //*                        94T #W-FINAL-PYMT (EM=Z,ZZZ,ZZZ,ZZZ.99)
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TRANS
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-USER-AREA-TOTALS
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
        //*             94T #W-TOT-FINAL-PYMT (EM=Z,ZZZ,ZZZ.99)
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MODE-REPORT
        //* **********************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PROD-CODE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PROD-INDX
    }
    private void sub_Check_For_Break() throws Exception                                                                                                                   //Natural: CHECK-FOR-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(input_Rec_Pnd_Transaction_Num.equals(902)))                                                                                                         //Natural: IF INPUT-REC.#TRANSACTION-NUM = 902
        {
            if (condition((input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area)) || (input_Rec_Pnd_Contract_Number.notEquals(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr))  //Natural: IF ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( INPUT-REC.#CONTRACT-NUMBER NOT = #W-HOLD-CNTRCT-NBR ) OR ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM ) OR ( INPUT-REC.#SEQUENCE-NUM-902 NOT = #W-HOLD-SEQUENCE-NUMBER )
                || (input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num)) || (input_Rec_Pnd_Sequence_Num_902.notEquals(miscellaneous_Fields_Pnd_W_Hold_Sequence_Number))))
            {
                miscellaneous_Fields_Pnd_W_Trans_Count.nadd(1);                                                                                                           //Natural: ADD 1 TO #W-TRANS-COUNT
                miscellaneous_Fields_Pnd_W_Tot_Trans_Count.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TOT-TRANS-COUNT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TRANS
                sub_Calculate_Trans();
                if (condition(Global.isEscape())) {return;}
                //*         ADD 1 TO #W-USER-AREA-TRANS-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(input_Rec_Pnd_Transaction_Num.equals(906)))                                                                                                     //Natural: IF INPUT-REC.#TRANSACTION-NUM = 906
            {
                if (condition((input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area)) || (input_Rec_Pnd_Contract_Number.notEquals(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr))  //Natural: IF ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( INPUT-REC.#CONTRACT-NUMBER NOT = #W-HOLD-CNTRCT-NBR ) OR ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM ) OR ( INPUT-REC.#SEQUENCE-NUM-906 NOT = #W-HOLD-SEQUENCE-NUMBER )
                    || (input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num)) || (input_Rec_Pnd_Sequence_Num_906.notEquals(miscellaneous_Fields_Pnd_W_Hold_Sequence_Number))))
                {
                    miscellaneous_Fields_Pnd_W_Trans_Count.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TRANS-COUNT
                    miscellaneous_Fields_Pnd_W_Tot_Trans_Count.nadd(1);                                                                                                   //Natural: ADD 1 TO #W-TOT-TRANS-COUNT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TRANS
                    sub_Calculate_Trans();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition((input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area)) || (input_Rec_Pnd_Contract_Number.notEquals(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr))  //Natural: IF ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( INPUT-REC.#CONTRACT-NUMBER NOT = #W-HOLD-CNTRCT-NBR ) OR ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM )
                    || (input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num))))
                {
                    miscellaneous_Fields_Pnd_W_Trans_Count.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TRANS-COUNT
                    miscellaneous_Fields_Pnd_W_Tot_Trans_Count.nadd(1);                                                                                                   //Natural: ADD 1 TO #W-TOT-TRANS-COUNT
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TRANS
                    sub_Calculate_Trans();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        if (condition(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(33) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(35) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(50))) //Natural: IF ( #W-HOLD-TRANSACTION-NUM = 33 OR = 35 OR = 50 )
        {
            if (condition((input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area)) || (input_Rec_Pnd_Contract_Number.notEquals(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr))  //Natural: IF ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( INPUT-REC.#CONTRACT-NUMBER NOT = #W-HOLD-CNTRCT-NBR ) OR ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM )
                || (input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num))))
            {
                batch_Rec_Pnd_Filler46.reset();                                                                                                                           //Natural: RESET #FILLER46
                batch_Rec_Pnd_Batch_Number.setValue(miscellaneous_Fields_Pnd_Next_Batch_Number);                                                                          //Natural: MOVE #NEXT-BATCH-NUMBER TO BATCH-REC.#BATCH-NUMBER
                batch_Rec_Pnd_Check_Date.setValue(miscellaneous_Fields_Pnd_W_Hold_Check_Date);                                                                            //Natural: MOVE #W-HOLD-CHECK-DATE TO BATCH-REC.#CHECK-DATE
                batch_Rec_Pnd_Card_Count.setValue(miscellaneous_Fields_Pnd_W_Card_Count);                                                                                 //Natural: MOVE #W-CARD-COUNT TO BATCH-REC.#CARD-COUNT
                batch_Rec_Pnd_Field3.setValue(1);                                                                                                                         //Natural: MOVE 01 TO BATCH-REC.#FIELD3
                batch_Rec_Pnd_Periodic_Div.setValue(miscellaneous_Fields_Pnd_W_Periodic_Div);                                                                             //Natural: MOVE #W-PERIODIC-DIV TO BATCH-REC.#PERIODIC-DIV
                batch_Rec_Pnd_Final_Pymt.setValue(miscellaneous_Fields_Pnd_W_Final_Pymt);                                                                                 //Natural: MOVE #W-FINAL-PYMT TO BATCH-REC.#FINAL-PYMT
                miscellaneous_Fields_Pnd_W_Display_Cntrct_Nbr.setValue(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr8);                                                      //Natural: ASSIGN #W-DISPLAY-CNTRCT-NBR := #W-HOLD-CNTRCT-NBR8
                miscellaneous_Fields_Pnd_W_Display_Cntrct_Payee.setValue(miscellaneous_Fields_Pnd_W_Hold_Contract_Payee);                                                 //Natural: ASSIGN #W-DISPLAY-CNTRCT-PAYEE := #W-HOLD-CONTRACT-PAYEE
                if (condition(miscellaneous_Fields_Pnd_W_Hold_Contract_Prefix.equals("0") || miscellaneous_Fields_Pnd_W_Hold_Contract_Prefix.equals("6")))                //Natural: IF #W-HOLD-CONTRACT-PREFIX = '0' OR = '6'
                {
                    batch_Rec_Pnd_Periodic_Unit.setValue(miscellaneous_Fields_Pnd_W_Periodic_Unit);                                                                       //Natural: MOVE #W-PERIODIC-UNIT TO BATCH-REC.#PERIODIC-UNIT
                    getReports().write(1, ReportOption.NOTITLE,miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(8),batch_Rec_Pnd_Batch_Number,                    //Natural: WRITE ( 1 ) #W-HOLD-USER-AREA 8T BATCH-REC.#BATCH-NUMBER ( EM = ZZZ9 ) 13T BATCH-REC.#CHECK-DATE ( EM = 99/99/99 ) 24T BATCH-REC.#CARD-COUNT ( EM = Z9 ) 29T BATCH-REC.#TRANSACTION-COUNT ( EM = Z9 ) 32T #W-HOLD-TRANSACTION-NUM ( EM = 999 ) 36T #W-HOLD-MODE 40T #W-DISPLAY-CNTRCT 52T #W-HOLD-PRODUCT 67T #W-PERIODIC-DIV ( EM = Z,ZZZ,ZZZ.99 ) 80T #W-PERIODIC-UNIT ( EM = Z,ZZZ,ZZZ.999 ) 94T #W-FINAL-PYMT ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                        new ReportEditMask ("ZZZ9"),new TabSetting(13),batch_Rec_Pnd_Check_Date, new ReportEditMask ("99/99/99"),new TabSetting(24),batch_Rec_Pnd_Card_Count, 
                        new ReportEditMask ("Z9"),new TabSetting(29),batch_Rec_Pnd_Transaction_Count, new ReportEditMask ("Z9"),new TabSetting(32),miscellaneous_Fields_Pnd_W_Hold_Transaction_Num, 
                        new ReportEditMask ("999"),new TabSetting(36),miscellaneous_Fields_Pnd_W_Hold_Mode,new TabSetting(40),miscellaneous_Fields_Pnd_W_Display_Cntrct,new 
                        TabSetting(52),miscellaneous_Fields_Pnd_W_Hold_Product,new TabSetting(67),miscellaneous_Fields_Pnd_W_Periodic_Div, new ReportEditMask 
                        ("Z,ZZZ,ZZZ.99"),new TabSetting(80),miscellaneous_Fields_Pnd_W_Periodic_Unit, new ReportEditMask ("Z,ZZZ,ZZZ.999"),new TabSetting(94),miscellaneous_Fields_Pnd_W_Final_Pymt, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    batch_Rec_Pnd_Periodic_Pymt.setValue(miscellaneous_Fields_Pnd_W_Periodic_Pymt);                                                                       //Natural: MOVE #W-PERIODIC-PYMT TO BATCH-REC.#PERIODIC-PYMT
                    getReports().write(1, ReportOption.NOTITLE,miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(8),batch_Rec_Pnd_Batch_Number,                    //Natural: WRITE ( 1 ) #W-HOLD-USER-AREA 8T BATCH-REC.#BATCH-NUMBER ( EM = ZZZ9 ) 13T BATCH-REC.#CHECK-DATE ( EM = 99/99/99 ) 24T BATCH-REC.#CARD-COUNT ( EM = Z9 ) 29T BATCH-REC.#TRANSACTION-COUNT ( EM = Z9 ) 32T #W-HOLD-TRANSACTION-NUM ( EM = 999 ) 36T #W-HOLD-MODE 40T #W-DISPLAY-CNTRCT 52T #W-HOLD-PRODUCT 54T #W-PERIODIC-PYMT ( EM = Z,ZZZ,ZZZ.99 ) 67T #W-PERIODIC-DIV ( EM = ZZ,ZZZ,ZZZ.99 ) 90T #W-FINAL-PYMT ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
                        new ReportEditMask ("ZZZ9"),new TabSetting(13),batch_Rec_Pnd_Check_Date, new ReportEditMask ("99/99/99"),new TabSetting(24),batch_Rec_Pnd_Card_Count, 
                        new ReportEditMask ("Z9"),new TabSetting(29),batch_Rec_Pnd_Transaction_Count, new ReportEditMask ("Z9"),new TabSetting(32),miscellaneous_Fields_Pnd_W_Hold_Transaction_Num, 
                        new ReportEditMask ("999"),new TabSetting(36),miscellaneous_Fields_Pnd_W_Hold_Mode,new TabSetting(40),miscellaneous_Fields_Pnd_W_Display_Cntrct,new 
                        TabSetting(52),miscellaneous_Fields_Pnd_W_Hold_Product,new TabSetting(54),miscellaneous_Fields_Pnd_W_Periodic_Pymt, new ReportEditMask 
                        ("Z,ZZZ,ZZZ.99"),new TabSetting(67),miscellaneous_Fields_Pnd_W_Periodic_Div, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new TabSetting(90),miscellaneous_Fields_Pnd_W_Final_Pymt, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(2, false, batch_Rec);                                                                                                                //Natural: WRITE WORK FILE 2 BATCH-REC
                miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                       //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TOT-NBR-BATCHES
                if (condition(miscellaneous_Fields_Pnd_Next_Batch_Number.equals(9999)))                                                                                   //Natural: IF #NEXT-BATCH-NUMBER = 9999
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.setValue(100);                                                                                             //Natural: MOVE 0100 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                   //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: END-IF
                miscellaneous_Fields_Pnd_W_Card_Count.reset();                                                                                                            //Natural: RESET #W-CARD-COUNT #W-TRANS-COUNT #W-PERIODIC-PYMT #W-PERIODIC-UNIT #W-PERIODIC-DIV #W-FINAL-PYMT #W-HOLD-MODE #W-HOLD-PRODUCT
                miscellaneous_Fields_Pnd_W_Trans_Count.reset();
                miscellaneous_Fields_Pnd_W_Periodic_Pymt.reset();
                miscellaneous_Fields_Pnd_W_Periodic_Unit.reset();
                miscellaneous_Fields_Pnd_W_Periodic_Div.reset();
                miscellaneous_Fields_Pnd_W_Final_Pymt.reset();
                miscellaneous_Fields_Pnd_W_Hold_Mode.reset();
                miscellaneous_Fields_Pnd_W_Hold_Product.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        if (condition(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(20) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(37) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(40)  //Natural: IF ( #W-HOLD-TRANSACTION-NUM = 20 OR = 37 OR = 40 OR = 64 OR = 66 OR = 70 OR = 102 OR = 104 OR = 106 OR = 304 )
            || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(64) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(66) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(70) 
            || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(102) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(104) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(106) 
            || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(304)))
        {
            if (condition((input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num)) || (input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area))  //Natural: IF ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM ) OR ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( #W-CARD-COUNT = 25 )
                || (miscellaneous_Fields_Pnd_W_Card_Count.equals(25))))
            {
                batch_Rec_Pnd_Filler46.reset();                                                                                                                           //Natural: RESET #FILLER46
                batch_Rec_Pnd_Batch_Number.setValue(miscellaneous_Fields_Pnd_Next_Batch_Number);                                                                          //Natural: MOVE #NEXT-BATCH-NUMBER TO BATCH-REC.#BATCH-NUMBER
                batch_Rec_Pnd_Check_Date.setValue(miscellaneous_Fields_Pnd_W_Hold_Check_Date);                                                                            //Natural: MOVE #W-HOLD-CHECK-DATE TO BATCH-REC.#CHECK-DATE
                batch_Rec_Pnd_Card_Count.setValue(miscellaneous_Fields_Pnd_W_Card_Count);                                                                                 //Natural: MOVE #W-CARD-COUNT TO BATCH-REC.#CARD-COUNT
                batch_Rec_Pnd_Transaction_Count.setValue(miscellaneous_Fields_Pnd_W_Trans_Count);                                                                         //Natural: MOVE #W-TRANS-COUNT TO BATCH-REC.#TRANSACTION-COUNT
                getReports().write(1, ReportOption.NOTITLE,miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(8),batch_Rec_Pnd_Batch_Number, new                    //Natural: WRITE ( 1 ) #W-HOLD-USER-AREA 8T BATCH-REC.#BATCH-NUMBER ( EM = ZZZ9 ) 13T BATCH-REC.#CHECK-DATE ( EM = 99/99/99 ) 24T BATCH-REC.#CARD-COUNT ( EM = Z9 ) 29T BATCH-REC.#TRANSACTION-COUNT ( EM = Z9 ) 32T #W-HOLD-TRANSACTION-NUM ( EM = 999 )
                    ReportEditMask ("ZZZ9"),new TabSetting(13),batch_Rec_Pnd_Check_Date, new ReportEditMask ("99/99/99"),new TabSetting(24),batch_Rec_Pnd_Card_Count, 
                    new ReportEditMask ("Z9"),new TabSetting(29),batch_Rec_Pnd_Transaction_Count, new ReportEditMask ("Z9"),new TabSetting(32),miscellaneous_Fields_Pnd_W_Hold_Transaction_Num, 
                    new ReportEditMask ("999"));
                if (Global.isEscape()) return;
                getWorkFiles().write(2, false, batch_Rec);                                                                                                                //Natural: WRITE WORK FILE 2 BATCH-REC
                miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                       //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TOT-NBR-BATCHES
                if (condition(miscellaneous_Fields_Pnd_Next_Batch_Number.equals(9999)))                                                                                   //Natural: IF #NEXT-BATCH-NUMBER = 9999
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.setValue(100);                                                                                             //Natural: MOVE 0100 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                   //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: END-IF
                miscellaneous_Fields_Pnd_W_Card_Count.reset();                                                                                                            //Natural: RESET #W-CARD-COUNT #W-TRANS-COUNT
                miscellaneous_Fields_Pnd_W_Trans_Count.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(724))))                                                                                     //Natural: IF ( #W-HOLD-TRANSACTION-NUM = 724 )
        {
            if (condition((input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num)) || (input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area))  //Natural: IF ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM ) OR ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( #W-CARD-COUNT = 25 )
                || (miscellaneous_Fields_Pnd_W_Card_Count.equals(25))))
            {
                batch_Rec_Pnd_Filler46.reset();                                                                                                                           //Natural: RESET #FILLER46
                batch_Rec_Pnd_Batch_Number.setValue(miscellaneous_Fields_Pnd_Next_Batch_Number);                                                                          //Natural: MOVE #NEXT-BATCH-NUMBER TO BATCH-REC.#BATCH-NUMBER
                batch_Rec_Pnd_Check_Date.setValue(miscellaneous_Fields_Pnd_W_Hold_Check_Date);                                                                            //Natural: MOVE #W-HOLD-CHECK-DATE TO BATCH-REC.#CHECK-DATE
                batch_Rec_Pnd_Card_Count.setValue(miscellaneous_Fields_Pnd_W_Card_Count);                                                                                 //Natural: MOVE #W-CARD-COUNT TO BATCH-REC.#CARD-COUNT
                batch_Rec_Pnd_Transaction_Count.setValue(miscellaneous_Fields_Pnd_W_Trans_Count);                                                                         //Natural: MOVE #W-TRANS-COUNT TO BATCH-REC.#TRANSACTION-COUNT
                batch_Rec_Pnd_Periodic_Tax_With.setValue(miscellaneous_Fields_Pnd_W_Periodic_Tax_With);                                                                   //Natural: MOVE #W-PERIODIC-TAX-WITH TO BATCH-REC.#PERIODIC-TAX-WITH
                batch_Rec_Pnd_Ytd_Tax_With.setValue(miscellaneous_Fields_Pnd_W_Ytd_Tax_With);                                                                             //Natural: MOVE #W-YTD-TAX-WITH TO BATCH-REC.#YTD-TAX-WITH
                batch_Rec_Pnd_Invest_In_Cntrct.setValue(miscellaneous_Fields_Pnd_W_Invest_In_Cntrct);                                                                     //Natural: MOVE #W-INVEST-IN-CNTRCT TO BATCH-REC.#INVEST-IN-CNTRCT
                getReports().write(1, ReportOption.NOTITLE,miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(8),batch_Rec_Pnd_Batch_Number, new                    //Natural: WRITE ( 1 ) #W-HOLD-USER-AREA 8T BATCH-REC.#BATCH-NUMBER ( EM = ZZZ9 ) 13T BATCH-REC.#CHECK-DATE ( EM = 99/99/99 ) 24T BATCH-REC.#CARD-COUNT ( EM = Z9 ) 29T BATCH-REC.#TRANSACTION-COUNT ( EM = ZZZ9 ) 32T #W-HOLD-TRANSACTION-NUM ( EM = 999 ) 107T #W-INVEST-IN-CNTRCT ( EM = Z,ZZZ,ZZZ.99 )
                    ReportEditMask ("ZZZ9"),new TabSetting(13),batch_Rec_Pnd_Check_Date, new ReportEditMask ("99/99/99"),new TabSetting(24),batch_Rec_Pnd_Card_Count, 
                    new ReportEditMask ("Z9"),new TabSetting(29),batch_Rec_Pnd_Transaction_Count, new ReportEditMask ("Z9"),new TabSetting(32),miscellaneous_Fields_Pnd_W_Hold_Transaction_Num, 
                    new ReportEditMask ("999"),new TabSetting(107),miscellaneous_Fields_Pnd_W_Invest_In_Cntrct, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
                if (Global.isEscape()) return;
                getWorkFiles().write(2, false, batch_Rec);                                                                                                                //Natural: WRITE WORK FILE 2 BATCH-REC
                miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                       //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TOT-NBR-BATCHES
                if (condition(miscellaneous_Fields_Pnd_Next_Batch_Number.equals(9999)))                                                                                   //Natural: IF #NEXT-BATCH-NUMBER = 9999
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.setValue(100);                                                                                             //Natural: MOVE 0100 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                   //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: END-IF
                miscellaneous_Fields_Pnd_W_Card_Count.reset();                                                                                                            //Natural: RESET #W-CARD-COUNT #W-TRANS-COUNT #W-PERIODIC-TAX-WITH #W-YTD-TAX-WITH #W-INVEST-IN-CNTRCT
                miscellaneous_Fields_Pnd_W_Trans_Count.reset();
                miscellaneous_Fields_Pnd_W_Periodic_Tax_With.reset();
                miscellaneous_Fields_Pnd_W_Ytd_Tax_With.reset();
                miscellaneous_Fields_Pnd_W_Invest_In_Cntrct.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        if (condition(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(902) || miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(906)))                        //Natural: IF ( #W-HOLD-TRANSACTION-NUM = 902 OR = 906 )
        {
            if (condition((input_Rec_Pnd_Transaction_Num.notEquals(miscellaneous_Fields_Pnd_W_Hold_Transaction_Num)) || (input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area))  //Natural: IF ( INPUT-REC.#TRANSACTION-NUM NOT = #W-HOLD-TRANSACTION-NUM ) OR ( INPUT-REC.#USER-AREA NOT = #W-HOLD-USER-AREA ) OR ( #W-CARD-COUNT = 25 )
                || (miscellaneous_Fields_Pnd_W_Card_Count.equals(25))))
            {
                batch_Rec_Pnd_Filler46.reset();                                                                                                                           //Natural: RESET #FILLER46
                batch_Rec_Pnd_Batch_Number.setValue(miscellaneous_Fields_Pnd_Next_Batch_Number);                                                                          //Natural: MOVE #NEXT-BATCH-NUMBER TO BATCH-REC.#BATCH-NUMBER
                batch_Rec_Pnd_Check_Date.setValue(miscellaneous_Fields_Pnd_W_Hold_Check_Date);                                                                            //Natural: MOVE #W-HOLD-CHECK-DATE TO BATCH-REC.#CHECK-DATE
                batch_Rec_Pnd_Card_Count.setValue(miscellaneous_Fields_Pnd_W_Card_Count);                                                                                 //Natural: MOVE #W-CARD-COUNT TO BATCH-REC.#CARD-COUNT
                batch_Rec_Pnd_Transaction_Count.setValue(miscellaneous_Fields_Pnd_W_Trans_Count);                                                                         //Natural: MOVE #W-TRANS-COUNT TO BATCH-REC.#TRANSACTION-COUNT
                batch_Rec_Pnd_Periodic_Ded_Amt.setValue(miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt);                                                                     //Natural: MOVE #W-PERIODIC-DED-AMT TO BATCH-REC.#PERIODIC-DED-AMT
                getReports().write(1, ReportOption.NOTITLE,miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(8),batch_Rec_Pnd_Batch_Number, new                    //Natural: WRITE ( 1 ) #W-HOLD-USER-AREA 8T BATCH-REC.#BATCH-NUMBER ( EM = ZZZ9 ) 13T BATCH-REC.#CHECK-DATE ( EM = 99/99/99 ) 24T BATCH-REC.#CARD-COUNT ( EM = Z9 ) 29T BATCH-REC.#TRANSACTION-COUNT ( EM = Z9 ) 32T #W-HOLD-TRANSACTION-NUM ( EM = 999 ) 121T #W-PERIODIC-DED-AMT ( EM = Z,ZZZ,ZZZ.99 )
                    ReportEditMask ("ZZZ9"),new TabSetting(13),batch_Rec_Pnd_Check_Date, new ReportEditMask ("99/99/99"),new TabSetting(24),batch_Rec_Pnd_Card_Count, 
                    new ReportEditMask ("Z9"),new TabSetting(29),batch_Rec_Pnd_Transaction_Count, new ReportEditMask ("Z9"),new TabSetting(32),miscellaneous_Fields_Pnd_W_Hold_Transaction_Num, 
                    new ReportEditMask ("999"),new TabSetting(121),miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
                if (Global.isEscape()) return;
                getWorkFiles().write(2, false, batch_Rec);                                                                                                                //Natural: WRITE WORK FILE 2 BATCH-REC
                miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                       //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-TOT-NBR-BATCHES
                if (condition(miscellaneous_Fields_Pnd_Next_Batch_Number.equals(9999)))                                                                                   //Natural: IF #NEXT-BATCH-NUMBER = 9999
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.setValue(100);                                                                                             //Natural: MOVE 0100 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    miscellaneous_Fields_Pnd_Next_Batch_Number.nadd(1);                                                                                                   //Natural: ADD 1 TO #NEXT-BATCH-NUMBER
                }                                                                                                                                                         //Natural: END-IF
                miscellaneous_Fields_Pnd_W_Card_Count.reset();                                                                                                            //Natural: RESET #W-CARD-COUNT #W-TRANS-COUNT #W-PERIODIC-DED-AMT #W-TOT-DED-AMT
                miscellaneous_Fields_Pnd_W_Trans_Count.reset();
                miscellaneous_Fields_Pnd_W_Periodic_Ded_Amt.reset();
                miscellaneous_Fields_Pnd_W_Tot_Ded_Amt.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  -----------------------------------------------------------------
        if (condition(input_Rec_Pnd_User_Area.notEquals(miscellaneous_Fields_Pnd_W_Hold_User_Area)))                                                                      //Natural: IF #USER-AREA NE #W-HOLD-USER-AREA
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-USER-AREA-TOTALS
            sub_Print_User_Area_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Trans() throws Exception                                                                                                                   //Natural: CALCULATE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet573 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #W-HOLD-TRANSACTION-NUM;//Natural: VALUE 020
        if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(20))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(1).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 1 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 1 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 033
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(33))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 2 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(2).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 2 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 035
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(35))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(3).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 3 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(3).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 3 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 037
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(37))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(4).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 4 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(4).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 4 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 040
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(40))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(5).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 5 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(5).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 5 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 50 : 59
        else if (condition(((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.greaterOrEqual(50) && miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.lessOrEqual(59)))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(6).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 6 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(6).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 6 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 64
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(64))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(7).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 7 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(7).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 7 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 66
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(66))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(8).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 8 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(8).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 8 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 70
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(70))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(9).nadd(1);                                                                                                              //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 9 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(9).nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TRANS ( 9 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 102
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(102))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(10).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 10 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(10).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 10 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 104
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(104))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(11).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 11 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(11).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 11 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 106
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(106))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(12).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 12 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(12).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 12 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 304
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(304))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(13).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 13 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(13).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 13 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 724
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(724))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(14).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 14 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(14).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 14 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 902
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(902))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(15).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 15 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(15).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 15 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: VALUE 906
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Transaction_Num.equals(906))))
        {
            decideConditionsMet573++;
            pnd_W_User_Area_Trans_Count.getValue(16).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 16 )
            pnd_W_User_Area_Trans_Count.getValue(17).nadd(1);                                                                                                             //Natural: ADD 1 TO #W-USER-AREA-TRANS-COUNT ( 17 )
            pnd_Total_Trans.getValue(16).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 16 )
            pnd_Total_Trans.getValue(17).nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-TRANS ( 17 )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_User_Area_Totals() throws Exception                                                                                                            //Natural: PRINT-USER-AREA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Printing_User_Totals.setValue(true);                                                                                                                        //Natural: ASSIGN #W-PRINTING-USER-TOTALS := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"TOTAL");                                                                   //Natural: WRITE ( 1 ) /// 26T 'TOTAL'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TRANS",new TabSetting(26),"TRANS");                                                                 //Natural: WRITE ( 1 ) 1T 'TRANS' 26T 'TRANS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"-----",new TabSetting(26),"-----");                                                                 //Natural: WRITE ( 1 ) 1T '-----' 26T '-----'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 020",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(1), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 020' 24T #W-USER-AREA-TRANS-COUNT ( 1 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 033",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(2), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 033' 24T #W-USER-AREA-TRANS-COUNT ( 2 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 035",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(3), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 035' 24T #W-USER-AREA-TRANS-COUNT ( 3 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 037",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(4), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 037' 24T #W-USER-AREA-TRANS-COUNT ( 4 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 040",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 040' 24T #W-USER-AREA-TRANS-COUNT ( 5 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 050",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(6), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 050' 24T #W-USER-AREA-TRANS-COUNT ( 6 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 064",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(7), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 064' 24T #W-USER-AREA-TRANS-COUNT ( 7 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 066",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(8), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 066' 24T #W-USER-AREA-TRANS-COUNT ( 8 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 070",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(9), new ReportEditMask ("ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) ' 070' 24T #W-USER-AREA-TRANS-COUNT ( 9 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 102",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(10), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 102' 24T #W-USER-AREA-TRANS-COUNT ( 10 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 104",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(11), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 104' 24T #W-USER-AREA-TRANS-COUNT ( 11 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 106",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(12), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 106' 24T #W-USER-AREA-TRANS-COUNT ( 12 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 304",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(13), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 304' 24T #W-USER-AREA-TRANS-COUNT ( 13 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 724",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(14), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 724' 24T #W-USER-AREA-TRANS-COUNT ( 14 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 902",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(15), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 902' 24T #W-USER-AREA-TRANS-COUNT ( 15 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 906",new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(16), new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) ' 906' 24T #W-USER-AREA-TRANS-COUNT ( 16 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total ",miscellaneous_Fields_Pnd_W_Hold_User_Area,new TabSetting(24),pnd_W_User_Area_Trans_Count.getValue(17),        //Natural: WRITE ( 1 ) 'Total ' #W-HOLD-USER-AREA 24T #W-USER-AREA-TRANS-COUNT ( 17 ) ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_W_User_Area_Trans_Count.getValue("*").reset();                                                                                                                //Natural: RESET #W-USER-AREA-TRANS-COUNT ( * )
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL",new TabSetting(8),miscellaneous_Fields_Pnd_W_Tot_Nbr_Batches, new ReportEditMask ("ZZZ9"),new          //Natural: WRITE ( 1 ) / 'TOTAL' 8T #W-TOT-NBR-BATCHES ( EM = ZZZ9 ) 17T #W-TOT-CARD-COUNT ( EM = ZZZZZZ9 ) 26T #W-TOT-TRANS-COUNT ( EM = ZZZZZZ9 ) 54T #W-TOT-PERIODIC-PYMT ( EM = Z,ZZZ,ZZZ.99 ) 67T #W-TOT-PERIODIC-DIV ( EM = Z,ZZZ,ZZZ.99 ) 80T #W-TOT-PERIODIC-UNIT ( EM = Z,ZZZ,ZZZ.999 ) 94T #W-TOT-FINAL-PYMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) 112T #W-TOT-INVEST-IN-CNTRCT ( EM = Z,ZZZ,ZZZ.99 ) 125T #W-TOT-PERIODIC-DED-AMT ( EM = Z,ZZZ,ZZZ.99 )
            TabSetting(17),miscellaneous_Fields_Pnd_W_Tot_Card_Count, new ReportEditMask ("ZZZZZZ9"),new TabSetting(26),miscellaneous_Fields_Pnd_W_Tot_Trans_Count, 
            new ReportEditMask ("ZZZZZZ9"),new TabSetting(54),miscellaneous_Fields_Pnd_W_Tot_Periodic_Pymt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(67),miscellaneous_Fields_Pnd_W_Tot_Periodic_Div, 
            new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(80),miscellaneous_Fields_Pnd_W_Tot_Periodic_Unit, new ReportEditMask ("Z,ZZZ,ZZZ.999"),new 
            TabSetting(94),miscellaneous_Fields_Pnd_W_Tot_Final_Pymt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),new TabSetting(112),miscellaneous_Fields_Pnd_W_Tot_Invest_In_Cntrct, 
            new ReportEditMask ("Z,ZZZ,ZZZ.99"),new TabSetting(125),miscellaneous_Fields_Pnd_W_Tot_Periodic_Ded_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_W_Printing_User_Totals.setValue(true);                                                                                                                        //Natural: ASSIGN #W-PRINTING-USER-TOTALS := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),"GRAND TOTAL ALL AREAS");                                                                           //Natural: WRITE ( 1 ) 52T 'GRAND TOTAL ALL AREAS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(9),"TOTAL");                                                                    //Natural: WRITE ( 1 ) /// 9T 'TOTAL'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TRANS",new TabSetting(9),"TRANS");                                                                  //Natural: WRITE ( 1 ) 1T 'TRANS' 9T 'TRANS'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"-----",new TabSetting(7),"-------");                                                                //Natural: WRITE ( 1 ) 1T '-----' 7T '-------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 020",new TabSetting(24),pnd_Total_Trans.getValue(1), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 020' 24T #TOTAL-TRANS ( 1 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 033",new TabSetting(24),pnd_Total_Trans.getValue(2), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 033' 24T #TOTAL-TRANS ( 2 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 035",new TabSetting(24),pnd_Total_Trans.getValue(3), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 035' 24T #TOTAL-TRANS ( 3 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 037",new TabSetting(24),pnd_Total_Trans.getValue(4), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 037' 24T #TOTAL-TRANS ( 4 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 040",new TabSetting(24),pnd_Total_Trans.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 040' 24T #TOTAL-TRANS ( 5 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 050",new TabSetting(24),pnd_Total_Trans.getValue(6), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 050' 24T #TOTAL-TRANS ( 6 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 064",new TabSetting(24),pnd_Total_Trans.getValue(7), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 064' 24T #TOTAL-TRANS ( 7 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 066",new TabSetting(24),pnd_Total_Trans.getValue(8), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 066' 24T #TOTAL-TRANS ( 8 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 070",new TabSetting(24),pnd_Total_Trans.getValue(9), new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE ( 1 ) ' 070' 24T #TOTAL-TRANS ( 9 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 102",new TabSetting(24),pnd_Total_Trans.getValue(10), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 102' 24T #TOTAL-TRANS ( 10 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 104",new TabSetting(24),pnd_Total_Trans.getValue(11), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 104' 24T #TOTAL-TRANS ( 11 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 106",new TabSetting(24),pnd_Total_Trans.getValue(12), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 106' 24T #TOTAL-TRANS ( 12 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 304",new TabSetting(24),pnd_Total_Trans.getValue(13), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 304' 24T #TOTAL-TRANS ( 13 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 724",new TabSetting(24),pnd_Total_Trans.getValue(14), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 724' 24T #TOTAL-TRANS ( 14 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 902",new TabSetting(24),pnd_Total_Trans.getValue(15), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 902' 24T #TOTAL-TRANS ( 15 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," 906",new TabSetting(24),pnd_Total_Trans.getValue(16), new ReportEditMask ("ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) ' 906' 24T #TOTAL-TRANS ( 16 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(130));                                                                                              //Natural: WRITE ( 1 ) '-' ( 130 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL",new TabSetting(24),pnd_Total_Trans.getValue(17), new ReportEditMask ("ZZZ,ZZ9"));                        //Natural: WRITE ( 1 ) 'GRAND TOTAL' 24T #TOTAL-TRANS ( 17 ) ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
    }
    private void sub_Write_Mode_Report() throws Exception                                                                                                                 //Natural: WRITE-MODE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL BY MODE FOR     US",new TabSetting(35),"MODE",NEWLINE,new TabSetting(35),"100 ",NEWLINE,new             //Natural: WRITE ( 2 ) / 'TOTAL BY MODE FOR     US' 35T 'MODE' / 35T '100 ' / 06T 'TIAA PERIODIC PMT' 30T #W-T-RPT-AMT ( 1,1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA PERIODIC DIV' 30T #W-T-RPT-AMT ( 2,1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA REAL ESTATE ' 30T #W-C-RPT-UNITS ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF STCK UNITS' 30T #W-C-RPT-UNITS ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF MMA  UNITS' 30T #W-C-RPT-UNITS ( 3,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF SCA  UNITS' 30T #W-C-RPT-UNITS ( 4,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GLOB UNITS' 30T #W-C-RPT-UNITS ( 5,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GRTH UNITS' 30T #W-C-RPT-UNITS ( 6,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF EQTY UNITS' 30T #W-C-RPT-UNITS ( 7,1 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 'TOTAL BY MODE FOR     US' 35T 'MODE' 51T 'MODE' 68T 'MODE' / 35T '601 ' 51T '602' 68T '603' / 06T 'TIAA PERIODIC PMT' 30T #W-T-RPT-AMT ( 1,2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 1,3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 1,4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA PERIODIC DIV' 30T #W-T-RPT-AMT ( 2,2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 2,3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 2,4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA REAL ESTATE' 30T #W-C-RPT-UNITS ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF STCK UNITS' 30T #W-C-RPT-UNITS ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF MMA  UNITS' 30T #W-C-RPT-UNITS ( 3,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 3,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 3,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF SCA  UNITS' 30T #W-C-RPT-UNITS ( 4,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 4,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 4,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GLOB UNITS' 30T #W-C-RPT-UNITS ( 5,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 5,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 5,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GRTH UNITS' 30T #W-C-RPT-UNITS ( 6,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 6,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 6,4 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF EQTY UNITS' 30T #W-C-RPT-UNITS ( 7,2 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 7,3 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 7,4 ) ( EM = ZZ,ZZZ,ZZZ.999 )
            TabSetting(6),"TIAA PERIODIC PMT",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(6),"TIAA PERIODIC DIV",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(6),"TIAA REAL ESTATE ",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF STCK UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF MMA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF SCA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF GLOB UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF GRTH UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF EQTY UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,1), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,"TOTAL BY MODE FOR     US",new 
            TabSetting(35),"MODE",new TabSetting(51),"MODE",new TabSetting(68),"MODE",NEWLINE,new TabSetting(35),"601 ",new TabSetting(51),"602",new TabSetting(68),"603",NEWLINE,new 
            TabSetting(6),"TIAA PERIODIC PMT",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,3), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(6),"TIAA PERIODIC DIV",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,3), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(6),"TIAA REAL ESTATE",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,2), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,4), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF STCK UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,2), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,3), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF MMA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,2), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,3), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,4), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF SCA  UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,2), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,4), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF GLOB UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,2), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,3), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GRTH UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,2), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,3), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,4), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF EQTY UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,2), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,4), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL BY MODE FOR     US",new TabSetting(35),"MODE",new TabSetting(51),"MODE",new TabSetting(68),"MODE",new   //Natural: WRITE ( 2 ) / 'TOTAL BY MODE FOR     US' 35T 'MODE' 51T 'MODE' 68T 'MODE' 85T 'MODE' 102T 'MODE' 119T 'MODE' / 35T '701' 51T '702' 68T '703' 85T '704' 102T '705' 119T '706' / 06T 'TIAA PERIODIC PMT' 30T #W-T-RPT-AMT ( 1,5 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 1,6 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 1,7 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 1,8 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 1,9 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 1,10 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA PERIODIC DIV' 30T #W-T-RPT-AMT ( 2,5 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 2,6 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 2,7 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 2,8 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 2,9 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 2,10 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA REAL ESTATE' 30T #W-C-RPT-UNITS ( 1,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 1,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 1,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 1,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 1,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 1,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF STCK UNITS' 30T #W-C-RPT-UNITS ( 2,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 2,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 2,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 2,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 2,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 2,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF MMA  UNITS' 30T #W-C-RPT-UNITS ( 3,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 3,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 3,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 3,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 3,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 3,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF SCA  UNITS' 30T #W-C-RPT-UNITS ( 4,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 4,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 4,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 4,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 4,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 4,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GLOB UNITS' 30T #W-C-RPT-UNITS ( 5,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 5,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 5,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 5,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 5,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 5,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GRTH UNITS' 30T #W-C-RPT-UNITS ( 6,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 6,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 6,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 6,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 6,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 6,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF EQTY UNITS' 30T #W-C-RPT-UNITS ( 7,5 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 7,6 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 7,7 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 7,8 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 7,9 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 7,10 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 'TOTAL BY MODE FOR     US' 35T 'MODE' 51T 'MODE' 68T 'MODE' 85T 'MODE' 102T 'MODE' 119T 'MODE' / 35T '801' 51T '802' 68T '803' 85T '804' 102T '805' 119T '806' / 06T 'TIAA PERIODIC PMT' 30T #W-T-RPT-AMT ( 1,11 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 1,12 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 1,13 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 1,14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 1,15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 1,16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA PERIODIC DIV' 30T #W-T-RPT-AMT ( 2,11 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 2,12 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 2,13 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 2,14 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 2,15 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 2,16 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA REAL ESTATE' 30T #W-C-RPT-UNITS ( 1,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 1,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 1,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 1,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 1,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 1,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF STCK UNITS' 30T #W-C-RPT-UNITS ( 2,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 2,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 2,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 2,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 2,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 2,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF MMA  UNITS' 30T #W-C-RPT-UNITS ( 3,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 3,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 3,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 3,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 3,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 3,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF SCA  UNITS' 30T #W-C-RPT-UNITS ( 4,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 4,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 4,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 4,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 4,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 4,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GLOB UNITS' 30T #W-C-RPT-UNITS ( 5,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 5,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 5,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 5,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 5,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 5,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GRTH UNITS' 30T #W-C-RPT-UNITS ( 6,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 6,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 6,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 6,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 6,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 6,16 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF EQTY UNITS' 30T #W-C-RPT-UNITS ( 7,11 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 7,12 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 7,13 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 7,14 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 7,15 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 7,16 ) ( EM = ZZ,ZZZ,ZZZ.999 )
            TabSetting(85),"MODE",new TabSetting(102),"MODE",new TabSetting(119),"MODE",NEWLINE,new TabSetting(35),"701",new TabSetting(51),"702",new TabSetting(68),"703",new 
            TabSetting(85),"704",new TabSetting(102),"705",new TabSetting(119),"706",NEWLINE,new TabSetting(6),"TIAA PERIODIC PMT",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,6), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,7), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,8), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,9), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,10), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(6),"TIAA PERIODIC DIV",new 
            TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,5), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,6), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,7), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,8), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,10), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(6),"TIAA REAL ESTATE",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,5), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,6), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,7), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,8), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,9), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,10), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF STCK UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,5), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,6), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,7), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,8), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,9), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,10), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF MMA  UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,5), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,6), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,7), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,8), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,9), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,10), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF SCA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,5), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,6), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,7), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,8), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,9), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,10), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GLOB UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,5), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,6), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,7), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,8), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,9), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,10), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GRTH UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,5), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,6), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,7), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,8), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,9), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,10), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF EQTY UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,5), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,6), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,7), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,8), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,9), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,10), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,"TOTAL BY MODE FOR     US",new TabSetting(35),"MODE",new TabSetting(51),"MODE",new TabSetting(68),"MODE",new 
            TabSetting(85),"MODE",new TabSetting(102),"MODE",new TabSetting(119),"MODE",NEWLINE,new TabSetting(35),"801",new TabSetting(51),"802",new TabSetting(68),"803",new 
            TabSetting(85),"804",new TabSetting(102),"805",new TabSetting(119),"806",NEWLINE,new TabSetting(6),"TIAA PERIODIC PMT",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,11), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,12), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,13), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,14), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,15), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,16), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(6),"TIAA PERIODIC DIV",new 
            TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,11), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,12), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,13), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,14), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,15), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,16), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(6),"TIAA REAL ESTATE",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,11), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,12), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,13), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,14), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,15), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,16), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF STCK UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,11), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,12), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,13), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,14), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,15), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,16), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF MMA  UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,11), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,12), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,13), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,14), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,15), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,16), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF SCA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,11), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,12), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,13), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,14), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,15), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,16), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GLOB UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,11), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,12), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,13), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,14), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,15), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,16), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GRTH UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,11), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,12), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,13), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,14), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,15), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,16), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF EQTY UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,11), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,12), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,13), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,14), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,15), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,16), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TOTAL BY MODE FOR     US",new TabSetting(35),"MODE",new TabSetting(51),"MODE",new TabSetting(68),"MODE",new   //Natural: WRITE ( 2 ) / 'TOTAL BY MODE FOR     US' 35T 'MODE' 51T 'MODE' 68T 'MODE' 85T 'MODE' 102T 'MODE' 119T 'MODE' / 35T '807' 51T '808' 68T '809' 85T '810' 102T '811' 119T '812' / 06T 'TIAA PERIODIC PMT' 30T #W-T-RPT-AMT ( 1,17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 1,18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 1,19 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 1,20 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 1,21 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 1,22 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA PERIODIC DIV' 30T #W-T-RPT-AMT ( 2,17 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 47T #W-T-RPT-AMT ( 2,18 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 64T #W-T-RPT-AMT ( 2,19 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 81T #W-T-RPT-AMT ( 2,20 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 98T #W-T-RPT-AMT ( 2,21 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 115T #W-T-RPT-AMT ( 2,22 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) / 06T 'TIAA REAL ESTATE' 30T #W-C-RPT-UNITS ( 1,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 1,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 1,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 1,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 1,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 1,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF STCK UNITS' 30T #W-C-RPT-UNITS ( 2,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 2,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 2,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 2,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 2,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 2,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF MMA  UNITS' 30T #W-C-RPT-UNITS ( 3,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 3,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 3,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 3,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 3,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 3,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF SCA  UNITS' 30T #W-C-RPT-UNITS ( 4,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 4,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 4,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 4,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 4,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 4,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GLOB UNITS' 30T #W-C-RPT-UNITS ( 5,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 5,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 5,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 5,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 5,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 5,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF GRTH UNITS' 30T #W-C-RPT-UNITS ( 6,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 6,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 6,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 6,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 6,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 6,22 ) ( EM = ZZ,ZZZ,ZZZ.999 ) / 06T 'CREF EQTY UNITS' 30T #W-C-RPT-UNITS ( 7,17 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 47T #W-C-RPT-UNITS ( 7,18 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 64T #W-C-RPT-UNITS ( 7,19 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 81T #W-C-RPT-UNITS ( 7,20 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 98T #W-C-RPT-UNITS ( 7,21 ) ( EM = ZZ,ZZZ,ZZZ.999 ) 115T #W-C-RPT-UNITS ( 7,22 ) ( EM = ZZ,ZZZ,ZZZ.999 )
            TabSetting(85),"MODE",new TabSetting(102),"MODE",new TabSetting(119),"MODE",NEWLINE,new TabSetting(35),"807",new TabSetting(51),"808",new TabSetting(68),"809",new 
            TabSetting(85),"810",new TabSetting(102),"811",new TabSetting(119),"812",NEWLINE,new TabSetting(6),"TIAA PERIODIC PMT",new TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,17), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,18), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,19), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,20), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,21), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(1,22), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(6),"TIAA PERIODIC DIV",new 
            TabSetting(30),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,17), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(47),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,18), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(64),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,19), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,20), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(98),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,21), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new TabSetting(115),mode_Report_Array_Pnd_W_T_Rpt_Amt.getValue(2,22), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(6),"TIAA REAL ESTATE",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,17), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,18), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,19), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,20), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,21), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(1,22), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF STCK UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,17), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,18), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,19), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,20), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,21), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(2,22), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF MMA  UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,17), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,18), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,19), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,20), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,21), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(3,22), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF SCA  UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,17), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,18), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,19), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,20), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,21), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(4,22), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GLOB UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,17), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,18), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,19), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,20), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,21), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(5,22), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new TabSetting(6),"CREF GRTH UNITS",new 
            TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,17), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,18), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,19), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,20), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,21), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(6,22), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),NEWLINE,new 
            TabSetting(6),"CREF EQTY UNITS",new TabSetting(30),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,17), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(47),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,18), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(64),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,19), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(81),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,20), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
            TabSetting(98),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,21), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new TabSetting(115),mode_Report_Array_Pnd_W_C_Rpt_Units.getValue(7,22), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Prod_Code() throws Exception                                                                                                               //Natural: DETERMINE-PROD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet758 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #W-HOLD-CNTRCT-NBR,1,7 ) GT 'W024999' AND SUBSTR ( #W-HOLD-CNTRCT-NBR,1,7 ) LT 'W090000'
        if (condition(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr.getSubstring(1,7).compareTo("W024999") > 0 && miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr.getSubstring(1,7).compareTo("W090000") 
            < 0))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("G");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'G'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0T' ) OR = MASK ( '0U' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'0L'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'0M'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'0N'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'0T'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'0U'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("C");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'C'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L3' ) OR = MASK ( '6M3' ) OR = MASK ( '6N3' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L3'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M3'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N3'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("S");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'S'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L4' ) OR = MASK ( '6M4' ) OR = MASK ( '6N4' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L4'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M4'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N4'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("W");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'W'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L5' ) OR = MASK ( '6M5' ) OR = MASK ( '6N5' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L5'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M5'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N5'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("L");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'L'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L6' ) OR = MASK ( '6M6' ) OR = MASK ( '6N6' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L6'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M6'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N6'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("E");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'E'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L7'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M7'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N7'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("R");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'R'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6L'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6M'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'6N'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("M");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'M'
        }                                                                                                                                                                 //Natural: WHEN #W-HOLD-CNTRCT-NBR = MASK ( 'GA' ) OR = MASK ( 'GW' ) OR = MASK ( 'IA' ) OR = MASK ( 'IB' ) OR = MASK ( 'IC' ) OR = MASK ( 'ID' ) OR = MASK ( 'IE' ) OR = MASK ( 'IF' ) OR = MASK ( 'S0' ) OR = MASK ( 'W0' )
        else if (condition(DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'GA'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'GW'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'IA'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'IB'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'IC'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'ID'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'IE'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'IF'") 
            || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,"'S0'") || DbsUtil.maskMatches(miscellaneous_Fields_Pnd_W_Hold_Cntrct_Nbr,
            "'W0'")))
        {
            decideConditionsMet758++;
            miscellaneous_Fields_Pnd_W_Hold_Product.setValue("T");                                                                                                        //Natural: ASSIGN #W-HOLD-PRODUCT := 'T'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Prod_Indx() throws Exception                                                                                                               //Natural: DETERMINE-PROD-INDX
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet785 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #W-HOLD-PRODUCT;//Natural: VALUE 'R'
        if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("R"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(1);                                                                                                                //Natural: ASSIGN #W-C-INDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("C"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(2);                                                                                                                //Natural: ASSIGN #W-C-INDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("M"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(3);                                                                                                                //Natural: ASSIGN #W-C-INDX := 3
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("S"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(4);                                                                                                                //Natural: ASSIGN #W-C-INDX := 4
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("W"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(5);                                                                                                                //Natural: ASSIGN #W-C-INDX := 5
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("L"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(6);                                                                                                                //Natural: ASSIGN #W-C-INDX := 6
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((miscellaneous_Fields_Pnd_W_Hold_Product.equals("E"))))
        {
            decideConditionsMet785++;
            miscellaneous_Fields_Pnd_W_C_Indx.setValue(7);                                                                                                                //Natural: ASSIGN #W-C-INDX := 7
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    miscellaneous_Fields_Pnd_W_Page_Ctr.nadd(1);                                                                                                          //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(17),"IA ADMINISTRATION TRANSACTION CONVERSION BATCH CONTROL LISTING",new  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 17X 'IA ADMINISTRATION TRANSACTION CONVERSION BATCH CONTROL LISTING' 10X 'PAGE: ' #W-PAGE-CTR ( EM = ZZZ,ZZZ,ZZ9 )
                        ColumnSpacing(10),"PAGE: ",miscellaneous_Fields_Pnd_W_Page_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"   DATE",miscellaneous_Fields_Pnd_W_Date_Out);                                                            //Natural: WRITE ( 1 ) '   DATE' #W-DATE-OUT
                    if (condition(! (pnd_W_Printing_User_Totals.getBoolean())))                                                                                           //Natural: IF NOT #W-PRINTING-USER-TOTALS
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"USER",new TabSetting(8),"BTCH",new TabSetting(15),"CHECK",new TabSetting(22),"CARD TRNS",new  //Natural: WRITE ( 1 ) 2T 'USER' 8T 'BTCH' 15T 'CHECK' 22T 'CARD TRNS' 41T 'CONTRACT' 52T 'P' 120T 'TOT PERIODIC'
                            TabSetting(41),"CONTRACT",new TabSetting(52),"P",new TabSetting(120),"TOT PERIODIC");
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"AREA",new TabSetting(9),"NBR",new TabSetting(15),"DATE",new TabSetting(23),"CNT",new  //Natural: WRITE ( 1 ) 2T 'AREA' 9T 'NBR' 15T 'DATE' 23T 'CNT' 28T 'CNT TRN MDE' 41T 'NBR/PAYEE' 52T 'C PERIODIC PAY' 67T 'PERIODIC DIV' 82T 'CREF UNITS' 96T 'FINAL PAY' 109T 'TOTAL IVC' 120T 'DEDUCTION AMT'
                            TabSetting(28),"CNT TRN MDE",new TabSetting(41),"NBR/PAYEE",new TabSetting(52),"C PERIODIC PAY",new TabSetting(67),"PERIODIC DIV",new 
                            TabSetting(82),"CREF UNITS",new TabSetting(96),"FINAL PAY",new TabSetting(109),"TOTAL IVC",new TabSetting(120),"DEDUCTION AMT");
                        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(6),new TabSetting(8),"-",new RepeatItem(4),new TabSetting(13),"-",new               //Natural: WRITE ( 1 ) '-' ( 6 ) 8T '-' ( 4 ) 13T '-' ( 7 ) 22T '-' ( 4 ) 27T '-' ( 4 ) 32T '-' ( 3 ) 36T '-' ( 3 ) 40T '-' ( 11 ) 52T '-' ( 1 ) 54T '-' ( 12 ) 67T '-' ( 12 ) 80T '-' ( 13 ) 94T '-' ( 12 ) 107T '-' ( 12 ) 120T '-' ( 13 )
                            RepeatItem(7),new TabSetting(22),"-",new RepeatItem(4),new TabSetting(27),"-",new RepeatItem(4),new TabSetting(32),"-",new RepeatItem(3),new 
                            TabSetting(36),"-",new RepeatItem(3),new TabSetting(40),"-",new RepeatItem(11),new TabSetting(52),"-",new RepeatItem(1),new 
                            TabSetting(54),"-",new RepeatItem(12),new TabSetting(67),"-",new RepeatItem(12),new TabSetting(80),"-",new RepeatItem(13),new 
                            TabSetting(94),"-",new RepeatItem(12),new TabSetting(107),"-",new RepeatItem(12),new TabSetting(120),"-",new RepeatItem(13));
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W_Printing_User_Totals.setValue(false);                                                                                                           //Natural: ASSIGN #W-PRINTING-USER-TOTALS := FALSE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    miscellaneous_Fields_Pnd_W_Page_Ctr.nadd(1);                                                                                                          //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(17),"IA ADMINISTRATION TRANSACTION CONVERSION BATCH CONTROL LISTING",new  //Natural: WRITE ( 2 ) NOTITLE 'PROGRAM ' *PROGRAM 17X 'IA ADMINISTRATION TRANSACTION CONVERSION BATCH CONTROL LISTING' 10X 'PAGE: ' #W-PAGE-CTR ( EM = ZZZ,ZZZ,ZZ9 )
                        ColumnSpacing(10),"PAGE: ",miscellaneous_Fields_Pnd_W_Page_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,"   DATE",miscellaneous_Fields_Pnd_W_Date_Out);                                                            //Natural: WRITE ( 2 ) '   DATE' #W-DATE-OUT
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56");
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
