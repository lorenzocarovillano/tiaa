/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:42 PM
**        * FROM NATURAL PROGRAM : Iaap580a
************************************************************
**        * FILE NAME            : Iaap580a.java
**        * CLASS NAME           : Iaap580a
**        * INSTANCE NAME        : Iaap580a
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP580A   READ WORK FILE (CREATED IN IAAP580S)   *
*      DATE     -  02/95      AND WRITE OUT (SURV & BENE) NEW ISSUES *
*    AUTHOR     -  ARI G.     REPORT SORTED BY SSN, PAYEE CODE AND   *
*                             CONTRACT. THIS REPORT DOES NOT CONTAIN *
*                             NRA INDIVIDUALS.                       *
*  04/2017 OS PIN EXPANSION CHANGES MARKED 082017.                   *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap580a extends BLNatBase
{
    // Data Areas
    private LdaIaal580a ldaIaal580a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Dc_Pmt_Audit;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Payee_Cde;

    private DbsGroup iaa_Dc_Pmt_Audit_Paudit_Installments;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField pnd_I1;
    private DbsField part_Key;

    private DbsGroup part_Key__R_Field_1;
    private DbsField part_Key_Pnd_Ppcn;
    private DbsField part_Key_Pnd_Pay_Cd;
    private DbsField line_1_S;

    private DbsGroup line_1_S__R_Field_2;
    private DbsField line_1_S_Pnd_F;
    private DbsField line_1_S_Pnd_Paudit_Instllmnt_Typ;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal580a = new LdaIaal580a();
        registerRecord(ldaIaal580a);
        registerRecord(ldaIaal580a.getVw_iaa_Cntrct());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Dc_Pmt_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Pmt_Audit", "IAA-DC-PMT-AUDIT"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_DC_PMT_AUDIT"));
        iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Payee_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");

        iaa_Dc_Pmt_Audit_Paudit_Installments = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupInGroup("iaa_Dc_Pmt_Audit_Paudit_Installments", "PAUDIT-INSTALLMENTS", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldArrayInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ", "PAUDIT-INSTLLMNT-TYP", 
            FieldType.STRING, 4, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_TYP", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        registerRecord(vw_iaa_Dc_Pmt_Audit);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.NUMERIC, 3);
        part_Key = localVariables.newFieldInRecord("part_Key", "PART-KEY", FieldType.STRING, 12);

        part_Key__R_Field_1 = localVariables.newGroupInRecord("part_Key__R_Field_1", "REDEFINE", part_Key);
        part_Key_Pnd_Ppcn = part_Key__R_Field_1.newFieldInGroup("part_Key_Pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        part_Key_Pnd_Pay_Cd = part_Key__R_Field_1.newFieldInGroup("part_Key_Pnd_Pay_Cd", "#PAY-CD", FieldType.NUMERIC, 2);
        line_1_S = localVariables.newFieldInRecord("line_1_S", "LINE-1-S", FieldType.STRING, 132);

        line_1_S__R_Field_2 = localVariables.newGroupInRecord("line_1_S__R_Field_2", "REDEFINE", line_1_S);
        line_1_S_Pnd_F = line_1_S__R_Field_2.newFieldInGroup("line_1_S_Pnd_F", "#F", FieldType.STRING, 128);
        line_1_S_Pnd_Paudit_Instllmnt_Typ = line_1_S__R_Field_2.newFieldInGroup("line_1_S_Pnd_Paudit_Instllmnt_Typ", "#PAUDIT-INSTLLMNT-TYP", FieldType.STRING, 
            4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Dc_Pmt_Audit.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        ldaIaal580a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap580a() throws Exception
    {
        super("Iaap580a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "***************************",NEWLINE,"  START OF IAAP580A PROGRAM",NEWLINE,"***************************");                                 //Natural: WRITE '***************************' / '  START OF IAAP580A PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal580a.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        ldaIaal580a.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getIaa_Parm_Card_Pnd_Parm_Date_Mm(), "/",                   //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
            ldaIaal580a.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", ldaIaal580a.getIaa_Parm_Card_Pnd_Parm_Date_Yy()));
        boolean endOfDataRw = true;                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD
        boolean firstRw = true;
        RW:
        while (condition(getWorkFiles().read(1, ldaIaal580a.getPnd_Work_Record())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRw();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRw = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #PAUDIT-PYEE-TAX-ID-NBR
            ldaIaal580a.getPnd_Work_Records_Read().nadd(1);                                                                                                               //Natural: ADD 1 TO #WORK-RECORDS-READ
            ldaIaal580a.getPnd_Count_Contracts().nadd(1);                                                                                                                 //Natural: ADD 1 TO #COUNT-CONTRACTS
            short decideConditionsMet205 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #PAUDIT-PAYEE-CODE;//Natural: VALUE 02
            if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Payee_Code().equals(2))))
            {
                decideConditionsMet205++;
                if (condition(ldaIaal580a.getPnd_Flag_02().notEquals("N")))                                                                                               //Natural: IF #FLAG-02 NE 'N'
                {
                    ldaIaal580a.getPnd_Count_02().nadd(1);                                                                                                                //Natural: ADD 1 TO #COUNT-02
                    ldaIaal580a.getPnd_Flag_02().setValue("N");                                                                                                           //Natural: MOVE 'N' TO #FLAG-02
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 03:99
            else if (condition(((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Payee_Code().greaterOrEqual(3) && ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Payee_Code().lessOrEqual(99)))))
            {
                decideConditionsMet205++;
                if (condition(ldaIaal580a.getPnd_Flag_03_99().notEquals("N")))                                                                                            //Natural: IF #FLAG-03-99 NE 'N'
                {
                    ldaIaal580a.getPnd_Count_03_99().nadd(1);                                                                                                             //Natural: ADD 1 TO #COUNT-03-99
                    ldaIaal580a.getPnd_Flag_03_99().setValue("N");                                                                                                        //Natural: MOVE 'N' TO #FLAG-03-99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE1
            sub_Pnd_Setup_Line1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE2
            sub_Pnd_Setup_Line2();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-OTHER-LINES
            sub_Pnd_Setup_Other_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal580a.getPnd_Lines_Left().lessOrEqual(ldaIaal580a.getPnd_Lines_To_Print())))                                                              //Natural: IF #LINES-LEFT NOT > #LINES-TO-PRINT
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            line_1_S.setValue(ldaIaal580a.getPnd_Line_1());                                                                                                               //Natural: ASSIGN LINE-1-S := #LINE-1
            FOR01:                                                                                                                                                        //Natural: FOR #I1 1 TO 99
            for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(99)); pnd_I1.nadd(1))
            {
                if (condition(iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ.getValue(pnd_I1).equals("LS  ")))                                                                     //Natural: IF PAUDIT-INSTLLMNT-TYP ( #I1 ) = 'LS  '
                {
                    line_1_S_Pnd_Paudit_Instllmnt_Typ.setValue(iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ.getValue(pnd_I1));                                                   //Natural: ASSIGN #PAUDIT-INSTLLMNT-TYP := PAUDIT-INSTLLMNT-TYP ( #I1 )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,line_1_S);                                                                                                 //Natural: WRITE ( 1 ) / LINE-1-S
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal580a.getPnd_Line_2().notEquals(" ")))                                                                                                    //Natural: IF #LINE-2 NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal580a.getPnd_Line_2());                                                                                  //Natural: WRITE ( 1 ) #LINE-2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            F5:                                                                                                                                                           //Natural: FOR #I = 1 TO #OTHER-LINES
            for (ldaIaal580a.getPnd_I().setValue(1); condition(ldaIaal580a.getPnd_I().lessOrEqual(ldaIaal580a.getPnd_Other_Lines())); ldaIaal580a.getPnd_I().nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(ldaIaal580a.getPnd_I()));                      //Natural: WRITE ( 1 ) #TABLE-GROUP ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal580a.getPnd_Lines_Left().nsubtract(ldaIaal580a.getPnd_Lines_To_Print());                                                                               //Natural: SUBTRACT #LINES-TO-PRINT FROM #LINES-LEFT
                                                                                                                                                                          //Natural: PERFORM #RESET-FIELDS
            sub_Pnd_Reset_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRw(endOfDataRw);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #PRINT-FINAL-TOTALS
        sub_Pnd_Print_Final_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "WORK FILE READS =====> ",ldaIaal580a.getPnd_Work_Records_Read());                                                                          //Natural: WRITE 'WORK FILE READS =====> ' #WORK-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "***************************",NEWLINE,"  END OF IAAP580A PROGRAM",NEWLINE,"***************************");                                   //Natural: WRITE '***************************' / '  END OF IAAP580A PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE1
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE2
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-OTHER-LINES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CORR-ADDRESS-COUNT-1
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CORR-ADDRESS-COUNT-2
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PAYM-ADDRESS-COUNT
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-FINAL-TOTALS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FIELDS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-FINANCE-INFO
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Setup_Line1() throws Exception                                                                                                                   //Natural: #SETUP-LINE1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Dob_Dte().notEquals(" ")))                                                                                //Natural: IF #PAUDIT-DOB-DTE NE ' '
        {
            ldaIaal580a.getPnd_Date_Ccyymmdd().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Dob_Dte());                                                             //Natural: MOVE #PAUDIT-DOB-DTE TO #DATE-CCYYMMDD
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),             //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-YY INTO #LN1-DATE LEAVING NO
                "/", ldaIaal580a.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", ldaIaal580a.getPnd_Date_Ccyymmdd_Pnd_Date_Yy()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Date().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #LN1-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* * WRITE #PAUDIT-PYEE-TAX-ID-NBR
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-PYEE-TAX-ID-NBR NE ' '
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Work_Record_Pnd_Ssn_1_3(),                //Natural: COMPRESS #SSN-1-3 '-' #SSN-4-5 '-' #SSN-6-9 INTO #LN1-SSN LEAVING NO
                "-", ldaIaal580a.getPnd_Work_Record_Pnd_Ssn_4_5(), "-", ldaIaal580a.getPnd_Work_Record_Pnd_Ssn_6_9()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-SSN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr().notEquals(" ")))                                                                               //Natural: IF #PAUDIT-PPCN-NBR NE ' '
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7(),  //Natural: COMPRESS #PAUDIT-PPCN-NBR-1-7 '-' #PAUDIT-PPCN-NBR-8 INTO #LN1-CONTRACT LEAVING NO
                "-", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Contract().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #LN1-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet327 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-SEX-CODE;//Natural: VALUE '1'
        if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Sex_Code().equals("1"))))
        {
            decideConditionsMet327++;
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue("M");                                                                                                //Natural: MOVE 'M' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Sex_Code().equals("2"))))
        {
            decideConditionsMet327++;
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue("F");                                                                                                //Natural: MOVE 'F' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Payee_Code().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Payee_Code());                                                  //Natural: MOVE #PAUDIT-PAYEE-CODE TO #LN1-PAYEE-CODE
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Opt_Cde().reset();                                                                                                              //Natural: RESET #LN1-OPT-CDE
        ldaIaal580a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT CNTRCT-PPCN-NBR = #PAUDIT-PPCN-NBR
        (
        "FQ",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr(), WcType.WITH) },
        1
        );
        FQ:
        while (condition(ldaIaal580a.getVw_iaa_Cntrct().readNextRow("FQ")))
        {
            ldaIaal580a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Opt_Cde().setValue(ldaIaal580a.getIaa_Cntrct_Pnd_Cntrct_Optn_Cde_A());                                                      //Natural: MOVE #CNTRCT-OPTN-CDE-A TO #LN1-OPT-CDE
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Orgn().setValueEdited(ldaIaal580a.getIaa_Cntrct_Cntrct_Orgn_Cde(),new ReportEditMask("99"));                                //Natural: MOVE EDITED CNTRCT-ORGN-CDE ( EM = 99 ) TO #LN1-ORGN
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        part_Key_Pnd_Pay_Cd.setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Payee_Code());                                                                             //Natural: ASSIGN #PAY-CD := #PAUDIT-PAYEE-CODE
        part_Key_Pnd_Ppcn.setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr());                                                                                 //Natural: ASSIGN #PPCN := #PAUDIT-PPCN-NBR
        vw_iaa_Dc_Pmt_Audit.startDatabaseFind                                                                                                                             //Natural: FIND IAA-DC-PMT-AUDIT WITH PAUDIT-PPCN-PYE-KEY = PART-KEY
        (
        "FIND01",
        new Wc[] { new Wc("PAUDIT_PPCN_PYE_KEY", "=", part_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Dc_Pmt_Audit.readNextRow("FIND01")))
        {
            vw_iaa_Dc_Pmt_Audit.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Operator_Id().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Oper_Id());                                                    //Natural: MOVE #PAUDIT-OPER-ID TO #LN1-OPERATOR-ID
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_First_Name().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ph_Name_First());                                          //Natural: MOVE #PAUDIT-PH-NAME-FIRST TO #LN1-CORR-FIRST-NAME
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ph_Name_Middle());                                        //Natural: MOVE #PAUDIT-PH-NAME-MIDDLE TO #LN1-CORR-MIDDLE-NAME
        ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Ph_Name_Last());                                            //Natural: MOVE #PAUDIT-PH-NAME-LAST TO #LN1-CORR-LAST-NAME
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_First_Name()), new ExamineSearch(","), new ExamineReplace(" "));                         //Natural: EXAMINE #LN1-CORR-FIRST-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_First_Name()), new ExamineSearch("."), new ExamineReplace(" "));                         //Natural: EXAMINE #LN1-CORR-FIRST-NAME FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN1-CORR-MIDDLE-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN1-CORR-MIDDLE-NAME FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name()), new ExamineSearch(","), new ExamineReplace(" "));                          //Natural: EXAMINE #LN1-CORR-LAST-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name()), new ExamineSearch("."), new ExamineReplace(" "));                          //Natural: EXAMINE #LN1-CORR-LAST-NAME FOR '.' REPLACE WITH ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1)))                                                                        //Natural: IF #PAUDIT-NAME-ADDRESS-GRP-NUM = 1
        {
            ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Payment_Name().setValue("SAME AS CORRESPONDENCE");                                                                          //Natural: MOVE 'SAME AS CORRESPONDENCE' TO #LN1-PAYMENT-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2)))                                                                    //Natural: IF #PAUDIT-NAME-ADDRESS-GRP-NUM = 2
            {
                ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Payment_Name().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name().getValue(2));                                  //Natural: MOVE #PAUDIT-NAME ( 2 ) TO #LN1-PAYMENT-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Payment_Name()), new ExamineSearch(","), new ExamineReplace(" "));                            //Natural: EXAMINE #LN1-PAYMENT-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_1_Pnd_Ln1_Payment_Name()), new ExamineSearch("."), new ExamineReplace(" "));                            //Natural: EXAMINE #LN1-PAYMENT-NAME FOR '.' REPLACE WITH ' '
        ldaIaal580a.getPnd_Lines_To_Print().nadd(2);                                                                                                                      //Natural: ADD 2 TO #LINES-TO-PRINT
    }
    private void sub_Pnd_Setup_Line2() throws Exception                                                                                                                   //Natural: #SETUP-LINE2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Operator_Unit().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Oper_User_Grp());                                            //Natural: MOVE #PAUDIT-OPER-USER-GRP TO #LN2-OPERATOR-UNIT
        short decideConditionsMet378 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-NAME-ADDRESS-GRP-NUM;//Natural: VALUE 1
        if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1))))
        {
            decideConditionsMet378++;
            ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(1));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 1 ) TO #LN2-CORR-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580a.getPnd_Lines_To_Print().nadd(1);                                                                                                                  //Natural: ADD 1 TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2))))
        {
            decideConditionsMet378++;
            ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Paym_Address().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(2));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 2 ) TO #LN2-PAYM-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Paym_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-PAYM-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Paym_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-PAYM-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(1));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 1 ) TO #LN2-CORR-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580a.getPnd_Lines_To_Print().nadd(1);                                                                                                                  //Natural: ADD 1 TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Setup_Other_Lines() throws Exception                                                                                                             //Natural: #SETUP-OTHER-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet401 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-NAME-ADDRESS-GRP-NUM;//Natural: VALUE 1
        if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1))))
        {
            decideConditionsMet401++;
                                                                                                                                                                          //Natural: PERFORM #CORR-ADDRESS-COUNT-1
            sub_Pnd_Corr_Address_Count_1();
            if (condition(Global.isEscape())) {return;}
            ldaIaal580a.getPnd_Other_Lines().setValue(ldaIaal580a.getPnd_Corr_Count());                                                                                   //Natural: MOVE #CORR-COUNT TO #OTHER-LINES
            ldaIaal580a.getPnd_Lines_To_Print().nadd(ldaIaal580a.getPnd_Corr_Count());                                                                                    //Natural: ADD #CORR-COUNT TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2))))
        {
            decideConditionsMet401++;
                                                                                                                                                                          //Natural: PERFORM #CORR-ADDRESS-COUNT-2
            sub_Pnd_Corr_Address_Count_2();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PAYM-ADDRESS-COUNT
            sub_Pnd_Paym_Address_Count();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal580a.getPnd_Corr_Count().greater(ldaIaal580a.getPnd_Paym_Count())))                                                                      //Natural: IF #CORR-COUNT > #PAYM-COUNT
            {
                ldaIaal580a.getPnd_Other_Lines().setValue(ldaIaal580a.getPnd_Corr_Count());                                                                               //Natural: MOVE #CORR-COUNT TO #OTHER-LINES
                ldaIaal580a.getPnd_Lines_To_Print().nadd(ldaIaal580a.getPnd_Corr_Count());                                                                                //Natural: ADD #CORR-COUNT TO #LINES-TO-PRINT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal580a.getPnd_Other_Lines().setValue(ldaIaal580a.getPnd_Paym_Count());                                                                               //Natural: MOVE #PAYM-COUNT TO #OTHER-LINES
                ldaIaal580a.getPnd_Lines_To_Print().nadd(ldaIaal580a.getPnd_Paym_Count());                                                                                //Natural: ADD #PAYM-COUNT TO #LINES-TO-PRINT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Corr_Address_Count_1() throws Exception                                                                                                          //Natural: #CORR-ADDRESS-COUNT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet423 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1).notEquals(" ")))
        {
            decideConditionsMet423++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1).notEquals(" ")))
        {
            decideConditionsMet423++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1).notEquals(" ")))
        {
            decideConditionsMet423++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet423 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR '.' REPLACE WITH ' '
        ldaIaal580a.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1), ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 1 ) #PAUDIT-ADDRSS-STATE ( 1 ) INTO #CITY-STATE
        ldaIaal580a.getPnd_Zip_Breakdown().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #CORR-COUNT
        ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(DbsUtil.compress(ldaIaal580a.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-CORR-ADDRESS ( #CORR-COUNT )
            ldaIaal580a.getPnd_Total_Zip()));
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #CORR-COUNT
        for (ldaIaal580a.getPnd_I().setValue(1); condition(ldaIaal580a.getPnd_I().lessOrEqual(ldaIaal580a.getPnd_Corr_Count())); ldaIaal580a.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Corr_Address_Count_2() throws Exception                                                                                                          //Natural: #CORR-ADDRESS-COUNT-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet462 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1).notEquals(" ")))
        {
            decideConditionsMet462++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1).notEquals(" ")))
        {
            decideConditionsMet462++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 1 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1).notEquals(" ")))
        {
            decideConditionsMet462++;
            ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet462 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR '.' REPLACE WITH ' '
        ldaIaal580a.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1), ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 1 ) #PAUDIT-ADDRSS-STATE ( 1 ) INTO #CITY-STATE
        ldaIaal580a.getPnd_Zip_Breakdown().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580a.getPnd_Corr_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #CORR-COUNT
        ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_Corr_Count()).setValue(DbsUtil.compress(ldaIaal580a.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-CORR-ADDRESS ( #CORR-COUNT )
            ldaIaal580a.getPnd_Total_Zip()));
        F2:                                                                                                                                                               //Natural: FOR #I = 1 TO #CORR-COUNT
        for (ldaIaal580a.getPnd_I().setValue(1); condition(ldaIaal580a.getPnd_I().lessOrEqual(ldaIaal580a.getPnd_Corr_Count())); ldaIaal580a.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Paym_Address_Count() throws Exception                                                                                                            //Natural: #PAYM-ADDRESS-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet502 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 2 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(2).notEquals(" ")))
        {
            decideConditionsMet502++;
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 2 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(2).notEquals(" ")))
        {
            decideConditionsMet502++;
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 2 ) NOT = ' '
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(2).notEquals(" ")))
        {
            decideConditionsMet502++;
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet502 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 2 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 2 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 2 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 2 ) FOR '.' REPLACE WITH ' '
        ldaIaal580a.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2), ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 2 ) #PAUDIT-ADDRSS-STATE ( 2 ) INTO #CITY-STATE
        ldaIaal580a.getPnd_Zip_Breakdown().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(2));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 2 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(2));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 2 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580a.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580a.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #PAYM-COUNT
        ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580a.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580a.getPnd_Total_Zip()));
        F3:                                                                                                                                                               //Natural: FOR #I = 1 TO #PAYM-COUNT
        for (ldaIaal580a.getPnd_I().setValue(1); condition(ldaIaal580a.getPnd_I().lessOrEqual(ldaIaal580a.getPnd_Paym_Count())); ldaIaal580a.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-PAYM-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-PAYM-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Type_Ind().equals("P")))                                                                                  //Natural: IF #PAUDIT-TYPE-IND = 'P'
        {
            ldaIaal580a.getPnd_Paym_Count().nadd(2);                                                                                                                      //Natural: ADD 2 TO #PAYM-COUNT
                                                                                                                                                                          //Natural: PERFORM #WRITE-FINANCE-INFO
            sub_Pnd_Write_Finance_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Print_Final_Totals() throws Exception                                                                                                            //Natural: #PRINT-FINAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(getReports().getAstLinesLeft(1).less(9)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 9 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"REPORT TOTALS:",NEWLINE,"Total number of survivors    ",ldaIaal580a.getPnd_Count_02(),                //Natural: WRITE ( 1 ) // 'REPORT TOTALS:' / 'Total number of survivors    ' #COUNT-02 / 'Total number of Beneficiaries' #COUNT-03-99 / 'Total number of contracts    ' #COUNT-CONTRACTS
            NEWLINE,"Total number of Beneficiaries",ldaIaal580a.getPnd_Count_03_99(),NEWLINE,"Total number of contracts    ",ldaIaal580a.getPnd_Count_Contracts());
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Fields() throws Exception                                                                                                                  //Natural: #RESET-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal580a.getPnd_Line_1().reset();                                                                                                                              //Natural: RESET #LINE-1 #LINE-2 #CORR-COUNT #PAYM-COUNT #LINES-TO-PRINT #OTHER-LINES #TABLE-GROUP ( 1:9 )
        ldaIaal580a.getPnd_Line_2().reset();
        ldaIaal580a.getPnd_Corr_Count().reset();
        ldaIaal580a.getPnd_Paym_Count().reset();
        ldaIaal580a.getPnd_Lines_To_Print().reset();
        ldaIaal580a.getPnd_Other_Lines().reset();
        ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(1,":",9).reset();
    }
    private void sub_Pnd_Write_Finance_Info() throws Exception                                                                                                            //Natural: #WRITE-FINANCE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*   WRITE '=' #PAUDIT-TYPE-REQ-IND '=' #NEW-ACCT-ID
        short decideConditionsMet557 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PAUDIT-TYPE-REQ-IND = 1 AND #NEW-ACCT-ID = 'Y'
        if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(1) && ldaIaal580a.getPnd_Work_Record_Pnd_New_Acct_Id().equals("Y")))
        {
            decideConditionsMet557++;
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue("ACCT: NEW ACCOUNT");                          //Natural: MOVE 'ACCT: NEW ACCOUNT' TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-CHK-SAV-IND NOT = ' '
            {
                ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                  //Natural: ADD 1 TO #PAYM-COUNT
                ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress("TYPE",                   //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                    ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 1 AND #NEW-ACCT-ID = 'N'
        else if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(1) && ldaIaal580a.getPnd_Work_Record_Pnd_New_Acct_Id().equals("N")))
        {
            decideConditionsMet557++;
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress("ACCT:", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr())); //Natural: COMPRESS 'ACCT:' #PAUDIT-EFT-ACCT-NBR INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress("TYPE", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind())); //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 2 AND #NEW-ACCT-ID = 'Y'
        else if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(2) && ldaIaal580a.getPnd_Work_Record_Pnd_New_Acct_Id().equals("Y")))
        {
            decideConditionsMet557++;
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue("ACCT: NEW ACCOUNT");                          //Natural: MOVE 'ACCT: NEW ACCOUNT' TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Hold_Variable_1().setValue(DbsUtil.compress("ABA:", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Eft_Transit_Id()));                          //Natural: COMPRESS 'ABA:' #PAUDIT-EFT-TRANSIT-ID INTO #HOLD-VARIABLE-1
            if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-CHK-SAV-IND NOT = ' '
            {
                ldaIaal580a.getPnd_Hold_Variable_2().setValue(DbsUtil.compress("TYPE", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));                         //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #HOLD-VARIABLE-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal580a.getPnd_Hold_Variable_2().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #HOLD-VARIABLE-2
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580a.getPnd_Hold_Variable_1(),  //Natural: COMPRESS #HOLD-VARIABLE-1 #HOLD-VARIABLE-2 INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                ldaIaal580a.getPnd_Hold_Variable_2()));
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 2 AND #NEW-ACCT-ID = 'N'
        else if (condition(ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(2) && ldaIaal580a.getPnd_Work_Record_Pnd_New_Acct_Id().equals("N")))
        {
            decideConditionsMet557++;
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress("ACCT:", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr())); //Natural: COMPRESS 'ACCT:' #PAUDIT-EFT-ACCT-NBR INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580a.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580a.getPnd_Hold_Variable_1().setValue(DbsUtil.compress("ABA:", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Eft_Transit_Id()));                          //Natural: COMPRESS 'ABA:' #PAUDIT-EFT-TRANSIT-ID INTO #HOLD-VARIABLE-1
            ldaIaal580a.getPnd_Hold_Variable_2().setValue(DbsUtil.compress("TYPE", ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));                             //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #HOLD-VARIABLE-2
            ldaIaal580a.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580a.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580a.getPnd_Hold_Variable_1(),  //Natural: COMPRESS #HOLD-VARIABLE-1 #HOLD-VARIABLE-2 INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                ldaIaal580a.getPnd_Hold_Variable_2()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal580a.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal580a.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal580a.getPnd_Page_Ctr().nadd(1);                                                                                                                //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(44),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 44T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR
                        TabSetting(119),"PAGE ",ldaIaal580a.getPnd_Page_Ctr());
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(45),"SURVIVOR AND BENEFICIARY NEW ISSUES REPORT                                      "); //Natural: WRITE ( 1 ) '   DATE ' *DATU 45T 'SURVIVOR AND BENEFICIARY NEW ISSUES REPORT                                      '
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(57)," FOR",new TabSetting(62),ldaIaal580a.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 57T ' FOR' 62T #W-PARM-DATE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Operator",new TabSetting(11),"   NAME/ADDRESS OF BENEFICIARY         ORG",new           //Natural: WRITE ( 1 ) 001T 'Operator' 011T '   NAME/ADDRESS OF BENEFICIARY         ORG' 055T 'DATE OF' 067T 'SOCIAL' 078T 'S' 080T 'OPT' 084T 'CONTRACT' 094T 'PY' 2X '          PAYMENT ADDRESS  ' 4X 'LUMP'
                        TabSetting(55),"DATE OF",new TabSetting(67),"SOCIAL",new TabSetting(78),"S",new TabSetting(80),"OPT",new TabSetting(84),"CONTRACT",new 
                        TabSetting(94),"PY",new ColumnSpacing(2),"          PAYMENT ADDRESS  ",new ColumnSpacing(4),"LUMP");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"ID/Unit ",new TabSetting(11),"     LAST           FIRST     MI       CDE",new           //Natural: WRITE ( 1 ) 001T 'ID/Unit ' 011T '     LAST           FIRST     MI       CDE' 055T ' BIRTH ' 065T 'SECURITY NO.' 078T 'X' 080T 'CDE' 084T ' NUMBER ' 37X' SUM'
                        TabSetting(55)," BIRTH ",new TabSetting(65),"SECURITY NO.",new TabSetting(78),"X",new TabSetting(80),"CDE",new TabSetting(84)," NUMBER ",new 
                        ColumnSpacing(37)," SUM");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(11),"----------------  ----------  --       ---",new           //Natural: WRITE ( 1 ) 001T '--------' 011T '----------------  ----------  --       ---' 055T '--------' 065T '------------' 078T '-' 080T '---' 084T '---------' 094T '--' 098T '-----------------------------------'
                        TabSetting(55),"--------",new TabSetting(65),"------------",new TabSetting(78),"-",new TabSetting(80),"---",new TabSetting(84),"---------",new 
                        TabSetting(94),"--",new TabSetting(98),"-----------------------------------");
                    ldaIaal580a.getPnd_Lines_Left().setValue(49);                                                                                                         //Natural: ASSIGN #LINES-LEFT := 49
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRw() throws Exception {atBreakEventRw(false);}
    private void atBreakEventRw(boolean endOfData) throws Exception
    {
        boolean ldaIaal580a_getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrIsBreak = ldaIaal580a.getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr().isBreak(endOfData);
        if (condition(ldaIaal580a_getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_NbrIsBreak))
        {
            ldaIaal580a.getPnd_Flag_02().setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #FLAG-02
            ldaIaal580a.getPnd_Flag_03_99().setValue("Y");                                                                                                                //Natural: MOVE 'Y' TO #FLAG-03-99
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
