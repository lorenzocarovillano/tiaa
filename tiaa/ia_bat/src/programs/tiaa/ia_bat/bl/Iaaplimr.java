/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:19 PM
**        * FROM NATURAL PROGRAM : Iaaplimr
************************************************************
**        * FILE NAME            : Iaaplimr.java
**        * CLASS NAME           : Iaaplimr
**        * INSTANCE NAME        : Iaaplimr
************************************************************
**********************************************************************
* PROGRAM:  IAAPLIMR                                                 *
* DATE   :  03/28/2013                                               *
* PURPOSE:  CREATE A QUARTERLY EXTRACT OF SPIA CONTRACTS FOR LIMRA
* HISTORY:
*
* 05/19/2014 - RC - ORIGINAL CODE
* 11/19/2014 - JT - ADDED OWNER AND JOINT OWNER DOB TO END OF FILE
*              AND POPULATED #BASE-RATE FROM AIAN051. SYNC'd columns
*              SCAN 11/14.
*
* 08/20/2015 - JT - REPLCED MDMN100 WITH MDMN100A FOR BATCH. SC 08/15
* 04/2017      OS - PIN EXPANSION CHANGES MARKED 082017.
* 08/2018      JT - ADDED 3 NEW FIELDS FOR ALIP DECOMMISSION. SC 082018
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaaplimr extends BLNatBase
{
    // Data Areas
    private LdaIaal011 ldaIaal011;
    private PdaAdspda_M pdaAdspda_M;
    private PdaNeca4000 pdaNeca4000;
    private LdaIaalimra ldaIaalimra;
    private PdaMdma101 pdaMdma101;
    private PdaAiaa510 pdaAiaa510;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_T_6;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Issue_Dte;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rsdncy_At_Issue;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Iss_St;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_First_Annt_Sex_Cde;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Input_Pnd_W1_Cntrct_At_Iss_Re;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler14;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Cmpny_Fnd_Cde;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input__Filler15;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input__Filler16;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input__Filler17;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input__Filler18;
    private DbsField pnd_Input_Pnd_Res_Cde;
    private DbsField pnd_Input__Filler19;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler20;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input__Filler21;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input__Filler22;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Rtb_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Rtb_Pct;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input__Filler23;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input__Filler24;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input__Filler25;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input__Filler26;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input__Filler27;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input__Filler28;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Lst_Trans_Dte;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Actvty_Cde;
    private DbsField trans_Trans_Check_Dte;
    private DbsField trans_Trans_Todays_Dte;
    private DbsField trans_Trans_Effective_Dte;
    private DbsField trans_Trans_User_Area;
    private DbsField trans_Trans_User_Id;
    private DbsField trans_Trans_Verify_Cde;
    private DbsField trans_Trans_Verify_Id;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField pnd_Term_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Tot_Pmt;
    private DbsField pnd_S_Status_Cde;
    private DbsField pnd_S_Cntrct_Rtb_Amt;
    private DbsField pnd_S_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_S_Mode_Ind;
    private DbsField pnd_S_Cpr_Id_Nbr;
    private DbsField pnd_S_Ppcn_Nbr;
    private DbsField pnd_S_Payee_Cde_A;
    private DbsField pnd_S_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_S_First_Annt_Sex_Cde;
    private DbsField pnd_S_Scnd_Annt_Sex_Cde;
    private DbsField pnd_S_First_Ann_Dob;
    private DbsField pnd_S_Scnd_Ann_Dob;
    private DbsField pnd_Dob_A;
    private DbsField pnd_S_Orgn_Cde;
    private DbsField pnd_S_Issue_Dte;

    private DbsGroup pnd_S_Issue_Dte__R_Field_17;
    private DbsField pnd_S_Issue_Dte_Pnd_S_Issue_Dte_A;
    private DbsField pnd_Ws_St_Date;

    private DbsGroup pnd_Ws_St_Date__R_Field_18;
    private DbsField pnd_Ws_St_Date_Pnd_Ws_St_Date_Yy;
    private DbsField pnd_Ws_St_Date_Pnd_Ws_St_Date_Mm;

    private DbsGroup pnd_Ws_St_Date__R_Field_19;
    private DbsField pnd_Ws_St_Date_Pnd_Ws_St_Date_A;
    private DbsField pnd_Ws_Comp_T_Start;
    private DbsField pnd_Ws_Out_Issue_Dte;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_S_Optn;
    private DbsField pnd_Write_It;
    private DbsField pnd_Issue_St;
    private DbsField pnd_Res_St;
    private DbsField pnd_No_Yrs;
    private DbsField pnd_Bypass;
    private DbsField pnd_Cpr_Bypass;
    private DbsField pnd_Dc_Cntrct_Key;

    private DbsGroup pnd_Dc_Cntrct_Key__R_Field_20;
    private DbsField pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr;
    private DbsField pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde;
    private DbsField pnd_Dc_Cntrct_Key_Pnd_Dc_Trans_Dte;
    private DbsField pnd_Dc_Cntrct_Key_Pnd_Dc_Cde;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_In_Aian026;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_21;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_22;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_23;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Out_Aian026;

    private DbsGroup aian026_Pnd_Out;
    private DbsField aian026_Pnd_Rtrn_Cde_Pgm;

    private DbsGroup aian026__R_Field_24;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_25;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Newf1;
    private DbsField aian026_Pnd_Newf2;
    private DbsField pnd_Proc_Dte;

    private DbsGroup pnd_Proc_Dte__R_Field_26;
    private DbsField pnd_Proc_Dte_Pnd_Proc_Dte_N;
    private DbsField pnd_Proc_Dte_Pnd_Filler;

    private DbsGroup pnd_Mdm_Vallues;
    private DbsField pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Mdm_Vallues_Pnd_Ws_St_Prov;
    private DbsField pnd_Mdm_Vallues_Pnd_Ws_Zip_Code;
    private DbsField pnd_Mdm_Vallues_Pnd_Ws_Country;

    private DbsGroup pnd_Sng_Jnt;
    private DbsField pnd_Sng_Jnt_Pnd_Sln;
    private DbsField pnd_Sng_Jnt_Pnd_Sl;
    private DbsField pnd_Sng_Jnt_Pnd_Jln;
    private DbsField pnd_Sng_Jnt_Pnd_Jl;
    private DbsField pnd_Ws_Lives_Type;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_System_Date;
    private DbsField pnd_Day_Number;
    private DbsField pnd_End_Dte;

    private DbsGroup pnd_End_Dte__R_Field_27;
    private DbsField pnd_End_Dte_Pnd_End_Dte_Y;
    private DbsField pnd_End_Dte_Pnd_End_Dte_M;
    private DbsField pnd_End_Dte_Pnd_End_Dte_D;

    private DbsGroup pnd_End_Dte__R_Field_28;
    private DbsField pnd_End_Dte_Pnd_End_Dte_Ym;

    private DbsGroup pnd_End_Dte__R_Field_29;
    private DbsField pnd_End_Dte_Pnd_End_Dte_A;
    private DbsField pnd_Start_Dte;

    private DbsGroup pnd_Start_Dte__R_Field_30;
    private DbsField pnd_Start_Dte_Pnd_Start_Dte_Y;
    private DbsField pnd_Start_Dte_Pnd_Start_Dte_M;

    private DbsGroup pnd_Start_Dte__R_Field_31;
    private DbsField pnd_Start_Dte_Pnd_Start_Dte_Ym;

    private DbsGroup pnd_Start_Dte__R_Field_32;
    private DbsField pnd_Start_Dte_Pnd_Start_Dte_A;
    private DbsField pnd_W_Iss_Dte;

    private DbsGroup pnd_W_Iss_Dte__R_Field_33;
    private DbsField pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy;
    private DbsField pnd_W_Iss_Dte_Pnd_W_Iss_Mm;
    private DbsField pnd_W_Final_Dte;

    private DbsGroup pnd_W_Final_Dte__R_Field_34;
    private DbsField pnd_W_Final_Dte_Pnd_W_Final_Yyyy;
    private DbsField pnd_W_Final_Dte_Pnd_W_Final_Mm;
    private DbsField pnd_W_Issue_Yr;
    private DbsField pnd_W_Issue_Mo;
    private DbsField pnd_Guar_Prd;
    private DbsField pnd_Sl_Iss_Dte;

    private DbsGroup pnd_Sl_Iss_Dte__R_Field_35;
    private DbsField pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr;
    private DbsField pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo;
    private DbsField pnd_Sl_Fin_Dte;

    private DbsGroup pnd_Sl_Fin_Dte__R_Field_36;
    private DbsField pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Yr;
    private DbsField pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo;
    private DbsField pnd_Type_Call;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_W_Dte_A;

    private DbsGroup pnd_W_Dte_A__R_Field_37;
    private DbsField pnd_W_Dte_A_Pnd_W_Dte_Y;
    private DbsField pnd_W_Dte_A_Pnd_W_Dte_M;
    private DbsField pnd_Cnt;
    private DbsField pnd_Mrk;
    private DbsField pnd_Record_Code_10;
    private DbsField pnd_Record_Code_20;
    private DbsField pnd_Aian021_Linkage_Array;
    private DbsField pnd_Ann_Factor;
    private DbsField pnd_Ws_Tpa_Ind;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal011 = new LdaIaal011();
        registerRecord(ldaIaal011);
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        ldaIaalimra = new LdaIaalimra();
        registerRecord(ldaIaalimra);
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaAiaa510 = new PdaAiaa510(localVariables);

        // Local Variables

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Ppcn_Nbr);
        pnd_Input__Filler1 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_T_6 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_T_6", "#T-6", FieldType.STRING, 6);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input__Filler2 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input__Filler3 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Rsdncy_At_Issue = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Rsdncy_At_Issue", "#RSDNCY-AT-ISSUE", FieldType.STRING, 3);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Rsdncy_At_Issue);
        pnd_Input__Filler5 = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 1);
        pnd_Input_Iss_St = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Iss_St", "ISS-ST", FieldType.STRING, 2);
        pnd_Input__Filler6 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler7 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 4);
        pnd_Input_Pnd_First_Annt_Sex_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Annt_Sex_Cde", "#FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input__Filler8 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 1);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler9 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler10 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 4);
        pnd_Input_Pnd_Scnd_Annt_Sex_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Annt_Sex_Cde", "#SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler11 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input__Filler12 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_W1_Cntrct_Type = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_W1_Cntrct_At_Iss_Re = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_At_Iss_Re", "#W1-CNTRCT-AT-ISS-RE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_5.newFieldArrayInGroup("pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte", "#W1-CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_W1_Cntrct_Strt_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Strt_Dte", "#W1-CNTRCT-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Cntrct_Issue_Dte_Dd);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A", "#CNTRCT-ISSUE-DTE-DD-A", FieldType.STRING, 
            2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_8 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input__Filler13 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input__Filler14 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input__Filler14", "_FILLER14", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_11 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cmpny_Fnd_Cde = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Cmpny_Fnd_Cde", "#CMPNY-FND-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_11.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Cmpny_Fnd_Cde);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler15 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input__Filler15", "_FILLER15", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_11.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input__Filler16 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input__Filler16", "_FILLER16", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_14 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input__Filler17 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler17", "_FILLER17", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_14.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler18 = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input__Filler18", "_FILLER18", FieldType.STRING, 1);
        pnd_Input_Pnd_Res_Cde = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Res_Cde", "#RES-CDE", FieldType.STRING, 2);
        pnd_Input__Filler19 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler19", "_FILLER19", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler20 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler20", "_FILLER20", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input__Filler21 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler21", "_FILLER21", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input__Filler22 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler22", "_FILLER22", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Rtb_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Rtb_Amt", "#CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Rtb_Pct = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Rtb_Pct", "#CNTRCT-RTB-PCT", FieldType.PACKED_DECIMAL, 
            7, 4, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input__Filler23 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler23", "_FILLER23", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input__Filler24 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler24", "_FILLER24", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_16 = pnd_Input__R_Field_14.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input__Filler25 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler25", "_FILLER25", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input__Filler26 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler26", "_FILLER26", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input__Filler27 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler27", "_FILLER27", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input__Filler28 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input__Filler28", "_FILLER28", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Lst_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Actvty_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_ACTVTY_CDE");
        trans_Trans_Check_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        trans_Trans_Todays_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_TODAYS_DTE");
        trans_Trans_Effective_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TRANS_EFFECTIVE_DTE");
        trans_Trans_User_Area = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TRANS_USER_AREA");
        trans_Trans_User_Id = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TRANS_USER_ID");
        trans_Trans_Verify_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_CDE");
        trans_Trans_Verify_Id = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_ID");
        registerRecord(vw_trans);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        registerRecord(vw_cntrl);

        pnd_Term_Dte = localVariables.newFieldInRecord("pnd_Term_Dte", "#TERM-DTE", FieldType.STRING, 8);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);
        pnd_Tot_Pmt = localVariables.newFieldInRecord("pnd_Tot_Pmt", "#TOT-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_S_Status_Cde = localVariables.newFieldInRecord("pnd_S_Status_Cde", "#S-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_S_Cntrct_Rtb_Amt = localVariables.newFieldInRecord("pnd_S_Cntrct_Rtb_Amt", "#S-CNTRCT-RTB-AMT", FieldType.STRING, 12);
        pnd_S_Cntrct_Fin_Per_Pay_Dte = localVariables.newFieldInRecord("pnd_S_Cntrct_Fin_Per_Pay_Dte", "#S-CNTRCT-FIN-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        pnd_S_Mode_Ind = localVariables.newFieldInRecord("pnd_S_Mode_Ind", "#S-MODE-IND", FieldType.NUMERIC, 3);
        pnd_S_Cpr_Id_Nbr = localVariables.newFieldInRecord("pnd_S_Cpr_Id_Nbr", "#S-CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_S_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_S_Ppcn_Nbr", "#S-PPCN-NBR", FieldType.STRING, 10);
        pnd_S_Payee_Cde_A = localVariables.newFieldInRecord("pnd_S_Payee_Cde_A", "#S-PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_S_Orig_Da_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_S_Orig_Da_Cntrct_Nbr", "#S-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_S_First_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_S_First_Annt_Sex_Cde", "#S-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_S_Scnd_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_S_Scnd_Annt_Sex_Cde", "#S-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_S_First_Ann_Dob = localVariables.newFieldInRecord("pnd_S_First_Ann_Dob", "#S-FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_S_Scnd_Ann_Dob = localVariables.newFieldInRecord("pnd_S_Scnd_Ann_Dob", "#S-SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Dob_A = localVariables.newFieldInRecord("pnd_Dob_A", "#DOB-A", FieldType.STRING, 8);
        pnd_S_Orgn_Cde = localVariables.newFieldInRecord("pnd_S_Orgn_Cde", "#S-ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_S_Issue_Dte = localVariables.newFieldInRecord("pnd_S_Issue_Dte", "#S-ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_S_Issue_Dte__R_Field_17 = localVariables.newGroupInRecord("pnd_S_Issue_Dte__R_Field_17", "REDEFINE", pnd_S_Issue_Dte);
        pnd_S_Issue_Dte_Pnd_S_Issue_Dte_A = pnd_S_Issue_Dte__R_Field_17.newFieldInGroup("pnd_S_Issue_Dte_Pnd_S_Issue_Dte_A", "#S-ISSUE-DTE-A", FieldType.STRING, 
            6);
        pnd_Ws_St_Date = localVariables.newFieldInRecord("pnd_Ws_St_Date", "#WS-ST-DATE", FieldType.NUMERIC, 6);

        pnd_Ws_St_Date__R_Field_18 = localVariables.newGroupInRecord("pnd_Ws_St_Date__R_Field_18", "REDEFINE", pnd_Ws_St_Date);
        pnd_Ws_St_Date_Pnd_Ws_St_Date_Yy = pnd_Ws_St_Date__R_Field_18.newFieldInGroup("pnd_Ws_St_Date_Pnd_Ws_St_Date_Yy", "#WS-ST-DATE-YY", FieldType.NUMERIC, 
            4);
        pnd_Ws_St_Date_Pnd_Ws_St_Date_Mm = pnd_Ws_St_Date__R_Field_18.newFieldInGroup("pnd_Ws_St_Date_Pnd_Ws_St_Date_Mm", "#WS-ST-DATE-MM", FieldType.NUMERIC, 
            2);

        pnd_Ws_St_Date__R_Field_19 = localVariables.newGroupInRecord("pnd_Ws_St_Date__R_Field_19", "REDEFINE", pnd_Ws_St_Date);
        pnd_Ws_St_Date_Pnd_Ws_St_Date_A = pnd_Ws_St_Date__R_Field_19.newFieldInGroup("pnd_Ws_St_Date_Pnd_Ws_St_Date_A", "#WS-ST-DATE-A", FieldType.STRING, 
            6);
        pnd_Ws_Comp_T_Start = localVariables.newFieldInRecord("pnd_Ws_Comp_T_Start", "#WS-COMP-T-START", FieldType.BOOLEAN, 1);
        pnd_Ws_Out_Issue_Dte = localVariables.newFieldInRecord("pnd_Ws_Out_Issue_Dte", "#WS-OUT-ISSUE-DTE", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_S_Optn = localVariables.newFieldInRecord("pnd_S_Optn", "#S-OPTN", FieldType.NUMERIC, 2);
        pnd_Write_It = localVariables.newFieldInRecord("pnd_Write_It", "#WRITE-IT", FieldType.BOOLEAN, 1);
        pnd_Issue_St = localVariables.newFieldInRecord("pnd_Issue_St", "#ISSUE-ST", FieldType.STRING, 20);
        pnd_Res_St = localVariables.newFieldInRecord("pnd_Res_St", "#RES-ST", FieldType.STRING, 20);
        pnd_No_Yrs = localVariables.newFieldInRecord("pnd_No_Yrs", "#NO-YRS", FieldType.NUMERIC, 2);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Cpr_Bypass = localVariables.newFieldInRecord("pnd_Cpr_Bypass", "#CPR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Dc_Cntrct_Key = localVariables.newFieldInRecord("pnd_Dc_Cntrct_Key", "#DC-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Dc_Cntrct_Key__R_Field_20 = localVariables.newGroupInRecord("pnd_Dc_Cntrct_Key__R_Field_20", "REDEFINE", pnd_Dc_Cntrct_Key);
        pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr = pnd_Dc_Cntrct_Key__R_Field_20.newFieldInGroup("pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr", "#DC-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde = pnd_Dc_Cntrct_Key__R_Field_20.newFieldInGroup("pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde", "#DC-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Dc_Cntrct_Key_Pnd_Dc_Trans_Dte = pnd_Dc_Cntrct_Key__R_Field_20.newFieldInGroup("pnd_Dc_Cntrct_Key_Pnd_Dc_Trans_Dte", "#DC-TRANS-DTE", FieldType.NUMERIC, 
            12);
        pnd_Dc_Cntrct_Key_Pnd_Dc_Cde = pnd_Dc_Cntrct_Key__R_Field_20.newFieldInGroup("pnd_Dc_Cntrct_Key_Pnd_Dc_Cde", "#DC-CDE", FieldType.NUMERIC, 3);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_In_Aian026 = aian026.newGroupInGroup("aian026_Pnd_In_Aian026", "#IN-AIAN026");
        aian026_Pnd_Call_Type = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_21 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_21", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_21.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_22 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_22", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_22.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_22.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_22.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_23 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_23", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_23.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_23.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Out_Aian026 = aian026.newGroupInGroup("aian026_Pnd_Out_Aian026", "#OUT-AIAN026");

        aian026_Pnd_Out = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026_Pnd_Out", "#OUT");
        aian026_Pnd_Rtrn_Cde_Pgm = aian026_Pnd_Out.newFieldInGroup("aian026_Pnd_Rtrn_Cde_Pgm", "#RTRN-CDE-PGM", FieldType.STRING, 11);

        aian026__R_Field_24 = aian026_Pnd_Out.newGroupInGroup("aian026__R_Field_24", "REDEFINE", aian026_Pnd_Rtrn_Cde_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_24.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_24.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_25 = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026__R_Field_25", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_25.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Newf1 = aian026.newFieldInGroup("aian026_Pnd_Newf1", "#NEWF1", FieldType.NUMERIC, 2);
        aian026_Pnd_Newf2 = aian026.newFieldInGroup("aian026_Pnd_Newf2", "#NEWF2", FieldType.NUMERIC, 2);
        pnd_Proc_Dte = localVariables.newFieldInRecord("pnd_Proc_Dte", "#PROC-DTE", FieldType.STRING, 8);

        pnd_Proc_Dte__R_Field_26 = localVariables.newGroupInRecord("pnd_Proc_Dte__R_Field_26", "REDEFINE", pnd_Proc_Dte);
        pnd_Proc_Dte_Pnd_Proc_Dte_N = pnd_Proc_Dte__R_Field_26.newFieldInGroup("pnd_Proc_Dte_Pnd_Proc_Dte_N", "#PROC-DTE-N", FieldType.NUMERIC, 6);
        pnd_Proc_Dte_Pnd_Filler = pnd_Proc_Dte__R_Field_26.newFieldInGroup("pnd_Proc_Dte_Pnd_Filler", "#FILLER", FieldType.STRING, 2);

        pnd_Mdm_Vallues = localVariables.newGroupInRecord("pnd_Mdm_Vallues", "#MDM-VALLUES");
        pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr = pnd_Mdm_Vallues.newFieldInGroup("pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr", "#WS-#TAX-ID-NBR", FieldType.NUMERIC, 
            9);
        pnd_Mdm_Vallues_Pnd_Ws_St_Prov = pnd_Mdm_Vallues.newFieldInGroup("pnd_Mdm_Vallues_Pnd_Ws_St_Prov", "#WS-ST-PROV", FieldType.STRING, 2);
        pnd_Mdm_Vallues_Pnd_Ws_Zip_Code = pnd_Mdm_Vallues.newFieldInGroup("pnd_Mdm_Vallues_Pnd_Ws_Zip_Code", "#WS-ZIP-CODE", FieldType.STRING, 10);
        pnd_Mdm_Vallues_Pnd_Ws_Country = pnd_Mdm_Vallues.newFieldInGroup("pnd_Mdm_Vallues_Pnd_Ws_Country", "#WS-COUNTRY", FieldType.STRING, 35);

        pnd_Sng_Jnt = localVariables.newGroupInRecord("pnd_Sng_Jnt", "#SNG-JNT");
        pnd_Sng_Jnt_Pnd_Sln = pnd_Sng_Jnt.newFieldArrayInGroup("pnd_Sng_Jnt_Pnd_Sln", "#SLN", FieldType.NUMERIC, 2, new DbsArrayController(1, 3));
        pnd_Sng_Jnt_Pnd_Sl = pnd_Sng_Jnt.newFieldArrayInGroup("pnd_Sng_Jnt_Pnd_Sl", "#SL", FieldType.NUMERIC, 2, new DbsArrayController(1, 6));
        pnd_Sng_Jnt_Pnd_Jln = pnd_Sng_Jnt.newFieldArrayInGroup("pnd_Sng_Jnt_Pnd_Jln", "#JLN", FieldType.NUMERIC, 2, new DbsArrayController(1, 5));
        pnd_Sng_Jnt_Pnd_Jl = pnd_Sng_Jnt.newFieldArrayInGroup("pnd_Sng_Jnt_Pnd_Jl", "#JL", FieldType.NUMERIC, 2, new DbsArrayController(1, 15));
        pnd_Ws_Lives_Type = localVariables.newFieldInRecord("pnd_Ws_Lives_Type", "#WS-LIVES-TYPE", FieldType.STRING, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_System_Date = localVariables.newFieldInRecord("pnd_System_Date", "#SYSTEM-DATE", FieldType.DATE);
        pnd_Day_Number = localVariables.newFieldInRecord("pnd_Day_Number", "#DAY-NUMBER", FieldType.STRING, 1);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.NUMERIC, 8);

        pnd_End_Dte__R_Field_27 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_27", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_End_Dte_Y = pnd_End_Dte__R_Field_27.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_Y", "#END-DTE-Y", FieldType.NUMERIC, 4);
        pnd_End_Dte_Pnd_End_Dte_M = pnd_End_Dte__R_Field_27.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_M", "#END-DTE-M", FieldType.NUMERIC, 2);
        pnd_End_Dte_Pnd_End_Dte_D = pnd_End_Dte__R_Field_27.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_D", "#END-DTE-D", FieldType.NUMERIC, 2);

        pnd_End_Dte__R_Field_28 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_28", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_End_Dte_Ym = pnd_End_Dte__R_Field_28.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_Ym", "#END-DTE-YM", FieldType.NUMERIC, 6);

        pnd_End_Dte__R_Field_29 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_29", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_End_Dte_A = pnd_End_Dte__R_Field_29.newFieldInGroup("pnd_End_Dte_Pnd_End_Dte_A", "#END-DTE-A", FieldType.STRING, 8);
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.NUMERIC, 8);

        pnd_Start_Dte__R_Field_30 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_30", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_Start_Dte_Y = pnd_Start_Dte__R_Field_30.newFieldInGroup("pnd_Start_Dte_Pnd_Start_Dte_Y", "#START-DTE-Y", FieldType.NUMERIC, 
            4);
        pnd_Start_Dte_Pnd_Start_Dte_M = pnd_Start_Dte__R_Field_30.newFieldInGroup("pnd_Start_Dte_Pnd_Start_Dte_M", "#START-DTE-M", FieldType.NUMERIC, 
            2);

        pnd_Start_Dte__R_Field_31 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_31", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_Start_Dte_Ym = pnd_Start_Dte__R_Field_31.newFieldInGroup("pnd_Start_Dte_Pnd_Start_Dte_Ym", "#START-DTE-YM", FieldType.NUMERIC, 
            6);

        pnd_Start_Dte__R_Field_32 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_32", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_Start_Dte_A = pnd_Start_Dte__R_Field_32.newFieldInGroup("pnd_Start_Dte_Pnd_Start_Dte_A", "#START-DTE-A", FieldType.STRING, 8);
        pnd_W_Iss_Dte = localVariables.newFieldInRecord("pnd_W_Iss_Dte", "#W-ISS-DTE", FieldType.NUMERIC, 6);

        pnd_W_Iss_Dte__R_Field_33 = localVariables.newGroupInRecord("pnd_W_Iss_Dte__R_Field_33", "REDEFINE", pnd_W_Iss_Dte);
        pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy = pnd_W_Iss_Dte__R_Field_33.newFieldInGroup("pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy", "#W-ISS-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Iss_Dte_Pnd_W_Iss_Mm = pnd_W_Iss_Dte__R_Field_33.newFieldInGroup("pnd_W_Iss_Dte_Pnd_W_Iss_Mm", "#W-ISS-MM", FieldType.NUMERIC, 2);
        pnd_W_Final_Dte = localVariables.newFieldInRecord("pnd_W_Final_Dte", "#W-FINAL-DTE", FieldType.NUMERIC, 6);

        pnd_W_Final_Dte__R_Field_34 = localVariables.newGroupInRecord("pnd_W_Final_Dte__R_Field_34", "REDEFINE", pnd_W_Final_Dte);
        pnd_W_Final_Dte_Pnd_W_Final_Yyyy = pnd_W_Final_Dte__R_Field_34.newFieldInGroup("pnd_W_Final_Dte_Pnd_W_Final_Yyyy", "#W-FINAL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Final_Dte_Pnd_W_Final_Mm = pnd_W_Final_Dte__R_Field_34.newFieldInGroup("pnd_W_Final_Dte_Pnd_W_Final_Mm", "#W-FINAL-MM", FieldType.NUMERIC, 
            2);
        pnd_W_Issue_Yr = localVariables.newFieldInRecord("pnd_W_Issue_Yr", "#W-ISSUE-YR", FieldType.NUMERIC, 4);
        pnd_W_Issue_Mo = localVariables.newFieldInRecord("pnd_W_Issue_Mo", "#W-ISSUE-MO", FieldType.NUMERIC, 2);
        pnd_Guar_Prd = localVariables.newFieldInRecord("pnd_Guar_Prd", "#GUAR-PRD", FieldType.NUMERIC, 5);
        pnd_Sl_Iss_Dte = localVariables.newFieldInRecord("pnd_Sl_Iss_Dte", "#SL-ISS-DTE", FieldType.NUMERIC, 6);

        pnd_Sl_Iss_Dte__R_Field_35 = localVariables.newGroupInRecord("pnd_Sl_Iss_Dte__R_Field_35", "REDEFINE", pnd_Sl_Iss_Dte);
        pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr = pnd_Sl_Iss_Dte__R_Field_35.newFieldInGroup("pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr", "#SL-ISS-YR", FieldType.NUMERIC, 4);
        pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo = pnd_Sl_Iss_Dte__R_Field_35.newFieldInGroup("pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo", "#SL-ISS-MO", FieldType.NUMERIC, 2);
        pnd_Sl_Fin_Dte = localVariables.newFieldInRecord("pnd_Sl_Fin_Dte", "#SL-FIN-DTE", FieldType.NUMERIC, 6);

        pnd_Sl_Fin_Dte__R_Field_36 = localVariables.newGroupInRecord("pnd_Sl_Fin_Dte__R_Field_36", "REDEFINE", pnd_Sl_Fin_Dte);
        pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Yr = pnd_Sl_Fin_Dte__R_Field_36.newFieldInGroup("pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Yr", "#SL-FIN-YR", FieldType.NUMERIC, 4);
        pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo = pnd_Sl_Fin_Dte__R_Field_36.newFieldInGroup("pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo", "#SL-FIN-MO", FieldType.NUMERIC, 2);
        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.STRING, 8);
        pnd_W_Dte_A = localVariables.newFieldInRecord("pnd_W_Dte_A", "#W-DTE-A", FieldType.STRING, 6);

        pnd_W_Dte_A__R_Field_37 = localVariables.newGroupInRecord("pnd_W_Dte_A__R_Field_37", "REDEFINE", pnd_W_Dte_A);
        pnd_W_Dte_A_Pnd_W_Dte_Y = pnd_W_Dte_A__R_Field_37.newFieldInGroup("pnd_W_Dte_A_Pnd_W_Dte_Y", "#W-DTE-Y", FieldType.NUMERIC, 4);
        pnd_W_Dte_A_Pnd_W_Dte_M = pnd_W_Dte_A__R_Field_37.newFieldInGroup("pnd_W_Dte_A_Pnd_W_Dte_M", "#W-DTE-M", FieldType.NUMERIC, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.INTEGER, 2);
        pnd_Mrk = localVariables.newFieldInRecord("pnd_Mrk", "#MRK", FieldType.STRING, 1);
        pnd_Record_Code_10 = localVariables.newFieldInRecord("pnd_Record_Code_10", "#RECORD-CODE-10", FieldType.NUMERIC, 2);
        pnd_Record_Code_20 = localVariables.newFieldInRecord("pnd_Record_Code_20", "#RECORD-CODE-20", FieldType.NUMERIC, 2);
        pnd_Aian021_Linkage_Array = localVariables.newFieldArrayInRecord("pnd_Aian021_Linkage_Array", "#AIAN021-LINKAGE-ARRAY", FieldType.STRING, 69, 
            new DbsArrayController(1, 250));
        pnd_Ann_Factor = localVariables.newFieldArrayInRecord("pnd_Ann_Factor", "#ANN-FACTOR", FieldType.NUMERIC, 8, 5, new DbsArrayController(1, 250));
        pnd_Ws_Tpa_Ind = localVariables.newFieldInRecord("pnd_Ws_Tpa_Ind", "#WS-TPA-IND", FieldType.STRING, 1);
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trans.reset();
        vw_cntrl.reset();

        ldaIaal011.initializeValues();
        ldaIaalimra.initializeValues();

        localVariables.reset();
        pnd_Sng_Jnt_Pnd_Sln.getValue(1).setInitialValue(1);
        pnd_Sng_Jnt_Pnd_Sln.getValue(2).setInitialValue(2);
        pnd_Sng_Jnt_Pnd_Sln.getValue(3).setInitialValue(19);
        pnd_Sng_Jnt_Pnd_Sl.getValue(1).setInitialValue(5);
        pnd_Sng_Jnt_Pnd_Sl.getValue(2).setInitialValue(6);
        pnd_Sng_Jnt_Pnd_Sl.getValue(3).setInitialValue(9);
        pnd_Sng_Jnt_Pnd_Sl.getValue(4).setInitialValue(21);
        pnd_Sng_Jnt_Pnd_Sl.getValue(5).setInitialValue(32);
        pnd_Sng_Jnt_Pnd_Sl.getValue(6).setInitialValue(50);
        pnd_Sng_Jnt_Pnd_Jln.getValue(1).setInitialValue(3);
        pnd_Sng_Jnt_Pnd_Jln.getValue(2).setInitialValue(4);
        pnd_Sng_Jnt_Pnd_Jln.getValue(3).setInitialValue(7);
        pnd_Sng_Jnt_Pnd_Jln.getValue(4).setInitialValue(18);
        pnd_Sng_Jnt_Pnd_Jln.getValue(5).setInitialValue(57);
        pnd_Sng_Jnt_Pnd_Jl.getValue(1).setInitialValue(8);
        pnd_Sng_Jnt_Pnd_Jl.getValue(2).setInitialValue(10);
        pnd_Sng_Jnt_Pnd_Jl.getValue(3).setInitialValue(11);
        pnd_Sng_Jnt_Pnd_Jl.getValue(4).setInitialValue(12);
        pnd_Sng_Jnt_Pnd_Jl.getValue(5).setInitialValue(13);
        pnd_Sng_Jnt_Pnd_Jl.getValue(6).setInitialValue(14);
        pnd_Sng_Jnt_Pnd_Jl.getValue(7).setInitialValue(15);
        pnd_Sng_Jnt_Pnd_Jl.getValue(8).setInitialValue(16);
        pnd_Sng_Jnt_Pnd_Jl.getValue(9).setInitialValue(17);
        pnd_Sng_Jnt_Pnd_Jl.getValue(10).setInitialValue(51);
        pnd_Sng_Jnt_Pnd_Jl.getValue(11).setInitialValue(52);
        pnd_Sng_Jnt_Pnd_Jl.getValue(12).setInitialValue(53);
        pnd_Sng_Jnt_Pnd_Jl.getValue(13).setInitialValue(54);
        pnd_Sng_Jnt_Pnd_Jl.getValue(14).setInitialValue(55);
        pnd_Sng_Jnt_Pnd_Jl.getValue(15).setInitialValue(56);
        pnd_Fund_2.setInitialValue("09");
        pnd_Type_Call.setInitialValue("R");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaaplimr() throws Exception
    {
        super("Iaaplimr");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 0 LS = 250
        //*  08/15 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  WORK FILE WILL BE CREATED REGARDLESS
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER
        sub_Write_Header();
        if (condition(Global.isEscape())) {return;}
        pnd_Dc_Cntrct_Key_Pnd_Dc_Trans_Dte.reset();                                                                                                                       //Natural: RESET #DC-TRANS-DTE #DC-CDE
        pnd_Dc_Cntrct_Key_Pnd_Dc_Cde.reset();
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 2 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        2
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            pnd_Proc_Dte.setValueEdited(cntrl_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #PROC-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cntrl.readNextRow("READ02")))
        {
            pnd_Todays_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DTE
            pnd_W_Dte_A.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMM"));                                                                              //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMM ) TO #W-DTE-A
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date().setValue(cntrl_Cntrl_Todays_Dte);                                                             //Natural: ASSIGN NAZ-ACT-CURR-BSNSS-DATE := CNTRL-TODAYS-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM SET-START-END-DATES
        sub_Set_Start_End_Dates();
        if (condition(Global.isEscape())) {return;}
        //*    #START-DTE := 20180101
        //*    #END-DTE   := 20180824
        aian026_Pnd_Call_Type.setValue("P");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'P'
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        aian026_Pnd_Prtcptn_Dte.compute(new ComputeParameters(false, aian026_Pnd_Prtcptn_Dte), pnd_Proc_Dte.val());                                                       //Natural: ASSIGN #PRTCPTN-DTE := VAL ( #PROC-DTE )
        aian026_Pnd_Uv_Req_Dte.compute(new ComputeParameters(false, aian026_Pnd_Uv_Req_Dte), pnd_Proc_Dte.val());                                                         //Natural: ASSIGN #UV-REQ-DTE := VAL ( #PROC-DTE )
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtrn_Cde);                                                                 //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTRN-CDE
        if (condition(Global.isEscape())) return;
        READWORK03:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                //*      #PPCN-NBR = MASK(N) OR
                //*      #PPCN-NBR = MASK('Y') OR
                //*      #PPCN-NBR = MASK('Z')
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.less("IG000000")))                                                                                                       //Natural: IF #PPCN-NBR LT 'IG000000'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.greater("W0399999")))                                                                                                    //Natural: IF #PPCN-NBR GT 'W0399999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20) || pnd_Input_Pnd_Record_Code.equals(30))))                       //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20 OR = 30
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS #CPR-BYPASS
                pnd_Cpr_Bypass.reset();
                if (condition(pnd_Write_It.getBoolean()))                                                                                                                 //Natural: IF #WRITE-IT
                {
                    //*  WRITE PREV REC 10
                                                                                                                                                                          //Natural: PERFORM DISPLAY-IT
                    sub_Display_It();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                if (condition(pnd_Input_Pnd_Orgn_Cde.equals(40) && pnd_Input_Pnd_Issue_Dte.greaterOrEqual(pnd_Start_Dte_Pnd_Start_Dte_Ym) && pnd_Input_Pnd_Issue_Dte.lessOrEqual(pnd_End_Dte_Pnd_End_Dte_Ym))) //Natural: IF #ORGN-CDE = 40 AND #ISSUE-DTE = #START-DTE-YM THRU #END-DTE-YM
                {
                    pnd_S_Orgn_Cde.setValue(pnd_Input_Pnd_Orgn_Cde);                                                                                                      //Natural: ASSIGN #S-ORGN-CDE := #ORGN-CDE
                    pnd_S_Optn.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                          //Natural: ASSIGN #S-OPTN := #OPTN-CDE
                    pnd_S_Issue_Dte.setValue(pnd_Input_Pnd_Issue_Dte);                                                                                                    //Natural: ASSIGN #S-ISSUE-DTE := #ISSUE-DTE
                    pnd_S_First_Ann_Dob.setValue(pnd_Input_Pnd_First_Ann_Dob);                                                                                            //Natural: ASSIGN #S-FIRST-ANN-DOB := #FIRST-ANN-DOB
                    pnd_S_Scnd_Ann_Dob.setValue(pnd_Input_Pnd_Scnd_Ann_Dob);                                                                                              //Natural: ASSIGN #S-SCND-ANN-DOB := #SCND-ANN-DOB
                    pnd_S_First_Annt_Sex_Cde.setValue(pnd_Input_Pnd_First_Annt_Sex_Cde);                                                                                  //Natural: ASSIGN #S-FIRST-ANNT-SEX-CDE := #FIRST-ANNT-SEX-CDE
                    pnd_S_Scnd_Annt_Sex_Cde.setValue(pnd_Input_Pnd_Scnd_Annt_Sex_Cde);                                                                                    //Natural: ASSIGN #S-SCND-ANNT-SEX-CDE := #SCND-ANNT-SEX-CDE
                    pnd_S_Orig_Da_Cntrct_Nbr.setValue(pnd_Input_Pnd_Orig_Da_Cntrct_Nbr);                                                                                  //Natural: ASSIGN #S-ORIG-DA-CNTRCT-NBR := #ORIG-DA-CNTRCT-NBR
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-ISSUE-STATE
                    sub_Translate_Issue_State();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  05/05
                                                                                                                                                                          //Natural: PERFORM GET-LIVES-TYP-USING-OPTN-CODE
                    sub_Get_Lives_Typ_Using_Optn_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM POPULATE-FIELDS-10
                    sub_Populate_Fields_10();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Bypass.getBoolean()))                                                                                                                       //Natural: IF #BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                pnd_Cpr_Bypass.reset();                                                                                                                                   //Natural: RESET #CPR-BYPASS
                if (condition(pnd_Input_Pnd_Status_Cde.equals(9) || pnd_Input_Pnd_Payee_Cde.greater(1)))                                                                  //Natural: IF #STATUS-CDE = 9 OR #PAYEE-CDE GT 01
                {
                    pnd_Cpr_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #CPR-BYPASS := TRUE
                    //*      PERFORM GET-TERM-DTE
                    //*  AS PER JUN 05/05
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM GET-TRANS-DTE
                    sub_Get_Trans_Dte();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Tax_Id_Nbr.notEquals(pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr)))                                                                 //Natural: IF #TAX-ID-NBR NE #WS-#TAX-ID-NBR
                {
                    pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr.setValue(pnd_Input_Pnd_Tax_Id_Nbr);                                                                             //Natural: ASSIGN #WS-#TAX-ID-NBR := #TAX-ID-NBR
                                                                                                                                                                          //Natural: PERFORM CALL-MDMN100
                    sub_Call_Mdmn100();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_S_Status_Cde.setValue(pnd_Input_Pnd_Status_Cde);                                                                                                      //Natural: ASSIGN #S-STATUS-CDE := #STATUS-CDE
                pnd_S_Cntrct_Rtb_Amt.setValueEdited(pnd_Input_Pnd_Cntrct_Rtb_Amt.getValue(1),new ReportEditMask("ZZZZ,ZZ9.99"));                                          //Natural: MOVE EDITED #CNTRCT-RTB-AMT ( 1 ) ( EM = ZZZZ,ZZ9.99 ) TO #S-CNTRCT-RTB-AMT
                ldaIaalimra.getPnd_Output_Pnd_Premium_Amt().setValueEdited(pnd_Input_Pnd_Cntrct_Rtb_Amt.getValue(1),new ReportEditMask("9999999.99"));                    //Natural: MOVE EDITED #CNTRCT-RTB-AMT ( 1 ) ( EM = 9999999.99 ) TO #PREMIUM-AMT
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(1).setValue(pnd_Input_Pnd_Cntrct_Rtb_Amt.getValue(1));                         //Natural: ASSIGN NAZ-ACT-TIAA-STD-RATE-AMT ( 1 ) := #CNTRCT-RTB-AMT ( 1 )
                pnd_S_Mode_Ind.setValue(pnd_Input_Pnd_Mode_Ind);                                                                                                          //Natural: ASSIGN #S-MODE-IND := #MODE-IND
                pnd_S_Cntrct_Fin_Per_Pay_Dte.setValue(pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte);                                                                              //Natural: ASSIGN #S-CNTRCT-FIN-PER-PAY-DTE := #CNTRCT-FIN-PER-PAY-DTE
                pnd_S_Cpr_Id_Nbr.setValue(pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                                      //Natural: ASSIGN #S-CPR-ID-NBR := #CPR-ID-NBR
                pnd_S_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                                          //Natural: ASSIGN #S-PPCN-NBR := #PPCN-NBR
                pnd_S_Payee_Cde_A.setValue(pnd_Input_Pnd_Payee_Cde_A);                                                                                                    //Natural: ASSIGN #S-PAYEE-CDE-A := #PAYEE-CDE-A
                //*  FIXED PERIOD
                if (condition(pnd_S_Optn.equals(21)))                                                                                                                     //Natural: IF #S-OPTN = 21
                {
                                                                                                                                                                          //Natural: PERFORM GET-GUARANTEED-PERIOD
                    sub_Get_Guaranteed_Period();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-RES-STATE
                sub_Translate_Res_State();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM POPULATE-FIELDS-20
                sub_Populate_Fields_20();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cpr_Bypass.getBoolean()))                                                                                                                   //Natural: IF #CPR-BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(30)))                                                                                                          //Natural: IF #RECORD-CODE = 30
            {
                pnd_Write_It.setValue(true);                                                                                                                              //Natural: ASSIGN #WRITE-IT := TRUE
                //*  05/09
                //*  05/09
                if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("T")))                                                                                                  //Natural: IF #SUMM-CMPNY-CDE = 'T'
                {
                    pnd_Ws_Comp_T_Start.setValue(true);                                                                                                                   //Natural: ASSIGN #WS-COMP-T-START := TRUE
                    pnd_Ws_St_Date.setValue(pnd_S_Issue_Dte);                                                                                                             //Natural: ASSIGN #WS-ST-DATE := #S-ISSUE-DTE
                    pnd_Ws_St_Date_Pnd_Ws_St_Date_Mm.setValue(1);                                                                                                         //Natural: MOVE 01 TO #WS-ST-DATE-MM
                    pnd_Ws_St_Date_Pnd_Ws_St_Date_Yy.nadd(1);                                                                                                             //Natural: ADD 01 TO #WS-ST-DATE-YY
                                                                                                                                                                          //Natural: PERFORM GET-BASE-RATE
                    sub_Get_Base_Rate();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("U") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W")))                                                      //Natural: IF #SUMM-CMPNY-CDE = 'U' OR = 'W'
                {
                    pnd_Input_Pnd_Summ_Per_Dvdnd.reset();                                                                                                                 //Natural: RESET #SUMM-PER-DVDND
                    if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W")))                                                                                              //Natural: IF #SUMM-CMPNY-CDE = 'W'
                    {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                        sub_Get_Auv();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  05/09
                    //*  05/09
                    if (condition(! (pnd_Ws_Comp_T_Start.getBoolean())))                                                                                                  //Natural: IF NOT #WS-COMP-T-START
                    {
                        pnd_Ws_St_Date.setValue(pnd_S_Issue_Dte);                                                                                                         //Natural: ASSIGN #WS-ST-DATE := #S-ISSUE-DTE
                        pnd_Ws_St_Date_Pnd_Ws_St_Date_Yy.nadd(1);                                                                                                         //Natural: ADD 01 TO #WS-ST-DATE-YY
                        pnd_Ws_St_Date_Pnd_Ws_St_Date_Mm.setValue(5);                                                                                                     //Natural: MOVE 05 TO #WS-ST-DATE-MM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Pmt.compute(new ComputeParameters(false, pnd_Tot_Pmt), pnd_Tot_Pmt.add(pnd_Input_Pnd_Summ_Per_Pymnt).add(pnd_Input_Pnd_Summ_Per_Dvdnd));          //Natural: ASSIGN #TOT-PMT := #TOT-PMT + #SUMM-PER-PYMNT + #SUMM-PER-DVDND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        //*  08/15 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getWorkFiles().write(3, false, pnd_Mrk);                                                                                                                          //Natural: WRITE WORK FILE 3 #MRK
        getReports().write(0, "TOTAL RECORDS EXTRACTED:",pnd_I, new ReportEditMask ("ZZZ9"));                                                                             //Natural: WRITE ( 0 ) 'TOTAL RECORDS EXTRACTED:' #I ( EM = ZZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-IT
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-FIELDS-10
        //* **  $$$$$   APPEARED TWICE  $$$$$
        //* **  $$$$$   APPEARED TWICE  $$$$$
        //* *
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-FIELDS-20
        //*  FROM MDMN100
        //* *
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TERM-DTE
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TRANS-DTE
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-GUARANTEED-PERIOD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-ISSUE-STATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-RES-STATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  DEFINE SUBROUTINE SET-START-END-DATES
        //* ***********************************************************************
        //*  REPEAT
        //*   IF #W-DTE-M = 03 OR = 06 OR = 09 OR = 12
        //*     #END-DTE := VAL(#W-DTE-A)
        //*     #START-DTE := #END-DTE
        //*     SUBTRACT 2 FROM #START-DTE-M
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF #W-DTE-M = 01
        //*     #W-DTE-M := 12
        //*     SUBTRACT 1 FROM #W-DTE-Y
        //*     ESCAPE TOP
        //*   END-IF
        //*   SUBTRACT 1 FROM #W-DTE-M
        //*  END-REPEAT
        //*  END-SUBROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-START-END-DATES
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-OUTPUT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-INIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER
        //* * 'PARTKEY8,'
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-MDMN100
        //*     #MDMA100.#I-SSN   := #TAX-ID-NBR
        //*       MSG-INFO-SUB.##MSG := #MDMA100.#O-RETURN-TEXT
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LIVES-TYP-USING-OPTN-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BASE-RATE
        //* ***********************************************************************
        //*  11/14 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SL-GUAR-PERIOD
        //* ***********************************************************************
        //*  11/14 - END
    }
    private void sub_Display_It() throws Exception                                                                                                                        //Natural: DISPLAY-IT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cnt.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #CNT
        ldaIaalimra.getPnd_Output_Pnd_Txn_Type().setValue(1);                                                                                                             //Natural: ASSIGN #TXN-TYPE := 1
        ldaIaalimra.getPnd_Output_Pnd_Txn_Nbr().setValue(pnd_Cnt);                                                                                                        //Natural: ASSIGN #TXN-NBR := #CNT
        //* * #PAYOUT-AMT                    := #TOT-PMT /* OPTIONAL
        ldaIaalimra.getPnd_Output_Pnd_Payout_Amt().setValueEdited(pnd_Tot_Pmt,new ReportEditMask("9999999.99"));                                                          //Natural: MOVE EDITED #TOT-PMT ( EM = 9999999.99 ) TO #PAYOUT-AMT
        //*  05/09
        ldaIaalimra.getPnd_Output_Pnd_Out_Start_Dte().setValue(pnd_Ws_St_Date);                                                                                           //Natural: MOVE #WS-ST-DATE TO #OUT-START-DTE
        //*  START-DTE AND END-DTE
        //*  11/14 ****************
        //*  11/14 INSERTED
        //*  11/14 INSERTED
        //*  082018
        //*  082018
        //*  082018
        getWorkFiles().write(2, true, ldaIaalimra.getPnd_Output_Pnd_Xml_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Pmry_Obj_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Ref_Guid(),  //Natural: WRITE WORK FILE 2 VARIABLE #XML-ID ',' #PMRY-OBJ-ID ',' #TRANS-REF-GUID ',' #TRANS-TYPE ',' #TRANS-SUBTYPE ',' #TRANS-EXE-DTE ',' #TRANS-EXE-TME ',' #TRANS-MODE ',' #INQ-LEVEL ',' #PEND-RESPONSE ',' #NO-RESPONSE ',' #TEST-IND ',' #PRIMARY-OBJECT-TYP ',' #CREAT-DTE ',' #CREAT-TME ',' #SOURCE-INFO-NME ',' #SOURCE-INFO-DESC ',' #SOURCE-INFO-COMMENT ',' #FILE-CONTROL-ID ',' #HOLDING-ID ',' #HOLDING-TYPE-CDE ',' #HOLDING-STAT ',' #AS-OF-DTE ',' #SHARE-CLASS ',' #POLICY-NBR ',' #CERT-NBR ',' #LOB ',' #PRODUCT-TYPE ',' #PRODUCT-CDE ',' #CARRIER-CDE ',' #PLAN-NAME ',' #POLICY-STAT ',' #ISSUE-NATION ',' #ISSUE-TYPE ',' #JURISDICTION ',' #REPLACEMENT-TYPE ',' #POLICY-VALUE ',' #EFF-DATE ',' #OUT-ISSUE-DTE ',' #STATUS-CHANGE-DTE ',' #PRIOR-POLICY-NBR ',' #PAYOUT-TYPE ',' #QUAL-PLAN-TYPE ',' #QUAL-PLAN-SUB-TYPE ',' #CUM-WTHDRWL-AMTITD ',' #DEATH-BENEFIT-AMT ',' #SURRENDER-VALUE ',' #GUARINT-RATE ',' #GROSS-PREM-AMTITD ',' #CUM-PREM-AMT-YR1 ',' #PAYOUT-ID ',' #INCOME-OPTION ',' #PAYOUT-AMT ',' #PAYOUT-MODE ',' #START-DATE ',' #ANNUITY-START-DTE ',' #LIVES-TYPE ',' #ANNUAL-INDX-TYPE ',' #PERIOD-CERTAIN-MODE ',' #PERIOD-CERTAIN-PERIODS ',' #PAYOUT-CHANGE-ID ',' #CHANGE-MODE ',' #PAYOUT-CHANGE-AMT ',' #AMT-QUALIFIER ',' #PAYOUT-PCT-TYPE ',' #DESCRIPTION ',' #OUT-START-DTE ',' #OUT-END-DTE ',' #NEXT-ACTIVITY-DTE ',' #TIMING-BASIS ',' #PAYOUT-PCT ',' #RIDER-ID ',' #RIDER-TYPE-CDE ',' #RIDER-TYPE-SUB-CDE ',' #RIDER-CDE ',' #EFF-DTE ',' #TERM-DATE ',' #TOT-AMT ',' #TOT-AMT-LAST-ANN ',' #LIVES-TYP ',' #BENEFIT-PCT ',' #EVENT-TYPE ',' #EVENT-YEAR ',' #RIDER-FREE-AMT ',' #FINANCIAL-ACT-ID ',' #APPLIES-TO-OVERAGE-ID ',' #FINACT-TYPE ',' #FIN-ACT-SUB-TYPE ',' #REVERSAL-IND ',' #FIN-ACT-GROSS-AMT ',' #ACC-VALUE-LAST-ANN ',' #SUB-ACCT-ID ',' #ASSET-CLASS ',' #TOT-VALUE ',' #BASE-RATE ',' #DUR-QUALIFIER ',' #GUARANTEE-DURATION ',' #CAP-RATE ',' #PART-RATE ',' #MARKET-VAL-ADJUST-IND ',' #BONUS-RATE ',' #ACCT-VALUE-LAST-ANN ',' #FINANCIAL-ACTID ',' #APPLIES-TO-COVERAGE-ID ',' #FIN-ACT-TYPE ',' #FIN-ACT-SUBTYPE ',' #REV-IND ',' #FIN-ACTIVITY-GROSS-AMT ',' #ARRANGEMENT-ID ',' #ARR-TYPE ',' #MODAL-AMT ',' #PARTY-ID1 ',' #PARTY-TYPE-CDE1 ',' #PARTY-KEY1 ',' #RES-STATE1 ',' #RES-COUNTRY1 ',' #RES-ZIP1 ',' #NIPR1 ',' #CO-PROD-ID1 ',' #CARRIER-APPT-STAT1 ',' #DIST-CHANNEL1 ',' #DIST-CHANNEL-SUB-TYPE1 ',' #DIST-CHANNEL-NAME1 ',' #DIST-CHANNEL-CDE1 ',' #PARTY-ID2 ',' #PARTY-TYPE-CDE2 ',' #PARTY-KEY2 ',' #RES-STATE2 ',' #RES-COUNTRY2 ',' #RES-ZIP2 ',' #NIPR2 ',' #CO-PROD-ID2 ',' #CARRIER-APPT-STAT2 ',' #DIST-CHANNEL2 ',' #DIST-CHANNEL-SUB-TYPE2 ',' #DIST-CHANNEL-NAME2 ',' #DIST-CHANNEL-CDE2 ',' #PARTY-ID3 ',' #PARTY-TYPE-CDE3 ',' #PARTY-KEY3 ',' #RES-STATE3 ',' #RES-COUNTRY3 ',' #RES-ZIP3 ',' #PARTY-ID4 ',' #PARTY-TYPE-CDE4 ',' #PARTY-KEY4 ',' #RES-STATE4 ',' #RES-COUNTRY4 ',' #RES-ZIP4 ',' #PARTY-ID5 ',' #PARTY-TYPE-CDE5 ',' #PARTY-KEY5 ',' #RES-STATE5 ',' #RES-COUNTRY5 ',' #RES-ZIP5 ',' #PARTY-ID6 ',' #PARTY-TYPE-CDE6 ',' #PARTY-KEY6 ',' #FULL-NME6 ',' #GOV-ID6 ',' #GOV-ID-TYPE-CDE6 ',' #RES-STATE6 ',' #RES-COUNTRY6 ',' #RES-ZIP6 ',' #PARTY-ID7 ',' #PARTY-TYPE-CDE7 ',' #PARTY-KEY7 ',' #FULL-NME7 ',' #GOV-ID7 ',' #GOV-ID-TYPE-CDE7 ',' #RES-STATE7 ',' #RES-COUNTRY7 ',' #RES-ZIP7 ',' #PARTY-KEY8 ',' #FULL-NME8 ',' #PARTY-TYPE-CDE8 ',' #RES-STATE8 ',' #RES-COUNTRY8 ',' #RES-ZIP8 ',' #PARTY-TYPE-CDE9 ',' #PARTY-KEY9 ',' #FULL-NME9 ',' #RES-STATE9 ',' #RES-COUNTRY9 ',' #RES-ZIP9 ',' #REL-ID1 ',' #ORIG-OBJECT-ID1 ',' #REL-OBJECT-ID1 ',' #ORIG-OBJECT-TYPE1 ',' #REL-OBJECT-TYPE1 ',' #REL-ROLE-CODE1 ',' #INT-PCT1 ',' #REL-ID2 ',' #ORIG-OBJECT-ID2 ',' #REL-OBJECT-ID2 ',' #ORIG-OBJECT-TYPE2 ',' #REL-OBJECT-TYPE2 ',' #REL-ROLE-CODE2 ',' #INT-PCT2 ',' #REL-ID3 ',' #ORIG-OBJECT-ID3 ',' #REL-OBJECT-ID3 ',' #ORIG-OBJECT-TYPE3 ',' #REL-OBJECT-TYPE3 ',' #REL-ROLE-CODE3 ',' #REL-ID4 ',' #ORIG-OBJECT-ID4 ',' #REL-OBJECT-ID4 ',' #ORIG-OBJECT-TYPE4 ',' #REL-OBJECT-TYPE4 ',' #REL-ROLE-CODE4 ',' #REL-ID5 ',' #ORIG-OBJECT-ID5 ',' #REL-OBJECT-ID5 ',' #ORIG-OBJECT-TYPE5 ',' #REL-OBJECT-TYPE5 ',' #REL-ROLE-CODE5 ',' #REL-ID6 ',' #ORIG-OBJECT-ID6 ',' #REL-OBJECT-ID6 ',' #ORIG-OBJECT-TYPE6 ',' #REL-OBJECT-TYPE6 ',' #REL-ROLE-CODE6 ',' #REL-ID7 ',' #ORIG-OBJECT-ID7 ',' #REL-OBJECT-ID7 ',' #ORIG-OBJECT-TYPE7 ',' #REL-OBJECT-TYPE7 ',' #REL-ROLE-CODE7 ',' #OWNER-DOB ',' #JNT-OWNER-DOB ',' #TXN-TYPE ',' #TXN-NBR ',' #PREMIUM-AMT ','
            ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Subtype(), ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Exe_Dte(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Exe_Tme(), ",", ldaIaalimra.getPnd_Output_Pnd_Trans_Mode(), ",", ldaIaalimra.getPnd_Output_Pnd_Inq_Level(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Pend_Response(), ",", ldaIaalimra.getPnd_Output_Pnd_No_Response(), ",", ldaIaalimra.getPnd_Output_Pnd_Test_Ind(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Primary_Object_Typ(), ",", ldaIaalimra.getPnd_Output_Pnd_Creat_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Creat_Tme(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Source_Info_Nme(), ",", ldaIaalimra.getPnd_Output_Pnd_Source_Info_Desc(), ",", ldaIaalimra.getPnd_Output_Pnd_Source_Info_Comment(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_File_Control_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Holding_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Holding_Type_Cde(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Holding_Stat(), ",", ldaIaalimra.getPnd_Output_Pnd_As_Of_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Share_Class(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Policy_Nbr(), ",", ldaIaalimra.getPnd_Output_Pnd_Cert_Nbr(), ",", ldaIaalimra.getPnd_Output_Pnd_Lob(), ",", 
            ldaIaalimra.getPnd_Output_Pnd_Product_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Product_Cde(), ",", ldaIaalimra.getPnd_Output_Pnd_Carrier_Cde(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Plan_Name(), ",", ldaIaalimra.getPnd_Output_Pnd_Policy_Stat(), ",", ldaIaalimra.getPnd_Output_Pnd_Issue_Nation(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Issue_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Jurisdiction(), ",", ldaIaalimra.getPnd_Output_Pnd_Replacement_Type(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Policy_Value(), ",", ldaIaalimra.getPnd_Output_Pnd_Eff_Date(), ",", ldaIaalimra.getPnd_Output_Pnd_Out_Issue_Dte(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Status_Change_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Prior_Policy_Nbr(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Type(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Qual_Plan_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Qual_Plan_Sub_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Cum_Wthdrwl_Amtitd(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Death_Benefit_Amt(), ",", ldaIaalimra.getPnd_Output_Pnd_Surrender_Value(), ",", ldaIaalimra.getPnd_Output_Pnd_Guarint_Rate(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Gross_Prem_Amtitd(), ",", ldaIaalimra.getPnd_Output_Pnd_Cum_Prem_Amt_Yr1(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Id(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Income_Option(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Amt(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Mode(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Start_Date(), ",", ldaIaalimra.getPnd_Output_Pnd_Annuity_Start_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Lives_Type(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Annual_Indx_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Period_Certain_Mode(), ",", ldaIaalimra.getPnd_Output_Pnd_Period_Certain_Periods(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Change_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Change_Mode(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Change_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Amt_Qualifier(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Pct_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Description(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Out_Start_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Out_End_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Next_Activity_Dte(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Timing_Basis(), ",", ldaIaalimra.getPnd_Output_Pnd_Payout_Pct(), ",", ldaIaalimra.getPnd_Output_Pnd_Rider_Id(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rider_Type_Cde(), ",", ldaIaalimra.getPnd_Output_Pnd_Rider_Type_Sub_Cde(), ",", ldaIaalimra.getPnd_Output_Pnd_Rider_Cde(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Eff_Dte(), ",", ldaIaalimra.getPnd_Output_Pnd_Term_Date(), ",", ldaIaalimra.getPnd_Output_Pnd_Tot_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Tot_Amt_Last_Ann(), ",", ldaIaalimra.getPnd_Output_Pnd_Lives_Typ(), ",", ldaIaalimra.getPnd_Output_Pnd_Benefit_Pct(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Event_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Event_Year(), ",", ldaIaalimra.getPnd_Output_Pnd_Rider_Free_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Financial_Act_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Applies_To_Overage_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Finact_Type(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Sub_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Reversal_Ind(), ",", ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Gross_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Acc_Value_Last_Ann(), ",", ldaIaalimra.getPnd_Output_Pnd_Sub_Acct_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Asset_Class(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Tot_Value(), ",", ldaIaalimra.getPnd_Output_Pnd_Base_Rate(), ",", ldaIaalimra.getPnd_Output_Pnd_Dur_Qualifier(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Guarantee_Duration(), ",", ldaIaalimra.getPnd_Output_Pnd_Cap_Rate(), ",", ldaIaalimra.getPnd_Output_Pnd_Part_Rate(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Market_Val_Adjust_Ind(), ",", ldaIaalimra.getPnd_Output_Pnd_Bonus_Rate(), ",", ldaIaalimra.getPnd_Output_Pnd_Acct_Value_Last_Ann(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Financial_Actid(), ",", ldaIaalimra.getPnd_Output_Pnd_Applies_To_Coverage_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Type(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Subtype(), ",", ldaIaalimra.getPnd_Output_Pnd_Rev_Ind(), ",", ldaIaalimra.getPnd_Output_Pnd_Fin_Activity_Gross_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Arrangement_Id(), ",", ldaIaalimra.getPnd_Output_Pnd_Arr_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Modal_Amt(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id1(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde1(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_State1(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country1(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Nipr1(), ",", ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id1(), ",", ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel1(), ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type1(), ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde1(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id2(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key2(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State2(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip2(), ",", ldaIaalimra.getPnd_Output_Pnd_Nipr2(), ",", ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat2(), ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel2(), ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name2(), ",", ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde2(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id3(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde3(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key3(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State3(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country3(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip3(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id4(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde4(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key4(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State4(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country4(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip4(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id5(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde5(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key5(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State5(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country5(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip5(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id6(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde6(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key6(), ",", ldaIaalimra.getPnd_Output_Pnd_Full_Nme6(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Gov_Id6(), ",", ldaIaalimra.getPnd_Output_Pnd_Gov_Id_Type_Cde6(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State6(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country6(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip6(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Id7(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde7(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key7(), ",", ldaIaalimra.getPnd_Output_Pnd_Full_Nme7(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Gov_Id7(), ",", ldaIaalimra.getPnd_Output_Pnd_Gov_Id_Type_Cde7(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State7(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country7(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip7(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key8(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Full_Nme8(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde8(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State8(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country8(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip8(), ",", ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde9(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Party_Key9(), ",", ldaIaalimra.getPnd_Output_Pnd_Full_Nme9(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_State9(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Res_Country9(), ",", ldaIaalimra.getPnd_Output_Pnd_Res_Zip9(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id1(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id1(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type1(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code1(), ",", ldaIaalimra.getPnd_Output_Pnd_Int_Pct1(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id2(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id2(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type2(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type2(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code2(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Int_Pct2(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id3(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id3(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id3(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type3(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type3(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code3(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id4(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id4(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id4(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type4(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type4(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code4(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id5(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id5(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id5(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type5(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type5(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code5(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id6(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id6(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id6(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type6(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type6(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code6(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Id7(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id7(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id7(), ",", ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type7(), ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type7(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code7(), ",", ldaIaalimra.getPnd_Output_Pnd_Owner_Dob(), ",", ldaIaalimra.getPnd_Output_Pnd_Jnt_Owner_Dob(), 
            ",", ldaIaalimra.getPnd_Output_Pnd_Txn_Type(), ",", ldaIaalimra.getPnd_Output_Pnd_Txn_Nbr(), ",", ldaIaalimra.getPnd_Output_Pnd_Premium_Amt(), 
            ",");
        pnd_Write_It.reset();                                                                                                                                             //Natural: RESET #WRITE-IT #TERM-DTE #TRANS-DTE #TOT-PMT
        pnd_Term_Dte.reset();
        pnd_Trans_Dte.reset();
        pnd_Tot_Pmt.reset();
        //*  05/09
        ldaIaalimra.getPnd_Output().reset();                                                                                                                              //Natural: RESET #OUTPUT #WS-COMP-T-START #WS-ST-DATE
        pnd_Ws_Comp_T_Start.reset();
        pnd_Ws_St_Date.reset();
    }
    //*  OPTIONAL
    //*  OPTIONAL
    //*  OPTIONAL
    //*  OPTIONAL
    //*  REQUIRED ???
    //*  CONDITIONAL
    //*  REQUIRED ???
    //*  REQUIRED ???
    //*  REQUIRED
    //*  NONE FOR SPIA
    //*  REQUIRED ???
    //*  OPTIONAL
    //*  CONDITIONAL
    //*  OPTIONAL
    private void sub_Populate_Fields_10() throws Exception                                                                                                                //Natural: POPULATE-FIELDS-10
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaalimra.getPnd_Output_Pnd_Xml_Id().setValue(" ");                                                                                                             //Natural: ASSIGN #XML-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Pmry_Obj_Id().setValue(" ");                                                                                                        //Natural: ASSIGN #PMRY-OBJ-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Trans_Ref_Guid().setValue(" ");                                                                                                     //Natural: ASSIGN #TRANS-REF-GUID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Trans_Type().setValue("180");                                                                                                       //Natural: ASSIGN #TRANS-TYPE := '180'
        ldaIaalimra.getPnd_Output_Pnd_Trans_Subtype().setValue("18002");                                                                                                  //Natural: ASSIGN #TRANS-SUBTYPE := '18002'
        ldaIaalimra.getPnd_Output_Pnd_Trans_Start_Dte().setValue(pnd_Start_Dte);                                                                                          //Natural: ASSIGN #TRANS-START-DTE := #START-DTE
        ldaIaalimra.getPnd_Output_Pnd_Trans_End_Dte().setValue(pnd_End_Dte);                                                                                              //Natural: ASSIGN #TRANS-END-DTE := #END-DTE
        ldaIaalimra.getPnd_Output_Pnd_Trans_Exe_Tme().setValue(Global.getTIME());                                                                                         //Natural: ASSIGN #TRANS-EXE-TME := *TIME
        ldaIaalimra.getPnd_Output_Pnd_Trans_Mode().setValue("2");                                                                                                         //Natural: ASSIGN #TRANS-MODE := '2'
        ldaIaalimra.getPnd_Output_Pnd_Inq_Level().setValue("3");                                                                                                          //Natural: ASSIGN #INQ-LEVEL := '3'
        ldaIaalimra.getPnd_Output_Pnd_Pend_Response().setValue("0");                                                                                                      //Natural: ASSIGN #PEND-RESPONSE := '0'
        ldaIaalimra.getPnd_Output_Pnd_No_Response().setValue("1");                                                                                                        //Natural: ASSIGN #NO-RESPONSE := '1'
        ldaIaalimra.getPnd_Output_Pnd_Test_Ind().setValue("0");                                                                                                           //Natural: ASSIGN #TEST-IND := '0'
        ldaIaalimra.getPnd_Output_Pnd_Primary_Object_Typ().setValue("4");                                                                                                 //Natural: ASSIGN #PRIMARY-OBJECT-TYP := '4'
        ldaIaalimra.getPnd_Output_Pnd_Creat_Dte().setValue(" ");                                                                                                          //Natural: ASSIGN #CREAT-DTE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Creat_Tme().setValue(" ");                                                                                                          //Natural: ASSIGN #CREAT-TME := ' '
        ldaIaalimra.getPnd_Output_Pnd_Source_Info_Nme().setValue(" ");                                                                                                    //Natural: ASSIGN #SOURCE-INFO-NME := ' '
        ldaIaalimra.getPnd_Output_Pnd_Source_Info_Desc().setValue(" ");                                                                                                   //Natural: ASSIGN #SOURCE-INFO-DESC := ' '
        ldaIaalimra.getPnd_Output_Pnd_Source_Info_Comment().setValue(" ");                                                                                                //Natural: ASSIGN #SOURCE-INFO-COMMENT := ' '
        ldaIaalimra.getPnd_Output_Pnd_File_Control_Id().setValue(" ");                                                                                                    //Natural: ASSIGN #FILE-CONTROL-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Holding_Id().setValue(" ");                                                                                                         //Natural: ASSIGN #HOLDING-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Holding_Type_Cde().setValue("2");                                                                                                   //Natural: ASSIGN #HOLDING-TYPE-CDE := '2'
        ldaIaalimra.getPnd_Output_Pnd_Share_Class().setValue(" ");                                                                                                        //Natural: ASSIGN #SHARE-CLASS := ' '
        ldaIaalimra.getPnd_Output_Pnd_Policy_Nbr().setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #POLICY-NBR := #PPCN-NBR
        ldaIaalimra.getPnd_Output_Pnd_Cert_Nbr().setValue(" ");                                                                                                           //Natural: ASSIGN #CERT-NBR := ' '
        ldaIaalimra.getPnd_Output_Pnd_Lob().setValue("2");                                                                                                                //Natural: ASSIGN #LOB := '2'
        ldaIaalimra.getPnd_Output_Pnd_Product_Type().setValue("9");                                                                                                       //Natural: ASSIGN #PRODUCT-TYPE := '9'
        ldaIaalimra.getPnd_Output_Pnd_Product_Cde().setValue("SPIA");                                                                                                     //Natural: ASSIGN #PRODUCT-CDE := 'SPIA'
        ldaIaalimra.getPnd_Output_Pnd_Carrier_Cde().setValue("60142");                                                                                                    //Natural: ASSIGN #CARRIER-CDE := '60142'
        ldaIaalimra.getPnd_Output_Pnd_Plan_Name().setValue("SPIA");                                                                                                       //Natural: ASSIGN #PLAN-NAME := 'SPIA'
        ldaIaalimra.getPnd_Output_Pnd_Policy_Stat().setValue("1");                                                                                                        //Natural: ASSIGN #POLICY-STAT := '1'
        ldaIaalimra.getPnd_Output_Pnd_Issue_Nation().setValue("1");                                                                                                       //Natural: ASSIGN #ISSUE-NATION := '1'
        ldaIaalimra.getPnd_Output_Pnd_Issue_Type().setValue(" ");                                                                                                         //Natural: ASSIGN #ISSUE-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Jurisdiction().setValue(pnd_Issue_St);                                                                                              //Natural: ASSIGN #JURISDICTION := #ISSUE-ST
        ldaIaalimra.getPnd_Output_Pnd_Policy_Value().setValue(" ");                                                                                                       //Natural: ASSIGN #POLICY-VALUE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Eff_Date().setValue(" ");                                                                                                           //Natural: ASSIGN #EFF-DATE := ' '
        pnd_Ws_Out_Issue_Dte.setValue(pnd_S_Issue_Dte);                                                                                                                   //Natural: ASSIGN #WS-OUT-ISSUE-DTE := #S-ISSUE-DTE
        if (condition(pnd_Input_Pnd_Cntrct_Issue_Dte_Dd.equals(getZero())))                                                                                               //Natural: IF #CNTRCT-ISSUE-DTE-DD = 0
        {
            pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A.setValue("01");                                                                                                           //Natural: ASSIGN #CNTRCT-ISSUE-DTE-DD-A := '01'
        }                                                                                                                                                                 //Natural: END-IF
        //*  OPTIONAL
        //*  OPTIONAL
        //*  REQUIRED
        //*  REQUIRED
        //*  CONDICITONAL
        //*  CONDITIONAL
        //*  CONDITIONAL
        //*  OPTIONAL
        //*  CONDITIONAL
        //*  CONDITIONAL
        //*  CONDITIONAL
        setValueToSubstring(pnd_Input_Pnd_Cntrct_Issue_Dte_Dd_A,pnd_Ws_Out_Issue_Dte,7,2);                                                                                //Natural: MOVE #CNTRCT-ISSUE-DTE-DD-A TO SUBSTR ( #WS-OUT-ISSUE-DTE,7,2 )
        ldaIaalimra.getPnd_Output_Pnd_Out_Issue_Dte().setValue(pnd_Ws_Out_Issue_Dte);                                                                                     //Natural: ASSIGN #OUT-ISSUE-DTE := #WS-OUT-ISSUE-DTE
        ldaIaalimra.getPnd_Output_Pnd_Status_Change_Dte().setValue(" ");                                                                                                  //Natural: ASSIGN #STATUS-CHANGE-DTE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Prior_Policy_Nbr().setValue(" ");                                                                                                   //Natural: ASSIGN #PRIOR-POLICY-NBR := ' '
        ldaIaalimra.getPnd_Output_Pnd_Payout_Type().setValue("1");                                                                                                        //Natural: ASSIGN #PAYOUT-TYPE := '1'
        ldaIaalimra.getPnd_Output_Pnd_Qual_Plan_Type().setValue("1");                                                                                                     //Natural: ASSIGN #QUAL-PLAN-TYPE := '1'
        ldaIaalimra.getPnd_Output_Pnd_Qual_Plan_Sub_Type().setValue(" ");                                                                                                 //Natural: ASSIGN #QUAL-PLAN-SUB-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Cum_Wthdrwl_Amtitd().setValue(" ");                                                                                                 //Natural: ASSIGN #CUM-WTHDRWL-AMTITD := ' '
        ldaIaalimra.getPnd_Output_Pnd_Death_Benefit_Amt().setValue(" ");                                                                                                  //Natural: ASSIGN #DEATH-BENEFIT-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Surrender_Value().setValue(" ");                                                                                                    //Natural: ASSIGN #SURRENDER-VALUE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Guarint_Rate().setValue(" ");                                                                                                       //Natural: ASSIGN #GUARINT-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Gross_Prem_Amtitd().setValue(" ");                                                                                                  //Natural: ASSIGN #GROSS-PREM-AMTITD := ' '
        ldaIaalimra.getPnd_Output_Pnd_Cum_Prem_Amt_Yr1().setValue(" ");                                                                                                   //Natural: ASSIGN #CUM-PREM-AMT-YR1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Payout_Id().setValue(" ");                                                                                                          //Natural: ASSIGN #PAYOUT-ID := ' '
        if (condition(pnd_Input_Pnd_Optn_Cde.equals(21)))                                                                                                                 //Natural: IF #OPTN-CDE = 21
        {
            ldaIaalimra.getPnd_Output_Pnd_Income_Option().setValue("3");                                                                                                  //Natural: ASSIGN #INCOME-OPTION := '3'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalimra.getPnd_Output_Pnd_Income_Option().setValue("1");                                                                                                  //Natural: ASSIGN #INCOME-OPTION := '1'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Start_Date().setValue(pnd_Input_Pnd_1st_Due_Dte);                                                                                   //Natural: ASSIGN #START-DATE := #1ST-DUE-DTE
        //*  OPTIONAL
        //*  OPTIONAL
        setValueToSubstring("01",ldaIaalimra.getPnd_Output_Pnd_Start_Date(),7,2);                                                                                         //Natural: MOVE '01' TO SUBSTR ( #START-DATE,7,2 )
        ldaIaalimra.getPnd_Output_Pnd_Annuity_Start_Dte().setValue(ldaIaalimra.getPnd_Output_Pnd_Start_Date());                                                           //Natural: ASSIGN #ANNUITY-START-DTE := #START-DATE
        ldaIaalimra.getPnd_Output_Pnd_Lives_Type().setValue(pnd_Ws_Lives_Type);                                                                                           //Natural: ASSIGN #LIVES-TYPE := #WS-LIVES-TYPE
        ldaIaalimra.getPnd_Output_Pnd_Annual_Indx_Type().setValue(" ");                                                                                                   //Natural: ASSIGN #ANNUAL-INDX-TYPE := ' '
        //*  OPTIONAL
        if (condition(pnd_Input_Pnd_Optn_Cde.equals(21)))                                                                                                                 //Natural: IF #OPTN-CDE = 21
        {
            ldaIaalimra.getPnd_Output_Pnd_Period_Certain_Mode().setValue("4");                                                                                            //Natural: ASSIGN #PERIOD-CERTAIN-MODE := '4'
            //*  OPTIONAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalimra.getPnd_Output_Pnd_Period_Certain_Mode().setValue("1");                                                                                            //Natural: ASSIGN #PERIOD-CERTAIN-MODE := '1'
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  CONDITIONAL
            //*  REQUIRED ???
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  CONDITIONAL
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  OPTIONAL
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED $$$$
            //*  OPTIONAL
            //*  REQUIRED $$$
            //*  REQUIRED $$$
            //*  REQUIRED
            //*  REQUIRED
            //*  OPTIONAL
            //*  CONDITIONAL
            //*  CONDITIONAL
            //*  CONDITIONAL
            //*  CONDITIONAL
            //*  CONDITIONAL
            //*  CONDITIONAL
            //*  REQUIRED
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED
            //*  REQUIRED
            //*  CONDITIONAL
            //*  REQUIRED ???
            //*  REQUIRED ???
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Payout_Change_Id().setValue(" ");                                                                                                   //Natural: ASSIGN #PAYOUT-CHANGE-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Change_Mode().setValue(" ");                                                                                                        //Natural: ASSIGN #CHANGE-MODE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Payout_Change_Amt().setValue(" ");                                                                                                  //Natural: ASSIGN #PAYOUT-CHANGE-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Amt_Qualifier().setValue(" ");                                                                                                      //Natural: ASSIGN #AMT-QUALIFIER := ' '
        ldaIaalimra.getPnd_Output_Pnd_Payout_Pct_Type().setValue(" ");                                                                                                    //Natural: ASSIGN #PAYOUT-PCT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Description().setValue(" ");                                                                                                        //Natural: ASSIGN #DESCRIPTION := ' '
        ldaIaalimra.getPnd_Output_Pnd_Out_End_Dte().setValue(" ");                                                                                                        //Natural: ASSIGN #OUT-END-DTE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Next_Activity_Dte().setValue(" ");                                                                                                  //Natural: ASSIGN #NEXT-ACTIVITY-DTE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Timing_Basis().setValue(" ");                                                                                                       //Natural: ASSIGN #TIMING-BASIS := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Id().setValue(" ");                                                                                                           //Natural: ASSIGN #RIDER-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Type_Cde().setValue(" ");                                                                                                     //Natural: ASSIGN #RIDER-TYPE-CDE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Type_Sub_Cde().setValue(" ");                                                                                                 //Natural: ASSIGN #RIDER-TYPE-SUB-CDE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Cde().setValue(" ");                                                                                                          //Natural: ASSIGN #RIDER-CDE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Eff_Dte().setValue(" ");                                                                                                            //Natural: ASSIGN #EFF-DTE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Term_Date().setValue(" ");                                                                                                          //Natural: ASSIGN #TERM-DATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Tot_Amt().setValue(" ");                                                                                                            //Natural: ASSIGN #TOT-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Tot_Amt_Last_Ann().setValue(" ");                                                                                                   //Natural: ASSIGN #TOT-AMT-LAST-ANN := ' '
        ldaIaalimra.getPnd_Output_Pnd_Lives_Typ().setValue(" ");                                                                                                          //Natural: ASSIGN #LIVES-TYP := ' '
        ldaIaalimra.getPnd_Output_Pnd_Benefit_Pct().setValue(" ");                                                                                                        //Natural: ASSIGN #BENEFIT-PCT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Event_Type().setValue(" ");                                                                                                         //Natural: ASSIGN #EVENT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Event_Year().setValue(" ");                                                                                                         //Natural: ASSIGN #EVENT-YEAR := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Free_Amt().setValue(" ");                                                                                                     //Natural: ASSIGN #RIDER-FREE-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Financial_Act_Id().setValue(" ");                                                                                                   //Natural: ASSIGN #FINANCIAL-ACT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Applies_To_Overage_Id().setValue(" ");                                                                                              //Natural: ASSIGN #APPLIES-TO-OVERAGE-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Finact_Type().setValue(" ");                                                                                                        //Natural: ASSIGN #FINACT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Sub_Type().setValue(" ");                                                                                                   //Natural: ASSIGN #FIN-ACT-SUB-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Reversal_Ind().setValue(" ");                                                                                                       //Natural: ASSIGN #REVERSAL-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Gross_Amt().setValue(" ");                                                                                                  //Natural: ASSIGN #FIN-ACT-GROSS-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Acc_Value_Last_Ann().setValue(" ");                                                                                                 //Natural: ASSIGN #ACC-VALUE-LAST-ANN := ' '
        ldaIaalimra.getPnd_Output_Pnd_Sub_Acct_Id().setValue(" ");                                                                                                        //Natural: ASSIGN #SUB-ACCT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Asset_Class().setValue(" ");                                                                                                        //Natural: ASSIGN #ASSET-CLASS := ' '
        ldaIaalimra.getPnd_Output_Pnd_Tot_Value().setValue(" ");                                                                                                          //Natural: ASSIGN #TOT-VALUE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Base_Rate().setValue(" ");                                                                                                          //Natural: ASSIGN #BASE-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dur_Qualifier().setValue(" ");                                                                                                      //Natural: ASSIGN #DUR-QUALIFIER := ' '
        ldaIaalimra.getPnd_Output_Pnd_Guarantee_Duration().setValue(" ");                                                                                                 //Natural: ASSIGN #GUARANTEE-DURATION := ' '
        ldaIaalimra.getPnd_Output_Pnd_Cap_Rate().setValue(" ");                                                                                                           //Natural: ASSIGN #CAP-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Part_Rate().setValue(" ");                                                                                                          //Natural: ASSIGN #PART-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Market_Val_Adjust_Ind().setValue(" ");                                                                                              //Natural: ASSIGN #MARKET-VAL-ADJUST-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Bonus_Rate().setValue(" ");                                                                                                         //Natural: ASSIGN #BONUS-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Acct_Value_Last_Ann().setValue(" ");                                                                                                //Natural: ASSIGN #ACCT-VALUE-LAST-ANN := ' '
        ldaIaalimra.getPnd_Output_Pnd_Financial_Actid().setValue(" ");                                                                                                    //Natural: ASSIGN #FINANCIAL-ACTID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Applies_To_Coverage_Id().setValue(" ");                                                                                             //Natural: ASSIGN #APPLIES-TO-COVERAGE-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Type().setValue(" ");                                                                                                       //Natural: ASSIGN #FIN-ACT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Subtype().setValue(" ");                                                                                                    //Natural: ASSIGN #FIN-ACT-SUBTYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rev_Ind().setValue(" ");                                                                                                            //Natural: ASSIGN #REV-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Activity_Gross_Amt().setValue(0);                                                                                               //Natural: ASSIGN #FIN-ACTIVITY-GROSS-AMT := 0000000.00
        ldaIaalimra.getPnd_Output_Pnd_Arrangement_Id().setValue(" ");                                                                                                     //Natural: ASSIGN #ARRANGEMENT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Arr_Type().setValue("2");                                                                                                           //Natural: ASSIGN #ARR-TYPE := '2'
        ldaIaalimra.getPnd_Output_Pnd_Modal_Amt().setValue(" ");                                                                                                          //Natural: ASSIGN #MODAL-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id1().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde1().setValue(" ");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Key1().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State1().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country1().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip1().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Nipr1().setValue(" ");                                                                                                              //Natural: ASSIGN #NIPR1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id1().setValue(" ");                                                                                                        //Natural: ASSIGN #CO-PROD-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat1().setValue(" ");                                                                                                 //Natural: ASSIGN #CARRIER-APPT-STAT1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel1().setValue(" ");                                                                                                      //Natural: ASSIGN #DIST-CHANNEL1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type1().setValue(" ");                                                                                             //Natural: ASSIGN #DIST-CHANNEL-SUB-TYPE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name1().setValue(" ");                                                                                                 //Natural: ASSIGN #DIST-CHANNEL-NAME1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde1().setValue(" ");                                                                                                  //Natural: ASSIGN #DIST-CHANNEL-CDE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id2().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde2().setValue(" ");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Key2().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State2().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country2().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip2().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Nipr2().setValue(" ");                                                                                                              //Natural: ASSIGN #NIPR2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id2().setValue(" ");                                                                                                        //Natural: ASSIGN #CO-PROD-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat2().setValue(" ");                                                                                                 //Natural: ASSIGN #CARRIER-APPT-STAT2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel2().setValue(" ");                                                                                                      //Natural: ASSIGN #DIST-CHANNEL2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type2().setValue(" ");                                                                                             //Natural: ASSIGN #DIST-CHANNEL-SUB-TYPE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name2().setValue(" ");                                                                                                 //Natural: ASSIGN #DIST-CHANNEL-NAME2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde2().setValue(" ");                                                                                                  //Natural: ASSIGN #DIST-CHANNEL-CDE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id5().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde5().setValue(" ");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Key5().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State5().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country5().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip5().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id6().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde6().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE6 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key6().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Full_Nme6().setValue(" ");                                                                                                          //Natural: ASSIGN #FULL-NME6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Gov_Id6().setValue(" ");                                                                                                            //Natural: ASSIGN #GOV-ID6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Gov_Id_Type_Cde6().setValue(" ");                                                                                                   //Natural: ASSIGN #GOV-ID-TYPE-CDE6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State6().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country6().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip6().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id7().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde7().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE7 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key7().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Full_Nme7().setValue(" ");                                                                                                          //Natural: ASSIGN #FULL-NME7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Gov_Id7().setValue(" ");                                                                                                            //Natural: ASSIGN #GOV-ID7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Gov_Id_Type_Cde7().setValue(" ");                                                                                                   //Natural: ASSIGN #GOV-ID-TYPE-CDE7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State7().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country7().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip7().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id1().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id1().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id1().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type1().setValue(" ");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type1().setValue(" ");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code1().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Int_Pct1().setValue(" ");                                                                                                           //Natural: ASSIGN #INT-PCT1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id2().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id2().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id2().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type2().setValue(" ");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type2().setValue(" ");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code2().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Int_Pct2().setValue(" ");                                                                                                           //Natural: ASSIGN #INT-PCT2 := ' '
        //*  11/14
        ldaIaalimra.getPnd_Output_Pnd_Owner_Dob().setValueEdited(pnd_Input_Pnd_First_Ann_Dob,new ReportEditMask("99999999"));                                             //Natural: MOVE EDITED #FIRST-ANN-DOB ( EM = 99999999 ) TO #OWNER-DOB
        //*  11/14
        if (condition(pnd_Input_Pnd_Scnd_Ann_Dob.greater(getZero())))                                                                                                     //Natural: IF #SCND-ANN-DOB GT 0
        {
            //*  11/14
            ldaIaalimra.getPnd_Output_Pnd_Jnt_Owner_Dob().setValueEdited(pnd_Input_Pnd_Scnd_Ann_Dob,new ReportEditMask("99999999"));                                      //Natural: MOVE EDITED #SCND-ANN-DOB ( EM = 99999999 ) TO #JNT-OWNER-DOB
            //*  11/14
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  11/14
            ldaIaalimra.getPnd_Output_Pnd_Jnt_Owner_Dob().reset();                                                                                                        //Natural: RESET #JNT-OWNER-DOB
            //*  11/14
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Fields_20() throws Exception                                                                                                                //Natural: POPULATE-FIELDS-20
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        if (condition(pnd_Input_Pnd_Status_Cde.equals(9)))                                                                                                                //Natural: IF #STATUS-CDE = 9
        {
            ldaIaalimra.getPnd_Output_Pnd_Holding_Stat().setValue("2");                                                                                                   //Natural: ASSIGN #HOLDING-STAT := '2'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalimra.getPnd_Output_Pnd_Holding_Stat().setValue("1");                                                                                                   //Natural: ASSIGN #HOLDING-STAT := '1'
        }                                                                                                                                                                 //Natural: END-IF
        //*   WHEN ACTIVE
        if (condition(pnd_Input_Pnd_Status_Cde.notEquals(9)))                                                                                                             //Natural: IF #STATUS-CDE NE 9
        {
            pnd_System_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_End_Dte_Pnd_End_Dte_A);                                                                     //Natural: MOVE EDITED #END-DTE-A TO #SYSTEM-DATE ( EM = YYYYMMDD )
            pnd_Day_Number.setValueEdited(pnd_System_Date,new ReportEditMask("O"));                                                                                       //Natural: MOVE EDITED #SYSTEM-DATE ( EM = O ) TO #DAY-NUMBER
            //*   SATURDAY
            if (condition(pnd_Day_Number.equals("7")))                                                                                                                    //Natural: IF #DAY-NUMBER = '7'
            {
                pnd_End_Dte_Pnd_End_Dte_D.nsubtract(1);                                                                                                                   //Natural: ASSIGN #END-DTE-D := #END-DTE-D - 1
            }                                                                                                                                                             //Natural: END-IF
            //*   SUNDAY
            if (condition(pnd_Day_Number.equals("1")))                                                                                                                    //Natural: IF #DAY-NUMBER = '1'
            {
                pnd_End_Dte_Pnd_End_Dte_D.nsubtract(2);                                                                                                                   //Natural: ASSIGN #END-DTE-D := #END-DTE-D - 2
            }                                                                                                                                                             //Natural: END-IF
            ldaIaalimra.getPnd_Output_Pnd_As_Of_Dte().setValue(pnd_End_Dte_Pnd_End_Dte_A);                                                                                //Natural: ASSIGN #AS-OF-DTE := #END-DTE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalimra.getPnd_Output_Pnd_As_Of_Dte().setValue(pnd_Term_Dte);                                                                                             //Natural: ASSIGN #AS-OF-DTE := #TERM-DTE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Replacement_Type().setValue(" ");                                                                                                   //Natural: ASSIGN #REPLACEMENT-TYPE := ' '
        //*  $$$$$
        short decideConditionsMet2689 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #MODE-IND;//Natural: VALUE 100
        if (condition((pnd_Input_Pnd_Mode_Ind.equals(100))))
        {
            decideConditionsMet2689++;
            ldaIaalimra.getPnd_Output_Pnd_Payout_Mode().setValue("4");                                                                                                    //Natural: ASSIGN #PAYOUT-MODE := '4'
        }                                                                                                                                                                 //Natural: VALUE 601:699
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(601) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(699)))))
        {
            decideConditionsMet2689++;
            ldaIaalimra.getPnd_Output_Pnd_Payout_Mode().setValue("3");                                                                                                    //Natural: ASSIGN #PAYOUT-MODE := '3'
        }                                                                                                                                                                 //Natural: VALUE 701:799
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(701) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(799)))))
        {
            decideConditionsMet2689++;
            ldaIaalimra.getPnd_Output_Pnd_Payout_Mode().setValue("2");                                                                                                    //Natural: ASSIGN #PAYOUT-MODE := '2'
        }                                                                                                                                                                 //Natural: VALUE 801:899
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(801) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(899)))))
        {
            decideConditionsMet2689++;
            ldaIaalimra.getPnd_Output_Pnd_Payout_Mode().setValue("1");                                                                                                    //Natural: ASSIGN #PAYOUT-MODE := '1'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaIaalimra.getPnd_Output_Pnd_Payout_Mode().setValue(" ");                                                                                                    //Natural: ASSIGN #PAYOUT-MODE := ' '
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  REQUIRED ???
            //*  #RES-CDE
            //*  REQUIRED ???
            //*  REQUIRED $$$
            //*  REQUIRED
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaalimra.getPnd_Output_Pnd_Period_Certain_Periods().setValue(pnd_Guar_Prd);                                                                                    //Natural: ASSIGN #PERIOD-CERTAIN-PERIODS := #GUAR-PRD
        ldaIaalimra.getPnd_Output_Pnd_Benefit_Pct().setValue(" ");                                                                                                        //Natural: ASSIGN #BENEFIT-PCT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Event_Type().setValue(" ");                                                                                                         //Natural: ASSIGN #EVENT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Event_Year().setValue(" ");                                                                                                         //Natural: ASSIGN #EVENT-YEAR := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rider_Free_Amt().setValue(" ");                                                                                                     //Natural: ASSIGN #RIDER-FREE-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Financial_Act_Id().setValue(" ");                                                                                                   //Natural: ASSIGN #FINANCIAL-ACT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Applies_To_Overage_Id().setValue(" ");                                                                                              //Natural: ASSIGN #APPLIES-TO-OVERAGE-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Finact_Type().setValue(" ");                                                                                                        //Natural: ASSIGN #FINACT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Sub_Type().setValue(" ");                                                                                                   //Natural: ASSIGN #FIN-ACT-SUB-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Reversal_Ind().setValue(" ");                                                                                                       //Natural: ASSIGN #REVERSAL-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Gross_Amt().setValue(" ");                                                                                                  //Natural: ASSIGN #FIN-ACT-GROSS-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Acc_Value_Last_Ann().setValue(" ");                                                                                                 //Natural: ASSIGN #ACC-VALUE-LAST-ANN := ' '
        ldaIaalimra.getPnd_Output_Pnd_Sub_Acct_Id().setValue(" ");                                                                                                        //Natural: ASSIGN #SUB-ACCT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Asset_Class().setValue(" ");                                                                                                        //Natural: ASSIGN #ASSET-CLASS := ' '
        ldaIaalimra.getPnd_Output_Pnd_Tot_Value().setValue(" ");                                                                                                          //Natural: ASSIGN #TOT-VALUE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Base_Rate().setValue(" ");                                                                                                          //Natural: ASSIGN #BASE-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dur_Qualifier().setValue(" ");                                                                                                      //Natural: ASSIGN #DUR-QUALIFIER := ' '
        ldaIaalimra.getPnd_Output_Pnd_Guarantee_Duration().setValue(" ");                                                                                                 //Natural: ASSIGN #GUARANTEE-DURATION := ' '
        ldaIaalimra.getPnd_Output_Pnd_Cap_Rate().setValue(" ");                                                                                                           //Natural: ASSIGN #CAP-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Part_Rate().setValue(" ");                                                                                                          //Natural: ASSIGN #PART-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Market_Val_Adjust_Ind().setValue(" ");                                                                                              //Natural: ASSIGN #MARKET-VAL-ADJUST-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Bonus_Rate().setValue(" ");                                                                                                         //Natural: ASSIGN #BONUS-RATE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Acct_Value_Last_Ann().setValue(" ");                                                                                                //Natural: ASSIGN #ACCT-VALUE-LAST-ANN := ' '
        ldaIaalimra.getPnd_Output_Pnd_Financial_Actid().setValue(" ");                                                                                                    //Natural: ASSIGN #FINANCIAL-ACTID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Applies_To_Coverage_Id().setValue(" ");                                                                                             //Natural: ASSIGN #APPLIES-TO-COVERAGE-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Type().setValue(" ");                                                                                                       //Natural: ASSIGN #FIN-ACT-TYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Act_Subtype().setValue(" ");                                                                                                    //Natural: ASSIGN #FIN-ACT-SUBTYPE := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rev_Ind().setValue(" ");                                                                                                            //Natural: ASSIGN #REV-IND := ' '
        ldaIaalimra.getPnd_Output_Pnd_Fin_Activity_Gross_Amt().setValue(0);                                                                                               //Natural: ASSIGN #FIN-ACTIVITY-GROSS-AMT := 0000000.00
        ldaIaalimra.getPnd_Output_Pnd_Arrangement_Id().setValue(" ");                                                                                                     //Natural: ASSIGN #ARRANGEMENT-ID := ' '
        ldaIaalimra.getPnd_Output_Pnd_Arr_Type().setValue("2");                                                                                                           //Natural: ASSIGN #ARR-TYPE := '2'
        ldaIaalimra.getPnd_Output_Pnd_Modal_Amt().setValue(" ");                                                                                                          //Natural: ASSIGN #MODAL-AMT := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id1().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde1().setValue(" ");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Key1().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State1().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country1().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip1().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Nipr1().setValue(" ");                                                                                                              //Natural: ASSIGN #NIPR1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id1().setValue(" ");                                                                                                        //Natural: ASSIGN #CO-PROD-ID1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat1().setValue(" ");                                                                                                 //Natural: ASSIGN #CARRIER-APPT-STAT1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel1().setValue(" ");                                                                                                      //Natural: ASSIGN #DIST-CHANNEL1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type1().setValue(" ");                                                                                             //Natural: ASSIGN #DIST-CHANNEL-SUB-TYPE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name1().setValue(" ");                                                                                                 //Natural: ASSIGN #DIST-CHANNEL-NAME1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde1().setValue(" ");                                                                                                  //Natural: ASSIGN #DIST-CHANNEL-CDE1 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id2().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde2().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE2 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key2().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State2().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country2().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip2().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Nipr2().setValue(" ");                                                                                                              //Natural: ASSIGN #NIPR2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Co_Prod_Id2().setValue(" ");                                                                                                        //Natural: ASSIGN #CO-PROD-ID2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Carrier_Appt_Stat2().setValue(" ");                                                                                                 //Natural: ASSIGN #CARRIER-APPT-STAT2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel2().setValue(" ");                                                                                                      //Natural: ASSIGN #DIST-CHANNEL2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Sub_Type2().setValue(" ");                                                                                             //Natural: ASSIGN #DIST-CHANNEL-SUB-TYPE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Name2().setValue(" ");                                                                                                 //Natural: ASSIGN #DIST-CHANNEL-NAME2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Dist_Channel_Cde2().setValue(" ");                                                                                                  //Natural: ASSIGN #DIST-CHANNEL-CDE2 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Id3().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID3 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde3().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE3 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key3().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY3 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State3().setValue(pnd_Mdm_Vallues_Pnd_Ws_St_Prov);                                                                              //Natural: ASSIGN #RES-STATE3 := #WS-ST-PROV
        ldaIaalimra.getPnd_Output_Pnd_Res_Country3().setValue(pnd_Mdm_Vallues_Pnd_Ws_Country);                                                                            //Natural: ASSIGN #RES-COUNTRY3 := #WS-COUNTRY
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip3().setValue(pnd_Mdm_Vallues_Pnd_Ws_Zip_Code);                                                                               //Natural: ASSIGN #RES-ZIP3 := #WS-ZIP-CODE
        ldaIaalimra.getPnd_Output_Pnd_Party_Id4().setValue(" ");                                                                                                          //Natural: ASSIGN #PARTY-ID4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde4().setValue(" ");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Party_Key4().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State4().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country4().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip4().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP4 := ' '
        if (condition(pnd_Ws_Lives_Type.equals("10")))                                                                                                                    //Natural: IF #WS-LIVES-TYPE = '10'
        {
            ldaIaalimra.getPnd_Output_Pnd_Res_State4().setValue(pnd_Mdm_Vallues_Pnd_Ws_St_Prov);                                                                          //Natural: ASSIGN #RES-STATE4 := #WS-ST-PROV
            ldaIaalimra.getPnd_Output_Pnd_Res_Country4().setValue(pnd_Mdm_Vallues_Pnd_Ws_Country);                                                                        //Natural: ASSIGN #RES-COUNTRY4 := #WS-COUNTRY
            ldaIaalimra.getPnd_Output_Pnd_Res_Zip4().setValue(pnd_Mdm_Vallues_Pnd_Ws_Zip_Code);                                                                           //Natural: ASSIGN #RES-ZIP4 := #WS-ZIP-CODE
            //*  REQUIRED
            //*  REQUIRED $$$
            //*  NOT IN THE LDA
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde8().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE8 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key8().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY8 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Full_Nme8().setValue(" ");                                                                                                          //Natural: ASSIGN #FULL-NME8 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State8().setValue(pnd_Mdm_Vallues_Pnd_Ws_St_Prov);                                                                              //Natural: ASSIGN #RES-STATE8 := #WS-ST-PROV
        ldaIaalimra.getPnd_Output_Pnd_Res_Country8().setValue(pnd_Mdm_Vallues_Pnd_Ws_Country);                                                                            //Natural: ASSIGN #RES-COUNTRY8 := #WS-COUNTRY
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip8().setValue(pnd_Mdm_Vallues_Pnd_Ws_Zip_Code);                                                                               //Natural: ASSIGN #RES-ZIP8 := #WS-ZIP-CODE
        ldaIaalimra.getPnd_Output_Pnd_Party_Type_Cde9().setValue("1");                                                                                                    //Natural: ASSIGN #PARTY-TYPE-CDE9 := '1'
        ldaIaalimra.getPnd_Output_Pnd_Party_Key9().setValue(" ");                                                                                                         //Natural: ASSIGN #PARTY-KEY9 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Full_Nme9().setValue(" ");                                                                                                          //Natural: ASSIGN #FULL-NME9 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_State9().setValue(" ");                                                                                                         //Natural: ASSIGN #RES-STATE9 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Country9().setValue(" ");                                                                                                       //Natural: ASSIGN #RES-COUNTRY9 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Res_Zip9().setValue(" ");                                                                                                           //Natural: ASSIGN #RES-ZIP9 := ' '
        if (condition(pnd_Ws_Lives_Type.equals("10")))                                                                                                                    //Natural: IF #WS-LIVES-TYPE = '10'
        {
            ldaIaalimra.getPnd_Output_Pnd_Res_State9().setValue(pnd_Mdm_Vallues_Pnd_Ws_St_Prov);                                                                          //Natural: ASSIGN #RES-STATE9 := #WS-ST-PROV
            ldaIaalimra.getPnd_Output_Pnd_Res_Country9().setValue(pnd_Mdm_Vallues_Pnd_Ws_Country);                                                                        //Natural: ASSIGN #RES-COUNTRY9 := #WS-COUNTRY
            ldaIaalimra.getPnd_Output_Pnd_Res_Zip9().setValue(pnd_Mdm_Vallues_Pnd_Ws_Zip_Code);                                                                           //Natural: ASSIGN #RES-ZIP9 := #WS-ZIP-CODE
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id3().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID3 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id3().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID3 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id3().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID3 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type3().setValue("4");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE3 := '4'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type3().setValue("6");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE3 := '6'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code3().setValue("35");                                                                                                    //Natural: ASSIGN #REL-ROLE-CODE3 := '35'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id4().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id4().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id4().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type4().setValue(" ");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type4().setValue(" ");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE4 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code4().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE4 := ' '
        //*  OPTIONAL
        //*  OPTIONAL
        //*  REQUIRED
        if (condition(pnd_Ws_Lives_Type.equals("10")))                                                                                                                    //Natural: IF #WS-LIVES-TYPE = '10'
        {
            ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type4().setValue("4");                                                                                              //Natural: ASSIGN #ORIG-OBJECT-TYPE4 := '4'
            ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type4().setValue("6");                                                                                               //Natural: ASSIGN #REL-OBJECT-TYPE4 := '6'
            ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code4().setValue("35");                                                                                                //Natural: ASSIGN #REL-ROLE-CODE4 := '35'
            //*  REQUIRED
            //*  REQUIRED
            //*  OPTIONAL
            //*  OPTIONAL
            //*  REQUIRED
            //*  REQUIRED
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id5().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id5().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id5().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type5().setValue(" ");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type5().setValue(" ");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code5().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE5 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id6().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id6().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id6().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID6 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type6().setValue("4");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE6 := '4'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type6().setValue("6");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE6 := '6'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code6().setValue("8");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE6 := '8'
        ldaIaalimra.getPnd_Output_Pnd_Rel_Id7().setValue(" ");                                                                                                            //Natural: ASSIGN #REL-ID7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Id7().setValue(" ");                                                                                                    //Natural: ASSIGN #ORIG-OBJECT-ID7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Id7().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-OBJECT-ID7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Orig_Object_Type7().setValue(" ");                                                                                                  //Natural: ASSIGN #ORIG-OBJECT-TYPE7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Object_Type7().setValue(" ");                                                                                                   //Natural: ASSIGN #REL-OBJECT-TYPE7 := ' '
        ldaIaalimra.getPnd_Output_Pnd_Rel_Role_Code7().setValue(" ");                                                                                                     //Natural: ASSIGN #REL-ROLE-CODE7 := ' '
    }
    private void sub_Get_Term_Dte() throws Exception                                                                                                                      //Natural: GET-TERM-DTE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #DC-PPCN-NBR := #PPCN-NBR
        pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde.setValue(pnd_Input_Pnd_Payee_Cde);                                                                                             //Natural: ASSIGN #DC-PAYEE-CDE := #PAYEE-CDE
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) TRANS BY TRANS-CNTRCT-KEY STARTING FROM #DC-CNTRCT-KEY
        (
        "READ04",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Dc_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_trans.readNextRow("READ04")))
        {
            if (condition(trans_Trans_Ppcn_Nbr.notEquals(pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr) || trans_Trans_Payee_Cde.notEquals(pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde)))      //Natural: IF TRANS.TRANS-PPCN-NBR NE #DC-PPCN-NBR OR TRANS.TRANS-PAYEE-CDE NE #DC-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Term_Dte.setValueEdited(trans_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED TRANS.TRANS-DTE ( EM = YYYYMMDD ) TO #TERM-DTE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        aian026_Pnd_Ia_Fund_Code.reset();                                                                                                                                 //Natural: RESET #IA-FUND-CODE
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(pnd_Input_Pnd_Summ_Fund_Cde.val())))                                                                    //Natural: IF +FUND-NUM-CDE ( #I ) = VAL ( #SUMM-FUND-CDE )
            {
                aian026_Pnd_Ia_Fund_Code.setValue(pls_Fund_Alpha_Cde.getValue(pnd_I));                                                                                    //Natural: ASSIGN #IA-FUND-CODE := +FUND-ALPHA-CDE ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(aian026_Pnd_Ia_Fund_Code.equals(" ")))                                                                                                              //Natural: IF #IA-FUND-CODE = ' '
        {
            getReports().write(0, "ERROR: INVALID FUND CODE FOR:",pnd_S_Ppcn_Nbr,pnd_S_Payee_Cde_A);                                                                      //Natural: WRITE 'ERROR: INVALID FUND CODE FOR:' #S-PPCN-NBR #S-PAYEE-CDE-A
            if (Global.isEscape()) return;
            DbsUtil.terminate(77);  if (true) return;                                                                                                                     //Natural: TERMINATE 77
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.equals(getZero())))                                                                                                             //Natural: IF #RTRN-CD = 0
        {
            pnd_Input_Pnd_Summ_Per_Pymnt.compute(new ComputeParameters(true, pnd_Input_Pnd_Summ_Per_Pymnt), pnd_Input_Pnd_Summ_Units.multiply(aian026_Pnd_Iuv));          //Natural: COMPUTE ROUNDED #SUMM-PER-PYMNT = #SUMM-UNITS * #IUV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR IN CNTRCT:",pnd_S_Ppcn_Nbr,pnd_S_Payee_Cde_A,"No unit value for fund:",aian026_Pnd_Ia_Fund_Code);                                //Natural: WRITE 'ERROR IN CNTRCT:' #S-PPCN-NBR #S-PAYEE-CDE-A 'No unit value for fund:' #IA-FUND-CODE
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Trans_Dte() throws Exception                                                                                                                     //Natural: GET-TRANS-DTE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #DC-PPCN-NBR := #PPCN-NBR
        pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde.setValue(pnd_Input_Pnd_Payee_Cde);                                                                                             //Natural: ASSIGN #DC-PAYEE-CDE := #PAYEE-CDE
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CNTRCT-KEY STARTING FROM #DC-CNTRCT-KEY
        (
        "READ05",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Dc_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ05:
        while (condition(vw_trans.readNextRow("READ05")))
        {
            if (condition(trans_Trans_Ppcn_Nbr.notEquals(pnd_Dc_Cntrct_Key_Pnd_Dc_Ppcn_Nbr) || trans_Trans_Payee_Cde.notEquals(pnd_Dc_Cntrct_Key_Pnd_Dc_Payee_Cde)))      //Natural: IF TRANS.TRANS-PPCN-NBR NE #DC-PPCN-NBR OR TRANS.TRANS-PAYEE-CDE NE #DC-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(trans_Trans_Cde.equals(33)))                                                                                                                    //Natural: IF TRANS.TRANS-CDE = 033
            {
                pnd_Trans_Dte.setValueEdited(trans_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED TRANS.TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DTE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Guaranteed_Period() throws Exception                                                                                                             //Natural: GET-GUARANTEED-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Guar_Prd.reset();                                                                                                                                             //Natural: RESET #GUAR-PRD
        pnd_W_Iss_Dte.setValue(pnd_S_Issue_Dte);                                                                                                                          //Natural: ASSIGN #W-ISS-DTE := #S-ISSUE-DTE
        pnd_W_Final_Dte.setValue(pnd_S_Cntrct_Fin_Per_Pay_Dte);                                                                                                           //Natural: ASSIGN #W-FINAL-DTE := #S-CNTRCT-FIN-PER-PAY-DTE
        if (condition(pnd_W_Iss_Dte_Pnd_W_Iss_Mm.greater(pnd_W_Final_Dte_Pnd_W_Final_Mm)))                                                                                //Natural: IF #W-ISS-MM GT #W-FINAL-MM
        {
            pnd_W_Final_Dte_Pnd_W_Final_Mm.nadd(12);                                                                                                                      //Natural: ADD 12 TO #W-FINAL-MM
            pnd_W_Final_Dte_Pnd_W_Final_Yyyy.nsubtract(1);                                                                                                                //Natural: SUBTRACT 1 FROM #W-FINAL-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Issue_Yr.compute(new ComputeParameters(false, pnd_W_Issue_Yr), pnd_W_Final_Dte_Pnd_W_Final_Yyyy.subtract(pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy));                    //Natural: ASSIGN #W-ISSUE-YR := #W-FINAL-YYYY - #W-ISS-YYYY
        pnd_W_Issue_Mo.compute(new ComputeParameters(false, pnd_W_Issue_Mo), pnd_W_Final_Dte_Pnd_W_Final_Mm.subtract(pnd_W_Iss_Dte_Pnd_W_Iss_Mm));                        //Natural: ASSIGN #W-ISSUE-MO := #W-FINAL-MM - #W-ISS-MM
        short decideConditionsMet2922 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #S-MODE-IND;//Natural: VALUE 100
        if (condition((pnd_S_Mode_Ind.equals(100))))
        {
            decideConditionsMet2922++;
            pnd_W_Issue_Mo.nadd(1);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 1
        }                                                                                                                                                                 //Natural: VALUE 600:699
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(600) && pnd_S_Mode_Ind.lessOrEqual(699)))))
        {
            decideConditionsMet2922++;
            pnd_W_Issue_Mo.nadd(3);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 3
        }                                                                                                                                                                 //Natural: VALUE 700:799
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(700) && pnd_S_Mode_Ind.lessOrEqual(799)))))
        {
            decideConditionsMet2922++;
            pnd_W_Issue_Mo.nadd(6);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 6
        }                                                                                                                                                                 //Natural: VALUE 800:899
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(800) && pnd_S_Mode_Ind.lessOrEqual(899)))))
        {
            decideConditionsMet2922++;
            pnd_W_Issue_Yr.nadd(1);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-YR := #W-ISSUE-YR + 1
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet2922 > 0))
        {
            if (condition(pnd_W_Issue_Mo.greaterOrEqual(12)))                                                                                                             //Natural: IF #W-ISSUE-MO GE 12
            {
                pnd_W_Issue_Mo.nsubtract(12);                                                                                                                             //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO - 12
                pnd_W_Issue_Yr.nadd(1);                                                                                                                                   //Natural: ASSIGN #W-ISSUE-YR := #W-ISSUE-YR + 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Guar_Prd.compute(new ComputeParameters(false, pnd_Guar_Prd), pnd_W_Issue_Yr.multiply(12).add(pnd_W_Issue_Mo));                                            //Natural: ASSIGN #GUAR-PRD := #W-ISSUE-YR * 12 + #W-ISSUE-MO
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Translate_Issue_State() throws Exception                                                                                                             //Natural: TRANSLATE-ISSUE-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Function_Cde().setValue("STT");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'STT'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Stt_Key_State_Cde().setValue(pnd_Input_Pnd_Rsdncy_At_Issue);                                                                              //Natural: ASSIGN NECA4000.STT-KEY-STATE-CDE := #RSDNCY-AT-ISSUE
        if (condition(pnd_Input_Pnd_Rsdncy_At_Issue.equals("000")))                                                                                                       //Natural: IF #RSDNCY-AT-ISSUE = '000'
        {
            pnd_Issue_St.setValue("Unknown");                                                                                                                             //Natural: ASSIGN #ISSUE-ST := 'Unknown'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                                                                                 //Natural: IF RETURN-CDE = '00'
        {
            pnd_Issue_St.setValue(pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1));                                                                              //Natural: ASSIGN #ISSUE-ST := NECA4000.STT-ABBR-STATE-CDE ( 1 )
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().setValue(pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1));                            //Natural: ASSIGN NAZ-ACT-STATE-AT-ISSUE := NECA4000.STT-ABBR-STATE-CDE ( 1 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Input_Iss_St, pnd_Issue_St);                      //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL ISS-ST #ISSUE-ST
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Issue_St.equals(" ")))                                                                                                                          //Natural: IF #ISSUE-ST = ' '
        {
            pnd_Issue_St.setValue("Unknown");                                                                                                                             //Natural: ASSIGN #ISSUE-ST := 'Unknown'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Translate_Res_State() throws Exception                                                                                                               //Natural: TRANSLATE-RES-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Function_Cde().setValue("STT");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'STT'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Stt_Key_State_Cde().setValue(pnd_Input_Pnd_Rsdncy_Cde);                                                                                   //Natural: ASSIGN NECA4000.STT-KEY-STATE-CDE := #RSDNCY-CDE
        if (condition(pnd_Input_Pnd_Rsdncy_Cde.equals("000")))                                                                                                            //Natural: IF #RSDNCY-CDE = '000'
        {
            pnd_Res_St.setValue("Unknown");                                                                                                                               //Natural: ASSIGN #RES-ST := 'Unknown'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                                                                                 //Natural: IF RETURN-CDE = '00'
        {
            pnd_Res_St.setValue(pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1));                                                                                //Natural: ASSIGN #RES-ST := NECA4000.STT-ABBR-STATE-CDE ( 1 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Input_Pnd_Res_Cde, pnd_Res_St);                   //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL #RES-CDE #RES-ST
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Res_St.equals(" ")))                                                                                                                            //Natural: IF #RES-ST = ' '
        {
            pnd_Res_St.setValue("Unknown");                                                                                                                               //Natural: ASSIGN #RES-ST := 'Unknown'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Start_End_Dates() throws Exception                                                                                                               //Natural: SET-START-END-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_End_Dte.compute(new ComputeParameters(false, pnd_End_Dte), pnd_W_Dte_A.val());                                                                                //Natural: ASSIGN #END-DTE := VAL ( #W-DTE-A )
        getReports().write(0, "=",pnd_End_Dte,"=",pnd_W_Dte_A);                                                                                                           //Natural: WRITE '=' #END-DTE '=' #W-DTE-A
        if (Global.isEscape()) return;
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_W_Dte_A_Pnd_W_Dte_M.equals(3) || pnd_W_Dte_A_Pnd_W_Dte_M.equals(6) || pnd_W_Dte_A_Pnd_W_Dte_M.equals(9) || pnd_W_Dte_A_Pnd_W_Dte_M.equals(12))) //Natural: IF #W-DTE-M = 03 OR = 06 OR = 09 OR = 12
            {
                setValueToSubstring(pnd_W_Dte_A,pnd_End_Dte_Pnd_End_Dte_A,1,6);                                                                                           //Natural: MOVE #W-DTE-A TO SUBSTR ( #END-DTE-A,1,6 )
                setValueToSubstring(pnd_W_Dte_A,pnd_Start_Dte_Pnd_Start_Dte_A,1,6);                                                                                       //Natural: MOVE #W-DTE-A TO SUBSTR ( #START-DTE-A,1,6 )
                setValueToSubstring("01",pnd_Start_Dte_Pnd_Start_Dte_A,7,2);                                                                                              //Natural: MOVE '01' TO SUBSTR ( #START-DTE-A,7,2 )
                pnd_Start_Dte_Pnd_Start_Dte_M.nsubtract(2);                                                                                                               //Natural: SUBTRACT 2 FROM #START-DTE-M
                if (condition(pnd_W_Dte_A_Pnd_W_Dte_M.equals(3) || pnd_W_Dte_A_Pnd_W_Dte_M.equals(12)))                                                                   //Natural: IF #W-DTE-M = 03 OR = 12
                {
                    setValueToSubstring("31",pnd_End_Dte_Pnd_End_Dte_A,7,2);                                                                                              //Natural: MOVE '31' TO SUBSTR ( #END-DTE-A,7,2 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Dte_A_Pnd_W_Dte_M.equals(6) || pnd_W_Dte_A_Pnd_W_Dte_M.equals(9)))                                                                    //Natural: IF #W-DTE-M = 06 OR = 09
                {
                    setValueToSubstring("30",pnd_End_Dte_Pnd_End_Dte_A,7,2);                                                                                              //Natural: MOVE '30' TO SUBSTR ( #END-DTE-A,7,2 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Dte_A_Pnd_W_Dte_M.equals(1)))                                                                                                             //Natural: IF #W-DTE-M = 01
            {
                pnd_W_Dte_A_Pnd_W_Dte_M.setValue(12);                                                                                                                     //Natural: ASSIGN #W-DTE-M := 12
                pnd_W_Dte_A_Pnd_W_Dte_Y.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #W-DTE-Y
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Dte_A_Pnd_W_Dte_M.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #W-DTE-M
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Start_Dte,"=",pnd_End_Dte_Pnd_End_Dte_A);                                                                                           //Natural: WRITE '=' #START-DTE '='#END-DTE-A
        if (Global.isEscape()) return;
    }
    private void sub_Populate_Output() throws Exception                                                                                                                   //Natural: POPULATE-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaalimra.getPnd_Output().reset();                                                                                                                              //Natural: RESET #OUTPUT
                                                                                                                                                                          //Natural: PERFORM RESET-INIT
        sub_Reset_Init();
        if (condition(Global.isEscape())) {return;}
        //*  ACCUM AND PERIODIC PAYMENT
    }
    private void sub_Reset_Init() throws Exception                                                                                                                        //Natural: RESET-INIT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  TXLIFE
        ldaIaalimra.getPnd_Output_Pnd_Trans_Type().resetInitial();                                                                                                        //Natural: RESET INITIAL #TRANS-TYPE #TRANS-SUBTYPE #TRANS-EXE-TME #TRANS-MODE #INQ-LEVEL #PEND-RESPONSE #NO-RESPONSE #TEST-IND #PRIMARY-OBJECT-TYP
        ldaIaalimra.getPnd_Output_Pnd_Trans_Subtype().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Trans_Exe_Tme().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Trans_Mode().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Inq_Level().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Pend_Response().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_No_Response().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Test_Ind().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Primary_Object_Typ().resetInitial();
        ldaIaalimra.getPnd_Output_Pnd_Holding_Type_Cde().resetInitial();                                                                                                  //Natural: RESET INITIAL #HOLDING-TYPE-CDE
        ldaIaalimra.getPnd_Output_Pnd_Lob().resetInitial();                                                                                                               //Natural: RESET INITIAL #LOB #ISSUE-NATION
        ldaIaalimra.getPnd_Output_Pnd_Issue_Nation().resetInitial();
        //*  RESET DELIMITERS
    }
    private void sub_Write_Header() throws Exception                                                                                                                      //Natural: WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RESET #OUTPUT
        //*  PERFORM RESET-INIT
        //*  #START-DTE AND #END-DTE
        //*  10
        //*  20
        //*  30
        //*  40
        //*  50
        //*  60
        //*  70
        //*  80
        //*  90
        //*  100
        //*  110
        //*  120
        //*  11/14 RENAMED FROM PARTKEY1
        //*  130
        //*  11/14 RENAMED FROM PARTKEY3
        //*  140
        //*  11/14 RENAMED FROM PARTKEY4
        //*  150
        //*  11/14 RENAMED FROM PARTKEY4
        //*  11/14 RENAMED FROM PARTKEY6
        //*  160
        //*  11/14 RENAMED FROM PARTKEY7
        //*  170
        //* ******
        //* ************
        //*  180
        //*  190
        //*  200
        //*  210
        //*  220
        //*  230
        //*  NEW  11/14
        //*  NEW  11/14
        //*  NEW  11/14
        //*  NEW  082018
        //*  NEW  082018
        getWorkFiles().write(2, true, "XMLID,", "POBJID,", "TREFGUID,", "TRANTYP,", "TRANSTYP,", "SRTENDEXDTE,", "EXTME,", "TRANSMDE,", "INQLVL,", "PENDRESP,",           //Natural: WRITE WORK FILE 2 VARIABLE 'XMLID,' 'POBJID,' 'TREFGUID,' 'TRANTYP,' 'TRANSTYP,' 'SRTENDEXDTE,' 'EXTME,' 'TRANSMDE,' 'INQLVL,' 'PENDRESP,' 'NORESP,' 'TESTIND,' 'POBJTYPE,' 'CREATEDTE,' 'CREATETME,' 'INFONME,' 'INFODESC,' 'INFOCMT,' 'CTRLID,' 'HOLDID,' 'HOLDTYP,' 'HOLDSTAT,' 'ASOFDTE,' 'SHARECLS,' 'POLICY,' 'CERTNBR,' 'LOB,' 'PRODTYPE,' 'PRODCDE,' 'CARRIER,' 'PLANME,' 'POLICYSTAT,' 'ISSUENAT,' 'ISSUETYP,' 'JRSDCTN,' 'RPLCMNTYP,' 'PLCYVAL,' 'EFFDTE,' 'ISSUEDTE,' 'STAT-CHDTE,' 'PRPOLNBR,' 'PYOUTYPE,' 'QPLNTYP,' 'QPLNSTYP,' 'CMWDAMT,' 'DTHBENAMT,' 'SURRVAL,' 'GRTRATE,' 'GPREMAMT,' 'CMPREMAMT,' 'PYOUTID,' 'INCOMOPT,' 'PYOUTAMT,' 'PYOUTMDE,' 'STRDTE,' 'ANNSTRDTE,' 'LIVESTYPE,' 'ANNINDXTYP,' 'PCERTMDE,' 'PCERTPRD,' 'PYOUTCHID,' 'CHMODE,' 'PYOUTCHAMT,' 'AMTQUAL,' 'PYOUTPCTYP,' 'DESC,' 'OUTSTDTE,' 'OUTENDTE,' 'NXTACTDTE,' 'TMNGBS,' 'PYOUTPCT,' 'RDID,' 'RDTYPCDE,' 'RDTYPSCDE,' 'RDCDE,' 'EFFDTE,' 'TERMDTE,' 'TOTAMT,' 'TOTAMTLST,' 'LIVESTYP,' 'BENPCT,' 'EVNTTYP,' 'EVENTYR,' 'RDFREEAMT,' 'FINACTID,' 'OVERAGEID,' 'FINACTYP,' 'FINACTSTYP,' 'REVIND,' 'FINACTGAMT,' 'ACVALAST,' 'SACCTID,' 'ASSETCLS,' 'TOTVAL,' 'BASERTE,' 'DURQUAL,' 'GUARDUR,' 'CAPRATE,' 'PARTRATE,' 'MKTVALADIND,' 'BNSRTE,' 'ACTVALAST,' 'FINACTID,' 'APPCOVID,' 'FINACTYP,' 'FINACTSTYP,' 'REVIND,' 'FINACTGAMT,' 'ARRMNTID,' 'ARRTYP,' 'MODALAMT,' 'PARTID1,' 'PARTYPECDE1,' 'PARTKEY1,' 'RESTATE1,' 'RESCNTRY1,' 'RESZIP1,' 'NIRP1,' 'COPRODID1,' 'CARAPPSTAT1,' 'DSTCHNL,' 'DSTCHNLSTYP,' 'DSTCHNLNME,' 'DSTCHNLCDE,' 'PARTID2,' 'PARTYPECDE2,' 'PARTKEY2,' 'RESTATE2,' 'RESCNTRY2,' 'RESZIP2,' 'NPIR2,' 'COPRODID2,' 'CARAPPSTAT2,' 'DSTCHNL,' 'DSTCHNLSTYP,' 'DSTCHNLNME,' 'DSTCHNLCDE,' 'PARTID3,' 'PARTYPECDE3,' 'PARTKEY3,' 'RESTATE3,' 'RESCNTRY3,' 'RESZIP3,' 'PARTID4,' 'PARTYPECDE4,' 'PARTKEY4,' 'RESTATE4,' 'RESCNTRY4,' 'RESZIP4,' 'PARTID5,' 'PARTYPECDE5,' 'PARTKEY5,' 'RESTATE5,' 'RESCNTRY5,' 'RESZIP5,' 'PARTID6,' 'PARTYPECDE6,' 'PARTKEY6,' 'FULLNME6,' 'GOVID6,' 'GOVIDTC6,' 'RESTATE6,' 'RESCNTRY6,' 'RESZIP6,' 'PARTID7,' 'PARTYPECDE7,' 'PARTKEY7,' 'FULLNME7,' 'GOVID7,' 'GOVIDTC7,' 'RESTATE7,' 'RESCNTRY7,' 'RESZIP7,' 'PARTKEY8,' 'FULLNME8,' 'PARTYPECDE8,' 'RESTATE8,' 'RESCNTRY8,' 'RESZIP8,' 'PARTYPECDE9,' 'PARTKEY9,' 'FULLNME9,' 'RESTATE9,' 'RESCNTRY9,' 'RESZIP9,' 'RELID1,' 'ORIGOBJID1,' 'RELOBJID1,' 'ORIGOBJTYP1,' 'RELOBJTYP1,' 'RELROLECDE1,' 'INTPCT1,' 'RELID2,' 'ORIGOBJID2,' 'RELOBJID2,' 'ORIGOBJTYP2,' 'RELOBJTYP2,' 'RELROLECDE2,' 'INTPCT2,' 'RELID3,' 'ORIGOBJID3,' 'RELOBJID3,' 'ORIGOBJTYP3,' 'RELOBJTYP3,' 'RELROLECDE3,' 'RELID4,' 'ORIGOBJID4,' 'RELOBJID4,' 'ORIGOBJTYP4,' 'RELOBJTYP4,' 'RELROLECDE4,' 'RELID5,' 'ORIGOBJID5,' 'RELOBJID5,' 'ORIGOBJTYP5,' 'RELOBJTYP5,' 'RELROLECDE5,' 'RELID6,' 'ORIGOBJID6,' 'RELOBJID6,' 'ORIGOBJTYP6,' 'RELOBJTYP6,' 'RELROLECDE6,' 'RELID7,' 'ORIGOBJID7,' 'RELOBJID7,' 'ORIGOBJTYP7,' 'RELOBJTYP7,' 'RELROLECDE7,' 'OWNERDOB,' 'JTOWNDOB,' 'TRNTYPE,' 'TRNKEY,' 'PREMIUM,'
            "NORESP,", "TESTIND,", "POBJTYPE,", "CREATEDTE,", "CREATETME,", "INFONME,", "INFODESC,", "INFOCMT,", "CTRLID,", "HOLDID,", "HOLDTYP,", "HOLDSTAT,", 
            "ASOFDTE,", "SHARECLS,", "POLICY,", "CERTNBR,", "LOB,", "PRODTYPE,", "PRODCDE,", "CARRIER,", "PLANME,", "POLICYSTAT,", "ISSUENAT,", "ISSUETYP,", 
            "JRSDCTN,", "RPLCMNTYP,", "PLCYVAL,", "EFFDTE,", "ISSUEDTE,", "STAT-CHDTE,", "PRPOLNBR,", "PYOUTYPE,", "QPLNTYP,", "QPLNSTYP,", "CMWDAMT,", 
            "DTHBENAMT,", "SURRVAL,", "GRTRATE,", "GPREMAMT,", "CMPREMAMT,", "PYOUTID,", "INCOMOPT,", "PYOUTAMT,", "PYOUTMDE,", "STRDTE,", "ANNSTRDTE,", 
            "LIVESTYPE,", "ANNINDXTYP,", "PCERTMDE,", "PCERTPRD,", "PYOUTCHID,", "CHMODE,", "PYOUTCHAMT,", "AMTQUAL,", "PYOUTPCTYP,", "DESC,", "OUTSTDTE,", 
            "OUTENDTE,", "NXTACTDTE,", "TMNGBS,", "PYOUTPCT,", "RDID,", "RDTYPCDE,", "RDTYPSCDE,", "RDCDE,", "EFFDTE,", "TERMDTE,", "TOTAMT,", "TOTAMTLST,", 
            "LIVESTYP,", "BENPCT,", "EVNTTYP,", "EVENTYR,", "RDFREEAMT,", "FINACTID,", "OVERAGEID,", "FINACTYP,", "FINACTSTYP,", "REVIND,", "FINACTGAMT,", 
            "ACVALAST,", "SACCTID,", "ASSETCLS,", "TOTVAL,", "BASERTE,", "DURQUAL,", "GUARDUR,", "CAPRATE,", "PARTRATE,", "MKTVALADIND,", "BNSRTE,", "ACTVALAST,", 
            "FINACTID,", "APPCOVID,", "FINACTYP,", "FINACTSTYP,", "REVIND,", "FINACTGAMT,", "ARRMNTID,", "ARRTYP,", "MODALAMT,", "PARTID1,", "PARTYPECDE1,", 
            "PARTKEY1,", "RESTATE1,", "RESCNTRY1,", "RESZIP1,", "NIRP1,", "COPRODID1,", "CARAPPSTAT1,", "DSTCHNL,", "DSTCHNLSTYP,", "DSTCHNLNME,", "DSTCHNLCDE,", 
            "PARTID2,", "PARTYPECDE2,", "PARTKEY2,", "RESTATE2,", "RESCNTRY2,", "RESZIP2,", "NPIR2,", "COPRODID2,", "CARAPPSTAT2,", "DSTCHNL,", "DSTCHNLSTYP,", 
            "DSTCHNLNME,", "DSTCHNLCDE,", "PARTID3,", "PARTYPECDE3,", "PARTKEY3,", "RESTATE3,", "RESCNTRY3,", "RESZIP3,", "PARTID4,", "PARTYPECDE4,", "PARTKEY4,", 
            "RESTATE4,", "RESCNTRY4,", "RESZIP4,", "PARTID5,", "PARTYPECDE5,", "PARTKEY5,", "RESTATE5,", "RESCNTRY5,", "RESZIP5,", "PARTID6,", "PARTYPECDE6,", 
            "PARTKEY6,", "FULLNME6,", "GOVID6,", "GOVIDTC6,", "RESTATE6,", "RESCNTRY6,", "RESZIP6,", "PARTID7,", "PARTYPECDE7,", "PARTKEY7,", "FULLNME7,", 
            "GOVID7,", "GOVIDTC7,", "RESTATE7,", "RESCNTRY7,", "RESZIP7,", "PARTKEY8,", "FULLNME8,", "PARTYPECDE8,", "RESTATE8,", "RESCNTRY8,", "RESZIP8,", 
            "PARTYPECDE9,", "PARTKEY9,", "FULLNME9,", "RESTATE9,", "RESCNTRY9,", "RESZIP9,", "RELID1,", "ORIGOBJID1,", "RELOBJID1,", "ORIGOBJTYP1,", "RELOBJTYP1,", 
            "RELROLECDE1,", "INTPCT1,", "RELID2,", "ORIGOBJID2,", "RELOBJID2,", "ORIGOBJTYP2,", "RELOBJTYP2,", "RELROLECDE2,", "INTPCT2,", "RELID3,", "ORIGOBJID3,", 
            "RELOBJID3,", "ORIGOBJTYP3,", "RELOBJTYP3,", "RELROLECDE3,", "RELID4,", "ORIGOBJID4,", "RELOBJID4,", "ORIGOBJTYP4,", "RELOBJTYP4,", "RELROLECDE4,", 
            "RELID5,", "ORIGOBJID5,", "RELOBJID5,", "ORIGOBJTYP5,", "RELOBJTYP5,", "RELROLECDE5,", "RELID6,", "ORIGOBJID6,", "RELOBJID6,", "ORIGOBJTYP6,", 
            "RELOBJTYP6,", "RELROLECDE6,", "RELID7,", "ORIGOBJID7,", "RELOBJID7,", "ORIGOBJTYP7,", "RELOBJTYP7,", "RELROLECDE7,", "OWNERDOB,", "JTOWNDOB,", 
            "TRNTYPE,", "TRNKEY,", "PREMIUM,");
    }
    private void sub_Call_Mdmn100() throws Exception                                                                                                                      //Natural: CALL-MDMN100
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        if (condition(pnd_Input_Pnd_Tax_Id_Nbr.notEquals(pnd_Mdm_Vallues_Pnd_Ws_Pnd_Tax_Id_Nbr)))                                                                         //Natural: IF #TAX-ID-NBR NE #WS-#TAX-ID-NBR
        {
            pnd_Mdm_Vallues_Pnd_Ws_Zip_Code.reset();                                                                                                                      //Natural: RESET #WS-ZIP-CODE #WS-ST-PROV #WS-COUNTRY
            pnd_Mdm_Vallues_Pnd_Ws_St_Prov.reset();
            pnd_Mdm_Vallues_Pnd_Ws_Country.reset();
            pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pnd_Input_Pnd_Tax_Id_Nbr);                                                                                     //Natural: ASSIGN #MDMA101.#I-SSN := #TAX-ID-NBR
            //*     CALLNAT 'MDMN100A' #MDMA100 /* 08/15
            //*  08/15
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
            //*     IF #MDMA100.#O-RETURN-CODE NE '0000'
            //*  082017 END
            //*  082017
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #MDMA101.#O-RETURN-CODE NE '0000'
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text());                                                        //Natural: ASSIGN MSG-INFO-SUB.##MSG := #MDMA101.#O-RETURN-TEXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Mdm_Vallues_Pnd_Ws_Zip_Code.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Zip_Code());                                                        //Natural: ASSIGN #WS-ZIP-CODE := #O-BASE-ADDRESS-ZIP-CODE
                pnd_Mdm_Vallues_Pnd_Ws_St_Prov.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_St_Prov());                                                          //Natural: ASSIGN #WS-ST-PROV := #O-BASE-ADDRESS-ST-PROV
                pnd_Mdm_Vallues_Pnd_Ws_Country.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Country());                                                          //Natural: ASSIGN #WS-COUNTRY := #O-BASE-ADDRESS-COUNTRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Lives_Typ_Using_Optn_Code() throws Exception                                                                                                     //Natural: GET-LIVES-TYP-USING-OPTN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        if (condition(pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(1)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(2)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(3))  //Natural: IF #S-OPTN = #SL ( * ) OR = #SLN ( * )
            || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(4)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(5)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sl.getValue(6)) 
            || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sln.getValue(1)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sln.getValue(2)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Sln.getValue(3))))
        {
            pnd_Ws_Lives_Type.setValue("1");                                                                                                                              //Natural: ASSIGN #WS-LIVES-TYPE := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(1)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(2)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(3))  //Natural: IF #S-OPTN = #JL ( * ) OR = #JLN ( * )
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(4)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(5)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(6)) 
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(7)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(8)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(9)) 
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(10)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(11)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(12)) 
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(13)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(14)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jl.getValue(15)) 
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jln.getValue(1)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jln.getValue(2)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jln.getValue(3)) 
                || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jln.getValue(4)) || pnd_S_Optn.equals(pnd_Sng_Jnt_Pnd_Jln.getValue(5))))
            {
                pnd_Ws_Lives_Type.setValue("10");                                                                                                                         //Natural: ASSIGN #WS-LIVES-TYPE := '10'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Lives_Type.setValue("N/A");                                                                                                                        //Natural: ASSIGN #WS-LIVES-TYPE := 'N/A'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Base_Rate() throws Exception                                                                                                                     //Natural: GET-BASE-RATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr().setValue(pnd_S_Ppcn_Nbr);                                                                     //Natural: ASSIGN NAZ-ACT-TIAA-IA-CNTRCT-NMBR := #S-PPCN-NBR
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Out_Issue_Dte);                                     //Natural: MOVE EDITED #WS-OUT-ISSUE-DTE TO NAZ-ACT-ASD-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("1");                                                                                       //Natural: ASSIGN NAZ-ACT-TYPE-OF-CALL := '1'
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(pnd_S_Optn);                                                                                  //Natural: ASSIGN NAZ-ACT-OPTION-CDE := #S-OPTN
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(pnd_S_Orgn_Cde);                                                                              //Natural: ASSIGN NAZ-ACT-ORIGIN-CDE := #S-ORGN-CDE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().reset();                                                                                          //Natural: RESET NAZ-ACT-GUAR-PERIOD-MTH NAZ-ACT-GUAR-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().reset();
        if (condition(pnd_S_Optn.equals(21)))                                                                                                                             //Natural: IF #S-OPTN = 21
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth()),  //Natural: DIVIDE 12 INTO #GUAR-PRD GIVING NAZ-ACT-GUAR-PERIOD REMAINDER NAZ-ACT-GUAR-PERIOD-MTH
                pnd_Guar_Prd.mod(12));
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period()), 
                pnd_Guar_Prd.divide(12));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet3115 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #S-OPTN;//Natural: VALUE 05,08,16,17,54
            if (condition((pnd_S_Optn.equals(5) || pnd_S_Optn.equals(8) || pnd_S_Optn.equals(16) || pnd_S_Optn.equals(17) || pnd_S_Optn.equals(54))))
            {
                decideConditionsMet3115++;
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(10);                                                                                 //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD := 10
            }                                                                                                                                                             //Natural: VALUE 50,51,52,53
            else if (condition((pnd_S_Optn.equals(50) || pnd_S_Optn.equals(51) || pnd_S_Optn.equals(52) || pnd_S_Optn.equals(53))))
            {
                decideConditionsMet3115++;
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(5);                                                                                  //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD := 5
            }                                                                                                                                                             //Natural: VALUE 09,13,14,15,55
            else if (condition((pnd_S_Optn.equals(9) || pnd_S_Optn.equals(13) || pnd_S_Optn.equals(14) || pnd_S_Optn.equals(15) || pnd_S_Optn.equals(55))))
            {
                decideConditionsMet3115++;
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(15);                                                                                 //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD := 15
            }                                                                                                                                                             //Natural: VALUE 06,10,11,12,56
            else if (condition((pnd_S_Optn.equals(6) || pnd_S_Optn.equals(10) || pnd_S_Optn.equals(11) || pnd_S_Optn.equals(12) || pnd_S_Optn.equals(56))))
            {
                decideConditionsMet3115++;
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(20);                                                                                 //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD := 20
            }                                                                                                                                                             //Natural: VALUE 32
            else if (condition((pnd_S_Optn.equals(32))))
            {
                decideConditionsMet3115++;
                if (condition(pnd_S_Cntrct_Fin_Per_Pay_Dte.greater(getZero())))                                                                                           //Natural: IF #S-CNTRCT-FIN-PER-PAY-DTE GT 0
                {
                                                                                                                                                                          //Natural: PERFORM GET-SL-GUAR-PERIOD
                    sub_Get_Sl_Guar_Period();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode().setValue(pnd_S_Mode_Ind);                                                                              //Natural: ASSIGN NAZ-ACT-PYMNT-MODE := #S-MODE-IND
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().reset();                                                                                           //Natural: RESET NAZ-ACT-ANN-DT-OF-BRTH NAZ-ACT-2ND-ANN-DT-OF-BRTH
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth().reset();
        if (condition(pnd_S_First_Ann_Dob.greater(getZero())))                                                                                                            //Natural: IF #S-FIRST-ANN-DOB GT 0
        {
            pnd_Dob_A.setValueEdited(pnd_S_First_Ann_Dob,new ReportEditMask("99999999"));                                                                                 //Natural: MOVE EDITED #S-FIRST-ANN-DOB ( EM = 99999999 ) TO #DOB-A
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dob_A);                                      //Natural: MOVE EDITED #DOB-A TO NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Scnd_Ann_Dob.greater(getZero())))                                                                                                             //Natural: IF #S-SCND-ANN-DOB GT 0
        {
            pnd_Dob_A.setValueEdited(pnd_S_Scnd_Ann_Dob,new ReportEditMask("99999999"));                                                                                  //Natural: MOVE EDITED #S-SCND-ANN-DOB ( EM = 99999999 ) TO #DOB-A
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dob_A);                                  //Natural: MOVE EDITED #DOB-A TO NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex().setValue(pnd_S_First_Annt_Sex_Cde);                                                                       //Natural: ASSIGN NAZ-ACT-ANN-SEX := #S-FIRST-ANNT-SEX-CDE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex().setValue(pnd_S_Scnd_Annt_Sex_Cde);                                                                    //Natural: ASSIGN NAZ-ACT-2ND-ANN-SEX := #S-SCND-ANNT-SEX-CDE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("Y");                                                                                      //Natural: ASSIGN NAZ-ACT-TIAA-STD-CALL := 'Y'
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(1).setValue("32");                                                                     //Natural: ASSIGN NAZ-ACT-TIAA-STD-RATE-CDE ( 1 ) := '32'
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date()),          //Natural: ASSIGN NAZ-ACT-EFF-DATE := NAZ-ACT-ASD-DATE -1
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().subtract(1));
        DbsUtil.callnat(Aian051.class , getCurrentProcessState(), pdaAiaa510.getNaz_Act_Calc_Input_Area(), pnd_Aian021_Linkage_Array.getValue("*"), pnd_Ann_Factor.getValue("*"),  //Natural: CALLNAT 'AIAN051' NAZ-ACT-CALC-INPUT-AREA #AIAN021-LINKAGE-ARRAY ( * ) #ANN-FACTOR ( * ) #WS-TPA-IND
            pnd_Ws_Tpa_Ind);
        if (condition(Global.isEscape())) return;
        getReports().display(0, "AIAN051 RC",                                                                                                                             //Natural: DISPLAY ( 0 ) 'AIAN051 RC' NAZ-ACT-RETURN-CODE 'BASE RATE' NAZ-ACT-TIAA-STD-EFF-RATE-INT ( 1 ) 'CNTRCT' #S-PPCN-NBR 'GUAR YR' NAZ-ACT-GUAR-PERIOD 'GUAR MO' NAZ-ACT-GUAR-PERIOD-MTH 'OPT' NAZ-ACT-OPTION-CDE 'MODE' NAZ-ACT-PYMNT-MODE 'PREMIUM' NAZ-ACT-TIAA-STD-RATE-AMT ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 'SEX1' NAZ-ACT-ANN-SEX 'SEX2' NAZ-ACT-2ND-ANN-SEX '1st DOB' NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD ) '2nd DOB' NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = YYYYMMDD ) 'Iss DTE' NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) 'Eff Dte' NAZ-ACT-EFF-DATE ( EM = YYYYMMDD ) 'ST' NAZ-ACT-STATE-AT-ISSUE
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code(),"BASE RATE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(1),"CNTRCT",
        		pnd_S_Ppcn_Nbr,"GUAR YR",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period(),"GUAR MO",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth(),"OPT",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde(),"MODE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode(),"PREMIUM",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"SEX1",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex(),"SEX2",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex(),"1st DOB",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth(), new ReportEditMask ("YYYYMMDD"),"2nd DOB",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth(), new ReportEditMask ("YYYYMMDD"),"Iss DTE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(), new ReportEditMask ("YYYYMMDD"),"Eff Dte",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(), new ReportEditMask ("YYYYMMDD"),"ST",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue());
        if (Global.isEscape()) return;
        ldaIaalimra.getPnd_Output_Pnd_Base_Rate().setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(1),new                    //Natural: MOVE EDITED NAZ-ACT-TIAA-STD-EFF-RATE-INT ( 1 ) ( EM = 99.999 ) TO #BASE-RATE
            ReportEditMask("99.999"));
    }
    private void sub_Get_Sl_Guar_Period() throws Exception                                                                                                                //Natural: GET-SL-GUAR-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Sl_Iss_Dte.setValue(pnd_S_Issue_Dte);                                                                                                                         //Natural: ASSIGN #SL-ISS-DTE := #S-ISSUE-DTE
        pnd_Sl_Fin_Dte.setValue(pnd_S_Cntrct_Fin_Per_Pay_Dte);                                                                                                            //Natural: ASSIGN #SL-FIN-DTE := #S-CNTRCT-FIN-PER-PAY-DTE
        if (condition(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.greater(pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo)))                                                                                //Natural: IF #SL-ISS-MO GT #SL-FIN-MO
        {
            pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo.nadd(12);                                                                                                                        //Natural: ADD 12 TO #SL-FIN-MO
            pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Yr.nsubtract(1);                                                                                                                    //Natural: SUBTRACT 1 FROM #SL-FIN-YR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr.compute(new ComputeParameters(false, pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr), pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Yr.subtract(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr)); //Natural: ASSIGN #SL-ISS-YR := #SL-FIN-YR - #SL-ISS-YR
        pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.compute(new ComputeParameters(false, pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo), pnd_Sl_Fin_Dte_Pnd_Sl_Fin_Mo.subtract(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo)); //Natural: ASSIGN #SL-ISS-MO := #SL-FIN-MO - #SL-ISS-MO
        //* *WRITE '=' #SL-ISS-YR '=' #SL-ISS-MO
        short decideConditionsMet3164 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #S-MODE-IND;//Natural: VALUE 100
        if (condition((pnd_S_Mode_Ind.equals(100))))
        {
            decideConditionsMet3164++;
            pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.nadd(1);                                                                                                                         //Natural: ASSIGN #SL-ISS-MO := #SL-ISS-MO + 1
        }                                                                                                                                                                 //Natural: VALUE 600:699
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(600) && pnd_S_Mode_Ind.lessOrEqual(699)))))
        {
            decideConditionsMet3164++;
            pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.nadd(3);                                                                                                                         //Natural: ASSIGN #SL-ISS-MO := #SL-ISS-MO + 3
        }                                                                                                                                                                 //Natural: VALUE 700:799
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(700) && pnd_S_Mode_Ind.lessOrEqual(799)))))
        {
            decideConditionsMet3164++;
            pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.nadd(6);                                                                                                                         //Natural: ASSIGN #SL-ISS-MO := #SL-ISS-MO + 6
        }                                                                                                                                                                 //Natural: VALUE 800:899
        else if (condition(((pnd_S_Mode_Ind.greaterOrEqual(800) && pnd_S_Mode_Ind.lessOrEqual(899)))))
        {
            decideConditionsMet3164++;
            pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr.nadd(1);                                                                                                                         //Natural: ASSIGN #SL-ISS-YR := #SL-ISS-YR + 1
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet3164 > 0))
        {
            if (condition(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.greaterOrEqual(12)))                                                                                               //Natural: IF #SL-ISS-MO GE 12
            {
                pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo.nsubtract(12);                                                                                                               //Natural: ASSIGN #SL-ISS-MO := #SL-ISS-MO - 12
                pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr.nadd(1);                                                                                                                     //Natural: ASSIGN #SL-ISS-YR := #SL-ISS-YR + 1
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr);                                                           //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD := #SL-ISS-YR
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth().setValue(pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo);                                                       //Natural: ASSIGN NAZ-ACT-GUAR-PERIOD-MTH := #SL-ISS-MO
            getReports().write(0, "=",pnd_S_Ppcn_Nbr,"=",pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Yr,"=",pnd_Sl_Iss_Dte_Pnd_Sl_Iss_Mo);                                                  //Natural: WRITE '=' #S-PPCN-NBR '=' #SL-ISS-YR '=' #SL-ISS-MO
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=0 LS=250");

        getReports().setDisplayColumns(0, "AIAN051 RC",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code(),"BASE RATE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int(),"CNTRCT",
        		pnd_S_Ppcn_Nbr,"GUAR YR",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period(),"GUAR MO",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth(),"OPT",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde(),"MODE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode(),"PREMIUM",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"SEX1",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex(),"SEX2",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex(),"1st DOB",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth(), new ReportEditMask ("YYYYMMDD"),"2nd DOB",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth(), new ReportEditMask ("YYYYMMDD"),"Iss DTE",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(), new ReportEditMask ("YYYYMMDD"),"Eff Dte",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(), new ReportEditMask ("YYYYMMDD"),"ST",
        		pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue());
    }
}
