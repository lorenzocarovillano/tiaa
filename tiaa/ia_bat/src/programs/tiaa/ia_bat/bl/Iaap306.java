/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:07 PM
**        * FROM NATURAL PROGRAM : Iaap306
************************************************************
**        * FILE NAME            : Iaap306.java
**        * CLASS NAME           : Iaap306
**        * INSTANCE NAME        : Iaap306
************************************************************
************************************************************************
* PROGRAM:  IAAP306 - ORIGINAL VERSION SAVED AS OLDP306S
* FUNCTION: CREATE A WORK FILE FOR BERWYN EXTRACT. THIS IS CLONED FROM
*           IAAP306 BUT REMOVED REFERENCES TO COR/NAAD AS PART OF
*           COR/NAAD DECOMMISSION. THIS WILL READ ETL FILES FROM MDM
*           TO GET PARTICIPANT INFO.
* DATE    : 09/18/2014 - JUN TINIO
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
*            ADDITIONAL CHANGES TO FIX LOOPING ISSUE.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap306 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_W1_Rec;

    private DbsGroup pnd_W1_Rec__R_Field_1;
    private DbsField pnd_W1_Rec_Pnd_W1_Ssn;
    private DbsField pnd_W1_Rec_Pnd_W1_Last_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_First_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Mddle_Name;
    private DbsField pnd_W1_Rec_Pnd_W1_Dob;

    private DbsGroup pnd_W1_Rec__R_Field_2;
    private DbsField pnd_W1_Rec_Pnd_W1_Dob_N;
    private DbsField pnd_W1_Rec_Pnd_W1_Contract;
    private DbsField pnd_W1_Rec_Pnd_W1_Payee;
    private DbsField pnd_W1_Rec_Pnd_W1_Pend;
    private DbsField pnd_W1_Rec_Pnd_W1_Option_Code;
    private DbsField pnd_W1_Rec_Pnd_W1_Pin;
    private DbsField pnd_W1_Rec_Pnd_W1_Orgn_Cde;
    private DbsField pnd_W1_Rec_Pnd_W1_Sex_Cde;
    private DbsField pnd_Cor_Rec;

    private DbsGroup pnd_Cor_Rec__R_Field_3;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot;

    private DbsGroup pnd_Cor_Rec__R_Field_4;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Payee;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Pin;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Dob;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Dod;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Sex_Cde;

    private DbsGroup pnd_Ia_Rec;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot;

    private DbsGroup pnd_Ia_Rec__R_Field_5;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Payee;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha;

    private DbsGroup pnd_Ia_Rec__R_Field_6;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Ssn;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Last_Nme;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Fst_Nme;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Optn;
    private DbsField pnd_Ia_Rec_Pnd_Ia_X_Ref_1st;

    private DbsGroup pnd_Ia_Rec__R_Field_7;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_1_Last;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_1_First;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_1_Middle;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Dob_1st;
    private DbsField pnd_Ia_Rec_Pnd_Ia_X_Ref_2nd;

    private DbsGroup pnd_Ia_Rec__R_Field_8;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_2_Last;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_2_First;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_2_Middle;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Dob_2nd;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Bnfcry_Xref;

    private DbsGroup pnd_Ia_Rec__R_Field_9;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_B_Last;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_B_First;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Xref_B_Middle;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Pend;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Pin;
    private DbsField pnd_Ia_Rec_Pnd_Ia_Orgn_Cde;
    private DbsField pnd_Ia_End_Of_File;
    private DbsField pnd_Core_End_Of_File;
    private DbsField pnd_Write_Recs;
    private DbsField pnd_Read_Recs;
    private DbsField pnd_Read_Core_Recs;
    private DbsField pnd_No_Core_Cntrct;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_W1_Rec = localVariables.newFieldInRecord("pnd_W1_Rec", "#W1-REC", FieldType.STRING, 97);

        pnd_W1_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_W1_Rec__R_Field_1", "REDEFINE", pnd_W1_Rec);
        pnd_W1_Rec_Pnd_W1_Ssn = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Ssn", "#W1-SSN", FieldType.NUMERIC, 9);
        pnd_W1_Rec_Pnd_W1_Last_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Last_Name", "#W1-LAST-NAME", FieldType.STRING, 20);
        pnd_W1_Rec_Pnd_W1_First_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_First_Name", "#W1-FIRST-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Mddle_Name = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Mddle_Name", "#W1-MDDLE-NAME", FieldType.STRING, 15);
        pnd_W1_Rec_Pnd_W1_Dob = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Dob", "#W1-DOB", FieldType.STRING, 8);

        pnd_W1_Rec__R_Field_2 = pnd_W1_Rec__R_Field_1.newGroupInGroup("pnd_W1_Rec__R_Field_2", "REDEFINE", pnd_W1_Rec_Pnd_W1_Dob);
        pnd_W1_Rec_Pnd_W1_Dob_N = pnd_W1_Rec__R_Field_2.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Dob_N", "#W1-DOB-N", FieldType.NUMERIC, 8);
        pnd_W1_Rec_Pnd_W1_Contract = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Contract", "#W1-CONTRACT", FieldType.STRING, 10);
        pnd_W1_Rec_Pnd_W1_Payee = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 2);
        pnd_W1_Rec_Pnd_W1_Pend = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pend", "#W1-PEND", FieldType.STRING, 1);
        pnd_W1_Rec_Pnd_W1_Option_Code = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Option_Code", "#W1-OPTION-CODE", FieldType.NUMERIC, 2);
        pnd_W1_Rec_Pnd_W1_Pin = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Pin", "#W1-PIN", FieldType.NUMERIC, 12);
        pnd_W1_Rec_Pnd_W1_Orgn_Cde = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.STRING, 2);
        pnd_W1_Rec_Pnd_W1_Sex_Cde = pnd_W1_Rec__R_Field_1.newFieldInGroup("pnd_W1_Rec_Pnd_W1_Sex_Cde", "#W1-SEX-CDE", FieldType.STRING, 1);
        pnd_Cor_Rec = localVariables.newFieldInRecord("pnd_Cor_Rec", "#COR-REC", FieldType.STRING, 155);

        pnd_Cor_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Cor_Rec__R_Field_3", "REDEFINE", pnd_Cor_Rec);
        pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot", "#COR-CNTRCT-TOT", FieldType.STRING, 
            12);

        pnd_Cor_Rec__R_Field_4 = pnd_Cor_Rec__R_Field_3.newGroupInGroup("pnd_Cor_Rec__R_Field_4", "REDEFINE", pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot);
        pnd_Cor_Rec_Pnd_Cor_Cntrct = pnd_Cor_Rec__R_Field_4.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_Rec_Pnd_Cor_Payee = pnd_Cor_Rec__R_Field_4.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Pin = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);
        pnd_Cor_Rec_Pnd_Cor_Type_Cde = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Ssn = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_Rec_Pnd_Cor_Dob = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Dod = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Last_Nm = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_First_Nm = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Middle_Nm = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Status_Cde = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Cor_Rec_Pnd_Cor_Sex_Cde = pnd_Cor_Rec__R_Field_3.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);

        pnd_Ia_Rec = localVariables.newGroupInRecord("pnd_Ia_Rec", "#IA-REC");
        pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot", "#IA-CNTRCT-TOT", FieldType.STRING, 12);

        pnd_Ia_Rec__R_Field_5 = pnd_Ia_Rec.newGroupInGroup("pnd_Ia_Rec__R_Field_5", "REDEFINE", pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot);
        pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr = pnd_Ia_Rec__R_Field_5.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr", "#IA-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Ia_Rec_Pnd_Ia_Payee = pnd_Ia_Rec__R_Field_5.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Payee", "#IA-PAYEE", FieldType.NUMERIC, 2);
        pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha", "#IA-SSN-ALPHA", FieldType.STRING, 9);

        pnd_Ia_Rec__R_Field_6 = pnd_Ia_Rec.newGroupInGroup("pnd_Ia_Rec__R_Field_6", "REDEFINE", pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha);
        pnd_Ia_Rec_Pnd_Ia_Ssn = pnd_Ia_Rec__R_Field_6.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Ssn", "#IA-SSN", FieldType.NUMERIC, 9);
        pnd_Ia_Rec_Pnd_Ia_Last_Nme = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Last_Nme", "#IA-LAST-NME", FieldType.STRING, 12);
        pnd_Ia_Rec_Pnd_Ia_Fst_Nme = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Fst_Nme", "#IA-FST-NME", FieldType.STRING, 10);
        pnd_Ia_Rec_Pnd_Ia_Optn = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Optn", "#IA-OPTN", FieldType.NUMERIC, 2);
        pnd_Ia_Rec_Pnd_Ia_X_Ref_1st = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_X_Ref_1st", "#IA-X-REF-1ST", FieldType.STRING, 9);

        pnd_Ia_Rec__R_Field_7 = pnd_Ia_Rec.newGroupInGroup("pnd_Ia_Rec__R_Field_7", "REDEFINE", pnd_Ia_Rec_Pnd_Ia_X_Ref_1st);
        pnd_Ia_Rec_Pnd_Ia_Xref_1_Last = pnd_Ia_Rec__R_Field_7.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_1_Last", "#IA-XREF-1-LAST", FieldType.STRING, 7);
        pnd_Ia_Rec_Pnd_Ia_Xref_1_First = pnd_Ia_Rec__R_Field_7.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_1_First", "#IA-XREF-1-FIRST", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Xref_1_Middle = pnd_Ia_Rec__R_Field_7.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_1_Middle", "#IA-XREF-1-MIDDLE", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Dob_1st = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Dob_1st", "#IA-DOB-1ST", FieldType.NUMERIC, 8);
        pnd_Ia_Rec_Pnd_Ia_X_Ref_2nd = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_X_Ref_2nd", "#IA-X-REF-2ND", FieldType.STRING, 9);

        pnd_Ia_Rec__R_Field_8 = pnd_Ia_Rec.newGroupInGroup("pnd_Ia_Rec__R_Field_8", "REDEFINE", pnd_Ia_Rec_Pnd_Ia_X_Ref_2nd);
        pnd_Ia_Rec_Pnd_Ia_Xref_2_Last = pnd_Ia_Rec__R_Field_8.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_2_Last", "#IA-XREF-2-LAST", FieldType.STRING, 7);
        pnd_Ia_Rec_Pnd_Ia_Xref_2_First = pnd_Ia_Rec__R_Field_8.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_2_First", "#IA-XREF-2-FIRST", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Xref_2_Middle = pnd_Ia_Rec__R_Field_8.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_2_Middle", "#IA-XREF-2-MIDDLE", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Dob_2nd = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Dob_2nd", "#IA-DOB-2ND", FieldType.NUMERIC, 8);
        pnd_Ia_Rec_Pnd_Ia_Bnfcry_Xref = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Bnfcry_Xref", "#IA-BNFCRY-XREF", FieldType.STRING, 9);

        pnd_Ia_Rec__R_Field_9 = pnd_Ia_Rec.newGroupInGroup("pnd_Ia_Rec__R_Field_9", "REDEFINE", pnd_Ia_Rec_Pnd_Ia_Bnfcry_Xref);
        pnd_Ia_Rec_Pnd_Ia_Xref_B_Last = pnd_Ia_Rec__R_Field_9.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_B_Last", "#IA-XREF-B-LAST", FieldType.STRING, 7);
        pnd_Ia_Rec_Pnd_Ia_Xref_B_First = pnd_Ia_Rec__R_Field_9.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_B_First", "#IA-XREF-B-FIRST", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Xref_B_Middle = pnd_Ia_Rec__R_Field_9.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Xref_B_Middle", "#IA-XREF-B-MIDDLE", FieldType.STRING, 
            1);
        pnd_Ia_Rec_Pnd_Ia_Pend = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Pend", "#IA-PEND", FieldType.STRING, 1);
        pnd_Ia_Rec_Pnd_Ia_Pin = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Pin", "#IA-PIN", FieldType.NUMERIC, 12);
        pnd_Ia_Rec_Pnd_Ia_Orgn_Cde = pnd_Ia_Rec.newFieldInGroup("pnd_Ia_Rec_Pnd_Ia_Orgn_Cde", "#IA-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ia_End_Of_File = localVariables.newFieldInRecord("pnd_Ia_End_Of_File", "#IA-END-OF-FILE", FieldType.BOOLEAN, 1);
        pnd_Core_End_Of_File = localVariables.newFieldInRecord("pnd_Core_End_Of_File", "#CORE-END-OF-FILE", FieldType.BOOLEAN, 1);
        pnd_Write_Recs = localVariables.newFieldInRecord("pnd_Write_Recs", "#WRITE-RECS", FieldType.NUMERIC, 9);
        pnd_Read_Recs = localVariables.newFieldInRecord("pnd_Read_Recs", "#READ-RECS", FieldType.NUMERIC, 9);
        pnd_Read_Core_Recs = localVariables.newFieldInRecord("pnd_Read_Core_Recs", "#READ-CORE-RECS", FieldType.NUMERIC, 9);
        pnd_No_Core_Cntrct = localVariables.newFieldInRecord("pnd_No_Core_Cntrct", "#NO-CORE-CNTRCT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap306() throws Exception
    {
        super("Iaap306");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM #READ-IA
        sub_Pnd_Read_Ia();
        if (condition(Global.isEscape())) {return;}
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ia_End_Of_File.getBoolean())) {break;}                                                                                                      //Natural: UNTIL #IA-END-OF-FILE
            short decideConditionsMet109 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #IA-CNTRCT-TOT > #COR-CNTRCT-TOT
            if (condition(pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot.greater(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot)))
            {
                decideConditionsMet109++;
                                                                                                                                                                          //Natural: PERFORM #READ-CORE
                sub_Pnd_Read_Core();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #IA-CNTRCT-TOT = #COR-CNTRCT-TOT
            else if (condition(pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot.equals(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot)))
            {
                decideConditionsMet109++;
                if (condition(pnd_Cor_Rec_Pnd_Cor_Status_Cde.equals(" ")))                                                                                                //Natural: IF #COR-STATUS-CDE = ' '
                {
                                                                                                                                                                          //Natural: PERFORM #MOVE-FIELDS
                    sub_Pnd_Move_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM #READ-CORE
                    sub_Pnd_Read_Core();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM #READ-IA
                    sub_Pnd_Read_Ia();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM #READ-CORE
                    sub_Pnd_Read_Core();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #IA-CNTRCT-TOT < #COR-CNTRCT-TOT
            else if (condition(pnd_Ia_Rec_Pnd_Ia_Cntrct_Tot.less(pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot)))
            {
                decideConditionsMet109++;
                //*  IF THERE's SSN, get MDM record using SSN
                if (condition(pnd_Ia_Rec_Pnd_Ia_Ssn.notEquals(getZero()) && pnd_Ia_Rec_Pnd_Ia_Ssn.notEquals(999999999)))                                                  //Natural: IF #IA-SSN NE 0 AND #IA-SSN NE 999999999
                {
                    getWorkFiles().write(4, false, pnd_Ia_Rec);                                                                                                           //Natural: WRITE WORK FILE 4 #IA-REC
                                                                                                                                                                          //Natural: PERFORM #MOVE-FIELDS
                    sub_Pnd_Move_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM #READ-IA
                    sub_Pnd_Read_Ia();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*        END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "NO CORE CONTRACT FOR IA CONTRACT",pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr,pnd_Ia_Rec_Pnd_Ia_Payee);                                           //Natural: WRITE 'NO CORE CONTRACT FOR IA CONTRACT' #IA-CNTRCT-NBR #IA-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Core_Cntrct.nadd(1);                                                                                                                               //Natural: ADD 1 TO #NO-CORE-CNTRCT
                                                                                                                                                                          //Natural: PERFORM #MOVE-IA-FIELDS
                sub_Pnd_Move_Ia_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #READ-IA
                sub_Pnd_Read_Ia();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(0, "IA READS ===================> ",pnd_Read_Recs);                                                                                            //Natural: WRITE 'IA READS ===================> ' #READ-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "CORE READS =================> ",pnd_Read_Core_Recs);                                                                                       //Natural: WRITE 'CORE READS =================> ' #READ-CORE-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "NO MATCHING CORE CONTRACT ==> ",pnd_No_Core_Cntrct);                                                                                       //Natural: WRITE 'NO MATCHING CORE CONTRACT ==> ' #NO-CORE-CNTRCT
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS WRITTEN ============> ",pnd_Write_Recs);                                                                                           //Natural: WRITE 'RECORDS WRITTEN ============> ' #WRITE-RECS
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-CORE
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-IA
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MOVE-FIELDS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MOVE-IA-FIELDS
    }
    private void sub_Pnd_Read_Core() throws Exception                                                                                                                     //Natural: #READ-CORE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Core_End_Of_File.getBoolean()))                                                                                                                 //Natural: IF #CORE-END-OF-FILE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(1, pnd_Cor_Rec);                                                                                                                              //Natural: READ WORK FILE 1 ONCE #COR-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            //*  082017
            pnd_Core_End_Of_File.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #CORE-END-OF-FILE
            pnd_Cor_Rec_Pnd_Cor_Cntrct_Tot.setValue("H'FFFFFFFFFFFFFFFFFFFFFFFF'");                                                                                       //Natural: ASSIGN #COR-CNTRCT-TOT := H'FFFFFFFFFFFFFFFFFFFFFFFF'
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Read_Core_Recs.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #READ-CORE-RECS
    }
    private void sub_Pnd_Read_Ia() throws Exception                                                                                                                       //Natural: #READ-IA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Ia_End_Of_File.getBoolean()))                                                                                                                   //Natural: IF #IA-END-OF-FILE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(2, pnd_Ia_Rec);                                                                                                                               //Natural: READ WORK FILE 2 ONCE #IA-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Ia_End_Of_File.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #IA-END-OF-FILE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(! (DbsUtil.maskMatches(pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha,"NNNNNNNNN"))))                                                                                  //Natural: IF #IA-SSN-ALPHA NOT = MASK ( NNNNNNNNN )
        {
            getReports().write(0, "=",pnd_Ia_Rec_Pnd_Ia_Ssn_Alpha,"NOT NUMERIC","=",pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr,"=",pnd_Ia_Rec_Pnd_Ia_Payee);                            //Natural: WRITE '=' #IA-SSN-ALPHA 'NOT NUMERIC' '=' #IA-CNTRCT-NBR '=' #IA-PAYEE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Read_Recs.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #READ-RECS
    }
    private void sub_Pnd_Move_Fields() throws Exception                                                                                                                   //Natural: #MOVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Ia_Rec_Pnd_Ia_Ssn.greater(getZero()) && pnd_Ia_Rec_Pnd_Ia_Payee.less(3)))                                                                       //Natural: IF #IA-SSN > 0 AND #IA-PAYEE LT 03
        {
            pnd_W1_Rec_Pnd_W1_Ssn.setValue(pnd_Ia_Rec_Pnd_Ia_Ssn);                                                                                                        //Natural: ASSIGN #W1-SSN := #IA-SSN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W1_Rec_Pnd_W1_Ssn.setValue(pnd_Cor_Rec_Pnd_Cor_Ssn);                                                                                                      //Natural: ASSIGN #W1-SSN := #COR-SSN
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W1_Rec_Pnd_W1_Last_Name.setValue(pnd_Cor_Rec_Pnd_Cor_Last_Nm);                                                                                                //Natural: ASSIGN #W1-LAST-NAME := #COR-LAST-NM
        pnd_W1_Rec_Pnd_W1_First_Name.setValue(pnd_Cor_Rec_Pnd_Cor_First_Nm);                                                                                              //Natural: ASSIGN #W1-FIRST-NAME := #COR-FIRST-NM
        pnd_W1_Rec_Pnd_W1_Mddle_Name.setValue(pnd_Cor_Rec_Pnd_Cor_Middle_Nm);                                                                                             //Natural: ASSIGN #W1-MDDLE-NAME := #COR-MIDDLE-NM
        pnd_W1_Rec_Pnd_W1_Dob_N.setValue(pnd_Cor_Rec_Pnd_Cor_Dob);                                                                                                        //Natural: ASSIGN #W1-DOB-N := #COR-DOB
        pnd_W1_Rec_Pnd_W1_Sex_Cde.setValue(pnd_Cor_Rec_Pnd_Cor_Sex_Cde);                                                                                                  //Natural: ASSIGN #W1-SEX-CDE := #COR-SEX-CDE
        pnd_W1_Rec_Pnd_W1_Contract.setValue(pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr);                                                                                                //Natural: ASSIGN #W1-CONTRACT := #IA-CNTRCT-NBR
        pnd_W1_Rec_Pnd_W1_Payee.setValue(pnd_Ia_Rec_Pnd_Ia_Payee);                                                                                                        //Natural: ASSIGN #W1-PAYEE := #IA-PAYEE
        pnd_W1_Rec_Pnd_W1_Pend.setValue(pnd_Ia_Rec_Pnd_Ia_Pend);                                                                                                          //Natural: ASSIGN #W1-PEND := #IA-PEND
        pnd_W1_Rec_Pnd_W1_Option_Code.setValue(pnd_Ia_Rec_Pnd_Ia_Optn);                                                                                                   //Natural: ASSIGN #W1-OPTION-CODE := #IA-OPTN
        pnd_W1_Rec_Pnd_W1_Orgn_Cde.setValue(pnd_Ia_Rec_Pnd_Ia_Orgn_Cde);                                                                                                  //Natural: ASSIGN #W1-ORGN-CDE := #IA-ORGN-CDE
        pnd_W1_Rec_Pnd_W1_Pin.setValue(pnd_Ia_Rec_Pnd_Ia_Pin);                                                                                                            //Natural: ASSIGN #W1-PIN := #IA-PIN
        getWorkFiles().write(3, false, pnd_W1_Rec);                                                                                                                       //Natural: WRITE WORK FILE 3 #W1-REC
        pnd_Write_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRITE-RECS
    }
    private void sub_Pnd_Move_Ia_Fields() throws Exception                                                                                                                //Natural: #MOVE-IA-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_W1_Rec.reset();                                                                                                                                               //Natural: RESET #W1-REC
        pnd_W1_Rec_Pnd_W1_Ssn.setValue(pnd_Ia_Rec_Pnd_Ia_Ssn);                                                                                                            //Natural: ASSIGN #W1-SSN := #IA-SSN
        pnd_W1_Rec_Pnd_W1_Contract.setValue(pnd_Ia_Rec_Pnd_Ia_Cntrct_Nbr);                                                                                                //Natural: ASSIGN #W1-CONTRACT := #IA-CNTRCT-NBR
        pnd_W1_Rec_Pnd_W1_Payee.setValue(pnd_Ia_Rec_Pnd_Ia_Payee);                                                                                                        //Natural: ASSIGN #W1-PAYEE := #IA-PAYEE
        pnd_W1_Rec_Pnd_W1_Pend.setValue(pnd_Ia_Rec_Pnd_Ia_Pend);                                                                                                          //Natural: ASSIGN #W1-PEND := #IA-PEND
        pnd_W1_Rec_Pnd_W1_Option_Code.setValue(pnd_Ia_Rec_Pnd_Ia_Optn);                                                                                                   //Natural: ASSIGN #W1-OPTION-CODE := #IA-OPTN
        pnd_W1_Rec_Pnd_W1_Orgn_Cde.setValue(pnd_Ia_Rec_Pnd_Ia_Orgn_Cde);                                                                                                  //Natural: ASSIGN #W1-ORGN-CDE := #IA-ORGN-CDE
        pnd_W1_Rec_Pnd_W1_Pin.setValue(pnd_Ia_Rec_Pnd_Ia_Pin);                                                                                                            //Natural: ASSIGN #W1-PIN := #IA-PIN
        short decideConditionsMet237 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #IA-PAYEE;//Natural: VALUE 01
        if (condition((pnd_Ia_Rec_Pnd_Ia_Payee.equals(1))))
        {
            decideConditionsMet237++;
            pnd_W1_Rec_Pnd_W1_Last_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_1_Last);                                                                                          //Natural: ASSIGN #W1-LAST-NAME := #IA-XREF-1-LAST
            pnd_W1_Rec_Pnd_W1_First_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_1_First);                                                                                        //Natural: ASSIGN #W1-FIRST-NAME := #IA-XREF-1-FIRST
            pnd_W1_Rec_Pnd_W1_Dob.setValue(pnd_Ia_Rec_Pnd_Ia_Dob_1st);                                                                                                    //Natural: ASSIGN #W1-DOB := #IA-DOB-1ST
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Ia_Rec_Pnd_Ia_Payee.equals(2))))
        {
            decideConditionsMet237++;
            pnd_W1_Rec_Pnd_W1_Last_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_2_Last);                                                                                          //Natural: ASSIGN #W1-LAST-NAME := #IA-XREF-2-LAST
            pnd_W1_Rec_Pnd_W1_First_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_2_First);                                                                                        //Natural: ASSIGN #W1-FIRST-NAME := #IA-XREF-2-FIRST
            pnd_W1_Rec_Pnd_W1_Dob.setValue(pnd_Ia_Rec_Pnd_Ia_Dob_2nd);                                                                                                    //Natural: ASSIGN #W1-DOB := #IA-DOB-2ND
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_W1_Rec_Pnd_W1_Last_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_B_Last);                                                                                          //Natural: ASSIGN #W1-LAST-NAME := #IA-XREF-B-LAST
            pnd_W1_Rec_Pnd_W1_First_Name.setValue(pnd_Ia_Rec_Pnd_Ia_Xref_B_First);                                                                                        //Natural: ASSIGN #W1-FIRST-NAME := #IA-XREF-B-FIRST
            pnd_W1_Rec_Pnd_W1_Dob.setValue(0);                                                                                                                            //Natural: ASSIGN #W1-DOB := 0
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(3, false, pnd_W1_Rec);                                                                                                                       //Natural: WRITE WORK FILE 3 #W1-REC
        pnd_Write_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRITE-RECS
    }

    //
}
