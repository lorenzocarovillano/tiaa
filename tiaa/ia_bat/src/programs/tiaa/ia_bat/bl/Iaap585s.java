/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:06 PM
**        * FROM NATURAL PROGRAM : Iaap585s
************************************************************
**        * FILE NAME            : Iaap585s.java
**        * CLASS NAME           : Iaap585s
**        * INSTANCE NAME        : Iaap585s
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP585S   A DEATH CLAIMS SELECTION PROGRAM FOR ALL *
*      DATE   -  05/95      PAYMENTS THAT DAY PULLED FROM THE AUDIT  *
*    AUTHOR   -  ARI G.     FILE. IT WILL PULL ALL CONTRACT PAYMENT  *
*                           INFORMATION, INCLUDING CURRENT AND FUTURE*
*                           PAYMENTS.
*   HISTORY   -  03/99      FOR MULTI FUNDING, FUTURES ARE NOW AT
*                           BEGINING OF INSTALLMENT PERIODIC GROUP.
*             10/12/2000    WRITE INSTALLMENT TYPE TO THE WORK FILE
*                           TO SEPARATE CONTINUED PAYMENT FROM LUMP SUM
*
* 03/25/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE AUDIT FILE.   *
*                   SC 032510.
* JUN 2017 J BREMER       PIN EXPANSION RE-STOW ONLY
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap585s extends BLNatBase
{
    // Data Areas
    private LdaIaal585s ldaIaal585s;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Write_Ls;
    private DbsField pnd_Write_Pp;
    private DbsField pnd_Dated;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal585s = new LdaIaal585s();
        registerRecord(ldaIaal585s);
        registerRecord(ldaIaal585s.getVw_audit_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Write_Ls = localVariables.newFieldInRecord("pnd_Write_Ls", "#WRITE-LS", FieldType.NUMERIC, 9);
        pnd_Write_Pp = localVariables.newFieldInRecord("pnd_Write_Pp", "#WRITE-PP", FieldType.NUMERIC, 9);
        pnd_Dated = localVariables.newFieldInRecord("pnd_Dated", "#DATED", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal585s.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap585s() throws Exception
    {
        super("Iaap585s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***********************************************************************
        //*  START OF PROGRAM
        //* ***********************************************************************
        //* *   PERFORM #SETUP-SYSTEM-DATE-TIME
        //* *
        getReports().write(0, "*** START OF PROGRAM IAAP585S ***");                                                                                                       //Natural: WRITE '*** START OF PROGRAM IAAP585S ***'
        if (Global.isEscape()) return;
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal585s.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
        //*  032510
        //*  032510
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        //* *. READ  AUDIT-VIEW PHYSICAL
        ldaIaal585s.getVw_audit_View().startDatabaseRead                                                                                                                  //Natural: READ AUDIT-VIEW BY PAUDIT-STATUS-TIMESTAMP STARTING FROM #DATED
        (
        "RD",
        new Wc[] { new Wc("PAUDIT_STATUS_TIMESTAMP", ">=", pnd_Dated, WcType.BY) },
        new Oc[] { new Oc("PAUDIT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(ldaIaal585s.getVw_audit_View().readNextRow("RD")))
        {
            ldaIaal585s.getPnd_Audit_Reads().nadd(1);                                                                                                                     //Natural: ADD 1 TO #AUDIT-READS
            ldaIaal585s.getPnd_Fl_Date_Yyyymmdd_Alph().setValueEdited(ldaIaal585s.getAudit_View_Paudit_Status_Timestamp(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED PAUDIT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            //*  032510
            if (condition(ldaIaal585s.getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num().notEquals(ldaIaal585s.getIaa_Parm_Card_Pnd_Parm_Date_N())))                 //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                //*  032510
                if (true) break RD;                                                                                                                                       //Natural: ESCAPE BOTTOM ( RD. )
                //*  032510
            }                                                                                                                                                             //Natural: END-IF
            //* * 10/22
            if (condition(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Typ().getValue(1).equals(" ")))                                                                      //Natural: REJECT IF PAUDIT-INSTLLMNT-TYP ( 1 ) = ' '
            {
                continue;
            }
            //* *
            //*  ACCEPT IF #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N /* 032510
            ldaIaal585s.getPnd_Audit_Time_Selects().nadd(1);                                                                                                              //Natural: ADD 1 TO #AUDIT-TIME-SELECTS
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
            sub_Pnd_Reset_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Contract().setValue(ldaIaal585s.getAudit_View_Paudit_Ppcn_Nbr());                                                     //Natural: MOVE PAUDIT-PPCN-NBR TO #W1-CONTRACT
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Payee_Cde().setValue(ldaIaal585s.getAudit_View_Pnd_Paudit_Payee_Cde_A());                                             //Natural: MOVE #PAUDIT-PAYEE-CDE-A TO #W1-PAYEE-CDE
            ldaIaal585s.getPnd_Num().setValue(ldaIaal585s.getAudit_View_Count_Castpaudit_Installments());                                                                 //Natural: MOVE C*PAUDIT-INSTALLMENTS TO #NUM
            if (condition(!(ldaIaal585s.getPnd_Num().greater(getZero()))))                                                                                                //Natural: ACCEPT IF #NUM > 0
            {
                continue;
            }
            //*      COMPUTE #NUM-MINUS-ONE = #NUM - 1
            F4:                                                                                                                                                           //Natural: FOR #I = 1 TO #NUM
            for (ldaIaal585s.getPnd_I().setValue(1); condition(ldaIaal585s.getPnd_I().lessOrEqual(ldaIaal585s.getPnd_Num())); ldaIaal585s.getPnd_I().nadd(1))
            {
                if (condition(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Futr_Ind().getValue(ldaIaal585s.getPnd_I()).equals("Y")))                                        //Natural: IF PAUDIT-INSTLLMNT-FUTR-IND ( #I ) = 'Y'
                {
                    ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Ft_Gross().setValue(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Gross().getValue(ldaIaal585s.getPnd_I()));     //Natural: MOVE PAUDIT-INSTLLMNT-GROSS ( #I ) TO #W1-FT-GROSS
                    ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Ft_Ovp().setValue(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Ovrpymnt().getValue(ldaIaal585s.getPnd_I()));    //Natural: MOVE PAUDIT-INSTLLMNT-OVRPYMNT ( #I ) TO #W1-FT-OVP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal585s.getPnd_Cr_Gross().nadd(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Gross().getValue(ldaIaal585s.getPnd_I()));                              //Natural: ADD PAUDIT-INSTLLMNT-GROSS ( #I ) TO #CR-GROSS
                    ldaIaal585s.getPnd_Cr_Dci().nadd(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Dci().getValue(ldaIaal585s.getPnd_I()));                                  //Natural: ADD PAUDIT-INSTLLMNT-DCI ( #I ) TO #CR-DCI
                    ldaIaal585s.getPnd_Cr_Ovp().nadd(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Ovrpymnt().getValue(ldaIaal585s.getPnd_I()));                             //Natural: ADD PAUDIT-INSTLLMNT-OVRPYMNT ( #I ) TO #CR-OVP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Gross().setValue(ldaIaal585s.getPnd_Cr_Gross());                                                                   //Natural: MOVE #CR-GROSS TO #W1-CR-GROSS
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Past_Due().setValue(ldaIaal585s.getPnd_Cr_Gross());                                                                   //Natural: MOVE #CR-GROSS TO #W1-PAST-DUE
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Dci().setValue(ldaIaal585s.getPnd_Cr_Dci());                                                                       //Natural: MOVE #CR-DCI TO #W1-CR-DCI
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp().setValue(ldaIaal585s.getPnd_Cr_Ovp());                                                                       //Natural: MOVE #CR-OVP TO #W1-CR-OVP
            ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().setValue(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Typ().getValue(1));                               //Natural: MOVE PAUDIT-INSTLLMNT-TYP ( 1 ) TO #W1-INSTLLMNT-TYP
            if (condition(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Typ().getValue(1).equals("LS")))                                                                     //Natural: IF PAUDIT-INSTLLMNT-TYP ( 1 ) = 'LS'
            {
                pnd_Write_Ls.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WRITE-LS
                ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Gross().setValue(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Gross().getValue(ldaIaal585s.getPnd_Num()));       //Natural: MOVE PAUDIT-INSTLLMNT-GROSS ( #NUM ) TO #W1-CR-GROSS
                ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Past_Due().nsubtract(ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Gross());                                         //Natural: COMPUTE #W1-PAST-DUE = #W1-PAST-DUE - #W1-CR-GROSS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal585s.getAudit_View_Paudit_Instllmnt_Typ().getValue(1).equals("PP")))                                                                     //Natural: IF PAUDIT-INSTLLMNT-TYP ( 1 ) = 'PP'
            {
                pnd_Write_Pp.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WRITE-PP
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, ldaIaal585s.getPnd_Work_Record_1());                                                                                           //Natural: WRITE WORK FILE 1 #WORK-RECORD-1
            ldaIaal585s.getPnd_Work_Records_Written().nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-RECORDS-WRITTEN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "*** END OF PROGRAM IAAP585S ***");                                                                                                         //Natural: WRITE '*** END OF PROGRAM IAAP585S ***'
        if (Global.isEscape()) return;
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-SYSTEM-DATE-TIME
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* ********************************************************************
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE READS ==========> ",ldaIaal585s.getPnd_Audit_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                    //Natural: WRITE '  AUDIT FILE READS ==========> ' #AUDIT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  AUDIT FILE TIME SELECTS ===> ",ldaIaal585s.getPnd_Audit_Time_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                             //Natural: WRITE '  AUDIT FILE TIME SELECTS ===> ' #AUDIT-TIME-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK RECORDS LUMP SUM =====> ",pnd_Write_Ls, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE '  WORK RECORDS LUMP SUM =====> ' #WRITE-LS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK RECORDS PP ===========> ",pnd_Write_Pp, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE '  WORK RECORDS PP ===========> ' #WRITE-PP ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  WORK RECORDS WRITTEN ======> ",ldaIaal585s.getPnd_Work_Records_Written(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                           //Natural: WRITE '  WORK RECORDS WRITTEN ======> ' #WORK-RECORDS-WRITTEN ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "====================================================");                                                                                    //Natural: WRITE '===================================================='
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*   RESET #WORK-RECORD-1
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Contract().reset();                                                                                                       //Natural: RESET #W1-CONTRACT #W1-INSTLLMNT-TYP #W1-PAYEE-CDE #W1-FT-GROSS #W1-FT-OVP #W1-CR-GROSS #W1-CR-DCI #W1-CR-OVP #W1-PAST-DUE #CR-GROSS #CR-DCI #CR-OVP
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Payee_Cde().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Ft_Gross().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Ft_Ovp().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Gross().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Dci().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp().reset();
        ldaIaal585s.getPnd_Work_Record_1_Pnd_W1_Past_Due().reset();
        ldaIaal585s.getPnd_Cr_Gross().reset();
        ldaIaal585s.getPnd_Cr_Dci().reset();
        ldaIaal585s.getPnd_Cr_Ovp().reset();
    }
    private void sub_Pnd_Setup_System_Date_Time() throws Exception                                                                                                        //Natural: #SETUP-SYSTEM-DATE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal585s.getPnd_Sys_Date().setValue(Global.getDATX());                                                                                                         //Natural: MOVE *DATX TO #SYS-DATE
        ldaIaal585s.getPnd_Sy_Date_Yymmdd_Alph().setValueEdited(ldaIaal585s.getPnd_Sys_Date(),new ReportEditMask("YYMMDD"));                                              //Natural: MOVE EDITED #SYS-DATE ( EM = YYMMDD ) TO #SY-DATE-YYMMDD-ALPH
        ldaIaal585s.getPnd_Sys_Time().setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO #SYS-TIME
        ldaIaal585s.getPnd_Sy_Time_Hhiiss_Alph().setValueEdited(ldaIaal585s.getPnd_Sys_Time(),new ReportEditMask("HHIISS"));                                              //Natural: MOVE EDITED #SYS-TIME ( EM = HHIISS ) TO #SY-TIME-HHIISS-ALPH
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal585s.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal585s.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032510
            ldaIaal585s.getIaa_Parm_Card_Pnd_Parm_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032510
        pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIaal585s.getIaa_Parm_Card_Pnd_Parm_Date());                                                            //Natural: MOVE EDITED #PARM-DATE TO #DATED ( EM = YYYYMMDD )
    }

    //
}
