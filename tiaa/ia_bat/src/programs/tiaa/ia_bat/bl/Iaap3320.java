/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:24:32 PM
**        * FROM NATURAL PROGRAM : Iaap3320
************************************************************
**        * FILE NAME            : Iaap3320.java
**        * CLASS NAME           : Iaap3320
**        * INSTANCE NAME        : Iaap3320
************************************************************
************************************************************************
* PROGRAM  : IAAP3320
* SYSTEM   : IA
* GENERATED: SEP 15,10
* FUNCTION : PRINTS THE MONTHLY PENDED PAYMMENT AND DPI LIABILITY
*            REPORT IA3300M3 (NON-PENSION)
* INPUT    : IA MASTER MONTHLY EXTRACT (NON-PENSION)
*
* HISTORY
* 09/15/10 O. SOTTO REWRITE OF THE CURRENT PROGRAM THAT PRODUCES
*                   IA3300M* REPORTS.
* 03/23/11 O. SOTTO PROD FIX. SC 032311.
* 04/05/12 O. SOTTO RATE BASE CHANGES. SC 040512.
* 04/02/14 O. SOTTO CALL IAAN702R FOR ALL PAYEES. CHANGES MARKED
*                   040214.
* 04/2017  O. SOTTO PIN EXPANSION CHANGES MARKED 082017.
*
* 06/2019  J. TINIO DPI CALCULATION CHANGES - SC 062019.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3320 extends BLNatBase
{
    // Data Areas
    private PdaAnta001 pdaAnta001;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_T_6;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input__Filler3;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input__Filler4;
    private DbsField pnd_Input_Pnd_Rsdncy_At_Issue;
    private DbsField pnd_Input__Filler5;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input__Filler6;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input__Filler7;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input__Filler8;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input__Filler9;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input__Filler10;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Input_Pnd_W1_Cntrct_At_Iss_Re;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input__Filler11;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input__Filler12;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input__Filler13;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input__Filler14;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input__Filler15;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input__Filler16;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input__Filler17;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input__Filler18;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input__Filler19;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input__Filler20;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input__Filler21;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input__Filler22;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input__Filler23;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input__Filler24;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input__Filler25;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input__Filler26;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Dashes;

    private DbsGroup pnd_Header_Term;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Lit;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Dte;

    private DbsGroup pnd_Hdr_Tot_Lit_Ia3300m3;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Fill1m3_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Fill1;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Prdct;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdrm3_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Chk;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Date;

    private DbsGroup pnd_Hdr_Wrksht_Ia3300m3;
    private DbsField pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Fill1m3;
    private DbsField pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Product;
    private DbsField pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Hdrm3;
    private DbsField pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Lit1;
    private DbsField pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date;

    private DbsGroup pnd_Tiaa_Colmn_Hdr1_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill;
    private DbsField pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit;

    private DbsGroup pnd_Tiaa_Colmn_Hdr2_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte;

    private DbsGroup pnd_Tiaa_Colmn_Hdr3_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4;

    private DbsGroup pnd_Wrksht_Hdr1;
    private DbsField pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1;
    private DbsField pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit;
    private DbsField pnd_Tiaa_Wrksht_Hdr2;
    private DbsField pnd_Tiaa_Wrksht_Hdr3;

    private DbsGroup pnd_Tiaa_Total_Detail;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due;
    private DbsField pnd_Tiaa_Wrksht_Total_Line;

    private DbsGroup pnd_Tiaa_Wrksht_Total_Line__R_Field_15;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due;

    private DbsGroup pnd_W_Detail_Tiaa;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_16;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee_N;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Iss_Dte;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Rsdnce_Cde;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_17;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode_N;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte;

    private DbsGroup pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Guar_Due;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Div_Due;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymt_Cnt;

    private DbsGroup pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_Yy;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_18;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_19;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_Cc;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_Y;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_20;
    private DbsField pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm_A;
    private DbsField pnd_W_Detail_Tiaa_Pnd_To_Yy;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_21;
    private DbsField pnd_W_Detail_Tiaa_Pnd_To_Cc;
    private DbsField pnd_W_Detail_Tiaa_Pnd_To_Yy_A;
    private DbsField pnd_W_Detail_Tiaa_Pnd_To_Mm;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_22;
    private DbsField pnd_W_Detail_Tiaa_Pnd_To_Mm_A;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Num_Chks;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Guar;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_23;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Alpha_4;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Alpha_6;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_24;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div_R;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Dpi;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals;

    private DbsGroup pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Guar;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Div;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_25;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_4;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_7;

    private DbsGroup pnd_W_Detail_Tiaa__R_Field_26;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi_R;
    private DbsField pnd_W_Detail_Tiaa_Pnd_W_Dtl_Ovrall_Total;
    private DbsField pnd_Ia3300_Hdr_Lit;
    private DbsField pnd_Tot_Fill1m2_Lit;
    private DbsField pnd_Tot_Hdr1m2_Lit;
    private DbsField pnd_Wrksht_Total_Lit;

    private DbsGroup pnd_W_Tiaa_Total_Table;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Curs;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot;

    private DbsGroup pnd_W_Tiaa_Ttl_Tbl;

    private DbsGroup pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi;
    private DbsField pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl;

    private DbsGroup pnd_W_Tiaa_Tot_Ovrall_Totals;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd;

    private DbsGroup pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi;
    private DbsField pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot;
    private DbsField pnd_W_Cntrct;

    private DbsGroup pnd_W_Cntrct__R_Field_27;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3;

    private DbsGroup pnd_W_Cntrct__R_Field_28;
    private DbsField pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1;
    private DbsField pnd_W_Payee_Cde;

    private DbsGroup pnd_Tab_Fund_Table;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Fund;

    private DbsGroup pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Install_Date;

    private DbsGroup pnd_Tab_Fund_Table__R_Field_29;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Install_Date_N;

    private DbsGroup pnd_Tab_Fund_Table__R_Field_30;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Install_Yymm;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Auv;
    private DbsField pnd_Tab_Fund_Table_Pnd_Tab_Reval;
    private DbsField pnd_Tab_Fund_Table_Pnd_Table_Cnt;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Bottom_Date;

    private DbsGroup pnd_Bottom_Date__R_Field_31;
    private DbsField pnd_Bottom_Date_Pnd_Bottom_Date_A;
    private DbsField pnd_Ret_Cde;

    private DbsGroup pnd_Wrksht_File;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee;

    private DbsGroup pnd_Wrksht_File__R_Field_32;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Payee;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dod;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Opt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Org;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cur;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Pend;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Mode;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Fund;

    private DbsGroup pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte;

    private DbsGroup pnd_Wrksht_File__R_Field_33;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Div;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Guar;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Units;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Reval;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Auv;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Total;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_34;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    private DbsGroup pnd_Ndxs_For_Tiaa_Total_Table;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx;
    private DbsField pnd_Max_Wrk;
    private DbsField pnd_Max_Pend;
    private DbsField pnd_Max_Detail_Tbl;
    private DbsField pnd_Max_Tiaa_Fnd;
    private DbsField pnd_Max_Funds;
    private DbsField pnd_Single_Life;
    private DbsField pnd_In_Cnt;
    private DbsField pnd_Processed_Cnt;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_E;
    private DbsField pnd_F;
    private DbsField pnd_G;
    private DbsField pnd_H;
    private DbsField pnd_Fnd_Cnt;
    private DbsField pnd_Tot_Ndx1;
    private DbsField pnd_Tot_Ndx2;
    private DbsField pnd_Tot_Ndx3;
    private DbsField pnd_Fnd_Ndx;
    private DbsField pnd_Naz_Cnt;
    private DbsField pnd_Naz_Written;
    private DbsField pnd_Excp_Cnt;
    private DbsField pnd_Rec10_Cnt;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Tiaa_Var;
    private DbsField pnd_Term;
    private DbsField pnd_Bypass;
    private DbsField pnd_Process;
    private DbsField pnd_First_Time;
    private DbsField pnd_Detail;
    private DbsField pnd_Totals;
    private DbsField pnd_Wrksht;
    private DbsField pnd_Limited;
    private DbsField pnd_W_Issue_Date;

    private DbsGroup pnd_W_Issue_Date__R_Field_35;
    private DbsField pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm;
    private DbsField pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd;
    private DbsField pnd_W_Work_Date;

    private DbsGroup pnd_W_Work_Date__R_Field_36;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Date_A;

    private DbsGroup pnd_W_Work_Date__R_Field_37;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Yyyymm;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Dd;
    private DbsField pnd_W_Install_Date;

    private DbsGroup pnd_W_Install_Date__R_Field_38;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yyyy;

    private DbsGroup pnd_W_Install_Date__R_Field_39;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Cc;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yy;

    private DbsGroup pnd_W_Install_Date__R_Field_40;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yyyy_A;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Mm;

    private DbsGroup pnd_W_Install_Date__R_Field_41;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Date_N;

    private DbsGroup pnd_W_Install_Date__R_Field_42;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm;
    private DbsField pnd_W_Chk_Dte;

    private DbsGroup pnd_W_Chk_Dte__R_Field_43;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A;

    private DbsGroup pnd_W_Chk_Dte__R_Field_44;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy;

    private DbsGroup pnd_W_Chk_Dte__R_Field_45;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Mm;

    private DbsGroup pnd_W_Chk_Dte__R_Field_46;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N;

    private DbsGroup pnd_W_Chk_Dte__R_Field_47;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm;

    private DbsGroup pnd_W_Chk_Dte__R_Field_48;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm_N;
    private DbsField pnd_W_Pend_Date;

    private DbsGroup pnd_W_Pend_Date__R_Field_49;
    private DbsField pnd_W_Pend_Date_Pnd_W_Pend_Yyyy;
    private DbsField pnd_W_Pend_Date_Pnd_W_Pend_Mm;
    private DbsField pnd_W_First_Ann_Dod;
    private DbsField pnd_W_Scnd_Ann_Dod;
    private DbsField pnd_W_Pyee_Cde;

    private DbsGroup pnd_W_Pyee_Cde__R_Field_50;
    private DbsField pnd_W_Pyee_Cde_Pnd_W_Pyee_Cde_A;
    private DbsField pnd_W_Mde_Cde;

    private DbsGroup pnd_W_Mde_Cde__R_Field_51;
    private DbsField pnd_W_Mde_Cde_Pnd_W_Mde_1st;
    private DbsField pnd_W_Mde_Cde_Pnd_W_Mde_2nd;

    private DbsGroup pnd_W_Mde_Cde__R_Field_52;
    private DbsField pnd_W_Mde_Cde_Pnd_W_Mde_Cde_A;
    private DbsField pnd_W_Pnd_Cde;
    private DbsField pnd_W_Date_D;
    private DbsField pnd_W_Orign;
    private DbsField pnd_W_Rc;
    private DbsField pnd_W_Cur;
    private DbsField pnd_W_Optn;
    private DbsField pnd_W_Cntrct_Payee;
    private DbsField pnd_W_Save_Cntrct_Pyee;

    private DbsGroup pnd_Total_Amts;
    private DbsField pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt;
    private DbsField pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt;
    private DbsField pnd_Total_Amts_Pnd_Total_Pymt;
    private DbsField pnd_Total_Amts_Pnd_Total_Dpi;
    private DbsField pnd_Total_Amts_Pnd_Total_Due;
    private DbsField pnd_W_Work_Amt;
    private DbsField pnd_W_Exp_Line;
    private DbsField pnd_W_Optn_Cde;
    private DbsField pnd_W_Orgn_Cde;
    private DbsField pnd_W_Final_Dte;
    private DbsField pnd_W_Exception_Msg;
    private DbsField pnd_Fp_Pay_Dte;

    private DbsGroup pnd_Fp_Pay_Dte__R_Field_53;
    private DbsField pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dte;
    private DbsField pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dd;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAnta001 = new PdaAnta001(localVariables);

        // Local Variables

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Ppcn_Nbr);
        pnd_Input__Filler1 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_T_6 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_T_6", "#T-6", FieldType.STRING, 6);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input__Filler2 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input__Filler3 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input__Filler4 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler4", "_FILLER4", FieldType.STRING, 9);
        pnd_Input_Pnd_Rsdncy_At_Issue = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Rsdncy_At_Issue", "#RSDNCY-AT-ISSUE", FieldType.STRING, 3);
        pnd_Input__Filler5 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler5", "_FILLER5", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler6 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler6", "_FILLER6", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler7 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler7", "_FILLER7", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input__Filler8 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler8", "_FILLER8", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input__Filler9 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler9", "_FILLER9", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input__Filler10 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input__Filler10", "_FILLER10", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_W1_Cntrct_Type = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_W1_Cntrct_At_Iss_Re = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_At_Iss_Re", "#W1-CNTRCT-AT-ISS-RE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_5.newFieldArrayInGroup("pnd_Input_Pnd_W1_Cntrct_Fnl_Prm_Dte", "#W1-CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_W1_Cntrct_Strt_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Strt_Dte", "#W1-CNTRCT-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_8 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input__Filler11 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input__Filler11", "_FILLER11", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input__Filler12 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input__Filler12", "_FILLER12", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_11 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input__Filler13 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input__Filler13", "_FILLER13", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_11.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input__Filler14 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input__Filler14", "_FILLER14", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_13 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input__Filler15 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler15", "_FILLER15", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input__Filler16 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler16", "_FILLER16", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input__Filler17 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler17", "_FILLER17", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input__Filler18 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler18", "_FILLER18", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input__Filler19 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler19", "_FILLER19", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input__Filler20 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler20", "_FILLER20", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input__Filler21 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler21", "_FILLER21", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input__Filler22 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler22", "_FILLER22", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_14 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input__Filler23 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler23", "_FILLER23", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input__Filler24 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler24", "_FILLER24", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input__Filler25 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler25", "_FILLER25", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_13.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input__Filler26 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input__Filler26", "_FILLER26", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 132);

        pnd_Header_Term = localVariables.newGroupInRecord("pnd_Header_Term", "#HEADER-TERM");
        pnd_Header_Term_Pnd_Hdr_Term_Lit = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Lit", "#HDR-TERM-LIT", FieldType.STRING, 102);
        pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit", "#HDR-TERM-DTE-LIT", FieldType.STRING, 
            11);
        pnd_Header_Term_Pnd_Hdr_Term_Dte = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Dte", "#HDR-TERM-DTE", FieldType.STRING, 10);

        pnd_Hdr_Tot_Lit_Ia3300m3 = localVariables.newGroupInRecord("pnd_Hdr_Tot_Lit_Ia3300m3", "#HDR-TOT-LIT-IA3300M3");
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Fill1m3_Var = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Fill1m3_Var", "#TOT-FILL1M3-VAR", 
            FieldType.STRING, 13);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Fill1 = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Fill1", "#TOT-HDR1M3-FILL1", 
            FieldType.STRING, 12);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Var = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Var", "#TOT-HDR1M3-VAR", 
            FieldType.STRING, 5);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Prdct = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Prdct", "#TOT-HDR1M3-PRDCT", 
            FieldType.STRING, 4);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdrm3_Var = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdrm3_Var", "#TOT-HDRM3-VAR", 
            FieldType.STRING, 63);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Chk = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Chk", "#TOT-HDR1M3-CHK", 
            FieldType.STRING, 10);
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Date = pnd_Hdr_Tot_Lit_Ia3300m3.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Date", "#TOT-HDR1M3-DATE", 
            FieldType.STRING, 10);

        pnd_Hdr_Wrksht_Ia3300m3 = localVariables.newGroupInRecord("pnd_Hdr_Wrksht_Ia3300m3", "#HDR-WRKSHT-IA3300M3");
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Fill1m3 = pnd_Hdr_Wrksht_Ia3300m3.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Fill1m3", "#WRKSHT-FILL1M3", 
            FieldType.STRING, 30);
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Product = pnd_Hdr_Wrksht_Ia3300m3.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Product", "#WRKSHT-PRODUCT", 
            FieldType.STRING, 4);
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Hdrm3 = pnd_Hdr_Wrksht_Ia3300m3.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Hdrm3", "#WRKSHT-HDRM3", 
            FieldType.STRING, 67);
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Lit1 = pnd_Hdr_Wrksht_Ia3300m3.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Lit1", "#WRKSHT-LIT1", FieldType.STRING, 
            11);
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date = pnd_Hdr_Wrksht_Ia3300m3.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date", "#WRKSHT-DATE", FieldType.STRING, 
            10);

        pnd_Tiaa_Colmn_Hdr1_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr1_Tot", "#TIAA-COLMN-HDR1-TOT");
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill = pnd_Tiaa_Colmn_Hdr1_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill", 
            "#TIAA-COLMN-HDR1-TOT-FILL", FieldType.STRING, 53);
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit = pnd_Tiaa_Colmn_Hdr1_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit", 
            "#TIAA-COLMN-HDR1-TOT-LIT", FieldType.STRING, 11);

        pnd_Tiaa_Colmn_Hdr2_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr2_Tot", "#TIAA-COLMN-HDR2-TOT");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1", 
            "#TIAA-COLMN-HDR2-TOT-LIT1", FieldType.STRING, 8);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1", 
            "#TIAA-COLMN-HDR2-TOT-FILL1", FieldType.STRING, 17);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2", 
            "#TIAA-COLMN-HDR2-TOT-LIT2", FieldType.STRING, 4);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2", 
            "#TIAA-COLMN-HDR2-TOT-FILL2", FieldType.STRING, 19);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3", 
            "#TIAA-COLMN-HDR2-TOT-LIT3", FieldType.STRING, 16);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3", 
            "#TIAA-COLMN-HDR2-TOT-FILL3", FieldType.STRING, 18);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4", 
            "#TIAA-COLMN-HDR2-TOT-LIT4", FieldType.STRING, 26);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte", 
            "#TIAA-COLMN-HDR2-TOT-DTE", FieldType.STRING, 7);

        pnd_Tiaa_Colmn_Hdr3_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr3_Tot", "#TIAA-COLMN-HDR3-TOT");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1", 
            "#TIAA-COLMN-HDR3-TOT-LIT1", FieldType.STRING, 6);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1", 
            "#TIAA-COLMN-HDR3-TOT-FILL1", FieldType.STRING, 9);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2", 
            "#TIAA-COLMN-HDR3-TOT-LIT2", FieldType.STRING, 3);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2", 
            "#TIAA-COLMN-HDR3-TOT-FILL2", FieldType.STRING, 5);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3", 
            "#TIAA-COLMN-HDR3-TOT-LIT3", FieldType.STRING, 4);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4", 
            "#TIAA-COLMN-HDR3-TOT-LIT4", FieldType.STRING, 100);

        pnd_Wrksht_Hdr1 = localVariables.newGroupInRecord("pnd_Wrksht_Hdr1", "#WRKSHT-HDR1");
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1 = pnd_Wrksht_Hdr1.newFieldInGroup("pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1", "#WRKSHT-HDR1-FILL1", FieldType.STRING, 
            43);
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit = pnd_Wrksht_Hdr1.newFieldInGroup("pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit", "#WRKSHT-HDR1-LIT", FieldType.STRING, 
            5);
        pnd_Tiaa_Wrksht_Hdr2 = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Hdr2", "#TIAA-WRKSHT-HDR2", FieldType.STRING, 132);
        pnd_Tiaa_Wrksht_Hdr3 = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Hdr3", "#TIAA-WRKSHT-HDR3", FieldType.STRING, 132);

        pnd_Tiaa_Total_Detail = localVariables.newGroupInRecord("pnd_Tiaa_Total_Detail", "#TIAA-TOTAL-DETAIL");
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit", "#TIAA-TOT-LIT", FieldType.STRING, 
            5);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix", "#TIAA-TOT-CNTRCT-PREFIX", 
            FieldType.STRING, 11);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd", "#TIAA-TOT-FND", FieldType.STRING, 
            1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1", "#TIAA-TOT-FILL1", 
            FieldType.STRING, 6);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde", "#TIAA-TOT-PEND-CDE", 
            FieldType.STRING, 3);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2", "#TIAA-TOT-FILL2", 
            FieldType.STRING, 1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts", "#TIAA-TOT-NUM-CNTRCTS", 
            FieldType.NUMERIC, 7);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks", "#TIAA-TOT-NUM-CHKS", 
            FieldType.NUMERIC, 7);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar", "#TIAA-TOT-CUR-GUAR", 
            FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div", "#TIAA-TOT-CUR-DIV", 
            FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6", "#TIAA-TOT-FILL6", 
            FieldType.STRING, 1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar", "#TIAA-TOT-DUE-GUAR", 
            FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div", "#TIAA-TOT-DUE-DIV", 
            FieldType.NUMERIC, 10, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi", "#TIAA-TOT-DUE-DPI", 
            FieldType.NUMERIC, 10, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total", "#TIAA-TOT-DUE-TOTAL", 
            FieldType.NUMERIC, 12, 2);

        pnd_Tiaa_Wrksht_Detail_Line = localVariables.newGroupInRecord("pnd_Tiaa_Wrksht_Detail_Line", "#TIAA-WRKSHT-DETAIL-LINE");

        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1 = pnd_Tiaa_Wrksht_Detail_Line.newGroupInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1", 
            "#TIAA-WRKSHT-DTL-PART1");
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct", 
            "#TIAA-WRKSHT-DTL-CNTRCT", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee", 
            "#TIAA-WRKSHT-DTL-PYEE", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn", 
            "#TIAA-WRKSHT-DTL-OPTN", FieldType.STRING, 3);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org", 
            "#TIAA-WRKSHT-DTL-ORG", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte", 
            "#TIAA-WRKSHT-DTL-DOD-DTE", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte", 
            "#TIAA-WRKSHT-DTL-FIRST-DTE", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend", 
            "#TIAA-WRKSHT-DTL-PEND", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7", 
            "#TIAA-WRKSHT-DTL-FILL7", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode", 
            "#TIAA-WRKSHT-DTL-MODE", FieldType.STRING, 3);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte", 
            "#TIAA-WRKSHT-DTL-PYMT-DTE", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd", 
            "#TIAA-WRKSHT-DTL-FND", FieldType.STRING, 1);

        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2 = pnd_Tiaa_Wrksht_Detail_Line.newGroupInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2", 
            "#TIAA-WRKSHT-DTL-PART2");
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte", 
            "#TIAA-WRKSHT-DTL-DUE-DTE", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar", 
            "#TIAA-WRKSHT-DTL-GUAR", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11", 
            "#TIAA-WRKSHT-DTL-FILL11", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div", 
            "#TIAA-WRKSHT-DTL-DIV", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt", 
            "#TIAA-WRKSHT-DTL-TOT-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13", 
            "#TIAA-WRKSHT-DTL-FILL13", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact", 
            "#TIAA-WRKSHT-DTL-FACT", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi", 
            "#TIAA-WRKSHT-DTL-DPI", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due", 
            "#TIAA-WRKSHT-DTL-TOT-DUE", FieldType.STRING, 12);
        pnd_Tiaa_Wrksht_Total_Line = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Total_Line", "#TIAA-WRKSHT-TOTAL-LINE", FieldType.STRING, 132);

        pnd_Tiaa_Wrksht_Total_Line__R_Field_15 = localVariables.newGroupInRecord("pnd_Tiaa_Wrksht_Total_Line__R_Field_15", "REDEFINE", pnd_Tiaa_Wrksht_Total_Line);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit", 
            "#TIAA-WRKSHT-TOTAL-LIT", FieldType.STRING, 25);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0", 
            "#TIAA-WRKSHT-TOTAL-FILL0", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1", 
            "#TIAA-WRKSHT-TOTAL-FILL1", FieldType.STRING, 36);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt", 
            "#TIAA-WRKSHT-TOTAL-GUR-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2", 
            "#TIAA-WRKSHT-TOTAL-FILL2", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt", 
            "#TIAA-WRKSHT-TOTAL-DIV-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3", 
            "#TIAA-WRKSHT-TOTAL-FILL3", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt", 
            "#TIAA-WRKSHT-TOTAL-PYMT", FieldType.STRING, 12);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4", 
            "#TIAA-WRKSHT-TOTAL-FILL4", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi", 
            "#TIAA-WRKSHT-TOTAL-DPI", FieldType.STRING, 9);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5 = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5", 
            "#TIAA-WRKSHT-TOTAL-FILL5", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due = pnd_Tiaa_Wrksht_Total_Line__R_Field_15.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due", 
            "#TIAA-WRKSHT-TOTAL-DUE", FieldType.STRING, 12);

        pnd_W_Detail_Tiaa = localVariables.newGroupInRecord("pnd_W_Detail_Tiaa", "#W-DETAIL-TIAA");
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct", "#W-DTL-CNTRCT", FieldType.STRING, 
            8);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee", "#W-DTL-PAYEE", FieldType.STRING, 2);

        pnd_W_Detail_Tiaa__R_Field_16 = pnd_W_Detail_Tiaa.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_16", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee_N = pnd_W_Detail_Tiaa__R_Field_16.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee_N", "#W-DTL-PAYEE-N", FieldType.NUMERIC, 
            2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Iss_Dte = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Iss_Dte", "#W-DTL-ISS-DTE", FieldType.NUMERIC, 
            6);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Rsdnce_Cde = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Rsdnce_Cde", "#W-DTL-RSDNCE-CDE", FieldType.STRING, 
            2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde", "#W-DTL-OPTN-CDE", FieldType.STRING, 
            2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn", "#W-DTL-ORGN", FieldType.NUMERIC, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur", "#W-DTL-CUR", FieldType.STRING, 1);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte", "#W-DTL-1ST-DTE", FieldType.STRING, 
            6);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend", "#W-DTL-PEND", FieldType.STRING, 1);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte", "#W-DTL-PEND-DTE", FieldType.NUMERIC, 
            6);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode", "#W-DTL-MODE", FieldType.STRING, 3);

        pnd_W_Detail_Tiaa__R_Field_17 = pnd_W_Detail_Tiaa.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_17", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode_N = pnd_W_Detail_Tiaa__R_Field_17.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode_N", "#W-DTL-MODE-N", FieldType.NUMERIC, 
            3);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte = pnd_W_Detail_Tiaa.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte", "#W-DTL-FINAL-PAYMT-DTE", 
            FieldType.STRING, 6);

        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table = pnd_W_Detail_Tiaa.newGroupArrayInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table", "#W-DTL-FUND-TABLE", 
            new DbsArrayController(1, 2));
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund", "#W-DTL-FUND", FieldType.STRING, 
            1);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Guar_Due = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Guar_Due", 
            "#W-DTL-CUR-GUAR-DUE", FieldType.NUMERIC, 9, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Div_Due = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Div_Due", "#W-DTL-CUR-DIV-DUE", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymt_Cnt = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymt_Cnt", "#W-DTL-PYMT-CNT", 
            FieldType.PACKED_DECIMAL, 3);

        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newGroupArrayInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due", 
            "#W-DTL-PYMNTS-DUE", new DbsArrayController(1, 20));
        pnd_W_Detail_Tiaa_Pnd_Frm_Yy = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_Yy", "#FRM-YY", FieldType.NUMERIC, 
            4);

        pnd_W_Detail_Tiaa__R_Field_18 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_18", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_Frm_Yy);
        pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A = pnd_W_Detail_Tiaa__R_Field_18.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A", "#FRM-YY-A", FieldType.STRING, 
            4);

        pnd_W_Detail_Tiaa__R_Field_19 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_19", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_Frm_Yy);
        pnd_W_Detail_Tiaa_Pnd_Frm_Cc = pnd_W_Detail_Tiaa__R_Field_19.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_Cc", "#FRM-CC", FieldType.STRING, 2);
        pnd_W_Detail_Tiaa_Pnd_Frm_Y = pnd_W_Detail_Tiaa__R_Field_19.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_Y", "#FRM-Y", FieldType.STRING, 2);
        pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm", "#FRM-1ST-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Detail_Tiaa__R_Field_20 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_20", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm);
        pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm_A = pnd_W_Detail_Tiaa__R_Field_20.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm_A", "#FRM-1ST-MM-A", FieldType.STRING, 
            2);
        pnd_W_Detail_Tiaa_Pnd_To_Yy = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_To_Yy", "#TO-YY", FieldType.NUMERIC, 
            4);

        pnd_W_Detail_Tiaa__R_Field_21 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_21", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_To_Yy);
        pnd_W_Detail_Tiaa_Pnd_To_Cc = pnd_W_Detail_Tiaa__R_Field_21.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_To_Cc", "#TO-CC", FieldType.STRING, 2);
        pnd_W_Detail_Tiaa_Pnd_To_Yy_A = pnd_W_Detail_Tiaa__R_Field_21.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_To_Yy_A", "#TO-YY-A", FieldType.STRING, 2);
        pnd_W_Detail_Tiaa_Pnd_To_Mm = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_To_Mm", "#TO-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Detail_Tiaa__R_Field_22 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_22", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_To_Mm);
        pnd_W_Detail_Tiaa_Pnd_To_Mm_A = pnd_W_Detail_Tiaa__R_Field_22.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_To_Mm_A", "#TO-MM-A", FieldType.STRING, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Num_Chks = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Num_Chks", "#W-DTL-NUM-CHKS", 
            FieldType.NUMERIC, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Guar = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Guar", "#W-DTL-DUE-GUAR", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div", "#W-DTL-DUE-DIV", 
            FieldType.NUMERIC, 9, 2);

        pnd_W_Detail_Tiaa__R_Field_23 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_23", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div);
        pnd_W_Detail_Tiaa_Pnd_W_Alpha_4 = pnd_W_Detail_Tiaa__R_Field_23.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Alpha_4", "#W-ALPHA-4", FieldType.STRING, 
            3);
        pnd_W_Detail_Tiaa_Pnd_W_Alpha_6 = pnd_W_Detail_Tiaa__R_Field_23.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Alpha_6", "#W-ALPHA-6", FieldType.STRING, 
            6);

        pnd_W_Detail_Tiaa__R_Field_24 = pnd_W_Detail_Tiaa__R_Field_23.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_24", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Alpha_6);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div_R = pnd_W_Detail_Tiaa__R_Field_24.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div_R", "#W-DTL-DUE-DIV-R", 
            FieldType.NUMERIC, 6, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Dpi = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Dpi", "#W-DTL-DUE-DPI", 
            FieldType.NUMERIC, 11, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals", "#W-DTL-DUE-TOTALS", 
            FieldType.NUMERIC, 9, 2);

        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.newGroupInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals", "#W-DTL-TOTALS");
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Guar = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Guar", "#W-DTL-TOT-GUAR", 
            FieldType.NUMERIC, 11, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Div = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Div", "#W-DTL-TOT-DIV", 
            FieldType.NUMERIC, 11, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi", "#W-DTL-TOT-DPI", 
            FieldType.NUMERIC, 11, 2);

        pnd_W_Detail_Tiaa__R_Field_25 = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_25", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_4 = pnd_W_Detail_Tiaa__R_Field_25.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_4", "#W-DTL-TOT-ALPHA-4", 
            FieldType.STRING, 4);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_7 = pnd_W_Detail_Tiaa__R_Field_25.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_7", "#W-DTL-TOT-ALPHA-7", 
            FieldType.STRING, 7);

        pnd_W_Detail_Tiaa__R_Field_26 = pnd_W_Detail_Tiaa__R_Field_25.newGroupInGroup("pnd_W_Detail_Tiaa__R_Field_26", "REDEFINE", pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Alpha_7);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi_R = pnd_W_Detail_Tiaa__R_Field_26.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi_R", "#W-DTL-TOT-DPI-R", 
            FieldType.NUMERIC, 7, 2);
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Ovrall_Total = pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.newFieldInGroup("pnd_W_Detail_Tiaa_Pnd_W_Dtl_Ovrall_Total", "#W-DTL-OVRALL-TOTAL", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia3300_Hdr_Lit = localVariables.newFieldInRecord("pnd_Ia3300_Hdr_Lit", "#IA3300-HDR-LIT", FieldType.STRING, 63);
        pnd_Tot_Fill1m2_Lit = localVariables.newFieldInRecord("pnd_Tot_Fill1m2_Lit", "#TOT-FILL1M2-LIT", FieldType.STRING, 13);
        pnd_Tot_Hdr1m2_Lit = localVariables.newFieldInRecord("pnd_Tot_Hdr1m2_Lit", "#TOT-HDR1M2-LIT", FieldType.STRING, 5);
        pnd_Wrksht_Total_Lit = localVariables.newFieldInRecord("pnd_Wrksht_Total_Lit", "#WRKSHT-TOTAL-LIT", FieldType.STRING, 25);

        pnd_W_Tiaa_Total_Table = localVariables.newGroupArrayInRecord("pnd_W_Tiaa_Total_Table", "#W-TIAA-TOTAL-TABLE", new DbsArrayController(1, 8));

        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Curs = pnd_W_Tiaa_Total_Table.newGroupArrayInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Curs", "#W-TIAA-TOT-CURS", 
            new DbsArrayController(1, 2));
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Curs.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd", 
            "#W-TIAA-TOT-DUE-FND", FieldType.STRING, 1);

        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Curs.newGroupArrayInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds", 
            "#W-TIAA-TOT-FLDS", new DbsArrayController(1, 20));
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend", 
            "#W-TIAA-TOT-DUE-PEND", FieldType.STRING, 3);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts", 
            "#W-TIAA-TOT-DUE-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks", 
            "#W-TIAA-TOT-DUE-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar", 
            "#W-TIAA-TOT-DUE-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd", 
            "#W-TIAA-TOT-DUE-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar", 
            "#W-TIAA-TOT-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd", 
            "#W-TIAA-TOT-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi", 
            "#W-TIAA-TOT-DPI", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot", "#W-TIAA-TOT", 
            FieldType.NUMERIC, 12, 2);

        pnd_W_Tiaa_Ttl_Tbl = localVariables.newGroupArrayInRecord("pnd_W_Tiaa_Ttl_Tbl", "#W-TIAA-TTL-TBL", new DbsArrayController(1, 8));

        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds = pnd_W_Tiaa_Ttl_Tbl.newGroupArrayInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds", "#W-TIAA-TTL-FLDS", 
            new DbsArrayController(1, 20));
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend", 
            "#W-TIAA-TTL-DUE-PEND", FieldType.STRING, 3);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts", 
            "#W-TIAA-TTL-DUE-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks", 
            "#W-TIAA-TTL-DUE-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar", 
            "#W-TIAA-TTL-DUE-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd", 
            "#W-TIAA-TTL-DUE-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar", "#W-TIAA-TTL-GUAR", 
            FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd", "#W-TIAA-TTL-DIVD", 
            FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi", "#W-TIAA-TTL-DPI", 
            FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl = pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Flds.newFieldInGroup("pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl", "#W-TIAA-TTL", 
            FieldType.NUMERIC, 12, 2);

        pnd_W_Tiaa_Tot_Ovrall_Totals = localVariables.newGroupArrayInRecord("pnd_W_Tiaa_Tot_Ovrall_Totals", "#W-TIAA-TOT-OVRALL-TOTALS", new DbsArrayController(1, 
            2));
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd = pnd_W_Tiaa_Tot_Ovrall_Totals.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd", 
            "#W-TIAA-TOT-DUE-OVRALL-FND", FieldType.STRING, 1);

        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall = pnd_W_Tiaa_Tot_Ovrall_Totals.newGroupArrayInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall", 
            "#W-TIAA-OVRALL", new DbsArrayController(1, 20));
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend", 
            "#W-TIAA-TOT-DUE-OVRALL-PEND", FieldType.STRING, 3);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts", 
            "#W-TIAA-TOT-DUE-OVRALL-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks", 
            "#W-TIAA-TOT-DUE-OVRALL-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar", 
            "#W-TIAA-TOT-DUE-OVRALL-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd", 
            "#W-TIAA-TOT-DUE-OVRALL-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar", 
            "#W-TIAA-TOT-OVRALL-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd", 
            "#W-TIAA-TOT-OVRALL-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi", 
            "#W-TIAA-TOT-OVRALL-DPI", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot = pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall.newFieldInGroup("pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot", 
            "#W-TIAA-OVRALL-TOT", FieldType.NUMERIC, 12, 2);
        pnd_W_Cntrct = localVariables.newFieldInRecord("pnd_W_Cntrct", "#W-CNTRCT", FieldType.STRING, 10);

        pnd_W_Cntrct__R_Field_27 = localVariables.newGroupInRecord("pnd_W_Cntrct__R_Field_27", "REDEFINE", pnd_W_Cntrct);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3 = pnd_W_Cntrct__R_Field_27.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3", "#PPCN-NBR-A3", FieldType.STRING, 3);

        pnd_W_Cntrct__R_Field_28 = pnd_W_Cntrct__R_Field_27.newGroupInGroup("pnd_W_Cntrct__R_Field_28", "REDEFINE", pnd_W_Cntrct_Pnd_Ppcn_Nbr_A3);
        pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1 = pnd_W_Cntrct__R_Field_28.newFieldInGroup("pnd_W_Cntrct_Pnd_Ppcn_Nbr_A1", "#PPCN-NBR-A1", FieldType.STRING, 1);
        pnd_W_Payee_Cde = localVariables.newFieldInRecord("pnd_W_Payee_Cde", "#W-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Tab_Fund_Table = localVariables.newGroupArrayInRecord("pnd_Tab_Fund_Table", "#TAB-FUND-TABLE", new DbsArrayController(1, 40));
        pnd_Tab_Fund_Table_Pnd_Tab_Fund = pnd_Tab_Fund_Table.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund", "#TAB-FUND", FieldType.STRING, 1);

        pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info = pnd_Tab_Fund_Table.newGroupArrayInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info", "#TAB-FUND-INFO", new DbsArrayController(1, 
            240));
        pnd_Tab_Fund_Table_Pnd_Tab_Install_Date = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Install_Date", "#TAB-INSTALL-DATE", 
            FieldType.STRING, 8);

        pnd_Tab_Fund_Table__R_Field_29 = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newGroupInGroup("pnd_Tab_Fund_Table__R_Field_29", "REDEFINE", pnd_Tab_Fund_Table_Pnd_Tab_Install_Date);
        pnd_Tab_Fund_Table_Pnd_Tab_Install_Date_N = pnd_Tab_Fund_Table__R_Field_29.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Install_Date_N", "#TAB-INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Tab_Fund_Table__R_Field_30 = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newGroupInGroup("pnd_Tab_Fund_Table__R_Field_30", "REDEFINE", pnd_Tab_Fund_Table_Pnd_Tab_Install_Date);
        pnd_Tab_Fund_Table_Pnd_Tab_Install_Yymm = pnd_Tab_Fund_Table__R_Field_30.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Install_Yymm", "#TAB-INSTALL-YYMM", 
            FieldType.STRING, 6);
        pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt", "#TAB-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt", "#TAB-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Fund_Units", "#TAB-FUND-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pnd_Tab_Fund_Table_Pnd_Tab_Auv = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Auv", "#TAB-AUV", FieldType.NUMERIC, 
            9, 4);
        pnd_Tab_Fund_Table_Pnd_Tab_Reval = pnd_Tab_Fund_Table_Pnd_Tab_Fund_Info.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Tab_Reval", "#TAB-REVAL", FieldType.STRING, 
            1);
        pnd_Tab_Fund_Table_Pnd_Table_Cnt = pnd_Tab_Fund_Table.newFieldInGroup("pnd_Tab_Fund_Table_Pnd_Table_Cnt", "#TABLE-CNT", FieldType.NUMERIC, 3);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Bottom_Date = localVariables.newFieldInRecord("pnd_Bottom_Date", "#BOTTOM-DATE", FieldType.NUMERIC, 8);

        pnd_Bottom_Date__R_Field_31 = localVariables.newGroupInRecord("pnd_Bottom_Date__R_Field_31", "REDEFINE", pnd_Bottom_Date);
        pnd_Bottom_Date_Pnd_Bottom_Date_A = pnd_Bottom_Date__R_Field_31.newFieldInGroup("pnd_Bottom_Date_Pnd_Bottom_Date_A", "#BOTTOM-DATE-A", FieldType.STRING, 
            8);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.STRING, 2);

        pnd_Wrksht_File = localVariables.newGroupInRecord("pnd_Wrksht_File", "#WRKSHT-FILE");
        pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee", "#W-WRKSHT-CNTRCT-PAYEE", 
            FieldType.STRING, 10);

        pnd_Wrksht_File__R_Field_32 = pnd_Wrksht_File.newGroupInGroup("pnd_Wrksht_File__R_Field_32", "REDEFINE", pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);
        pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct = pnd_Wrksht_File__R_Field_32.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct", "#W-WRKSHT-CNTRCT", FieldType.STRING, 
            8);
        pnd_Wrksht_File_Pnd_W_Wrksht_Payee = pnd_Wrksht_File__R_Field_32.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Payee", "#W-WRKSHT-PAYEE", FieldType.STRING, 
            2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dod = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dod", "#W-WRKSHT-DOD", FieldType.NUMERIC, 6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Opt = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Opt", "#W-WRKSHT-OPT", FieldType.STRING, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Org = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Org", "#W-WRKSHT-ORG", FieldType.NUMERIC, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Cur = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cur", "#W-WRKSHT-CUR", FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte", "#W-WRKSHT-1ST-DTE", FieldType.STRING, 
            6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Pend = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Pend", "#W-WRKSHT-PEND", FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_Mode = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Mode", "#W-WRKSHT-MODE", FieldType.STRING, 3);
        pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt", "#W-WRKSHT-FINAL-PYMT", FieldType.STRING, 
            6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Fund = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Fund", "#W-WRKSHT-FUND", FieldType.STRING, 1);

        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts = pnd_Wrksht_File.newGroupArrayInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts", "#W-WRKSHT-DUE-PYMTS", 
            new DbsArrayController(1, 240));
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte", "#W-WRKSHT-DUE-DTE", 
            FieldType.STRING, 6);

        pnd_Wrksht_File__R_Field_33 = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newGroupInGroup("pnd_Wrksht_File__R_Field_33", "REDEFINE", pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte);
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy = pnd_Wrksht_File__R_Field_33.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy", "#W-WRKSHT-DUE-YYYY", 
            FieldType.STRING, 4);
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm = pnd_Wrksht_File__R_Field_33.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm", "#W-WRKSHT-DUE-MM", FieldType.STRING, 
            2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Div = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Div", "#W-WRKSHT-DIV", 
            FieldType.NUMERIC, 9, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Guar = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Guar", "#W-WRKSHT-GUAR", 
            FieldType.NUMERIC, 9, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Units = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Units", "#W-WRKSHT-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pnd_Wrksht_File_Pnd_W_Wrksht_Reval = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Reval", "#W-WRKSHT-REVAL", 
            FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_Auv = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Auv", "#W-WRKSHT-AUV", 
            FieldType.NUMERIC, 9, 4);
        pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt", "#W-WRKSHT-TOT-PYMT", 
            FieldType.NUMERIC, 10, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact", "#W-WRKSHT-DPI-FACT", 
            FieldType.NUMERIC, 6, 5);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt", "#W-WRKSHT-DPI-AMT", 
            FieldType.NUMERIC, 7, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Total = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Total", "#W-WRKSHT-TOTAL", 
            FieldType.NUMERIC, 11, 2);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", 
            "NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn = naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn", 
            "NAZ-TBL-SCRN-FLD-DSCRPTN", FieldType.STRING, 35, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.setDdmHeader("HD = FIELD");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_34 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_34", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_34.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_34.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_34.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_34.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);

        pnd_Ndxs_For_Tiaa_Total_Table = localVariables.newGroupInRecord("pnd_Ndxs_For_Tiaa_Total_Table", "#NDXS-FOR-TIAA-TOTAL-TABLE");
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx", "#GA-0L-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx", "#GW-0M-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx", "#IA-0N-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx", "#IP-6L-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx", "#S0-6M-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx", "#W0-6N-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx", "#W0-GRP-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx", "#Z0-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx", "#Y0-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Max_Wrk = localVariables.newFieldInRecord("pnd_Max_Wrk", "#MAX-WRK", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Pend = localVariables.newFieldInRecord("pnd_Max_Pend", "#MAX-PEND", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Detail_Tbl = localVariables.newFieldInRecord("pnd_Max_Detail_Tbl", "#MAX-DETAIL-TBL", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Tiaa_Fnd = localVariables.newFieldInRecord("pnd_Max_Tiaa_Fnd", "#MAX-TIAA-FND", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Funds = localVariables.newFieldInRecord("pnd_Max_Funds", "#MAX-FUNDS", FieldType.PACKED_DECIMAL, 3);
        pnd_Single_Life = localVariables.newFieldArrayInRecord("pnd_Single_Life", "#SINGLE-LIFE", FieldType.STRING, 2, new DbsArrayController(1, 10));
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Processed_Cnt = localVariables.newFieldInRecord("pnd_Processed_Cnt", "#PROCESSED-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.PACKED_DECIMAL, 3);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.PACKED_DECIMAL, 3);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.PACKED_DECIMAL, 3);
        pnd_H = localVariables.newFieldInRecord("pnd_H", "#H", FieldType.PACKED_DECIMAL, 3);
        pnd_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Fnd_Cnt", "#FND-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx1 = localVariables.newFieldInRecord("pnd_Tot_Ndx1", "#TOT-NDX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx2 = localVariables.newFieldInRecord("pnd_Tot_Ndx2", "#TOT-NDX2", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx3 = localVariables.newFieldInRecord("pnd_Tot_Ndx3", "#TOT-NDX3", FieldType.PACKED_DECIMAL, 3);
        pnd_Fnd_Ndx = localVariables.newFieldInRecord("pnd_Fnd_Ndx", "#FND-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Naz_Cnt = localVariables.newFieldInRecord("pnd_Naz_Cnt", "#NAZ-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Naz_Written = localVariables.newFieldInRecord("pnd_Naz_Written", "#NAZ-WRITTEN", FieldType.PACKED_DECIMAL, 5);
        pnd_Excp_Cnt = localVariables.newFieldInRecord("pnd_Excp_Cnt", "#EXCP-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Rec10_Cnt = localVariables.newFieldInRecord("pnd_Rec10_Cnt", "#REC10-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Var = localVariables.newFieldInRecord("pnd_Tiaa_Var", "#TIAA-VAR", FieldType.BOOLEAN, 1);
        pnd_Term = localVariables.newFieldInRecord("pnd_Term", "#TERM", FieldType.BOOLEAN, 1);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Process = localVariables.newFieldInRecord("pnd_Process", "#PROCESS", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Detail = localVariables.newFieldInRecord("pnd_Detail", "#DETAIL", FieldType.BOOLEAN, 1);
        pnd_Totals = localVariables.newFieldInRecord("pnd_Totals", "#TOTALS", FieldType.BOOLEAN, 1);
        pnd_Wrksht = localVariables.newFieldInRecord("pnd_Wrksht", "#WRKSHT", FieldType.BOOLEAN, 1);
        pnd_Limited = localVariables.newFieldInRecord("pnd_Limited", "#LIMITED", FieldType.BOOLEAN, 1);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);

        pnd_W_Issue_Date__R_Field_35 = localVariables.newGroupInRecord("pnd_W_Issue_Date__R_Field_35", "REDEFINE", pnd_W_Issue_Date);
        pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm = pnd_W_Issue_Date__R_Field_35.newFieldInGroup("pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm", "#W-ISSUE-DATE-YYMM", 
            FieldType.NUMERIC, 6);
        pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd = pnd_W_Issue_Date__R_Field_35.newFieldInGroup("pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd", "#W-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_W_Work_Date = localVariables.newFieldInRecord("pnd_W_Work_Date", "#W-WORK-DATE", FieldType.NUMERIC, 8);

        pnd_W_Work_Date__R_Field_36 = localVariables.newGroupInRecord("pnd_W_Work_Date__R_Field_36", "REDEFINE", pnd_W_Work_Date);
        pnd_W_Work_Date_Pnd_W_Work_Date_A = pnd_W_Work_Date__R_Field_36.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Date_A", "#W-WORK-DATE-A", FieldType.STRING, 
            8);

        pnd_W_Work_Date__R_Field_37 = localVariables.newGroupInRecord("pnd_W_Work_Date__R_Field_37", "REDEFINE", pnd_W_Work_Date);
        pnd_W_Work_Date_Pnd_W_Work_Yyyymm = pnd_W_Work_Date__R_Field_37.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Yyyymm", "#W-WORK-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_W_Work_Date_Pnd_W_Work_Dd = pnd_W_Work_Date__R_Field_37.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Dd", "#W-WORK-DD", FieldType.NUMERIC, 
            2);
        pnd_W_Install_Date = localVariables.newFieldInRecord("pnd_W_Install_Date", "#W-INSTALL-DATE", FieldType.STRING, 8);

        pnd_W_Install_Date__R_Field_38 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_38", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Yyyy = pnd_W_Install_Date__R_Field_38.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yyyy", "#W-INSTALL-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_W_Install_Date__R_Field_39 = pnd_W_Install_Date__R_Field_38.newGroupInGroup("pnd_W_Install_Date__R_Field_39", "REDEFINE", pnd_W_Install_Date_Pnd_W_Install_Yyyy);
        pnd_W_Install_Date_Pnd_W_Install_Cc = pnd_W_Install_Date__R_Field_39.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Cc", "#W-INSTALL-CC", FieldType.NUMERIC, 
            2);
        pnd_W_Install_Date_Pnd_W_Install_Yy = pnd_W_Install_Date__R_Field_39.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yy", "#W-INSTALL-YY", FieldType.NUMERIC, 
            2);

        pnd_W_Install_Date__R_Field_40 = pnd_W_Install_Date__R_Field_38.newGroupInGroup("pnd_W_Install_Date__R_Field_40", "REDEFINE", pnd_W_Install_Date_Pnd_W_Install_Yyyy);
        pnd_W_Install_Date_Pnd_W_Install_Yyyy_A = pnd_W_Install_Date__R_Field_40.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yyyy_A", "#W-INSTALL-YYYY-A", 
            FieldType.STRING, 4);
        pnd_W_Install_Date_Pnd_W_Install_Mm = pnd_W_Install_Date__R_Field_38.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Mm", "#W-INSTALL-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Install_Date__R_Field_41 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_41", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Date_N = pnd_W_Install_Date__R_Field_41.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Date_N", "#W-INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_W_Install_Date__R_Field_42 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_42", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm = pnd_W_Install_Date__R_Field_42.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm", 
            "#W-INSTALL-DATE-YYYYMM", FieldType.STRING, 6);
        pnd_W_Chk_Dte = localVariables.newFieldInRecord("pnd_W_Chk_Dte", "#W-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_W_Chk_Dte__R_Field_43 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_43", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A = pnd_W_Chk_Dte__R_Field_43.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A", "#W-CHK-DTE-A", FieldType.STRING, 8);

        pnd_W_Chk_Dte__R_Field_44 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_44", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy = pnd_W_Chk_Dte__R_Field_44.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy", "#W-CHK-YYYY", FieldType.STRING, 4);

        pnd_W_Chk_Dte__R_Field_45 = pnd_W_Chk_Dte__R_Field_44.newGroupInGroup("pnd_W_Chk_Dte__R_Field_45", "REDEFINE", pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N = pnd_W_Chk_Dte__R_Field_45.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N", "#W-CHK-YYYY-N", FieldType.NUMERIC, 
            4);
        pnd_W_Chk_Dte_Pnd_W_Chk_Mm = pnd_W_Chk_Dte__R_Field_44.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Mm", "#W-CHK-MM", FieldType.STRING, 2);

        pnd_W_Chk_Dte__R_Field_46 = pnd_W_Chk_Dte__R_Field_44.newGroupInGroup("pnd_W_Chk_Dte__R_Field_46", "REDEFINE", pnd_W_Chk_Dte_Pnd_W_Chk_Mm);
        pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N = pnd_W_Chk_Dte__R_Field_46.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N", "#W-CHK-MM-N", FieldType.NUMERIC, 2);

        pnd_W_Chk_Dte__R_Field_47 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_47", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm = pnd_W_Chk_Dte__R_Field_47.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm", "#W-CHK-YYYYMM", FieldType.STRING, 
            6);

        pnd_W_Chk_Dte__R_Field_48 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_48", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm_N = pnd_W_Chk_Dte__R_Field_48.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm_N", "#W-CHK-YYYYMM-N", FieldType.NUMERIC, 
            6);
        pnd_W_Pend_Date = localVariables.newFieldInRecord("pnd_W_Pend_Date", "#W-PEND-DATE", FieldType.NUMERIC, 6);

        pnd_W_Pend_Date__R_Field_49 = localVariables.newGroupInRecord("pnd_W_Pend_Date__R_Field_49", "REDEFINE", pnd_W_Pend_Date);
        pnd_W_Pend_Date_Pnd_W_Pend_Yyyy = pnd_W_Pend_Date__R_Field_49.newFieldInGroup("pnd_W_Pend_Date_Pnd_W_Pend_Yyyy", "#W-PEND-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Pend_Date_Pnd_W_Pend_Mm = pnd_W_Pend_Date__R_Field_49.newFieldInGroup("pnd_W_Pend_Date_Pnd_W_Pend_Mm", "#W-PEND-MM", FieldType.NUMERIC, 
            2);
        pnd_W_First_Ann_Dod = localVariables.newFieldInRecord("pnd_W_First_Ann_Dod", "#W-FIRST-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_W_Scnd_Ann_Dod = localVariables.newFieldInRecord("pnd_W_Scnd_Ann_Dod", "#W-SCND-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_W_Pyee_Cde = localVariables.newFieldInRecord("pnd_W_Pyee_Cde", "#W-PYEE-CDE", FieldType.NUMERIC, 2);

        pnd_W_Pyee_Cde__R_Field_50 = localVariables.newGroupInRecord("pnd_W_Pyee_Cde__R_Field_50", "REDEFINE", pnd_W_Pyee_Cde);
        pnd_W_Pyee_Cde_Pnd_W_Pyee_Cde_A = pnd_W_Pyee_Cde__R_Field_50.newFieldInGroup("pnd_W_Pyee_Cde_Pnd_W_Pyee_Cde_A", "#W-PYEE-CDE-A", FieldType.STRING, 
            2);
        pnd_W_Mde_Cde = localVariables.newFieldInRecord("pnd_W_Mde_Cde", "#W-MDE-CDE", FieldType.NUMERIC, 3);

        pnd_W_Mde_Cde__R_Field_51 = localVariables.newGroupInRecord("pnd_W_Mde_Cde__R_Field_51", "REDEFINE", pnd_W_Mde_Cde);
        pnd_W_Mde_Cde_Pnd_W_Mde_1st = pnd_W_Mde_Cde__R_Field_51.newFieldInGroup("pnd_W_Mde_Cde_Pnd_W_Mde_1st", "#W-MDE-1ST", FieldType.NUMERIC, 1);
        pnd_W_Mde_Cde_Pnd_W_Mde_2nd = pnd_W_Mde_Cde__R_Field_51.newFieldInGroup("pnd_W_Mde_Cde_Pnd_W_Mde_2nd", "#W-MDE-2ND", FieldType.NUMERIC, 2);

        pnd_W_Mde_Cde__R_Field_52 = localVariables.newGroupInRecord("pnd_W_Mde_Cde__R_Field_52", "REDEFINE", pnd_W_Mde_Cde);
        pnd_W_Mde_Cde_Pnd_W_Mde_Cde_A = pnd_W_Mde_Cde__R_Field_52.newFieldInGroup("pnd_W_Mde_Cde_Pnd_W_Mde_Cde_A", "#W-MDE-CDE-A", FieldType.STRING, 3);
        pnd_W_Pnd_Cde = localVariables.newFieldInRecord("pnd_W_Pnd_Cde", "#W-PND-CDE", FieldType.STRING, 1);
        pnd_W_Date_D = localVariables.newFieldInRecord("pnd_W_Date_D", "#W-DATE-D", FieldType.DATE);
        pnd_W_Orign = localVariables.newFieldInRecord("pnd_W_Orign", "#W-ORIGN", FieldType.NUMERIC, 2);
        pnd_W_Rc = localVariables.newFieldInRecord("pnd_W_Rc", "#W-RC", FieldType.NUMERIC, 2);
        pnd_W_Cur = localVariables.newFieldInRecord("pnd_W_Cur", "#W-CUR", FieldType.STRING, 1);
        pnd_W_Optn = localVariables.newFieldInRecord("pnd_W_Optn", "#W-OPTN", FieldType.STRING, 2);
        pnd_W_Cntrct_Payee = localVariables.newFieldInRecord("pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", FieldType.STRING, 10);
        pnd_W_Save_Cntrct_Pyee = localVariables.newFieldInRecord("pnd_W_Save_Cntrct_Pyee", "#W-SAVE-CNTRCT-PYEE", FieldType.STRING, 10);

        pnd_Total_Amts = localVariables.newGroupInRecord("pnd_Total_Amts", "#TOTAL-AMTS");
        pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt", "#TIAA-TOTAL-DIV-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt", "#TIAA-TOTAL-GUR-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Total_Amts_Pnd_Total_Pymt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Pymt", "#TOTAL-PYMT", FieldType.NUMERIC, 10, 2);
        pnd_Total_Amts_Pnd_Total_Dpi = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Dpi", "#TOTAL-DPI", FieldType.NUMERIC, 8, 2);
        pnd_Total_Amts_Pnd_Total_Due = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Due", "#TOTAL-DUE", FieldType.NUMERIC, 11, 2);
        pnd_W_Work_Amt = localVariables.newFieldInRecord("pnd_W_Work_Amt", "#W-WORK-AMT", FieldType.NUMERIC, 10, 2);
        pnd_W_Exp_Line = localVariables.newFieldInRecord("pnd_W_Exp_Line", "#W-EXP-LINE", FieldType.STRING, 132);
        pnd_W_Optn_Cde = localVariables.newFieldInRecord("pnd_W_Optn_Cde", "#W-OPTN-CDE", FieldType.STRING, 2);
        pnd_W_Orgn_Cde = localVariables.newFieldInRecord("pnd_W_Orgn_Cde", "#W-ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_W_Final_Dte = localVariables.newFieldInRecord("pnd_W_Final_Dte", "#W-FINAL-DTE", FieldType.STRING, 6);
        pnd_W_Exception_Msg = localVariables.newFieldInRecord("pnd_W_Exception_Msg", "#W-EXCEPTION-MSG", FieldType.STRING, 80);
        pnd_Fp_Pay_Dte = localVariables.newFieldInRecord("pnd_Fp_Pay_Dte", "#FP-PAY-DTE", FieldType.NUMERIC, 8);

        pnd_Fp_Pay_Dte__R_Field_53 = localVariables.newGroupInRecord("pnd_Fp_Pay_Dte__R_Field_53", "REDEFINE", pnd_Fp_Pay_Dte);
        pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dte = pnd_Fp_Pay_Dte__R_Field_53.newFieldInGroup("pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dte", "#FIN-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dd = pnd_Fp_Pay_Dte__R_Field_53.newFieldInGroup("pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dd", "#FIN-PER-PAY-DD", FieldType.NUMERIC, 
            2);
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
        pnd_Header_Term_Pnd_Hdr_Term_Lit.setInitialValue("CONTRACTS TERMINATING THIS MONTH BECAUSE OF EXPIRY OF FINAL PERIODIC OR IRREGULAR PAYMENT DATE");
        pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit.setInitialValue("CHECK DATE");
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Chk.setInitialValue("CHECK DATE");
        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Lit1.setInitialValue("CHECK DATE ");
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit.setInitialValue("PAYMENT DUE");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1.setInitialValue("CONTRACT");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2.setInitialValue("PEND");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3.setInitialValue("IN CURRENT MONTH");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4.setInitialValue("-- TOTAL PAYMENTS DUE THRU");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1.setInitialValue("NUMBER");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2.setInitialValue("FND");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3.setInitialValue("CODE");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4.setInitialValue("#CONTRACTS    #CKS  ---GUAR---  ---DIVD---      ---GUAR---   ---DIVD---    ---DPI---    ---TOTAL---");
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit.setInitialValue("FINAL");
        pnd_Tiaa_Wrksht_Hdr2.setInitialValue("CONTRACT                    FIRST PEND      PAYT        DUE    GUARANTEED     DIVIDEND      TOTAL     DPI/DCI      DPI/");
        pnd_Tiaa_Wrksht_Hdr3.setInitialValue("NUMBER   PY OPT ORG DOD     DATE  CODE MODE DATE   FND  DATE      PAYMENT     PAYMENT      PAYMENT     FACTOR      DCI    TOTAL DUE");
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit.setInitialValue("TOTAL");
        pnd_Ia3300_Hdr_Lit.setInitialValue("NON-PENSION PENDED PAYMENT AND DPI LIABILITY");
        pnd_Tot_Fill1m2_Lit.setInitialValue("WS - 6: ATT-3");
        pnd_Tot_Hdr1m2_Lit.setInitialValue("TOTAL");
        pnd_Wrksht_Total_Lit.setInitialValue("TOTALS FOR CONTRACT/PAYEE");
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx.setInitialValue(1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx.setInitialValue(2);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx.setInitialValue(3);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx.setInitialValue(4);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx.setInitialValue(5);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx.setInitialValue(6);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx.setInitialValue(7);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx.setInitialValue(7);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx.setInitialValue(8);
        pnd_Max_Wrk.setInitialValue(240);
        pnd_Max_Pend.setInitialValue(20);
        pnd_Max_Detail_Tbl.setInitialValue(20);
        pnd_Max_Tiaa_Fnd.setInitialValue(2);
        pnd_Max_Funds.setInitialValue(40);
        pnd_Single_Life.getValue(1).setInitialValue("01");
        pnd_Single_Life.getValue(2).setInitialValue("02");
        pnd_Single_Life.getValue(3).setInitialValue("05");
        pnd_Single_Life.getValue(4).setInitialValue("06");
        pnd_Single_Life.getValue(5).setInitialValue("09");
        pnd_Single_Life.getValue(6).setInitialValue("19");
        pnd_Single_Life.getValue(7).setInitialValue("21");
        pnd_Single_Life.getValue(8).setInitialValue("22");
        pnd_Single_Life.getValue(9).setInitialValue("28");
        pnd_Single_Life.getValue(10).setInitialValue("30");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3320() throws Exception
    {
        super("Iaap3320");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  MAKE SURE TABLE IS 'CLEAN'                                                                                                                                   //Natural: FORMAT ( 1 ) PS = 60 LS = 133
                                                                                                                                                                          //Natural: PERFORM DELETE-FROM-TABLE
        sub_Delete_From_Table();
        if (condition(Global.isEscape())) {return;}
        //*  062019
        pnd_Naz_Cnt.reset();                                                                                                                                              //Natural: RESET #NAZ-CNT #NAZ-WRITTEN
        pnd_Naz_Written.reset();
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Fill1m3_Var.setValue(pnd_Tot_Fill1m2_Lit);                                                                                       //Natural: ASSIGN #TOT-FILL1M3-VAR := #TOT-FILL1M2-LIT
        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Var.setValue(pnd_Tot_Hdr1m2_Lit);                                                                                         //Natural: ASSIGN #TOT-HDR1M3-VAR := #TOT-HDR1M2-LIT
        pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dd.setValue(1);                                                                                                                    //Natural: ASSIGN #FIN-PER-PAY-DD := 01
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                                   //Natural: IF #INPUT.#PPCN-NBR = '   CHEADER'
            {
                pnd_Check_Date.setValue(pnd_Input_Pnd_Header_Chk_Dte);                                                                                                    //Natural: ASSIGN #CHECK-DATE := #W-CHK-DTE := #INPUT.#HEADER-CHK-DTE
                pnd_W_Chk_Dte.setValue(pnd_Input_Pnd_Header_Chk_Dte);
                pnd_W_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A);                                                                //Natural: MOVE EDITED #W-CHK-DTE-A TO #W-DATE-D ( EM = YYYYMMDD )
                pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date.setValueEdited(pnd_W_Date_D,new ReportEditMask("MM/DD/YYYY"));                                                    //Natural: MOVE EDITED #W-DATE-D ( EM = MM/DD/YYYY ) TO #WRKSHT-DATE
                pnd_Header_Term_Pnd_Hdr_Term_Dte.setValue(pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date);                                                                       //Natural: ASSIGN #HDR-TERM-DTE := #TOT-HDR1M3-DATE := #WRKSHT-DATE
                pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Date.setValue(pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date);
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
            short decideConditionsMet675 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RECORD-CODE = 10
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))
            {
                decideConditionsMet675++;
                pnd_First_Time.setValue(true);                                                                                                                            //Natural: ASSIGN #FIRST-TIME := TRUE
                if (condition(! (pnd_Process.getBoolean())))                                                                                                              //Natural: IF NOT #PROCESS
                {
                    pnd_W_Detail_Tiaa.reset();                                                                                                                            //Natural: RESET #W-DETAIL-TIAA
                    //*  PROCESS THE PREVIOUS CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Processed_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #PROCESSED-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-PAYEE
                    sub_Process_Payee();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_W_Detail_Tiaa.reset();                                                                                                                            //Natural: RESET #W-DETAIL-TIAA
                    pnd_Process.setValue(false);                                                                                                                          //Natural: ASSIGN #PROCESS := FALSE
                    //*        ADD 1 TO #REC10-CNT       /* TEMPOS
                    //*        IF #REC10-CNT GT 1  /* TEMPOS
                    //*          ESCAPE BOTTOM     /* TEMPOS
                    //*        END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Cntrct.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                                            //Natural: ASSIGN #W-CNTRCT := #INPUT.#PPCN-NBR
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn.setValue(pnd_Input_Pnd_Orgn_Cde);                                                                                        //Natural: ASSIGN #W-DTL-ORGN := #W-ORIGN := #INPUT.#ORGN-CDE
                pnd_W_Orign.setValue(pnd_Input_Pnd_Orgn_Cde);
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Iss_Dte.setValue(pnd_Input_Pnd_Issue_Dte);                                                                                    //Natural: ASSIGN #W-DTL-ISS-DTE := #INPUT.#ISSUE-DTE
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct.setValue(pnd_Input_Pnd_Cntrct_Payee.getSubstring(1,8));                                                                //Natural: ASSIGN #W-DTL-CNTRCT := SUBSTR ( #INPUT.#CNTRCT-PAYEE,1,8 )
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde.setValueEdited(pnd_Input_Pnd_Optn_Cde,new ReportEditMask("99"));                                                     //Natural: MOVE EDITED #INPUT.#OPTN-CDE ( EM = 99 ) TO #W-DTL-OPTN-CDE
                pnd_W_Optn.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde);                                                                                                //Natural: ASSIGN #W-OPTN := #W-DTL-OPTN-CDE
                pnd_W_Cur.setValue(pnd_Input_Pnd_Crrncy_Cde_A);                                                                                                           //Natural: ASSIGN #W-CUR := #W-DTL-CUR := #INPUT.#CRRNCY-CDE-A
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur.setValue(pnd_Input_Pnd_Crrncy_Cde_A);
                pnd_W_First_Ann_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                //Natural: ASSIGN #W-FIRST-ANN-DOD := #INPUT.#FIRST-ANN-DOD
                pnd_W_Scnd_Ann_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                  //Natural: ASSIGN #W-SCND-ANN-DOD := #INPUT.#SCND-ANN-DOD
            }                                                                                                                                                             //Natural: WHEN #INPUT.#RECORD-CODE = 20
            else if (condition(pnd_Input_Pnd_Record_Code.equals(20)))
            {
                decideConditionsMet675++;
                pnd_Fnd_Cnt.reset();                                                                                                                                      //Natural: RESET #FND-CNT
                if (condition(pnd_First_Time.getBoolean()))                                                                                                               //Natural: IF #FIRST-TIME
                {
                    pnd_First_Time.setValue(false);                                                                                                                       //Natural: ASSIGN #FIRST-TIME := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Pnd_Payee_Cde_A.notEquals(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee)))                                                                //Natural: IF #INPUT.#PAYEE-CDE-A NE #W-DTL-PAYEE
                    {
                        if (condition(! (pnd_Process.getBoolean())))                                                                                                      //Natural: IF NOT #PROCESS
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Processed_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PROCESSED-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-PAYEE
                        sub_Process_Payee();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund_Table.getValue("*").reset();                                                                                     //Natural: RESET #W-DTL-FUND-TABLE ( * )
                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct.setValue(pnd_W_Cntrct.getSubstring(1,8));                                                                      //Natural: ASSIGN #W-DTL-CNTRCT := SUBSTR ( #W-CNTRCT,1,8 )
                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde.setValue(pnd_W_Optn);                                                                                        //Natural: ASSIGN #W-DTL-OPTN-CDE := #W-OPTN
                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur.setValue(pnd_W_Cur);                                                                                              //Natural: ASSIGN #W-DTL-CUR := #W-CUR
                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn.setValue(pnd_W_Orign);                                                                                           //Natural: ASSIGN #W-DTL-ORGN := #W-ORIGN
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee.setValue(pnd_Input_Pnd_Payee_Cde_A);                                                                                    //Natural: ASSIGN #W-DTL-PAYEE := #INPUT.#PAYEE-CDE-A
                pnd_W_Payee_Cde.setValue(pnd_Input_Pnd_Payee_Cde);                                                                                                        //Natural: ASSIGN #W-PAYEE-CDE := #INPUT.#PAYEE-CDE
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee.setValue(pnd_Input_Pnd_Payee_Cde_A);                                                                                    //Natural: ASSIGN #W-DTL-PAYEE := #INPUT.#PAYEE-CDE-A
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Rsdnce_Cde.setValue(pnd_Input_Pnd_Rsdncy_Cde.getSubstring(2,2));                                                              //Natural: ASSIGN #W-DTL-RSDNCE-CDE := SUBSTR ( #INPUT.#RSDNCY-CDE,2,2 )
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend.setValue(pnd_Input_Pnd_Pend_Cde);                                                                                        //Natural: ASSIGN #W-DTL-PEND := #INPUT.#PEND-CDE
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode.setValueEdited(pnd_Input_Pnd_Mode_Ind,new ReportEditMask("999"));                                                        //Natural: MOVE EDITED #INPUT.#MODE-IND ( EM = 999 ) TO #W-DTL-MODE
                //*  062019
                //*  REC 30
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte.setValueEdited(pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte,new ReportEditMask("999999"));                            //Natural: MOVE EDITED #INPUT.#CNTRCT-FIN-PER-PAY-DTE ( EM = 999999 ) TO #W-DTL-FINAL-PAYMT-DTE
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte.setValue(pnd_Input_Pnd_Pend_Dte);                                                                                    //Natural: ASSIGN #W-DTL-PEND-DTE := #INPUT.#PEND-DTE
                pnd_Fp_Pay_Dte_Pnd_Fin_Per_Pay_Dte.setValue(pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte);                                                                        //Natural: ASSIGN #FIN-PER-PAY-DTE := #CNTRCT-FIN-PER-PAY-DTE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (condition(! (pnd_Input_Pnd_Summ_Cmpny_Cde.equals("T"))))                                                                                              //Natural: IF NOT #INPUT.#SUMM-CMPNY-CDE = 'T'
                {
                    //*  ONLY TIAA FOR FIRST PORTION OF THE REPORT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Process.setValue(true);                                                                                                                               //Natural: ASSIGN #PROCESS := TRUE
                pnd_Fnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #FND-CNT
                if (condition(pnd_Fnd_Cnt.greater(2)))                                                                                                                    //Natural: IF #FND-CNT GT 2
                {
                    getReports().write(0, "LOGIC ERROR",pnd_W_Cntrct);                                                                                                    //Natural: WRITE 'LOGIC ERROR' #W-CNTRCT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Summ_Fund_Cde.equals("1S") || pnd_Input_Pnd_Summ_Fund_Cde.equals("1 ")))                                                      //Natural: IF #INPUT.#SUMM-FUND-CDE = '1S' OR = '1 '
                {
                    pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund.getValue(pnd_Fnd_Cnt).setValue("T");                                                                                 //Natural: ASSIGN #W-DTL-FUND ( #FND-CNT ) := 'T'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund.getValue(pnd_Fnd_Cnt).setValue("G");                                                                                 //Natural: ASSIGN #W-DTL-FUND ( #FND-CNT ) := 'G'
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Guar_Due.getValue(pnd_Fnd_Cnt).setValue(pnd_Input_Pnd_Summ_Per_Pymnt);                                                    //Natural: ASSIGN #W-DTL-CUR-GUAR-DUE ( #FND-CNT ) := #INPUT.#SUMM-PER-PYMNT
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur_Div_Due.getValue(pnd_Fnd_Cnt).setValue(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                     //Natural: ASSIGN #W-DTL-CUR-DIV-DUE ( #FND-CNT ) := #INPUT.#SUMM-PER-DVDND
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  PROCESS THE LAST CONTRACT
        if (condition(pnd_Process.getBoolean()))                                                                                                                          //Natural: IF #PROCESS
        {
            pnd_Processed_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PROCESSED-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-PAYEE
            sub_Process_Payee();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Processed_Cnt.greater(getZero())))                                                                                                              //Natural: IF #PROCESSED-CNT GT 0
        {
            pnd_Wrksht.reset();                                                                                                                                           //Natural: RESET #WRKSHT
            getWorkFiles().close(1);                                                                                                                                      //Natural: CLOSE WORK FILE 1
            getWorkFiles().close(2);                                                                                                                                      //Natural: CLOSE WORK FILE 2
            pnd_Wrksht.setValue(true);                                                                                                                                    //Natural: ASSIGN #WRKSHT := TRUE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-WORKSHEET
            sub_Process_Tiaa_Worksheet();
            if (condition(Global.isEscape())) {return;}
            pnd_Totals.setValue(true);                                                                                                                                    //Natural: ASSIGN #TOTALS := TRUE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-TOTALS
            sub_Print_Tiaa_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Term.setValue(true);                                                                                                                                      //Natural: ASSIGN #TERM := TRUE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            if (condition(pnd_Naz_Cnt.greater(getZero())))                                                                                                                //Natural: IF #NAZ-CNT GT 0
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Naz_Written.greater(getZero())))                                                                                                            //Natural: IF #NAZ-WRITTEN GT 0
            {
                getReports().write(0, "EXPIRING CONTRACTS WRITTEN =",pnd_Naz_Written);                                                                                    //Natural: WRITE 'EXPIRING CONTRACTS WRITTEN =' #NAZ-WRITTEN
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-EXPIRING-CONTRACTS-REPORT
                sub_Write_Expiring_Contracts_Report();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DELETE-FROM-TABLE
                sub_Delete_From_Table();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "EXPIRING CONTRACTS DELETED =",pnd_Naz_Written);                                                                                    //Natural: WRITE 'EXPIRING CONTRACTS DELETED =' #NAZ-WRITTEN
                if (Global.isEscape()) return;
                pnd_Naz_Cnt.reset();                                                                                                                                      //Natural: RESET #NAZ-CNT #NAZ-WRITTEN
                pnd_Naz_Written.reset();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, pnd_Header_Term_Pnd_Hdr_Term_Lit,pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit,pnd_Header_Term_Pnd_Hdr_Term_Dte);                            //Natural: WRITE ( 1 ) #HEADER-TERM
                if (Global.isEscape()) return;
                pnd_W_Exp_Line.setValue("NO EXPIRING CONTRACTS THIS MONTH");                                                                                              //Natural: ASSIGN #W-EXP-LINE := 'NO EXPIRING CONTRACTS THIS MONTH'
                getReports().write(1, pnd_W_Exp_Line);                                                                                                                    //Natural: WRITE ( 1 ) #W-EXP-LINE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, "NO PENDED TIAA NON-PENSION CONTRACTS PROCESSED");                                                                                      //Natural: WRITE ( 1 ) 'NO PENDED TIAA NON-PENSION CONTRACTS PROCESSED'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "TOTAL RECORDS PROCESSED :",pnd_Processed_Cnt);                                                                                             //Natural: WRITE 'TOTAL RECORDS PROCESSED :' #PROCESSED-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL EXCEPTIONS WRITTEN:",pnd_Excp_Cnt);                                                                                                  //Natural: WRITE 'TOTAL EXCEPTIONS WRITTEN:' #EXCP-CNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-FROM-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DCI-DPI
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PAYMENTS-DUE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PEND-DTE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DETAIL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DTL-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DETAIL-TIAA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TIAA-TOTALS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TIAA-WRKSHT-CONTRACT-TOTALS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PAYEE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-WORKSHEET
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXCEPTIONS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXPIRING-CONTRACTS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXPIRING-CONTRACTS-REPORT
    }
    private void sub_Delete_From_Table() throws Exception                                                                                                                 //Natural: DELETE-FROM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Naz_Written.reset();                                                                                                                                          //Natural: RESET #NAZ-WRITTEN
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ099");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ099'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("EXP");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'EXP'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(" ");                                                                                                            //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ' '
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER1 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ02",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
        );
        READ02:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ02")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                                                                             //Natural: ACCEPT IF NAZ-TBL-RCRD-TYP-IND = 'C'
            {
                continue;
            }
            vw_naz_Table_Ddm.deleteDBRow("READ02");                                                                                                                       //Natural: DELETE
            pnd_Naz_Written.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NAZ-WRITTEN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Get_Dci_Dpi() throws Exception                                                                                                                       //Natural: GET-DCI-DPI
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaAnta001.getPnd_Ls_Dpi_Info().reset();                                                                                                                          //Natural: RESET #LS-DPI-INFO
        //*  062019 ONLY CALCULATE DPI ON
        if (condition(pnd_Fp_Pay_Dte.notEquals(pnd_W_Install_Date_Pnd_W_Install_Date_N)))                                                                                 //Natural: IF #FP-PAY-DTE NE #W-INSTALL-DATE-N
        {
            //*  062019 FINAL PAYMENT.
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  062019
        }                                                                                                                                                                 //Natural: END-IF
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Trans_Code().setValue("DP");                                                                                                 //Natural: ASSIGN #LS-TRANS-CODE := 'DP'
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Pay_Date().setValue(pnd_W_Chk_Dte);                                                                                          //Natural: ASSIGN #LS-PAY-DATE := #W-CHK-DTE
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ppcn().setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct);                                                                         //Natural: ASSIGN #LS-PPCN := #W-DTL-CNTRCT
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Proof_Date().setValue(pnd_W_Install_Date_Pnd_W_Install_Date_N);                                                              //Natural: ASSIGN #LS-PROOF-DATE := #LS-EFF-DATE := #W-INSTALL-DATE-N
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Eff_Date().setValue(pnd_W_Install_Date_Pnd_W_Install_Date_N);
        if (condition(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Proof_Date().equals(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Pay_Date())))                                         //Natural: IF #LS-PROOF-DATE = #LS-PAY-DATE
        {
            pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Dpi_Amt().setValue(0);                                                                                                   //Natural: ASSIGN #LS-DPI-AMT := 0
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Settle_Type().setValue("L");                                                                                                 //Natural: ASSIGN #LS-SETTLE-TYPE := 'L'
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_State_Code().setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Rsdnce_Cde);                                                               //Natural: ASSIGN #LS-STATE-CODE := #W-DTL-RSDNCE-CDE
        if (condition(pnd_W_First_Ann_Dod.greater(pnd_W_Scnd_Ann_Dod)))                                                                                                   //Natural: IF #W-FIRST-ANN-DOD GT #W-SCND-ANN-DOD
        {
            pnd_W_Work_Date_Pnd_W_Work_Yyyymm.setValue(pnd_W_First_Ann_Dod);                                                                                              //Natural: ASSIGN #W-WORK-YYYYMM := #W-FIRST-ANN-DOD
            pnd_W_Work_Date_Pnd_W_Work_Dd.setValue(1);                                                                                                                    //Natural: ASSIGN #W-WORK-DD := 01
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Work_Date_Pnd_W_Work_Yyyymm.setValue(pnd_W_Scnd_Ann_Dod);                                                                                               //Natural: ASSIGN #W-WORK-YYYYMM := #W-SCND-ANN-DOD
            pnd_W_Work_Date_Pnd_W_Work_Dd.setValue(1);                                                                                                                    //Natural: ASSIGN #W-WORK-DD := 01
        }                                                                                                                                                                 //Natural: END-IF
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Death_Date().setValue(pnd_W_Work_Date);                                                                                      //Natural: ASSIGN #LS-DEATH-DATE := #W-WORK-DATE
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Prod_Code().setValue("T");                                                                                                   //Natural: ASSIGN #LS-PROD-CODE := 'T'
        pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Trans_Amt().compute(new ComputeParameters(false, pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Trans_Amt()), pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_G, //Natural: ASSIGN #LS-TRANS-AMT := #TAB-PER-AMT ( #G,#A ) + #TAB-DIV-AMT ( #G,#A )
            pnd_A).add(pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_G,pnd_A)));
        DbsUtil.callnat(Antn001.class , getCurrentProcessState(), pdaAnta001.getPnd_Ls_Dpi_Info());                                                                       //Natural: CALLNAT 'ANTN001' #LS-DPI-INFO
        if (condition(Global.isEscape())) return;
        if (condition(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ret_Code().equals(getZero()) || pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ret_Code().equals(84)                     //Natural: IF #LS-RET-CODE = 0 OR = 0084 OR = 0085
            || pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ret_Code().equals(85)))
        {
            pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi.getValue(pnd_F).nadd(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Dpi_Amt());                                                     //Natural: ASSIGN #W-DTL-TOT-DPI ( #F ) := #W-DTL-TOT-DPI ( #F ) + #LS-DPI-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Invalid RC from ANTN001 for ",pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ppcn());                                                            //Natural: WRITE 'Invalid RC from ANTN001 for ' #LS-PPCN
            if (Global.isEscape()) return;
            getReports().write(0, "RC=",pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ret_Code());                                                                                 //Natural: WRITE 'RC=' #LS-RET-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "Message",pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Ret_Msg());                                                                              //Natural: WRITE 'Message' #LS-RET-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Payments_Due() throws Exception                                                                                                                  //Natural: GET-PAYMENTS-DUE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tab_Fund_Table.getValue("*").reset();                                                                                                                         //Natural: RESET #TAB-FUND-TABLE ( * ) #RET-CDE
        pnd_Ret_Cde.reset();
        DbsUtil.callnat(Iaan3300.class , getCurrentProcessState(), pnd_W_Cntrct, pnd_W_Payee_Cde, pnd_Tab_Fund_Table.getValue("*"), pnd_Check_Date, pnd_Bottom_Date,      //Natural: CALLNAT 'IAAN3300' #W-CNTRCT #W-PAYEE-CDE #TAB-FUND-TABLE ( * ) #CHECK-DATE #BOTTOM-DATE #RET-CDE
            pnd_Ret_Cde);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Pend_Dte() throws Exception                                                                                                                      //Natural: GET-PEND-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Pend_Date.reset();                                                                                                                                          //Natural: RESET #W-PEND-DATE
        DbsUtil.callnat(Iaan702r.class , getCurrentProcessState(), pnd_W_Cntrct, pnd_W_Pyee_Cde, pnd_W_First_Ann_Dod, pnd_W_Scnd_Ann_Dod, pnd_W_Mde_Cde,                  //Natural: CALLNAT 'IAAN702R' #W-CNTRCT #W-PYEE-CDE #W-FIRST-ANN-DOD #W-SCND-ANN-DOD #W-MDE-CDE #W-PND-CDE #W-PEND-DATE
            pnd_W_Pnd_Cde, pnd_W_Pend_Date);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Load_Detail() throws Exception                                                                                                                       //Natural: LOAD-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Num_Chks.getValue(pnd_F,pnd_B).nadd(1);                                                                                               //Natural: ADD 1 TO #W-DTL-NUM-CHKS ( #F,#B )
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Guar.getValue(pnd_F,pnd_B).nadd(pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_G,pnd_A));                                        //Natural: ASSIGN #W-DTL-DUE-GUAR ( #F,#B ) := #W-DTL-DUE-GUAR ( #F,#B ) + #TAB-PER-AMT ( #G,#A )
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Div.getValue(pnd_F,pnd_B).nadd(pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_G,pnd_A));                                         //Natural: ASSIGN #W-DTL-DUE-DIV ( #F,#B ) := #W-DTL-DUE-DIV ( #F,#B ) + #TAB-DIV-AMT ( #G,#A )
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Guar.getValue(pnd_F).nadd(pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_G,pnd_A));                                              //Natural: ASSIGN #W-DTL-TOT-GUAR ( #F ) := #W-DTL-TOT-GUAR ( #F ) + #TAB-PER-AMT ( #G,#A )
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Div.getValue(pnd_F).nadd(pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_G,pnd_A));                                               //Natural: ASSIGN #W-DTL-TOT-DIV ( #F ) := #W-DTL-TOT-DIV ( #F ) + #TAB-DIV-AMT ( #G,#A )
                                                                                                                                                                          //Natural: PERFORM GET-DCI-DPI
        sub_Get_Dci_Dpi();
        if (condition(Global.isEscape())) {return;}
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Dpi.getValue(pnd_F,pnd_B).nadd(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Dpi_Amt());                                                   //Natural: ASSIGN #W-DTL-DUE-DPI ( #F,#B ) := #W-DTL-DUE-DPI ( #F,#B ) + #LS-DPI-AMT
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals.getValue(pnd_F,pnd_B).compute(new ComputeParameters(false, pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals.getValue(pnd_F,pnd_B)),  //Natural: ASSIGN #W-DTL-DUE-TOTALS ( #F,#B ) := #W-DTL-DUE-TOTALS ( #F,#B ) + #TAB-PER-AMT ( #G,#A ) + #TAB-DIV-AMT ( #G,#A ) + #LS-DPI-AMT
            pnd_W_Detail_Tiaa_Pnd_W_Dtl_Due_Totals.getValue(pnd_F,pnd_B).add(pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_G,pnd_A)).add(pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_G,
            pnd_A)).add(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Dpi_Amt()));
        pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_E).setValue(pdaAnta001.getPnd_Ls_Dpi_Info_Pnd_Ls_Dpi_Amt());                                                    //Natural: ASSIGN #W-WRKSHT-DPI-AMT ( #E ) := #LS-DPI-AMT
        pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_E).compute(new ComputeParameters(false, pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_E)), pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_E).add(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_E))); //Natural: ASSIGN #W-WRKSHT-TOTAL ( #E ) := #W-WRKSHT-TOT-PYMT ( #E ) + #W-WRKSHT-DPI-AMT ( #E )
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_E).greater(getZero())))                                                                          //Natural: IF #W-WRKSHT-TOT-PYMT ( #E ) GT 0
        {
            pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact.getValue(pnd_E).compute(new ComputeParameters(true, pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact.getValue(pnd_E)),             //Natural: COMPUTE ROUNDED #W-WRKSHT-DPI-FACT ( #E ) = #W-WRKSHT-TOTAL ( #E ) / #W-WRKSHT-TOT-PYMT ( #E )
                pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_E).divide(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_E)));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact.getValue(pnd_E).setValue(0);                                                                                            //Natural: ASSIGN #W-WRKSHT-DPI-FACT ( #E ) := 0
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Dtl_Table() throws Exception                                                                                                                    //Natural: LOAD-DTL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  032311
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymnts_Due.getValue("*","*").reset();                                                                                                 //Natural: RESET #W-DTL-PYMNTS-DUE ( *,* ) #W-DTL-TOTALS ( * ) #WRKSHT-FILE #W-EXCEPTION-MSG
        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Totals.getValue("*").reset();
        pnd_Wrksht_File.reset();
        pnd_W_Exception_Msg.reset();
        pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cntrct);                                                                                 //Natural: ASSIGN #W-WRKSHT-CNTRCT := #W-DTL-CNTRCT
        pnd_Wrksht_File_Pnd_W_Wrksht_Payee.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee);                                                                                   //Natural: ASSIGN #W-WRKSHT-PAYEE := #W-DTL-PAYEE
        if (condition(pnd_W_First_Ann_Dod.greater(pnd_W_Scnd_Ann_Dod)))                                                                                                   //Natural: IF #W-FIRST-ANN-DOD GT #W-SCND-ANN-DOD
        {
            pnd_Wrksht_File_Pnd_W_Wrksht_Dod.setValue(pnd_W_First_Ann_Dod);                                                                                               //Natural: ASSIGN #W-WRKSHT-DOD := #W-FIRST-ANN-DOD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wrksht_File_Pnd_W_Wrksht_Dod.setValue(pnd_W_Scnd_Ann_Dod);                                                                                                //Natural: ASSIGN #W-WRKSHT-DOD := #W-SCND-ANN-DOD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wrksht_File_Pnd_W_Wrksht_Opt.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde);                                                                                  //Natural: ASSIGN #W-WRKSHT-OPT := #W-DTL-OPTN-CDE
        pnd_Wrksht_File_Pnd_W_Wrksht_Cur.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Cur);                                                                                       //Natural: ASSIGN #W-WRKSHT-CUR := #W-DTL-CUR
        pnd_Wrksht_File_Pnd_W_Wrksht_Org.setValue(pnd_W_Orign);                                                                                                           //Natural: ASSIGN #W-WRKSHT-ORG := #W-ORIGN
        pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte);                                                                               //Natural: ASSIGN #W-WRKSHT-1ST-DTE := #W-DTL-1ST-DTE
        pnd_Wrksht_File_Pnd_W_Wrksht_Pend.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend);                                                                                     //Natural: ASSIGN #W-WRKSHT-PEND := #W-DTL-PEND
        pnd_Wrksht_File_Pnd_W_Wrksht_Mode.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode);                                                                                     //Natural: ASSIGN #W-WRKSHT-MODE := #W-DTL-MODE
        pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte);                                                                    //Natural: ASSIGN #W-WRKSHT-FINAL-PYMT := #W-DTL-FINAL-PAYMT-DTE
        pnd_Limited.setValue(false);                                                                                                                                      //Natural: ASSIGN #LIMITED := FALSE
        if (condition(! (pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt.equals(" ") || pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt.equals("000000"))))                               //Natural: IF NOT #W-WRKSHT-FINAL-PYMT = ' ' OR = '000000'
        {
            if (condition((pnd_Wrksht_File_Pnd_W_Wrksht_Opt.equals(pnd_Single_Life.getValue("*")) && pnd_Wrksht_File_Pnd_W_Wrksht_Dod.greater(getZero()))                 //Natural: IF ( #W-WRKSHT-OPT = #SINGLE-LIFE ( * ) AND #W-WRKSHT-DOD GT 0 ) OR ( #W-FIRST-ANN-DOD GT 0 AND #W-SCND-ANN-DOD GT 0 )
                || (pnd_W_First_Ann_Dod.greater(getZero()) && pnd_W_Scnd_Ann_Dod.greater(getZero()))))
            {
                pnd_Limited.setValue(true);                                                                                                                               //Natural: ASSIGN #LIMITED := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Limited.getBoolean()))                                                                                                                          //Natural: IF #LIMITED
        {
            if (condition(pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm.equals(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt)))                                                                //Natural: IF #W-CHK-YYYYMM EQ #W-WRKSHT-FINAL-PYMT
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-EXPIRING-CONTRACTS
                sub_Write_Expiring_Contracts();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #F 1 2
        for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(2)); pnd_F.nadd(1))
        {
            if (condition(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund.getValue(pnd_F).equals(" ")))                                                                                  //Natural: IF #W-DTL-FUND ( #F ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_E.reset();                                                                                                                                                //Natural: RESET #E
            FOR02:                                                                                                                                                        //Natural: FOR #G 1 40
            for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(40)); pnd_G.nadd(1))
            {
                if (condition(pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue(pnd_G).equals(" ")))                                                                               //Natural: IF #TAB-FUND ( #G ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tab_Fund_Table_Pnd_Tab_Fund.getValue(pnd_G).equals(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund.getValue(pnd_F))))                                  //Natural: IF #TAB-FUND ( #G ) = #W-DTL-FUND ( #F )
                {
                    pnd_Wrksht_File_Pnd_W_Wrksht_Fund.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Fund.getValue(pnd_F));                                                         //Natural: ASSIGN #W-WRKSHT-FUND := #W-DTL-FUND ( #F )
                    FOR03:                                                                                                                                                //Natural: FOR #A #TABLE-CNT ( #G ) 1 STEP -1
                    for (pnd_A.setValue(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_G)); condition(pnd_A.greaterOrEqual(1)); pnd_A.nsubtract(1))
                    {
                        if (condition(pnd_Limited.getBoolean() && pnd_Tab_Fund_Table_Pnd_Tab_Install_Yymm.getValue(pnd_G,pnd_A).greater(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt))) //Natural: IF #LIMITED AND #TAB-INSTALL-YYMM ( #G,#A ) GT #W-WRKSHT-FINAL-PYMT
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_W_Install_Date.setValue(pnd_Tab_Fund_Table_Pnd_Tab_Install_Date.getValue(pnd_G,pnd_A));                                                       //Natural: ASSIGN #W-INSTALL-DATE := #TAB-INSTALL-DATE ( #G,#A )
                        //*  LOAD THE WORKSHEET FILE
                        pnd_E.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #E
                        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_E).setValue(pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm);                                      //Natural: ASSIGN #W-WRKSHT-DUE-DTE ( #E ) := #W-INSTALL-DATE-YYYYMM
                        pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_E).setValue(pnd_Tab_Fund_Table_Pnd_Tab_Div_Amt.getValue(pnd_G,pnd_A));                              //Natural: ASSIGN #W-WRKSHT-DIV ( #E ) := #TAB-DIV-AMT ( #G,#A )
                        pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_E).setValue(pnd_Tab_Fund_Table_Pnd_Tab_Per_Amt.getValue(pnd_G,pnd_A));                             //Natural: ASSIGN #W-WRKSHT-GUAR ( #E ) := #TAB-PER-AMT ( #G,#A )
                        pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_E).compute(new ComputeParameters(false, pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_E)),  //Natural: ASSIGN #W-WRKSHT-TOT-PYMT ( #E ) := #W-WRKSHT-GUAR ( #E ) + #W-WRKSHT-DIV ( #E )
                            pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_E).add(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_E)));
                        if (condition(pnd_A.equals(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue(pnd_G))))                                                                    //Natural: IF #A = #TABLE-CNT ( #G )
                        {
                            pnd_B.setValue(1);                                                                                                                            //Natural: ASSIGN #B := 1
                            pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymt_Cnt.getValue(pnd_F).setValue(pnd_B);                                                                         //Natural: ASSIGN #W-DTL-PYMT-CNT ( #F ) := #B
                            pnd_W_Detail_Tiaa_Pnd_To_Yy.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Yyyy);                                            //Natural: ASSIGN #TO-YY ( #F,#B ) := #FRM-YY ( #F,#B ) := #W-INSTALL-YYYY
                            pnd_W_Detail_Tiaa_Pnd_Frm_Yy.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Yyyy);
                            pnd_W_Detail_Tiaa_Pnd_To_Mm.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Mm);                                              //Natural: ASSIGN #TO-MM ( #F,#B ) := #FRM-1ST-MM ( #F,#B ) := #W-INSTALL-MM
                            pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Mm);
                                                                                                                                                                          //Natural: PERFORM LOAD-DETAIL
                            sub_Load_Detail();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_W_Install_Date_Pnd_W_Install_Yyyy_A.equals(pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A.getValue(pnd_F,"*"))))                            //Natural: IF #W-INSTALL-YYYY-A = #FRM-YY-A ( #F,* )
                            {
                                DbsUtil.examine(new ExamineSource(pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A.getValue(pnd_F,"*")), new ExamineSearch(pnd_W_Install_Date_Pnd_W_Install_Yyyy_A),  //Natural: EXAMINE #FRM-YY-A ( #F,* ) FOR #W-INSTALL-YYYY-A GIVING INDEX #D #B
                                    new ExamineGivingIndex(pnd_D, pnd_B));
                                pnd_W_Detail_Tiaa_Pnd_To_Mm.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Mm);                                          //Natural: ASSIGN #TO-MM ( #F,#B ) := #W-INSTALL-MM
                                                                                                                                                                          //Natural: PERFORM LOAD-DETAIL
                                sub_Load_Detail();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                FOR04:                                                                                                                                    //Natural: FOR #B 1 #MAX-DETAIL-TBL
                                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Detail_Tbl)); pnd_B.nadd(1))
                                {
                                    if (condition(pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A.getValue(pnd_F,pnd_B).equals(" ") || pnd_W_Detail_Tiaa_Pnd_Frm_Yy_A.getValue(pnd_F,      //Natural: IF #FRM-YY-A ( #F,#B ) = ' ' OR = '0000'
                                        pnd_B).equals("0000")))
                                    {
                                        pnd_W_Detail_Tiaa_Pnd_To_Yy.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Yyyy);                                //Natural: ASSIGN #TO-YY ( #F,#B ) := #FRM-YY ( #F,#B ) := #W-INSTALL-YYYY
                                        pnd_W_Detail_Tiaa_Pnd_Frm_Yy.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Yyyy);
                                        pnd_W_Detail_Tiaa_Pnd_To_Mm.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Mm);                                  //Natural: ASSIGN #TO-MM ( #F,#B ) := #FRM-1ST-MM ( #F,#B ) := #W-INSTALL-MM
                                        pnd_W_Detail_Tiaa_Pnd_Frm_1st_Mm.getValue(pnd_F,pnd_B).setValue(pnd_W_Install_Date_Pnd_W_Install_Mm);
                                        pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pymt_Cnt.getValue(pnd_F).nadd(1);                                                                     //Natural: ASSIGN #W-DTL-PYMT-CNT ( #F ) := #W-DTL-PYMT-CNT ( #F ) + 1
                                                                                                                                                                          //Natural: PERFORM LOAD-DETAIL
                                        sub_Load_Detail();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  032311 START
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(1).equals(" ")))                                                                                  //Natural: IF #W-WRKSHT-DUE-DTE ( 1 ) = ' '
            {
                pnd_W_Cntrct.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct);                                                                                               //Natural: ASSIGN #W-CNTRCT := #W-WRKSHT-CNTRCT
                pnd_W_Pyee_Cde_Pnd_W_Pyee_Cde_A.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Payee);                                                                             //Natural: ASSIGN #W-PYEE-CDE-A := #W-WRKSHT-PAYEE
                pnd_W_Optn_Cde.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Opt);                                                                                                //Natural: ASSIGN #W-OPTN-CDE := #W-WRKSHT-OPT
                pnd_W_Orgn_Cde.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Org);                                                                                                //Natural: ASSIGN #W-ORGN-CDE := #W-WRKSHT-ORG
                pnd_W_First_Ann_Dod.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Dod);                                                                                           //Natural: ASSIGN #W-FIRST-ANN-DOD := #W-WRKSHT-DOD
                pnd_W_Scnd_Ann_Dod.setValue(0);                                                                                                                           //Natural: ASSIGN #W-SCND-ANN-DOD := 0
                pnd_W_Pnd_Cde.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);                                                                                                //Natural: ASSIGN #W-PND-CDE := #W-WRKSHT-PEND
                pnd_W_Pend_Date.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte);                                                                                           //Natural: ASSIGN #W-PEND-DATE := #W-DTL-PEND-DTE
                pnd_W_Mde_Cde_Pnd_W_Mde_Cde_A.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Mode);                                                                                //Natural: ASSIGN #W-MDE-CDE-A := #W-WRKSHT-MODE
                pnd_W_Final_Dte.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt);                                                                                        //Natural: ASSIGN #W-FINAL-DTE := #W-WRKSHT-FINAL-PYMT
                pnd_W_Exception_Msg.setValue("Check IAIQ");                                                                                                               //Natural: ASSIGN #W-EXCEPTION-MSG := 'Check IAIQ'
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTIONS
                sub_Write_Exceptions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***** Exception *****");                                                                                                           //Natural: WRITE '***** Exception *****'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Check IAIQ for contract/payee",pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);                                                         //Natural: WRITE 'Check IAIQ for contract/payee' #W-WRKSHT-CNTRCT-PAYEE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Processed_Cnt.nsubtract(1);                                                                                                                           //Natural: SUBTRACT 1 FROM #PROCESSED-CNT
                //*  032311 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(2, false, pnd_Wrksht_File);                                                                                                          //Natural: WRITE WORK FILE 2 #WRKSHT-FILE
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_Ovrall_Total.getValue(pnd_F).compute(new ComputeParameters(false, pnd_W_Detail_Tiaa_Pnd_W_Dtl_Ovrall_Total.getValue(pnd_F)),  //Natural: ASSIGN #W-DTL-OVRALL-TOTAL ( #F ) := #W-DTL-TOT-GUAR ( #F ) + #W-DTL-TOT-DIV ( #F ) + #W-DTL-TOT-DPI ( #F )
                    pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Guar.getValue(pnd_F).add(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Div.getValue(pnd_F)).add(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Tot_Dpi.getValue(pnd_F)));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Load_Detail_Tiaa() throws Exception                                                                                                                  //Natural: LOAD-DETAIL-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1012 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'GA' )
        if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'GA'")))
        {
            decideConditionsMet1012++;
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX1 := #GA-0L-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'GW' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'GW'")))
        {
            decideConditionsMet1012++;
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX1 := #GW-0M-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'IP' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'IP'")))
        {
            decideConditionsMet1012++;
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX1 := #IP-6L-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'I' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'I'")))
        {
            decideConditionsMet1012++;
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX1 := #IA-0N-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'S0' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'S0'")))
        {
            decideConditionsMet1012++;
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX1 := #S0-6M-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'W0' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'W0'")))
        {
            decideConditionsMet1012++;
            //*  GROUP
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Org.equals(4)))                                                                                                    //Natural: IF #W-WRKSHT-ORG = 04
            {
                pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx);                                                                                      //Natural: ASSIGN #TOT-NDX1 := #W0-GRP-NDX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx);                                                                                       //Natural: ASSIGN #TOT-NDX1 := #W0-6N-NDX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Tot_Ndx1.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx);                                                                                              //Natural: ASSIGN #TOT-NDX1 := #Y0-NDX
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Fund.equals(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,"*"))))                                //Natural: IF #W-WRKSHT-FUND = #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,* )
        {
            DbsUtil.examine(new ExamineSource(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,"*")), new ExamineSearch(pnd_Wrksht_File_Pnd_W_Wrksht_Fund),  //Natural: EXAMINE #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,* ) FOR #W-WRKSHT-FUND GIVING INDEX #A #TOT-NDX2
                new ExamineGivingIndex(pnd_A, pnd_Tot_Ndx2));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR05:                                                                                                                                                        //Natural: FOR #TOT-NDX2 1 #MAX-TIAA-FND
            for (pnd_Tot_Ndx2.setValue(1); condition(pnd_Tot_Ndx2.lessOrEqual(pnd_Max_Tiaa_Fnd)); pnd_Tot_Ndx2.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).equals(" ")))                                             //Natural: IF #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) = ' '
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Fund);                        //Natural: ASSIGN #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) := #W-WRKSHT-FUND
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Fund.equals(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,"*"))))                                //Natural: IF #W-WRKSHT-FUND = #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,* )
        {
            DbsUtil.examine(new ExamineSource(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,"*")), new ExamineSearch(pnd_Wrksht_File_Pnd_W_Wrksht_Fund),  //Natural: EXAMINE #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,* ) FOR #W-WRKSHT-FUND GIVING INDEX #A #TOT-NDX2
                new ExamineGivingIndex(pnd_A, pnd_Tot_Ndx2));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR06:                                                                                                                                                        //Natural: FOR #TOT-NDX2 1 #MAX-TIAA-FND
            for (pnd_Tot_Ndx2.setValue(1); condition(pnd_Tot_Ndx2.lessOrEqual(pnd_Max_Tiaa_Fnd)); pnd_Tot_Ndx2.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).equals(" ")))                                             //Natural: IF #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) = ' '
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Fund);                        //Natural: ASSIGN #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) := #W-WRKSHT-FUND
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,"*").equals(pnd_Wrksht_File_Pnd_W_Wrksht_Pend)))                  //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,* ) = #W-WRKSHT-PEND
        {
            FOR07:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(pnd_Wrksht_File_Pnd_W_Wrksht_Pend))) //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = #W-WRKSHT-PEND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR08:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(" ")))                               //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = ' '
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);          //Natural: ASSIGN #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-WRKSHT-PEND
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Save_Cntrct_Pyee.notEquals(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee)))                                                                       //Natural: IF #W-SAVE-CNTRCT-PYEE NE #W-WRKSHT-CNTRCT-PAYEE
        {
            pnd_W_Save_Cntrct_Pyee.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);                                                                                   //Natural: ASSIGN #W-SAVE-CNTRCT-PYEE := #W-WRKSHT-CNTRCT-PAYEE
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(1);                                                   //Natural: ASSIGN #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + 1
            FOR09:                                                                                                                                                        //Natural: FOR #B 1 #MAX-WRK
            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Wrk)); pnd_B.nadd(1))
            {
                if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_B).equals(" ")))                                                                          //Natural: IF #W-WRKSHT-DUE-DTE ( #B ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_B).equals(pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm)))                                               //Natural: IF #W-WRKSHT-DUE-DTE ( #B ) = #W-CHK-YYYYMM
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(1);                                              //Natural: ASSIGN #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR10:                                                                                                                                                        //Natural: FOR #B 1 #MAX-WRK
            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Wrk)); pnd_B.nadd(1))
            {
                if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_B).equals(" ")))                                                                          //Natural: IF #W-WRKSHT-DUE-DTE ( #B ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_B.nsubtract(1);                                                                                                                                               //Natural: ASSIGN #B := #B - 1
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_B).equals(pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm)))                                                       //Natural: IF #W-WRKSHT-DUE-DTE ( #B ) = #W-CHK-YYYYMM
        {
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_B));      //Natural: ASSIGN #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( #B )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_B));       //Natural: ASSIGN #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DIV ( #B )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue("*"));                //Natural: ASSIGN #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue("*"));                 //Natural: ASSIGN #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DIV ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue("*"));              //Natural: ASSIGN #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DPI-AMT ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).compute(new ComputeParameters(false, pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3)),  //Natural: ASSIGN #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).add(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,
            pnd_Tot_Ndx2,pnd_Tot_Ndx3)).add(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3)));
    }
    private void sub_Print_Tiaa_Totals() throws Exception                                                                                                                 //Natural: PRINT-TIAA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR11:                                                                                                                                                            //Natural: FOR #TOT-NDX1 1 8
        for (pnd_Tot_Ndx1.setValue(1); condition(pnd_Tot_Ndx1.lessOrEqual(8)); pnd_Tot_Ndx1.nadd(1))
        {
            FOR12:                                                                                                                                                        //Natural: FOR #TOT-NDX2 1 #MAX-TIAA-FND
            for (pnd_Tot_Ndx2.setValue(1); condition(pnd_Tot_Ndx2.lessOrEqual(pnd_Max_Tiaa_Fnd)); pnd_Tot_Ndx2.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).equals(" ")))                                             //Natural: IF #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).equals("T")))                                             //Natural: IF #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 ) = 'T'
                {
                    pnd_Fnd_Ndx.setValue(1);                                                                                                                              //Natural: ASSIGN #FND-NDX := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fnd_Ndx.setValue(2);                                                                                                                              //Natural: ASSIGN #FND-NDX := 2
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd.getValue(pnd_Fnd_Ndx).setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Fnd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-FND ( #FND-NDX ) := #W-TIAA-TOT-DUE-FND ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tot_Ndx2));
                FOR13:                                                                                                                                                    //Natural: FOR #TOT-NDX3 1 #MAX-PEND
                for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
                {
                    if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).notEquals(" ")))                        //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) NE ' '
                    {
                        if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1, //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,* )
                            "*"))))
                        {
                            FOR14:                                                                                                                                        //Natural: FOR #C 1 #MAX-PEND
                            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Max_Pend)); pnd_C.nadd(1))
                            {
                                if (condition(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_C).equals(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1, //Natural: IF #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#C ) = #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                                    pnd_Tot_Ndx2,pnd_Tot_Ndx3))))
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR15:                                                                                                                                        //Natural: FOR #C 1 #MAX-PEND
                            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Max_Pend)); pnd_C.nadd(1))
                            {
                                if (condition(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_C).equals(" ")))                                       //Natural: IF #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#C ) = ' '
                                {
                                    pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_C).setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#C ) := #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                                        pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_Fnd_Ndx, //Natural: IF #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = #W-TIAA-TOT-DUE-OVRALL-PEND ( #FND-NDX,* )
                            "*"))))
                        {
                            FOR16:                                                                                                                                        //Natural: FOR #B 1 #MAX-PEND
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Pend)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_Fnd_Ndx,pnd_B).equals(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1, //Natural: IF #W-TIAA-TOT-DUE-OVRALL-PEND ( #FND-NDX,#B ) = #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                                    pnd_Tot_Ndx2,pnd_Tot_Ndx3))))
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            FOR17:                                                                                                                                        //Natural: FOR #B 1 #MAX-PEND
                            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Pend)); pnd_B.nadd(1))
                            {
                                if (condition(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_Fnd_Ndx,pnd_B).equals(" ")))                       //Natural: IF #W-TIAA-TOT-DUE-OVRALL-PEND ( #FND-NDX,#B ) = ' '
                                {
                                    pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_Fnd_Ndx,pnd_B).setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Pend.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-PEND ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                                        pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-CNTRCTS ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-CNTRCTS ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-CNTRCTS ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-CNTRCTS ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-CHKS ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-CHKS ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-CHKS ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-CHKS ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-GUAR ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-GUAR ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-GUAR ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-GUAR ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-DIVD ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-DIVD ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-DIVD ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-DIVD ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,        //Natural: ASSIGN #W-TIAA-TTL-GUAR ( #TOT-NDX1,#C ) := #W-TIAA-TTL-GUAR ( #TOT-NDX1,#C ) + #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-GUAR ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-GUAR ( #FND-NDX,#B ) + #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,        //Natural: ASSIGN #W-TIAA-TTL-DIVD ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DIVD ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-DIVD ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-DIVD ( #FND-NDX,#B ) + #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,          //Natural: ASSIGN #W-TIAA-TTL-DPI ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DPI ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-DPI ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-DPI ( #FND-NDX,#B ) + #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,                  //Natural: ASSIGN #W-TIAA-TTL ( #TOT-NDX1,#C ) := #W-TIAA-TTL ( #TOT-NDX1,#C ) + #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,  //Natural: ASSIGN #W-TIAA-OVRALL-TOT ( #FND-NDX,#B ) := #W-TIAA-OVRALL-TOT ( #FND-NDX,#B ) + #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                            pnd_Tot_Ndx2,pnd_Tot_Ndx3));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_A.compute(new ComputeParameters(false, pnd_A), pnd_Tot_Ndx3.subtract(1));                                                                     //Natural: ASSIGN #A := #TOT-NDX3 - 1
                        pnd_C.setValue(pnd_Max_Pend);                                                                                                                     //Natural: ASSIGN #C := #MAX-PEND
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_C).setValue("ALL");                                                          //Natural: ASSIGN #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#C ) := 'ALL'
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-CNTRCTS ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-CNTRCTS ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-CHKS ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-CHKS ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-GUAR ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-GUAR ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TTL-DUE-DIVD ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DUE-DIVD ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,        //Natural: ASSIGN #W-TIAA-TTL-GUAR ( #TOT-NDX1,#C ) := #W-TIAA-TTL-GUAR ( #TOT-NDX1,#C ) + #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,        //Natural: ASSIGN #W-TIAA-TTL-DIVD ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DIVD ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,          //Natural: ASSIGN #W-TIAA-TTL-DPI ( #TOT-NDX1,#C ) := #W-TIAA-TTL-DPI ( #TOT-NDX1,#C ) + #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl.getValue(pnd_Tot_Ndx1,pnd_C).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,                  //Natural: ASSIGN #W-TIAA-TTL ( #TOT-NDX1,#C ) := #W-TIAA-TTL ( #TOT-NDX1,#C ) + #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_B.setValue(pnd_Max_Pend);                                                                                                                     //Natural: ASSIGN #B := #MAX-PEND
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_Fnd_Ndx,pnd_B).setValue("ALL");                                          //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-PEND ( #FND-NDX,#B ) := 'ALL'
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Cntrcts.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-CNTRCTS ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-CNTRCTS ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Chks.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-CHKS ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-CHKS ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-GUAR ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-GUAR ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Due_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-DUE-OVRALL-DIVD ( #FND-NDX,#B ) := #W-TIAA-TOT-DUE-OVRALL-DIVD ( #FND-NDX,#B ) + #W-TIAA-TOT-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-GUAR ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-GUAR ( #FND-NDX,#B ) + #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-DIVD ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-DIVD ( #FND-NDX,#B ) + #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1, //Natural: ASSIGN #W-TIAA-TOT-OVRALL-DPI ( #FND-NDX,#B ) := #W-TIAA-TOT-OVRALL-DPI ( #FND-NDX,#B ) + #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot.getValue(pnd_Fnd_Ndx,pnd_B).nadd(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,  //Natural: ASSIGN #W-TIAA-OVRALL-TOT ( #FND-NDX,#B ) := #W-TIAA-OVRALL-TOT ( #FND-NDX,#B ) + #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,1:#A )
                            pnd_Tot_Ndx2,1,":",pnd_A));
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR18:                                                                                                                                                            //Natural: FOR #TOT-NDX1 1 8
        for (pnd_Tot_Ndx1.setValue(1); condition(pnd_Tot_Ndx1.lessOrEqual(8)); pnd_Tot_Ndx1.nadd(1))
        {
            FOR19:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).notEquals(" ")))                                             //Natural: IF #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#TOT-NDX3 ) NE ' '
                {
                    short decideConditionsMet1184 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TOT-NDX1 = 1
                    if (condition(pnd_Tot_Ndx1.equals(1)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("GA");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'GA'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 2
                    else if (condition(pnd_Tot_Ndx1.equals(2)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("GW");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'GW'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 3
                    else if (condition(pnd_Tot_Ndx1.equals(3)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("IA-IH IJ");                                                                            //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'IA-IH IJ'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 4
                    else if (condition(pnd_Tot_Ndx1.equals(4)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("IP");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'IP'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 5
                    else if (condition(pnd_Tot_Ndx1.equals(5)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("S0");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'S0'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 6
                    else if (condition(pnd_Tot_Ndx1.equals(6)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("W0");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'W0'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX1 = 7
                    else if (condition(pnd_Tot_Ndx1.equals(7)))
                    {
                        decideConditionsMet1184++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("W0 GROUP");                                                                            //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'W0 GROUP'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("Y0-Y9");                                                                               //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'Y0-Y9'
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd.setValue(" ");                                                                                                 //Natural: ASSIGN #TIAA-TOT-FND := ' '
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-PEND-CDE := #W-TIAA-TTL-DUE-PEND ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));           //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-TTL-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-TTL-DUE-CHKS ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-TTL-DUE-GUAR ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                  //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-TTL-DUE-DIVD ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                     //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-TTL-GUAR ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                      //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-TTL-DIVD ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                       //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-TTL-DPI ( #TOT-NDX1,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Ttl_Tbl_Pnd_W_Tiaa_Ttl.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                         //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-TTL ( #TOT-NDX1,#TOT-NDX3 )
                    getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd, //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.equals("ALL")))                                                                             //Natural: IF #TIAA-TOT-PEND-CDE = 'ALL'
                    {
                        getReports().skip(1, 1);                                                                                                                          //Natural: SKIP ( 1 ) 1
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        getReports().write(1, pnd_Dashes);                                                                                                                                //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        FOR20:                                                                                                                                                            //Natural: FOR #A 1 #MAX-TIAA-FND
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Tiaa_Fnd)); pnd_A.nadd(1))
        {
            if (condition(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd.getValue(pnd_A).equals(" ")))                                                        //Natural: IF #W-TIAA-TOT-DUE-OVRALL-FND ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Fnd.getValue(pnd_A));                                  //Natural: ASSIGN #TIAA-TOT-FND := #W-TIAA-TOT-DUE-OVRALL-FND ( #A )
            FOR21:                                                                                                                                                        //Natural: FOR #B 1 #MAX-PEND
            for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Pend)); pnd_B.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_A,pnd_B).equals(" ")))                                             //Natural: IF #W-TIAA-TOT-DUE-OVRALL-PEND ( #A,#B ) = ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("TIAA#");                                                                                   //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'TIAA#'
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Pend.getValue(pnd_A,pnd_B));              //Natural: ASSIGN #TIAA-TOT-PEND-CDE := #W-TIAA-TOT-DUE-OVRALL-PEND ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Cntrcts.getValue(pnd_A,                //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-TOT-DUE-OVRALL-CNTRCTS ( #A,#B )
                        pnd_B));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Chks.getValue(pnd_A,pnd_B));              //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-TOT-DUE-OVRALL-CHKS ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Guar.getValue(pnd_A,pnd_B));              //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-TOT-DUE-OVRALL-GUAR ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Due_Ovrall_Divd.getValue(pnd_A,pnd_B));               //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-TOT-DUE-OVRALL-DIVD ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Guar.getValue(pnd_A,pnd_B));                  //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-TOT-OVRALL-GUAR ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Divd.getValue(pnd_A,pnd_B));                   //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-TOT-OVRALL-DIVD ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Tot_Ovrall_Dpi.getValue(pnd_A,pnd_B));                    //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-TOT-OVRALL-DPI ( #A,#B )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Tot_Ovrall_Totals_Pnd_W_Tiaa_Ovrall_Tot.getValue(pnd_A,pnd_B));                      //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-OVRALL-TOT ( #A,#B )
                    getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fnd, //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.equals("ALL")))                                                                             //Natural: IF #TIAA-TOT-PEND-CDE = 'ALL'
                    {
                        //*        SKIP (1) 1
                        getReports().write(1, pnd_Dashes);                                                                                                                //Natural: WRITE ( 1 ) #DASHES
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Tiaa_Wrksht_Contract_Totals() throws Exception                                                                                                 //Natural: PRINT-TIAA-WRKSHT-CONTRACT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit.setValue(pnd_Wrksht_Total_Lit);                                                                              //Natural: ASSIGN #TIAA-WRKSHT-TOTAL-LIT := #WRKSHT-TOTAL-LIT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt,new ReportEditMask("ZZZZZZZ9.99"));                //Natural: MOVE EDITED #TIAA-TOTAL-DIV-AMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DIV-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt,new ReportEditMask("ZZZZZZZ9.99"));                //Natural: MOVE EDITED #TIAA-TOTAL-GUR-AMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-GUR-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Total_Pymt,new ReportEditMask("ZZZZZZZ9.99"));                            //Natural: MOVE EDITED #TOTAL-PYMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi.setValueEdited(pnd_Total_Amts_Pnd_Total_Dpi,new ReportEditMask("ZZZZZ9.99"));                                //Natural: MOVE EDITED #TOTAL-DPI ( EM = ZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DPI
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due.setValueEdited(pnd_Total_Amts_Pnd_Total_Due,new ReportEditMask("ZZZZZZZZ9.99"));                             //Natural: MOVE EDITED #TOTAL-DUE ( EM = ZZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DUE
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1.setValue(pnd_W_Cntrct_Payee);                                                                              //Natural: ASSIGN #TIAA-WRKSHT-TOTAL-FILL1 := #W-CNTRCT-PAYEE
        getReports().write(1, pnd_Tiaa_Wrksht_Total_Line);                                                                                                                //Natural: WRITE ( 1 ) #TIAA-WRKSHT-TOTAL-LINE
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Process_Payee() throws Exception                                                                                                                     //Natural: PROCESS-PAYEE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *IF #W-DTL-PEND = 'A' AND #W-DTL-PAYEE-N GT 02  /* 040214
        //*  040214
        if (condition(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend.equals("A")))                                                                                                      //Natural: IF #W-DTL-PEND = 'A'
        {
            pnd_W_Pyee_Cde.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Payee_N);                                                                                                 //Natural: ASSIGN #W-PYEE-CDE := #W-DTL-PAYEE-N
            pnd_W_Mde_Cde.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Mode_N);                                                                                                   //Natural: ASSIGN #W-MDE-CDE := #W-DTL-MODE-N
            pnd_W_Pnd_Cde.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend);                                                                                                     //Natural: ASSIGN #W-PND-CDE := #W-DTL-PEND
            pnd_W_Optn_Cde.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Optn_Cde);                                                                                                //Natural: ASSIGN #W-OPTN-CDE := #W-DTL-OPTN-CDE
            pnd_W_Orgn_Cde.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Orgn);                                                                                                    //Natural: ASSIGN #W-ORGN-CDE := #W-DTL-ORGN
            pnd_W_Final_Dte.setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Final_Paymt_Dte);                                                                                        //Natural: ASSIGN #W-FINAL-DTE := #W-DTL-FINAL-PAYMT-DTE
                                                                                                                                                                          //Natural: PERFORM GET-PEND-DTE
            sub_Get_Pend_Dte();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_W_Pend_Date.greater(pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm_N)))                                                                                     //Natural: IF #W-PEND-DATE GT #W-CHK-YYYYMM-N
            {
                getReports().write(0, "PEND DATE",pnd_W_Pend_Date," FOR CONTRACT",pnd_W_Cntrct,"GT CHECK DATE");                                                          //Natural: WRITE 'PEND DATE' #W-PEND-DATE ' FOR CONTRACT' #W-CNTRCT 'GT CHECK DATE'
                if (Global.isEscape()) return;
                pnd_Processed_Cnt.nsubtract(1);                                                                                                                           //Natural: SUBTRACT 1 FROM #PROCESSED-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTIONS
                sub_Write_Exceptions();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Pend_Date.equals(getZero())))                                                                                                             //Natural: IF #W-PEND-DATE = 0
            {
                //*              WRITE 'ZERO PEND DATE RETURNED FROM IAAN702R FOR '
                //*                #W-CNTRCT
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte.setValueEdited(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte,new ReportEditMask("999999"));                                    //Natural: MOVE EDITED #W-DTL-PEND-DTE ( EM = 999999 ) TO #W-DTL-1ST-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte.setValueEdited(pnd_W_Pend_Date,new ReportEditMask("999999"));                                                         //Natural: MOVE EDITED #W-PEND-DATE ( EM = 999999 ) TO #W-DTL-1ST-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte.setValueEdited(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend_Dte,new ReportEditMask("999999"));                                        //Natural: MOVE EDITED #W-DTL-PEND-DTE ( EM = 999999 ) TO #W-DTL-1ST-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Bottom_Date_Pnd_Bottom_Date_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Detail_Tiaa_Pnd_W_Dtl_1st_Dte, "01"));                           //Natural: COMPRESS #W-DTL-1ST-DTE '01' INTO #BOTTOM-DATE-A LEAVING NO
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENTS-DUE
        sub_Get_Payments_Due();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ret_Cde.notEquals(" ")))                                                                                                                        //Natural: IF #RET-CDE NE ' '
        {
            getReports().write(0, "Contract",pnd_W_Cntrct,"Payee",pnd_W_Payee_Cde);                                                                                       //Natural: WRITE 'Contract' #W-CNTRCT 'Payee' #W-PAYEE-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "Bad RC from IAAN3300. RC=",pnd_Ret_Cde);                                                                                               //Natural: WRITE 'Bad RC from IAAN3300. RC=' #RET-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tab_Fund_Table_Pnd_Table_Cnt.getValue("*").greater(getZero())))                                                                                 //Natural: IF #TABLE-CNT ( * ) GT 0
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-DTL-TABLE
            sub_Load_Dtl_Table();
            if (condition(Global.isEscape())) {return;}
            pnd_W_Detail_Tiaa.reset();                                                                                                                                    //Natural: RESET #W-DETAIL-TIAA #WRKSHT-FILE
            pnd_Wrksht_File.reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tiaa_Worksheet() throws Exception                                                                                                            //Natural: PROCESS-TIAA-WORKSHEET
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Cntrct_Payee.reset();                                                                                                                                       //Natural: RESET #W-CNTRCT-PAYEE
        READWORK03:                                                                                                                                                       //Natural: READ WORK 2 #WRKSHT-FILE
        while (condition(getWorkFiles().read(2, pnd_Wrksht_File)))
        {
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee.notEquals(pnd_W_Cntrct_Payee)))                                                                       //Natural: IF #W-WRKSHT-CNTRCT-PAYEE NE #W-CNTRCT-PAYEE
            {
                if (condition(pnd_W_Cntrct_Payee.notEquals(" ")))                                                                                                         //Natural: IF #W-CNTRCT-PAYEE NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-WRKSHT-CONTRACT-TOTALS
                    sub_Print_Tiaa_Wrksht_Contract_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tiaa_Wrksht_Detail_Line.reset();                                                                                                                      //Natural: RESET #TIAA-WRKSHT-DETAIL-LINE #TOTAL-AMTS
                pnd_Total_Amts.reset();
                pnd_W_Cntrct_Payee.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);                                                                                   //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-WRKSHT-CNTRCT-PAYEE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-DETAIL-TIAA
            sub_Load_Detail_Tiaa();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct);                                                         //Natural: ASSIGN #TIAA-WRKSHT-DTL-CNTRCT := #W-WRKSHT-CNTRCT
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Payee);                                                            //Natural: ASSIGN #TIAA-WRKSHT-DTL-PYEE := #W-WRKSHT-PAYEE
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Dod.greater(getZero())))                                                                                           //Natural: IF #W-WRKSHT-DOD GT 0
            {
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dod,new ReportEditMask("9999/99"));                   //Natural: MOVE EDITED #W-WRKSHT-DOD ( EM = 9999/99 ) TO #TIAA-WRKSHT-DTL-DOD-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Opt);                                                              //Natural: ASSIGN #TIAA-WRKSHT-DTL-OPTN := #W-WRKSHT-OPT
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Org,new ReportEditMask("99"));                                //Natural: MOVE EDITED #W-WRKSHT-ORG ( EM = 99 ) TO #TIAA-WRKSHT-DTL-ORG
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte,new ReportEditMask("XXXX/XX"));                 //Natural: MOVE EDITED #W-WRKSHT-1ST-DTE ( EM = XXXX/XX ) TO #TIAA-WRKSHT-DTL-FIRST-DTE
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);                                                             //Natural: ASSIGN #TIAA-WRKSHT-DTL-PEND := #W-WRKSHT-PEND
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Mode);                                                             //Natural: ASSIGN #TIAA-WRKSHT-DTL-MODE := #W-WRKSHT-MODE
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt.notEquals("000000")))                                                                                   //Natural: IF #W-WRKSHT-FINAL-PYMT NE '000000'
            {
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt,new ReportEditMask("XXXX/XX"));           //Natural: MOVE EDITED #W-WRKSHT-FINAL-PYMT ( EM = XXXX/XX ) TO #TIAA-WRKSHT-DTL-PYMT-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Fund);                                                              //Natural: ASSIGN #TIAA-WRKSHT-DTL-FND := #W-WRKSHT-FUND
            FOR22:                                                                                                                                                        //Natural: FOR #A 1 #MAX-WRK
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Wrk)); pnd_A.nadd(1))
            {
                if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_A).equals(" ")))                                                                          //Natural: IF #W-WRKSHT-DUE-DTE ( #A ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #A GT 1
                //*      RESET #TIAA-WRKSHT-DTL-PART1
                //*    END-IF
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm.getValue(pnd_A),  //Natural: COMPRESS #W-WRKSHT-DUE-MM ( #A ) '/' #W-WRKSHT-DUE-YYYY ( #A ) INTO #TIAA-WRKSHT-DTL-DUE-DTE LEAVING NO
                    "/", pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy.getValue(pnd_A)));
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_A),new ReportEditMask("ZZZZZZ9.99"));    //Natural: MOVE EDITED #W-WRKSHT-DIV ( #A ) ( EM = ZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-DIV
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_A),new ReportEditMask("ZZZZZZ9.99"));  //Natural: MOVE EDITED #W-WRKSHT-GUAR ( #A ) ( EM = ZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-GUAR
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_A),new ReportEditMask("ZZZZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-TOT-PYMT ( #A ) ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-TOT-PYMT
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact.getValue(pnd_A),new ReportEditMask("9.99999")); //Natural: MOVE EDITED #W-WRKSHT-DPI-FACT ( #A ) ( EM = 9.99999 ) TO #TIAA-WRKSHT-DTL-FACT
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_A),new ReportEditMask("ZZZZ9.99"));  //Natural: MOVE EDITED #W-WRKSHT-DPI-AMT ( #A ) ( EM = ZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-DPI
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_A),new ReportEditMask("ZZZZZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-TOTAL ( #A ) ( EM = ZZZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-TOT-DUE
                pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_A));                                                             //Natural: ASSIGN #TIAA-TOTAL-DIV-AMT := #TIAA-TOTAL-DIV-AMT + #W-WRKSHT-DIV ( #A )
                pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_A));                                                            //Natural: ASSIGN #TIAA-TOTAL-GUR-AMT := #TIAA-TOTAL-GUR-AMT + #W-WRKSHT-GUAR ( #A )
                pnd_Total_Amts_Pnd_Total_Pymt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_A));                                                                //Natural: ASSIGN #TOTAL-PYMT := #TOTAL-PYMT + #W-WRKSHT-TOT-PYMT ( #A )
                pnd_Total_Amts_Pnd_Total_Dpi.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_A));                                                                  //Natural: ASSIGN #TOTAL-DPI := #TOTAL-DPI + #W-WRKSHT-DPI-AMT ( #A )
                pnd_Total_Amts_Pnd_Total_Due.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_A));                                                                    //Natural: ASSIGN #TOTAL-DUE := #TOTAL-DUE + #W-WRKSHT-TOTAL ( #A )
                getReports().write(1, pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn, //Natural: WRITE ( 1 ) #TIAA-WRKSHT-DETAIL-LINE
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte,
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode,
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte,
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div,
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact,
                    pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-WRKSHT-CONTRACT-TOTALS
        sub_Print_Tiaa_Wrksht_Contract_Totals();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
    }
    private void sub_Write_Exceptions() throws Exception                                                                                                                  //Natural: WRITE-EXCEPTIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ099");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ099'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("EXC");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'EXC'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Cntrct, pnd_W_Pyee_Cde_Pnd_W_Pyee_Cde_A));                 //Natural: COMPRESS #W-CNTRCT #W-PYEE-CDE-A INTO #NAZ-TABLE-LVL3-ID LEAVING NO
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01", true)))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(vw_naz_Table_Ddm.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setValue(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind);                                                                  //Natural: ASSIGN NAZ-TBL-RCRD-TYP-IND := #NAZ-TBL-RCRD-TYP-IND
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setValue(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id);                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-LVL1-ID := #NAZ-TABLE-LVL1-ID
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setValue(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id);                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-LVL2-ID := #NAZ-TABLE-LVL2-ID
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id);                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-LVL3-ID := #NAZ-TABLE-LVL3-ID
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(1).setValue(pnd_W_Optn_Cde);                                                                              //Natural: ASSIGN NAZ-TBL-SCRN-FLD-DSCRPTN ( 1 ) := #W-OPTN-CDE
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(2).setValueEdited(pnd_W_Orgn_Cde,new ReportEditMask("99"));                                               //Natural: MOVE EDITED #W-ORGN-CDE ( EM = 99 ) TO NAZ-TBL-SCRN-FLD-DSCRPTN ( 2 )
                if (condition(pnd_W_First_Ann_Dod.greater(pnd_W_Scnd_Ann_Dod)))                                                                                           //Natural: IF #W-FIRST-ANN-DOD GT #W-SCND-ANN-DOD
                {
                    naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(3).setValueEdited(pnd_W_First_Ann_Dod,new ReportEditMask("999999"));                                  //Natural: MOVE EDITED #W-FIRST-ANN-DOD ( EM = 999999 ) TO NAZ-TBL-SCRN-FLD-DSCRPTN ( 3 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(3).setValueEdited(pnd_W_Scnd_Ann_Dod,new ReportEditMask("999999"));                                   //Natural: MOVE EDITED #W-SCND-ANN-DOD ( EM = 999999 ) TO NAZ-TBL-SCRN-FLD-DSCRPTN ( 3 )
                }                                                                                                                                                         //Natural: END-IF
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(4).setValueEdited(pnd_W_Pend_Date,new ReportEditMask("999999"));                                          //Natural: MOVE EDITED #W-PEND-DATE ( EM = 999999 ) TO NAZ-TBL-SCRN-FLD-DSCRPTN ( 4 )
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(5).setValue(pnd_W_Detail_Tiaa_Pnd_W_Dtl_Pend);                                                            //Natural: ASSIGN NAZ-TBL-SCRN-FLD-DSCRPTN ( 5 ) := #W-DTL-PEND
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(6).setValueEdited(pnd_W_Mde_Cde,new ReportEditMask("999"));                                               //Natural: MOVE EDITED #W-MDE-CDE ( EM = 999 ) TO NAZ-TBL-SCRN-FLD-DSCRPTN ( 6 )
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(7).setValue(pnd_W_Final_Dte);                                                                             //Natural: ASSIGN NAZ-TBL-SCRN-FLD-DSCRPTN ( 7 ) := #W-FINAL-DTE
                naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(8).setValue(pnd_W_Exception_Msg);                                                                         //Natural: ASSIGN NAZ-TBL-SCRN-FLD-DSCRPTN ( 8 ) := #W-EXCEPTION-MSG
                vw_naz_Table_Ddm.insertDBRow();                                                                                                                           //Natural: STORE NAZ-TABLE-DDM
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Excp_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #EXCP-CNT
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Write_Expiring_Contracts() throws Exception                                                                                                          //Natural: WRITE-EXPIRING-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ099");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ099'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("EXP");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'EXP'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(DbsUtil.compress(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct, pnd_Wrksht_File_Pnd_W_Wrksht_Payee));                      //Natural: COMPRESS #W-WRKSHT-CNTRCT #W-WRKSHT-PAYEE INTO #NAZ-TABLE-LVL3-ID
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND02", true)))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(vw_naz_Table_Ddm.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN NAZ-TBL-RCRD-TYP-IND := 'C'
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ099");                                                                                                    //Natural: ASSIGN NAZ-TBL-RCRD-LVL1-ID := 'NAZ099'
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setValue("EXP");                                                                                                       //Natural: ASSIGN NAZ-TBL-RCRD-LVL2-ID := 'EXP'
                naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id);                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-LVL3-ID := #NAZ-TABLE-LVL3-ID
                vw_naz_Table_Ddm.insertDBRow();                                                                                                                           //Natural: STORE NAZ-TABLE-DDM
                pnd_Naz_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NAZ-CNT
                if (condition(pnd_Naz_Cnt.greater(100)))                                                                                                                  //Natural: IF #NAZ-CNT GT 100
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Naz_Cnt.reset();                                                                                                                                  //Natural: RESET #NAZ-CNT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Naz_Written.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NAZ-WRITTEN
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Write_Expiring_Contracts_Report() throws Exception                                                                                                   //Natural: WRITE-EXPIRING-CONTRACTS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, pnd_Header_Term_Pnd_Hdr_Term_Lit,pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit,pnd_Header_Term_Pnd_Hdr_Term_Dte);                                    //Natural: WRITE ( 1 ) #HEADER-TERM
        if (Global.isEscape()) return;
        pnd_W_Exp_Line.reset();                                                                                                                                           //Natural: RESET #W-EXP-LINE #NAZ-CNT
        pnd_Naz_Cnt.reset();
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ099");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ099'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("EXP");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'EXP'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(" ");                                                                                                            //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ' '
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ04",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ04:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ04")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                                                                             //Natural: ACCEPT IF NAZ-TBL-RCRD-TYP-IND = 'C'
            {
                continue;
            }
            pnd_Naz_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #NAZ-CNT
            if (condition(pnd_Naz_Cnt.equals(1)))                                                                                                                         //Natural: IF #NAZ-CNT EQ 1
            {
                pnd_W_Exp_Line.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id);                                                                                              //Natural: ASSIGN #W-EXP-LINE := NAZ-TBL-RCRD-LVL3-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Naz_Cnt.lessOrEqual(5)))                                                                                                                //Natural: IF #NAZ-CNT LE 5
                {
                    pnd_W_Exp_Line.setValue(DbsUtil.compress(pnd_W_Exp_Line, naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id));                                                        //Natural: COMPRESS #W-EXP-LINE NAZ-TBL-RCRD-LVL3-ID INTO #W-EXP-LINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Naz_Cnt.setValue(1);                                                                                                                              //Natural: ASSIGN #NAZ-CNT := 1
                    getReports().write(1, pnd_W_Exp_Line);                                                                                                                //Natural: WRITE ( 1 ) #W-EXP-LINE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_W_Exp_Line.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id);                                                                                          //Natural: ASSIGN #W-EXP-LINE := NAZ-TBL-RCRD-LVL3-ID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Naz_Cnt.greater(getZero())))                                                                                                                    //Natural: IF #NAZ-CNT GT 0
        {
            getReports().write(1, pnd_W_Exp_Line);                                                                                                                        //Natural: WRITE ( 1 ) #W-EXP-LINE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (pnd_Term.getBoolean())))                                                                                                             //Natural: IF NOT #TERM
                    {
                        pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Product.setValue("TIAA");                                                                                      //Natural: ASSIGN #WRKSHT-PRODUCT := #TOT-HDR1M3-PRDCT := 'TIAA'
                        pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Prdct.setValue("TIAA");
                        if (condition(pnd_Totals.getBoolean()))                                                                                                           //Natural: IF #TOTALS
                        {
                            pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Chk_Dte_Pnd_W_Chk_Mm,      //Natural: COMPRESS #W-CHK-MM '/' #W-CHK-YYYY INTO #TIAA-COLMN-HDR2-TOT-DTE LEAVING NO
                                "/", pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy));
                            pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdrm3_Var.setValue(pnd_Ia3300_Hdr_Lit);                                                                      //Natural: ASSIGN #TOT-HDRM3-VAR := #IA3300-HDR-LIT
                            getReports().write(1, pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Fill1m3_Var,pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Fill1,pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Var, //Natural: WRITE ( 1 ) #HDR-TOT-LIT-IA3300M3
                                pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Prdct,pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdrm3_Var,pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Chk,
                                pnd_Hdr_Tot_Lit_Ia3300m3_Pnd_Tot_Hdr1m3_Date);
                            getReports().write(1, pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill,pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit);              //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR1-TOT
                            getReports().write(1, pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1,             //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR2-TOT
                                pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3,
                                pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte);
                            getReports().write(1, pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1,             //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR3-TOT
                                pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3,
                                pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4);
                            //*   #WRKSHT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Hdrm3.setValue(pnd_Ia3300_Hdr_Lit);                                                                        //Natural: ASSIGN #WRKSHT-HDRM3 := #IA3300-HDR-LIT
                            getReports().write(1, pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Fill1m3,pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Product,pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Hdrm3, //Natural: WRITE ( 1 ) #HDR-WRKSHT-IA3300M3
                                pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Lit1,pnd_Hdr_Wrksht_Ia3300m3_Pnd_Wrksht_Date);
                            getReports().write(1, pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1,pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit);                                             //Natural: WRITE ( 1 ) #WRKSHT-HDR1
                            getReports().write(1, pnd_Tiaa_Wrksht_Hdr2);                                                                                                  //Natural: WRITE ( 1 ) #TIAA-WRKSHT-HDR2
                            getReports().write(1, pnd_Tiaa_Wrksht_Hdr3);                                                                                                  //Natural: WRITE ( 1 ) #TIAA-WRKSHT-HDR3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
