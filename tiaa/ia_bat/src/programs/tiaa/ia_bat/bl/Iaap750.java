/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:32 PM
**        * FROM NATURAL PROGRAM : Iaap750
************************************************************
**        * FILE NAME            : Iaap750.java
**        * CLASS NAME           : Iaap750
**        * INSTANCE NAME        : Iaap750
************************************************************
************************************************************************
* PROGRAM   : IAAP750
* FUNCTION  : IVC REPORTS / UPDATE
*
* CHANGE HISTORY: LOGIC NEW ANNUITIZATION REMOVE CALL TO AIAN003
*     6/00        ADD LOGIC TO HANDLE NEW PA SELECT FUNDS
*                 DO SCAN ON 6/00
*
*     1/98        ADD LOGIC TO HANDLE CREF MONTHLY UNIT VALUES
*                 DO SCAN ON 1/98
*
*     5/97        FOR CREF UNIT VALUES NOW IN CREF FUND RECORD
*                 DO SCAN ON 5/97
*
*     9/96        LOGIC TO USE EXTERNALIZATION FILE FOR CREF PRODUCTS
*                 DO SCAN ON 9/96
*     4/02        CHANGED IAAN0510 TO IAAN051Z FOR NEW EXTERNALZATION
*     6/03    1)  ADDED NEW ROLL DEST CODES 57BT & 57BC
*             2)  ADDED NEW EXTRACT FILE OF TPA CONTRACTS WITH IVC
*                 INFORMATION TO SYNC IAIQ & TPA REINVESTMENT FILES
*                 DO SCAN ON 6/03
*     4/08        ADDED LOGIC FOR ROTH - DO NOT UPDATE IVC USED
*                 DO SCAN ON 4/08
*     12/08       ADDED RESTARTABLILTY LOGIC
*      1/09       ADDED TIAA ACCESS AND ADDITIONAL ROTH ORIGIN CODES
*      6/11       ADDED NEW FIELDS FOR AIAN026 FOR MEOW
*      3/12       RATE BASE EXPANSION
*     04/2017 OS  PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap750 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Parm_Fund_1;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;

    private DataAccessProgramView vw_get_Cntrct_Prtcpnt_Role;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField get_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField get_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField get_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField get_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;

    private DataAccessProgramView vw_new_Cntrct_Prtcpnt_Role;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField new_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField new_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField new_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField new_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;

    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd_1;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Rcrd_1__R_Field_2;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte;

    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Lst_Trans_Dte;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;

    private DataAccessProgramView vw_get_Deduction;
    private DbsField get_Deduction_Lst_Trans_Dte;
    private DbsField get_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField get_Deduction_Ddctn_Payee_Cde;
    private DbsField get_Deduction_Ddctn_Id_Nbr;
    private DbsField get_Deduction_Ddctn_Cde;
    private DbsField get_Deduction_Ddctn_Seq_Nbr;
    private DbsField get_Deduction_Ddctn_Payee;
    private DbsField get_Deduction_Ddctn_Per_Amt;
    private DbsField get_Deduction_Ddctn_Ytd_Amt;
    private DbsField get_Deduction_Ddctn_Pd_To_Dte;
    private DbsField get_Deduction_Ddctn_Tot_Amt;
    private DbsField get_Deduction_Ddctn_Intent_Cde;
    private DbsField get_Deduction_Ddctn_Strt_Dte;
    private DbsField get_Deduction_Ddctn_Stp_Dte;
    private DbsField get_Deduction_Ddctn_Final_Dte;

    private DataAccessProgramView vw_new_Deduction;
    private DbsField new_Deduction_Lst_Trans_Dte;
    private DbsField new_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField new_Deduction_Ddctn_Payee_Cde;
    private DbsField new_Deduction_Ddctn_Id_Nbr;
    private DbsField new_Deduction_Ddctn_Cde;
    private DbsField new_Deduction_Ddctn_Seq_Nbr;
    private DbsField new_Deduction_Ddctn_Payee;
    private DbsField new_Deduction_Ddctn_Per_Amt;
    private DbsField new_Deduction_Ddctn_Ytd_Amt;
    private DbsField new_Deduction_Ddctn_Pd_To_Dte;
    private DbsField new_Deduction_Ddctn_Tot_Amt;
    private DbsField new_Deduction_Ddctn_Intent_Cde;
    private DbsField new_Deduction_Ddctn_Strt_Dte;
    private DbsField new_Deduction_Ddctn_Stp_Dte;
    private DbsField new_Deduction_Ddctn_Final_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Rcrd_Key;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_Month;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Restart_Key;

    private DbsGroup pnd_Restart_Key__R_Field_3;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ppcn;
    private DbsField pnd_Restart_Key_Pnd_Restart_Payee;
    private DbsField pnd_Last_Key;

    private DbsGroup pnd_Last_Key__R_Field_4;
    private DbsField pnd_Last_Key_Pnd_Last_Ppcn;
    private DbsField pnd_Last_Key_Pnd_Last_Payee;
    private DbsField pnd_Purge;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_5;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_Occ_A;

    private DbsGroup pnd_Occ_A__R_Field_6;
    private DbsField pnd_Occ_A_Pnd_Occ_1a;
    private DbsField pnd_Occ_A_Pnd_Occ;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_7;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_8;
    private DbsField pnd_Today_Pnd_Today_N;

    private DbsGroup pnd_Today__R_Field_9;
    private DbsField pnd_Today_Pnd_Today_Yyyymm;
    private DbsField pnd_Today_Pnd_Today_Dd;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_10;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;
    private DbsField pnd_Fund_Cde_A;

    private DbsGroup pnd_Fund_Cde_A__R_Field_11;
    private DbsField pnd_Fund_Cde_A_Pnd_Fund_Cde_N;
    private DbsField pnd_Cntrl_Dte_A;

    private DbsGroup pnd_Cntrl_Dte_A__R_Field_12;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm;

    private DbsGroup pnd_Cntrl_Dte_A__R_Field_13;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy;
    private DbsField pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Ivc_Amt;
    private DbsField pnd_Remain_Ivc;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Update;
    private DbsField pnd_Update_R2;
    private DbsField pnd_Accum_Tot;
    private DbsField pnd_Temp_Accum;
    private DbsField pnd_Accum_Units;
    private DbsField pnd_Accum_Div;
    private DbsField pnd_Return_Code;

    private DbsGroup ia_Aian026_Linkage;
    private DbsField ia_Aian026_Linkage_Ia_Call_Type;
    private DbsField ia_Aian026_Linkage_Ia_Fund_Code;
    private DbsField ia_Aian026_Linkage_Ia_Reval_Methd;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_14;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Check_Dd;

    private DbsGroup ia_Aian026_Linkage__R_Field_15;
    private DbsField ia_Aian026_Linkage_Ia_Check_Year;
    private DbsField ia_Aian026_Linkage_Ia_Check_Month;
    private DbsField ia_Aian026_Linkage_Ia_Check_Day;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_16;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Day;

    private DbsGroup ia_Aian026_Linkage__R_Field_17;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date_A;
    private DbsField ia_Aian026_Linkage_Return_Code_11;

    private DbsGroup ia_Aian026_Linkage__R_Field_18;
    private DbsField ia_Aian026_Linkage_Return_Code_Pgm;
    private DbsField ia_Aian026_Linkage_Return_Code;
    private DbsField ia_Aian026_Linkage_Auv_Returned;
    private DbsField ia_Aian026_Linkage_Auv_Date_Return;
    private DbsField ia_Aian026_Linkage_Pnd_Days_In_Request_Month;
    private DbsField ia_Aian026_Linkage_Pnd_Days_In_Particip_Month;

    private DbsGroup pnd_O_Tpa_Ivc;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Pin;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Dest;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Tot_Contracts;
    private DbsField pnd_Tot_Ivc_Amt;
    private DbsField pnd_Tot_Used_Ivc;
    private DbsField pnd_Tot_Per_Ivc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_Parm_Fund_1 = localVariables.newFieldInRecord("pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("IAA_CNTRCT_PRTCPNT_ROLE_RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_get_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_get_Cntrct_Prtcpnt_Role", "GET-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        get_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        get_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        get_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        get_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        get_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        get_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        get_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        get_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_get_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = get_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        get_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        get_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        get_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        get_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        get_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        get_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        get_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        get_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        get_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        get_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        get_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        get_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_get_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("GET_CNTRCT_PRTCPNT_ROLE_RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        get_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        get_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_get_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("get_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        registerRecord(vw_get_Cntrct_Prtcpnt_Role);

        vw_new_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_new_Cntrct_Prtcpnt_Role", "NEW-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        new_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        new_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        new_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        new_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        new_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        new_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        new_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_new_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = new_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        new_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        new_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        new_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        new_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        new_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        new_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        new_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        new_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        new_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        new_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        new_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        new_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_new_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("NEW_CNTRCT_PRTCPNT_ROLE_RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        new_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        new_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_new_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("new_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        registerRecord(vw_new_Cntrct_Prtcpnt_Role);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Cref_Fund_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd_1", "IAA-CREF-FUND-RCRD-1"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_CREF_CNTRCT_PAYEE_CDE", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Rcrd_1__R_Field_2 = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1__R_Field_2", "REDEFINE", iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_1__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_1__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_CREF_UNIT_VAL", "CREF-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_CREF_OLD_PER_AMT", "CREF-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("IAA_CREF_FUND_RCRD_1_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_CREF_RATE_DTE", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_RCRD_1_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("IAA_CREF_FUND_RCRD_1_CREF_XFR_ISS_DTE", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_iaa_Cref_Fund_Rcrd_1);

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Ytd_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_Ddctn_Pd_To_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Deduction);

        vw_get_Deduction = new DataAccessProgramView(new NameInfo("vw_get_Deduction", "GET-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        get_Deduction_Lst_Trans_Dte = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        get_Deduction_Ddctn_Ppcn_Nbr = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        get_Deduction_Ddctn_Payee_Cde = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        get_Deduction_Ddctn_Id_Nbr = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        get_Deduction_Ddctn_Cde = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        get_Deduction_Ddctn_Seq_Nbr = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        get_Deduction_Ddctn_Payee = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        get_Deduction_Ddctn_Per_Amt = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        get_Deduction_Ddctn_Ytd_Amt = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        get_Deduction_Ddctn_Pd_To_Dte = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        get_Deduction_Ddctn_Tot_Amt = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        get_Deduction_Ddctn_Intent_Cde = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        get_Deduction_Ddctn_Strt_Dte = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        get_Deduction_Ddctn_Stp_Dte = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        get_Deduction_Ddctn_Final_Dte = vw_get_Deduction.getRecord().newFieldInGroup("get_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_get_Deduction);

        vw_new_Deduction = new DataAccessProgramView(new NameInfo("vw_new_Deduction", "NEW-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        new_Deduction_Lst_Trans_Dte = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        new_Deduction_Ddctn_Ppcn_Nbr = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        new_Deduction_Ddctn_Payee_Cde = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        new_Deduction_Ddctn_Id_Nbr = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        new_Deduction_Ddctn_Cde = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        new_Deduction_Ddctn_Seq_Nbr = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        new_Deduction_Ddctn_Payee = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        new_Deduction_Ddctn_Per_Amt = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        new_Deduction_Ddctn_Ytd_Amt = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        new_Deduction_Ddctn_Pd_To_Dte = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        new_Deduction_Ddctn_Tot_Amt = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        new_Deduction_Ddctn_Intent_Cde = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        new_Deduction_Ddctn_Strt_Dte = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        new_Deduction_Ddctn_Stp_Dte = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        new_Deduction_Ddctn_Final_Dte = vw_new_Deduction.getRecord().newFieldInGroup("new_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_new_Deduction);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_Cntrl_Rcrd_Key = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Rcrd_Key", "CNTRL-RCRD-KEY", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRL_RCRD_KEY");
        iaa_Cntrl_Rcrd_1_Cntrl_Rcrd_Key.setSuperDescriptor(true);
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrl_Rcrd_Month = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_Month", "IAA-CNTRL-RCRD-MONTH"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_MONTH_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_Month.getRecord().newGroupArrayInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Units = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Amt = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        registerRecord(vw_iaa_Cntrl_Rcrd_Month);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Restart_Key = localVariables.newFieldInRecord("pnd_Restart_Key", "#RESTART-KEY", FieldType.STRING, 12);

        pnd_Restart_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Restart_Key__R_Field_3", "REDEFINE", pnd_Restart_Key);
        pnd_Restart_Key_Pnd_Restart_Ppcn = pnd_Restart_Key__R_Field_3.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ppcn", "#RESTART-PPCN", FieldType.STRING, 
            10);
        pnd_Restart_Key_Pnd_Restart_Payee = pnd_Restart_Key__R_Field_3.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Payee", "#RESTART-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Last_Key = localVariables.newFieldInRecord("pnd_Last_Key", "#LAST-KEY", FieldType.STRING, 12);

        pnd_Last_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Last_Key__R_Field_4", "REDEFINE", pnd_Last_Key);
        pnd_Last_Key_Pnd_Last_Ppcn = pnd_Last_Key__R_Field_4.newFieldInGroup("pnd_Last_Key_Pnd_Last_Ppcn", "#LAST-PPCN", FieldType.STRING, 10);
        pnd_Last_Key_Pnd_Last_Payee = pnd_Last_Key__R_Field_4.newFieldInGroup("pnd_Last_Key_Pnd_Last_Payee", "#LAST-PAYEE", FieldType.NUMERIC, 2);
        pnd_Purge = localVariables.newFieldInRecord("pnd_Purge", "#PURGE", FieldType.BOOLEAN, 1);
        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_5", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_Occ_A = localVariables.newFieldInRecord("pnd_Occ_A", "#OCC-A", FieldType.STRING, 3);

        pnd_Occ_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Occ_A__R_Field_6", "REDEFINE", pnd_Occ_A);
        pnd_Occ_A_Pnd_Occ_1a = pnd_Occ_A__R_Field_6.newFieldInGroup("pnd_Occ_A_Pnd_Occ_1a", "#OCC-1A", FieldType.STRING, 1);
        pnd_Occ_A_Pnd_Occ = pnd_Occ_A__R_Field_6.newFieldInGroup("pnd_Occ_A_Pnd_Occ", "#OCC", FieldType.NUMERIC, 2);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_7", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_7.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_7.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_8 = localVariables.newGroupInRecord("pnd_Today__R_Field_8", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_N = pnd_Today__R_Field_8.newFieldInGroup("pnd_Today_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_9 = pnd_Today__R_Field_8.newGroupInGroup("pnd_Today__R_Field_9", "REDEFINE", pnd_Today_Pnd_Today_N);
        pnd_Today_Pnd_Today_Yyyymm = pnd_Today__R_Field_9.newFieldInGroup("pnd_Today_Pnd_Today_Yyyymm", "#TODAY-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Today_Pnd_Today_Dd = pnd_Today__R_Field_9.newFieldInGroup("pnd_Today_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_10", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_10.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_10.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_10.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);
        pnd_Fund_Cde_A = localVariables.newFieldInRecord("pnd_Fund_Cde_A", "#FUND-CDE-A", FieldType.STRING, 2);

        pnd_Fund_Cde_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Fund_Cde_A__R_Field_11", "REDEFINE", pnd_Fund_Cde_A);
        pnd_Fund_Cde_A_Pnd_Fund_Cde_N = pnd_Fund_Cde_A__R_Field_11.newFieldInGroup("pnd_Fund_Cde_A_Pnd_Fund_Cde_N", "#FUND-CDE-N", FieldType.NUMERIC, 
            2);
        pnd_Cntrl_Dte_A = localVariables.newFieldInRecord("pnd_Cntrl_Dte_A", "#CNTRL-DTE-A", FieldType.STRING, 8);

        pnd_Cntrl_Dte_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Cntrl_Dte_A__R_Field_12", "REDEFINE", pnd_Cntrl_Dte_A);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm = pnd_Cntrl_Dte_A__R_Field_12.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm", "#CNTRL-YYYYMM", FieldType.NUMERIC, 
            6);

        pnd_Cntrl_Dte_A__R_Field_13 = pnd_Cntrl_Dte_A__R_Field_12.newGroupInGroup("pnd_Cntrl_Dte_A__R_Field_13", "REDEFINE", pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy = pnd_Cntrl_Dte_A__R_Field_13.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyy", "#CNTRL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm = pnd_Cntrl_Dte_A__R_Field_13.newFieldInGroup("pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm", "#CNTRL-MM", FieldType.NUMERIC, 2);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Ivc_Amt = localVariables.newFieldInRecord("pnd_Ivc_Amt", "#IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Remain_Ivc = localVariables.newFieldInRecord("pnd_Remain_Ivc", "#REMAIN-IVC", FieldType.NUMERIC, 9, 2);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 3);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Update_R2 = localVariables.newFieldInRecord("pnd_Update_R2", "#UPDATE-R2", FieldType.BOOLEAN, 1);
        pnd_Accum_Tot = localVariables.newFieldInRecord("pnd_Accum_Tot", "#ACCUM-TOT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Accum = localVariables.newFieldInRecord("pnd_Temp_Accum", "#TEMP-ACCUM", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Accum_Units = localVariables.newFieldInRecord("pnd_Accum_Units", "#ACCUM-UNITS", FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Accum_Div = localVariables.newFieldInRecord("pnd_Accum_Div", "#ACCUM-DIV", FieldType.NUMERIC, 7, 3);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 2);

        ia_Aian026_Linkage = localVariables.newGroupInRecord("ia_Aian026_Linkage", "IA-AIAN026-LINKAGE");
        ia_Aian026_Linkage_Ia_Call_Type = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Call_Type", "IA-CALL-TYPE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Fund_Code = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Fund_Code", "IA-FUND-CODE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Reval_Methd = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Reval_Methd", "IA-REVAL-METHD", FieldType.STRING, 
            1);
        ia_Aian026_Linkage_Ia_Check_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date", "IA-CHECK-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_14 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_14", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Date_Ccyymm = ia_Aian026_Linkage__R_Field_14.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date_Ccyymm", "IA-CHECK-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        ia_Aian026_Linkage_Ia_Check_Dd = ia_Aian026_Linkage__R_Field_14.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Dd", "IA-CHECK-DD", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_15 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_15", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Year = ia_Aian026_Linkage__R_Field_15.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Year", "IA-CHECK-YEAR", FieldType.NUMERIC, 
            4);
        ia_Aian026_Linkage_Ia_Check_Month = ia_Aian026_Linkage__R_Field_15.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Month", "IA-CHECK-MONTH", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Check_Day = ia_Aian026_Linkage__R_Field_15.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Day", "IA-CHECK-DAY", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Issue_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date", "IA-ISSUE-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_16 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_16", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Ccyymm = ia_Aian026_Linkage__R_Field_16.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Ccyymm", "IA-ISSUE-CCYYMM", FieldType.NUMERIC, 
            6);
        ia_Aian026_Linkage_Ia_Issue_Day = ia_Aian026_Linkage__R_Field_16.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Day", "IA-ISSUE-DAY", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_17 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_17", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Date_A = ia_Aian026_Linkage__R_Field_17.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date_A", "IA-ISSUE-DATE-A", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code_11 = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Return_Code_11", "RETURN-CODE-11", FieldType.STRING, 
            11);

        ia_Aian026_Linkage__R_Field_18 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_18", "REDEFINE", ia_Aian026_Linkage_Return_Code_11);
        ia_Aian026_Linkage_Return_Code_Pgm = ia_Aian026_Linkage__R_Field_18.newFieldInGroup("ia_Aian026_Linkage_Return_Code_Pgm", "RETURN-CODE-PGM", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code = ia_Aian026_Linkage__R_Field_18.newFieldInGroup("ia_Aian026_Linkage_Return_Code", "RETURN-CODE", FieldType.NUMERIC, 
            3);
        ia_Aian026_Linkage_Auv_Returned = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Returned", "AUV-RETURNED", FieldType.NUMERIC, 8, 
            4);
        ia_Aian026_Linkage_Auv_Date_Return = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Date_Return", "AUV-DATE-RETURN", FieldType.NUMERIC, 
            8);
        ia_Aian026_Linkage_Pnd_Days_In_Request_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        ia_Aian026_Linkage_Pnd_Days_In_Particip_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);

        pnd_O_Tpa_Ivc = localVariables.newGroupInRecord("pnd_O_Tpa_Ivc", "#O-TPA-IVC");
        pnd_O_Tpa_Ivc_Pnd_O_Pin = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 12);
        pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr", "#O-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde", "#O-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind", "#O-RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1);
        pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt", "#O-PER-IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt", "#O-IVC-USED-AMT", FieldType.NUMERIC, 9, 
            2);
        pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt", "#O-IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_O_Tpa_Ivc_Pnd_O_Dest = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Dest", "#O-DEST", FieldType.STRING, 4);
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr", "#O-RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind", "#O-RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind", "#O-RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde = pnd_O_Tpa_Ivc.newFieldArrayInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde", "#O-RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1, 4));
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde", "#O-RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.NUMERIC, 2);
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind = pnd_O_Tpa_Ivc.newFieldInGroup("pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind", "#O-RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Tot_Contracts = localVariables.newFieldInRecord("pnd_Tot_Contracts", "#TOT-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Tot_Ivc_Amt = localVariables.newFieldInRecord("pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Used_Ivc = localVariables.newFieldInRecord("pnd_Tot_Used_Ivc", "#TOT-USED-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Per_Ivc = localVariables.newFieldInRecord("pnd_Tot_Per_Ivc", "#TOT-PER-IVC", FieldType.NUMERIC, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_get_Cntrct_Prtcpnt_Role.reset();
        vw_new_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cref_Fund_Rcrd_1.reset();
        vw_iaa_Deduction.reset();
        vw_get_Deduction.reset();
        vw_new_Deduction.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrl_Rcrd_Month.reset();
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
        ia_Aian026_Linkage_Ia_Call_Type.setInitialValue("P");
        ia_Aian026_Linkage_Ia_Reval_Methd.setInitialValue("M");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap750() throws Exception
    {
        super("Iaap750");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE;//Natural: FORMAT PS = 55 LS = 133;//Natural: FORMAT ( 1 ) PS = 55 LS = 133;//Natural: FORMAT ( 2 ) PS = 55 LS = 133;//Natural: FORMAT ( 3 ) PS = 55 LS = 133
        //*  ADDED FOLLOWING   9/96
        //*  READS EXTERNALIZATION FILE & PASSES
        //*  TIAA & CREF ACTIVE PRODUCTS
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY
        (
        "READ01",
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Cntrl_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CNTRL-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  12/08 CHECK IF RESTART NEEDED.
                                                                                                                                                                          //Natural: PERFORM CHECK-RESTART
        sub_Check_Restart();
        if (condition(Global.isEscape())) {return;}
        //*  WILL HAVE
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM #LAST-PPCN
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_Last_Key_Pnd_Last_Ppcn, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
        {
            //*  THE RESTART KEY FROM AN ABEND
            vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                  //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM CNTRCT-PPCN-NBR
            (
            "R1",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", iaa_Cntrct_Cntrct_Ppcn_Nbr, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
            );
            R1:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("R1")))
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cntrct_Cntrct_Ppcn_Nbr)))                                                        //Natural: IF CNTRCT-PART-PPCN-NBR NE IAA-CNTRCT.CNTRCT-PPCN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  12/08 THIS WILL HAPPEN
                //*  ONLY IF THERE's a
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.equals(pnd_Last_Key_Pnd_Last_Ppcn) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.lessOrEqual(pnd_Last_Key_Pnd_Last_Payee))) //Natural: IF CNTRCT-PART-PPCN-NBR = #LAST-PPCN AND CNTRCT-PART-PAYEE-CDE LE #LAST-PAYEE
                {
                    //*  RESTART RECORD DUE TO
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  AN ABEND
                }                                                                                                                                                         //Natural: END-IF
                pnd_Payment_Due.reset();                                                                                                                                  //Natural: RESET #PAYMENT-DUE
                                                                                                                                                                          //Natural: PERFORM CHECK-PAYMENT
                sub_Check_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.notEquals(9) && pnd_Payment_Due.getBoolean()))                                                    //Natural: IF CNTRCT-ACTVTY-CDE NE 9 AND #PAYMENT-DUE
                {
                    vw_new_Cntrct_Prtcpnt_Role.setValuesByName(vw_iaa_Cntrct_Prtcpnt_Role);                                                                               //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO NEW-CNTRCT-PRTCPNT-ROLE
                    //*                                                                                                                                                   //Natural: DECIDE FOR FIRST CONDITION
                    short decideConditionsMet600 = 0;                                                                                                                     //Natural: WHEN CNTRCT-ACTVTY-CDE = 5
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(5)))
                    {
                        decideConditionsMet600++;
                        new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(7);                                                                                            //Natural: ASSIGN NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 7
                        pnd_Update.setValue(true);                                                                                                                        //Natural: ASSIGN #UPDATE := TRUE
                    }                                                                                                                                                     //Natural: WHEN CNTRCT-FINAL-PER-PAY-DTE = #CNTRL-YYYYMM AND CNTRCT-FINAL-PAY-DTE = 0
                    else if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte.equals(getZero())))
                    {
                        decideConditionsMet600++;
                        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.greater(2)) || (iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.less(3)                //Natural: IF ( CNTRCT-PART-PAYEE-CDE > 02 ) OR ( CNTRCT-PART-PAYEE-CDE < 03 AND CNTRCT-OPTN-CDE = 21 )
                            && iaa_Cntrct_Cntrct_Optn_Cde.equals(21))))
                        {
                            new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(3);                                                                                        //Natural: ASSIGN NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 3
                            pnd_Update.setValue(true);                                                                                                                    //Natural: ASSIGN #UPDATE := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN CNTRCT-FINAL-PER-PAY-DTE = #CNTRL-YYYYMM AND CNTRCT-FINAL-PAY-DTE GT #CNTRL-YYYYMM
                    else if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte.greater(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm)))
                    {
                        decideConditionsMet600++;
                        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.greater(2)) || (iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.less(3)                //Natural: IF ( CNTRCT-PART-PAYEE-CDE > 02 ) OR ( CNTRCT-PART-PAYEE-CDE < 03 AND CNTRCT-OPTN-CDE = 21 )
                            && iaa_Cntrct_Cntrct_Optn_Cde.equals(21))))
                        {
                            new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(5);                                                                                        //Natural: ASSIGN NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 5
                            pnd_Update.setValue(true);                                                                                                                    //Natural: ASSIGN #UPDATE := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN CNTRCT-FINAL-PER-PAY-DTE = #CNTRL-YYYYMM AND CNTRCT-FINAL-PAY-DTE = CNTRCT-FINAL-PER-PAY-DTE
                    else if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte.equals(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte.equals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte)))
                    {
                        decideConditionsMet600++;
                        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.greater(2)) || (iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.less(3)                //Natural: IF ( CNTRCT-PART-PAYEE-CDE > 02 ) OR ( CNTRCT-PART-PAYEE-CDE < 03 AND CNTRCT-OPTN-CDE = 21 )
                            && iaa_Cntrct_Cntrct_Optn_Cde.equals(21))))
                        {
                            new_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(7);                                                                                        //Natural: ASSIGN NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 7
                            pnd_Update.setValue(true);                                                                                                                    //Natural: ASSIGN #UPDATE := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("0")))                                                                                   //Natural: IF CNTRCT-PEND-CDE = '0'
                    {
                        //*                                       DETERMINE IF TIAA OR CREF
                        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).equals("T")))                                                                 //Natural: IF CNTRCT-COMPANY-CD ( 1 ) = 'T'
                        {
                            pnd_Occ_A_Pnd_Occ.setValue(1);                                                                                                                //Natural: ASSIGN #OCC := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Occ_A_Pnd_Occ.setValue(2);                                                                                                                //Natural: ASSIGN #OCC := 2
                        }                                                                                                                                                 //Natural: END-IF
                        //*                                                                                                                                               //Natural: DECIDE ON FIRST VALUE CNTRCT-RCVRY-TYPE-IND ( #OCC )
                        short decideConditionsMet637 = 0;                                                                                                                 //Natural: VALUE 0
                        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(pnd_Occ_A_Pnd_Occ).equals(0))))
                        {
                            decideConditionsMet637++;
                            ignore();
                        }                                                                                                                                                 //Natural: VALUE 2, 5
                        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(pnd_Occ_A_Pnd_Occ).equals(2) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(pnd_Occ_A_Pnd_Occ).equals(5))))
                        {
                            decideConditionsMet637++;
                            //*  4/08
                            //*  4/08
                            //*  1/09
                            if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ).greater(getZero()) && ! ((((iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(51)  //Natural: IF CNTRCT-PER-IVC-AMT ( #OCC ) > 0 AND NOT ( CNTRCT-ORGN-CDE = 51 THRU 58 OR CNTRCT-ORGN-CDE = 71 THRU 78 OR CNTRCT-ORGN-CDE = 24 THRU 27 )
                                && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(58)) || (iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(71) && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(78))) 
                                || (iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(24) && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(27)))))))
                            {
                                //*  BYPASS ROTH CONTRACTS - DO NOT UPDATE IVC USED AMOUNT
                                //*  FOR #ACCUM-TOT
                                                                                                                                                                          //Natural: PERFORM GET-FUND-REC
                                sub_Get_Fund_Rec();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("R1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ).lessOrEqual(pnd_Accum_Tot)))                         //Natural: IF CNTRCT-PER-IVC-AMT ( #OCC ) <= #ACCUM-TOT
                                {
                                    new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(pnd_Occ_A_Pnd_Occ).nadd(new_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ)); //Natural: ADD NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( #OCC ) TO NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( #OCC )
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(pnd_Occ_A_Pnd_Occ).nadd(pnd_Accum_Tot);                                          //Natural: ADD #ACCUM-TOT TO NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( #OCC )
                                    new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ).compute(new ComputeParameters(false, new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ)),  //Natural: COMPUTE NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( #OCC ) = NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( #OCC ) - #ACCUM-TOT + CNTRCT-PER-IVC-AMT ( #OCC )
                                        new_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ).subtract(pnd_Accum_Tot).add(iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ)));
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Update.setValue(true);                                                                                                                //Natural: ASSIGN #UPDATE := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 1
                        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(pnd_Occ_A_Pnd_Occ).equals(1))))
                        {
                            decideConditionsMet637++;
                            //*  BYPASS ROTH CONTRACTS - DO NOT UPDATE IVC USED AMOUNT  /* 4/08
                            //*  4/08
                            //*  4/08
                            //*  1/09
                            //*  4/08
                            if (condition((((iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(51) && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(58)) || (iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(71)  //Natural: IF CNTRCT-ORGN-CDE = 51 THRU 58 OR CNTRCT-ORGN-CDE = 71 THRU 78 OR CNTRCT-ORGN-CDE = 24 THRU 27
                                && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(78))) || (iaa_Cntrct_Cntrct_Orgn_Cde.greaterOrEqual(24) && iaa_Cntrct_Cntrct_Orgn_Cde.lessOrEqual(27)))))
                            {
                                ignore();
                                //*  4/08
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM GET-FUND-REC
                                sub_Get_Fund_Rec();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("R1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.greater(2)))                                                                  //Natural: IF CNTRCT-PART-PAYEE-CDE GT 02
                                {
                                    pnd_Remain_Ivc.compute(new ComputeParameters(false, pnd_Remain_Ivc), iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt.getValue(pnd_Occ_A_Pnd_Occ).subtract(iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(pnd_Occ_A_Pnd_Occ))); //Natural: COMPUTE #REMAIN-IVC = CNTRCT-IVC-AMT ( #OCC ) - CNTRCT-IVC-USED-AMT ( #OCC )
                                    if (condition(pnd_Remain_Ivc.less(getZero())))                                                                                        //Natural: IF #REMAIN-IVC LT 0
                                    {
                                        pnd_Remain_Ivc.setValue(0);                                                                                                       //Natural: ASSIGN #REMAIN-IVC := 0
                                    }                                                                                                                                     //Natural: END-IF
                                    if (condition(pnd_Remain_Ivc.greater(pnd_Accum_Tot)))                                                                                 //Natural: IF #REMAIN-IVC GT #ACCUM-TOT
                                    {
                                        pnd_Remain_Ivc.setValue(pnd_Accum_Tot);                                                                                           //Natural: ASSIGN #REMAIN-IVC := #ACCUM-TOT
                                    }                                                                                                                                     //Natural: END-IF
                                    new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(pnd_Occ_A_Pnd_Occ).nadd(pnd_Remain_Ivc);                                         //Natural: ADD #REMAIN-IVC TO NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( #OCC )
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    new_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(pnd_Occ_A_Pnd_Occ).nadd(pnd_Accum_Tot);                                          //Natural: ADD #ACCUM-TOT TO NEW-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( #OCC )
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Update.setValue(true);                                                                                                                //Natural: ASSIGN #UPDATE := TRUE
                                //*  4/08
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                //Natural: ASSIGN #FUND-PPCN := CNTRCT-PART-PPCN-NBR
                        pnd_Fund_Key_Pnd_Fund_Paye.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                               //Natural: ASSIGN #FUND-PAYE := CNTRCT-PART-PAYEE-CDE
                        vw_iaa_Deduction.startDatabaseRead                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #FUND-KEY
                        (
                        "R2",
                        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Fund_Key, WcType.BY) },
                        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
                        );
                        R2:
                        while (condition(vw_iaa_Deduction.readNextRow("R2")))
                        {
                            if (condition(pnd_Fund_Key_Pnd_Fund_Ppcn.notEquals(iaa_Deduction_Ddctn_Ppcn_Nbr) || pnd_Fund_Key_Pnd_Fund_Paye.notEquals(iaa_Deduction_Ddctn_Payee_Cde))) //Natural: IF #FUND-PPCN NE DDCTN-PPCN-NBR OR #FUND-PAYE NE DDCTN-PAYEE-CDE
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
                            }                                                                                                                                             //Natural: END-IF
                            //*  ADDED 6/03
                            if (condition((iaa_Cntrct_Cntrct_Optn_Cde.equals(21) && (((((((((((iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("IRAT")                //Natural: REJECT IF ( CNTRCT-OPTN-CDE = 21 ) AND ( CNTRCT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' OR = '57BT' OR = '57BC' OR = '57BX' )
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("IRAC")) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("IRAX")) 
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("03BT")) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("03BC")) 
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("03BX")) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("QPLT")) 
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("QPLC")) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("QPLX")) 
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("57BT")) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("57BC")) 
                                || iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde.equals("57BX")))))
                            {
                                continue;
                            }
                            if (condition(iaa_Deduction_Ddctn_Stp_Dte.notEquals(getZero()) && iaa_Deduction_Ddctn_Stp_Dte.less(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte)))        //Natural: REJECT IF DDCTN-STP-DTE NE 0 AND DDCTN-STP-DTE < IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE
                            {
                                continue;
                            }
                            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("0")))                                                                           //Natural: IF CNTRCT-PEND-CDE = '0'
                            {
                                vw_new_Deduction.setValuesByName(vw_iaa_Deduction);                                                                                       //Natural: MOVE BY NAME IAA-DEDUCTION TO NEW-DEDUCTION
                                new_Deduction_Ddctn_Ytd_Amt.compute(new ComputeParameters(false, new_Deduction_Ddctn_Ytd_Amt), iaa_Deduction_Ddctn_Ytd_Amt.add(iaa_Deduction_Ddctn_Per_Amt)); //Natural: ASSIGN NEW-DEDUCTION.DDCTN-YTD-AMT := IAA-DEDUCTION.DDCTN-YTD-AMT + IAA-DEDUCTION.DDCTN-PER-AMT
                                new_Deduction_Ddctn_Pd_To_Dte.compute(new ComputeParameters(false, new_Deduction_Ddctn_Pd_To_Dte), iaa_Deduction_Ddctn_Pd_To_Dte.add(new_Deduction_Ddctn_Per_Amt)); //Natural: ASSIGN NEW-DEDUCTION.DDCTN-PD-TO-DTE := IAA-DEDUCTION.DDCTN-PD-TO-DTE + NEW-DEDUCTION.DDCTN-PER-AMT
                                pnd_Update_R2.setValue(true);                                                                                                             //Natural: ASSIGN #UPDATE-R2 := TRUE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Update_R2.getBoolean()))                                                                                                    //Natural: IF #UPDATE-R2
                            {
                                pnd_Update_R2.reset();                                                                                                                    //Natural: RESET #UPDATE-R2
                                //*            ADD 1 TO #REC-CNT
                                G2:                                                                                                                                       //Natural: GET GET-DEDUCTION *ISN ( R2. )
                                vw_get_Deduction.readByID(vw_iaa_Deduction.getAstISN("R2"), "G2");
                                vw_get_Deduction.setValuesByName(vw_new_Deduction);                                                                                       //Natural: MOVE BY NAME NEW-DEDUCTION TO GET-DEDUCTION
                                vw_get_Deduction.updateDBRow("G2");                                                                                                       //Natural: UPDATE ( G2. )
                            }                                                                                                                                             //Natural: END-IF
                            //*          IF #REC-CNT > 50  /* 12/08 START  ET WILL BE TRACKED
                            //*            RESET #REC-CNT                  ON CPR SIDE.
                            //*            END TRANSACTION
                            //*          END-IF            /* 12/08 END
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  PEND-CDE = 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Update.getBoolean()))                                                                                                                   //Natural: IF #UPDATE
                {
                    pnd_Update.reset();                                                                                                                                   //Natural: RESET #UPDATE
                    pnd_Rec_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-CNT
                    G1:                                                                                                                                                   //Natural: GET GET-CNTRCT-PRTCPNT-ROLE *ISN ( R1. )
                    vw_get_Cntrct_Prtcpnt_Role.readByID(vw_iaa_Cntrct_Prtcpnt_Role.getAstISN("R1"), "G1");
                    vw_get_Cntrct_Prtcpnt_Role.setValuesByName(vw_new_Cntrct_Prtcpnt_Role);                                                                               //Natural: MOVE BY NAME NEW-CNTRCT-PRTCPNT-ROLE TO GET-CNTRCT-PRTCPNT-ROLE
                    vw_get_Cntrct_Prtcpnt_Role.updateDBRow("G1");                                                                                                         //Natural: UPDATE ( G1. )
                    //*  ADDED FOLLOWING 6/03 EXTRACT TPA CONTRACTS TO SYNC IVC WITH TPA REINV
                    if (condition(((iaa_Cntrct_Cntrct_Optn_Cde.equals(28) || iaa_Cntrct_Cntrct_Optn_Cde.equals(30)) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.equals(1)))) //Natural: IF CNTRCT-OPTN-CDE = 28 OR = 30 AND CNTRCT-PART-PAYEE-CDE = 01
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-TPA-EXTRACT
                        sub_Write_Tpa_Extract();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  END OF ADD 6/03 EXTRACT TPA CONTRACTS
                }                                                                                                                                                         //Natural: END-IF
                //*  12/08  ET WILL BE TRACKED ON CPR SIDE TO
                if (condition(pnd_Rec_Cnt.greater(40)))                                                                                                                   //Natural: IF #REC-CNT > 40
                {
                    //*         SUPPORT RESTARTABILITY
                    pnd_Rec_Cnt.reset();                                                                                                                                  //Natural: RESET #REC-CNT
                    //*  12/08
                                                                                                                                                                          //Natural: PERFORM ADD-PURGE-RESTART
                    sub_Add_Purge_Restart();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  12/08  -- PURGE RESTART KEY
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Purge.setValue(true);                                                                                                                                         //Natural: ASSIGN #PURGE := TRUE
        //*  12/08
                                                                                                                                                                          //Natural: PERFORM ADD-PURGE-RESTART
        sub_Add_Purge_Restart();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ADDED 6/03 WRITE TOTAL FILE
        getWorkFiles().write(2, false, pnd_Tot_Contracts, pnd_Tot_Ivc_Amt, pnd_Tot_Used_Ivc, pnd_Tot_Per_Ivc);                                                            //Natural: WRITE WORK FILE 2 #TOT-CONTRACTS #TOT-IVC-AMT #TOT-USED-IVC #TOT-PER-IVC
        getReports().write(0, "Total Contracts -->",pnd_Tot_Contracts);                                                                                                   //Natural: WRITE 'Total Contracts -->' #TOT-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, "Total Ivc Amt ---->",pnd_Tot_Ivc_Amt);                                                                                                     //Natural: WRITE 'Total Ivc Amt ---->' #TOT-IVC-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "Total ivd used --->",pnd_Tot_Used_Ivc);                                                                                                    //Natural: WRITE 'Total ivd used --->' #TOT-USED-IVC
        if (Global.isEscape()) return;
        getReports().write(0, "Total per ivc----->",pnd_Tot_Per_Ivc);                                                                                                     //Natural: WRITE 'Total per ivc----->' #TOT-PER-IVC
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING 6/03 EXTRACT TPA CONTRACTS TO SYNC IVC WITH TPA REINV
        //* ****************************************************************
        //*  ==> WRITE EXTRACT DETAIL TPA IVC INFO FOR PAYMENTS DUE
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TPA-EXTRACT
        //*  END OF ADD 6/03 EXTRACT TPA CONTRACTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PAYMENT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-REC
        //*   ADDED FOLLOWING ROUTINE CREF MONTHLY UNIT VALUE   1/98
        //*  ***********************************************************
        //*   GET UNIT VALUE FOR MONTHLY UNIT VALUE CREF CONTRACTS
        //*  ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN026-CREF-UNIT-VALUE
        //*  ****************************************************************
        //*  ADDED FOLLOWING ROUTINE  9/96
        //*  CONVERT 2 BYTE FUND-CDE TO 1 BYTE ALPHA-CDE FOR CALL TO
        //*  ACTUARAIL MODULE FOR UNIT-VALUE
        //*  ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALPHA-PROD-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RESTART
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-PURGE-RESTART
        //* ***********************************************************************
    }
    private void sub_Write_Tpa_Extract() throws Exception                                                                                                                 //Natural: WRITE-TPA-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tot_Contracts.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-CONTRACTS
        pnd_Tot_Ivc_Amt.nadd(get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt.getValue(1));                                                                                         //Natural: ADD GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 ) TO #TOT-IVC-AMT
        pnd_Tot_Used_Ivc.nadd(get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(1));                                                                                   //Natural: ADD GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 1 ) TO #TOT-USED-IVC
        pnd_Tot_Per_Ivc.nadd(get_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(1));                                                                                     //Natural: ADD GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 ) TO #TOT-PER-IVC
        pnd_O_Tpa_Ivc_Pnd_O_Pin.setValue(get_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr);                                                                                             //Natural: ASSIGN #O-PIN := GET-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR
        pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                         //Natural: ASSIGN #O-PART-PPCN-NBR := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
        pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                       //Natural: ASSIGN #O-PART-PAYEE-CDE := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE
        pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(1));                                                           //Natural: ASSIGN #O-RCVRY-TYPE-IND := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( 1 )
        pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(1));                                                                 //Natural: ASSIGN #O-PER-IVC-AMT := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 )
        pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt.getValue(1));                                                                         //Natural: ASSIGN #O-IVC-AMT := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 )
        pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(1));                                                               //Natural: ASSIGN #O-IVC-USED-AMT := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 1 )
        pnd_O_Tpa_Ivc_Pnd_O_Dest.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde);                                                                                  //Natural: ASSIGN #O-DEST := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr.setValue(get_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr);                                                                          //Natural: ASSIGN #O-RLLVR-CNTRCT-NBR := GET-CNTRCT-PRTCPNT-ROLE.RLLVR-CNTRCT-NBR
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind.setValue(get_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind);                                                                                //Natural: ASSIGN #O-RLLVR-IVC-IND := GET-CNTRCT-PRTCPNT-ROLE.RLLVR-IVC-IND
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind.setValue(get_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind);                                                                          //Natural: ASSIGN #O-RLLVR-ELGBLE-IND := GET-CNTRCT-PRTCPNT-ROLE.RLLVR-ELGBLE-IND
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 4
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            if (condition(DbsUtil.is(get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde.getValue(pnd_I).getText(),"N2")))                                                     //Natural: IF GET-CNTRCT-PRTCPNT-ROLE.RLLVR-DSTRBTNG-IRC-CDE ( #I ) IS ( N2 )
            {
                pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde.getValue(pnd_I).compute(new ComputeParameters(false, pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde.getValue(pnd_I)),  //Natural: ASSIGN #O-RLLVR-DSTRBTNG-IRC-CDE ( #I ) := VAL ( GET-CNTRCT-PRTCPNT-ROLE.RLLVR-DSTRBTNG-IRC-CDE ( #I ) )
                    get_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde.getValue(pnd_I).val());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(DbsUtil.is(get_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde.getText(),"N2")))                                                                          //Natural: IF GET-CNTRCT-PRTCPNT-ROLE.RLLVR-ACCPTNG-IRC-CDE IS ( N2 )
        {
            pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde.compute(new ComputeParameters(false, pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde), get_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde.val()); //Natural: ASSIGN #O-RLLVR-ACCPTNG-IRC-CDE := VAL ( GET-CNTRCT-PRTCPNT-ROLE.RLLVR-ACCPTNG-IRC-CDE )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind.setValue(get_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind);                                                                      //Natural: ASSIGN #O-RLLVR-PLN-ADMN-IND := GET-CNTRCT-PRTCPNT-ROLE.RLLVR-PLN-ADMN-IND
        getWorkFiles().write(1, false, pnd_O_Tpa_Ivc);                                                                                                                    //Natural: WRITE WORK FILE 1 #O-TPA-IVC
        getReports().display(0, "/Cntrct-nbr",                                                                                                                            //Natural: DISPLAY '/Cntrct-nbr' #O-PART-PPCN-NBR '/Paye' #O-PART-PAYEE-CDE 'Rcvy/Ind ' #O-RCVRY-TYPE-IND '/Per-Ivc-Amt' #O-PER-IVC-AMT '/Ivc-Used-Amt' #O-IVC-USED-AMT '/Tot-Ivc-Amt' #O-IVC-AMT '/Dest' #O-DEST '/Roll-Cntrct' #O-RLLVR-CNTRCT-NBR 'Roll/Ivc-Ind' #O-RLLVR-IVC-IND 'Roll-Elgble/Ind' #O-RLLVR-ELGBLE-IND 'Distrib/Irc-Cdes' #O-RLLVR-DSTRBTNG-IRC-CDE ( * ) 'Accpt/Irc-Cdes' #O-RLLVR-ACCPTNG-IRC-CDE 'Plan/Admn-Ind' #O-RLLVR-PLN-ADMN-IND
        		pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr,"/Paye",
        		pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde,"Rcvy/Ind ",
        		pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind,"/Per-Ivc-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt,"/Ivc-Used-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt,"/Tot-Ivc-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt,"/Dest",
        		pnd_O_Tpa_Ivc_Pnd_O_Dest,"/Roll-Cntrct",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr,"Roll/Ivc-Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind,"Roll-Elgble/Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind,"Distrib/Irc-Cdes",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde.getValue("*"),"Accpt/Irc-Cdes",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde,"Plan/Admn-Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind);
        if (Global.isEscape()) return;
    }
    private void sub_Check_Payment() throws Exception                                                                                                                     //Natural: CHECK-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet850 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(100))))
        {
            decideConditionsMet850++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(601))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7) ||                   //Natural: IF #CNTRL-MM = 1 OR = 4 OR = 7 OR = 10
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(602))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8) ||                   //Natural: IF #CNTRL-MM = 2 OR = 5 OR = 8 OR = 11
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(603))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9) ||                   //Natural: IF #CNTRL-MM = 3 OR = 6 OR = 9 OR = 12
                pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(701))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7)))                                                              //Natural: IF #CNTRL-MM = 1 OR = 7
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(702))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8)))                                                              //Natural: IF #CNTRL-MM = 2 OR = 8
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(703))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9)))                                                              //Natural: IF #CNTRL-MM = 3 OR = 9
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(704))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))                                                             //Natural: IF #CNTRL-MM = 4 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(705))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))                                                             //Natural: IF #CNTRL-MM = 5 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(706))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6) || pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))                                                             //Natural: IF #CNTRL-MM = 6 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(801))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(1)))                                                                                                        //Natural: IF #CNTRL-MM = 1
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(802))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(2)))                                                                                                        //Natural: IF #CNTRL-MM = 2
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(803))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(3)))                                                                                                        //Natural: IF #CNTRL-MM = 3
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(804))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(4)))                                                                                                        //Natural: IF #CNTRL-MM = 4
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(805))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(5)))                                                                                                        //Natural: IF #CNTRL-MM = 5
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(806))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(6)))                                                                                                        //Natural: IF #CNTRL-MM = 6
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(807))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(7)))                                                                                                        //Natural: IF #CNTRL-MM = 7
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(808))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(8)))                                                                                                        //Natural: IF #CNTRL-MM = 8
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(809))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(9)))                                                                                                        //Natural: IF #CNTRL-MM = 9
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(810))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(10)))                                                                                                       //Natural: IF #CNTRL-MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(811))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(11)))                                                                                                       //Natural: IF #CNTRL-MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(812))))
        {
            decideConditionsMet850++;
            if (condition(pnd_Cntrl_Dte_A_Pnd_Cntrl_Mm.equals(12)))                                                                                                       //Natural: IF #CNTRL-MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Fund_Rec() throws Exception                                                                                                                      //Natural: GET-FUND-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Accum_Tot.reset();                                                                                                                                            //Natural: RESET #ACCUM-TOT #ACCUM-UNITS
        pnd_Accum_Units.reset();
        pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                                //Natural: ASSIGN #FUND-PPCN := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
        pnd_Fund_Key_Pnd_Fund_Paye.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                               //Natural: ASSIGN #FUND-PAYE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE
        if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).equals("T")))                                                                                 //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) = 'T'
        {
            vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                       //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
            (
            "READ03",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
            );
            READ03:
            while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ03")))
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE TIAA-CNTRCT-PPCN-NBR OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE TIAA-CNTRCT-PAYEE-CDE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //* ADDED ACCESS 1/09
                //* ADDED PA SELECT 6/00
                //* ADDED PA SELECT 6/00
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("09") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("11") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.greater("40"))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE = '09' OR = '11' OR IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE GT '40'
                {
                    FOR02:                                                                                                                                                //Natural: FOR #I2 1 C*TIAA-RATE-DATA-GRP
                    for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp)); pnd_I2.nadd(1))
                    {
                        pnd_Accum_Units.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(pnd_I2));                                                                         //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( #I2 ) TO #ACCUM-UNITS
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  ADDED FOLLOWING FOR CREF MONTHLY UNIT VALUES    1/98
                    //*  PA SELECT MONTHLY
                    if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde.equals("W")))                                                                                         //Natural: IF TIAA-CMPNY-CDE = 'W'
                    {
                        pnd_Fund_Cde_A.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde);                                                                                        //Natural: ASSIGN #FUND-CDE-A := IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE
                        //* ADDED PA SELECT 6/00
                        //* ADDED PA SELECT 6/00
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-PROD-CDE
                        sub_Get_Alpha_Prod_Cde();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ia_Aian026_Linkage_Ia_Fund_Code.setValue(pnd_Parm_Fund_1);                                                                                        //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
                        //*  ADDED 1/98
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-CREF-UNIT-VALUE
                        sub_Call_Aian026_Cref_Unit_Value();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Accum_Tot.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                          //Natural: COMPUTE ROUNDED #ACCUM-TOT = #ACCUM-TOT + IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                        //*  COMMENTED  5/97
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR03:                                                                                                                                                //Natural: FOR #I2 1 C*TIAA-RATE-DATA-GRP
                    for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp)); pnd_I2.nadd(1))
                    {
                        pnd_Accum_Tot.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(pnd_I2));                                                                         //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I2 ) TO #ACCUM-TOT
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_iaa_Cref_Fund_Rcrd_1.startDatabaseRead                                                                                                                     //Natural: READ IAA-CREF-FUND-RCRD-1 BY CREF-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
            (
            "READ04",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
            );
            READ04:
            while (condition(vw_iaa_Cref_Fund_Rcrd_1.readNextRow("READ04")))
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE CREF-CNTRCT-PPCN-NBR OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE CREF-CNTRCT-PAYEE-CDE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED OR = 4 1/98
                if (condition(! (iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde.equals("2") || iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde.equals("4"))))                                    //Natural: REJECT IF NOT ( CREF-CMPNY-CDE = '2' OR = '4' )
                {
                    continue;
                }
                pnd_Accum_Units.reset();                                                                                                                                  //Natural: RESET #ACCUM-UNITS
                FOR04:                                                                                                                                                    //Natural: FOR #I2 1 C*CREF-RATE-DATA-GRP
                for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp)); pnd_I2.nadd(1))
                {
                    pnd_Accum_Units.nadd(iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt.getValue(pnd_I2));                                                                           //Natural: ADD IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( #I2 ) TO #ACCUM-UNITS
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADDED FOLLOWING FOR CREF MONTHLY UNIT VALUES    1/98
                //*  MONTHLY UNIT VALUE
                if (condition(iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde.equals("4")))                                                                                           //Natural: IF CREF-CMPNY-CDE = '4'
                {
                    pnd_Fund_Cde_A.setValue(iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde);                                                                                          //Natural: ASSIGN #FUND-CDE-A := IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
                    //*  MNTHLY UNIT VAL
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-PROD-CDE
                    sub_Get_Alpha_Prod_Cde();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ia_Aian026_Linkage_Ia_Fund_Code.setValue(pnd_Parm_Fund_1);                                                                                            //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
                    //*  RECALC FOR PMT
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-CREF-UNIT-VALUE
                    sub_Call_Aian026_Cref_Unit_Value();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ANNUAL UNIT VAL
                    //*  PMT IN FUND RCRD
                    pnd_Accum_Tot.nadd(iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt);                                                                                            //Natural: COMPUTE ROUNDED #ACCUM-TOT = #ACCUM-TOT + IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Aian026_Cref_Unit_Value() throws Exception                                                                                                      //Natural: CALL-AIAN026-CREF-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        ia_Aian026_Linkage_Return_Code.reset();                                                                                                                           //Natural: RESET RETURN-CODE AUV-RETURNED AUV-DATE-RETURN
        ia_Aian026_Linkage_Auv_Returned.reset();
        ia_Aian026_Linkage_Auv_Date_Return.reset();
        ia_Aian026_Linkage_Ia_Check_Date_Ccyymm.setValue(pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm);                                                                               //Natural: ASSIGN IA-CHECK-DATE-CCYYMM := #CNTRL-YYYYMM
        ia_Aian026_Linkage_Ia_Check_Dd.setValue(1);                                                                                                                       //Natural: ASSIGN IA-CHECK-DD := 01
        ia_Aian026_Linkage_Ia_Issue_Ccyymm.setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                                         //Natural: ASSIGN IA-ISSUE-CCYYMM := CNTRCT-ISSUE-DTE
        if (condition(iaa_Cntrct_Cntrct_Issue_Dte_Dd.equals(getZero())))                                                                                                  //Natural: IF CNTRCT-ISSUE-DTE-DD = 0
        {
            ia_Aian026_Linkage_Ia_Issue_Day.setValue(1);                                                                                                                  //Natural: ASSIGN IA-ISSUE-DAY := 01
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ia_Aian026_Linkage_Ia_Issue_Day.setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                                                     //Natural: ASSIGN IA-ISSUE-DAY := CNTRCT-ISSUE-DTE-DD
        }                                                                                                                                                                 //Natural: END-IF
        //*  NEW ACTUARY MODULE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ia_Aian026_Linkage);                                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ia_Aian026_Linkage_Return_Code.notEquals(getZero())))                                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(0, "******************************************************");                                                                              //Natural: WRITE '******************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "**<--------------- E R R O R ---------------------->**");                                                                              //Natural: WRITE '**<--------------- E R R O R ---------------------->**'
            if (Global.isEscape()) return;
            getReports().write(0, "**                                                  **");                                                                              //Natural: WRITE '**                                                  **'
            if (Global.isEscape()) return;
            getReports().write(0, "** NON ZERO RETURN CODE FROM CALL TO AIAN0026 MODULE**");                                                                              //Natural: WRITE '** NON ZERO RETURN CODE FROM CALL TO AIAN0026 MODULE**'
            if (Global.isEscape()) return;
            getReports().write(0, "** FOR  CREF MONTHLY UNIT VALUE                   ****");                                                                              //Natural: WRITE '** FOR  CREF MONTHLY UNIT VALUE                   ****'
            if (Global.isEscape()) return;
            getReports().write(0, "** RETURN CODE---->",ia_Aian026_Linkage_Return_Code);                                                                                  //Natural: WRITE '** RETURN CODE---->' RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "** FOR CONTRACT ---->",pnd_Fund_Key_Pnd_Fund_Ppcn,pnd_Fund_Key_Pnd_Fund_Paye);                                                         //Natural: WRITE '** FOR CONTRACT ---->' #FUND-PPCN #FUND-PAYE
            if (Global.isEscape()) return;
            getReports().write(0, "** PARAMETERS PASSED TO AIAN026 FOLLOW            ****",NEWLINE,"=",ia_Aian026_Linkage_Ia_Call_Type,NEWLINE,"=",ia_Aian026_Linkage_Ia_Fund_Code,NEWLINE,"=",ia_Aian026_Linkage_Ia_Reval_Methd,NEWLINE,"=",ia_Aian026_Linkage_Ia_Check_Date,  //Natural: WRITE '** PARAMETERS PASSED TO AIAN026 FOLLOW            ****' / '=' IA-CALL-TYPE / '=' IA-FUND-CODE / '=' IA-REVAL-METHD / '=' IA-CHECK-DATE ( EM = 9999/99/99 ) / '=' IA-ISSUE-DATE ( EM = 9999/99/99 ) /
                new ReportEditMask ("9999/99/99"),NEWLINE,"=",ia_Aian026_Linkage_Ia_Issue_Date, new ReportEditMask ("9999/99/99"),NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(0, "** PARAMETERS RETURNED FROM AIAN026 FOLLOW        ****",NEWLINE,"=",ia_Aian026_Linkage_Return_Code,NEWLINE);                           //Natural: WRITE '** PARAMETERS RETURNED FROM AIAN026 FOLLOW        ****' / '=' RETURN-CODE /
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ia_Aian026_Linkage_Auv_Returned,NEWLINE,"=",ia_Aian026_Linkage_Auv_Date_Return,                    //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED / '=' AUV-DATE-RETURN /
                NEWLINE);
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ia_Aian026_Linkage_Auv_Returned.equals(getZero())))                                                                                                 //Natural: IF AUV-RETURNED = 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  E R R O R                             *");                                                                          //Natural: WRITE '*                  E R R O R                             *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                        *");                                                                          //Natural: WRITE '*                                                        *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an ANNUITY-UNIT-VALUE of '0'         *");                                                                          //Natural: WRITE '* and returned with an ANNUITY-UNIT-VALUE of "0"         *'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Call_Type);                                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Fund_Code);                                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Reval_Methd);                                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Check_Date);                                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Issue_Date);                                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ia_Aian026_Linkage_Ia_Check_Date);                                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ia_Aian026_Linkage_Auv_Date_Return);                                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ia_Aian026_Linkage_Auv_Returned);                                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Accum.compute(new ComputeParameters(true, pnd_Temp_Accum), (ia_Aian026_Linkage_Auv_Returned.multiply(pnd_Accum_Units)));                                 //Natural: COMPUTE ROUNDED #TEMP-ACCUM = ( AUV-RETURNED * #ACCUM-UNITS )
        pnd_Accum_Tot.nadd(pnd_Temp_Accum);                                                                                                                               //Natural: ADD #TEMP-ACCUM TO #ACCUM-TOT
        //* *COMPUTE ROUNDED #ACCUM-TOT = (AUV-RETURNED * #ACCUM-UNITS)
        //* *  + #ACCUM-TOT
    }
    private void sub_Get_Alpha_Prod_Cde() throws Exception                                                                                                                //Natural: GET-ALPHA-PROD-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Fund_1.reset();                                                                                                                                          //Natural: RESET #PARM-FUND-1 #I
        pnd_I.reset();
        DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(pnd_Fund_Cde_A, true), new                    //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL #FUND-CDE-A GIVING INDEX #I
            ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            pnd_Parm_Fund_1.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                                                      //Natural: ASSIGN #PARM-FUND-1 := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"FUND CODE NOT FOUND IN IAAA0510 READ EXTERN FILE");                                                                                //Natural: WRITE / 'FUND CODE NOT FOUND IN IAAA0510 READ EXTERN FILE'
        if (Global.isEscape()) return;
        getReports().write(0, "FOR #CNTRCT-PPCN-NBR->",iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                       //Natural: WRITE 'FOR #CNTRCT-PPCN-NBR->' CNTRCT-PPCN-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "CHECK IVC AMOUNT FOR THIS CONTRACT");                                                                                                      //Natural: WRITE 'CHECK IVC AMOUNT FOR THIS CONTRACT'
        if (Global.isEscape()) return;
        getReports().write(0, "FUND-CODE ON FILE & NOT FOUND IN TABLE-->",pnd_Fund_Cde_A);                                                                                //Natural: WRITE 'FUND-CODE ON FILE & NOT FOUND IN TABLE-->' #FUND-CDE-A
        if (Global.isEscape()) return;
        //* *WRITE      'CMPNY-CDE ON FILE -->'
        //* *  CREF-CMPNY-CDE
    }
    private void sub_Check_Restart() throws Exception                                                                                                                     //Natural: CHECK-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Last_Key.reset();                                                                                                                                             //Natural: RESET #LAST-KEY
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ070");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ070'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("IAR");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'IAR'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("RESTART");                                                                                                      //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'RESTART'
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_Last_Key.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                                //Natural: ASSIGN #LAST-KEY := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Add_Purge_Restart() throws Exception                                                                                                                 //Natural: ADD-PURGE-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ070");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ070'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("IAR");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'IAR'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("RESTART");                                                                                                      //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'RESTART'
        pnd_Restart_Key_Pnd_Restart_Ppcn.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                          //Natural: ASSIGN #RESTART-PPCN := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR
        pnd_Restart_Key_Pnd_Restart_Payee.setValue(get_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                        //Natural: ASSIGN #RESTART-PAYEE := GET-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "F001",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) },
        1
        );
        F001:
        while (condition(vw_naz_Table_Ddm.readNextRow("F001", true)))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(vw_naz_Table_Ddm.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                if (condition(! (pnd_Purge.getBoolean())))                                                                                                                //Natural: IF NOT #PURGE
                {
                    naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-TYP-IND := 'C'
                    naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ070");                                                                                                //Natural: ASSIGN NAZ-TBL-RCRD-LVL1-ID := 'NAZ070'
                    naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setValue("IAR");                                                                                                   //Natural: ASSIGN NAZ-TBL-RCRD-LVL2-ID := 'IAR'
                    naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setValue("RESTART");                                                                                               //Natural: ASSIGN NAZ-TBL-RCRD-LVL3-ID := 'RESTART'
                    naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setValue(pnd_Restart_Key);                                                                                     //Natural: ASSIGN NAZ-TBL-RCRD-DSCRPTN-TXT := #RESTART-KEY
                    vw_naz_Table_Ddm.insertDBRow();                                                                                                                       //Natural: STORE NAZ-TABLE-DDM
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pnd_Purge.getBoolean()))                                                                                                                        //Natural: IF #PURGE
            {
                vw_naz_Table_Ddm.deleteDBRow("F001");                                                                                                                     //Natural: DELETE ( F001. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setValue(pnd_Restart_Key);                                                                                         //Natural: ASSIGN NAZ-TBL-RCRD-DSCRPTN-TXT := #RESTART-KEY
                vw_naz_Table_Ddm.updateDBRow("F001");                                                                                                                     //Natural: UPDATE ( F001. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, "CHECK DATE",pnd_Cntrl_Dte_A_Pnd_Cntrl_Yyyymm, new ReportEditMask ("9999-99"));                                                 //Natural: WRITE 'CHECK DATE' #CNTRL-YYYYMM ( EM = 9999-99 )
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=133");
        Global.format(1, "PS=55 LS=133");
        Global.format(2, "PS=55 LS=133");
        Global.format(3, "PS=55 LS=133");

        getReports().setDisplayColumns(0, "/Cntrct-nbr",
        		pnd_O_Tpa_Ivc_Pnd_O_Part_Ppcn_Nbr,"/Paye",
        		pnd_O_Tpa_Ivc_Pnd_O_Part_Payee_Cde,"Rcvy/Ind ",
        		pnd_O_Tpa_Ivc_Pnd_O_Rcvry_Type_Ind,"/Per-Ivc-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Per_Ivc_Amt,"/Ivc-Used-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Ivc_Used_Amt,"/Tot-Ivc-Amt",
        		pnd_O_Tpa_Ivc_Pnd_O_Ivc_Amt,"/Dest",
        		pnd_O_Tpa_Ivc_Pnd_O_Dest,"/Roll-Cntrct",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Cntrct_Nbr,"Roll/Ivc-Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Ivc_Ind,"Roll-Elgble/Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Elgble_Ind,"Distrib/Irc-Cdes",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Dstrbtng_Irc_Cde,"Accpt/Irc-Cdes",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Accptng_Irc_Cde,"Plan/Admn-Ind",
        		pnd_O_Tpa_Ivc_Pnd_O_Rllvr_Pln_Admn_Ind);
    }
}
