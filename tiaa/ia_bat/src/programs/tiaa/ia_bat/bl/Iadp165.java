/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:56 PM
**        * FROM NATURAL PROGRAM : Iadp165
************************************************************
**        * FILE NAME            : Iadp165.java
**        * CLASS NAME           : Iadp165
**        * INSTANCE NAME        : Iadp165
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IADP165    READS IAA TRANS RECORD                 *
*      DATE     -  05/01      SELECT TPA,IPRO & P/I NEW ISSUE TRANS  *
*                             CODE 030, 033(MANUAL NEW ISSU)         *
*                             NEW ISSUES FOR QTRLY INTERFACE         *
*                             GET CHECK-DATE FROM CNTRL RECORD       *
*                                                                    *
*                  01/03      CHANGED IF STMNT                       *
*                             DO SCAN ON 01/03 FOR CHANGE            *
*                  04/2017 OS RE-STOWED ONLY FOR PIN EXPANSION       *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp165 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Sve_Aa_Cntl_Today_Dte;

    private DbsGroup pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1;
    private DbsField pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Ccyymm;
    private DbsField pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Mm;
    private DbsField pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Dd;

    private DbsGroup pnd_Sve_Aa_Cntl_Today_Dte__R_Field_2;
    private DbsField pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Sve_Aa_Cntl_Today_Dte_N;
    private DbsField pnd_Work_Date_Ccyymmdd;

    private DbsGroup pnd_Work_Date_Ccyymmdd__R_Field_3;
    private DbsField pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymm;
    private DbsField pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Dd;

    private DbsGroup pnd_Work_Date_Ccyymmdd__R_Field_4;
    private DbsField pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymmdd_N;

    private DbsGroup pnd_Work_Date_Ccyymmdd__R_Field_5;
    private DbsField pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Fill;
    private DbsField pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Mm;
    private DbsField pnd_Day;

    private DbsGroup iaa_Prev_Check_Date;
    private DbsField iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N;

    private DbsGroup iaa_Prev_Check_Date__R_Field_6;
    private DbsField iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Cc;
    private DbsField iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Mm;
    private DbsField iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Dd;
    private DbsField pnd_Curr_Check_Dte_N;

    private DbsGroup pnd_Curr_Check_Dte_N__R_Field_7;
    private DbsField pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Cc;
    private DbsField pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yy;
    private DbsField pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Mm;
    private DbsField pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Dd;

    private DbsGroup pnd_Curr_Check_Dte_N__R_Field_8;
    private DbsField pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yyyymm;
    private DbsField pnd_Prev_Mnth_Date_N;

    private DbsGroup pnd_Prev_Mnth_Date_N__R_Field_9;
    private DbsField pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Ccyy;
    private DbsField pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Mm;
    private DbsField pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Lst_Bus_Dd;

    private DbsGroup pnd_Prev_Mnth_Date_N__R_Field_10;
    private DbsField pnd_Prev_Mnth_Date_N_Pnd_Dd;

    private DbsGroup pnd_Prev_Mnth_Date_N__R_Field_11;
    private DbsField pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Date_A;
    private DbsField pnd_Datd;
    private DbsField pnd_Begin_Mnth_Date_N;

    private DbsGroup pnd_Begin_Mnth_Date_N__R_Field_12;
    private DbsField pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Ccyy;
    private DbsField pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Mm;
    private DbsField pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Dd;
    private DbsField pnd_End_Mnth_Date_N;

    private DbsGroup pnd_End_Mnth_Date_N__R_Field_13;
    private DbsField pnd_End_Mnth_Date_N_Pnd_End_Mnth_Date_A;

    private DbsGroup pnd_End_Mnth_Date_N__R_Field_14;
    private DbsField pnd_End_Mnth_Date_N_Pnd_End_Mnth_Ccyy;
    private DbsField pnd_End_Mnth_Date_N_Pnd_End_Mnth_Mm;
    private DbsField pnd_End_Mnth_Date_N_Pnd_End_Mnth_Dd;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;

    private DbsGroup iaa_Trans_Rcrd__R_Field_15;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr_8;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;

    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField pnd_Final_Tot;
    private DbsField pnd_Save_Trans_Ppcn_Nbr;
    private DbsField pnd_Save_Trans_Payee_Cde;
    private DbsField pnd_Save_Trans_Cde;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_A;
    private DbsField pnd_Cntrl_Frst_Trans_Dte_T;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_16;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Last_Batch_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key;

    private DbsGroup pnd_Iaa_Cntrct_Prtcpnt_Key__R_Field_17;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cntrct_Trn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Key;

    private DbsGroup pnd_Iaa_Cpr_Trans_Key__R_Field_18;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte;

    private DbsGroup pnd_Logical_Variables;
    private DbsField pnd_Logical_Variables_Pnd_1st_Grp;
    private DbsField pnd_Logical_Variables_Pnd_Records_Processed;

    private DbsGroup pnd_Packed_Variables;
    private DbsField pnd_Packed_Variables_Pnd_Records_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Processed_Ctr;
    private DbsField pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Trans__R_Field_19;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Cref_Fund_Trans;
    private DbsField iaa_Cref_Fund_Trans_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Trans__R_Field_20;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Trans_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Aftr_Imge_Id;
    private DbsField pnd_Total_Units;
    private DbsField pnd_Total_Per_Pay;
    private DbsField pnd_Total_Per_Div;
    private DbsField pnd_Sve_Fund_Cde;

    private DbsGroup pnd_Sve_Fund_Cde__R_Field_21;
    private DbsField pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1;
    private DbsField pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Trans_Date;
    private DbsField pnd_Trans_Time;
    private DbsField pnd_Sve_Ppcn_Nbr;
    private DbsField pnd_Sve_Payee_Cde;
    private DbsField pnd_Trans_Code;
    private DbsField pnd_Frst_Tme;
    private DbsField pnd_I;
    private DbsField pnd_Fund_Trans_Key;

    private DbsGroup pnd_Fund_Trans_Key__R_Field_22;
    private DbsField pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde;
    private DbsField pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date;

    private DbsGroup pnd_Out_Selct_Trans;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte;
    private DbsField pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte;

    private DbsGroup pnd_Out_Sortd_Trans;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Cntrct_Nbr;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Payee_Cde;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Opt_Cde;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Issu_Dte;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Data_Grp;

    private DbsGroup pnd_Out_Sortd_Trans__R_Field_23;

    private DbsGroup pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cde;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Dte;
    private DbsField pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Ck_Dte;
    private DbsField select_Trans_Sw;
    private DbsField first_Tme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Sve_Aa_Cntl_Today_Dte = localVariables.newFieldInRecord("pnd_Sve_Aa_Cntl_Today_Dte", "#SVE-AA-CNTL-TODAY-DTE", FieldType.STRING, 8);

        pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1", "REDEFINE", pnd_Sve_Aa_Cntl_Today_Dte);
        pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Ccyymm = pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1.newFieldInGroup("pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Ccyymm", 
            "#SAVE-AA-CNTL-TODAY-DATE-CCYYMM", FieldType.STRING, 4);
        pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Mm = pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1.newFieldInGroup("pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Mm", 
            "#SAVE-AA-CNTL-TODAY-DATE-MM", FieldType.STRING, 2);
        pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Dd = pnd_Sve_Aa_Cntl_Today_Dte__R_Field_1.newFieldInGroup("pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Dd", 
            "#SAVE-AA-CNTL-TODAY-DATE-DD", FieldType.STRING, 2);

        pnd_Sve_Aa_Cntl_Today_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Sve_Aa_Cntl_Today_Dte__R_Field_2", "REDEFINE", pnd_Sve_Aa_Cntl_Today_Dte);
        pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Sve_Aa_Cntl_Today_Dte_N = pnd_Sve_Aa_Cntl_Today_Dte__R_Field_2.newFieldInGroup("pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Sve_Aa_Cntl_Today_Dte_N", 
            "#SVE-AA-CNTL-TODAY-DTE-N", FieldType.NUMERIC, 8);
        pnd_Work_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Work_Date_Ccyymmdd", "#WORK-DATE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Work_Date_Ccyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Date_Ccyymmdd__R_Field_3", "REDEFINE", pnd_Work_Date_Ccyymmdd);
        pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymm = pnd_Work_Date_Ccyymmdd__R_Field_3.newFieldInGroup("pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymm", 
            "#WORK-DATE-CCYYMM", FieldType.STRING, 6);
        pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Dd = pnd_Work_Date_Ccyymmdd__R_Field_3.newFieldInGroup("pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Dd", "#WORK-DATE-DD", 
            FieldType.STRING, 2);

        pnd_Work_Date_Ccyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Date_Ccyymmdd__R_Field_4", "REDEFINE", pnd_Work_Date_Ccyymmdd);
        pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymmdd_N = pnd_Work_Date_Ccyymmdd__R_Field_4.newFieldInGroup("pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymmdd_N", 
            "#WORK-DATE-CCYYMMDD-N", FieldType.NUMERIC, 8);

        pnd_Work_Date_Ccyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Date_Ccyymmdd__R_Field_5", "REDEFINE", pnd_Work_Date_Ccyymmdd);
        pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Fill = pnd_Work_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Fill", "#WORK-DATE-FILL", 
            FieldType.STRING, 4);
        pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Mm = pnd_Work_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Mm", "#WORK-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 1);

        iaa_Prev_Check_Date = localVariables.newGroupInRecord("iaa_Prev_Check_Date", "IAA-PREV-CHECK-DATE");
        iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N = iaa_Prev_Check_Date.newFieldInGroup("iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N", "#PREV-CHECK-DTE-N", 
            FieldType.NUMERIC, 8);

        iaa_Prev_Check_Date__R_Field_6 = iaa_Prev_Check_Date.newGroupInGroup("iaa_Prev_Check_Date__R_Field_6", "REDEFINE", iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N);
        iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Cc = iaa_Prev_Check_Date__R_Field_6.newFieldInGroup("iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Cc", "#PREV-CHECK-DTE-CC", 
            FieldType.NUMERIC, 4);
        iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Mm = iaa_Prev_Check_Date__R_Field_6.newFieldInGroup("iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Mm", "#PREV-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Dd = iaa_Prev_Check_Date__R_Field_6.newFieldInGroup("iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_Dd", "#PREV-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Check_Dte_N = localVariables.newFieldInRecord("pnd_Curr_Check_Dte_N", "#CURR-CHECK-DTE-N", FieldType.NUMERIC, 8);

        pnd_Curr_Check_Dte_N__R_Field_7 = localVariables.newGroupInRecord("pnd_Curr_Check_Dte_N__R_Field_7", "REDEFINE", pnd_Curr_Check_Dte_N);
        pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Cc = pnd_Curr_Check_Dte_N__R_Field_7.newFieldInGroup("pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Cc", "#CURR-CHECK-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yy = pnd_Curr_Check_Dte_N__R_Field_7.newFieldInGroup("pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yy", "#CURR-CHECK-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Mm = pnd_Curr_Check_Dte_N__R_Field_7.newFieldInGroup("pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Mm", "#CURR-CHECK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Dd = pnd_Curr_Check_Dte_N__R_Field_7.newFieldInGroup("pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Dd", "#CURR-CHECK-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Curr_Check_Dte_N__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Check_Dte_N__R_Field_8", "REDEFINE", pnd_Curr_Check_Dte_N);
        pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yyyymm = pnd_Curr_Check_Dte_N__R_Field_8.newFieldInGroup("pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yyyymm", 
            "#CURR-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Prev_Mnth_Date_N = localVariables.newFieldInRecord("pnd_Prev_Mnth_Date_N", "#PREV-MNTH-DATE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Mnth_Date_N__R_Field_9 = localVariables.newGroupInRecord("pnd_Prev_Mnth_Date_N__R_Field_9", "REDEFINE", pnd_Prev_Mnth_Date_N);
        pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Ccyy = pnd_Prev_Mnth_Date_N__R_Field_9.newFieldInGroup("pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Ccyy", "#PREV-MNTH-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Mm = pnd_Prev_Mnth_Date_N__R_Field_9.newFieldInGroup("pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Mm", "#PREV-MNTH-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Lst_Bus_Dd = pnd_Prev_Mnth_Date_N__R_Field_9.newFieldInGroup("pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Lst_Bus_Dd", 
            "#PREV-MNTH-LST-BUS-DD", FieldType.NUMERIC, 2);

        pnd_Prev_Mnth_Date_N__R_Field_10 = pnd_Prev_Mnth_Date_N__R_Field_9.newGroupInGroup("pnd_Prev_Mnth_Date_N__R_Field_10", "REDEFINE", pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Lst_Bus_Dd);
        pnd_Prev_Mnth_Date_N_Pnd_Dd = pnd_Prev_Mnth_Date_N__R_Field_10.newFieldInGroup("pnd_Prev_Mnth_Date_N_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Prev_Mnth_Date_N__R_Field_11 = localVariables.newGroupInRecord("pnd_Prev_Mnth_Date_N__R_Field_11", "REDEFINE", pnd_Prev_Mnth_Date_N);
        pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Date_A = pnd_Prev_Mnth_Date_N__R_Field_11.newFieldInGroup("pnd_Prev_Mnth_Date_N_Pnd_Prev_Mnth_Date_A", "#PREV-MNTH-DATE-A", 
            FieldType.STRING, 8);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Begin_Mnth_Date_N = localVariables.newFieldInRecord("pnd_Begin_Mnth_Date_N", "#BEGIN-MNTH-DATE-N", FieldType.NUMERIC, 8);

        pnd_Begin_Mnth_Date_N__R_Field_12 = localVariables.newGroupInRecord("pnd_Begin_Mnth_Date_N__R_Field_12", "REDEFINE", pnd_Begin_Mnth_Date_N);
        pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Ccyy = pnd_Begin_Mnth_Date_N__R_Field_12.newFieldInGroup("pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Ccyy", "#BEGIN-MNTH-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Mm = pnd_Begin_Mnth_Date_N__R_Field_12.newFieldInGroup("pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Mm", "#BEGIN-MNTH-MM", 
            FieldType.NUMERIC, 2);
        pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Dd = pnd_Begin_Mnth_Date_N__R_Field_12.newFieldInGroup("pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Dd", "#BEGIN-MNTH-DD", 
            FieldType.NUMERIC, 2);
        pnd_End_Mnth_Date_N = localVariables.newFieldInRecord("pnd_End_Mnth_Date_N", "#END-MNTH-DATE-N", FieldType.NUMERIC, 8);

        pnd_End_Mnth_Date_N__R_Field_13 = localVariables.newGroupInRecord("pnd_End_Mnth_Date_N__R_Field_13", "REDEFINE", pnd_End_Mnth_Date_N);
        pnd_End_Mnth_Date_N_Pnd_End_Mnth_Date_A = pnd_End_Mnth_Date_N__R_Field_13.newFieldInGroup("pnd_End_Mnth_Date_N_Pnd_End_Mnth_Date_A", "#END-MNTH-DATE-A", 
            FieldType.STRING, 8);

        pnd_End_Mnth_Date_N__R_Field_14 = localVariables.newGroupInRecord("pnd_End_Mnth_Date_N__R_Field_14", "REDEFINE", pnd_End_Mnth_Date_N);
        pnd_End_Mnth_Date_N_Pnd_End_Mnth_Ccyy = pnd_End_Mnth_Date_N__R_Field_14.newFieldInGroup("pnd_End_Mnth_Date_N_Pnd_End_Mnth_Ccyy", "#END-MNTH-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_End_Mnth_Date_N_Pnd_End_Mnth_Mm = pnd_End_Mnth_Date_N__R_Field_14.newFieldInGroup("pnd_End_Mnth_Date_N_Pnd_End_Mnth_Mm", "#END-MNTH-MM", FieldType.NUMERIC, 
            2);
        pnd_End_Mnth_Date_N_Pnd_End_Mnth_Dd = pnd_End_Mnth_Date_N__R_Field_14.newFieldInGroup("pnd_End_Mnth_Date_N_Pnd_End_Mnth_Dd", "#END-MNTH-DD", FieldType.NUMERIC, 
            2);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");

        iaa_Trans_Rcrd__R_Field_15 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_15", "REDEFINE", iaa_Trans_Rcrd_Trans_Ppcn_Nbr);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx = iaa_Trans_Rcrd__R_Field_15.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx", "TRANS-PPCN-NBR-PFX", FieldType.STRING, 
            2);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr_8 = iaa_Trans_Rcrd__R_Field_15.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr_8", "TRANS-PPCN-NBR-8", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cpr_Trans);

        pnd_Final_Tot = localVariables.newFieldInRecord("pnd_Final_Tot", "#FINAL-TOT", FieldType.PACKED_DECIMAL, 12);
        pnd_Save_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Trans_Ppcn_Nbr", "#SAVE-TRANS-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Trans_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Trans_Payee_Cde", "#SAVE-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Trans_Cde = localVariables.newFieldInRecord("pnd_Save_Trans_Cde", "#SAVE-TRANS-CDE", FieldType.NUMERIC, 3);
        pnd_Cntrl_Frst_Trans_Dte_A = localVariables.newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_A", "#CNTRL-FRST-TRANS-DTE-A", FieldType.STRING, 6);
        pnd_Cntrl_Frst_Trans_Dte_T = localVariables.newFieldInRecord("pnd_Cntrl_Frst_Trans_Dte_T", "#CNTRL-FRST-TRANS-DTE-T", FieldType.TIME);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_16", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_16.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_16.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Last_Batch_Nbr = localVariables.newFieldInRecord("pnd_Last_Batch_Nbr", "#LAST-BATCH-NBR", FieldType.NUMERIC, 4);
        pnd_Iaa_Cntrct_Prtcpnt_Key = localVariables.newFieldInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key", "#IAA-CNTRCT-PRTCPNT-KEY", FieldType.STRING, 12);

        pnd_Iaa_Cntrct_Prtcpnt_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Iaa_Cntrct_Prtcpnt_Key__R_Field_17", "REDEFINE", pnd_Iaa_Cntrct_Prtcpnt_Key);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cntrct_Prtcpnt_Key__R_Field_17.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cntrct_Prtcpnt_Key__R_Field_17.newFieldInGroup("pnd_Iaa_Cntrct_Prtcpnt_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Trn_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Trn_Ppcn_Nbr", "#CNTRCT-TRN-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Key = localVariables.newFieldInRecord("pnd_Iaa_Cpr_Trans_Key", "#IAA-CPR-TRANS-KEY", FieldType.STRING, 25);

        pnd_Iaa_Cpr_Trans_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Iaa_Cpr_Trans_Key__R_Field_18", "REDEFINE", pnd_Iaa_Cpr_Trans_Key);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id = pnd_Iaa_Cpr_Trans_Key__R_Field_18.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", 
            FieldType.STRING, 1);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Iaa_Cpr_Trans_Key__R_Field_18.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Iaa_Cpr_Trans_Key__R_Field_18.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte = pnd_Iaa_Cpr_Trans_Key__R_Field_18.newFieldInGroup("pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);

        pnd_Logical_Variables = localVariables.newGroupInRecord("pnd_Logical_Variables", "#LOGICAL-VARIABLES");
        pnd_Logical_Variables_Pnd_1st_Grp = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_1st_Grp", "#1ST-GRP", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Variables_Pnd_Records_Processed = pnd_Logical_Variables.newFieldInGroup("pnd_Logical_Variables_Pnd_Records_Processed", "#RECORDS-PROCESSED", 
            FieldType.BOOLEAN, 1);

        pnd_Packed_Variables = localVariables.newGroupInRecord("pnd_Packed_Variables", "#PACKED-VARIABLES");
        pnd_Packed_Variables_Pnd_Records_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Ctr", "#RECORDS-CTR", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Packed_Variables_Pnd_Records_Processed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Processed_Ctr", "#RECORDS-PROCESSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr = pnd_Packed_Variables.newFieldInGroup("pnd_Packed_Variables_Pnd_Records_Bypassed_Ctr", "#RECORDS-BYPASSED-CTR", 
            FieldType.PACKED_DECIMAL, 9);

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Trans__R_Field_19 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans__R_Field_19", "REDEFINE", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans__R_Field_19.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans__R_Field_19.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Tiaa_Fund_Trans);

        vw_iaa_Cref_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans", "IAA-CREF-FUND-TRANS"), "IAA_CREF_FUND_TRANS_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Trans__R_Field_20 = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans__R_Field_20", "REDEFINE", iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans__R_Field_20.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Cref_Fund_Trans_Cref_Fund_Cde = iaa_Cref_Fund_Trans__R_Field_20.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Trans_Cref_Unit_Val = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Unit_Val", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Cde = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Dte = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Units_Cnt = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cref_Fund_Trans);

        pnd_Total_Units = localVariables.newFieldInRecord("pnd_Total_Units", "#TOTAL-UNITS", FieldType.NUMERIC, 8, 3);
        pnd_Total_Per_Pay = localVariables.newFieldInRecord("pnd_Total_Per_Pay", "#TOTAL-PER-PAY", FieldType.NUMERIC, 9, 2);
        pnd_Total_Per_Div = localVariables.newFieldInRecord("pnd_Total_Per_Div", "#TOTAL-PER-DIV", FieldType.NUMERIC, 9, 2);
        pnd_Sve_Fund_Cde = localVariables.newFieldInRecord("pnd_Sve_Fund_Cde", "#SVE-FUND-CDE", FieldType.STRING, 2);

        pnd_Sve_Fund_Cde__R_Field_21 = localVariables.newGroupInRecord("pnd_Sve_Fund_Cde__R_Field_21", "REDEFINE", pnd_Sve_Fund_Cde);
        pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1 = pnd_Sve_Fund_Cde__R_Field_21.newFieldInGroup("pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1", "#SVE-FUND-CDE1", FieldType.STRING, 
            1);
        pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2 = pnd_Sve_Fund_Cde__R_Field_21.newFieldInGroup("pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2", "#SVE-FUND-CDE2", FieldType.STRING, 
            1);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 4);
        pnd_Trans_Date = localVariables.newFieldInRecord("pnd_Trans_Date", "#TRANS-DATE", FieldType.STRING, 8);
        pnd_Trans_Time = localVariables.newFieldInRecord("pnd_Trans_Time", "#TRANS-TIME", FieldType.STRING, 8);
        pnd_Sve_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Sve_Ppcn_Nbr", "#SVE-PPCN-NBR", FieldType.STRING, 10);
        pnd_Sve_Payee_Cde = localVariables.newFieldInRecord("pnd_Sve_Payee_Cde", "#SVE-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Trans_Code = localVariables.newFieldInRecord("pnd_Trans_Code", "#TRANS-CODE", FieldType.NUMERIC, 3);
        pnd_Frst_Tme = localVariables.newFieldInRecord("pnd_Frst_Tme", "#FRST-TME", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Fund_Trans_Key = localVariables.newFieldInRecord("pnd_Fund_Trans_Key", "#FUND-TRANS-KEY", FieldType.STRING, 27);

        pnd_Fund_Trans_Key__R_Field_22 = localVariables.newGroupInRecord("pnd_Fund_Trans_Key__R_Field_22", "REDEFINE", pnd_Fund_Trans_Key);
        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id = pnd_Fund_Trans_Key__R_Field_22.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Fund_Trans_Key__R_Field_22.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde = pnd_Fund_Trans_Key__R_Field_22.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde = pnd_Fund_Trans_Key__R_Field_22.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde", "#CNTRCT-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date = pnd_Fund_Trans_Key__R_Field_22.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date", "#INVRSE-TRANS-DATE", 
            FieldType.NUMERIC, 12);

        pnd_Out_Selct_Trans = localVariables.newGroupInRecord("pnd_Out_Selct_Trans", "#OUT-SELCT-TRANS");
        pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr", "#OUT-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde", "#OUT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde", "#OUT-TRN-CDE", FieldType.NUMERIC, 
            3);
        pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde", "#OUT-OPT-CDE", FieldType.NUMERIC, 
            2);
        pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte", "#OUT-ISSU-DTE", FieldType.NUMERIC, 
            6);
        pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte", "#OUT-TRN-DTE", FieldType.NUMERIC, 
            8);
        pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte = pnd_Out_Selct_Trans.newFieldInGroup("pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte", "#OUT-TRN-CK-DTE", FieldType.NUMERIC, 
            8);

        pnd_Out_Sortd_Trans = localVariables.newGroupInRecord("pnd_Out_Sortd_Trans", "#OUT-SORTD-TRANS");
        pnd_Out_Sortd_Trans_Pnd_Srt_Cntrct_Nbr = pnd_Out_Sortd_Trans.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Cntrct_Nbr", "#SRT-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Out_Sortd_Trans_Pnd_Srt_Payee_Cde = pnd_Out_Sortd_Trans.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Payee_Cde", "#SRT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Out_Sortd_Trans_Pnd_Srt_Opt_Cde = pnd_Out_Sortd_Trans.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Opt_Cde", "#SRT-OPT-CDE", FieldType.NUMERIC, 
            2);
        pnd_Out_Sortd_Trans_Pnd_Srt_Issu_Dte = pnd_Out_Sortd_Trans.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Issu_Dte", "#SRT-ISSU-DTE", FieldType.NUMERIC, 
            6);
        pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt = pnd_Out_Sortd_Trans.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt", "#SRT-TRN-CNT", FieldType.NUMERIC, 
            2);
        pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Data_Grp = pnd_Out_Sortd_Trans.newFieldArrayInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Data_Grp", "#SRT-TRAN-CDE-DATA-GRP", 
            FieldType.STRING, 1, new DbsArrayController(1, 76));

        pnd_Out_Sortd_Trans__R_Field_23 = pnd_Out_Sortd_Trans.newGroupInGroup("pnd_Out_Sortd_Trans__R_Field_23", "REDEFINE", pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Data_Grp);

        pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp = pnd_Out_Sortd_Trans__R_Field_23.newGroupArrayInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp", "#SRT-TRAN-CDE-GRP", 
            new DbsArrayController(1, 4));
        pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cde = pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cde", "#SRT-TRN-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Dte = pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Dte", "#SRT-TRN-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Ck_Dte = pnd_Out_Sortd_Trans_Pnd_Srt_Tran_Cde_Grp.newFieldInGroup("pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Ck_Dte", "#SRT-TRN-CK-DTE", 
            FieldType.NUMERIC, 8);
        select_Trans_Sw = localVariables.newFieldInRecord("select_Trans_Sw", "SELECT-TRANS-SW", FieldType.STRING, 1);
        first_Tme = localVariables.newFieldInRecord("first_Tme", "FIRST-TME", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Trans.reset();

        localVariables.reset();
        pnd_Frst_Tme.setInitialValue("Y");
        first_Tme.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iadp165() throws Exception
    {
        super("Iadp165");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IADP165", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //* *  #BEGIN-MNTH-DATE-N  (EM=9999-99-99) ' thru '                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  GET CHECK & BUSINESS DATES TO SELECT ON
                                                                                                                                                                          //Natural: PERFORM READ-CNTRL-RCRD
        sub_Read_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
        //*  SELECT VERIFIED RECORDS FOR TPA, IPRO & P/I NEW ISSUE TRANS
        //*                 CODE 030, 033
        //*       V = RECORD HAS NOT BEEN VERIFIED
        //*  BLANK  = RECORD HAS BEEN  VERIFIED
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #PREV-CHECK-DTE-N
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ01")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.notEquals(pnd_Curr_Check_Dte_N) && iaa_Trans_Rcrd_Trans_Check_Dte.notEquals(iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N))) //Natural: REJECT IAA-TRANS-RCRD.TRANS-CHECK-DTE NE #CURR-CHECK-DTE-N AND IAA-TRANS-RCRD.TRANS-CHECK-DTE NE #PREV-CHECK-DTE-N
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Todays_Dte.less(pnd_Prev_Mnth_Date_N)))                                                                                    //Natural: REJECT IAA-TRANS-RCRD.TRANS-TODAYS-DTE LT #PREV-MNTH-DATE-N
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Todays_Dte.greater(pnd_End_Mnth_Date_N)))                                                                                  //Natural: REJECT IAA-TRANS-RCRD.TRANS-TODAYS-DTE GT #END-MNTH-DATE-N
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.equals(pnd_Sve_Ppcn_Nbr) && iaa_Trans_Rcrd_Trans_Payee_Cde.equals(pnd_Sve_Payee_Cde) && iaa_Trans_Rcrd_Trans_Cde.equals(pnd_Save_Trans_Cde))) //Natural: REJECT IAA-TRANS-RCRD.TRANS-PPCN-NBR = #SVE-PPCN-NBR AND IAA-TRANS-RCRD.TRANS-PAYEE-CDE = #SVE-PAYEE-CDE AND IAA-TRANS-RCRD.TRANS-CDE = #SAVE-TRANS-CDE
            {
                continue;
            }
            pnd_Sve_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                                     //Natural: ASSIGN #SVE-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            pnd_Sve_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                                                   //Natural: ASSIGN #SVE-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                                        //Natural: ASSIGN #SAVE-TRANS-CDE := IAA-TRANS-RCRD.TRANS-CDE
            if (condition(iaa_Trans_Rcrd_Trans_Verify_Cde.equals(" ")))                                                                                                   //Natural: IF IAA-TRANS-RCRD.TRANS-VERIFY-CDE = ' '
            {
                //*  30 = ADAM NEW ISSUE
                if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(30) || iaa_Trans_Rcrd_Trans_Cde.equals(33) || iaa_Trans_Rcrd_Trans_Cde.equals(60)))                         //Natural: IF TRANS-CDE = 030 OR = 033 OR = 60
                {
                    //* ***  OR= 006 OR= 040 OR= 066         /* 33 = MANUAL NEW ISSUE
                    //* ***                                  /* 60 = ADAM RESETTLEMENT SWEEP
                    //* ***               COMMENTED ABOVE 10/01
                    //*  31 = MONTHLY SETTLE NEW ISSUE
                    if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IA") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IB") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IC")  //Natural: IF TRANS-PPCN-NBR-PFX = 'IA' OR = 'IB' OR = 'IC' OR = 'ID' OR = 'IE' OR = 'IF' OR = 'IG' OR = 'IH' OR = 'IJ' OR = 'IP' OR = 'S0'
                        || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("ID") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IE") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IF") 
                        || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IG") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IH") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IJ") 
                        || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("IP") || iaa_Trans_Rcrd_Trans_Ppcn_Nbr_Pfx.equals("S0")))
                    {
                        //*  GET IAA-CNTRCT RECORD FOR OPTION
                                                                                                                                                                          //Natural: PERFORM GET-IAA-CNTRCT
                        sub_Get_Iaa_Cntrct();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(select_Trans_Sw.equals("Y")))                                                                                                       //Natural: IF SELECT-TRANS-SW = 'Y'
                        {
                            select_Trans_Sw.reset();                                                                                                                      //Natural: RESET SELECT-TRANS-SW
                            //*          ADD 1 TO #RECORDS-CTR
                            pnd_Save_Trans_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                              //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := TRANS-PPCN-NBR
                            pnd_Save_Trans_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                            //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := TRANS-PAYEE-CDE
                            pnd_Save_Trans_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                                        //Natural: ASSIGN #SAVE-TRANS-CDE := TRANS-CDE
                            //*    PERFORM GET-CURRENT-CPR-TRANS
                            //*    PERFORM GET-CURRENT-FUND-TRANS
                            pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr.setValue(pnd_Save_Trans_Ppcn_Nbr);                                                                     //Natural: ASSIGN #OUT-CNTRCT-NBR := #SAVE-TRANS-PPCN-NBR
                            pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde.setValue(pnd_Save_Trans_Payee_Cde);                                                                     //Natural: ASSIGN #OUT-PAYEE-CDE := #SAVE-TRANS-PAYEE-CDE
                            pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde.setValue(iaa_Trans_Rcrd_Trans_Cde);                                                                       //Natural: ASSIGN #OUT-TRN-CDE := IAA-TRANS-RCRD.TRANS-CDE
                            pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte.setValue(iaa_Trans_Rcrd_Trans_Todays_Dte);                                                                //Natural: ASSIGN #OUT-TRN-DTE := IAA-TRANS-RCRD.TRANS-TODAYS-DTE
                            pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte.setValue(iaa_Trans_Rcrd_Trans_Check_Dte);                                                              //Natural: ASSIGN #OUT-TRN-CK-DTE := IAA-TRANS-RCRD.TRANS-CHECK-DTE
                            pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde.setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                     //Natural: ASSIGN #OUT-OPT-CDE := CNTRCT-OPTN-CDE
                            pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte.setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                   //Natural: ASSIGN #OUT-ISSU-DTE := CNTRCT-ISSUE-DTE
                            getWorkFiles().write(1, false, pnd_Out_Selct_Trans);                                                                                          //Natural: WRITE WORK FILE 1 #OUT-SELCT-TRANS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #OUT-SELCT-TRANS
        while (condition(getWorkFiles().read(1, pnd_Out_Selct_Trans)))
        {
            pnd_Packed_Variables_Pnd_Records_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #RECORDS-CTR
            getSort().writeSortInData(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr, pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde, pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde,                 //Natural: END-ALL
                pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde, pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte, pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte, pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr, pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde, pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde);                           //Natural: SORT BY #OUT-CNTRCT-NBR ASCENDING #OUT-PAYEE-CDE #OUT-TRN-CDE USING #OUT-OPT-CDE #OUT-ISSU-DTE #OUT-TRN-DTE #OUT-TRN-CK-DTE
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr, pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde, pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde, 
            pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde, pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte, pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte, pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte)))
        {
            getReports().display(1, "/Contract",                                                                                                                          //Natural: DISPLAY ( 1 ) '/Contract' #OUT-CNTRCT-NBR ( AL = 8 ) '/Py' #OUT-PAYEE-CDE ( EM = 99 ) '/Opt/cde' #OUT-OPT-CDE ( EM = 99 ) '/trn/cde' #OUT-TRN-CDE '/Issue-Date' #OUT-ISSU-DTE ( EM = 9999-99 ) '/Tran/Today date' #OUT-TRN-DTE ( EM = 9999-99-99 ) '/Tran/Check Date' #OUT-TRN-CK-DTE ( EM = 9999-99-99 )
            		pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr, new AlphanumericLength (8),"/Py",
            		pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde, new ReportEditMask ("99"),"/Opt/cde",
            		pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde, new ReportEditMask ("99"),"/trn/cde",
            		pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde,"/Issue-Date",
            		pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte, new ReportEditMask ("9999-99"),"/Tran/Today date",
            		pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte, new ReportEditMask ("9999-99-99"),"/Tran/Check Date",
            		pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte, new ReportEditMask ("9999-99-99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '/trn/Date'        #OUT-TRN-DTE (EM=9999-99-99)
            //*  ADDED FOLLOWING 4/02
            if (condition(pnd_Frst_Tme.equals("Y")))                                                                                                                      //Natural: IF #FRST-TME = 'Y'
            {
                pnd_Frst_Tme.setValue("N");                                                                                                                               //Natural: ASSIGN #FRST-TME := 'N'
                pnd_Sve_Ppcn_Nbr.setValue(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr);                                                                                        //Natural: ASSIGN #SVE-PPCN-NBR := #OUT-CNTRCT-NBR
                pnd_Sve_Payee_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde);                                                                                        //Natural: ASSIGN #SVE-PAYEE-CDE := #OUT-PAYEE-CDE
                pnd_Out_Sortd_Trans_Pnd_Srt_Cntrct_Nbr.setValue(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr);                                                                  //Natural: ASSIGN #SRT-CNTRCT-NBR := #OUT-CNTRCT-NBR
                pnd_Out_Sortd_Trans_Pnd_Srt_Payee_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde);                                                                    //Natural: ASSIGN #SRT-PAYEE-CDE := #OUT-PAYEE-CDE
                pnd_Out_Sortd_Trans_Pnd_Srt_Opt_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde);                                                                        //Natural: ASSIGN #SRT-OPT-CDE := #OUT-OPT-CDE
                pnd_Out_Sortd_Trans_Pnd_Srt_Issu_Dte.setValue(pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte);                                                                      //Natural: ASSIGN #SRT-ISSU-DTE := #OUT-ISSU-DTE
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 4/02
            //*    WRITE WORK 2 #OUT-SELCT-TRANS
            //*  =>REPLACED ABOVE COMMENTED WITH FOLLOWING 4/02
            if (condition(pnd_Sve_Ppcn_Nbr.notEquals(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr) || pnd_Sve_Payee_Cde.notEquals(pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde)))      //Natural: IF #SVE-PPCN-NBR NE #OUT-CNTRCT-NBR OR #SVE-PAYEE-CDE NE #OUT-PAYEE-CDE
            {
                pnd_Sve_Ppcn_Nbr.setValue(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr);                                                                                        //Natural: ASSIGN #SVE-PPCN-NBR := #OUT-CNTRCT-NBR
                pnd_Sve_Payee_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde);                                                                                        //Natural: ASSIGN #SVE-PAYEE-CDE := #OUT-PAYEE-CDE
                getWorkFiles().write(2, false, pnd_Out_Sortd_Trans);                                                                                                      //Natural: WRITE WORK FILE 2 #OUT-SORTD-TRANS
                pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt.reset();                                                                                                              //Natural: RESET #SRT-TRN-CNT #SRT-TRN-CDE ( * ) #SRT-TRN-DTE ( * ) #SRT-TRN-CK-DTE ( * )
                pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cde.getValue("*").reset();
                pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Dte.getValue("*").reset();
                pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Ck_Dte.getValue("*").reset();
                pnd_Out_Sortd_Trans_Pnd_Srt_Cntrct_Nbr.setValue(pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr);                                                                  //Natural: ASSIGN #SRT-CNTRCT-NBR := #OUT-CNTRCT-NBR
                pnd_Out_Sortd_Trans_Pnd_Srt_Payee_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde);                                                                    //Natural: ASSIGN #SRT-PAYEE-CDE := #OUT-PAYEE-CDE
                pnd_Out_Sortd_Trans_Pnd_Srt_Opt_Cde.setValue(pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde);                                                                        //Natural: ASSIGN #SRT-OPT-CDE := #OUT-OPT-CDE
                pnd_Out_Sortd_Trans_Pnd_Srt_Issu_Dte.setValue(pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte);                                                                      //Natural: ASSIGN #SRT-ISSU-DTE := #OUT-ISSU-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt.nadd(1);                                                                                                                  //Natural: ADD 1 TO #SRT-TRN-CNT
            pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cde.getValue(pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt).setValue(pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde);                              //Natural: ASSIGN #SRT-TRN-CDE ( #SRT-TRN-CNT ) := #OUT-TRN-CDE
            pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Dte.getValue(pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt).setValue(pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte);                              //Natural: ASSIGN #SRT-TRN-DTE ( #SRT-TRN-CNT ) := #OUT-TRN-DTE
            pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Ck_Dte.getValue(pnd_Out_Sortd_Trans_Pnd_Srt_Trn_Cnt).setValue(pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte);                        //Natural: ASSIGN #SRT-TRN-CK-DTE ( #SRT-TRN-CNT ) := #OUT-TRN-CK-DTE
            //*  => END OF CHANGE 4/02
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Number of trans records selected    : ",pnd_Packed_Variables_Pnd_Records_Ctr);                                //Natural: WRITE ( 1 ) / 'Number of trans records selected    : ' #RECORDS-CTR
        if (Global.isEscape()) return;
        //* ******************************************************************
        //*  GET CNTRCT FOR OPTION CODE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IAA-CNTRCT
        //* ******************************************************************
        //*  GET CPR TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-CPR-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-FUND-TRANS
        //* ******************************************************************
        //*  GET TIAA FUND TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-FUND-RECORD
        //* ******************************************************************
        //*  GET CREF FUND TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CREF-FUND-RECORD
        //* *******************************************************
        //*  1) GET CURRENT & PREV CHECK DATE FROM AA CNTRL-RECORD
        //*  2) GET TODAYS DATE FROM AA CNTRL-RECORD USED FOR TRANS
        //*     SELECTION
        //*  3) GET LAST BUSINESS DAY OF PREVIOUS MONTH
        //*  4) WRITE CURRENT-CHECK DATE TO FILE
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CNTRL-RCRD
        //*  SET UP CURRENT MONTH DATES BEGINING DATE & END-OF-MONTH DATE
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Get_Iaa_Cntrct() throws Exception                                                                                                                    //Natural: GET-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Trn_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                                                  //Natural: ASSIGN #CNTRCT-TRN-PPCN-NBR := TRANS-PPCN-NBR
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-TRN-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Trn_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, Global.getPROGRAM(),"Tran not selected No IAA-CNTRCT Record Found For: ",iaa_Trans_Rcrd_Trans_Ppcn_Nbr,iaa_Trans_Rcrd_Trans_Payee_Cde, //Natural: WRITE *PROGRAM 'Tran not selected No IAA-CNTRCT Record Found For: ' TRANS-PPCN-NBR TRANS-PAYEE-CDE IAA-TRANS-RCRD.TRANS-CDE IAA-TRANS-RCRD.TRANS-TODAYS-DTE IAA-TRANS-RCRD.TRANS-CHECK-DTE
                    iaa_Trans_Rcrd_Trans_Cde,iaa_Trans_Rcrd_Trans_Todays_Dte,iaa_Trans_Rcrd_Trans_Check_Dte);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(22) || iaa_Cntrct_Cntrct_Optn_Cde.equals(25) || iaa_Cntrct_Cntrct_Optn_Cde.equals(27) || iaa_Cntrct_Cntrct_Optn_Cde.equals(28)  //Natural: IF CNTRCT-OPTN-CDE = 22 OR = 25 OR = 27 OR = 28 OR = 30
                || iaa_Cntrct_Cntrct_Optn_Cde.equals(30)))
            {
                select_Trans_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN SELECT-TRANS-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Current_Cpr_Trans() throws Exception                                                                                                             //Natural: GET-CURRENT-CPR-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                             //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#AFTR-IMGE-ID := '2'
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                                                           //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
        pnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Trans_Rcrd_Trans_Payee_Cde);                                                                         //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
        pnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte.setValue(iaa_Trans_Rcrd_Invrse_Trans_Dte);                                                                             //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#INVRSE-TRANS-DTE := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
        vw_iaa_Cpr_Trans.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) IAA-CPR-TRANS WITH CPR-AFTR-KEY = #IAA-CPR-TRANS-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CPR_AFTR_KEY", "=", pnd_Iaa_Cpr_Trans_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_iaa_Cpr_Trans.readNextRow("FIND02", true)))
        {
            vw_iaa_Cpr_Trans.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cpr_Trans.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                getReports().write(1, ReportOption.NOTITLE,"NO TRANS PARTICIPANT RECORD FOUND FOR: ",iaa_Trans_Rcrd_Trans_Ppcn_Nbr);                                      //Natural: WRITE ( 1 ) 'NO TRANS PARTICIPANT RECORD FOUND FOR: ' TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Current_Fund_Trans() throws Exception                                                                                                            //Natural: GET-CURRENT-FUND-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(iaa_Cpr_Trans_Cntrct_Company_Cd.getValue(1).equals("T") || iaa_Cpr_Trans_Cntrct_Company_Cd.getValue(1).equals("G")))                                //Natural: IF IAA-CPR-TRANS.CNTRCT-COMPANY-CD ( 1 ) = 'T' OR = 'G'
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-FUND-RECORD
            sub_Process_Tiaa_Fund_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-FUND-RECORD
            sub_Process_Cref_Fund_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tiaa_Fund_Record() throws Exception                                                                                                          //Natural: PROCESS-TIAA-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#AFTR-IMGE-ID := '2'
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Sve_Ppcn_Nbr);                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PPCN-NBR := #SVE-PPCN-NBR
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Sve_Payee_Cde);                                                                                              //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PAYEE-CDE := #SVE-PAYEE-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY STARTING FROM #FUND-TRANS-KEY
        (
        "READ03",
        new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", ">=", pnd_Fund_Trans_Key, WcType.BY) },
        new Oc[] { new Oc("TIAA_FUND_AFTR_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ03")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(pnd_Sve_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(pnd_Sve_Payee_Cde)))          //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = #SVE-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = #SVE-PAYEE-CDE
        {
            pnd_Total_Per_Pay.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                                             //Natural: ASSIGN #TOTAL-PER-PAY := IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT
            pnd_Total_Per_Div.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                                             //Natural: ASSIGN #TOTAL-PER-DIV := IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT
            pnd_Sve_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                                                 //Natural: ASSIGN #SVE-FUND-CDE := IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO TRANS TIAA-FUND RECORD FOUND FOR: ",pnd_Sve_Ppcn_Nbr);                                                                              //Natural: WRITE 'NO TRANS TIAA-FUND RECORD FOUND FOR: ' #SVE-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Cref_Fund_Record() throws Exception                                                                                                          //Natural: PROCESS-CREF-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#AFTR-IMGE-ID := '2'
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Sve_Ppcn_Nbr);                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PPCN-NBR := #SVE-PPCN-NBR
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Sve_Payee_Cde);                                                                                              //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PAYEE-CDE := #SVE-PAYEE-CDE
        vw_iaa_Cref_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS BY CREF-FUND-AFTR-KEY STARTING FROM #FUND-TRANS-KEY
        (
        "READ04",
        new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", ">=", pnd_Fund_Trans_Key, WcType.BY) },
        new Oc[] { new Oc("TIAA_FUND_AFTR_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_iaa_Cref_Fund_Trans.readNextRow("READ04")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr.equals(pnd_Sve_Ppcn_Nbr) && iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde.equals(pnd_Sve_Payee_Cde)))          //Natural: IF IAA-CREF-FUND-TRANS.CREF-CNTRCT-PPCN-NBR = #SVE-PPCN-NBR AND IAA-CREF-FUND-TRANS.CREF-CNTRCT-PAYEE-CDE = #SVE-PAYEE-CDE
        {
            pnd_Total_Units.compute(new ComputeParameters(false, pnd_Total_Units), DbsField.add(getZero(),iaa_Cref_Fund_Trans_Cref_Units_Cnt.getValue("*")));             //Natural: ASSIGN #TOTAL-UNITS := 0 + IAA-CREF-FUND-TRANS.CREF-UNITS-CNT ( * )
            pnd_Sve_Fund_Cde.setValue(iaa_Cref_Fund_Trans_Cref_Fund_Cde);                                                                                                 //Natural: ASSIGN #SVE-FUND-CDE := IAA-CREF-FUND-TRANS.CREF-FUND-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO TRANS CREF-FUND RECORD FOUND FOR: ",pnd_Sve_Ppcn_Nbr);                                                                              //Natural: WRITE 'NO TRANS CREF-FUND RECORD FOUND FOR: ' #SVE-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Cntrl_Rcrd() throws Exception                                                                                                                   //Natural: READ-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 2 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ05",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        2
        );
        READ05:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ05")))
        {
            pnd_Work_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #WORK-DATE-CCYYMMDD
            //*  1ST = CURRENT CHECK DATE
            if (condition(first_Tme.equals("Y")))                                                                                                                         //Natural: IF FIRST-TME = 'Y'
            {
                first_Tme.setValue("N");                                                                                                                                  //Natural: ASSIGN FIRST-TME := 'N'
                pnd_Curr_Check_Dte_N.compute(new ComputeParameters(false, pnd_Curr_Check_Dte_N), pnd_Work_Date_Ccyymmdd.val());                                           //Natural: ASSIGN #CURR-CHECK-DTE-N := VAL ( #WORK-DATE-CCYYMMDD )
                getWorkFiles().write(3, false, pnd_Curr_Check_Dte_N_Pnd_Curr_Check_Dte_Yyyymm);                                                                           //Natural: WRITE WORK FILE 3 #CURR-CHECK-DTE-YYYYMM
                pnd_Work_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                    //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #WORK-DATE-CCYYMMDD
                //*  SAVE BUSINESS DATE
                pnd_Sve_Aa_Cntl_Today_Dte.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #SVE-AA-CNTL-TODAY-DTE
                pnd_Begin_Mnth_Date_N.compute(new ComputeParameters(false, pnd_Begin_Mnth_Date_N), pnd_Work_Date_Ccyymmdd.val());                                         //Natural: ASSIGN #BEGIN-MNTH-DATE-N := VAL ( #WORK-DATE-CCYYMMDD )
                pnd_Begin_Mnth_Date_N_Pnd_Begin_Mnth_Dd.setValue(1);                                                                                                      //Natural: ASSIGN #BEGIN-MNTH-DD := 01
                pnd_End_Mnth_Date_N.setValue(pnd_Begin_Mnth_Date_N);                                                                                                      //Natural: ASSIGN #END-MNTH-DATE-N := #BEGIN-MNTH-DATE-N
                pnd_End_Mnth_Date_N_Pnd_End_Mnth_Dd.setValue(31);                                                                                                         //Natural: ASSIGN #END-MNTH-DD := 31
                REPEAT01:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    //*  LAST DAY OF CURRENT MONTH
                    if (condition(DbsUtil.maskMatches(pnd_End_Mnth_Date_N_Pnd_End_Mnth_Date_A,"YYYYMMDD")))                                                               //Natural: IF #END-MNTH-DATE-A = MASK ( YYYYMMDD )
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_End_Mnth_Date_N_Pnd_End_Mnth_Dd.nsubtract(1);                                                                                                 //Natural: SUBTRACT 1 FROM #END-MNTH-DD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-REPEAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* 2ND PREV CHECK DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N.compute(new ComputeParameters(false, iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N), pnd_Work_Date_Ccyymmdd.val());   //Natural: ASSIGN #PREV-CHECK-DTE-N := VAL ( #WORK-DATE-CCYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING 4/02
        //*  READ DC CNTRL RECORD TO GET LAST-BUSINESS DATE OF PREVIOUS MONTH
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ06",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ06:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ06")))
        {
            pnd_Work_Date_Ccyymmdd.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #WORK-DATE-CCYYMMDD
            //*   DISPLAY 'CHECK-DATE'    CNTRL-CHECK-DTE
            //*    'TODAY-CURRENT DATE '     CNTRL-TODAYS-DTE
            //*  IF #FRST-READ = 'Y'
            //*    #FRST-READ := 'N'
            //*    #SVE-TODAYS-DTE := #WORK-DATE-CCYYMMDD
            //*  END-IF
            //*  GET LAST BUSINES DAY OF PREVIOUS MONTH FOR TRANS SELECTION
            //*  IF #WORK-DATE-MM LT #SAVE-AA-CNTL-TODAY-DATE-MM
            //*  CHANGED ABOVE COMMENTED TO FOLLOWING 1/03
            if (condition(pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Mm.notEquals(pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Save_Aa_Cntl_Today_Date_Mm)))                                   //Natural: IF #WORK-DATE-MM NE #SAVE-AA-CNTL-TODAY-DATE-MM
            {
                pnd_Prev_Mnth_Date_N.setValue(pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymmdd_N);                                                                           //Natural: ASSIGN #PREV-MNTH-DATE-N := #WORK-DATE-CCYYMMDD-N
                getReports().write(0, " Saved AA Control record business date (todays-date)-->",pnd_Sve_Aa_Cntl_Today_Dte_Pnd_Sve_Aa_Cntl_Today_Dte_N);                   //Natural: WRITE ' Saved AA Control record business date (todays-date)-->' #SVE-AA-CNTL-TODAY-DTE-N
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Mnth_Date_N.setValue(pnd_Work_Date_Ccyymmdd_Pnd_Work_Date_Ccyymmdd_N);                                                                           //Natural: ASSIGN #PREV-MNTH-DATE-N := #WORK-DATE-CCYYMMDD-N
                getReports().write(0, " Last business date for previous month---------------->",pnd_Prev_Mnth_Date_N);                                                    //Natural: WRITE ' Last business date for previous month---------------->' #PREV-MNTH-DATE-N
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END OF ADD 4/02
        //*  OLD CODE
        //*  #PREV-MNTH-DATE-N  := #BEGIN-MNTH-DATE-N/*GET THE LST DAY OF PREV-MNTH
        //*  SUBTRACT 1 FROM #PREV-MNTH-MM
        //*  IF #PREV-MNTH-MM = 0
        //*   MOVE 12  TO #PREV-MNTH-MM
        //*   SUBTRACT 1 FROM #PREV-MNTH-CCYY
        //*  END-IF
        //*  FOR  #DD = 31 TO 1  STEP -1 /* FIND LAST VALID BUSINEES DAY
        //*   IF #PREV-MNTH-DATE-N  = MASK(YYYYMMDD)
        //*     MOVE EDITED #PREV-MNTH-DATE-A TO #DATD (EM=YYYYMMDD)
        //*     MOVE EDITED  #DATD (EM=N) TO #DAY
        //*     IF #DAY = 'M' OR= 'T' OR= 'W' OR= 'F'
        //*       ESCAPE BOTTOM
        //*     END-IF
        //*   END-IF
        //*  END-FOR
        //*  FOR #END-MNTH-DD  = 31 TO 1  STEP -1    /*GET BUSINESS DAY B4
        //*   IF #END-MNTH-DATE-N  = MASK(YYYYMMDD) /*LAST BUSINESS DAY OF MONTH
        //*     MOVE EDITED #END-MNTH-DATE-A TO #DATD (EM=YYYYMMDD)
        //*     MOVE EDITED  #DATD (EM=N) TO #DAY
        //*     IF #DAY = 'M'
        //*        SUBTRACT 3 FROM #END-MNTH-DD
        //*       ESCAPE BOTTOM
        //*     END-IF
        //*     IF #DAY = 'T' OR= 'W' OR= 'F'
        //*        SUBTRACT 1 FROM #END-MNTH-DD
        //*       ESCAPE BOTTOM
        //*     END-IF
        //*   END-IF
        //*  END-FOR
        //*  END OF OLD CODE
        getReports().write(0, "Following dates used for selection of iaiq tpa,ipro & p&i trans");                                                                         //Natural: WRITE 'Following dates used for selection of iaiq tpa,ipro & p&i trans'
        if (Global.isEscape()) return;
        getReports().write(0, "Previous ","=",iaa_Cntrl_Rcrd_Cntrl_Check_Dte, new ReportEditMask ("YYYYMMDD"));                                                           //Natural: WRITE 'Previous ' '=' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        getReports().write(0, "Current check date......:",pnd_Curr_Check_Dte_N, new ReportEditMask ("9999-99-99"));                                                       //Natural: WRITE 'Current check date......:' #CURR-CHECK-DTE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
        getReports().write(0, "Previous check date.....:",iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N, new ReportEditMask ("9999-99-99"));                                   //Natural: WRITE 'Previous check date.....:' #PREV-CHECK-DTE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
        getReports().write(0, "last bus date prev month:",pnd_Prev_Mnth_Date_N, new ReportEditMask ("9999-99-99"));                                                       //Natural: WRITE 'last bus date prev month:' #PREV-MNTH-DATE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
        getReports().write(0, "Begin of month date.....:",pnd_Begin_Mnth_Date_N, new ReportEditMask ("9999-99-99"));                                                      //Natural: WRITE 'Begin of month date.....:' #BEGIN-MNTH-DATE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
        getReports().write(0, "End of month date.......:",pnd_End_Mnth_Date_N, new ReportEditMask ("9999-99-99"));                                                        //Natural: WRITE 'End of month date.......:' #END-MNTH-DATE-N ( EM = 9999-99-99 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(10),"List of selected TPA, IPRO & P/I Transactions");     //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 10X 'List of selected TPA, IPRO & P/I Transactions'
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(20),"Selected For Curr Chk Date:",pnd_Curr_Check_Dte_N, new ReportEditMask               //Natural: WRITE ( 1 ) 20X 'Selected For Curr Chk Date:' #CURR-CHECK-DTE-N ( EM = 9999/99/99 ) 'and Prev Chk Date:' #PREV-CHECK-DTE-N ( EM = 9999/99/99 ) 4X 'PAGE: ' #W-PAGE-CTR ( EM = ZZZ,ZZZ,ZZ9 )
                        ("9999/99/99"),"and Prev Chk Date:",iaa_Prev_Check_Date_Pnd_Prev_Check_Dte_N, new ReportEditMask ("9999/99/99"),new ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, 
                        new ReportEditMask ("Z,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,new ColumnSpacing(10),"    Business Days",pnd_Prev_Mnth_Date_N,                  //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT 10X '    Business Days' #PREV-MNTH-DATE-N ( EM = 9999-99-99 ) ' thru ' #END-MNTH-DATE-N ( EM = 9999-99-99 )
                        new ReportEditMask ("9999-99-99")," thru ",pnd_End_Mnth_Date_N, new ReportEditMask ("9999-99-99"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                       //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");

        getReports().setDisplayColumns(1, "/Contract",
        		pnd_Out_Selct_Trans_Pnd_Out_Cntrct_Nbr, new AlphanumericLength (8),"/Py",
        		pnd_Out_Selct_Trans_Pnd_Out_Payee_Cde, new ReportEditMask ("99"),"/Opt/cde",
        		pnd_Out_Selct_Trans_Pnd_Out_Opt_Cde, new ReportEditMask ("99"),"/trn/cde",
        		pnd_Out_Selct_Trans_Pnd_Out_Trn_Cde,"/Issue-Date",
        		pnd_Out_Selct_Trans_Pnd_Out_Issu_Dte, new ReportEditMask ("9999-99"),"/Tran/Today date",
        		pnd_Out_Selct_Trans_Pnd_Out_Trn_Dte, new ReportEditMask ("9999-99-99"),"/Tran/Check Date",
        		pnd_Out_Selct_Trans_Pnd_Out_Trn_Ck_Dte, new ReportEditMask ("9999-99-99"));
    }
}
