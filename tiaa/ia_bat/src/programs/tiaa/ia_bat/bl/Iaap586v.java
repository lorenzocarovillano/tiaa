/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:13 PM
**        * FROM NATURAL PROGRAM : Iaap586v
************************************************************
**        * FILE NAME            : Iaap586v.java
**        * CLASS NAME           : Iaap586v
**        * INSTANCE NAME        : Iaap586v
************************************************************
************************************************************************
*
* PROGRAM: IAAP586V
* CALLS "CALM" SYSTEM MODULE LAMN1400, WHICH WRITES RECORDS TO
* THE COMMON LEDGER TRANSACTION FILE (DB 003, FILE 150).
*
* MAINTENANCE:
*
* NAME          DATE       DESCRIPTION
* ----          ----       -----------
* O. SOTTO     04/15/14    NEW MODULE FOR OVERPAYMENTS.
*              05/23/14    NEW MODULE FOR CALM INTERFACE - LAMN1500
*              08/08/14    GET NAME FROM BNFCRY-NAME-FREE IF NAME IS
*                          SPACES.
* O. SOTTO     08/28/14    DON't pass those with participation greater
*                          THAN CURRENT DATE.  WRITE OUT TO A FILE
*                          FOR NEXT DAY's processing. Changes marked
*                          082814.
* O. SOTTO     11/04/14    BYPASS TRANSACTIONS WITH ZERO AMOUNTS.
*                          CHANGES MARKED 110414.
* O. SOTTO     03/06/15    CALM SUNSET CHANGES MARKED CALM2015.
* J. BREMER    08/26/16    RESTOW FOR IAALCTLS PIN EXPANSION
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO REVISED IATLCALM FOR PIN EXPANSION.
*          CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap586v extends BLNatBase
{
    // Data Areas
    private LdaIatlcalm ldaIatlcalm;
    private PdaNeca4000 pdaNeca4000;
    private LdaIaalctls ldaIaalctls;
    private PdaIaaactls pdaIaaactls;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;

    private DataAccessProgramView vw_iaa_Bene;
    private DbsField iaa_Bene_Bnfcry_Id_Nbr;

    private DbsGroup iaa_Bene_Bnfcry_Grp_List;
    private DbsField iaa_Bene_Bnfcry_Name_First;
    private DbsField iaa_Bene_Bnfcry_Name_Middle;
    private DbsField iaa_Bene_Bnfcry_Name_Last;
    private DbsField iaa_Bene_Bnfcry_Name_Free;

    private DataAccessProgramView vw_iaa_Sttlmnt;
    private DbsField iaa_Sttlmnt_Sttlmnt_Id_Nbr;
    private DbsField iaa_Sttlmnt_Sttlmnt_Process_Type;
    private DbsField iaa_Sttlmnt_Sttlmnt_Dod_Dte;
    private DbsField iaa_Sttlmnt_Sttlmnt_2nd_Dod_Dte;

    private DataAccessProgramView vw_pnd_Ext_Fund;
    private DbsField pnd_Ext_Fund_Nec_Table_Cde;
    private DbsField pnd_Ext_Fund_Nec_Ticker_Symbol;
    private DbsField pnd_Ext_Fund_Nec_Investment_Grouping_Id;

    private DbsGroup pnd_W2_File;
    private DbsField pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr;

    private DbsGroup pnd_W2_File__R_Field_1;
    private DbsField pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8;
    private DbsField pnd_W2_File_Pnd_W2_Payee_Cde_Alpha;

    private DbsGroup pnd_W2_File__R_Field_2;
    private DbsField pnd_W2_File_Pnd_W2_Payee_Cde_Num;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code;

    private DbsGroup pnd_W2_File__R_Field_3;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_1;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_2_3;

    private DbsGroup pnd_W2_File__R_Field_4;
    private DbsField pnd_W2_File_Pnd_W2_Fund_Code_2_3_N;
    private DbsField pnd_W2_File_Pnd_W2_Desc;
    private DbsField pnd_W2_File_Pnd_W2_Mode;
    private DbsField pnd_W2_File_Pnd_W2_Date;
    private DbsField pnd_W2_File_Pnd_W2_Guar_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Divd_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Unit_Payment;
    private DbsField pnd_W2_File_Pnd_W2_Guar_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Divd_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Unit_Overpayment;
    private DbsField pnd_W2_File_Pnd_W2_Msg;
    private DbsField pnd_W2_File_Pnd_W2_Pin;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_5;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_6;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_First_Call;
    private DbsField pnd_First_Time;
    private DbsField pnd_Debug;
    private DbsField pnd_Tot_Trans_Amt;
    private DbsField pnd_Dx;
    private DbsField pnd_Fund_Cnt;
    private DbsField pnd_Work_Date_X;
    private DbsField pnd_Work_Date;

    private DbsGroup pnd_Work_Date__R_Field_7;
    private DbsField pnd_Work_Date_Pnd_W_Yyyy;
    private DbsField pnd_Work_Date_Pnd_W_Mm;
    private DbsField pnd_Work_Date_Pnd_W_Dd;

    private DbsGroup pnd_Work_Date__R_Field_8;
    private DbsField pnd_Work_Date_Pnd_W_Yy;
    private DbsField pnd_Work_Date_Pnd_W_Mmdd;
    private DbsField pnd_Dte_Alpha;

    private DbsGroup pnd_Dte_Alpha__R_Field_9;
    private DbsField pnd_Dte_Alpha_Pnd_Dte;
    private DbsField pnd_Todays_Dte;

    private DbsGroup pnd_Todays_Dte__R_Field_10;
    private DbsField pnd_Todays_Dte_Pnd_Todays_Dte_A;
    private DbsField pnd_Stts_Cde;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Bypassed;
    private DbsField pnd_Tot_Amt;
    private DbsField pnd_Actl_Amt;
    private DbsField pnd_Day;
    private DbsField pnd_Fr_Ticker;
    private DbsField pnd_To_Ticker;
    private DbsField pnd_Ws_Ticker;
    private DbsField pnd_Fr_Contract_Pair;
    private DbsField pnd_To_Contract_Pair;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Tckr;
    private DbsField pnd_Tiaa_Lit;
    private DbsField pnd_Grdd_Sub_Lob;
    private DbsField pnd_Stdd_Sub_Lob;
    private DbsField pnd_Tiaa_And_Access;
    private DbsField pnd_Tiaa_Fnd_Cdes;
    private DbsField pnd_Annual;
    private DbsField pnd_Mnthly;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Ws_Ppcn;
    private DbsField pnd_Ws_Payee;

    private DbsGroup pnd_Ws_Payee__R_Field_11;
    private DbsField pnd_Ws_Payee_Pnd_Ws_Payee_N;
    private DbsField pnd_Ws_Pin_Key;

    private DbsGroup pnd_Ws_Pin_Key__R_Field_12;
    private DbsField pnd_Ws_Pin_Key_Pnd_Ws_Pin;

    private DbsGroup pnd_Ws_Pin_Key__R_Field_13;
    private DbsField pnd_Ws_Pin_Key_Pnd_Ws_Pin_A;
    private DbsField pnd_Ws_Pin_Key_Pnd_Filler;
    private DbsField pnd_Pin_Taxid_Bnfcry_Key;

    private DbsGroup pnd_Pin_Taxid_Bnfcry_Key__R_Field_14;
    private DbsField pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Tax_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Grp_Nmbr;
    private DbsField pnd_Res_Cde;
    private DbsField pnd_Ws_Dod;
    private DbsField pnd_Ws_Name;
    private DbsField pnd_Ws_Status;
    private DbsField pnd_Return_Cd;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_15;
    private DbsField pnd_W_Date_Pnd_W_Date_A;
    private DbsField pnd_Datd;
    private DbsField pnd_Naz_Tbl_Super1;

    private DbsGroup pnd_Naz_Tbl_Super1__R_Field_16;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField pnd_From_Tckr;
    private DbsField pnd_To_Acct;
    private DbsField pnd_To_Tckr;

    private DbsGroup pnd_T_Hdr1;
    private DbsField pnd_T_Hdr1_Pnd_Tprog;
    private DbsField pnd_T_Hdr1_Pnd_Tfill1;
    private DbsField pnd_T_Hdr1_Pnd_Thdr1;
    private DbsField pnd_T_Hdr1_Pnd_Tfill2;
    private DbsField pnd_T_Hdr1_Pnd_Thdr2;
    private DbsField pnd_T_Hdr1_Pnd_Thdr_Dte;

    private DbsGroup pnd_T_Hdr2;
    private DbsField pnd_T_Hdr2_Pnd_Tfill;
    private DbsField pnd_T_Hdr2_Pnd_Thdr;
    private DbsField pnd_T_Hdr2_Pnd_Thdr_Jdate;
    private DbsField pnd_T_Hdr2_Pnd_Tfill2;
    private DbsField pnd_T_Hdr2_Pnd_Tpage_Lit;
    private DbsField pnd_T_Hdr2_Pnd_Thdr_Page;

    private DbsGroup pnd_Hdr3;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld4;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld6;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld7;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld8;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld9;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld10;
    private DbsField pnd_Hdr3_Pnd_Hdr3_Fld11;

    private DbsGroup pnd_Dashes;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld4;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld6;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld7;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld8;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld9;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld10;
    private DbsField pnd_Dashes_Pnd_Dsh_Fld11;

    private DbsGroup pnd_Report_Detail;
    private DbsField pnd_Report_Detail_Pnd_To_Contract;
    private DbsField pnd_Report_Detail_Pnd_F4;
    private DbsField pnd_Report_Detail_Pnd_Ctls_To_Ticker;
    private DbsField pnd_Report_Detail_Pnd_F6;
    private DbsField pnd_Report_Detail_Pnd_Trans_Amt;
    private DbsField pnd_Report_Detail_Pnd_F7;
    private DbsField pnd_Report_Detail_Pnd_Ldgr_Ind;
    private DbsField pnd_Report_Detail_Pnd_F8;
    private DbsField pnd_Report_Detail_Pnd_Ind1;
    private DbsField pnd_Report_Detail_Pnd_F9;
    private DbsField pnd_Report_Detail_Pnd_Ind2;
    private DbsField pnd_Report_Detail_Pnd_Part_Date;
    private DbsField pnd_Ctls_Seq_Num;
    private DbsField pnd_Begin_Timn;

    private DbsGroup pnd_Begin_Timn__R_Field_17;
    private DbsField pnd_Begin_Timn_Pnd_Begin_Time;
    private DbsField pnd_End_Timn;

    private DbsGroup pnd_End_Timn__R_Field_18;
    private DbsField pnd_End_Timn_Pnd_End_Time;
    private DbsField pnd_Detail_Record;

    private DbsGroup pnd_Detail_Record__R_Field_19;
    private DbsField pnd_Detail_Record_Pnd_Fill;
    private DbsField pnd_Detail_Record_Pnd_Dtl_End;

    private DbsGroup pnd_Detail_Record__R_Field_20;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rcrd_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Sqnce;
    private DbsField pnd_Detail_Record_Pnd_Trans_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Typ_Rcrd;
    private DbsField pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rte_Basis;
    private DbsField pnd_Detail_Record_Pnd_Trans_Undldgr_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde;
    private DbsField pnd_Detail_Record_Pnd_Trans_Dept_Id;
    private DbsField pnd_Detail_Record_Pnd_Trans_Allctn_Ndx;
    private DbsField pnd_Detail_Record_Pnd_Trans_Vltn_Basis;
    private DbsField pnd_Detail_Record_Pnd_Trans_Amt_Sign;
    private DbsField pnd_Detail_Record_Pnd_Trans_Dtl_Amt;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Tkr;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Sub_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Tiaa_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Cref_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_From_Num_Units;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Tkr;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Sub_Lob;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Tiaa_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Cref_No;
    private DbsField pnd_Detail_Record_Pnd_Trans_To_Num_Units;
    private DbsField pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ldgr_Typ;
    private DbsField pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ind2;
    private DbsField pnd_Detail_Record_Pnd_Trans_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Tax_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte;
    private DbsField pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind;
    private DbsField pnd_Detail_Record_Pnd_Trans_Subplan;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatlcalm = new LdaIatlcalm();
        registerRecord(ldaIatlcalm);
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        ldaIaalctls = new LdaIaalctls();
        registerRecord(ldaIaalctls);
        pdaIaaactls = new PdaIaaactls(localVariables);

        // Local Variables

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Bene = new DataAccessProgramView(new NameInfo("vw_iaa_Bene", "IAA-BENE"), "IAA_DC_BNFCRY", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_BNFCRY"));
        iaa_Bene_Bnfcry_Id_Nbr = vw_iaa_Bene.getRecord().newFieldInGroup("iaa_Bene_Bnfcry_Id_Nbr", "BNFCRY-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "BNFCRY_ID_NBR");

        iaa_Bene_Bnfcry_Grp_List = vw_iaa_Bene.getRecord().newGroupInGroup("iaa_Bene_Bnfcry_Grp_List", "BNFCRY-GRP-LIST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_DEATH_CLAIMS_BNFCRY_GRP_LIST");
        iaa_Bene_Bnfcry_Name_First = iaa_Bene_Bnfcry_Grp_List.newFieldArrayInGroup("iaa_Bene_Bnfcry_Name_First", "BNFCRY-NAME-FIRST", FieldType.STRING, 
            30, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BNFCRY_NAME_FIRST", "IA_DEATH_CLAIMS_BNFCRY_GRP_LIST");
        iaa_Bene_Bnfcry_Name_Middle = iaa_Bene_Bnfcry_Grp_List.newFieldArrayInGroup("iaa_Bene_Bnfcry_Name_Middle", "BNFCRY-NAME-MIDDLE", FieldType.STRING, 
            30, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BNFCRY_NAME_MIDDLE", "IA_DEATH_CLAIMS_BNFCRY_GRP_LIST");
        iaa_Bene_Bnfcry_Name_Last = iaa_Bene_Bnfcry_Grp_List.newFieldArrayInGroup("iaa_Bene_Bnfcry_Name_Last", "BNFCRY-NAME-LAST", FieldType.STRING, 30, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BNFCRY_NAME_LAST", "IA_DEATH_CLAIMS_BNFCRY_GRP_LIST");
        iaa_Bene_Bnfcry_Name_Free = iaa_Bene_Bnfcry_Grp_List.newFieldArrayInGroup("iaa_Bene_Bnfcry_Name_Free", "BNFCRY-NAME-FREE", FieldType.STRING, 35, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BNFCRY_NAME_FREE", "IA_DEATH_CLAIMS_BNFCRY_GRP_LIST");
        registerRecord(vw_iaa_Bene);

        vw_iaa_Sttlmnt = new DataAccessProgramView(new NameInfo("vw_iaa_Sttlmnt", "IAA-STTLMNT"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        iaa_Sttlmnt_Sttlmnt_Id_Nbr = vw_iaa_Sttlmnt.getRecord().newFieldInGroup("iaa_Sttlmnt_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "STTLMNT_ID_NBR");
        iaa_Sttlmnt_Sttlmnt_Process_Type = vw_iaa_Sttlmnt.getRecord().newFieldInGroup("iaa_Sttlmnt_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "STTLMNT_PROCESS_TYPE");
        iaa_Sttlmnt_Sttlmnt_Dod_Dte = vw_iaa_Sttlmnt.getRecord().newFieldInGroup("iaa_Sttlmnt_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "STTLMNT_DOD_DTE");
        iaa_Sttlmnt_Sttlmnt_2nd_Dod_Dte = vw_iaa_Sttlmnt.getRecord().newFieldInGroup("iaa_Sttlmnt_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "STTLMNT_2ND_DOD_DTE");
        registerRecord(vw_iaa_Sttlmnt);

        vw_pnd_Ext_Fund = new DataAccessProgramView(new NameInfo("vw_pnd_Ext_Fund", "#EXT-FUND"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        pnd_Ext_Fund_Nec_Table_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        pnd_Ext_Fund_Nec_Ticker_Symbol = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        pnd_Ext_Fund_Nec_Investment_Grouping_Id = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_pnd_Ext_Fund);

        pnd_W2_File = localVariables.newGroupInRecord("pnd_W2_File", "#W2-FILE");
        pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr", "#W2-OVRPYMNT-PPCN-NBR", FieldType.STRING, 
            10);

        pnd_W2_File__R_Field_1 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_1", "REDEFINE", pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);
        pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8 = pnd_W2_File__R_Field_1.newFieldInGroup("pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8", "#W2-OVRPYMNT-PPCN-NBR-8", 
            FieldType.STRING, 8);
        pnd_W2_File_Pnd_W2_Payee_Cde_Alpha = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Payee_Cde_Alpha", "#W2-PAYEE-CDE-ALPHA", FieldType.STRING, 
            2);

        pnd_W2_File__R_Field_2 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_2", "REDEFINE", pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);
        pnd_W2_File_Pnd_W2_Payee_Cde_Num = pnd_W2_File__R_Field_2.newFieldInGroup("pnd_W2_File_Pnd_W2_Payee_Cde_Num", "#W2-PAYEE-CDE-NUM", FieldType.NUMERIC, 
            2);
        pnd_W2_File_Pnd_W2_Fund_Code = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code", "#W2-FUND-CODE", FieldType.STRING, 3);

        pnd_W2_File__R_Field_3 = pnd_W2_File.newGroupInGroup("pnd_W2_File__R_Field_3", "REDEFINE", pnd_W2_File_Pnd_W2_Fund_Code);
        pnd_W2_File_Pnd_W2_Fund_Code_1 = pnd_W2_File__R_Field_3.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_1", "#W2-FUND-CODE-1", FieldType.STRING, 
            1);
        pnd_W2_File_Pnd_W2_Fund_Code_2_3 = pnd_W2_File__R_Field_3.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_2_3", "#W2-FUND-CODE-2-3", FieldType.STRING, 
            2);

        pnd_W2_File__R_Field_4 = pnd_W2_File__R_Field_3.newGroupInGroup("pnd_W2_File__R_Field_4", "REDEFINE", pnd_W2_File_Pnd_W2_Fund_Code_2_3);
        pnd_W2_File_Pnd_W2_Fund_Code_2_3_N = pnd_W2_File__R_Field_4.newFieldInGroup("pnd_W2_File_Pnd_W2_Fund_Code_2_3_N", "#W2-FUND-CODE-2-3-N", FieldType.NUMERIC, 
            2);
        pnd_W2_File_Pnd_W2_Desc = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Desc", "#W2-DESC", FieldType.STRING, 20);
        pnd_W2_File_Pnd_W2_Mode = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Mode", "#W2-MODE", FieldType.NUMERIC, 3);
        pnd_W2_File_Pnd_W2_Date = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Date", "#W2-DATE", FieldType.NUMERIC, 8);
        pnd_W2_File_Pnd_W2_Guar_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Guar_Payment", "#W2-GUAR-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Divd_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Divd_Payment", "#W2-DIVD-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Unit_Payment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Unit_Payment", "#W2-UNIT-PAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Guar_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Guar_Overpayment", "#W2-GUAR-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Divd_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Divd_Overpayment", "#W2-DIVD-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Unit_Overpayment = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Unit_Overpayment", "#W2-UNIT-OVERPAYMENT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_W2_File_Pnd_W2_Msg = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Msg", "#W2-MSG", FieldType.STRING, 3);
        pnd_W2_File_Pnd_W2_Pin = pnd_W2_File.newFieldInGroup("pnd_W2_File_Pnd_W2_Pin", "#W2-PIN", FieldType.NUMERIC, 12);
        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_5 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_5", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super1__R_Field_5.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol = pnd_Nec_Fnd_Super1__R_Field_5.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol", "#NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_First_Call = localVariables.newFieldInRecord("pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt", "#TOT-TRANS-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Dx = localVariables.newFieldInRecord("pnd_Dx", "#DX", FieldType.INTEGER, 4);
        pnd_Fund_Cnt = localVariables.newFieldInRecord("pnd_Fund_Cnt", "#FUND-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Date_X = localVariables.newFieldInRecord("pnd_Work_Date_X", "#WORK-DATE-X", FieldType.STRING, 8);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);

        pnd_Work_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_7", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yyyy = pnd_Work_Date__R_Field_7.newFieldInGroup("pnd_Work_Date_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mm = pnd_Work_Date__R_Field_7.newFieldInGroup("pnd_Work_Date_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_Work_Date_Pnd_W_Dd = pnd_Work_Date__R_Field_7.newFieldInGroup("pnd_Work_Date_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);

        pnd_Work_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_8", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_W_Yy = pnd_Work_Date__R_Field_8.newFieldInGroup("pnd_Work_Date_Pnd_W_Yy", "#W-YY", FieldType.NUMERIC, 4);
        pnd_Work_Date_Pnd_W_Mmdd = pnd_Work_Date__R_Field_8.newFieldInGroup("pnd_Work_Date_Pnd_W_Mmdd", "#W-MMDD", FieldType.NUMERIC, 4);
        pnd_Dte_Alpha = localVariables.newFieldInRecord("pnd_Dte_Alpha", "#DTE-ALPHA", FieldType.STRING, 8);

        pnd_Dte_Alpha__R_Field_9 = localVariables.newGroupInRecord("pnd_Dte_Alpha__R_Field_9", "REDEFINE", pnd_Dte_Alpha);
        pnd_Dte_Alpha_Pnd_Dte = pnd_Dte_Alpha__R_Field_9.newFieldInGroup("pnd_Dte_Alpha_Pnd_Dte", "#DTE", FieldType.NUMERIC, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);

        pnd_Todays_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Todays_Dte__R_Field_10", "REDEFINE", pnd_Todays_Dte);
        pnd_Todays_Dte_Pnd_Todays_Dte_A = pnd_Todays_Dte__R_Field_10.newFieldInGroup("pnd_Todays_Dte_Pnd_Todays_Dte_A", "#TODAYS-DTE-A", FieldType.STRING, 
            8);
        pnd_Stts_Cde = localVariables.newFieldInRecord("pnd_Stts_Cde", "#STTS-CDE", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 4);
        pnd_Bypassed = localVariables.newFieldInRecord("pnd_Bypassed", "#BYPASSED", FieldType.NUMERIC, 5);
        pnd_Tot_Amt = localVariables.newFieldInRecord("pnd_Tot_Amt", "#TOT-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Actl_Amt = localVariables.newFieldInRecord("pnd_Actl_Amt", "#ACTL-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Fr_Ticker = localVariables.newFieldInRecord("pnd_Fr_Ticker", "#FR-TICKER", FieldType.STRING, 10);
        pnd_To_Ticker = localVariables.newFieldInRecord("pnd_To_Ticker", "#TO-TICKER", FieldType.STRING, 10);
        pnd_Ws_Ticker = localVariables.newFieldInRecord("pnd_Ws_Ticker", "#WS-TICKER", FieldType.STRING, 10);
        pnd_Fr_Contract_Pair = localVariables.newFieldInRecord("pnd_Fr_Contract_Pair", "#FR-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_To_Contract_Pair = localVariables.newFieldInRecord("pnd_To_Contract_Pair", "#TO-CONTRACT-PAIR", FieldType.STRING, 10);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Tckr = localVariables.newFieldInRecord("pnd_Tckr", "#TCKR", FieldType.STRING, 10);
        pnd_Tiaa_Lit = localVariables.newFieldInRecord("pnd_Tiaa_Lit", "#TIAA-LIT", FieldType.STRING, 10);
        pnd_Grdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Grdd_Sub_Lob", "#GRDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Stdd_Sub_Lob = localVariables.newFieldInRecord("pnd_Stdd_Sub_Lob", "#STDD-SUB-LOB", FieldType.STRING, 12);
        pnd_Tiaa_And_Access = localVariables.newFieldArrayInRecord("pnd_Tiaa_And_Access", "#TIAA-AND-ACCESS", FieldType.STRING, 1, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Fnd_Cdes = localVariables.newFieldArrayInRecord("pnd_Tiaa_Fnd_Cdes", "#TIAA-FND-CDES", FieldType.STRING, 2, new DbsArrayController(1, 
            4));
        pnd_Annual = localVariables.newFieldArrayInRecord("pnd_Annual", "#ANNUAL", FieldType.STRING, 1, new DbsArrayController(1, 2));
        pnd_Mnthly = localVariables.newFieldArrayInRecord("pnd_Mnthly", "#MNTHLY", FieldType.STRING, 1, new DbsArrayController(1, 2));

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Ws_Ppcn = localVariables.newFieldInRecord("pnd_Ws_Ppcn", "#WS-PPCN", FieldType.STRING, 10);
        pnd_Ws_Payee = localVariables.newFieldInRecord("pnd_Ws_Payee", "#WS-PAYEE", FieldType.STRING, 2);

        pnd_Ws_Payee__R_Field_11 = localVariables.newGroupInRecord("pnd_Ws_Payee__R_Field_11", "REDEFINE", pnd_Ws_Payee);
        pnd_Ws_Payee_Pnd_Ws_Payee_N = pnd_Ws_Payee__R_Field_11.newFieldInGroup("pnd_Ws_Payee_Pnd_Ws_Payee_N", "#WS-PAYEE-N", FieldType.NUMERIC, 2);
        pnd_Ws_Pin_Key = localVariables.newFieldInRecord("pnd_Ws_Pin_Key", "#WS-PIN-KEY", FieldType.BINARY, 24);

        pnd_Ws_Pin_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Ws_Pin_Key__R_Field_12", "REDEFINE", pnd_Ws_Pin_Key);
        pnd_Ws_Pin_Key_Pnd_Ws_Pin = pnd_Ws_Pin_Key__R_Field_12.newFieldInGroup("pnd_Ws_Pin_Key_Pnd_Ws_Pin", "#WS-PIN", FieldType.NUMERIC, 12);

        pnd_Ws_Pin_Key__R_Field_13 = pnd_Ws_Pin_Key__R_Field_12.newGroupInGroup("pnd_Ws_Pin_Key__R_Field_13", "REDEFINE", pnd_Ws_Pin_Key_Pnd_Ws_Pin);
        pnd_Ws_Pin_Key_Pnd_Ws_Pin_A = pnd_Ws_Pin_Key__R_Field_13.newFieldInGroup("pnd_Ws_Pin_Key_Pnd_Ws_Pin_A", "#WS-PIN-A", FieldType.STRING, 12);
        pnd_Ws_Pin_Key_Pnd_Filler = pnd_Ws_Pin_Key__R_Field_12.newFieldInGroup("pnd_Ws_Pin_Key_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Bnfcry_Key = localVariables.newFieldInRecord("pnd_Pin_Taxid_Bnfcry_Key", "#PIN-TAXID-BNFCRY-KEY", FieldType.BINARY, 23);

        pnd_Pin_Taxid_Bnfcry_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Bnfcry_Key__R_Field_14", "REDEFINE", pnd_Pin_Taxid_Bnfcry_Key);
        pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Id_Nbr = pnd_Pin_Taxid_Bnfcry_Key__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Id_Nbr", 
            "#BNFCRY-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Tax_Id_Nbr = pnd_Pin_Taxid_Bnfcry_Key__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Tax_Id_Nbr", 
            "#BNFCRY-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Grp_Nmbr = pnd_Pin_Taxid_Bnfcry_Key__R_Field_14.newFieldInGroup("pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Grp_Nmbr", 
            "#BNFCRY-GRP-NMBR", FieldType.NUMERIC, 2);
        pnd_Res_Cde = localVariables.newFieldInRecord("pnd_Res_Cde", "#RES-CDE", FieldType.STRING, 2);
        pnd_Ws_Dod = localVariables.newFieldInRecord("pnd_Ws_Dod", "#WS-DOD", FieldType.NUMERIC, 8);
        pnd_Ws_Name = localVariables.newFieldInRecord("pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 35);
        pnd_Ws_Status = localVariables.newFieldInRecord("pnd_Ws_Status", "#WS-STATUS", FieldType.STRING, 18);
        pnd_Return_Cd = localVariables.newFieldInRecord("pnd_Return_Cd", "#RETURN-CD", FieldType.STRING, 2);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);

        pnd_W_Date__R_Field_15 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_15", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Date_A = pnd_W_Date__R_Field_15.newFieldInGroup("pnd_W_Date_Pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Naz_Tbl_Super1 = localVariables.newFieldInRecord("pnd_Naz_Tbl_Super1", "#NAZ-TBL-SUPER1", FieldType.STRING, 29);

        pnd_Naz_Tbl_Super1__R_Field_16 = localVariables.newGroupInRecord("pnd_Naz_Tbl_Super1__R_Field_16", "REDEFINE", pnd_Naz_Tbl_Super1);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Tbl_Super1__R_Field_16.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", "#NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Tbl_Super1__R_Field_16.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", "#NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Tbl_Super1__R_Field_16.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", "#NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_From_Tckr = localVariables.newFieldInRecord("pnd_From_Tckr", "#FROM-TCKR", FieldType.STRING, 10);
        pnd_To_Acct = localVariables.newFieldArrayInRecord("pnd_To_Acct", "#TO-ACCT", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_To_Tckr = localVariables.newFieldArrayInRecord("pnd_To_Tckr", "#TO-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 50));

        pnd_T_Hdr1 = localVariables.newGroupInRecord("pnd_T_Hdr1", "#T-HDR1");
        pnd_T_Hdr1_Pnd_Tprog = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tprog", "#TPROG", FieldType.STRING, 8);
        pnd_T_Hdr1_Pnd_Tfill1 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tfill1", "#TFILL1", FieldType.STRING, 15);
        pnd_T_Hdr1_Pnd_Thdr1 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr1", "#THDR1", FieldType.STRING, 37);
        pnd_T_Hdr1_Pnd_Tfill2 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Tfill2", "#TFILL2", FieldType.STRING, 10);
        pnd_T_Hdr1_Pnd_Thdr2 = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr2", "#THDR2", FieldType.STRING, 10);
        pnd_T_Hdr1_Pnd_Thdr_Dte = pnd_T_Hdr1.newFieldInGroup("pnd_T_Hdr1_Pnd_Thdr_Dte", "#THDR-DTE", FieldType.STRING, 10);

        pnd_T_Hdr2 = localVariables.newGroupInRecord("pnd_T_Hdr2", "#T-HDR2");
        pnd_T_Hdr2_Pnd_Tfill = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tfill", "#TFILL", FieldType.STRING, 29);
        pnd_T_Hdr2_Pnd_Thdr = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr", "#THDR", FieldType.STRING, 14);
        pnd_T_Hdr2_Pnd_Thdr_Jdate = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr_Jdate", "#THDR-JDATE", FieldType.STRING, 10);
        pnd_T_Hdr2_Pnd_Tfill2 = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tfill2", "#TFILL2", FieldType.STRING, 17);
        pnd_T_Hdr2_Pnd_Tpage_Lit = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Tpage_Lit", "#TPAGE-LIT", FieldType.STRING, 6);
        pnd_T_Hdr2_Pnd_Thdr_Page = pnd_T_Hdr2.newFieldInGroup("pnd_T_Hdr2_Pnd_Thdr_Page", "#THDR-PAGE", FieldType.NUMERIC, 7);

        pnd_Hdr3 = localVariables.newGroupInRecord("pnd_Hdr3", "#HDR3");
        pnd_Hdr3_Pnd_Hdr3_Fld4 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld4", "#HDR3-FLD4", FieldType.STRING, 12);
        pnd_Hdr3_Pnd_Hdr3_Fld6 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld6", "#HDR3-FLD6", FieldType.STRING, 13);
        pnd_Hdr3_Pnd_Hdr3_Fld7 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld7", "#HDR3-FLD7", FieldType.STRING, 14);
        pnd_Hdr3_Pnd_Hdr3_Fld8 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld8", "#HDR3-FLD8", FieldType.STRING, 11);
        pnd_Hdr3_Pnd_Hdr3_Fld9 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld9", "#HDR3-FLD9", FieldType.STRING, 6);
        pnd_Hdr3_Pnd_Hdr3_Fld10 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld10", "#HDR3-FLD10", FieldType.STRING, 4);
        pnd_Hdr3_Pnd_Hdr3_Fld11 = pnd_Hdr3.newFieldInGroup("pnd_Hdr3_Pnd_Hdr3_Fld11", "#HDR3-FLD11", FieldType.STRING, 10);

        pnd_Dashes = localVariables.newGroupInRecord("pnd_Dashes", "#DASHES");
        pnd_Dashes_Pnd_Dsh_Fld4 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld4", "#DSH-FLD4", FieldType.STRING, 12);
        pnd_Dashes_Pnd_Dsh_Fld6 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld6", "#DSH-FLD6", FieldType.STRING, 13);
        pnd_Dashes_Pnd_Dsh_Fld7 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld7", "#DSH-FLD7", FieldType.STRING, 14);
        pnd_Dashes_Pnd_Dsh_Fld8 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld8", "#DSH-FLD8", FieldType.STRING, 11);
        pnd_Dashes_Pnd_Dsh_Fld9 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld9", "#DSH-FLD9", FieldType.STRING, 6);
        pnd_Dashes_Pnd_Dsh_Fld10 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld10", "#DSH-FLD10", FieldType.STRING, 4);
        pnd_Dashes_Pnd_Dsh_Fld11 = pnd_Dashes.newFieldInGroup("pnd_Dashes_Pnd_Dsh_Fld11", "#DSH-FLD11", FieldType.STRING, 9);

        pnd_Report_Detail = localVariables.newGroupInRecord("pnd_Report_Detail", "#REPORT-DETAIL");
        pnd_Report_Detail_Pnd_To_Contract = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_To_Contract", "#TO-CONTRACT", FieldType.STRING, 10);
        pnd_Report_Detail_Pnd_F4 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Ctls_To_Ticker = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ctls_To_Ticker", "#CTLS-TO-TICKER", FieldType.STRING, 
            10);
        pnd_Report_Detail_Pnd_F6 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_Trans_Amt = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Trans_Amt", "#TRANS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Report_Detail_Pnd_F7 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F7", "#F7", FieldType.STRING, 4);
        pnd_Report_Detail_Pnd_Ldgr_Ind = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ldgr_Ind", "#LDGR-IND", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_F8 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F8", "#F8", FieldType.STRING, 7);
        pnd_Report_Detail_Pnd_Ind1 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ind1", "#IND1", FieldType.STRING, 1);
        pnd_Report_Detail_Pnd_F9 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_F9", "#F9", FieldType.STRING, 4);
        pnd_Report_Detail_Pnd_Ind2 = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Ind2", "#IND2", FieldType.STRING, 3);
        pnd_Report_Detail_Pnd_Part_Date = pnd_Report_Detail.newFieldInGroup("pnd_Report_Detail_Pnd_Part_Date", "#PART-DATE", FieldType.STRING, 8);
        pnd_Ctls_Seq_Num = localVariables.newFieldInRecord("pnd_Ctls_Seq_Num", "#CTLS-SEQ-NUM", FieldType.NUMERIC, 9);
        pnd_Begin_Timn = localVariables.newFieldInRecord("pnd_Begin_Timn", "#BEGIN-TIMN", FieldType.NUMERIC, 7);

        pnd_Begin_Timn__R_Field_17 = localVariables.newGroupInRecord("pnd_Begin_Timn__R_Field_17", "REDEFINE", pnd_Begin_Timn);
        pnd_Begin_Timn_Pnd_Begin_Time = pnd_Begin_Timn__R_Field_17.newFieldInGroup("pnd_Begin_Timn_Pnd_Begin_Time", "#BEGIN-TIME", FieldType.NUMERIC, 
            6);
        pnd_End_Timn = localVariables.newFieldInRecord("pnd_End_Timn", "#END-TIMN", FieldType.NUMERIC, 7);

        pnd_End_Timn__R_Field_18 = localVariables.newGroupInRecord("pnd_End_Timn__R_Field_18", "REDEFINE", pnd_End_Timn);
        pnd_End_Timn_Pnd_End_Time = pnd_End_Timn__R_Field_18.newFieldInGroup("pnd_End_Timn_Pnd_End_Time", "#END-TIME", FieldType.NUMERIC, 6);
        pnd_Detail_Record = localVariables.newFieldInRecord("pnd_Detail_Record", "#DETAIL-RECORD", FieldType.STRING, 250);

        pnd_Detail_Record__R_Field_19 = localVariables.newGroupInRecord("pnd_Detail_Record__R_Field_19", "REDEFINE", pnd_Detail_Record);
        pnd_Detail_Record_Pnd_Fill = pnd_Detail_Record__R_Field_19.newFieldInGroup("pnd_Detail_Record_Pnd_Fill", "#FILL", FieldType.STRING, 249);
        pnd_Detail_Record_Pnd_Dtl_End = pnd_Detail_Record__R_Field_19.newFieldInGroup("pnd_Detail_Record_Pnd_Dtl_End", "#DTL-END", FieldType.STRING, 1);

        pnd_Detail_Record__R_Field_20 = localVariables.newGroupInRecord("pnd_Detail_Record__R_Field_20", "REDEFINE", pnd_Detail_Record);
        pnd_Detail_Record_Pnd_Trans_Rcrd_Typ = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rcrd_Typ", "#TRANS-RCRD-TYP", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Sqnce = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Sqnce", "#TRANS-SQNCE", FieldType.NUMERIC, 
            9);
        pnd_Detail_Record_Pnd_Trans_Typ = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Typ", "#TRANS-TYP", FieldType.STRING, 
            2);
        pnd_Detail_Record_Pnd_Trans_Typ_Rcrd = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Typ_Rcrd", "#TRANS-TYP-RCRD", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr", "#TRANS-OCCRNCE-NBR", 
            FieldType.NUMERIC, 4);
        pnd_Detail_Record_Pnd_Trans_Rte_Basis = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rte_Basis", "#TRANS-RTE-BASIS", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Undldgr_Ind = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Undldgr_Ind", "#TRANS-UNDLDGR-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde", "#TRANS-RVRSL-CDE", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Dept_Id = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Dept_Id", "#TRANS-DEPT-ID", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_Allctn_Ndx = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Allctn_Ndx", "#TRANS-ALLCTN-NDX", 
            FieldType.STRING, 6);
        pnd_Detail_Record_Pnd_Trans_Vltn_Basis = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Vltn_Basis", "#TRANS-VLTN-BASIS", 
            FieldType.STRING, 5);
        pnd_Detail_Record_Pnd_Trans_Amt_Sign = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Amt_Sign", "#TRANS-AMT-SIGN", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Dtl_Amt = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Dtl_Amt", "#TRANS-DTL-AMT", FieldType.NUMERIC, 
            16, 2);
        pnd_Detail_Record_Pnd_Trans_From_Tkr = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Tkr", "#TRANS-FROM-TKR", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Lob = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Lob", "#TRANS-FROM-LOB", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Sub_Lob = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Sub_Lob", "#TRANS-FROM-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Detail_Record_Pnd_Trans_From_Tiaa_No = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Tiaa_No", "#TRANS-FROM-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Cref_No = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Cref_No", "#TRANS-FROM-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_From_Num_Units = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_From_Num_Units", "#TRANS-FROM-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Detail_Record_Pnd_Trans_To_Tkr = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Tkr", "#TRANS-TO-TKR", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_To_Lob = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Lob", "#TRANS-TO-LOB", FieldType.STRING, 
            10);
        pnd_Detail_Record_Pnd_Trans_To_Sub_Lob = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Sub_Lob", "#TRANS-TO-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Detail_Record_Pnd_Trans_To_Tiaa_No = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Tiaa_No", "#TRANS-TO-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_To_Cref_No = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Cref_No", "#TRANS-TO-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_Trans_To_Num_Units = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_To_Num_Units", "#TRANS-TO-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id", "#TRANS-PITS-RQST-ID", 
            FieldType.STRING, 29);
        pnd_Detail_Record_Pnd_Trans_Ldgr_Typ = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ldgr_Typ", "#TRANS-LDGR-TYP", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw", "#TRANS-CNCL-RDRW", 
            FieldType.STRING, 2);
        pnd_Detail_Record_Pnd_Trans_Ind2 = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ind2", "#TRANS-IND2", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Ind = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Ind", "#TRANS-IND", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Tax_Ind = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Tax_Ind", "#TRANS-TAX-IND", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind", "#TRANS-NET-CASH-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte", "#TRANS-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind", "#TRANS-1ST-YR-RNWL-IND", 
            FieldType.STRING, 1);
        pnd_Detail_Record_Pnd_Trans_Subplan = pnd_Detail_Record__R_Field_20.newFieldInGroup("pnd_Detail_Record_Pnd_Trans_Subplan", "#TRANS-SUBPLAN", FieldType.STRING, 
            6);
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Bene.reset();
        vw_iaa_Sttlmnt.reset();
        vw_pnd_Ext_Fund.reset();
        vw_naz_Table_Ddm.reset();

        ldaIatlcalm.initializeValues();
        ldaIaalctls.initializeValues();

        localVariables.reset();
        pnd_First_Call.setInitialValue(true);
        pnd_First_Time.setInitialValue(true);
        pnd_Dx.setInitialValue(0);
        pnd_Fund_Cnt.setInitialValue(20);
        pnd_Tiaa_Lit.setInitialValue("TIAA#");
        pnd_Grdd_Sub_Lob.setInitialValue("GBPM-IA");
        pnd_Stdd_Sub_Lob.setInitialValue("IA");
        pnd_Tiaa_And_Access.getValue(1).setInitialValue("T");
        pnd_Tiaa_And_Access.getValue(2).setInitialValue("G");
        pnd_Tiaa_And_Access.getValue(3).setInitialValue("D");
        pnd_Tiaa_Fnd_Cdes.getValue(1).setInitialValue("1S");
        pnd_Tiaa_Fnd_Cdes.getValue(2).setInitialValue("1G");
        pnd_Tiaa_Fnd_Cdes.getValue(3).setInitialValue("09");
        pnd_Tiaa_Fnd_Cdes.getValue(4).setInitialValue("11");
        pnd_Annual.getValue(1).setInitialValue("U");
        pnd_Annual.getValue(2).setInitialValue("2");
        pnd_Mnthly.getValue(1).setInitialValue("W");
        pnd_Mnthly.getValue(2).setInitialValue("4");
        pnd_T_Hdr1_Pnd_Thdr1.setInitialValue("IA Overpayment Interface to CALM/CTLS");
        pnd_T_Hdr1_Pnd_Thdr2.setInitialValue("Run Date: ");
        pnd_T_Hdr2_Pnd_Thdr.setInitialValue("Journal Date: ");
        pnd_T_Hdr2_Pnd_Tpage_Lit.setInitialValue("Page: ");
        pnd_Hdr3_Pnd_Hdr3_Fld4.setInitialValue("PPCN");
        pnd_Hdr3_Pnd_Hdr3_Fld6.setInitialValue("CTLS Tkr");
        pnd_Hdr3_Pnd_Hdr3_Fld7.setInitialValue("Trans Amt");
        pnd_Hdr3_Pnd_Hdr3_Fld8.setInitialValue("Ledgered");
        pnd_Hdr3_Pnd_Hdr3_Fld9.setInitialValue("Ind1");
        pnd_Hdr3_Pnd_Hdr3_Fld10.setInitialValue("Ind2");
        pnd_Hdr3_Pnd_Hdr3_Fld11.setInitialValue("Part Date");
        pnd_Dashes_Pnd_Dsh_Fld4.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld6.setInitialValue("----------");
        pnd_Dashes_Pnd_Dsh_Fld7.setInitialValue("------------");
        pnd_Dashes_Pnd_Dsh_Fld8.setInitialValue("--------  ");
        pnd_Dashes_Pnd_Dsh_Fld9.setInitialValue("---- ");
        pnd_Dashes_Pnd_Dsh_Fld10.setInitialValue("----");
        pnd_Dashes_Pnd_Dsh_Fld11.setInitialValue("---------");
        pnd_Ctls_Seq_Num.setInitialValue(300000000);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap586v() throws Exception
    {
        super("Iaap586v");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_T_Hdr1_Pnd_Thdr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                        //Natural: FORMAT LS = 133 PS = 60;//Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #THDR-DTE
        pnd_Debug.reset();                                                                                                                                                //Natural: AT TOP OF PAGE ( 1 );//Natural: RESET #DEBUG
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = 'NAZ021UTLUTL3'
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", "NAZ021UTLUTL3", WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.equals("TEST")))                                                                                         //Natural: IF NAZ-TBL-RCRD-DSCRPTN-TXT = 'TEST'
            {
                pnd_Debug.setValue(true);                                                                                                                                 //Natural: ASSIGN #DEBUG := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Debug.setValue(false);                                                                                                                                //Natural: ASSIGN #DEBUG := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  LOAD INDEPENDENTS
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYMBOL
        sub_Get_Ticker_Symbol();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().display(0, "TKR",                                                                                                                                //Natural: DISPLAY 'TKR' +TCKR-SYMBL ( * ) 'FNUM' +FUND-NUM-CDE ( * ) 'FALPHA' +FUND-ALPHA-CDE ( * )
            		pls_Tckr_Symbl.getValue("*"),"FNUM",
            		pls_Fund_Num_Cde.getValue("*"),"FALPHA",
            		pls_Fund_Alpha_Cde.getValue("*"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  082814 START
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 3 #TODAYS-DTE
        while (condition(getWorkFiles().read(3, pnd_Todays_Dte)))
        {
            pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Todays_Dte_Pnd_Todays_Dte_A);                                                                      //Natural: MOVE EDITED #TODAYS-DTE-A TO #DATD ( EM = YYYYMMDD )
            pnd_Day.setValueEdited(pnd_Datd,new ReportEditMask("NNNNNNNNN"));                                                                                             //Natural: MOVE EDITED #DATD ( EM = N ( 9 ) ) TO #DAY
            getReports().write(0, "TODAYS RUN DATE IS",pnd_Datd, new ReportEditMask ("YYYYMMDD"),pnd_Day);                                                                //Natural: WRITE 'TODAYS RUN DATE IS' #DATD ( EM = YYYYMMDD ) #DAY
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  082814 END
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CALM2015
        ldaIatlcalm.getLama1001_Group_Pda().reset();                                                                                                                      //Natural: RESET LAMA1001.GROUP-PDA #TOT-TRANS-AMT
        pnd_Tot_Trans_Amt.reset();
        pnd_Begin_Timn.setValue(Global.getTIMN());                                                                                                                        //Natural: ASSIGN #BEGIN-TIMN := *TIMN
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #W2-FILE
        while (condition(getWorkFiles().read(1, pnd_W2_File)))
        {
            if (condition(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8.equals("99999999")))                                                                                     //Natural: IF #W2-OVRPYMNT-PPCN-NBR-8 = '99999999'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  110414 START
            if (condition(pnd_W2_File_Pnd_W2_Guar_Overpayment.equals(getZero()) && pnd_W2_File_Pnd_W2_Divd_Overpayment.equals(getZero()) && pnd_W2_File_Pnd_W2_Unit_Overpayment.equals(getZero()))) //Natural: IF #W2-GUAR-OVERPAYMENT EQ 0 AND #W2-DIVD-OVERPAYMENT EQ 0 AND #W2-UNIT-OVERPAYMENT EQ 0
            {
                getReports().write(0, "CONTRACT ",pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8," BYPASSED DUE TO ZERO AMOUNTS");                                                //Natural: WRITE 'CONTRACT ' #W2-OVRPYMNT-PPCN-NBR-8 ' BYPASSED DUE TO ZERO AMOUNTS'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  110414 END
            if (condition(!(pnd_W2_File_Pnd_W2_Guar_Overpayment.greater(getZero()) || pnd_W2_File_Pnd_W2_Divd_Overpayment.greater(getZero()) || pnd_W2_File_Pnd_W2_Unit_Overpayment.greater(getZero())))) //Natural: ACCEPT IF #W2-GUAR-OVERPAYMENT GT 0 OR #W2-DIVD-OVERPAYMENT GT 0 OR #W2-UNIT-OVERPAYMENT GT 0
            {
                continue;
            }
            //*  082814 START
            if (condition(pnd_W2_File_Pnd_W2_Date.greater(pnd_Todays_Dte) || pnd_Day.equals("Saturday")))                                                                 //Natural: IF #W2-DATE GT #TODAYS-DTE OR #DAY = 'Saturday'
            {
                getWorkFiles().write(2, false, pnd_W2_File);                                                                                                              //Natural: WRITE WORK FILE 2 #W2-FILE
                pnd_Bypassed.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #BYPASSED
                getReports().write(0, "Bypassed ",pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8," with part date of",pnd_W2_File_Pnd_W2_Date);                                   //Natural: WRITE 'Bypassed ' #W2-OVRPYMNT-PPCN-NBR-8 ' with part date of' #W2-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  082814 END
            }                                                                                                                                                             //Natural: END-IF
            //*  082017
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME := FALSE
                pnd_Ws_Ppcn.setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #WS-PPCN := #W2-OVRPYMNT-PPCN-NBR
                pnd_Ws_Payee.setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                                //Natural: ASSIGN #WS-PAYEE := #W2-PAYEE-CDE-ALPHA
                pnd_Pin_Taxid_Bnfcry_Key_Pnd_Bnfcry_Id_Nbr.setValue(pnd_W2_File_Pnd_W2_Pin);                                                                              //Natural: ASSIGN #BNFCRY-ID-NBR := #WS-PIN := #W2-PIN
                pnd_Ws_Pin_Key_Pnd_Ws_Pin.setValue(pnd_W2_File_Pnd_W2_Pin);
                pnd_Dte_Alpha_Pnd_Dte.setValue(pnd_W2_File_Pnd_W2_Date);                                                                                                  //Natural: ASSIGN #DTE := #W2-DATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Ppcn.notEquals(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr) || pnd_Ws_Payee.notEquals(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha) ||                    //Natural: IF #WS-PPCN NE #W2-OVRPYMNT-PPCN-NBR OR #WS-PAYEE NE #W2-PAYEE-CDE-ALPHA OR #WS-PIN NE #W2-PIN OR #DTE NE #W2-DATE
                pnd_Ws_Pin_Key_Pnd_Ws_Pin.notEquals(pnd_W2_File_Pnd_W2_Pin) || pnd_Dte_Alpha_Pnd_Dte.notEquals(pnd_W2_File_Pnd_W2_Date)))
            {
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Ws_Ppcn);                                                                                      //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := #WS-PPCN
                pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(2);                                                                                               //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 02
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCE
                sub_Get_Residence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-DOD
                sub_Get_Dod();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-SURVIVOR-NAME
                sub_Get_Survivor_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-STATUS
                sub_Get_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-RCRD-FIELDS
                sub_Write_Hdr_Rcrd_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CALM2015
                                                                                                                                                                          //Natural: PERFORM WRITE-CTLS-TRANSACTIONS
                sub_Write_Ctls_Transactions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Return_Cd.notEquals(" ")))                                                                                                              //Natural: IF #RETURN-CD NE ' '
                {
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                //* *  END TRANSACTION                      /* CALM2015
                pnd_Tot_Trans_Amt.reset();                                                                                                                                //Natural: RESET #TOT-TRANS-AMT LAMA1001.GROUP-PDA #RETURN-CD
                ldaIatlcalm.getLama1001_Group_Pda().reset();
                pnd_Return_Cd.reset();
                pnd_Ws_Ppcn.setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #WS-PPCN := #W2-OVRPYMNT-PPCN-NBR
                pnd_Ws_Payee.setValue(pnd_W2_File_Pnd_W2_Payee_Cde_Alpha);                                                                                                //Natural: ASSIGN #WS-PAYEE := #W2-PAYEE-CDE-ALPHA
                pnd_Ws_Pin_Key_Pnd_Ws_Pin.setValue(pnd_W2_File_Pnd_W2_Pin);                                                                                               //Natural: ASSIGN #WS-PIN := #W2-PIN
                pnd_Dte_Alpha_Pnd_Dte.setValue(pnd_W2_File_Pnd_W2_Date);                                                                                                  //Natural: ASSIGN #DTE := #W2-DATE
                //*  CALM2015 START
                pnd_Ctls_Seq_Num.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CTLS-SEQ-NUM
                //*  CALM2015 END
                ldaIaalctls.getPnd_Ctls_Hdr_Record().reset();                                                                                                             //Natural: RESET #CTLS-HDR-RECORD #CTLS-TRANS-RECORD ( * ) #DX #CTLS-TRANS-CNT
                ldaIaalctls.getPnd_Ctls_Trans_Record().getValue("*").reset();
                pnd_Dx.reset();
                ldaIaalctls.getPnd_Ctls_Trans_Cnt().reset();
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD
                sub_Process_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Return_Cd.notEquals(" ")))                                                                                                              //Natural: IF #RETURN-CD NE ' '
                {
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD
                sub_Process_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Return_Cd.notEquals(" ")))                                                                                                              //Natural: IF #RETURN-CD NE ' '
                {
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Ppcn.notEquals(" ")))                                                                                                                        //Natural: IF #WS-PPCN NE ' '
        {
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Ws_Ppcn);                                                                                          //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := #WS-PPCN
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(2);                                                                                                   //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 02
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCE
            sub_Get_Residence();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-DOD
            sub_Get_Dod();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-SURVIVOR-NAME
            sub_Get_Survivor_Name();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-STATUS
            sub_Get_Status();
            if (condition(Global.isEscape())) {return;}
            //*  WRITE THE LAST RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-RCRD-FIELDS
            sub_Write_Hdr_Rcrd_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  CALM2015
                                                                                                                                                                          //Natural: PERFORM WRITE-CTLS-TRANSACTIONS
            sub_Write_Ctls_Transactions();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALM2015
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAILER
        sub_Write_Trailer();
        if (condition(Global.isEscape())) {return;}
        //* *END TRANSACTION                     /* CALM2015
        //*  082814 START
        if (condition(pnd_Bypassed.equals(getZero())))                                                                                                                    //Natural: IF #BYPASSED = 0
        {
            //*  WRITE EMPTY FILE
            pnd_W2_File.reset();                                                                                                                                          //Natural: RESET #W2-FILE
            pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr_8.setValue("99999999");                                                                                                  //Natural: ASSIGN #W2-OVRPYMNT-PPCN-NBR-8 := '99999999'
            getWorkFiles().write(2, false, pnd_W2_File);                                                                                                                  //Natural: WRITE WORK FILE 2 #W2-FILE
            //*  082814 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tot_Amt.greater(getZero())))                                                                                                                    //Natural: IF #TOT-AMT GT 0
        {
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Total:",new ColumnSpacing(15),pnd_Tot_Amt);                                                                       //Natural: WRITE ( 1 ) 'Total:' 15X #TOT-AMT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*** No overpayments to report today ***");                                                        //Natural: WRITE ( 1 ) // '*** No overpayments to report today ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EXT-MODULE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENCE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SHORT-NAME
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TICKER-SYMBOL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DETAIL-DATA
        //* *DTL-FROM-FUND-TICKER-SYMBOL (#DX) :=
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETAIL-FOR-TIAA
        //* ***********************************************************************
        //*  DTL-FROM-FUND-TICKER-SYMBOL (#DX) :=
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOD
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATUS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SURVIVOR-NAME
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HDR-RCRD-FIELDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HDR-DATA
        //* ***********************************************************************
        //* *IF #DX > 50
        //* *  MORE-DATA                 := 'Y'
        //* *  DETAIL-REC-CNT            := 50
        //* *ELSE
        //* *  MORE-DATA                 := 'N'
        //* *  DETAIL-REC-CNT            := #DX
        //* *END-IF
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STTS-CDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FIELDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORTS
        //* *FOR #Y 1 50
        //*  #CTLS-TO-TICKER := DTL-TO-FUND-TICKER-SYMBOL (#Y)
        //*  #TRANS-AMT := DTL-TRANSACTION-AMT (#Y)
        //*  #IND1 := DTL-SETTLEMENT-IND (#Y)
        //*  #IND2 := DTL-SETTLEMENT-IND2(#Y)
        //*  CALM2015 START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CTLS-TRANSACTIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAILER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TRANS-FIELDS
        //*  CALM2015 END
    }
    private void sub_Process_Record() throws Exception                                                                                                                    //Natural: PROCESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_From_Tckr.reset();                                                                                                                                            //Natural: RESET #FROM-TCKR
        if (condition(pnd_W2_File_Pnd_W2_Fund_Code_1.equals("T")))                                                                                                        //Natural: IF #W2-FUND-CODE-1 = 'T'
        {
            pnd_From_Tckr.setValue(pnd_Tiaa_Lit);                                                                                                                         //Natural: ASSIGN #FROM-TCKR := #TIAA-LIT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 #FUND-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Fund_Cnt)); pnd_I.nadd(1))
            {
                if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(pnd_W2_File_Pnd_W2_Fund_Code_2_3_N)))                                                               //Natural: IF +FUND-NUM-CDE ( #I ) = #W2-FUND-CODE-2-3-N
                {
                    pnd_From_Tckr.setValue(pls_Tckr_Symbl.getValue(pnd_I));                                                                                               //Natural: ASSIGN #FROM-TCKR := +TCKR-SYMBL ( #I )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_From_Tckr.equals(" ")))                                                                                                                         //Natural: IF #FROM-TCKR = ' '
        {
            pnd_Return_Cd.setValue("E1");                                                                                                                                 //Natural: ASSIGN #RETURN-CD := 'E1'
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "INVALID ACCT CODE",pnd_W2_File_Pnd_W2_Fund_Code);                                                                                      //Natural: WRITE 'INVALID ACCT CODE' #W2-FUND-CODE
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Fr_Ticker.setValue(pnd_From_Tckr);                                                                                                                            //Natural: ASSIGN #FR-TICKER := #FROM-TCKR
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
        sub_Move_Detail_Data();
        if (condition(Global.isEscape())) {return;}
    }
    //*  GET PRODUCT/CONTRACT TYPE
    //*  BY ACCT
    private void sub_Call_Ext_Module() throws Exception                                                                                                                   //Natural: CALL-EXT-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Residence() throws Exception                                                                                                                     //Natural: GET-RESIDENCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Res_Cde.reset();                                                                                                                                              //Natural: RESET #RES-CDE
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                      //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("FIND02")))
        {
            vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
            pnd_Res_Cde.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT PRTCPNT-RSDNCY-CDE TO #RES-CDE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Short_Name() throws Exception                                                                                                                    //Natural: GET-SHORT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol.setValue(pnd_Ws_Ticker);                                                                                                 //Natural: ASSIGN #NEC-TICKER-SYMBOL := #WS-TICKER
        vw_pnd_Ext_Fund.startDatabaseFind                                                                                                                                 //Natural: FIND #EXT-FUND WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
        (
        "FIND03",
        new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_pnd_Ext_Fund.readNextRow("FIND03")))
        {
            vw_pnd_Ext_Fund.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(pnd_Ext_Fund_Nec_Investment_Grouping_Id);                                                                                              //Natural: ASSIGN #WS-TICKER := NEC-INVESTMENT-GROUPING-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ084");                                                                                                   //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ084'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("ADS");                                                                                                      //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'ADS'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Ws_Ticker);                                                                                              //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #WS-TICKER
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TBL-SUPER1
        (
        "FIND04",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Tbl_Super1, WcType.WITH) },
        1
        );
        FIND04:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND04")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                               //Natural: ASSIGN #WS-TICKER := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Ticker_Symbol() throws Exception                                                                                                                 //Natural: GET-TICKER-SYMBOL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaan021x.class , getCurrentProcessState(), pnd_Fund_Cde, pnd_Tckr);                                                                               //Natural: CALLNAT 'IAAN021X' #FUND-CDE #TCKR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Move_Detail_Data() throws Exception                                                                                                                  //Natural: MOVE-DETAIL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_W2_File_Pnd_W2_Fund_Code_1.equals("T")))                                                                                                        //Natural: IF #W2-FUND-CODE-1 = 'T'
        {
                                                                                                                                                                          //Natural: PERFORM DETAIL-FOR-TIAA
            sub_Detail_For_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Trans_Amt.nadd(pnd_W2_File_Pnd_W2_Unit_Overpayment);                                                                                                  //Natural: ASSIGN #TOT-TRANS-AMT := #TOT-TRANS-AMT + #W2-UNIT-OVERPAYMENT
            pnd_Actl_Amt.setValue(pnd_W2_File_Pnd_W2_Unit_Overpayment);                                                                                                   //Natural: ASSIGN #ACTL-AMT := #W2-UNIT-OVERPAYMENT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALM2015
        ldaIaalctls.getPnd_Ctls_Trans_Cnt().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CTLS-TRANS-CNT
        pnd_Dx.nadd(1);                                                                                                                                                   //Natural: ADD 1 TO #DX
        if (condition(pnd_Dx.greater(50)))                                                                                                                                //Natural: IF #DX > 50
        {
            //*  CALM2015 START
            //*  #CNTRCT-PART-PPCN-NBR := #WS-PPCN
            //*  #CNTRCT-PART-PAYEE-CDE := 02
            //*  PERFORM GET-RESIDENCE
            //*  PERFORM GET-DOD
            //*  PERFORM GET-SURVIVOR-NAME
            //*  PERFORM GET-STATUS
            //*  PERFORM WRITE-HDR-RCRD-FIELDS
            //*  CALM2015 END
            ldaIatlcalm.getLama1001_Group_Pda().reset();                                                                                                                  //Natural: RESET LAMA1001.GROUP-PDA
            pnd_Dx.setValue(1);                                                                                                                                           //Natural: ASSIGN #DX := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx).setValue(pnd_Actl_Amt);                                                                            //Natural: ASSIGN DTL-TRANSACTION-AMT ( #DX ) := #ACTL-AMT
        ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Dx).setValue("PH");                                                                                   //Natural: ASSIGN DTL-TRANSACTION-TYPE ( #DX ) := 'PH'
        ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Dx).setValue("T");                                                                                         //Natural: ASSIGN DTL-TYPE-RECORD ( #DX ) := 'T'
        ldaIatlcalm.getLama1001_Dtl_Occurrence_Nbr().getValue(pnd_Dx).setValue(pnd_Dx);                                                                                   //Natural: ASSIGN DTL-OCCURRENCE-NBR ( #DX ) := #DX
        if (condition(pnd_W2_File_Pnd_W2_Fund_Code_2_3.equals(pnd_Tiaa_Fnd_Cdes.getValue("*"))))                                                                          //Natural: IF #W2-FUND-CODE-2-3 = #TIAA-FND-CDES ( * )
        {
            ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                             //Natural: ASSIGN DTL-FROM-TIAA-CONTRACT ( #DX ) := DTL-TO-TIAA-CONTRACT ( #DX ) := #W2-OVRPYMNT-PPCN-NBR
            ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                             //Natural: ASSIGN DTL-FROM-CREF-CONTRACT ( #DX ) := DTL-TO-CREF-CONTRACT ( #DX ) := #W2-OVRPYMNT-PPCN-NBR
            ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ticker.setValue(pnd_Fr_Ticker);                                                                                                                            //Natural: ASSIGN #WS-TICKER := #FR-TICKER
                                                                                                                                                                          //Natural: PERFORM GET-SHORT-NAME
        sub_Get_Short_Name();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Ticker.notEquals(" ")))                                                                                                                      //Natural: IF #WS-TICKER NE ' '
        {
            pnd_Fr_Ticker.setValue(pnd_Ws_Ticker);                                                                                                                        //Natural: ASSIGN #FR-TICKER := #WS-TICKER
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Dx).setValue(pnd_Fr_Ticker);                                                                     //Natural: ASSIGN DTL-TO-FUND-TICKER-SYMBOL ( #DX ) := #FR-TICKER
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                                       //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #W2-OVRPYMNT-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
        sub_Call_Ext_Module();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                             //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
        {
            ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                      //Natural: ASSIGN DTL-FROM-LOB ( #DX ) := DTL-TO-LOB ( #DX ) := PRD-PRODUCT-CDE ( 1 )
            ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));
            ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                              //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := DTL-TO-SUB-LOB ( #DX ) := PRD-SUB-PRODUCT-CDE ( 1 )
            ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Return_Cd.setValue("I1");                                                                                                                                 //Natural: ASSIGN #RETURN-CD := 'I1'
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                          //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                                       //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx).setValue("N");                                                                                  //Natural: ASSIGN DTL-UNLEDGER-INDICATOR ( #DX ) := 'N'
        ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Dx).setValue("N");                                                                                       //Natural: ASSIGN DTL-REVERSAL-CODE ( #DX ) := 'N'
        if (condition(pnd_W2_File_Pnd_W2_Fund_Code_1.equals(pnd_Annual.getValue("*"))))                                                                                   //Natural: IF #W2-FUND-CODE-1 = #ANNUAL ( * )
        {
            ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx).setValue("A");                                                                                  //Natural: ASSIGN DTL-SETTLEMENT-IND ( #DX ) := 'A'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx).setValue("M");                                                                                  //Natural: ASSIGN DTL-SETTLEMENT-IND ( #DX ) := 'M'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue(" ");                                                                                     //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Eor().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("X");                                            //Natural: ASSIGN #CTLS-TRANS-EOR ( #CTLS-TRANS-CNT ) := 'X'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("T");                                       //Natural: ASSIGN #CTLS-TRANS-RCRD-TYP ( #CTLS-TRANS-CNT ) := 'T'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Ctls_Seq_Num);                             //Natural: ASSIGN #CTLS-TRANS-SQNCE ( #CTLS-TRANS-CNT ) := #CTLS-SEQ-NUM
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TYP ( #CTLS-TRANS-CNT ) := DTL-TRANSACTION-TYPE ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TYP-RCRD ( #CTLS-TRANS-CNT ) := DTL-TYPE-RECORD ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt());    //Natural: ASSIGN #CTLS-TRANS-OCCRNCE-NBR ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-CNT
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                      //Natural: ASSIGN #CTLS-TRANS-RTE-BASIS ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-UNDLDGR-IND ( #CTLS-TRANS-CNT ) := DTL-UNLEDGER-INDICATOR ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-RVRSL-CDE ( #CTLS-TRANS-CNT ) := DTL-REVERSAL-CODE ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-DEPT-ID ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                     //Natural: ASSIGN #CTLS-TRANS-ALLCTN-NDX ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                     //Natural: ASSIGN #CTLS-TRANS-VLTN-BASIS ( #CTLS-TRANS-CNT ) := ' '
        if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx).less(getZero())))                                                                    //Natural: IF DTL-TRANSACTION-AMT ( #DX ) < 0
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("-");                                   //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) := DTL-TRANSACTION-AMT ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-TKR ( #CTLS-TRANS-CNT ) := DTL-FROM-FUND-TICKER-SYMBOL ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-LOB ( #CTLS-TRANS-CNT ) := DTL-FROM-LOB ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT ) := DTL-FROM-SUB-LOB ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT ) := DTL-FROM-TIAA-CONTRACT ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT ) := DTL-FROM-CREF-CONTRACT ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                   //Natural: ASSIGN #CTLS-TRANS-FROM-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-TKR ( #CTLS-TRANS-CNT ) := DTL-TO-FUND-TICKER-SYMBOL ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-LOB ( #CTLS-TRANS-CNT ) := DTL-TO-LOB ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := DTL-TO-SUB-LOB ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := DTL-TO-TIAA-CONTRACT ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := DTL-TO-CREF-CONTRACT ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                     //Natural: ASSIGN #CTLS-TRANS-TO-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-PITS-RQST-ID ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                       //Natural: ASSIGN #CTLS-TRANS-LDGR-TYP ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                      //Natural: ASSIGN #CTLS-TRANS-CNCL-RDRW ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-IND2 ( #CTLS-TRANS-CNT ) := DTL-SETTLEMENT-IND2 ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-IND ( #CTLS-TRANS-CNT ) := DTL-SETTLEMENT-IND ( #DX )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-TAX-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                   //Natural: ASSIGN #CTLS-TRANS-NET-CASH-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                                    //Natural: ASSIGN #CTLS-TRANS-INSTLLMNT-DTE ( #CTLS-TRANS-CNT ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                //Natural: ASSIGN #CTLS-TRANS-1ST-YR-RNWL-IND ( #CTLS-TRANS-CNT ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                        //Natural: ASSIGN #CTLS-TRANS-SUBPLAN ( #CTLS-TRANS-CNT ) := ' '
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Amt_Ttl().nadd(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ADD #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) TO #CTLS-TRLR-DTL-AMT-TTL
        //*  CALM2015 END
    }
    private void sub_Detail_For_Tiaa() throws Exception                                                                                                                   //Natural: DETAIL-FOR-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tot_Trans_Amt.compute(new ComputeParameters(false, pnd_Tot_Trans_Amt), pnd_Tot_Trans_Amt.add(pnd_W2_File_Pnd_W2_Guar_Overpayment).add(pnd_W2_File_Pnd_W2_Divd_Overpayment)); //Natural: ASSIGN #TOT-TRANS-AMT := #TOT-TRANS-AMT + #W2-GUAR-OVERPAYMENT + #W2-DIVD-OVERPAYMENT
        pnd_X.reset();                                                                                                                                                    //Natural: RESET #X
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
            if (condition(pnd_X.greater(2)))                                                                                                                              //Natural: IF #X GT 2
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_X.equals(1)))                                                                                                                               //Natural: IF #X = 1
            {
                pnd_Actl_Amt.setValue(pnd_W2_File_Pnd_W2_Guar_Overpayment);                                                                                               //Natural: ASSIGN #ACTL-AMT := #W2-GUAR-OVERPAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Actl_Amt.setValue(pnd_W2_File_Pnd_W2_Divd_Overpayment);                                                                                               //Natural: ASSIGN #ACTL-AMT := #W2-DIVD-OVERPAYMENT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Actl_Amt.greater(getZero())))                                                                                                               //Natural: IF #ACTL-AMT GT 0
            {
                //*  CALM2015
                ldaIaalctls.getPnd_Ctls_Trans_Cnt().nadd(1);                                                                                                              //Natural: ADD 1 TO #CTLS-TRANS-CNT
                pnd_Dx.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #DX
                if (condition(pnd_Dx.greater(50)))                                                                                                                        //Natural: IF #DX > 50
                {
                    //*      #CNTRCT-PART-PPCN-NBR := #WS-PPCN/* CALM2015 START
                    //*      #CNTRCT-PART-PAYEE-CDE := 02
                    //*      PERFORM GET-RESIDENCE
                    //*      PERFORM GET-DOD
                    //*      PERFORM GET-SURVIVOR-NAME
                    //*      PERFORM GET-STATUS
                    //*      PERFORM WRITE-HDR-RCRD-FIELDS    /* CALM2015 END
                    ldaIatlcalm.getLama1001_Group_Pda().reset();                                                                                                          //Natural: RESET LAMA1001.GROUP-PDA
                    pnd_Dx.setValue(1);                                                                                                                                   //Natural: ASSIGN #DX := 1
                }                                                                                                                                                         //Natural: END-IF
                ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx).setValue(pnd_Actl_Amt);                                                                    //Natural: ASSIGN DTL-TRANSACTION-AMT ( #DX ) := #ACTL-AMT
                ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Dx).setValue("PH");                                                                           //Natural: ASSIGN DTL-TRANSACTION-TYPE ( #DX ) := 'PH'
                ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Dx).setValue("T");                                                                                 //Natural: ASSIGN DTL-TYPE-RECORD ( #DX ) := 'T'
                ldaIatlcalm.getLama1001_Dtl_Occurrence_Nbr().getValue(pnd_Dx).setValue(pnd_Dx);                                                                           //Natural: ASSIGN DTL-OCCURRENCE-NBR ( #DX ) := #DX
                ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                         //Natural: ASSIGN DTL-FROM-TIAA-CONTRACT ( #DX ) := DTL-TO-TIAA-CONTRACT ( #DX ) := #W2-OVRPYMNT-PPCN-NBR
                ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx).setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);
                ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Dx).setValue(pnd_Fr_Ticker);                                                             //Natural: ASSIGN DTL-TO-FUND-TICKER-SYMBOL ( #DX ) := #FR-TICKER
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000
                pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(pnd_W2_File_Pnd_W2_Ovrpymnt_Ppcn_Nbr);                                                               //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #W2-OVRPYMNT-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
                sub_Call_Ext_Module();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                     //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
                {
                    ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                              //Natural: ASSIGN DTL-FROM-LOB ( #DX ) := DTL-TO-LOB ( #DX ) := PRD-PRODUCT-CDE ( 1 )
                    ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));
                    ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                      //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := DTL-TO-SUB-LOB ( #DX ) := PRD-SUB-PRODUCT-CDE ( 1 )
                    ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Return_Cd.setValue("I1");                                                                                                                         //Natural: ASSIGN #RETURN-CD := 'I1'
                    getReports().write(0, Global.getPROGRAM());                                                                                                           //Natural: WRITE *PROGRAM
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "ERROR FROM EXTERNALIZATION FOR",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr());                                                  //Natural: WRITE 'ERROR FROM EXTERNALIZATION FOR' NECA4000.PRD-KEY4-ACCT-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "RC=",pdaNeca4000.getNeca4000_Return_Cde(),pdaNeca4000.getNeca4000_Return_Msg());                                               //Natural: WRITE 'RC=' NECA4000.RETURN-CDE NECA4000.RETURN-MSG
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1945 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W2-FUND-CODE-2-3 = '1G'
                if (condition(pnd_W2_File_Pnd_W2_Fund_Code_2_3.equals("1G")))
                {
                    decideConditionsMet1945++;
                    ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Grdd_Sub_Lob);                                                               //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := DTL-TO-SUB-LOB ( #DX ) := #GRDD-SUB-LOB
                    ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Grdd_Sub_Lob);
                    ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx).setValue("G");                                                                          //Natural: ASSIGN DTL-SETTLEMENT-IND ( #DX ) := 'G'
                }                                                                                                                                                         //Natural: WHEN #W2-FUND-CODE-2-3 = '1S'
                else if (condition(pnd_W2_File_Pnd_W2_Fund_Code_2_3.equals("1S")))
                {
                    decideConditionsMet1945++;
                    ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Stdd_Sub_Lob);                                                               //Natural: ASSIGN DTL-FROM-SUB-LOB ( #DX ) := DTL-TO-SUB-LOB ( #DX ) := #STDD-SUB-LOB
                    ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx).setValue(pnd_Stdd_Sub_Lob);
                    ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx).setValue("S");                                                                          //Natural: ASSIGN DTL-SETTLEMENT-IND ( #DX ) := 'S'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx).setValue("N");                                                                          //Natural: ASSIGN DTL-UNLEDGER-INDICATOR ( #DX ) := 'N'
                ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Dx).setValue("N");                                                                               //Natural: ASSIGN DTL-REVERSAL-CODE ( #DX ) := 'N'
                //*  GUARANTEED
                if (condition(pnd_X.equals(1)))                                                                                                                           //Natural: IF #X = 1
                {
                    ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue("G");                                                                         //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := 'G'
                    //*  DIVIDEND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx).setValue("D");                                                                         //Natural: ASSIGN DTL-SETTLEMENT-IND2 ( #DX ) := 'D'
                }                                                                                                                                                         //Natural: END-IF
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Eor().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("X");                                    //Natural: ASSIGN #CTLS-TRANS-EOR ( #CTLS-TRANS-CNT ) := 'X'
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("T");                               //Natural: ASSIGN #CTLS-TRANS-RCRD-TYP ( #CTLS-TRANS-CNT ) := 'T'
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(pnd_Ctls_Seq_Num);                     //Natural: ASSIGN #CTLS-TRANS-SQNCE ( #CTLS-TRANS-CNT ) := #CTLS-SEQ-NUM
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TYP ( #CTLS-TRANS-CNT ) := DTL-TRANSACTION-TYPE ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TYP-RCRD ( #CTLS-TRANS-CNT ) := DTL-TYPE-RECORD ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()); //Natural: ASSIGN #CTLS-TRANS-OCCRNCE-NBR ( #CTLS-TRANS-CNT ) := #CTLS-TRANS-CNT
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                              //Natural: ASSIGN #CTLS-TRANS-RTE-BASIS ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-UNDLDGR-IND ( #CTLS-TRANS-CNT ) := DTL-UNLEDGER-INDICATOR ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-RVRSL-CDE ( #CTLS-TRANS-CNT ) := DTL-REVERSAL-CODE ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                //Natural: ASSIGN #CTLS-TRANS-DEPT-ID ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                             //Natural: ASSIGN #CTLS-TRANS-ALLCTN-NDX ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                             //Natural: ASSIGN #CTLS-TRANS-VLTN-BASIS ( #CTLS-TRANS-CNT ) := ' '
                if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx).less(getZero())))                                                            //Natural: IF DTL-TRANSACTION-AMT ( #DX ) < 0
                {
                    ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue("-");                           //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := '-'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                           //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #CTLS-TRANS-CNT ) := ' '
                }                                                                                                                                                         //Natural: END-IF
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) := DTL-TRANSACTION-AMT ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-TKR ( #CTLS-TRANS-CNT ) := DTL-FROM-FUND-TICKER-SYMBOL ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-LOB ( #CTLS-TRANS-CNT ) := DTL-FROM-LOB ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #CTLS-TRANS-CNT ) := DTL-FROM-SUB-LOB ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-TIAA-NO ( #CTLS-TRANS-CNT ) := DTL-FROM-TIAA-CONTRACT ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-FROM-CREF-NO ( #CTLS-TRANS-CNT ) := DTL-FROM-CREF-CONTRACT ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                           //Natural: ASSIGN #CTLS-TRANS-FROM-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-TKR ( #CTLS-TRANS-CNT ) := DTL-TO-FUND-TICKER-SYMBOL ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-LOB ( #CTLS-TRANS-CNT ) := DTL-TO-LOB ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #CTLS-TRANS-CNT ) := DTL-TO-SUB-LOB ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #CTLS-TRANS-CNT ) := DTL-TO-TIAA-CONTRACT ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #CTLS-TRANS-CNT ) := DTL-TO-CREF-CONTRACT ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                             //Natural: ASSIGN #CTLS-TRANS-TO-NUM-UNITS ( #CTLS-TRANS-CNT ) := 0
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                           //Natural: ASSIGN #CTLS-TRANS-PITS-RQST-ID ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                               //Natural: ASSIGN #CTLS-TRANS-LDGR-TYP ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                              //Natural: ASSIGN #CTLS-TRANS-CNCL-RDRW ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-IND2 ( #CTLS-TRANS-CNT ) := DTL-SETTLEMENT-IND2 ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Dx)); //Natural: ASSIGN #CTLS-TRANS-IND ( #CTLS-TRANS-CNT ) := DTL-SETTLEMENT-IND ( #DX )
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                //Natural: ASSIGN #CTLS-TRANS-TAX-IND ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                           //Natural: ASSIGN #CTLS-TRANS-NET-CASH-IND ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(0);                            //Natural: ASSIGN #CTLS-TRANS-INSTLLMNT-DTE ( #CTLS-TRANS-CNT ) := 0
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                        //Natural: ASSIGN #CTLS-TRANS-1ST-YR-RNWL-IND ( #CTLS-TRANS-CNT ) := ' '
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt()).setValue(" ");                                //Natural: ASSIGN #CTLS-TRANS-SUBPLAN ( #CTLS-TRANS-CNT ) := ' '
                pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Amt_Ttl().nadd(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); //Natural: ADD #CTLS-TRANS-AMT ( #CTLS-TRANS-CNT ) TO #CTLS-TRLR-DTL-AMT-TTL
                //*  CALM2015 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Dod() throws Exception                                                                                                                           //Natural: GET-DOD
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Ws_Dod.reset();                                                                                                                                               //Natural: RESET #WS-DOD
        vw_iaa_Sttlmnt.startDatabaseRead                                                                                                                                  //Natural: READ IAA-STTLMNT BY PIN-TAXID-SEQ-KEY STARTING FROM #WS-PIN-KEY
        (
        "READ03",
        new Wc[] { new Wc("PIN_TAXID_SEQ_KEY", ">=", pnd_Ws_Pin_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Sttlmnt.readNextRow("READ03")))
        {
            if (condition(iaa_Sttlmnt_Sttlmnt_Id_Nbr.notEquals(pnd_Ws_Pin_Key_Pnd_Ws_Pin)))                                                                               //Natural: IF STTLMNT-ID-NBR NE #WS-PIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Sttlmnt_Sttlmnt_Process_Type.equals("ID1A")))                                                                                               //Natural: IF STTLMNT-PROCESS-TYPE = 'ID1A'
            {
                pnd_Ws_Dod.setValue(iaa_Sttlmnt_Sttlmnt_Dod_Dte);                                                                                                         //Natural: ASSIGN #WS-DOD := STTLMNT-DOD-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Dod.setValue(iaa_Sttlmnt_Sttlmnt_2nd_Dod_Dte);                                                                                                     //Natural: ASSIGN #WS-DOD := STTLMNT-2ND-DOD-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Status() throws Exception                                                                                                                        //Natural: GET-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Ws_Status.reset();                                                                                                                                            //Natural: RESET #WS-STATUS
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseFind                                                                                                                      //Natural: FIND IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "FIND05",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) }
        );
        FIND05:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("FIND05")))
        {
            vw_iaa_Cntrct_Prtcpnt_Role.setIfNotFoundControlFlag(false);
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(1)))                                                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 1
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.notEquals("0")))                                                                                    //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE NE '0'
                {
                    pnd_Ws_Status.setValue("PENDED");                                                                                                                     //Natural: ASSIGN #WS-STATUS := 'PENDED'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Status.setValue("ACTIVE");                                                                                                                     //Natural: ASSIGN #WS-STATUS := 'ACTIVE'
                }                                                                                                                                                         //Natural: END-IF
                //*  ELSE
                //*    #WS-STATUS := 'TERMINATED'   /* SHOULD NOT HAPPEN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Survivor_Name() throws Exception                                                                                                                 //Natural: GET-SURVIVOR-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Ws_Name.reset();                                                                                                                                              //Natural: RESET #WS-NAME
        //*  082017
        vw_iaa_Bene.startDatabaseRead                                                                                                                                     //Natural: READ IAA-BENE BY PIN-TAXID-BNFCRY-KEY STARTING FROM #PIN-TAXID-BNFCRY-KEY
        (
        "READ04",
        new Wc[] { new Wc("PIN_TAXID_BNFCRY_KEY", ">=", pnd_Pin_Taxid_Bnfcry_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_BNFCRY_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Bene.readNextRow("READ04")))
        {
            if (condition(iaa_Bene_Bnfcry_Id_Nbr.notEquals(pnd_Ws_Pin_Key_Pnd_Ws_Pin)))                                                                                   //Natural: IF BNFCRY-ID-NBR NE #WS-PIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Name.setValue(DbsUtil.compress(iaa_Bene_Bnfcry_Name_Last.getValue(1), iaa_Bene_Bnfcry_Name_First.getValue(1), iaa_Bene_Bnfcry_Name_Middle.getValue(1).getSubstring(1, //Natural: COMPRESS BNFCRY-NAME-LAST ( 1 ) BNFCRY-NAME-FIRST ( 1 ) SUBSTR ( BNFCRY-NAME-MIDDLE ( 1 ) ,1,1 ) INTO #WS-NAME
                1)));
            if (condition(pnd_Ws_Name.equals(" ")))                                                                                                                       //Natural: IF #WS-NAME = ' '
            {
                pnd_Ws_Name.setValue(iaa_Bene_Bnfcry_Name_Free.getValue(1));                                                                                              //Natural: ASSIGN #WS-NAME := BNFCRY-NAME-FREE ( 1 )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Name), new ExamineSearch(","), new ExamineReplace(" "));                                                         //Natural: EXAMINE #WS-NAME FOR ',' REPLACE WITH ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Hdr_Rcrd_Fields() throws Exception                                                                                                             //Natural: WRITE-HDR-RCRD-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
                                                                                                                                                                          //Natural: PERFORM WRITE-HDR-DATA
        sub_Write_Hdr_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-FIELDS
            sub_Write_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALM2015 START
        //* *CALLNAT 'LAMN1400' LAMA1001 /* REPLACED WITH LAMN1500 05/23/14
        //* *CALLNAT 'LAMN1500' LAMA1001   /* NEW CALM INTERFACE     05/23/14
        //* *IF LAMA1001.RETURN-CDE NE ' '
        //* *  MOVE 'E6' TO #STTS-CDE
        //* *  PERFORM SET-STTS-CDS
        //* *ELSE
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORTS
        sub_Write_Reports();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF
        //*  CALM2015 END
    }
    private void sub_Write_Hdr_Data() throws Exception                                                                                                                    //Natural: WRITE-HDR-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaIatlcalm.getLama1001_Hdr_Total_Transaction_Amt().setValue(pnd_Tot_Trans_Amt);                                                                                  //Natural: ASSIGN HDR-TOTAL-TRANSACTION-AMT := #TOT-TRANS-AMT
        ldaIatlcalm.getLama1001_Hdr_Origin_System().setValue("PHRCV");                                                                                                    //Natural: ASSIGN HDR-ORIGIN-SYSTEM := 'PHRCV'
        ldaIatlcalm.getLama1001_Hdr_Record_Type().setValue("H ");                                                                                                         //Natural: ASSIGN HDR-RECORD-TYPE := 'H '
        ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp().setValue(Global.getDATN());                                                                                     //Natural: ASSIGN HDR-CREATION-DATE-STAMP := *DATN
        ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp().setValue(Global.getTIMN());                                                                                     //Natural: ASSIGN HDR-CREATION-TIME-STAMP := *TIMN
        ldaIatlcalm.getLama1001_Hdr_Journal_Date().setValue(pnd_Todays_Dte);                                                                                              //Natural: ASSIGN HDR-JOURNAL-DATE := #TODAYS-DTE
        ldaIatlcalm.getLama1001_Hdr_Participation_Date().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                                 //Natural: ASSIGN HDR-PARTICIPATION-DATE := #DTE
        ldaIatlcalm.getLama1001_Hdr_Pin_Number().setValue(pnd_Ws_Pin_Key_Pnd_Ws_Pin_A);                                                                                   //Natural: ASSIGN HDR-PIN-NUMBER := #WS-PIN-A
        ldaIatlcalm.getLama1001_Hdr_Record_Flow_Status().setValue("O");                                                                                                   //Natural: ASSIGN HDR-RECORD-FLOW-STATUS := 'O'
        ldaIatlcalm.getLama1001_Hdr_State_Code().setValue(pnd_Res_Cde);                                                                                                   //Natural: ASSIGN HDR-STATE-CODE := #RES-CDE
        ldaIatlcalm.getLama1001_Hdr_Name().setValue(pnd_Ws_Name);                                                                                                         //Natural: ASSIGN HDR-NAME := #WS-NAME
        ldaIatlcalm.getLama1001_Hdr_Schedule_Date().setValue(pnd_Ws_Dod);                                                                                                 //Natural: ASSIGN HDR-SCHEDULE-DATE := #WS-DOD
        ldaIatlcalm.getLama1001_Hdr_Cca_Id().setValue(pnd_Ws_Status);                                                                                                     //Natural: ASSIGN HDR-CCA-ID := #WS-STATUS
        pnd_Dx.reset();                                                                                                                                                   //Natural: RESET #DX
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Eor().setValue("X");                                                                                              //Natural: ASSIGN #CTLS-HDR-EOR := 'X'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Typ().setValue(ldaIatlcalm.getLama1001_Hdr_Record_Type());                                                   //Natural: ASSIGN #CTLS-HDR-RCRD-TYP := HDR-RECORD-TYPE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sqnce().setValue(pnd_Ctls_Seq_Num);                                                                               //Natural: ASSIGN #CTLS-HDR-SQNCE := #CTLS-SEQ-NUM
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Jrnl_Dte().setValue(ldaIatlcalm.getLama1001_Hdr_Journal_Date());                                                  //Natural: ASSIGN #CTLS-HDR-JRNL-DTE := HDR-JOURNAL-DATE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Orgn_Sys().setValue(ldaIatlcalm.getLama1001_Hdr_Origin_System());                                                 //Natural: ASSIGN #CTLS-HDR-ORGN-SYS := HDR-ORIGIN-SYSTEM
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Flow_Stts().setValue(ldaIatlcalm.getLama1001_Hdr_Record_Flow_Status());                                      //Natural: ASSIGN #CTLS-HDR-RCRD-FLOW-STTS := HDR-RECORD-FLOW-STATUS
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Dte_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp());                                      //Natural: ASSIGN #CTLS-HDR-CRTE-DTE-STMP := HDR-CREATION-DATE-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Tme_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp());                                      //Natural: ASSIGN #CTLS-HDR-CRTE-TME-STMP := HDR-CREATION-TIME-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Dte_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp());                                      //Natural: ASSIGN #CTLS-HDR-RCVD-DTE-STMP := HDR-CREATION-DATE-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Tme_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp());                                      //Natural: ASSIGN #CTLS-HDR-RCVD-TME-STMP := HDR-CREATION-TIME-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Dte_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp());                                     //Natural: ASSIGN #CTLS-HDR-PRCSS-DTE-STMP := HDR-CREATION-DATE-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Tme_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp());                                     //Natural: ASSIGN #CTLS-HDR-PRCSS-TME-STMP := HDR-CREATION-TIME-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Dte_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp());                                //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-DTE-STMP := HDR-CREATION-DATE-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Tme_Stmp().setValue(ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp());                                //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-TME-STMP := HDR-CREATION-TIME-STAMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Nme().setValue(ldaIatlcalm.getLama1001_Hdr_Name());                                                            //Natural: ASSIGN #CTLS-HDR-PH-NME := HDR-NAME
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Schdl_Dte().setValue(ldaIatlcalm.getLama1001_Hdr_Schedule_Date());                                                //Natural: ASSIGN #CTLS-HDR-SCHDL-DTE := HDR-SCHEDULE-DATE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Csh_Cntrl_Acct_Id().setValue(ldaIatlcalm.getLama1001_Hdr_Cca_Id());                                               //Natural: ASSIGN #CTLS-HDR-CSH-CNTRL-ACCT-ID := HDR-CCA-ID
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Intrfce_Dte().setValue(0);                                                                                        //Natural: ASSIGN #CTLS-HDR-INTRFCE-DTE := 0
        if (condition(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().less(getZero())))                                                                    //Natural: IF #CTLS-HDR-TTL-TRNS-AMT < 0
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue("-");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue(" ");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().setValue(ldaIatlcalm.getLama1001_Hdr_Total_Transaction_Amt());                                     //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT := HDR-TOTAL-TRANSACTION-AMT
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Part_Dte().setValue(ldaIatlcalm.getLama1001_Hdr_Participation_Date());                                            //Natural: ASSIGN #CTLS-HDR-PART-DTE := HDR-PARTICIPATION-DATE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Num().setValue(ldaIatlcalm.getLama1001_Hdr_Pin_Number());                                                      //Natural: ASSIGN #CTLS-HDR-PH-NUM := HDR-PIN-NUMBER
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_St_Cde().setValue(ldaIatlcalm.getLama1001_Hdr_State_Code());                                                      //Natural: ASSIGN #CTLS-HDR-ST-CDE := HDR-STATE-CODE
        getWorkFiles().write(4, false, ldaIaalctls.getPnd_Ctls_Hdr_Record());                                                                                             //Natural: WRITE WORK FILE 4 #CTLS-HDR-RECORD
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Rcrd_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #CTLS-TRLR-HDR-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #CTLS-TRLR-TTL-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Amt_Ttl().nadd(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt());                             //Natural: ADD #CTLS-HDR-TTL-TRNS-AMT TO #CTLS-TRLR-HDR-AMT-TTL
        //*  CALM2015 END
    }
    private void sub_Set_Stts_Cds() throws Exception                                                                                                                      //Natural: SET-STTS-CDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pnd_Return_Cd.setValue(pnd_Stts_Cde);                                                                                                                             //Natural: MOVE #STTS-CDE TO #RETURN-CD
        getReports().write(0, Global.getPROGRAM());                                                                                                                       //Natural: WRITE *PROGRAM
        if (Global.isEscape()) return;
        getReports().write(0, "Bad return from LAMN1400");                                                                                                                //Natural: WRITE 'Bad return from LAMN1400'
        if (Global.isEscape()) return;
        getReports().write(0, "RC=",ldaIatlcalm.getLama1001_Return_Cde(),ldaIatlcalm.getLama1001_Return_Msg());                                                           //Natural: WRITE 'RC=' LAMA1001.RETURN-CDE LAMA1001.RETURN-MSG
        if (Global.isEscape()) return;
    }
    private void sub_Write_Fields() throws Exception                                                                                                                      //Natural: WRITE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "HEADER RECORD FIELDS");                                                                                                                    //Natural: WRITE 'HEADER RECORD FIELDS'
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaIatlcalm.getLama1001_More_Data(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Total_Transaction_Amt(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Origin_System(), //Natural: WRITE '=' MORE-DATA / '=' HDR-TOTAL-TRANSACTION-AMT / '=' HDR-ORIGIN-SYSTEM / '=' HDR-RECORD-TYPE / '=' HDR-CREATION-DATE-STAMP / '=' HDR-CREATION-TIME-STAMP / '=' HDR-JOURNAL-DATE / '=' HDR-PARTICIPATION-DATE / '=' HDR-PIN-NUMBER / '=' HDR-STATE-CODE / '=' HDR-NAME / '=' HDR-SCHEDULE-DATE / '=' HDR-CCA-ID / '=' HDR-RECORD-FLOW-STATUS
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Record_Type(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Creation_Date_Stamp(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Creation_Time_Stamp(),
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Journal_Date(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Participation_Date(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Pin_Number(),
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_State_Code(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Name(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Schedule_Date(),
            NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Cca_Id(),NEWLINE,"=",ldaIatlcalm.getLama1001_Hdr_Record_Flow_Status());
        if (Global.isEscape()) return;
        //*  CALM2015
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        getReports().write(0, NEWLINE,"DETAIL RECORD FIELDS");                                                                                                            //Natural: WRITE / 'DETAIL RECORD FIELDS'
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #Y 1 50
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(50)); pnd_Y.nadd(1))
        {
            if (condition(ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y).equals(" ")))                                                                    //Natural: IF DTL-TRANSACTION-TYPE ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",ldaIatlcalm.getLama1001_Dtl_Transaction_Amt().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Transaction_Type().getValue(pnd_Y), //Natural: WRITE '=' DTL-TRANSACTION-AMT ( #Y ) / '=' DTL-TRANSACTION-TYPE ( #Y ) / '=' DTL-TYPE-RECORD ( #Y ) / '=' DTL-OCCURRENCE-NBR ( #Y ) / '=' DTL-FROM-TIAA-CONTRACT ( #Y ) / '=' DTL-FROM-CREF-CONTRACT ( #Y ) / '=' DTL-FROM-FUND-TICKER-SYMBOL ( #Y ) / '=' DTL-FROM-LOB ( #Y ) / '=' DTL-FROM-SUB-LOB ( #Y ) / '=' DTL-TO-TIAA-CONTRACT ( #Y ) / '=' DTL-TO-CREF-CONTRACT ( #Y ) / '=' DTL-TO-FUND-TICKER-SYMBOL ( #Y ) / '=' DTL-TO-LOB ( #Y ) / '=' DTL-TO-SUB-LOB ( #Y ) / '=' DTL-UNLEDGER-INDICATOR ( #Y ) / '=' DTL-REVERSAL-CODE ( #Y ) / '=' DTL-SETTLEMENT-IND ( #Y ) / '=' DTL-SETTLEMENT-IND2 ( #Y )
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Type_Record().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Occurrence_Nbr().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Tiaa_Contract().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Cref_Contract().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Fund_Ticker_Symbol().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Lob().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_From_Sub_Lob().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Tiaa_Contract().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Cref_Contract().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Fund_Ticker_Symbol().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Lob().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_To_Sub_Lob().getValue(pnd_Y),NEWLINE,
                "=",ldaIatlcalm.getLama1001_Dtl_Unledger_Indicator().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Reversal_Code().getValue(pnd_Y),
                NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Settlement_Ind().getValue(pnd_Y),NEWLINE,"=",ldaIatlcalm.getLama1001_Dtl_Settlement_Ind2().getValue(pnd_Y));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Reports() throws Exception                                                                                                                     //Natural: WRITE-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Report_Detail.reset();                                                                                                                                        //Natural: RESET #REPORT-DETAIL
        pnd_W_Date.setValue(ldaIatlcalm.getLama1001_Hdr_Journal_Date());                                                                                                  //Natural: ASSIGN #W-DATE := HDR-JOURNAL-DATE
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date_Pnd_W_Date_A);                                                                                  //Natural: MOVE EDITED #W-DATE-A TO #DATD ( EM = YYYYMMDD )
        pnd_T_Hdr2_Pnd_Thdr_Jdate.setValueEdited(pnd_Datd,new ReportEditMask("MM/DD/YYYY"));                                                                              //Natural: MOVE EDITED #DATD ( EM = MM/DD/YYYY ) TO #THDR-JDATE
        FOR03:                                                                                                                                                            //Natural: FOR #Y 1 #CTLS-TRANS-CNT
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); pnd_Y.nadd(1))
        {
            //*  IF DTL-TRANSACTION-TYPE (#Y) = ' '
            //*    ESCAPE BOTTOM
            //*  END-IF
            //*  IF DTL-TO-TIAA-CONTRACT (#Y) NE ' '
            //*    #TO-CONTRACT := DTL-TO-TIAA-CONTRACT (#Y)
            //*  ELSE
            //*    #TO-CONTRACT := DTL-TO-CREF-CONTRACT (#Y)
            //*  END-IF
            if (condition(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Y).notEquals(" ")))                                               //Natural: IF #CTLS-TRANS-TO-TIAA-NO ( #Y ) NE ' '
            {
                pnd_Report_Detail_Pnd_To_Contract.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Y));                             //Natural: ASSIGN #TO-CONTRACT := #CTLS-TRANS-TO-TIAA-NO ( #Y )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Report_Detail_Pnd_To_Contract.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Y));                             //Natural: ASSIGN #TO-CONTRACT := #CTLS-TRANS-TO-CREF-NO ( #Y )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Report_Detail_Pnd_Ctls_To_Ticker.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Y));                                  //Natural: ASSIGN #CTLS-TO-TICKER := #CTLS-TRANS-TO-TKR ( #Y )
            pnd_Report_Detail_Pnd_Trans_Amt.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Y));                                          //Natural: ASSIGN #TRANS-AMT := #CTLS-TRANS-AMT ( #Y )
            pnd_Tot_Amt.nadd(pnd_Report_Detail_Pnd_Trans_Amt);                                                                                                            //Natural: ASSIGN #TOT-AMT := #TOT-AMT + #TRANS-AMT
            //*  IF DTL-UNLEDGER-INDICATOR (#Y) = 'N'
            if (condition(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Y).equals("N")))                                                 //Natural: IF #CTLS-TRANS-UNDLDGR-IND ( #Y ) = 'N'
            {
                pnd_Report_Detail_Pnd_Ldgr_Ind.setValue("Y");                                                                                                             //Natural: ASSIGN #LDGR-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Report_Detail_Pnd_Ldgr_Ind.setValue("N");                                                                                                             //Natural: ASSIGN #LDGR-IND := 'N'
                //*  082814
            }                                                                                                                                                             //Natural: END-IF
            pnd_Report_Detail_Pnd_Ind1.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Y));                                               //Natural: ASSIGN #IND1 := #CTLS-TRANS-IND ( #Y )
            pnd_Report_Detail_Pnd_Ind2.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Y));                                              //Natural: ASSIGN #IND2 := #CTLS-TRANS-IND2 ( #Y )
            pnd_Report_Detail_Pnd_Part_Date.setValue(pnd_Dte_Alpha);                                                                                                      //Natural: ASSIGN #PART-DATE := #DTE-ALPHA
            getReports().write(1, ReportOption.NOTITLE,pnd_Report_Detail_Pnd_To_Contract,pnd_Report_Detail_Pnd_F4,pnd_Report_Detail_Pnd_Ctls_To_Ticker,                   //Natural: WRITE ( 1 ) #REPORT-DETAIL
                pnd_Report_Detail_Pnd_F6,pnd_Report_Detail_Pnd_Trans_Amt,pnd_Report_Detail_Pnd_F7,pnd_Report_Detail_Pnd_Ldgr_Ind,pnd_Report_Detail_Pnd_F8,
                pnd_Report_Detail_Pnd_Ind1,pnd_Report_Detail_Pnd_F9,pnd_Report_Detail_Pnd_Ind2,pnd_Report_Detail_Pnd_Part_Date);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Ctls_Transactions() throws Exception                                                                                                           //Natural: WRITE-CTLS-TRANSACTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, NEWLINE,"DETAIL RECORD FIELDS");                                                                                                        //Natural: WRITE / 'DETAIL RECORD FIELDS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #Z 1 #CTLS-TRANS-CNT
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); pnd_Z.nadd(1))
        {
            pnd_Detail_Record_Pnd_Trans_Rcrd_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-RCRD-TYP := #CTLS-TRANS-RCRD-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Sqnce.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_Z));                                      //Natural: ASSIGN #TRANS-SQNCE := #CTLS-TRANS-SQNCE ( #Z )
            pnd_Detail_Record_Pnd_Trans_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Z));                                          //Natural: ASSIGN #TRANS-TYP := #CTLS-TRANS-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Typ_Rcrd.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-TYP-RCRD := #CTLS-TRANS-TYP-RCRD ( #Z )
            pnd_Detail_Record_Pnd_Trans_Occrnce_Nbr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_Z));                          //Natural: ASSIGN #TRANS-OCCRNCE-NBR := #CTLS-TRANS-OCCRNCE-NBR ( #Z )
            pnd_Detail_Record_Pnd_Trans_Rte_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-RTE-BASIS := #CTLS-TRANS-RTE-BASIS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Undldgr_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Z));                          //Natural: ASSIGN #TRANS-UNDLDGR-IND := #CTLS-TRANS-UNDLDGR-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Rvrsl_Cde.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-RVRSL-CDE := #CTLS-TRANS-RVRSL-CDE ( #Z )
            pnd_Detail_Record_Pnd_Trans_Dept_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-DEPT-ID := #CTLS-TRANS-DEPT-ID ( #Z )
            pnd_Detail_Record_Pnd_Trans_Allctn_Ndx.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-ALLCTN-NDX := #CTLS-TRANS-ALLCTN-NDX ( #Z )
            pnd_Detail_Record_Pnd_Trans_Vltn_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-VLTN-BASIS := #CTLS-TRANS-VLTN-BASIS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Amt_Sign.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-AMT-SIGN := #CTLS-TRANS-AMT-SIGN ( #Z )
            pnd_Detail_Record_Pnd_Trans_Dtl_Amt.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Z));                                      //Natural: ASSIGN #TRANS-DTL-AMT := #CTLS-TRANS-AMT ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-FROM-TKR := #CTLS-TRANS-FROM-TKR ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-FROM-LOB := #CTLS-TRANS-FROM-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-SUB-LOB := #CTLS-TRANS-FROM-SUB-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-TIAA-NO := #CTLS-TRANS-FROM-TIAA-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-FROM-CREF-NO := #CTLS-TRANS-FROM-CREF-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_From_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_Z));                    //Natural: ASSIGN #TRANS-FROM-NUM-UNITS := #CTLS-TRANS-FROM-NUM-UNITS ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Z));                                    //Natural: ASSIGN #TRANS-TO-TKR := #CTLS-TRANS-TO-TKR ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_Z));                                    //Natural: ASSIGN #TRANS-TO-LOB := #CTLS-TRANS-TO-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-SUB-LOB := #CTLS-TRANS-TO-SUB-LOB ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-TIAA-NO := #CTLS-TRANS-TO-TIAA-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Z));                            //Natural: ASSIGN #TRANS-TO-CREF-NO := #CTLS-TRANS-TO-CREF-NO ( #Z )
            pnd_Detail_Record_Pnd_Trans_To_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-TO-NUM-UNITS := #CTLS-TRANS-TO-NUM-UNITS ( #Z )
            pnd_Detail_Record_Pnd_Trans_Pits_Rqst_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-PITS-RQST-ID := #CTLS-TRANS-PITS-RQST-ID ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ldgr_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_Z));                                //Natural: ASSIGN #TRANS-LDGR-TYP := #CTLS-TRANS-LDGR-TYP ( #Z )
            pnd_Detail_Record_Pnd_Trans_Cncl_Rdrw.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_Z));                              //Natural: ASSIGN #TRANS-CNCL-RDRW := #CTLS-TRANS-CNCL-RDRW ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ind2.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Z));                                        //Natural: ASSIGN #TRANS-IND2 := #CTLS-TRANS-IND2 ( #Z )
            pnd_Detail_Record_Pnd_Trans_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Z));                                          //Natural: ASSIGN #TRANS-IND := #CTLS-TRANS-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Tax_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-TAX-IND := #CTLS-TRANS-TAX-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Net_Cash_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_Z));                        //Natural: ASSIGN #TRANS-NET-CASH-IND := #CTLS-TRANS-NET-CASH-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Instllmnt_Dte.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_Z));                      //Natural: ASSIGN #TRANS-INSTLLMNT-DTE := #CTLS-TRANS-INSTLLMNT-DTE ( #Z )
            pnd_Detail_Record_Pnd_Trans_1st_Yr_Rnwl_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_Z));                  //Natural: ASSIGN #TRANS-1ST-YR-RNWL-IND := #CTLS-TRANS-1ST-YR-RNWL-IND ( #Z )
            pnd_Detail_Record_Pnd_Trans_Subplan.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_Z));                                  //Natural: ASSIGN #TRANS-SUBPLAN := #CTLS-TRANS-SUBPLAN ( #Z )
            pnd_Detail_Record_Pnd_Dtl_End.setValue("X");                                                                                                                  //Natural: ASSIGN #DTL-END := 'X'
            getWorkFiles().write(4, false, pnd_Detail_Record);                                                                                                            //Natural: WRITE WORK FILE 4 #DETAIL-RECORD
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #CTLS-TRLR-TTL-RCRD-CNT
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Rcrd_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #CTLS-TRLR-DTL-RCRD-CNT
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRANS-FIELDS
                sub_Display_Trans_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Trailer() throws Exception                                                                                                                     //Natural: WRITE-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Rcrd_Cnt().greater(getZero())))                                                               //Natural: IF #CTLS-TRLR-HDR-RCRD-CNT GT 0
        {
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Eor().setValue("X");                                                                                        //Natural: ASSIGN #CTLS-TRLR-EOR := 'X'
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Rcrd_Typ().setValue("C");                                                                                   //Natural: ASSIGN #CTLS-TRLR-RCRD-TYP := 'C'
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Run_Dte().setValue(Global.getDATN());                                                                       //Natural: ASSIGN #CTLS-TRLR-RUN-DTE := *DATN
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Begin_Run_Tme().setValue(pnd_Begin_Timn_Pnd_Begin_Time);                                                    //Natural: ASSIGN #CTLS-TRLR-BEGIN-RUN-TME := #BEGIN-TIME
            pnd_End_Timn.setValue(Global.getTIMN());                                                                                                                      //Natural: ASSIGN #END-TIMN := *TIMN
            pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_End_Run_Tme().setValue(pnd_End_Timn_Pnd_End_Time);                                                          //Natural: ASSIGN #CTLS-TRLR-END-RUN-TME := #END-TIME
            getWorkFiles().write(4, false, pdaIaaactls.getPnd_Ctls_Trlr_Record());                                                                                        //Natural: WRITE WORK FILE 4 #CTLS-TRLR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Trans_Fields() throws Exception                                                                                                              //Natural: DISPLAY-TRANS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_Z), //Natural: WRITE / '=' #CTLS-TRANS-RCRD-TYP ( #Z ) / '=' #CTLS-TRANS-SQNCE ( #Z ) / '=' #CTLS-TRANS-TYP ( #Z ) / '=' #CTLS-TRANS-TYP-RCRD ( #Z ) / '=' #CTLS-TRANS-OCCRNCE-NBR ( #Z ) / '=' #CTLS-TRANS-RTE-BASIS ( #Z ) / '=' #CTLS-TRANS-UNDLDGR-IND ( #Z ) / '=' #CTLS-TRANS-RVRSL-CDE ( #Z ) / '=' #CTLS-TRANS-DEPT-ID ( #Z ) / '=' #CTLS-TRANS-ALLCTN-NDX ( #Z ) / '=' #CTLS-TRANS-VLTN-BASIS ( #Z ) / '=' #CTLS-TRANS-AMT-SIGN ( #Z ) / '=' #CTLS-TRANS-AMT ( #Z ) / '=' #CTLS-TRANS-FROM-TKR ( #Z ) / '=' #CTLS-TRANS-FROM-LOB ( #Z ) / '=' #CTLS-TRANS-FROM-SUB-LOB ( #Z ) / '=' #CTLS-TRANS-FROM-TIAA-NO ( #Z ) / '=' #CTLS-TRANS-FROM-CREF-NO ( #Z ) / '=' #CTLS-TRANS-FROM-NUM-UNITS ( #Z ) / '=' #CTLS-TRANS-TO-TKR ( #Z )
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Z));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, "=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_Z), //Natural: WRITE '=' #CTLS-TRANS-TO-LOB ( #Z ) / '=' #CTLS-TRANS-TO-SUB-LOB ( #Z ) / '=' #CTLS-TRANS-TO-TIAA-NO ( #Z ) / '=' #CTLS-TRANS-TO-CREF-NO ( #Z ) / '=' #CTLS-TRANS-TO-NUM-UNITS ( #Z ) / '=' #CTLS-TRANS-PITS-RQST-ID ( #Z ) / '=' #CTLS-TRANS-LDGR-TYP ( #Z ) / '=' #CTLS-TRANS-CNCL-RDRW ( #Z ) / '=' #CTLS-TRANS-IND2 ( #Z ) / '=' #CTLS-TRANS-IND ( #Z ) / '=' #CTLS-TRANS-TAX-IND ( #Z ) / '=' #CTLS-TRANS-NET-CASH-IND ( #Z ) / '=' #CTLS-TRANS-INSTLLMNT-DTE ( #Z ) / '=' #CTLS-TRANS-1ST-YR-RNWL-IND ( #Z ) / '=' #CTLS-TRANS-SUBPLAN ( #Z )
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_Z),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_Z),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_Z));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_T_Hdr1_Pnd_Tprog.setValue(Global.getPROGRAM());                                                                                                   //Natural: ASSIGN #TPROG := *PROGRAM
                    pnd_T_Hdr2_Pnd_Thdr_Page.nadd(1);                                                                                                                     //Natural: ADD 1 TO #THDR-PAGE
                    getReports().write(1, ReportOption.NOTITLE,pnd_T_Hdr1_Pnd_Tprog,pnd_T_Hdr1_Pnd_Tfill1,pnd_T_Hdr1_Pnd_Thdr1,pnd_T_Hdr1_Pnd_Tfill2,pnd_T_Hdr1_Pnd_Thdr2, //Natural: WRITE ( 1 ) NOTITLE #T-HDR1
                        pnd_T_Hdr1_Pnd_Thdr_Dte);
                    getReports().write(1, ReportOption.NOTITLE,pnd_T_Hdr2_Pnd_Tfill,pnd_T_Hdr2_Pnd_Thdr,pnd_T_Hdr2_Pnd_Thdr_Jdate,pnd_T_Hdr2_Pnd_Tfill2,                  //Natural: WRITE ( 1 ) #T-HDR2
                        pnd_T_Hdr2_Pnd_Tpage_Lit,pnd_T_Hdr2_Pnd_Thdr_Page);
                    getReports().write(1, ReportOption.NOTITLE,pnd_Hdr3_Pnd_Hdr3_Fld4,pnd_Hdr3_Pnd_Hdr3_Fld6,pnd_Hdr3_Pnd_Hdr3_Fld7,pnd_Hdr3_Pnd_Hdr3_Fld8,               //Natural: WRITE ( 1 ) #HDR3
                        pnd_Hdr3_Pnd_Hdr3_Fld9,pnd_Hdr3_Pnd_Hdr3_Fld10,pnd_Hdr3_Pnd_Hdr3_Fld11);
                    getReports().write(1, ReportOption.NOTITLE,pnd_Dashes_Pnd_Dsh_Fld4,pnd_Dashes_Pnd_Dsh_Fld6,pnd_Dashes_Pnd_Dsh_Fld7,pnd_Dashes_Pnd_Dsh_Fld8,           //Natural: WRITE ( 1 ) #DASHES
                        pnd_Dashes_Pnd_Dsh_Fld9,pnd_Dashes_Pnd_Dsh_Fld10,pnd_Dashes_Pnd_Dsh_Fld11);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");

        getReports().setDisplayColumns(0, "TKR",
        		pls_Tckr_Symbl,"FNUM",
        		pls_Fund_Num_Cde,"FALPHA",
        		pls_Fund_Alpha_Cde);
    }
}
