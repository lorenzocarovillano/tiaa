/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:42 PM
**        * FROM NATURAL PROGRAM : Iaap799p
************************************************************
**        * FILE NAME            : Iaap799p.java
**        * CLASS NAME           : Iaap799p
**        * INSTANCE NAME        : Iaap799p
************************************************************
**********************************************************************
*                                                                    *
* PROGRAM    -   IAAP799P                                          *
* 08/2017    -   CREATES AC LT 10 YEARS AND P&I PIN LIST FOR
*                REGULATORY COMPLIANCE
* HISTORY
* 08/28/2017 - JUN TINIO - ORIGINAL CODE
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap799p extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Issue_Yyyy;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_Iss_St;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Iss;
    private DbsField pnd_Input_Pnd_F3a;
    private DbsField pnd_Input_Pnd_First_Ann_Dob_Dte;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob_Dte;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_5;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde_F;
    private DbsField pnd_Input_Pnd_F8;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_17;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Res;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Pnd_State_Cde;
    private DbsField pnd_Input_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Pnd_Local_Cde;
    private DbsField pnd_Input_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_19;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Begin_Dte_A;

    private DbsGroup pnd_Begin_Dte_A__R_Field_20;
    private DbsField pnd_Begin_Dte_A_Pnd_Begin_Dte_N;
    private DbsField pnd_S_Optn;
    private DbsField pnd_S_Issue_Dte;
    private DbsField pnd_Elgble;
    private DbsField pnd_Cpr_Bypass;
    private DbsField pnd_Bypass;
    private DbsField pnd_Start_Dte;

    private DbsGroup pnd_Start_Dte__R_Field_21;
    private DbsField pnd_Start_Dte_Pnd_S_Yyyy;
    private DbsField pnd_Start_Dte_Pnd_S_Mm;
    private DbsField pnd_End_Dte;

    private DbsGroup pnd_End_Dte__R_Field_22;
    private DbsField pnd_End_Dte_Pnd_E_Yyyy;
    private DbsField pnd_End_Dte_Pnd_E_Mm;
    private DbsField pnd_Max_Dte;

    private DbsGroup pnd_Max_Dte__R_Field_23;
    private DbsField pnd_Max_Dte_Pnd_Max_Dte_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Yyyy = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Yyyy", "#ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 9);
        pnd_Input_Pnd_Iss_St = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Iss_St);
        pnd_Input__Filler1 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_Iss = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Iss", "#ISS", FieldType.STRING, 2);
        pnd_Input_Pnd_F3a = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3a", "#F3A", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob_Dte", "#FIRST-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob_Dte", "#SCND-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde_5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_5", "#PPG-CDE-5", FieldType.STRING, 5);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ppg_Cde_5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Ppg_Cde_F = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde_F", "#PPG-CDE-F", FieldType.STRING, 1);
        pnd_Input_Pnd_F8 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 8);

        pnd_Input__R_Field_10 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_13 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);

        pnd_Input__R_Field_14 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Summ_Fund_Cde);
        pnd_Input_Fund_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 7, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_16 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_17 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_17", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler2 = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_Res = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input_Pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Trmnte_Rsn = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_18 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_18", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_Spirt_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Input_Pnd_Spirt_Srce = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Arr_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Input_Pnd_Spirt_Prcss_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Fed_Tax_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Pnd_State_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_State_Tax_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Local_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Local_Tax_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Lst_Chnge_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cpr_Xfr_Term_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cpr_Lgl_Res_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_16.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_19", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_19.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_19.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_19.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_19.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Begin_Dte_A = localVariables.newFieldInRecord("pnd_Begin_Dte_A", "#BEGIN-DTE-A", FieldType.STRING, 6);

        pnd_Begin_Dte_A__R_Field_20 = localVariables.newGroupInRecord("pnd_Begin_Dte_A__R_Field_20", "REDEFINE", pnd_Begin_Dte_A);
        pnd_Begin_Dte_A_Pnd_Begin_Dte_N = pnd_Begin_Dte_A__R_Field_20.newFieldInGroup("pnd_Begin_Dte_A_Pnd_Begin_Dte_N", "#BEGIN-DTE-N", FieldType.NUMERIC, 
            6);
        pnd_S_Optn = localVariables.newFieldInRecord("pnd_S_Optn", "#S-OPTN", FieldType.NUMERIC, 2);
        pnd_S_Issue_Dte = localVariables.newFieldInRecord("pnd_S_Issue_Dte", "#S-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Elgble = localVariables.newFieldInRecord("pnd_Elgble", "#ELGBLE", FieldType.BOOLEAN, 1);
        pnd_Cpr_Bypass = localVariables.newFieldInRecord("pnd_Cpr_Bypass", "#CPR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.STRING, 6);

        pnd_Start_Dte__R_Field_21 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_21", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_S_Yyyy = pnd_Start_Dte__R_Field_21.newFieldInGroup("pnd_Start_Dte_Pnd_S_Yyyy", "#S-YYYY", FieldType.NUMERIC, 4);
        pnd_Start_Dte_Pnd_S_Mm = pnd_Start_Dte__R_Field_21.newFieldInGroup("pnd_Start_Dte_Pnd_S_Mm", "#S-MM", FieldType.NUMERIC, 2);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.STRING, 6);

        pnd_End_Dte__R_Field_22 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_22", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_E_Yyyy = pnd_End_Dte__R_Field_22.newFieldInGroup("pnd_End_Dte_Pnd_E_Yyyy", "#E-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Dte_Pnd_E_Mm = pnd_End_Dte__R_Field_22.newFieldInGroup("pnd_End_Dte_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        pnd_Max_Dte = localVariables.newFieldInRecord("pnd_Max_Dte", "#MAX-DTE", FieldType.STRING, 6);

        pnd_Max_Dte__R_Field_23 = localVariables.newGroupInRecord("pnd_Max_Dte__R_Field_23", "REDEFINE", pnd_Max_Dte);
        pnd_Max_Dte_Pnd_Max_Dte_N = pnd_Max_Dte__R_Field_23.newFieldInGroup("pnd_Max_Dte_Pnd_Max_Dte_N", "#MAX-DTE-N", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap799p() throws Exception
    {
        super("Iaap799p");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 0 ) LS = 180 PS = 0
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 180 PS = 0
                                                                                                                                                                          //Natural: PERFORM GET-END-MAX-DATE
        sub_Get_End_Max_Date();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20))))                                                               //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS #CPR-BYPASS
                pnd_Cpr_Bypass.reset();
                if (condition(pnd_Input_Pnd_Optn_Cde.equals(21) || pnd_Input_Pnd_Optn_Cde.equals(22)))                                                                    //Natural: IF ( #OPTN-CDE = 21 OR = 22 )
                {
                    if (condition((pnd_Input_Pnd_Optn_Cde.equals(22) && (((((((((((pnd_Input_Pnd_Orgn_Cde.equals(3) || pnd_Input_Pnd_Orgn_Cde.equals(17))                 //Natural: IF #OPTN-CDE = 22 AND ( #ORGN-CDE = 03 OR = 17 OR = 18 OR = 35 OR = 36 OR = 37 OR = 38 OR = 40 OR = 43 OR = 44 OR = 45 OR = 46 )
                        || pnd_Input_Pnd_Orgn_Cde.equals(18)) || pnd_Input_Pnd_Orgn_Cde.equals(35)) || pnd_Input_Pnd_Orgn_Cde.equals(36)) || pnd_Input_Pnd_Orgn_Cde.equals(37)) 
                        || pnd_Input_Pnd_Orgn_Cde.equals(38)) || pnd_Input_Pnd_Orgn_Cde.equals(40)) || pnd_Input_Pnd_Orgn_Cde.equals(43)) || pnd_Input_Pnd_Orgn_Cde.equals(44)) 
                        || pnd_Input_Pnd_Orgn_Cde.equals(45)) || pnd_Input_Pnd_Orgn_Cde.equals(46)))))
                    {
                        pnd_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #BYPASS := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Pnd_Issue_Dte.greater(pnd_Max_Dte_Pnd_Max_Dte_N)))                                                                            //Natural: IF #ISSUE-DTE GT #MAX-DTE-N
                    {
                        pnd_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #BYPASS := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_S_Issue_Dte.setValue(pnd_Input_Pnd_Issue_Dte);                                                                                                    //Natural: ASSIGN #S-ISSUE-DTE := #ISSUE-DTE
                    pnd_S_Optn.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                          //Natural: ASSIGN #S-OPTN := #OPTN-CDE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Bypass.getBoolean()))                                                                                                                       //Natural: IF #BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                pnd_Cpr_Bypass.reset();                                                                                                                                   //Natural: RESET #CPR-BYPASS
                if (condition(((((pnd_Input_Pnd_Status_Cde.notEquals(1) || pnd_Input_Pnd_Pend_Cde.equals("R")) || pnd_Input_Pnd_Cpr_Id_Nbr.equals(getZero()))             //Natural: IF #STATUS-CDE NE 1 OR #PEND-CDE = 'R' OR #CPR-ID-NBR = 0 OR #PAYEE-CDE GT 02 AND #PEND-CDE = 'A' OR = 'C' OR ( #S-OPTN = 21 AND #CNTRCT-FIN-PER-PAY-DTE LE #BEGIN-DTE-N )
                    || (pnd_Input_Pnd_Payee_Cde.greater(2) && (pnd_Input_Pnd_Pend_Cde.equals("A") || pnd_Input_Pnd_Pend_Cde.equals("C")))) || (pnd_S_Optn.equals(21) 
                    && pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte.lessOrEqual(pnd_Begin_Dte_A_Pnd_Begin_Dte_N)))))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_S_Optn.equals(21)))                                                                                                                     //Natural: IF #S-OPTN = 21
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-LT-10YRS
                    sub_Determine_Lt_10yrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Elgble.getBoolean() || pnd_S_Optn.equals(22)))                                                                                          //Natural: IF #ELGBLE OR #S-OPTN = 22
                {
                    getWorkFiles().write(2, false, pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                             //Natural: WRITE WORK FILE 2 #CPR-ID-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-LT-10YRS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-END-MAX-DATE
        //* ***********************************************************************
    }
    private void sub_Determine_Lt_10yrs() throws Exception                                                                                                                //Natural: DETERMINE-LT-10YRS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Start_Dte.setValueEdited(pnd_S_Issue_Dte,new ReportEditMask("999999"));                                                                                       //Natural: MOVE EDITED #S-ISSUE-DTE ( EM = 999999 ) TO #START-DTE
        pnd_End_Dte.setValueEdited(pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte,new ReportEditMask("999999"));                                                                    //Natural: MOVE EDITED #CNTRCT-FIN-PER-PAY-DTE ( EM = 999999 ) TO #END-DTE
        short decideConditionsMet277 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MODE-IND;//Natural: VALUE 100
        if (condition((pnd_Input_Pnd_Mode_Ind.equals(100))))
        {
            decideConditionsMet277++;
            pnd_End_Dte_Pnd_E_Mm.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(601) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(603)))))
        {
            decideConditionsMet277++;
            pnd_End_Dte_Pnd_E_Mm.nadd(3);                                                                                                                                 //Natural: ADD 3 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(701) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(706)))))
        {
            decideConditionsMet277++;
            pnd_End_Dte_Pnd_E_Mm.nadd(6);                                                                                                                                 //Natural: ADD 6 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pnd_Input_Pnd_Mode_Ind.greaterOrEqual(801) && pnd_Input_Pnd_Mode_Ind.lessOrEqual(812)))))
        {
            decideConditionsMet277++;
            pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                               //Natural: ADD 1 TO #E-YYYY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_End_Dte_Pnd_E_Mm.greater(12)))                                                                                                                  //Natural: IF #E-MM GT 12
        {
            pnd_End_Dte_Pnd_E_Mm.nsubtract(12);                                                                                                                           //Natural: SUBTRACT 12 FROM #E-MM
            pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                               //Natural: ADD 1 TO #E-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Start_Dte_Pnd_S_Mm.greater(pnd_End_Dte_Pnd_E_Mm)))                                                                                              //Natural: IF #S-MM GT #E-MM
        {
            pnd_End_Dte_Pnd_E_Yyyy.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #E-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_End_Dte_Pnd_E_Yyyy.subtract(pnd_Start_Dte_Pnd_S_Yyyy)).less(10)))                                                                              //Natural: IF ( #E-YYYY - #S-YYYY ) LT 10
        {
            pnd_Elgble.setValue(true);                                                                                                                                    //Natural: ASSIGN #ELGBLE := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Elgble.setValue(false);                                                                                                                                   //Natural: ASSIGN #ELGBLE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_End_Max_Date() throws Exception                                                                                                                  //Natural: GET-END-MAX-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Max_Dte.setValue("999999");                                                                                                                                   //Natural: ASSIGN #MAX-DTE := '999999'
        pnd_Begin_Dte_A.setValue("000000");                                                                                                                               //Natural: ASSIGN #BEGIN-DTE-A := '000000'
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ076");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ076'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ACPNI");                                                                                                        //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ACPNI'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ02",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ02:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ02")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Begin_Dte_A.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id);                                                                                                 //Natural: ASSIGN #BEGIN-DTE-A := NAZ-TBL-RCRD-LVL3-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("MAX");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'MAX'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ03",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ03:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ03")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Max_Dte.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id);                                                                                                     //Natural: ASSIGN #MAX-DTE := NAZ-TBL-RCRD-LVL3-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=180 PS=0");
        Global.format(1, "LS=180 PS=0");
    }
}
