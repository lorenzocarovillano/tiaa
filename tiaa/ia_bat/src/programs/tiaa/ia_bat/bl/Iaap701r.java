/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:47 PM
**        * FROM NATURAL PROGRAM : Iaap701r
************************************************************
**        * FILE NAME            : Iaap701r.java
**        * CLASS NAME           : Iaap701r
**        * INSTANCE NAME        : Iaap701r
************************************************************
**********************************************************************
* PROGRAM    -   IAAP701R
* PURPOSE    -   CREATE A FILE FEED TO RECONNET OF MONTHLY PENDED
*                PAYMENTS PASSED TO CPS FOR THE ANNUITY FUND
*                RECONCILIATION PROJECT.
*
*                INPUT: DSN=PPDT.PNA.IA.CPS.FILE(0),DISP=SHR
*                       FROM P1930IAM.
*                OUTPUT: DSN=PIA.ANN.IA.RECON.MONTHLY,DISP=SHR
*                       TO PIA1931X
*
* HISTORY    -   08/17/10 ORIGINAL CODE - JUN TINIO
* MODIFIED   -
*                03/14/17 - PIN EXPANSION. SCAN 03/17
*                04/2017 OS PIN EXPANSION CHANGES MARKED 082017.     *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap701r extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup gtn_Pda_E;
    private DbsField gtn_Pda_E_Pymnt_Corp_Wpid;
    private DbsField gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time;
    private DbsField gtn_Pda_E_Cntrct_Unq_Id_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Ppcn_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Cmbn_Nbr;
    private DbsField gtn_Pda_E_Cntrct_Cref_Nbr;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Ind;
    private DbsField gtn_Pda_E_Annt_Soc_Sec_Nbr;
    private DbsField gtn_Pda_E_Annt_Ctznshp_Cde;
    private DbsField gtn_Pda_E_Annt_Rsdncy_Cde;
    private DbsField gtn_Pda_E_Annt_Locality_Cde;

    private DbsGroup gtn_Pda_E_Ph_Name;
    private DbsField gtn_Pda_E_Ph_Last_Name;
    private DbsField gtn_Pda_E_Ph_Middle_Name;
    private DbsField gtn_Pda_E_Ph_First_Name;
    private DbsField gtn_Pda_E_Pymnt_Dob;

    private DbsGroup gtn_Pda_E_Pymnt_Nme_And_Addr_Grp;
    private DbsField gtn_Pda_E_Pymnt_Nme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Lines;

    private DbsGroup gtn_Pda_E__R_Field_1;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line_Txt;

    private DbsGroup gtn_Pda_E__R_Field_2;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line1_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line2_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line3_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line4_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line5_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Line6_Txt;
    private DbsField gtn_Pda_E_Pymnt_Addr_Zip_Cde;
    private DbsField gtn_Pda_E_Pymnt_Postl_Data;
    private DbsField gtn_Pda_E_Pymnt_Addr_Type_Ind;
    private DbsField gtn_Pda_E_Pymnt_Foreign_Cde;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X;

    private DbsGroup gtn_Pda_E__R_Field_3;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X;

    private DbsGroup gtn_Pda_E__R_Field_4;
    private DbsField gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme;
    private DbsField gtn_Pda_E_Pymnt_Addr_Chg_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settl_Ivc_Ind;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Total_Check_Amt;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Fund_Payee;
    private DbsField gtn_Pda_E_Inv_Acct_Cde;
    private DbsField gtn_Pda_E_Inv_Acct_Settl_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Cntrct_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Dvdnd_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Value;
    private DbsField gtn_Pda_E_Inv_Acct_Unit_Qty;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Ivc_Ind;
    private DbsField gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt;
    private DbsField gtn_Pda_E_Inv_Acct_Valuat_Period;

    private DbsGroup gtn_Pda_E_Pnd_Pnd_Deductions;
    private DbsField gtn_Pda_E_Pymnt_Ded_Cde;
    private DbsField gtn_Pda_E_Pymnt_Ded_Amt;
    private DbsField gtn_Pda_E_Pymnt_Ded_Payee_Cde;
    private DbsField gtn_Pda_E_Pymnt_Payee_Tax_Ind;
    private DbsField gtn_Pda_E_Pymnt_Settlmnt_Dte;
    private DbsField gtn_Pda_E_Pymnt_Check_Dte;
    private DbsField gtn_Pda_E_Pymnt_Cycle_Dte;
    private DbsField gtn_Pda_E_Pymnt_Acctg_Dte;
    private DbsField gtn_Pda_E_Pymnt_Ia_Issue_Dte;
    private DbsField gtn_Pda_E_Pymnt_Intrfce_Dte;
    private DbsField gtn_Pda_E_Cntrct_Check_Crrncy_Cde;
    private DbsField gtn_Pda_E_Cntrct_Orgn_Cde;
    private DbsField gtn_Pda_E_Cntrct_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Payee_Cde;

    private DbsGroup gtn_Pda_E__R_Field_5;
    private DbsField gtn_Pda_E_Pnd_Payee_N;
    private DbsField gtn_Pda_E_Cntrct_Option_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ac_Lt_10yrs;
    private DbsField gtn_Pda_E_Cntrct_Mode_Cde;
    private DbsField gtn_Pda_E_Pymnt_Method_Cde;
    private DbsField gtn_Pda_E_Pymnt_Pay_Type_Req_Ind;
    private DbsField gtn_Pda_E_Pymnt_Spouse_Pay_Stats;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind;
    private DbsField gtn_Pda_E_Cntrct_Pymnt_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Roll_Dest_Cde;
    private DbsField gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde;
    private DbsField gtn_Pda_E_Pymnt_Eft_Acct_Nbr;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id_X;

    private DbsGroup gtn_Pda_E__R_Field_6;
    private DbsField gtn_Pda_E_Pymnt_Eft_Transit_Id;
    private DbsField gtn_Pda_E_Pymnt_Chk_Sav_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Cde;
    private DbsField gtn_Pda_E_Cntrct_Hold_Ind;
    private DbsField gtn_Pda_E_Cntrct_Hold_Grp;
    private DbsField gtn_Pda_E_Cntrct_Hold_User_Id;
    private DbsField gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types;

    private DbsGroup gtn_Pda_E__R_Field_7;
    private DbsField gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField gtn_Pda_E_Pnd_Pnd_State_Tax_Code;
    private DbsField gtn_Pda_E_Pnd_Pnd_Local_Tax_Code;
    private DbsField gtn_Pda_E_Pymnt_Tax_Exempt_Ind;
    private DbsField gtn_Pda_E_Pymnt_Tax_Form;
    private DbsField gtn_Pda_E_Pymnt_Tax_Calc_Cde;
    private DbsField gtn_Pda_E_Cntrct_Qlfied_Cde;
    private DbsField gtn_Pda_E_Cntrct_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Sub_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Ia_Lob_Cde;
    private DbsField gtn_Pda_E_Cntrct_Annty_Ins_Type;
    private DbsField gtn_Pda_E_Cntrct_Annty_Type_Cde;
    private DbsField gtn_Pda_E_Cntrct_Insurance_Option;
    private DbsField gtn_Pda_E_Cntrct_Life_Contingency;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Cde;
    private DbsField gtn_Pda_E_Pymnt_Suspend_Dte;
    private DbsField gtn_Pda_E_Pnd_Pnd_Pda_Count;
    private DbsField gtn_Pda_E_Pnd_Pnd_This_Pymnt;
    private DbsField gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField gtn_Pda_E_Gtn_Ret_Code;

    private DbsGroup gtn_Pda_E__R_Field_8;
    private DbsField gtn_Pda_E_Global_Country;
    private DbsField gtn_Pda_E_Gtn_Ret_Msg;
    private DbsField gtn_Pda_E_Temp_Pend_Cde;
    private DbsField gtn_Pda_E_Error_Code_1;
    private DbsField gtn_Pda_E_Error_Code_2;
    private DbsField gtn_Pda_E_Current_Mode;
    private DbsField gtn_Pda_E_Egtrra_Eligibility_Ind;
    private DbsField gtn_Pda_E_Originating_Irs_Cde;

    private DbsGroup gtn_Pda_E__R_Field_9;
    private DbsField gtn_Pda_E_Distributing_Irc_Cde;
    private DbsField gtn_Pda_E_Receiving_Irc_Cde;
    private DbsField gtn_Pda_E_Cntrct_Money_Source;
    private DbsField gtn_Pda_E_Roth_Dob;
    private DbsField gtn_Pda_E_Roth_First_Contrib_Dte;
    private DbsField gtn_Pda_E_Roth_Death_Dte;
    private DbsField gtn_Pda_E_Roth_Disability_Dte;
    private DbsField gtn_Pda_E_Roth_Money_Source;
    private DbsField gtn_Pda_E_Roth_Qual_Non_Qual_Distrib;
    private DbsField gtn_Pda_E_Ia_Orgn_Cde;
    private DbsField gtn_Pda_E_Plan_Number;
    private DbsField gtn_Pda_E_Sub_Plan;
    private DbsField gtn_Pda_E_Originating_Cntrct_Nbr;
    private DbsField gtn_Pda_E_Originating_Sub_Plan;

    private DbsGroup pnd_Recon_Out;
    private DbsField pnd_Recon_Out_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Recon_Out_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Recon_Out_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Recon_Out_Annt_Soc_Sec_Ind;
    private DbsField pnd_Recon_Out_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Recon_Out_Annt_Ctznshp_Cde;
    private DbsField pnd_Recon_Out_Annt_Rsdncy_Cde;
    private DbsField pnd_Recon_Out_Annt_Locality_Cde;

    private DbsGroup pnd_Recon_Out_Ph_Name;
    private DbsField pnd_Recon_Out_Ph_Last_Name;
    private DbsField pnd_Recon_Out_Ph_Middle_Name;
    private DbsField pnd_Recon_Out_Ph_First_Name;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Foreign_Cde;

    private DbsGroup pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Cde;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Amt;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Dpi_Amt;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Fund_Dpi_Amt;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Total_Check_Amt;

    private DbsGroup pnd_Recon_Out_Pnd_Pnd_Fund_Payee;
    private DbsField pnd_Recon_Out_Inv_Acct_Cde;
    private DbsField pnd_Recon_Out_Inv_Acct_Settl_Amt;
    private DbsField pnd_Recon_Out_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Recon_Out_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Recon_Out_Inv_Acct_Unit_Value;
    private DbsField pnd_Recon_Out_Inv_Acct_Unit_Qty;
    private DbsField pnd_Recon_Out_Inv_Acct_Valuat_Period;

    private DbsGroup pnd_Recon_Out_Pnd_Pnd_Deductions;
    private DbsField pnd_Recon_Out_Pymnt_Ded_Cde;
    private DbsField pnd_Recon_Out_Pymnt_Ded_Amt;
    private DbsField pnd_Recon_Out_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Cycle_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Acctg_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Ia_Issue_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Intrfce_Dte;
    private DbsField pnd_Recon_Out_Cntrct_Orgn_Cde;
    private DbsField pnd_Recon_Out_Cntrct_Type_Cde;
    private DbsField pnd_Recon_Out_Cntrct_Payee_Cde;
    private DbsField pnd_Recon_Out_Cntrct_Option_Cde;
    private DbsField pnd_Recon_Out_Cntrct_Mode_Cde;
    private DbsField pnd_Recon_Out_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Recon_Out_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Recon_Out_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Recon_Out_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Recon_Out_Pymnt_Suspend_Cde;
    private DbsField pnd_Recon_Out_Pnd_Pymnt_Suspend_Dte;

    private DbsGroup pnd_Recon_Out__R_Field_10;
    private DbsField pnd_Recon_Out_Pnd_Pend_Dte;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Pda_Count;
    private DbsField pnd_Recon_Out_Pnd_Pnd_This_Pymnt;
    private DbsField pnd_Recon_Out_Pnd_Pnd_Nbr_Of_Pymnts;
    private DbsField pnd_Recon_Out_Global_Country;
    private DbsField pnd_Recon_Out_Error_Code_1;
    private DbsField pnd_Recon_Out_Error_Code_2;
    private DbsField pnd_Recon_Out_Pnd_Dod;
    private DbsField pnd_Recon_Out_Pnd_Final_Per_Pay_Dte;
    private DbsField pnd_Recon_Out_Ia_Orgn_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Cntrct_Pend_Dte;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_11;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Ppcn_Nbr;
    private DbsField pnd_Cpr_Key_Pnd_Cpr_Payee;
    private DbsField pnd_Override_Ind;
    private DbsField pnd_Cnt;
    private DbsField pnd_Pend_Cnt;
    private DbsField pnd_Dod_1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        gtn_Pda_E = localVariables.newGroupInRecord("gtn_Pda_E", "GTN-PDA-E");
        gtn_Pda_E_Pymnt_Corp_Wpid = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        gtn_Pda_E_Cntrct_Unq_Id_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        gtn_Pda_E_Cntrct_Ppcn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Cntrct_Cmbn_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        gtn_Pda_E_Cntrct_Cref_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Annt_Soc_Sec_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        gtn_Pda_E_Annt_Soc_Sec_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        gtn_Pda_E_Annt_Ctznshp_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Annt_Rsdncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Annt_Locality_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);

        gtn_Pda_E_Ph_Name = gtn_Pda_E.newGroupInGroup("gtn_Pda_E_Ph_Name", "PH-NAME");
        gtn_Pda_E_Ph_Last_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        gtn_Pda_E_Ph_Middle_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        gtn_Pda_E_Ph_First_Name = gtn_Pda_E_Ph_Name.newFieldInGroup("gtn_Pda_E_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        gtn_Pda_E_Pymnt_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);

        gtn_Pda_E_Pymnt_Nme_And_Addr_Grp = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1, 
            2));
        gtn_Pda_E_Pymnt_Nme = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        gtn_Pda_E_Pymnt_Addr_Lines = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", FieldType.STRING, 
            210);

        gtn_Pda_E__R_Field_1 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_1", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line_Txt = gtn_Pda_E__R_Field_1.newFieldArrayInGroup("gtn_Pda_E_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));

        gtn_Pda_E__R_Field_2 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_2", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Lines);
        gtn_Pda_E_Pymnt_Addr_Line1_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line2_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line3_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line4_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line5_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Line6_Txt = gtn_Pda_E__R_Field_2.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        gtn_Pda_E_Pymnt_Addr_Zip_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 
            9);
        gtn_Pda_E_Pymnt_Postl_Data = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        gtn_Pda_E_Pymnt_Addr_Type_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Foreign_Cde = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X", "PYMNT-ADDR-LAST-CHG-DTE-X", 
            FieldType.STRING, 8);

        gtn_Pda_E__R_Field_3 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_3", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte_X);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte = gtn_Pda_E__R_Field_3.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X", "PYMNT-ADDR-LAST-CHG-TME-X", 
            FieldType.STRING, 7);

        gtn_Pda_E__R_Field_4 = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("gtn_Pda_E__R_Field_4", "REDEFINE", gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme_X);
        gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme = gtn_Pda_E__R_Field_4.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 
            7);
        gtn_Pda_E_Pymnt_Addr_Chg_Ind = gtn_Pda_E_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("gtn_Pda_E_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Settl_Ivc_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);

        gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt", "##FUND-SETTLMNT", new DbsArrayController(1, 
            40));
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde", "##FUND-SETTL-CDE", FieldType.STRING, 
            2);
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt", "##FUND-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt", "##FUND-SETTL-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt = gtn_Pda_E.newFieldArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt", "##FUND-DPI-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 40));
        gtn_Pda_E_Pnd_Pnd_Total_Check_Amt = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Total_Check_Amt", "##TOTAL-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        gtn_Pda_E_Pnd_Pnd_Fund_Payee = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1, 40));
        gtn_Pda_E_Inv_Acct_Cde = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Inv_Acct_Settl_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Cntrct_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Dvdnd_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        gtn_Pda_E_Inv_Acct_Unit_Value = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4);
        gtn_Pda_E_Inv_Acct_Unit_Qty = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        gtn_Pda_E_Inv_Acct_Ivc_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_E_Inv_Acct_Ivc_Ind = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        gtn_Pda_E_Inv_Acct_Valuat_Period = gtn_Pda_E_Pnd_Pnd_Fund_Payee.newFieldInGroup("gtn_Pda_E_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);

        gtn_Pda_E_Pnd_Pnd_Deductions = gtn_Pda_E.newGroupArrayInGroup("gtn_Pda_E_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1, 10));
        gtn_Pda_E_Pymnt_Ded_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Ded_Amt = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        gtn_Pda_E_Pymnt_Ded_Payee_Cde = gtn_Pda_E_Pnd_Pnd_Deductions.newFieldInGroup("gtn_Pda_E_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8);
        gtn_Pda_E_Pymnt_Payee_Tax_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Payee_Tax_Ind", "PYMNT-PAYEE-TAX-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Settlmnt_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Check_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Cycle_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Acctg_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Ia_Issue_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        gtn_Pda_E_Pymnt_Intrfce_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        gtn_Pda_E_Cntrct_Check_Crrncy_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Orgn_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);

        gtn_Pda_E__R_Field_5 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_5", "REDEFINE", gtn_Pda_E_Cntrct_Payee_Cde);
        gtn_Pda_E_Pnd_Payee_N = gtn_Pda_E__R_Field_5.newFieldInGroup("gtn_Pda_E_Pnd_Payee_N", "#PAYEE-N", FieldType.NUMERIC, 2);
        gtn_Pda_E_Cntrct_Option_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        gtn_Pda_E_Cntrct_Ac_Lt_10yrs = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 1);
        gtn_Pda_E_Cntrct_Mode_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        gtn_Pda_E_Pymnt_Method_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Pay_Type_Req_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        gtn_Pda_E_Pymnt_Spouse_Pay_Stats = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Pymnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Pymnt_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Roll_Dest_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        gtn_Pda_E_Pymnt_Eft_Acct_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        gtn_Pda_E_Pymnt_Eft_Transit_Id_X = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id_X", "PYMNT-EFT-TRANSIT-ID-X", FieldType.STRING, 9);

        gtn_Pda_E__R_Field_6 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_6", "REDEFINE", gtn_Pda_E_Pymnt_Eft_Transit_Id_X);
        gtn_Pda_E_Pymnt_Eft_Transit_Id = gtn_Pda_E__R_Field_6.newFieldInGroup("gtn_Pda_E_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 
            9);
        gtn_Pda_E_Pymnt_Chk_Sav_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Hold_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Hold_Grp = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        gtn_Pda_E_Cntrct_Hold_User_Id = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 3);
        gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types", "##TX-WITHHOLDING-TAX-TYPES", 
            FieldType.STRING, 3);

        gtn_Pda_E__R_Field_7 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_7", "REDEFINE", gtn_Pda_E_Pnd_Pnd_Tx_Withholding_Tax_Types);
        gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr = gtn_Pda_E__R_Field_7.newFieldInGroup("gtn_Pda_E_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        gtn_Pda_E_Pnd_Pnd_State_Tax_Code = gtn_Pda_E__R_Field_7.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_State_Tax_Code", "##STATE-TAX-CODE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pnd_Pnd_Local_Tax_Code = gtn_Pda_E__R_Field_7.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Local_Tax_Code", "##LOCAL-TAX-CODE", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Tax_Exempt_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Tax_Form = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        gtn_Pda_E_Pymnt_Tax_Calc_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Qlfied_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Sub_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        gtn_Pda_E_Cntrct_Ia_Lob_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Annty_Ins_Type = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Annty_Type_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Cntrct_Insurance_Option = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        gtn_Pda_E_Cntrct_Life_Contingency = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        gtn_Pda_E_Pymnt_Suspend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Pymnt_Suspend_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", FieldType.DATE);
        gtn_Pda_E_Pnd_Pnd_Pda_Count = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Pda_Count", "##PDA-COUNT", FieldType.PACKED_DECIMAL, 2);
        gtn_Pda_E_Pnd_Pnd_This_Pymnt = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 2);
        gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 2);
        gtn_Pda_E_Gtn_Ret_Code = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);

        gtn_Pda_E__R_Field_8 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_8", "REDEFINE", gtn_Pda_E_Gtn_Ret_Code);
        gtn_Pda_E_Global_Country = gtn_Pda_E__R_Field_8.newFieldInGroup("gtn_Pda_E_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        gtn_Pda_E_Gtn_Ret_Msg = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Gtn_Ret_Msg", "GTN-RET-MSG", FieldType.STRING, 60);
        gtn_Pda_E_Temp_Pend_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Temp_Pend_Cde", "TEMP-PEND-CDE", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_1 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        gtn_Pda_E_Error_Code_2 = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        gtn_Pda_E_Current_Mode = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Current_Mode", "CURRENT-MODE", FieldType.STRING, 1);
        gtn_Pda_E_Egtrra_Eligibility_Ind = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        gtn_Pda_E_Originating_Irs_Cde = gtn_Pda_E.newFieldArrayInGroup("gtn_Pda_E_Originating_Irs_Cde", "ORIGINATING-IRS-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1, 8));

        gtn_Pda_E__R_Field_9 = gtn_Pda_E.newGroupInGroup("gtn_Pda_E__R_Field_9", "REDEFINE", gtn_Pda_E_Originating_Irs_Cde);
        gtn_Pda_E_Distributing_Irc_Cde = gtn_Pda_E__R_Field_9.newFieldInGroup("gtn_Pda_E_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        gtn_Pda_E_Receiving_Irc_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Cntrct_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Dob = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_First_Contrib_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8);
        gtn_Pda_E_Roth_Death_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Disability_Dte = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 8);
        gtn_Pda_E_Roth_Money_Source = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5);
        gtn_Pda_E_Roth_Qual_Non_Qual_Distrib = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", FieldType.STRING, 
            1);
        gtn_Pda_E_Ia_Orgn_Cde = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 2);
        gtn_Pda_E_Plan_Number = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Plan_Number", "PLAN-NUMBER", FieldType.STRING, 6);
        gtn_Pda_E_Sub_Plan = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Sub_Plan", "SUB-PLAN", FieldType.STRING, 6);
        gtn_Pda_E_Originating_Cntrct_Nbr = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Originating_Cntrct_Nbr", "ORIGINATING-CNTRCT-NBR", FieldType.STRING, 10);
        gtn_Pda_E_Originating_Sub_Plan = gtn_Pda_E.newFieldInGroup("gtn_Pda_E_Originating_Sub_Plan", "ORIGINATING-SUB-PLAN", FieldType.STRING, 6);

        pnd_Recon_Out = localVariables.newGroupInRecord("pnd_Recon_Out", "#RECON-OUT");
        pnd_Recon_Out_Cntrct_Unq_Id_Nbr = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Recon_Out_Cntrct_Ppcn_Nbr = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Recon_Out_Cntrct_Cmbn_Nbr = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Recon_Out_Annt_Soc_Sec_Ind = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        pnd_Recon_Out_Annt_Soc_Sec_Nbr = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Recon_Out_Annt_Ctznshp_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        pnd_Recon_Out_Annt_Rsdncy_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Recon_Out_Annt_Locality_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);

        pnd_Recon_Out_Ph_Name = pnd_Recon_Out.newGroupInGroup("pnd_Recon_Out_Ph_Name", "PH-NAME");
        pnd_Recon_Out_Ph_Last_Name = pnd_Recon_Out_Ph_Name.newFieldInGroup("pnd_Recon_Out_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Recon_Out_Ph_Middle_Name = pnd_Recon_Out_Ph_Name.newFieldInGroup("pnd_Recon_Out_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Recon_Out_Ph_First_Name = pnd_Recon_Out_Ph_Name.newFieldInGroup("pnd_Recon_Out_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Recon_Out_Pnd_Pymnt_Foreign_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Foreign_Cde", "#PYMNT-FOREIGN-CDE", FieldType.STRING, 
            1);

        pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt = pnd_Recon_Out.newGroupArrayInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt", "##FUND-SETTLMNT", new DbsArrayController(1, 
            40));
        pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Cde = pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Cde", "##FUND-SETTL-CDE", 
            FieldType.STRING, 2);
        pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Amt = pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Amt", "##FUND-SETTL-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Dpi_Amt = pnd_Recon_Out_Pnd_Pnd_Fund_Settlmnt.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Dpi_Amt", "##FUND-SETTL-DPI-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Recon_Out_Pnd_Pnd_Fund_Dpi_Amt = pnd_Recon_Out.newFieldArrayInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Dpi_Amt", "##FUND-DPI-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Recon_Out_Pnd_Pnd_Total_Check_Amt = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Total_Check_Amt", "##TOTAL-CHECK-AMT", FieldType.NUMERIC, 
            11, 2);

        pnd_Recon_Out_Pnd_Pnd_Fund_Payee = pnd_Recon_Out.newGroupArrayInGroup("pnd_Recon_Out_Pnd_Pnd_Fund_Payee", "##FUND-PAYEE", new DbsArrayController(1, 
            40));
        pnd_Recon_Out_Inv_Acct_Cde = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Recon_Out_Inv_Acct_Settl_Amt = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Recon_Out_Inv_Acct_Cntrct_Amt = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Recon_Out_Inv_Acct_Dvdnd_Amt = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Recon_Out_Inv_Acct_Unit_Value = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.NUMERIC, 9, 4);
        pnd_Recon_Out_Inv_Acct_Unit_Qty = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.NUMERIC, 
            9, 3);
        pnd_Recon_Out_Inv_Acct_Valuat_Period = pnd_Recon_Out_Pnd_Pnd_Fund_Payee.newFieldInGroup("pnd_Recon_Out_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);

        pnd_Recon_Out_Pnd_Pnd_Deductions = pnd_Recon_Out.newGroupArrayInGroup("pnd_Recon_Out_Pnd_Pnd_Deductions", "##DEDUCTIONS", new DbsArrayController(1, 
            10));
        pnd_Recon_Out_Pymnt_Ded_Cde = pnd_Recon_Out_Pnd_Pnd_Deductions.newFieldInGroup("pnd_Recon_Out_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3);
        pnd_Recon_Out_Pymnt_Ded_Amt = pnd_Recon_Out_Pnd_Pnd_Deductions.newFieldInGroup("pnd_Recon_Out_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Recon_Out_Pymnt_Ded_Payee_Cde = pnd_Recon_Out_Pnd_Pnd_Deductions.newFieldInGroup("pnd_Recon_Out_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8);
        pnd_Recon_Out_Pnd_Pymnt_Settlmnt_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Settlmnt_Dte", "#PYMNT-SETTLMNT-DTE", FieldType.STRING, 
            8);
        pnd_Recon_Out_Pnd_Pymnt_Check_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 8);
        pnd_Recon_Out_Pnd_Pymnt_Cycle_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Cycle_Dte", "#PYMNT-CYCLE-DTE", FieldType.STRING, 8);
        pnd_Recon_Out_Pnd_Pymnt_Acctg_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Acctg_Dte", "#PYMNT-ACCTG-DTE", FieldType.STRING, 8);
        pnd_Recon_Out_Pnd_Pymnt_Ia_Issue_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Ia_Issue_Dte", "#PYMNT-IA-ISSUE-DTE", FieldType.STRING, 
            8);
        pnd_Recon_Out_Pnd_Pymnt_Intrfce_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Intrfce_Dte", "#PYMNT-INTRFCE-DTE", FieldType.STRING, 
            8);
        pnd_Recon_Out_Cntrct_Orgn_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Recon_Out_Cntrct_Type_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        pnd_Recon_Out_Cntrct_Payee_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Recon_Out_Cntrct_Option_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Recon_Out_Cntrct_Mode_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pnd_Recon_Out_Pymnt_Pay_Type_Req_Ind = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Recon_Out_Cntrct_Pymnt_Type_Ind = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Recon_Out_Cntrct_Sttlmnt_Type_Ind = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Recon_Out_Cntrct_Annty_Ins_Type = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        pnd_Recon_Out_Pymnt_Suspend_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 1);
        pnd_Recon_Out_Pnd_Pymnt_Suspend_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pymnt_Suspend_Dte", "#PYMNT-SUSPEND-DTE", FieldType.STRING, 
            6);

        pnd_Recon_Out__R_Field_10 = pnd_Recon_Out.newGroupInGroup("pnd_Recon_Out__R_Field_10", "REDEFINE", pnd_Recon_Out_Pnd_Pymnt_Suspend_Dte);
        pnd_Recon_Out_Pnd_Pend_Dte = pnd_Recon_Out__R_Field_10.newFieldInGroup("pnd_Recon_Out_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);
        pnd_Recon_Out_Pnd_Pnd_Pda_Count = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Pda_Count", "##PDA-COUNT", FieldType.NUMERIC, 2);
        pnd_Recon_Out_Pnd_Pnd_This_Pymnt = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_This_Pymnt", "##THIS-PYMNT", FieldType.NUMERIC, 2);
        pnd_Recon_Out_Pnd_Pnd_Nbr_Of_Pymnts = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Pnd_Nbr_Of_Pymnts", "##NBR-OF-PYMNTS", FieldType.NUMERIC, 
            2);
        pnd_Recon_Out_Global_Country = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Recon_Out_Error_Code_1 = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Error_Code_1", "ERROR-CODE-1", FieldType.STRING, 1);
        pnd_Recon_Out_Error_Code_2 = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Error_Code_2", "ERROR-CODE-2", FieldType.STRING, 1);
        pnd_Recon_Out_Pnd_Dod = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Dod", "#DOD", FieldType.NUMERIC, 6);
        pnd_Recon_Out_Pnd_Final_Per_Pay_Dte = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Pnd_Final_Per_Pay_Dte", "#FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        pnd_Recon_Out_Ia_Orgn_Cde = pnd_Recon_Out.newFieldInGroup("pnd_Recon_Out_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 2);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Cntrct_Pend_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_DTE");
        registerRecord(vw_cpr);

        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_11", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cpr_Ppcn_Nbr = pnd_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Ppcn_Nbr", "#CPR-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cpr_Payee = pnd_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Key_Pnd_Cpr_Payee", "#CPR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Override_Ind = localVariables.newFieldInRecord("pnd_Override_Ind", "#OVERRIDE-IND", FieldType.STRING, 1);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Pend_Cnt = localVariables.newFieldInRecord("pnd_Pend_Cnt", "#PEND-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Dod_1 = localVariables.newFieldInRecord("pnd_Dod_1", "#DOD-1", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap701r() throws Exception
    {
        super("Iaap701r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 150 PS = 0
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 GTN-PDA-E
        while (condition(getWorkFiles().read(1, gtn_Pda_E)))
        {
            if (condition(gtn_Pda_E_Current_Mode.notEquals("1")))                                                                                                         //Natural: IF GTN-PDA-E.CURRENT-MODE NE '1'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gtn_Pda_E_Cntrct_Ppcn_Nbr.notEquals(pnd_Recon_Out_Cntrct_Ppcn_Nbr)))                                                                            //Natural: IF GTN-PDA-E.CNTRCT-PPCN-NBR NE #RECON-OUT.CNTRCT-PPCN-NBR
            {
                                                                                                                                                                          //Natural: PERFORM GET-DOD
                sub_Get_Dod();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recon_Out.setValuesByName(gtn_Pda_E);                                                                                                                     //Natural: MOVE BY NAME GTN-PDA-E TO #RECON-OUT
            pnd_Recon_Out_Pnd_Pymnt_Ia_Issue_Dte.setValueEdited(gtn_Pda_E_Pymnt_Ia_Issue_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED PYMNT-IA-ISSUE-DTE ( EM = YYYYMMDD ) TO #PYMNT-IA-ISSUE-DTE
            pnd_Recon_Out_Pnd_Pymnt_Foreign_Cde.setValue(gtn_Pda_E_Pymnt_Foreign_Cde.getValue(1));                                                                        //Natural: ASSIGN #PYMNT-FOREIGN-CDE := PYMNT-FOREIGN-CDE ( 1 )
            if (condition(gtn_Pda_E_Pymnt_Suspend_Dte.greater(getZero())))                                                                                                //Natural: IF PYMNT-SUSPEND-DTE GT 0
            {
                pnd_Recon_Out_Pnd_Pymnt_Suspend_Dte.setValueEdited(gtn_Pda_E_Pymnt_Suspend_Dte,new ReportEditMask("YYYYMM"));                                             //Natural: MOVE EDITED PYMNT-SUSPEND-DTE ( EM = YYYYMM ) TO #PYMNT-SUSPEND-DTE
                if (condition(gtn_Pda_E_Pymnt_Suspend_Cde.notEquals("A") || gtn_Pda_E_Pnd_Payee_N.notEquals(2)))                                                          //Natural: IF GTN-PDA-E.PYMNT-SUSPEND-CDE NE 'A' OR #PAYEE-N NE 02
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.callnat(Iaan702r.class , getCurrentProcessState(), gtn_Pda_E_Cntrct_Ppcn_Nbr, gtn_Pda_E_Pnd_Payee_N, pnd_Recon_Out_Pnd_Dod,                   //Natural: CALLNAT 'IAAN702R' GTN-PDA-E.CNTRCT-PPCN-NBR GTN-PDA-E.#PAYEE-N #DOD #DOD-1 GTN-PDA-E.CNTRCT-MODE-CDE GTN-PDA-E.PYMNT-SUSPEND-CDE #PEND-DTE
                        pnd_Dod_1, gtn_Pda_E_Cntrct_Mode_Cde, gtn_Pda_E_Pymnt_Suspend_Cde, pnd_Recon_Out_Pnd_Pend_Dte);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recon_Out_Pnd_Pymnt_Settlmnt_Dte.setValueEdited(gtn_Pda_E_Pymnt_Settlmnt_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #PYMNT-SETTLMNT-DTE
            pnd_Recon_Out_Pnd_Pymnt_Check_Dte.setValueEdited(gtn_Pda_E_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #PYMNT-CHECK-DTE
            pnd_Recon_Out_Pnd_Pymnt_Cycle_Dte.setValueEdited(gtn_Pda_E_Pymnt_Cycle_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = YYYYMMDD ) TO #PYMNT-CYCLE-DTE
            pnd_Recon_Out_Pnd_Pymnt_Acctg_Dte.setValueEdited(gtn_Pda_E_Pymnt_Acctg_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO #PYMNT-ACCTG-DTE
            pnd_Recon_Out_Pnd_Pymnt_Intrfce_Dte.setValueEdited(gtn_Pda_E_Pymnt_Intrfce_Dte,new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED PYMNT-INTRFCE-DTE ( EM = YYYYMMDD ) TO #PYMNT-INTRFCE-DTE
            pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Cde.getValue("*").setValue(gtn_Pda_E_Pnd_Pnd_Fund_Settl_Cde.getValue("*"));                                                  //Natural: ASSIGN #RECON-OUT.##FUND-SETTL-CDE ( * ) := GTN-PDA-E.##FUND-SETTL-CDE ( * )
            pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Amt.getValue("*").setValue(gtn_Pda_E_Pnd_Pnd_Fund_Settl_Amt.getValue("*"));                                                  //Natural: ASSIGN #RECON-OUT.##FUND-SETTL-AMT ( * ) := GTN-PDA-E.##FUND-SETTL-AMT ( * )
            pnd_Recon_Out_Pnd_Pnd_Fund_Settl_Dpi_Amt.getValue("*").setValue(gtn_Pda_E_Pnd_Pnd_Fund_Settl_Dpi_Amt.getValue("*"));                                          //Natural: ASSIGN #RECON-OUT.##FUND-SETTL-DPI-AMT ( * ) := GTN-PDA-E.##FUND-SETTL-DPI-AMT ( * )
            pnd_Recon_Out_Pnd_Pnd_Fund_Dpi_Amt.getValue("*").setValue(gtn_Pda_E_Pnd_Pnd_Fund_Dpi_Amt.getValue("*"));                                                      //Natural: ASSIGN #RECON-OUT.##FUND-DPI-AMT ( * ) := GTN-PDA-E.##FUND-DPI-AMT ( * )
            pnd_Recon_Out_Inv_Acct_Cde.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Cde.getValue("*"));                                                                      //Natural: ASSIGN #RECON-OUT.INV-ACCT-CDE ( * ) := GTN-PDA-E.INV-ACCT-CDE ( * )
            pnd_Recon_Out_Inv_Acct_Settl_Amt.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Settl_Amt.getValue("*"));                                                          //Natural: ASSIGN #RECON-OUT.INV-ACCT-SETTL-AMT ( * ) := GTN-PDA-E.INV-ACCT-SETTL-AMT ( * )
            pnd_Recon_Out_Inv_Acct_Cntrct_Amt.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Cntrct_Amt.getValue("*"));                                                        //Natural: ASSIGN #RECON-OUT.INV-ACCT-CNTRCT-AMT ( * ) := GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( * )
            pnd_Recon_Out_Inv_Acct_Dvdnd_Amt.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Dvdnd_Amt.getValue("*"));                                                          //Natural: ASSIGN #RECON-OUT.INV-ACCT-DVDND-AMT ( * ) := GTN-PDA-E.INV-ACCT-DVDND-AMT ( * )
            pnd_Recon_Out_Inv_Acct_Unit_Value.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Unit_Value.getValue("*"));                                                        //Natural: ASSIGN #RECON-OUT.INV-ACCT-UNIT-VALUE ( * ) := GTN-PDA-E.INV-ACCT-UNIT-VALUE ( * )
            pnd_Recon_Out_Inv_Acct_Unit_Qty.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Unit_Qty.getValue("*"));                                                            //Natural: ASSIGN #RECON-OUT.INV-ACCT-UNIT-QTY ( * ) := GTN-PDA-E.INV-ACCT-UNIT-QTY ( * )
            pnd_Recon_Out_Inv_Acct_Valuat_Period.getValue("*").setValue(gtn_Pda_E_Inv_Acct_Valuat_Period.getValue("*"));                                                  //Natural: ASSIGN #RECON-OUT.INV-ACCT-VALUAT-PERIOD ( * ) := GTN-PDA-E.INV-ACCT-VALUAT-PERIOD ( * )
            pnd_Recon_Out_Pymnt_Ded_Cde.getValue("*").setValue(gtn_Pda_E_Pymnt_Ded_Cde.getValue("*"));                                                                    //Natural: ASSIGN #RECON-OUT.PYMNT-DED-CDE ( * ) := GTN-PDA-E.PYMNT-DED-CDE ( * )
            pnd_Recon_Out_Pymnt_Ded_Amt.getValue("*").setValue(gtn_Pda_E_Pymnt_Ded_Amt.getValue("*"));                                                                    //Natural: ASSIGN #RECON-OUT.PYMNT-DED-AMT ( * ) := GTN-PDA-E.PYMNT-DED-AMT ( * )
            pnd_Recon_Out_Pymnt_Ded_Payee_Cde.getValue("*").setValue(gtn_Pda_E_Pymnt_Ded_Payee_Cde.getValue("*"));                                                        //Natural: ASSIGN #RECON-OUT.PYMNT-DED-PAYEE-CDE ( * ) := GTN-PDA-E.PYMNT-DED-PAYEE-CDE ( * )
                                                                                                                                                                          //Natural: PERFORM GET-FINAL-PER-PAY-DTE
            sub_Get_Final_Per_Pay_Dte();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, pnd_Recon_Out);                                                                                                                //Natural: WRITE WORK FILE 2 #RECON-OUT
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            if (condition(gtn_Pda_E_Pymnt_Suspend_Cde.equals("0") || gtn_Pda_E_Pymnt_Suspend_Cde.equals(" ")))                                                            //Natural: IF GTN-PDA-E.PYMNT-SUSPEND-CDE = '0' OR = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pend_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #PEND-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL RECORDS COUNT:",pnd_Cnt);                                                                                                            //Natural: WRITE 'TOTAL RECORDS COUNT:' #CNT
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL PEND    COUNT:",pnd_Pend_Cnt);                                                                                                       //Natural: WRITE 'TOTAL PEND    COUNT:' #PEND-CNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FINAL-PER-PAY-DTE
    }
    private void sub_Get_Dod() throws Exception                                                                                                                           //Natural: GET-DOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Recon_Out_Pnd_Dod.reset();                                                                                                                                    //Natural: RESET #DOD
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM GTN-PDA-E.CNTRCT-PPCN-NBR
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", gtn_Pda_E_Cntrct_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.notEquals(gtn_Pda_E_Cntrct_Ppcn_Nbr)))                                                                               //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR NE GTN-PDA-E.CNTRCT-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.greater(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte)))                                                             //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT CNTRCT-SCND-ANNT-DOD-DTE
            {
                pnd_Recon_Out_Pnd_Dod.setValue(iaa_Cntrct_Cntrct_First_Annt_Dod_Dte);                                                                                     //Natural: ASSIGN #DOD := CNTRCT-FIRST-ANNT-DOD-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Recon_Out_Pnd_Dod.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                                                      //Natural: ASSIGN #DOD := CNTRCT-SCND-ANNT-DOD-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Final_Per_Pay_Dte() throws Exception                                                                                                             //Natural: GET-FINAL-PER-PAY-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Recon_Out_Pnd_Final_Per_Pay_Dte.reset();                                                                                                                      //Natural: RESET #FINAL-PER-PAY-DTE
        pnd_Cpr_Key_Pnd_Cpr_Ppcn_Nbr.setValue(gtn_Pda_E_Cntrct_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #CPR-PPCN-NBR := GTN-PDA-E.CNTRCT-PPCN-NBR
        pnd_Cpr_Key_Pnd_Cpr_Payee.setValue(gtn_Pda_E_Pnd_Payee_N);                                                                                                        //Natural: ASSIGN #CPR-PAYEE := GTN-PDA-E.#PAYEE-N
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CPR-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cpr.readNextRow("READ03")))
        {
            if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cpr_Key_Pnd_Cpr_Ppcn_Nbr) || cpr_Cntrct_Part_Payee_Cde.notEquals(pnd_Cpr_Key_Pnd_Cpr_Payee)))            //Natural: IF CPR.CNTRCT-PART-PPCN-NBR NE #CPR-PPCN-NBR OR CPR.CNTRCT-PART-PAYEE-CDE NE #CPR-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recon_Out_Pnd_Final_Per_Pay_Dte.setValue(cpr_Cntrct_Final_Per_Pay_Dte);                                                                                   //Natural: ASSIGN #FINAL-PER-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=150 PS=0");
    }
}
