/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:42 PM
**        * FROM NATURAL PROGRAM : Iaap324
************************************************************
**        * FILE NAME            : Iaap324.java
**        * CLASS NAME           : Iaap324
**        * INSTANCE NAME        : Iaap324
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: NET CHANGE MAILING LABEL INDICATOR
**SAG SYSTEM: IASTPA
************************************************************************
* PROGRAM  : IAAP324
* SYSTEM   : IASTPA
* TITLE    : MANDATORY STATE CHANGES EXTRACT FILE
* GENERATED: FEB 05,96 AT 04:59 PM
* FUNCTION : THIS PROGRAM WILL READ THE NET CHANGE FILE.
*          |
*          | IF ANY OF THE CONTRACTS OF A PIN HAVE A MSTATE CHANGE FLAG
*          | SET TO ONE THEN EXTRACT ALL THE CONTRACTS FOR THE PIN.
*          |
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* SEP 26 05  A. YOUNG  - ADDED MARYLAND AS A NEW MANDATORY STATE.
*                      - ADDED TEST SWITCH LOGIC WHICH WILL POPULATE
*                        VARIABLES WITH DATA NEEDED TO FORCE MANDATORY
*                        LETTERS FOR EVERY OTHER RECORD.
*                        IAAA320.CURR-STATE-RSDNCY-CDE := '23'
*                        #MSTATE-CHG                   := 1
*                        IF #TEST IS TRUE.
*                        #TEST MUST BE SET TO 'FALSE' IN PROD.!!!
*
* 04/25/17    SAI K      PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap324 extends BLNatBase
{
    // Data Areas
    private PdaIaaa323 pdaIaaa323;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Bypass_Cnt;
    private DbsField pnd_Prev_Pin;
    private DbsField pnd_Mstate_Chg;
    private DbsField pnd_Test;
    private DbsField pnd_Pin_Cnt;
    private DbsField pnd_Pin_Cnt_Test;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa323 = new PdaIaaa323(localVariables);

        // Local Variables
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 7);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.NUMERIC, 7);
        pnd_Bypass_Cnt = localVariables.newFieldInRecord("pnd_Bypass_Cnt", "#BYPASS-CNT", FieldType.NUMERIC, 7);
        pnd_Prev_Pin = localVariables.newFieldInRecord("pnd_Prev_Pin", "#PREV-PIN", FieldType.NUMERIC, 12);
        pnd_Mstate_Chg = localVariables.newFieldInRecord("pnd_Mstate_Chg", "#MSTATE-CHG", FieldType.NUMERIC, 1);
        pnd_Test = localVariables.newFieldInRecord("pnd_Test", "#TEST", FieldType.BOOLEAN, 1);
        pnd_Pin_Cnt = localVariables.newFieldInRecord("pnd_Pin_Cnt", "#PIN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Pin_Cnt_Test = localVariables.newFieldInRecord("pnd_Pin_Cnt_Test", "#PIN-CNT-TEST", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Read_Cnt.setInitialValue(0);
        pnd_Write_Cnt.setInitialValue(0);
        pnd_Bypass_Cnt.setInitialValue(0);
        pnd_Prev_Pin.setInitialValue(0);
        pnd_Mstate_Chg.setInitialValue(0);
        pnd_Test.setInitialValue(false);
        pnd_Pin_Cnt.setInitialValue(0);
        pnd_Pin_Cnt_Test.setInitialValue(2);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap324() throws Exception
    {
        super("Iaap324");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT ( 0 ) LS = 132 PS = 66;//Natural: FORMAT ( 1 ) LS = 132 PS = 66;//Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        //*  MAIN PROGRAM LOOP
        //*  <<< IMPORTANT >>> #TEST MUST BE SET TO 'FALSE' IN PRODUCTION!!!
        //*                    COMMENT OUT THE FOLLOWING LINE!!!
        //*  #TEST := TRUE                                           /* AY 09/26/05
        READ_NETCHG_FILE:                                                                                                                                                 //Natural: READ WORK FILE 1 IAAA320
        while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320())))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: AT END OF DATA;//Natural: ADD 1 TO #READ-CNT
            if (condition(pnd_Prev_Pin.notEquals(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                                       //Natural: IF #PREV-PIN NE IAAA320.PIN-NBR
            {
                pnd_Prev_Pin.setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                                   //Natural: ASSIGN #PREV-PIN := IAAA320.PIN-NBR
                pnd_Mstate_Chg.setValue(pdaIaaa323.getIaaa320_State_Rsdncy_Chg_Flag());                                                                                   //Natural: ASSIGN #MSTATE-CHG := IAAA320.STATE-RSDNCY-CHG-FLAG
                //*  AY 09/06/05
                pnd_Pin_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #PIN-CNT
            }                                                                                                                                                             //Natural: END-IF
            //*  AY 09/26/05
            if (condition(pnd_Test.getBoolean() && pnd_Pin_Cnt_Test.lessOrEqual(100)))                                                                                    //Natural: IF #TEST AND #PIN-CNT-TEST LE 100
            {
                //*  TEST MANDATORY STATE
                //*  STATE RESIDENCY CHGS
                if (condition(pnd_Pin_Cnt.equals(pnd_Pin_Cnt_Test)))                                                                                                      //Natural: IF #PIN-CNT = #PIN-CNT-TEST
                {
                    pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde().setValue("23");                                                                                         //Natural: ASSIGN IAAA320.CURR-STATE-RSDNCY-CDE := '23'
                    //*  #MSTATE-CHG POPULATES IAAA320.STATE-RSDNCY-CHG-FLAG.
                    pnd_Mstate_Chg.setValue(1);                                                                                                                           //Natural: ASSIGN #MSTATE-CHG := 1
                    //*       IAAA320.TAX-ST-CHG-FLAG       := 1        /* TAX AMOUNT CHANGES
                    getReports().display(0, "=",                                                                                                                          //Natural: DISPLAY '=' #READ-CNT '=' #PIN-CNT '=' IAAA320.PIN-NBR '=' IAAA320.PRIOR-STATE-RSDNCY-CDE '=' IAAA320.CURR-STATE-RSDNCY-CDE 'IAAA320.STATE-RSDNCY-CHG-FLAG' #MSTATE-CHG
                    		pnd_Read_Cnt,"=",
                    		pnd_Pin_Cnt,"=",
                    		pdaIaaa323.getIaaa320_Pin_Nbr(),"=",
                    		pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde(),"=",
                    		pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde(),"IAAA320.STATE-RSDNCY-CHG-FLAG",
                    		pnd_Mstate_Chg);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_NETCHG_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_NETCHG_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*         '='                             IAAA320.STATE-RSDNCY-CHG-FLAG
                    //*         '='                             IAAA320.TAX-ST-CHG-FLAG
                    pnd_Pin_Cnt_Test.compute(new ComputeParameters(false, pnd_Pin_Cnt_Test), pnd_Pin_Cnt.add(2));                                                         //Natural: ASSIGN #PIN-CNT-TEST := #PIN-CNT + 2
                }                                                                                                                                                         //Natural: END-IF
                //*  AY 09/26/05
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Mstate_Chg.equals(1)))                                                                                                                      //Natural: IF #MSTATE-CHG = 1
            {
                pnd_Write_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WRITE-CNT
                pdaIaaa323.getIaaa320_State_Rsdncy_Chg_Flag().setValue(pnd_Mstate_Chg);                                                                                   //Natural: ASSIGN IAAA320.STATE-RSDNCY-CHG-FLAG := #MSTATE-CHG
                //*  FINAL NET CHANGE FILE
                getWorkFiles().write(2, false, pdaIaaa323.getIaaa320());                                                                                                  //Natural: WRITE WORK FILE 2 IAAA320
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bypass_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BYPASS-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READ_NETCHG_FILE_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "READ_NETCHG_FILE");                                                                          //Natural: ESCAPE BOTTOM ( READ-NETCHG-FILE. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*   WRITE CONTROL REPORT
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 41T 'IA NET CHANGE FILE SELECTION AND CONTROL PROCESS' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *DATX 46T 'MANDATORY STATE CHANGE EXTRACT REPORT' 120T *TIMX
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS READ:               ",pnd_Read_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"TOTAL BYPASSED (NO CHANGE):       ",pnd_Bypass_Cnt,  //Natural: WRITE ( 1 ) /// / 'TOTAL RECORDS READ:               ' #READ-CNT ( EM = -Z,ZZZ,ZZ9 ) / 'TOTAL BYPASSED (NO CHANGE):       ' #BYPASS-CNT ( EM = -Z,ZZZ,ZZ9 ) / 'TOTAL MANDATORY RECORDS WRITTEN:  ' #WRITE-CNT ( EM = -Z,ZZZ,ZZ9 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"TOTAL MANDATORY RECORDS WRITTEN:  ",pnd_Write_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66");
        Global.format(1, "LS=132 PS=66");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(41),"IA NET CHANGE FILE SELECTION AND CONTROL PROCESS",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(46),"MANDATORY STATE CHANGE EXTRACT REPORT",new 
            TabSetting(120),Global.getTIMX());

        getReports().setDisplayColumns(0, "=",
        		pnd_Read_Cnt,"=",
        		pnd_Pin_Cnt,"=",
        		pdaIaaa323.getIaaa320_Pin_Nbr(),"=",
        		pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde(),"=",
        		pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde(),"IAAA320.STATE-RSDNCY-CHG-FLAG",
        		pnd_Mstate_Chg);
    }
}
