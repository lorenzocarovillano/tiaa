/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:11 PM
**        * FROM NATURAL PROGRAM : Aiap019a
************************************************************
**        * FILE NAME            : Aiap019a.java
**        * CLASS NAME           : Aiap019a
**        * INSTANCE NAME        : Aiap019a
************************************************************
************************************************************************
* AIAP019A - TIAA/CREF IA TRANSFER SUMMARY
*  VERSION 1.1 - 9/30/99
* UPDATED FOR RATE EXPANSION - 02/16/05 L.W.
* 07/10/08  KCD  ROTH/99 RATES IMPLEMENTATION
*
* STARTING FROM 02/02/05, THE TRANSFER RECORD IN IAA-XFR-ACTRL-RCRD-VIEW
* IS WRITTEN ON NEW FORMAT WITH RATE EXPANSION.
* 10-08-12   DY  RBE PHASE 2 PROJECT; REF AS RBE2
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap019a extends BLNatBase
{
    // Data Areas
    private LdaAial0131 ldaAial0131;
    private LdaAial0192 ldaAial0192;
    private LdaAial019b ldaAial019b;
    private LdaAial019c ldaAial019c;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Cntrl_Sd_Cde;
    private DbsField pnd_Input_Begin_Date;

    private DbsGroup pnd_Input_Begin_Date__R_Field_1;
    private DbsField pnd_Input_Begin_Date_Pnd_Input_Begin_Date_A;

    private DbsGroup pnd_Input_Begin_Date__R_Field_2;
    private DbsField pnd_Input_Begin_Date_Pnd_Input_Begin_Year;
    private DbsField pnd_Input_Begin_Date_Pnd_Input_Begin_Month;
    private DbsField pnd_Input_Begin_Date_Pnd_Input_Begin_Day;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_4;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2;
    private DbsField pnd_First_Read;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        ldaAial0192 = new LdaAial0192();
        registerRecord(ldaAial0192);
        ldaAial019b = new LdaAial019b();
        registerRecord(ldaAial019b);
        ldaAial019c = new LdaAial019c();
        registerRecord(ldaAial019c);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_cntrl);

        pnd_Cntrl_Sd_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Sd_Cde", "#CNTRL-SD-CDE", FieldType.STRING, 2);
        pnd_Input_Begin_Date = localVariables.newFieldInRecord("pnd_Input_Begin_Date", "#INPUT-BEGIN-DATE", FieldType.NUMERIC, 8);

        pnd_Input_Begin_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Begin_Date__R_Field_1", "REDEFINE", pnd_Input_Begin_Date);
        pnd_Input_Begin_Date_Pnd_Input_Begin_Date_A = pnd_Input_Begin_Date__R_Field_1.newFieldInGroup("pnd_Input_Begin_Date_Pnd_Input_Begin_Date_A", "#INPUT-BEGIN-DATE-A", 
            FieldType.STRING, 8);

        pnd_Input_Begin_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Begin_Date__R_Field_2", "REDEFINE", pnd_Input_Begin_Date);
        pnd_Input_Begin_Date_Pnd_Input_Begin_Year = pnd_Input_Begin_Date__R_Field_2.newFieldInGroup("pnd_Input_Begin_Date_Pnd_Input_Begin_Year", "#INPUT-BEGIN-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Input_Begin_Date_Pnd_Input_Begin_Month = pnd_Input_Begin_Date__R_Field_2.newFieldInGroup("pnd_Input_Begin_Date_Pnd_Input_Begin_Month", "#INPUT-BEGIN-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Input_Begin_Date_Pnd_Input_Begin_Day = pnd_Input_Begin_Date__R_Field_2.newFieldInGroup("pnd_Input_Begin_Date_Pnd_Input_Begin_Day", "#INPUT-BEGIN-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Iaxfr_Actrl_Cntrct_Py = localVariables.newFieldInRecord("pnd_Iaxfr_Actrl_Cntrct_Py", "#IAXFR-ACTRL-CNTRCT-PY", FieldType.STRING, 12);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3 = localVariables.newGroupInRecord("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3", "REDEFINE", pnd_Iaxfr_Actrl_Cntrct_Py);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10", 
            "#IAXFR-ACTRL-CNTRCT-PY-A10", FieldType.STRING, 10);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number", 
            "#IAXFR-ACTRL-RECORD-NUMBER", FieldType.NUMERIC, 2);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_4 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_3.newGroupInGroup("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_4", "REDEFINE", 
            pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_4.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2", 
            "#IAXFR-ACTRL-RECORD-NUMBER-A2", FieldType.STRING, 2);
        pnd_First_Read = localVariables.newFieldInRecord("pnd_First_Read", "#FIRST-READ", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaAial0131.initializeValues();
        ldaAial0192.initializeValues();
        ldaAial019b.initializeValues();
        ldaAial019c.initializeValues();

        localVariables.reset();
        pnd_First_Read.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Aiap019a() throws Exception
    {
        super("Aiap019a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  RBE2
        //*  RBE2                                                                                                                                                         //Natural: FORMAT LS = 132 PS = 0 ES = ON
        getReports().write(1, "****  AIAP019A ****");                                                                                                                     //Natural: FORMAT ( 1 ) LS = 132 PS = 0 ES = ON;//Natural: WRITE ( 1 ) '****  AIAP019A ****'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, "=",pnd_Input_Begin_Date);                                                                                                                  //Natural: WRITE ( 1 ) '=' #INPUT-BEGIN-DATE
        if (Global.isEscape()) return;
        //*  FIND IAA-XFR-ACTRL-RCRD-VIEW                             /* RBE2
        //*  RBE2
        ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().startDatabaseFind                                                                                                    //Natural: FIND IAA-XFR-ACTRL-RCRD-VIEW2 WITH IAXFR-ACTRL-PRCSS-DTE = #INPUT-BEGIN-DATE
        (
        "FIND01",
        new Wc[] { new Wc("IAXFR_ACTRL_PRCSS_DTE", "=", pnd_Input_Begin_Date, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().readNextRow("FIND01", true)))
        {
            ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().setIfNotFoundControlFlag(false);
            //*    WITH IAXFR-ACTRL-PRCSS-DTE = 20121019 THRU 20121019
            //*    WITH IAXFR-ACTRL-PRCSS-DTE = 20130422
            if (condition(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().getAstCOUNTER().equals(0)))                                                                        //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO IA RECORDS FOUND ");                                                                                                            //Natural: WRITE 'NO IA RECORDS FOUND '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pnd_First_Read.equals("Y")))                                                                                                                    //Natural: IF #FIRST-READ = 'Y'
            {
                getReports().write(1, "SORTING AND RE-FORMATTING OF ADABAS SEQ RECORDS IS PROCEEDING");                                                                   //Natural: WRITE ( 1 ) 'SORTING AND RE-FORMATTING OF ADABAS SEQ RECORDS IS PROCEEDING'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Read.setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #FIRST-READ
            }                                                                                                                                                             //Natural: END-IF
            //*  RBE2
            pnd_Iaxfr_Actrl_Cntrct_Py.setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Cntrct_Py());                                                          //Natural: MOVE IAXFR-ACTRL-CNTRCT-PY TO #IAXFR-ACTRL-CNTRCT-PY
            //*  IF IAXFR-ACTRL-RECORD-NUMBER-A2 = '  '                   /* RBE2
            //*  RBE2
            if (condition(pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2.equals("  ")))                                                                       //Natural: IF #IAXFR-ACTRL-RECORD-NUMBER-A2 = '  '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area1().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1());                                         //Natural: MOVE IAXFR-ACTRL-FLD-1 TO #SA-AREA1
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area2().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_2());                                         //Natural: MOVE IAXFR-ACTRL-FLD-2 TO #SA-AREA2
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area3().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_3());                                         //Natural: MOVE IAXFR-ACTRL-FLD-3 TO #SA-AREA3
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area4().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_4());                                         //Natural: MOVE IAXFR-ACTRL-FLD-4 TO #SA-AREA4
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area5().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_5());                                         //Natural: MOVE IAXFR-ACTRL-FLD-5 TO #SA-AREA5
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area6().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_6());                                         //Natural: MOVE IAXFR-ACTRL-FLD-6 TO #SA-AREA6
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area7().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_7());                                         //Natural: MOVE IAXFR-ACTRL-FLD-7 TO #SA-AREA7
            //*  RBE2 FIX BEGINS >>
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area8().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_8());                                         //Natural: MOVE IAXFR-ACTRL-FLD-8 TO #SA-AREA8
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area9().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_9());                                         //Natural: MOVE IAXFR-ACTRL-FLD-9 TO #SA-AREA9
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area10().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_10());                                       //Natural: MOVE IAXFR-ACTRL-FLD-10 TO #SA-AREA10
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area11().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_11());                                       //Natural: MOVE IAXFR-ACTRL-FLD-11 TO #SA-AREA11
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area12().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_12());                                       //Natural: MOVE IAXFR-ACTRL-FLD-12 TO #SA-AREA12
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area13().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_13());                                       //Natural: MOVE IAXFR-ACTRL-FLD-13 TO #SA-AREA13
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area14().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_14());                                       //Natural: MOVE IAXFR-ACTRL-FLD-14 TO #SA-AREA14
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area15().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_15());                                       //Natural: MOVE IAXFR-ACTRL-FLD-15 TO #SA-AREA15
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area16().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_16());                                       //Natural: MOVE IAXFR-ACTRL-FLD-16 TO #SA-AREA16
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area17().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_17());                                       //Natural: MOVE IAXFR-ACTRL-FLD-17 TO #SA-AREA17
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area18().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_18());                                       //Natural: MOVE IAXFR-ACTRL-FLD-18 TO #SA-AREA18
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area19().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_19());                                       //Natural: MOVE IAXFR-ACTRL-FLD-19 TO #SA-AREA19
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area20().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_20());                                       //Natural: MOVE IAXFR-ACTRL-FLD-20 TO #SA-AREA20
            //*  MOVE IAXFR-ACTRL-CNTRCT-PY     TO #SA-KEY-CNTRCT-PY
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py().setValue(pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10);                                     //Natural: MOVE #IAXFR-ACTRL-CNTRCT-PY-A10 TO #SA-KEY-CNTRCT-PY
            //*  MOVE IAXFR-ACTRL-RECORD-NUMBER TO #SA-KEY-RECORD-NUMBER
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Record_Number().setValue(pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number);                                 //Natural: MOVE #IAXFR-ACTRL-RECORD-NUMBER TO #SA-KEY-RECORD-NUMBER
            //*  RBE2 FIX ENDS   <<
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Prcss_Dte());                             //Natural: MOVE IAXFR-ACTRL-PRCSS-DTE TO #SA-KEY-PRCSS-DTE
            //*  RBE2
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Sequence_Number());                             //Natural: MOVE SEQUENCE-NUMBER TO #SA-KEY-SEQUENCE-NUMBER
            //*  RBE2
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Logical_Transaction_Key());                //Natural: MOVE LOGICAL-TRANSACTION-KEY TO #SA-KEY-LOGC-TRANSACTION-KEY
            getWorkFiles().write(1, false, ldaAial019c.getPnd_Sorta_Record());                                                                                            //Natural: WRITE WORK FILE 1 #SORTA-RECORD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #SORTA-RECORD
        while (condition(getWorkFiles().read(1, ldaAial019c.getPnd_Sorta_Record())))
        {
            getSort().writeSortInData(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Contract_Payee(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key(),        //Natural: END-ALL
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Payment_Method(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area1(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area2(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area3(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area4(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area5(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area6(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area7(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area8(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area9(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area10(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area11(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area12(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area13(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area14(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area15(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area16(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area17(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area18(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area19(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area20(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Record_Number(), 
                ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  RBE2 FIX BEGINS >>
        //*  ADD #SA-KEY-LOGC-TRANSACTION-KEY AND #SA-KEY-SEQUENCE-NUMBER
        //*  TO THE SORT
        //*  ADD #SA-AREA8 THRU #SA-AREA20 ON THE SORT CLAUSE
        //*  RBE2 FIX ENDS   <<
        //*  RBE2
        //*  RBE2
        getSort().sortData(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Contract_Payee(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key(),                   //Natural: SORT BY #SA1-CONTRACT-PAYEE #SA-KEY-LOGC-TRANSACTION-KEY #SA1-PAYMENT-METHOD DESC #SA1-CURRENT-RECORD-NUMBER USING #SA-AREA1 #SA-AREA2 #SA-AREA3 #SA-AREA4 #SA-AREA5 #SA-AREA6 #SA-AREA7 #SA-AREA8 #SA-AREA9 #SA-AREA10 #SA-AREA11 #SA-AREA12 #SA-AREA13 #SA-AREA14 #SA-AREA15 #SA-AREA16 #SA-AREA17 #SA-AREA18 #SA-AREA19 #SA-AREA20 #SA-KEY-CNTRCT-PY #SA-KEY-RECORD-NUMBER #SA-KEY-PRCSS-DTE #SA-KEY-SEQUENCE-NUMBER
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Payment_Method(), "DESCENDING", ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number());
        SORT01:
        while (condition(getSort().readSortOutData(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Contract_Payee(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Payment_Method(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area1(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area2(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area3(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area4(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area5(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area6(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area7(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area8(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area9(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area10(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area11(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area12(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area13(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area14(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area15(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area16(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area17(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area18(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area19(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area20(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Record_Number(), 
            ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte(), ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number())))
        {
            getWorkFiles().write(1, false, ldaAial019c.getPnd_Sorta_Record());                                                                                            //Natural: WRITE WORK FILE 1 #SORTA-RECORD
            if (condition(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number().equals(0) || ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number().equals(50))) //Natural: IF ( #SA1-CURRENT-RECORD-NUMBER = 00 OR = 50 )
            {
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area1().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area1());                                      //Natural: MOVE #SA-AREA1 TO #SB-COMMON-DATA-AREA1
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2().setValue(" ");                                                                                 //Natural: MOVE ' ' TO #SB-COMMON-DATA-AREA2
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Contract_Payee().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Contract_Payee());                             //Natural: MOVE #SA1-CONTRACT-PAYEE TO #SBC2-CONTRACT-PAYEE
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Payment_Method().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Payment_Method());                             //Natural: MOVE #SA1-PAYMENT-METHOD TO #SBC2-PAYMENT-METHOD
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number());               //Natural: MOVE #SA1-CURRENT-RECORD-NUMBER TO #SBC2-CURRENT-RECORD-NUMBER
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Final_Record_Number().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Final_Record_Number());                   //Natural: MOVE #SA1-FINAL-RECORD-NUMBER TO #SBC2-FINAL-RECORD-NUMBER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa1_Current_Record_Number());               //Natural: MOVE #SA1-CURRENT-RECORD-NUMBER TO #SBC1-CURRENT-RECORD-NUMBER
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area1());                                      //Natural: MOVE #SA-AREA1 TO #SB-COMMON-DATA-AREA2
            }                                                                                                                                                             //Natural: END-IF
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area1().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area2());                                                //Natural: MOVE #SA-AREA2 TO #SB-ARRAY-AREA1
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area2().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area3());                                                //Natural: MOVE #SA-AREA3 TO #SB-ARRAY-AREA2
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area3().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area4());                                                //Natural: MOVE #SA-AREA4 TO #SB-ARRAY-AREA3
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area4().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area5());                                                //Natural: MOVE #SA-AREA5 TO #SB-ARRAY-AREA4
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area5().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area6());                                                //Natural: MOVE #SA-AREA6 TO #SB-ARRAY-AREA5
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area6().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area7());                                                //Natural: MOVE #SA-AREA7 TO #SB-ARRAY-AREA6
            //*  RBE2 FIX BEGINS >>
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area7().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area8());                                                //Natural: MOVE #SA-AREA8 TO #SB-ARRAY-AREA7
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area8().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area9());                                                //Natural: MOVE #SA-AREA9 TO #SB-ARRAY-AREA8
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area9().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area10());                                               //Natural: MOVE #SA-AREA10 TO #SB-ARRAY-AREA9
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area10().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area11());                                              //Natural: MOVE #SA-AREA11 TO #SB-ARRAY-AREA10
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area11().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area12());                                              //Natural: MOVE #SA-AREA12 TO #SB-ARRAY-AREA11
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area12().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area13());                                              //Natural: MOVE #SA-AREA13 TO #SB-ARRAY-AREA12
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area13().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area14());                                              //Natural: MOVE #SA-AREA14 TO #SB-ARRAY-AREA13
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area14().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area15());                                              //Natural: MOVE #SA-AREA15 TO #SB-ARRAY-AREA14
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area15().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area16());                                              //Natural: MOVE #SA-AREA16 TO #SB-ARRAY-AREA15
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area16().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area17());                                              //Natural: MOVE #SA-AREA17 TO #SB-ARRAY-AREA16
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area17().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area18());                                              //Natural: MOVE #SA-AREA18 TO #SB-ARRAY-AREA17
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area18().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area19());                                              //Natural: MOVE #SA-AREA19 TO #SB-ARRAY-AREA18
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area19().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Area20());                                              //Natural: MOVE #SA-AREA20 TO #SB-ARRAY-AREA19
            //*  RBE2 FIX ENDS   <<
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Cntrct_Py());                                      //Natural: MOVE #SA-KEY-CNTRCT-PY TO #SB-KEY-CNTRCT-PY
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Record_Number().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Record_Number());                              //Natural: MOVE #SA-KEY-RECORD-NUMBER TO #SB-KEY-RECORD-NUMBER
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Prcss_Dte());                                      //Natural: MOVE #SA-KEY-PRCSS-DTE TO #SB-KEY-PRCSS-DTE
            //*  RBE2
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Sequence_Number());                          //Natural: MOVE #SA-KEY-SEQUENCE-NUMBER TO #SB-KEY-SEQUENCE-NUMBER
            //*  RBE2
            //*  RBE2
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key().setValue(ldaAial019c.getPnd_Sorta_Record_Pnd_Sa_Key_Logc_Transaction_Key());                //Natural: MOVE #SA-KEY-LOGC-TRANSACTION-KEY TO #SB-KEY-LOGC-TRANSACTION-KEY
            getWorkFiles().write(2, false, ldaAial019b.getPnd_Sortb_Record());                                                                                            //Natural: WRITE WORK FILE 2 #SORTB-RECORD
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        getReports().write(1, "*** LEAVING AIAP019A ***");                                                                                                                //Natural: WRITE ( 1 ) '*** LEAVING AIAP019A ***'
        if (Global.isEscape()) return;
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Cntrl_Sd_Cde.setValue("DC");                                                                                                                                  //Natural: MOVE 'DC' TO #CNTRL-SD-CDE
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-SD-CDE
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Sd_Cde, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cntrl.readNextRow("READ02")))
        {
            //*  WRITE 'CNTRL = ' CNTRL
            pnd_Input_Begin_Date_Pnd_Input_Begin_Date_A.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #INPUT-BEGIN-DATE-A
            //*  MOVE #INPUT-BEGIN-DATE TO #INPUT-END-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WRITE 'BEGIN-DATE' #INPUT-BEGIN-DATE
        //*  COMPRESS #INPUT-BEGIN-MONTH '/' #INPUT-BEGIN-DAY '/'
        //*   #INPUT-BEGIN-YEAR INTO #DATE1 LEAVING NO SPACE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=0 ES=ON");
        Global.format(1, "LS=132 PS=0 ES=ON");
    }
}
