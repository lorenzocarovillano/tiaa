/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:49 PM
**        * FROM NATURAL PROGRAM : Iaap804
************************************************************
**        * FILE NAME            : Iaap804.java
**        * CLASS NAME           : Iaap804
**        * INSTANCE NAME        : Iaap804
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAP804  : READS NAME CHECK/CORRESP FLAT FILE      *
*      DATE     -  02/15      REMOVE VACATION CHECK/CORRESP FLAT FILE *
*                             WRITES ONE FILE WITH DUMMY VACATION     *
*                             RECORD.                                 *
*
*  HISTORY:
*
*  3/2015       - BYPASS VACATION FILE READ - COR/NAAD DECOMMISSION.
*                 VACATION FILE WILL NO LONGER BE MAINTAINED.
*                 SC 3/2015 FOR CHANGES.
*  04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap804 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Name;
    private DbsField pnd_Name_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee;

    private DbsGroup pnd_Name__R_Field_1;
    private DbsField pnd_Name_Cntrct_Nmbr;
    private DbsField pnd_Name_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name__R_Field_2;
    private DbsField pnd_Name_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Addrss_Lne_1_K;
    private DbsField pnd_Name_Addrss_Lne_2_K;
    private DbsField pnd_Name_Addrss_Lne_3_K;
    private DbsField pnd_Name_Addrss_Lne_4_K;
    private DbsField pnd_Name_Addrss_Lne_5_K;
    private DbsField pnd_Name_Addrss_Lne_6_K;

    private DbsGroup pnd_Name__R_Field_3;
    private DbsField pnd_Name_Addrss_Lne;
    private DbsField pnd_Name_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name__R_Field_4;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_K;

    private DbsGroup pnd_Name__R_Field_5;
    private DbsField pnd_Name_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_Eft_Status_Ind_K;
    private DbsField pnd_Name_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Pnd_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name__R_Field_6;
    private DbsField pnd_Name_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Addrss_Lne_1_R;
    private DbsField pnd_Name_Addrss_Lne_2_R;
    private DbsField pnd_Name_Addrss_Lne_3_R;
    private DbsField pnd_Name_Addrss_Lne_4_R;
    private DbsField pnd_Name_Addrss_Lne_5_R;
    private DbsField pnd_Name_Addrss_Lne_6_R;
    private DbsField pnd_Name_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name__R_Field_7;
    private DbsField pnd_Name_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Pnd_Rest_Of_Record_R;

    private DbsGroup pnd_Name__R_Field_8;
    private DbsField pnd_Name_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_Eft_Status_Ind_R;
    private DbsField pnd_Name_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Addrss_Geographic_Cde_R;

    private DbsGroup pnd_Vacation;
    private DbsField pnd_Vacation_V_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Vacation_V_Cntrct_Payee;

    private DbsGroup pnd_Vacation__R_Field_9;
    private DbsField pnd_Vacation_V_Cntrct_Nmbr;
    private DbsField pnd_Vacation_V_Cntrct_Payee_Cde;
    private DbsField pnd_Vacation_Pnd_V_Cntrct_Name_Add_K;

    private DbsGroup pnd_Vacation__R_Field_10;
    private DbsField pnd_Vacation_V_Cntrct_Name_Free_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_1_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_2_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_3_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_4_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_5_K;
    private DbsField pnd_Vacation_V_Addrss_Lne_6_K;

    private DbsGroup pnd_Vacation__R_Field_11;
    private DbsField pnd_Vacation_Addrss_Lne_V;
    private DbsField pnd_Vacation_V_Addrss_Postal_Data_K;

    private DbsGroup pnd_Vacation__R_Field_12;
    private DbsField pnd_Vacation_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Vacation_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Vacation_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Vacation_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Vacation_Pnd_V_Rest_Of_Record_K;

    private DbsGroup pnd_Vacation__R_Field_13;
    private DbsField pnd_Vacation_V_Addrss_Type_Cde_K;
    private DbsField pnd_Vacation_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Vacation_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Vacation_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Vacation_V_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Vacation_V_Eft_Status_Ind_K;
    private DbsField pnd_Vacation_V_Checking_Saving_Cde_K;
    private DbsField pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Vacation_V_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Vacation_V_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Vacation_V_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Vacation_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Vacation_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Vacation_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Vacation_V_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Vacation_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Vacation_Pnd_V_Cntrct_Name_Add_R;

    private DbsGroup pnd_Vacation__R_Field_14;
    private DbsField pnd_Vacation_V_Cntrct_Name_Free_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_1_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_2_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_3_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_4_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_5_R;
    private DbsField pnd_Vacation_V_Addrss_Lne_6_R;
    private DbsField pnd_Vacation_V_Addrss_Postal_Data_R;

    private DbsGroup pnd_Vacation__R_Field_15;
    private DbsField pnd_Vacation_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Vacation_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Vacation_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Vacation_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Vacation_Pnd_V_Rest_Of_Record_R;

    private DbsGroup pnd_Vacation__R_Field_16;
    private DbsField pnd_Vacation_V_Addrss_Type_Cde_R;
    private DbsField pnd_Vacation_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Vacation_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Vacation_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Vacation_V_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Vacation_V_Eft_Status_Ind_R;
    private DbsField pnd_Vacation_V_Checking_Saving_Cde_R;
    private DbsField pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Vacation_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Vacation_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Vacation_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Vacation_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Vacation_V_Addrss_Geographic_Cde_R;

    private DbsGroup pnd_Legal_Override;
    private DbsField pnd_Legal_Override_Lo_Record_Key;

    private DbsGroup pnd_Legal_Override__R_Field_17;
    private DbsField pnd_Legal_Override_Lo_Contract_Number;
    private DbsField pnd_Legal_Override_Lo_Payee_Code;
    private DbsField pnd_Legal_Override_Lo_Sequence_No;

    private DbsGroup pnd_Legal_Override_Lo_Common_Data;
    private DbsField pnd_Legal_Override_Lo_Status_Code;
    private DbsField pnd_Legal_Override_Lo_Effective_Date;
    private DbsField pnd_Legal_Override_Lo_Entry_Date;
    private DbsField pnd_Legal_Override_Lo_Entry_Operator;
    private DbsField pnd_Legal_Override_Lo_Activity_Date;
    private DbsField pnd_Legal_Override_Lo_Activity_Operator;

    private DbsGroup pnd_Legal_Override__R_Field_18;
    private DbsField pnd_Legal_Override__Filler1;
    private DbsField pnd_Legal_Override_Pnd_Global_Cntry;
    private DbsField pnd_Legal_Override_Lo_State_Code;
    private DbsField pnd_Legal_Override_Lo_Ia_State_Code;
    private DbsField pnd_Legal_Override_Lo_State_Name;
    private DbsField pnd_Legal_Override_Pnd_Global_Ind;
    private DbsField pnd_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Lo_Cntrct_Payee;

    private DbsGroup pnd_Lo_Cntrct_Payee__R_Field_19;
    private DbsField pnd_Lo_Cntrct_Payee_Pnd_Cntrct;
    private DbsField pnd_Lo_Cntrct_Payee_Pnd_Payee;

    private DbsGroup pnd_Name_Out;
    private DbsField pnd_Name_Out_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Out_Cntrct_Payee;

    private DbsGroup pnd_Name_Out__R_Field_20;
    private DbsField pnd_Name_Out_Cntrct_Nmbr;
    private DbsField pnd_Name_Out_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Out_Pnd_Cntrct_Name_Add_K;

    private DbsGroup pnd_Name_Out__R_Field_21;
    private DbsField pnd_Name_Out_Cntrct_Name_Free_K;
    private DbsField pnd_Name_Out_Addrss_Lne_1_K;
    private DbsField pnd_Name_Out_Addrss_Lne_2_K;
    private DbsField pnd_Name_Out_Addrss_Lne_3_K;
    private DbsField pnd_Name_Out_Addrss_Lne_4_K;
    private DbsField pnd_Name_Out_Addrss_Lne_5_K;
    private DbsField pnd_Name_Out_Addrss_Lne_6_K;
    private DbsField pnd_Name_Out_Addrss_Postal_Data_K;

    private DbsGroup pnd_Name_Out__R_Field_22;
    private DbsField pnd_Name_Out_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Name_Out_Addrss_Carrier_Rte_K;
    private DbsField pnd_Name_Out_Addrss_Walk_Rte_K;
    private DbsField pnd_Name_Out_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Name_Out_Pnd_Rest_Of_Record_K;

    private DbsGroup pnd_Name_Out__R_Field_23;
    private DbsField pnd_Name_Out_Addrss_Type_Cde_K;
    private DbsField pnd_Name_Out_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Name_Out_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Name_Out_Permanent_Addrss_Ind_K;
    private DbsField pnd_Name_Out_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Name_Out_Eft_Status_Ind_K;
    private DbsField pnd_Name_Out_Checking_Saving_Cde_K;
    private DbsField pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Name_Out_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Out_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Name_Out_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Name_Out_Addrss_Geographic_Cde_K;
    private DbsField pnd_Name_Out_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Out_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Out_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Out_Pnd_Cntrct_Name_Add_R;

    private DbsGroup pnd_Name_Out__R_Field_24;
    private DbsField pnd_Name_Out_Cntrct_Name_Free_R;
    private DbsField pnd_Name_Out_Addrss_Lne_1_R;
    private DbsField pnd_Name_Out_Addrss_Lne_2_R;
    private DbsField pnd_Name_Out_Addrss_Lne_3_R;
    private DbsField pnd_Name_Out_Addrss_Lne_4_R;
    private DbsField pnd_Name_Out_Addrss_Lne_5_R;
    private DbsField pnd_Name_Out_Addrss_Lne_6_R;
    private DbsField pnd_Name_Out_Addrss_Postal_Data_R;

    private DbsGroup pnd_Name_Out__R_Field_25;
    private DbsField pnd_Name_Out_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Name_Out_Addrss_Carrier_Rte_R;
    private DbsField pnd_Name_Out_Addrss_Walk_Rte_R;
    private DbsField pnd_Name_Out_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Name_Out_Pnd_Rest_Of_Record_R;

    private DbsGroup pnd_Name_Out__R_Field_26;
    private DbsField pnd_Name_Out_Addrss_Type_Cde_R;
    private DbsField pnd_Name_Out_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Name_Out_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Name_Out_Permanent_Addrss_Ind_R;
    private DbsField pnd_Name_Out_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Name_Out_Eft_Status_Ind_R;
    private DbsField pnd_Name_Out_Checking_Saving_Cde_R;
    private DbsField pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Name_Out_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Out_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_R;
    private DbsField pnd_Name_Out_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Name_Out_Addrss_Geographic_Cde_R;

    private DbsGroup pnd_Vacation_Out;
    private DbsField pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_K;

    private DbsGroup pnd_Vacation_Out__R_Field_27;
    private DbsField pnd_Vacation_Out_V_Cntrct_Name_Free_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_1_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_2_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_3_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_4_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_5_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_6_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Postal_Data_K;

    private DbsGroup pnd_Vacation_Out__R_Field_28;
    private DbsField pnd_Vacation_Out_V_Addrss_Zip_Plus_4_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Carrier_Rte_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Walk_Rte_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Usps_Future_Use_K;
    private DbsField pnd_Vacation_Out_Pnd_V_Rest_Of_Record_K;

    private DbsGroup pnd_Vacation_Out__R_Field_29;
    private DbsField pnd_Vacation_Out_V_Addrss_Type_Cde_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_K;
    private DbsField pnd_Vacation_Out_V_Permanent_Addrss_Ind_K;
    private DbsField pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_K;
    private DbsField pnd_Vacation_Out_V_Eft_Status_Ind_K;
    private DbsField pnd_Vacation_Out_V_Checking_Saving_Cde_K;
    private DbsField pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_K;
    private DbsField pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_K;
    private DbsField pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_K;
    private DbsField pnd_Vacation_Out_V_Pending_Perm_Addrss_Chnge_Dte_K;
    private DbsField pnd_Vacation_Out_V_Check_Mailing_Addrss_Ind_K;
    private DbsField pnd_Vacation_Out_V_Addrss_Geographic_Cde_K;
    private DbsField pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Vacation_Out_V_Iintl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Vacation_Out_V_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_R;

    private DbsGroup pnd_Vacation_Out__R_Field_30;
    private DbsField pnd_Vacation_Out_V_Cntrct_Name_Free_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_1_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_2_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_3_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_4_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_5_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Lne_6_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Postal_Data_R;

    private DbsGroup pnd_Vacation_Out__R_Field_31;
    private DbsField pnd_Vacation_Out_V_Addrss_Zip_Plus_4_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Carrier_Rte_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Walk_Rte_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Usps_Future_Use_R;
    private DbsField pnd_Vacation_Out_Pnd_V_Rest_Of_Record_R;

    private DbsGroup pnd_Vacation_Out__R_Field_32;
    private DbsField pnd_Vacation_Out_V_Addrss_Type_Cde_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_R;
    private DbsField pnd_Vacation_Out_V_Permanent_Addrss_Ind_R;
    private DbsField pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_R;
    private DbsField pnd_Vacation_Out_V_Eft_Status_Ind_R;
    private DbsField pnd_Vacation_Out_V_Checking_Saving_Cde_R;
    private DbsField pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_R;
    private DbsField pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_R;
    private DbsField pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_R;
    private DbsField pnd_Vacation_Out_V_Pending_Perm_Add_Chnge_Dte_R;
    private DbsField pnd_Vacation_Out_V_Correspondence_Addrss_Ind_R;
    private DbsField pnd_Vacation_Out_V_Addrss_Geographic_Cde_R;
    private DbsField pnd_Vacation_Out_Global_Indicator;
    private DbsField pnd_Vacation_Out_Global_Country;
    private DbsField pnd_Vacation_Out_Legal_State;

    private DbsGroup miscellaneous_Variables;
    private DbsField miscellaneous_Variables_Pnd_Name_Eof;
    private DbsField miscellaneous_Variables_Pnd_Vacation_Eof;
    private DbsField miscellaneous_Variables_Pnd_Legal_Eof;
    private DbsField miscellaneous_Variables_Pnd_Name_Ctr;
    private DbsField miscellaneous_Variables_Pnd_Vacation_Ctr;
    private DbsField miscellaneous_Variables_Pnd_Legal_Ctr;
    private DbsField miscellaneous_Variables_Pnd_Name_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_Vacation_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_Both_Ctr_Out;
    private DbsField miscellaneous_Variables_Pnd_Line_Coun;
    private DbsField pnd_Mm;
    private DbsField pnd_I;
    private DbsField pnd_I_Save;
    private DbsField pnd_I_S;
    private DbsField pnd_Rout;
    private DbsField pnd_Parameters;

    private DbsGroup pnd_Parameters__R_Field_33;
    private DbsField pnd_Parameters_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Parameters_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Name_Save;

    private DbsGroup pnd_Cntrct_Name_Save__R_Field_34;
    private DbsField pnd_Cntrct_Name_Save_Addrss_Lne_1_S;
    private DbsField pnd_Cntrct_Name_Save_Addrss_Lne_2_S;
    private DbsField pnd_Cntrct_Name_Save_Addrss_Lne_3_S;
    private DbsField pnd_Cntrct_Name_Save_Addrss_Lne_4_S;

    private DbsGroup pnd_Cntrct_Name_Save__R_Field_35;
    private DbsField pnd_Cntrct_Name_Save_Addrss_Lne_S;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Name = localVariables.newGroupInRecord("pnd_Name", "#NAME");
        pnd_Name_Ph_Unque_Id_Nmbr = pnd_Name.newFieldInGroup("pnd_Name_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Cntrct_Payee = pnd_Name.newFieldInGroup("pnd_Name_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name__R_Field_1 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_1", "REDEFINE", pnd_Name_Cntrct_Payee);
        pnd_Name_Cntrct_Nmbr = pnd_Name__R_Field_1.newFieldInGroup("pnd_Name_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Cntrct_Payee_Cde = pnd_Name__R_Field_1.newFieldInGroup("pnd_Name_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Name_Pnd_Cntrct_Name_Add_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 245);

        pnd_Name__R_Field_2 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_2", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Cntrct_Name_Free_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_K = pnd_Name__R_Field_2.newFieldInGroup("pnd_Name_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 35);

        pnd_Name__R_Field_3 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_3", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Addrss_Lne = pnd_Name__R_Field_3.newFieldArrayInGroup("pnd_Name_Addrss_Lne", "ADDRSS-LNE", FieldType.STRING, 35, new DbsArrayController(1, 
            7));
        pnd_Name_Addrss_Postal_Data_K = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 32);

        pnd_Name__R_Field_4 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_4", "REDEFINE", pnd_Name_Addrss_Postal_Data_K);
        pnd_Name_Addrss_Zip_Plus_4_K = pnd_Name__R_Field_4.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_K = pnd_Name__R_Field_4.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_K = pnd_Name__R_Field_4.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_K = pnd_Name__R_Field_4.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_K = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);

        pnd_Name__R_Field_5 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_5", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_K);
        pnd_Name_Addrss_Type_Cde_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Check_Mailing_Addrss_Ind_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_K = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", FieldType.STRING, 
            2);
        pnd_Name_Intl_Eft_Pay_Type_Cde = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", FieldType.STRING, 
            35);
        pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name__R_Field_5.newFieldInGroup("pnd_Name_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            35);
        pnd_Name_Pnd_Cntrct_Name_Add_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 245);

        pnd_Name__R_Field_6 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_6", "REDEFINE", pnd_Name_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Cntrct_Name_Free_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_1_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_2_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_3_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_4_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_5_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Lne_6_R = pnd_Name__R_Field_6.newFieldInGroup("pnd_Name_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_Addrss_Postal_Data_R = pnd_Name.newFieldInGroup("pnd_Name_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 32);

        pnd_Name__R_Field_7 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_7", "REDEFINE", pnd_Name_Addrss_Postal_Data_R);
        pnd_Name_Addrss_Zip_Plus_4_R = pnd_Name__R_Field_7.newFieldInGroup("pnd_Name_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 9);
        pnd_Name_Addrss_Carrier_Rte_R = pnd_Name__R_Field_7.newFieldInGroup("pnd_Name_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Addrss_Walk_Rte_R = pnd_Name__R_Field_7.newFieldInGroup("pnd_Name_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 4);
        pnd_Name_Addrss_Usps_Future_Use_R = pnd_Name__R_Field_7.newFieldInGroup("pnd_Name_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", FieldType.STRING, 
            15);
        pnd_Name_Pnd_Rest_Of_Record_R = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name__R_Field_8 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_8", "REDEFINE", pnd_Name_Pnd_Rest_Of_Record_R);
        pnd_Name_Addrss_Type_Cde_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 1);
        pnd_Name_Addrss_Last_Chnge_Dte_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", FieldType.NUMERIC, 
            8);
        pnd_Name_Addrss_Last_Chnge_Time_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", FieldType.NUMERIC, 
            7);
        pnd_Name_Permanent_Addrss_Ind_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_Bank_Aba_Acct_Nmbr_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_Eft_Status_Ind_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 1);
        pnd_Name_Checking_Saving_Cde_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", FieldType.STRING, 
            21);
        pnd_Name_Pending_Addrss_Chnge_Dte_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Addrss_Restore_Dte_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Correspondence_Addrss_Ind_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Addrss_Geographic_Cde_R = pnd_Name__R_Field_8.newFieldInGroup("pnd_Name_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", FieldType.STRING, 
            2);

        pnd_Vacation = localVariables.newGroupInRecord("pnd_Vacation", "#VACATION");
        pnd_Vacation_V_Ph_Unque_Id_Nmbr = pnd_Vacation.newFieldInGroup("pnd_Vacation_V_Ph_Unque_Id_Nmbr", "V-PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Vacation_V_Cntrct_Payee = pnd_Vacation.newFieldInGroup("pnd_Vacation_V_Cntrct_Payee", "V-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Vacation__R_Field_9 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_9", "REDEFINE", pnd_Vacation_V_Cntrct_Payee);
        pnd_Vacation_V_Cntrct_Nmbr = pnd_Vacation__R_Field_9.newFieldInGroup("pnd_Vacation_V_Cntrct_Nmbr", "V-CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Vacation_V_Cntrct_Payee_Cde = pnd_Vacation__R_Field_9.newFieldInGroup("pnd_Vacation_V_Cntrct_Payee_Cde", "V-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Vacation_Pnd_V_Cntrct_Name_Add_K = pnd_Vacation.newFieldInGroup("pnd_Vacation_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);

        pnd_Vacation__R_Field_10 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_10", "REDEFINE", pnd_Vacation_Pnd_V_Cntrct_Name_Add_K);
        pnd_Vacation_V_Cntrct_Name_Free_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_1_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_2_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_3_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_4_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_5_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_6_K = pnd_Vacation__R_Field_10.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 
            35);

        pnd_Vacation__R_Field_11 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_11", "REDEFINE", pnd_Vacation_Pnd_V_Cntrct_Name_Add_K);
        pnd_Vacation_Addrss_Lne_V = pnd_Vacation__R_Field_11.newFieldArrayInGroup("pnd_Vacation_Addrss_Lne_V", "ADDRSS-LNE-V", FieldType.STRING, 35, new 
            DbsArrayController(1, 7));
        pnd_Vacation_V_Addrss_Postal_Data_K = pnd_Vacation.newFieldInGroup("pnd_Vacation_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);

        pnd_Vacation__R_Field_12 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_12", "REDEFINE", pnd_Vacation_V_Addrss_Postal_Data_K);
        pnd_Vacation_V_Addrss_Zip_Plus_4_K = pnd_Vacation__R_Field_12.newFieldInGroup("pnd_Vacation_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Vacation_V_Addrss_Carrier_Rte_K = pnd_Vacation__R_Field_12.newFieldInGroup("pnd_Vacation_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Vacation_V_Addrss_Walk_Rte_K = pnd_Vacation__R_Field_12.newFieldInGroup("pnd_Vacation_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Vacation_V_Addrss_Usps_Future_Use_K = pnd_Vacation__R_Field_12.newFieldInGroup("pnd_Vacation_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Vacation_Pnd_V_Rest_Of_Record_K = pnd_Vacation.newFieldInGroup("pnd_Vacation_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 
            148);

        pnd_Vacation__R_Field_13 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_13", "REDEFINE", pnd_Vacation_Pnd_V_Rest_Of_Record_K);
        pnd_Vacation_V_Addrss_Type_Cde_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Vacation_V_Addrss_Last_Chnge_Dte_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Addrss_Last_Chnge_Time_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Vacation_V_Permanent_Addrss_Ind_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Bank_Aba_Acct_Nmbr_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Vacation_V_Eft_Status_Ind_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Vacation_V_Checking_Saving_Cde_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Vacation_V_Pending_Addrss_Chnge_Dte_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Pending_Addrss_Chnge_Dte_K", "V-PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Pending_Addrss_Restore_Dte_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Pending_Addrss_Restore_Dte_K", "V-PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Pending_Perm_Addrss_Chnge_Dte_K", "V-PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Check_Mailing_Addrss_Ind_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Check_Mailing_Addrss_Ind_K", "V-CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Addrss_Geographic_Cde_K = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Vacation_V_Intl_Eft_Pay_Type_Cde = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Vacation_V_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Intl_Bank_Pymnt_Eft_Nmbr", "V-INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Vacation_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Vacation__R_Field_13.newFieldInGroup("pnd_Vacation_V_Intl_Bank_Pymnt_Acct_Nmbr", "V-INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Vacation_Pnd_V_Cntrct_Name_Add_R = pnd_Vacation.newFieldInGroup("pnd_Vacation_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);

        pnd_Vacation__R_Field_14 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_14", "REDEFINE", pnd_Vacation_Pnd_V_Cntrct_Name_Add_R);
        pnd_Vacation_V_Cntrct_Name_Free_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_1_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_2_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_3_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_4_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_5_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Lne_6_R = pnd_Vacation__R_Field_14.newFieldInGroup("pnd_Vacation_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Vacation_V_Addrss_Postal_Data_R = pnd_Vacation.newFieldInGroup("pnd_Vacation_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);

        pnd_Vacation__R_Field_15 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_15", "REDEFINE", pnd_Vacation_V_Addrss_Postal_Data_R);
        pnd_Vacation_V_Addrss_Zip_Plus_4_R = pnd_Vacation__R_Field_15.newFieldInGroup("pnd_Vacation_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Vacation_V_Addrss_Carrier_Rte_R = pnd_Vacation__R_Field_15.newFieldInGroup("pnd_Vacation_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Vacation_V_Addrss_Walk_Rte_R = pnd_Vacation__R_Field_15.newFieldInGroup("pnd_Vacation_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Vacation_V_Addrss_Usps_Future_Use_R = pnd_Vacation__R_Field_15.newFieldInGroup("pnd_Vacation_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Vacation_Pnd_V_Rest_Of_Record_R = pnd_Vacation.newFieldInGroup("pnd_Vacation_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 
            76);

        pnd_Vacation__R_Field_16 = pnd_Vacation.newGroupInGroup("pnd_Vacation__R_Field_16", "REDEFINE", pnd_Vacation_Pnd_V_Rest_Of_Record_R);
        pnd_Vacation_V_Addrss_Type_Cde_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Vacation_V_Addrss_Last_Chnge_Dte_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Addrss_Last_Chnge_Time_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Vacation_V_Permanent_Addrss_Ind_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Bank_Aba_Acct_Nmbr_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Vacation_V_Eft_Status_Ind_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Vacation_V_Checking_Saving_Cde_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Vacation_V_Pending_Addrss_Chnge_Dte_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Pending_Addrss_Chnge_Dte_R", "V-PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Pending_Addrss_Restore_Dte_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Pending_Addrss_Restore_Dte_R", "V-PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Pending_Perm_Add_Chnge_Dte_R", "V-PENDING-PERM-ADD-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_V_Correspondence_Addrss_Ind_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Correspondence_Addrss_Ind_R", "V-CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Vacation_V_Addrss_Geographic_Cde_R = pnd_Vacation__R_Field_16.newFieldInGroup("pnd_Vacation_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);

        pnd_Legal_Override = localVariables.newGroupInRecord("pnd_Legal_Override", "#LEGAL-OVERRIDE");
        pnd_Legal_Override_Lo_Record_Key = pnd_Legal_Override.newFieldInGroup("pnd_Legal_Override_Lo_Record_Key", "LO-RECORD-KEY", FieldType.STRING, 13);

        pnd_Legal_Override__R_Field_17 = pnd_Legal_Override.newGroupInGroup("pnd_Legal_Override__R_Field_17", "REDEFINE", pnd_Legal_Override_Lo_Record_Key);
        pnd_Legal_Override_Lo_Contract_Number = pnd_Legal_Override__R_Field_17.newFieldInGroup("pnd_Legal_Override_Lo_Contract_Number", "LO-CONTRACT-NUMBER", 
            FieldType.STRING, 8);
        pnd_Legal_Override_Lo_Payee_Code = pnd_Legal_Override__R_Field_17.newFieldInGroup("pnd_Legal_Override_Lo_Payee_Code", "LO-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Legal_Override_Lo_Sequence_No = pnd_Legal_Override__R_Field_17.newFieldInGroup("pnd_Legal_Override_Lo_Sequence_No", "LO-SEQUENCE-NO", FieldType.NUMERIC, 
            3);

        pnd_Legal_Override_Lo_Common_Data = pnd_Legal_Override.newGroupInGroup("pnd_Legal_Override_Lo_Common_Data", "LO-COMMON-DATA");
        pnd_Legal_Override_Lo_Status_Code = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Status_Code", "LO-STATUS-CODE", FieldType.STRING, 
            1);
        pnd_Legal_Override_Lo_Effective_Date = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Effective_Date", "LO-EFFECTIVE-DATE", 
            FieldType.STRING, 8);
        pnd_Legal_Override_Lo_Entry_Date = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Entry_Date", "LO-ENTRY-DATE", FieldType.STRING, 
            8);
        pnd_Legal_Override_Lo_Entry_Operator = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Entry_Operator", "LO-ENTRY-OPERATOR", 
            FieldType.STRING, 4);
        pnd_Legal_Override_Lo_Activity_Date = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Activity_Date", "LO-ACTIVITY-DATE", 
            FieldType.STRING, 8);
        pnd_Legal_Override_Lo_Activity_Operator = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Activity_Operator", "LO-ACTIVITY-OPERATOR", 
            FieldType.STRING, 4);

        pnd_Legal_Override__R_Field_18 = pnd_Legal_Override_Lo_Common_Data.newGroupInGroup("pnd_Legal_Override__R_Field_18", "REDEFINE", pnd_Legal_Override_Lo_Activity_Operator);
        pnd_Legal_Override__Filler1 = pnd_Legal_Override__R_Field_18.newFieldInGroup("pnd_Legal_Override__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Legal_Override_Pnd_Global_Cntry = pnd_Legal_Override__R_Field_18.newFieldInGroup("pnd_Legal_Override_Pnd_Global_Cntry", "#GLOBAL-CNTRY", FieldType.STRING, 
            3);
        pnd_Legal_Override_Lo_State_Code = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_State_Code", "LO-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Legal_Override_Lo_Ia_State_Code = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_Ia_State_Code", "LO-IA-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Legal_Override_Lo_State_Name = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Lo_State_Name", "LO-STATE-NAME", FieldType.STRING, 
            17);
        pnd_Legal_Override_Pnd_Global_Ind = pnd_Legal_Override_Lo_Common_Data.newFieldInGroup("pnd_Legal_Override_Pnd_Global_Ind", "#GLOBAL-IND", FieldType.STRING, 
            1);
        pnd_Pymnt_Pay_Type_Req_Ind = localVariables.newFieldInRecord("pnd_Pymnt_Pay_Type_Req_Ind", "#PYMNT-PAY-TYPE-REQ-IND", FieldType.STRING, 1);
        pnd_Lo_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Lo_Cntrct_Payee", "#LO-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Lo_Cntrct_Payee__R_Field_19 = localVariables.newGroupInRecord("pnd_Lo_Cntrct_Payee__R_Field_19", "REDEFINE", pnd_Lo_Cntrct_Payee);
        pnd_Lo_Cntrct_Payee_Pnd_Cntrct = pnd_Lo_Cntrct_Payee__R_Field_19.newFieldInGroup("pnd_Lo_Cntrct_Payee_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 
            10);
        pnd_Lo_Cntrct_Payee_Pnd_Payee = pnd_Lo_Cntrct_Payee__R_Field_19.newFieldInGroup("pnd_Lo_Cntrct_Payee_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 
            2);

        pnd_Name_Out = localVariables.newGroupInRecord("pnd_Name_Out", "#NAME-OUT");
        pnd_Name_Out_Ph_Unque_Id_Nmbr = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 12);
        pnd_Name_Out_Cntrct_Payee = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Cntrct_Payee", "CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Name_Out__R_Field_20 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_20", "REDEFINE", pnd_Name_Out_Cntrct_Payee);
        pnd_Name_Out_Cntrct_Nmbr = pnd_Name_Out__R_Field_20.newFieldInGroup("pnd_Name_Out_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Out_Cntrct_Payee_Cde = pnd_Name_Out__R_Field_20.newFieldInGroup("pnd_Name_Out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Out_Pnd_Cntrct_Name_Add_K = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Pnd_Cntrct_Name_Add_K", "#CNTRCT-NAME-ADD-K", FieldType.STRING, 
            245);

        pnd_Name_Out__R_Field_21 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_21", "REDEFINE", pnd_Name_Out_Pnd_Cntrct_Name_Add_K);
        pnd_Name_Out_Cntrct_Name_Free_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Cntrct_Name_Free_K", "CNTRCT-NAME-FREE-K", FieldType.STRING, 
            35);
        pnd_Name_Out_Addrss_Lne_1_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_1_K", "ADDRSS-LNE-1-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_2_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_2_K", "ADDRSS-LNE-2-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_3_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_3_K", "ADDRSS-LNE-3-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_4_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_4_K", "ADDRSS-LNE-4-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_5_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_5_K", "ADDRSS-LNE-5-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_6_K = pnd_Name_Out__R_Field_21.newFieldInGroup("pnd_Name_Out_Addrss_Lne_6_K", "ADDRSS-LNE-6-K", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Postal_Data_K = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Addrss_Postal_Data_K", "ADDRSS-POSTAL-DATA-K", FieldType.STRING, 
            32);

        pnd_Name_Out__R_Field_22 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_22", "REDEFINE", pnd_Name_Out_Addrss_Postal_Data_K);
        pnd_Name_Out_Addrss_Zip_Plus_4_K = pnd_Name_Out__R_Field_22.newFieldInGroup("pnd_Name_Out_Addrss_Zip_Plus_4_K", "ADDRSS-ZIP-PLUS-4-K", FieldType.STRING, 
            9);
        pnd_Name_Out_Addrss_Carrier_Rte_K = pnd_Name_Out__R_Field_22.newFieldInGroup("pnd_Name_Out_Addrss_Carrier_Rte_K", "ADDRSS-CARRIER-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Out_Addrss_Walk_Rte_K = pnd_Name_Out__R_Field_22.newFieldInGroup("pnd_Name_Out_Addrss_Walk_Rte_K", "ADDRSS-WALK-RTE-K", FieldType.STRING, 
            4);
        pnd_Name_Out_Addrss_Usps_Future_Use_K = pnd_Name_Out__R_Field_22.newFieldInGroup("pnd_Name_Out_Addrss_Usps_Future_Use_K", "ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Name_Out_Pnd_Rest_Of_Record_K = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Pnd_Rest_Of_Record_K", "#REST-OF-RECORD-K", FieldType.STRING, 148);

        pnd_Name_Out__R_Field_23 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_23", "REDEFINE", pnd_Name_Out_Pnd_Rest_Of_Record_K);
        pnd_Name_Out_Addrss_Type_Cde_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Addrss_Type_Cde_K", "ADDRSS-TYPE-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Out_Addrss_Last_Chnge_Dte_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Addrss_Last_Chnge_Dte_K", "ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Addrss_Last_Chnge_Time_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Addrss_Last_Chnge_Time_K", "ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Name_Out_Permanent_Addrss_Ind_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Permanent_Addrss_Ind_K", "PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Out_Bank_Aba_Acct_Nmbr_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Bank_Aba_Acct_Nmbr_K", "BANK-ABA-ACCT-NMBR-K", FieldType.STRING, 
            9);
        pnd_Name_Out_Eft_Status_Ind_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Eft_Status_Ind_K", "EFT-STATUS-IND-K", FieldType.STRING, 
            1);
        pnd_Name_Out_Checking_Saving_Cde_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Checking_Saving_Cde_K", "CHECKING-SAVING-CDE-K", FieldType.STRING, 
            1);
        pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_K", "PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Name_Out_Pending_Addrss_Chnge_Dte_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Pending_Addrss_Chnge_Dte_K", "PENDING-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Pending_Addrss_Restore_Dte_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Pending_Addrss_Restore_Dte_K", "PENDING-ADDRSS-RESTORE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_K", "PENDING-PERM-ADDRSS-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Check_Mailing_Addrss_Ind_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Check_Mailing_Addrss_Ind_K", "CHECK-MAILING-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Name_Out_Addrss_Geographic_Cde_K = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Addrss_Geographic_Cde_K", "ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Name_Out_Intl_Eft_Pay_Type_Cde = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Name_Out_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Out_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Out__R_Field_23.newFieldInGroup("pnd_Name_Out_Intl_Bank_Pymnt_Acct_Nmbr", "INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Out_Pnd_Cntrct_Name_Add_R = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Pnd_Cntrct_Name_Add_R", "#CNTRCT-NAME-ADD-R", FieldType.STRING, 
            245);

        pnd_Name_Out__R_Field_24 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_24", "REDEFINE", pnd_Name_Out_Pnd_Cntrct_Name_Add_R);
        pnd_Name_Out_Cntrct_Name_Free_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Cntrct_Name_Free_R", "CNTRCT-NAME-FREE-R", FieldType.STRING, 
            35);
        pnd_Name_Out_Addrss_Lne_1_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_1_R", "ADDRSS-LNE-1-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_2_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_2_R", "ADDRSS-LNE-2-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_3_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_3_R", "ADDRSS-LNE-3-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_4_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_4_R", "ADDRSS-LNE-4-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_5_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_5_R", "ADDRSS-LNE-5-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Lne_6_R = pnd_Name_Out__R_Field_24.newFieldInGroup("pnd_Name_Out_Addrss_Lne_6_R", "ADDRSS-LNE-6-R", FieldType.STRING, 35);
        pnd_Name_Out_Addrss_Postal_Data_R = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Addrss_Postal_Data_R", "ADDRSS-POSTAL-DATA-R", FieldType.STRING, 
            32);

        pnd_Name_Out__R_Field_25 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_25", "REDEFINE", pnd_Name_Out_Addrss_Postal_Data_R);
        pnd_Name_Out_Addrss_Zip_Plus_4_R = pnd_Name_Out__R_Field_25.newFieldInGroup("pnd_Name_Out_Addrss_Zip_Plus_4_R", "ADDRSS-ZIP-PLUS-4-R", FieldType.STRING, 
            9);
        pnd_Name_Out_Addrss_Carrier_Rte_R = pnd_Name_Out__R_Field_25.newFieldInGroup("pnd_Name_Out_Addrss_Carrier_Rte_R", "ADDRSS-CARRIER-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Out_Addrss_Walk_Rte_R = pnd_Name_Out__R_Field_25.newFieldInGroup("pnd_Name_Out_Addrss_Walk_Rte_R", "ADDRSS-WALK-RTE-R", FieldType.STRING, 
            4);
        pnd_Name_Out_Addrss_Usps_Future_Use_R = pnd_Name_Out__R_Field_25.newFieldInGroup("pnd_Name_Out_Addrss_Usps_Future_Use_R", "ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Name_Out_Pnd_Rest_Of_Record_R = pnd_Name_Out.newFieldInGroup("pnd_Name_Out_Pnd_Rest_Of_Record_R", "#REST-OF-RECORD-R", FieldType.STRING, 76);

        pnd_Name_Out__R_Field_26 = pnd_Name_Out.newGroupInGroup("pnd_Name_Out__R_Field_26", "REDEFINE", pnd_Name_Out_Pnd_Rest_Of_Record_R);
        pnd_Name_Out_Addrss_Type_Cde_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Addrss_Type_Cde_R", "ADDRSS-TYPE-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Out_Addrss_Last_Chnge_Dte_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Addrss_Last_Chnge_Dte_R", "ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Addrss_Last_Chnge_Time_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Addrss_Last_Chnge_Time_R", "ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Name_Out_Permanent_Addrss_Ind_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Permanent_Addrss_Ind_R", "PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Out_Bank_Aba_Acct_Nmbr_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Bank_Aba_Acct_Nmbr_R", "BANK-ABA-ACCT-NMBR-R", FieldType.STRING, 
            9);
        pnd_Name_Out_Eft_Status_Ind_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Eft_Status_Ind_R", "EFT-STATUS-IND-R", FieldType.STRING, 
            1);
        pnd_Name_Out_Checking_Saving_Cde_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Checking_Saving_Cde_R", "CHECKING-SAVING-CDE-R", FieldType.STRING, 
            1);
        pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Ph_Bank_Pymnt_Acct_Nmbr_R", "PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Name_Out_Pending_Addrss_Chnge_Dte_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Pending_Addrss_Chnge_Dte_R", "PENDING-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Pending_Addrss_Restore_Dte_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Pending_Addrss_Restore_Dte_R", "PENDING-ADDRSS-RESTORE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Pending_Perm_Addrss_Chnge_Dte_R", "PENDING-PERM-ADDRSS-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Name_Out_Correspondence_Addrss_Ind_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Correspondence_Addrss_Ind_R", "CORRESPONDENCE-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Name_Out_Addrss_Geographic_Cde_R = pnd_Name_Out__R_Field_26.newFieldInGroup("pnd_Name_Out_Addrss_Geographic_Cde_R", "ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);

        pnd_Vacation_Out = localVariables.newGroupInRecord("pnd_Vacation_Out", "#VACATION-OUT");
        pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_K = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_K", "#V-CNTRCT-NAME-ADD-K", 
            FieldType.STRING, 245);

        pnd_Vacation_Out__R_Field_27 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_27", "REDEFINE", pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_K);
        pnd_Vacation_Out_V_Cntrct_Name_Free_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Cntrct_Name_Free_K", "V-CNTRCT-NAME-FREE-K", 
            FieldType.STRING, 35);
        pnd_Vacation_Out_V_Addrss_Lne_1_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_1_K", "V-ADDRSS-LNE-1-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_2_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_2_K", "V-ADDRSS-LNE-2-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_3_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_3_K", "V-ADDRSS-LNE-3-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_4_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_4_K", "V-ADDRSS-LNE-4-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_5_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_5_K", "V-ADDRSS-LNE-5-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_6_K = pnd_Vacation_Out__R_Field_27.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_6_K", "V-ADDRSS-LNE-6-K", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Postal_Data_K = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Postal_Data_K", "V-ADDRSS-POSTAL-DATA-K", 
            FieldType.STRING, 32);

        pnd_Vacation_Out__R_Field_28 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_28", "REDEFINE", pnd_Vacation_Out_V_Addrss_Postal_Data_K);
        pnd_Vacation_Out_V_Addrss_Zip_Plus_4_K = pnd_Vacation_Out__R_Field_28.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Zip_Plus_4_K", "V-ADDRSS-ZIP-PLUS-4-K", 
            FieldType.STRING, 9);
        pnd_Vacation_Out_V_Addrss_Carrier_Rte_K = pnd_Vacation_Out__R_Field_28.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Carrier_Rte_K", "V-ADDRSS-CARRIER-RTE-K", 
            FieldType.STRING, 4);
        pnd_Vacation_Out_V_Addrss_Walk_Rte_K = pnd_Vacation_Out__R_Field_28.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Walk_Rte_K", "V-ADDRSS-WALK-RTE-K", 
            FieldType.STRING, 4);
        pnd_Vacation_Out_V_Addrss_Usps_Future_Use_K = pnd_Vacation_Out__R_Field_28.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Usps_Future_Use_K", "V-ADDRSS-USPS-FUTURE-USE-K", 
            FieldType.STRING, 15);
        pnd_Vacation_Out_Pnd_V_Rest_Of_Record_K = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Pnd_V_Rest_Of_Record_K", "#V-REST-OF-RECORD-K", FieldType.STRING, 
            148);

        pnd_Vacation_Out__R_Field_29 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_29", "REDEFINE", pnd_Vacation_Out_Pnd_V_Rest_Of_Record_K);
        pnd_Vacation_Out_V_Addrss_Type_Cde_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Type_Cde_K", "V-ADDRSS-TYPE-CDE-K", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_K", "V-ADDRSS-LAST-CHNGE-DTE-K", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_K", "V-ADDRSS-LAST-CHNGE-TIME-K", 
            FieldType.NUMERIC, 7);
        pnd_Vacation_Out_V_Permanent_Addrss_Ind_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Permanent_Addrss_Ind_K", "V-PERMANENT-ADDRSS-IND-K", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_K", "V-BANK-ABA-ACCT-NMBR-K", 
            FieldType.STRING, 9);
        pnd_Vacation_Out_V_Eft_Status_Ind_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Eft_Status_Ind_K", "V-EFT-STATUS-IND-K", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Checking_Saving_Cde_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Checking_Saving_Cde_K", "V-CHECKING-SAVING-CDE-K", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_K", "V-PH-BANK-PYMNT-ACCT-NMBR-K", 
            FieldType.STRING, 21);
        pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_K", 
            "V-PENDING-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_K", 
            "V-PENDING-ADDRSS-RESTORE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Pending_Perm_Addrss_Chnge_Dte_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Pending_Perm_Addrss_Chnge_Dte_K", 
            "V-PENDING-PERM-ADDRSS-CHNGE-DTE-K", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Check_Mailing_Addrss_Ind_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Check_Mailing_Addrss_Ind_K", 
            "V-CHECK-MAILING-ADDRSS-IND-K", FieldType.STRING, 1);
        pnd_Vacation_Out_V_Addrss_Geographic_Cde_K = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Geographic_Cde_K", "V-ADDRSS-GEOGRAPHIC-CDE-K", 
            FieldType.STRING, 2);
        pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde", "V-INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Vacation_Out_V_Iintl_Bank_Pymnt_Eft_Nmbr = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Iintl_Bank_Pymnt_Eft_Nmbr", "V-IINTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Vacation_Out_V_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Vacation_Out__R_Field_29.newFieldInGroup("pnd_Vacation_Out_V_Intl_Bank_Pymnt_Acct_Nmbr", "V-INTL-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 35);
        pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_R = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_R", "#V-CNTRCT-NAME-ADD-R", 
            FieldType.STRING, 245);

        pnd_Vacation_Out__R_Field_30 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_30", "REDEFINE", pnd_Vacation_Out_Pnd_V_Cntrct_Name_Add_R);
        pnd_Vacation_Out_V_Cntrct_Name_Free_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Cntrct_Name_Free_R", "V-CNTRCT-NAME-FREE-R", 
            FieldType.STRING, 35);
        pnd_Vacation_Out_V_Addrss_Lne_1_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_1_R", "V-ADDRSS-LNE-1-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_2_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_2_R", "V-ADDRSS-LNE-2-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_3_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_3_R", "V-ADDRSS-LNE-3-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_4_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_4_R", "V-ADDRSS-LNE-4-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_5_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_5_R", "V-ADDRSS-LNE-5-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Lne_6_R = pnd_Vacation_Out__R_Field_30.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Lne_6_R", "V-ADDRSS-LNE-6-R", FieldType.STRING, 
            35);
        pnd_Vacation_Out_V_Addrss_Postal_Data_R = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Postal_Data_R", "V-ADDRSS-POSTAL-DATA-R", 
            FieldType.STRING, 32);

        pnd_Vacation_Out__R_Field_31 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_31", "REDEFINE", pnd_Vacation_Out_V_Addrss_Postal_Data_R);
        pnd_Vacation_Out_V_Addrss_Zip_Plus_4_R = pnd_Vacation_Out__R_Field_31.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Zip_Plus_4_R", "V-ADDRSS-ZIP-PLUS-4-R", 
            FieldType.STRING, 9);
        pnd_Vacation_Out_V_Addrss_Carrier_Rte_R = pnd_Vacation_Out__R_Field_31.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Carrier_Rte_R", "V-ADDRSS-CARRIER-RTE-R", 
            FieldType.STRING, 4);
        pnd_Vacation_Out_V_Addrss_Walk_Rte_R = pnd_Vacation_Out__R_Field_31.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Walk_Rte_R", "V-ADDRSS-WALK-RTE-R", 
            FieldType.STRING, 4);
        pnd_Vacation_Out_V_Addrss_Usps_Future_Use_R = pnd_Vacation_Out__R_Field_31.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Usps_Future_Use_R", "V-ADDRSS-USPS-FUTURE-USE-R", 
            FieldType.STRING, 15);
        pnd_Vacation_Out_Pnd_V_Rest_Of_Record_R = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Pnd_V_Rest_Of_Record_R", "#V-REST-OF-RECORD-R", FieldType.STRING, 
            76);

        pnd_Vacation_Out__R_Field_32 = pnd_Vacation_Out.newGroupInGroup("pnd_Vacation_Out__R_Field_32", "REDEFINE", pnd_Vacation_Out_Pnd_V_Rest_Of_Record_R);
        pnd_Vacation_Out_V_Addrss_Type_Cde_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Type_Cde_R", "V-ADDRSS-TYPE-CDE-R", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Last_Chnge_Dte_R", "V-ADDRSS-LAST-CHNGE-DTE-R", 
            FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Last_Chnge_Time_R", "V-ADDRSS-LAST-CHNGE-TIME-R", 
            FieldType.NUMERIC, 7);
        pnd_Vacation_Out_V_Permanent_Addrss_Ind_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Permanent_Addrss_Ind_R", "V-PERMANENT-ADDRSS-IND-R", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Bank_Aba_Acct_Nmbr_R", "V-BANK-ABA-ACCT-NMBR-R", 
            FieldType.STRING, 9);
        pnd_Vacation_Out_V_Eft_Status_Ind_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Eft_Status_Ind_R", "V-EFT-STATUS-IND-R", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Checking_Saving_Cde_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Checking_Saving_Cde_R", "V-CHECKING-SAVING-CDE-R", 
            FieldType.STRING, 1);
        pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Ph_Bank_Pymnt_Acct_Nmbr_R", "V-PH-BANK-PYMNT-ACCT-NMBR-R", 
            FieldType.STRING, 21);
        pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Pending_Addrss_Chnge_Dte_R", 
            "V-PENDING-ADDRSS-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Pending_Addrss_Restore_Dte_R", 
            "V-PENDING-ADDRSS-RESTORE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Pending_Perm_Add_Chnge_Dte_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Pending_Perm_Add_Chnge_Dte_R", 
            "V-PENDING-PERM-ADD-CHNGE-DTE-R", FieldType.NUMERIC, 8);
        pnd_Vacation_Out_V_Correspondence_Addrss_Ind_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Correspondence_Addrss_Ind_R", 
            "V-CORRESPONDENCE-ADDRSS-IND-R", FieldType.STRING, 1);
        pnd_Vacation_Out_V_Addrss_Geographic_Cde_R = pnd_Vacation_Out__R_Field_32.newFieldInGroup("pnd_Vacation_Out_V_Addrss_Geographic_Cde_R", "V-ADDRSS-GEOGRAPHIC-CDE-R", 
            FieldType.STRING, 2);
        pnd_Vacation_Out_Global_Indicator = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Global_Indicator", "GLOBAL-INDICATOR", FieldType.STRING, 
            1);
        pnd_Vacation_Out_Global_Country = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Global_Country", "GLOBAL-COUNTRY", FieldType.STRING, 3);
        pnd_Vacation_Out_Legal_State = pnd_Vacation_Out.newFieldInGroup("pnd_Vacation_Out_Legal_State", "LEGAL-STATE", FieldType.STRING, 2);

        miscellaneous_Variables = localVariables.newGroupInRecord("miscellaneous_Variables", "MISCELLANEOUS-VARIABLES");
        miscellaneous_Variables_Pnd_Name_Eof = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Name_Eof", "#NAME-EOF", FieldType.BOOLEAN, 
            1);
        miscellaneous_Variables_Pnd_Vacation_Eof = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Vacation_Eof", "#VACATION-EOF", 
            FieldType.BOOLEAN, 1);
        miscellaneous_Variables_Pnd_Legal_Eof = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Legal_Eof", "#LEGAL-EOF", FieldType.BOOLEAN, 
            1);
        miscellaneous_Variables_Pnd_Name_Ctr = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Name_Ctr", "#NAME-CTR", FieldType.PACKED_DECIMAL, 
            9);
        miscellaneous_Variables_Pnd_Vacation_Ctr = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Vacation_Ctr", "#VACATION-CTR", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Legal_Ctr = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Legal_Ctr", "#LEGAL-CTR", FieldType.PACKED_DECIMAL, 
            9);
        miscellaneous_Variables_Pnd_Name_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Name_Ctr_Out", "#NAME-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Vacation_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Vacation_Ctr_Out", "#VACATION-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Both_Ctr_Out = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Both_Ctr_Out", "#BOTH-CTR-OUT", 
            FieldType.PACKED_DECIMAL, 9);
        miscellaneous_Variables_Pnd_Line_Coun = miscellaneous_Variables.newFieldInGroup("miscellaneous_Variables_Pnd_Line_Coun", "#LINE-COUN", FieldType.NUMERIC, 
            1);
        pnd_Mm = localVariables.newFieldInRecord("pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_I_Save = localVariables.newFieldInRecord("pnd_I_Save", "#I-SAVE", FieldType.NUMERIC, 2);
        pnd_I_S = localVariables.newFieldInRecord("pnd_I_S", "#I-S", FieldType.NUMERIC, 2);
        pnd_Rout = localVariables.newFieldInRecord("pnd_Rout", "#ROUT", FieldType.STRING, 2);
        pnd_Parameters = localVariables.newFieldInRecord("pnd_Parameters", "#PARAMETERS", FieldType.STRING, 12);

        pnd_Parameters__R_Field_33 = localVariables.newGroupInRecord("pnd_Parameters__R_Field_33", "REDEFINE", pnd_Parameters);
        pnd_Parameters_Pnd_Cntrct_Ppcn_Nbr = pnd_Parameters__R_Field_33.newFieldInGroup("pnd_Parameters_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Parameters_Pnd_Cntrct_Payee_Cde = pnd_Parameters__R_Field_33.newFieldInGroup("pnd_Parameters_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Name_Save = localVariables.newFieldInRecord("pnd_Cntrct_Name_Save", "#CNTRCT-NAME-SAVE", FieldType.STRING, 140);

        pnd_Cntrct_Name_Save__R_Field_34 = localVariables.newGroupInRecord("pnd_Cntrct_Name_Save__R_Field_34", "REDEFINE", pnd_Cntrct_Name_Save);
        pnd_Cntrct_Name_Save_Addrss_Lne_1_S = pnd_Cntrct_Name_Save__R_Field_34.newFieldInGroup("pnd_Cntrct_Name_Save_Addrss_Lne_1_S", "ADDRSS-LNE-1-S", 
            FieldType.STRING, 35);
        pnd_Cntrct_Name_Save_Addrss_Lne_2_S = pnd_Cntrct_Name_Save__R_Field_34.newFieldInGroup("pnd_Cntrct_Name_Save_Addrss_Lne_2_S", "ADDRSS-LNE-2-S", 
            FieldType.STRING, 35);
        pnd_Cntrct_Name_Save_Addrss_Lne_3_S = pnd_Cntrct_Name_Save__R_Field_34.newFieldInGroup("pnd_Cntrct_Name_Save_Addrss_Lne_3_S", "ADDRSS-LNE-3-S", 
            FieldType.STRING, 35);
        pnd_Cntrct_Name_Save_Addrss_Lne_4_S = pnd_Cntrct_Name_Save__R_Field_34.newFieldInGroup("pnd_Cntrct_Name_Save_Addrss_Lne_4_S", "ADDRSS-LNE-4-S", 
            FieldType.STRING, 35);

        pnd_Cntrct_Name_Save__R_Field_35 = localVariables.newGroupInRecord("pnd_Cntrct_Name_Save__R_Field_35", "REDEFINE", pnd_Cntrct_Name_Save);
        pnd_Cntrct_Name_Save_Addrss_Lne_S = pnd_Cntrct_Name_Save__R_Field_35.newFieldArrayInGroup("pnd_Cntrct_Name_Save_Addrss_Lne_S", "ADDRSS-LNE-S", 
            FieldType.STRING, 35, new DbsArrayController(1, 4));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Rout.setInitialValue("//");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap804() throws Exception
    {
        super("Iaap804");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP804", onError);
        setupReports();
        //*  TO BYPASS VACATION FILE READ - 3/2015
        miscellaneous_Variables_Pnd_Vacation_Eof.setValue(true);                                                                                                          //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: ASSIGN #VACATION-EOF := TRUE
                                                                                                                                                                          //Natural: PERFORM READ-NAME
        sub_Read_Name();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-VACATION
        sub_Read_Vacation();
        if (condition(Global.isEscape())) {return;}
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(miscellaneous_Variables_Pnd_Name_Eof.getBoolean() && miscellaneous_Variables_Pnd_Vacation_Eof.getBoolean()))                                    //Natural: IF #NAME-EOF AND #VACATION-EOF
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Name_Eof.getBoolean()))                                                                                             //Natural: IF #NAME-EOF
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-VACATION
                sub_Write_Vacation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Vacation_Eof.getBoolean()))                                                                                         //Natural: IF #VACATION-EOF
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME
                sub_Write_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Cntrct_Payee.greater(pnd_Vacation_V_Cntrct_Payee)))                                                                                    //Natural: IF #NAME.CNTRCT-PAYEE GT #VACATION.V-CNTRCT-PAYEE
            {
                                                                                                                                                                          //Natural: PERFORM NAME-GT-VACATION
                sub_Name_Gt_Vacation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Name_Cntrct_Payee.less(pnd_Vacation_V_Cntrct_Payee)))                                                                                   //Natural: IF #NAME.CNTRCT-PAYEE LT #VACATION.V-CNTRCT-PAYEE
                {
                                                                                                                                                                          //Natural: PERFORM NAME-LT-VACATION
                    sub_Name_Lt_Vacation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH
                    sub_Write_Both();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(0, "=",miscellaneous_Variables_Pnd_Name_Ctr,"=",miscellaneous_Variables_Pnd_Vacation_Ctr,"=",miscellaneous_Variables_Pnd_Legal_Ctr,            //Natural: WRITE '=' #NAME-CTR '=' #VACATION-CTR '=' #LEGAL-CTR '=' #NAME-CTR-OUT '=' #VACATION-CTR-OUT '=' #BOTH-CTR-OUT
            "=",miscellaneous_Variables_Pnd_Name_Ctr_Out,"=",miscellaneous_Variables_Pnd_Vacation_Ctr_Out,"=",miscellaneous_Variables_Pnd_Both_Ctr_Out);
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-VACATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-LEGAL-OVERRIDE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-EFT-GLOBAL
        //*  3/2015 - END
        //*      #NAME-OUT.ADDRSS-LNE-4-K:= ' '
        //*    #VACATION-OUT.V-ADDRSS-LNE-4-K:= ' '
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-GT-VACATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-LT-VACATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-VACATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NAME
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BOTH
        //*  WRITE 'TOTAL NAME ADDRESS READ           : ' #TOT-NAME-CTR
        //*  WRITE 'TOTAL IA NAME ADDRESS READ        : ' #TOT-NAME-CTR-I
        //*  WRITE 'TOTAL NAME CHECK MAILING READ     : ' #CHECK-CTR-NAME-NAME
        //*  WRITE 'TOTAL VAC CHECK MAILING READ      : ' #CHECK-CTR-NAME-VAC
        //*  WRITE 'TOTAL NAME CORR READ              : ' #CORR-CTR-NAME-NAME
        //*  WRITE 'TOTAL VAC CORR READ               : ' #CORR-CTR-NAME-VAC
        //*  WRITE 'TOTAL NAME CHECK MAILING OUT      : ' #CHECK-CTR-NAME-NAME-OUT
        //*  WRITE 'TOTAL VAC CHECK MAILING OUT       : ' #CHECK-CTR-NAME-VAC-OUT
        //*  WRITE 'TOTAL NAME CORR MAILING OUT       : ' #CORR-CTR-NAME-NAME-OUT
        //*  WRITE 'TOTAL VAC CORR MAILING OUT        : ' #CORR-CTR-NAME-VAC-OUT
        //*  WRITE 'TOTAL RECORDS WRITTEN             : ' #TOT-CTR-OUT
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Read_Name() throws Exception                                                                                                                         //Natural: READ-NAME
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(miscellaneous_Variables_Pnd_Name_Eof.getBoolean()))                                                                                                 //Natural: IF #NAME-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(1, pnd_Name);                                                                                                                                 //Natural: READ WORK 1 ONCE #NAME
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            miscellaneous_Variables_Pnd_Name_Eof.setValue(true);                                                                                                          //Natural: MOVE TRUE TO #NAME-EOF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  END-IF
        miscellaneous_Variables_Pnd_Name_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NAME-CTR
    }
    private void sub_Read_Vacation() throws Exception                                                                                                                     //Natural: READ-VACATION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(miscellaneous_Variables_Pnd_Vacation_Eof.getBoolean()))                                                                                             //Natural: IF #VACATION-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(2, pnd_Vacation);                                                                                                                             //Natural: READ WORK 2 ONCE #VACATION
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            miscellaneous_Variables_Pnd_Vacation_Eof.setValue(true);                                                                                                      //Natural: MOVE TRUE TO #VACATION-EOF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        miscellaneous_Variables_Pnd_Vacation_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #VACATION-CTR
    }
    private void sub_Read_Legal_Override() throws Exception                                                                                                               //Natural: READ-LEGAL-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(miscellaneous_Variables_Pnd_Legal_Eof.getBoolean()))                                                                                                //Natural: IF #LEGAL-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Lo_Cntrct_Payee.greater(pnd_Name_Cntrct_Payee)))                                                                                            //Natural: IF #LO-CNTRCT-PAYEE GT #NAME.CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lo_Cntrct_Payee.less(pnd_Name_Cntrct_Payee)))                                                                                               //Natural: IF #LO-CNTRCT-PAYEE LT #NAME.CNTRCT-PAYEE
            {
                ignore();
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lo_Cntrct_Payee.equals(pnd_Name_Cntrct_Payee)))                                                                                             //Natural: IF #LO-CNTRCT-PAYEE EQ #NAME.CNTRCT-PAYEE
            {
                pnd_Vacation_Out_Global_Indicator.setValue(pnd_Legal_Override_Pnd_Global_Ind);                                                                            //Natural: ASSIGN #VACATION-OUT.GLOBAL-INDICATOR := #LEGAL-OVERRIDE.#GLOBAL-IND
                pnd_Vacation_Out_Global_Country.setValue(pnd_Legal_Override_Pnd_Global_Cntry);                                                                            //Natural: ASSIGN #VACATION-OUT.GLOBAL-COUNTRY := #LEGAL-OVERRIDE.#GLOBAL-CNTRY
                pnd_Vacation_Out_Legal_State.setValue(pnd_Legal_Override_Lo_Ia_State_Code);                                                                               //Natural: ASSIGN #VACATION-OUT.LEGAL-STATE := #LEGAL-OVERRIDE.LO-IA-STATE-CODE
                                                                                                                                                                          //Natural: PERFORM GET-EFT-GLOBAL
                sub_Get_Eft_Global();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(3, pnd_Legal_Override);                                                                                                                   //Natural: READ WORK 3 ONCE #LEGAL-OVERRIDE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                miscellaneous_Variables_Pnd_Legal_Eof.setValue(true);                                                                                                     //Natural: MOVE TRUE TO #LEGAL-EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-ENDFILE
            miscellaneous_Variables_Pnd_Legal_Ctr.nadd(1);                                                                                                                //Natural: ADD 1 TO #LEGAL-CTR
            pnd_Lo_Cntrct_Payee_Pnd_Cntrct.setValue(pnd_Legal_Override_Lo_Contract_Number);                                                                               //Natural: ASSIGN #LO-CNTRCT-PAYEE.#CNTRCT := #LEGAL-OVERRIDE.LO-CONTRACT-NUMBER
            pnd_Lo_Cntrct_Payee_Pnd_Payee.setValue(pnd_Legal_Override_Lo_Payee_Code);                                                                                     //Natural: ASSIGN #LO-CNTRCT-PAYEE.#PAYEE := #LEGAL-OVERRIDE.LO-PAYEE-CODE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Eft_Global() throws Exception                                                                                                                    //Natural: GET-EFT-GLOBAL
    {
        if (BLNatReinput.isReinput()) return;

        //*  3/2015 - START   - REFER TO BUSINESS BEFORE IMPLEMENTING.
        //*  ALLOW CURRENCY FOR
        //*  NON-BANK ADDRESS
        if (condition(pnd_Name_Intl_Eft_Pay_Type_Cde.equals(" ") && pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde.equals(" ")))                                                //Natural: IF #NAME.INTL-EFT-PAY-TYPE-CDE = ' ' AND #VACATION-OUT.V-INTL-EFT-PAY-TYPE-CDE = ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Parameters.setValue(pnd_Name_Cntrct_Payee);                                                                                                                   //Natural: ASSIGN #PARAMETERS := #NAME.CNTRCT-PAYEE
        DbsUtil.callnat(Iaan700.class , getCurrentProcessState(), pnd_Parameters_Pnd_Cntrct_Ppcn_Nbr, pnd_Parameters_Pnd_Cntrct_Payee_Cde, pnd_Pymnt_Pay_Type_Req_Ind);   //Natural: CALLNAT 'IAAN700' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE #PYMNT-PAY-TYPE-REQ-IND
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Pymnt_Pay_Type_Req_Ind.equals("4") || pnd_Pymnt_Pay_Type_Req_Ind.equals("9")))                                                                  //Natural: IF #PYMNT-PAY-TYPE-REQ-IND = '4' OR #PYMNT-PAY-TYPE-REQ-IND = '9'
        {
            pnd_Vacation_Out_Global_Indicator.setValue(pnd_Pymnt_Pay_Type_Req_Ind);                                                                                       //Natural: ASSIGN #VACATION-OUT.GLOBAL-INDICATOR := #PYMNT-PAY-TYPE-REQ-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Name_Intl_Eft_Pay_Type_Cde.notEquals(" ") && pnd_Name_Intl_Eft_Pay_Type_Cde.notEquals("SW")))                                                   //Natural: IF #NAME.INTL-EFT-PAY-TYPE-CDE NE ' ' AND #NAME.INTL-EFT-PAY-TYPE-CDE NE 'SW'
        {
            miscellaneous_Variables_Pnd_Line_Coun.reset();                                                                                                                //Natural: RESET #LINE-COUN
            pnd_I_Save.reset();                                                                                                                                           //Natural: RESET #I-SAVE
            pnd_Cntrct_Name_Save.reset();                                                                                                                                 //Natural: RESET #CNTRCT-NAME-SAVE
            FOR01:                                                                                                                                                        //Natural: FOR #I 2 TO 7
            for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
            {
                if (condition(pnd_Name_Addrss_Lne.getValue(pnd_I).notEquals(" ")))                                                                                        //Natural: IF ADDRSS-LNE ( #I ) NE ' '
                {
                    miscellaneous_Variables_Pnd_Line_Coun.nadd(1);                                                                                                        //Natural: ADD 1 TO #LINE-COUN
                    pnd_I_Save.setValue(pnd_I);                                                                                                                           //Natural: ASSIGN #I-SAVE := #I
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Name_Out_Addrss_Lne_6_K.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rout, pnd_Name_Intl_Eft_Pay_Type_Cde, pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr)); //Natural: COMPRESS #ROUT #NAME.INTL-EFT-PAY-TYPE-CDE #NAME.INTL-BANK-PYMNT-EFT-NMBR INTO #NAME-OUT.ADDRSS-LNE-6-K LEAVING NO SPACE
            //*  ********************************************************
            if (condition(miscellaneous_Variables_Pnd_Line_Coun.greater(3)))                                                                                              //Natural: IF #LINE-COUN GT 3
            {
                pnd_I_Save.nsubtract(1);                                                                                                                                  //Natural: ASSIGN #I-SAVE := #I-SAVE - 1
                pnd_I_S.setValue(4);                                                                                                                                      //Natural: ASSIGN #I-S := 4
                if (condition(miscellaneous_Variables_Pnd_Line_Coun.equals(4)))                                                                                           //Natural: IF #LINE-COUN = 4
                {
                    pnd_I_S.setValue(3);                                                                                                                                  //Natural: ASSIGN #I-S := 3
                }                                                                                                                                                         //Natural: END-IF
                REPEAT03:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pnd_I_S.equals(getZero()))) {break;}                                                                                                    //Natural: UNTIL #I-S = 0
                    if (condition(pnd_Name_Addrss_Lne.getValue(pnd_I_Save).notEquals(" ")))                                                                               //Natural: IF ADDRSS-LNE ( #I-SAVE ) NE ' '
                    {
                        pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(pnd_I_S).setValue(pnd_Name_Addrss_Lne.getValue(pnd_I_Save));                                           //Natural: ASSIGN ADDRSS-LNE-S ( #I-S ) := ADDRSS-LNE ( #I-SAVE )
                        pnd_I_S.nsubtract(1);                                                                                                                             //Natural: ASSIGN #I-S := #I-S - 1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_I_Save.nsubtract(1);                                                                                                                              //Natural: ASSIGN #I-SAVE := #I-SAVE - 1
                }                                                                                                                                                         //Natural: END-REPEAT
                if (Global.isEscape()) return;
                pnd_Name_Out_Cntrct_Name_Free_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(1));                                                                  //Natural: ASSIGN #NAME-OUT.CNTRCT-NAME-FREE-K := ADDRSS-LNE-S ( 1 )
                pnd_Name_Out_Addrss_Lne_1_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(2));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-1-K := ADDRSS-LNE-S ( 2 )
                pnd_Name_Out_Addrss_Lne_2_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(3));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-2-K := ADDRSS-LNE-S ( 3 )
                pnd_Name_Out_Addrss_Lne_3_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(4));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-3-K := ADDRSS-LNE-S ( 4 )
                pnd_Name_Out_Addrss_Lne_4_K.setValue(" ");                                                                                                                //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-4-K := ' '
                pnd_Name_Out_Addrss_Lne_5_K.setValue(" ");                                                                                                                //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-5-K := ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Line_Coun.less(4)))                                                                                                 //Natural: IF #LINE-COUN LT 4
            {
                pnd_I_S.reset();                                                                                                                                          //Natural: RESET #I-S
                FOR02:                                                                                                                                                    //Natural: FOR #I 2 TO 7
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Name_Addrss_Lne.getValue(pnd_I).notEquals(" ")))                                                                                    //Natural: IF ADDRSS-LNE ( #I ) NE ' '
                    {
                        pnd_I_S.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #I-S
                        pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(pnd_I_S).setValue(pnd_Name_Addrss_Lne.getValue(pnd_I));                                                //Natural: ASSIGN ADDRSS-LNE-S ( #I-S ) := ADDRSS-LNE ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Name_Out_Addrss_Lne_1_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(1));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-1-K := ADDRSS-LNE-S ( 1 )
                pnd_Name_Out_Addrss_Lne_2_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(2));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-2-K := ADDRSS-LNE-S ( 2 )
                pnd_Name_Out_Addrss_Lne_3_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(3));                                                                      //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-3-K := ADDRSS-LNE-S ( 3 )
                pnd_Name_Out_Addrss_Lne_4_K.setValue(" ");                                                                                                                //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-4-K := ' '
                pnd_Name_Out_Addrss_Lne_5_K.setValue(" ");                                                                                                                //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-5-K := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Name_Intl_Eft_Pay_Type_Cde.equals("SW")))                                                                                                       //Natural: IF #NAME.INTL-EFT-PAY-TYPE-CDE = 'SW'
        {
            pnd_Name_Out_Addrss_Lne_5_K.setValue(" ");                                                                                                                    //Natural: ASSIGN #NAME-OUT.ADDRSS-LNE-5-K := ' '
            pnd_Name_Out_Addrss_Lne_6_K.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rout, pnd_Name_Intl_Eft_Pay_Type_Cde, pnd_Name_Intl_Bank_Pymnt_Eft_Nmbr)); //Natural: COMPRESS #ROUT #NAME.INTL-EFT-PAY-TYPE-CDE #NAME.INTL-BANK-PYMNT-EFT-NMBR INTO #NAME-OUT.ADDRSS-LNE-6-K LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ********************************************************
        //*  ACATION-OUT.V-INTL-EFT-PAY-TYPE-CDE:= 'SW'
        if (condition(pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde.notEquals(" ") && pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde.notEquals("SW")))                               //Natural: IF #VACATION-OUT.V-INTL-EFT-PAY-TYPE-CDE NE ' ' AND #VACATION-OUT.V-INTL-EFT-PAY-TYPE-CDE NE 'SW'
        {
            miscellaneous_Variables_Pnd_Line_Coun.reset();                                                                                                                //Natural: RESET #LINE-COUN
            pnd_I_Save.reset();                                                                                                                                           //Natural: RESET #I-SAVE
            pnd_Cntrct_Name_Save.reset();                                                                                                                                 //Natural: RESET #CNTRCT-NAME-SAVE
            FOR03:                                                                                                                                                        //Natural: FOR #I 2 TO 7
            for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
            {
                if (condition(pnd_Vacation_Addrss_Lne_V.getValue(pnd_I).notEquals(" ")))                                                                                  //Natural: IF ADDRSS-LNE-V ( #I ) NE ' '
                {
                    miscellaneous_Variables_Pnd_Line_Coun.nadd(1);                                                                                                        //Natural: ADD 1 TO #LINE-COUN
                    pnd_I_Save.setValue(pnd_I);                                                                                                                           //Natural: ASSIGN #I-SAVE := #I
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Vacation_Out_V_Addrss_Lne_6_K.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rout, pnd_Vacation_V_Intl_Eft_Pay_Type_Cde,                    //Natural: COMPRESS #ROUT #VACATION.V-INTL-EFT-PAY-TYPE-CDE #VACATION.V-INTL-BANK-PYMNT-EFT-NMBR INTO #VACATION-OUT.V-ADDRSS-LNE-6-K LEAVING NO SPACE
                pnd_Vacation_V_Intl_Bank_Pymnt_Eft_Nmbr));
            if (condition(miscellaneous_Variables_Pnd_Line_Coun.greater(3)))                                                                                              //Natural: IF #LINE-COUN GT 3
            {
                pnd_I_Save.nsubtract(1);                                                                                                                                  //Natural: ASSIGN #I-SAVE := #I-SAVE - 1
                pnd_I_S.setValue(4);                                                                                                                                      //Natural: ASSIGN #I-S := 4
                if (condition(miscellaneous_Variables_Pnd_Line_Coun.equals(4)))                                                                                           //Natural: IF #LINE-COUN = 4
                {
                    pnd_I_S.setValue(3);                                                                                                                                  //Natural: ASSIGN #I-S := 3
                }                                                                                                                                                         //Natural: END-IF
                REPEAT04:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pnd_I_S.equals(getZero()))) {break;}                                                                                                    //Natural: UNTIL #I-S = 0
                    if (condition(pnd_Vacation_Addrss_Lne_V.getValue(pnd_I_Save).notEquals(" ")))                                                                         //Natural: IF ADDRSS-LNE-V ( #I-SAVE ) NE ' '
                    {
                        pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(pnd_I_S).setValue(pnd_Vacation_Addrss_Lne_V.getValue(pnd_I_Save));                                     //Natural: ASSIGN ADDRSS-LNE-S ( #I-S ) := ADDRSS-LNE-V ( #I-SAVE )
                        pnd_I_S.nsubtract(1);                                                                                                                             //Natural: ASSIGN #I-S := #I-S - 1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_I_Save.nsubtract(1);                                                                                                                              //Natural: ASSIGN #I-SAVE := #I-SAVE - 1
                }                                                                                                                                                         //Natural: END-REPEAT
                if (Global.isEscape()) return;
                pnd_Name_Out_Cntrct_Name_Free_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(1));                                                                  //Natural: ASSIGN #NAME-OUT.CNTRCT-NAME-FREE-K := ADDRSS-LNE-S ( 1 )
                pnd_Vacation_Out_V_Addrss_Lne_1_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(2));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-1-K := ADDRSS-LNE-S ( 2 )
                pnd_Vacation_Out_V_Addrss_Lne_2_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(3));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-2-K := ADDRSS-LNE-S ( 3 )
                pnd_Vacation_Out_V_Addrss_Lne_3_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(4));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-3-K := ADDRSS-LNE-S ( 4 )
                pnd_Vacation_Out_V_Addrss_Lne_4_K.setValue(" ");                                                                                                          //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-4-K := ' '
                pnd_Vacation_Out_V_Addrss_Lne_5_K.setValue(" ");                                                                                                          //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-5-K := ' '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(miscellaneous_Variables_Pnd_Line_Coun.less(4)))                                                                                                 //Natural: IF #LINE-COUN LT 4
            {
                pnd_I_S.reset();                                                                                                                                          //Natural: RESET #I-S
                FOR04:                                                                                                                                                    //Natural: FOR #I 2 TO 7
                for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Vacation_Addrss_Lne_V.getValue(pnd_I).notEquals(" ")))                                                                              //Natural: IF ADDRSS-LNE-V ( #I ) NE ' '
                    {
                        pnd_I_S.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #I-S
                        pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(pnd_I_S).setValue(pnd_Vacation_Addrss_Lne_V.getValue(pnd_I));                                          //Natural: ASSIGN ADDRSS-LNE-S ( #I-S ) := ADDRSS-LNE-V ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Vacation_Out_V_Addrss_Lne_1_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(1));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-1-K := ADDRSS-LNE-S ( 1 )
                pnd_Vacation_Out_V_Addrss_Lne_2_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(2));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-2-K := ADDRSS-LNE-S ( 2 )
                pnd_Vacation_Out_V_Addrss_Lne_3_K.setValue(pnd_Cntrct_Name_Save_Addrss_Lne_S.getValue(3));                                                                //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-3-K := ADDRSS-LNE-S ( 3 )
                pnd_Vacation_Out_V_Addrss_Lne_4_K.setValue(" ");                                                                                                          //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-4-K := ' '
                pnd_Vacation_Out_V_Addrss_Lne_5_K.setValue(" ");                                                                                                          //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-5-K := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Vacation_Out_V_Intl_Eft_Pay_Type_Cde.equals("SW")))                                                                                             //Natural: IF #VACATION-OUT.V-INTL-EFT-PAY-TYPE-CDE = 'SW'
        {
            pnd_Vacation_Out_V_Addrss_Lne_5_K.setValue(" ");                                                                                                              //Natural: ASSIGN #VACATION-OUT.V-ADDRSS-LNE-5-K := ' '
            pnd_Vacation_Out_V_Addrss_Lne_6_K.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rout, pnd_Vacation_V_Intl_Eft_Pay_Type_Cde,                    //Natural: COMPRESS #ROUT #VACATION.V-INTL-EFT-PAY-TYPE-CDE #VACATION.V-INTL-BANK-PYMNT-EFT-NMBR INTO #VACATION-OUT.V-ADDRSS-LNE-6-K LEAVING NO SPACE
                pnd_Vacation_V_Intl_Bank_Pymnt_Eft_Nmbr));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Name_Gt_Vacation() throws Exception                                                                                                                  //Natural: NAME-GT-VACATION
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Vacation_V_Cntrct_Payee.greaterOrEqual(pnd_Name_Cntrct_Payee)))                                                                             //Natural: IF #VACATION.V-CNTRCT-PAYEE GE #NAME.CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-VACATION
                sub_Write_Vacation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Vacation_V_Cntrct_Payee.equals(pnd_Name_Cntrct_Payee)))                                                                                         //Natural: IF #VACATION.V-CNTRCT-PAYEE = #NAME.CNTRCT-PAYEE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH
            sub_Write_Both();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME
            sub_Write_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Name_Lt_Vacation() throws Exception                                                                                                                  //Natural: NAME-LT-VACATION
    {
        if (BLNatReinput.isReinput()) return;

        REPEAT06:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Name_Cntrct_Payee.greaterOrEqual(pnd_Vacation_V_Cntrct_Payee)))                                                                             //Natural: IF #NAME.CNTRCT-PAYEE GE #VACATION.V-CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME
                sub_Write_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pnd_Vacation_V_Cntrct_Payee.equals(pnd_Name_Cntrct_Payee)))                                                                                         //Natural: IF #VACATION.V-CNTRCT-PAYEE = #NAME.CNTRCT-PAYEE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BOTH
            sub_Write_Both();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-VACATION
            sub_Write_Vacation();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Vacation() throws Exception                                                                                                                    //Natural: WRITE-VACATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name_Out.reset();                                                                                                                                             //Natural: RESET #NAME-OUT
        pnd_Name_Out_Ph_Unque_Id_Nmbr.setValue(pnd_Vacation_V_Ph_Unque_Id_Nmbr);                                                                                          //Natural: ASSIGN #NAME-OUT.PH-UNQUE-ID-NMBR := #VACATION.V-PH-UNQUE-ID-NMBR
        pnd_Name_Out_Cntrct_Payee.setValue(pnd_Vacation_V_Cntrct_Payee);                                                                                                  //Natural: ASSIGN #NAME-OUT.CNTRCT-PAYEE := #VACATION.V-CNTRCT-PAYEE
        pnd_Vacation_Out.setValuesByName(pnd_Vacation);                                                                                                                   //Natural: MOVE BY NAME #VACATION TO #VACATION-OUT
                                                                                                                                                                          //Natural: PERFORM READ-LEGAL-OVERRIDE
        sub_Read_Legal_Override();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(4, false, pnd_Name_Out, pnd_Vacation_Out);                                                                                                   //Natural: WRITE WORK FILE 4 #NAME-OUT #VACATION-OUT
        miscellaneous_Variables_Pnd_Vacation_Ctr_Out.nadd(1);                                                                                                             //Natural: ADD 1 TO #VACATION-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-VACATION
        sub_Read_Vacation();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Name() throws Exception                                                                                                                        //Natural: WRITE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Vacation_Out.reset();                                                                                                                                         //Natural: RESET #VACATION-OUT
        pnd_Name_Out_Ph_Unque_Id_Nmbr.setValue(pnd_Name_Ph_Unque_Id_Nmbr);                                                                                                //Natural: ASSIGN #NAME-OUT.PH-UNQUE-ID-NMBR := #NAME.PH-UNQUE-ID-NMBR
        pnd_Name_Out_Cntrct_Payee.setValue(pnd_Name_Cntrct_Payee);                                                                                                        //Natural: ASSIGN #NAME-OUT.CNTRCT-PAYEE := #NAME.CNTRCT-PAYEE
        pnd_Name_Out.setValuesByName(pnd_Name);                                                                                                                           //Natural: MOVE BY NAME #NAME TO #NAME-OUT
                                                                                                                                                                          //Natural: PERFORM READ-LEGAL-OVERRIDE
        sub_Read_Legal_Override();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(4, false, pnd_Name_Out, pnd_Vacation_Out);                                                                                                   //Natural: WRITE WORK FILE 4 #NAME-OUT #VACATION-OUT
        miscellaneous_Variables_Pnd_Name_Ctr_Out.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NAME-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-NAME
        sub_Read_Name();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Both() throws Exception                                                                                                                        //Natural: WRITE-BOTH
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name_Out.reset();                                                                                                                                             //Natural: RESET #NAME-OUT #VACATION-OUT
        pnd_Vacation_Out.reset();
        pnd_Name_Out.setValuesByName(pnd_Name);                                                                                                                           //Natural: MOVE BY NAME #NAME TO #NAME-OUT
        pnd_Vacation_Out.setValuesByName(pnd_Vacation);                                                                                                                   //Natural: MOVE BY NAME #VACATION TO #VACATION-OUT
                                                                                                                                                                          //Natural: PERFORM READ-LEGAL-OVERRIDE
        sub_Read_Legal_Override();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(4, false, pnd_Name_Out, pnd_Vacation_Out);                                                                                                   //Natural: WRITE WORK FILE 4 #NAME-OUT #VACATION-OUT
        miscellaneous_Variables_Pnd_Both_Ctr_Out.nadd(1);                                                                                                                 //Natural: ADD 1 TO #BOTH-CTR-OUT
                                                                                                                                                                          //Natural: PERFORM READ-VACATION
        sub_Read_Vacation();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-NAME
        sub_Read_Name();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM());                                                                                                   //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM
        getReports().write(0, "LINE NUMBER: ",Global.getERROR_LINE());                                                                                                    //Natural: WRITE 'LINE NUMBER: ' *ERROR-LINE
        getReports().write(0, "ERROR NUMBER: ",Global.getERROR_NR());                                                                                                     //Natural: WRITE 'ERROR NUMBER: ' *ERROR-NR
        getReports().write(0, "=",miscellaneous_Variables_Pnd_Name_Ctr,"=",miscellaneous_Variables_Pnd_Vacation_Ctr,"=",miscellaneous_Variables_Pnd_Name_Ctr_Out,         //Natural: WRITE '=' #NAME-CTR '=' #VACATION-CTR '=' #NAME-CTR-OUT '=' #VACATION-CTR-OUT '=' #BOTH-CTR-OUT
            "=",miscellaneous_Variables_Pnd_Vacation_Ctr_Out,"=",miscellaneous_Variables_Pnd_Both_Ctr_Out);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
