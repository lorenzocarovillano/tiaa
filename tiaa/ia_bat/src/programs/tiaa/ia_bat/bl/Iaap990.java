/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:35 PM
**        * FROM NATURAL PROGRAM : Iaap990
************************************************************
**        * FILE NAME            : Iaap990.java
**        * CLASS NAME           : Iaap990
**        * INSTANCE NAME        : Iaap990
************************************************************
************************************************************************
* PROGRAM   : IAAP990
*
* FUNCTION  : UPDATE LAST CHANGE DATE
*
* DATE      : 4/05/1996
*
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap990 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;

    private DbsGroup pnd_Cntrct_Payee_Key_Pnd_Key_Struct;
    private DbsField pnd_Cntrct_Payee_Key_Trans_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Trans_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte;

    private DbsGroup pnd_Trans_Chck_Dte__R_Field_2;
    private DbsField pnd_Trans_Chck_Dte_Pnd_Trans_Chck_Dte_6;

    private DbsGroup pnd_Old;
    private DbsField pnd_Old_Trans_Ppcn_Nbr;
    private DbsField pnd_Old_Trans_Payee_Cde;
    private DbsField pnd_Et_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);

        pnd_Cntrct_Payee_Key_Pnd_Key_Struct = pnd_Cntrct_Payee_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Payee_Key_Pnd_Key_Struct", "#KEY-STRUCT");
        pnd_Cntrct_Payee_Key_Trans_Ppcn_Nbr = pnd_Cntrct_Payee_Key_Pnd_Key_Struct.newFieldInGroup("pnd_Cntrct_Payee_Key_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Trans_Payee_Cde = pnd_Cntrct_Payee_Key_Pnd_Key_Struct.newFieldInGroup("pnd_Cntrct_Payee_Key_Trans_Payee_Cde", "TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte", "#TRANS-CHCK-DTE", FieldType.STRING, 8);

        pnd_Trans_Chck_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte__R_Field_2", "REDEFINE", pnd_Trans_Chck_Dte);
        pnd_Trans_Chck_Dte_Pnd_Trans_Chck_Dte_6 = pnd_Trans_Chck_Dte__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Pnd_Trans_Chck_Dte_6", "#TRANS-CHCK-DTE-6", 
            FieldType.NUMERIC, 6);

        pnd_Old = localVariables.newGroupInRecord("pnd_Old", "#OLD");
        pnd_Old_Trans_Ppcn_Nbr = pnd_Old.newFieldInGroup("pnd_Old_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10);
        pnd_Old_Trans_Payee_Cde = pnd_Old.newFieldInGroup("pnd_Old_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap990() throws Exception
    {
        super("Iaap990");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "PRT");                                                                                                                             //Natural: DEFINE PRINTER ( PRT = 01 ) OUTPUT 'CMPRT01'
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "R1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("R1")))
        {
            pnd_Trans_Chck_Dte.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #TRANS-CHCK-DTE
            pnd_Trans_Chck_Dte_Key.setValue(pnd_Trans_Chck_Dte);                                                                                                          //Natural: ASSIGN #TRANS-CHCK-DTE-KEY := #TRANS-CHCK-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "R2",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        R2:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("R2")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.greater(pnd_Trans_Chck_Dte.val())))                                                                              //Natural: IF IAA-TRANS-RCRD.TRANS-CHECK-DTE GT VAL ( #TRANS-CHCK-DTE )
            {
                if (true) break R2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R2. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(55)))                                                                                                           //Natural: REJECT IF IAA-TRANS-RCRD.TRANS-CDE = 55
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.notEquals(pnd_Old_Trans_Ppcn_Nbr) || iaa_Trans_Rcrd_Trans_Payee_Cde.notEquals(pnd_Old_Trans_Payee_Cde)))          //Natural: IF IAA-TRANS-RCRD.TRANS-PPCN-NBR NE #OLD.TRANS-PPCN-NBR OR IAA-TRANS-RCRD.TRANS-PAYEE-CDE NE #OLD.TRANS-PAYEE-CDE
            {
                pnd_Cntrct_Payee_Key_Pnd_Key_Struct.setValuesByName(vw_iaa_Trans_Rcrd);                                                                                   //Natural: MOVE BY NAME IAA-TRANS-RCRD TO #KEY-STRUCT
                                                                                                                                                                          //Natural: PERFORM UPDATE-CPR-RECORD
                sub_Update_Cpr_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Old.setValuesByName(vw_iaa_Trans_Rcrd);                                                                                                               //Natural: MOVE BY NAME IAA-TRANS-RCRD TO #OLD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CPR-RECORD
    }
    private void sub_Update_Cpr_Record() throws Exception                                                                                                                 //Natural: UPDATE-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R3:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("R3")))
        {
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cntrct_Payee_Key_Trans_Ppcn_Nbr) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(pnd_Cntrct_Payee_Key_Trans_Payee_Cde))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #CNTRCT-PAYEE-KEY.TRANS-PPCN-NBR OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PAYEE-KEY.TRANS-PAYEE-CDE
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            G1:                                                                                                                                                           //Natural: GET IAA-CNTRCT-PRTCPNT-ROLE *ISN ( R3. )
            vw_iaa_Cntrct_Prtcpnt_Role.readByID(vw_iaa_Cntrct_Prtcpnt_Role.getAstISN("R3"), "G1");
            iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte.setValue(pnd_Trans_Chck_Dte_Pnd_Trans_Chck_Dte_6);                                                               //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-LST-CHNGE-DTE := #TRANS-CHCK-DTE-6
            vw_iaa_Cntrct_Prtcpnt_Role.updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            if (condition(pnd_Et_Count.greaterOrEqual(50)))                                                                                                               //Natural: IF #ET-COUNT GE 50
            {
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(20),"UPDATED CONTRACT PAYEE REPORT",new ColumnSpacing(20),Global.getDATX(),NEWLINE,"        ",new  //Natural: WRITE ( 1 ) *PROGRAM 20X 'UPDATED CONTRACT PAYEE REPORT' 20X *DATX / '        '20X 'STATUS CODE FIX FOR TRANSFER REQUEST'
                        ColumnSpacing(20),"STATUS CODE FIX FOR TRANSFER REQUEST");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133");
    }
}
