/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:22 PM
**        * FROM NATURAL PROGRAM : Iaap950
************************************************************
**        * FILE NAME            : Iaap950.java
**        * CLASS NAME           : Iaap950
**        * INSTANCE NAME        : Iaap950
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP950    READS IAA TRANS RECORD                 *
*      DATE     -  10/94      CREATES VERIFICATION REPORT            *
*  04/2017 OS RE-STOWED FOR IAAL950 PIN EXPANSION.                   *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap950 extends BLNatBase
{
    // Data Areas
    private LdaIaal950 ldaIaal950;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Trans__R_Field_1;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;

    private DataAccessProgramView vw_iaa_Cref_Fund_Trans;
    private DbsField iaa_Cref_Fund_Trans_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde;

    private DbsGroup iaa_Cref_Fund_Trans__R_Field_2;
    private DbsField iaa_Cref_Fund_Trans_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_Aftr_Imge_Id;
    private DbsField pnd_Check_Date_Ccyymmdd;
    private DbsField pnd_Total_Units;
    private DbsField pnd_Total_Per_Pay;
    private DbsField pnd_Total_Per_Div;
    private DbsField pnd_Sve_Fund_Cde;

    private DbsGroup pnd_Sve_Fund_Cde__R_Field_3;
    private DbsField pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1;
    private DbsField pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;
    private DbsField pnd_Trans_Date;
    private DbsField pnd_Trans_Time;
    private DbsField pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Code;
    private DbsField pnd_Fund_Trans_Key;

    private DbsGroup pnd_Fund_Trans_Key__R_Field_4;
    private DbsField pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde;
    private DbsField pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal950 = new LdaIaal950();
        registerRecord(ldaIaal950);
        registerRecord(ldaIaal950.getVw_iaa_Cntrl_Rcrd());
        registerRecord(ldaIaal950.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal950.getVw_iaa_Cpr_Trans());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_CNTRCT_PAYEE_CDE", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Trans__R_Field_1 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans__R_Field_1", "REDEFINE", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans__R_Field_1.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("IAA_TIAA_FUND_TRANS_TIAA_TOT_DIV_AMT", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_TRANS_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Tiaa_Fund_Trans);

        vw_iaa_Cref_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans", "IAA-CREF-FUND-TRANS"), "IAA_CREF_FUND_TRANS_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("IAA_CREF_FUND_TRANS_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Cref_Fund_Trans__R_Field_2 = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans__R_Field_2", "REDEFINE", iaa_Cref_Fund_Trans_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Cref_Fund_Trans_Cref_Fund_Cde = iaa_Cref_Fund_Trans__R_Field_2.newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");

        iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans.getRecord().newGroupInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Cde = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Rate_Dte = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Cref_Units_Cnt = iaa_Cref_Fund_Trans_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_CREF_FUND_TRANS_CREF_UNITS_CNT", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Cref_Fund_Trans);

        pnd_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Check_Date_Ccyymmdd", "#CHECK-DATE-CCYYMMDD", FieldType.STRING, 8);
        pnd_Total_Units = localVariables.newFieldInRecord("pnd_Total_Units", "#TOTAL-UNITS", FieldType.NUMERIC, 9, 3);
        pnd_Total_Per_Pay = localVariables.newFieldInRecord("pnd_Total_Per_Pay", "#TOTAL-PER-PAY", FieldType.NUMERIC, 9, 2);
        pnd_Total_Per_Div = localVariables.newFieldInRecord("pnd_Total_Per_Div", "#TOTAL-PER-DIV", FieldType.NUMERIC, 9, 2);
        pnd_Sve_Fund_Cde = localVariables.newFieldInRecord("pnd_Sve_Fund_Cde", "#SVE-FUND-CDE", FieldType.STRING, 2);

        pnd_Sve_Fund_Cde__R_Field_3 = localVariables.newGroupInRecord("pnd_Sve_Fund_Cde__R_Field_3", "REDEFINE", pnd_Sve_Fund_Cde);
        pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1 = pnd_Sve_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1", "#SVE-FUND-CDE1", FieldType.STRING, 
            1);
        pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2 = pnd_Sve_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde2", "#SVE-FUND-CDE2", FieldType.STRING, 
            1);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.NUMERIC, 4);
        pnd_Trans_Date = localVariables.newFieldInRecord("pnd_Trans_Date", "#TRANS-DATE", FieldType.STRING, 8);
        pnd_Trans_Time = localVariables.newFieldInRecord("pnd_Trans_Time", "#TRANS-TIME", FieldType.STRING, 8);
        pnd_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 10);
        pnd_Trans_Payee_Cde = localVariables.newFieldInRecord("pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Trans_Code = localVariables.newFieldInRecord("pnd_Trans_Code", "#TRANS-CODE", FieldType.NUMERIC, 3);
        pnd_Fund_Trans_Key = localVariables.newFieldInRecord("pnd_Fund_Trans_Key", "#FUND-TRANS-KEY", FieldType.STRING, 27);

        pnd_Fund_Trans_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Fund_Trans_Key__R_Field_4", "REDEFINE", pnd_Fund_Trans_Key);
        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id = pnd_Fund_Trans_Key__R_Field_4.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id", "#AFTR-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Fund_Trans_Key__R_Field_4.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde = pnd_Fund_Trans_Key__R_Field_4.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde = pnd_Fund_Trans_Key__R_Field_4.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Cntrct_Fund_Cde", "#CNTRCT-FUND-CDE", 
            FieldType.STRING, 2);
        pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date = pnd_Fund_Trans_Key__R_Field_4.newFieldInGroup("pnd_Fund_Trans_Key_Pnd_Invrse_Trans_Date", "#INVRSE-TRANS-DATE", 
            FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Trans.reset();

        ldaIaal950.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap950() throws Exception
    {
        super("Iaap950");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP950", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM READ-CNTRL-RCRD
        sub_Read_Cntrl_Rcrd();
        if (condition(Global.isEscape())) {return;}
        //*  SELECT RECORDS THAT HAVE NOT BEEN VERIFIED V IN VERIFY-CDE
        //*       V = RECORD HAS TO BE VERIFIED
        //*  BLANK  = RECORD HAS BEEN  VERIFIED
        ldaIaal950.getVw_iaa_Trans_Rcrd().startDatabaseRead                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #CHECK-DATE-CCYYMMDD
        (
        "READ01",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Check_Date_Ccyymmdd, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaIaal950.getVw_iaa_Trans_Rcrd().readNextRow("READ01")))
        {
            if (condition(ldaIaal950.getIaa_Trans_Rcrd_Trans_Check_Dte().notEquals(ldaIaal950.getPnd_Parm_Check_Dte_N())))                                                //Natural: REJECT IAA-TRANS-RCRD.TRANS-CHECK-DTE NE #PARM-CHECK-DTE-N
            {
                continue;
            }
            if (condition(ldaIaal950.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().equals(pnd_Trans_Ppcn_Nbr) && ldaIaal950.getIaa_Trans_Rcrd_Trans_Payee_Cde().equals(pnd_Trans_Payee_Cde)  //Natural: REJECT IAA-TRANS-RCRD.TRANS-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-TRANS-RCRD.TRANS-PAYEE-CDE = #TRANS-PAYEE-CDE AND IAA-TRANS-RCRD.TRANS-CDE = #SAVE-TRANS-CDE
                && ldaIaal950.getIaa_Trans_Rcrd_Trans_Cde().equals(ldaIaal950.getPnd_Save_Trans_Cde())))
            {
                continue;
            }
            pnd_Trans_Ppcn_Nbr.setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                                                                   //Natural: ASSIGN #TRANS-PPCN-NBR := IAA-TRANS-RCRD.TRANS-PPCN-NBR
            pnd_Trans_Payee_Cde.setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                                                                 //Natural: ASSIGN #TRANS-PAYEE-CDE := IAA-TRANS-RCRD.TRANS-PAYEE-CDE
            ldaIaal950.getPnd_Save_Trans_Cde().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Cde());                                                                        //Natural: ASSIGN #SAVE-TRANS-CDE := IAA-TRANS-RCRD.TRANS-CDE
            if (condition(ldaIaal950.getIaa_Trans_Rcrd_Trans_Verify_Cde().equals("V")))                                                                                   //Natural: IF IAA-TRANS-RCRD.TRANS-VERIFY-CDE = 'V'
            {
                ldaIaal950.getPnd_Packed_Variables_Pnd_Records_Ctr().nadd(1);                                                                                             //Natural: ADD 1 TO #RECORDS-CTR
                pnd_Total_Per_Pay.setValue(0);                                                                                                                            //Natural: ASSIGN #TOTAL-PER-PAY := 0
                pnd_Total_Per_Div.setValue(0);                                                                                                                            //Natural: ASSIGN #TOTAL-PER-DIV := 0
                pnd_Total_Units.setValue(0);                                                                                                                              //Natural: ASSIGN #TOTAL-UNITS := 0
                ldaIaal950.getPnd_Save_Trans_Ppcn_Nbr().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                                          //Natural: ASSIGN #SAVE-TRANS-PPCN-NBR := TRANS-PPCN-NBR
                ldaIaal950.getPnd_Save_Trans_Payee_Cde().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                                        //Natural: ASSIGN #SAVE-TRANS-PAYEE-CDE := TRANS-PAYEE-CDE
                ldaIaal950.getPnd_Save_Trans_Cde().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Cde());                                                                    //Natural: ASSIGN #SAVE-TRANS-CDE := TRANS-CDE
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-CPR-TRANS
                sub_Get_Current_Cpr_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-FUND-TRANS
                sub_Get_Current_Fund_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,ldaIaal950.getPnd_Save_Trans_Ppcn_Nbr(),ldaIaal950.getPnd_Save_Trans_Payee_Cde(),new ColumnSpacing(5),ldaIaal950.getIaa_Trans_Rcrd_Trans_User_Area(),new  //Natural: WRITE ( 1 ) #SAVE-TRANS-PPCN-NBR #SAVE-TRANS-PAYEE-CDE 5X IAA-TRANS-RCRD.TRANS-USER-AREA 2X IAA-TRANS-RCRD.TRANS-USER-ID 3X IAA-TRANS-RCRD.TRANS-CDE 4X IAA-TRANS-RCRD.TRANS-DTE ( EM = MM/DD/YY ) 1X IAA-TRANS-RCRD.TRANS-DTE ( EM = HH:II:SS ) 4X #SVE-FUND-CDE1 2X #TOTAL-PER-PAY ( EM = Z,ZZZ,ZZZ.99 ) 2X #TOTAL-PER-DIV ( EM = Z,ZZZ,ZZZ.99 ) 3X #TOTAL-UNITS ( EM = ZZZ,ZZZ.999 )
                    ColumnSpacing(2),ldaIaal950.getIaa_Trans_Rcrd_Trans_User_Id(),new ColumnSpacing(3),ldaIaal950.getIaa_Trans_Rcrd_Trans_Cde(),new ColumnSpacing(4),ldaIaal950.getIaa_Trans_Rcrd_Trans_Dte(), 
                    new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(1),ldaIaal950.getIaa_Trans_Rcrd_Trans_Dte(), new ReportEditMask ("HH:II:SS"),new ColumnSpacing(4),pnd_Sve_Fund_Cde_Pnd_Sve_Fund_Cde1,new 
                    ColumnSpacing(2),pnd_Total_Per_Pay, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Total_Per_Div, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(3),pnd_Total_Units, new ReportEditMask ("ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE ( 1 ) / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"NUMBER OF TRANSACTIONS NOT VERIFIED   : ",ldaIaal950.getPnd_Packed_Variables_Pnd_Records_Ctr());              //Natural: WRITE ( 1 ) / 'NUMBER OF TRANSACTIONS NOT VERIFIED   : ' #RECORDS-CTR
        if (Global.isEscape()) return;
        //* ******************************************************************
        //*  GET CPR TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-CPR-TRANS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-FUND-TRANS
        //* ******************************************************************
        //*  GET TIAA FUND TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-FUND-RECORD
        //* ******************************************************************
        //*  GET CREF FUND TRANSACTION RECORD AFTER IMAGE
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CREF-FUND-RECORD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CNTRL-RCRD
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Get_Current_Cpr_Trans() throws Exception                                                                                                             //Natural: GET-CURRENT-CPR-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal950.getPnd_Iaa_Cpr_Trans_Key_Pnd_Aftr_Imge_Id().setValue("2");                                                                                             //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#AFTR-IMGE-ID := '2'
        ldaIaal950.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Ppcn_Nbr().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                                           //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PPCN-NBR := TRANS-PPCN-NBR
        ldaIaal950.getPnd_Iaa_Cpr_Trans_Key_Pnd_Cntrct_Part_Payee_Cde().setValue(ldaIaal950.getIaa_Trans_Rcrd_Trans_Payee_Cde());                                         //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#CNTRCT-PART-PAYEE-CDE := TRANS-PAYEE-CDE
        ldaIaal950.getPnd_Iaa_Cpr_Trans_Key_Pnd_Invrse_Trans_Dte().setValue(ldaIaal950.getIaa_Trans_Rcrd_Invrse_Trans_Dte());                                             //Natural: ASSIGN #IAA-CPR-TRANS-KEY.#INVRSE-TRANS-DTE := IAA-TRANS-RCRD.INVRSE-TRANS-DTE
        ldaIaal950.getVw_iaa_Cpr_Trans().startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) IAA-CPR-TRANS WITH CPR-AFTR-KEY = #IAA-CPR-TRANS-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CPR_AFTR_KEY", "=", ldaIaal950.getPnd_Iaa_Cpr_Trans_Key(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal950.getVw_iaa_Cpr_Trans().readNextRow("FIND01", true)))
        {
            ldaIaal950.getVw_iaa_Cpr_Trans().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal950.getVw_iaa_Cpr_Trans().getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORDS FOUND
            {
                getReports().write(1, ReportOption.NOTITLE,"NO TRANS PARTICIPANT RECORD FOUND FOR: ",ldaIaal950.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr());                      //Natural: WRITE ( 1 ) 'NO TRANS PARTICIPANT RECORD FOUND FOR: ' TRANS-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Current_Fund_Trans() throws Exception                                                                                                            //Natural: GET-CURRENT-FUND-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaIaal950.getIaa_Cpr_Trans_Cntrct_Company_Cd().getValue(1).equals("T") || ldaIaal950.getIaa_Cpr_Trans_Cntrct_Company_Cd().getValue(1).equals("G"))) //Natural: IF IAA-CPR-TRANS.CNTRCT-COMPANY-CD ( 1 ) = 'T' OR = 'G'
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-FUND-RECORD
            sub_Process_Tiaa_Fund_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-CREF-FUND-RECORD
            sub_Process_Cref_Fund_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tiaa_Fund_Record() throws Exception                                                                                                          //Natural: PROCESS-TIAA-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#AFTR-IMGE-ID := '2'
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Trans_Ppcn_Nbr);                                                                                              //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Trans_Payee_Cde);                                                                                            //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
        vw_iaa_Tiaa_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-TIAA-FUND-TRANS BY TIAA-FUND-AFTR-KEY STARTING FROM #FUND-TRANS-KEY
        (
        "READ02",
        new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", ">=", pnd_Fund_Trans_Key, WcType.BY) },
        new Oc[] { new Oc("TIAA_FUND_AFTR_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Tiaa_Fund_Trans.readNextRow("READ02")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr.equals(pnd_Trans_Ppcn_Nbr) && iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde.equals(pnd_Trans_Payee_Cde)))      //Natural: IF IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE = #TRANS-PAYEE-CDE
        {
            pnd_Total_Per_Pay.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt);                                                                                             //Natural: ASSIGN #TOTAL-PER-PAY := IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT
            pnd_Total_Per_Div.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt);                                                                                             //Natural: ASSIGN #TOTAL-PER-DIV := IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT
            pnd_Sve_Fund_Cde.setValue(iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde);                                                                                                 //Natural: ASSIGN #SVE-FUND-CDE := IAA-TIAA-FUND-TRANS.TIAA-FUND-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO TRANS TIAA-FUND RECORD FOUND FOR: ",pnd_Trans_Ppcn_Nbr);                                                                            //Natural: WRITE 'NO TRANS TIAA-FUND RECORD FOUND FOR: ' #TRANS-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Cref_Fund_Record() throws Exception                                                                                                          //Natural: PROCESS-CREF-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Trans_Key_Pnd_Aftr_Imge_Id.setValue("2");                                                                                                                //Natural: ASSIGN #FUND-TRANS-KEY.#AFTR-IMGE-ID := '2'
        pnd_Fund_Trans_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Trans_Ppcn_Nbr);                                                                                              //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PPCN-NBR := #TRANS-PPCN-NBR
        pnd_Fund_Trans_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Trans_Payee_Cde);                                                                                            //Natural: ASSIGN #FUND-TRANS-KEY.#CNTRCT-PAYEE-CDE := #TRANS-PAYEE-CDE
        vw_iaa_Cref_Fund_Trans.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) IAA-CREF-FUND-TRANS BY CREF-FUND-AFTR-KEY STARTING FROM #FUND-TRANS-KEY
        (
        "READ03",
        new Wc[] { new Wc("TIAA_FUND_AFTR_KEY", ">=", pnd_Fund_Trans_Key, WcType.BY) },
        new Oc[] { new Oc("TIAA_FUND_AFTR_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Cref_Fund_Trans.readNextRow("READ03")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(iaa_Cref_Fund_Trans_Cref_Cntrct_Ppcn_Nbr.equals(pnd_Trans_Ppcn_Nbr) && iaa_Cref_Fund_Trans_Cref_Cntrct_Payee_Cde.equals(pnd_Trans_Payee_Cde)))      //Natural: IF IAA-CREF-FUND-TRANS.CREF-CNTRCT-PPCN-NBR = #TRANS-PPCN-NBR AND IAA-CREF-FUND-TRANS.CREF-CNTRCT-PAYEE-CDE = #TRANS-PAYEE-CDE
        {
            pnd_Total_Units.compute(new ComputeParameters(false, pnd_Total_Units), DbsField.add(getZero(),iaa_Cref_Fund_Trans_Cref_Units_Cnt.getValue("*")));             //Natural: ASSIGN #TOTAL-UNITS := 0 + IAA-CREF-FUND-TRANS.CREF-UNITS-CNT ( * )
            pnd_Sve_Fund_Cde.setValue(iaa_Cref_Fund_Trans_Cref_Fund_Cde);                                                                                                 //Natural: ASSIGN #SVE-FUND-CDE := IAA-CREF-FUND-TRANS.CREF-FUND-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO TRANS CREF-FUND RECORD FOUND FOR: ",pnd_Trans_Ppcn_Nbr);                                                                            //Natural: WRITE 'NO TRANS CREF-FUND RECORD FOUND FOR: ' #TRANS-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Cntrl_Rcrd() throws Exception                                                                                                                   //Natural: READ-CNTRL-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //*  #CNTRL-CDE := 'AA'
        //*  COMPUTE #CNTRL-INVRSE-DTE = 100000000 - #PARM-CHECK-DTE-N
        ldaIaal950.getVw_iaa_Cntrl_Rcrd().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY
        (
        "READ04",
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(ldaIaal950.getVw_iaa_Cntrl_Rcrd().readNextRow("READ04")))
        {
            pnd_Check_Date_Ccyymmdd.setValueEdited(ldaIaal950.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED IAA-CNTRL-RCRD.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE-CCYYMMDD
            ldaIaal950.getPnd_Parm_Check_Dte_N().compute(new ComputeParameters(false, ldaIaal950.getPnd_Parm_Check_Dte_N()), pnd_Check_Date_Ccyymmdd.val());              //Natural: ASSIGN #PARM-CHECK-DTE-N := VAL ( #CHECK-DATE-CCYYMMDD )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_W_Page_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #W-PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(10),"IA ADMINISTRATION TRANSACTIONS NOT VERIFIED CONTROL REPORT FOR CHECK DATE: ",ldaIaal950.getIaa_Cntrl_Rcrd_Cntrl_Check_Dte(),new  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 10X 'IA ADMINISTRATION TRANSACTIONS NOT VERIFIED CONTROL REPORT FOR CHECK DATE: ' IAA-CNTRL-RCRD.CNTRL-CHECK-DTE 4X 'PAGE: ' #W-PAGE-CTR ( EM = ZZZ,ZZZ,ZZ9 )
                        ColumnSpacing(4),"PAGE: ",pnd_W_Page_Ctr, new ReportEditMask ("Z,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out);                                                                                //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT
                    getReports().write(1, ReportOption.NOTITLE," CONTRACT PAYEE    ---TRANS-USER---    TRANS   TRANS-TIME-STAMP  PROD  ");                                //Natural: WRITE ( 1 ) ' CONTRACT PAYEE    ---TRANS-USER---    TRANS   TRANS-TIME-STAMP  PROD  '
                    getReports().write(1, ReportOption.NOTITLE," NUMBER   CODE      AREA     ID        CODE     DATE ","  TIME     CODE PERIODIC-PMT  PERIODIC-DIV  CREF-UNITS"); //Natural: WRITE ( 1 ) ' NUMBER   CODE      AREA     ID        CODE     DATE ' '  TIME     CODE PERIODIC-PMT  PERIODIC-DIV  CREF-UNITS'
                    getReports().write(1, ReportOption.NOTITLE,"--------  ------   ------  --------    -----  --------","--------  ---- -------------  ------------   ---------"); //Natural: WRITE ( 1 ) '--------  ------   ------  --------    -----  --------' '--------  ---- -------------  ------------   ---------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                       //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
