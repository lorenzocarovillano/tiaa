/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:22 PM
**        * FROM NATURAL PROGRAM : Iaap110a
************************************************************
**        * FILE NAME            : Iaap110a.java
**        * CLASS NAME           : Iaap110a
**        * INSTANCE NAME        : Iaap110a
************************************************************
***********************************************************************
*                                                                     *
*   PROGRAM     -  IAAP110A    READ WORK FILE (CREATED IN IAAP100)    *
*      DATE     -  11/95       AND PRODUCE A SERIES OF MODE CONTROL   *
*    AUTHOR     -  ARI G.      REPORTS. THERE ARE A FIXED NUMBER OF   *
*                              ONE PAGE REPORTS AND A SERIES OF       *
*                              TOTAL REPORTS.                         *
*                              NOTE: FUND CODE '1 ' IS USED AS '1Z'   *
*                              IN THIS PROGRAM. THIS WAS DONE FOR     *
*                              SORTING PURPOSES.                      *
*                                                                     *
*                  REPORT 8 :- TIAA TOTALS REPORT                     *
*                  REPORT 9 :- CREF TOTALS REPORT                     *
*                                                                     *
*   HISTORY     -  04/97       ADDING NEW ANNUIT. LOGIC               *
*   HISTORY     -  05/00       PA SELECT FUNDS CHANGES                *
*   HISTORY     -  09/01       ADDED S0 PREFIX TO ACCUM OPTION 21     *
*                              REA FUNDS                             *
*                  04/02       RESTOWED FOR OIA CHANGES IN IAAN051A  *
*                              AND IAAN051E                          *
*                  06/03       ADDED NEW ROLL DEST CODES 57BT & 57BC *
*                              DO SCAN ON 6/03                       *
*                                                                    *
*                09/04  USE OUTPUT-UNITS-CNT TO CAPTURE ORIGIN CODES *
*                       03 (ISSUE-DATE GT 12/86), 17,18, 37,38 & 40  *
*                       TO REPORT THESE IN IAAP110A                  *
*                       DO SCAN ON 9/04 FOR CHANGES                  *
*                02/07  ADDED ORIGIN 43-46. 43-45 = 38;46=03         *
*                09/08  ADDED NEW TIAA ACCESS FUND TO BE PROCESSED   *
*                       LIKE REA
*                07/11  ADDED ORIGIN 35-36.                          *
*                03/12  FIELD EXPANSION FOR RATE BASE                *
*                       SCAN ON 03/12
*                08/12  FIX CONTRACT RANGES FOR GROUP CONTRACTS      *
*                       SCAN ON 08/12
*                04/15  CHANGE W0430000 THRU W0449999 CONTRACTS FROM *
*                       DIRECT PAY TO NON-PARTICIPATING. SCAN 04/15  *
*                10/17  CREATE A NEW CATEGORY FOR ORIGIN 46          *
*                       SCAN ON 10/2017
***********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap110a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Record;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_8;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_2;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Payee_Cde;

    private DbsGroup pnd_Input_Record__R_Field_5;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Mode_Ind;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte;
    private DbsField pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde;
    private DbsField pnd_Input_Record_Pnd_I_Ddctn_Per_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt;
    private DbsField pnd_Input_Record_Pnd_I_Cntrct_Optn_Code;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_T;
    private DbsField pnd_Cnt_Payee_Field;
    private DbsField pnd_Final_Pay_Flag;
    private DbsField pnd_Fmt3_Mode_Sub;
    private DbsField pnd_Fmt4_Cnt_Amt_Tot;
    private DbsField pnd_Fmt4_Per_Div_Tot;
    private DbsField pnd_Fmt3_Tiaa_Table_Tot;
    private DbsField pnd_Fmt3_Tiaa_Table;
    private DbsField pnd_Fmt5_Tiaa_Table_Tot;
    private DbsField pnd_Fmt5_Tiaa_Table;
    private DbsField pnd_Fmt6_Tiaa_Table_Tot;
    private DbsField pnd_Fmt6_Tiaa_Table;
    private DbsField pnd_Fmt7_Tiaa_Table_Tot;
    private DbsField pnd_Fmt7_Tiaa_Table;
    private DbsField pnd_Fmt8_Tiaa_Table_Tot;
    private DbsField pnd_Fmt8_Tiaa_Table;
    private DbsField pnd_Fmt9_Tiaa_Table_Tot;
    private DbsField pnd_Fmt9_Tiaa_Table;
    private DbsField pnd_Fmt10_Tiaa_Table_Tot;
    private DbsField pnd_Fmt10_Tiaa_Table;
    private DbsField pnd_Fmt11_Tiaa_Table_Tot;
    private DbsField pnd_Fmt11_Tiaa_Table;
    private DbsField pnd_Fmt3_Units_Tot;
    private DbsField pnd_Fmt3_Units;
    private DbsField pnd_Fmt3_Dollars;
    private DbsField pnd_Fmt3_Dollars_Tot;
    private DbsField pnd_Fmt3_Deductions;
    private DbsField pnd_Fmt5_Units_Tot;
    private DbsField pnd_Fmt5_Units;
    private DbsField pnd_Fmt5_Dollars;
    private DbsField pnd_Fmt5_Dollars_Tot;
    private DbsField pnd_Fmt5_Deductions;
    private DbsField pnd_Fmt6_Units_Tot;
    private DbsField pnd_Fmt6_Units;
    private DbsField pnd_Fmt6_Dollars;
    private DbsField pnd_Fmt6_Dollars_Tot;
    private DbsField pnd_Fmt6_Deductions;
    private DbsField pnd_Fmt7_Units_Tot;
    private DbsField pnd_Fmt7_Units;
    private DbsField pnd_Fmt7_Dollars;
    private DbsField pnd_Fmt7_Dollars_Tot;
    private DbsField pnd_Fmt7_Deductions;
    private DbsField pnd_Generic_Unit_Value;
    private DbsField pnd_Generic_Dollar_Amount;
    private DbsField pnd_Fund_Types;
    private DbsField pnd_Tiaa_Title;
    private DbsField pnd_Cref_Title;
    private DbsField pnd_Tiaa_Title_Tab;
    private DbsField pnd_Cref_Title_Tab;
    private DbsField pnd_Mode_1;
    private DbsField pnd_Cnt_Type_Sub;
    private DbsField pnd_Fund_Cde_Hold_Tot;

    private DbsGroup pnd_Fund_Cde_Hold_Tot__R_Field_7;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold;

    private DbsGroup pnd_Fund_Cde_Hold_Tot__R_Field_8;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1;
    private DbsField pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2;
    private DbsField pnd_Tiaa_Table_1;
    private DbsField pnd_Tiaa_Table_2;
    private DbsField pnd_Tiaa_Table_3;
    private DbsField pnd_Tiaa_Table_4;
    private DbsField pnd_Tiaa_Table_5;
    private DbsField pnd_Tiaa_Table_6;
    private DbsField pnd_Tiaa_Table_7;
    private DbsField pnd_Tiaa_Table_Tot;
    private DbsField pnd_Tiaa_Table_Tot_4;
    private DbsField pnd_Tiaa_Table_Tot_5;
    private DbsField pnd_Tiaa_Table_Tot_6;
    private DbsField pnd_Tiaa_Table_Tot_7;
    private DbsField pnd_Tiaa_Table_Tot_G;
    private DbsField pnd_Cref_Units_1;
    private DbsField pnd_Cref_Units_2;
    private DbsField pnd_Cref_Units_3;
    private DbsField pnd_Cref_Units_Total;
    private DbsField pnd_Cref_Dollars_1;
    private DbsField pnd_Cref_Dollars_2;
    private DbsField pnd_Cref_Dollars_3;
    private DbsField pnd_Cref_Dollars_Total;
    private DbsField pnd_Cref_Deduct_1;
    private DbsField pnd_Cref_Deduct_2;
    private DbsField pnd_Cref_Deduct_3;
    private DbsField pnd_Cref_Deduct_Total;
    private DbsField pnd_Cref_Units_Tot;
    private DbsField pnd_Cref_Dollars_Tot;
    private DbsField pnd_Cref_Deduct_Tot;
    private DbsField pnd_Mode_Table_2;
    private DbsField pnd_Unit_Text_Table;
    private DbsField pnd_Dollars_Text_Table;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_4;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_5;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_6;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_7;
    private DbsField pnd_Tiaa_Per_Ded_Tot;
    private DbsField pnd_Tiaa_Per_Ded_Tot_4;
    private DbsField pnd_Tiaa_Per_Ded_Tot_5;
    private DbsField pnd_Tiaa_Per_Ded_Tot_6;
    private DbsField pnd_Tiaa_Per_Ded_Tot_7;
    private DbsField pnd_Tiaa_Fin_Pay_Tot;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_4;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_5;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_6;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_7;
    private DbsField pnd_Tiaa_Per_Div_Tot;
    private DbsField pnd_Tiaa_Per_Div_Tot_4;
    private DbsField pnd_Tiaa_Per_Div_Tot_5;
    private DbsField pnd_Tiaa_Per_Div_Tot_6;
    private DbsField pnd_Tiaa_Per_Div_Tot_7;
    private DbsField pnd_Tiaa_Fin_Div_Tot;
    private DbsField pnd_Tiaa_Fin_Div_Tot_4;
    private DbsField pnd_Tiaa_Fin_Div_Tot_5;
    private DbsField pnd_Tiaa_Fin_Div_Tot_6;
    private DbsField pnd_Tiaa_Fin_Div_Tot_7;
    private DbsField pnd_Tiaa_Cnt_Amt_Tot_G;
    private DbsField pnd_Tiaa_Per_Ded_Tot_G;
    private DbsField pnd_Tiaa_Fin_Pay_Tot_G;
    private DbsField pnd_Tiaa_Per_Div_Tot_G;
    private DbsField pnd_Tiaa_Fin_Div_Tot_G;
    private DbsField pnd_Pay_Mode_Table;
    private DbsField pnd_W_Rec_3;

    private DbsGroup pnd_W_Rec_3__R_Field_9;
    private DbsField pnd_W_Rec_3_Pnd_W_Rec_3_A;

    private DbsGroup iaaa0510;
    private DbsField iaaa0510_Pnd_Cm_Std_Srt_Seq;
    private DbsField iaaa0510_Pnd_Ia_Std_Nm_Cd;
    private DbsField pnd_Tpa_Rinv_Cntrcts;
    private DbsField pnd_Tpa_Roll_Cntrcts;
    private DbsField pnd_Ipr_Roll_Cntrcts;
    private DbsField pnd_P_I_Roll_Cntrcts;
    private DbsField pnd_Ac_Roll_Cntrcts;
    private DbsField pnd_Tpa_Roll_Cntrcts_Tot;
    private DbsField pnd_Ipr_Roll_Cntrcts_Tot;
    private DbsField pnd_P_I_Roll_Cntrcts_Tot;
    private DbsField pnd_Ac_Roll_Cntrcts_Tot;
    private DbsField pnd_Tiaa_Non_Life;
    private DbsField pnd_Org_03_Tab_G;
    private DbsField pnd_Org_03_Tot_G;
    private DbsField pnd_Org_03_Due_G;
    private DbsField pnd_Org_46_Tab_G;
    private DbsField pnd_Org_46_Tot_G;
    private DbsField pnd_Org_46_Due_G;
    private DbsField pnd_Org_03_Tab_Std_S0;
    private DbsField pnd_Org_03_Tot_Std_S0;
    private DbsField pnd_Org_46_Tab_Std_S0;
    private DbsField pnd_Org_46_Tot_Std_S0;
    private DbsField pnd_Org_17_Tab_Std_S0;
    private DbsField pnd_Org_17_Tot_Std_S0;
    private DbsField pnd_Org_37_Tab_Std_S0;
    private DbsField pnd_Org_37_Tot_Std_S0;
    private DbsField pnd_Org_03_Tab_S0_Pi;
    private DbsField pnd_Org_03_Tot_S0_Pi;
    private DbsField pnd_Org_46_Tab_S0_Pi;
    private DbsField pnd_Org_46_Tot_S0_Pi;
    private DbsField pnd_Org_17_Tab_S0_Pi;
    private DbsField pnd_Org_17_Tot_S0_Pi;
    private DbsField pnd_Org_37_Tab_S0_Pi;
    private DbsField pnd_Org_37_Tot_S0_Pi;
    private DbsField pnd_Org_03_Tab_Pi;
    private DbsField pnd_Org_03_Tot_Pi;
    private DbsField pnd_Org_46_Tab_Pi;
    private DbsField pnd_Org_46_Tot_Pi;
    private DbsField pnd_Org_17_Tab_Pi;
    private DbsField pnd_Org_17_Tot_Pi;
    private DbsField pnd_Org_37_Tab_Pi;
    private DbsField pnd_Org_37_Tot_Pi;
    private DbsField pnd_Org_03_Tab;
    private DbsField pnd_Org_03_Tot;
    private DbsField pnd_Org_03_Due;
    private DbsField pnd_Org_46_Tab;
    private DbsField pnd_Org_46_Tot;
    private DbsField pnd_Org_46_Due;
    private DbsField pnd_Org_17_Tab;
    private DbsField pnd_Org_17_Tot;
    private DbsField pnd_Org_17_Due;
    private DbsField pnd_Org_37_Tab;
    private DbsField pnd_Org_37_Tot;
    private DbsField pnd_Org_37_Due;
    private DbsField pnd_Org_18_Tab;
    private DbsField pnd_Org_18_Tot;
    private DbsField pnd_Org_18_Due;
    private DbsField pnd_Org_38_Tab;
    private DbsField pnd_Org_38_Tot;
    private DbsField pnd_Org_38_Due;
    private DbsField pnd_Org_40_Tab;
    private DbsField pnd_Org_40_Tot;
    private DbsField pnd_Org_40_Due;
    private DbsField pnd_Z;
    private DbsField pnd_Blnk5;
    private DbsField pnd_U;
    private DbsField pnd_Y;
    private DbsField pnd_G;
    private DbsField pnd_Sub;
    private DbsField pnd_Pa_Contract;
    private DbsField pnd_Cref_Contract;
    private DbsField pnd_End_Of_Tiaa;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Table_Date;
    private DbsField pnd_W_Save_Check_Dte_A;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_10;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Cc;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Yy;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Mm;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Dd;

    private DbsGroup pnd_W_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_15;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_16;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_17;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_18;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_19;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_20;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_21;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Return_Code;

    private DbsGroup pnd_Table;
    private DbsField pnd_Table_Pnd_Tbl_Fund_Cde;
    private DbsField pnd_Table_Pnd_Tbl_Unit_Value;
    private DbsField pnd_Table_Pnd_Tbl_Issue_Date;
    private DbsField pnd_Cnt_Tot;
    private DbsField pnd_Cnt1;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Cnt_None;
    private DbsField pnd_Cnt_1s;
    private DbsField pnd_Cnt_1s1;
    private DbsField pnd_Cnt_1s2;
    private DbsField pnd_Cnt_1s3;
    private DbsField pnd_Cnt_1s_None;
    private DbsField pnd_Cnt_1z;
    private DbsField pnd_Cnt_1z1;
    private DbsField pnd_Cnt_1z2;
    private DbsField pnd_Cnt_1z_None;
    private DbsField pnd_Cnt_Cref;
    private DbsField pnd_Cnt_Cref1;
    private DbsField pnd_Cnt_Cref2;
    private DbsField pnd_Cnt_Cref3;
    private DbsField pnd_Cnt_Cref_None;
    private DbsField pnd_Eof_Sw;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Page_Num;
    private DbsField pnd_Fn_Code;
    private DbsField pnd_Ck_Date;
    private DbsField pnd_U_Val;
    private DbsField pnd_Rc;
    private DbsField pnd_Issu_Date;
    private DbsField pnd_Is_Date;

    private DbsGroup pnd_Is_Date__R_Field_22;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Yyyy;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Mm;

    private DbsGroup pnd_Is_Date__R_Field_23;
    private DbsField pnd_Is_Date_Pnd_Is_Date_Mm_A;
    private DbsField pnd_Fund_Code_2;
    private DbsField pnd_Len;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_Fund_Desc;

    private DbsGroup pnd_Fund_Desc__R_Field_24;
    private DbsField pnd_Fund_Desc_Pnd_Fund_Desc_10;
    private DbsField pnd_T_Fund_Code;
    private DbsField pnd_Table_Fund_Desc;
    private DbsField pnd_Fnd_Desc;
    private DbsField pnd_W_Variable;
    private DbsField pnd_Heading1;
    private DbsField pnd_Debug;
    private DbsField pnd_Rollover_Title;
    private DbsField pnd_Print_Tpa_Ipro_Pi;
    private DbsField pnd_Pmt_Modes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Record = localVariables.newGroupInRecord("pnd_Input_Record", "#INPUT-RECORD");
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot", "#I-TIAA-FUND-CDE-TOT", 
            FieldType.STRING, 3);

        pnd_Input_Record__R_Field_1 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot);
        pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde", "#I-TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde", "#I-TIAA-FUND-CDE", 
            FieldType.STRING, 2);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_1", "#I-TIAA-FUND-CDE-1", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_2", "#I-TIAA-FUND-CDE-2", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_I_Cnt_Nbr = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr", "#I-CNT-NBR", FieldType.STRING, 10);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_I_Cnt_Nbr);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_8 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_8", "#I-CNT-NBR-8", FieldType.STRING, 
            8);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_I_Cnt_Nbr);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_2 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_2", "#I-CNT-NBR-2", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7", "#I-CNT-NBR-3-7", FieldType.NUMERIC, 
            5);
        pnd_Input_Record_Pnd_I_Cnt_Payee_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Payee_Cde", "#I-CNT-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        pnd_Input_Record__R_Field_5 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_5", "REDEFINE", pnd_Input_Record_Pnd_I_Cnt_Payee_Cde);
        pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A", "#I-CNT-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Actvty_Cde", "#I-CNT-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte", "#I-CNT-ISSUE-DTE", FieldType.NUMERIC, 
            6);

        pnd_Input_Record__R_Field_6 = pnd_Input_Record.newGroupInGroup("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record_Pnd_I_Cnt_Issue_Dte);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Yymm", "#I-CNT-ISSUE-DTE-YYMM", 
            FieldType.NUMERIC, 4);
        pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Issue_Dte_Dd", "#I-CNT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_I_Cnt_Mode_Ind = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Mode_Ind", "#I-CNT-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte", "#I-CNT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde", "#I-CNT-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Input_Record_Pnd_I_Ddctn_Per_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Ddctn_Per_Amt", "#I-DDCTN-PER-AMT", FieldType.NUMERIC, 
            5);
        pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt", "#I-TIAA-TOT-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Tot_Div_Amt", "#I-TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt", "#I-TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt", "#I-TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt", "#I-TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt", "#I-TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt", "#I-TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Record_Pnd_I_Cntrct_Optn_Code = pnd_Input_Record.newFieldInGroup("pnd_Input_Record_Pnd_I_Cntrct_Optn_Code", "#I-CNTRCT-OPTN-CODE", FieldType.NUMERIC, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 3);
        pnd_Cnt_Payee_Field = localVariables.newFieldInRecord("pnd_Cnt_Payee_Field", "#CNT-PAYEE-FIELD", FieldType.STRING, 11);
        pnd_Final_Pay_Flag = localVariables.newFieldInRecord("pnd_Final_Pay_Flag", "#FINAL-PAY-FLAG", FieldType.STRING, 1);
        pnd_Fmt3_Mode_Sub = localVariables.newFieldInRecord("pnd_Fmt3_Mode_Sub", "#FMT3-MODE-SUB", FieldType.NUMERIC, 1);
        pnd_Fmt4_Cnt_Amt_Tot = localVariables.newFieldInRecord("pnd_Fmt4_Cnt_Amt_Tot", "#FMT4-CNT-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt4_Per_Div_Tot = localVariables.newFieldInRecord("pnd_Fmt4_Per_Div_Tot", "#FMT4-PER-DIV-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt3_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt3_Tiaa_Table_Tot", "#FMT3-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt3_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt3_Tiaa_Table", "#FMT3-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt5_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt5_Tiaa_Table_Tot", "#FMT5-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt5_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt5_Tiaa_Table", "#FMT5-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt6_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt6_Tiaa_Table_Tot", "#FMT6-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt6_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt6_Tiaa_Table", "#FMT6-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt7_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt7_Tiaa_Table_Tot", "#FMT7-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt7_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt7_Tiaa_Table", "#FMT7-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt8_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt8_Tiaa_Table_Tot", "#FMT8-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt8_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt8_Tiaa_Table", "#FMT8-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt9_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt9_Tiaa_Table_Tot", "#FMT9-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt9_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt9_Tiaa_Table", "#FMT9-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt10_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt10_Tiaa_Table_Tot", "#FMT10-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt10_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt10_Tiaa_Table", "#FMT10-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt11_Tiaa_Table_Tot = localVariables.newFieldInRecord("pnd_Fmt11_Tiaa_Table_Tot", "#FMT11-TIAA-TABLE-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt11_Tiaa_Table = localVariables.newFieldArrayInRecord("pnd_Fmt11_Tiaa_Table", "#FMT11-TIAA-TABLE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Fmt3_Units_Tot = localVariables.newFieldInRecord("pnd_Fmt3_Units_Tot", "#FMT3-UNITS-TOT", FieldType.NUMERIC, 11, 3);
        pnd_Fmt3_Units = localVariables.newFieldArrayInRecord("pnd_Fmt3_Units", "#FMT3-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20, 
            1, 4));
        pnd_Fmt3_Dollars = localVariables.newFieldArrayInRecord("pnd_Fmt3_Dollars", "#FMT3-DOLLARS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            20, 1, 4));
        pnd_Fmt3_Dollars_Tot = localVariables.newFieldInRecord("pnd_Fmt3_Dollars_Tot", "#FMT3-DOLLARS-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt3_Deductions = localVariables.newFieldArrayInRecord("pnd_Fmt3_Deductions", "#FMT3-DEDUCTIONS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Fmt5_Units_Tot = localVariables.newFieldInRecord("pnd_Fmt5_Units_Tot", "#FMT5-UNITS-TOT", FieldType.NUMERIC, 11, 3);
        pnd_Fmt5_Units = localVariables.newFieldArrayInRecord("pnd_Fmt5_Units", "#FMT5-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20, 
            1, 4));
        pnd_Fmt5_Dollars = localVariables.newFieldArrayInRecord("pnd_Fmt5_Dollars", "#FMT5-DOLLARS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            20, 1, 4));
        pnd_Fmt5_Dollars_Tot = localVariables.newFieldInRecord("pnd_Fmt5_Dollars_Tot", "#FMT5-DOLLARS-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt5_Deductions = localVariables.newFieldArrayInRecord("pnd_Fmt5_Deductions", "#FMT5-DEDUCTIONS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Fmt6_Units_Tot = localVariables.newFieldInRecord("pnd_Fmt6_Units_Tot", "#FMT6-UNITS-TOT", FieldType.NUMERIC, 11, 3);
        pnd_Fmt6_Units = localVariables.newFieldArrayInRecord("pnd_Fmt6_Units", "#FMT6-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20, 
            1, 4));
        pnd_Fmt6_Dollars = localVariables.newFieldArrayInRecord("pnd_Fmt6_Dollars", "#FMT6-DOLLARS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            20, 1, 4));
        pnd_Fmt6_Dollars_Tot = localVariables.newFieldInRecord("pnd_Fmt6_Dollars_Tot", "#FMT6-DOLLARS-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt6_Deductions = localVariables.newFieldArrayInRecord("pnd_Fmt6_Deductions", "#FMT6-DEDUCTIONS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Fmt7_Units_Tot = localVariables.newFieldInRecord("pnd_Fmt7_Units_Tot", "#FMT7-UNITS-TOT", FieldType.NUMERIC, 11, 3);
        pnd_Fmt7_Units = localVariables.newFieldArrayInRecord("pnd_Fmt7_Units", "#FMT7-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20, 
            1, 4));
        pnd_Fmt7_Dollars = localVariables.newFieldArrayInRecord("pnd_Fmt7_Dollars", "#FMT7-DOLLARS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            20, 1, 4));
        pnd_Fmt7_Dollars_Tot = localVariables.newFieldInRecord("pnd_Fmt7_Dollars_Tot", "#FMT7-DOLLARS-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Fmt7_Deductions = localVariables.newFieldArrayInRecord("pnd_Fmt7_Deductions", "#FMT7-DEDUCTIONS", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Generic_Unit_Value = localVariables.newFieldInRecord("pnd_Generic_Unit_Value", "#GENERIC-UNIT-VALUE", FieldType.NUMERIC, 5, 2);
        pnd_Generic_Dollar_Amount = localVariables.newFieldInRecord("pnd_Generic_Dollar_Amount", "#GENERIC-DOLLAR-AMOUNT", FieldType.NUMERIC, 11, 2);
        pnd_Fund_Types = localVariables.newFieldInRecord("pnd_Fund_Types", "#FUND-TYPES", FieldType.NUMERIC, 1);
        pnd_Tiaa_Title = localVariables.newFieldInRecord("pnd_Tiaa_Title", "#TIAA-TITLE", FieldType.STRING, 60);
        pnd_Cref_Title = localVariables.newFieldInRecord("pnd_Cref_Title", "#CREF-TITLE", FieldType.STRING, 30);
        pnd_Tiaa_Title_Tab = localVariables.newFieldArrayInRecord("pnd_Tiaa_Title_Tab", "#TIAA-TITLE-TAB", FieldType.STRING, 60, new DbsArrayController(1, 
            4));
        pnd_Cref_Title_Tab = localVariables.newFieldArrayInRecord("pnd_Cref_Title_Tab", "#CREF-TITLE-TAB", FieldType.STRING, 30, new DbsArrayController(1, 
            4));
        pnd_Mode_1 = localVariables.newFieldInRecord("pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 2);
        pnd_Cnt_Type_Sub = localVariables.newFieldInRecord("pnd_Cnt_Type_Sub", "#CNT-TYPE-SUB", FieldType.NUMERIC, 2);
        pnd_Fund_Cde_Hold_Tot = localVariables.newFieldInRecord("pnd_Fund_Cde_Hold_Tot", "#FUND-CDE-HOLD-TOT", FieldType.STRING, 3);

        pnd_Fund_Cde_Hold_Tot__R_Field_7 = localVariables.newGroupInRecord("pnd_Fund_Cde_Hold_Tot__R_Field_7", "REDEFINE", pnd_Fund_Cde_Hold_Tot);
        pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold = pnd_Fund_Cde_Hold_Tot__R_Field_7.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold", "#CMPNY-CDE-HOLD", 
            FieldType.STRING, 1);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold = pnd_Fund_Cde_Hold_Tot__R_Field_7.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold", "#FUND-CDE-HOLD", 
            FieldType.STRING, 2);

        pnd_Fund_Cde_Hold_Tot__R_Field_8 = pnd_Fund_Cde_Hold_Tot__R_Field_7.newGroupInGroup("pnd_Fund_Cde_Hold_Tot__R_Field_8", "REDEFINE", pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1 = pnd_Fund_Cde_Hold_Tot__R_Field_8.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1", "#FUND-CDE-HOLD-1", 
            FieldType.STRING, 1);
        pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2 = pnd_Fund_Cde_Hold_Tot__R_Field_8.newFieldInGroup("pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_2", "#FUND-CDE-HOLD-2", 
            FieldType.STRING, 1);
        pnd_Tiaa_Table_1 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_1", "#TIAA-TABLE-1", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_2 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_2", "#TIAA-TABLE-2", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_3 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_3", "#TIAA-TABLE-3", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_4 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_4", "#TIAA-TABLE-4", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_5 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_5", "#TIAA-TABLE-5", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_6 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_6", "#TIAA-TABLE-6", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_7 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_7", "#TIAA-TABLE-7", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot", "#TIAA-TABLE-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot_4 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot_4", "#TIAA-TABLE-TOT-4", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot_5 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot_5", "#TIAA-TABLE-TOT-5", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot_6 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot_6", "#TIAA-TABLE-TOT-6", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot_7 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot_7", "#TIAA-TABLE-TOT-7", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Tiaa_Table_Tot_G = localVariables.newFieldArrayInRecord("pnd_Tiaa_Table_Tot_G", "#TIAA-TABLE-TOT-G", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            22, 1, 4));
        pnd_Cref_Units_1 = localVariables.newFieldArrayInRecord("pnd_Cref_Units_1", "#CREF-UNITS-1", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            22));
        pnd_Cref_Units_2 = localVariables.newFieldArrayInRecord("pnd_Cref_Units_2", "#CREF-UNITS-2", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            22));
        pnd_Cref_Units_3 = localVariables.newFieldArrayInRecord("pnd_Cref_Units_3", "#CREF-UNITS-3", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            22));
        pnd_Cref_Units_Total = localVariables.newFieldInRecord("pnd_Cref_Units_Total", "#CREF-UNITS-TOTAL", FieldType.NUMERIC, 11, 3);
        pnd_Cref_Dollars_1 = localVariables.newFieldArrayInRecord("pnd_Cref_Dollars_1", "#CREF-DOLLARS-1", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Dollars_2 = localVariables.newFieldArrayInRecord("pnd_Cref_Dollars_2", "#CREF-DOLLARS-2", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Dollars_3 = localVariables.newFieldArrayInRecord("pnd_Cref_Dollars_3", "#CREF-DOLLARS-3", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Dollars_Total = localVariables.newFieldInRecord("pnd_Cref_Dollars_Total", "#CREF-DOLLARS-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Deduct_1 = localVariables.newFieldArrayInRecord("pnd_Cref_Deduct_1", "#CREF-DEDUCT-1", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Deduct_2 = localVariables.newFieldArrayInRecord("pnd_Cref_Deduct_2", "#CREF-DEDUCT-2", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Deduct_3 = localVariables.newFieldArrayInRecord("pnd_Cref_Deduct_3", "#CREF-DEDUCT-3", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Deduct_Total = localVariables.newFieldInRecord("pnd_Cref_Deduct_Total", "#CREF-DEDUCT-TOTAL", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Units_Tot = localVariables.newFieldArrayInRecord("pnd_Cref_Units_Tot", "#CREF-UNITS-TOT", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            22));
        pnd_Cref_Dollars_Tot = localVariables.newFieldArrayInRecord("pnd_Cref_Dollars_Tot", "#CREF-DOLLARS-TOT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Cref_Deduct_Tot = localVariables.newFieldArrayInRecord("pnd_Cref_Deduct_Tot", "#CREF-DEDUCT-TOT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            22));
        pnd_Mode_Table_2 = localVariables.newFieldArrayInRecord("pnd_Mode_Table_2", "#MODE-TABLE-2", FieldType.STRING, 3, new DbsArrayController(1, 22));
        pnd_Unit_Text_Table = localVariables.newFieldArrayInRecord("pnd_Unit_Text_Table", "#UNIT-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1, 
            20));
        pnd_Dollars_Text_Table = localVariables.newFieldArrayInRecord("pnd_Dollars_Text_Table", "#DOLLARS-TEXT-TABLE", FieldType.STRING, 24, new DbsArrayController(1, 
            20));
        pnd_Tiaa_Cnt_Amt_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot", "#TIAA-CNT-AMT-TOT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Cnt_Amt_Tot_4 = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_4", "#TIAA-CNT-AMT-TOT-4", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Cnt_Amt_Tot_5 = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_5", "#TIAA-CNT-AMT-TOT-5", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Cnt_Amt_Tot_6 = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_6", "#TIAA-CNT-AMT-TOT-6", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Cnt_Amt_Tot_7 = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_7", "#TIAA-CNT-AMT-TOT-7", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot", "#TIAA-PER-DED-TOT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot_4 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_4", "#TIAA-PER-DED-TOT-4", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot_5 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_5", "#TIAA-PER-DED-TOT-5", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot_6 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_6", "#TIAA-PER-DED-TOT-6", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot_7 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_7", "#TIAA-PER-DED-TOT-7", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot", "#TIAA-FIN-PAY-TOT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot_4 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_4", "#TIAA-FIN-PAY-TOT-4", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot_5 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_5", "#TIAA-FIN-PAY-TOT-5", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot_6 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_6", "#TIAA-FIN-PAY-TOT-6", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot_7 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_7", "#TIAA-FIN-PAY-TOT-7", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot", "#TIAA-PER-DIV-TOT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot_4 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_4", "#TIAA-PER-DIV-TOT-4", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot_5 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_5", "#TIAA-PER-DIV-TOT-5", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot_6 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_6", "#TIAA-PER-DIV-TOT-6", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot_7 = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_7", "#TIAA-PER-DIV-TOT-7", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot", "#TIAA-FIN-DIV-TOT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot_4 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_4", "#TIAA-FIN-DIV-TOT-4", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot_5 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_5", "#TIAA-FIN-DIV-TOT-5", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot_6 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_6", "#TIAA-FIN-DIV-TOT-6", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot_7 = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_7", "#TIAA-FIN-DIV-TOT-7", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Cnt_Amt_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Cnt_Amt_Tot_G", "#TIAA-CNT-AMT-TOT-G", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Ded_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Per_Ded_Tot_G", "#TIAA-PER-DED-TOT-G", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Pay_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Pay_Tot_G", "#TIAA-FIN-PAY-TOT-G", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Per_Div_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Per_Div_Tot_G", "#TIAA-PER-DIV-TOT-G", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Fin_Div_Tot_G = localVariables.newFieldInRecord("pnd_Tiaa_Fin_Div_Tot_G", "#TIAA-FIN-DIV-TOT-G", FieldType.NUMERIC, 13, 2);
        pnd_Pay_Mode_Table = localVariables.newFieldArrayInRecord("pnd_Pay_Mode_Table", "#PAY-MODE-TABLE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            4));
        pnd_W_Rec_3 = localVariables.newFieldInRecord("pnd_W_Rec_3", "#W-REC-3", FieldType.NUMERIC, 8);

        pnd_W_Rec_3__R_Field_9 = localVariables.newGroupInRecord("pnd_W_Rec_3__R_Field_9", "REDEFINE", pnd_W_Rec_3);
        pnd_W_Rec_3_Pnd_W_Rec_3_A = pnd_W_Rec_3__R_Field_9.newFieldInGroup("pnd_W_Rec_3_Pnd_W_Rec_3_A", "#W-REC-3-A", FieldType.STRING, 8);

        iaaa0510 = localVariables.newGroupInRecord("iaaa0510", "IAAA0510");
        iaaa0510_Pnd_Cm_Std_Srt_Seq = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Cm_Std_Srt_Seq", "#CM-STD-SRT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            80));
        iaaa0510_Pnd_Ia_Std_Nm_Cd = iaaa0510.newFieldArrayInGroup("iaaa0510_Pnd_Ia_Std_Nm_Cd", "#IA-STD-NM-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            80));
        pnd_Tpa_Rinv_Cntrcts = localVariables.newFieldArrayInRecord("pnd_Tpa_Rinv_Cntrcts", "#TPA-RINV-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Tpa_Roll_Cntrcts = localVariables.newFieldArrayInRecord("pnd_Tpa_Roll_Cntrcts", "#TPA-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Ipr_Roll_Cntrcts = localVariables.newFieldArrayInRecord("pnd_Ipr_Roll_Cntrcts", "#IPR-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_P_I_Roll_Cntrcts = localVariables.newFieldArrayInRecord("pnd_P_I_Roll_Cntrcts", "#P-I-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Ac_Roll_Cntrcts = localVariables.newFieldArrayInRecord("pnd_Ac_Roll_Cntrcts", "#AC-ROLL-CNTRCTS", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            5));
        pnd_Tpa_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Tpa_Roll_Cntrcts_Tot", "#TPA-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Ipr_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Ipr_Roll_Cntrcts_Tot", "#IPR-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_P_I_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_P_I_Roll_Cntrcts_Tot", "#P-I-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Ac_Roll_Cntrcts_Tot = localVariables.newFieldInRecord("pnd_Ac_Roll_Cntrcts_Tot", "#AC-ROLL-CNTRCTS-TOT", FieldType.NUMERIC, 11);
        pnd_Tiaa_Non_Life = localVariables.newFieldInRecord("pnd_Tiaa_Non_Life", "#TIAA-NON-LIFE", FieldType.STRING, 30);
        pnd_Org_03_Tab_G = localVariables.newFieldArrayInRecord("pnd_Org_03_Tab_G", "#ORG-03-TAB-G", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tot_G = localVariables.newFieldArrayInRecord("pnd_Org_03_Tot_G", "#ORG-03-TOT-G", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Due_G = localVariables.newFieldArrayInRecord("pnd_Org_03_Due_G", "#ORG-03-DUE-G", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Org_46_Tab_G = localVariables.newFieldArrayInRecord("pnd_Org_46_Tab_G", "#ORG-46-TAB-G", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tot_G = localVariables.newFieldArrayInRecord("pnd_Org_46_Tot_G", "#ORG-46-TOT-G", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Due_G = localVariables.newFieldArrayInRecord("pnd_Org_46_Due_G", "#ORG-46-DUE-G", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            2, 1, 4));
        pnd_Org_03_Tab_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_03_Tab_Std_S0", "#ORG-03-TAB-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tot_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_03_Tot_Std_S0", "#ORG-03-TOT-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tab_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_46_Tab_Std_S0", "#ORG-46-TAB-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tot_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_46_Tot_Std_S0", "#ORG-46-TOT-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tab_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_17_Tab_Std_S0", "#ORG-17-TAB-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tot_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_17_Tot_Std_S0", "#ORG-17-TOT-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tab_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_37_Tab_Std_S0", "#ORG-37-TAB-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tot_Std_S0 = localVariables.newFieldArrayInRecord("pnd_Org_37_Tot_Std_S0", "#ORG-37-TOT-STD-S0", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tab_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_03_Tab_S0_Pi", "#ORG-03-TAB-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tot_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_03_Tot_S0_Pi", "#ORG-03-TOT-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tab_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_46_Tab_S0_Pi", "#ORG-46-TAB-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tot_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_46_Tot_S0_Pi", "#ORG-46-TOT-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tab_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_17_Tab_S0_Pi", "#ORG-17-TAB-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tot_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_17_Tot_S0_Pi", "#ORG-17-TOT-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tab_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_37_Tab_S0_Pi", "#ORG-37-TAB-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tot_S0_Pi = localVariables.newFieldArrayInRecord("pnd_Org_37_Tot_S0_Pi", "#ORG-37-TOT-S0-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tab_Pi = localVariables.newFieldArrayInRecord("pnd_Org_03_Tab_Pi", "#ORG-03-TAB-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tot_Pi = localVariables.newFieldArrayInRecord("pnd_Org_03_Tot_Pi", "#ORG-03-TOT-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tab_Pi = localVariables.newFieldArrayInRecord("pnd_Org_46_Tab_Pi", "#ORG-46-TAB-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_46_Tot_Pi = localVariables.newFieldArrayInRecord("pnd_Org_46_Tot_Pi", "#ORG-46-TOT-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tab_Pi = localVariables.newFieldArrayInRecord("pnd_Org_17_Tab_Pi", "#ORG-17-TAB-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_17_Tot_Pi = localVariables.newFieldArrayInRecord("pnd_Org_17_Tot_Pi", "#ORG-17-TOT-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tab_Pi = localVariables.newFieldArrayInRecord("pnd_Org_37_Tab_Pi", "#ORG-37-TAB-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_37_Tot_Pi = localVariables.newFieldArrayInRecord("pnd_Org_37_Tot_Pi", "#ORG-37-TOT-PI", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 
            23, 1, 4));
        pnd_Org_03_Tab = localVariables.newFieldArrayInRecord("pnd_Org_03_Tab", "#ORG-03-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_03_Tot = localVariables.newFieldArrayInRecord("pnd_Org_03_Tot", "#ORG-03-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_03_Due = localVariables.newFieldArrayInRecord("pnd_Org_03_Due", "#ORG-03-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_46_Tab = localVariables.newFieldArrayInRecord("pnd_Org_46_Tab", "#ORG-46-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_46_Tot = localVariables.newFieldArrayInRecord("pnd_Org_46_Tot", "#ORG-46-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_46_Due = localVariables.newFieldArrayInRecord("pnd_Org_46_Due", "#ORG-46-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_17_Tab = localVariables.newFieldArrayInRecord("pnd_Org_17_Tab", "#ORG-17-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_17_Tot = localVariables.newFieldArrayInRecord("pnd_Org_17_Tot", "#ORG-17-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_17_Due = localVariables.newFieldArrayInRecord("pnd_Org_17_Due", "#ORG-17-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_37_Tab = localVariables.newFieldArrayInRecord("pnd_Org_37_Tab", "#ORG-37-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_37_Tot = localVariables.newFieldArrayInRecord("pnd_Org_37_Tot", "#ORG-37-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_37_Due = localVariables.newFieldArrayInRecord("pnd_Org_37_Due", "#ORG-37-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_18_Tab = localVariables.newFieldArrayInRecord("pnd_Org_18_Tab", "#ORG-18-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_18_Tot = localVariables.newFieldArrayInRecord("pnd_Org_18_Tot", "#ORG-18-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_18_Due = localVariables.newFieldArrayInRecord("pnd_Org_18_Due", "#ORG-18-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_38_Tab = localVariables.newFieldArrayInRecord("pnd_Org_38_Tab", "#ORG-38-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_38_Tot = localVariables.newFieldArrayInRecord("pnd_Org_38_Tot", "#ORG-38-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_38_Due = localVariables.newFieldArrayInRecord("pnd_Org_38_Due", "#ORG-38-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Org_40_Tab = localVariables.newFieldArrayInRecord("pnd_Org_40_Tab", "#ORG-40-TAB", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_40_Tot = localVariables.newFieldArrayInRecord("pnd_Org_40_Tot", "#ORG-40-TOT", FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23, 
            1, 4));
        pnd_Org_40_Due = localVariables.newFieldArrayInRecord("pnd_Org_40_Due", "#ORG-40-DUE", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2, 
            1, 4));
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 1);
        pnd_Blnk5 = localVariables.newFieldInRecord("pnd_Blnk5", "#BLNK5", FieldType.STRING, 5);
        pnd_U = localVariables.newFieldInRecord("pnd_U", "#U", FieldType.NUMERIC, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 2);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 2);
        pnd_Pa_Contract = localVariables.newFieldInRecord("pnd_Pa_Contract", "#PA-CONTRACT", FieldType.STRING, 1);
        pnd_Cref_Contract = localVariables.newFieldInRecord("pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 1);
        pnd_End_Of_Tiaa = localVariables.newFieldInRecord("pnd_End_Of_Tiaa", "#END-OF-TIAA", FieldType.STRING, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Table_Date = localVariables.newFieldInRecord("pnd_Table_Date", "#TABLE-DATE", FieldType.NUMERIC, 6);
        pnd_W_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_W_Save_Check_Dte_A", "#W-SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_W_Save_Check_Dte_A__R_Field_10 = localVariables.newGroupInRecord("pnd_W_Save_Check_Dte_A__R_Field_10", "REDEFINE", pnd_W_Save_Check_Dte_A);
        pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte = pnd_W_Save_Check_Dte_A__R_Field_10.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte", 
            "#W-SAVE-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_W_Save_Check_Dte_A__R_Field_11 = pnd_W_Save_Check_Dte_A__R_Field_10.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);
        pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy = pnd_W_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 
            4);

        pnd_W_Save_Check_Dte_A__R_Field_12 = pnd_W_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Yyyy);
        pnd_W_Save_Check_Dte_A_Pnd_W_Cc = pnd_W_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Cc", "#W-CC", FieldType.NUMERIC, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Yy = pnd_W_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Yy", "#W-YY", FieldType.NUMERIC, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Mm = pnd_W_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Save_Check_Dte_A__R_Field_13 = pnd_W_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Mm);
        pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A = pnd_W_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Mm_A", "#W-MM-A", FieldType.STRING, 
            2);
        pnd_W_Save_Check_Dte_A_Pnd_W_Dd = pnd_W_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Save_Check_Dte_A__R_Field_14 = pnd_W_Save_Check_Dte_A__R_Field_10.newGroupInGroup("pnd_W_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);
        pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm = pnd_W_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte_Yyyymm", 
            "#W-SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_15", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_15.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_16 = pnd_Save_Check_Dte_A__R_Field_15.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_16", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_17 = pnd_Save_Check_Dte_A__R_Field_16.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_17", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_18 = pnd_Save_Check_Dte_A__R_Field_16.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_18", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_18.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_19 = pnd_Save_Check_Dte_A__R_Field_15.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_19", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_19.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);

        pnd_Payment_Due_Dte__R_Field_20 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_20", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_20.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_21 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_21", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_21.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_21.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);

        pnd_Table = localVariables.newGroupArrayInRecord("pnd_Table", "#TABLE", new DbsArrayController(1, 240));
        pnd_Table_Pnd_Tbl_Fund_Cde = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Fund_Cde", "#TBL-FUND-CDE", FieldType.STRING, 2);
        pnd_Table_Pnd_Tbl_Unit_Value = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Unit_Value", "#TBL-UNIT-VALUE", FieldType.NUMERIC, 5, 2);
        pnd_Table_Pnd_Tbl_Issue_Date = pnd_Table.newFieldInGroup("pnd_Table_Pnd_Tbl_Issue_Date", "#TBL-ISSUE-DATE", FieldType.NUMERIC, 6);
        pnd_Cnt_Tot = localVariables.newFieldInRecord("pnd_Cnt_Tot", "#CNT-TOT", FieldType.NUMERIC, 9);
        pnd_Cnt1 = localVariables.newFieldInRecord("pnd_Cnt1", "#CNT1", FieldType.NUMERIC, 9);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.NUMERIC, 9);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.NUMERIC, 9);
        pnd_Cnt_None = localVariables.newFieldInRecord("pnd_Cnt_None", "#CNT-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_1s = localVariables.newFieldInRecord("pnd_Cnt_1s", "#CNT-1S", FieldType.NUMERIC, 9);
        pnd_Cnt_1s1 = localVariables.newFieldInRecord("pnd_Cnt_1s1", "#CNT-1S1", FieldType.NUMERIC, 9);
        pnd_Cnt_1s2 = localVariables.newFieldInRecord("pnd_Cnt_1s2", "#CNT-1S2", FieldType.NUMERIC, 9);
        pnd_Cnt_1s3 = localVariables.newFieldInRecord("pnd_Cnt_1s3", "#CNT-1S3", FieldType.NUMERIC, 9);
        pnd_Cnt_1s_None = localVariables.newFieldInRecord("pnd_Cnt_1s_None", "#CNT-1S-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_1z = localVariables.newFieldInRecord("pnd_Cnt_1z", "#CNT-1Z", FieldType.NUMERIC, 9);
        pnd_Cnt_1z1 = localVariables.newFieldInRecord("pnd_Cnt_1z1", "#CNT-1Z1", FieldType.NUMERIC, 9);
        pnd_Cnt_1z2 = localVariables.newFieldInRecord("pnd_Cnt_1z2", "#CNT-1Z2", FieldType.NUMERIC, 9);
        pnd_Cnt_1z_None = localVariables.newFieldInRecord("pnd_Cnt_1z_None", "#CNT-1Z-NONE", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref = localVariables.newFieldInRecord("pnd_Cnt_Cref", "#CNT-CREF", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref1 = localVariables.newFieldInRecord("pnd_Cnt_Cref1", "#CNT-CREF1", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref2 = localVariables.newFieldInRecord("pnd_Cnt_Cref2", "#CNT-CREF2", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref3 = localVariables.newFieldInRecord("pnd_Cnt_Cref3", "#CNT-CREF3", FieldType.NUMERIC, 9);
        pnd_Cnt_Cref_None = localVariables.newFieldInRecord("pnd_Cnt_Cref_None", "#CNT-CREF-NONE", FieldType.NUMERIC, 9);
        pnd_Eof_Sw = localVariables.newFieldInRecord("pnd_Eof_Sw", "#EOF-SW", FieldType.STRING, 1);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 4);
        pnd_Page_Num = localVariables.newFieldInRecord("pnd_Page_Num", "#PAGE-NUM", FieldType.NUMERIC, 4);
        pnd_Fn_Code = localVariables.newFieldInRecord("pnd_Fn_Code", "#FN-CODE", FieldType.STRING, 1);
        pnd_Ck_Date = localVariables.newFieldInRecord("pnd_Ck_Date", "#CK-DATE", FieldType.NUMERIC, 6);
        pnd_U_Val = localVariables.newFieldInRecord("pnd_U_Val", "#U-VAL", FieldType.NUMERIC, 5, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Issu_Date = localVariables.newFieldInRecord("pnd_Issu_Date", "#ISSU-DATE", FieldType.NUMERIC, 6);
        pnd_Is_Date = localVariables.newFieldInRecord("pnd_Is_Date", "#IS-DATE", FieldType.NUMERIC, 6);

        pnd_Is_Date__R_Field_22 = localVariables.newGroupInRecord("pnd_Is_Date__R_Field_22", "REDEFINE", pnd_Is_Date);
        pnd_Is_Date_Pnd_Is_Date_Yyyy = pnd_Is_Date__R_Field_22.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Yyyy", "#IS-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Is_Date_Pnd_Is_Date_Mm = pnd_Is_Date__R_Field_22.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Mm", "#IS-DATE-MM", FieldType.NUMERIC, 2);

        pnd_Is_Date__R_Field_23 = pnd_Is_Date__R_Field_22.newGroupInGroup("pnd_Is_Date__R_Field_23", "REDEFINE", pnd_Is_Date_Pnd_Is_Date_Mm);
        pnd_Is_Date_Pnd_Is_Date_Mm_A = pnd_Is_Date__R_Field_23.newFieldInGroup("pnd_Is_Date_Pnd_Is_Date_Mm_A", "#IS-DATE-MM-A", FieldType.STRING, 2);
        pnd_Fund_Code_2 = localVariables.newFieldInRecord("pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_Fund_Desc = localVariables.newFieldInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 35);

        pnd_Fund_Desc__R_Field_24 = localVariables.newGroupInRecord("pnd_Fund_Desc__R_Field_24", "REDEFINE", pnd_Fund_Desc);
        pnd_Fund_Desc_Pnd_Fund_Desc_10 = pnd_Fund_Desc__R_Field_24.newFieldInGroup("pnd_Fund_Desc_Pnd_Fund_Desc_10", "#FUND-DESC-10", FieldType.STRING, 
            10);
        pnd_T_Fund_Code = localVariables.newFieldArrayInRecord("pnd_T_Fund_Code", "#T-FUND-CODE", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Table_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Table_Fund_Desc", "#TABLE-FUND-DESC", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pnd_Fnd_Desc = localVariables.newFieldInRecord("pnd_Fnd_Desc", "#FND-DESC", FieldType.STRING, 10);
        pnd_W_Variable = localVariables.newFieldInRecord("pnd_W_Variable", "#W-VARIABLE", FieldType.STRING, 2);
        pnd_Heading1 = localVariables.newFieldInRecord("pnd_Heading1", "#HEADING1", FieldType.STRING, 69);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Rollover_Title = localVariables.newFieldInRecord("pnd_Rollover_Title", "#ROLLOVER-TITLE", FieldType.STRING, 20);
        pnd_Print_Tpa_Ipro_Pi = localVariables.newFieldInRecord("pnd_Print_Tpa_Ipro_Pi", "#PRINT-TPA-IPRO-PI", FieldType.BOOLEAN, 1);
        pnd_Pmt_Modes = localVariables.newFieldArrayInRecord("pnd_Pmt_Modes", "#PMT-MODES", FieldType.NUMERIC, 3, new DbsArrayController(1, 22));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Eof_Sw.setInitialValue("B");
        pnd_Len.setInitialValue(6);
        pnd_Heading1.setInitialValue("IA ADMINISTRATION - MODE CONTROL FOR PAYMENTS DUE ");
        pnd_Debug.setInitialValue(false);
        pnd_Print_Tpa_Ipro_Pi.setInitialValue(false);
        pnd_Pmt_Modes.getValue(1).setInitialValue(100);
        pnd_Pmt_Modes.getValue(2).setInitialValue(601);
        pnd_Pmt_Modes.getValue(3).setInitialValue(602);
        pnd_Pmt_Modes.getValue(4).setInitialValue(603);
        pnd_Pmt_Modes.getValue(5).setInitialValue(701);
        pnd_Pmt_Modes.getValue(6).setInitialValue(702);
        pnd_Pmt_Modes.getValue(7).setInitialValue(703);
        pnd_Pmt_Modes.getValue(8).setInitialValue(704);
        pnd_Pmt_Modes.getValue(9).setInitialValue(705);
        pnd_Pmt_Modes.getValue(10).setInitialValue(706);
        pnd_Pmt_Modes.getValue(11).setInitialValue(801);
        pnd_Pmt_Modes.getValue(12).setInitialValue(802);
        pnd_Pmt_Modes.getValue(13).setInitialValue(803);
        pnd_Pmt_Modes.getValue(14).setInitialValue(804);
        pnd_Pmt_Modes.getValue(15).setInitialValue(805);
        pnd_Pmt_Modes.getValue(16).setInitialValue(806);
        pnd_Pmt_Modes.getValue(17).setInitialValue(807);
        pnd_Pmt_Modes.getValue(18).setInitialValue(808);
        pnd_Pmt_Modes.getValue(19).setInitialValue(809);
        pnd_Pmt_Modes.getValue(20).setInitialValue(810);
        pnd_Pmt_Modes.getValue(21).setInitialValue(811);
        pnd_Pmt_Modes.getValue(22).setInitialValue(812);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap110a() throws Exception
    {
        super("Iaap110a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP110A", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt8, 8);
        getReports().atTopOfPage(atTopEventRpt9, 9);
        setupReports();
        //* *=====================================================================
        //*                     SET AT TOP OF PAGE
        //* *=====================================================================
        //*  TIAA TOTALS                                                                                                                                                  //Natural: FORMAT ( 1 ) LS = 133 PS = 158;//Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: FORMAT ( 3 ) LS = 133 PS = 57;//Natural: FORMAT ( 4 ) LS = 133 PS = 56;//Natural: FORMAT ( 5 ) LS = 133 PS = 56;//Natural: FORMAT ( 6 ) LS = 133 PS = 57;//Natural: FORMAT ( 7 ) LS = 133 PS = 56
        //*  CREF TOTALS                                                                                                                                                  //Natural: FORMAT ( 8 ) LS = 133 PS = 56
        //*  PAGE 1
        //*                                                                                                                                                               //Natural: FORMAT ( 9 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 8 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 9 )
        //* *======================================================================
        //*                    START OF MAIN PROGRAM LOGIC
        //* *======================================================================
        getReports().write(0, "*****************************");                                                                                                           //Natural: WRITE '*****************************'
        if (Global.isEscape()) return;
        getReports().write(0, "  START OF IAAP110A PROGRAM");                                                                                                             //Natural: WRITE '  START OF IAAP110A PROGRAM'
        if (Global.isEscape()) return;
        getReports().write(0, "*****************************");                                                                                                           //Natural: WRITE '*****************************'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #INITIALIZATION
        sub_Pnd_Initialization();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #SETUP-MODE-TABLE2
        sub_Pnd_Setup_Mode_Table2();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #SETUP-OTHER-TABLES
        sub_Pnd_Setup_Other_Tables();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "=",pnd_W_Save_Check_Dte_A_Pnd_W_Save_Check_Dte);                                                                                       //Natural: WRITE '=' #W-SAVE-CHECK-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ WORK FILE 1
        boolean endOfDataR1 = true;                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstR1 = true;
        R1:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventR1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataR1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #I-TIAA-FUND-CDE-TOT
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr);         //Natural: WRITE '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-CMPNY-CDE '=' #I-CNT-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde.notEquals("T") && pnd_End_Of_Tiaa.equals(" ")))                                                           //Natural: IF #I-TIAA-CMPNY-CDE NOT = 'T' AND #END-OF-TIAA = ' '
            {
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, "!!!!!!   END OF TEACHERS   !!!!!!!!!!!    ");                                                                                  //Natural: WRITE '!!!!!!   END OF TEACHERS   !!!!!!!!!!!    '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_End_Of_Tiaa.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #END-OF-TIAA
                //*  TIAA GRAND TOTAL
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-TIAA-TOTALS
                sub_Pnd_Write_Out_Tiaa_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Cde_Hold_Tot.setValue(pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot);                                                                                     //Natural: MOVE #I-TIAA-FUND-CDE-TOT TO #FUND-CDE-HOLD-TOT
                                                                                                                                                                          //Natural: PERFORM #FIND-MODE-SUB
            sub_Pnd_Find_Mode_Sub();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND
            sub_Pnd_Process_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #CHECK-PAYMENT-MODE
            sub_Pnd_Check_Payment_Mode();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TOTALS-REPORTS
                sub_Pnd_Process_Totals_Reports();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  #INPUT-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventR1(endOfDataR1);
        }
        if (Global.isEscape()) return;
        pnd_Page_Number.setValue(getReports().getPageNumberDbs(1));                                                                                                       //Natural: MOVE *PAGE-NUMBER ( 1 ) TO #PAGE-NUMBER
        pnd_Eof_Sw.setValue("E");                                                                                                                                         //Natural: MOVE 'E' TO #EOF-SW
        //*  WRITE REPORT 1 FOR ROLLOVERS
        DbsUtil.callnat(Iaap110c.class , getCurrentProcessState(), pnd_Eof_Sw, pnd_Mode_1, pnd_Input_Record_Pnd_I_Cnt_Nbr, pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde,      //Natural: CALLNAT 'IAAP110C' #EOF-SW #MODE-1 #I-CNT-NBR #I-CNT-CURR-DIST-CDE #I-CNTRCT-OPTN-CODE #I-TIAA-PER-PAY-AMT #I-TIAA-PER-DIV-AMT #I-TIAA-RATE-FINAL-PAY-AMT #I-TIAA-RATE-FINAL-DIV-AMT #MODE-TABLE-2 ( * ) #PAGE-NUMBER
            pnd_Input_Record_Pnd_I_Cntrct_Optn_Code, pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt, pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt, pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt, 
            pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt, pnd_Mode_Table_2.getValue("*"), pnd_Page_Number);
        if (condition(Global.isEscape())) return;
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(20),"TOTAL",new ColumnSpacing(9),pnd_Fmt4_Cnt_Amt_Tot, new ReportEditMask            //Natural: WRITE ( 4 ) // 20X 'TOTAL' 09X #FMT4-CNT-AMT-TOT ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT4-PER-DIV-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt4_Per_Div_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*              07X #-DDCTN-PER-AMT
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS
        sub_Pnd_Write_Totals_Reports();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS-5
        sub_Pnd_Write_Totals_Reports_5();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTALS-REPORTS-6
        sub_Pnd_Write_Totals_Reports_6();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM #WRITE-TOTALS-REPORTS-7   /* COMMENTED 9/01
        //*  ADDED FOLLOWING 9/01
        //*  IAAL110 WRITE REPORT 7
        DbsUtil.callnat(Iaap110b.class , getCurrentProcessState(), pnd_Fmt3_Tiaa_Table_Tot, pnd_Fmt3_Tiaa_Table.getValue("*","*"), pnd_Fmt5_Tiaa_Table_Tot,               //Natural: CALLNAT 'IAAP110B' #FMT3-TIAA-TABLE-TOT #FMT3-TIAA-TABLE ( *,* ) #FMT5-TIAA-TABLE-TOT #FMT5-TIAA-TABLE ( *,* ) #FMT6-TIAA-TABLE-TOT #FMT6-TIAA-TABLE ( *,* ) #FMT7-TIAA-TABLE-TOT #FMT7-TIAA-TABLE ( *,* ) #FMT8-TIAA-TABLE-TOT #FMT8-TIAA-TABLE ( *,* ) #FMT9-TIAA-TABLE-TOT #FMT9-TIAA-TABLE ( *,* ) #FMT10-TIAA-TABLE-TOT #FMT10-TIAA-TABLE ( *,* ) #FMT11-TIAA-TABLE-TOT #FMT11-TIAA-TABLE ( *,* ) #FMT7-UNITS-TOT #FMT7-UNITS ( *,* ) #FMT7-DOLLARS ( *,* ) #FMT7-DOLLARS-TOT #PAY-MODE-TABLE ( * ) #UNIT-TEXT-TABLE ( * ) #DOLLARS-TEXT-TABLE ( * ) #TPA-RINV-CNTRCTS ( * ) #TPA-ROLL-CNTRCTS ( * ) #IPR-ROLL-CNTRCTS ( * ) #P-I-ROLL-CNTRCTS ( * ) #AC-ROLL-CNTRCTS ( * ) #U #PAYMENT-DUE-DTE
            pnd_Fmt5_Tiaa_Table.getValue("*","*"), pnd_Fmt6_Tiaa_Table_Tot, pnd_Fmt6_Tiaa_Table.getValue("*","*"), pnd_Fmt7_Tiaa_Table_Tot, pnd_Fmt7_Tiaa_Table.getValue("*","*"), 
            pnd_Fmt8_Tiaa_Table_Tot, pnd_Fmt8_Tiaa_Table.getValue("*","*"), pnd_Fmt9_Tiaa_Table_Tot, pnd_Fmt9_Tiaa_Table.getValue("*","*"), pnd_Fmt10_Tiaa_Table_Tot, 
            pnd_Fmt10_Tiaa_Table.getValue("*","*"), pnd_Fmt11_Tiaa_Table_Tot, pnd_Fmt11_Tiaa_Table.getValue("*","*"), pnd_Fmt7_Units_Tot, pnd_Fmt7_Units.getValue("*","*"), 
            pnd_Fmt7_Dollars.getValue("*","*"), pnd_Fmt7_Dollars_Tot, pnd_Pay_Mode_Table.getValue("*"), pnd_Unit_Text_Table.getValue("*"), pnd_Dollars_Text_Table.getValue("*"), 
            pnd_Tpa_Rinv_Cntrcts.getValue("*"), pnd_Tpa_Roll_Cntrcts.getValue("*"), pnd_Ipr_Roll_Cntrcts.getValue("*"), pnd_P_I_Roll_Cntrcts.getValue("*"), 
            pnd_Ac_Roll_Cntrcts.getValue("*"), pnd_U, pnd_Payment_Due_Dte);
        if (condition(Global.isEscape())) return;
        //*  END OF ADD 9/01
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "NUM OF FUNDS",pnd_U);                                                                                                                  //Natural: WRITE 'NUM OF FUNDS' #U
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *====================================================================
        //*                     START OF SUBROUTINES
        //* *====================================================================
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TIAA-TRAD
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CNT-TYPE-SUB-PARA
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FUND-INFORMATION
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TIAA-FUND-INFO
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-1G
        //*  ACCUM MODE BY CONTRACT TYPE & TOTAL PAGE TO ACCUM LIKE TYPES
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-1S
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-1S-PA-CONTRACTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-1Z
        //*      (#I-CNT-NBR-3-7 = 43000 THRU 46999) OR
        //* ****
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TPA-IPRO-PI
        //*  ADDED FOLLOWING 9/04 ACCUM PA CONTRACTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ACCUM-OPT22-S0
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TOTALS-REPORTS
        //* *****************************************************************
        //*    CURRENT MODE PAYMENTS DUE REPORT
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-TEACHER-CONTRACTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-CREF-CONTRACTS
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-CREF
        //*  PA SELECT FUND CHANGES
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-PA-CONTRACTS
        //*  PA SELECT FUND CHANGES
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-PA-SELECT-FUND
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PROCESS-FUND-09-11
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FIND-MODE-SUB
        //* *****************************************************************
        //* * 9/08
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-MODE-TABLE2
        //* *****************************************************************
        //*  9/08
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-OTHER-TABLES
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-REPORT
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-TIAA-TOTALS
        //*  MODE REPORT BY CONTRACT PREFIX TYPE & TOTAL REPORT BY CONTRACT TYPE
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TEACHER-FORMAT
        //*                  PAGE 4 TIAA STANDARD GW W0(1-24999)6L 1S
        //*                  PAGE 11 GROUP W0 (80000-89999) 1Z
        //*   WRITE TPA,IPRO OPTION 22
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TIAA-FORMAT
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS-5
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTALS-REPORTS-6
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-CREF-FORMAT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #INITIALIZATION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DOLLAR-AMOUNT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PAYMENT-MODE
        //* ***********************************************************************
        //*  *****************ON ERROR ******************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Pnd_Process_Fund() throws Exception                                                                                                                  //Natural: #PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Cref_Contract.reset();                                                                                                                                        //Natural: RESET #CREF-CONTRACT #PA-CONTRACT
        pnd_Pa_Contract.reset();
        if (condition(pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("41") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("42") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("43")  //Natural: IF #I-TIAA-FUND-CDE = '41' OR = '42' OR = '43' OR = '44' OR = '45' OR = '46' OR = '47' OR = '48' OR = '49'
            || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("44") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("45") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("46") 
            || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("47") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("48") || pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("49")))
        {
            pnd_Pa_Contract.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #PA-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
            sub_Pnd_Cnt_Type_Sub_Para();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PROCESS-PA-SELECT-FUND
            sub_Pnd_Process_Pa_Select_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet837 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #I-TIAA-CMPNY-CDE;//Natural: VALUE 'T'
            if (condition((pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde.equals("T"))))
            {
                decideConditionsMet837++;
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TIAA-TRAD
                sub_Pnd_Process_Tiaa_Trad();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde.equals("2"))))
            {
                decideConditionsMet837++;
                pnd_Cref_Contract.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #CREF-CONTRACT
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
                sub_Pnd_Cnt_Type_Sub_Para();
                if (condition(Global.isEscape())) {return;}
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-CREF
                sub_Pnd_Process_Fund_Cref();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'U'
            else if (condition((pnd_Input_Record_Pnd_I_Tiaa_Cmpny_Cde.equals("U"))))
            {
                decideConditionsMet837++;
                pnd_Cref_Contract.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #CREF-CONTRACT
                //*  9/08  NEW
                                                                                                                                                                          //Natural: PERFORM #CNT-TYPE-SUB-PARA
                sub_Pnd_Cnt_Type_Sub_Para();
                if (condition(Global.isEscape())) {return;}
                //*      MOVE 1 TO #CNT-TYPE-SUB      /* 9/08
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                //*  9/08
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-09-11
                sub_Pnd_Process_Fund_09_11();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-FUND
    }
    private void sub_Pnd_Process_Tiaa_Trad() throws Exception                                                                                                             //Natural: #PROCESS-TIAA-TRAD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet862 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #I-TIAA-FUND-CDE;//Natural: VALUE '1G'
        if (condition((pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("1G"))))
        {
            decideConditionsMet862++;
            if (condition(pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(25) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(27)  //Natural: IF #I-CNTRCT-OPTN-CODE = 22 OR = 25 OR = 27 OR = 28 OR = 30
                || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-1G
                sub_Pnd_Process_Fund_1g();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '1S'
        else if (condition((pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("1S"))))
        {
            decideConditionsMet862++;
            if (condition(pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(25) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(27)  //Natural: IF #I-CNTRCT-OPTN-CODE = 22 OR = 25 OR = 27 OR = 28 OR = 30
                || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-1S
                sub_Pnd_Process_Fund_1s();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '1Z'
        else if (condition((pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde.equals("1Z"))))
        {
            decideConditionsMet862++;
            if (condition(pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(25) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(27)  //Natural: IF #I-CNTRCT-OPTN-CODE = 22 OR = 25 OR = 27 OR = 28 OR = 30
                || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-TOT
                                                                                                                                                                          //Natural: PERFORM #PROCESS-FUND-1Z
                sub_Pnd_Process_Fund_1z();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet862 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TPA-IPRO-PI
            sub_Pnd_Process_Tpa_Ipro_Pi();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-TIAA-TRAD
    }
    private void sub_Pnd_Cnt_Type_Sub_Para() throws Exception                                                                                                             //Natural: #CNT-TYPE-SUB-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_W_Variable.setValue(pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);                                                                                                    //Natural: MOVE #I-TIAA-FUND-CDE TO #W-VARIABLE
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #U
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_U)); pnd_I.nadd(1))
        {
            if (condition(pnd_W_Variable.equals(pnd_T_Fund_Code.getValue(pnd_I))))                                                                                        //Natural: IF #W-VARIABLE = #T-FUND-CODE ( #I )
            {
                pnd_Cnt_Type_Sub.setValue(pnd_I);                                                                                                                         //Natural: ASSIGN #CNT-TYPE-SUB := #I
                //*  9/08
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #CNT-TYPE-SUB-PARA
    }
    private void sub_Pnd_Fund_Information() throws Exception                                                                                                              //Natural: #FUND-INFORMATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Fund_Desc.reset();                                                                                                                                            //Natural: RESET #FUND-DESC #COMP-DESC #FUND-TYPES
        pnd_Comp_Desc.reset();
        pnd_Fund_Types.reset();
        pnd_Fund_Types.setValue(3);                                                                                                                                       //Natural: MOVE 3 TO #FUND-TYPES
        short decideConditionsMet911 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #CMPNY-CDE-HOLD;//Natural: VALUE 'T'
        if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("T"))))
        {
            decideConditionsMet911++;
            //*  LB 11/97
                                                                                                                                                                          //Natural: PERFORM #TIAA-FUND-INFO
            sub_Pnd_Tiaa_Fund_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '2','4'
        else if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("2") || pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("4"))))
        {
            decideConditionsMet911++;
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                   //Natural: CALLNAT 'IAAN051A' #FUND-CDE-HOLD #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            pnd_Cref_Title_Tab.getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0L 6L"));                                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0L 6L' TO #CREF-TITLE-TAB ( 1 )
            pnd_Cref_Title_Tab.getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0M 0T 0U 6M Z"));                                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0M 0T 0U 6M Z' TO #CREF-TITLE-TAB ( 2 )
            pnd_Cref_Title_Tab.getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "0N 6N"));                                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '0N 6N' TO #CREF-TITLE-TAB ( 3 )
            //*  LB 11/97
            pnd_Cref_Title_Tab.getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                                            //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'U','W'
        else if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("U") || pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("W"))))
        {
            decideConditionsMet911++;
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                   //Natural: CALLNAT 'IAAN051A' #FUND-CDE-HOLD #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            //*  PA SELECT FUND CHANGES
            if (condition(pnd_Comp_Desc.equals("T/L ")))                                                                                                                  //Natural: IF #COMP-DESC = 'T/L '
            {
                pnd_Cref_Title_Tab.getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PSI GW WO"));                                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PSI GW WO' TO #CREF-TITLE-TAB ( 1 )
                pnd_Cref_Title_Tab.getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PSI IA-IF"));                                    //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PSI IA-IF' TO #CREF-TITLE-TAB ( 2 )
                pnd_Cref_Title_Tab.getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "PS S0"));                                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'PS S0' TO #CREF-TITLE-TAB ( 3 )
                pnd_Cref_Title_Tab.getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Title_Tab.getValue(1).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6L GW W0"));                                     //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6L GW W0' TO #CREF-TITLE-TAB ( 1 )
                pnd_Cref_Title_Tab.getValue(2).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6M GA IA-IF Y"));                                //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6M GA IA-IF Y' TO #CREF-TITLE-TAB ( 2 )
                //*   ADDED S0 TO FOLLOWING FOR REA FUND 9/01
                pnd_Cref_Title_Tab.getValue(3).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "6N S0"));                                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 '6N S0' TO #CREF-TITLE-TAB ( 3 )
                pnd_Cref_Title_Tab.getValue(4).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "TOTAL"));                                        //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'TOTAL' TO #CREF-TITLE-TAB ( 4 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #FUND-INFORMATION
    }
    private void sub_Pnd_Tiaa_Fund_Info() throws Exception                                                                                                                //Natural: #TIAA-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Fund_Types.reset();                                                                                                                                           //Natural: RESET #FUND-TYPES
        short decideConditionsMet948 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #FUND-CDE-HOLD;//Natural: VALUE '1G'
        if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold.equals("1G"))))
        {
            decideConditionsMet948++;
            pnd_Fund_Types.setValue(2);                                                                                                                                   //Natural: MOVE 2 TO #FUND-TYPES
            //* PAGE 1
            pnd_Tiaa_Title_Tab.getValue(1).setValue("TIAA PENSION GRADED IA ILC    ");                                                                                    //Natural: MOVE 'TIAA PENSION GRADED IA ILC    ' TO #TIAA-TITLE-TAB ( 1 )
            //* PAGE 2
            pnd_Tiaa_Title_Tab.getValue(2).setValue("TIAA PENSION GRADED SC ILC    ");                                                                                    //Natural: MOVE 'TIAA PENSION GRADED SC ILC    ' TO #TIAA-TITLE-TAB ( 2 )
            pnd_Tiaa_Title_Tab.getValue(3).setValue("                              ");                                                                                    //Natural: MOVE '                              ' TO #TIAA-TITLE-TAB ( 3 )
            pnd_Tiaa_Title_Tab.getValue(4).setValue("TIAA GRADED TOTAL             ");                                                                                    //Natural: MOVE 'TIAA GRADED TOTAL             ' TO #TIAA-TITLE-TAB ( 4 )
            pnd_Print_Tpa_Ipro_Pi.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #PRINT-TPA-IPRO-PI
        }                                                                                                                                                                 //Natural: VALUE '1S'
        else if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold.equals("1S"))))
        {
            decideConditionsMet948++;
            pnd_Fund_Types.setValue(3);                                                                                                                                   //Natural: MOVE 3 TO #FUND-TYPES
            //* PAGE 3
            pnd_Tiaa_Title_Tab.getValue(1).setValue("TIAA PENSION STANDARD IA ILC        ");                                                                              //Natural: MOVE 'TIAA PENSION STANDARD IA ILC        ' TO #TIAA-TITLE-TAB ( 1 )
            //* PAGE 4
            pnd_Tiaa_Title_Tab.getValue(2).setValue("TIAA PENSION STANDARD SC ILC  ");                                                                                    //Natural: MOVE 'TIAA PENSION STANDARD SC ILC  ' TO #TIAA-TITLE-TAB ( 2 )
            //* PAGE 5
            pnd_Tiaa_Title_Tab.getValue(3).setValue("TIAA STANDARD PENSION S0                          ");                                                                //Natural: MOVE 'TIAA STANDARD PENSION S0                          ' TO #TIAA-TITLE-TAB ( 3 )
            pnd_Tiaa_Title_Tab.getValue(4).setValue("TIAA STANDARD TOTAL           ");                                                                                    //Natural: MOVE 'TIAA STANDARD TOTAL           ' TO #TIAA-TITLE-TAB ( 4 )
            //*  GROUP CONTRACTS ** 7/25/08
            pnd_Print_Tpa_Ipro_Pi.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #PRINT-TPA-IPRO-PI
        }                                                                                                                                                                 //Natural: VALUE '1Z'
        else if (condition((pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold.equals("1Z"))))
        {
            decideConditionsMet948++;
            pnd_Fund_Types.setValue(2);                                                                                                                                   //Natural: MOVE 2 TO #FUND-TYPES
            pnd_Tiaa_Title_Tab.getValue(1).setValue("Group Annuity Direct Payment - Insurance Type");                                                                     //Natural: MOVE 'Group Annuity Direct Payment - Insurance Type' TO #TIAA-TITLE-TAB ( 1 )
            pnd_Tiaa_Title_Tab.getValue(2).setValue("Group Annuity Non-Participating");                                                                                   //Natural: MOVE 'Group Annuity Non-Participating' TO #TIAA-TITLE-TAB ( 2 )
            pnd_Tiaa_Title_Tab.getValue(3).setValue("Group Annuity Direct Payment - Deposit Type");                                                                       //Natural: MOVE 'Group Annuity Direct Payment - Deposit Type' TO #TIAA-TITLE-TAB ( 3 )
            pnd_Tiaa_Title_Tab.getValue(4).setValue("GROUP TOTAL                   ");                                                                                    //Natural: MOVE 'GROUP TOTAL                   ' TO #TIAA-TITLE-TAB ( 4 )
            pnd_Print_Tpa_Ipro_Pi.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #PRINT-TPA-IPRO-PI
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #TIAA-FUND-INFO
    }
    private void sub_Pnd_Process_Fund_1g() throws Exception                                                                                                               //Natural: #PROCESS-FUND-1G
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN 3 PAGE 2
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.greater(getZero())))                                                                                           //Natural: IF #I-DDCTN-PER-AMT GT 0
        {
            //*  10/2017 START
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                             //Natural: IF #I-DDCTN-PER-AMT = 3 OR = 46
            {
                if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 3
                {
                    pnd_Org_03_Tab_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB-G ( #MODE-1,1 )
                    pnd_Org_03_Tab_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB-G ( #MODE-1,2 )
                    pnd_Org_03_Tab_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB-G ( #MODE-1,3 )
                    pnd_Org_03_Tab_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB-G ( #MODE-1,4 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TOT ( #MODE-1,1 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TOT ( #MODE-1,2 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TOT ( #MODE-1,3 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TOT ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                        new ReportEditMask ("ZZZZZZ9.99"));
                    if (Global.isEscape()) return;
                    pnd_Org_46_Tab_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB-G ( #MODE-1,1 )
                    pnd_Org_46_Tab_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB-G ( #MODE-1,2 )
                    pnd_Org_46_Tab_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB-G ( #MODE-1,3 )
                    pnd_Org_46_Tab_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB-G ( #MODE-1,4 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TOT ( #MODE-1,1 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TOT ( #MODE-1,2 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TOT ( #MODE-1,3 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TOT ( #MODE-1,4 )
                    //*  10/2017 END
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
                pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
                pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
                pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
                pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
                pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
                pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
                pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
                pnd_Cnt2.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT2
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        short decideConditionsMet1028 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = 'GA' OR = '6M' OR #I-CNT-NBR-2 = 'IA' THRU 'IF' OR #I-CNT-NBR-2 = 'Y0' THRU 'Y9'
        if (condition((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GA") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6M")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("IA") 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("IF"))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("Y0") && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("Y9")))))
        {
            decideConditionsMet1028++;
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,1 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,2 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,3 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'GW' OR = '6L' OR ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 > 0 AND #I-CNT-NBR-3-7 < 25000 ) )
        else if (condition(((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GW") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6L")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") 
            && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greater(getZero()) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.less(25000))))))
        {
            decideConditionsMet1028++;
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,1 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,2 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,3 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "FUND 1G ERROR  ","=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);                                         //Natural: WRITE 'FUND 1G ERROR  ' '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-1G
    }
    private void sub_Pnd_Process_Fund_1s() throws Exception                                                                                                               //Natural: #PROCESS-FUND-1S
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  PAGE 3
        //*  ADDED 7/03/2001
        short decideConditionsMet1074 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = 'GA' OR = '6M' OR #I-CNT-NBR-2 = 'IA' THRU 'IF' OR #I-CNT-NBR-2 = 'IG' OR = 'IP' OR #I-CNT-NBR-2 = 'Y0' THRU 'Y9'
        if (condition(((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GA") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6M")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("IA") 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("IF"))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IG") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IP"))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("Y0") && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("Y9")))))
        {
            decideConditionsMet1074++;
            //*  ADDED FOLLOWING 9/04 ACCUM PA CONTRACTS
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(18)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 18
            {
                pnd_Org_18_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-18-TAB ( #MODE-1,1 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-18-TAB ( #MODE-1,2 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-18-TAB ( #MODE-1,3 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-18-TAB ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  02/07 JFT
            //*  07/11 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(38) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(43) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(44)  //Natural: IF #I-DDCTN-PER-AMT = 38 OR = 43 OR = 44 OR = 45 OR = 35 OR = 36
                || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(45) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(35) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(36)))
            {
                pnd_Org_38_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-38-TAB ( #MODE-1,1 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-38-TAB ( #MODE-1,2 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-38-TAB ( #MODE-1,3 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-38-TAB ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(40)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 40
            {
                pnd_Org_40_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-40-TAB ( #MODE-1,1 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-40-TAB ( #MODE-1,2 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-40-TAB ( #MODE-1,3 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-40-TAB ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 9/04 ACCUM PA CONTRACTS
            //*  ADDED 9/04
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(getZero())))                                                                                        //Natural: IF #I-DDCTN-PER-AMT = 0
            {
                pnd_Tiaa_Table_1.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,1 )
                pnd_Tiaa_Table_1.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,2 )
                pnd_Tiaa_Table_1.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,3 )
                pnd_Tiaa_Table_1.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,4 )
                //*  ADDED 9/04
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            //*  PAGE 4
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'GW' OR = '6L' OR ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 > 0 AND #I-CNT-NBR-3-7 < 25000 ) )
        else if (condition(((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GW") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6L")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") 
            && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greater(getZero()) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.less(25000))))))
        {
            decideConditionsMet1074++;
            //*  ADDED FOLLOWING 9/04 ACCUM PA CONTRACTS
            //*  02/07 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                             //Natural: IF #I-DDCTN-PER-AMT = 03 OR = 46
            {
                //*  10/2017 START
                if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 03
                {
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB ( #MODE-1,1 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB ( #MODE-1,2 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB ( #MODE-1,3 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                        new ReportEditMask ("ZZZZZZ9.99"));
                    if (Global.isEscape()) return;
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB ( #MODE-1,1 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB ( #MODE-1,2 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB ( #MODE-1,3 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: END-IF
                //*  10/2017 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 17
            {
                pnd_Org_17_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TAB ( #MODE-1,1 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TAB ( #MODE-1,2 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TAB ( #MODE-1,3 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TAB ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 37
            {
                pnd_Org_37_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TAB ( #MODE-1,1 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TAB ( #MODE-1,2 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TAB ( #MODE-1,3 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TAB ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 9/04 ACCUM PA CONTRACTS
            //*  ADDED 9/04
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(getZero())))                                                                                        //Natural: IF #I-DDCTN-PER-AMT = 0
            {
                pnd_Tiaa_Table_2.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,1 )
                pnd_Tiaa_Table_2.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,2 )
                pnd_Tiaa_Table_2.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,3 )
                pnd_Tiaa_Table_2.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,4 )
                //*  ADDED 9/04
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            //*  PAGE 5
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'S0'
        else if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("S0")))
        {
            decideConditionsMet1074++;
            //*  ADDED FOLLOWING 9/04 ACCUM PA CONTRACTS
            //*  02/07 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                             //Natural: IF #I-DDCTN-PER-AMT = 03 OR = 46
            {
                //*  10/2017 START
                if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 03
                {
                    pnd_Org_03_Tab_Std_S0.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB-STD-S0 ( #MODE-1,1 )
                    pnd_Org_03_Tab_Std_S0.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB-STD-S0 ( #MODE-1,2 )
                    pnd_Org_03_Tab_Std_S0.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB-STD-S0 ( #MODE-1,3 )
                    pnd_Org_03_Tab_Std_S0.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB-STD-S0 ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                        new ReportEditMask ("ZZZZZZ9.99"));
                    if (Global.isEscape()) return;
                    pnd_Org_46_Tab_Std_S0.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB-STD-S0 ( #MODE-1,1 )
                    pnd_Org_46_Tab_Std_S0.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB-STD-S0 ( #MODE-1,2 )
                    pnd_Org_46_Tab_Std_S0.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                    //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB-STD-S0 ( #MODE-1,3 )
                    pnd_Org_46_Tab_Std_S0.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                    //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB-STD-S0 ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: END-IF
                //*  10/2017 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 17
            {
                pnd_Org_17_Tab_Std_S0.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TAB-STD-S0 ( #MODE-1,1 )
                pnd_Org_17_Tab_Std_S0.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TAB-STD-S0 ( #MODE-1,2 )
                pnd_Org_17_Tab_Std_S0.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TAB-STD-S0 ( #MODE-1,3 )
                pnd_Org_17_Tab_Std_S0.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TAB-STD-S0 ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 37
            {
                pnd_Org_37_Tab_Std_S0.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TAB-STD-S0 ( #MODE-1,1 )
                pnd_Org_37_Tab_Std_S0.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TAB-STD-S0 ( #MODE-1,2 )
                pnd_Org_37_Tab_Std_S0.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TAB-STD-S0 ( #MODE-1,3 )
                pnd_Org_37_Tab_Std_S0.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TAB-STD-S0 ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 9/04 ACCUM PA CONTRACTS
            //*  ADDED 9/04
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(getZero())))                                                                                        //Natural: IF #I-DDCTN-PER-AMT = 0
            {
                pnd_Tiaa_Table_3.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-3 ( #MODE-1,1 )
                pnd_Tiaa_Table_3.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-3 ( #MODE-1,2 )
                pnd_Tiaa_Table_3.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-3 ( #MODE-1,3 )
                pnd_Tiaa_Table_3.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-3 ( #MODE-1,4 )
                //*  ADDED 9/04
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "FUND 1S ERROR  ","=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);                                         //Natural: WRITE 'FUND 1S ERROR  ' '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
            //*      IGNORE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   #PROCESS-FUND-1S
    }
    private void sub_Pnd_Process_Fund_1s_Pa_Contracts() throws Exception                                                                                                  //Natural: #PROCESS-FUND-1S-PA-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.greater(getZero())))                                                                                           //Natural: IF #I-DDCTN-PER-AMT GT 0
        {
            //*  02/07 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                             //Natural: IF #I-DDCTN-PER-AMT = 3 OR = 46
            {
                //*  10/2017 START
                if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 3
                {
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB ( #MODE-1,1 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB ( #MODE-1,2 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB ( #MODE-1,3 )
                    pnd_Org_03_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB ( #MODE-1,4 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TOT ( #MODE-1,1 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TOT ( #MODE-1,2 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TOT ( #MODE-1,3 )
                    pnd_Org_03_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TOT ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                        new ReportEditMask ("ZZZZZZ9.99"));
                    if (Global.isEscape()) return;
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB ( #MODE-1,1 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB ( #MODE-1,2 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB ( #MODE-1,3 )
                    pnd_Org_46_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB ( #MODE-1,4 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TOT ( #MODE-1,1 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TOT ( #MODE-1,2 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TOT ( #MODE-1,3 )
                    pnd_Org_46_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                           //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TOT ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: END-IF
                //*  10/2017 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 17
            {
                pnd_Org_17_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TAB ( #MODE-1,1 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TAB ( #MODE-1,2 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TAB ( #MODE-1,3 )
                pnd_Org_17_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TAB ( #MODE-1,4 )
                pnd_Org_17_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TOT ( #MODE-1,1 )
                pnd_Org_17_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TOT ( #MODE-1,2 )
                pnd_Org_17_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TOT ( #MODE-1,3 )
                pnd_Org_17_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TOT ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(18)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 18
            {
                pnd_Org_18_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-18-TAB ( #MODE-1,1 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-18-TAB ( #MODE-1,2 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-18-TAB ( #MODE-1,3 )
                pnd_Org_18_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-18-TAB ( #MODE-1,4 )
                pnd_Org_18_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-18-TOT ( #MODE-1,1 )
                pnd_Org_18_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-18-TOT ( #MODE-1,2 )
                pnd_Org_18_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-18-TOT ( #MODE-1,3 )
                pnd_Org_18_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-18-TOT ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 37
            {
                pnd_Org_37_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TAB ( #MODE-1,1 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TAB ( #MODE-1,2 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TAB ( #MODE-1,3 )
                pnd_Org_37_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TAB ( #MODE-1,4 )
                pnd_Org_37_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TOT ( #MODE-1,1 )
                pnd_Org_37_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TOT ( #MODE-1,2 )
                pnd_Org_37_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TOT ( #MODE-1,3 )
                pnd_Org_37_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TOT ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  02/07 JFT
            //*  07/11 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(38) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(43) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(44)  //Natural: IF #I-DDCTN-PER-AMT = 38 OR = 43 OR = 44 OR = 45 OR = 35 OR = 36
                || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(45) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(35) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(36)))
            {
                pnd_Org_38_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-38-TAB ( #MODE-1,1 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-38-TAB ( #MODE-1,2 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-38-TAB ( #MODE-1,3 )
                pnd_Org_38_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-38-TAB ( #MODE-1,4 )
                pnd_Org_38_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-38-TOT ( #MODE-1,1 )
                pnd_Org_38_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-38-TOT ( #MODE-1,2 )
                pnd_Org_38_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-38-TOT ( #MODE-1,3 )
                pnd_Org_38_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-38-TOT ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(40)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 40
            {
                pnd_Org_40_Tab.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-40-TAB ( #MODE-1,1 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-40-TAB ( #MODE-1,2 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-40-TAB ( #MODE-1,3 )
                pnd_Org_40_Tab.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-40-TAB ( #MODE-1,4 )
                pnd_Org_40_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-40-TOT ( #MODE-1,1 )
                pnd_Org_40_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-40-TOT ( #MODE-1,2 )
                pnd_Org_40_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-40-TOT ( #MODE-1,3 )
                pnd_Org_40_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-40-TOT ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        //*   #PROCESS-FUND-1S PA CONTRACTS
    }
    private void sub_Pnd_Process_Fund_1z() throws Exception                                                                                                               //Natural: #PROCESS-FUND-1Z
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //* **** 7/25/08 CHANGES
        //*  08/12 - MODIFIED CONTRACT RANGES FOR DEPOSIT, INSURANCE, NON-PAR
        //* GROUP DIRECT PAYMENT - INSURANCE TYPE
        //*  04/15 MOVED TO NON-PART
        short decideConditionsMet1346 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 = 25000 THRU 36999 ) OR ( #I-CNT-NBR-3-7 = 41000 THRU 41999 ) OR ( #I-CNT-NBR-3-7 = 45000 THRU 46999 ) OR ( #I-CNT-NBR-3-7 = 48000 THRU 51999 ) OR ( #I-CNT-NBR-3-7 = 53000 THRU 54099 ) OR ( #I-CNT-NBR-3-7 = 56000 THRU 56024 ) OR ( #I-CNT-NBR-3-7 = 60000 THRU 75999 ) )
        if (condition((((((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(25000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(36999))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(41000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(41999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(45000) 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(46999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(48000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(51999))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(53000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(54099))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(56000) 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(56024))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(60000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(75999)))))
        {
            decideConditionsMet1346++;
            //*      (#I-CNT-NBR-3-7 > 24999 AND #I-CNT-NBR-3-7 < 29000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 29999 AND #I-CNT-NBR-3-7 < 37000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 40999 AND #I-CNT-NBR-3-7 < 42000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 42999 AND #I-CNT-NBR-3-7 < 47000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 47999 AND #I-CNT-NBR-3-7 < 52000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 52999 AND #I-CNT-NBR-3-7 < 57000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 59999 AND #I-CNT-NBR-3-7 < 76450) )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,1 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,2 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-1 ( #MODE-1,3 )
            pnd_Tiaa_Table_1.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-1 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            //*  GROUP NON-PARTICIPATING
            //*  04/15 FROM DIRECT PAY
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 = 37000 THRU 40999 ) OR ( #I-CNT-NBR-3-7 = 42000 THRU 42999 ) OR ( #I-CNT-NBR-3-7 = 43000 THRU 44999 ) OR ( #I-CNT-NBR-3-7 = 47000 THRU 47999 ) OR ( #I-CNT-NBR-3-7 = 52000 THRU 52999 ) OR ( #I-CNT-NBR-3-7 = 59000 THRU 59999 ) OR ( #I-CNT-NBR-3-7 = 80000 THRU 89999 ) )
        else if (condition((((((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(37000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(40999))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(42000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(42999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(43000) 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(44999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(47000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(47999))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(52000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(52999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(59000) 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(59999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(80000) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(89999)))))
        {
            decideConditionsMet1346++;
            //*      (#I-CNT-NBR-3-7 > 28999 AND #I-CNT-NBR-3-7 < 30000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 36999 AND #I-CNT-NBR-3-7 < 41000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 41999 AND #I-CNT-NBR-3-7 < 43000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 46999 AND #I-CNT-NBR-3-7 < 48000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 51999 AND #I-CNT-NBR-3-7 < 53000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 58999 AND #I-CNT-NBR-3-7 < 60000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 79999 AND #I-CNT-NBR-3-7 < 90000) )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,1 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,2 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-2 ( #MODE-1,3 )
            pnd_Tiaa_Table_2.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-2 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            //*  GROUP ANNUITY DIRECT PAYMENT-DEPOSIT
            //*  TYPE
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 = 54100 THRU 55999 ) OR ( #I-CNT-NBR-3-7 = 56025 THRU 58999 ) OR ( #I-CNT-NBR-3-7 = 76000 THRU 79999 ) )
        else if (condition((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(54100) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(55999))) 
            || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(56025) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(58999))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greaterOrEqual(76000) 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.lessOrEqual(79999)))))
        {
            decideConditionsMet1346++;
            //*  08/12 - END
            //*      (#I-CNT-NBR-3-7 > 56999 AND #I-CNT-NBR-3-7 < 59000)
            //*      OR
            //*      (#I-CNT-NBR-3-7 > 76449 AND #I-CNT-NBR-3-7 < 80000))
            pnd_Tiaa_Table_3.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-3 ( #MODE-1,1 )
            pnd_Tiaa_Table_3.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-3 ( #MODE-1,2 )
            pnd_Tiaa_Table_3.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-3 ( #MODE-1,3 )
            pnd_Tiaa_Table_3.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-3 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                               //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-G ( #MODE-1,4 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "FUND 1  ERROR  ","=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt); //Natural: WRITE 'FUND 1  ERROR  ' '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE '=' #I-TIAA-PER-PAY-AMT
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
            //*      IGNORE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-1Z
    }
    private void sub_Pnd_Process_Tpa_Ipro_Pi() throws Exception                                                                                                           //Natural: #PROCESS-TPA-IPRO-PI
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  TPA
        short decideConditionsMet1443 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #I-CNTRCT-OPTN-CODE;//Natural: VALUE 28,30
        if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30))))
        {
            decideConditionsMet1443++;
            pnd_Tiaa_Table_4.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-4 ( #MODE-1,1 )
            pnd_Tiaa_Table_4.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-4 ( #MODE-1,2 )
            pnd_Tiaa_Table_4.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-4 ( #MODE-1,3 )
            pnd_Tiaa_Table_4.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-4 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_4.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-4 ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_4.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-4 ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_4.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-4 ( #MODE-1,3 )
            //*  IPRO
            pnd_Tiaa_Table_Tot_4.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-4 ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(25) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(27))))
        {
            decideConditionsMet1443++;
            pnd_Tiaa_Table_5.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-5 ( #MODE-1,1 )
            pnd_Tiaa_Table_5.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-5 ( #MODE-1,2 )
            pnd_Tiaa_Table_5.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-5 ( #MODE-1,3 )
            pnd_Tiaa_Table_5.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-5 ( #MODE-1,4 )
            pnd_Tiaa_Table_Tot_5.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-5 ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_5.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-5 ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_5.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-5 ( #MODE-1,3 )
            //*  P&I S0 PAGE 8
            pnd_Tiaa_Table_Tot_5.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-5 ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: VALUE 22
        else if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22))))
        {
            decideConditionsMet1443++;
            //*  ADDED 9/04
            if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("S0")))                                                                                                 //Natural: IF #I-CNT-NBR-2 = 'S0'
            {
                //*  P&I S0 OPT22    PAGE 8
                                                                                                                                                                          //Natural: PERFORM #ACCUM-OPT22-S0
                sub_Pnd_Accum_Opt22_S0();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED FOLLOWING 9/04             /* P&I NON-S0 OPT22 PAGE 9
            //*  02/07 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                             //Natural: IF #I-DDCTN-PER-AMT = 03 OR = 46
            {
                //*  10/2017 START
                if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 03
                {
                    pnd_Org_03_Tab_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB-PI ( #MODE-1,1 )
                    pnd_Org_03_Tab_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB-PI ( #MODE-1,2 )
                    pnd_Org_03_Tab_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB-PI ( #MODE-1,3 )
                    pnd_Org_03_Tab_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB-PI ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                        new ReportEditMask ("ZZZZZZ9.99"));
                    if (Global.isEscape()) return;
                    pnd_Org_46_Tab_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB-PI ( #MODE-1,1 )
                    pnd_Org_46_Tab_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB-PI ( #MODE-1,2 )
                    pnd_Org_46_Tab_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB-PI ( #MODE-1,3 )
                    pnd_Org_46_Tab_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                        //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB-PI ( #MODE-1,4 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 17
            {
                pnd_Org_17_Tab_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                   //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TAB-PI ( #MODE-1,1 )
                pnd_Org_17_Tab_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                   //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TAB-PI ( #MODE-1,2 )
                pnd_Org_17_Tab_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                            //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TAB-PI ( #MODE-1,3 )
                pnd_Org_17_Tab_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                            //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TAB-PI ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 37
            {
                pnd_Org_37_Tab_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                   //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TAB-PI ( #MODE-1,1 )
                pnd_Org_37_Tab_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                   //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TAB-PI ( #MODE-1,2 )
                pnd_Org_37_Tab_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                            //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TAB-PI ( #MODE-1,3 )
                pnd_Org_37_Tab_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                            //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TAB-PI ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 9/04 ACCUM PA CONTRACTS
            //*  ADDED 9/04
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(getZero())))                                                                                        //Natural: IF #I-DDCTN-PER-AMT = 0
            {
                pnd_Tiaa_Table_7.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-7 ( #MODE-1,1 )
                pnd_Tiaa_Table_7.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-7 ( #MODE-1,2 )
                pnd_Tiaa_Table_7.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-7 ( #MODE-1,3 )
                pnd_Tiaa_Table_7.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-7 ( #MODE-1,4 )
                //*  ADDED 9/04
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Table_Tot_7.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-7 ( #MODE-1,1 )
            pnd_Tiaa_Table_Tot_7.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-7 ( #MODE-1,2 )
            pnd_Tiaa_Table_Tot_7.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-7 ( #MODE-1,3 )
            pnd_Tiaa_Table_Tot_7.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-7 ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ADDED 6/03
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BT")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = 'RINV' OR = '57BC' OR = '57BT'
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") 
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("RINV") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
        {
            pnd_Eof_Sw.setValue("B");                                                                                                                                     //Natural: MOVE 'B' TO #EOF-SW
            //*  WRITE REPORT 1 FOR ROLLOVERS
            DbsUtil.callnat(Iaap110c.class , getCurrentProcessState(), pnd_Eof_Sw, pnd_Mode_1, pnd_Input_Record_Pnd_I_Cnt_Nbr, pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde,  //Natural: CALLNAT 'IAAP110C' #EOF-SW #MODE-1 #I-CNT-NBR #I-CNT-CURR-DIST-CDE #I-CNTRCT-OPTN-CODE #I-TIAA-PER-PAY-AMT #I-TIAA-PER-DIV-AMT #I-TIAA-RATE-FINAL-PAY-AMT #I-TIAA-RATE-FINAL-DIV-AMT #MODE-TABLE-2 ( * ) #PAGE-NUMBER
                pnd_Input_Record_Pnd_I_Cntrct_Optn_Code, pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt, pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt, pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt, 
                pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt, pnd_Mode_Table_2.getValue("*"), pnd_Page_Number);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-TPA-IPRO-PI
    }
    private void sub_Pnd_Accum_Opt22_S0() throws Exception                                                                                                                //Natural: #ACCUM-OPT22-S0
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  02/07 JFT
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                                 //Natural: IF #I-DDCTN-PER-AMT = 03 OR = 46
        {
            //*  10/2017 START
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                                //Natural: IF #I-DDCTN-PER-AMT = 03
            {
                pnd_Org_03_Tab_S0_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-TAB-S0-PI ( #MODE-1,1 )
                pnd_Org_03_Tab_S0_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-TAB-S0-PI ( #MODE-1,2 )
                pnd_Org_03_Tab_S0_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-03-TAB-S0-PI ( #MODE-1,3 )
                pnd_Org_03_Tab_S0_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-03-TAB-S0-PI ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                    new ReportEditMask ("ZZZZZZ9.99"));
                if (Global.isEscape()) return;
                pnd_Org_46_Tab_S0_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-TAB-S0-PI ( #MODE-1,1 )
                pnd_Org_46_Tab_S0_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-TAB-S0-PI ( #MODE-1,2 )
                pnd_Org_46_Tab_S0_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-46-TAB-S0-PI ( #MODE-1,3 )
                pnd_Org_46_Tab_S0_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                         //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-46-TAB-S0-PI ( #MODE-1,4 )
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2017 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                                   //Natural: IF #I-DDCTN-PER-AMT = 17
        {
            pnd_Org_17_Tab_S0_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-TAB-S0-PI ( #MODE-1,1 )
            pnd_Org_17_Tab_S0_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-TAB-S0-PI ( #MODE-1,2 )
            pnd_Org_17_Tab_S0_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-17-TAB-S0-PI ( #MODE-1,3 )
            pnd_Org_17_Tab_S0_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-17-TAB-S0-PI ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                                   //Natural: IF #I-DDCTN-PER-AMT = 37
        {
            pnd_Org_37_Tab_S0_Pi.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                    //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-TAB-S0-PI ( #MODE-1,1 )
            pnd_Org_37_Tab_S0_Pi.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                    //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-TAB-S0-PI ( #MODE-1,2 )
            pnd_Org_37_Tab_S0_Pi.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #ORG-37-TAB-S0-PI ( #MODE-1,3 )
            pnd_Org_37_Tab_S0_Pi.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                             //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #ORG-37-TAB-S0-PI ( #MODE-1,4 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 9/04 ACCUM PA CONTRACTS
        //*  ADDED 9/04
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(getZero())))                                                                                            //Natural: IF #I-DDCTN-PER-AMT = 0
        {
            pnd_Tiaa_Table_6.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-6 ( #MODE-1,1 )
            pnd_Tiaa_Table_6.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-6 ( #MODE-1,2 )
            pnd_Tiaa_Table_6.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-6 ( #MODE-1,3 )
            pnd_Tiaa_Table_6.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-6 ( #MODE-1,4 )
            //*  ADDED 9/04
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Table_Tot_6.getValue(pnd_Mode_1,1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                        //Natural: ADD #I-TIAA-PER-PAY-AMT TO #TIAA-TABLE-TOT-6 ( #MODE-1,1 )
        pnd_Tiaa_Table_Tot_6.getValue(pnd_Mode_1,2).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                        //Natural: ADD #I-TIAA-PER-DIV-AMT TO #TIAA-TABLE-TOT-6 ( #MODE-1,2 )
        pnd_Tiaa_Table_Tot_6.getValue(pnd_Mode_1,3).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-PAY-AMT TO #TIAA-TABLE-TOT-6 ( #MODE-1,3 )
        pnd_Tiaa_Table_Tot_6.getValue(pnd_Mode_1,4).nadd(pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt);                                                                 //Natural: ADD #I-TIAA-RATE-FINAL-DIV-AMT TO #TIAA-TABLE-TOT-6 ( #MODE-1,4 )
        //*  ADDED 6/03
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BT")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = 'RINV' OR = '57BC' OR = '57BT'
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") 
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("RINV") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
        {
            pnd_Eof_Sw.setValue("B");                                                                                                                                     //Natural: MOVE 'B' TO #EOF-SW
            //*  WRITE REPORT 1 FOR ROLLOVERS
            DbsUtil.callnat(Iaap110c.class , getCurrentProcessState(), pnd_Eof_Sw, pnd_Mode_1, pnd_Input_Record_Pnd_I_Cnt_Nbr, pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde,  //Natural: CALLNAT 'IAAP110C' #EOF-SW #MODE-1 #I-CNT-NBR #I-CNT-CURR-DIST-CDE #I-CNTRCT-OPTN-CODE #I-TIAA-PER-PAY-AMT #I-TIAA-PER-DIV-AMT #I-TIAA-RATE-FINAL-PAY-AMT #I-TIAA-RATE-FINAL-DIV-AMT #MODE-TABLE-2 ( * ) #PAGE-NUMBER
                pnd_Input_Record_Pnd_I_Cntrct_Optn_Code, pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt, pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt, pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt, 
                pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt, pnd_Mode_Table_2.getValue("*"), pnd_Page_Number);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #ACCUM-OPT22-S0
    }
    private void sub_Pnd_Process_Totals_Reports() throws Exception                                                                                                        //Natural: #PROCESS-TOTALS-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        FP:                                                                                                                                                               //Natural: FOR #Z = 1 TO 4
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(4)); pnd_Z.nadd(1))
        {
            if (condition(pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(pnd_Pay_Mode_Table.getValue(pnd_Z))))                                                                //Natural: IF #I-CNT-MODE-IND = #PAY-MODE-TABLE ( #Z )
            {
                pnd_Fmt3_Mode_Sub.setValue(pnd_Z);                                                                                                                        //Natural: MOVE #Z TO #FMT3-MODE-SUB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Cref_Contract.equals("Y")))                                                                                                                     //Natural: IF #CREF-CONTRACT = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-CREF-CONTRACTS
            sub_Pnd_Process_Cref_Contracts();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Pa_Contract.equals("Y")))                                                                                                                   //Natural: IF #PA-CONTRACT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-PA-CONTRACTS
                sub_Pnd_Process_Pa_Contracts();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM #PROCESS-TEACHER-CONTRACTS
                sub_Pnd_Process_Teacher_Contracts();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-TOTALS-REPORTS
    }
    private void sub_Pnd_Process_Teacher_Contracts() throws Exception                                                                                                     //Natural: #PROCESS-TEACHER-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  FOR VALUED NOT PAID CONTRACTS FIN-PAY-AMTS
        pnd_Final_Pay_Flag.reset();                                                                                                                                       //Natural: RESET #FINAL-PAY-FLAG
        pnd_Fmt3_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                  //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT3-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
        pnd_Fmt3_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                  //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT3-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
        if (condition(((pnd_Input_Record_Pnd_I_Cnt_Final_Pay_Dte.equals(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte) && (pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Pay_Amt.greater(getZero())  //Natural: IF ( #I-CNT-FINAL-PAY-DTE = #SAVE-CHECK-DTE ) AND ( #I-TIAA-RATE-FINAL-PAY-AMT > 0 OR #I-TIAA-RATE-FINAL-DIV-AMT > 0 ) AND ( #I-CNT-PAYEE-CDE > 2 OR ( #I-CNT-PAYEE-CDE < 3 AND #I-CNTRCT-OPTN-CODE = 21 OR = 22 OR = 23 OR = 28 OR = 30 ) )
            || pnd_Input_Record_Pnd_I_Tiaa_Rate_Final_Div_Amt.greater(getZero()))) && (pnd_Input_Record_Pnd_I_Cnt_Payee_Cde.greater(2) || (pnd_Input_Record_Pnd_I_Cnt_Payee_Cde.less(3) 
            && ((((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(21) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22)) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(23)) 
            || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28)) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30)))))))
        {
            pnd_Cnt_Payee_Field.setValue(DbsUtil.compress(pnd_Input_Record_Pnd_I_Cnt_Nbr_8, pnd_Input_Record_Pnd_I_Cnt_Payee_Cde_A));                                     //Natural: COMPRESS #I-CNT-NBR-8 #I-CNT-PAYEE-CDE-A INTO #CNT-PAYEE-FIELD
            getReports().write(4, ReportOption.NOTITLE,pnd_Cnt_Payee_Field,new ColumnSpacing(10),pnd_Input_Record_Pnd_I_Cnt_Mode_Ind,new ColumnSpacing(12),pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,new  //Natural: WRITE ( 4 ) #CNT-PAYEE-FIELD 10X #I-CNT-MODE-IND 12X #I-TIAA-PER-PAY-AMT 10X #I-TIAA-PER-DIV-AMT
                ColumnSpacing(10),pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);
            if (Global.isEscape()) return;
            pnd_Final_Pay_Flag.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #FINAL-PAY-FLAG
            pnd_Fmt4_Cnt_Amt_Tot.nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                                                           //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT4-CNT-AMT-TOT
            pnd_Fmt4_Per_Div_Tot.nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                                                           //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT4-PER-DIV-TOT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Final_Pay_Flag.equals("Y")))                                                                                                                    //Natural: IF #FINAL-PAY-FLAG = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fmt5_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                              //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT5-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
            pnd_Fmt5_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                              //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT5-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
            //*  ADDED 6/03
            if (condition(pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BT")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
                || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") 
                || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
            {
                //*  TPA
                short decideConditionsMet1636 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #I-CNTRCT-OPTN-CODE;//Natural: VALUE 28,30
                if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30))))
                {
                    decideConditionsMet1636++;
                    //*  ADDED 9/01
                    pnd_Tpa_Roll_Cntrcts.getValue(pnd_Fmt3_Mode_Sub).nadd(1);                                                                                             //Natural: ADD 1 TO #TPA-ROLL-CNTRCTS ( #FMT3-MODE-SUB )
                    pnd_Fmt8_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT8-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    //* IPRO
                    pnd_Fmt8_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT8-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: VALUE 25,27
                else if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(25) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(27))))
                {
                    decideConditionsMet1636++;
                    //*  ADDED 9/01
                    pnd_Ipr_Roll_Cntrcts.getValue(pnd_Fmt3_Mode_Sub).nadd(1);                                                                                             //Natural: ADD 1 TO #IPR-ROLL-CNTRCTS ( #FMT3-MODE-SUB )
                    pnd_Fmt9_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT9-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    //*  P/I
                    pnd_Fmt9_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT9-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: VALUE 22
                else if (condition((pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(22))))
                {
                    decideConditionsMet1636++;
                    //*  ADDED 9/01
                    pnd_P_I_Roll_Cntrcts.getValue(pnd_Fmt3_Mode_Sub).nadd(1);                                                                                             //Natural: ADD 1 TO #P-I-ROLL-CNTRCTS ( #FMT3-MODE-SUB )
                    pnd_Fmt10_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                     //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT10-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    //*   ANNUITY CERTAIN
                    pnd_Fmt10_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                     //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT10-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    //*  ADDED 9/01
                    pnd_Ac_Roll_Cntrcts.getValue(pnd_Fmt3_Mode_Sub).nadd(1);                                                                                              //Natural: ADD 1 TO #AC-ROLL-CNTRCTS ( #FMT3-MODE-SUB )
                    pnd_Fmt7_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT7-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    pnd_Fmt7_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT7-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition((pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("RINV") && (pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(28) || pnd_Input_Record_Pnd_I_Cntrct_Optn_Code.equals(30))))) //Natural: IF #I-CNT-CURR-DIST-CDE = 'RINV' AND #I-CNTRCT-OPTN-CODE = 28 OR = 30
                {
                    //*  ADDED 9/01
                    pnd_Tpa_Rinv_Cntrcts.getValue(pnd_Fmt3_Mode_Sub).nadd(1);                                                                                             //Natural: ADD 1 TO #TPA-RINV-CNTRCTS ( #FMT3-MODE-SUB )
                    pnd_Fmt11_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                     //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT11-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    pnd_Fmt11_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                     //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT11-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fmt6_Tiaa_Table.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                      //Natural: ADD #I-TIAA-PER-PAY-AMT TO #FMT6-TIAA-TABLE ( 1,#FMT3-MODE-SUB )
                    pnd_Fmt6_Tiaa_Table.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                      //Natural: ADD #I-TIAA-PER-DIV-AMT TO #FMT6-TIAA-TABLE ( 2,#FMT3-MODE-SUB )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        //*        FOR CURRENT MODE PAYMENTS DUE REPORT
        if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.greater(getZero())))                                                                                           //Natural: IF #I-DDCTN-PER-AMT GT 0
        {
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(3)))                                                                                                //Natural: IF #I-DDCTN-PER-AMT = 3
            {
                pnd_Org_03_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-03-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_03_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-03-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2017 START
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(46)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 46
            {
                getReports().write(0, "=",pnd_Input_Record_Pnd_I_Ddctn_Per_Amt,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot,"=",pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt,  //Natural: WRITE '=' #I-DDCTN-PER-AMT '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE-TOT '=' #I-TIAA-PER-PAY-AMT ( EM = ZZZZZZ9.99 )
                    new ReportEditMask ("ZZZZZZ9.99"));
                if (Global.isEscape()) return;
                pnd_Org_46_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-46-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_46_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-46-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2017 END
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(17)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 17
            {
                pnd_Org_17_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-17-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_17_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-17-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(18)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 18
            {
                pnd_Org_18_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-18-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_18_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-18-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(37)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 37
            {
                pnd_Org_37_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-37-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_37_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-37-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            //*  02/07 JFT
            //*  07/11 JFT
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(38) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(43) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(44)  //Natural: IF #I-DDCTN-PER-AMT = 38 OR = 43 OR = 44 OR = 45 OR = 35 OR = 36
                || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(45) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(35) || pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(36)))
            {
                pnd_Org_38_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-38-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_38_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-38-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_I_Ddctn_Per_Amt.equals(40)))                                                                                               //Natural: IF #I-DDCTN-PER-AMT = 40
            {
                pnd_Org_40_Due.getValue(1,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Pay_Amt);                                                               //Natural: ADD #I-TIAA-PER-PAY-AMT TO #ORG-40-DUE ( 1,#FMT3-MODE-SUB )
                pnd_Org_40_Due.getValue(2,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Per_Div_Amt);                                                               //Natural: ADD #I-TIAA-PER-DIV-AMT TO #ORG-40-DUE ( 2,#FMT3-MODE-SUB )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        //*  #PROCESS-TEACHER-CONTRACTS
    }
    private void sub_Pnd_Process_Cref_Contracts() throws Exception                                                                                                        //Natural: #PROCESS-CREF-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Fmt3_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT3-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        pnd_Fmt5_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT5-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
        sub_Pnd_Get_Dollar_Amount();
        if (condition(Global.isEscape())) {return;}
        pnd_Fmt3_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT3-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        pnd_Fmt5_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT5-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        //*  ADDED 6/03
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BT")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") 
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
        {
            pnd_Fmt7_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                      //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT7-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            pnd_Fmt7_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT7-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fmt6_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                      //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT6-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            pnd_Fmt6_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT6-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-CREF-CONTRACTS
    }
    private void sub_Pnd_Process_Fund_Cref() throws Exception                                                                                                             //Natural: #PROCESS-FUND-CREF
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet1743 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = '0L' OR = '6L'
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("0L") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6L")))
        {
            decideConditionsMet1743++;
            pnd_Cref_Units_1.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_1.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '0M' OR = '0T' OR = '0U' OR = '6M' OR #I-CNT-NBR-2 = 'Z0' THRU 'Z9'
        else if (condition(((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("0M") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("0T")) || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("0U")) 
            || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6M")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("Z0") && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("Z9")))))
        {
            decideConditionsMet1743++;
            pnd_Cref_Units_2.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_2.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '0N' OR = '6N'
        else if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("0N") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6N")))
        {
            decideConditionsMet1743++;
            pnd_Cref_Units_3.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_3.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "CREF FUND ERROR  ","=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr);                                       //Natural: WRITE 'CREF FUND ERROR  ' '=' #I-TIAA-FUND-CDE '=' #I-CNT-NBR
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-CREF
    }
    private void sub_Pnd_Process_Pa_Contracts() throws Exception                                                                                                          //Natural: #PROCESS-PA-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Fmt3_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT3-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        pnd_Fmt5_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT5-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
        sub_Pnd_Get_Dollar_Amount();
        if (condition(Global.isEscape())) {return;}
        pnd_Fmt3_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT3-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        pnd_Fmt5_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT5-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        //*  ADDED 6/03
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BT")  //Natural: IF #I-CNT-CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BC' OR = '57BT'
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("03BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("QPLC") 
            || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BC") || pnd_Input_Record_Pnd_I_Cnt_Curr_Dist_Cde.equals("57BT")))
        {
            pnd_Fmt7_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                      //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT7-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            pnd_Fmt7_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT7-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fmt6_Units.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                      //Natural: ADD #I-TIAA-UNITS-CNT TO #FMT6-UNITS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
            pnd_Fmt6_Dollars.getValue(pnd_Cnt_Type_Sub,pnd_Fmt3_Mode_Sub).nadd(pnd_Generic_Dollar_Amount);                                                                //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #FMT6-DOLLARS ( #CNT-TYPE-SUB,#FMT3-MODE-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        //*  #PROCESS-CREF-CONTRACTS
    }
    private void sub_Pnd_Process_Pa_Select_Fund() throws Exception                                                                                                        //Natural: #PROCESS-PA-SELECT-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet1795 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = 'GW' OR = 'W0'
        if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GW") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0")))
        {
            decideConditionsMet1795++;
            pnd_Cref_Units_1.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_1.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'IA' OR = 'IB' OR = 'IC' OR = 'ID' OR = 'IE' OR = 'IF' OR = 'IG'
        else if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IA") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IB") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IC") 
            || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("ID") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IE") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IF") 
            || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("IG")))
        {
            decideConditionsMet1795++;
            pnd_Cref_Units_2.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_2.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = 'S0'
        else if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("S0")))
        {
            decideConditionsMet1795++;
            pnd_Cref_Units_3.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_3.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "PA SELECT FUND ERROR  ","=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde,"=",pnd_Input_Record_Pnd_I_Cnt_Nbr);                                  //Natural: WRITE 'PA SELECT FUND ERROR  ' '=' #I-TIAA-FUND-CDE '=' #I-CNT-NBR
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-PA-SELECT-FUND
    }
    //*  9/08
    private void sub_Pnd_Process_Fund_09_11() throws Exception                                                                                                            //Natural: #PROCESS-FUND-09-11
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        short decideConditionsMet1827 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CNT-NBR-2 = '6L' OR = 'GW' OR ( #I-CNT-NBR-2 = 'W0' AND ( #I-CNT-NBR-3-7 > 0 AND #I-CNT-NBR-3-7 < 25000 ) )
        if (condition(((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6L") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GW")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("W0") 
            && (pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.greater(getZero()) && pnd_Input_Record_Pnd_I_Cnt_Nbr_3_7.less(25000))))))
        {
            decideConditionsMet1827++;
            pnd_Cref_Units_1.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-1 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_1.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-1 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt1.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT1
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '6M' OR = 'GA' OR #I-CNT-NBR-2 = 'IA' THRU 'IG' OR #I-CNT-NBR-2 = 'Y0' THRU 'Y9'
        else if (condition((((pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6M") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("GA")) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("IA") 
            && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("IG"))) || (pnd_Input_Record_Pnd_I_Cnt_Nbr_2.greaterOrEqual("Y0") && pnd_Input_Record_Pnd_I_Cnt_Nbr_2.lessOrEqual("Y9")))))
        {
            decideConditionsMet1827++;
            pnd_Cref_Units_2.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-2 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_2.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-2 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            //*  ADDED 9/01 FOR OPT-21 REA
            pnd_Cnt2.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT2
        }                                                                                                                                                                 //Natural: WHEN #I-CNT-NBR-2 = '6N' OR = 'S0'
        else if (condition(pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("6N") || pnd_Input_Record_Pnd_I_Cnt_Nbr_2.equals("S0")))
        {
            decideConditionsMet1827++;
            pnd_Cref_Units_3.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                            //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-3 ( #MODE-1 )
                                                                                                                                                                          //Natural: PERFORM #GET-DOLLAR-AMOUNT
            sub_Pnd_Get_Dollar_Amount();
            if (condition(Global.isEscape())) {return;}
            pnd_Cref_Dollars_3.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                      //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-3 ( #MODE-1 )
            pnd_Cref_Units_Tot.getValue(pnd_Mode_1).nadd(pnd_Input_Record_Pnd_I_Tiaa_Units_Cnt);                                                                          //Natural: ADD #I-TIAA-UNITS-CNT TO #CREF-UNITS-TOT ( #MODE-1 )
            pnd_Cref_Dollars_Tot.getValue(pnd_Mode_1).nadd(pnd_Generic_Dollar_Amount);                                                                                    //Natural: ADD #GENERIC-DOLLAR-AMOUNT TO #CREF-DOLLARS-TOT ( #MODE-1 )
            pnd_Cnt3.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CNT3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, "FUND 09-11 ERROR  ","=",pnd_Input_Record_Pnd_I_Cnt_Nbr,"=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde);                                      //Natural: WRITE 'FUND 09-11 ERROR  ' '=' #I-CNT-NBR '=' #I-TIAA-FUND-CDE
            if (Global.isEscape()) return;
            pnd_Cnt_None.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-NONE
            //*      IGNORE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #PROCESS-FUND-09
    }
    private void sub_Pnd_Find_Mode_Sub() throws Exception                                                                                                                 //Natural: #FIND-MODE-SUB
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            if (condition(pnd_Pmt_Modes.getValue(pnd_I).equals(pnd_Input_Record_Pnd_I_Cnt_Mode_Ind)))                                                                     //Natural: IF #PMT-MODES ( #I ) = #I-CNT-MODE-IND
            {
                pnd_Mode_1.setValue(pnd_I);                                                                                                                               //Natural: ASSIGN #MODE-1 := #I
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #FIND-MODE-SUB
    }
    private void sub_Pnd_Setup_Mode_Table2() throws Exception                                                                                                             //Natural: #SETUP-MODE-TABLE2
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            pnd_Mode_Table_2.getValue(pnd_I).setValueEdited(pnd_Pmt_Modes.getValue(pnd_I),new ReportEditMask("999"));                                                     //Natural: MOVE EDITED #PMT-MODES ( #I ) ( EM = 999 ) TO #MODE-TABLE-2 ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #SETUP-MODE-TABLE2
    }
    private void sub_Pnd_Setup_Other_Tables() throws Exception                                                                                                            //Natural: #SETUP-OTHER-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Pay_Mode_Table.getValue(1).setValue(100);                                                                                                                     //Natural: MOVE 100 TO #PAY-MODE-TABLE ( 1 )
        short decideConditionsMet1878 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #MM;//Natural: VALUE 01
        if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(1))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(601);                                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(701);                                                                                                                 //Natural: MOVE 701 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(801);                                                                                                                 //Natural: MOVE 801 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(2))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(602);                                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(702);                                                                                                                 //Natural: MOVE 702 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(802);                                                                                                                 //Natural: MOVE 802 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 03
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(3))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(603);                                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(703);                                                                                                                 //Natural: MOVE 703 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(803);                                                                                                                 //Natural: MOVE 803 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 04
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(4))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(601);                                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(704);                                                                                                                 //Natural: MOVE 704 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(804);                                                                                                                 //Natural: MOVE 804 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 05
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(5))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(602);                                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(705);                                                                                                                 //Natural: MOVE 705 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(805);                                                                                                                 //Natural: MOVE 805 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 06
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(6))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(603);                                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(706);                                                                                                                 //Natural: MOVE 706 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(806);                                                                                                                 //Natural: MOVE 806 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 07
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(7))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(601);                                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(701);                                                                                                                 //Natural: MOVE 701 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(807);                                                                                                                 //Natural: MOVE 807 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 08
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(8))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(602);                                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(702);                                                                                                                 //Natural: MOVE 702 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(808);                                                                                                                 //Natural: MOVE 808 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 09
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(9))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(603);                                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(703);                                                                                                                 //Natural: MOVE 703 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(809);                                                                                                                 //Natural: MOVE 809 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(601);                                                                                                                 //Natural: MOVE 601 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(704);                                                                                                                 //Natural: MOVE 704 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(810);                                                                                                                 //Natural: MOVE 810 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 11
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(602);                                                                                                                 //Natural: MOVE 602 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(705);                                                                                                                 //Natural: MOVE 705 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(811);                                                                                                                 //Natural: MOVE 811 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 12
        else if (condition((pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))))
        {
            decideConditionsMet1878++;
            pnd_Pay_Mode_Table.getValue(2).setValue(603);                                                                                                                 //Natural: MOVE 603 TO #PAY-MODE-TABLE ( 2 )
            pnd_Pay_Mode_Table.getValue(3).setValue(706);                                                                                                                 //Natural: MOVE 706 TO #PAY-MODE-TABLE ( 3 )
            pnd_Pay_Mode_Table.getValue(4).setValue(812);                                                                                                                 //Natural: MOVE 812 TO #PAY-MODE-TABLE ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PA SELECT CHAGES
        DbsUtil.callnat(Iaan051e.class , getCurrentProcessState(), iaaa0510);                                                                                             //Natural: CALLNAT 'IAAN051E' IAAA0510
        if (condition(Global.isEscape())) return;
        FE:                                                                                                                                                               //Natural: FOR #G = 4 TO 80
        for (pnd_G.setValue(4); condition(pnd_G.lessOrEqual(80)); pnd_G.nadd(1))
        {
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "=",pnd_G,"=",pnd_Sub,"=",iaaa0510_Pnd_Cm_Std_Srt_Seq.getValue(pnd_G),"=",iaaa0510_Pnd_Ia_Std_Nm_Cd.getValue(pnd_G));               //Natural: WRITE '=' #G '=' #SUB '=' #CM-STD-SRT-SEQ ( #G ) '=' #IA-STD-NM-CD ( #G )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sub.setValue(iaaa0510_Pnd_Cm_Std_Srt_Seq.getValue(pnd_G));                                                                                                //Natural: MOVE #CM-STD-SRT-SEQ ( #G ) TO #SUB
            if (condition(pnd_Sub.equals(getZero())))                                                                                                                     //Natural: IF #SUB = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_U.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #U
            pnd_T_Fund_Code.getValue(pnd_Sub).setValue(iaaa0510_Pnd_Ia_Std_Nm_Cd.getValue(pnd_G));                                                                        //Natural: MOVE #IA-STD-NM-CD ( #G ) TO #T-FUND-CODE ( #SUB )
            //*  FE.
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "=",pnd_T_Fund_Code.getValue(1,":",20),"=",pnd_U);                                                                                      //Natural: WRITE '=' #T-FUND-CODE ( 1:20 ) '=' #U
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        F9:                                                                                                                                                               //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            pnd_Fund_Desc.reset();                                                                                                                                        //Natural: RESET #FUND-DESC #COMP-DESC
            pnd_Comp_Desc.reset();
            pnd_Fund_Code_2.setValue(pnd_T_Fund_Code.getValue(pnd_J));                                                                                                    //Natural: MOVE #T-FUND-CODE ( #J ) TO #FUND-CODE-2
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Fund_Code_2, pnd_Fund_Desc, pnd_Comp_Desc, pnd_Len);                                           //Natural: CALLNAT 'IAAN051A' #FUND-CODE-2 #FUND-DESC #COMP-DESC #LEN
            if (condition(Global.isEscape())) return;
            pnd_Table_Fund_Desc.getValue(pnd_J).setValue(pnd_Fund_Desc);                                                                                                  //Natural: MOVE #FUND-DESC TO #TABLE-FUND-DESC ( #J )
            pnd_Unit_Text_Table.getValue(pnd_J).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_Pnd_Fund_Desc_10, "UNITS"));                                       //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 'UNITS' TO #UNIT-TEXT-TABLE ( #J )
            pnd_Dollars_Text_Table.getValue(pnd_J).setValue(DbsUtil.compress(pnd_Blnk5, pnd_Fund_Desc_Pnd_Fund_Desc_10, "DOLLARS"));                                      //Natural: COMPRESS #BLNK5 #FUND-DESC-10 'DOLLARS' TO #DOLLARS-TEXT-TABLE ( #J )
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "=",pnd_Table_Fund_Desc.getValue(pnd_J));                                                                                           //Natural: WRITE '=' #TABLE-FUND-DESC ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Unit_Text_Table.getValue(pnd_J));                                                                                           //Natural: WRITE '=' #UNIT-TEXT-TABLE ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Dollars_Text_Table.getValue(pnd_J));                                                                                        //Natural: WRITE '=' #DOLLARS-TEXT-TABLE ( #J )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F9"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F9"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #SETUP-OTHER-TABLES
    }
    private void sub_Pnd_Write_Out_Report() throws Exception                                                                                                              //Natural: #WRITE-OUT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        if (condition(pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold_1.equals("1") && pnd_Fund_Cde_Hold_Tot_Pnd_Cmpny_Cde_Hold.equals("T")))                                     //Natural: IF #FUND-CDE-HOLD-1 = '1' AND #CMPNY-CDE-HOLD = 'T'
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TEACHER-FORMAT
            sub_Pnd_Write_Teacher_Format();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Print_Tpa_Ipro_Pi.equals(true)))                                                                                                            //Natural: IF #PRINT-TPA-IPRO-PI = TRUE
            {
                                                                                                                                                                          //Natural: PERFORM #WRITE-TIAA-FORMAT
                sub_Pnd_Write_Tiaa_Format();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-CREF-FORMAT
            sub_Pnd_Write_Cref_Format();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  #WRITE-OUT-REPORT
    }
    private void sub_Pnd_Write_Out_Tiaa_Totals() throws Exception                                                                                                         //Natural: #WRITE-OUT-TIAA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Tiaa_Title.setValue("TIAA GRAND TOTAL              ");                                                                                                        //Natural: MOVE 'TIAA GRAND TOTAL              ' TO #TIAA-TITLE
        //*  WRITE GRAND TOTAL FOR TIAA CONTRACTS
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        FQA:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1),   //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT-G ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-G ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-G ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT-G ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FQA"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FQA"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot_G.nadd(pnd_Tiaa_Table_Tot_G.getValue("*",1));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-G ( *,1 ) TO #TIAA-CNT-AMT-TOT-G
        pnd_Tiaa_Per_Div_Tot_G.nadd(pnd_Tiaa_Table_Tot_G.getValue("*",2));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-G ( *,2 ) TO #TIAA-PER-DIV-TOT-G
        pnd_Tiaa_Fin_Pay_Tot_G.nadd(pnd_Tiaa_Table_Tot_G.getValue("*",3));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-G ( *,3 ) TO #TIAA-FIN-PAY-TOT-G
        pnd_Tiaa_Fin_Div_Tot_G.nadd(pnd_Tiaa_Table_Tot_G.getValue("*",4));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-G ( *,4 ) TO #TIAA-FIN-DIV-TOT-G
        //*  CHANGED 9/01
        //*  CHANGED 9/01
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot_G, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT-G ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT-G ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 8X #TIAA-FIN-PAY-TOT-G ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT-G ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot_G, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),pnd_Tiaa_Fin_Pay_Tot_G, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot_G, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  #WRITE-OUT-TIAA-TOTALS
    }
    private void sub_Pnd_Write_Teacher_Format() throws Exception                                                                                                          //Natural: #WRITE-TEACHER-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  PAGE 1 & 3 9/04
        pnd_Tiaa_Title.setValue(pnd_Tiaa_Title_Tab.getValue(1));                                                                                                          //Natural: MOVE #TIAA-TITLE-TAB ( 1 ) TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F1A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            //*  TAM 01/2001
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_1.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-1 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-1 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-1 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-1 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_1.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_1.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_1.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_1.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-1 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_1.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-1 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_1.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-1 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_1.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-1 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        //*  WRITE TOTAL LINE FOR MODE REPORT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN PA ORGN 18,38,40 PAGE 3
        //*  ==========================================
        //*   WRITE TIAA NON-PENSION & TC LIFE FIXED
        //*  ==========================================
        //*  3 = COMPANY FUND T1S PAGE 3
        if (condition(pnd_Fund_Types.equals(3)))                                                                                                                          //Natural: IF #FUND-TYPES = 3
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION IA - PA MATURITY)        ");                                                                                        //Natural: MOVE 'TIAA NON-PENSION IA - PA MATURITY)        ' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P3:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_18_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-18-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-18-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-18-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-18-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_18_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_18_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_18_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_18_Tab.getValue(23,1).nadd(pnd_Org_18_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-18-TAB ( *,1 ) TO #ORG-18-TAB ( 23,1 )
            pnd_Org_18_Tab.getValue(23,2).nadd(pnd_Org_18_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-18-TAB ( *,2 ) TO #ORG-18-TAB ( 23,2 )
            pnd_Org_18_Tab.getValue(23,3).nadd(pnd_Org_18_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-18-TAB ( *,3 ) TO #ORG-18-TAB ( 23,3 )
            pnd_Org_18_Tab.getValue(23,4).nadd(pnd_Org_18_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-18-TAB ( *,4 ) TO #ORG-18-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_18_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-18-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-18-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-18-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-18-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_18_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_18_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_18_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_18_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-18-TAB ( 23,* )
            pnd_Tiaa_Title.setValue("TIAA-CREF LIFE IA - PA SELECT MATURITY) ");                                                                                          //Natural: MOVE 'TIAA-CREF LIFE IA - PA SELECT MATURITY) ' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P5:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_38_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-38-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-38-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-38-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-38-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_38_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_38_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_38_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_38_Tab.getValue(23,1).nadd(pnd_Org_38_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-38-TAB ( *,1 ) TO #ORG-38-TAB ( 23,1 )
            pnd_Org_38_Tab.getValue(23,2).nadd(pnd_Org_38_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-38-TAB ( *,2 ) TO #ORG-38-TAB ( 23,2 )
            pnd_Org_38_Tab.getValue(23,3).nadd(pnd_Org_38_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-38-TAB ( *,3 ) TO #ORG-38-TAB ( 23,3 )
            pnd_Org_38_Tab.getValue(23,4).nadd(pnd_Org_38_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-38-TAB ( *,4 ) TO #ORG-38-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_38_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-38-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-38-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-38-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-38-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_38_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_38_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_38_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_38_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-38-TAB ( 23,* )
            pnd_Tiaa_Title.setValue("TIAA-CREF LIFE IA  -  SPIA                ");                                                                                        //Natural: MOVE 'TIAA-CREF LIFE IA  -  SPIA                ' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P6:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_40_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-40-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-40-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-40-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-40-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_40_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_40_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_40_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P6"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P6"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_40_Tab.getValue(23,1).nadd(pnd_Org_40_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-40-TAB ( *,1 ) TO #ORG-40-TAB ( 23,1 )
            pnd_Org_40_Tab.getValue(23,2).nadd(pnd_Org_40_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-40-TAB ( *,2 ) TO #ORG-40-TAB ( 23,2 )
            pnd_Org_40_Tab.getValue(23,3).nadd(pnd_Org_40_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-40-TAB ( *,3 ) TO #ORG-40-TAB ( 23,3 )
            pnd_Org_40_Tab.getValue(23,4).nadd(pnd_Org_40_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-40-TAB ( *,4 ) TO #ORG-40-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_40_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-40-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-40-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-40-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-40-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_40_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_40_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_40_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_40_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-40-TAB ( 23,* )
            //*  END OF PAGE 3
        }                                                                                                                                                                 //Natural: END-IF
        //*  ===================================================================
        //*         PAGE 2 ,4 & 11
        pnd_Tiaa_Title.setValue(pnd_Tiaa_Title_Tab.getValue(2));                                                                                                          //Natural: MOVE #TIAA-TITLE-TAB ( 2 ) TO #TIAA-TITLE
        //*  PAGE 2 TIAA GRADED GW W0(1-24999) 6L ORIGIN 03 1G
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F2A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_2.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-2 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-2 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-2 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-2 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_2.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_2.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_2.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F2A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F2A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_2.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-2 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_2.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-2 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_2.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-2 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_2.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-2 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*  ==================================================
        //*   WRITE TIAA NON-PENSION & PA DEATH STANDARD PAGE 4
        //*  ==================================================
        //*  3 = COMPANY FUND T1S PAGE 4
        if (condition(pnd_Fund_Types.equals(3)))                                                                                                                          //Natural: IF #FUND-TYPES = 3
        {
            //*  10/2017 START
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION STANDARD SC ILC (TIAA INSURANCE)");                                                                                 //Natural: MOVE 'TIAA NON-PENSION STANDARD SC ILC (TIAA INSURANCE)' TO #TIAA-TITLE
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            P1:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_03_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-03-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_03_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_03_Tab.getValue(23,1).nadd(pnd_Org_03_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-03-TAB ( *,1 ) TO #ORG-03-TAB ( 23,1 )
            pnd_Org_03_Tab.getValue(23,2).nadd(pnd_Org_03_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-03-TAB ( *,2 ) TO #ORG-03-TAB ( 23,2 )
            pnd_Org_03_Tab.getValue(23,3).nadd(pnd_Org_03_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-03-TAB ( *,3 ) TO #ORG-03-TAB ( 23,3 )
            pnd_Org_03_Tab.getValue(23,4).nadd(pnd_Org_03_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-03-TAB ( *,4 ) TO #ORG-03-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_03_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-03-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_03_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-03-TAB ( 23,* )
            //*  10/2017 START
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION STANDARD SC ILC (TC-LIFE INSURANCE)");                                                                              //Natural: MOVE 'TIAA NON-PENSION STANDARD SC ILC (TC-LIFE INSURANCE)' TO #TIAA-TITLE
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            P1A:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_46_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-46-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_46_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P1A"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P1A"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_46_Tab.getValue(23,1).nadd(pnd_Org_46_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-46-TAB ( *,1 ) TO #ORG-46-TAB ( 23,1 )
            pnd_Org_46_Tab.getValue(23,2).nadd(pnd_Org_46_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-46-TAB ( *,2 ) TO #ORG-46-TAB ( 23,2 )
            pnd_Org_46_Tab.getValue(23,3).nadd(pnd_Org_46_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-46-TAB ( *,3 ) TO #ORG-46-TAB ( 23,3 )
            pnd_Org_46_Tab.getValue(23,4).nadd(pnd_Org_46_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-46-TAB ( *,4 ) TO #ORG-46-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_46_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-46-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_46_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-46-TAB ( 23,* )
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION SC ILC (PA DEATH)          ");                                                                                      //Natural: MOVE 'TIAA NON-PENSION SC ILC (PA DEATH)          ' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P2:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_17_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-17-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_17_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_17_Tab.getValue(23,1).nadd(pnd_Org_17_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-17-TAB ( *,1 ) TO #ORG-17-TAB ( 23,1 )
            pnd_Org_17_Tab.getValue(23,2).nadd(pnd_Org_17_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-17-TAB ( *,2 ) TO #ORG-17-TAB ( 23,2 )
            pnd_Org_17_Tab.getValue(23,3).nadd(pnd_Org_17_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-17-TAB ( *,3 ) TO #ORG-17-TAB ( 23,3 )
            pnd_Org_17_Tab.getValue(23,4).nadd(pnd_Org_17_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-17-TAB ( *,4 ) TO #ORG-17-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_17_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-17-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_17_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-17-TAB ( 23,* )
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Tiaa_Title.setValue("TIAA-CREF LIFE SC ILC (PAS DEATH)");                                                                                                 //Natural: MOVE 'TIAA-CREF LIFE SC ILC (PAS DEATH)' TO #TIAA-TITLE
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P4:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_37_Tab.getValue(pnd_I,1),     //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-37-TAB ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-37-TAB ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_37_Tab.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_37_Tab.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_37_Tab.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P4"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P4"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_37_Tab.getValue(23,1).nadd(pnd_Org_37_Tab.getValue("*",1));                                                                                           //Natural: ADD #ORG-37-TAB ( *,1 ) TO #ORG-37-TAB ( 23,1 )
            pnd_Org_37_Tab.getValue(23,2).nadd(pnd_Org_37_Tab.getValue("*",2));                                                                                           //Natural: ADD #ORG-37-TAB ( *,2 ) TO #ORG-37-TAB ( 23,2 )
            pnd_Org_37_Tab.getValue(23,3).nadd(pnd_Org_37_Tab.getValue("*",3));                                                                                           //Natural: ADD #ORG-37-TAB ( *,3 ) TO #ORG-37-TAB ( 23,3 )
            pnd_Org_37_Tab.getValue(23,4).nadd(pnd_Org_37_Tab.getValue("*",4));                                                                                           //Natural: ADD #ORG-37-TAB ( *,4 ) TO #ORG-37-TAB ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_37_Tab.getValue(23,1), new ReportEditMask           //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-37-TAB ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-37-TAB ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_37_Tab.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_37_Tab.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_37_Tab.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_37_Tab.getValue(23,"*").reset();                                                                                                                      //Natural: RESET #ORG-37-TAB ( 23,* )
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,37 PAGE 4
        //* ***********************************************************************
        //*  7/25/08
        if (condition(pnd_Fund_Types.equals(2) && pnd_Tiaa_Title_Tab.getValue(3).greater(" ")))                                                                           //Natural: IF #FUND-TYPES = 2 AND #TIAA-TITLE-TAB ( 3 ) GT ' '
        {
            pnd_Tiaa_Title.setValue(pnd_Tiaa_Title_Tab.getValue(3));                                                                                                      //Natural: MOVE #TIAA-TITLE-TAB ( 3 ) TO #TIAA-TITLE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            FXX:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_3.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-3 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-3 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-3 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-3 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_3.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Tiaa_Table_3.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_3.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FXX"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FXX"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",1));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,1 ) TO #TIAA-CNT-AMT-TOT
            pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",2));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,2 ) TO #TIAA-PER-DIV-TOT
            pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",3));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,3 ) TO #TIAA-FIN-PAY-TOT
            pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",4));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,4 ) TO #TIAA-FIN-DIV-TOT
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask                    //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                 //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
            pnd_Tiaa_Per_Div_Tot.reset();
            pnd_Tiaa_Per_Ded_Tot.reset();
            pnd_Tiaa_Fin_Pay_Tot.reset();
            pnd_Tiaa_Fin_Div_Tot.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/25/08 END
        //* ***********************************************************************
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN 3
        //*  ==========================================
        //*   WRITE TIAA NON-PENSION GRADED PAGE 2
        //*  ==========================================
        //*  2 = COMPANY FUND T1G GRADED PAGE 2
        if (condition(pnd_Fund_Types.equals(2) && pnd_Fund_Cde_Hold_Tot_Pnd_Fund_Cde_Hold.equals("1G")))                                                                  //Natural: IF #FUND-TYPES = 2 AND #FUND-CDE-HOLD = '1G'
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            //*  10/2017 START
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION GRADED SC ILC (TIAA INSURANCE)");                                                                                   //Natural: MOVE 'TIAA NON-PENSION GRADED SC ILC (TIAA INSURANCE)' TO #TIAA-TITLE
            //*  10/2017 END
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),pnd_Tiaa_Title);                                                                                //Natural: WRITE ( 1 ) 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            PG:                                                                                                                                                           //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_03_Tab_G.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-03-TAB-G ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-G ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-G ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-G ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_G.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_03_Tab_G.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_G.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_03_Tab_G.getValue(23,1).nadd(pnd_Org_03_Tab_G.getValue("*",1));                                                                                       //Natural: ADD #ORG-03-TAB-G ( *,1 ) TO #ORG-03-TAB-G ( 23,1 )
            pnd_Org_03_Tab_G.getValue(23,2).nadd(pnd_Org_03_Tab_G.getValue("*",2));                                                                                       //Natural: ADD #ORG-03-TAB-G ( *,2 ) TO #ORG-03-TAB-G ( 23,2 )
            pnd_Org_03_Tab_G.getValue(23,3).nadd(pnd_Org_03_Tab_G.getValue("*",3));                                                                                       //Natural: ADD #ORG-03-TAB-G ( *,3 ) TO #ORG-03-TAB-G ( 23,3 )
            pnd_Org_03_Tab_G.getValue(23,4).nadd(pnd_Org_03_Tab_G.getValue("*",4));                                                                                       //Natural: ADD #ORG-03-TAB-G ( *,4 ) TO #ORG-03-TAB-G ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_03_Tab_G.getValue(23,1), new ReportEditMask         //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-03-TAB-G ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-G ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-G ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-G ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_G.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_G.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_G.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_03_Tab_G.getValue(23,"*").reset();                                                                                                                    //Natural: RESET #ORG-03-TAB-G ( 23,* )
            //*  10/2017 START
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION GRADED SC ILC (TC-LIFE INSURANCE)");                                                                                //Natural: MOVE 'TIAA NON-PENSION GRADED SC ILC (TC-LIFE INSURANCE)' TO #TIAA-TITLE
            //*  10/2017 END
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),pnd_Tiaa_Title);                                                                                //Natural: WRITE ( 1 ) 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            PGA:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_46_Tab_G.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-46-TAB-G ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-G ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-G ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-G ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_G.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_46_Tab_G.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_G.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PGA"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PGA"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_46_Tab_G.getValue(23,1).nadd(pnd_Org_46_Tab_G.getValue("*",1));                                                                                       //Natural: ADD #ORG-46-TAB-G ( *,1 ) TO #ORG-46-TAB-G ( 23,1 )
            pnd_Org_46_Tab_G.getValue(23,2).nadd(pnd_Org_46_Tab_G.getValue("*",2));                                                                                       //Natural: ADD #ORG-46-TAB-G ( *,2 ) TO #ORG-46-TAB-G ( 23,2 )
            pnd_Org_46_Tab_G.getValue(23,3).nadd(pnd_Org_46_Tab_G.getValue("*",3));                                                                                       //Natural: ADD #ORG-46-TAB-G ( *,3 ) TO #ORG-46-TAB-G ( 23,3 )
            pnd_Org_46_Tab_G.getValue(23,4).nadd(pnd_Org_46_Tab_G.getValue("*",4));                                                                                       //Natural: ADD #ORG-46-TAB-G ( *,4 ) TO #ORG-46-TAB-G ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_46_Tab_G.getValue(23,1), new ReportEditMask         //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-46-TAB-G ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-G ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-G ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-G ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_G.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_G.getValue(23,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_G.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_46_Tab_G.getValue(23,"*").reset();                                                                                                                    //Natural: RESET #ORG-46-TAB-G ( 23,* )
        }                                                                                                                                                                 //Natural: END-IF
        //*  10/2017 END
        //*  END OF ADD 9/04 ACCUMULATE ORGN 3 GRADED PAGE 2
        //*  ==========================================
        //*   WRITE TIAA STANDARD PAGE 5
        //*  ==========================================
        //*  3 = COMPANY FUND T1S PAGE 5
        if (condition(pnd_Fund_Types.equals(3)))                                                                                                                          //Natural: IF #FUND-TYPES = 3
        {
            pnd_Tiaa_Title.setValue(pnd_Tiaa_Title_Tab.getValue(3));                                                                                                      //Natural: MOVE #TIAA-TITLE-TAB ( 3 ) TO #TIAA-TITLE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            F3A:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_3.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-3 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-3 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-3 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-3 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_3.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Tiaa_Table_3.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_3.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3A"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3A"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",1));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,1 ) TO #TIAA-CNT-AMT-TOT
            pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",2));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,2 ) TO #TIAA-PER-DIV-TOT
            pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",3));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,3 ) TO #TIAA-FIN-PAY-TOT
            pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_3.getValue("*",4));                                                                                                  //Natural: ADD #TIAA-TABLE-3 ( *,4 ) TO #TIAA-FIN-DIV-TOT
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask                    //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                 //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
            pnd_Tiaa_Per_Div_Tot.reset();
            pnd_Tiaa_Per_Ded_Tot.reset();
            pnd_Tiaa_Fin_Pay_Tot.reset();
            pnd_Tiaa_Fin_Div_Tot.reset();
            //*   S0 CONTRACTS NOT OPT 22 STD.
            //*  PAGE 5
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            //*  10/2017
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION - (TIAA INSURANCE)");                                                                                               //Natural: MOVE 'TIAA NON-PENSION - (TIAA INSURANCE)' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P4A:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_03_Tab_Std_S0.getValue(pnd_I,1),  //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-03-TAB-STD-S0 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-STD-S0 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-STD-S0 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-STD-S0 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_Std_S0.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_03_Tab_Std_S0.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_Std_S0.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P4A"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P4A"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_03_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_03_Tab_Std_S0.getValue("*",1));                                                                             //Natural: ADD #ORG-03-TAB-STD-S0 ( *,1 ) TO #ORG-03-TAB-STD-S0 ( 23,1 )
            pnd_Org_03_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_03_Tab_Std_S0.getValue("*",2));                                                                             //Natural: ADD #ORG-03-TAB-STD-S0 ( *,2 ) TO #ORG-03-TAB-STD-S0 ( 23,2 )
            pnd_Org_03_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_03_Tab_Std_S0.getValue("*",3));                                                                             //Natural: ADD #ORG-03-TAB-STD-S0 ( *,3 ) TO #ORG-03-TAB-STD-S0 ( 23,3 )
            pnd_Org_03_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_03_Tab_Std_S0.getValue("*",4));                                                                             //Natural: ADD #ORG-03-TAB-STD-S0 ( *,4 ) TO #ORG-03-TAB-STD-S0 ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_03_Tab_Std_S0.getValue(23,1), new                   //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-03-TAB-STD-S0 ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-STD-S0 ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-STD-S0 ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-STD-S0 ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_Std_S0.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_03_Tab_Std_S0.getValue(23,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_Std_S0.getValue(23,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_03_Tab_Std_S0.getValue(23,"*").reset();                                                                                                               //Natural: RESET #ORG-03-TAB-STD-S0 ( 23,* )
            //*  10/2017 START
            //*  PAGE 5
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            //* *MOVE 'TIAA NON-PENSION - (TC-LIFE INSURANCE)'
            pnd_Tiaa_Title.setValue("TC-LIFE INSURANCE AC");                                                                                                              //Natural: MOVE 'TC-LIFE INSURANCE AC' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P46:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_46_Tab_Std_S0.getValue(pnd_I,1),  //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-46-TAB-STD-S0 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-STD-S0 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-STD-S0 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-STD-S0 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_Std_S0.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_46_Tab_Std_S0.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_Std_S0.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P46"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P46"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_46_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_46_Tab_Std_S0.getValue("*",1));                                                                             //Natural: ADD #ORG-46-TAB-STD-S0 ( *,1 ) TO #ORG-46-TAB-STD-S0 ( 23,1 )
            pnd_Org_46_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_46_Tab_Std_S0.getValue("*",2));                                                                             //Natural: ADD #ORG-46-TAB-STD-S0 ( *,2 ) TO #ORG-46-TAB-STD-S0 ( 23,2 )
            pnd_Org_46_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_46_Tab_Std_S0.getValue("*",3));                                                                             //Natural: ADD #ORG-46-TAB-STD-S0 ( *,3 ) TO #ORG-46-TAB-STD-S0 ( 23,3 )
            pnd_Org_46_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_46_Tab_Std_S0.getValue("*",4));                                                                             //Natural: ADD #ORG-46-TAB-STD-S0 ( *,4 ) TO #ORG-46-TAB-STD-S0 ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_46_Tab_Std_S0.getValue(23,1), new                   //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-46-TAB-STD-S0 ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-STD-S0 ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-STD-S0 ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-STD-S0 ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_Std_S0.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_46_Tab_Std_S0.getValue(23,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_Std_S0.getValue(23,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_46_Tab_Std_S0.getValue(23,"*").reset();                                                                                                               //Natural: RESET #ORG-46-TAB-STD-S0 ( 23,* )
            //*  10/2017 END
            //*  PAGE 5
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION (PA DEATH)");                                                                                                       //Natural: MOVE 'TIAA NON-PENSION (PA DEATH)' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            P4B:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_17_Tab_Std_S0.getValue(pnd_I,1),  //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-17-TAB-STD-S0 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-STD-S0 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-STD-S0 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-STD-S0 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_Std_S0.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_17_Tab_Std_S0.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_Std_S0.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("P4B"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("P4B"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_17_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_17_Tab_Std_S0.getValue("*",1));                                                                             //Natural: ADD #ORG-17-TAB-STD-S0 ( *,1 ) TO #ORG-17-TAB-STD-S0 ( 23,1 )
            pnd_Org_17_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_17_Tab_Std_S0.getValue("*",2));                                                                             //Natural: ADD #ORG-17-TAB-STD-S0 ( *,2 ) TO #ORG-17-TAB-STD-S0 ( 23,2 )
            pnd_Org_17_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_17_Tab_Std_S0.getValue("*",3));                                                                             //Natural: ADD #ORG-17-TAB-STD-S0 ( *,3 ) TO #ORG-17-TAB-STD-S0 ( 23,3 )
            pnd_Org_17_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_17_Tab_Std_S0.getValue("*",4));                                                                             //Natural: ADD #ORG-17-TAB-STD-S0 ( *,4 ) TO #ORG-17-TAB-STD-S0 ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_17_Tab_Std_S0.getValue(23,1), new                   //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-17-TAB-STD-S0 ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-STD-S0 ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-STD-S0 ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-STD-S0 ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_Std_S0.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_17_Tab_Std_S0.getValue(23,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_Std_S0.getValue(23,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_17_Tab_Std_S0.getValue(23,"*").reset();                                                                                                               //Natural: RESET #ORG-17-TAB-STD-S0 ( 23,* )
            //*  PAGE 5
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                      //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
            pnd_Tiaa_Title.setValue("TIAA NON-PENSION (PAS DEATH)");                                                                                                      //Natural: MOVE 'TIAA NON-PENSION (PAS DEATH)' TO #TIAA-TITLE
            getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                   //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
            if (Global.isEscape()) return;
            PPGE5:                                                                                                                                                        //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_37_Tab_Std_S0.getValue(pnd_I,1),  //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-37-TAB-STD-S0 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB-STD-S0 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB-STD-S0 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-37-TAB-STD-S0 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_37_Tab_Std_S0.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                    ColumnSpacing(11),pnd_Org_37_Tab_Std_S0.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_37_Tab_Std_S0.getValue(pnd_I,4), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PPGE5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PPGE5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Org_37_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_37_Tab_Std_S0.getValue("*",1));                                                                             //Natural: ADD #ORG-37-TAB-STD-S0 ( *,1 ) TO #ORG-37-TAB-STD-S0 ( 23,1 )
            pnd_Org_37_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_37_Tab_Std_S0.getValue("*",2));                                                                             //Natural: ADD #ORG-37-TAB-STD-S0 ( *,2 ) TO #ORG-37-TAB-STD-S0 ( 23,2 )
            pnd_Org_37_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_37_Tab_Std_S0.getValue("*",3));                                                                             //Natural: ADD #ORG-37-TAB-STD-S0 ( *,3 ) TO #ORG-37-TAB-STD-S0 ( 23,3 )
            pnd_Org_37_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_37_Tab_Std_S0.getValue("*",4));                                                                             //Natural: ADD #ORG-37-TAB-STD-S0 ( *,4 ) TO #ORG-37-TAB-STD-S0 ( 23,4 )
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_37_Tab_Std_S0.getValue(23,1), new                   //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-37-TAB-STD-S0 ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB-STD-S0 ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-37-TAB-STD-S0 ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-37-TAB-STD-S0 ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_37_Tab_Std_S0.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_37_Tab_Std_S0.getValue(23,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_37_Tab_Std_S0.getValue(23,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Org_37_Tab_Std_S0.getValue(23,"*").reset();                                                                                                               //Natural: RESET #ORG-37-TAB-STD-S0 ( 23,* )
            //*  END OF PAGE 5
        }                                                                                                                                                                 //Natural: END-IF
        //*  =============================
        //*   WRITE TIAA TOTAL REPORT (8)
        //*  =============================
        pnd_Tiaa_Title.setValue(pnd_Tiaa_Title_Tab.getValue(4));                                                                                                          //Natural: MOVE #TIAA-TITLE-TAB ( 4 ) TO #TIAA-TITLE
        //*  WRITE MODES 100 THRU 812
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F4A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot.getValue(pnd_I,1),     //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F4A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F4A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_Tot.getValue("*",1));                                                                                                    //Natural: ADD #TIAA-TABLE-TOT ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_Tot.getValue("*",2));                                                                                                    //Natural: ADD #TIAA-TABLE-TOT ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_Tot.getValue("*",3));                                                                                                    //Natural: ADD #TIAA-TABLE-TOT ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_Tot.getValue("*",4));                                                                                                    //Natural: ADD #TIAA-TABLE-TOT ( *,4 ) TO #TIAA-FIN-DIV-TOT
        //*  WRITE TOTAL LINE FOR MODES 100 THRU 812
        //*  WRITE TOTAL LINE
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  ==============
        //*   RESET VALUES
        //*  ==============
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT #TIAA-TABLE-1 ( *,* ) #TIAA-TABLE-2 ( *,* ) #TIAA-TABLE-3 ( *,* ) #TIAA-TABLE-TOT ( *,* )
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        pnd_Tiaa_Table_1.getValue("*","*").reset();
        pnd_Tiaa_Table_2.getValue("*","*").reset();
        pnd_Tiaa_Table_3.getValue("*","*").reset();
        pnd_Tiaa_Table_Tot.getValue("*","*").reset();
        //*  #WRITE-TEACHER-FORMAT
    }
    private void sub_Pnd_Write_Tiaa_Format() throws Exception                                                                                                             //Natural: #WRITE-TIAA-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        //* * TAM 02/2001 >>
        //*  FOR TPA OPTION CODE  = 28 OR 30
        pnd_Tiaa_Title.setValue("TIAA PENSION TPA");                                                                                                                      //Natural: MOVE 'TIAA PENSION TPA' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F7A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_4.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-4 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-4 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-4 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-4 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_4.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_4.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_4.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F7A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F7A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Tiaa_Table_4.getValue(pnd_I,1));                                                                              //Natural: ADD #TIAA-TABLE-4 ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Tiaa_Table_4.getValue(pnd_I,2));                                                                              //Natural: ADD #TIAA-TABLE-4 ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Tiaa_Table_4.getValue(pnd_I,3));                                                                              //Natural: ADD #TIAA-TABLE-4 ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Tiaa_Table_4.getValue(pnd_I,4));                                                                              //Natural: ADD #TIAA-TABLE-4 ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_4.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-4 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_4.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-4 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_4.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-4 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_4.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-4 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*  FOR IPRO OPTION CODE = 25 OR 27
        pnd_Tiaa_Title.setValue("TIAA PENSION IPRO");                                                                                                                     //Natural: MOVE 'TIAA PENSION IPRO' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F8A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            //* CHANGED 9/01
            //*  CHANGE 10/23/2001
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_5.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-5 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-5 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-5 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-5 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_5.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_5.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_5.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F8A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F8A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Tiaa_Table_5.getValue(pnd_I,1));                                                                              //Natural: ADD #TIAA-TABLE-5 ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Tiaa_Table_5.getValue(pnd_I,2));                                                                              //Natural: ADD #TIAA-TABLE-5 ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Tiaa_Table_5.getValue(pnd_I,3));                                                                              //Natural: ADD #TIAA-TABLE-5 ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Tiaa_Table_5.getValue(pnd_I,4));                                                                              //Natural: ADD #TIAA-TABLE-5 ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_5.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-5 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_5.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-5 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_5.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-5 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_5.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-5 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        //*  CHANGED 9/01
        //*  1/23/2001
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 17X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(17),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*  ======================================================================
        //* * FOR P/I OPTION CODE = 22 AND PREFIX SO PAGE 8
        pnd_Tiaa_Title.setValue("TIAA P&I/S0");                                                                                                                           //Natural: MOVE 'TIAA P&I/S0' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F9A:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_6.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-6 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-6 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-6 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-6 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_6.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_6.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_6.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F9A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F9A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Tiaa_Table_6.getValue(pnd_I,1));                                                                              //Natural: ADD #TIAA-TABLE-6 ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Tiaa_Table_6.getValue(pnd_I,2));                                                                              //Natural: ADD #TIAA-TABLE-6 ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Tiaa_Table_6.getValue(pnd_I,3));                                                                              //Natural: ADD #TIAA-TABLE-6 ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Tiaa_Table_6.getValue(pnd_I,4));                                                                              //Natural: ADD #TIAA-TABLE-6 ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_6.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-6 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_6.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-6 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_6.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-6 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_6.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-6 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*   S0 CONTRACTS OPT 22 ADDED FOLLOWING 9/04
        //*  PAGE 8
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        //*  10/2017
        pnd_Tiaa_Title.setValue("TIAA NON-PENSION TIAA INSURANCE");                                                                                                       //Natural: MOVE 'TIAA NON-PENSION TIAA INSURANCE' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        P4C:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-03-TAB-S0-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-S0-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-S0-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-S0-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("P4C"))) break;
                else if (condition(Global.isEscapeBottomImmediate("P4C"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,1));                                                                          //Natural: ADD #ORG-03-TAB-S0-PI ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,2));                                                                          //Natural: ADD #ORG-03-TAB-S0-PI ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,3));                                                                          //Natural: ADD #ORG-03-TAB-S0-PI ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Org_03_Tab_S0_Pi.getValue(pnd_I,4));                                                                          //Natural: ADD #ORG-03-TAB-S0-PI ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_03_Tab_S0_Pi.getValue(23,1).nadd(pnd_Org_03_Tab_S0_Pi.getValue("*",1));                                                                                   //Natural: ADD #ORG-03-TAB-S0-PI ( *,1 ) TO #ORG-03-TAB-S0-PI ( 23,1 )
        pnd_Org_03_Tab_S0_Pi.getValue(23,2).nadd(pnd_Org_03_Tab_S0_Pi.getValue("*",2));                                                                                   //Natural: ADD #ORG-03-TAB-S0-PI ( *,2 ) TO #ORG-03-TAB-S0-PI ( 23,2 )
        pnd_Org_03_Tab_S0_Pi.getValue(23,3).nadd(pnd_Org_03_Tab_S0_Pi.getValue("*",3));                                                                                   //Natural: ADD #ORG-03-TAB-S0-PI ( *,3 ) TO #ORG-03-TAB-S0-PI ( 23,3 )
        pnd_Org_03_Tab_S0_Pi.getValue(23,4).nadd(pnd_Org_03_Tab_S0_Pi.getValue("*",4));                                                                                   //Natural: ADD #ORG-03-TAB-S0-PI ( *,4 ) TO #ORG-03-TAB-S0-PI ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_03_Tab_S0_Pi.getValue(23,1), new ReportEditMask         //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-03-TAB-S0-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-S0-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-S0-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-S0-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_S0_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_S0_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_S0_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_03_Tab_S0_Pi.getValue(23,"*").reset();                                                                                                                    //Natural: RESET #ORG-03-TAB-S0-PI ( 23,* )
        //*  10/2017 START
        //*  PAGE 8
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        //* *MOVE 'TIAA NON-PENSION TC-LIFE INSURANCE' TO #TIAA-TITLE
        pnd_Tiaa_Title.setValue("TCLIFE - INSURANCE P&I");                                                                                                                //Natural: MOVE 'TCLIFE - INSURANCE P&I' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        P4C46:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-46-TAB-S0-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-S0-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-S0-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-S0-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("P4C46"))) break;
                else if (condition(Global.isEscapeBottomImmediate("P4C46"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,1));                                                                          //Natural: ADD #ORG-46-TAB-S0-PI ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,2));                                                                          //Natural: ADD #ORG-46-TAB-S0-PI ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,3));                                                                          //Natural: ADD #ORG-46-TAB-S0-PI ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Org_46_Tab_S0_Pi.getValue(pnd_I,4));                                                                          //Natural: ADD #ORG-46-TAB-S0-PI ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_46_Tab_S0_Pi.getValue(23,1).nadd(pnd_Org_46_Tab_S0_Pi.getValue("*",1));                                                                                   //Natural: ADD #ORG-46-TAB-S0-PI ( *,1 ) TO #ORG-46-TAB-S0-PI ( 23,1 )
        pnd_Org_46_Tab_S0_Pi.getValue(23,2).nadd(pnd_Org_46_Tab_S0_Pi.getValue("*",2));                                                                                   //Natural: ADD #ORG-46-TAB-S0-PI ( *,2 ) TO #ORG-46-TAB-S0-PI ( 23,2 )
        pnd_Org_46_Tab_S0_Pi.getValue(23,3).nadd(pnd_Org_46_Tab_S0_Pi.getValue("*",3));                                                                                   //Natural: ADD #ORG-46-TAB-S0-PI ( *,3 ) TO #ORG-46-TAB-S0-PI ( 23,3 )
        pnd_Org_46_Tab_S0_Pi.getValue(23,4).nadd(pnd_Org_46_Tab_S0_Pi.getValue("*",4));                                                                                   //Natural: ADD #ORG-46-TAB-S0-PI ( *,4 ) TO #ORG-46-TAB-S0-PI ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_46_Tab_S0_Pi.getValue(23,1), new ReportEditMask         //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-46-TAB-S0-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-S0-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-S0-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-S0-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_S0_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_S0_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_S0_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_46_Tab_S0_Pi.getValue(23,"*").reset();                                                                                                                    //Natural: RESET #ORG-46-TAB-S0-PI ( 23,* )
        //*  10/2017 END
        //*  PAGE 8
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        //*  ADDED 9/04
        pnd_Tiaa_Title.setValue("TIAA NON-PENSION (PA DEATH)");                                                                                                           //Natural: MOVE 'TIAA NON-PENSION (PA DEATH)' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        PPGE8:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,1),   //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-17-TAB-S0-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-S0-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-S0-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-S0-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PPGE8"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PPGE8"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,1));                                                                          //Natural: ADD #ORG-17-TAB-S0-PI ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,2));                                                                          //Natural: ADD #ORG-17-TAB-S0-PI ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,3));                                                                          //Natural: ADD #ORG-17-TAB-S0-PI ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Org_17_Tab_S0_Pi.getValue(pnd_I,4));                                                                          //Natural: ADD #ORG-17-TAB-S0-PI ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_17_Tab_S0_Pi.getValue(23,1).nadd(pnd_Org_17_Tab_S0_Pi.getValue("*",1));                                                                                   //Natural: ADD #ORG-17-TAB-S0-PI ( *,1 ) TO #ORG-17-TAB-S0-PI ( 23,1 )
        pnd_Org_17_Tab_S0_Pi.getValue(23,2).nadd(pnd_Org_17_Tab_S0_Pi.getValue("*",2));                                                                                   //Natural: ADD #ORG-17-TAB-S0-PI ( *,2 ) TO #ORG-17-TAB-S0-PI ( 23,2 )
        pnd_Org_17_Tab_S0_Pi.getValue(23,3).nadd(pnd_Org_17_Tab_S0_Pi.getValue("*",3));                                                                                   //Natural: ADD #ORG-17-TAB-S0-PI ( *,3 ) TO #ORG-17-TAB-S0-PI ( 23,3 )
        pnd_Org_17_Tab_S0_Pi.getValue(23,4).nadd(pnd_Org_17_Tab_S0_Pi.getValue("*",4));                                                                                   //Natural: ADD #ORG-17-TAB-S0-PI ( *,4 ) TO #ORG-17-TAB-S0-PI ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_17_Tab_S0_Pi.getValue(23,1), new ReportEditMask         //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-17-TAB-S0-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-S0-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-S0-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-S0-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_S0_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_S0_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_S0_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_17_Tab_S0_Pi.getValue(23,"*").reset();                                                                                                                    //Natural: RESET #ORG-17-TAB-S0-PI ( 23,* )
        //*  END OF ADD 9/04
        pnd_Org_37_Tab_S0_Pi.getValue(23,1).nadd(pnd_Org_37_Tab_S0_Pi.getValue("*",1));                                                                                   //Natural: ADD #ORG-37-TAB-S0-PI ( *,1 ) TO #ORG-37-TAB-S0-PI ( 23,1 )
        pnd_Org_37_Tab_S0_Pi.getValue(23,2).nadd(pnd_Org_37_Tab_S0_Pi.getValue("*",2));                                                                                   //Natural: ADD #ORG-37-TAB-S0-PI ( *,2 ) TO #ORG-37-TAB-S0-PI ( 23,2 )
        //* * FOR P&I OPTION CODE = 22 AND PREFIX IA - IF PAGE 9
        pnd_Tiaa_Title.setValue("TIAA P&I NON-S0 ");                                                                                                                      //Natural: MOVE 'TIAA P&I NON-S0 ' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        F15A:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_7.getValue(pnd_I,1),       //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-7 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-7 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-7 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-7 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_7.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_7.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_7.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F15A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F15A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     FOR GRAND TOTAL PAGE
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,1).nadd(pnd_Tiaa_Table_7.getValue(pnd_I,1));                                                                              //Natural: ADD #TIAA-TABLE-7 ( #I,1 ) TO #TIAA-TABLE-TOT-G ( #I,1 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,2).nadd(pnd_Tiaa_Table_7.getValue(pnd_I,2));                                                                              //Natural: ADD #TIAA-TABLE-7 ( #I,2 ) TO #TIAA-TABLE-TOT-G ( #I,2 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,3).nadd(pnd_Tiaa_Table_7.getValue(pnd_I,3));                                                                              //Natural: ADD #TIAA-TABLE-7 ( #I,3 ) TO #TIAA-TABLE-TOT-G ( #I,3 )
            pnd_Tiaa_Table_Tot_G.getValue(pnd_I,4).nadd(pnd_Tiaa_Table_7.getValue(pnd_I,4));                                                                              //Natural: ADD #TIAA-TABLE-7 ( #I,4 ) TO #TIAA-TABLE-TOT-G ( #I,4 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.nadd(pnd_Tiaa_Table_7.getValue("*",1));                                                                                                      //Natural: ADD #TIAA-TABLE-7 ( *,1 ) TO #TIAA-CNT-AMT-TOT
        pnd_Tiaa_Per_Div_Tot.nadd(pnd_Tiaa_Table_7.getValue("*",2));                                                                                                      //Natural: ADD #TIAA-TABLE-7 ( *,2 ) TO #TIAA-PER-DIV-TOT
        pnd_Tiaa_Fin_Pay_Tot.nadd(pnd_Tiaa_Table_7.getValue("*",3));                                                                                                      //Natural: ADD #TIAA-TABLE-7 ( *,3 ) TO #TIAA-FIN-PAY-TOT
        pnd_Tiaa_Fin_Div_Tot.nadd(pnd_Tiaa_Table_7.getValue("*",4));                                                                                                      //Natural: ADD #TIAA-TABLE-7 ( *,4 ) TO #TIAA-FIN-DIV-TOT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        //*   P&I CONTRACTS NOT PREFIX S0
        //*  PAGE 9
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        //*  10/2017
        pnd_Tiaa_Title.setValue("TIAA NON-PENSION TIAA INSURANCE");                                                                                                       //Natural: MOVE 'TIAA NON-PENSION TIAA INSURANCE' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        PGE9:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_03_Tab_Pi.getValue(pnd_I,1),      //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-03-TAB-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_03_Tab_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PGE9"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PGE9"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_03_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_03_Tab_Pi.getValue("*",1));                                                                                     //Natural: ADD #ORG-03-TAB-PI ( *,1 ) TO #ORG-03-TAB-STD-S0 ( 23,1 )
        pnd_Org_03_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_03_Tab_Pi.getValue("*",2));                                                                                     //Natural: ADD #ORG-03-TAB-PI ( *,2 ) TO #ORG-03-TAB-STD-S0 ( 23,2 )
        pnd_Org_03_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_03_Tab_Pi.getValue("*",3));                                                                                     //Natural: ADD #ORG-03-TAB-PI ( *,3 ) TO #ORG-03-TAB-STD-S0 ( 23,3 )
        pnd_Org_03_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_03_Tab_Pi.getValue("*",4));                                                                                     //Natural: ADD #ORG-03-TAB-PI ( *,4 ) TO #ORG-03-TAB-STD-S0 ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_03_Tab_Pi.getValue(23,1), new ReportEditMask            //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-03-TAB-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-03-TAB-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-03-TAB-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_03_Tab_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_03_Tab_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_03_Tab_Pi.getValue(23,"*").reset();                                                                                                                       //Natural: RESET #ORG-03-TAB-PI ( 23,* )
        //*  10/2017 START
        //*  PAGE 9
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        //* *MOVE 'TIAA NON-PENSION TC-LIFE INSURANCE' TO #TIAA-TITLE
        pnd_Tiaa_Title.setValue("TIAA NON-PENSION - INSURANCE");                                                                                                          //Natural: MOVE 'TIAA NON-PENSION - INSURANCE' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        PGE9_46:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_46_Tab_Pi.getValue(pnd_I,1),      //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-46-TAB-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_46_Tab_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PGE9_46"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PGE9_46"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_46_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_46_Tab_Pi.getValue("*",1));                                                                                     //Natural: ADD #ORG-46-TAB-PI ( *,1 ) TO #ORG-46-TAB-STD-S0 ( 23,1 )
        pnd_Org_46_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_46_Tab_Pi.getValue("*",2));                                                                                     //Natural: ADD #ORG-46-TAB-PI ( *,2 ) TO #ORG-46-TAB-STD-S0 ( 23,2 )
        pnd_Org_46_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_46_Tab_Pi.getValue("*",3));                                                                                     //Natural: ADD #ORG-46-TAB-PI ( *,3 ) TO #ORG-46-TAB-STD-S0 ( 23,3 )
        pnd_Org_46_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_46_Tab_Pi.getValue("*",4));                                                                                     //Natural: ADD #ORG-46-TAB-PI ( *,4 ) TO #ORG-46-TAB-STD-S0 ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_46_Tab_Pi.getValue(23,1), new ReportEditMask            //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-46-TAB-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-46-TAB-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-46-TAB-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_46_Tab_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_46_Tab_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_46_Tab_Pi.getValue(23,"*").reset();                                                                                                                       //Natural: RESET #ORG-46-TAB-PI ( 23,* )
        //*  10/2017 END
        //*  PAGE 9
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        pnd_Page_Num.setValue(getReports().getPageNumberDbs(1));                                                                                                          //Natural: ASSIGN #PAGE-NUM := *PAGE-NUMBER ( 1 )
        pnd_Tiaa_Title.setValue("TIAA NON-PENSION (PA DEATH)");                                                                                                           //Natural: MOVE 'TIAA NON-PENSION (PA DEATH)' TO #TIAA-TITLE
        getReports().write(1, ReportOption.NOTITLE,"Continue Page",pnd_Page_Num,new TabSetting(40),pnd_Tiaa_Title);                                                       //Natural: WRITE ( 1 ) 'Continue Page' #PAGE-NUM 40T #TIAA-TITLE
        if (Global.isEscape()) return;
        PGE9B:                                                                                                                                                            //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Org_17_Tab_Pi.getValue(pnd_I,1),      //Natural: WRITE ( 1 ) 3T #MODE-TABLE-2 ( #I ) 10X #ORG-17-TAB-PI ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-PI ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-PI ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-PI ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_Pi.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Org_17_Tab_Pi.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_Pi.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PGE9B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PGE9B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Org_17_Tab_Std_S0.getValue(23,1).nadd(pnd_Org_17_Tab_Pi.getValue("*",1));                                                                                     //Natural: ADD #ORG-17-TAB-PI ( *,1 ) TO #ORG-17-TAB-STD-S0 ( 23,1 )
        pnd_Org_17_Tab_Std_S0.getValue(23,2).nadd(pnd_Org_17_Tab_Pi.getValue("*",2));                                                                                     //Natural: ADD #ORG-17-TAB-PI ( *,2 ) TO #ORG-17-TAB-STD-S0 ( 23,2 )
        pnd_Org_17_Tab_Std_S0.getValue(23,3).nadd(pnd_Org_17_Tab_Pi.getValue("*",3));                                                                                     //Natural: ADD #ORG-17-TAB-PI ( *,3 ) TO #ORG-17-TAB-STD-S0 ( 23,3 )
        pnd_Org_17_Tab_Std_S0.getValue(23,4).nadd(pnd_Org_17_Tab_Pi.getValue("*",4));                                                                                     //Natural: ADD #ORG-17-TAB-PI ( *,4 ) TO #ORG-17-TAB-STD-S0 ( 23,4 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Org_17_Tab_Pi.getValue(23,1), new ReportEditMask            //Natural: WRITE ( 1 ) / 3T 'TOTAL' 8X #ORG-17-TAB-PI ( 23,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-PI ( 23,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #ORG-17-TAB-PI ( 23,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #ORG-17-TAB-PI ( 23,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_Pi.getValue(23,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Org_17_Tab_Pi.getValue(23,3), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Org_17_Tab_Pi.getValue(23,4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Org_17_Tab_Pi.getValue(23,"*").reset();                                                                                                                       //Natural: RESET #ORG-17-TAB-PI ( 23,* )
        //*  END OF PAGE 9
        //*  ======================================================================
        //*  FOR TPA OPTION CODE = 28 OR 30
        pnd_Tiaa_Title.setValue("TIAA TPA TOTAL          ");                                                                                                              //Natural: MOVE 'TIAA TPA TOTAL          ' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F10A:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot_4.getValue(pnd_I,1),   //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT-4 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-4 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-4 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT-4 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot_4.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot_4.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot_4.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F10A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F10A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot_4.nadd(pnd_Tiaa_Table_Tot_4.getValue("*",1));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-4 ( *,1 ) TO #TIAA-CNT-AMT-TOT-4
        pnd_Tiaa_Per_Div_Tot_4.nadd(pnd_Tiaa_Table_Tot_4.getValue("*",2));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-4 ( *,2 ) TO #TIAA-PER-DIV-TOT-4
        pnd_Tiaa_Fin_Pay_Tot_4.nadd(pnd_Tiaa_Table_Tot_4.getValue("*",3));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-4 ( *,3 ) TO #TIAA-FIN-PAY-TOT-4
        pnd_Tiaa_Fin_Div_Tot_4.nadd(pnd_Tiaa_Table_Tot_4.getValue("*",4));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-4 ( *,4 ) TO #TIAA-FIN-DIV-TOT-4
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot_4, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT-4 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT-4 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT-4 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT-4 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot_4, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot_4, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot_4, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FOR IPRO OPTION CODE = 25 OR 27
        pnd_Tiaa_Title.setValue("TIAA IPRO TOTAL         ");                                                                                                              //Natural: MOVE 'TIAA IPRO TOTAL         ' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F11A:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            //*  CHANGED 9/01
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot_5.getValue(pnd_I,1),   //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT-5 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-5 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-5 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT-5 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot_5.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot_5.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot_5.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F11A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F11A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot_5.nadd(pnd_Tiaa_Table_Tot_5.getValue("*",1));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-5 ( *,1 ) TO #TIAA-CNT-AMT-TOT-5
        pnd_Tiaa_Per_Div_Tot_5.nadd(pnd_Tiaa_Table_Tot_5.getValue("*",2));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-5 ( *,2 ) TO #TIAA-PER-DIV-TOT-5
        pnd_Tiaa_Fin_Pay_Tot_5.nadd(pnd_Tiaa_Table_Tot_5.getValue("*",3));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-5 ( *,3 ) TO #TIAA-FIN-PAY-TOT-5
        pnd_Tiaa_Fin_Div_Tot_5.nadd(pnd_Tiaa_Table_Tot_5.getValue("*",4));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-5 ( *,4 ) TO #TIAA-FIN-DIV-TOT-5
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot_5, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT-5 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT-5 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT-5 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT-5 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot_5, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot_5, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot_5, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FOR P&I OPTION CODE = 22 AND PREFIX S0
        pnd_Tiaa_Title.setValue("TIAA P&I S0 TOTAL       ");                                                                                                              //Natural: MOVE 'TIAA P&I S0 TOTAL       ' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F12A:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot_6.getValue(pnd_I,1),   //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT-6 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-6 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-6 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT-6 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot_6.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot_6.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot_6.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F12A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F12A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tiaa_Cnt_Amt_Tot_6.nadd(pnd_Tiaa_Table_Tot_6.getValue("*",1));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-6 ( *,1 ) TO #TIAA-CNT-AMT-TOT-6
        pnd_Tiaa_Per_Div_Tot_6.nadd(pnd_Tiaa_Table_Tot_6.getValue("*",2));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-6 ( *,2 ) TO #TIAA-PER-DIV-TOT-6
        pnd_Tiaa_Fin_Pay_Tot_6.nadd(pnd_Tiaa_Table_Tot_6.getValue("*",3));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-6 ( *,3 ) TO #TIAA-FIN-PAY-TOT-6
        pnd_Tiaa_Fin_Div_Tot_6.nadd(pnd_Tiaa_Table_Tot_6.getValue("*",4));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-6 ( *,4 ) TO #TIAA-FIN-DIV-TOT-6
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(8),pnd_Tiaa_Cnt_Amt_Tot_6, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 8X #TIAA-CNT-AMT-TOT-6 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT-6 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT-6 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT-6 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot_6, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot_6, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot_6, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FOR P&I OPTION CODE = 22 AND PREFIX IA - IF
        pnd_Tiaa_Title.setValue("TIAA P&I NON-S0 TOTAL   ");                                                                                                              //Natural: MOVE 'TIAA P&I NON-S0 TOTAL   ' TO #TIAA-TITLE
        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        F16A:                                                                                                                                                             //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(10),pnd_Tiaa_Table_Tot_7.getValue(pnd_I,1),   //Natural: WRITE ( 8 ) 3T #MODE-TABLE-2 ( #I ) 10X #TIAA-TABLE-TOT-7 ( #I,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-7 ( #I,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-TABLE-TOT-7 ( #I,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-TABLE-TOT-7 ( #I,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Table_Tot_7.getValue(pnd_I,2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(11),pnd_Tiaa_Table_Tot_7.getValue(pnd_I,3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Table_Tot_7.getValue(pnd_I,4), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F16A"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F16A"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CHANGED FROM 6
        pnd_Tiaa_Cnt_Amt_Tot_7.nadd(pnd_Tiaa_Table_Tot_7.getValue("*",1));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-7 ( *,1 ) TO #TIAA-CNT-AMT-TOT-7
        pnd_Tiaa_Per_Div_Tot_7.nadd(pnd_Tiaa_Table_Tot_7.getValue("*",2));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-7 ( *,2 ) TO #TIAA-PER-DIV-TOT-7
        pnd_Tiaa_Fin_Pay_Tot_7.nadd(pnd_Tiaa_Table_Tot_7.getValue("*",3));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-7 ( *,3 ) TO #TIAA-FIN-PAY-TOT-7
        pnd_Tiaa_Fin_Div_Tot_7.nadd(pnd_Tiaa_Table_Tot_7.getValue("*",4));                                                                                                //Natural: ADD #TIAA-TABLE-TOT-7 ( *,4 ) TO #TIAA-FIN-DIV-TOT-7
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(9),pnd_Tiaa_Cnt_Amt_Tot_7, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 8 ) / 3T 'TOTAL' 9X #TIAA-CNT-AMT-TOT-7 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-PER-DIV-TOT-7 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 11X #TIAA-FIN-PAY-TOT-7 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 14X #TIAA-FIN-DIV-TOT-7 ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(11),pnd_Tiaa_Per_Div_Tot_7, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(11),pnd_Tiaa_Fin_Pay_Tot_7, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(14),pnd_Tiaa_Fin_Div_Tot_7, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  ==============
        //*   RESET VALUES
        //*  ==============
        pnd_Tiaa_Cnt_Amt_Tot.reset();                                                                                                                                     //Natural: RESET #TIAA-CNT-AMT-TOT #TIAA-PER-DIV-TOT #TIAA-PER-DED-TOT #TIAA-FIN-PAY-TOT #TIAA-FIN-DIV-TOT #TIAA-TABLE-4 ( *,* ) #TIAA-TABLE-5 ( *,* ) #TIAA-TABLE-6 ( *,* ) #TIAA-TABLE-TOT-4 ( *,* ) #TIAA-TABLE-TOT-5 ( *,* ) #TIAA-TABLE-TOT-6 ( *,* )
        pnd_Tiaa_Per_Div_Tot.reset();
        pnd_Tiaa_Per_Ded_Tot.reset();
        pnd_Tiaa_Fin_Pay_Tot.reset();
        pnd_Tiaa_Fin_Div_Tot.reset();
        pnd_Tiaa_Table_4.getValue("*","*").reset();
        pnd_Tiaa_Table_5.getValue("*","*").reset();
        pnd_Tiaa_Table_6.getValue("*","*").reset();
        pnd_Tiaa_Table_Tot_4.getValue("*","*").reset();
        pnd_Tiaa_Table_Tot_5.getValue("*","*").reset();
        pnd_Tiaa_Table_Tot_6.getValue("*","*").reset();
    }
    private void sub_Pnd_Write_Totals_Reports() throws Exception                                                                                                          //Natural: #WRITE-TOTALS-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(3, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 3 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(3, 2);                                                                                                                                          //Natural: SKIP ( 3 ) 2
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Fmt3_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT3-TIAA-TABLE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'TIAA CONTRACTUAL AMOUNT ' 6X #FMT3-TIAA-TABLE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Fmt3_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT3-TIAA-TABLE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(12),pnd_Fmt3_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND ' 12X #FMT3-TIAA-TABLE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        FY:                                                                                                                                                               //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(3, 1);                                                                                                                                      //Natural: SKIP ( 3 ) 1
            pnd_Fmt3_Units_Tot.nadd(pnd_Fmt3_Units.getValue(pnd_Y,"*"));                                                                                                  //Natural: ADD #FMT3-UNITS ( #Y,* ) TO #FMT3-UNITS-TOT
            getReports().write(3, ReportOption.NOTITLE,pnd_Unit_Text_Table.getValue(pnd_Y),new ColumnSpacing(6),pnd_Fmt3_Units.getValue(pnd_Y,1), new                     //Natural: WRITE ( 3 ) #UNIT-TEXT-TABLE ( #Y ) 6X #FMT3-UNITS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 6X #FMT3-UNITS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 6X #FMT3-UNITS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 6X #FMT3-UNITS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 6X #FMT3-UNITS-TOT ( EM = ZZZ,ZZZ,ZZ9.999 )
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),pnd_Fmt3_Units.getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),pnd_Fmt3_Units.getValue(pnd_Y,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),pnd_Fmt3_Units.getValue(pnd_Y,4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),pnd_Fmt3_Units_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt3_Units_Tot.reset();                                                                                                                                   //Natural: RESET #FMT3-UNITS-TOT
            pnd_Fmt3_Dollars_Tot.nadd(pnd_Fmt3_Dollars.getValue(pnd_Y,"*"));                                                                                              //Natural: ADD #FMT3-DOLLARS ( #Y,* ) TO #FMT3-DOLLARS-TOT
            getReports().write(3, ReportOption.NOTITLE,pnd_Dollars_Text_Table.getValue(pnd_Y),new ColumnSpacing(6),pnd_Fmt3_Dollars.getValue(pnd_Y,1),                    //Natural: WRITE ( 3 ) #DOLLARS-TEXT-TABLE ( #Y ) 6X #FMT3-DOLLARS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-DOLLARS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-DOLLARS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-DOLLARS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-DOLLARS-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Dollars.getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(6),pnd_Fmt3_Dollars.getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Dollars.getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Dollars_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt3_Dollars_Tot.reset();                                                                                                                                 //Natural: RESET #FMT3-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING 9/04 ACCUMULATE ORGN 3 & PA ORGN 17,18,37,38,40
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().write(3, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 3 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_03_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-03-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 03 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_03_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 03 CONTRACTUAL AMOUNT ' 6X #ORG-03-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_03_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_03_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_03_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_03_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-03-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_03_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-03-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-03-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_03_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_03_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_03_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,".");                                                                                                                  //Natural: WRITE ( 3 ) '.'
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        //*  10/2017 START
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_46_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-46-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 46 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_46_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 46 CONTRACTUAL AMOUNT ' 6X #ORG-46-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_46_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_46_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_46_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_46_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-46-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_46_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-46-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-46-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_46_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_46_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_46_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,".");                                                                                                                  //Natural: WRITE ( 3 ) '.'
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        //*  10/2017 END
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_17_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-17-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 17 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_17_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 17 CONTRACTUAL AMOUNT ' 6X #ORG-17-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_17_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_17_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_17_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_17_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-17-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_17_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-17-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-17-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_17_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_17_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_17_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,".");                                                                                                                  //Natural: WRITE ( 3 ) '.'
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_18_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-18-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 18 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_18_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 18 CONTRACTUAL AMOUNT ' 6X #ORG-18-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_18_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_18_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_18_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_18_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-18-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_18_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-18-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-18-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_18_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_18_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_18_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,".");                                                                                                                  //Natural: WRITE ( 3 ) '.'
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_37_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-37-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 37 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_37_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 37 CONTRACTUAL AMOUNT ' 6X #ORG-37-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_37_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_37_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_37_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_37_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-37-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_37_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-37-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-37-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_37_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_37_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_37_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_38_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-38-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 38 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_38_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 38 CONTRACTUAL AMOUNT ' 6X #ORG-38-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_38_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_38_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_38_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_38_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-38-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_38_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-38-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-38-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_38_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_38_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_38_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,".");                                                                                                                  //Natural: WRITE ( 3 ) '.'
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_40_Due.getValue(1,"*"));                                                                                                     //Natural: ADD #ORG-40-DUE ( 1,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"ORIGIN 40 CONTRACTUAL AMOUNT ",new ColumnSpacing(6),pnd_Org_40_Due.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 3 ) 'ORIGIN 40 CONTRACTUAL AMOUNT ' 6X #ORG-40-DUE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_40_Due.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_40_Due.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_40_Due.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt3_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT3-TIAA-TABLE-TOT
        pnd_Fmt3_Tiaa_Table_Tot.nadd(pnd_Org_40_Due.getValue(2,"*"));                                                                                                     //Natural: ADD #ORG-40-DUE ( 2,* ) TO #FMT3-TIAA-TABLE-TOT
        getReports().write(3, ReportOption.NOTITLE,"PERIODIC DIVIDEND       ",new ColumnSpacing(11),pnd_Org_40_Due.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 3 ) 'PERIODIC DIVIDEND       ' 11X #ORG-40-DUE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #ORG-40-DUE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 6X #FMT3-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Org_40_Due.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_40_Due.getValue(2,3), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Org_40_Due.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Fmt3_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS
    }
    private void sub_Pnd_Write_Totals_Reports_5() throws Exception                                                                                                        //Natural: #WRITE-TOTALS-REPORTS-5
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(5, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 5 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(5, 2);                                                                                                                                          //Natural: SKIP ( 5 ) 2
        pnd_Fmt5_Tiaa_Table_Tot.nadd(pnd_Fmt5_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT5-TIAA-TABLE ( 1,* ) TO #FMT5-TIAA-TABLE-TOT
        getReports().write(5, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 5 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT5-TIAA-TABLE ( 1,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 1,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 1,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 1,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt5_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT5-TIAA-TABLE-TOT
        pnd_Fmt5_Tiaa_Table_Tot.nadd(pnd_Fmt5_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT5-TIAA-TABLE ( 2,* ) TO #FMT5-TIAA-TABLE-TOT
        getReports().write(5, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt5_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 5 ) 'PERIODIC DIVIDEND ' 13X #FMT5-TIAA-TABLE ( 2,1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 2,2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 2,3 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE ( 2,4 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-TIAA-TABLE-TOT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt5_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT5-TIAA-TABLE-TOT
        FY5:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1
            pnd_Fmt5_Units_Tot.nadd(pnd_Fmt5_Units.getValue(pnd_Y,"*"));                                                                                                  //Natural: ADD #FMT5-UNITS ( #Y,* ) TO #FMT5-UNITS-TOT
            getReports().write(5, ReportOption.NOTITLE,pnd_Unit_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt5_Units.getValue(pnd_Y,1), new                     //Natural: WRITE ( 5 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT5-UNITS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X #FMT5-UNITS-TOT ( EM = ZZZ,ZZZ,ZZ9.999 )
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt5_Units.getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt5_Units.getValue(pnd_Y,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt5_Units.getValue(pnd_Y,4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt5_Units_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY5"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY5"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt5_Units_Tot.reset();                                                                                                                                   //Natural: RESET #FMT5-UNITS-TOT
            pnd_Fmt5_Dollars_Tot.nadd(pnd_Fmt5_Dollars.getValue(pnd_Y,"*"));                                                                                              //Natural: ADD #FMT5-DOLLARS ( #Y,* ) TO #FMT5-DOLLARS-TOT
            getReports().write(5, ReportOption.NOTITLE,pnd_Dollars_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt5_Dollars.getValue(pnd_Y,1),                    //Natural: WRITE ( 5 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT5-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT5-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Dollars.getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),pnd_Fmt5_Dollars.getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Dollars.getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt5_Dollars_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY5"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY5"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt5_Dollars_Tot.reset();                                                                                                                                 //Natural: RESET #FMT5-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS-5
    }
    private void sub_Pnd_Write_Totals_Reports_6() throws Exception                                                                                                        //Natural: #WRITE-TOTALS-REPORTS-6
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        getReports().write(6, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(29),pnd_Pay_Mode_Table.getValue(1),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(2),new  //Natural: WRITE ( 6 ) 'PAYMENT MODE' 29X #PAY-MODE-TABLE ( 1 ) 17X #PAY-MODE-TABLE ( 2 ) 17X #PAY-MODE-TABLE ( 3 ) 17X #PAY-MODE-TABLE ( 4 ) 16X 'TOTAL'
            ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(3),new ColumnSpacing(17),pnd_Pay_Mode_Table.getValue(4),new ColumnSpacing(16),"TOTAL");
        if (Global.isEscape()) return;
        getReports().skip(6, 2);                                                                                                                                          //Natural: SKIP ( 6 ) 2
        pnd_Fmt6_Tiaa_Table_Tot.nadd(pnd_Fmt6_Tiaa_Table.getValue(1,"*"));                                                                                                //Natural: ADD #FMT6-TIAA-TABLE ( 1,* ) TO #FMT6-TIAA-TABLE-TOT
        getReports().write(6, ReportOption.NOTITLE,"TIAA CONTRACTUAL AMOUNT ",new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(1,1), new ReportEditMask                  //Natural: WRITE ( 6 ) 'TIAA CONTRACTUAL AMOUNT ' 7X #FMT6-TIAA-TABLE ( 1,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 1,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 1,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 1,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(1,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(1,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(1,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt6_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT6-TIAA-TABLE-TOT
        pnd_Fmt6_Tiaa_Table_Tot.nadd(pnd_Fmt6_Tiaa_Table.getValue(2,"*"));                                                                                                //Natural: ADD #FMT6-TIAA-TABLE ( 2,* ) TO #FMT6-TIAA-TABLE-TOT
        getReports().write(6, ReportOption.NOTITLE,"PERIODIC DIVIDEND ",new ColumnSpacing(13),pnd_Fmt6_Tiaa_Table.getValue(2,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( 6 ) 'PERIODIC DIVIDEND ' 13X #FMT6-TIAA-TABLE ( 2,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 2,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 2,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE ( 2,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-TIAA-TABLE-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(2,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(2,3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table.getValue(2,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Tiaa_Table_Tot, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Fmt6_Tiaa_Table_Tot.reset();                                                                                                                                  //Natural: RESET #FMT6-TIAA-TABLE-TOT
        FY6:                                                                                                                                                              //Natural: FOR #Y = 1 TO #U
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_U)); pnd_Y.nadd(1))
        {
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1
            pnd_Fmt6_Units_Tot.nadd(pnd_Fmt6_Units.getValue(pnd_Y,"*"));                                                                                                  //Natural: ADD #FMT6-UNITS ( #Y,* ) TO #FMT6-UNITS-TOT
            getReports().write(6, ReportOption.NOTITLE,pnd_Unit_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt6_Units.getValue(pnd_Y,1), new                     //Natural: WRITE ( 6 ) #UNIT-TEXT-TABLE ( #Y ) 7X #FMT6-UNITS ( #Y,1 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,2 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,3 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS ( #Y,4 ) ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #FMT6-UNITS-TOT ( EM = ZZ,ZZZ,ZZ9.999 )
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt6_Units.getValue(pnd_Y,2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt6_Units.getValue(pnd_Y,3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt6_Units.getValue(pnd_Y,4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pnd_Fmt6_Units_Tot, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY6"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY6"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt6_Units_Tot.reset();                                                                                                                                   //Natural: RESET #FMT6-UNITS-TOT
            pnd_Fmt6_Dollars_Tot.nadd(pnd_Fmt6_Dollars.getValue(pnd_Y,"*"));                                                                                              //Natural: ADD #FMT6-DOLLARS ( #Y,* ) TO #FMT6-DOLLARS-TOT
            getReports().write(6, ReportOption.NOTITLE,pnd_Dollars_Text_Table.getValue(pnd_Y),new ColumnSpacing(7),pnd_Fmt6_Dollars.getValue(pnd_Y,1),                    //Natural: WRITE ( 6 ) #DOLLARS-TEXT-TABLE ( #Y ) 7X #FMT6-DOLLARS ( #Y,1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,3 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS ( #Y,4 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #FMT6-DOLLARS-TOT ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Dollars.getValue(pnd_Y,2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(7),pnd_Fmt6_Dollars.getValue(pnd_Y,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Dollars.getValue(pnd_Y,4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Fmt6_Dollars_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FY6"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FY6"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fmt6_Dollars_Tot.reset();                                                                                                                                 //Natural: RESET #FMT6-DOLLARS-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  #WRITE-TOTALS-REPORTS-6
    }
    private void sub_Pnd_Write_Cref_Format() throws Exception                                                                                                             //Natural: #WRITE-CREF-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        //*  WRITE 'WRITE CREF FORMAT'
        pnd_Cref_Title.setValue(pnd_Cref_Title_Tab.getValue(1));                                                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 1 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        F1B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(16),pnd_Cref_Units_1.getValue(pnd_I),         //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-1 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-1 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),pnd_Cref_Dollars_1.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.nadd(pnd_Cref_Units_1.getValue("*"));                                                                                                        //Natural: ADD #CREF-UNITS-1 ( * ) TO #CREF-UNITS-TOTAL
        pnd_Cref_Dollars_Total.nadd(pnd_Cref_Dollars_1.getValue("*"));                                                                                                    //Natural: ADD #CREF-DOLLARS-1 ( * ) TO #CREF-DOLLARS-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),pnd_Cref_Units_Total, new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new  //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(17),pnd_Cref_Dollars_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.reset();                                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
        pnd_Cref_Dollars_Total.reset();
        pnd_Cref_Title.setValue(pnd_Cref_Title_Tab.getValue(2));                                                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 2 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        F2B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(16),pnd_Cref_Units_2.getValue(pnd_I),         //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-2 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-2 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),pnd_Cref_Dollars_2.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F2B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F2B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.nadd(pnd_Cref_Units_2.getValue("*"));                                                                                                        //Natural: ADD #CREF-UNITS-2 ( * ) TO #CREF-UNITS-TOTAL
        pnd_Cref_Dollars_Total.nadd(pnd_Cref_Dollars_2.getValue("*"));                                                                                                    //Natural: ADD #CREF-DOLLARS-2 ( * ) TO #CREF-DOLLARS-TOTAL
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),pnd_Cref_Units_Total, new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new  //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(17),pnd_Cref_Dollars_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.reset();                                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
        pnd_Cref_Dollars_Total.reset();
        if (condition(pnd_Fund_Types.equals(3)))                                                                                                                          //Natural: IF #FUND-TYPES = 3
        {
            pnd_Cref_Title.setValue(pnd_Cref_Title_Tab.getValue(3));                                                                                                      //Natural: MOVE #CREF-TITLE-TAB ( 3 ) TO #CREF-TITLE
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            F3B:                                                                                                                                                          //Natural: FOR #I = 1 TO 22
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
            {
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(16),pnd_Cref_Units_3.getValue(pnd_I),     //Natural: WRITE ( 2 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-3 ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-3 ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),pnd_Cref_Dollars_3.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F3B"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F3B"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cref_Units_Total.nadd(pnd_Cref_Units_3.getValue("*"));                                                                                                    //Natural: ADD #CREF-UNITS-3 ( * ) TO #CREF-UNITS-TOTAL
            pnd_Cref_Dollars_Total.nadd(pnd_Cref_Dollars_3.getValue("*"));                                                                                                //Natural: ADD #CREF-DOLLARS-3 ( * ) TO #CREF-DOLLARS-TOTAL
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),pnd_Cref_Units_Total, new ReportEditMask                   //Natural: WRITE ( 2 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),pnd_Cref_Dollars_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Cref_Units_Total.reset();                                                                                                                                 //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL
            pnd_Cref_Dollars_Total.reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  =============================
        //*   WRITE CREF TOTAL REPORT (9)
        //*  =============================
        pnd_Cref_Title.setValue(pnd_Cref_Title_Tab.getValue(4));                                                                                                          //Natural: MOVE #CREF-TITLE-TAB ( 4 ) TO #CREF-TITLE
        getReports().newPage(new ReportSpecification(9));                                                                                                                 //Natural: NEWPAGE ( 9 )
        if (condition(Global.isEscape())){return;}
        F4B:                                                                                                                                                              //Natural: FOR #I = 1 TO 22
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(22)); pnd_I.nadd(1))
        {
            getReports().write(9, ReportOption.NOTITLE,new TabSetting(3),pnd_Mode_Table_2.getValue(pnd_I),new ColumnSpacing(16),pnd_Cref_Units_Tot.getValue(pnd_I),       //Natural: WRITE ( 9 ) 3T #MODE-TABLE-2 ( #I ) 16X #CREF-UNITS-TOT ( #I ) ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(17),pnd_Cref_Dollars_Tot.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F4B"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F4B"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.nadd(pnd_Cref_Units_Tot.getValue("*"));                                                                                                      //Natural: ADD #CREF-UNITS-TOT ( * ) TO #CREF-UNITS-TOTAL
        pnd_Cref_Dollars_Total.nadd(pnd_Cref_Dollars_Tot.getValue("*"));                                                                                                  //Natural: ADD #CREF-DOLLARS-TOT ( * ) TO #CREF-DOLLARS-TOTAL
        getReports().write(9, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"TOTAL",new ColumnSpacing(14),pnd_Cref_Units_Total, new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new  //Natural: WRITE ( 9 ) / 3T 'TOTAL' 14X #CREF-UNITS-TOTAL ( EM = ZZ,ZZZ,ZZ9.999 ) 17X #CREF-DOLLARS-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(17),pnd_Cref_Dollars_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Cref_Units_Total.reset();                                                                                                                                     //Natural: RESET #CREF-UNITS-TOTAL #CREF-DOLLARS-TOTAL #CREF-UNITS-1 ( * ) #CREF-UNITS-2 ( * ) #CREF-UNITS-3 ( * ) #CREF-DOLLARS-1 ( * ) #CREF-DOLLARS-2 ( * ) #CREF-DOLLARS-3 ( * ) #CREF-DEDUCT-1 ( * ) #CREF-DEDUCT-2 ( * ) #CREF-DEDUCT-3 ( * ) #CREF-UNITS-TOT ( * ) #CREF-DOLLARS-TOT ( * ) #CREF-DEDUCT-TOT ( * )
        pnd_Cref_Dollars_Total.reset();
        pnd_Cref_Units_1.getValue("*").reset();
        pnd_Cref_Units_2.getValue("*").reset();
        pnd_Cref_Units_3.getValue("*").reset();
        pnd_Cref_Dollars_1.getValue("*").reset();
        pnd_Cref_Dollars_2.getValue("*").reset();
        pnd_Cref_Dollars_3.getValue("*").reset();
        pnd_Cref_Deduct_1.getValue("*").reset();
        pnd_Cref_Deduct_2.getValue("*").reset();
        pnd_Cref_Deduct_3.getValue("*").reset();
        pnd_Cref_Units_Tot.getValue("*").reset();
        pnd_Cref_Dollars_Tot.getValue("*").reset();
        pnd_Cref_Deduct_Tot.getValue("*").reset();
        //*  #WRITE-CREF-FORMAT
    }
    private void sub_Pnd_Initialization() throws Exception                                                                                                                //Natural: #INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().read(3, pnd_W_Rec_3);                                                                                                                              //Natural: READ WORK FILE 3 ONCE #W-REC-3
        pnd_Save_Check_Dte_A.setValue(pnd_W_Rec_3_Pnd_W_Rec_3_A);                                                                                                         //Natural: MOVE #W-REC-3-A TO #SAVE-CHECK-DTE-A
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, pnd_Save_Check_Dte_A,pnd_W_Rec_3_Pnd_W_Rec_3_A);                                                                                        //Natural: WRITE #SAVE-CHECK-DTE-A #W-REC-3-A
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm.setValue(pnd_Save_Check_Dte_A_Pnd_Mm);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-MM := #MM
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd.setValue(pnd_Save_Check_Dte_A_Pnd_Dd);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-DD := #DD
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy.setValue(pnd_Save_Check_Dte_A_Pnd_Yyyy);                                                                                 //Natural: ASSIGN #PAYMENT-DUE-YYYY := #YYYY
        pnd_Payment_Due_Dte_Pnd_Slash1.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH1 := '/'
        pnd_Payment_Due_Dte_Pnd_Slash2.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH2 := '/'
        //*  #INITIALIZATION
    }
    private void sub_Pnd_Get_Dollar_Amount() throws Exception                                                                                                             //Natural: #GET-DOLLAR-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Generic_Unit_Value.reset();                                                                                                                                   //Natural: RESET #GENERIC-UNIT-VALUE
        pnd_Generic_Dollar_Amount.setValue(pnd_Input_Record_Pnd_I_Tiaa_Tot_Pay_Amt);                                                                                      //Natural: ASSIGN #GENERIC-DOLLAR-AMOUNT := #I-TIAA-TOT-PAY-AMT
        //*   #GET-DOLLAR-AMOUNT
    }
    private void sub_Pnd_Check_Payment_Mode() throws Exception                                                                                                            //Natural: #CHECK-PAYMENT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Payment_Due.setValue(false);                                                                                                                                  //Natural: ASSIGN #PAYMENT-DUE := FALSE
        short decideConditionsMet2876 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #I-CNT-MODE-IND;//Natural: VALUE 100
        if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(100))))
        {
            decideConditionsMet2876++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(601))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))) //Natural: IF #MM = 01 OR = 04 OR = 07 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(602))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))) //Natural: IF #MM = 02 OR = 05 OR = 08 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(603))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))) //Natural: IF #MM = 03 OR = 06 OR = 09 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(701))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                //Natural: IF #MM = 01 OR = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(702))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                //Natural: IF #MM = 02 OR = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(703))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                //Natural: IF #MM = 03 OR = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(704))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                               //Natural: IF #MM = 04 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(705))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                               //Natural: IF #MM = 05 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(706))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                               //Natural: IF #MM = 06 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(801))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1)))                                                                                                         //Natural: IF #MM = 01
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(802))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2)))                                                                                                         //Natural: IF #MM = 02
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(803))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3)))                                                                                                         //Natural: IF #MM = 03
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(804))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4)))                                                                                                         //Natural: IF #MM = 04
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(805))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5)))                                                                                                         //Natural: IF #MM = 05
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(806))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6)))                                                                                                         //Natural: IF #MM = 06
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(807))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                                                         //Natural: IF #MM = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(808))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                                                         //Natural: IF #MM = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(809))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                                                         //Natural: IF #MM = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(810))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                                                                        //Natural: IF #MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(811))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                                                                        //Natural: IF #MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Input_Record_Pnd_I_Cnt_Mode_Ind.equals(812))))
        {
            decideConditionsMet2876++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                                                                        //Natural: IF #MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  #CHECK-PAYMENT-MODE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 1 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(1));
                    //*  CHANGED
                    //*  62T TO 30T 9/04
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(40),pnd_Tiaa_Title);         //Natural: WRITE ( 1 ) '   DATE ' *DATX ( EM = MM/DD/YYYY ) 40T #TIAA-TITLE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    //*  TAM 01/2001
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(21),"CONTRACTUAL ",new TabSetting(53),"PERIODIC",new TabSetting(82)," FINAL",new            //Natural: WRITE ( 1 ) 021T 'CONTRACTUAL ' 053T 'PERIODIC' 082T ' FINAL' 112T '  FINAL'
                        TabSetting(112),"  FINAL");
                    //*  TAM
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(21),"  AMOUNT    ",new TabSetting(53),"DIVIDEND",new               //Natural: WRITE ( 1 ) 003T 'MODE' 021T '  AMOUNT    ' 053T 'DIVIDEND' 082T 'PAYMENT' 112T 'DIVIDEND'
                        TabSetting(82),"PAYMENT",new TabSetting(112),"DIVIDEND");
                    //*  MODE
                    //*  CONTRACTUAL AMOUNT
                    //*  10/23/2001
                    //*  10/23/2001
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(19),"--------------",new TabSetting(47),"--------------",new       //Natural: WRITE ( 1 ) 003T '----' 019T '--------------' 047T '--------------' 075T '--------------' 106T '--------------'
                        TabSetting(75),"--------------",new TabSetting(106),"--------------");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    //*  PAGE 1
                    //*  PAGE 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) NOTITLE ' '
                    getReports().write(2, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 2 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 2 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(2));
                    getReports().write(2, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),pnd_Cref_Title);         //Natural: WRITE ( 2 ) '   DATE ' *DATX ( EM = MM/DD/YYYY ) 62T #CREF-TITLE
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(54),"  TIAA/CREF");                                      //Natural: WRITE ( 2 ) 023T '  TIAA/CREF  ' 054T '  TIAA/CREF'
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(55),"  DOLLARS ");              //Natural: WRITE ( 2 ) 003T 'MODE' 024T '   UNITS     ' 055T '  DOLLARS '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"--------------");         //Natural: WRITE ( 2 ) 003T '----' 022T '--------------' 053T '--------------'
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                    //*  PAGE 2
                    //*  PAGE 3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) NOTITLE ' '
                    getReports().write(3, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 3 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 3 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(3));
                    getReports().write(3, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 3 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 3 ) 51T '                CURRENT MODE                 '
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(51),"(INCLUDING PERIODIC PAYMENTS VALUED NOT PAID)");                                       //Natural: WRITE ( 3 ) 51T '(INCLUDING PERIODIC PAYMENTS VALUED NOT PAID)'
                    getReports().skip(3, 2);                                                                                                                              //Natural: SKIP ( 3 ) 2
                    //*  PAGE 3
                    //*  PAGE 4
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 4 ) NOTITLE ' '
                    getReports().write(4, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 4 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 4 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(4));
                    getReports().write(4, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 4 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 4 ) 51T '                CURRENT MODE                 '
                    getReports().write(4, ReportOption.NOTITLE,NEWLINE,new TabSetting(51),"PERIODIC PAYMENTS VALUED NOT PAID (DETAILS)");                                 //Natural: WRITE ( 4 ) / 51T 'PERIODIC PAYMENTS VALUED NOT PAID (DETAILS)'
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 4 ) 1
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(35),"  CONTRACTUAL",new TabSetting(56),"   PERIODIC");                                      //Natural: WRITE ( 4 ) 035T '  CONTRACTUAL' 056T '   PERIODIC'
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT/RS",new TabSetting(22),"MODE",new TabSetting(35),"    AMOUNT",new              //Natural: WRITE ( 4 ) 001T 'CONTRACT/RS' 022T 'MODE' 035T '    AMOUNT' 056T '   DIVIDEND'
                        TabSetting(56),"   DIVIDEND");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"-----------",new TabSetting(22),"----",new TabSetting(35),"--------------",new          //Natural: WRITE ( 4 ) 001T '-----------' 022T '----' 035T '--------------' 056T '--------------'
                        TabSetting(56),"--------------");
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 4 ) 1
                    //*  PAGE 4
                    //*  PAGE 5
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 5 ) NOTITLE ' '
                    getReports().write(5, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 5 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 5 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(5));
                    getReports().write(5, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 5 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 5 ) 51T '                CURRENT MODE                 '
                    getReports().skip(5, 2);                                                                                                                              //Natural: SKIP ( 5 ) 2
                    //*  PAGE 5
                    //*  PAGE 6
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 6 ) NOTITLE ' '
                    getReports().write(6, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 6 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 6 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(6));
                    getReports().write(6, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"));                                           //Natural: WRITE ( 6 ) '   DATE ' *DATX ( EM = MM/DD/YYYY )
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(51),"                CURRENT MODE                 ");                                       //Natural: WRITE ( 6 ) 51T '                CURRENT MODE                 '
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(51),"               (PAYMENTS PAID)               ");                                       //Natural: WRITE ( 6 ) 51T '               (PAYMENTS PAID)               '
                    getReports().skip(6, 2);                                                                                                                              //Natural: SKIP ( 6 ) 2
                    //*  PAGE 6
                    //*  PAGE 8  TIAA TOTALS
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt8 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(8, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 8 ) NOTITLE ' '
                    getReports().write(8, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 8 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 8 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(8));
                    getReports().write(8, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),pnd_Tiaa_Title);         //Natural: WRITE ( 8 ) '   DATE ' *DATX ( EM = MM/DD/YYYY ) 62T #TIAA-TITLE
                    getReports().skip(8, 1);                                                                                                                              //Natural: SKIP ( 8 ) 1
                    getReports().write(8, ReportOption.NOTITLE,new TabSetting(21),"CONTRACTUAL ",new TabSetting(53),"PERIODIC",new TabSetting(82)," FINAL",new            //Natural: WRITE ( 8 ) 021T 'CONTRACTUAL ' 053T 'PERIODIC' 082T ' FINAL' 112T '  FINAL'
                        TabSetting(112),"  FINAL");
                    getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(21),"  AMOUNT    ",new TabSetting(53),"DIVIDEND",new               //Natural: WRITE ( 8 ) 003T 'MODE' 021T '  AMOUNT    ' 053T 'DIVIDEND' 082T 'PAYMENT' 112T 'DIVIDEND'
                        TabSetting(82),"PAYMENT",new TabSetting(112),"DIVIDEND");
                    //*  10/23/2001
                    getReports().write(8, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(19),"--------------",new TabSetting(47),"--------------",new       //Natural: WRITE ( 8 ) 003T '----' 019T '--------------' 047T '--------------' 075T '--------------' 106T '--------------'
                        TabSetting(75),"--------------",new TabSetting(106),"--------------");
                    getReports().skip(8, 1);                                                                                                                              //Natural: SKIP ( 8 ) 1
                    //*  PAGE 8
                    //*  PAGE 9
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt9 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(9, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 9 ) NOTITLE ' '
                    getReports().write(9, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(34),pnd_Heading1,pnd_Payment_Due_Dte,new                     //Natural: WRITE ( 9 ) 'PROGRAM ' *PROGRAM 34T #HEADING1 #PAYMENT-DUE-DTE 121T 'PAGE ' *PAGE-NUMBER ( 9 )
                        TabSetting(121),"PAGE ",getReports().getPageNumberDbs(9));
                    getReports().write(9, ReportOption.NOTITLE,"   DATE ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(62),pnd_Cref_Title);         //Natural: WRITE ( 9 ) '   DATE ' *DATX ( EM = MM/DD/YYYY ) 62T #CREF-TITLE
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 9 ) 1
                    getReports().write(9, ReportOption.NOTITLE,new TabSetting(23),"  TIAA/CREF  ",new TabSetting(54),"  TIAA/CREF");                                      //Natural: WRITE ( 9 ) 023T '  TIAA/CREF  ' 054T '  TIAA/CREF'
                    getReports().write(9, ReportOption.NOTITLE,new TabSetting(3),"MODE",new TabSetting(24),"   UNITS     ",new TabSetting(55),"  DOLLARS ");              //Natural: WRITE ( 9 ) 003T 'MODE' 024T '   UNITS     ' 055T '  DOLLARS '
                    getReports().write(9, ReportOption.NOTITLE,new TabSetting(3),"----",new TabSetting(22),"--------------",new TabSetting(53),"--------------");         //Natural: WRITE ( 9 ) 003T '----' 022T '--------------' 053T '--------------'
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 9 ) 1
                    //*  PAGE 9
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE,NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,NEWLINE,                //Natural: WRITE NOHDR '==================================' // 'ERROR IN     ' *PROGRAM // 'ERROR NUMBER ' *ERROR-NR // 'ERROR LINE   ' *ERROR-LINE // '==================================' /
            "ERROR NUMBER ",Global.getERROR_NR(),NEWLINE,NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,NEWLINE,"==================================",
            NEWLINE);
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventR1() throws Exception {atBreakEventR1(false);}
    private void atBreakEventR1(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotIsBreak = pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_TotIsBreak))
        {
            pnd_Cnt_Tot.reset();                                                                                                                                          //Natural: RESET #CNT-TOT #CNT1 #CNT2 #CNT3 #CNT-NONE
            pnd_Cnt1.reset();
            pnd_Cnt2.reset();
            pnd_Cnt3.reset();
            pnd_Cnt_None.reset();
                                                                                                                                                                          //Natural: PERFORM #FUND-INFORMATION
            sub_Pnd_Fund_Information();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-REPORT
            sub_Pnd_Write_Out_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "AT BREAK","=",pnd_Input_Record_Pnd_I_Tiaa_Fund_Cde_Tot);                                                                           //Natural: WRITE 'AT BREAK' '=' #I-TIAA-FUND-CDE-TOT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=158");
        Global.format(2, "LS=133 PS=56");
        Global.format(3, "LS=133 PS=57");
        Global.format(4, "LS=133 PS=56");
        Global.format(5, "LS=133 PS=56");
        Global.format(6, "LS=133 PS=57");
        Global.format(7, "LS=133 PS=56");
        Global.format(8, "LS=133 PS=56");
        Global.format(9, "LS=133 PS=56");
    }
}
