/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:24:51 PM
**        * FROM NATURAL PROGRAM : Iaap3340
************************************************************
**        * FILE NAME            : Iaap3340.java
**        * CLASS NAME           : Iaap3340
**        * INSTANCE NAME        : Iaap3340
************************************************************
************************************************************************
* PROGRAM  : IAAP3340
* SYSTEM   : IA
* GENERATED: OCT 07,10
* FUNCTION : PRINTS THE MONTHLY PENDED PAYMMENT AND DPI LIABILITY
*            REPORT IA3300M5 (NON-PENSION BY ORIGIN)
* INPUT    : NON-PENSION EXTRACT
*            NON-PENSION WORKSHEET FILE (CREATED BY IAAP3320).
*
* HISTORY
* 10/07/10 O. SOTTO REWRITE OF THE CURRENT PROGRAM THAT PRODUCES
*                   IA3300M* REPORTS.
* 04/05/12 O. SOTTO RATE BASE CHANGES. SC 040512.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap3340 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_T_6;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_348;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;
    private DbsField pnd_Dashes;
    private DbsField pnd_Asterisk;

    private DbsGroup pnd_Header_Term;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Lit;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit;
    private DbsField pnd_Header_Term_Pnd_Hdr_Term_Dte;

    private DbsGroup pnd_Hdr_Tot_Lit_Ia3300m5;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Fill1m5_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Fill1;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Prdct;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdrm5_Var;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Chk;
    private DbsField pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Date;

    private DbsGroup pnd_Hdr_Wrksht_Ia3300m5;
    private DbsField pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Fill1m5;
    private DbsField pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Product;
    private DbsField pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Hdrm5;
    private DbsField pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Lit1;
    private DbsField pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date;

    private DbsGroup pnd_Tiaa_Colmn_Hdr1_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill;
    private DbsField pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit;

    private DbsGroup pnd_Tiaa_Colmn_Hdr2_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4;
    private DbsField pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte;

    private DbsGroup pnd_Tiaa_Colmn_Hdr3_Tot;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3;
    private DbsField pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4;

    private DbsGroup pnd_Wrksht_Hdr1;
    private DbsField pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1;
    private DbsField pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit;
    private DbsField pnd_Tiaa_Wrksht_Hdr2;
    private DbsField pnd_Tiaa_Wrksht_Hdr3;
    private DbsField pnd_Total_Orgn_Line;

    private DbsGroup pnd_Total_Orgn_Line__R_Field_5;
    private DbsField pnd_Total_Orgn_Line_Pnd_Orgin_Lit;
    private DbsField pnd_Total_Orgn_Line_Pnd_Origin_Cde;
    private DbsField pnd_Total_Orgn_Line_Pnd_Wrksht_Orgn_Fill1;
    private DbsField pnd_Total_Orgn_Line_Pnd_Origin_Name;

    private DbsGroup pnd_Tiaa_Total_Detail;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi;
    private DbsField pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd;

    private DbsGroup pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi;
    private DbsField pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due;
    private DbsField pnd_Tiaa_Wrksht_Total_Line;

    private DbsGroup pnd_Tiaa_Wrksht_Total_Line__R_Field_6;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5;
    private DbsField pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due;
    private DbsField pnd_Ia3300_Hdr_Lit;
    private DbsField pnd_Tot_Fill1m2_Lit;
    private DbsField pnd_Tot_Hdr1m2_Lit;
    private DbsField pnd_Wrksht_Total_Lit;
    private DbsField pnd_Wrksht_Org_Lit;

    private DbsGroup pnd_W_Tiaa_Total_Table;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Origin;

    private DbsGroup pnd_W_Tiaa_Total_Table__R_Field_7;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Totals;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Cntrcts;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Chks;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Dpi;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot;

    private DbsGroup pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cntrcts;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Chks;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Total;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Total_For_Tiaa;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Cntrcts;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Chks;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Dpi;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot;

    private DbsGroup pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cntrcts;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Chks;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Guar;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Divd;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Dpi;
    private DbsField pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Total;

    private DbsGroup pnd_Wrksht_File;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee;

    private DbsGroup pnd_Wrksht_File__R_Field_8;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Payee;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dod;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Opt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Org;

    private DbsGroup pnd_Wrksht_File__R_Field_9;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Org_A;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Cur;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Pend;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Mode;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Fund;

    private DbsGroup pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte;

    private DbsGroup pnd_Wrksht_File__R_Field_10;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Div;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Guar;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Units;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Reval;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Auv;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt;
    private DbsField pnd_Wrksht_File_Pnd_W_Wrksht_Total;

    private DbsGroup pnd_Ndxs_For_Tiaa_Total_Table;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx;
    private DbsField pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx;

    private DbsGroup pnd_Origin_Codes;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code1;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code1_Name;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code2;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code2_Name;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code3;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code3_Name;

    private DbsGroup pnd_Origin_Codes__R_Field_11;

    private DbsGroup pnd_Origin_Codes_Pnd_Org_Code_Name;
    private DbsField pnd_Origin_Codes_Pnd_Org_Code;
    private DbsField pnd_Origin_Codes_Pnd_Org_Name;
    private DbsField pnd_Max_Wrk;
    private DbsField pnd_Max_Org;
    private DbsField pnd_Max_Pend;
    private DbsField pnd_Max_Fund;
    private DbsField pnd_Max_Detail_Tbl;
    private DbsField pnd_Max_Prfx;
    private DbsField pnd_In_Cnt;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_E;
    private DbsField pnd_F;
    private DbsField pnd_G;
    private DbsField pnd_H;
    private DbsField pnd_Chk_Cnt;
    private DbsField pnd_Fnd_Cnt;
    private DbsField pnd_Tot_Ndx1;
    private DbsField pnd_Tot_Ndx2;
    private DbsField pnd_Tot_Ndx3;
    private DbsField pnd_Rec10_Cnt;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Tiaa_Var;
    private DbsField pnd_Bypass;
    private DbsField pnd_Process;
    private DbsField pnd_First_Time;
    private DbsField pnd_Detail;
    private DbsField pnd_Totals;
    private DbsField pnd_Wrksht;
    private DbsField pnd_Written;
    private DbsField pnd_W_Issue_Date;

    private DbsGroup pnd_W_Issue_Date__R_Field_12;
    private DbsField pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm;
    private DbsField pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd;
    private DbsField pnd_W_Work_Date;

    private DbsGroup pnd_W_Work_Date__R_Field_13;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Date_A;

    private DbsGroup pnd_W_Work_Date__R_Field_14;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Yyyymm;
    private DbsField pnd_W_Work_Date_Pnd_W_Work_Dd;
    private DbsField pnd_W_Install_Date;

    private DbsGroup pnd_W_Install_Date__R_Field_15;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yyyy;

    private DbsGroup pnd_W_Install_Date__R_Field_16;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Cc;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yy;

    private DbsGroup pnd_W_Install_Date__R_Field_17;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Yyyy_A;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Mm;

    private DbsGroup pnd_W_Install_Date__R_Field_18;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Date_N;

    private DbsGroup pnd_W_Install_Date__R_Field_19;
    private DbsField pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm;
    private DbsField pnd_W_Chk_Dte;

    private DbsGroup pnd_W_Chk_Dte__R_Field_20;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A;

    private DbsGroup pnd_W_Chk_Dte__R_Field_21;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy;

    private DbsGroup pnd_W_Chk_Dte__R_Field_22;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Mm;

    private DbsGroup pnd_W_Chk_Dte__R_Field_23;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N;

    private DbsGroup pnd_W_Chk_Dte__R_Field_24;
    private DbsField pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm;
    private DbsField pnd_W_Pend_Date;
    private DbsField pnd_W_First_Ann_Dod;
    private DbsField pnd_W_Scnd_Ann_Dod;
    private DbsField pnd_W_Pyee_Cde;
    private DbsField pnd_W_Mde_Cde;
    private DbsField pnd_W_Pnd_Cde;
    private DbsField pnd_W_Date_D;
    private DbsField pnd_W_Orign;
    private DbsField pnd_W_Rc;
    private DbsField pnd_W_Cur;
    private DbsField pnd_W_Optn;
    private DbsField pnd_W_Cntrct_Payee;

    private DbsGroup pnd_Total_Amts;
    private DbsField pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt;
    private DbsField pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt;
    private DbsField pnd_Total_Amts_Pnd_Total_Pymt;
    private DbsField pnd_Total_Amts_Pnd_Total_Dpi;
    private DbsField pnd_Total_Amts_Pnd_Total_Due;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Ppcn_Nbr);
        pnd_Input__Filler1 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_T_6 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_T_6", "#T-6", FieldType.STRING, 6);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_348 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_348", "#REST-OF-RECORD-348", FieldType.STRING, 
            1, new DbsArrayController(1, 348));

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_348);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 132);
        pnd_Asterisk = localVariables.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 132);

        pnd_Header_Term = localVariables.newGroupInRecord("pnd_Header_Term", "#HEADER-TERM");
        pnd_Header_Term_Pnd_Hdr_Term_Lit = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Lit", "#HDR-TERM-LIT", FieldType.STRING, 102);
        pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit", "#HDR-TERM-DTE-LIT", FieldType.STRING, 
            11);
        pnd_Header_Term_Pnd_Hdr_Term_Dte = pnd_Header_Term.newFieldInGroup("pnd_Header_Term_Pnd_Hdr_Term_Dte", "#HDR-TERM-DTE", FieldType.STRING, 10);

        pnd_Hdr_Tot_Lit_Ia3300m5 = localVariables.newGroupInRecord("pnd_Hdr_Tot_Lit_Ia3300m5", "#HDR-TOT-LIT-IA3300M5");
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Fill1m5_Var = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Fill1m5_Var", "#TOT-FILL1M5-VAR", 
            FieldType.STRING, 13);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Fill1 = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Fill1", "#TOT-HDR1M5-FILL1", 
            FieldType.STRING, 12);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Var = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Var", "#TOT-HDR1M5-VAR", 
            FieldType.STRING, 5);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Prdct = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Prdct", "#TOT-HDR1M5-PRDCT", 
            FieldType.STRING, 4);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdrm5_Var = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdrm5_Var", "#TOT-HDRM5-VAR", 
            FieldType.STRING, 63);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Chk = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Chk", "#TOT-HDR1M5-CHK", 
            FieldType.STRING, 10);
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Date = pnd_Hdr_Tot_Lit_Ia3300m5.newFieldInGroup("pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Date", "#TOT-HDR1M5-DATE", 
            FieldType.STRING, 10);

        pnd_Hdr_Wrksht_Ia3300m5 = localVariables.newGroupInRecord("pnd_Hdr_Wrksht_Ia3300m5", "#HDR-WRKSHT-IA3300M5");
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Fill1m5 = pnd_Hdr_Wrksht_Ia3300m5.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Fill1m5", "#WRKSHT-FILL1M5", 
            FieldType.STRING, 30);
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Product = pnd_Hdr_Wrksht_Ia3300m5.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Product", "#WRKSHT-PRODUCT", 
            FieldType.STRING, 4);
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Hdrm5 = pnd_Hdr_Wrksht_Ia3300m5.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Hdrm5", "#WRKSHT-HDRM5", 
            FieldType.STRING, 67);
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Lit1 = pnd_Hdr_Wrksht_Ia3300m5.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Lit1", "#WRKSHT-LIT1", FieldType.STRING, 
            11);
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date = pnd_Hdr_Wrksht_Ia3300m5.newFieldInGroup("pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date", "#WRKSHT-DATE", FieldType.STRING, 
            10);

        pnd_Tiaa_Colmn_Hdr1_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr1_Tot", "#TIAA-COLMN-HDR1-TOT");
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill = pnd_Tiaa_Colmn_Hdr1_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill", 
            "#TIAA-COLMN-HDR1-TOT-FILL", FieldType.STRING, 53);
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit = pnd_Tiaa_Colmn_Hdr1_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit", 
            "#TIAA-COLMN-HDR1-TOT-LIT", FieldType.STRING, 11);

        pnd_Tiaa_Colmn_Hdr2_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr2_Tot", "#TIAA-COLMN-HDR2-TOT");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1", 
            "#TIAA-COLMN-HDR2-TOT-LIT1", FieldType.STRING, 8);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1", 
            "#TIAA-COLMN-HDR2-TOT-FILL1", FieldType.STRING, 17);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2", 
            "#TIAA-COLMN-HDR2-TOT-LIT2", FieldType.STRING, 4);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2", 
            "#TIAA-COLMN-HDR2-TOT-FILL2", FieldType.STRING, 19);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3", 
            "#TIAA-COLMN-HDR2-TOT-LIT3", FieldType.STRING, 16);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3", 
            "#TIAA-COLMN-HDR2-TOT-FILL3", FieldType.STRING, 18);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4 = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4", 
            "#TIAA-COLMN-HDR2-TOT-LIT4", FieldType.STRING, 26);
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte = pnd_Tiaa_Colmn_Hdr2_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte", 
            "#TIAA-COLMN-HDR2-TOT-DTE", FieldType.STRING, 7);

        pnd_Tiaa_Colmn_Hdr3_Tot = localVariables.newGroupInRecord("pnd_Tiaa_Colmn_Hdr3_Tot", "#TIAA-COLMN-HDR3-TOT");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1", 
            "#TIAA-COLMN-HDR3-TOT-LIT1", FieldType.STRING, 6);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1", 
            "#TIAA-COLMN-HDR3-TOT-FILL1", FieldType.STRING, 9);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2", 
            "#TIAA-COLMN-HDR3-TOT-LIT2", FieldType.STRING, 3);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2", 
            "#TIAA-COLMN-HDR3-TOT-FILL2", FieldType.STRING, 5);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3", 
            "#TIAA-COLMN-HDR3-TOT-LIT3", FieldType.STRING, 4);
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4 = pnd_Tiaa_Colmn_Hdr3_Tot.newFieldInGroup("pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4", 
            "#TIAA-COLMN-HDR3-TOT-LIT4", FieldType.STRING, 100);

        pnd_Wrksht_Hdr1 = localVariables.newGroupInRecord("pnd_Wrksht_Hdr1", "#WRKSHT-HDR1");
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1 = pnd_Wrksht_Hdr1.newFieldInGroup("pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1", "#WRKSHT-HDR1-FILL1", FieldType.STRING, 
            43);
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit = pnd_Wrksht_Hdr1.newFieldInGroup("pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit", "#WRKSHT-HDR1-LIT", FieldType.STRING, 
            5);
        pnd_Tiaa_Wrksht_Hdr2 = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Hdr2", "#TIAA-WRKSHT-HDR2", FieldType.STRING, 132);
        pnd_Tiaa_Wrksht_Hdr3 = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Hdr3", "#TIAA-WRKSHT-HDR3", FieldType.STRING, 132);
        pnd_Total_Orgn_Line = localVariables.newFieldInRecord("pnd_Total_Orgn_Line", "#TOTAL-ORGN-LINE", FieldType.STRING, 132);

        pnd_Total_Orgn_Line__R_Field_5 = localVariables.newGroupInRecord("pnd_Total_Orgn_Line__R_Field_5", "REDEFINE", pnd_Total_Orgn_Line);
        pnd_Total_Orgn_Line_Pnd_Orgin_Lit = pnd_Total_Orgn_Line__R_Field_5.newFieldInGroup("pnd_Total_Orgn_Line_Pnd_Orgin_Lit", "#ORGIN-LIT", FieldType.STRING, 
            19);
        pnd_Total_Orgn_Line_Pnd_Origin_Cde = pnd_Total_Orgn_Line__R_Field_5.newFieldInGroup("pnd_Total_Orgn_Line_Pnd_Origin_Cde", "#ORIGIN-CDE", FieldType.STRING, 
            2);
        pnd_Total_Orgn_Line_Pnd_Wrksht_Orgn_Fill1 = pnd_Total_Orgn_Line__R_Field_5.newFieldInGroup("pnd_Total_Orgn_Line_Pnd_Wrksht_Orgn_Fill1", "#WRKSHT-ORGN-FILL1", 
            FieldType.STRING, 1);
        pnd_Total_Orgn_Line_Pnd_Origin_Name = pnd_Total_Orgn_Line__R_Field_5.newFieldInGroup("pnd_Total_Orgn_Line_Pnd_Origin_Name", "#ORIGIN-NAME", FieldType.STRING, 
            20);

        pnd_Tiaa_Total_Detail = localVariables.newGroupInRecord("pnd_Tiaa_Total_Detail", "#TIAA-TOTAL-DETAIL");
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit", "#TIAA-TOT-LIT", FieldType.STRING, 
            5);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix", "#TIAA-TOT-CNTRCT-PREFIX", 
            FieldType.STRING, 11);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr", "#TIAA-TOT-CURR", FieldType.STRING, 
            1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1", "#TIAA-TOT-FILL1", 
            FieldType.STRING, 6);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde", "#TIAA-TOT-PEND-CDE", 
            FieldType.STRING, 3);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2", "#TIAA-TOT-FILL2", 
            FieldType.STRING, 1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts", "#TIAA-TOT-NUM-CNTRCTS", 
            FieldType.NUMERIC, 7);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks", "#TIAA-TOT-NUM-CHKS", 
            FieldType.NUMERIC, 7);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar", "#TIAA-TOT-CUR-GUAR", 
            FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div", "#TIAA-TOT-CUR-DIV", 
            FieldType.NUMERIC, 9, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6 = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6", "#TIAA-TOT-FILL6", 
            FieldType.STRING, 1);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar", "#TIAA-TOT-DUE-GUAR", 
            FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div", "#TIAA-TOT-DUE-DIV", 
            FieldType.NUMERIC, 10, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi", "#TIAA-TOT-DUE-DPI", 
            FieldType.NUMERIC, 10, 2);
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total = pnd_Tiaa_Total_Detail.newFieldInGroup("pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total", "#TIAA-TOT-DUE-TOTAL", 
            FieldType.NUMERIC, 12, 2);

        pnd_Tiaa_Wrksht_Detail_Line = localVariables.newGroupInRecord("pnd_Tiaa_Wrksht_Detail_Line", "#TIAA-WRKSHT-DETAIL-LINE");

        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1 = pnd_Tiaa_Wrksht_Detail_Line.newGroupInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1", 
            "#TIAA-WRKSHT-DTL-PART1");
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct", 
            "#TIAA-WRKSHT-DTL-CNTRCT", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee", 
            "#TIAA-WRKSHT-DTL-PYEE", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn", 
            "#TIAA-WRKSHT-DTL-OPTN", FieldType.STRING, 3);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org", 
            "#TIAA-WRKSHT-DTL-ORG", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte", 
            "#TIAA-WRKSHT-DTL-DOD-DTE", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte", 
            "#TIAA-WRKSHT-DTL-FIRST-DTE", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend", 
            "#TIAA-WRKSHT-DTL-PEND", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7", 
            "#TIAA-WRKSHT-DTL-FILL7", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode", 
            "#TIAA-WRKSHT-DTL-MODE", FieldType.STRING, 3);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte", 
            "#TIAA-WRKSHT-DTL-PYMT-DTE", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part1.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd", 
            "#TIAA-WRKSHT-DTL-FND", FieldType.STRING, 1);

        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2 = pnd_Tiaa_Wrksht_Detail_Line.newGroupInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2", 
            "#TIAA-WRKSHT-DTL-PART2");
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte", 
            "#TIAA-WRKSHT-DTL-DUE-DTE", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar", 
            "#TIAA-WRKSHT-DTL-GUAR", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11", 
            "#TIAA-WRKSHT-DTL-FILL11", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div", 
            "#TIAA-WRKSHT-DTL-DIV", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt", 
            "#TIAA-WRKSHT-DTL-TOT-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13 = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13", 
            "#TIAA-WRKSHT-DTL-FILL13", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact", 
            "#TIAA-WRKSHT-DTL-FACT", FieldType.STRING, 7);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi", 
            "#TIAA-WRKSHT-DTL-DPI", FieldType.STRING, 8);
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due = pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Part2.newFieldInGroup("pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due", 
            "#TIAA-WRKSHT-DTL-TOT-DUE", FieldType.STRING, 12);
        pnd_Tiaa_Wrksht_Total_Line = localVariables.newFieldInRecord("pnd_Tiaa_Wrksht_Total_Line", "#TIAA-WRKSHT-TOTAL-LINE", FieldType.STRING, 132);

        pnd_Tiaa_Wrksht_Total_Line__R_Field_6 = localVariables.newGroupInRecord("pnd_Tiaa_Wrksht_Total_Line__R_Field_6", "REDEFINE", pnd_Tiaa_Wrksht_Total_Line);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit", 
            "#TIAA-WRKSHT-TOTAL-LIT", FieldType.STRING, 25);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill0", 
            "#TIAA-WRKSHT-TOTAL-FILL0", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1", 
            "#TIAA-WRKSHT-TOTAL-FILL1", FieldType.STRING, 36);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt", 
            "#TIAA-WRKSHT-TOTAL-GUR-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill2", 
            "#TIAA-WRKSHT-TOTAL-FILL2", FieldType.STRING, 2);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt", 
            "#TIAA-WRKSHT-TOTAL-DIV-PYMT", FieldType.STRING, 11);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill3", 
            "#TIAA-WRKSHT-TOTAL-FILL3", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt", 
            "#TIAA-WRKSHT-TOTAL-PYMT", FieldType.STRING, 12);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill4", 
            "#TIAA-WRKSHT-TOTAL-FILL4", FieldType.STRING, 10);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi", 
            "#TIAA-WRKSHT-TOTAL-DPI", FieldType.STRING, 9);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5 = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill5", 
            "#TIAA-WRKSHT-TOTAL-FILL5", FieldType.STRING, 1);
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due = pnd_Tiaa_Wrksht_Total_Line__R_Field_6.newFieldInGroup("pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due", 
            "#TIAA-WRKSHT-TOTAL-DUE", FieldType.STRING, 12);
        pnd_Ia3300_Hdr_Lit = localVariables.newFieldInRecord("pnd_Ia3300_Hdr_Lit", "#IA3300-HDR-LIT", FieldType.STRING, 63);
        pnd_Tot_Fill1m2_Lit = localVariables.newFieldInRecord("pnd_Tot_Fill1m2_Lit", "#TOT-FILL1M2-LIT", FieldType.STRING, 13);
        pnd_Tot_Hdr1m2_Lit = localVariables.newFieldInRecord("pnd_Tot_Hdr1m2_Lit", "#TOT-HDR1M2-LIT", FieldType.STRING, 5);
        pnd_Wrksht_Total_Lit = localVariables.newFieldInRecord("pnd_Wrksht_Total_Lit", "#WRKSHT-TOTAL-LIT", FieldType.STRING, 25);
        pnd_Wrksht_Org_Lit = localVariables.newFieldInRecord("pnd_Wrksht_Org_Lit", "#WRKSHT-ORG-LIT", FieldType.STRING, 19);

        pnd_W_Tiaa_Total_Table = localVariables.newGroupArrayInRecord("pnd_W_Tiaa_Total_Table", "#W-TIAA-TOTAL-TABLE", new DbsArrayController(1, 3));
        pnd_W_Tiaa_Total_Table_Pnd_W_Origin = pnd_W_Tiaa_Total_Table.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Origin", "#W-ORIGIN", FieldType.NUMERIC, 
            2);

        pnd_W_Tiaa_Total_Table__R_Field_7 = pnd_W_Tiaa_Total_Table.newGroupInGroup("pnd_W_Tiaa_Total_Table__R_Field_7", "REDEFINE", pnd_W_Tiaa_Total_Table_Pnd_W_Origin);
        pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A = pnd_W_Tiaa_Total_Table__R_Field_7.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A", "#W-ORIGIN-A", 
            FieldType.STRING, 2);

        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Totals = pnd_W_Tiaa_Total_Table.newGroupArrayInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Totals", "#W-TIAA-TOTALS", 
            new DbsArrayController(1, 8));

        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Totals.newGroupArrayInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds", 
            "#W-TIAA-TOT-FLDS", new DbsArrayController(1, 20));
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend", 
            "#W-TIAA-DUE-PEND", FieldType.STRING, 1);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Cntrcts = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Cntrcts", 
            "#W-TIAA-DUE-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Chks = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Chks", 
            "#W-TIAA-DUE-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Guar", 
            "#W-TIAA-DUE-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Divd", 
            "#W-TIAA-DUE-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Guar", 
            "#W-TIAA-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Divd", 
            "#W-TIAA-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Dpi = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Dpi", "#W-TIAA-DPI", 
            FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot", "#W-TIAA-TOT", 
            FieldType.NUMERIC, 12, 2);

        pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Totals.newGroupInGroup("pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx", 
            "W-TOTALS-BY-PRFX");
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cntrcts = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cntrcts", 
            "#W-TIAA-TOT-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Chks = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Chks", 
            "#W-TIAA-TOT-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Guar = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Guar", 
            "#W-TIAA-TOT-CUR-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Divd = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Divd", 
            "#W-TIAA-TOT-CUR-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar", 
            "#W-TIAA-TOT-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd", 
            "#W-TIAA-TOT-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi", 
            "#W-TIAA-TOT-DPI", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Total = pnd_W_Tiaa_Total_Table_W_Totals_By_Prfx.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Total", "#W-TIAA-TOTAL", 
            FieldType.NUMERIC, 12, 2);

        pnd_W_Tiaa_Total_Table_Pnd_W_Total_For_Tiaa = pnd_W_Tiaa_Total_Table.newGroupInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Total_For_Tiaa", "#W-TOTAL-FOR-TIAA");

        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds = pnd_W_Tiaa_Total_Table_Pnd_W_Total_For_Tiaa.newGroupArrayInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds", 
            "#W-TIAA-TOT-FND-FLDS", new DbsArrayController(1, 20));
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend", 
            "#W-TIAA-FND-DUE-PEND", FieldType.STRING, 1);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Cntrcts = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Cntrcts", 
            "#W-TIAA-FND-DUE-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Chks = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Chks", 
            "#W-TIAA-FND-DUE-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Guar", 
            "#W-TIAA-FND-DUE-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Divd", 
            "#W-TIAA-FND-DUE-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Guar", 
            "#W-TIAA-FND-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Divd", 
            "#W-TIAA-FND-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Dpi = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Dpi", 
            "#W-TIAA-FND-DPI", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot = pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Fnd_Flds.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot", 
            "#W-TIAA-FND-TOT", FieldType.NUMERIC, 12, 2);

        pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals = pnd_W_Tiaa_Total_Table_Pnd_W_Total_For_Tiaa.newGroupInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals", 
            "#W-FUND-TOTALS");
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cntrcts = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cntrcts", 
            "#W-TIAA-FND-TOT-CNTRCTS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Chks = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Chks", 
            "#W-TIAA-FND-TOT-CHKS", FieldType.NUMERIC, 7);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Guar", 
            "#W-TIAA-FND-TOT-CUR-GUAR", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Divd", 
            "#W-TIAA-FND-TOT-CUR-DIVD", FieldType.NUMERIC, 9, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Guar = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Guar", 
            "#W-TIAA-FND-TOT-GUAR", FieldType.NUMERIC, 11, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Divd = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Divd", 
            "#W-TIAA-FND-TOT-DIVD", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Dpi = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Dpi", 
            "#W-TIAA-FND-TOT-DPI", FieldType.NUMERIC, 10, 2);
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Total = pnd_W_Tiaa_Total_Table_Pnd_W_Fund_Totals.newFieldInGroup("pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Total", 
            "#W-TIAA-FND-TOTAL", FieldType.NUMERIC, 12, 2);

        pnd_Wrksht_File = localVariables.newGroupInRecord("pnd_Wrksht_File", "#WRKSHT-FILE");
        pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee", "#W-WRKSHT-CNTRCT-PAYEE", 
            FieldType.STRING, 10);

        pnd_Wrksht_File__R_Field_8 = pnd_Wrksht_File.newGroupInGroup("pnd_Wrksht_File__R_Field_8", "REDEFINE", pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);
        pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct = pnd_Wrksht_File__R_Field_8.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct", "#W-WRKSHT-CNTRCT", FieldType.STRING, 
            8);
        pnd_Wrksht_File_Pnd_W_Wrksht_Payee = pnd_Wrksht_File__R_Field_8.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Payee", "#W-WRKSHT-PAYEE", FieldType.STRING, 
            2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dod = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dod", "#W-WRKSHT-DOD", FieldType.NUMERIC, 6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Opt = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Opt", "#W-WRKSHT-OPT", FieldType.STRING, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Org = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Org", "#W-WRKSHT-ORG", FieldType.NUMERIC, 2);

        pnd_Wrksht_File__R_Field_9 = pnd_Wrksht_File.newGroupInGroup("pnd_Wrksht_File__R_Field_9", "REDEFINE", pnd_Wrksht_File_Pnd_W_Wrksht_Org);
        pnd_Wrksht_File_Pnd_W_Wrksht_Org_A = pnd_Wrksht_File__R_Field_9.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Org_A", "#W-WRKSHT-ORG-A", FieldType.STRING, 
            2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Cur = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Cur", "#W-WRKSHT-CUR", FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte", "#W-WRKSHT-1ST-DTE", FieldType.STRING, 
            6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Pend = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Pend", "#W-WRKSHT-PEND", FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_Mode = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Mode", "#W-WRKSHT-MODE", FieldType.STRING, 3);
        pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt", "#W-WRKSHT-FINAL-PYMT", FieldType.STRING, 
            6);
        pnd_Wrksht_File_Pnd_W_Wrksht_Fund = pnd_Wrksht_File.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Fund", "#W-WRKSHT-FUND", FieldType.STRING, 1);

        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts = pnd_Wrksht_File.newGroupArrayInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts", "#W-WRKSHT-DUE-PYMTS", 
            new DbsArrayController(1, 240));
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte", "#W-WRKSHT-DUE-DTE", 
            FieldType.STRING, 6);

        pnd_Wrksht_File__R_Field_10 = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newGroupInGroup("pnd_Wrksht_File__R_Field_10", "REDEFINE", pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte);
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy = pnd_Wrksht_File__R_Field_10.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy", "#W-WRKSHT-DUE-YYYY", 
            FieldType.STRING, 4);
        pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm = pnd_Wrksht_File__R_Field_10.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm", "#W-WRKSHT-DUE-MM", FieldType.STRING, 
            2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Div = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Div", "#W-WRKSHT-DIV", 
            FieldType.NUMERIC, 9, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Guar = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Guar", "#W-WRKSHT-GUAR", 
            FieldType.NUMERIC, 9, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Units = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Units", "#W-WRKSHT-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pnd_Wrksht_File_Pnd_W_Wrksht_Reval = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Reval", "#W-WRKSHT-REVAL", 
            FieldType.STRING, 1);
        pnd_Wrksht_File_Pnd_W_Wrksht_Auv = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Auv", "#W-WRKSHT-AUV", 
            FieldType.NUMERIC, 9, 4);
        pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt", "#W-WRKSHT-TOT-PYMT", 
            FieldType.NUMERIC, 10, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact", "#W-WRKSHT-DPI-FACT", 
            FieldType.NUMERIC, 6, 5);
        pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt", "#W-WRKSHT-DPI-AMT", 
            FieldType.NUMERIC, 7, 2);
        pnd_Wrksht_File_Pnd_W_Wrksht_Total = pnd_Wrksht_File_Pnd_W_Wrksht_Due_Pymts.newFieldInGroup("pnd_Wrksht_File_Pnd_W_Wrksht_Total", "#W-WRKSHT-TOTAL", 
            FieldType.NUMERIC, 11, 2);

        pnd_Ndxs_For_Tiaa_Total_Table = localVariables.newGroupInRecord("pnd_Ndxs_For_Tiaa_Total_Table", "#NDXS-FOR-TIAA-TOTAL-TABLE");
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx", "#GA-0L-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx", "#GW-0M-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx", "#IA-0N-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx", "#IP-6L-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx", "#S0-6M-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx", "#W0-6N-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx", "#W0-GRP-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx", "#Z0-NDX", 
            FieldType.NUMERIC, 1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx = pnd_Ndxs_For_Tiaa_Total_Table.newFieldInGroup("pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx", "#Y0-NDX", 
            FieldType.NUMERIC, 1);

        pnd_Origin_Codes = localVariables.newGroupInRecord("pnd_Origin_Codes", "#ORIGIN-CODES");
        pnd_Origin_Codes_Pnd_Org_Code1 = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code1", "#ORG-CODE1", FieldType.NUMERIC, 2);
        pnd_Origin_Codes_Pnd_Org_Code1_Name = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code1_Name", "#ORG-CODE1-NAME", FieldType.STRING, 
            20);
        pnd_Origin_Codes_Pnd_Org_Code2 = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code2", "#ORG-CODE2", FieldType.NUMERIC, 2);
        pnd_Origin_Codes_Pnd_Org_Code2_Name = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code2_Name", "#ORG-CODE2-NAME", FieldType.STRING, 
            20);
        pnd_Origin_Codes_Pnd_Org_Code3 = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code3", "#ORG-CODE3", FieldType.NUMERIC, 2);
        pnd_Origin_Codes_Pnd_Org_Code3_Name = pnd_Origin_Codes.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code3_Name", "#ORG-CODE3-NAME", FieldType.STRING, 
            20);

        pnd_Origin_Codes__R_Field_11 = localVariables.newGroupInRecord("pnd_Origin_Codes__R_Field_11", "REDEFINE", pnd_Origin_Codes);

        pnd_Origin_Codes_Pnd_Org_Code_Name = pnd_Origin_Codes__R_Field_11.newGroupArrayInGroup("pnd_Origin_Codes_Pnd_Org_Code_Name", "#ORG-CODE-NAME", 
            new DbsArrayController(1, 3));
        pnd_Origin_Codes_Pnd_Org_Code = pnd_Origin_Codes_Pnd_Org_Code_Name.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Code", "#ORG-CODE", FieldType.STRING, 
            2);
        pnd_Origin_Codes_Pnd_Org_Name = pnd_Origin_Codes_Pnd_Org_Code_Name.newFieldInGroup("pnd_Origin_Codes_Pnd_Org_Name", "#ORG-NAME", FieldType.STRING, 
            20);
        pnd_Max_Wrk = localVariables.newFieldInRecord("pnd_Max_Wrk", "#MAX-WRK", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Org = localVariables.newFieldInRecord("pnd_Max_Org", "#MAX-ORG", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Pend = localVariables.newFieldInRecord("pnd_Max_Pend", "#MAX-PEND", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Fund = localVariables.newFieldInRecord("pnd_Max_Fund", "#MAX-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Detail_Tbl = localVariables.newFieldInRecord("pnd_Max_Detail_Tbl", "#MAX-DETAIL-TBL", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Prfx = localVariables.newFieldInRecord("pnd_Max_Prfx", "#MAX-PRFX", FieldType.PACKED_DECIMAL, 3);
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.PACKED_DECIMAL, 3);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.PACKED_DECIMAL, 3);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.PACKED_DECIMAL, 3);
        pnd_H = localVariables.newFieldInRecord("pnd_H", "#H", FieldType.PACKED_DECIMAL, 3);
        pnd_Chk_Cnt = localVariables.newFieldInRecord("pnd_Chk_Cnt", "#CHK-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Fnd_Cnt", "#FND-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx1 = localVariables.newFieldInRecord("pnd_Tot_Ndx1", "#TOT-NDX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx2 = localVariables.newFieldInRecord("pnd_Tot_Ndx2", "#TOT-NDX2", FieldType.PACKED_DECIMAL, 3);
        pnd_Tot_Ndx3 = localVariables.newFieldInRecord("pnd_Tot_Ndx3", "#TOT-NDX3", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec10_Cnt = localVariables.newFieldInRecord("pnd_Rec10_Cnt", "#REC10-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Var = localVariables.newFieldInRecord("pnd_Tiaa_Var", "#TIAA-VAR", FieldType.BOOLEAN, 1);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Process = localVariables.newFieldInRecord("pnd_Process", "#PROCESS", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Detail = localVariables.newFieldInRecord("pnd_Detail", "#DETAIL", FieldType.BOOLEAN, 1);
        pnd_Totals = localVariables.newFieldInRecord("pnd_Totals", "#TOTALS", FieldType.BOOLEAN, 1);
        pnd_Wrksht = localVariables.newFieldInRecord("pnd_Wrksht", "#WRKSHT", FieldType.BOOLEAN, 1);
        pnd_Written = localVariables.newFieldInRecord("pnd_Written", "#WRITTEN", FieldType.BOOLEAN, 1);
        pnd_W_Issue_Date = localVariables.newFieldInRecord("pnd_W_Issue_Date", "#W-ISSUE-DATE", FieldType.NUMERIC, 8);

        pnd_W_Issue_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_W_Issue_Date__R_Field_12", "REDEFINE", pnd_W_Issue_Date);
        pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm = pnd_W_Issue_Date__R_Field_12.newFieldInGroup("pnd_W_Issue_Date_Pnd_W_Issue_Date_Yymm", "#W-ISSUE-DATE-YYMM", 
            FieldType.NUMERIC, 6);
        pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd = pnd_W_Issue_Date__R_Field_12.newFieldInGroup("pnd_W_Issue_Date_Pnd_W_Issue_Date_Dd", "#W-ISSUE-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_W_Work_Date = localVariables.newFieldInRecord("pnd_W_Work_Date", "#W-WORK-DATE", FieldType.NUMERIC, 8);

        pnd_W_Work_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_W_Work_Date__R_Field_13", "REDEFINE", pnd_W_Work_Date);
        pnd_W_Work_Date_Pnd_W_Work_Date_A = pnd_W_Work_Date__R_Field_13.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Date_A", "#W-WORK-DATE-A", FieldType.STRING, 
            8);

        pnd_W_Work_Date__R_Field_14 = localVariables.newGroupInRecord("pnd_W_Work_Date__R_Field_14", "REDEFINE", pnd_W_Work_Date);
        pnd_W_Work_Date_Pnd_W_Work_Yyyymm = pnd_W_Work_Date__R_Field_14.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Yyyymm", "#W-WORK-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_W_Work_Date_Pnd_W_Work_Dd = pnd_W_Work_Date__R_Field_14.newFieldInGroup("pnd_W_Work_Date_Pnd_W_Work_Dd", "#W-WORK-DD", FieldType.NUMERIC, 
            2);
        pnd_W_Install_Date = localVariables.newFieldInRecord("pnd_W_Install_Date", "#W-INSTALL-DATE", FieldType.STRING, 8);

        pnd_W_Install_Date__R_Field_15 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_15", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Yyyy = pnd_W_Install_Date__R_Field_15.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yyyy", "#W-INSTALL-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_W_Install_Date__R_Field_16 = pnd_W_Install_Date__R_Field_15.newGroupInGroup("pnd_W_Install_Date__R_Field_16", "REDEFINE", pnd_W_Install_Date_Pnd_W_Install_Yyyy);
        pnd_W_Install_Date_Pnd_W_Install_Cc = pnd_W_Install_Date__R_Field_16.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Cc", "#W-INSTALL-CC", FieldType.NUMERIC, 
            2);
        pnd_W_Install_Date_Pnd_W_Install_Yy = pnd_W_Install_Date__R_Field_16.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yy", "#W-INSTALL-YY", FieldType.NUMERIC, 
            2);

        pnd_W_Install_Date__R_Field_17 = pnd_W_Install_Date__R_Field_15.newGroupInGroup("pnd_W_Install_Date__R_Field_17", "REDEFINE", pnd_W_Install_Date_Pnd_W_Install_Yyyy);
        pnd_W_Install_Date_Pnd_W_Install_Yyyy_A = pnd_W_Install_Date__R_Field_17.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Yyyy_A", "#W-INSTALL-YYYY-A", 
            FieldType.STRING, 4);
        pnd_W_Install_Date_Pnd_W_Install_Mm = pnd_W_Install_Date__R_Field_15.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Mm", "#W-INSTALL-MM", FieldType.NUMERIC, 
            2);

        pnd_W_Install_Date__R_Field_18 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_18", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Date_N = pnd_W_Install_Date__R_Field_18.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Date_N", "#W-INSTALL-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_W_Install_Date__R_Field_19 = localVariables.newGroupInRecord("pnd_W_Install_Date__R_Field_19", "REDEFINE", pnd_W_Install_Date);
        pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm = pnd_W_Install_Date__R_Field_19.newFieldInGroup("pnd_W_Install_Date_Pnd_W_Install_Date_Yyyymm", 
            "#W-INSTALL-DATE-YYYYMM", FieldType.STRING, 6);
        pnd_W_Chk_Dte = localVariables.newFieldInRecord("pnd_W_Chk_Dte", "#W-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_W_Chk_Dte__R_Field_20 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_20", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A = pnd_W_Chk_Dte__R_Field_20.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A", "#W-CHK-DTE-A", FieldType.STRING, 8);

        pnd_W_Chk_Dte__R_Field_21 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_21", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy = pnd_W_Chk_Dte__R_Field_21.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy", "#W-CHK-YYYY", FieldType.STRING, 4);

        pnd_W_Chk_Dte__R_Field_22 = pnd_W_Chk_Dte__R_Field_21.newGroupInGroup("pnd_W_Chk_Dte__R_Field_22", "REDEFINE", pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N = pnd_W_Chk_Dte__R_Field_22.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy_N", "#W-CHK-YYYY-N", FieldType.NUMERIC, 
            4);
        pnd_W_Chk_Dte_Pnd_W_Chk_Mm = pnd_W_Chk_Dte__R_Field_21.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Mm", "#W-CHK-MM", FieldType.STRING, 2);

        pnd_W_Chk_Dte__R_Field_23 = pnd_W_Chk_Dte__R_Field_21.newGroupInGroup("pnd_W_Chk_Dte__R_Field_23", "REDEFINE", pnd_W_Chk_Dte_Pnd_W_Chk_Mm);
        pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N = pnd_W_Chk_Dte__R_Field_23.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Mm_N", "#W-CHK-MM-N", FieldType.NUMERIC, 2);

        pnd_W_Chk_Dte__R_Field_24 = localVariables.newGroupInRecord("pnd_W_Chk_Dte__R_Field_24", "REDEFINE", pnd_W_Chk_Dte);
        pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm = pnd_W_Chk_Dte__R_Field_24.newFieldInGroup("pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm", "#W-CHK-YYYYMM", FieldType.STRING, 
            6);
        pnd_W_Pend_Date = localVariables.newFieldInRecord("pnd_W_Pend_Date", "#W-PEND-DATE", FieldType.NUMERIC, 6);
        pnd_W_First_Ann_Dod = localVariables.newFieldInRecord("pnd_W_First_Ann_Dod", "#W-FIRST-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_W_Scnd_Ann_Dod = localVariables.newFieldInRecord("pnd_W_Scnd_Ann_Dod", "#W-SCND-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_W_Pyee_Cde = localVariables.newFieldInRecord("pnd_W_Pyee_Cde", "#W-PYEE-CDE", FieldType.NUMERIC, 2);
        pnd_W_Mde_Cde = localVariables.newFieldInRecord("pnd_W_Mde_Cde", "#W-MDE-CDE", FieldType.NUMERIC, 3);
        pnd_W_Pnd_Cde = localVariables.newFieldInRecord("pnd_W_Pnd_Cde", "#W-PND-CDE", FieldType.STRING, 1);
        pnd_W_Date_D = localVariables.newFieldInRecord("pnd_W_Date_D", "#W-DATE-D", FieldType.DATE);
        pnd_W_Orign = localVariables.newFieldInRecord("pnd_W_Orign", "#W-ORIGN", FieldType.NUMERIC, 2);
        pnd_W_Rc = localVariables.newFieldInRecord("pnd_W_Rc", "#W-RC", FieldType.NUMERIC, 2);
        pnd_W_Cur = localVariables.newFieldInRecord("pnd_W_Cur", "#W-CUR", FieldType.STRING, 1);
        pnd_W_Optn = localVariables.newFieldInRecord("pnd_W_Optn", "#W-OPTN", FieldType.STRING, 2);
        pnd_W_Cntrct_Payee = localVariables.newFieldInRecord("pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", FieldType.STRING, 10);

        pnd_Total_Amts = localVariables.newGroupInRecord("pnd_Total_Amts", "#TOTAL-AMTS");
        pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt", "#TIAA-TOTAL-DIV-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt", "#TIAA-TOTAL-GUR-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Total_Amts_Pnd_Total_Pymt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Pymt", "#TOTAL-PYMT", FieldType.NUMERIC, 10, 2);
        pnd_Total_Amts_Pnd_Total_Dpi = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Dpi", "#TOTAL-DPI", FieldType.NUMERIC, 8, 2);
        pnd_Total_Amts_Pnd_Total_Due = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Total_Due", "#TOTAL-DUE", FieldType.NUMERIC, 11, 2);
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Header_Term_Pnd_Hdr_Term_Lit.setInitialValue("CONTRACTS TERMINATING THIS MONTH BECAUSE OF EXPIRY OF FINAL PERIODIC OR IRREGULAR PAYMENT DATE");
        pnd_Header_Term_Pnd_Hdr_Term_Dte_Lit.setInitialValue("CHECK DATE");
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Chk.setInitialValue("CHECK DATE");
        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Lit1.setInitialValue("CHECK DATE ");
        pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit.setInitialValue("PAYMENT DUE");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1.setInitialValue("CONTRACT");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2.setInitialValue("PEND");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3.setInitialValue("IN CURRENT MONTH");
        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4.setInitialValue("-- TOTAL PAYMENTS DUE THRU");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1.setInitialValue("NUMBER");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2.setInitialValue("CUR");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3.setInitialValue("CODE");
        pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4.setInitialValue("#CONTRACTS    #CKS  ---GUAR---  ---DIVD---      ---GUAR---   ---DIVD---    ---DPI---    ---TOTAL---");
        pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit.setInitialValue("FINAL");
        pnd_Tiaa_Wrksht_Hdr2.setInitialValue("CONTRACT                    FIRST PEND      PAYT        DUE    GUARANTEED     DIVIDEND      TOTAL     DPI/DCI      DPI/");
        pnd_Tiaa_Wrksht_Hdr3.setInitialValue("NUMBER   PY OPT ORG DOD     DATE  CODE MODE DATE   FND  DATE      PAYMENT     PAYMENT      PAYMENT     FACTOR      DCI    TOTAL DUE");
        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit.setInitialValue("TOTAL");
        pnd_Ia3300_Hdr_Lit.setInitialValue("NON-PENSION BY ORIGIN PENDED PAYMENT AND DPI LIABILITY");
        pnd_Tot_Fill1m2_Lit.setInitialValue("WS - 6: ATT-3");
        pnd_Tot_Hdr1m2_Lit.setInitialValue("TOTAL");
        pnd_Wrksht_Total_Lit.setInitialValue("TOTALS FOR CONTRACT/PAYEE");
        pnd_Wrksht_Org_Lit.setInitialValue("**** ORIGIN CODE = ");
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx.setInitialValue(1);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx.setInitialValue(2);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx.setInitialValue(3);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx.setInitialValue(4);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx.setInitialValue(5);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx.setInitialValue(6);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx.setInitialValue(7);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Z0_Ndx.setInitialValue(7);
        pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx.setInitialValue(8);
        pnd_Origin_Codes_Pnd_Org_Code1.setInitialValue(3);
        pnd_Origin_Codes_Pnd_Org_Code1_Name.setInitialValue("Insurance");
        pnd_Origin_Codes_Pnd_Org_Code2.setInitialValue(17);
        pnd_Origin_Codes_Pnd_Org_Code2_Name.setInitialValue("PA death");
        pnd_Origin_Codes_Pnd_Org_Code3.setInitialValue(18);
        pnd_Origin_Codes_Pnd_Org_Code3_Name.setInitialValue("PA Maturity");
        pnd_Max_Wrk.setInitialValue(240);
        pnd_Max_Org.setInitialValue(3);
        pnd_Max_Pend.setInitialValue(20);
        pnd_Max_Fund.setInitialValue(40);
        pnd_Max_Detail_Tbl.setInitialValue(20);
        pnd_Max_Prfx.setInitialValue(8);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap3340() throws Exception
    {
        super("Iaap3340");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Fill1m5_Var.setValue(pnd_Tot_Fill1m2_Lit);                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: ASSIGN #TOT-FILL1M5-VAR := #TOT-FILL1M2-LIT
        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Var.setValue(pnd_Tot_Hdr1m2_Lit);                                                                                         //Natural: ASSIGN #TOT-HDR1M5-VAR := #TOT-HDR1M2-LIT
        pnd_Asterisk.moveAll("*");                                                                                                                                        //Natural: MOVE ALL '*' TO #ASTERISK
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                                   //Natural: IF #INPUT.#PPCN-NBR = '   CHEADER'
            {
                pnd_W_Chk_Dte.setValue(pnd_Input_Pnd_Header_Chk_Dte);                                                                                                     //Natural: ASSIGN #W-CHK-DTE := #INPUT.#HEADER-CHK-DTE
                pnd_W_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Chk_Dte_Pnd_W_Chk_Dte_A);                                                                //Natural: MOVE EDITED #W-CHK-DTE-A TO #W-DATE-D ( EM = YYYYMMDD )
                pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date.setValueEdited(pnd_W_Date_D,new ReportEditMask("MM/DD/YYYY"));                                                    //Natural: MOVE EDITED #W-DATE-D ( EM = MM/DD/YYYY ) TO #WRKSHT-DATE
                pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Date.setValue(pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date);                                                           //Natural: ASSIGN #TOT-HDR1M5-DATE := #WRKSHT-DATE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Wrksht.setValue(true);                                                                                                                                        //Natural: ASSIGN #WRKSHT := TRUE
        READWORK02:                                                                                                                                                       //Natural: READ WORK 2 #WRKSHT-FILE
        while (condition(getWorkFiles().read(2, pnd_Wrksht_File)))
        {
            pnd_In_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IN-CNT
            pnd_W_Orign.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Org);                                                                                                       //Natural: ASSIGN #W-ORIGN := #W-WRKSHT-ORG
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIAA-WORKSHEET
            sub_Process_Tiaa_Worksheet();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_In_Cnt.greater(getZero())))                                                                                                                     //Natural: IF #IN-CNT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-WRKSHT-CNTRCT-TOTALS
            sub_Print_Tiaa_Wrksht_Cntrct_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Totals.setValue(true);                                                                                                                                    //Natural: ASSIGN #TOTALS := TRUE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-TOTALS
            sub_Print_Tiaa_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, "NO PENDED TIAA NON-PENSION CONTRACT PROCESSED");                                                                                       //Natural: WRITE ( 1 ) 'NO PENDED TIAA NON-PENSION CONTRACT PROCESSED'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "TOTAL RECORDS:",pnd_In_Cnt);                                                                                                               //Natural: WRITE 'TOTAL RECORDS:' #IN-CNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TIAA-TOTALS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TIAA-WRKSHT-CNTRCT-TOTALS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIAA-WORKSHEET
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-TOTALS
    }
    private void sub_Print_Tiaa_Totals() throws Exception                                                                                                                 //Natural: PRINT-TIAA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #TOT-NDX1 1 #MAX-ORG
        for (pnd_Tot_Ndx1.setValue(1); condition(pnd_Tot_Ndx1.lessOrEqual(pnd_Max_Org)); pnd_Tot_Ndx1.nadd(1))
        {
            if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Origin.getValue(pnd_Tot_Ndx1).equals(getZero())))                                                                  //Natural: IF #W-ORIGIN ( #TOT-NDX1 ) EQ 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Origin_Codes_Pnd_Org_Code.getValue("*")), new ExamineSearch(pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A.getValue(pnd_Tot_Ndx1)),  //Natural: EXAMINE #ORG-CODE ( * ) FOR #W-ORIGIN-A ( #TOT-NDX1 ) GIVING INDEX #A
                new ExamineGivingIndex(pnd_A));
            if (condition(pnd_A.equals(getZero())))                                                                                                                       //Natural: IF #A = 0
            {
                getReports().write(0, "logic error");                                                                                                                     //Natural: WRITE 'logic error'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, pnd_Asterisk);                                                                                                                          //Natural: WRITE ( 1 ) #ASTERISK
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Orgn_Line_Pnd_Orgin_Lit.setValue(pnd_Wrksht_Org_Lit);                                                                                               //Natural: ASSIGN #ORGIN-LIT := #WRKSHT-ORG-LIT
            pnd_Total_Orgn_Line_Pnd_Origin_Cde.setValueEdited(pnd_W_Tiaa_Total_Table_Pnd_W_Origin.getValue(pnd_Tot_Ndx1),new ReportEditMask("99"));                       //Natural: MOVE EDITED #W-ORIGIN ( #TOT-NDX1 ) ( EM = 99 ) TO #ORIGIN-CDE
            pnd_Total_Orgn_Line_Pnd_Origin_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Origin_Codes_Pnd_Org_Name.getValue(pnd_A),              //Natural: COMPRESS '(' #ORG-NAME ( #A ) ')' INTO #ORIGIN-NAME LEAVING NO
                ")"));
            getReports().write(1, pnd_Total_Orgn_Line);                                                                                                                   //Natural: WRITE ( 1 ) #TOTAL-ORGN-LINE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, pnd_Asterisk);                                                                                                                          //Natural: WRITE ( 1 ) #ASTERISK
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #TOT-NDX2 1 #MAX-PRFX
            for (pnd_Tot_Ndx2.setValue(1); condition(pnd_Tot_Ndx2.lessOrEqual(pnd_Max_Prfx)); pnd_Tot_Ndx2.nadd(1))
            {
                FOR03:                                                                                                                                                    //Natural: FOR #TOT-NDX3 1 #MAX-PEND
                for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
                {
                    if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(" ")))                               //Natural: IF #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet428 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TOT-NDX2 = 1
                    if (condition(pnd_Tot_Ndx2.equals(1)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("GA");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'GA'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 2
                    else if (condition(pnd_Tot_Ndx2.equals(2)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("GW");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'GW'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 3
                    else if (condition(pnd_Tot_Ndx2.equals(3)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("IA-IH IJ");                                                                            //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'IA-IH IJ'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 4
                    else if (condition(pnd_Tot_Ndx2.equals(4)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("IP");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'IP'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 5
                    else if (condition(pnd_Tot_Ndx2.equals(5)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("S0");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'S0'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 6
                    else if (condition(pnd_Tot_Ndx2.equals(6)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("W0");                                                                                  //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'W0'
                    }                                                                                                                                                     //Natural: WHEN #TOT-NDX2 = 7
                    else if (condition(pnd_Tot_Ndx2.equals(7)))
                    {
                        decideConditionsMet428++;
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("W0 GROUP");                                                                            //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'W0 GROUP'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("Y0-Y9");                                                                               //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'Y0-Y9'
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr.setValue("1");                                                                                                //Natural: ASSIGN #TIAA-TOT-CURR := '1'
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,                   //Natural: ASSIGN #TIAA-TOT-PEND-CDE := #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                        pnd_Tot_Ndx3));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,             //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                        pnd_Tot_Ndx3));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,                   //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                        pnd_Tot_Ndx3));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,                   //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                        pnd_Tot_Ndx3));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,                    //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                        pnd_Tot_Ndx3));
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3));        //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3));         //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3));          //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3));        //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
                    getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr, //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Written.setValue(true);                                                                                                                           //Natural: ASSIGN #WRITTEN := TRUE
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Written.getBoolean()))                                                                                                                  //Natural: IF #WRITTEN
                {
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue("ALL");                                                                                          //Natural: ASSIGN #TIAA-TOT-PEND-CDE := 'ALL'
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr.setValue("1");                                                                                                //Natural: ASSIGN #TIAA-TOT-CURR := '1'
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));           //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-TOT-CNTRCTS ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));                 //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-TOT-CHKS ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));             //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-TOT-CUR-GUAR ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));              //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-TOT-CUR-DIVD ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));                 //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));                  //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));                   //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2 )
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Total.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2));                   //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-TOTAL ( #TOT-NDX1,#TOT-NDX2 )
                    getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr, //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,
                        pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Written.setValue(false);                                                                                                                          //Natural: ASSIGN #WRITTEN := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, " ");                                                                                                                                   //Natural: WRITE ( 1 ) ' '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR04:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).equals(" ")))                                            //Natural: IF #W-TIAA-FND-DUE-PEND ( #TOT-NDX1,#TOT-NDX3 ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix.setValue("TIAA#");                                                                                       //Natural: ASSIGN #TIAA-TOT-CNTRCT-PREFIX := 'TIAA#'
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr.setValue("1");                                                                                                    //Natural: ASSIGN #TIAA-TOT-CURR := '1'
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-PEND-CDE := #W-TIAA-FND-DUE-PEND ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));           //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-FND-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-FND-DUE-CHKS ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                 //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-FND-DUE-GUAR ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                  //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-FND-DUE-DIVD ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                     //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-FND-GUAR ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                      //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-FND-DIVD ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                       //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-FND-DPI ( #TOT-NDX1,#TOT-NDX3 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3));                     //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-FND-TOT ( #TOT-NDX1,#TOT-NDX3 )
                getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr,    //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Written.setValue(true);                                                                                                                               //Natural: ASSIGN #WRITTEN := TRUE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Written.getBoolean()))                                                                                                                      //Natural: IF #WRITTEN
            {
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde.setValue("ALL");                                                                                              //Natural: ASSIGN #TIAA-TOT-PEND-CDE := 'ALL'
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr.setValue("1");                                                                                                    //Natural: ASSIGN #TIAA-TOT-CURR := '1'
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cntrcts.getValue(pnd_Tot_Ndx1));                        //Natural: ASSIGN #TIAA-TOT-NUM-CNTRCTS := #W-TIAA-FND-TOT-CNTRCTS ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Chks.getValue(pnd_Tot_Ndx1));                              //Natural: ASSIGN #TIAA-TOT-NUM-CHKS := #W-TIAA-FND-TOT-CHKS ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Guar.getValue(pnd_Tot_Ndx1));                          //Natural: ASSIGN #TIAA-TOT-CUR-GUAR := #W-TIAA-FND-TOT-CUR-GUAR ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Divd.getValue(pnd_Tot_Ndx1));                           //Natural: ASSIGN #TIAA-TOT-CUR-DIV := #W-TIAA-FND-TOT-CUR-DIVD ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Guar.getValue(pnd_Tot_Ndx1));                              //Natural: ASSIGN #TIAA-TOT-DUE-GUAR := #W-TIAA-FND-TOT-GUAR ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Divd.getValue(pnd_Tot_Ndx1));                               //Natural: ASSIGN #TIAA-TOT-DUE-DIV := #W-TIAA-FND-TOT-DIVD ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Dpi.getValue(pnd_Tot_Ndx1));                                //Natural: ASSIGN #TIAA-TOT-DUE-DPI := #W-TIAA-FND-TOT-DPI ( #TOT-NDX1 )
                pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total.setValue(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Total.getValue(pnd_Tot_Ndx1));                                //Natural: ASSIGN #TIAA-TOT-DUE-TOTAL := #W-TIAA-FND-TOTAL ( #TOT-NDX1 )
                getReports().write(1, pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Lit,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cntrct_Prefix,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Curr,    //Natural: WRITE ( 1 ) #TIAA-TOTAL-DETAIL
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill1,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Pend_Cde,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill2,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Cntrcts,
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Num_Chks,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Cur_Div,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Fill6,
                    pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Guar,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Div,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Dpi,pnd_Tiaa_Total_Detail_Pnd_Tiaa_Tot_Due_Total);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Written.setValue(false);                                                                                                                              //Natural: ASSIGN #WRITTEN := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Tiaa_Wrksht_Cntrct_Totals() throws Exception                                                                                                   //Natural: PRINT-TIAA-WRKSHT-CNTRCT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Lit.setValue(pnd_Wrksht_Total_Lit);                                                                              //Natural: ASSIGN #TIAA-WRKSHT-TOTAL-LIT := #WRKSHT-TOTAL-LIT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Div_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt,new ReportEditMask("ZZZZZZZ9.99"));                //Natural: MOVE EDITED #TIAA-TOTAL-DIV-AMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DIV-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Gur_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt,new ReportEditMask("ZZZZZZZ9.99"));                //Natural: MOVE EDITED #TIAA-TOTAL-GUR-AMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-GUR-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Pymt.setValueEdited(pnd_Total_Amts_Pnd_Total_Pymt,new ReportEditMask("ZZZZZZZ9.99"));                            //Natural: MOVE EDITED #TOTAL-PYMT ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-PYMT
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Dpi.setValueEdited(pnd_Total_Amts_Pnd_Total_Dpi,new ReportEditMask("ZZZZZ9.99"));                                //Natural: MOVE EDITED #TOTAL-DPI ( EM = ZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DPI
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Due.setValueEdited(pnd_Total_Amts_Pnd_Total_Due,new ReportEditMask("ZZZZZZZZ9.99"));                             //Natural: MOVE EDITED #TOTAL-DUE ( EM = ZZZZZZZZ9.99 ) TO #TIAA-WRKSHT-TOTAL-DUE
        pnd_Tiaa_Wrksht_Total_Line_Pnd_Tiaa_Wrksht_Total_Fill1.setValue(pnd_W_Cntrct_Payee);                                                                              //Natural: ASSIGN #TIAA-WRKSHT-TOTAL-FILL1 := #W-CNTRCT-PAYEE
        getReports().write(1, pnd_Tiaa_Wrksht_Total_Line);                                                                                                                //Natural: WRITE ( 1 ) #TIAA-WRKSHT-TOTAL-LINE
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Process_Tiaa_Worksheet() throws Exception                                                                                                            //Natural: PROCESS-TIAA-WORKSHEET
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee.notEquals(pnd_W_Cntrct_Payee)))                                                                           //Natural: IF #W-WRKSHT-CNTRCT-PAYEE NE #W-CNTRCT-PAYEE
        {
            if (condition(pnd_W_Cntrct_Payee.notEquals(" ")))                                                                                                             //Natural: IF #W-CNTRCT-PAYEE NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-TIAA-WRKSHT-CNTRCT-TOTALS
                sub_Print_Tiaa_Wrksht_Cntrct_Totals();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Wrksht_Detail_Line.reset();                                                                                                                          //Natural: RESET #TIAA-WRKSHT-DETAIL-LINE #TOTAL-AMTS
            pnd_Total_Amts.reset();
            pnd_W_Cntrct_Payee.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct_Payee);                                                                                       //Natural: ASSIGN #W-CNTRCT-PAYEE := #W-WRKSHT-CNTRCT-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct);                                                             //Natural: ASSIGN #TIAA-WRKSHT-DTL-CNTRCT := #W-WRKSHT-CNTRCT
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Payee);                                                                //Natural: ASSIGN #TIAA-WRKSHT-DTL-PYEE := #W-WRKSHT-PAYEE
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Dod.greater(getZero())))                                                                                               //Natural: IF #W-WRKSHT-DOD GT 0
        {
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dod,new ReportEditMask("9999/99"));                       //Natural: MOVE EDITED #W-WRKSHT-DOD ( EM = 9999/99 ) TO #TIAA-WRKSHT-DTL-DOD-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Opt);                                                                  //Natural: ASSIGN #TIAA-WRKSHT-DTL-OPTN := #W-WRKSHT-OPT
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Org,new ReportEditMask("99"));                                    //Natural: MOVE EDITED #W-WRKSHT-ORG ( EM = 99 ) TO #TIAA-WRKSHT-DTL-ORG
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_1st_Dte,new ReportEditMask("XXXX/XX"));                     //Natural: MOVE EDITED #W-WRKSHT-1ST-DTE ( EM = XXXX/XX ) TO #TIAA-WRKSHT-DTL-FIRST-DTE
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);                                                                 //Natural: ASSIGN #TIAA-WRKSHT-DTL-PEND := #W-WRKSHT-PEND
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Mode);                                                                 //Natural: ASSIGN #TIAA-WRKSHT-DTL-MODE := #W-WRKSHT-MODE
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt.notEquals("000000")))                                                                                       //Natural: IF #W-WRKSHT-FINAL-PYMT NE '000000'
        {
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Final_Pymt,new ReportEditMask("XXXX/XX"));               //Natural: MOVE EDITED #W-WRKSHT-FINAL-PYMT ( EM = XXXX/XX ) TO #TIAA-WRKSHT-DTL-PYMT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd.setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Fund);                                                                  //Natural: ASSIGN #TIAA-WRKSHT-DTL-FND := #W-WRKSHT-FUND
        FOR05:                                                                                                                                                            //Natural: FOR #CHK-CNT 1 #MAX-WRK
        for (pnd_Chk_Cnt.setValue(1); condition(pnd_Chk_Cnt.lessOrEqual(pnd_Max_Wrk)); pnd_Chk_Cnt.nadd(1))
        {
            if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_Chk_Cnt).equals(" ")))                                                                        //Natural: IF #W-WRKSHT-DUE-DTE ( #CHK-CNT ) = ' '
            {
                pnd_Chk_Cnt.nsubtract(1);                                                                                                                                 //Natural: SUBTRACT 1 FROM #CHK-CNT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wrksht_File_Pnd_W_Wrksht_Due_Mm.getValue(pnd_Chk_Cnt),  //Natural: COMPRESS #W-WRKSHT-DUE-MM ( #CHK-CNT ) '/' #W-WRKSHT-DUE-YYYY ( #CHK-CNT ) INTO #TIAA-WRKSHT-DTL-DUE-DTE LEAVING NO
                "/", pnd_Wrksht_File_Pnd_W_Wrksht_Due_Yyyy.getValue(pnd_Chk_Cnt)));
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt),new ReportEditMask("ZZZZZZ9.99"));  //Natural: MOVE EDITED #W-WRKSHT-DIV ( #CHK-CNT ) ( EM = ZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-DIV
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt),new ReportEditMask("ZZZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-GUAR ( #CHK-CNT ) ( EM = ZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-GUAR
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_Chk_Cnt),new ReportEditMask("ZZZZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-TOT-PYMT ( #CHK-CNT ) ( EM = ZZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-TOT-PYMT
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Fact.getValue(pnd_Chk_Cnt),new ReportEditMask("9.99999")); //Natural: MOVE EDITED #W-WRKSHT-DPI-FACT ( #CHK-CNT ) ( EM = 9.99999 ) TO #TIAA-WRKSHT-DTL-FACT
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_Chk_Cnt),new ReportEditMask("ZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-DPI-AMT ( #CHK-CNT ) ( EM = ZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-DPI
            pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due.setValueEdited(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_Chk_Cnt),new ReportEditMask("ZZZZZZZZ9.99")); //Natural: MOVE EDITED #W-WRKSHT-TOTAL ( #CHK-CNT ) ( EM = ZZZZZZZZ9.99 ) TO #TIAA-WRKSHT-DTL-TOT-DUE
            pnd_Total_Amts_Pnd_Tiaa_Total_Div_Amt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt));                                                           //Natural: ASSIGN #TIAA-TOTAL-DIV-AMT := #TIAA-TOTAL-DIV-AMT + #W-WRKSHT-DIV ( #CHK-CNT )
            pnd_Total_Amts_Pnd_Tiaa_Total_Gur_Amt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt));                                                          //Natural: ASSIGN #TIAA-TOTAL-GUR-AMT := #TIAA-TOTAL-GUR-AMT + #W-WRKSHT-GUAR ( #CHK-CNT )
            pnd_Total_Amts_Pnd_Total_Pymt.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Tot_Pymt.getValue(pnd_Chk_Cnt));                                                              //Natural: ASSIGN #TOTAL-PYMT := #TOTAL-PYMT + #W-WRKSHT-TOT-PYMT ( #CHK-CNT )
            pnd_Total_Amts_Pnd_Total_Dpi.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue(pnd_Chk_Cnt));                                                                //Natural: ASSIGN #TOTAL-DPI := #TOTAL-DPI + #W-WRKSHT-DPI-AMT ( #CHK-CNT )
            pnd_Total_Amts_Pnd_Total_Due.nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue(pnd_Chk_Cnt));                                                                  //Natural: ASSIGN #TOTAL-DUE := #TOTAL-DUE + #W-WRKSHT-TOTAL ( #CHK-CNT )
            getReports().write(1, pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Cntrct,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pyee,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Optn, //Natural: WRITE ( 1 ) #TIAA-WRKSHT-DETAIL-LINE
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Org,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dod_Dte,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_First_Dte,
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pend,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill7,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Mode,
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Pymt_Dte,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fnd,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Due_Dte,
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Guar,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill11,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Div,
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Pymt,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fill13,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Fact,
                pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Dpi,pnd_Tiaa_Wrksht_Detail_Line_Pnd_Tiaa_Wrksht_Dtl_Tot_Due);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM LOAD-TOTALS
        sub_Load_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Load_Totals() throws Exception                                                                                                                       //Natural: LOAD-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Org.equals(pnd_W_Tiaa_Total_Table_Pnd_W_Origin.getValue("*"))))                                                        //Natural: IF #W-WRKSHT-ORG = #W-ORIGIN ( * )
        {
            DbsUtil.examine(new ExamineSource(pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A.getValue("*")), new ExamineSearch(pnd_Wrksht_File_Pnd_W_Wrksht_Org_A),                //Natural: EXAMINE #W-ORIGIN-A ( * ) FOR #W-WRKSHT-ORG-A GIVING INDEX #TOT-NDX1
                new ExamineGivingIndex(pnd_Tot_Ndx1));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR06:                                                                                                                                                        //Natural: FOR #TOT-NDX1 1 #MAX-ORG
            for (pnd_Tot_Ndx1.setValue(1); condition(pnd_Tot_Ndx1.lessOrEqual(pnd_Max_Org)); pnd_Tot_Ndx1.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A.getValue(pnd_Tot_Ndx1).equals(" ") || pnd_W_Tiaa_Total_Table_Pnd_W_Origin_A.getValue(pnd_Tot_Ndx1).equals("00"))) //Natural: IF #W-ORIGIN-A ( #TOT-NDX1 ) = ' ' OR = '00'
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Origin.getValue(pnd_Tot_Ndx1).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Org);                                                //Natural: ASSIGN #W-ORIGIN ( #TOT-NDX1 ) := #W-WRKSHT-ORG
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet576 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'GA' )
        if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'GA'")))
        {
            decideConditionsMet576++;
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ga_0l_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX2 := #GA-0L-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'GW' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'GW'")))
        {
            decideConditionsMet576++;
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Gw_0m_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX2 := #GW-0M-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'IP' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'IP'")))
        {
            decideConditionsMet576++;
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ip_6l_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX2 := #IP-6L-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'I' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'I'")))
        {
            decideConditionsMet576++;
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Ia_0n_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX2 := #IA-0N-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'S0' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'S0'")))
        {
            decideConditionsMet576++;
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_S0_6m_Ndx);                                                                                           //Natural: ASSIGN #TOT-NDX2 := #S0-6M-NDX
        }                                                                                                                                                                 //Natural: WHEN #W-WRKSHT-CNTRCT = MASK ( 'W0' )
        else if (condition(DbsUtil.maskMatches(pnd_Wrksht_File_Pnd_W_Wrksht_Cntrct,"'W0'")))
        {
            decideConditionsMet576++;
            //*  GROUP
            if (condition(pnd_W_Orign.equals(4)))                                                                                                                         //Natural: IF #W-ORIGN = 04
            {
                pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_Grp_Ndx);                                                                                      //Natural: ASSIGN #TOT-NDX2 := #W0-GRP-NDX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_W0_6n_Ndx);                                                                                       //Natural: ASSIGN #TOT-NDX2 := #W0-6N-NDX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Tot_Ndx2.setValue(pnd_Ndxs_For_Tiaa_Total_Table_Pnd_Y0_Ndx);                                                                                              //Natural: ASSIGN #TOT-NDX2 := #Y0-NDX
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Pend.equals(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,"*"))))                      //Natural: IF #W-WRKSHT-PEND = #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,* )
        {
            FOR07:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(pnd_Wrksht_File_Pnd_W_Wrksht_Pend)))     //Natural: IF #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = #W-WRKSHT-PEND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR08:                                                                                                                                                        //Natural: FOR #TOT-NDX3 1 #MAX-PEND
            for (pnd_Tot_Ndx3.setValue(1); condition(pnd_Tot_Ndx3.lessOrEqual(pnd_Max_Pend)); pnd_Tot_Ndx3.nadd(1))
            {
                if (condition(pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).equals(" ")))                                   //Natural: IF #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) = ' '
                {
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);              //Natural: ASSIGN #W-TIAA-DUE-PEND ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-WRKSHT-PEND
                    pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Pend.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).setValue(pnd_Wrksht_File_Pnd_W_Wrksht_Pend);                       //Natural: ASSIGN #W-TIAA-FND-DUE-PEND ( #TOT-NDX1,#TOT-NDX3 ) := #W-WRKSHT-PEND
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(1);                                                           //Natural: ADD 1 TO #W-TIAA-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(1);                                                                    //Natural: ADD 1 TO #W-TIAA-FND-DUE-CNTRCTS ( #TOT-NDX1,#TOT-NDX3 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cntrcts.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(1);                                                                        //Natural: ADD 1 TO #W-TIAA-TOT-CNTRCTS ( #TOT-NDX1,#TOT-NDX2 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cntrcts.getValue(pnd_Tot_Ndx1).nadd(1);                                                                                 //Natural: ADD 1 TO #W-TIAA-FND-TOT-CNTRCTS ( #TOT-NDX1 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Chk_Cnt);                                                    //Natural: ADD #CHK-CNT TO #W-TIAA-DUE-CHKS ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Chk_Cnt);                                                             //Natural: ADD #CHK-CNT TO #W-TIAA-FND-DUE-CHKS ( #TOT-NDX1,#TOT-NDX3 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Chks.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Chk_Cnt);                                                                 //Natural: ADD #CHK-CNT TO #W-TIAA-TOT-CHKS ( #TOT-NDX1,#TOT-NDX2 )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Chks.getValue(pnd_Tot_Ndx1).nadd(pnd_Chk_Cnt);                                                                          //Natural: ADD #CHK-CNT TO #W-TIAA-FND-TOT-CHKS ( #TOT-NDX1 )
        //*  120110
        if (condition(pnd_Wrksht_File_Pnd_W_Wrksht_Due_Dte.getValue(pnd_Chk_Cnt).equals(pnd_W_Chk_Dte_Pnd_W_Chk_Yyyymm)))                                                 //Natural: IF #W-WRKSHT-DUE-DTE ( #CHK-CNT ) = #W-CHK-YYYYMM
        {
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt));    //Natural: ASSIGN #W-TIAA-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-DUE-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt));             //Natural: ASSIGN #W-TIAA-FND-DUE-GUAR ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-DUE-GUAR ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt));     //Natural: ASSIGN #W-TIAA-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-DUE-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DIV ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Due_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt));              //Natural: ASSIGN #W-TIAA-FND-DUE-DIVD ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-DUE-DIVD ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-DIV ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt));             //Natural: ASSIGN #W-TIAA-TOT-CUR-GUAR ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOT-CUR-GUAR ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-GUAR ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Guar.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue(pnd_Chk_Cnt));                      //Natural: ASSIGN #W-TIAA-FND-TOT-CUR-GUAR ( #TOT-NDX1 ) := #W-TIAA-FND-TOT-CUR-GUAR ( #TOT-NDX1 ) + #W-WRKSHT-GUAR ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Cur_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt));              //Natural: ASSIGN #W-TIAA-TOT-CUR-DIVD ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOT-CUR-DIVD ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-DIV ( #CHK-CNT )
            pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Cur_Divd.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue(pnd_Chk_Cnt));                       //Natural: ASSIGN #W-TIAA-FND-TOT-CUR-DIVD ( #TOT-NDX1 ) := #W-TIAA-FND-TOT-CUR-DIVD ( #TOT-NDX1 ) + #W-WRKSHT-DIV ( #CHK-CNT )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue("*"));                    //Natural: ASSIGN #W-TIAA-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-GUAR ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue("*"));                             //Natural: ASSIGN #W-TIAA-FND-GUAR ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-GUAR ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-GUAR ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Guar.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue("*"));                             //Natural: ASSIGN #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOT-GUAR ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-GUAR ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Guar.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Guar.getValue("*"));                                      //Natural: ASSIGN #W-TIAA-FND-TOT-GUAR ( #TOT-NDX1 ) := #W-TIAA-FND-TOT-GUAR ( #TOT-NDX1 ) + #W-WRKSHT-GUAR ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue("*"));                     //Natural: ASSIGN #W-TIAA-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-DIVD ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DIV ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue("*"));                              //Natural: ASSIGN #W-TIAA-FND-DIVD ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-DIVD ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-DIV ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Divd.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue("*"));                              //Natural: ASSIGN #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOT-DIVD ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-DIV ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Divd.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Div.getValue("*"));                                       //Natural: ASSIGN #W-TIAA-FND-TOT-DIVD ( #TOT-NDX1 ) := #W-TIAA-FND-TOT-DIVD ( #TOT-NDX1 ) + #W-WRKSHT-DIV ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue("*"));                  //Natural: ASSIGN #W-TIAA-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-DPI ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-DPI-AMT ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue("*"));                           //Natural: ASSIGN #W-TIAA-FND-DPI ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-DPI ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-DPI-AMT ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot_Dpi.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue("*"));                           //Natural: ASSIGN #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOT-DPI ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-DPI-AMT ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot_Dpi.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Dpi_Amt.getValue("*"));                                    //Natural: ASSIGN #W-TIAA-FND-TOT-DPI ( #TOT-NDX1 ) := #W-TIAA-FND-TOT-DPI ( #TOT-NDX1 ) + #W-WRKSHT-DPI-AMT ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue("*"));                    //Natural: ASSIGN #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) := #W-TIAA-TOT ( #TOT-NDX1,#TOT-NDX2,#TOT-NDX3 ) + #W-WRKSHT-TOTAL ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Tot.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx3).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue("*"));                             //Natural: ASSIGN #W-TIAA-FND-TOT ( #TOT-NDX1,#TOT-NDX3 ) := #W-TIAA-FND-TOT ( #TOT-NDX1,#TOT-NDX3 ) + #W-WRKSHT-TOTAL ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Total.getValue(pnd_Tot_Ndx1,pnd_Tot_Ndx2).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue("*"));                               //Natural: ASSIGN #W-TIAA-TOTAL ( #TOT-NDX1,#TOT-NDX2 ) := #W-TIAA-TOTAL ( #TOT-NDX1,#TOT-NDX2 ) + #W-WRKSHT-TOTAL ( * )
        pnd_W_Tiaa_Total_Table_Pnd_W_Tiaa_Fnd_Total.getValue(pnd_Tot_Ndx1).nadd(pnd_Wrksht_File_Pnd_W_Wrksht_Total.getValue("*"));                                        //Natural: ASSIGN #W-TIAA-FND-TOTAL ( #TOT-NDX1 ) := #W-TIAA-FND-TOTAL ( #TOT-NDX1 ) + #W-WRKSHT-TOTAL ( * )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Product.setValue("TIAA");                                                                                          //Natural: ASSIGN #WRKSHT-PRODUCT := #TOT-HDR1M5-PRDCT := 'TIAA'
                    pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Prdct.setValue("TIAA");
                    if (condition(pnd_Totals.getBoolean()))                                                                                                               //Natural: IF #TOTALS
                    {
                        pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Chk_Dte_Pnd_W_Chk_Mm,          //Natural: COMPRESS #W-CHK-MM '/' #W-CHK-YYYY INTO #TIAA-COLMN-HDR2-TOT-DTE LEAVING NO
                            "/", pnd_W_Chk_Dte_Pnd_W_Chk_Yyyy));
                        pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdrm5_Var.setValue(pnd_Ia3300_Hdr_Lit);                                                                          //Natural: ASSIGN #TOT-HDRM5-VAR := #IA3300-HDR-LIT
                        getReports().write(1, pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Fill1m5_Var,pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Fill1,pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Var, //Natural: WRITE ( 1 ) #HDR-TOT-LIT-IA3300M5
                            pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Prdct,pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdrm5_Var,pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Chk,
                            pnd_Hdr_Tot_Lit_Ia3300m5_Pnd_Tot_Hdr1m5_Date);
                        getReports().write(1, pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Fill,pnd_Tiaa_Colmn_Hdr1_Tot_Pnd_Tiaa_Colmn_Hdr1_Tot_Lit);                  //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR1-TOT
                        getReports().write(1, pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit1,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill1,                 //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR2-TOT
                            pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit2,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill2,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit3,
                            pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Fill3,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Lit4,pnd_Tiaa_Colmn_Hdr2_Tot_Pnd_Tiaa_Colmn_Hdr2_Tot_Dte);
                        getReports().write(1, pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit1,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill1,                 //Natural: WRITE ( 1 ) #TIAA-COLMN-HDR3-TOT
                            pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit2,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Fill2,pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit3,
                            pnd_Tiaa_Colmn_Hdr3_Tot_Pnd_Tiaa_Colmn_Hdr3_Tot_Lit4);
                        //*   #WRKSHT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Hdrm5.setValue(pnd_Ia3300_Hdr_Lit);                                                                            //Natural: ASSIGN #WRKSHT-HDRM5 := #IA3300-HDR-LIT
                        getReports().write(1, pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Fill1m5,pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Product,pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Hdrm5, //Natural: WRITE ( 1 ) #HDR-WRKSHT-IA3300M5
                            pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Lit1,pnd_Hdr_Wrksht_Ia3300m5_Pnd_Wrksht_Date);
                        getReports().write(1, pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Fill1,pnd_Wrksht_Hdr1_Pnd_Wrksht_Hdr1_Lit);                                                 //Natural: WRITE ( 1 ) #WRKSHT-HDR1
                        getReports().write(1, pnd_Tiaa_Wrksht_Hdr2);                                                                                                      //Natural: WRITE ( 1 ) #TIAA-WRKSHT-HDR2
                        getReports().write(1, pnd_Tiaa_Wrksht_Hdr3);                                                                                                      //Natural: WRITE ( 1 ) #TIAA-WRKSHT-HDR3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
