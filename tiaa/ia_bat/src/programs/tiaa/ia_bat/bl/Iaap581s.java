/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:52 PM
**        * FROM NATURAL PROGRAM : Iaap581s
************************************************************
**        * FILE NAME            : Iaap581s.java
**        * CLASS NAME           : Iaap581s
**        * INSTANCE NAME        : Iaap581s
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM   -  IAAP581S   SELECT COMPLETED INITIAL DEATH TRANSACT. *
*      DATE   -  02/95      FOR A SPECIFIC CHECK CYCLE BASED ON PARM *
*    AUTHOR   -  ARI G.     DATE. WRITE OUT TO 2 WORK FILES          *
*                           1) NO FURTHUR PAYMENTS                   *
*                           2) FURTHUR PAYMENTS (EXCLUDING CONTRACTS *
*                              THAT CONTINUE THE SAME PAYMENTS)      *
*   HISTORY  :                                                       *
*               06/19/97   REPLACED PIN FILE WITH CORE FILE          *
*               10/10/2000 SPLIT FFP WORKFILE INTO WORK 2 (SURVIVOR) *
*                          AND WORK3 (BENEFICIARY)                   *
*               10/11/2001 ADD OPTION CODE 22,25,27,28 & 30 TO BE    *
*                          WRITTEN OUT IN WORK FILE3 (BENEFICIARY)
* 03/26/10  O SOTTO USE THE NEW DESCRIPTOR TO READ THE STTLMNT FILE. *
*                   SC 032610.
* 08/25/15  O SOTTO ADDED #W-NFP INDICATOR AND CALL MDM INSTEAD OF
*                   COR.  CHANGES MARKED 082515.
* 04/2017   O SOTTO PIN EXPANSION - SC 082017 FOR CHANGES.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap581s extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private LdaIaal581s ldaIaal581s;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Pin_Key;

    private DbsGroup pnd_Pin_Key__R_Field_1;
    private DbsField pnd_Pin_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Key_Pnd_Rec_Type;
    private DbsField pnd_Cntrct_Optn_Array1;
    private DbsField pnd_Cntrct_Optn_Array2;
    private DbsField pnd_Datd;
    private DbsField pnd_I;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        ldaIaal581s = new LdaIaal581s();
        registerRecord(ldaIaal581s);
        registerRecord(ldaIaal581s.getVw_iaa_Cntrct_Prtcpnt_Role_View());
        registerRecord(ldaIaal581s.getVw_iaa_Dc_Cntrct_View());
        registerRecord(ldaIaal581s.getVw_iaa_Sttlmnt_View());

        // Local Variables
        pnd_Pin_Key = localVariables.newFieldInRecord("pnd_Pin_Key", "#PIN-KEY", FieldType.BINARY, 14);

        pnd_Pin_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Key__R_Field_1", "REDEFINE", pnd_Pin_Key);
        pnd_Pin_Key_Pnd_Pin_Nbr = pnd_Pin_Key__R_Field_1.newFieldInGroup("pnd_Pin_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Pin_Key_Pnd_Rec_Type = pnd_Pin_Key__R_Field_1.newFieldInGroup("pnd_Pin_Key_Pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Optn_Array1 = localVariables.newFieldArrayInRecord("pnd_Cntrct_Optn_Array1", "#CNTRCT-OPTN-ARRAY1", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            11));
        pnd_Cntrct_Optn_Array2 = localVariables.newFieldArrayInRecord("pnd_Cntrct_Optn_Array2", "#CNTRCT-OPTN-ARRAY2", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            9));
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal581s.initializeValues();

        localVariables.reset();
        pnd_Cntrct_Optn_Array1.getValue(1).setInitialValue(2);
        pnd_Cntrct_Optn_Array1.getValue(2).setInitialValue(5);
        pnd_Cntrct_Optn_Array1.getValue(3).setInitialValue(6);
        pnd_Cntrct_Optn_Array1.getValue(4).setInitialValue(9);
        pnd_Cntrct_Optn_Array1.getValue(5).setInitialValue(19);
        pnd_Cntrct_Optn_Array1.getValue(6).setInitialValue(21);
        pnd_Cntrct_Optn_Array1.getValue(7).setInitialValue(22);
        pnd_Cntrct_Optn_Array1.getValue(8).setInitialValue(25);
        pnd_Cntrct_Optn_Array1.getValue(9).setInitialValue(27);
        pnd_Cntrct_Optn_Array1.getValue(10).setInitialValue(28);
        pnd_Cntrct_Optn_Array1.getValue(11).setInitialValue(30);
        pnd_Cntrct_Optn_Array2.getValue(1).setInitialValue(8);
        pnd_Cntrct_Optn_Array2.getValue(2).setInitialValue(10);
        pnd_Cntrct_Optn_Array2.getValue(3).setInitialValue(11);
        pnd_Cntrct_Optn_Array2.getValue(4).setInitialValue(12);
        pnd_Cntrct_Optn_Array2.getValue(5).setInitialValue(13);
        pnd_Cntrct_Optn_Array2.getValue(6).setInitialValue(14);
        pnd_Cntrct_Optn_Array2.getValue(7).setInitialValue(15);
        pnd_Cntrct_Optn_Array2.getValue(8).setInitialValue(16);
        pnd_Cntrct_Optn_Array2.getValue(9).setInitialValue(17);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap581s() throws Exception
    {
        super("Iaap581s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  082515 OPEN MDM QUEUE
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //*     PERFORM #SETUP-SYSTEM-DATE-TIME
        getReports().write(0, "*** START OF PROGRAM IAAP581S *** ");                                                                                                      //Natural: WRITE '*** START OF PROGRAM IAAP581S *** '
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 4 IAA-PARM-CARD
        while (condition(getWorkFiles().read(4, ldaIaal581s.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        //*  032610
        //*  032610
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  START OF MAIN PROCESS
        //* ***********************************************************************
        //* *. READ IAA-STTLMNT-VIEW PHYSICAL
        ldaIaal581s.getVw_iaa_Sttlmnt_View().startDatabaseRead                                                                                                            //Natural: READ IAA-STTLMNT-VIEW BY STTLMNT-STATUS-TIMESTAMP STARTING FROM #DATD
        (
        "RD",
        new Wc[] { new Wc("STTLMNT_STATUS_TIMESTAMP", ">=", pnd_Datd, WcType.BY) },
        new Oc[] { new Oc("STTLMNT_STATUS_TIMESTAMP", "ASC") }
        );
        RD:
        while (condition(ldaIaal581s.getVw_iaa_Sttlmnt_View().readNextRow("RD")))
        {
            ldaIaal581s.getPnd_Sttlmnt_Reads().nadd(1);                                                                                                                   //Natural: ADD 1 TO #STTLMNT-READS
            ldaIaal581s.getPnd_Fl_Date_Yyyymmdd_Alph().setValueEdited(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Status_Timestamp(),new ReportEditMask("YYYYMMDD"));         //Natural: MOVE EDITED STTLMNT-STATUS-TIMESTAMP ( EM = YYYYMMDD ) TO #FL-DATE-YYYYMMDD-ALPH
            //*  032610
            if (condition(ldaIaal581s.getPnd_Fl_Date_Yyyymmdd_Alph_Pnd_Fl_Date_Yyyymmdd_Num().notEquals(ldaIaal581s.getIaa_Parm_Card_Pnd_Parm_Date_N())))                 //Natural: IF #FL-DATE-YYYYMMDD-NUM NE #PARM-DATE-N
            {
                //*  032610
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  032610
            }                                                                                                                                                             //Natural: END-IF
            //*  ACCEPT IF #FL-DATE-YYYYMMDD-NUM EQ #PARM-DATE-N          /* 032610
            ldaIaal581s.getPnd_Sttlmnt_Time_Selects().nadd(1);                                                                                                            //Natural: ADD 1 TO #STTLMNT-TIME-SELECTS
            if (condition((ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Process_Type().equals("ID1A") || ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Process_Type().equals("ID2A") //Natural: IF ( STTLMNT-PROCESS-TYPE = 'ID1A' OR STTLMNT-PROCESS-TYPE = 'ID2A' OR STTLMNT-PROCESS-TYPE = 'ID3A' ) AND STTLMNT-STATUS-CDE = '99'
                || ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Process_Type().equals("ID3A")) && ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Status_Cde().equals("99")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal581s.getPnd_Sttlmnt_Id_Complete_Selects().nadd(1);                                                                                                     //Natural: ADD 1 TO #STTLMNT-ID-COMPLETE-SELECTS
                                                                                                                                                                          //Natural: PERFORM #MOVE-SETTLEMENT-FIELDS
            sub_Pnd_Move_Settlement_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     PERFORM #ACCESS-PIN-NAME
                                                                                                                                                                          //Natural: PERFORM #ACCESS-CORE-NAME
            sub_Pnd_Access_Core_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #ACCESS-CONTRACTS-AND-PROCESS
            sub_Pnd_Access_Contracts_And_Process();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #RESET-PARA
            sub_Pnd_Reset_Para();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-DISPLAYS
        sub_Pnd_Write_Out_Displays();
        if (condition(Global.isEscape())) {return;}
        //*  082515 CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "*** END OF PROGRAM IAAP581S *** ");                                                                                                        //Natural: WRITE '*** END OF PROGRAM IAAP581S *** '
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-DISPLAYS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #MOVE-SETTLEMENT-FIELDS
        //* ********************************************************************
        //*  082515 START
        //* *******************************************************************
        //* *DEFINE SUBROUTINE #ACCESS-PIN-NAME
        //* *******************************************************************
        //* *FN1. FIND PH-INFORMATION-FILE-VIEW PH-UNIQUE-ID-NO = #STTLMNT-ID-NBR
        //* *  IF NO RECORDS FOUND
        //* *    WRITE '!!! ' #STTLMNT-ID-NBR 'NOT FOUND ON PIN FILE !!!'
        //* *    MOVE ' ' TO #W-NAME
        //* *    ESCAPE BOTTOM(FN1.)
        //* *  END-NOREC
        //* *  MOVE PH-FIRST-NAME  TO #W-FIRST-NAME
        //* *  MOVE PH-LAST-NAME   TO #W-LAST-NAME
        //* *  MOVE PH-MIDDLE-NAME TO #W-MIDDLE-NAME
        //* *  ADD 1 TO #PH-INFO-READS
        //* *END-FIND
        //* *END-SUBROUTINE
        //*  082515 END
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ACCESS-CORE-NAME
        //* *#I-PIN := #STTLMNT-ID-NBR
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #ACCESS-CONTRACTS-AND-PROCESS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-FURTHUR-PAYMENT-AND-WRITE-REC
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TEST-FFP-CONDITIONS
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-SYSTEM-DATE-TIME
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-NFP-REC
        //* *******************************************************************
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-OUT-FFP-REC
        //* *******************************************************************
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #NFP-03-DECEDENT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FFP-03-DECEDENT
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PARA
        //* ********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* ********************************************************************
        //*  82515 START
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  082515 END
    }
    private void sub_Pnd_Write_Out_Displays() throws Exception                                                                                                            //Natural: #WRITE-OUT-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "========================================================");                                                                                //Natural: WRITE '========================================================'
        if (Global.isEscape()) return;
        getReports().write(0, "  STTLMNT FILE READS ==========> ",ldaIaal581s.getPnd_Sttlmnt_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                //Natural: WRITE '  STTLMNT FILE READS ==========> ' #STTLMNT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  STTLMNT TIME SELECTS ========> ",ldaIaal581s.getPnd_Sttlmnt_Time_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                         //Natural: WRITE '  STTLMNT TIME SELECTS ========> ' #STTLMNT-TIME-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  STTLMNT COMPL INIT DTH SLCT => ",ldaIaal581s.getPnd_Sttlmnt_Id_Complete_Selects(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                  //Natural: WRITE '  STTLMNT COMPL INIT DTH SLCT => ' #STTLMNT-ID-COMPLETE-SELECTS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  PH-INFORMATION READS ========> ",ldaIaal581s.getPnd_Ph_Info_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                //Natural: WRITE '  PH-INFORMATION READS ========> ' #PH-INFO-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  CONTRACT READS ==============> ",ldaIaal581s.getPnd_Contract_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE '  CONTRACT READS ==============> ' #CONTRACT-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  CONTRACT PARTICIPANT READS ==> ",ldaIaal581s.getPnd_Contract_Partic_Reads(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                        //Natural: WRITE '  CONTRACT PARTICIPANT READS ==> ' #CONTRACT-PARTIC-READS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  03 DECEDENT NFP RECORDS =====> ",ldaIaal581s.getPnd_Nfp_03_Deced(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE '  03 DECEDENT NFP RECORDS =====> ' #NFP-03-DECED ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  03 DECEDENT FFP RECORDS =====> ",ldaIaal581s.getPnd_Ffp_03_Deced(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE '  03 DECEDENT FFP RECORDS =====> ' #FFP-03-DECED ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  FFP BUT DONT WRITE RECORD ===> ",ldaIaal581s.getPnd_Ffp_But_Dont_Write(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                           //Natural: WRITE '  FFP BUT DONT WRITE RECORD ===> ' #FFP-BUT-DONT-WRITE ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  NFP RECORDS WRITTEN =========> ",ldaIaal581s.getPnd_Work_Record_Writes_1(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                         //Natural: WRITE '  NFP RECORDS WRITTEN =========> ' #WORK-RECORD-WRITES-1 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  FFP RECORDS WRITTEN SUR =====> ",ldaIaal581s.getPnd_Work_Record_Writes_2(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                         //Natural: WRITE '  FFP RECORDS WRITTEN SUR =====> ' #WORK-RECORD-WRITES-2 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "  FFP RECORDS WRITTEN BEN =====> ",ldaIaal581s.getPnd_Work_Record_Writes_3(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                         //Natural: WRITE '  FFP RECORDS WRITTEN BEN =====> ' #WORK-RECORD-WRITES-3 ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "========================================================");                                                                                //Natural: WRITE '========================================================'
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Move_Settlement_Fields() throws Exception                                                                                                        //Natural: #MOVE-SETTLEMENT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Work_Record_Pnd_W_User_Area().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Oper_User_Grp());                                                       //Natural: MOVE IAA-STTLMNT-VIEW.OPER-USER-GRP TO #W-USER-AREA
        ldaIaal581s.getPnd_Work_Record_Pnd_W_User_Id().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Oper_Id());                                                               //Natural: MOVE IAA-STTLMNT-VIEW.OPER-ID TO #W-USER-ID
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Name().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Name());                                             //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-NAME TO #W-NOTIFY-NAME
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Addr1().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr1());                                           //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-ADDR1 TO #W-NOTIFY-ADDR1
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Addr2().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr2());                                           //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-ADDR2 TO #W-NOTIFY-ADDR2
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Addr3().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr3());                                           //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-ADDR3 TO #W-NOTIFY-ADDR3
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Addr4().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Addr4());                                           //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-ADDR4 TO #W-NOTIFY-ADDR4
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_City().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_City());                                             //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-CITY TO #W-NOTIFY-CITY
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_State().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_State());                                           //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-STATE TO #W-NOTIFY-STATE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Notify_Zip().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Notifier_Zip());                                               //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-NOTIFIER-ZIP TO #W-NOTIFY-ZIP
        ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_1().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte());                                                           //Natural: MOVE IAA-STTLMNT-VIEW.#STTLMNT-DOD-DTE TO #STTLMNT-DOD-DTE-1
        ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_2nd_Dod_Dte());                                                       //Natural: MOVE IAA-STTLMNT-VIEW.#STTLMNT-2ND-DOD-DTE TO #STTLMNT-DOD-DTE-2
        //*   MOVE IAA-STTLMNT-VIEW.STTLMNT-DECEDENT-TYPE   TO
        ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type());                                                    //Natural: MOVE IAA-STTLMNT-VIEW.#STTLMNT-DCDNT-TYPE TO #STTLMNT-DECEDENT-TYPE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Pin().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Id_Nbr());                                                            //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-ID-NBR TO #W-PIN #STTLMNT-ID-NBR
        ldaIaal581s.getPnd_Sttlmnt_Id_Nbr().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Id_Nbr());
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Ssn().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Tax_Id_Nbr_A());                                                  //Natural: MOVE IAA-STTLMNT-VIEW.#STTLMNT-TAX-ID-NBR-A TO #W-SSN
        ldaIaal581s.getPnd_Sttlmnt_Ssn().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Tax_Id_Nbr());                                                                  //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-TAX-ID-NBR TO #STTLMNT-SSN
        ldaIaal581s.getPnd_Sttlmnt_Seq_Nbr().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Sttlmnt_Req_Seq_Nbr());                                                             //Natural: MOVE IAA-STTLMNT-VIEW.STTLMNT-REQ-SEQ-NBR TO #STTLMNT-SEQ-NBR
    }
    private void sub_Pnd_Access_Core_Name() throws Exception                                                                                                              //Natural: #ACCESS-CORE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  082515 START
        //* *#PIN-NBR := #STTLMNT-ID-NBR
        //* *#REC-TYPE := 01
        //* *READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE STARTING FROM #PIN-KEY
        //* *  IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE #STTLMNT-ID-NBR OR
        //* *      COR-XREF-PH.PH-RCD-TYPE-CDE NE 01
        //* *    ESCAPE BOTTOM
        //* *  END-IF
        //* *  MOVE PH-FIRST-NME  TO #W-FIRST-NAME
        //* *  MOVE PH-LAST-NME   TO #W-LAST-NAME
        //* *  COMPRESS PH-MDDLE-NME PH-SFFX-NME INTO #W-MIDDLE-NAME
        //* *  ADD 1 TO #PH-INFO-READS
        //* *END-READ
        //* *SET #MDMA100                                   /* 082017
        //*  082017
        //*  082017
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101 #W-NAME
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Name().reset();
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(ldaIaal581s.getPnd_Sttlmnt_Id_Nbr());                                                                          //Natural: ASSIGN #I-PIN-N12 := #STTLMNT-ID-NBR
        //* *LLNAT 'MDMN100A' #MDMA100                      /* 082017
        //*  082017
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #O-RETURN-CODE EQ '0000'
        {
            ldaIaal581s.getPnd_Work_Record_Pnd_W_First_Name().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                                     //Natural: ASSIGN #W-FIRST-NAME := #O-FIRST-NAME
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Last_Name().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                       //Natural: ASSIGN #W-LAST-NAME := #O-LAST-NAME
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Middle_Name().setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix())); //Natural: COMPRESS #O-MIDDLE-NAME #O-SUFFIX INTO #W-MIDDLE-NAME
            ldaIaal581s.getPnd_Ph_Info_Reads().nadd(1);                                                                                                                   //Natural: ADD 1 TO #PH-INFO-READS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  WRITE 'BAD RETURN FROM MDMN100A. RC=' #O-RETURN-CODE /* 082017
            //*  082017
            getReports().write(0, "BAD RETURN FROM MDMN101A. RC=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                                                         //Natural: WRITE 'BAD RETURN FROM MDMN101A. RC=' #O-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text(), new AlphanumericLength (70));                                                            //Natural: WRITE #O-RETURN-TEXT ( AL = 70 )
            if (Global.isEscape()) return;
            getReports().write(0, "PIN=",ldaIaal581s.getPnd_Sttlmnt_Id_Nbr());                                                                                            //Natural: WRITE 'PIN=' #STTLMNT-ID-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  082515 END
    }
    private void sub_Pnd_Access_Contracts_And_Process() throws Exception                                                                                                  //Natural: #ACCESS-CONTRACTS-AND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Tax_Id_Nbr_S().setValue(ldaIaal581s.getPnd_Sttlmnt_Ssn());                                                //Natural: MOVE #STTLMNT-SSN TO #PTSCK-TAX-ID-NBR-S #PTSCK-TAX-ID-NBR-E
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Tax_Id_Nbr_E().setValue(ldaIaal581s.getPnd_Sttlmnt_Ssn());
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Id_Nbr_S().setValue(ldaIaal581s.getPnd_Sttlmnt_Id_Nbr());                                                 //Natural: MOVE #STTLMNT-ID-NBR TO #PTSCK-ID-NBR-S #PTSCK-ID-NBR-E
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Id_Nbr_E().setValue(ldaIaal581s.getPnd_Sttlmnt_Id_Nbr());
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Req_Seq_Nbr_S().setValue(ldaIaal581s.getPnd_Sttlmnt_Seq_Nbr());                                           //Natural: MOVE #STTLMNT-SEQ-NBR TO #PTSCK-REQ-SEQ-NBR-S #PTSCK-REQ-SEQ-NBR-E
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Req_Seq_Nbr_E().setValue(ldaIaal581s.getPnd_Sttlmnt_Seq_Nbr());
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Ppcn_Nbr_S().setValue("H'00'");                                                                           //Natural: MOVE H'00' TO #PTSCK-PPCN-NBR-S
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S_Pnd_Ptsck_Payee_Cde_S().setValue(0);                                                                                //Natural: MOVE 0 TO #PTSCK-PAYEE-CDE-S
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Ppcn_Nbr_E().setValue("H'FF'");                                                                           //Natural: MOVE H'FF' TO #PTSCK-PPCN-NBR-E
        ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E_Pnd_Ptsck_Payee_Cde_E().setValue(99);                                                                               //Natural: MOVE 99 TO #PTSCK-PAYEE-CDE-E
        ldaIaal581s.getVw_iaa_Dc_Cntrct_View().startDatabaseRead                                                                                                          //Natural: READ IAA-DC-CNTRCT-VIEW BY PIN-TAXID-SEQ-CNTRCT-KEY STARTING FROM #PIN-TAXID-SEQ-CNTRCT-KEY-S ENDING AT #PIN-TAXID-SEQ-CNTRCT-KEY-E
        (
        "R4",
        new Wc[] { new Wc("PIN_TAXID_SEQ_CNTRCT_KEY", ">=", ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_S(), "And", WcType.BY) ,
        new Wc("PIN_TAXID_SEQ_CNTRCT_KEY", "<=", ldaIaal581s.getPnd_Pin_Taxid_Seq_Cntrct_Key_E(), WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_CNTRCT_KEY", "ASC") }
        );
        R4:
        while (condition(ldaIaal581s.getVw_iaa_Dc_Cntrct_View().readNextRow("R4")))
        {
            ldaIaal581s.getPnd_Num().setValue(ldaIaal581s.getIaa_Dc_Cntrct_View_Count_Castcntrct_Sttlmnt_Info_Cde());                                                     //Natural: MOVE C*CNTRCT-STTLMNT-INFO-CDE TO #NUM
            FR8:                                                                                                                                                          //Natural: FOR #G = 1 TO #NUM
            for (ldaIaal581s.getPnd_G().setValue(1); condition(ldaIaal581s.getPnd_G().lessOrEqual(ldaIaal581s.getPnd_Num())); ldaIaal581s.getPnd_G().nadd(1))
            {
                if (condition(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D032") || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D033")  //Natural: IF CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D032' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D033' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D038' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D039' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D046' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D053' OR CNTRCT-STTLMNT-INFO-CDE ( #G ) = 'D054'
                    || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D038") || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D039") 
                    || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D046") || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D053") 
                    || ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(ldaIaal581s.getPnd_G()).equals("D054")))
                {
                    ldaIaal581s.getPnd_Sttl_Comp().setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO #STTL-COMP
                    if (true) break FR8;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FR8. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!(ldaIaal581s.getPnd_Sttl_Comp().equals("Y"))))                                                                                                 //Natural: ACCEPT IF #STTL-COMP = 'Y'
            {
                continue;
            }
            ldaIaal581s.getPnd_Sttl_Comp().reset();                                                                                                                       //Natural: RESET #STTL-COMP
            ldaIaal581s.getPnd_Contract_Reads().nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONTRACT-READS
            ldaIaal581s.getPnd_Cntrct_Optn_Cde().setValue(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Optn_Cde());                                                           //Natural: MOVE CNTRCT-OPTN-CDE TO #CNTRCT-OPTN-CDE
            ldaIaal581s.getPnd_Contract_1_8().setValue(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr());                                                              //Natural: MOVE CNTRCT-PPCN-NBR TO #CONTRACT-1-8 #CPK-CONTRACT
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Contract().setValue(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Ppcn_Nbr());
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().setValue(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Payee_Cde());                                           //Natural: MOVE CNTRCT-PAYEE-CDE TO #CPK-PAYEE
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Cont_Stat().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581s.getPnd_Contract_1_8(),                  //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
                "-", ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A()));
            ldaIaal581s.getVw_iaa_Cntrct_Prtcpnt_Role_View().startDatabaseFind                                                                                            //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE-VIEW CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
            (
            "F3",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", ldaIaal581s.getPnd_Cntrct_Payee_Key(), WcType.WITH) },
            1
            );
            F3:
            while (condition(ldaIaal581s.getVw_iaa_Cntrct_Prtcpnt_Role_View().readNextRow("F3")))
            {
                ldaIaal581s.getVw_iaa_Cntrct_Prtcpnt_Role_View().setIfNotFoundControlFlag(false);
                ldaIaal581s.getPnd_Contract_Partic_Reads().nadd(1);                                                                                                       //Natural: ADD 1 TO #CONTRACT-PARTIC-READS
                ldaIaal581s.getPnd_Work_Record_Pnd_W_Contract_Mode().setValue(ldaIaal581s.getIaa_Cntrct_Prtcpnt_Role_View_Cntrct_Mode_Ind());                             //Natural: MOVE CNTRCT-MODE-IND TO #W-CONTRACT-MODE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #CHECK-FURTHUR-PAYMENT-AND-WRITE-REC
            sub_Pnd_Check_Furthur_Payment_And_Write_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Check_Furthur_Payment_And_Write_Rec() throws Exception                                                                                           //Natural: #CHECK-FURTHUR-PAYMENT-AND-WRITE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *WRITE 'P' #CPK-PAYEE 'DC' #STTLMNT-DECEDENT-TYPE 'CN' #CNTRCT-OPTN-CDE
        //* * 'PIN' #STTLMNT-ID-NBR
        if (condition(ldaIaal581s.getIaa_Dc_Cntrct_View_Cntrct_Sttlmnt_Info_Cde().getValue(1).equals("D032")))                                                            //Natural: IF IAA-DC-CNTRCT-VIEW.CNTRCT-STTLMNT-INFO-CDE ( 1 ) = 'D032'
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-NFP-REC
            sub_Pnd_Write_Out_Nfp_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #TEST-FFP-CONDITIONS
            sub_Pnd_Test_Ffp_Conditions();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Test_Ffp_Conditions() throws Exception                                                                                                           //Natural: #TEST-FFP-CONDITIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        short decideConditionsMet697 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CPK-PAYEE = 01 AND #STTLMNT-DCDNT-TYPE = '1' AND ( #CNTRCT-OPTN-CDE = 04 OR EQ 11 OR EQ 14 OR EQ 16 )
        if (condition(((ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(1) && ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type().equals("1")) 
            && (((ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(4) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(11)) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(14)) 
            || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(16)))))
        {
            decideConditionsMet697++;
            ldaIaal581s.getPnd_Ffp_But_Dont_Write().nadd(1);                                                                                                              //Natural: ADD 1 TO #FFP-BUT-DONT-WRITE
        }                                                                                                                                                                 //Natural: WHEN #CPK-PAYEE = 1 AND #STTLMNT-DCDNT-TYPE = '2' AND ( #CNTRCT-OPTN-CDE = 04 OR EQ 11 OR EQ 14 OR EQ 16 )
        else if (condition(((ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(1) && ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type().equals("2")) 
            && (((ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(4) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(11)) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(14)) 
            || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(16)))))
        {
            decideConditionsMet697++;
            ldaIaal581s.getPnd_Ffp_But_Dont_Write().nadd(1);                                                                                                              //Natural: ADD 1 TO #FFP-BUT-DONT-WRITE
        }                                                                                                                                                                 //Natural: WHEN #CPK-PAYEE = 01 AND #STTLMNT-DCDNT-TYPE = '2' AND ( #CNTRCT-OPTN-CDE = 03 OR EQ 12 OR EQ 15 OR EQ 17 )
        else if (condition(((ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(1) && ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dcdnt_Type().equals("2")) 
            && (((ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(3) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(12)) || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(15)) 
            || ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(17)))))
        {
            decideConditionsMet697++;
            ldaIaal581s.getPnd_Ffp_But_Dont_Write().nadd(1);                                                                                                              //Natural: ADD 1 TO #FFP-BUT-DONT-WRITE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM #WRITE-OUT-FFP-REC
            sub_Pnd_Write_Out_Ffp_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Setup_System_Date_Time() throws Exception                                                                                                        //Natural: #SETUP-SYSTEM-DATE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Sys_Date().setValue(Global.getDATX());                                                                                                         //Natural: MOVE *DATX TO #SYS-DATE
        ldaIaal581s.getPnd_Sy_Date_Yymmdd_Alph().setValueEdited(ldaIaal581s.getPnd_Sys_Date(),new ReportEditMask("YYMMDD"));                                              //Natural: MOVE EDITED #SYS-DATE ( EM = YYMMDD ) TO #SY-DATE-YYMMDD-ALPH
        ldaIaal581s.getPnd_Sys_Time().setValue(Global.getTIMX());                                                                                                         //Natural: MOVE *TIMX TO #SYS-TIME
        ldaIaal581s.getPnd_Sy_Time_Hhiiss_Alph().setValueEdited(ldaIaal581s.getPnd_Sys_Time(),new ReportEditMask("HHIISS"));                                              //Natural: MOVE EDITED #SYS-TIME ( EM = HHIISS ) TO #SY-TIME-HHIISS-ALPH
    }
    //*  082515
    private void sub_Pnd_Write_Out_Nfp_Rec() throws Exception                                                                                                             //Natural: #WRITE-OUT-NFP-REC
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal581s.getPnd_Work_Record_Pnd_W_Nfp().setValue("Y");                                                                                                         //Natural: ASSIGN #W-NFP := 'Y'
        short decideConditionsMet718 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STTLMNT-DECEDENT-TYPE = '1'
        if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("1")))
        {
            decideConditionsMet718++;
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("01");                                                                                              //Natural: MOVE '01' TO #W-ANNU-TYPE
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_1());                                                              //Natural: MOVE #STTLMNT-DOD-DTE-1 TO #W-DOD-DTE
            getWorkFiles().write(1, false, ldaIaal581s.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 1 #WORK-RECORD
            ldaIaal581s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
        }                                                                                                                                                                 //Natural: WHEN #STTLMNT-DECEDENT-TYPE = '2'
        else if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("2")))
        {
            decideConditionsMet718++;
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("02");                                                                                              //Natural: MOVE '02' TO #W-ANNU-TYPE
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2());                                                              //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
            getWorkFiles().write(1, false, ldaIaal581s.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 1 #WORK-RECORD
            ldaIaal581s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
        }                                                                                                                                                                 //Natural: WHEN #STTLMNT-DECEDENT-TYPE = '3'
        else if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("3")))
        {
            decideConditionsMet718++;
                                                                                                                                                                          //Natural: PERFORM #NFP-03-DECEDENT
            sub_Pnd_Nfp_03_Decedent();
            if (condition(Global.isEscape())) {return;}
            ldaIaal581s.getPnd_Nfp_03_Deced().nadd(1);                                                                                                                    //Natural: ADD 1 TO #NFP-03-DECED
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type());                                                        //Natural: MOVE #STTLMNT-DECEDENT-TYPE TO #W-ANNU-TYPE
            getWorkFiles().write(1, false, ldaIaal581s.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 1 #WORK-RECORD
            ldaIaal581s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  082515
    private void sub_Pnd_Write_Out_Ffp_Rec() throws Exception                                                                                                             //Natural: #WRITE-OUT-FFP-REC
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal581s.getPnd_Work_Record_Pnd_W_Nfp().setValue("N");                                                                                                         //Natural: ASSIGN #W-NFP := 'N'
        short decideConditionsMet741 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STTLMNT-DECEDENT-TYPE = '1'
        if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("1")))
        {
            decideConditionsMet741++;
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("01");                                                                                              //Natural: MOVE '01' TO #W-ANNU-TYPE
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_1());                                                              //Natural: MOVE #STTLMNT-DOD-DTE-1 TO #W-DOD-DTE
            //*  10/10/2000
            if (condition((ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(1) && ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(pnd_Cntrct_Optn_Array1.getValue("*")))  //Natural: IF ( #CPK-PAYEE = 01 AND #CNTRCT-OPTN-CDE = #CNTRCT-OPTN-ARRAY1 ( * ) ) OR ( #CPK-PAYEE = 02 AND #CNTRCT-OPTN-CDE = #CNTRCT-OPTN-ARRAY2 ( * ) )
                || (ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(2) && ldaIaal581s.getPnd_Cntrct_Optn_Cde().equals(pnd_Cntrct_Optn_Array2.getValue("*")))))
            {
                getReports().write(0, " 10/09/2001******* TRANSACTION *****");                                                                                            //Natural: WRITE ' 10/09/2001******* TRANSACTION *****'
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaIaal581s.getPnd_Cntrct_Optn_Cde());                                                                                          //Natural: WRITE '=' #CNTRCT-OPTN-CDE
                if (Global.isEscape()) return;
                getWorkFiles().write(3, false, ldaIaal581s.getPnd_Work_Record());                                                                                         //Natural: WRITE WORK FILE 3 #WORK-RECORD
                ldaIaal581s.getPnd_Work_Record_Writes_3().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, " ELSE ");                                                                                                                          //Natural: WRITE ' ELSE '
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaIaal581s.getPnd_Cntrct_Optn_Cde());                                                                                          //Natural: WRITE '=' #CNTRCT-OPTN-CDE
                if (Global.isEscape()) return;
                getWorkFiles().write(2, false, ldaIaal581s.getPnd_Work_Record());                                                                                         //Natural: WRITE WORK FILE 2 #WORK-RECORD
                ldaIaal581s.getPnd_Work_Record_Writes_2().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #STTLMNT-DECEDENT-TYPE = '2'
        else if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("2")))
        {
            decideConditionsMet741++;
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("02");                                                                                              //Natural: MOVE '02' TO #W-ANNU-TYPE
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2());                                                              //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
            //*  10/10/2000
            if (condition(ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee().equals(1)))                                                                                 //Natural: IF #CPK-PAYEE = 01
            {
                getWorkFiles().write(2, false, ldaIaal581s.getPnd_Work_Record());                                                                                         //Natural: WRITE WORK FILE 2 #WORK-RECORD
                ldaIaal581s.getPnd_Work_Record_Writes_2().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(3, false, ldaIaal581s.getPnd_Work_Record());                                                                                         //Natural: WRITE WORK FILE 3 #WORK-RECORD
                ldaIaal581s.getPnd_Work_Record_Writes_3().nadd(1);                                                                                                        //Natural: ADD 1 TO #WORK-RECORD-WRITES-3
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #STTLMNT-DECEDENT-TYPE = '3'
        else if (condition(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type().equals("3")))
        {
            decideConditionsMet741++;
                                                                                                                                                                          //Natural: PERFORM #FFP-03-DECEDENT
            sub_Pnd_Ffp_03_Decedent();
            if (condition(Global.isEscape())) {return;}
            ldaIaal581s.getPnd_Ffp_03_Deced().nadd(1);                                                                                                                    //Natural: ADD 1 TO #FFP-03-DECED
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue(ldaIaal581s.getPnd_Sttlmnt_Decedent_Type());                                                        //Natural: MOVE #STTLMNT-DECEDENT-TYPE TO #W-ANNU-TYPE
            getWorkFiles().write(2, false, ldaIaal581s.getPnd_Work_Record());                                                                                             //Natural: WRITE WORK FILE 2 #WORK-RECORD
            ldaIaal581s.getPnd_Work_Record_Writes_2().nadd(1);                                                                                                            //Natural: ADD 1 TO #WORK-RECORD-WRITES-2
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Nfp_03_Decedent() throws Exception                                                                                                               //Natural: #NFP-03-DECEDENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("01");                                                                                                  //Natural: MOVE '01' TO #W-ANNU-TYPE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte());                                                   //Natural: MOVE #STTLMNT-DOD-DTE TO #W-DOD-DTE
        if (condition(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte().lessOrEqual(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2())))                                         //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("01");                                                                                         //Natural: MOVE '01' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("02");                                                                                         //Natural: MOVE '02' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Cont_Stat().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581s.getPnd_Contract_1_8(), "-",                 //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A()));
        //* *IF #CPK-PAYEE-A = '01'
        //* *  WRITE WORK FILE 2 #WORK-RECORD
        //* *  ADD 1 TO #WORK-RECORD-WRITES-2
        //* *ELSE
        //* *  WRITE WORK FILE 1 #WORK-RECORD
        //* *  ADD 1 TO #WORK-RECORD-WRITES-1
        //* *END-IF
        getWorkFiles().write(1, false, ldaIaal581s.getPnd_Work_Record());                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-RECORD
        ldaIaal581s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                                //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("02");                                                                                                  //Natural: MOVE '02' TO #W-ANNU-TYPE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2());                                                                  //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
        if (condition(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte().lessOrEqual(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2())))                                         //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("02");                                                                                         //Natural: MOVE '02' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("01");                                                                                         //Natural: MOVE '01' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Cont_Stat().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581s.getPnd_Contract_1_8(), "-",                 //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A()));
        //* *IF #CPK-PAYEE-A = '01'
        //* *  WRITE WORK FILE 2 #WORK-RECORD
        //* *  ADD 1 TO #WORK-RECORD-WRITES-2
        //* *ELSE
        //* *  WRITE WORK FILE 1 #WORK-RECORD
        //* *  ADD 1 TO #WORK-RECORD-WRITES-1
        //* *END-IF
        getWorkFiles().write(1, false, ldaIaal581s.getPnd_Work_Record());                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-RECORD
        ldaIaal581s.getPnd_Work_Record_Writes_1().nadd(1);                                                                                                                //Natural: ADD 1 TO #WORK-RECORD-WRITES-1
    }
    private void sub_Pnd_Ffp_03_Decedent() throws Exception                                                                                                               //Natural: #FFP-03-DECEDENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("01");                                                                                                  //Natural: MOVE '01' TO #W-ANNU-TYPE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte());                                                   //Natural: MOVE #STTLMNT-DOD-DTE TO #W-DOD-DTE
        if (condition(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte().lessOrEqual(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2())))                                         //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("01");                                                                                         //Natural: MOVE '01' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("02");                                                                                         //Natural: MOVE '02' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Cont_Stat().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581s.getPnd_Contract_1_8(), "-",                 //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A()));
        //*  10/10/2000
        getWorkFiles().write(3, false, ldaIaal581s.getPnd_Work_Record());                                                                                                 //Natural: WRITE WORK FILE 3 #WORK-RECORD
        ldaIaal581s.getPnd_Work_Record_Writes_3().nadd(1);                                                                                                                //Natural: ADD 1 TO #WORK-RECORD-WRITES-3
        //* *
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Annu_Type().setValue("02");                                                                                                  //Natural: MOVE '02' TO #W-ANNU-TYPE
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Dod_Dte().setValue(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2());                                                                  //Natural: MOVE #STTLMNT-DOD-DTE-2 TO #W-DOD-DTE
        if (condition(ldaIaal581s.getIaa_Sttlmnt_View_Pnd_Sttlmnt_Dod_Dte().lessOrEqual(ldaIaal581s.getPnd_Sttlmnt_Dod_Dte_2())))                                         //Natural: IF #STTLMNT-DOD-DTE NOT > #STTLMNT-DOD-DTE-2
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("02");                                                                                         //Natural: MOVE '02' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A().setValue("01");                                                                                         //Natural: MOVE '01' TO #CPK-PAYEE-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal581s.getPnd_Work_Record_Pnd_W_Cont_Stat().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal581s.getPnd_Contract_1_8(), "-",                 //Natural: COMPRESS #CONTRACT-1-8 '-' #CPK-PAYEE-A INTO #W-CONT-STAT LEAVING NO
            ldaIaal581s.getPnd_Cntrct_Payee_Key_Pnd_Cpk_Payee_A()));
        //*  10/10/2000
        getWorkFiles().write(3, false, ldaIaal581s.getPnd_Work_Record());                                                                                                 //Natural: WRITE WORK FILE 3 #WORK-RECORD
        ldaIaal581s.getPnd_Work_Record_Writes_3().nadd(1);                                                                                                                //Natural: ADD 1 TO #WORK-RECORD-WRITES-3
    }
    private void sub_Pnd_Reset_Para() throws Exception                                                                                                                    //Natural: #RESET-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaIaal581s.getPnd_Work_Record().reset();                                                                                                                         //Natural: RESET #WORK-RECORD
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal581s.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal581s.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
            //*  032610
            ldaIaal581s.getIaa_Parm_Card_Pnd_Parm_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #PARM-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  032610
        pnd_Datd.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaIaal581s.getIaa_Parm_Card_Pnd_Parm_Date());                                                             //Natural: MOVE EDITED #PARM-DATE TO #DATD ( EM = YYYYMMDD )
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I).equals(" ")))                                                           //Natural: IF ##DATA-RESPONSE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    }

    //
}
