/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:59 PM
**        * FROM NATURAL PROGRAM : Iaap850
************************************************************
**        * FILE NAME            : Iaap850.java
**        * CLASS NAME           : Iaap850
**        * INSTANCE NAME        : Iaap850
************************************************************
******************************************************************
************************************************************************
**  PROGRAM NAME  :  IAAP850                                           *
**  DESCRIPTION   :  ODS IMMDIATE ANNUITY EXTRACT                      *
**  DATE          :  AUGUST 2010                                       *
**  FUNCTION      :  ODS IMMDIATE ANNUITY EXTRACT                      *
**                :  THIS PROGRAM WILL CREATE AN EXTRACT FOR ODS LOAD
**
**  MOD DATE   MOD BY    DESCRIPTION OF CHANGES
***
***  INPUT
***      ADABAS FILES:
***      IAA-CNTRCT-PRTCPNT-ROLE  DBID 3 FNR 200
***      IAA-CNTRCT               DBID 3 FNR 200
***      IAA-TIAA-FUND-RCRD       DBID 3 FNR 201
***
***  OUTPUT
***      EXTRACT- IA ODS EXTRACT FILE.
***      CONTROL- IA ODS CONTROL FILE.
***
*** 08/2017  R. CARREON -  PIN EXPANSION
***============================================================***
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap850 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_icpr;
    private DbsField icpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField icpr_Cntrct_Part_Payee_Cde;
    private DbsField icpr_Cpr_Id_Nbr;
    private DbsField icpr_Cntrct_Actvty_Cde;
    private DbsField icpr_Cntrct_Mode_Ind;
    private DbsField icpr_Cntrct_Wthdrwl_Dte;
    private DbsField icpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Icpr_Key;

    private DbsGroup pnd_Icpr_Key__R_Field_1;
    private DbsField pnd_Icpr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Icpr_Key_Pnd_Cntrct_Payee_Cde;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;

    private DataAccessProgramView vw_itfr;
    private DbsField itfr_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField itfr_Tiaa_Cntrct_Payee_Cde;
    private DbsField itfr_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup itfr__R_Field_2;
    private DbsField itfr_Tiaa_Cmpny_Cde;
    private DbsField itfr_Tiaa_Fund_Cde;

    private DbsGroup itfr_Tiaa_Rate_Data_Grp;
    private DbsField itfr_Tiaa_Rate_Final_Pay_Amt;
    private DbsField itfr_Tiaa_Tot_Per_Amt;
    private DbsField itfr_Tiaa_Tot_Div_Amt;
    private DbsField itfr_Tiaa_Old_Per_Amt;
    private DbsField itfr_Tiaa_Old_Div_Amt;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_3;
    private DbsField pnd_Fund_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fund_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Fund_Key_Cmpny_Fund_Cde;

    private DbsGroup pnd_Output_Rec;
    private DbsField pnd_Output_Rec_Pnd_Out_Pin;
    private DbsField pnd_Output_Rec_Pnd_Out_Separator1;
    private DbsField pnd_Output_Rec_Pnd_Out_Contract;
    private DbsField pnd_Output_Rec_Pnd_Out_Separator2;
    private DbsField pnd_Output_Rec_Pnd_Out_Final_Payment;
    private DbsField pnd_Output_Rec_Pnd_Out_Separator3;
    private DbsField pnd_Output_Rec_Pnd_Out_Ticker;
    private DbsField pnd_Output_Rec_Pnd_Out_Separator5;
    private DbsField pnd_Output_Rec_Pnd_Run_Date_Time;

    private DbsGroup pnd_Control_Rec;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Process;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Separator1;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Run_Started;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Separator2;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Run_Ended;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Separator3;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Tot_Recs;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Separator4;
    private DbsField pnd_Control_Rec_Pnd_Cntl_Tot_Amt;
    private DbsField pnd_Format_Run_Date_Time;

    private DbsGroup pnd_Format_Run_Date_Time__R_Field_4;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Date;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Blank;

    private DbsGroup pnd_Format_Run_Date_Time_Pnd_Format_Run_Time;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Hh;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill1;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Mm;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill2;
    private DbsField pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Ss;
    private DbsField pnd_Run_Tot_Amt;
    private DbsField pnd_Fund_Tot_Amt;
    private DbsField pnd_Final_Payment;
    private DbsField pnd_Fund_Old_Amt;
    private DbsField pnd_Found_Ipro;
    private DbsField pnd_Found_Pamp_I;
    private DbsField pnd_In_Contract;
    private DbsField pnd_Tot_Read_Iamstr;
    private DbsField pnd_Tot_Hits_Icpr;
    private DbsField pnd_Tot_W_Fin_Pymnt;
    private DbsField pnd_Tot_Recs_Out;
    private DbsField pnd_Total_Ipro_In;
    private DbsField pnd_Total_Pamp_I_In;
    private DbsField pnd_Total_Ipro_Out;
    private DbsField pnd_Total_Pamp_I_Out;
    private DbsField pnd_Recs_Disp;
    private DbsField pnd_Recs_Accptd;

    private DbsRecord setTimeRecord;
    private DbsField record_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_icpr = new DataAccessProgramView(new NameInfo("vw_icpr", "ICPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        icpr_Cntrct_Part_Ppcn_Nbr = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        icpr_Cntrct_Part_Payee_Cde = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        icpr_Cpr_Id_Nbr = vw_icpr.getRecord().newFieldInGroup("icpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        icpr_Cntrct_Actvty_Cde = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        icpr_Cntrct_Mode_Ind = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        icpr_Cntrct_Wthdrwl_Dte = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_WTHDRWL_DTE");
        icpr_Cntrct_Final_Per_Pay_Dte = vw_icpr.getRecord().newFieldInGroup("icpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_icpr);

        pnd_Icpr_Key = localVariables.newFieldInRecord("pnd_Icpr_Key", "#ICPR-KEY", FieldType.STRING, 12);

        pnd_Icpr_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Icpr_Key__R_Field_1", "REDEFINE", pnd_Icpr_Key);
        pnd_Icpr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Icpr_Key__R_Field_1.newFieldInGroup("pnd_Icpr_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Icpr_Key_Pnd_Cntrct_Payee_Cde = pnd_Icpr_Key__R_Field_1.newFieldInGroup("pnd_Icpr_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_itfr = new DataAccessProgramView(new NameInfo("vw_itfr", "ITFR"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        itfr_Tiaa_Cntrct_Ppcn_Nbr = vw_itfr.getRecord().newFieldInGroup("ITFR_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_PPCN_NBR");
        itfr_Tiaa_Cntrct_Payee_Cde = vw_itfr.getRecord().newFieldInGroup("itfr_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        itfr_Tiaa_Cmpny_Fund_Cde = vw_itfr.getRecord().newFieldInGroup("itfr_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIAA_CMPNY_FUND_CDE");

        itfr__R_Field_2 = vw_itfr.getRecord().newGroupInGroup("itfr__R_Field_2", "REDEFINE", itfr_Tiaa_Cmpny_Fund_Cde);
        itfr_Tiaa_Cmpny_Cde = itfr__R_Field_2.newFieldInGroup("itfr_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 1);
        itfr_Tiaa_Fund_Cde = itfr__R_Field_2.newFieldInGroup("itfr_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 2);

        itfr_Tiaa_Rate_Data_Grp = vw_itfr.getRecord().newGroupInGroup("itfr_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        itfr_Tiaa_Rate_Final_Pay_Amt = itfr_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("itfr_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        itfr_Tiaa_Tot_Per_Amt = vw_itfr.getRecord().newFieldInGroup("ITFR_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_TOT_PER_AMT");
        itfr_Tiaa_Tot_Div_Amt = vw_itfr.getRecord().newFieldInGroup("itfr_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_TOT_DIV_AMT");
        itfr_Tiaa_Old_Per_Amt = vw_itfr.getRecord().newFieldInGroup("ITFR_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "AJ");
        itfr_Tiaa_Old_Div_Amt = vw_itfr.getRecord().newFieldInGroup("ITFR_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_OLD_UNIT_VAL");
        registerRecord(vw_itfr);

        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_3", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Cntrct_Ppcn_Nbr = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Fund_Key_Cntrct_Payee_Cde = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Fund_Key_Cmpny_Fund_Cde = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 3);

        pnd_Output_Rec = localVariables.newGroupInRecord("pnd_Output_Rec", "#OUTPUT-REC");
        pnd_Output_Rec_Pnd_Out_Pin = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Pin", "#OUT-PIN", FieldType.NUMERIC, 12);
        pnd_Output_Rec_Pnd_Out_Separator1 = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Separator1", "#OUT-SEPARATOR1", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Out_Contract = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Contract", "#OUT-CONTRACT", FieldType.STRING, 10);
        pnd_Output_Rec_Pnd_Out_Separator2 = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Separator2", "#OUT-SEPARATOR2", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Out_Final_Payment = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Final_Payment", "#OUT-FINAL-PAYMENT", FieldType.STRING, 
            12);
        pnd_Output_Rec_Pnd_Out_Separator3 = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Separator3", "#OUT-SEPARATOR3", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Out_Ticker = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Ticker", "#OUT-TICKER", FieldType.STRING, 6);
        pnd_Output_Rec_Pnd_Out_Separator5 = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Out_Separator5", "#OUT-SEPARATOR5", FieldType.STRING, 1);
        pnd_Output_Rec_Pnd_Run_Date_Time = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Run_Date_Time", "#RUN-DATE-TIME", FieldType.STRING, 17);

        pnd_Control_Rec = localVariables.newGroupInRecord("pnd_Control_Rec", "#CONTROL-REC");
        pnd_Control_Rec_Pnd_Cntl_Process = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Process", "#CNTL-PROCESS", FieldType.STRING, 17);
        pnd_Control_Rec_Pnd_Cntl_Separator1 = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Separator1", "#CNTL-SEPARATOR1", FieldType.STRING, 
            1);
        pnd_Control_Rec_Pnd_Cntl_Run_Started = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Run_Started", "#CNTL-RUN-STARTED", FieldType.STRING, 
            17);
        pnd_Control_Rec_Pnd_Cntl_Separator2 = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Separator2", "#CNTL-SEPARATOR2", FieldType.STRING, 
            1);
        pnd_Control_Rec_Pnd_Cntl_Run_Ended = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Run_Ended", "#CNTL-RUN-ENDED", FieldType.STRING, 
            17);
        pnd_Control_Rec_Pnd_Cntl_Separator3 = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Separator3", "#CNTL-SEPARATOR3", FieldType.STRING, 
            1);
        pnd_Control_Rec_Pnd_Cntl_Tot_Recs = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Tot_Recs", "#CNTL-TOT-RECS", FieldType.NUMERIC, 
            7);
        pnd_Control_Rec_Pnd_Cntl_Separator4 = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Separator4", "#CNTL-SEPARATOR4", FieldType.STRING, 
            1);
        pnd_Control_Rec_Pnd_Cntl_Tot_Amt = pnd_Control_Rec.newFieldInGroup("pnd_Control_Rec_Pnd_Cntl_Tot_Amt", "#CNTL-TOT-AMT", FieldType.STRING, 14);
        pnd_Format_Run_Date_Time = localVariables.newFieldInRecord("pnd_Format_Run_Date_Time", "#FORMAT-RUN-DATE-TIME", FieldType.STRING, 17);

        pnd_Format_Run_Date_Time__R_Field_4 = localVariables.newGroupInRecord("pnd_Format_Run_Date_Time__R_Field_4", "REDEFINE", pnd_Format_Run_Date_Time);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Date = pnd_Format_Run_Date_Time__R_Field_4.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Date", 
            "#FORMAT-RUN-DATE", FieldType.STRING, 8);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Blank = pnd_Format_Run_Date_Time__R_Field_4.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Blank", 
            "#FORMAT-RUN-BLANK", FieldType.STRING, 1);

        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time = pnd_Format_Run_Date_Time__R_Field_4.newGroupInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time", 
            "#FORMAT-RUN-TIME");
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Hh = pnd_Format_Run_Date_Time_Pnd_Format_Run_Time.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Hh", 
            "#FORMAT-RUN-TIME-HH", FieldType.STRING, 2);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill1 = pnd_Format_Run_Date_Time_Pnd_Format_Run_Time.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill1", 
            "#FORMAT-RUN-TIME-FILL1", FieldType.STRING, 1);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Mm = pnd_Format_Run_Date_Time_Pnd_Format_Run_Time.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Mm", 
            "#FORMAT-RUN-TIME-MM", FieldType.STRING, 2);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill2 = pnd_Format_Run_Date_Time_Pnd_Format_Run_Time.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Fill2", 
            "#FORMAT-RUN-TIME-FILL2", FieldType.STRING, 1);
        pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Ss = pnd_Format_Run_Date_Time_Pnd_Format_Run_Time.newFieldInGroup("pnd_Format_Run_Date_Time_Pnd_Format_Run_Time_Ss", 
            "#FORMAT-RUN-TIME-SS", FieldType.STRING, 2);
        pnd_Run_Tot_Amt = localVariables.newFieldInRecord("pnd_Run_Tot_Amt", "#RUN-TOT-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Fund_Tot_Amt = localVariables.newFieldInRecord("pnd_Fund_Tot_Amt", "#FUND-TOT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Final_Payment = localVariables.newFieldInRecord("pnd_Final_Payment", "#FINAL-PAYMENT", FieldType.NUMERIC, 11, 2);
        pnd_Fund_Old_Amt = localVariables.newFieldInRecord("pnd_Fund_Old_Amt", "#FUND-OLD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Found_Ipro = localVariables.newFieldInRecord("pnd_Found_Ipro", "#FOUND-IPRO", FieldType.BOOLEAN, 1);
        pnd_Found_Pamp_I = localVariables.newFieldInRecord("pnd_Found_Pamp_I", "#FOUND-P&I", FieldType.BOOLEAN, 1);
        pnd_In_Contract = localVariables.newFieldInRecord("pnd_In_Contract", "#IN-CONTRACT", FieldType.STRING, 10);
        pnd_Tot_Read_Iamstr = localVariables.newFieldInRecord("pnd_Tot_Read_Iamstr", "#TOT-READ-IAMSTR", FieldType.INTEGER, 4);
        pnd_Tot_Hits_Icpr = localVariables.newFieldInRecord("pnd_Tot_Hits_Icpr", "#TOT-HITS-ICPR", FieldType.INTEGER, 4);
        pnd_Tot_W_Fin_Pymnt = localVariables.newFieldInRecord("pnd_Tot_W_Fin_Pymnt", "#TOT-W-FIN-PYMNT", FieldType.INTEGER, 4);
        pnd_Tot_Recs_Out = localVariables.newFieldInRecord("pnd_Tot_Recs_Out", "#TOT-RECS-OUT", FieldType.INTEGER, 4);
        pnd_Total_Ipro_In = localVariables.newFieldInRecord("pnd_Total_Ipro_In", "#TOTAL-IPRO-IN", FieldType.INTEGER, 4);
        pnd_Total_Pamp_I_In = localVariables.newFieldInRecord("pnd_Total_Pamp_I_In", "#TOTAL-P&I-IN", FieldType.INTEGER, 4);
        pnd_Total_Ipro_Out = localVariables.newFieldInRecord("pnd_Total_Ipro_Out", "#TOTAL-IPRO-OUT", FieldType.INTEGER, 4);
        pnd_Total_Pamp_I_Out = localVariables.newFieldInRecord("pnd_Total_Pamp_I_Out", "#TOTAL-P&I-OUT", FieldType.INTEGER, 4);
        pnd_Recs_Disp = localVariables.newFieldInRecord("pnd_Recs_Disp", "#RECS-DISP", FieldType.INTEGER, 4);
        pnd_Recs_Accptd = localVariables.newFieldInRecord("pnd_Recs_Accptd", "#RECS-ACCPTD", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        setTimeRecord = new DbsRecord();
        record_Time = setTimeRecord.newFieldInRecord("RECORD_TIME", "RECORD-TIME", FieldType.NUMERIC, 7);
        registerRecord(setTimeRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_icpr.reset();
        vw_iaa_Cntrct.reset();
        vw_itfr.reset();

        localVariables.reset();
        pnd_Output_Rec_Pnd_Out_Separator1.setInitialValue(",");
        pnd_Output_Rec_Pnd_Out_Separator2.setInitialValue(",");
        pnd_Output_Rec_Pnd_Out_Separator3.setInitialValue(",");
        pnd_Output_Rec_Pnd_Out_Separator5.setInitialValue(",");
        pnd_Control_Rec_Pnd_Cntl_Process.setInitialValue("IA ODS EXTRACT");
        pnd_Control_Rec_Pnd_Cntl_Separator1.setInitialValue(",");
        pnd_Control_Rec_Pnd_Cntl_Separator2.setInitialValue(",");
        pnd_Control_Rec_Pnd_Cntl_Separator3.setInitialValue(",");
        pnd_Control_Rec_Pnd_Cntl_Separator4.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap850() throws Exception
    {
        super("Iaap850");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        record_Time = Global.getTIMN();                                                                                                                                   //Natural: SET TIME
        pnd_Control_Rec_Pnd_Cntl_Run_Started.setValue(DbsUtil.compress(Global.getDATN(), Global.getTIME()));                                                              //Natural: COMPRESS *DATN *TIME INTO #CNTL-RUN-STARTED
        getReports().write(0, ReportOption.NOTITLE,"START OF EXECUTION AT: ",pnd_Control_Rec_Pnd_Cntl_Run_Started);                                                       //Natural: WRITE 'START OF EXECUTION AT: ' #CNTL-RUN-STARTED
        if (Global.isEscape()) return;
        pnd_Tot_Read_Iamstr.reset();                                                                                                                                      //Natural: RESET #TOT-READ-IAMSTR #TOT-HITS-ICPR #TOT-W-FIN-PYMNT #RECS-DISP #RECS-ACCPTD #TOTAL-IPRO-IN #TOTAL-P&I-IN #TOTAL-IPRO-OUT #TOTAL-P&I-OUT #RUN-TOT-AMT #TOT-RECS-OUT
        pnd_Tot_Hits_Icpr.reset();
        pnd_Tot_W_Fin_Pymnt.reset();
        pnd_Recs_Disp.reset();
        pnd_Recs_Accptd.reset();
        pnd_Total_Ipro_In.reset();
        pnd_Total_Pamp_I_In.reset();
        pnd_Total_Ipro_Out.reset();
        pnd_Total_Pamp_I_Out.reset();
        pnd_Run_Tot_Amt.reset();
        pnd_Tot_Recs_Out.reset();
        pnd_Final_Payment.reset();                                                                                                                                        //Natural: RESET #FINAL-PAYMENT #FUND-TOT-AMT #FUND-OLD-AMT
        pnd_Fund_Tot_Amt.reset();
        pnd_Fund_Old_Amt.reset();
        pnd_Found_Ipro.setValue(false);                                                                                                                                   //Natural: ASSIGN #FOUND-IPRO := FALSE
        pnd_Found_Pamp_I.setValue(false);                                                                                                                                 //Natural: ASSIGN #FOUND-P&I := FALSE
        //*   READ IA MASTER FILE ....ACCEPT IPRO AND P&I ONLY
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM 'G'
        (
        "READ_IA_MASTER",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", "G", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ_IA_MASTER:
        while (condition(vw_iaa_Cntrct.readNextRow("READ_IA_MASTER")))
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.greater("Y9999999")))                                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR GT 'Y9999999'
            {
                if (true) break READ_IA_MASTER;                                                                                                                           //Natural: ESCAPE BOTTOM ( READ-IA-MASTER. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Read_Iamstr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOT-READ-IAMSTR
            pnd_Found_Ipro.reset();                                                                                                                                       //Natural: RESET #FOUND-IPRO #FOUND-P&I
            pnd_Found_Pamp_I.reset();
            //*  ALL IPROS ARE CONVERTED...
            //*  OPTION CODE FOR IPRO AND ORIGIN CODE 3 IS INSURANCE.
            //*  IF (IAA-CNTRCT.CNTRCT-OPTN-CDE EQ 25 OR EQ 26 OR EQ 27 )
            //*      AND  IAA-CNTRCT.CNTRCT-ORGN-CDE NE 3
            //*    #FOUND-IPRO := TRUE
            //*    ADD 1 TO #TOTAL-IPRO-IN
            //*  END-IF
            //*  OPTION CODE IS 22 P & I CONTRACT AND ORIGIN CODE 3 IS INSURANCE.
            if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(22) && iaa_Cntrct_Cntrct_Orgn_Cde.notEquals(3)))                                                              //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE EQ 22 AND IAA-CNTRCT.CNTRCT-ORGN-CDE NE 3
            {
                pnd_Found_Pamp_I.setValue(true);                                                                                                                          //Natural: ASSIGN #FOUND-P&I := TRUE
                pnd_Total_Pamp_I_In.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-P&I-IN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Recs_Disp.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #RECS-DISP
            if (condition(pnd_Recs_Disp.greater(9999)))                                                                                                                   //Natural: IF #RECS-DISP GT 9999
            {
                pnd_Recs_Disp.reset();                                                                                                                                    //Natural: RESET #RECS-DISP
                getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getTIMX(),"RECS READ:",pnd_Tot_Read_Iamstr, new NumericLength (7),                   //Natural: WRITE NOTITLE NOHDR *TIMX 'RECS READ:' #TOT-READ-IAMSTR ( NL = 7 ) 'WRITTEN:' #TOT-RECS-OUT 'CURRENT REC:' IAA-CNTRCT.CNTRCT-PPCN-NBR
                    "WRITTEN:",pnd_Tot_Recs_Out,"CURRENT REC:",iaa_Cntrct_Cntrct_Ppcn_Nbr);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_IA_MASTER"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_IA_MASTER"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *'IPRO' #TOTAL-IPRO-IN
                //* *'P&I' #TOTAL-P&I-IN (NL=5)
            }                                                                                                                                                             //Natural: END-IF
            //* *REJECT IF NOT ( #FOUND-IPRO OR #FOUND-P&I )
            if (condition(! (pnd_Found_Pamp_I.getBoolean())))                                                                                                             //Natural: REJECT IF NOT #FOUND-P&I
            {
                continue;
            }
            pnd_Recs_Accptd.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #RECS-ACCPTD
            pnd_Icpr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                        //Natural: ASSIGN #CNTRCT-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
            pnd_Icpr_Key_Pnd_Cntrct_Payee_Cde.setValue(1);                                                                                                                //Natural: ASSIGN #CNTRCT-PAYEE-CDE := 01
            vw_icpr.startDatabaseRead                                                                                                                                     //Natural: READ ICPR BY CNTRCT-PAYEE-KEY FROM #ICPR-KEY
            (
            "READ_ICPR",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Icpr_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
            );
            READ_ICPR:
            while (condition(vw_icpr.readNextRow("READ_ICPR")))
            {
                if (condition(icpr_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Icpr_Key_Pnd_Cntrct_Ppcn_Nbr)))                                                                     //Natural: IF ICPR.CNTRCT-PART-PPCN-NBR NE #CNTRCT-PPCN-NBR
                {
                    if (true) break READ_ICPR;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ-ICPR. )
                }                                                                                                                                                         //Natural: END-IF
                //*   ACTIVITY CODE OF 9 IS TERMINATED. PAYEE CODE 1
                //*   IS THE ORIGINAL ANNUITANT.
                if (condition(icpr_Cntrct_Actvty_Cde.equals(9) || icpr_Cntrct_Part_Payee_Cde.greater(1)))                                                                 //Natural: IF ICPR.CNTRCT-ACTVTY-CDE EQ 9 OR ICPR.CNTRCT-PART-PAYEE-CDE GT 1
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Hits_Icpr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOT-HITS-ICPR
                                                                                                                                                                          //Natural: PERFORM READ-IA-MSTR-RATE-INFO
                sub_Read_Ia_Mstr_Rate_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_ICPR"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_ICPR"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Output_Rec_Pnd_Out_Pin.setValue(icpr_Cpr_Id_Nbr);                                                                                                     //Natural: ASSIGN #OUT-PIN := CPR-ID-NBR
                pnd_Output_Rec_Pnd_Out_Contract.setValue(icpr_Cntrct_Part_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #OUT-CONTRACT := CNTRCT-PART-PPCN-NBR
                //* *    #OUT-FINAL-PAYMENT := #FINAL-PAYMENT
                pnd_Output_Rec_Pnd_Out_Final_Payment.setValueEdited(pnd_Final_Payment,new ReportEditMask("999999999.99"));                                                //Natural: MOVE EDITED #FINAL-PAYMENT ( EM = 999999999.99 ) TO #OUT-FINAL-PAYMENT
                //* *    #OUT-FINAL-PAYMENT := #FINAL-PAYMENT
                if (condition(pnd_Final_Payment.greater(getZero())))                                                                                                      //Natural: IF #FINAL-PAYMENT GT 0
                {
                    pnd_Tot_W_Fin_Pymnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOT-W-FIN-PYMNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, ReportOption.NOTITLE,"NO FINAL PAYMENT FOR:",icpr_Cntrct_Part_Ppcn_Nbr,icpr_Cntrct_Part_Payee_Cde);                             //Natural: WRITE 'NO FINAL PAYMENT FOR:' ICPR.CNTRCT-PART-PPCN-NBR ICPR.CNTRCT-PART-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_ICPR"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_ICPR"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Rec_Pnd_Out_Ticker.setValue("TIAA#");                                                                                                          //Natural: ASSIGN #OUT-TICKER := 'TIAA#'
                pnd_Output_Rec_Pnd_Run_Date_Time.setValue(DbsUtil.compress(Global.getDATN(), Global.getTIME()));                                                          //Natural: COMPRESS *DATN *TIME INTO #RUN-DATE-TIME
                //*  FOR DEBUGGING PURPOSE
                //*    WRITE NOTITLE NOHDR #OUTPUT-REC
                //* *  IF #FOUND-IPRO
                //* *     ADD 1 TO #TOTAL-IPRO-OUT
                //* *  END-IF
                if (condition(pnd_Found_Pamp_I.getBoolean()))                                                                                                             //Natural: IF #FOUND-P&I
                {
                    pnd_Total_Pamp_I_Out.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-P&I-OUT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Recs_Out.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-RECS-OUT
                getWorkFiles().write(1, false, pnd_Output_Rec);                                                                                                           //Natural: WRITE WORK FILE 1 #OUTPUT-REC
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_IA_MASTER"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_IA_MASTER"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*    DONE PROCESSING.... WRITE CONTROL RECORD
        pnd_Control_Rec_Pnd_Cntl_Run_Ended.setValue(DbsUtil.compress(Global.getDATN(), Global.getTIME()));                                                                //Natural: COMPRESS *DATN *TIME INTO #CNTL-RUN-ENDED
        pnd_Control_Rec_Pnd_Cntl_Tot_Recs.setValue(pnd_Tot_Recs_Out);                                                                                                     //Natural: ASSIGN #CNTL-TOT-RECS := #TOT-RECS-OUT
        //* * #CNTL-TOT-AMT  := #RUN-TOT-AMT
        pnd_Control_Rec_Pnd_Cntl_Tot_Amt.setValueEdited(pnd_Run_Tot_Amt,new ReportEditMask("99999999999.99"));                                                            //Natural: MOVE EDITED #RUN-TOT-AMT ( EM = 99999999999.99 ) TO #CNTL-TOT-AMT
        getWorkFiles().write(2, false, pnd_Control_Rec);                                                                                                                  //Natural: WRITE WORK FILE 2 #CONTROL-REC
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-MSTR-RATE-INFO
        //* *****************************************
        //* ******************************************************************
        getReports().write(0, ReportOption.NOTITLE,"END OF EXECUTION AT:   ",Global.getTIME());                                                                           //Natural: WRITE 'END OF EXECUTION AT:   ' *TIME
        if (Global.isEscape()) return;
        //* ******************************************************************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"          RUN STATISTICS     ");                                                              //Natural: WRITE /// '          RUN STATISTICS     '
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"-",new RepeatItem(77));                                                                                               //Natural: WRITE '-' ( 77 )
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECS READ IA MASTER:",pnd_Tot_Read_Iamstr);                                                                     //Natural: WRITE 'TOTAL RECS READ IA MASTER:' #TOT-READ-IAMSTR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"TOTAL RECS ACCEPTED:      ",pnd_Recs_Accptd,NEWLINE,"P&I ACCEPTED:             ",pnd_Total_Pamp_I_In,                 //Natural: WRITE 'TOTAL RECS ACCEPTED:      ' #RECS-ACCPTD / 'P&I ACCEPTED:             ' #TOTAL-P&I-IN / 'TOTAL CNTRCT-PRTCPNT-ROLE:' #TOT-HITS-ICPR / 'TOTAL RECS W PAYMENT AMT: ' #TOT-W-FIN-PYMNT / '--------------- EXTRACT -------------------' / 'P&I WRITTEN:              ' #TOTAL-P&I-OUT / 'TOTAL RECS WRITTEN:       ' #TOT-RECS-OUT / 'TOTAL PAYMENT AMOUNT:   ' #RUN-TOT-AMT / '--------------- CONTROL -------------------'
            NEWLINE,"TOTAL CNTRCT-PRTCPNT-ROLE:",pnd_Tot_Hits_Icpr,NEWLINE,"TOTAL RECS W PAYMENT AMT: ",pnd_Tot_W_Fin_Pymnt,NEWLINE,"--------------- EXTRACT -------------------",
            NEWLINE,"P&I WRITTEN:              ",pnd_Total_Pamp_I_Out,NEWLINE,"TOTAL RECS WRITTEN:       ",pnd_Tot_Recs_Out,NEWLINE,"TOTAL PAYMENT AMOUNT:   ",
            pnd_Run_Tot_Amt,NEWLINE,"--------------- CONTROL -------------------");
        if (Global.isEscape()) return;
        //* *     'IPRO ACCEPTED:            ' #TOTAL-IPRO-IN
        //* *     'IPRO WRITTEN:             ' #TOTAL-IPRO-OUT
        getReports().write(0, ReportOption.NOTITLE,pnd_Control_Rec_Pnd_Cntl_Process,pnd_Control_Rec_Pnd_Cntl_Separator1,pnd_Control_Rec_Pnd_Cntl_Run_Started,             //Natural: WRITE #CONTROL-REC
            pnd_Control_Rec_Pnd_Cntl_Separator2,pnd_Control_Rec_Pnd_Cntl_Run_Ended,pnd_Control_Rec_Pnd_Cntl_Separator3,pnd_Control_Rec_Pnd_Cntl_Tot_Recs,
            pnd_Control_Rec_Pnd_Cntl_Separator4,pnd_Control_Rec_Pnd_Cntl_Tot_Amt);
        if (Global.isEscape()) return;
        //*                SET TIME TO CALCULATE TIME ELAPSED                    *
        getReports().write(0, ReportOption.NOTITLE,"ELAPSED TIME FOR THE JOB","(HH:MM:SS.T):",record_Time, new ReportEditMask ("99:99:99'.'9"));                          //Natural: WRITE 'ELAPSED TIME FOR THE JOB' '(HH:MM:SS.T):' *TIMD ( RECORD-TIME. ) ( EM = 99:99:99'.'9 )
        if (Global.isEscape()) return;
    }
    private void sub_Read_Ia_Mstr_Rate_Info() throws Exception                                                                                                            //Natural: READ-IA-MSTR-RATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Key_Cntrct_Ppcn_Nbr.setValue(icpr_Cntrct_Part_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #FUND-KEY.CNTRCT-PPCN-NBR := ICPR.CNTRCT-PART-PPCN-NBR
        pnd_Fund_Key_Cntrct_Payee_Cde.setValue(icpr_Cntrct_Part_Payee_Cde);                                                                                               //Natural: ASSIGN #FUND-KEY.CNTRCT-PAYEE-CDE := ICPR.CNTRCT-PART-PAYEE-CDE
        pnd_Final_Payment.reset();                                                                                                                                        //Natural: RESET #FINAL-PAYMENT #FUND-TOT-AMT #FUND-OLD-AMT
        pnd_Fund_Tot_Amt.reset();
        pnd_Fund_Old_Amt.reset();
        //* *---  READ IAA-TIAA-FUND-RCRD
        vw_itfr.startDatabaseRead                                                                                                                                         //Natural: READ ITFR BY TIAA-CNTRCT-FUND-KEY EQ #FUND-KEY
        (
        "READ_ITFR",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ_ITFR:
        while (condition(vw_itfr.readNextRow("READ_ITFR")))
        {
            if (condition(itfr_Tiaa_Cmpny_Cde.notEquals("T") || itfr_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Fund_Key_Cntrct_Ppcn_Nbr) || itfr_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Fund_Key_Cntrct_Payee_Cde))) //Natural: IF ITFR.TIAA-CMPNY-CDE NE 'T' OR ITFR.TIAA-CNTRCT-PPCN-NBR NE #FUND-KEY.CNTRCT-PPCN-NBR OR ITFR.TIAA-CNTRCT-PAYEE-CDE NE #FUND-KEY.CNTRCT-PAYEE-CDE
            {
                if (true) break READ_ITFR;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ-ITFR. )
            }                                                                                                                                                             //Natural: END-IF
            //*  ASSIGN #FUND-TOT-AMT = ITFR.TIAA-TOT-PER-AMT + ITFR.TIAA-TOT-DIV-AMT
            pnd_Final_Payment.nadd(itfr_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                                                           //Natural: ADD ITFR.TIAA-RATE-FINAL-PAY-AMT ( * ) TO #FINAL-PAYMENT
            pnd_Fund_Tot_Amt.compute(new ComputeParameters(false, pnd_Fund_Tot_Amt), pnd_Fund_Tot_Amt.add(itfr_Tiaa_Tot_Div_Amt).add(itfr_Tiaa_Tot_Per_Amt));             //Natural: ADD ITFR.TIAA-TOT-DIV-AMT ITFR.TIAA-TOT-PER-AMT TO #FUND-TOT-AMT
            pnd_Fund_Old_Amt.compute(new ComputeParameters(false, pnd_Fund_Old_Amt), pnd_Fund_Old_Amt.add(itfr_Tiaa_Old_Div_Amt).add(itfr_Tiaa_Old_Per_Amt));             //Natural: ADD ITFR.TIAA-OLD-DIV-AMT ITFR.TIAA-OLD-PER-AMT TO #FUND-OLD-AMT
            pnd_Run_Tot_Amt.nadd(pnd_Final_Payment);                                                                                                                      //Natural: ADD #FINAL-PAYMENT TO #RUN-TOT-AMT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
