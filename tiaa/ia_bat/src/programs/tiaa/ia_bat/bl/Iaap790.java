/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:32:19 PM
**        * FROM NATURAL PROGRAM : Iaap790
************************************************************
**        * FILE NAME            : Iaap790.java
**        * CLASS NAME           : Iaap790
**        * INSTANCE NAME        : Iaap790
************************************************************
************************************************************************
*
* PROGRAM NAME :- IAAP790
* DATE         :- 03/20/96
* AUTHOR       :- JEFF BERINGER
* DESCRIPTION  :- THIS PROGRAM UPDATES THE MODE CONTROL RECORDS
*              :- FOR TIAA(INCLUDES PA SELECT) & CREF FUNDS
*              :- AND WRITES THE "IA ADMINISTRATION MODE CONTROL"
*              :- REPORT FOR TIAA & CREF FUNDS
*              :- CALLS IAAP790A TO UPDATE PA SELECT VARIABLE FUNDS
*              :- MODES RECORDS & REPORT VARIABLE PA SELECT FUNDS
*
* REVISED 3/97 DON MEADE : INCLUDE CREF AMOUNTS
*         10/97 LB       : CHANGED TO SHOW TOTALS OF ANNUAL AND MONTHLY
*                          FUNDS
*
*          5/00 RM       : LOGIC TO HANDLE PA SELECT FUNDS
*                          DO SCAN ON 5/00
*
*          3/01 RM       : LOGIC TO INCLUDE FINAL-PAYMENT & FINAL-DIV
*                          AMTS IN MODE CNTRL RECORDS
*                          DO SCAN ON 3/01
*          11/07         : ADDED NEW ORIGIN CODES 43-46 FOR PA FUNDS
*          03/12         : RATE BASE EXPANSION  - SCAN ON 3/12
*          04/2017 OS    : PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap790 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Number;
    private DbsField pnd_Work_Record_1_Pnd_C_Payee;
    private DbsField pnd_Work_Record_1_Pnd_C_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_Rest_Of_Record_250;

    private DbsGroup pnd_Work_Record_1__R_Field_1;
    private DbsField pnd_Work_Record_1_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Work_Record_1__R_Field_2;
    private DbsField pnd_Work_Record_1_Pnd_Header_Chk_Dte_A;

    private DbsGroup pnd_Work_Record_1__R_Field_3;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Option;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Origin;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Issue_Date;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt;
    private DbsField pnd_Work_Record_1_Pnd_C_Currency_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Type_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Pension_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex;
    private DbsField pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct;
    private DbsField pnd_Work_Record_1_Pnd_C_Annuitant_Dod;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct;
    private DbsField pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee;
    private DbsField pnd_Work_Record_1_Pnd_C_Dividend_College_Code;
    private DbsField pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde;
    private DbsField pnd_Work_Record_1_Pnd_C_Last_Transaction_Date;

    private DbsGroup pnd_Work_Record_1__R_Field_4;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_1__Filler1;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde;
    private DbsField pnd_Work_Record_1__Filler2;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_1__Filler3;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Status_Code;
    private DbsField pnd_Work_Record_1__Filler4;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_1__Filler5;
    private DbsField pnd_Work_Record_1_Pnd_Cpr_Payment_Mode;

    private DbsGroup pnd_Work_Record_1__R_Field_5;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Company_Code_A3;

    private DbsGroup pnd_Work_Record_1__R_Field_6;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Company_Code;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Fund_Code;

    private DbsGroup pnd_Work_Record_1__R_Field_7;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Fund_Code_N;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rtb_Amont;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rate_Code;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Rate_Date;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Amount;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Payment;
    private DbsField pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Dividend;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_8;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Pnd_Date_Ccyy;
    private DbsField pnd_Good_Record;
    private DbsField pnd_Search_Key_Rcrd;

    private DbsGroup pnd_Search_Key_Rcrd__R_Field_9;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde;
    private DbsField pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte;
    private DbsField pnd_Search_Trans_Dte_A;
    private DbsField pnd_Func;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Prod_Cnt;
    private DbsField pnd_Max_Prd_Cde;
    private DbsField pnd_Mode_Sub;
    private DbsField pnd_Rept_Sub;
    private DbsField pnd_Prod_Sub;
    private DbsField pnd_Prod_Sub_Monthly;
    private DbsField pnd_Mode_Code;
    private DbsField pnd_Prod_Code;
    private DbsField pnd_Rept_Tot_Periodic_Payment;
    private DbsField pnd_Rept_Tot_Periodic_Dividend;
    private DbsField pnd_Rept_Tot_Fin_Periodic_Payment;
    private DbsField pnd_Rept_Tot_Fin_Periodic_Dividend;

    private DbsGroup pnd_Rept_Tots_By_Mode;
    private DbsField pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod;
    private DbsField pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod;
    private DbsField pnd_Rept_Prod_Titles;
    private DbsField pnd_Rept_Amt_Titles;

    private DbsGroup pnd_Store_Cntrl_Payees;
    private DbsField pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Tiaa_Payees;
    private DbsField pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees;
    private DbsField pnd_Cntrl_Check_Dte;

    private DbsGroup pnd_Cntrl_Check_Dte__R_Field_10;
    private DbsField pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A;
    private DbsField pnd_Frst_Tme_Sw;
    private DbsField pnd_Updt_Cntrl;
    private DbsField pnd_Sve_Cpr_Payment_Mode;
    private DbsField pnd_Sve_Contract_Origin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_C_Contract_Number = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Number", "#C-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_C_Payee = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Payee", "#C-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Record_Code = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Record_Code", "#C-RECORD-CODE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_Rest_Of_Record_250 = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Rest_Of_Record_250", "#REST-OF-RECORD-250", 
            FieldType.STRING, 250);

        pnd_Work_Record_1__R_Field_1 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_1", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Header_Chk_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_Record_1__R_Field_2 = pnd_Work_Record_1__R_Field_1.newGroupInGroup("pnd_Work_Record_1__R_Field_2", "REDEFINE", pnd_Work_Record_1_Pnd_Header_Chk_Dte);
        pnd_Work_Record_1_Pnd_Header_Chk_Dte_A = pnd_Work_Record_1__R_Field_2.newFieldInGroup("pnd_Work_Record_1_Pnd_Header_Chk_Dte_A", "#HEADER-CHK-DTE-A", 
            FieldType.STRING, 8);

        pnd_Work_Record_1__R_Field_3 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_3", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_C_Contract_Option = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Option", "#C-CONTRACT-OPTION", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Origin = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Origin", "#C-CONTRACT-ORIGIN", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Issue_Date = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Issue_Date", "#C-CONTRACT-ISSUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Due_Dt", 
            "#C-CONTRACT-1ST-PAY-DUE-DT", FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Pay_Pd_Dt", 
            "#C-CONTRACT-1ST-PAY-PD-DT", FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_C_Currency_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Currency_Code", "#C-CURRENCY-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Type_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Type_Code", "#C-CONTRACT-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Payement_Meth", 
            "#C-CONTRACT-PAYEMENT-METH", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Pension_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Pension_Code", 
            "#C-CONTRACT-PENSION-CODE", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Joint_Cnv_Rcrd", 
            "#C-CONTRACT-JOINT-CNV-RCRD", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Original_Da_No", 
            "#C-CONTRACT-ORIGINAL-DA-NO", FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Rsd_Issue_Code", 
            "#C-CONTRACT-RSD-ISSUE-CODE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_1st_Ann_Xref", 
            "#C-CONTRACT-1ST-ANN-XREF", FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Dob", "#C-FIRST-ANNUITANT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annt_Mrt_Yob_Dte", 
            "#C-FIRST-ANNT-MRT-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Sex", "#C-FIRST-ANNUITANT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_First_Annuitant_Life_Ct", 
            "#C-FIRST-ANNUITANT-LIFE-CT", FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Annuitant_Dod = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Annuitant_Dod", "#C-ANNUITANT-DOD", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_2nd_Ann_Xref", 
            "#C-CONTRACT-2ND-ANN-XREF", FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dob", "#C-SECOND-ANNUITANT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Mrt_Yob_Dte", "#C-SECOND-MRT-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Sex", "#C-SECOND-ANNUITANT-SEX", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Dod", "#C-SECOND-ANNUITANT-DOD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Life_Ct", 
            "#C-SECOND-ANNUITANT-LIFE-CT", FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Second_Annuitant_Ss_No", 
            "#C-SECOND-ANNUITANT-SS-NO", FieldType.NUMERIC, 9);
        pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Dividend_Payee", 
            "#C-CONTRACT-DIVIDEND-PAYEE", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_C_Dividend_College_Code = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Dividend_College_Code", 
            "#C-DIVIDEND-COLLEGE-CODE", FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Contract_Inst_Issue_Cde", 
            "#C-CONTRACT-INST-ISSUE-CDE", FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_C_Last_Transaction_Date = pnd_Work_Record_1__R_Field_3.newFieldInGroup("pnd_Work_Record_1_Pnd_C_Last_Transaction_Date", 
            "#C-LAST-TRANSACTION-DATE", FieldType.TIME);

        pnd_Work_Record_1__R_Field_4 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_4", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Cpr_Id_Nbr = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_1__Filler1 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler1", "_FILLER1", FieldType.STRING, 7);
        pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde", "#CPR-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde", "#CPR-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1__Filler2 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr", "#CPR-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_1__Filler3 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Cpr_Status_Code = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Status_Code", "#CPR-STATUS-CODE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1__Filler4 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler4", "_FILLER4", FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind = pnd_Work_Record_1__R_Field_4.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_Cpr_Rcvry_Type_Ind", "#CPR-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt = pnd_Work_Record_1__R_Field_4.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_Cpr_Cntrct_Per_Ivc_Amt", 
            "#CPR-CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_1__Filler5 = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1__Filler5", "_FILLER5", FieldType.STRING, 120);
        pnd_Work_Record_1_Pnd_Cpr_Payment_Mode = pnd_Work_Record_1__R_Field_4.newFieldInGroup("pnd_Work_Record_1_Pnd_Cpr_Payment_Mode", "#CPR-PAYMENT-MODE", 
            FieldType.NUMERIC, 3);

        pnd_Work_Record_1__R_Field_5 = pnd_Work_Record_1.newGroupInGroup("pnd_Work_Record_1__R_Field_5", "REDEFINE", pnd_Work_Record_1_Pnd_Rest_Of_Record_250);
        pnd_Work_Record_1_Pnd_Fund_Company_Code_A3 = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Company_Code_A3", "#FUND-COMPANY-CODE-A3", 
            FieldType.STRING, 3);

        pnd_Work_Record_1__R_Field_6 = pnd_Work_Record_1__R_Field_5.newGroupInGroup("pnd_Work_Record_1__R_Field_6", "REDEFINE", pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);
        pnd_Work_Record_1_Pnd_Fund_Company_Code = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Company_Code", "#FUND-COMPANY-CODE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_Fund_Fund_Code = pnd_Work_Record_1__R_Field_6.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Fund_Code", "#FUND-FUND-CODE", 
            FieldType.STRING, 2);

        pnd_Work_Record_1__R_Field_7 = pnd_Work_Record_1__R_Field_6.newGroupInGroup("pnd_Work_Record_1__R_Field_7", "REDEFINE", pnd_Work_Record_1_Pnd_Fund_Fund_Code);
        pnd_Work_Record_1_Pnd_Fund_Fund_Code_N = pnd_Work_Record_1__R_Field_7.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Fund_Code_N", "#FUND-FUND-CODE-N", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Ivc_Amt", "#FUND-PERIODIC-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Rtb_Amont = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rtb_Amont", "#FUND-RTB-AMONT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Payment", "#FUND-PERIODIC-PAYMENT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend", "#FUND-PERIODIC-DIVIDEND", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Old_Periodic_Payment", 
            "#FUND-OLD-PERIODIC-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Old_Dividend_Payment", 
            "#FUND-OLD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Rate_Code = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rate_Code", "#FUND-RATE-CODE", 
            FieldType.STRING, 2);
        pnd_Work_Record_1_Pnd_Fund_Rate_Date = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Rate_Date", "#FUND-RATE-DATE", 
            FieldType.DATE);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Amount = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Amount", "#FUND-PERIODIC-AMOUNT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2 = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend_2", 
            "#FUND-PERIODIC-DIVIDEND-2", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units", "#FUND-TIAA-CREF-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Payment = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Payment", 
            "#FUND-FINAL-PERIODIC-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Dividend = pnd_Work_Record_1__R_Field_5.newFieldInGroup("pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Dividend", 
            "#FUND-FINAL-PERIODIC-DIVIDEND", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Date__R_Field_8", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_8.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Good_Record = localVariables.newFieldInRecord("pnd_Good_Record", "#GOOD-RECORD", FieldType.NUMERIC, 1);
        pnd_Search_Key_Rcrd = localVariables.newFieldInRecord("pnd_Search_Key_Rcrd", "#SEARCH-KEY-RCRD", FieldType.STRING, 10);

        pnd_Search_Key_Rcrd__R_Field_9 = localVariables.newGroupInRecord("pnd_Search_Key_Rcrd__R_Field_9", "REDEFINE", pnd_Search_Key_Rcrd);
        pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde = pnd_Search_Key_Rcrd__R_Field_9.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde", "#SEARCH-CNTRL-CDE", 
            FieldType.STRING, 2);
        pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte = pnd_Search_Key_Rcrd__R_Field_9.newFieldInGroup("pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte", "#SEARCH-TRANS-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Search_Trans_Dte_A = localVariables.newFieldInRecord("pnd_Search_Trans_Dte_A", "#SEARCH-TRANS-DTE-A", FieldType.NUMERIC, 8);
        pnd_Func = localVariables.newFieldInRecord("pnd_Func", "#FUNC", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cde = localVariables.newFieldInRecord("pnd_Max_Prd_Cde", "#MAX-PRD-CDE", FieldType.NUMERIC, 2);
        pnd_Mode_Sub = localVariables.newFieldInRecord("pnd_Mode_Sub", "#MODE-SUB", FieldType.NUMERIC, 2);
        pnd_Rept_Sub = localVariables.newFieldInRecord("pnd_Rept_Sub", "#REPT-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub = localVariables.newFieldInRecord("pnd_Prod_Sub", "#PROD-SUB", FieldType.NUMERIC, 2);
        pnd_Prod_Sub_Monthly = localVariables.newFieldInRecord("pnd_Prod_Sub_Monthly", "#PROD-SUB-MONTHLY", FieldType.NUMERIC, 2);
        pnd_Mode_Code = localVariables.newFieldArrayInRecord("pnd_Mode_Code", "#MODE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 22));
        pnd_Prod_Code = localVariables.newFieldArrayInRecord("pnd_Prod_Code", "#PROD-CODE", FieldType.STRING, 3, new DbsArrayController(1, 80));
        pnd_Rept_Tot_Periodic_Payment = localVariables.newFieldArrayInRecord("pnd_Rept_Tot_Periodic_Payment", "#REPT-TOT-PERIODIC-PAYMENT", FieldType.NUMERIC, 
            13, 2, new DbsArrayController(1, 23));
        pnd_Rept_Tot_Periodic_Dividend = localVariables.newFieldArrayInRecord("pnd_Rept_Tot_Periodic_Dividend", "#REPT-TOT-PERIODIC-DIVIDEND", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 23));
        pnd_Rept_Tot_Fin_Periodic_Payment = localVariables.newFieldArrayInRecord("pnd_Rept_Tot_Fin_Periodic_Payment", "#REPT-TOT-FIN-PERIODIC-PAYMENT", 
            FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23));
        pnd_Rept_Tot_Fin_Periodic_Dividend = localVariables.newFieldArrayInRecord("pnd_Rept_Tot_Fin_Periodic_Dividend", "#REPT-TOT-FIN-PERIODIC-DIVIDEND", 
            FieldType.NUMERIC, 13, 2, new DbsArrayController(1, 23));

        pnd_Rept_Tots_By_Mode = localVariables.newGroupArrayInRecord("pnd_Rept_Tots_By_Mode", "#REPT-TOTS-BY-MODE", new DbsArrayController(1, 23));
        pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod = pnd_Rept_Tots_By_Mode.newFieldArrayInGroup("pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod", 
            "#REPT-TOT-MODE-UNITS-PROD", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 40));
        pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod = pnd_Rept_Tots_By_Mode.newFieldArrayInGroup("pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod", 
            "#REPT-TOT-MODE-AMTS-PROD", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 40));
        pnd_Rept_Prod_Titles = localVariables.newFieldArrayInRecord("pnd_Rept_Prod_Titles", "#REPT-PROD-TITLES", FieldType.STRING, 23, new DbsArrayController(1, 
            41));
        pnd_Rept_Amt_Titles = localVariables.newFieldArrayInRecord("pnd_Rept_Amt_Titles", "#REPT-AMT-TITLES", FieldType.STRING, 23, new DbsArrayController(1, 
            40));

        pnd_Store_Cntrl_Payees = localVariables.newGroupArrayInRecord("pnd_Store_Cntrl_Payees", "#STORE-CNTRL-PAYEES", new DbsArrayController(1, 22));
        pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Tiaa_Payees = pnd_Store_Cntrl_Payees.newFieldInGroup("pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Tiaa_Payees", 
            "#STORE-CNTRL-TIAA-PAYEES", FieldType.PACKED_DECIMAL, 9);
        pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees = pnd_Store_Cntrl_Payees.newFieldArrayInGroup("pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees", 
            "#STORE-CNTRL-CREF-PAYEES", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 40));
        pnd_Cntrl_Check_Dte = localVariables.newFieldInRecord("pnd_Cntrl_Check_Dte", "#CNTRL-CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Cntrl_Check_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Cntrl_Check_Dte__R_Field_10", "REDEFINE", pnd_Cntrl_Check_Dte);
        pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A = pnd_Cntrl_Check_Dte__R_Field_10.newFieldInGroup("pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A", "#CNTRL-CHECK-DTE-A", 
            FieldType.STRING, 8);
        pnd_Frst_Tme_Sw = localVariables.newFieldInRecord("pnd_Frst_Tme_Sw", "#FRST-TME-SW", FieldType.STRING, 1);
        pnd_Updt_Cntrl = localVariables.newFieldInRecord("pnd_Updt_Cntrl", "#UPDT-CNTRL", FieldType.STRING, 1);
        pnd_Sve_Cpr_Payment_Mode = localVariables.newFieldInRecord("pnd_Sve_Cpr_Payment_Mode", "#SVE-CPR-PAYMENT-MODE", FieldType.NUMERIC, 3);
        pnd_Sve_Contract_Origin = localVariables.newFieldInRecord("pnd_Sve_Contract_Origin", "#SVE-CONTRACT-ORIGIN", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
        pnd_Good_Record.setInitialValue(0);
        pnd_Prod_Cnt.setInitialValue(0);
        pnd_Max_Prd_Cde.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap790() throws Exception
    {
        super("Iaap790");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD-1
        while (condition(getWorkFiles().read(1, pnd_Work_Record_1)))
        {
            if (condition(pnd_Work_Record_1_Pnd_C_Contract_Number.equals("   CHEADER")))                                                                                  //Natural: IF #C-CONTRACT-NUMBER = '   CHEADER'
            {
                iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Work_Record_1_Pnd_Header_Chk_Dte_A);                                   //Natural: MOVE EDITED #HEADER-CHK-DTE-A TO CNTRL-CHECK-DTE ( EM = YYYYMMDD )
                pnd_Cntrl_Check_Dte.setValue(pnd_Work_Record_1_Pnd_Header_Chk_Dte);                                                                                       //Natural: MOVE #HEADER-CHK-DTE TO #CNTRL-CHECK-DTE
                iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte.compute(new ComputeParameters(false, iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte), DbsField.subtract(100000000,                   //Natural: COMPUTE CNTRL-INVRSE-DTE = 100000000 - #HEADER-CHK-DTE
                    pnd_Work_Record_1_Pnd_Header_Chk_Dte));
                pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.compute(new ComputeParameters(false, pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte), DbsField.subtract(100000000,     //Natural: COMPUTE #SEARCH-TRANS-DTE = 100000000 - #HEADER-CHK-DTE
                    pnd_Work_Record_1_Pnd_Header_Chk_Dte));
                pnd_Search_Trans_Dte_A.setValue(pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte);                                                                                //Natural: MOVE #SEARCH-TRANS-DTE TO #SEARCH-TRANS-DTE-A
                pnd_Search_Key_Rcrd_Pnd_Search_Trans_Dte.setValue(iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte);                                                                     //Natural: MOVE CNTRL-INVRSE-DTE TO #SEARCH-TRANS-DTE
                //*  ADDED FOLLOWING NEW PA SELECT                 5/00
                pnd_Frst_Tme_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #FRST-TME-SW := 'Y'
                DbsUtil.callnat(Iaap790a.class , getCurrentProcessState(), pnd_Work_Record_1, pnd_Mode_Sub, pnd_Updt_Cntrl, pnd_Frst_Tme_Sw);                             //Natural: CALLNAT 'IAAP790A' #WORK-RECORD-1 #MODE-SUB #UPDT-CNTRL #FRST-TME-SW
                if (condition(Global.isEscape())) return;
                pnd_Frst_Tme_Sw.reset();                                                                                                                                  //Natural: RESET #FRST-TME-SW
                //*  END OF ADD NEW PA SELECT                 5/00
            }                                                                                                                                                             //Natural: END-IF
            //*   ADDED FOLLOWING 5/00
            //*  CNTRCT RECORD
            if (condition(pnd_Work_Record_1_Pnd_C_Record_Code.equals(10)))                                                                                                //Natural: IF #C-RECORD-CODE = 10
            {
                pnd_Sve_Contract_Origin.setValue(pnd_Work_Record_1_Pnd_C_Contract_Origin);                                                                                //Natural: ASSIGN #SVE-CONTRACT-ORIGIN := #C-CONTRACT-ORIGIN
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 5/00
            //*  CPR RECORD
            if (condition(pnd_Work_Record_1_Pnd_C_Record_Code.equals(20)))                                                                                                //Natural: IF #C-RECORD-CODE = 20
            {
                if (condition(pnd_Work_Record_1_Pnd_Cpr_Status_Code.equals(9)))                                                                                           //Natural: IF #CPR-STATUS-CODE = 9
                {
                    pnd_Good_Record.setValue(0);                                                                                                                          //Natural: MOVE 0 TO #GOOD-RECORD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Good_Record.setValue(1);                                                                                                                          //Natural: MOVE 1 TO #GOOD-RECORD
                    short decideConditionsMet255 = 0;                                                                                                                     //Natural: DECIDE ON EVERY VALUE #CPR-PAYMENT-MODE;//Natural: VALUE 100
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(100)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(1);                                                                                                                         //Natural: MOVE 1 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 601
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(601)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(2);                                                                                                                         //Natural: MOVE 2 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 602
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(602)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(3);                                                                                                                         //Natural: MOVE 3 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 603
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(603)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(4);                                                                                                                         //Natural: MOVE 4 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 701
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(701)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(5);                                                                                                                         //Natural: MOVE 5 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 702
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(702)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(6);                                                                                                                         //Natural: MOVE 6 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 703
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(703)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(7);                                                                                                                         //Natural: MOVE 7 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 704
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(704)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(8);                                                                                                                         //Natural: MOVE 8 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 705
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(705)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(9);                                                                                                                         //Natural: MOVE 9 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 706
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(706)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(10);                                                                                                                        //Natural: MOVE 10 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 801
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(801)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(11);                                                                                                                        //Natural: MOVE 11 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 802
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(802)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(12);                                                                                                                        //Natural: MOVE 12 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 803
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(803)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(13);                                                                                                                        //Natural: MOVE 13 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 804
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(804)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(14);                                                                                                                        //Natural: MOVE 14 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 805
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(805)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(15);                                                                                                                        //Natural: MOVE 15 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 806
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(806)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(16);                                                                                                                        //Natural: MOVE 16 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 807
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(807)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(17);                                                                                                                        //Natural: MOVE 17 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 808
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(808)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(18);                                                                                                                        //Natural: MOVE 18 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 809
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(809)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(19);                                                                                                                        //Natural: MOVE 19 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 810
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(810)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(20);                                                                                                                        //Natural: MOVE 20 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 811
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(811)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(21);                                                                                                                        //Natural: MOVE 21 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: VALUE 812
                    if (condition(pnd_Work_Record_1_Pnd_Cpr_Payment_Mode.equals(812)))
                    {
                        decideConditionsMet255++;
                        pnd_Mode_Sub.setValue(22);                                                                                                                        //Natural: MOVE 22 TO #MODE-SUB
                    }                                                                                                                                                     //Natural: NONE VALUE
                    if (condition(decideConditionsMet255 == 0))
                    {
                        getReports().write(0, ReportOption.NOHDR,"********************************");                                                                     //Natural: WRITE NOHDR '********************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOHDR,"| INVALID MODE FOUND ",pnd_Work_Record_1_Pnd_Cpr_Payment_Mode,"    |");                                 //Natural: WRITE NOHDR '| INVALID MODE FOUND ' #CPR-PAYMENT-MODE '    |'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",pnd_Work_Record_1_Pnd_C_Contract_Number,"=",pnd_Work_Record_1_Pnd_C_Payee,"=",pnd_Work_Record_1_Pnd_C_Record_Code,      //Natural: WRITE '=' #C-CONTRACT-NUMBER '=' #C-PAYEE '=' #C-RECORD-CODE '=' #CPR-ID-NBR '=' #CPR-CTZNSHP-CDE
                            "=",pnd_Work_Record_1_Pnd_Cpr_Id_Nbr,"=",pnd_Work_Record_1_Pnd_Cpr_Ctznshp_Cde);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",pnd_Work_Record_1_Pnd_Cpr_Rsdncy_Cde,"=",pnd_Work_Record_1_Pnd_Cpr_Tax_Id_Nbr,"=",pnd_Work_Record_1_Pnd_Cpr_Status_Code, //Natural: WRITE '=' #CPR-RSDNCY-CDE '=' #CPR-TAX-ID-NBR '=' #CPR-STATUS-CODE '=' #CPR-PAYMENT-MODE
                            "=",pnd_Work_Record_1_Pnd_Cpr_Payment_Mode);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, ReportOption.NOHDR,"********************************");                                                                     //Natural: WRITE NOHDR '********************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Good_Record.setValue(0);                                                                                                                      //Natural: MOVE 0 TO #GOOD-RECORD
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Record_1_Pnd_C_Record_Code.equals(30) && pnd_Good_Record.equals(1)))                                                                   //Natural: IF #C-RECORD-CODE = 30 AND #GOOD-RECORD = 1
            {
                                                                                                                                                                          //Natural: PERFORM FUND-EXTRACT
                sub_Fund_Extract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  WRITE THE REPORT AND CHANGE THE CONTROL RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-1
        sub_Write_Report_1();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-RECORD
        sub_Write_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ADDED FOLLOWING NEW PA SELECT                 5/00
        pnd_Updt_Cntrl.setValue("Y");                                                                                                                                     //Natural: ASSIGN #UPDT-CNTRL := 'Y'
        DbsUtil.callnat(Iaap790a.class , getCurrentProcessState(), pnd_Work_Record_1, pnd_Mode_Sub, pnd_Updt_Cntrl, pnd_Frst_Tme_Sw);                                     //Natural: CALLNAT 'IAAP790A' #WORK-RECORD-1 #MODE-SUB #UPDT-CNTRL #FRST-TME-SW
        if (condition(Global.isEscape())) return;
        //*  END OF ADD NEW PA SELECT                 5/00
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FUND-EXTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-1
        //*  END OF ADD 3/01
        //*  ANNUAL FUND TOTAL
        //*  WRITE THE REAL ESTATE TOTALS
        //*  ADD TIAA ACCESS                        9/08 START
        //*  END OF ADD 3/01
        //*  ANNUAL FUND TOTAL
        //*  WRITE THE REAL ESTATE TOTALS AND TIAA ACCESS   9/08 START
        //*  END OF ADD 3/01
        //*  ANNUAL FUND TOTAL
        //*  WRITE THE REAL ESTATE TOTALS AND TIAA ACCESS    9/08 START
        //*  END OF ADD 3/01
        //*  ANNUAL FUND TOTAL
        //*  WRITE THE REAL ESTATE TOTALS  AND TIAA ACCESS    9/08 START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-RECORD
        //*  END OF ADD 3/01
    }
    private void sub_Fund_Extract() throws Exception                                                                                                                      //Natural: FUND-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S"))) //Natural: IF #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Record_1_Pnd_Fund_Fund_Code.setValue(pnd_Work_Record_1_Pnd_Fund_Fund_Code, MoveOption.RightJustified);                                               //Natural: MOVE RIGHT #FUND-FUND-CODE TO #FUND-FUND-CODE
            DbsUtil.examine(new ExamineSource(pnd_Work_Record_1_Pnd_Fund_Fund_Code), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE #FUND-FUND-CODE FOR ' ' REPLACE '0'
        }                                                                                                                                                                 //Natural: END-IF
        //*  LB 10/97
        short decideConditionsMet408 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S")))
        {
            decideConditionsMet408++;
            //*  ANNUAL FUNDS
            pnd_Prod_Sub.setValue(0);                                                                                                                                     //Natural: MOVE 0 TO #PROD-SUB
        }                                                                                                                                                                 //Natural: WHEN #FUND-COMPANY-CODE = '2' OR = 'U'
        else if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("2") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("U")))
        {
            decideConditionsMet408++;
            pnd_Prod_Sub.setValue(pnd_Work_Record_1_Pnd_Fund_Fund_Code_N);                                                                                                //Natural: MOVE #FUND-FUND-CODE-N TO #PROD-SUB
            //*  MONTHLY FUNDS
            pnd_Prod_Code.getValue(pnd_Prod_Sub).setValue(pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);                                                                    //Natural: MOVE #FUND-COMPANY-CODE-A3 TO #PROD-CODE ( #PROD-SUB )
        }                                                                                                                                                                 //Natural: WHEN #FUND-COMPANY-CODE = '4' OR = 'W'
        else if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("4") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("W")))
        {
            decideConditionsMet408++;
            pnd_Prod_Sub.compute(new ComputeParameters(false, pnd_Prod_Sub), pnd_Work_Record_1_Pnd_Fund_Fund_Code_N.add(20));                                             //Natural: ASSIGN #PROD-SUB = #FUND-FUND-CODE-N + 20
            pnd_Prod_Code.getValue(pnd_Prod_Sub).setValue(pnd_Work_Record_1_Pnd_Fund_Company_Code_A3);                                                                    //Natural: MOVE #FUND-COMPANY-CODE-A3 TO #PROD-CODE ( #PROD-SUB )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"| INVALID PROD CODE  ",pnd_Work_Record_1_Pnd_Fund_Fund_Code);                                                       //Natural: WRITE NOHDR '| INVALID PROD CODE  ' #FUND-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"| OR INVALID COMPANY CODE  ",pnd_Work_Record_1_Pnd_Fund_Company_Code);                                              //Natural: WRITE NOHDR '| OR INVALID COMPANY CODE  ' #FUND-COMPANY-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"=============================");                                                                                    //Natural: WRITE NOHDR '============================='
            if (Global.isEscape()) return;
            //*  LB 10/97
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ADD TOTALS
        if (condition(pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1 ") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1G") || pnd_Work_Record_1_Pnd_Fund_Fund_Code.equals("1S"))) //Natural: IF #FUND-FUND-CODE = '1 ' OR = '1G' OR = '1S'
        {
            pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                                       //Natural: ADD #FUND-PERIODIC-PAYMENT TO #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB )
            pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend);                                                     //Natural: ADD #FUND-PERIODIC-DIVIDEND TO #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB )
            pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Tiaa_Payees.getValue(pnd_Mode_Sub).nadd(1);                                                                            //Natural: ADD 1 TO #STORE-CNTRL-TIAA-PAYEES ( #MODE-SUB )
            //*  ADEED FOLLOWING 3/01 ACCUM FINAL PMT AMTS
            pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Payment);                                             //Natural: ADD #FUND-FINAL-PERIODIC-PAYMENT TO #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB )
            pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Dividend);                                           //Natural: ADD #FUND-FINAL-PERIODIC-DIVIDEND TO #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB )
            pnd_Rept_Tot_Fin_Periodic_Payment.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Payment);                                                       //Natural: ADD #FUND-FINAL-PERIODIC-PAYMENT TO #REPT-TOT-FIN-PERIODIC-PAYMENT ( 23 )
            pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Final_Periodic_Dividend);                                                     //Natural: ADD #FUND-FINAL-PERIODIC-DIVIDEND TO #REPT-TOT-FIN-PERIODIC-DIVIDEND ( 23 )
            //*  END OF ADD  3/01
            //*  ACCUMULATE TOTALS
            pnd_Rept_Tot_Periodic_Payment.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                                                                 //Natural: ADD #FUND-PERIODIC-PAYMENT TO #REPT-TOT-PERIODIC-PAYMENT ( 23 )
            pnd_Rept_Tot_Periodic_Dividend.getValue(23).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Dividend);                                                               //Natural: ADD #FUND-PERIODIC-DIVIDEND TO #REPT-TOT-PERIODIC-DIVIDEND ( 23 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   ADDED FOLLOWING 5/00
            //*  JFT 11/14/07 -- TO FIX
            if (condition(pnd_Sve_Contract_Origin.equals(37) || pnd_Sve_Contract_Origin.equals(38) || pnd_Sve_Contract_Origin.equals(40) || pnd_Sve_Contract_Origin.equals(35)  //Natural: IF #SVE-CONTRACT-ORIGIN = 37 OR = 38 OR = 40 OR = 35 OR = 36 OR = 43 OR = 44 OR = 45 OR = 46
                || pnd_Sve_Contract_Origin.equals(36) || pnd_Sve_Contract_Origin.equals(43) || pnd_Sve_Contract_Origin.equals(44) || pnd_Sve_Contract_Origin.equals(45) 
                || pnd_Sve_Contract_Origin.equals(46)))
            {
                //*  PRODUCTION PROBLEM
                DbsUtil.callnat(Iaap790a.class , getCurrentProcessState(), pnd_Work_Record_1, pnd_Mode_Sub, pnd_Updt_Cntrl, pnd_Frst_Tme_Sw);                             //Natural: CALLNAT 'IAAP790A' #WORK-RECORD-1 #MODE-SUB #UPDT-CNTRL #FRST-TME-SW
                if (condition(Global.isEscape())) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 5/00
            pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units);                      //Natural: ADD #FUND-TIAA-CREF-UNITS TO #REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB )
            pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Tiaa_Cref_Units);                                //Natural: ADD #FUND-TIAA-CREF-UNITS TO #REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB )
            pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(1);                                                               //Natural: ADD 1 TO #STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB )
            //*  ANNUAL FUNDS       /* LB 10/97
            if (condition(pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("2") || pnd_Work_Record_1_Pnd_Fund_Company_Code.equals("U")))                                    //Natural: IF #FUND-COMPANY-CODE = '2' OR = 'U'
            {
                //*  3/97
                pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(pnd_Mode_Sub,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                  //Natural: ADD #FUND-PERIODIC-PAYMENT TO #REPT-TOT-MODE-AMTS-PROD ( #MODE-SUB,#PROD-SUB )
                pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(23,pnd_Prod_Sub).nadd(pnd_Work_Record_1_Pnd_Fund_Periodic_Payment);                            //Natural: ADD #FUND-PERIODIC-PAYMENT TO #REPT-TOT-MODE-AMTS-PROD ( 23,#PROD-SUB )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FUND-EXTRACT
    }
    private void sub_Write_Report_1() throws Exception                                                                                                                    //Natural: WRITE-REPORT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Rept_Prod_Titles.getValue(1).setValue("TIAA PERIODIC PAYMENT ");                                                                                              //Natural: MOVE 'TIAA PERIODIC PAYMENT ' TO #REPT-PROD-TITLES ( 1 )
        pnd_Rept_Prod_Titles.getValue(41).setValue("TIAA PERIODIC DIVIDEND");                                                                                             //Natural: MOVE 'TIAA PERIODIC DIVIDEND' TO #REPT-PROD-TITLES ( 41 )
        pnd_Rept_Prod_Titles.getValue(2).setValue("CREF STOCK       UNITS");                                                                                              //Natural: MOVE 'CREF STOCK       UNITS' TO #REPT-PROD-TITLES ( 2 )
        pnd_Rept_Prod_Titles.getValue(3).setValue("CREF MMA         UNITS");                                                                                              //Natural: MOVE 'CREF MMA         UNITS' TO #REPT-PROD-TITLES ( 3 )
        pnd_Rept_Prod_Titles.getValue(4).setValue("CREF SOCIAL      UNITS");                                                                                              //Natural: MOVE 'CREF SOCIAL      UNITS' TO #REPT-PROD-TITLES ( 4 )
        pnd_Rept_Prod_Titles.getValue(5).setValue("CREF BOND        UNITS");                                                                                              //Natural: MOVE 'CREF BOND        UNITS' TO #REPT-PROD-TITLES ( 5 )
        pnd_Rept_Prod_Titles.getValue(6).setValue("CREF GLOBAL      UNITS");                                                                                              //Natural: MOVE 'CREF GLOBAL      UNITS' TO #REPT-PROD-TITLES ( 6 )
        pnd_Rept_Prod_Titles.getValue(7).setValue("CREF EQUITY      UNITS");                                                                                              //Natural: MOVE 'CREF EQUITY      UNITS' TO #REPT-PROD-TITLES ( 7 )
        pnd_Rept_Prod_Titles.getValue(8).setValue("CREF GROWTH      UNITS");                                                                                              //Natural: MOVE 'CREF GROWTH      UNITS' TO #REPT-PROD-TITLES ( 8 )
        pnd_Rept_Prod_Titles.getValue(9).setValue("TIAA REAL-ESTATE UNITS");                                                                                              //Natural: MOVE 'TIAA REAL-ESTATE UNITS' TO #REPT-PROD-TITLES ( 9 )
        pnd_Rept_Prod_Titles.getValue(10).setValue("CREF ILB         UNITS");                                                                                             //Natural: MOVE 'CREF ILB         UNITS' TO #REPT-PROD-TITLES ( 10 )
        pnd_Rept_Prod_Titles.getValue(11).setValue("TIAA ACCESS      UNITS");                                                                                             //Natural: MOVE 'TIAA ACCESS      UNITS' TO #REPT-PROD-TITLES ( 11 )
        //*  DOLLAR AMOUNT REPORT TITLES ADDED 3/97
        pnd_Rept_Amt_Titles.getValue(2).setValue("CREF STOCK      AMOUNT");                                                                                               //Natural: MOVE 'CREF STOCK      AMOUNT' TO #REPT-AMT-TITLES ( 2 )
        pnd_Rept_Amt_Titles.getValue(3).setValue("CREF MMA        AMOUNT");                                                                                               //Natural: MOVE 'CREF MMA        AMOUNT' TO #REPT-AMT-TITLES ( 3 )
        pnd_Rept_Amt_Titles.getValue(4).setValue("CREF SOCIAL     AMOUNT");                                                                                               //Natural: MOVE 'CREF SOCIAL     AMOUNT' TO #REPT-AMT-TITLES ( 4 )
        pnd_Rept_Amt_Titles.getValue(5).setValue("CREF BOND       AMOUNT");                                                                                               //Natural: MOVE 'CREF BOND       AMOUNT' TO #REPT-AMT-TITLES ( 5 )
        pnd_Rept_Amt_Titles.getValue(6).setValue("CREF GLOBAL     AMOUNT");                                                                                               //Natural: MOVE 'CREF GLOBAL     AMOUNT' TO #REPT-AMT-TITLES ( 6 )
        pnd_Rept_Amt_Titles.getValue(7).setValue("CREF EQUITY     AMOUNT");                                                                                               //Natural: MOVE 'CREF EQUITY     AMOUNT' TO #REPT-AMT-TITLES ( 7 )
        pnd_Rept_Amt_Titles.getValue(8).setValue("CREF GROWTH     AMOUNT");                                                                                               //Natural: MOVE 'CREF GROWTH     AMOUNT' TO #REPT-AMT-TITLES ( 8 )
        pnd_Rept_Amt_Titles.getValue(9).setValue("TIAA REAL-ESTATE AMOUNT");                                                                                              //Natural: MOVE 'TIAA REAL-ESTATE AMOUNT' TO #REPT-AMT-TITLES ( 9 )
        pnd_Rept_Amt_Titles.getValue(10).setValue("CREF ILB        AMOUNT");                                                                                              //Natural: MOVE 'CREF ILB        AMOUNT' TO #REPT-AMT-TITLES ( 10 )
        pnd_Rept_Amt_Titles.getValue(11).setValue("TIAA ACCESS     AMOUNT");                                                                                              //Natural: MOVE 'TIAA ACCESS     AMOUNT' TO #REPT-AMT-TITLES ( 11 )
        //*   #REPT-MODE-SUB = DETERMINES WHICH MODE TO PRINT
        //* **************************
        //*  PAGE 1
        //* **************************
        pnd_Mode_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #MODE-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        //*   PRINT MODES 100 THRU 603
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"100",new ColumnSpacing(51),"601",new ColumnSpacing(15),"602",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '100' 51X '601' 15X '602' 15X '603'
            ColumnSpacing(15),"603");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(1),new ColumnSpacing(1),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub),            //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 1 ) 01X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(41),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub),          //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 41 ) 03X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  LEFT
        //*  ADDED FOLLOWING 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT     ",new ColumnSpacing(1),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub),               //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT     ' 01X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new 
            ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL DIVIDEND    ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub),              //Natural: WRITE ( 1 ) 'TIAA FINAL DIVIDEND    ' 03X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #I ) 03X #REPT-TOT-MODE-UNITS-PROD ( 1,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X #REPT-TOT-MODE-UNITS-PROD ( 2,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 3,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 4,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(1,pnd_I),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #I ) 03X #REPT-TOT-MODE-AMTS-PROD ( 1,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-MODE-AMTS-PROD ( 2,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 3,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 4,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(2,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(3,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(4,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                //*  ADD NEW PRODUCT DESCRIPTION
                if (condition(pnd_Prod_Sub.greater(10)))                                                                                                                  //Natural: IF #PROD-SUB > 10
                {
                    pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub).reset();                                                                                                  //Natural: RESET #REPT-PROD-TITLES ( #PROD-SUB )
                    pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub).setValue(DbsUtil.compress("CODE ANN=", pnd_Prod_Code.getValue(pnd_Prod_Sub), "MNTHLY=",                   //Natural: COMPRESS 'CODE ANN=' #PROD-CODE ( #PROD-SUB ) 'MNTHLY=' #PROD-CODE ( #PROD-SUB + 20 ) INTO #REPT-PROD-TITLES ( #PROD-SUB )
                        pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20))));
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 1, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X #REPT-TOT-MODE-UNITS-PROD ( 2, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 3, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 4, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  3/97
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(1,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-AMTS-PROD ( 1, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 40X #REPT-TOT-MODE-AMTS-PROD ( 2, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 3, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 4, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(2,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(3,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(4,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        //*  WRITE THE REAL ESTATE TOTALS AND TIAA ACCESS   9/08 START
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #J ) 03X #REPT-TOT-MODE-UNITS-PROD ( 1, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X #REPT-TOT-MODE-UNITS-PROD ( 2, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 3, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 4, #I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #PROD-SUB = 1 TO 20
        for (pnd_Prod_Sub.setValue(1); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(1,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 1, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 40X #REPT-TOT-MODE-UNITS-PROD ( 2, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 3, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 4, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(40),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(2,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(3,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(4,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *************************
        //*  PAGE 2  PAYMENT MODES 701 THRU 706
        //* *************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(5);                                                                                                                                         //Natural: MOVE 5 TO #MODE-SUB
        //*   PRINT MODES 701 THRU 812
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"701",new ColumnSpacing(15),"702",new ColumnSpacing(15),"703",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '701' 15X '702' 15X '703' 15X '704' 15X '705' 15X '706'
            ColumnSpacing(15),"704",new ColumnSpacing(15),"705",new ColumnSpacing(15),"706");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(1),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub),            //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 1 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(41),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub),          //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 41 ) 03X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  LEFT
        //*  ADDED FOLLOWING 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT     ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub),               //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT     ' 03X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL DIVIDEND    ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub),              //Natural: WRITE ( 1 ) 'TIAA FINAL DIVIDEND    ' 03X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #I ) 03X #REPT-TOT-MODE-UNITS-PROD ( 5,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 6,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 7,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 8,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 9,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 10,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(5,pnd_I),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #I ) 03X #REPT-TOT-MODE-AMTS-PROD ( 5,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 6,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 7,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 8,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 9,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 10,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(6,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(7,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(8,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(9,pnd_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(10,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 5, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 6, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 7, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 8, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 9, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 10, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(5,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-AMTS-PROD ( 5, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 6, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 7, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 8, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 9, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 10,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(6,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(7,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(8,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(9,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(10,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  9/08 START
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #J ) 03X #REPT-TOT-MODE-UNITS-PROD ( 5,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 6,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 7,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 8,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 9,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 10,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(5,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 5, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 6, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 7, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 8, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 9, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 10, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(6,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(7,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(8,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(9,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(10,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **************************
        //*  PAGE 3
        //* **************************
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Mode_Sub.setValue(11);                                                                                                                                        //Natural: MOVE 11 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"801",new ColumnSpacing(15),"802",new ColumnSpacing(15),"803",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '801' 15X '802' 15X '803' 15X '804' 15X '805' 15X '806'
            ColumnSpacing(15),"804",new ColumnSpacing(15),"805",new ColumnSpacing(15),"806");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(1),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub),            //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 1 ) 03X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(41),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub),          //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 41 ) 03X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  LEFT
        //*  ADDED FOLLOWING 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT     ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub),               //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT     ' 03X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL DIVIDEND    ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub),              //Natural: WRITE ( 1 ) 'TIAA FINAL DIVIDEND    ' 03X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT05:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #I ) 03X #REPT-TOT-MODE-UNITS-PROD ( 11,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 12,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 13,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 14,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 15,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 16,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(11,pnd_I),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #I ) 03X #REPT-TOT-MODE-AMTS-PROD ( 11,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 12,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 13,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 14,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 15,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 16,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(12,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(13,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(14,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(15,pnd_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(16,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 11, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 12, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 13, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 14, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 15, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 16, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(11,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-AMTS-PROD ( 11,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 12,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 13,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 14,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 15,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 16,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(12,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(13,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(14,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(15,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(16,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  9/08 START
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT06:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #J ) 03X #REPT-TOT-MODE-UNITS-PROD ( 11, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 12, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 13, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 14, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 15, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 16, #I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(11,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 11, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 12, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 13, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 14, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 15, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 16, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(12,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(13,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(14,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(15,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(16,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **************************
        //*  PAGE 4
        //* **************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(17);                                                                                                                                        //Natural: MOVE 17 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"PAYMENT MODE",new ColumnSpacing(25),"807",new ColumnSpacing(15),"808",new ColumnSpacing(15),"809",new                 //Natural: WRITE ( 1 ) 'PAYMENT MODE' 25X '807' 15X '808' 15X '809' 15X '810' 15X '811' 15X '812'
            ColumnSpacing(15),"810",new ColumnSpacing(15),"811",new ColumnSpacing(15),"812");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Rept_Sub),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub), //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #REPT-SUB ) 03X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Rept_Sub),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #REPT-SUB ) 03X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB +5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  LEFT
        //*  ADDED FOLLOWING 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT     ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub),               //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT     ' 03X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL DIVIDEND    ",new ColumnSpacing(3),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub),              //Natural: WRITE ( 1 ) 'TIAA FINAL DIVIDEND    ' 03X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 1 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 2 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 3 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 4 ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB + 5 ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(1)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(2)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
            ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(3)), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(4)), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub.getDec().add(5)), new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT07:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #I ) 03X #REPT-TOT-MODE-UNITS-PROD ( 17,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 18,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 19,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 20,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 21,#I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 22,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(17,pnd_I),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #I ) 03X #REPT-TOT-MODE-AMTS-PROD ( 17,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 18,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 19,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 20,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 21,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 22,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(18,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(19,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(20,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(21,pnd_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(22,pnd_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 17, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 18, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 19, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 20, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 21, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 22, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT LINES ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(17,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-AMTS-PROD ( 17,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 18,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 19,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 20,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 21,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 04X #REPT-TOT-MODE-AMTS-PROD ( 22,#PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(18,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(19,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(20,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(21,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(22,pnd_Prod_Sub), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY FUND TOTAL
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT08:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #J ) 03X #REPT-TOT-MODE-UNITS-PROD ( 17, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 18, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 19, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 20, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 21, #I ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 22, #I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_I), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_I), new ReportEditMask 
                ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(17,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 17, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 18, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 19, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 20, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 21, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 ) 04X #REPT-TOT-MODE-UNITS-PROD ( 22, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(18,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(19,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(20,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(21,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(22,pnd_Prod_Sub_Monthly), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **************************
        //*  PAGE 5
        //* **************************
        pnd_Rept_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #REPT-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        pnd_Mode_Sub.setValue(23);                                                                                                                                        //Natural: MOVE 23 TO #MODE-SUB
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3
        getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL ALL MODES");                                                                                              //Natural: WRITE ( 1 ) 'GRAND TOTAL ALL MODES'
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        //*   PRINT GRAND TOTAL FOR ALL MODES
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Rept_Sub),new ColumnSpacing(1),pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub), //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #REPT-SUB ) 01X #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Rept_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #REPT-SUB
        //*   ADDED FOLLOWING  3/01 PRINT GRAND TOTALS
        //*  ADDED 3/01
        getReports().write(1, ReportOption.NOTITLE,"TIAA FINAL PAYMENT     ",new ColumnSpacing(1),pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub),               //Natural: WRITE ( 1 ) 'TIAA FINAL PAYMENT     ' 01X #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*   END OF ADD  3/01
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Rept_Sub),new ColumnSpacing(3),pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #REPT-SUB ) 03X #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        pnd_Rept_Sub.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #REPT-SUB
        //*  ANNUAL TOTALS
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  9/08 START
        getReports().write(1, ReportOption.NOTITLE,"************************************** ANNUAL FUND TOTALS","**************************************");                 //Natural: WRITE ( 1 ) '************************************** ANNUAL FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(9);                                                                                                                                                //Natural: ASSIGN #I := 9
        REPEAT09:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #I ) 03X #REPT-TOT-MODE-UNITS-PROD ( 23,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_I),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(23,pnd_I),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #I ) 03X #REPT-TOT-MODE-AMTS-PROD ( 23,#I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            if (condition(pnd_I.greater(11)))                                                                                                                             //Natural: IF #I GT 11
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
            //*  LB 10/97
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AMOUNT TOTALS ADDED 3/97
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Amt_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(23,pnd_Prod_Sub),  //Natural: WRITE ( 1 ) #REPT-AMT-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-AMTS-PROD ( 23, #PROD-SUB ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  MONTHLY TOTALS                                             /* LB 10/97
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  9/08 START
        getReports().write(1, ReportOption.NOTITLE,"************************************** MONTHLY FUND TOTALS","**************************************");                //Natural: WRITE ( 1 ) '************************************** MONTHLY FUND TOTALS' '**************************************'
        if (Global.isEscape()) return;
        pnd_I.setValue(29);                                                                                                                                               //Natural: ASSIGN #I := 29
        pnd_J.setValue(9);                                                                                                                                                //Natural: ASSIGN #J := 9
        REPEAT10:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_J),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_I),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #J ) 03X #REPT-TOT-MODE-UNITS-PROD ( 23,#I ) ( EM = ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #I
            pnd_J.nadd(2);                                                                                                                                                //Natural: ADD 2 TO #J
            if (condition(pnd_I.greater(31)))                                                                                                                             //Natural: IF #I GT 31
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  9/08 END
            //*  TIAA ACCESS 09/08
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(11),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,31),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( 11 ) 03X #REPT-TOT-MODE-UNITS-PROD ( 23,31 ) ( EM = ZZ,ZZZ,ZZZ.999 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #PROD-SUB = 2 TO 20
        for (pnd_Prod_Sub.setValue(2); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
        {
            if (condition(pnd_Prod_Sub.equals(9) || pnd_Prod_Sub.equals(11)))                                                                                             //Natural: IF #PROD-SUB = 9 OR = 11
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prod_Code.getValue(pnd_Prod_Sub).notEquals(" ") || pnd_Prod_Code.getValue(pnd_Prod_Sub.getDec().add(20)).notEquals(" ")))                   //Natural: IF #PROD-CODE ( #PROD-SUB ) NE ' ' OR #PROD-CODE ( #PROD-SUB + 20 ) NE ' '
            {
                pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                                   //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub).setValue(DbsUtil.compress(pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub), pnd_Prod_Code.getValue(pnd_Prod_Sub_Monthly))); //Natural: COMPRESS #REPT-PROD-TITLES ( #PROD-SUB ) #PROD-CODE ( #PROD-SUB-MONTHLY ) INTO #REPT-PROD-TITLES ( #PROD-SUB )
                getReports().write(1, ReportOption.NOTITLE,pnd_Rept_Prod_Titles.getValue(pnd_Prod_Sub),new ColumnSpacing(3),pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(23,pnd_Prod_Sub_Monthly),  //Natural: WRITE ( 1 ) #REPT-PROD-TITLES ( #PROD-SUB ) 03X #REPT-TOT-MODE-UNITS-PROD ( 23, #PROD-SUB-MONTHLY ) ( EM = ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-REPORT-1
    }
    private void sub_Write_Control_Record() throws Exception                                                                                                              //Natural: WRITE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Mode_Code.getValue(1).setValue("MA");                                                                                                                         //Natural: MOVE 'MA' TO #MODE-CODE ( 1 )
        pnd_Mode_Code.getValue(2).setValue("MB");                                                                                                                         //Natural: MOVE 'MB' TO #MODE-CODE ( 2 )
        pnd_Mode_Code.getValue(3).setValue("MC");                                                                                                                         //Natural: MOVE 'MC' TO #MODE-CODE ( 3 )
        pnd_Mode_Code.getValue(4).setValue("MD");                                                                                                                         //Natural: MOVE 'MD' TO #MODE-CODE ( 4 )
        pnd_Mode_Code.getValue(5).setValue("ME");                                                                                                                         //Natural: MOVE 'ME' TO #MODE-CODE ( 5 )
        pnd_Mode_Code.getValue(6).setValue("MF");                                                                                                                         //Natural: MOVE 'MF' TO #MODE-CODE ( 6 )
        pnd_Mode_Code.getValue(7).setValue("MG");                                                                                                                         //Natural: MOVE 'MG' TO #MODE-CODE ( 7 )
        pnd_Mode_Code.getValue(8).setValue("MH");                                                                                                                         //Natural: MOVE 'MH' TO #MODE-CODE ( 8 )
        pnd_Mode_Code.getValue(9).setValue("MI");                                                                                                                         //Natural: MOVE 'MI' TO #MODE-CODE ( 9 )
        pnd_Mode_Code.getValue(10).setValue("MJ");                                                                                                                        //Natural: MOVE 'MJ' TO #MODE-CODE ( 10 )
        pnd_Mode_Code.getValue(11).setValue("MK");                                                                                                                        //Natural: MOVE 'MK' TO #MODE-CODE ( 11 )
        pnd_Mode_Code.getValue(12).setValue("ML");                                                                                                                        //Natural: MOVE 'ML' TO #MODE-CODE ( 12 )
        pnd_Mode_Code.getValue(13).setValue("MM");                                                                                                                        //Natural: MOVE 'MM' TO #MODE-CODE ( 13 )
        pnd_Mode_Code.getValue(14).setValue("MN");                                                                                                                        //Natural: MOVE 'MN' TO #MODE-CODE ( 14 )
        pnd_Mode_Code.getValue(15).setValue("MO");                                                                                                                        //Natural: MOVE 'MO' TO #MODE-CODE ( 15 )
        pnd_Mode_Code.getValue(16).setValue("MP");                                                                                                                        //Natural: MOVE 'MP' TO #MODE-CODE ( 16 )
        pnd_Mode_Code.getValue(17).setValue("MQ");                                                                                                                        //Natural: MOVE 'MQ' TO #MODE-CODE ( 17 )
        pnd_Mode_Code.getValue(18).setValue("MR");                                                                                                                        //Natural: MOVE 'MR' TO #MODE-CODE ( 18 )
        pnd_Mode_Code.getValue(19).setValue("MS");                                                                                                                        //Natural: MOVE 'MS' TO #MODE-CODE ( 19 )
        pnd_Mode_Code.getValue(20).setValue("MT");                                                                                                                        //Natural: MOVE 'MT' TO #MODE-CODE ( 20 )
        pnd_Mode_Code.getValue(21).setValue("MU");                                                                                                                        //Natural: MOVE 'MU' TO #MODE-CODE ( 21 )
        pnd_Mode_Code.getValue(22).setValue("MV");                                                                                                                        //Natural: MOVE 'MV' TO #MODE-CODE ( 22 )
        pnd_Mode_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #MODE-SUB
        pnd_Prod_Sub.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #PROD-SUB
        REPEAT11:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Mode_Sub.greater(22))) {break;}                                                                                                             //Natural: UNTIL #MODE-SUB > 22
            pnd_Search_Key_Rcrd_Pnd_Search_Cntrl_Cde.setValue(pnd_Mode_Code.getValue(pnd_Mode_Sub));                                                                      //Natural: MOVE #MODE-CODE ( #MODE-SUB ) TO #SEARCH-CNTRL-CDE
            pnd_Func.setValue("  ");                                                                                                                                      //Natural: MOVE '  ' TO #FUNC
            vw_iaa_Cntrl_Rcrd_1.startDatabaseFind                                                                                                                         //Natural: FIND IAA-CNTRL-RCRD-1 WITH CNTRL-RCRD-KEY = #SEARCH-KEY-RCRD
            (
            "FINDC",
            new Wc[] { new Wc("CNTRL_RCRD_KEY", "=", pnd_Search_Key_Rcrd, WcType.WITH) }
            );
            FINDC:
            while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("FINDC", true)))
            {
                vw_iaa_Cntrl_Rcrd_1.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Cntrl_Rcrd_1.getAstCOUNTER().equals(0)))                                                                                             //Natural: IF NO RECORDS FOUND
                {
                    pnd_Func.setValue("ST");                                                                                                                              //Natural: MOVE 'ST' TO #FUNC
                }                                                                                                                                                         //Natural: END-NOREC
                iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Cntrl_Check_Dte_Pnd_Cntrl_Check_Dte_A);                                //Natural: MOVE EDITED #CNTRL-CHECK-DTE-A TO CNTRL-CHECK-DTE ( EM = YYYYMMDD )
                iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte.setValue(pnd_Search_Trans_Dte_A);                                                                                       //Natural: MOVE #SEARCH-TRANS-DTE-A TO CNTRL-INVRSE-DTE
                iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte.setValue(Global.getDATX());                                                                                             //Natural: ASSIGN CNTRL-TODAYS-DTE = *DATX
                iaa_Cntrl_Rcrd_1_Cntrl_Cde.setValue(pnd_Mode_Code.getValue(pnd_Mode_Sub));                                                                                //Natural: MOVE #MODE-CODE ( #MODE-SUB ) TO CNTRL-CDE
                iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees.setValue(pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Tiaa_Payees.getValue(pnd_Mode_Sub));                                   //Natural: MOVE #STORE-CNTRL-TIAA-PAYEES ( #MODE-SUB ) TO CNTRL-TIAA-PAYEES
                iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt.setValue(pnd_Rept_Tot_Periodic_Payment.getValue(pnd_Mode_Sub));                                                        //Natural: MOVE #REPT-TOT-PERIODIC-PAYMENT ( #MODE-SUB ) TO CNTRL-PER-PAY-AMT
                iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt.setValue(pnd_Rept_Tot_Periodic_Dividend.getValue(pnd_Mode_Sub));                                                       //Natural: MOVE #REPT-TOT-PERIODIC-DIVIDEND ( #MODE-SUB ) TO CNTRL-PER-DIV-AMT
                //*  ADDED 3/01
                iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt.setValue(pnd_Rept_Tot_Fin_Periodic_Payment.getValue(pnd_Mode_Sub));                                                  //Natural: MOVE #REPT-TOT-FIN-PERIODIC-PAYMENT ( #MODE-SUB ) TO CNTRL-FINAL-PAY-AMT
                //*  ADDED 3/01
                //*  LB 10/97
                iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt.setValue(pnd_Rept_Tot_Fin_Periodic_Dividend.getValue(pnd_Mode_Sub));                                                 //Natural: MOVE #REPT-TOT-FIN-PERIODIC-DIVIDEND ( #MODE-SUB ) TO CNTRL-FINAL-DIV-AMT
                FOR11:                                                                                                                                                    //Natural: FOR #PROD-SUB = 1 TO 20
                for (pnd_Prod_Sub.setValue(1); condition(pnd_Prod_Sub.lessOrEqual(20)); pnd_Prod_Sub.nadd(1))
                {
                    //*  FILL ANNUAL BUCKETS
                    iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde.getValue(pnd_Prod_Sub).setValue(pnd_Prod_Code.getValue(pnd_Prod_Sub));                                                //Natural: MOVE #PROD-CODE ( #PROD-SUB ) TO CNTRL-FUND-CDE ( #PROD-SUB )
                    iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_Prod_Sub).setValue(pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,          //Natural: MOVE #STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB ) TO CNTRL-FUND-PAYEES ( #PROD-SUB )
                        pnd_Prod_Sub));
                    iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_Prod_Sub).setValue(pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,                //Natural: MOVE #REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB ) TO CNTRL-UNITS ( #PROD-SUB )
                        pnd_Prod_Sub));
                    //*  3/97
                    iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_Prod_Sub).setValue(pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Amts_Prod.getValue(pnd_Mode_Sub,                   //Natural: MOVE #REPT-TOT-MODE-AMTS-PROD ( #MODE-SUB, #PROD-SUB ) TO CNTRL-AMT ( #PROD-SUB )
                        pnd_Prod_Sub));
                    //*  FILL MONTHLY BUCKETS                              /* LB 10/97
                    pnd_Prod_Sub_Monthly.compute(new ComputeParameters(false, pnd_Prod_Sub_Monthly), pnd_Prod_Sub.add(20));                                               //Natural: ASSIGN #PROD-SUB-MONTHLY = #PROD-SUB + 20
                    iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde.getValue(pnd_Prod_Sub_Monthly).setValue(pnd_Prod_Code.getValue(pnd_Prod_Sub_Monthly));                                //Natural: MOVE #PROD-CODE ( #PROD-SUB-MONTHLY ) TO CNTRL-FUND-CDE ( #PROD-SUB-MONTHLY )
                    iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_Prod_Sub_Monthly).setValue(pnd_Store_Cntrl_Payees_Pnd_Store_Cntrl_Cref_Payees.getValue(pnd_Mode_Sub,  //Natural: MOVE #STORE-CNTRL-CREF-PAYEES ( #MODE-SUB, #PROD-SUB-MONTHLY ) TO CNTRL-FUND-PAYEES ( #PROD-SUB-MONTHLY )
                        pnd_Prod_Sub_Monthly));
                    iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_Prod_Sub_Monthly).setValue(pnd_Rept_Tots_By_Mode_Pnd_Rept_Tot_Mode_Units_Prod.getValue(pnd_Mode_Sub,        //Natural: MOVE #REPT-TOT-MODE-UNITS-PROD ( #MODE-SUB, #PROD-SUB-MONTHLY ) TO CNTRL-UNITS ( #PROD-SUB-MONTHLY )
                        pnd_Prod_Sub_Monthly));
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  STORE OR UPDATE THE CONTROL RECORD
                if (condition(pnd_Func.equals("ST")))                                                                                                                     //Natural: IF #FUNC = 'ST'
                {
                    vw_iaa_Cntrl_Rcrd_1.insertDBRow();                                                                                                                    //Natural: STORE IAA-CNTRL-RCRD-1
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    vw_iaa_Cntrl_Rcrd_1.updateDBRow("FINDC");                                                                                                             //Natural: UPDATE ( FINDC. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Prod_Sub.setValue(1);                                                                                                                                     //Natural: MOVE 1 TO #PROD-SUB
            pnd_Mode_Sub.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #MODE-SUB
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  WRITE-CONTROL-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"PROGRAM    :",Global.getPROGRAM(),new ColumnSpacing(22),"IA ADMINISTRATION MODE CONTROL RECORD CREATE    ",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X 'PROGRAM    :' *PROGRAM 22X 'IA ADMINISTRATION MODE CONTROL RECORD CREATE    ' 115T 'Page :' *PAGE-NUMBER ( 1 ) ( AD = L ) / 01X 'CHECK DATE :' CNTRL-CHECK-DTE ( EM = MM/DD/YYYY ) / 01X 'RUN DATE   :' *DATX ( EM = MM/DD/YYYY ) //
                        TabSetting(115),"Page :",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new ColumnSpacing(1),"CHECK DATE :",iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new ColumnSpacing(1),"RUN DATE   :",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
    }
}
