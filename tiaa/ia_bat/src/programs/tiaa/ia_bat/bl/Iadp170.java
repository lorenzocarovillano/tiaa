/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:14 PM
**        * FROM NATURAL PROGRAM : Iadp170
************************************************************
**        * FILE NAME            : Iadp170.java
**        * CLASS NAME           : Iadp170
**        * INSTANCE NAME        : Iadp170
************************************************************
***********************************************************************
* PROGRAM  : IADP170  COPY OF OLD IADP170
* SYSTEM   : IA
* TITLE    : PRODUCE TPA/IPRO & P/I FILE FOR QTRLY REPROTING
* CREATED  : JAN 19, 2000
* FUNCTION : PRODUCE TPA/IPRO & P/I FILE FROM IAIQ MASTER IN OLD
* TITLE    : IA FORMAT IN TO QCOT FOR REPORTING
*
*
*
* CHANGED HISTORY
*  8/00         LOGIC TO HANDLE PEND CODE Q
*  9/00         LOGIC TO HANDLE MOVING PEND CODE TO QTRLY B RECORD
*               & MOVE ISSUE DATE TO QTRLY B RECORD
* 10/00         LOGIC TO HANDLE PASSING TPA ALLOCATION BY FUND TO QTRLY
*
*  4/01         LOGIC TO HANDLE COMMUTED VALUE FOR RETRO NEW ISSUES
*               DO SCAN ON 4/01
*
*  5/01         LOGIC TO HANDLE PEND CODE RESET PEND CODE TO 0 FOR
*               CONTRACTS NOT PENDED
*               DO SCAN ON 5/01
*
*  6/01         LOGIC TO HANDLE TO READ TPA REINVEST FILE FOR NEW
*               DESTINATIONS
*               DO SCAN ON 6/01 & 7/01
* 11/01         LOGIC TO HANDLE TO READ ADAM FILE FOR NEW ISSUES
*
*               DO SCAN ON 11/01
*  1/02         LOGIC TO USE ADAM TOT-ACCUM FOR TPA OPENING VALUE
*               ADDED ISSUE-DATE DAY TO 10 RECORD FOR TPA OPEN-VALUE
*               FOR NEW-ISSUES CALL TO ACTUARY
*               DO SCAN ON  1/02
*
*  4/02         LOGIC TO CALL IADN171 TO GET DA AMOUNT FOR ANY
*               PREMIUM SWEEP (RESETTLE) IAIQ TRANS 60 TO PASS QTRLY
*               T1, OR I1 OR P1 TRANS TO REFLECT NEW MONEY INTO THE
*               CONTRACT
*               DO SCAN ON  4/02
*
*   DO SCAN ON 05/02 INCREASE FIELD SIZE FOR PER-PMT & PER-DIV IN    *
*                    OUTPUT LVL 50 RATE RECORD                       *
*                    DO SCAN ON 5/02                                 *
*                                                                    *
*   DO SCAN ON 07/02 INCREASE NUMBER OF RATES FROM 30 TO 60          *
*                    DO SCAN ON 7/02                                 *
*                                                                    *
*     06/03          CHANGES TO HANDLE ALPHA RATES & UPTO 90 RATES   *
*                    AND CHANGE TPA PEND-CODE 'S' & 'T' TO '0'       *
*                    NEW PEND FOR SELF-REMITTER CONTRACTS            *
*                    DO SCAN ON 6/03                                 *
*   DO SCAN ON 03/05 CHANGE TO MOVE CNTRCT-TYPE INTO OUTPUT MASTER   *
*                    FOR TPA CONVERSION TO DETERMINE FULL WITHDRAWAL *
*                    DO SCAN ON 3/05                                 *
*   DO SCAN ON 08/05 IDENTIFY MANUAL NEW ISSUES AND PASS A CALL TYPE *
*                    OF M TO PIA3170.                                *
*   DO SCAN ON 02/06 FIX FOR THE HANDLING OF TRANS 40 - TERMINATIONS *
*                    DUE TO TPA/IPRO CONVERSION TO OMNI.             *
*   DO SCAN ON 04/06 FIX FOR THE HANDLING OF TRANS 50 - PARTIAL      *
*                    DUE TO TPA/IPRO CONVERSION TO OMNI.             *
*  SEP 2010  BREMER  MARKED JB01                                     *
*                    PASS SURVIVORS TO QRS - '03' PAYEES AND >       *
*  MAY 2012  TINIO   RATE BASE EXPANSION - MARKED 052012             *
*  AUG 2015  TINIO   COR/NAAD SUNSET     - MARKED 082015             *
*  04/2917   OS      PIN EXPANSION CHANGES MARKED 082017.
*                    ADDITIONAL CHANGE REQUESTED BY GINA TO          *
*                    LEFT JUSTIFY PIN.                               *
*  03/2017   RC      BYPASS IF TRANS DATE = 0 SCAN ON 3/29/17
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp170 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Core;

    private DbsGroup pnd_Core__R_Field_1;
    private DbsField pnd_Core_Pnd_Cor_Cntrct_Tot;

    private DbsGroup pnd_Core__R_Field_2;
    private DbsField pnd_Core_Pnd_Cor_Cntrct;
    private DbsField pnd_Core_Pnd_Cor_Payee;
    private DbsField pnd_Core_Pnd_Cor_Pin;
    private DbsField pnd_Core_Pnd_Cor_Type_Cde;
    private DbsField pnd_Core_Pnd_Cor_Ssn;
    private DbsField pnd_Core_Pnd_I_Cor_Dob;
    private DbsField pnd_Core_Pnd_Cor_Dod;
    private DbsField pnd_Core_Pnd_Cor_Last_Nm;
    private DbsField pnd_Core_Pnd_Cor_First_Nm;
    private DbsField pnd_Core_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Core_Pnd_Cor_Status_Cde;
    private DbsField pnd_Core_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Cntrct_Payee;

    private DbsGroup pnd_Cntrct_Payee__R_Field_3;
    private DbsField pnd_Cntrct_Payee_Pnd_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Payee_Pnd_Payee_Cde;
    private DbsField pnd_Core_Eof;

    private DataAccessProgramView vw_ia_Tpa;
    private DbsField ia_Tpa_Ph_Unque_Id_Nbr;
    private DbsField ia_Tpa_Ia_Cntrct_Nbr;
    private DbsField ia_Tpa_Sttlmnt_Dte;

    private DbsGroup ia_Tpa__R_Field_4;
    private DbsField ia_Tpa_Sttlmnt_Dte_Cc;
    private DbsField ia_Tpa_Sttlmnt_Dte_Yy;
    private DbsField ia_Tpa_Sttlmnt_Dte_Mm;
    private DbsField ia_Tpa_Ph_Scl_Scrty_Nbr;
    private DbsField ia_Tpa_Trnsfr_Dstntn_Cde;
    private DbsField ia_Tpa_Payee_Cde;

    private DbsGroup ia_Tpa__R_Field_5;
    private DbsField ia_Tpa_Payee_Cde_Num;
    private DbsField ia_Tpa_Trmntn_Cde;

    private DbsGroup ia_Tpa__R_Field_6;
    private DbsField ia_Tpa_Trmntn_Cde_R;
    private DbsField ia_Tpa_Intl_Payout_Ttb_Amt;
    private DbsField ia_Tpa_Ttl_Trnsfr_Amt;
    private DbsField ia_Tpa_Prjctd_Ia_Payout_Amt;
    private DbsField ia_Tpa_Ra_Gra_Cntrct_Nbr;
    private DbsField ia_Tpa_Cref_Cntrct_Nbr;
    private DbsField ia_Tpa_Pay_Nopay_Cde;

    private DataAccessProgramView vw_ia_Tpa_Fund;
    private DbsField ia_Tpa_Fund_Ph_Unque_Id_Nbr;
    private DbsField ia_Tpa_Fund_Ia_Cntrct_Nbr;
    private DbsField ia_Tpa_Fund_Sttlmnt_Dte;
    private DbsField ia_Tpa_Fund_Cref_Fund_Cde;
    private DbsField ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct;
    private DbsField pnd_Sve_Dest;
    private DbsField pnd_Ia_Tpa_Key;

    private DbsGroup pnd_Ia_Tpa_Key__R_Field_7;
    private DbsField pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr;
    private DbsField pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr;
    private DbsField pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte;
    private DbsField pnd_Ia_Tpa_Fund_Key;

    private DbsGroup pnd_Ia_Tpa_Fund_Key__R_Field_8;
    private DbsField pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ph_Unque_Id_Nbr;
    private DbsField pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ia_Cntrct_Nbr;
    private DbsField pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Sttlmnt_Dte;
    private DbsField pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Fund_Code;
    private DbsField pnd_W_Tia_Sw;
    private DbsField pnd_W_Crf_Sw;
    private DbsField ia_Mast_Rec;

    private DbsGroup ia_Mast_Rec__R_Field_9;
    private DbsField ia_Mast_Rec_Ia_Mstr_Key;

    private DbsGroup ia_Mast_Rec__R_Field_10;
    private DbsField ia_Mast_Rec_Ia_Contract_No;

    private DbsGroup ia_Mast_Rec__R_Field_11;
    private DbsField ia_Mast_Rec_Ia_Prefix;
    private DbsField ia_Mast_Rec_Ia_Cont_No;
    private DbsField ia_Mast_Rec_Ia_Chk_Digit;
    private DbsField ia_Mast_Rec_Ia_Payee_No;

    private DbsGroup ia_Mast_Rec__R_Field_12;
    private DbsField ia_Mast_Rec_Ia_Payee_No_A;
    private DbsField ia_Mast_Rec_Ia_Rec_No;
    private DbsField ia_Mast_Rec_Ia_Sequence_No;
    private DbsField ia_Mast_Rec_Ia_Mstr_Rcrd;

    private DbsGroup ia_Mast_Rec__R_Field_13;
    private DbsField ia_Mast_Rec_Iss_Prod;
    private DbsField ia_Mast_Rec_Iss_Currency;
    private DbsField ia_Mast_Rec_Iss_Mode;

    private DbsGroup ia_Mast_Rec__R_Field_14;
    private DbsField ia_Mast_Rec_Iss_Mode_1;
    private DbsField ia_Mast_Rec_Iss_Mode_Mm;
    private DbsField ia_Mast_Rec_Iss_Pend_Pay;
    private DbsField ia_Mast_Rec_Iss_Hold_Chk;
    private DbsField ia_Mast_Rec_Iss_Pend_Date;
    private DbsField ia_Mast_Rec_Iss_Option;
    private DbsField ia_Mast_Rec_Iss_Origin;
    private DbsField ia_Mast_Rec_Iss_Iss_Date;
    private DbsField ia_Mast_Rec_Iss_1st_Pay_Due_Date;
    private DbsField ia_Mast_Rec_Iss_1st_Pay_Paid_Date;
    private DbsField ia_Mast_Rec_Iss_Final_Per_Pay_Date;
    private DbsField ia_Mast_Rec_Iss_Final_Pay_Date;
    private DbsField ia_Mast_Rec_Iss_Withdraw_Date;
    private DbsField ia_Mast_Rec_Iss_Citizenship;
    private DbsField ia_Mast_Rec_Iss_State_At_Issue_Time;
    private DbsField ia_Mast_Rec_Iss_Current_State;
    private DbsField ia_Mast_Rec_Iss_College_Code;
    private DbsField ia_Mast_Rec_Iss_Rewrite;
    private DbsField ia_Mast_Rec_Iss_Delete;

    private DbsGroup ia_Mast_Rec__R_Field_15;
    private DbsField ia_Mast_Rec_Iss_Delete_R;
    private DbsField ia_Mast_Rec_Iss_Rtb_Amt;
    private DbsField ia_Mast_Rec_Iss_Last_Trans_Date;
    private DbsField ia_Mast_Rec_Iss_Joint_Conv_Rec_Ind;
    private DbsField ia_Mast_Rec_Iss_Temp_Hold_Code;
    private DbsField ia_Mast_Rec_Iss_Spirt_Code;
    private DbsField ia_Mast_Rec_Iss_Pens_Plan_Cd;
    private DbsField ia_Mast_Rec_Iss_Contr_Type_Cd;
    private DbsField ia_Mast_Rec_Iss_New_Issue_Flag;
    private DbsField ia_Mast_Rec_Iss_Dte_Dd;
    private DbsField ia_Mast_Rec_Iss_Tran_Code;

    private DbsGroup ia_Mast_Rec__R_Field_16;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_X_Ref;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Sex;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Dob;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Dod;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Mort_Yob;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Life_Count;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Div_Payee;
    private DbsField ia_Mast_Rec_Ann_1st_Ann_Coll_Cd_Div;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_X_Ref;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_Sex;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_Dob;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_Dod;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_Mort_Yob;
    private DbsField ia_Mast_Rec_Ann_2nd_Ann_Life_Count;
    private DbsField ia_Mast_Rec_Ann_Benef_X_Ref_No;
    private DbsField ia_Mast_Rec_Ann_Benef_Dod;
    private DbsField ia_Mast_Rec_Ann_Da_Contr_Num;
    private DbsField ia_Mast_Rec_Ann_Cash_Code;
    private DbsField ia_Mast_Rec_Ann_Emply_Code;
    private DbsField ia_Mast_Rec_Ann_Prev_Ttb_Disb;
    private DbsField ia_Mast_Rec_Ann_Curr_Tpa_Disb;
    private DbsField ia_Mast_Rec_Filler_1;

    private DbsGroup ia_Mast_Rec__R_Field_17;
    private DbsField ia_Mast_Rec_Rate_Tot_Old_Tiaa_Div;
    private DbsField ia_Mast_Rec_Rate_Rate_1;
    private DbsField ia_Mast_Rec_Rate_Per_Pay_1;
    private DbsField ia_Mast_Rec_Rate_Per_Div_1;
    private DbsField ia_Mast_Rec_Rate_Final_Pay_1;
    private DbsField ia_Mast_Rec_Rate_Date_1;
    private DbsField ia_Mast_Rec_Rate_Rate_2;
    private DbsField ia_Mast_Rec_Rate_Per_Pay_2;
    private DbsField ia_Mast_Rec_Rate_Per_Div_2;
    private DbsField ia_Mast_Rec_Rate_Final_Pay_2;
    private DbsField ia_Mast_Rec_Rate_Date_2;
    private DbsField ia_Mast_Rec_Rate_Rate_3;
    private DbsField ia_Mast_Rec_Rate_Per_Pay_3;
    private DbsField ia_Mast_Rec_Rate_Per_Div_3;
    private DbsField ia_Mast_Rec_Rate_Final_Pay_3;
    private DbsField ia_Mast_Rec_Rate_Date_3;
    private DbsField ia_Mast_Rec_Rate_Trans_Code;
    private DbsField ia_Mast_Rec_Rate_Trans_Date;
    private DbsField ia_Mast_Rec_Filler_1a;
    private DbsField ia_Mast_Rec_Rate_Tot_Old_Tiaa_Pmt;

    private DbsGroup ia_Mast_Rec__R_Field_18;
    private DbsField ia_Mast_Rec_Us_Tax_Auth;
    private DbsField ia_Mast_Rec_Us_Active_Code;
    private DbsField ia_Mast_Rec_Us_Per_Tax_Ded;
    private DbsField ia_Mast_Rec_Us_Ytd_Tax_Ded;
    private DbsField ia_Mast_Rec_Us_Recov_Type;
    private DbsField ia_Mast_Rec_Us_Per_Tax_Free_Amt;
    private DbsField ia_Mast_Rec_Us_Res_Tax_Free_Amt;
    private DbsField ia_Mast_Rec_Us_Cont_To_Date_Inv_Used;
    private DbsField ia_Mast_Rec_Us_Invest_In_Contr;
    private DbsField ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax;
    private DbsField ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax_Free;
    private DbsField ia_Mast_Rec_Us_Ytd_Div_Pay;
    private DbsField ia_Mast_Rec_Us_Ytd_Int_Pay;
    private DbsField ia_Mast_Rec_Us_Ytd_Int_Div_Pay;
    private DbsField ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id;

    private DbsGroup ia_Mast_Rec__R_Field_19;
    private DbsField ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id_Num;
    private DbsField ia_Mast_Rec_Us_Start_Date;
    private DbsField ia_Mast_Rec_Us_Stop_Date;
    private DbsField ia_Mast_Rec_Us_Sprt_Lump_Sum;
    private DbsField ia_Mast_Rec_Us_Spirt_Arrival_Date;
    private DbsField ia_Mast_Rec_Us_Spirt_Arrival_Source;
    private DbsField ia_Mast_Rec_Us_Spirt_Process_Date;
    private DbsField ia_Mast_Rec_Us_Filler_1b;

    private DbsGroup ia_Mast_Rec__R_Field_20;
    private DbsField ia_Mast_Rec_High_Value;
    private DbsField ia_Mast_Rec_Tiaa_Active_Payee_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Periodic_Div_Mitr;
    private DbsField ia_Mast_Rec_Tiaa_Final_Pmt_Mitr;
    private DbsField ia_Mast_Rec_Tpa_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_Tpa_Per_Amt;
    private DbsField ia_Mast_Rec_Tpa_Div_Amt;
    private DbsField ia_Mast_Rec_Ipro_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_Ipro_Per_Amt;
    private DbsField ia_Mast_Rec_Ipro_Per_Div;
    private DbsField ia_Mast_Rec_Ipro_Fin_Pmt;
    private DbsField ia_Mast_Rec_P_I_Active_Payees_Mitr;
    private DbsField ia_Mast_Rec_P_I_Per_Amt;
    private DbsField ia_Mast_Rec_P_I_Per_Div;
    private DbsField ia_Mast_Rec_P_I_Fin_Pmt;
    private DbsField ia_Mast_Rec_Inactive_Payees_Mitr;
    private DbsField pnd_W_Hex_High_Value;
    private DbsField w_Tape_Out;
    private DbsField w_Tape_Out_B;

    private DbsGroup w_Tape_Out_B__R_Field_21;
    private DbsField w_Tape_Out_B_W_Ia_Record_Type_Cd_B;
    private DbsField w_Tape_Out_B_W_Ia_Filler;
    private DbsField w_Tape_Out_B_W_Ia_Contract_Number;
    private DbsField w_Tape_Out_B_W_Ia_Issu_Coll_Code;
    private DbsField w_Tape_Out_B_W_Ia_Pin;
    private DbsField w_Tape_Out_B_W_Ia_Pull_Out_Cd;
    private DbsField w_Tape_Out_B_W_Ia_Social_Security_No;
    private DbsField w_Tape_Out_B_W_Ia_Latest_Da_Tiaa;
    private DbsField w_Tape_Out_B_W_Ia_College_Owned_Code;
    private DbsField w_Tape_Out_B_W_Ia_Sex;
    private DbsField w_Tape_Out_B_W_Currency_Cd;
    private DbsField w_Tape_Out_B_W_Ia_Paymt_Destination;
    private DbsField w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments;
    private DbsField w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date;
    private DbsField w_Tape_Out_B_W_Ia_Next_Payment_Amount;
    private DbsField w_Tape_Out_B_W_Ia_Num_Of_Prod;
    private DbsField w_Tape_Out_B_W_Ia_Product_Type_Cd;
    private DbsField w_Tape_Out_B_W_Ia_Open_Value;
    private DbsField w_Tape_Out_B_W_Ia_Close_Value;
    private DbsField w_Tape_Out_B_W_Ia_Pend_Code;
    private DbsField w_Tape_Out_B_W_Ia_Status_Flag;
    private DbsField w_Tape_Out_B_W_Ia_Dod;

    private DbsGroup w_Tape_Out_B__R_Field_22;
    private DbsField w_Tape_Out_B_W_Ia_Dod_Cc;
    private DbsField w_Tape_Out_B_W_Ia_Dod_Yy;
    private DbsField w_Tape_Out_B_W_Ia_Dod_Mm;
    private DbsField w_Tape_Out_B_W_Ia_Dob;

    private DbsGroup w_Tape_Out_B__R_Field_23;
    private DbsField w_Tape_Out_B_W_Ia_Dob_Yyyy;
    private DbsField w_Tape_Out_B_W_Ia_Dob_Mm;
    private DbsField w_Tape_Out_B_W_Ia_Dob_Dd;
    private DbsField w_Tape_Out_B_W_Ia_Optn_Code;
    private DbsField w_Tape_Out_B_W_Ia_Mode;

    private DbsGroup w_Tape_Out_B__R_Field_24;
    private DbsField w_Tape_Out_B_W_Ia_Mode_1;
    private DbsField w_Tape_Out_B_W_Ia_Mode_2;
    private DbsField w_Tape_Out_B_W_Ia_Conversion_Date;
    private DbsField w_Tape_Out_B_W_Ia_Issue_Dte;
    private DbsField w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value;
    private DbsField w_Tape_Out_B_W_Ia_Final_Per_Pay_Date;

    private DbsGroup w_Tape_Out_B_W_Ia_Fund_Prcnts;
    private DbsField w_Tape_Out_B_W_Ia_Fund_Cde_B_Rec;
    private DbsField w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec;

    private DbsGroup w_Tape_Out_B_W_Ia_Rates;
    private DbsField w_Tape_Out_B_W_Ia_Rate_Cde;
    private DbsField w_Tape_Out_B_W_Ia_Rate_Per_Pay;
    private DbsField w_Tape_Out_B_W_Ia_Rate_Per_Div;
    private DbsField w_Tape_Out_B_W_Rate_Final_Pay;
    private DbsField w_Tape_Out_N;

    private DbsGroup w_Tape_Out_N__R_Field_25;
    private DbsField w_Tape_Out_N_W_Ia_Record_Type_Cd_N;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Trans_Code;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Contract_Number;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Part_Date;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Transaction_Date;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Product_Code;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Amount;
    private DbsField w_Tape_Out_N_W_Ia_Trn_Filler;

    private DbsGroup w_Tape_Out_N_W_Ia_Fund_Prcnts;
    private DbsField w_Tape_Out_N_W_Ia_Fund_Cde_N_Rec;
    private DbsField w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec;
    private DbsField pnd_W_Total_Record;

    private DbsGroup pnd_W_Total_Record__R_Field_26;
    private DbsField pnd_W_Total_Record_Pnd_W_Total_Record_Parm;

    private DbsGroup pnd_W_Total_Record__R_Field_27;
    private DbsField pnd_W_Total_Record_W_Ia_Record_Type_Cd_X;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Written_B;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Written_N;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount;
    private DbsField pnd_W_Total_Record_W_Ia_Tpa_Pullouts;
    private DbsField pnd_W_Total_Record_W_Ia_Ipro_Pullouts;
    private DbsField pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts;
    private DbsField pnd_Ccyymmdd;

    private DbsGroup pnd_Ccyymmdd__R_Field_28;
    private DbsField pnd_Ccyymmdd_Pnd_Current_Ccyy;

    private DbsGroup pnd_Ccyymmdd__R_Field_29;
    private DbsField pnd_Ccyymmdd_Pnd_Current_Cc;
    private DbsField pnd_Ccyymmdd_Pnd_Current_Yy;
    private DbsField pnd_Ccyymmdd_Pnd_Current_Mm;
    private DbsField pnd_Ccyymmdd_Pnd_Current_Dd;
    private DbsField pnd_Next_Ccyymmdd;

    private DbsGroup pnd_Next_Ccyymmdd__R_Field_30;
    private DbsField pnd_Next_Ccyymmdd_Pnd_Next_Ccyy;

    private DbsGroup pnd_Next_Ccyymmdd__R_Field_31;
    private DbsField pnd_Next_Ccyymmdd_Pnd_Next_Cc;
    private DbsField pnd_Next_Ccyymmdd_Pnd_Next_Yy;
    private DbsField pnd_Next_Ccyymmdd_Pnd_Next_Mm;
    private DbsField pnd_Next_Ccyymmdd_Pnd_Next_Dd;

    private DbsGroup pnd_Work1;
    private DbsField pnd_Work1_Pnd_W_Check_Date;

    private DbsGroup pnd_Work1__R_Field_32;
    private DbsField pnd_Work1_Pnd_W_Check_Ccyy;
    private DbsField pnd_Work1_Pnd_W_Check_Mm;

    private DbsGroup pnd_Work1__R_Field_33;
    private DbsField pnd_Work1_Pnd_W_Check_Ccyymm;
    private DbsField pnd_W_Trans_Date;

    private DbsGroup pnd_W_Trans_Date__R_Field_34;
    private DbsField pnd_W_Trans_Date_Pnd_W_Trans_Ccyy;
    private DbsField pnd_W_Trans_Date_Pnd_W_Trans_Mm;
    private DbsField pnd_W_Trans_Date_Pnd_W_Trans_Dd;

    private DbsGroup pnd_W_Trans_Date__R_Field_35;
    private DbsField pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm;
    private DbsField pnd_W_Trans_Date_Pnd_W_Fill_Dd;

    private DbsGroup pnd_W_Trans_Date__R_Field_36;
    private DbsField pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm_A;
    private DbsField pnd_W_Trans_Date_Pnd_W_Fill_Dd_A;
    private DbsField pnd_W_Tot_Fin_Pmt_Amt;
    private DbsField pnd_Sve_W_Tot_Fin_Pmt_Amt;
    private DbsField pnd_W_Comm_Date;

    private DbsGroup pnd_W_Comm_Date__R_Field_37;
    private DbsField pnd_W_Comm_Date_Pnd_W_Comm_Ccyy;
    private DbsField pnd_W_Comm_Date_Pnd_W_Comm_Mm;
    private DbsField pnd_W_Comm_Date_Pnd_W_Comm_Dd;
    private DbsField pnd_W_Iss_Final_Per_Pay_Date;

    private DbsGroup pnd_W_Iss_Final_Per_Pay_Date__R_Field_38;
    private DbsField pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccyy;
    private DbsField pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccmm;
    private DbsField pnd_W_Hold_Date;

    private DbsGroup pnd_W_Hold_Date__R_Field_39;
    private DbsField pnd_W_Hold_Date_Pnd_W_Hold_Ccyy;
    private DbsField pnd_W_Hold_Date_Pnd_W_Hold_Mm;
    private DbsField pnd_W_Hold_Date_Pnd_W_Hold_Dd;
    private DbsField pnd_W_Iss_1st_Pay_Paid_Date;

    private DbsGroup pnd_W_Iss_1st_Pay_Paid_Date__R_Field_40;
    private DbsField pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Ccyy;
    private DbsField pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Mm;
    private DbsField pnd_W_Iss_Iss_Date;

    private DbsGroup pnd_W_Iss_Iss_Date__R_Field_41;
    private DbsField pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy;
    private DbsField pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm;
    private DbsField pnd_W_Iss_Dte_Dd;
    private DbsField pnd_W_Tax_Stop_Date;

    private DbsGroup pnd_W_Tax_Stop_Date__R_Field_42;
    private DbsField pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Ccyy;
    private DbsField pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm;
    private DbsField pnd_Sve_Tpa_Due_Date;
    private DbsField pnd_W_Iss_1st_Pay_Due_Date;

    private DbsGroup pnd_W_Iss_1st_Pay_Due_Date__R_Field_43;
    private DbsField pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Ccyy;
    private DbsField pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Mm;
    private DbsField pnd_Sub1;
    private DbsField pnd_W_Ia_Ipro_Open_Value_Less_Intr;
    private DbsField pnd_W_Ia_Ipro_Close_Value_Less_Intr;
    private DbsField pnd_W_Total_Payment_Amount;
    private DbsField pnd_Sve_W_Total_Payment_Amount;
    private DbsField pnd_W_Total_Tpa_Payment_Amount;
    private DbsField pnd_W_Total_Guarpymt_Amount;
    private DbsField pnd_W_Total_Guardivd_Amount;
    private DbsField pnd_W_Total_Guardivd_Prev;
    private DbsField pnd_W_Total_Transfer_Amount;
    private DbsField pnd_W_Ia_Pin;

    private DbsGroup pnd_W_Ia_Pin__R_Field_44;
    private DbsField pnd_W_Ia_Pin_Pnd_W_Ia_Pin_5;
    private DbsField pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7;
    private DbsField pnd_W_Next_Due_Dte_Ccyymmdd;

    private DbsGroup pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45;
    private DbsField pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm;

    private DbsGroup pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_46;
    private DbsField pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyy;
    private DbsField pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Mm;
    private DbsField pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Dd;
    private DbsField pnd_W_Hold_Date_Ccyymmdd;

    private DbsGroup pnd_W_Hold_Date_Ccyymmdd__R_Field_47;
    private DbsField pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm;
    private DbsField pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd;

    private DbsGroup pnd_W_Hold_Date_Ccyymmdd__R_Field_48;
    private DbsField pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy;
    private DbsField pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm;
    private DbsField pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd2;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_49;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Mm;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Dd;
    private DbsField pnd_W_Date_Pnd_W_Wrk_Ccyy;
    private DbsField pnd_Pia3170_Call_Type_G;
    private DbsField pnd_Pia3170_Call_Type;
    private DbsField pnd_Pia3170_Comm_Date;

    private DbsGroup pnd_Pia3170_Comm_Date__R_Field_50;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Cc;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Yy;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Mm;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd;

    private DbsGroup pnd_Pia3170_Comm_Date__R_Field_51;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm;
    private DbsField pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd2;
    private DbsField pnd_Pia3170_Cycle_Date;

    private DbsGroup pnd_Pia3170_Cycle_Date__R_Field_52;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Cc;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Yy;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Mm;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd;

    private DbsGroup pnd_Pia3170_Cycle_Date__R_Field_53;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Ccyymm;
    private DbsField pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd2;
    private DbsField pnd_Pia3170_Comm_Value;

    private DbsGroup pnd_Pia3170_Comm_Value__R_Field_54;
    private DbsField pnd_Pia3170_Comm_Value_Pnd_Pia3170_Comm_Value_A;
    private DbsField pnd_Pia3170_Error_01a;
    private DbsField pnd_Dummy1;
    private DbsField pnd_Dummy21;

    private DbsGroup pnd_Master_Rec_Linkage;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec1;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec2;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec3;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec4;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec5;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Rec_All;
    private DbsField pnd_Master_Rec_Linkage_Pnd_Filler_2;

    private DbsGroup pnd_Naz_Parm;
    private DbsField pnd_Naz_Parm_Pnd_I_Unique_Id_Naz;
    private DbsField pnd_Naz_Parm_Pnd_I_Da_Contract_Naz;
    private DbsField pnd_Naz_Parm_Pnd_I_Ia_Contract_Naz;
    private DbsField pnd_Naz_Parm_Pnd_I_Ia_Payee_Cde;
    private DbsField pnd_Naz_Parm_Pnd_I_Ia_Amount_Naz;
    private DbsField pnd_Naz_Parm_Pnd_I_Read_Type;
    private DbsField pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz;
    private DbsField pnd_Naz_Parm_Pnd_O_Total_Accum_Amt;
    private DbsField pnd_Naz_Parm_Pnd_O_Pmt_Amt;
    private DbsField pnd_Naz_Parm_Pnd_O_Found_Naz;
    private DbsField pnd_Naz_Parm_Pnd_O_25_Over_Sw_Naz;

    private DbsGroup pnd_Ia_Trn_Call_Parms;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_8_Np;

    private DbsGroup pnd_Ia_Trn_Call_Parms__R_Field_55;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Dd_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Da_Contract_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Contract_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Amount_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np;
    private DbsField pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np;
    private DbsField pnd_Hve_60_Trans;
    private DbsField pnd_Sve_Tran_Code;
    private DbsField pnd_Sve_Ia_Contract;
    private DbsField pnd_Sve_Ia_Contract_Payee;
    private DbsField pnd_Sve_Origin;
    private DbsField pnd_Sve_Optn_Code;
    private DbsField pnd_Sve_Iss_Delete_R;
    private DbsField pnd_Sve_Iss_Pend_Pay;
    private DbsField pnd_Sve_Iss_Last_Trans_Date;
    private DbsField pnd_Sve_Chk_Date;
    private DbsField pnd_Sve_Issu_Dte_Dd;
    private DbsField pnd_Sve_Prev_Chk_Date;

    private DbsGroup pnd_Sve_Prev_Chk_Date__R_Field_56;
    private DbsField pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyy;
    private DbsField pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm;

    private DbsGroup pnd_Sve_Prev_Chk_Date__R_Field_57;
    private DbsField pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyymm;
    private DbsField pnd_Sve_Open_Value;
    private DbsField pnd_W_Conv_Trans_Date;

    private DbsGroup pnd_W_Conv_Trans_Date__R_Field_58;
    private DbsField pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy;
    private DbsField pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm;
    private DbsField pnd_Sve_Rec5;
    private DbsField pnd_Sve_Mode;
    private DbsField pnd_W_Bypass;
    private DbsField pnd_W_Write_Qtrly_Sw;
    private DbsField pnd_W_Rewrite_Sw;
    private DbsField pnd_W_Pmt_Due_Sw;
    private DbsField pnd_W_New_Issue;
    private DbsField pnd_W_Manual_Issue;
    private DbsField pnd_W_Withdrawal;
    private DbsField pnd_W_Fin_Pay_Sw;
    private DbsField pnd_W_Death_Sw;
    private DbsField pnd_W_Retro_Sw;
    private DbsField pnd_W_Est;
    private DbsField pnd_W_Payee;
    private DbsField pnd_W_Iss_Mode_Mm;
    private DbsField pnd_W_Close_Val_Sw;
    private DbsField pnd_Ia_Contract9;

    private DbsGroup pnd_Summary_Report_Counters;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issue;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Family_Income_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div;
    private DbsField pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly;
    private DbsField pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt;
    private DbsField pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt;
    private DbsField pnd_Cnt_Rec;
    private DbsField pnd_Frst_Time_Sw;
    private DbsField pnd_Fnd_Tpa_For_B;
    private DbsField pnd_Tot_Amt;
    private DbsField pnd_I;
    private DbsField pnd_Rtex;
    private DbsField pnd_Tot_New_Not_Sel;
    private DbsField pnd_Tot_Ben_Not_Sel;
    private DbsField pnd_Call_Hist_Sw;
    private DbsField pnd_W_Mode;

    private DbsGroup pnd_W_Mode__R_Field_59;
    private DbsField pnd_W_Mode__Filler1;
    private DbsField pnd_W_Mode_Pnd_W_Mode_2;
    private DbsField pnd_Cntrct_Ppcn_Nbr_Parm;
    private DbsField pnd_Cntrct_Payee_Cde_Parm;
    private DbsField pnd_Optn_Cde;
    private DbsField pnd_Issue_Dte;

    private DbsGroup pnd_Issue_Dte__R_Field_60;
    private DbsField pnd_Issue_Dte__Filler2;
    private DbsField pnd_Issue_Dte_Pnd_Issue_Mm;
    private DbsField pnd_Pymnt_Amt;
    private DbsField pnd_Sve_Contr_Type_Cd;
    private int pia3170ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Core = localVariables.newFieldInRecord("pnd_Core", "#CORE", FieldType.STRING, 150);

        pnd_Core__R_Field_1 = localVariables.newGroupInRecord("pnd_Core__R_Field_1", "REDEFINE", pnd_Core);
        pnd_Core_Pnd_Cor_Cntrct_Tot = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Cntrct_Tot", "#COR-CNTRCT-TOT", FieldType.STRING, 12);

        pnd_Core__R_Field_2 = pnd_Core__R_Field_1.newGroupInGroup("pnd_Core__R_Field_2", "REDEFINE", pnd_Core_Pnd_Cor_Cntrct_Tot);
        pnd_Core_Pnd_Cor_Cntrct = pnd_Core__R_Field_2.newFieldInGroup("pnd_Core_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Core_Pnd_Cor_Payee = pnd_Core__R_Field_2.newFieldInGroup("pnd_Core_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Core_Pnd_Cor_Pin = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);
        pnd_Core_Pnd_Cor_Type_Cde = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Core_Pnd_Cor_Ssn = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Core_Pnd_I_Cor_Dob = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_I_Cor_Dob", "#I-COR-DOB", FieldType.NUMERIC, 8);
        pnd_Core_Pnd_Cor_Dod = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Core_Pnd_Cor_Last_Nm = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Core_Pnd_Cor_First_Nm = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Core_Pnd_Cor_Middle_Nm = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 30);
        pnd_Core_Pnd_Cor_Status_Cde = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 1);
        pnd_Core_Pnd_Cor_Sex_Cde = pnd_Core__R_Field_1.newFieldInGroup("pnd_Core_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Cntrct_Payee__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Payee__R_Field_3", "REDEFINE", pnd_Cntrct_Payee);
        pnd_Cntrct_Payee_Pnd_Cntrct_Nbr = pnd_Cntrct_Payee__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Pnd_Payee_Cde = pnd_Cntrct_Payee__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Core_Eof = localVariables.newFieldInRecord("pnd_Core_Eof", "#CORE-EOF", FieldType.BOOLEAN, 1);

        vw_ia_Tpa = new DataAccessProgramView(new NameInfo("vw_ia_Tpa", "IA-TPA"), "IA_TRANSFER_PAYOUT", "IA_TRANS_PAYOUT");
        ia_Tpa_Ph_Unque_Id_Nbr = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Ph_Unque_Id_Nbr", "PH-UNQUE-ID-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PH_UNQUE_ID_NBR");
        ia_Tpa_Ph_Unque_Id_Nbr.setDdmHeader("PIN");
        ia_Tpa_Ia_Cntrct_Nbr = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Ia_Cntrct_Nbr", "IA-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_CNTRCT_NBR");
        ia_Tpa_Ia_Cntrct_Nbr.setDdmHeader("IA/CONTRACT");
        ia_Tpa_Sttlmnt_Dte = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Sttlmnt_Dte", "STTLMNT-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "STTLMNT_DTE");
        ia_Tpa_Sttlmnt_Dte.setDdmHeader("SETTLE DATE");

        ia_Tpa__R_Field_4 = vw_ia_Tpa.getRecord().newGroupInGroup("ia_Tpa__R_Field_4", "REDEFINE", ia_Tpa_Sttlmnt_Dte);
        ia_Tpa_Sttlmnt_Dte_Cc = ia_Tpa__R_Field_4.newFieldInGroup("ia_Tpa_Sttlmnt_Dte_Cc", "STTLMNT-DTE-CC", FieldType.NUMERIC, 2);
        ia_Tpa_Sttlmnt_Dte_Yy = ia_Tpa__R_Field_4.newFieldInGroup("ia_Tpa_Sttlmnt_Dte_Yy", "STTLMNT-DTE-YY", FieldType.NUMERIC, 2);
        ia_Tpa_Sttlmnt_Dte_Mm = ia_Tpa__R_Field_4.newFieldInGroup("ia_Tpa_Sttlmnt_Dte_Mm", "STTLMNT-DTE-MM", FieldType.NUMERIC, 2);
        ia_Tpa_Ph_Scl_Scrty_Nbr = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Ph_Scl_Scrty_Nbr", "PH-SCL-SCRTY-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PH_SCL_SCRTY_NBR");
        ia_Tpa_Ph_Scl_Scrty_Nbr.setDdmHeader("SSN");
        ia_Tpa_Trnsfr_Dstntn_Cde = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Trnsfr_Dstntn_Cde", "TRNSFR-DSTNTN-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TRNSFR_DSTNTN_CDE");
        ia_Tpa_Trnsfr_Dstntn_Cde.setDdmHeader("DEST CDE");
        ia_Tpa_Payee_Cde = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAYEE_CDE");

        ia_Tpa__R_Field_5 = vw_ia_Tpa.getRecord().newGroupInGroup("ia_Tpa__R_Field_5", "REDEFINE", ia_Tpa_Payee_Cde);
        ia_Tpa_Payee_Cde_Num = ia_Tpa__R_Field_5.newFieldInGroup("ia_Tpa_Payee_Cde_Num", "PAYEE-CDE-NUM", FieldType.NUMERIC, 2);
        ia_Tpa_Trmntn_Cde = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Trmntn_Cde", "TRMNTN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRMNTN_CDE");

        ia_Tpa__R_Field_6 = vw_ia_Tpa.getRecord().newGroupInGroup("ia_Tpa__R_Field_6", "REDEFINE", ia_Tpa_Trmntn_Cde);
        ia_Tpa_Trmntn_Cde_R = ia_Tpa__R_Field_6.newFieldInGroup("ia_Tpa_Trmntn_Cde_R", "TRMNTN-CDE-R", FieldType.NUMERIC, 1);
        ia_Tpa_Intl_Payout_Ttb_Amt = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Intl_Payout_Ttb_Amt", "INTL-PAYOUT-TTB-AMT", FieldType.NUMERIC, 11, 
            2, RepeatingFieldStrategy.None, "INTL_PAYOUT_TTB_AMT");
        ia_Tpa_Intl_Payout_Ttb_Amt.setDdmHeader("TTB AMT");
        ia_Tpa_Ttl_Trnsfr_Amt = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Ttl_Trnsfr_Amt", "TTL-TRNSFR-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "TTL_TRNSFR_AMT");
        ia_Tpa_Ttl_Trnsfr_Amt.setDdmHeader("TOT AMT");
        ia_Tpa_Prjctd_Ia_Payout_Amt = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Prjctd_Ia_Payout_Amt", "PRJCTD-IA-PAYOUT-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "PRJCTD_IA_PAYOUT_AMT");
        ia_Tpa_Prjctd_Ia_Payout_Amt.setDdmHeader("PROJ IA AMT");
        ia_Tpa_Ra_Gra_Cntrct_Nbr = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Ra_Gra_Cntrct_Nbr", "RA-GRA-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RA_GRA_CNTRCT_NBR");
        ia_Tpa_Ra_Gra_Cntrct_Nbr.setDdmHeader("RA/GRA/CONTRACT");
        ia_Tpa_Cref_Cntrct_Nbr = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Cref_Cntrct_Nbr", "CREF-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_NBR");
        ia_Tpa_Cref_Cntrct_Nbr.setDdmHeader("CREF/CONTRACT/NUMBER");
        ia_Tpa_Pay_Nopay_Cde = vw_ia_Tpa.getRecord().newFieldInGroup("ia_Tpa_Pay_Nopay_Cde", "PAY-NOPAY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PAY_NOPAY_CDE");
        registerRecord(vw_ia_Tpa);

        vw_ia_Tpa_Fund = new DataAccessProgramView(new NameInfo("vw_ia_Tpa_Fund", "IA-TPA-FUND"), "IA_TRANSFER_PAYOUT_FUND", "IA_TRANS_PAY_FND");
        ia_Tpa_Fund_Ph_Unque_Id_Nbr = vw_ia_Tpa_Fund.getRecord().newFieldInGroup("ia_Tpa_Fund_Ph_Unque_Id_Nbr", "PH-UNQUE-ID-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PH_UNQUE_ID_NBR");
        ia_Tpa_Fund_Ph_Unque_Id_Nbr.setDdmHeader("PIN");
        ia_Tpa_Fund_Ia_Cntrct_Nbr = vw_ia_Tpa_Fund.getRecord().newFieldInGroup("ia_Tpa_Fund_Ia_Cntrct_Nbr", "IA-CNTRCT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_CNTRCT_NBR");
        ia_Tpa_Fund_Ia_Cntrct_Nbr.setDdmHeader("CONTRACT");
        ia_Tpa_Fund_Sttlmnt_Dte = vw_ia_Tpa_Fund.getRecord().newFieldInGroup("ia_Tpa_Fund_Sttlmnt_Dte", "STTLMNT-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "STTLMNT_DTE");
        ia_Tpa_Fund_Sttlmnt_Dte.setDdmHeader("SETTLE DATE");
        ia_Tpa_Fund_Cref_Fund_Cde = vw_ia_Tpa_Fund.getRecord().newFieldInGroup("ia_Tpa_Fund_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CREF_FUND_CDE");
        ia_Tpa_Fund_Cref_Fund_Cde.setDdmHeader("CREF FUND");
        ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct = vw_ia_Tpa_Fund.getRecord().newFieldInGroup("ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct", "CREF-FUND-TRNSFR-PCT", FieldType.NUMERIC, 
            5, 1, RepeatingFieldStrategy.None, "CREF_FUND_TRNSFR_PCT");
        ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct.setDdmHeader("FUND PCT");
        registerRecord(vw_ia_Tpa_Fund);

        pnd_Sve_Dest = localVariables.newFieldInRecord("pnd_Sve_Dest", "#SVE-DEST", FieldType.STRING, 4);
        pnd_Ia_Tpa_Key = localVariables.newFieldInRecord("pnd_Ia_Tpa_Key", "#IA-TPA-KEY", FieldType.STRING, 21);

        pnd_Ia_Tpa_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Ia_Tpa_Key__R_Field_7", "REDEFINE", pnd_Ia_Tpa_Key);
        pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr = pnd_Ia_Tpa_Key__R_Field_7.newFieldInGroup("pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr", "#TPA-PH-UNQUE-ID-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr = pnd_Ia_Tpa_Key__R_Field_7.newFieldInGroup("pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr", "#TPA-IA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte = pnd_Ia_Tpa_Key__R_Field_7.newFieldInGroup("pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte", "#TPA-STTLMNT-DTE", FieldType.NUMERIC, 
            6);
        pnd_Ia_Tpa_Fund_Key = localVariables.newFieldInRecord("pnd_Ia_Tpa_Fund_Key", "#IA-TPA-FUND-KEY", FieldType.STRING, 24);

        pnd_Ia_Tpa_Fund_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Ia_Tpa_Fund_Key__R_Field_8", "REDEFINE", pnd_Ia_Tpa_Fund_Key);
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ph_Unque_Id_Nbr = pnd_Ia_Tpa_Fund_Key__R_Field_8.newFieldInGroup("pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ph_Unque_Id_Nbr", 
            "#TPA-FND-PH-UNQUE-ID-NBR", FieldType.NUMERIC, 7);
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ia_Cntrct_Nbr = pnd_Ia_Tpa_Fund_Key__R_Field_8.newFieldInGroup("pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Ia_Cntrct_Nbr", 
            "#TPA-FND-IA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Sttlmnt_Dte = pnd_Ia_Tpa_Fund_Key__R_Field_8.newFieldInGroup("pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Sttlmnt_Dte", "#TPA-FND-STTLMNT-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Fund_Code = pnd_Ia_Tpa_Fund_Key__R_Field_8.newFieldInGroup("pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Fund_Code", "#TPA-FND-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_W_Tia_Sw = localVariables.newFieldInRecord("pnd_W_Tia_Sw", "#W-TIA-SW", FieldType.STRING, 1);
        pnd_W_Crf_Sw = localVariables.newFieldInRecord("pnd_W_Crf_Sw", "#W-CRF-SW", FieldType.STRING, 1);
        ia_Mast_Rec = localVariables.newFieldInRecord("ia_Mast_Rec", "IA-MAST-REC", FieldType.STRING, 100);

        ia_Mast_Rec__R_Field_9 = localVariables.newGroupInRecord("ia_Mast_Rec__R_Field_9", "REDEFINE", ia_Mast_Rec);
        ia_Mast_Rec_Ia_Mstr_Key = ia_Mast_Rec__R_Field_9.newFieldInGroup("ia_Mast_Rec_Ia_Mstr_Key", "IA-MSTR-KEY", FieldType.STRING, 15);

        ia_Mast_Rec__R_Field_10 = ia_Mast_Rec__R_Field_9.newGroupInGroup("ia_Mast_Rec__R_Field_10", "REDEFINE", ia_Mast_Rec_Ia_Mstr_Key);
        ia_Mast_Rec_Ia_Contract_No = ia_Mast_Rec__R_Field_10.newFieldInGroup("ia_Mast_Rec_Ia_Contract_No", "IA-CONTRACT-NO", FieldType.STRING, 8);

        ia_Mast_Rec__R_Field_11 = ia_Mast_Rec__R_Field_10.newGroupInGroup("ia_Mast_Rec__R_Field_11", "REDEFINE", ia_Mast_Rec_Ia_Contract_No);
        ia_Mast_Rec_Ia_Prefix = ia_Mast_Rec__R_Field_11.newFieldInGroup("ia_Mast_Rec_Ia_Prefix", "IA-PREFIX", FieldType.STRING, 2);
        ia_Mast_Rec_Ia_Cont_No = ia_Mast_Rec__R_Field_11.newFieldInGroup("ia_Mast_Rec_Ia_Cont_No", "IA-CONT-NO", FieldType.STRING, 5);
        ia_Mast_Rec_Ia_Chk_Digit = ia_Mast_Rec__R_Field_11.newFieldInGroup("ia_Mast_Rec_Ia_Chk_Digit", "IA-CHK-DIGIT", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Ia_Payee_No = ia_Mast_Rec__R_Field_10.newFieldInGroup("ia_Mast_Rec_Ia_Payee_No", "IA-PAYEE-NO", FieldType.NUMERIC, 2);

        ia_Mast_Rec__R_Field_12 = ia_Mast_Rec__R_Field_10.newGroupInGroup("ia_Mast_Rec__R_Field_12", "REDEFINE", ia_Mast_Rec_Ia_Payee_No);
        ia_Mast_Rec_Ia_Payee_No_A = ia_Mast_Rec__R_Field_12.newFieldInGroup("ia_Mast_Rec_Ia_Payee_No_A", "IA-PAYEE-NO-A", FieldType.STRING, 2);
        ia_Mast_Rec_Ia_Rec_No = ia_Mast_Rec__R_Field_10.newFieldInGroup("ia_Mast_Rec_Ia_Rec_No", "IA-REC-NO", FieldType.NUMERIC, 2);
        ia_Mast_Rec_Ia_Sequence_No = ia_Mast_Rec__R_Field_10.newFieldInGroup("ia_Mast_Rec_Ia_Sequence_No", "IA-SEQUENCE-NO", FieldType.NUMERIC, 3);
        ia_Mast_Rec_Ia_Mstr_Rcrd = ia_Mast_Rec__R_Field_9.newFieldInGroup("ia_Mast_Rec_Ia_Mstr_Rcrd", "IA-MSTR-RCRD", FieldType.STRING, 85);

        ia_Mast_Rec__R_Field_13 = ia_Mast_Rec__R_Field_9.newGroupInGroup("ia_Mast_Rec__R_Field_13", "REDEFINE", ia_Mast_Rec_Ia_Mstr_Rcrd);
        ia_Mast_Rec_Iss_Prod = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Prod", "ISS-PROD", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Currency = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Currency", "ISS-CURRENCY", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Iss_Mode = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Mode", "ISS-MODE", FieldType.NUMERIC, 3);

        ia_Mast_Rec__R_Field_14 = ia_Mast_Rec__R_Field_13.newGroupInGroup("ia_Mast_Rec__R_Field_14", "REDEFINE", ia_Mast_Rec_Iss_Mode);
        ia_Mast_Rec_Iss_Mode_1 = ia_Mast_Rec__R_Field_14.newFieldInGroup("ia_Mast_Rec_Iss_Mode_1", "ISS-MODE-1", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Iss_Mode_Mm = ia_Mast_Rec__R_Field_14.newFieldInGroup("ia_Mast_Rec_Iss_Mode_Mm", "ISS-MODE-MM", FieldType.NUMERIC, 2);
        ia_Mast_Rec_Iss_Pend_Pay = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Pend_Pay", "ISS-PEND-PAY", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Hold_Chk = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Hold_Chk", "ISS-HOLD-CHK", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Pend_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Pend_Date", "ISS-PEND-DATE", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Iss_Option = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Option", "ISS-OPTION", FieldType.NUMERIC, 2);
        ia_Mast_Rec_Iss_Origin = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Origin", "ISS-ORIGIN", FieldType.NUMERIC, 2);
        ia_Mast_Rec_Iss_Iss_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Iss_Date", "ISS-ISS-DATE", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Iss_1st_Pay_Due_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_1st_Pay_Due_Date", "ISS-1ST-PAY-DUE-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Iss_1st_Pay_Paid_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_1st_Pay_Paid_Date", "ISS-1ST-PAY-PAID-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Iss_Final_Per_Pay_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Final_Per_Pay_Date", "ISS-FINAL-PER-PAY-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Iss_Final_Pay_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Final_Pay_Date", "ISS-FINAL-PAY-DATE", FieldType.PACKED_DECIMAL, 
            8);
        ia_Mast_Rec_Iss_Withdraw_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Withdraw_Date", "ISS-WITHDRAW-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Iss_Citizenship = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Citizenship", "ISS-CITIZENSHIP", FieldType.NUMERIC, 3);
        ia_Mast_Rec_Iss_State_At_Issue_Time = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_State_At_Issue_Time", "ISS-STATE-AT-ISSUE-TIME", 
            FieldType.STRING, 3);
        ia_Mast_Rec_Iss_Current_State = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Current_State", "ISS-CURRENT-STATE", FieldType.STRING, 
            3);
        ia_Mast_Rec_Iss_College_Code = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_College_Code", "ISS-COLLEGE-CODE", FieldType.STRING, 5);
        ia_Mast_Rec_Iss_Rewrite = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Rewrite", "ISS-REWRITE", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Iss_Delete = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Delete", "ISS-DELETE", FieldType.NUMERIC, 1);

        ia_Mast_Rec__R_Field_15 = ia_Mast_Rec__R_Field_13.newGroupInGroup("ia_Mast_Rec__R_Field_15", "REDEFINE", ia_Mast_Rec_Iss_Delete);
        ia_Mast_Rec_Iss_Delete_R = ia_Mast_Rec__R_Field_15.newFieldInGroup("ia_Mast_Rec_Iss_Delete_R", "ISS-DELETE-R", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Rtb_Amt = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Rtb_Amt", "ISS-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        ia_Mast_Rec_Iss_Last_Trans_Date = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Last_Trans_Date", "ISS-LAST-TRANS-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Iss_Joint_Conv_Rec_Ind = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Joint_Conv_Rec_Ind", "ISS-JOINT-CONV-REC-IND", FieldType.STRING, 
            1);
        ia_Mast_Rec_Iss_Temp_Hold_Code = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Temp_Hold_Code", "ISS-TEMP-HOLD-CODE", FieldType.STRING, 
            1);
        ia_Mast_Rec_Iss_Spirt_Code = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Spirt_Code", "ISS-SPIRT-CODE", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Pens_Plan_Cd = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Pens_Plan_Cd", "ISS-PENS-PLAN-CD", FieldType.STRING, 1);
        ia_Mast_Rec_Iss_Contr_Type_Cd = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Contr_Type_Cd", "ISS-CONTR-TYPE-CD", FieldType.STRING, 
            1);
        ia_Mast_Rec_Iss_New_Issue_Flag = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_New_Issue_Flag", "ISS-NEW-ISSUE-FLAG", FieldType.STRING, 
            1);
        ia_Mast_Rec_Iss_Dte_Dd = ia_Mast_Rec__R_Field_13.newFieldInGroup("ia_Mast_Rec_Iss_Dte_Dd", "ISS-DTE-DD", FieldType.NUMERIC, 2);
        ia_Mast_Rec_Iss_Tran_Code = ia_Mast_Rec__R_Field_13.newFieldArrayInGroup("ia_Mast_Rec_Iss_Tran_Code", "ISS-TRAN-CODE", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 4));

        ia_Mast_Rec__R_Field_16 = ia_Mast_Rec__R_Field_9.newGroupInGroup("ia_Mast_Rec__R_Field_16", "REDEFINE", ia_Mast_Rec_Ia_Mstr_Rcrd);
        ia_Mast_Rec_Ann_1st_Ann_X_Ref = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_X_Ref", "ANN-1ST-ANN-X-REF", FieldType.STRING, 
            9);
        ia_Mast_Rec_Ann_1st_Ann_Sex = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Sex", "ANN-1ST-ANN-SEX", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Ann_1st_Ann_Dob = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Dob", "ANN-1ST-ANN-DOB", FieldType.PACKED_DECIMAL, 
            8);
        ia_Mast_Rec_Ann_1st_Ann_Dod = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Dod", "ANN-1ST-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Ann_1st_Ann_Mort_Yob = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Mort_Yob", "ANN-1ST-ANN-MORT-YOB", FieldType.PACKED_DECIMAL, 
            4);
        ia_Mast_Rec_Ann_1st_Ann_Life_Count = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Life_Count", "ANN-1ST-ANN-LIFE-COUNT", FieldType.NUMERIC, 
            1);
        ia_Mast_Rec_Ann_1st_Ann_Div_Payee = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Div_Payee", "ANN-1ST-ANN-DIV-PAYEE", FieldType.NUMERIC, 
            1);
        ia_Mast_Rec_Ann_1st_Ann_Coll_Cd_Div = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_1st_Ann_Coll_Cd_Div", "ANN-1ST-ANN-COLL-CD-DIV", 
            FieldType.STRING, 5);
        ia_Mast_Rec_Ann_2nd_Ann_X_Ref = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_X_Ref", "ANN-2ND-ANN-X-REF", FieldType.STRING, 
            9);
        ia_Mast_Rec_Ann_2nd_Ann_Sex = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_Sex", "ANN-2ND-ANN-SEX", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Ann_2nd_Ann_Dob = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_Dob", "ANN-2ND-ANN-DOB", FieldType.PACKED_DECIMAL, 
            8);
        ia_Mast_Rec_Ann_2nd_Ann_Dod = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_Dod", "ANN-2ND-ANN-DOD", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Ann_2nd_Ann_Mort_Yob = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_Mort_Yob", "ANN-2ND-ANN-MORT-YOB", FieldType.PACKED_DECIMAL, 
            4);
        ia_Mast_Rec_Ann_2nd_Ann_Life_Count = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_2nd_Ann_Life_Count", "ANN-2ND-ANN-LIFE-COUNT", FieldType.NUMERIC, 
            1);
        ia_Mast_Rec_Ann_Benef_X_Ref_No = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Benef_X_Ref_No", "ANN-BENEF-X-REF-NO", FieldType.STRING, 
            9);
        ia_Mast_Rec_Ann_Benef_Dod = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Benef_Dod", "ANN-BENEF-DOD", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Ann_Da_Contr_Num = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Da_Contr_Num", "ANN-DA-CONTR-NUM", FieldType.STRING, 8);
        ia_Mast_Rec_Ann_Cash_Code = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Cash_Code", "ANN-CASH-CODE", FieldType.STRING, 1);
        ia_Mast_Rec_Ann_Emply_Code = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Emply_Code", "ANN-EMPLY-CODE", FieldType.STRING, 1);
        ia_Mast_Rec_Ann_Prev_Ttb_Disb = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Prev_Ttb_Disb", "ANN-PREV-TTB-DISB", FieldType.STRING, 
            4);
        ia_Mast_Rec_Ann_Curr_Tpa_Disb = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Ann_Curr_Tpa_Disb", "ANN-CURR-TPA-DISB", FieldType.STRING, 
            4);
        ia_Mast_Rec_Filler_1 = ia_Mast_Rec__R_Field_16.newFieldInGroup("ia_Mast_Rec_Filler_1", "FILLER-1", FieldType.STRING, 2);

        ia_Mast_Rec__R_Field_17 = ia_Mast_Rec__R_Field_9.newGroupInGroup("ia_Mast_Rec__R_Field_17", "REDEFINE", ia_Mast_Rec_Ia_Mstr_Rcrd);
        ia_Mast_Rec_Rate_Tot_Old_Tiaa_Div = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Tot_Old_Tiaa_Div", "RATE-TOT-OLD-TIAA-DIV", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Rate_1 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Rate_1", "RATE-RATE-1", FieldType.STRING, 2);
        ia_Mast_Rec_Rate_Per_Pay_1 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Pay_1", "RATE-PER-PAY-1", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Per_Div_1 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Div_1", "RATE-PER-DIV-1", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Final_Pay_1 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Final_Pay_1", "RATE-FINAL-PAY-1", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Date_1 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Date_1", "RATE-DATE-1", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Rate_Rate_2 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Rate_2", "RATE-RATE-2", FieldType.STRING, 2);
        ia_Mast_Rec_Rate_Per_Pay_2 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Pay_2", "RATE-PER-PAY-2", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Per_Div_2 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Div_2", "RATE-PER-DIV-2", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Final_Pay_2 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Final_Pay_2", "RATE-FINAL-PAY-2", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Date_2 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Date_2", "RATE-DATE-2", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Rate_Rate_3 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Rate_3", "RATE-RATE-3", FieldType.STRING, 2);
        ia_Mast_Rec_Rate_Per_Pay_3 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Pay_3", "RATE-PER-PAY-3", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Per_Div_3 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Per_Div_3", "RATE-PER-DIV-3", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Final_Pay_3 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Final_Pay_3", "RATE-FINAL-PAY-3", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Rate_Date_3 = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Date_3", "RATE-DATE-3", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Rate_Trans_Code = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Trans_Code", "RATE-TRANS-CODE", FieldType.NUMERIC, 3);
        ia_Mast_Rec_Rate_Trans_Date = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Trans_Date", "RATE-TRANS-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Filler_1a = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Filler_1a", "FILLER-1A", FieldType.STRING, 6);
        ia_Mast_Rec_Rate_Tot_Old_Tiaa_Pmt = ia_Mast_Rec__R_Field_17.newFieldInGroup("ia_Mast_Rec_Rate_Tot_Old_Tiaa_Pmt", "RATE-TOT-OLD-TIAA-PMT", FieldType.INTEGER, 
            4);

        ia_Mast_Rec__R_Field_18 = ia_Mast_Rec__R_Field_9.newGroupInGroup("ia_Mast_Rec__R_Field_18", "REDEFINE", ia_Mast_Rec_Ia_Mstr_Rcrd);
        ia_Mast_Rec_Us_Tax_Auth = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Tax_Auth", "US-TAX-AUTH", FieldType.STRING, 3);
        ia_Mast_Rec_Us_Active_Code = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Active_Code", "US-ACTIVE-CODE", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Us_Per_Tax_Ded = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Per_Tax_Ded", "US-PER-TAX-DED", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Ytd_Tax_Ded = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Tax_Ded", "US-YTD-TAX-DED", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Us_Recov_Type = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Recov_Type", "US-RECOV-TYPE", FieldType.NUMERIC, 1);
        ia_Mast_Rec_Us_Per_Tax_Free_Amt = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Per_Tax_Free_Amt", "US-PER-TAX-FREE-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Res_Tax_Free_Amt = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Res_Tax_Free_Amt", "US-RES-TAX-FREE-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Cont_To_Date_Inv_Used = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Cont_To_Date_Inv_Used", "US-CONT-TO-DATE-INV-USED", 
            FieldType.PACKED_DECIMAL, 9, 2);
        ia_Mast_Rec_Us_Invest_In_Contr = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Invest_In_Contr", "US-INVEST-IN-CONTR", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax", "US-YTD-ANN-PAY-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax_Free = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Ann_Pay_Tax_Free", "US-YTD-ANN-PAY-TAX-FREE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        ia_Mast_Rec_Us_Ytd_Div_Pay = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Div_Pay", "US-YTD-DIV-PAY", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Ytd_Int_Pay = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Int_Pay", "US-YTD-INT-PAY", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Ytd_Int_Div_Pay = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Ytd_Int_Div_Pay", "US-YTD-INT-DIV-PAY", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id", "US-SOC-SEC-NO-OR-TAX-ID", 
            FieldType.STRING, 9);

        ia_Mast_Rec__R_Field_19 = ia_Mast_Rec__R_Field_18.newGroupInGroup("ia_Mast_Rec__R_Field_19", "REDEFINE", ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id);
        ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id_Num = ia_Mast_Rec__R_Field_19.newFieldInGroup("ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id_Num", "US-SOC-SEC-NO-OR-TAX-ID-NUM", 
            FieldType.NUMERIC, 9);
        ia_Mast_Rec_Us_Start_Date = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Start_Date", "US-START-DATE", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Us_Stop_Date = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Stop_Date", "US-STOP-DATE", FieldType.PACKED_DECIMAL, 6);
        ia_Mast_Rec_Us_Sprt_Lump_Sum = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Sprt_Lump_Sum", "US-SPRT-LUMP-SUM", FieldType.PACKED_DECIMAL, 
            7, 2);
        ia_Mast_Rec_Us_Spirt_Arrival_Date = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Spirt_Arrival_Date", "US-SPIRT-ARRIVAL-DATE", FieldType.PACKED_DECIMAL, 
            4);
        ia_Mast_Rec_Us_Spirt_Arrival_Source = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Spirt_Arrival_Source", "US-SPIRT-ARRIVAL-SOURCE", 
            FieldType.STRING, 1);
        ia_Mast_Rec_Us_Spirt_Process_Date = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Spirt_Process_Date", "US-SPIRT-PROCESS-DATE", FieldType.PACKED_DECIMAL, 
            6);
        ia_Mast_Rec_Us_Filler_1b = ia_Mast_Rec__R_Field_18.newFieldInGroup("ia_Mast_Rec_Us_Filler_1b", "US-FILLER-1B", FieldType.STRING, 2);

        ia_Mast_Rec__R_Field_20 = localVariables.newGroupInRecord("ia_Mast_Rec__R_Field_20", "REDEFINE", ia_Mast_Rec);
        ia_Mast_Rec_High_Value = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_High_Value", "HIGH-VALUE", FieldType.STRING, 2);
        ia_Mast_Rec_Tiaa_Active_Payee_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tiaa_Active_Payee_Mitr", "TIAA-ACTIVE-PAYEE-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tiaa_Periodic_Cont_Mitr", "TIAA-PERIODIC-CONT-MITR", 
            FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Tiaa_Periodic_Div_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tiaa_Periodic_Div_Mitr", "TIAA-PERIODIC-DIV-MITR", FieldType.PACKED_DECIMAL, 
            13, 2);
        ia_Mast_Rec_Tiaa_Final_Pmt_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tiaa_Final_Pmt_Mitr", "TIAA-FINAL-PMT-MITR", FieldType.PACKED_DECIMAL, 
            13, 2);
        ia_Mast_Rec_Tpa_Active_Payees_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tpa_Active_Payees_Mitr", "TPA-ACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_Tpa_Per_Amt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tpa_Per_Amt", "TPA-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Tpa_Div_Amt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Tpa_Div_Amt", "TPA-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Active_Payees_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Ipro_Active_Payees_Mitr", "IPRO-ACTIVE-PAYEES-MITR", 
            FieldType.PACKED_DECIMAL, 7);
        ia_Mast_Rec_Ipro_Per_Amt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Ipro_Per_Amt", "IPRO-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Per_Div = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Ipro_Per_Div", "IPRO-PER-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Ipro_Fin_Pmt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Ipro_Fin_Pmt", "IPRO-FIN-PMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Active_Payees_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_P_I_Active_Payees_Mitr", "P-I-ACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        ia_Mast_Rec_P_I_Per_Amt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_P_I_Per_Amt", "P-I-PER-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Per_Div = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_P_I_Per_Div", "P-I-PER-DIV", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_P_I_Fin_Pmt = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_P_I_Fin_Pmt", "P-I-FIN-PMT", FieldType.PACKED_DECIMAL, 13, 2);
        ia_Mast_Rec_Inactive_Payees_Mitr = ia_Mast_Rec__R_Field_20.newFieldInGroup("ia_Mast_Rec_Inactive_Payees_Mitr", "INACTIVE-PAYEES-MITR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_W_Hex_High_Value = localVariables.newFieldInRecord("pnd_W_Hex_High_Value", "#W-HEX-HIGH-VALUE", FieldType.STRING, 2);
        w_Tape_Out = localVariables.newFieldArrayInRecord("w_Tape_Out", "W-TAPE-OUT", FieldType.STRING, 1, new DbsArrayController(1, 1304));
        w_Tape_Out_B = localVariables.newFieldArrayInRecord("w_Tape_Out_B", "W-TAPE-OUT-B", FieldType.STRING, 1, new DbsArrayController(1, 1304));

        w_Tape_Out_B__R_Field_21 = localVariables.newGroupInRecord("w_Tape_Out_B__R_Field_21", "REDEFINE", w_Tape_Out_B);
        w_Tape_Out_B_W_Ia_Record_Type_Cd_B = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Record_Type_Cd_B", "W-IA-RECORD-TYPE-CD-B", FieldType.STRING, 
            1);
        w_Tape_Out_B_W_Ia_Filler = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Filler", "W-IA-FILLER", FieldType.STRING, 2);
        w_Tape_Out_B_W_Ia_Contract_Number = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Contract_Number", "W-IA-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        w_Tape_Out_B_W_Ia_Issu_Coll_Code = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Issu_Coll_Code", "W-IA-ISSU-COLL-CODE", FieldType.STRING, 
            5);
        w_Tape_Out_B_W_Ia_Pin = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Pin", "W-IA-PIN", FieldType.STRING, 12);
        w_Tape_Out_B_W_Ia_Pull_Out_Cd = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Pull_Out_Cd", "W-IA-PULL-OUT-CD", FieldType.STRING, 
            2);
        w_Tape_Out_B_W_Ia_Social_Security_No = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Social_Security_No", "W-IA-SOCIAL-SECURITY-NO", 
            FieldType.NUMERIC, 9);
        w_Tape_Out_B_W_Ia_Latest_Da_Tiaa = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Latest_Da_Tiaa", "W-IA-LATEST-DA-TIAA", FieldType.STRING, 
            9);
        w_Tape_Out_B_W_Ia_College_Owned_Code = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_College_Owned_Code", "W-IA-COLLEGE-OWNED-CODE", 
            FieldType.STRING, 1);
        w_Tape_Out_B_W_Ia_Sex = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Sex", "W-IA-SEX", FieldType.STRING, 1);
        w_Tape_Out_B_W_Currency_Cd = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Currency_Cd", "W-CURRENCY-CD", FieldType.NUMERIC, 1);
        w_Tape_Out_B_W_Ia_Paymt_Destination = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Paymt_Destination", "W-IA-PAYMT-DESTINATION", 
            FieldType.STRING, 4);
        w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments", "W-IA-TPA-REM-NBR-PAYMENTS", 
            FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date", "W-IA-TPA-NEXT-PAYMENT-DATE", 
            FieldType.PACKED_DECIMAL, 8);
        w_Tape_Out_B_W_Ia_Next_Payment_Amount = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Next_Payment_Amount", "W-IA-NEXT-PAYMENT-AMOUNT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        w_Tape_Out_B_W_Ia_Num_Of_Prod = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Num_Of_Prod", "W-IA-NUM-OF-PROD", FieldType.NUMERIC, 
            2);
        w_Tape_Out_B_W_Ia_Product_Type_Cd = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Product_Type_Cd", "W-IA-PRODUCT-TYPE-CD", FieldType.STRING, 
            1);
        w_Tape_Out_B_W_Ia_Open_Value = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Open_Value", "W-IA-OPEN-VALUE", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_B_W_Ia_Close_Value = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Close_Value", "W-IA-CLOSE-VALUE", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_B_W_Ia_Pend_Code = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Pend_Code", "W-IA-PEND-CODE", FieldType.STRING, 1);
        w_Tape_Out_B_W_Ia_Status_Flag = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Status_Flag", "W-IA-STATUS-FLAG", FieldType.STRING, 
            1);
        w_Tape_Out_B_W_Ia_Dod = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Dod", "W-IA-DOD", FieldType.NUMERIC, 6);

        w_Tape_Out_B__R_Field_22 = w_Tape_Out_B__R_Field_21.newGroupInGroup("w_Tape_Out_B__R_Field_22", "REDEFINE", w_Tape_Out_B_W_Ia_Dod);
        w_Tape_Out_B_W_Ia_Dod_Cc = w_Tape_Out_B__R_Field_22.newFieldInGroup("w_Tape_Out_B_W_Ia_Dod_Cc", "W-IA-DOD-CC", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Dod_Yy = w_Tape_Out_B__R_Field_22.newFieldInGroup("w_Tape_Out_B_W_Ia_Dod_Yy", "W-IA-DOD-YY", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Dod_Mm = w_Tape_Out_B__R_Field_22.newFieldInGroup("w_Tape_Out_B_W_Ia_Dod_Mm", "W-IA-DOD-MM", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Dob = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Dob", "W-IA-DOB", FieldType.NUMERIC, 8);

        w_Tape_Out_B__R_Field_23 = w_Tape_Out_B__R_Field_21.newGroupInGroup("w_Tape_Out_B__R_Field_23", "REDEFINE", w_Tape_Out_B_W_Ia_Dob);
        w_Tape_Out_B_W_Ia_Dob_Yyyy = w_Tape_Out_B__R_Field_23.newFieldInGroup("w_Tape_Out_B_W_Ia_Dob_Yyyy", "W-IA-DOB-YYYY", FieldType.NUMERIC, 4);
        w_Tape_Out_B_W_Ia_Dob_Mm = w_Tape_Out_B__R_Field_23.newFieldInGroup("w_Tape_Out_B_W_Ia_Dob_Mm", "W-IA-DOB-MM", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Dob_Dd = w_Tape_Out_B__R_Field_23.newFieldInGroup("w_Tape_Out_B_W_Ia_Dob_Dd", "W-IA-DOB-DD", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Optn_Code = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Optn_Code", "W-IA-OPTN-CODE", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Mode = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Mode", "W-IA-MODE", FieldType.NUMERIC, 3);

        w_Tape_Out_B__R_Field_24 = w_Tape_Out_B__R_Field_21.newGroupInGroup("w_Tape_Out_B__R_Field_24", "REDEFINE", w_Tape_Out_B_W_Ia_Mode);
        w_Tape_Out_B_W_Ia_Mode_1 = w_Tape_Out_B__R_Field_24.newFieldInGroup("w_Tape_Out_B_W_Ia_Mode_1", "W-IA-MODE-1", FieldType.NUMERIC, 1);
        w_Tape_Out_B_W_Ia_Mode_2 = w_Tape_Out_B__R_Field_24.newFieldInGroup("w_Tape_Out_B_W_Ia_Mode_2", "W-IA-MODE-2", FieldType.NUMERIC, 2);
        w_Tape_Out_B_W_Ia_Conversion_Date = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Conversion_Date", "W-IA-CONVERSION-DATE", FieldType.NUMERIC, 
            6);
        w_Tape_Out_B_W_Ia_Issue_Dte = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Issue_Dte", "W-IA-ISSUE-DTE", FieldType.NUMERIC, 6);
        w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value", "W-IA-OUT-OF-CYCLE-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 2);
        w_Tape_Out_B_W_Ia_Final_Per_Pay_Date = w_Tape_Out_B__R_Field_21.newFieldInGroup("w_Tape_Out_B_W_Ia_Final_Per_Pay_Date", "W-IA-FINAL-PER-PAY-DATE", 
            FieldType.PACKED_DECIMAL, 6);

        w_Tape_Out_B_W_Ia_Fund_Prcnts = w_Tape_Out_B__R_Field_21.newGroupArrayInGroup("w_Tape_Out_B_W_Ia_Fund_Prcnts", "W-IA-FUND-PRCNTS", new DbsArrayController(1, 
            20));
        w_Tape_Out_B_W_Ia_Fund_Cde_B_Rec = w_Tape_Out_B_W_Ia_Fund_Prcnts.newFieldInGroup("w_Tape_Out_B_W_Ia_Fund_Cde_B_Rec", "W-IA-FUND-CDE-B-REC", FieldType.STRING, 
            3);
        w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec = w_Tape_Out_B_W_Ia_Fund_Prcnts.newFieldInGroup("w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec", "W-IA-FUND-PCT-B-REC", FieldType.NUMERIC, 
            5, 1);

        w_Tape_Out_B_W_Ia_Rates = w_Tape_Out_B__R_Field_21.newGroupArrayInGroup("w_Tape_Out_B_W_Ia_Rates", "W-IA-RATES", new DbsArrayController(1, 60));
        w_Tape_Out_B_W_Ia_Rate_Cde = w_Tape_Out_B_W_Ia_Rates.newFieldInGroup("w_Tape_Out_B_W_Ia_Rate_Cde", "W-IA-RATE-CDE", FieldType.STRING, 2);
        w_Tape_Out_B_W_Ia_Rate_Per_Pay = w_Tape_Out_B_W_Ia_Rates.newFieldInGroup("w_Tape_Out_B_W_Ia_Rate_Per_Pay", "W-IA-RATE-PER-PAY", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_B_W_Ia_Rate_Per_Div = w_Tape_Out_B_W_Ia_Rates.newFieldInGroup("w_Tape_Out_B_W_Ia_Rate_Per_Div", "W-IA-RATE-PER-DIV", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_B_W_Rate_Final_Pay = w_Tape_Out_B_W_Ia_Rates.newFieldInGroup("w_Tape_Out_B_W_Rate_Final_Pay", "W-RATE-FINAL-PAY", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_N = localVariables.newFieldArrayInRecord("w_Tape_Out_N", "W-TAPE-OUT-N", FieldType.STRING, 1, new DbsArrayController(1, 1304));

        w_Tape_Out_N__R_Field_25 = localVariables.newGroupInRecord("w_Tape_Out_N__R_Field_25", "REDEFINE", w_Tape_Out_N);
        w_Tape_Out_N_W_Ia_Record_Type_Cd_N = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Record_Type_Cd_N", "W-IA-RECORD-TYPE-CD-N", FieldType.STRING, 
            1);
        w_Tape_Out_N_W_Ia_Trn_Trans_Code = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Trans_Code", "W-IA-TRN-TRANS-CODE", FieldType.STRING, 
            2);
        w_Tape_Out_N_W_Ia_Trn_Contract_Number = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Contract_Number", "W-IA-TRN-CONTRACT-NUMBER", 
            FieldType.STRING, 10);
        w_Tape_Out_N_W_Ia_Trn_Part_Date = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Part_Date", "W-IA-TRN-PART-DATE", FieldType.PACKED_DECIMAL, 
            8);
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Transaction_Date", "W-IA-TRN-TRANSACTION-DATE", 
            FieldType.PACKED_DECIMAL, 8);
        w_Tape_Out_N_W_Ia_Trn_Product_Code = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Product_Code", "W-IA-TRN-PRODUCT-CODE", FieldType.STRING, 
            1);
        w_Tape_Out_N_W_Ia_Trn_Amount = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Amount", "W-IA-TRN-AMOUNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        w_Tape_Out_N_W_Ia_Trn_Filler = w_Tape_Out_N__R_Field_25.newFieldInGroup("w_Tape_Out_N_W_Ia_Trn_Filler", "W-IA-TRN-FILLER", FieldType.STRING, 90);

        w_Tape_Out_N_W_Ia_Fund_Prcnts = w_Tape_Out_N__R_Field_25.newGroupArrayInGroup("w_Tape_Out_N_W_Ia_Fund_Prcnts", "W-IA-FUND-PRCNTS", new DbsArrayController(1, 
            20));
        w_Tape_Out_N_W_Ia_Fund_Cde_N_Rec = w_Tape_Out_N_W_Ia_Fund_Prcnts.newFieldInGroup("w_Tape_Out_N_W_Ia_Fund_Cde_N_Rec", "W-IA-FUND-CDE-N-REC", FieldType.STRING, 
            3);
        w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec = w_Tape_Out_N_W_Ia_Fund_Prcnts.newFieldInGroup("w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec", "W-IA-FUND-PCT-N-REC", FieldType.NUMERIC, 
            5, 1);
        pnd_W_Total_Record = localVariables.newFieldArrayInRecord("pnd_W_Total_Record", "#W-TOTAL-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            1304));

        pnd_W_Total_Record__R_Field_26 = localVariables.newGroupInRecord("pnd_W_Total_Record__R_Field_26", "REDEFINE", pnd_W_Total_Record);
        pnd_W_Total_Record_Pnd_W_Total_Record_Parm = pnd_W_Total_Record__R_Field_26.newFieldInGroup("pnd_W_Total_Record_Pnd_W_Total_Record_Parm", "#W-TOTAL-RECORD-PARM", 
            FieldType.STRING, 118);

        pnd_W_Total_Record__R_Field_27 = pnd_W_Total_Record__R_Field_26.newGroupInGroup("pnd_W_Total_Record__R_Field_27", "REDEFINE", pnd_W_Total_Record_Pnd_W_Total_Record_Parm);
        pnd_W_Total_Record_W_Ia_Record_Type_Cd_X = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Record_Type_Cd_X", "W-IA-RECORD-TYPE-CD-X", 
            FieldType.STRING, 1);
        pnd_W_Total_Record_W_Ia_Tpa_Written_B = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Written_B", "W-IA-TPA-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Tpa_Written_N = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Written_N", "W-IA-TPA-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Written_B = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Written_B", "W-IA-IPRO-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Written_N = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Written_N", "W-IA-IPRO-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_B = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Written_B", "W-IA-P&I-WRITTEN-B", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Written_N", "W-IA-P&I-WRITTEN-N", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Tpa_Open_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Open_Value", "W-IA-TPA-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Close_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Close_Value", "W-IA-TPA-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount", 
            "W-IA-TPA-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Open_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Open_Value", "W-IA-IPRO-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Close_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Close_Value", "W-IA-IPRO-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount", 
            "W-IA-IPRO-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value", "W-IA-P&I-OPEN-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value", "W-IA-P&I-CLOSE-VALUE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount", 
            "W-IA-P&I-NON-PREMIUM-AMOUNT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_W_Total_Record_W_Ia_Tpa_Pullouts = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Tpa_Pullouts", "W-IA-TPA-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Ipro_Pullouts = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Ipro_Pullouts", "W-IA-IPRO-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts = pnd_W_Total_Record__R_Field_27.newFieldInGroup("pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts", "W-IA-P&I-PULLOUTS", 
            FieldType.PACKED_DECIMAL, 11);
        pnd_Ccyymmdd = localVariables.newFieldInRecord("pnd_Ccyymmdd", "#CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ccyymmdd__R_Field_28 = localVariables.newGroupInRecord("pnd_Ccyymmdd__R_Field_28", "REDEFINE", pnd_Ccyymmdd);
        pnd_Ccyymmdd_Pnd_Current_Ccyy = pnd_Ccyymmdd__R_Field_28.newFieldInGroup("pnd_Ccyymmdd_Pnd_Current_Ccyy", "#CURRENT-CCYY", FieldType.NUMERIC, 
            4);

        pnd_Ccyymmdd__R_Field_29 = pnd_Ccyymmdd__R_Field_28.newGroupInGroup("pnd_Ccyymmdd__R_Field_29", "REDEFINE", pnd_Ccyymmdd_Pnd_Current_Ccyy);
        pnd_Ccyymmdd_Pnd_Current_Cc = pnd_Ccyymmdd__R_Field_29.newFieldInGroup("pnd_Ccyymmdd_Pnd_Current_Cc", "#CURRENT-CC", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Current_Yy = pnd_Ccyymmdd__R_Field_29.newFieldInGroup("pnd_Ccyymmdd_Pnd_Current_Yy", "#CURRENT-YY", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Current_Mm = pnd_Ccyymmdd__R_Field_28.newFieldInGroup("pnd_Ccyymmdd_Pnd_Current_Mm", "#CURRENT-MM", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Current_Dd = pnd_Ccyymmdd__R_Field_28.newFieldInGroup("pnd_Ccyymmdd_Pnd_Current_Dd", "#CURRENT-DD", FieldType.NUMERIC, 2);
        pnd_Next_Ccyymmdd = localVariables.newFieldInRecord("pnd_Next_Ccyymmdd", "#NEXT-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Next_Ccyymmdd__R_Field_30 = localVariables.newGroupInRecord("pnd_Next_Ccyymmdd__R_Field_30", "REDEFINE", pnd_Next_Ccyymmdd);
        pnd_Next_Ccyymmdd_Pnd_Next_Ccyy = pnd_Next_Ccyymmdd__R_Field_30.newFieldInGroup("pnd_Next_Ccyymmdd_Pnd_Next_Ccyy", "#NEXT-CCYY", FieldType.NUMERIC, 
            4);

        pnd_Next_Ccyymmdd__R_Field_31 = pnd_Next_Ccyymmdd__R_Field_30.newGroupInGroup("pnd_Next_Ccyymmdd__R_Field_31", "REDEFINE", pnd_Next_Ccyymmdd_Pnd_Next_Ccyy);
        pnd_Next_Ccyymmdd_Pnd_Next_Cc = pnd_Next_Ccyymmdd__R_Field_31.newFieldInGroup("pnd_Next_Ccyymmdd_Pnd_Next_Cc", "#NEXT-CC", FieldType.NUMERIC, 
            2);
        pnd_Next_Ccyymmdd_Pnd_Next_Yy = pnd_Next_Ccyymmdd__R_Field_31.newFieldInGroup("pnd_Next_Ccyymmdd_Pnd_Next_Yy", "#NEXT-YY", FieldType.NUMERIC, 
            2);
        pnd_Next_Ccyymmdd_Pnd_Next_Mm = pnd_Next_Ccyymmdd__R_Field_30.newFieldInGroup("pnd_Next_Ccyymmdd_Pnd_Next_Mm", "#NEXT-MM", FieldType.NUMERIC, 
            2);
        pnd_Next_Ccyymmdd_Pnd_Next_Dd = pnd_Next_Ccyymmdd__R_Field_30.newFieldInGroup("pnd_Next_Ccyymmdd_Pnd_Next_Dd", "#NEXT-DD", FieldType.NUMERIC, 
            2);

        pnd_Work1 = localVariables.newGroupInRecord("pnd_Work1", "#WORK1");
        pnd_Work1_Pnd_W_Check_Date = pnd_Work1.newFieldInGroup("pnd_Work1_Pnd_W_Check_Date", "#W-CHECK-DATE", FieldType.NUMERIC, 6);

        pnd_Work1__R_Field_32 = pnd_Work1.newGroupInGroup("pnd_Work1__R_Field_32", "REDEFINE", pnd_Work1_Pnd_W_Check_Date);
        pnd_Work1_Pnd_W_Check_Ccyy = pnd_Work1__R_Field_32.newFieldInGroup("pnd_Work1_Pnd_W_Check_Ccyy", "#W-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Work1_Pnd_W_Check_Mm = pnd_Work1__R_Field_32.newFieldInGroup("pnd_Work1_Pnd_W_Check_Mm", "#W-CHECK-MM", FieldType.NUMERIC, 2);

        pnd_Work1__R_Field_33 = pnd_Work1.newGroupInGroup("pnd_Work1__R_Field_33", "REDEFINE", pnd_Work1_Pnd_W_Check_Date);
        pnd_Work1_Pnd_W_Check_Ccyymm = pnd_Work1__R_Field_33.newFieldInGroup("pnd_Work1_Pnd_W_Check_Ccyymm", "#W-CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_W_Trans_Date = localVariables.newFieldInRecord("pnd_W_Trans_Date", "#W-TRANS-DATE", FieldType.NUMERIC, 8);

        pnd_W_Trans_Date__R_Field_34 = localVariables.newGroupInRecord("pnd_W_Trans_Date__R_Field_34", "REDEFINE", pnd_W_Trans_Date);
        pnd_W_Trans_Date_Pnd_W_Trans_Ccyy = pnd_W_Trans_Date__R_Field_34.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Trans_Ccyy", "#W-TRANS-CCYY", FieldType.NUMERIC, 
            4);
        pnd_W_Trans_Date_Pnd_W_Trans_Mm = pnd_W_Trans_Date__R_Field_34.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Trans_Mm", "#W-TRANS-MM", FieldType.NUMERIC, 
            2);
        pnd_W_Trans_Date_Pnd_W_Trans_Dd = pnd_W_Trans_Date__R_Field_34.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Trans_Dd", "#W-TRANS-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Trans_Date__R_Field_35 = localVariables.newGroupInRecord("pnd_W_Trans_Date__R_Field_35", "REDEFINE", pnd_W_Trans_Date);
        pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm = pnd_W_Trans_Date__R_Field_35.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm", "#W-TRANS-CCYYMM", FieldType.NUMERIC, 
            6);
        pnd_W_Trans_Date_Pnd_W_Fill_Dd = pnd_W_Trans_Date__R_Field_35.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Fill_Dd", "#W-FILL-DD", FieldType.NUMERIC, 
            2);

        pnd_W_Trans_Date__R_Field_36 = localVariables.newGroupInRecord("pnd_W_Trans_Date__R_Field_36", "REDEFINE", pnd_W_Trans_Date);
        pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm_A = pnd_W_Trans_Date__R_Field_36.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm_A", "#W-TRANS-CCYYMM-A", 
            FieldType.STRING, 6);
        pnd_W_Trans_Date_Pnd_W_Fill_Dd_A = pnd_W_Trans_Date__R_Field_36.newFieldInGroup("pnd_W_Trans_Date_Pnd_W_Fill_Dd_A", "#W-FILL-DD-A", FieldType.STRING, 
            2);
        pnd_W_Tot_Fin_Pmt_Amt = localVariables.newFieldInRecord("pnd_W_Tot_Fin_Pmt_Amt", "#W-TOT-FIN-PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Sve_W_Tot_Fin_Pmt_Amt = localVariables.newFieldInRecord("pnd_Sve_W_Tot_Fin_Pmt_Amt", "#SVE-W-TOT-FIN-PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_W_Comm_Date = localVariables.newFieldInRecord("pnd_W_Comm_Date", "#W-COMM-DATE", FieldType.STRING, 8);

        pnd_W_Comm_Date__R_Field_37 = localVariables.newGroupInRecord("pnd_W_Comm_Date__R_Field_37", "REDEFINE", pnd_W_Comm_Date);
        pnd_W_Comm_Date_Pnd_W_Comm_Ccyy = pnd_W_Comm_Date__R_Field_37.newFieldInGroup("pnd_W_Comm_Date_Pnd_W_Comm_Ccyy", "#W-COMM-CCYY", FieldType.NUMERIC, 
            4);
        pnd_W_Comm_Date_Pnd_W_Comm_Mm = pnd_W_Comm_Date__R_Field_37.newFieldInGroup("pnd_W_Comm_Date_Pnd_W_Comm_Mm", "#W-COMM-MM", FieldType.NUMERIC, 
            2);
        pnd_W_Comm_Date_Pnd_W_Comm_Dd = pnd_W_Comm_Date__R_Field_37.newFieldInGroup("pnd_W_Comm_Date_Pnd_W_Comm_Dd", "#W-COMM-DD", FieldType.NUMERIC, 
            2);
        pnd_W_Iss_Final_Per_Pay_Date = localVariables.newFieldInRecord("pnd_W_Iss_Final_Per_Pay_Date", "#W-ISS-FINAL-PER-PAY-DATE", FieldType.NUMERIC, 
            6);

        pnd_W_Iss_Final_Per_Pay_Date__R_Field_38 = localVariables.newGroupInRecord("pnd_W_Iss_Final_Per_Pay_Date__R_Field_38", "REDEFINE", pnd_W_Iss_Final_Per_Pay_Date);
        pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccyy = pnd_W_Iss_Final_Per_Pay_Date__R_Field_38.newFieldInGroup("pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccyy", 
            "#W-ISS-FINAL-PER-PAY-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccmm = pnd_W_Iss_Final_Per_Pay_Date__R_Field_38.newFieldInGroup("pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccmm", 
            "#W-ISS-FINAL-PER-PAY-DATE-CCMM", FieldType.NUMERIC, 2);
        pnd_W_Hold_Date = localVariables.newFieldInRecord("pnd_W_Hold_Date", "#W-HOLD-DATE", FieldType.NUMERIC, 8);

        pnd_W_Hold_Date__R_Field_39 = localVariables.newGroupInRecord("pnd_W_Hold_Date__R_Field_39", "REDEFINE", pnd_W_Hold_Date);
        pnd_W_Hold_Date_Pnd_W_Hold_Ccyy = pnd_W_Hold_Date__R_Field_39.newFieldInGroup("pnd_W_Hold_Date_Pnd_W_Hold_Ccyy", "#W-HOLD-CCYY", FieldType.NUMERIC, 
            4);
        pnd_W_Hold_Date_Pnd_W_Hold_Mm = pnd_W_Hold_Date__R_Field_39.newFieldInGroup("pnd_W_Hold_Date_Pnd_W_Hold_Mm", "#W-HOLD-MM", FieldType.NUMERIC, 
            2);
        pnd_W_Hold_Date_Pnd_W_Hold_Dd = pnd_W_Hold_Date__R_Field_39.newFieldInGroup("pnd_W_Hold_Date_Pnd_W_Hold_Dd", "#W-HOLD-DD", FieldType.NUMERIC, 
            2);
        pnd_W_Iss_1st_Pay_Paid_Date = localVariables.newFieldInRecord("pnd_W_Iss_1st_Pay_Paid_Date", "#W-ISS-1ST-PAY-PAID-DATE", FieldType.NUMERIC, 6);

        pnd_W_Iss_1st_Pay_Paid_Date__R_Field_40 = localVariables.newGroupInRecord("pnd_W_Iss_1st_Pay_Paid_Date__R_Field_40", "REDEFINE", pnd_W_Iss_1st_Pay_Paid_Date);
        pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Ccyy = pnd_W_Iss_1st_Pay_Paid_Date__R_Field_40.newFieldInGroup("pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Ccyy", 
            "#W-ISS-1ST-PAY-PAID-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Mm = pnd_W_Iss_1st_Pay_Paid_Date__R_Field_40.newFieldInGroup("pnd_W_Iss_1st_Pay_Paid_Date_Pnd_W_Iss_1st_Pay_Paid_Date_Mm", 
            "#W-ISS-1ST-PAY-PAID-DATE-MM", FieldType.NUMERIC, 2);
        pnd_W_Iss_Iss_Date = localVariables.newFieldInRecord("pnd_W_Iss_Iss_Date", "#W-ISS-ISS-DATE", FieldType.NUMERIC, 6);

        pnd_W_Iss_Iss_Date__R_Field_41 = localVariables.newGroupInRecord("pnd_W_Iss_Iss_Date__R_Field_41", "REDEFINE", pnd_W_Iss_Iss_Date);
        pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy = pnd_W_Iss_Iss_Date__R_Field_41.newFieldInGroup("pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy", "#W-ISS-ISS-DATE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm = pnd_W_Iss_Iss_Date__R_Field_41.newFieldInGroup("pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm", "#W-ISS-ISS-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_W_Iss_Dte_Dd = localVariables.newFieldInRecord("pnd_W_Iss_Dte_Dd", "#W-ISS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_W_Tax_Stop_Date = localVariables.newFieldInRecord("pnd_W_Tax_Stop_Date", "#W-TAX-STOP-DATE", FieldType.NUMERIC, 6);

        pnd_W_Tax_Stop_Date__R_Field_42 = localVariables.newGroupInRecord("pnd_W_Tax_Stop_Date__R_Field_42", "REDEFINE", pnd_W_Tax_Stop_Date);
        pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Ccyy = pnd_W_Tax_Stop_Date__R_Field_42.newFieldInGroup("pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Ccyy", 
            "#W-TAX-STOP-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm = pnd_W_Tax_Stop_Date__R_Field_42.newFieldInGroup("pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm", "#W-TAX-STOP-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Sve_Tpa_Due_Date = localVariables.newFieldInRecord("pnd_Sve_Tpa_Due_Date", "#SVE-TPA-DUE-DATE", FieldType.NUMERIC, 6);
        pnd_W_Iss_1st_Pay_Due_Date = localVariables.newFieldInRecord("pnd_W_Iss_1st_Pay_Due_Date", "#W-ISS-1ST-PAY-DUE-DATE", FieldType.NUMERIC, 6);

        pnd_W_Iss_1st_Pay_Due_Date__R_Field_43 = localVariables.newGroupInRecord("pnd_W_Iss_1st_Pay_Due_Date__R_Field_43", "REDEFINE", pnd_W_Iss_1st_Pay_Due_Date);
        pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Ccyy = pnd_W_Iss_1st_Pay_Due_Date__R_Field_43.newFieldInGroup("pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Ccyy", 
            "#W-ISS-1ST-PAY-DUE-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Mm = pnd_W_Iss_1st_Pay_Due_Date__R_Field_43.newFieldInGroup("pnd_W_Iss_1st_Pay_Due_Date_Pnd_W_Iss_1st_Pay_Due_Date_Mm", 
            "#W-ISS-1ST-PAY-DUE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sub1 = localVariables.newFieldInRecord("pnd_Sub1", "#SUB1", FieldType.NUMERIC, 4);
        pnd_W_Ia_Ipro_Open_Value_Less_Intr = localVariables.newFieldInRecord("pnd_W_Ia_Ipro_Open_Value_Less_Intr", "#W-IA-IPRO-OPEN-VALUE-LESS-INTR", 
            FieldType.NUMERIC, 13, 2);
        pnd_W_Ia_Ipro_Close_Value_Less_Intr = localVariables.newFieldInRecord("pnd_W_Ia_Ipro_Close_Value_Less_Intr", "#W-IA-IPRO-CLOSE-VALUE-LESS-INTR", 
            FieldType.NUMERIC, 13, 2);
        pnd_W_Total_Payment_Amount = localVariables.newFieldInRecord("pnd_W_Total_Payment_Amount", "#W-TOTAL-PAYMENT-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Sve_W_Total_Payment_Amount = localVariables.newFieldInRecord("pnd_Sve_W_Total_Payment_Amount", "#SVE-W-TOTAL-PAYMENT-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_W_Total_Tpa_Payment_Amount = localVariables.newFieldInRecord("pnd_W_Total_Tpa_Payment_Amount", "#W-TOTAL-TPA-PAYMENT-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_W_Total_Guarpymt_Amount = localVariables.newFieldInRecord("pnd_W_Total_Guarpymt_Amount", "#W-TOTAL-GUARPYMT-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_W_Total_Guardivd_Amount = localVariables.newFieldInRecord("pnd_W_Total_Guardivd_Amount", "#W-TOTAL-GUARDIVD-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_W_Total_Guardivd_Prev = localVariables.newFieldInRecord("pnd_W_Total_Guardivd_Prev", "#W-TOTAL-GUARDIVD-PREV", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_W_Total_Transfer_Amount = localVariables.newFieldInRecord("pnd_W_Total_Transfer_Amount", "#W-TOTAL-TRANSFER-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_W_Ia_Pin = localVariables.newFieldInRecord("pnd_W_Ia_Pin", "#W-IA-PIN", FieldType.NUMERIC, 12);

        pnd_W_Ia_Pin__R_Field_44 = localVariables.newGroupInRecord("pnd_W_Ia_Pin__R_Field_44", "REDEFINE", pnd_W_Ia_Pin);
        pnd_W_Ia_Pin_Pnd_W_Ia_Pin_5 = pnd_W_Ia_Pin__R_Field_44.newFieldInGroup("pnd_W_Ia_Pin_Pnd_W_Ia_Pin_5", "#W-IA-PIN-5", FieldType.NUMERIC, 5);
        pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7 = pnd_W_Ia_Pin__R_Field_44.newFieldInGroup("pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7", "#W-IA-PIN-7", FieldType.NUMERIC, 7);
        pnd_W_Next_Due_Dte_Ccyymmdd = localVariables.newFieldInRecord("pnd_W_Next_Due_Dte_Ccyymmdd", "#W-NEXT-DUE-DTE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45 = localVariables.newGroupInRecord("pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45", "REDEFINE", pnd_W_Next_Due_Dte_Ccyymmdd);
        pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm = pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45.newFieldInGroup("pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm", 
            "#W-NEXT-DUE-DTE-CCYYMM", FieldType.NUMERIC, 6);

        pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_46 = pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45.newGroupInGroup("pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_46", "REDEFINE", 
            pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm);
        pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyy = pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_46.newFieldInGroup("pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyy", 
            "#W-NEXT-DUE-DTE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Mm = pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_46.newFieldInGroup("pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Mm", 
            "#W-NEXT-DUE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Dd = pnd_W_Next_Due_Dte_Ccyymmdd__R_Field_45.newFieldInGroup("pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Dd", 
            "#W-NEXT-DUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_W_Hold_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_W_Hold_Date_Ccyymmdd", "#W-HOLD-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_W_Hold_Date_Ccyymmdd__R_Field_47 = localVariables.newGroupInRecord("pnd_W_Hold_Date_Ccyymmdd__R_Field_47", "REDEFINE", pnd_W_Hold_Date_Ccyymmdd);
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm = pnd_W_Hold_Date_Ccyymmdd__R_Field_47.newFieldInGroup("pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm", 
            "#W-HOLD-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd = pnd_W_Hold_Date_Ccyymmdd__R_Field_47.newFieldInGroup("pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd", 
            "#W-HOLD-DATE-DD", FieldType.NUMERIC, 2);

        pnd_W_Hold_Date_Ccyymmdd__R_Field_48 = localVariables.newGroupInRecord("pnd_W_Hold_Date_Ccyymmdd__R_Field_48", "REDEFINE", pnd_W_Hold_Date_Ccyymmdd);
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy = pnd_W_Hold_Date_Ccyymmdd__R_Field_48.newFieldInGroup("pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy", 
            "#W-HOLD-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm = pnd_W_Hold_Date_Ccyymmdd__R_Field_48.newFieldInGroup("pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm", 
            "#W-HOLD-DATE-MM", FieldType.NUMERIC, 2);
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd2 = pnd_W_Hold_Date_Ccyymmdd__R_Field_48.newFieldInGroup("pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd2", 
            "#W-HOLD-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.NUMERIC, 8);

        pnd_W_Date__R_Field_49 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_49", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Wrk_Mm = pnd_W_Date__R_Field_49.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Mm", "#W-WRK-MM", FieldType.NUMERIC, 2);
        pnd_W_Date_Pnd_W_Wrk_Dd = pnd_W_Date__R_Field_49.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Dd", "#W-WRK-DD", FieldType.NUMERIC, 2);
        pnd_W_Date_Pnd_W_Wrk_Ccyy = pnd_W_Date__R_Field_49.newFieldInGroup("pnd_W_Date_Pnd_W_Wrk_Ccyy", "#W-WRK-CCYY", FieldType.NUMERIC, 4);
        pnd_Pia3170_Call_Type_G = localVariables.newFieldInRecord("pnd_Pia3170_Call_Type_G", "#PIA3170-CALL-TYPE-G", FieldType.STRING, 1);
        pnd_Pia3170_Call_Type = localVariables.newFieldInRecord("pnd_Pia3170_Call_Type", "#PIA3170-CALL-TYPE", FieldType.STRING, 1);
        pnd_Pia3170_Comm_Date = localVariables.newFieldInRecord("pnd_Pia3170_Comm_Date", "#PIA3170-COMM-DATE", FieldType.NUMERIC, 8);

        pnd_Pia3170_Comm_Date__R_Field_50 = localVariables.newGroupInRecord("pnd_Pia3170_Comm_Date__R_Field_50", "REDEFINE", pnd_Pia3170_Comm_Date);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Cc = pnd_Pia3170_Comm_Date__R_Field_50.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Cc", "#PIA3170-COMM-CC", 
            FieldType.NUMERIC, 2);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Yy = pnd_Pia3170_Comm_Date__R_Field_50.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Yy", "#PIA3170-COMM-YY", 
            FieldType.NUMERIC, 2);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Mm = pnd_Pia3170_Comm_Date__R_Field_50.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Mm", "#PIA3170-COMM-MM", 
            FieldType.NUMERIC, 2);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd = pnd_Pia3170_Comm_Date__R_Field_50.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd", "#PIA3170-COMM-DD", 
            FieldType.NUMERIC, 2);

        pnd_Pia3170_Comm_Date__R_Field_51 = localVariables.newGroupInRecord("pnd_Pia3170_Comm_Date__R_Field_51", "REDEFINE", pnd_Pia3170_Comm_Date);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm = pnd_Pia3170_Comm_Date__R_Field_51.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm", 
            "#PIA3170-COMM-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd2 = pnd_Pia3170_Comm_Date__R_Field_51.newFieldInGroup("pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd2", "#PIA3170-COMM-DD2", 
            FieldType.NUMERIC, 2);
        pnd_Pia3170_Cycle_Date = localVariables.newFieldInRecord("pnd_Pia3170_Cycle_Date", "#PIA3170-CYCLE-DATE", FieldType.NUMERIC, 8);

        pnd_Pia3170_Cycle_Date__R_Field_52 = localVariables.newGroupInRecord("pnd_Pia3170_Cycle_Date__R_Field_52", "REDEFINE", pnd_Pia3170_Cycle_Date);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Cc = pnd_Pia3170_Cycle_Date__R_Field_52.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Cc", 
            "#PIA3170-CYCLE-CC", FieldType.NUMERIC, 2);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Yy = pnd_Pia3170_Cycle_Date__R_Field_52.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Yy", 
            "#PIA3170-CYCLE-YY", FieldType.NUMERIC, 2);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Mm = pnd_Pia3170_Cycle_Date__R_Field_52.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Mm", 
            "#PIA3170-CYCLE-MM", FieldType.NUMERIC, 2);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd = pnd_Pia3170_Cycle_Date__R_Field_52.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd", 
            "#PIA3170-CYCLE-DD", FieldType.NUMERIC, 2);

        pnd_Pia3170_Cycle_Date__R_Field_53 = localVariables.newGroupInRecord("pnd_Pia3170_Cycle_Date__R_Field_53", "REDEFINE", pnd_Pia3170_Cycle_Date);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Ccyymm = pnd_Pia3170_Cycle_Date__R_Field_53.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Ccyymm", 
            "#PIA3170-CYCLE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd2 = pnd_Pia3170_Cycle_Date__R_Field_53.newFieldInGroup("pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd2", 
            "#PIA3170-CYCLE-DD2", FieldType.NUMERIC, 2);
        pnd_Pia3170_Comm_Value = localVariables.newFieldInRecord("pnd_Pia3170_Comm_Value", "#PIA3170-COMM-VALUE", FieldType.NUMERIC, 11, 2);

        pnd_Pia3170_Comm_Value__R_Field_54 = localVariables.newGroupInRecord("pnd_Pia3170_Comm_Value__R_Field_54", "REDEFINE", pnd_Pia3170_Comm_Value);
        pnd_Pia3170_Comm_Value_Pnd_Pia3170_Comm_Value_A = pnd_Pia3170_Comm_Value__R_Field_54.newFieldInGroup("pnd_Pia3170_Comm_Value_Pnd_Pia3170_Comm_Value_A", 
            "#PIA3170-COMM-VALUE-A", FieldType.STRING, 11);
        pnd_Pia3170_Error_01a = localVariables.newFieldInRecord("pnd_Pia3170_Error_01a", "#PIA3170-ERROR-01A", FieldType.STRING, 1);
        pnd_Dummy1 = localVariables.newFieldInRecord("pnd_Dummy1", "#DUMMY1", FieldType.STRING, 1);
        pnd_Dummy21 = localVariables.newFieldInRecord("pnd_Dummy21", "#DUMMY21", FieldType.STRING, 30);

        pnd_Master_Rec_Linkage = localVariables.newGroupInRecord("pnd_Master_Rec_Linkage", "#MASTER-REC-LINKAGE");
        pnd_Master_Rec_Linkage_Pnd_Rec1 = pnd_Master_Rec_Linkage.newFieldInGroup("pnd_Master_Rec_Linkage_Pnd_Rec1", "#REC1", FieldType.STRING, 100);
        pnd_Master_Rec_Linkage_Pnd_Rec2 = pnd_Master_Rec_Linkage.newFieldInGroup("pnd_Master_Rec_Linkage_Pnd_Rec2", "#REC2", FieldType.STRING, 100);
        pnd_Master_Rec_Linkage_Pnd_Rec3 = pnd_Master_Rec_Linkage.newFieldInGroup("pnd_Master_Rec_Linkage_Pnd_Rec3", "#REC3", FieldType.STRING, 100);
        pnd_Master_Rec_Linkage_Pnd_Rec4 = pnd_Master_Rec_Linkage.newFieldArrayInGroup("pnd_Master_Rec_Linkage_Pnd_Rec4", "#REC4", FieldType.STRING, 100, 
            new DbsArrayController(1, 10));
        pnd_Master_Rec_Linkage_Pnd_Rec5 = pnd_Master_Rec_Linkage.newFieldArrayInGroup("pnd_Master_Rec_Linkage_Pnd_Rec5", "#REC5", FieldType.STRING, 100, 
            new DbsArrayController(1, 36));
        pnd_Master_Rec_Linkage_Pnd_Rec_All = pnd_Master_Rec_Linkage.newFieldArrayInGroup("pnd_Master_Rec_Linkage_Pnd_Rec_All", "#REC-ALL", FieldType.STRING, 
            100, new DbsArrayController(1, 45));
        pnd_Master_Rec_Linkage_Pnd_Filler_2 = pnd_Master_Rec_Linkage.newFieldInGroup("pnd_Master_Rec_Linkage_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 
            10);

        pnd_Naz_Parm = localVariables.newGroupInRecord("pnd_Naz_Parm", "#NAZ-PARM");
        pnd_Naz_Parm_Pnd_I_Unique_Id_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Unique_Id_Naz", "#I-UNIQUE-ID-NAZ", FieldType.NUMERIC, 7);
        pnd_Naz_Parm_Pnd_I_Da_Contract_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Da_Contract_Naz", "#I-DA-CONTRACT-NAZ", FieldType.STRING, 
            8);
        pnd_Naz_Parm_Pnd_I_Ia_Contract_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Ia_Contract_Naz", "#I-IA-CONTRACT-NAZ", FieldType.STRING, 
            8);
        pnd_Naz_Parm_Pnd_I_Ia_Payee_Cde = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Ia_Payee_Cde", "#I-IA-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Naz_Parm_Pnd_I_Ia_Amount_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Ia_Amount_Naz", "#I-IA-AMOUNT-NAZ", FieldType.NUMERIC, 11, 
            2);
        pnd_Naz_Parm_Pnd_I_Read_Type = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_I_Read_Type", "#I-READ-TYPE", FieldType.STRING, 1);
        pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz", "#O-SETTLE-AMT-NAZ", FieldType.NUMERIC, 
            11, 2);
        pnd_Naz_Parm_Pnd_O_Total_Accum_Amt = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_O_Total_Accum_Amt", "#O-TOTAL-ACCUM-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Naz_Parm_Pnd_O_Pmt_Amt = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_O_Pmt_Amt", "#O-PMT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Naz_Parm_Pnd_O_Found_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_O_Found_Naz", "#O-FOUND-NAZ", FieldType.STRING, 1);
        pnd_Naz_Parm_Pnd_O_25_Over_Sw_Naz = pnd_Naz_Parm.newFieldInGroup("pnd_Naz_Parm_Pnd_O_25_Over_Sw_Naz", "#O-25-OVER-SW-NAZ", FieldType.STRING, 1);

        pnd_Ia_Trn_Call_Parms = localVariables.newGroupInRecord("pnd_Ia_Trn_Call_Parms", "#IA-TRN-CALL-PARMS");
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_8_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_8_Np", "#I-IA-ISSUE-DTE-8-NP", 
            FieldType.STRING, 8);

        pnd_Ia_Trn_Call_Parms__R_Field_55 = pnd_Ia_Trn_Call_Parms.newGroupInGroup("pnd_Ia_Trn_Call_Parms__R_Field_55", "REDEFINE", pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_8_Np);
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Np = pnd_Ia_Trn_Call_Parms__R_Field_55.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Np", 
            "#I-IA-ISSUE-DTE-NP", FieldType.NUMERIC, 6);
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Dd_Np = pnd_Ia_Trn_Call_Parms__R_Field_55.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Dd_Np", 
            "#I-IA-ISSUE-DTE-DD-NP", FieldType.NUMERIC, 2);
        pnd_Ia_Trn_Call_Parms_Pnd_I_Da_Contract_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Da_Contract_Np", "#I-DA-CONTRACT-NP", 
            FieldType.STRING, 8);
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Contract_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Contract_Np", "#I-IA-CONTRACT-NP", 
            FieldType.STRING, 8);
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Amount_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Amount_Np", "#I-IA-AMOUNT-NP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np", "#O-SETTLE-AMT-NP", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np", "#O-FOUND-NP", FieldType.STRING, 
            1);
        pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np = pnd_Ia_Trn_Call_Parms.newFieldInGroup("pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np", "#O-25-OVER-SW-NP", 
            FieldType.STRING, 1);
        pnd_Hve_60_Trans = localVariables.newFieldInRecord("pnd_Hve_60_Trans", "#HVE-60-TRANS", FieldType.STRING, 1);
        pnd_Sve_Tran_Code = localVariables.newFieldArrayInRecord("pnd_Sve_Tran_Code", "#SVE-TRAN-CODE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            4));
        pnd_Sve_Ia_Contract = localVariables.newFieldInRecord("pnd_Sve_Ia_Contract", "#SVE-IA-CONTRACT", FieldType.STRING, 8);
        pnd_Sve_Ia_Contract_Payee = localVariables.newFieldInRecord("pnd_Sve_Ia_Contract_Payee", "#SVE-IA-CONTRACT-PAYEE", FieldType.NUMERIC, 2);
        pnd_Sve_Origin = localVariables.newFieldInRecord("pnd_Sve_Origin", "#SVE-ORIGIN", FieldType.NUMERIC, 2);
        pnd_Sve_Optn_Code = localVariables.newFieldInRecord("pnd_Sve_Optn_Code", "#SVE-OPTN-CODE", FieldType.NUMERIC, 2);
        pnd_Sve_Iss_Delete_R = localVariables.newFieldInRecord("pnd_Sve_Iss_Delete_R", "#SVE-ISS-DELETE-R", FieldType.STRING, 1);
        pnd_Sve_Iss_Pend_Pay = localVariables.newFieldInRecord("pnd_Sve_Iss_Pend_Pay", "#SVE-ISS-PEND-PAY", FieldType.STRING, 1);
        pnd_Sve_Iss_Last_Trans_Date = localVariables.newFieldInRecord("pnd_Sve_Iss_Last_Trans_Date", "#SVE-ISS-LAST-TRANS-DATE", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Sve_Chk_Date = localVariables.newFieldInRecord("pnd_Sve_Chk_Date", "#SVE-CHK-DATE", FieldType.NUMERIC, 6);
        pnd_Sve_Issu_Dte_Dd = localVariables.newFieldInRecord("pnd_Sve_Issu_Dte_Dd", "#SVE-ISSU-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Sve_Prev_Chk_Date = localVariables.newFieldInRecord("pnd_Sve_Prev_Chk_Date", "#SVE-PREV-CHK-DATE", FieldType.NUMERIC, 6);

        pnd_Sve_Prev_Chk_Date__R_Field_56 = localVariables.newGroupInRecord("pnd_Sve_Prev_Chk_Date__R_Field_56", "REDEFINE", pnd_Sve_Prev_Chk_Date);
        pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyy = pnd_Sve_Prev_Chk_Date__R_Field_56.newFieldInGroup("pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyy", 
            "#SVE-PREV-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm = pnd_Sve_Prev_Chk_Date__R_Field_56.newFieldInGroup("pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm", 
            "#SVE-PREV-CHECK-MM", FieldType.NUMERIC, 2);

        pnd_Sve_Prev_Chk_Date__R_Field_57 = localVariables.newGroupInRecord("pnd_Sve_Prev_Chk_Date__R_Field_57", "REDEFINE", pnd_Sve_Prev_Chk_Date);
        pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyymm = pnd_Sve_Prev_Chk_Date__R_Field_57.newFieldInGroup("pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyymm", 
            "#SVE-PREV-CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Sve_Open_Value = localVariables.newFieldInRecord("pnd_Sve_Open_Value", "#SVE-OPEN-VALUE", FieldType.NUMERIC, 11, 2);
        pnd_W_Conv_Trans_Date = localVariables.newFieldInRecord("pnd_W_Conv_Trans_Date", "#W-CONV-TRANS-DATE", FieldType.NUMERIC, 6);

        pnd_W_Conv_Trans_Date__R_Field_58 = localVariables.newGroupInRecord("pnd_W_Conv_Trans_Date__R_Field_58", "REDEFINE", pnd_W_Conv_Trans_Date);
        pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy = pnd_W_Conv_Trans_Date__R_Field_58.newFieldInGroup("pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy", 
            "#W-CONV-TRANS-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm = pnd_W_Conv_Trans_Date__R_Field_58.newFieldInGroup("pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm", 
            "#W-CONV-TRANS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Sve_Rec5 = localVariables.newFieldArrayInRecord("pnd_Sve_Rec5", "#SVE-REC5", FieldType.STRING, 100, new DbsArrayController(1, 36));
        pnd_Sve_Mode = localVariables.newFieldInRecord("pnd_Sve_Mode", "#SVE-MODE", FieldType.NUMERIC, 3);
        pnd_W_Bypass = localVariables.newFieldInRecord("pnd_W_Bypass", "#W-BYPASS", FieldType.STRING, 6);
        pnd_W_Write_Qtrly_Sw = localVariables.newFieldInRecord("pnd_W_Write_Qtrly_Sw", "#W-WRITE-QTRLY-SW", FieldType.STRING, 1);
        pnd_W_Rewrite_Sw = localVariables.newFieldInRecord("pnd_W_Rewrite_Sw", "#W-REWRITE-SW", FieldType.STRING, 1);
        pnd_W_Pmt_Due_Sw = localVariables.newFieldInRecord("pnd_W_Pmt_Due_Sw", "#W-PMT-DUE-SW", FieldType.STRING, 1);
        pnd_W_New_Issue = localVariables.newFieldInRecord("pnd_W_New_Issue", "#W-NEW-ISSUE", FieldType.STRING, 1);
        pnd_W_Manual_Issue = localVariables.newFieldInRecord("pnd_W_Manual_Issue", "#W-MANUAL-ISSUE", FieldType.BOOLEAN, 1);
        pnd_W_Withdrawal = localVariables.newFieldInRecord("pnd_W_Withdrawal", "#W-WITHDRAWAL", FieldType.STRING, 1);
        pnd_W_Fin_Pay_Sw = localVariables.newFieldInRecord("pnd_W_Fin_Pay_Sw", "#W-FIN-PAY-SW", FieldType.STRING, 1);
        pnd_W_Death_Sw = localVariables.newFieldInRecord("pnd_W_Death_Sw", "#W-DEATH-SW", FieldType.STRING, 1);
        pnd_W_Retro_Sw = localVariables.newFieldInRecord("pnd_W_Retro_Sw", "#W-RETRO-SW", FieldType.STRING, 1);
        pnd_W_Est = localVariables.newFieldInRecord("pnd_W_Est", "#W-EST", FieldType.NUMERIC, 12, 2);
        pnd_W_Payee = localVariables.newFieldInRecord("pnd_W_Payee", "#W-PAYEE", FieldType.NUMERIC, 2);
        pnd_W_Iss_Mode_Mm = localVariables.newFieldInRecord("pnd_W_Iss_Mode_Mm", "#W-ISS-MODE-MM", FieldType.NUMERIC, 2);
        pnd_W_Close_Val_Sw = localVariables.newFieldInRecord("pnd_W_Close_Val_Sw", "#W-CLOSE-VAL-SW", FieldType.STRING, 1);
        pnd_Ia_Contract9 = localVariables.newFieldInRecord("pnd_Ia_Contract9", "#IA-CONTRACT9", FieldType.STRING, 9);

        pnd_Summary_Report_Counters = localVariables.newGroupInRecord("pnd_Summary_Report_Counters", "#SUMMARY-REPORT-COUNTERS");
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue", "#TOT-OLD-ISSUE", 
            FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt", 
            "#TOT-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div", 
            "#TOT-OLD-ISSU-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issue = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issue", "#TOT-NEW-ISSUE", 
            FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt", 
            "#TOT-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div", 
            "#TOT-NEW-ISSU-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees", 
            "#TPA-BEN-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt", 
            "#TOT-BEN-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div", "#TOT-BEN-DIV", 
            FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees", 
            "#P&I-ORGN3-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt", 
            "#TOT-ORGN3-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div", "#TOT-ORGN3-DIV", 
            FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected", 
            "#IA-NEW-ISSU-NOT-SELECTED", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt", 
            "#TOT-NEW-NOT-SEL-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div", 
            "#TOT-NEW-NOT-SEL-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Family_Income_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Family_Income_Payees", 
            "#FAMILY-INCOME-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt", 
            "#TOT-FMLY-INC-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div", 
            "#TOT-FMLY-INC-DIV", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly", 
            "#INACTIVE-PAYEE-SENT-TO-QTRLY", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other", 
            "#INACTIVE-PAYEE-OTHER", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees", 
            "#TOT-INACTIVE-PAYEES", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt", 
            "#TPA-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt", 
            "#TPA-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt", 
            "#TPA-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt", 
            "#TPA-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value", 
            "#TPA-NEW-ISSU-COMM-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2", 
            "#TPA-NON-PREMIUM-AMT-T1-T2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt", 
            "#TPA-TO-CASH-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash", "#TPA-TO-CASH", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt", 
            "#TPA-TO-ALT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier", 
            "#TPA-TO-ALT-CARRIER", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt", 
            "#TPA-TO-TIAA-CREF-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt", 
            "#TPA-TO-TIAA-CREF-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt", 
            "#TPA-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8", 
            "#TPA-WITHDRAWAL-T8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt", 
            "#TPA-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt", 
            "#TPA-REWRITE-80-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt", 
            "#TPA-FLAG-N-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt", 
            "#TPA-OLD-CNTRCT-CNT", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt", 
            "#TPA-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt", 
            "#TPA-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt", 
            "#TPA-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value", 
            "#TPA-OLD-ISSU-COMM-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value", 
            "#TPA-OLD-ISSU-CLS-VALUE", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt", 
            "#IPRO-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt", 
            "#IPRO-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt", 
            "#IPRO-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt", 
            "#IPRO-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt", 
            "#IPRO-NEW-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt", 
            "#IPRO-OLD-ISSU-CNTRCT-CNT", FieldType.NUMERIC, 7);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt", 
            "#IPRO-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt", 
            "#IPRO-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt", 
            "#IPRO-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt", 
            "#IPRO-OLD-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2", 
            "#IPRO-NON-PREMIUM-AMT-I1-I2", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt", 
            "#IPRO-INTEREST-CRED-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred", 
            "#IPRO-INTEREST-CRED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt", "#IPRO-PMT-CNT", 
            FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt", "#IPRO-PMT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt", 
            "#IPRO-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8", 
            "#IPRO-WITHDRAWAL-I8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt", 
            "#IPRO-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_D_Flag_Amt", 
            "#IPRO-REWRITE-D-FLAG-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt", 
            "#P&I-NEW-ISSU-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt", 
            "#P&I-NEW-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt", 
            "#P&I-NEW-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt", 
            "#P&I-NEW-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt", 
            "#P&I-NEW-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt", 
            "#P&I-OLD-ISSU-CNTRCT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt", 
            "#P&I-OLD-ISSU-GUAR-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt", 
            "#P&I-OLD-ISSU-DIV-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt", 
            "#P&I-OLD-ISSU-TOT-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt", 
            "#P&I-OLD-ISSU-FIN-PMT", FieldType.NUMERIC, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1", 
            "#P&I-NON-PREMIUM-AMT-P1", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt", 
            "#P&I-INTEREST-CRED-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred", 
            "#P&I-INTEREST-CRED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt", 
            "#P&I-PMT-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt", 
            "#P&I-PMT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt", 
            "#P&I-WITHDRAWAL-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8 = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8", 
            "#P&I-WITHDRAWAL-P8", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt", 
            "#P&I-REWRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt", 
            "#P&I-REWRITE-D-FLAG-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt", "#TPA-SWEEP-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt", "#TPA-SWEEP-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt", 
            "#IPRO-SWEEP-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt = pnd_Summary_Report_Counters.newFieldInGroup("pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt", 
            "#IPRO-SWEEP-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cnt_Rec = localVariables.newFieldInRecord("pnd_Cnt_Rec", "#CNT-REC", FieldType.NUMERIC, 10);
        pnd_Frst_Time_Sw = localVariables.newFieldInRecord("pnd_Frst_Time_Sw", "#FRST-TIME-SW", FieldType.STRING, 1);
        pnd_Fnd_Tpa_For_B = localVariables.newFieldInRecord("pnd_Fnd_Tpa_For_B", "#FND-TPA-FOR-B", FieldType.STRING, 1);
        pnd_Tot_Amt = localVariables.newFieldInRecord("pnd_Tot_Amt", "#TOT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Rtex = localVariables.newFieldInRecord("pnd_Rtex", "#RTEX", FieldType.INTEGER, 2);
        pnd_Tot_New_Not_Sel = localVariables.newFieldInRecord("pnd_Tot_New_Not_Sel", "#TOT-NEW-NOT-SEL", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Ben_Not_Sel = localVariables.newFieldInRecord("pnd_Tot_Ben_Not_Sel", "#TOT-BEN-NOT-SEL", FieldType.NUMERIC, 13, 2);
        pnd_Call_Hist_Sw = localVariables.newFieldInRecord("pnd_Call_Hist_Sw", "#CALL-HIST-SW", FieldType.STRING, 1);
        pnd_W_Mode = localVariables.newFieldInRecord("pnd_W_Mode", "#W-MODE", FieldType.NUMERIC, 3);

        pnd_W_Mode__R_Field_59 = localVariables.newGroupInRecord("pnd_W_Mode__R_Field_59", "REDEFINE", pnd_W_Mode);
        pnd_W_Mode__Filler1 = pnd_W_Mode__R_Field_59.newFieldInGroup("pnd_W_Mode__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_W_Mode_Pnd_W_Mode_2 = pnd_W_Mode__R_Field_59.newFieldInGroup("pnd_W_Mode_Pnd_W_Mode_2", "#W-MODE-2", FieldType.NUMERIC, 2);
        pnd_Cntrct_Ppcn_Nbr_Parm = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr_Parm", "#CNTRCT-PPCN-NBR-PARM", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Cde_Parm = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Cde_Parm", "#CNTRCT-PAYEE-CDE-PARM", FieldType.NUMERIC, 2);
        pnd_Optn_Cde = localVariables.newFieldInRecord("pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Issue_Dte = localVariables.newFieldInRecord("pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Issue_Dte__R_Field_60 = localVariables.newGroupInRecord("pnd_Issue_Dte__R_Field_60", "REDEFINE", pnd_Issue_Dte);
        pnd_Issue_Dte__Filler2 = pnd_Issue_Dte__R_Field_60.newFieldInGroup("pnd_Issue_Dte__Filler2", "_FILLER2", FieldType.STRING, 4);
        pnd_Issue_Dte_Pnd_Issue_Mm = pnd_Issue_Dte__R_Field_60.newFieldInGroup("pnd_Issue_Dte_Pnd_Issue_Mm", "#ISSUE-MM", FieldType.STRING, 2);
        pnd_Pymnt_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sve_Contr_Type_Cd = localVariables.newFieldInRecord("pnd_Sve_Contr_Type_Cd", "#SVE-CONTR-TYPE-CD", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ia_Tpa.reset();
        vw_ia_Tpa_Fund.reset();

        localVariables.reset();
        pnd_W_Tia_Sw.setInitialValue(" ");
        pnd_W_Crf_Sw.setInitialValue(" ");
        pnd_W_Hex_High_Value.setInitialValue("H'FFFF'");
        pnd_W_Next_Due_Dte_Ccyymmdd.setInitialValue(1);
        pnd_W_Hold_Date_Ccyymmdd.setInitialValue(1);
        pnd_Pia3170_Call_Type_G.setInitialValue("G");
        pnd_Pia3170_Call_Type.setInitialValue("G");
        pnd_Dummy1.setInitialValue(" ");
        pnd_Dummy21.setInitialValue(" ");
        pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np.setInitialValue("N");
        pnd_Sve_Ia_Contract.setInitialValue("        ");
        pnd_Sve_Ia_Contract_Payee.setInitialValue(0);
        pnd_W_Write_Qtrly_Sw.setInitialValue(" ");
        pnd_W_Close_Val_Sw.setInitialValue("N");
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issue.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Family_Income_Payees.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Other.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_80_Amt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt.setInitialValue(0);
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt.setInitialValue(0);
        pnd_Frst_Time_Sw.setInitialValue("Y");
        pnd_Fnd_Tpa_For_B.setInitialValue("Y");
        pnd_W_Mode.setInitialValue(800);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iadp170() throws Exception
    {
        super("Iadp170");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IADP170", onError);
        setupReports();
        //*  082017
        //*  ADDED 6/03
        //*  ADDED 6/03
        pnd_W_Ia_Pin.reset();                                                                                                                                             //Natural: RESET #W-IA-PIN W-TAPE-OUT-B ( * ) W-IA-FUND-PCT-B-REC ( * ) W-IA-RATE-CDE ( * ) W-IA-RATE-PER-PAY ( * ) W-IA-RATE-PER-DIV ( * ) W-RATE-FINAL-PAY ( * ) W-IA-FUND-PCT-N-REC ( * ) W-IA-TPA-WRITTEN-B W-IA-TPA-WRITTEN-N W-IA-IPRO-WRITTEN-B W-IA-IPRO-WRITTEN-N W-IA-TPA-OPEN-VALUE W-IA-TPA-CLOSE-VALUE W-IA-TPA-NON-PREMIUM-AMOUNT W-IA-IPRO-OPEN-VALUE W-IA-IPRO-CLOSE-VALUE W-IA-IPRO-NON-PREMIUM-AMOUNT W-IA-TPA-PULLOUTS W-IA-IPRO-PULLOUTS W-IA-P&I-PULLOUTS W-IA-P&I-WRITTEN-B W-IA-P&I-WRITTEN-N W-IA-P&I-OPEN-VALUE W-IA-P&I-CLOSE-VALUE W-IA-P&I-NON-PREMIUM-AMOUNT #TPA-TO-ALT-CARRIER #TPA-TO-TIAA-CREF-AMT #P&I-NON-PREMIUM-AMT-P1 #P&I-INTEREST-CRED #P&I-PMT-AMT #P&I-WITHDRAWAL-P8 #P&I-REWRITE-D-FLAG-AMT #NAZ-PARM #SVE-ISS-PEND-PAY #CALL-HIST-SW #PYMNT-AMT
        w_Tape_Out_B.getValue("*").reset();
        w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec.getValue("*").reset();
        w_Tape_Out_B_W_Ia_Rate_Cde.getValue("*").reset();
        w_Tape_Out_B_W_Ia_Rate_Per_Pay.getValue("*").reset();
        w_Tape_Out_B_W_Ia_Rate_Per_Div.getValue("*").reset();
        w_Tape_Out_B_W_Rate_Final_Pay.getValue("*").reset();
        w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec.getValue("*").reset();
        pnd_W_Total_Record_W_Ia_Tpa_Written_B.reset();
        pnd_W_Total_Record_W_Ia_Tpa_Written_N.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Written_B.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Written_N.reset();
        pnd_W_Total_Record_W_Ia_Tpa_Open_Value.reset();
        pnd_W_Total_Record_W_Ia_Tpa_Close_Value.reset();
        pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Open_Value.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Close_Value.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount.reset();
        pnd_W_Total_Record_W_Ia_Tpa_Pullouts.reset();
        pnd_W_Total_Record_W_Ia_Ipro_Pullouts.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_B.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value.reset();
        pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount.reset();
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier.reset();
        pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt.reset();
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1.reset();
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred.reset();
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt.reset();
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8.reset();
        pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_D_Flag_Amt.reset();
        pnd_Naz_Parm.reset();
        pnd_Sve_Iss_Pend_Pay.reset();
        pnd_Call_Hist_Sw.reset();
        pnd_Pymnt_Amt.reset();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 66;//Natural: FORMAT ( 01 ) LS = 132 PS = 56;//Natural: FORMAT ( 02 ) LS = 132 PS = 56
        pnd_Ccyymmdd.setValue(Global.getDATN());                                                                                                                          //Natural: MOVE *DATN TO #CCYYMMDD
        pnd_Next_Ccyymmdd.setValue(Global.getDATN());                                                                                                                     //Natural: MOVE *DATN TO #NEXT-CCYYMMDD
        if (condition(pnd_Ccyymmdd_Pnd_Current_Mm.equals(12)))                                                                                                            //Natural: IF #CURRENT-MM = 12
        {
            pnd_Next_Ccyymmdd_Pnd_Next_Mm.setValue(1);                                                                                                                    //Natural: MOVE 1 TO #NEXT-MM
            pnd_Next_Ccyymmdd_Pnd_Next_Ccyy.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NEXT-CCYY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Next_Ccyymmdd_Pnd_Next_Mm.nadd(1);                                                                                                                        //Natural: ADD 1 TO #NEXT-MM
        }                                                                                                                                                                 //Natural: END-IF
        //*  >>> READ IA CHECK DATE
        //* **
        //*  IA CHECK DATE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #WORK1
        while (condition(getWorkFiles().read(1, pnd_Work1)))
        {
            //*  SUBTRACT 2 MONTHS CALC QTRLY CYCLE DATE 2 MONTHS BEHIND IA CHK-DATE
            pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm.setValue(pnd_Work1_Pnd_W_Check_Date);                                                                                     //Natural: MOVE #W-CHECK-DATE TO #W-TRANS-CCYYMM
            pnd_W_Trans_Date_Pnd_W_Trans_Dd.setValue(1);                                                                                                                  //Natural: MOVE 01 TO #W-TRANS-DD
            pnd_Sve_Chk_Date.setValue(pnd_Work1_Pnd_W_Check_Ccyymm);                                                                                                      //Natural: ASSIGN #SVE-CHK-DATE := #W-CHECK-CCYYMM
            if (condition(pnd_Work1_Pnd_W_Check_Mm.equals(1)))                                                                                                            //Natural: IF #W-CHECK-MM = 01
            {
                pnd_W_Trans_Date_Pnd_W_Trans_Mm.setValue(11);                                                                                                             //Natural: MOVE 11 TO #W-TRANS-MM
                pnd_W_Trans_Date_Pnd_W_Trans_Ccyy.nsubtract(1);                                                                                                           //Natural: SUBTRACT 1 FROM #W-TRANS-CCYY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Work1_Pnd_W_Check_Mm.equals(2)))                                                                                                        //Natural: IF #W-CHECK-MM = 02
                {
                    pnd_W_Trans_Date_Pnd_W_Trans_Mm.setValue(12);                                                                                                         //Natural: MOVE 12 TO #W-TRANS-MM
                    pnd_W_Trans_Date_Pnd_W_Trans_Ccyy.nsubtract(1);                                                                                                       //Natural: SUBTRACT 1 FROM #W-TRANS-CCYY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Trans_Date_Pnd_W_Trans_Mm.nsubtract(2);                                                                                                         //Natural: SUBTRACT 2 FROM #W-TRANS-MM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Mode_Pnd_W_Mode_2.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                            //Natural: ASSIGN #W-MODE-2 := #W-TRANS-MM
            //*  ADDED  FOLLOWING 9/00
            //*  FOLLOWING FOR MISSED NEW ISSUES INPUT OUT MONTH-END CYCLE
            pnd_Sve_Prev_Chk_Date.setValue(pnd_Work1_Pnd_W_Check_Date);                                                                                                   //Natural: MOVE #W-CHECK-DATE TO #SVE-PREV-CHK-DATE
            if (condition(pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm.equals(1)))                                                                                         //Natural: IF #SVE-PREV-CHECK-MM = 1
            {
                pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm.setValue(12);                                                                                                 //Natural: MOVE 12 TO #SVE-PREV-CHECK-MM
                pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Ccyy.nsubtract(1);                                                                                               //Natural: SUBTRACT 1 FROM #SVE-PREV-CHECK-CCYY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sve_Prev_Chk_Date_Pnd_Sve_Prev_Check_Mm.nsubtract(1);                                                                                                 //Natural: SUBTRACT 1 FROM #SVE-PREV-CHECK-MM
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 9/00
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* **
        //*  >>> MAIN LOGIC THIS IS DONE UNTIL END OF IA-MASTER
        //* **
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 03 IA-MAST-REC
        while (condition(getWorkFiles().read(3, ia_Mast_Rec)))
        {
            if (condition(pnd_Frst_Time_Sw.notEquals("Y")))                                                                                                               //Natural: IF #FRST-TIME-SW NE 'Y'
            {
                if (condition(ia_Mast_Rec_Ia_Prefix.equals(pnd_W_Hex_High_Value) || (ia_Mast_Rec_Ia_Rec_No.equals(10))))                                                  //Natural: IF IA-PREFIX = #W-HEX-HIGH-VALUE OR ( IA-REC-NO = 10 )
                {
                    if (condition(pnd_Sve_Iss_Delete_R.notEquals("9")))                                                                                                   //Natural: IF #SVE-ISS-DELETE-R NE '9'
                    {
                                                                                                                                                                          //Natural: PERFORM ACCUM-ACTIVE-CONTRACTS
                        sub_Accum_Active_Contracts();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Frst_Time_Sw.equals("Y")))                                                                                                                  //Natural: IF #FRST-TIME-SW = 'Y'
            {
                pnd_Frst_Time_Sw.setValue("N");                                                                                                                           //Natural: ASSIGN #FRST-TIME-SW := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Mast_Rec_Ia_Prefix.equals(pnd_W_Hex_High_Value)))                                                                                            //Natural: IF IA-PREFIX = #W-HEX-HIGH-VALUE
            {
                if (condition(pnd_W_Write_Qtrly_Sw.equals("Y")))                                                                                                          //Natural: IF #W-WRITE-QTRLY-SW = 'Y'
                {
                    if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25) || w_Tape_Out_B_W_Ia_Optn_Code.equals(27)))                                                      //Natural: IF W-IA-OPTN-CODE = 25 OR = 27
                    {
                                                                                                                                                                          //Natural: PERFORM IPRO-RTNE
                        sub_Ipro_Rtne();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28) || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))                                                  //Natural: IF W-IA-OPTN-CODE = 28 OR = 30
                        {
                                                                                                                                                                          //Natural: PERFORM TPA-RTNE
                            sub_Tpa_Rtne();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM OPT22-RTNE
                            sub_Opt22_Rtne();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*   ADDED 60 THRU 69 TO FOLLOWING        7/02
            if (condition(ia_Mast_Rec_Ia_Rec_No.equals(10) || ia_Mast_Rec_Ia_Rec_No.equals(20) || ia_Mast_Rec_Ia_Rec_No.equals(50) || ia_Mast_Rec_Ia_Rec_No.equals(51)    //Natural: IF ( IA-REC-NO = 10 OR = 20 OR = 50 OR = 51 OR = 52 OR = 53 OR = 54 OR = 55 OR = 56 OR = 57 OR = 58 OR = 59 OR = 60 OR = 61 OR = 62 OR = 63 OR = 64 OR = 65 OR = 66 OR = 67 OR = 68 OR = 69 OR = 70 )
                || ia_Mast_Rec_Ia_Rec_No.equals(52) || ia_Mast_Rec_Ia_Rec_No.equals(53) || ia_Mast_Rec_Ia_Rec_No.equals(54) || ia_Mast_Rec_Ia_Rec_No.equals(55) 
                || ia_Mast_Rec_Ia_Rec_No.equals(56) || ia_Mast_Rec_Ia_Rec_No.equals(57) || ia_Mast_Rec_Ia_Rec_No.equals(58) || ia_Mast_Rec_Ia_Rec_No.equals(59) 
                || ia_Mast_Rec_Ia_Rec_No.equals(60) || ia_Mast_Rec_Ia_Rec_No.equals(61) || ia_Mast_Rec_Ia_Rec_No.equals(62) || ia_Mast_Rec_Ia_Rec_No.equals(63) 
                || ia_Mast_Rec_Ia_Rec_No.equals(64) || ia_Mast_Rec_Ia_Rec_No.equals(65) || ia_Mast_Rec_Ia_Rec_No.equals(66) || ia_Mast_Rec_Ia_Rec_No.equals(67) 
                || ia_Mast_Rec_Ia_Rec_No.equals(68) || ia_Mast_Rec_Ia_Rec_No.equals(69) || ia_Mast_Rec_Ia_Rec_No.equals(70)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  >>>> PROCESS IA REC 10
            if (condition((ia_Mast_Rec_Ia_Rec_No.equals(10))))                                                                                                            //Natural: IF ( IA-REC-NO = 10 )
            {
                pnd_Sub1.reset();                                                                                                                                         //Natural: RESET #SUB1
                if (condition(pnd_W_Write_Qtrly_Sw.equals("Y")))                                                                                                          //Natural: IF #W-WRITE-QTRLY-SW = 'Y'
                {
                    if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25) || w_Tape_Out_B_W_Ia_Optn_Code.equals(27)))                                                      //Natural: IF W-IA-OPTN-CODE = 25 OR = 27
                    {
                                                                                                                                                                          //Natural: PERFORM IPRO-RTNE
                        sub_Ipro_Rtne();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28) || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))                                                  //Natural: IF W-IA-OPTN-CODE = 28 OR = 30
                        {
                                                                                                                                                                          //Natural: PERFORM TPA-RTNE
                            sub_Tpa_Rtne();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM OPT22-RTNE
                            sub_Opt22_Rtne();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Sve_Mode.setValue(ia_Mast_Rec_Iss_Mode);                                                                                                              //Natural: ASSIGN #SVE-MODE := ISS-MODE
                //*  08/05
                //*  ADDED 6/03
                //*  ADDED 6/03
                pnd_W_New_Issue.reset();                                                                                                                                  //Natural: RESET #W-NEW-ISSUE #W-MANUAL-ISSUE #W-WITHDRAWAL #W-DEATH-SW #W-RETRO-SW #W-WRITE-QTRLY-SW #W-FIN-PAY-SW #W-TAX-STOP-DATE #W-REWRITE-SW #SVE-ISS-PEND-PAY #SVE-ORIGIN #SVE-IA-CONTRACT #SVE-IA-CONTRACT-PAYEE #SVE-ISS-DELETE-R #SVE-OPTN-CODE #RTEX #SVE-ISSU-DTE-DD #PYMNT-AMT #CALL-HIST-SW
                pnd_W_Manual_Issue.reset();
                pnd_W_Withdrawal.reset();
                pnd_W_Death_Sw.reset();
                pnd_W_Retro_Sw.reset();
                pnd_W_Write_Qtrly_Sw.reset();
                pnd_W_Fin_Pay_Sw.reset();
                pnd_W_Tax_Stop_Date.reset();
                pnd_W_Rewrite_Sw.reset();
                pnd_Sve_Iss_Pend_Pay.reset();
                pnd_Sve_Origin.reset();
                pnd_Sve_Ia_Contract.reset();
                pnd_Sve_Ia_Contract_Payee.reset();
                pnd_Sve_Iss_Delete_R.reset();
                pnd_Sve_Optn_Code.reset();
                pnd_Rtex.reset();
                pnd_Sve_Issu_Dte_Dd.reset();
                pnd_Pymnt_Amt.reset();
                pnd_Call_Hist_Sw.reset();
                //*  08/05
                if (condition(ia_Mast_Rec_Iss_New_Issue_Flag.equals("N") || ia_Mast_Rec_Iss_New_Issue_Flag.equals("M")))                                                  //Natural: IF ISS-NEW-ISSUE-FLAG = 'N' OR = 'M'
                {
                    pnd_W_New_Issue.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #W-NEW-ISSUE
                    //*  MANUAL NEW ISSUE 08/05
                    //*  08/05
                    if (condition(ia_Mast_Rec_Iss_New_Issue_Flag.equals("M")))                                                                                            //Natural: IF ISS-NEW-ISSUE-FLAG = 'M'
                    {
                        pnd_W_Manual_Issue.setValue(true);                                                                                                                //Natural: ASSIGN #W-MANUAL-ISSUE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ADD NEW-ISSUES & OLD-ISSUES
                //*  ADDED FOLLOWING   3/00
                if (condition(ia_Mast_Rec_Iss_Delete_R.equals("9")))                                                                                                      //Natural: IF ISS-DELETE-R = '9'
                {
                    pnd_Sve_Iss_Delete_R.setValue(ia_Mast_Rec_Iss_Delete_R);                                                                                              //Natural: MOVE ISS-DELETE-R TO #SVE-ISS-DELETE-R
                    pnd_Summary_Report_Counters_Pnd_Tot_Inactive_Payees.nadd(1);                                                                                          //Natural: ADD 1 TO #TOT-INACTIVE-PAYEES
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ia_Mast_Rec_Ia_Payee_No.greater(1)))                                                                                                    //Natural: IF IA-PAYEE-NO > 01
                    {
                        pnd_Summary_Report_Counters_Pnd_Tpa_Ben_Payees.nadd(1);                                                                                           //Natural: ADD 1 TO #TPA-BEN-PAYEES
                        pnd_Sve_Ia_Contract_Payee.setValue(ia_Mast_Rec_Ia_Payee_No);                                                                                      //Natural: ASSIGN #SVE-IA-CONTRACT-PAYEE := IA-PAYEE-NO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ia_Mast_Rec_Iss_Option.equals(23)))                                                                                                 //Natural: IF ISS-OPTION = 23
                        {
                            pnd_Summary_Report_Counters_Pnd_Family_Income_Payees.nadd(1);                                                                                 //Natural: ADD 1 TO #FAMILY-INCOME-PAYEES
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ia_Mast_Rec_Iss_Option.equals(22) && ia_Mast_Rec_Iss_Origin.equals(3)))                                                         //Natural: IF ISS-OPTION = 22 AND ISS-ORIGIN = 03
                            {
                                pnd_Sve_Origin.setValue(ia_Mast_Rec_Iss_Origin);                                                                                          //Natural: ASSIGN #SVE-ORIGIN := ISS-ORIGIN
                                pnd_Summary_Report_Counters_Pnd_Pamp_I_Orgn3_Payees.nadd(1);                                                                              //Natural: ADD 1 TO #P&I-ORGN3-PAYEES
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_W_New_Issue.equals("Y")))                                                                                               //Natural: IF #W-NEW-ISSUE = 'Y'
                                {
                                    pnd_Summary_Report_Counters_Pnd_Tot_New_Issue.nadd(1);                                                                                //Natural: ADD 1 TO #TOT-NEW-ISSUE
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Summary_Report_Counters_Pnd_Tot_Old_Issue.nadd(1);                                                                                //Natural: ADD 1 TO #TOT-OLD-ISSUE
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD   3/00
                //*  SELECT
                if (condition(ia_Mast_Rec_Iss_Option.equals(28) || ia_Mast_Rec_Iss_Option.equals(30) || ia_Mast_Rec_Iss_Option.equals(25) || ia_Mast_Rec_Iss_Option.equals(27)  //Natural: IF ( ISS-OPTION = 28 OR = 30 OR = 25 OR = 27 OR = 22 )
                    || ia_Mast_Rec_Iss_Option.equals(22)))
                {
                    pnd_W_Bypass.reset();                                                                                                                                 //Natural: RESET #W-BYPASS
                    //*                                  /* ADDED 2/06 FOR FULLY DRAWN DOWN TPA
                    //*  & IPRO CONTRACTS DUE TO CONVERSION
                    if (condition(ia_Mast_Rec_Iss_Contr_Type_Cd.equals("F") && (ia_Mast_Rec_Iss_Last_Trans_Date.lessOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)          //Natural: IF ISS-CONTR-TYPE-CD = 'F' AND ( ISS-LAST-TRANS-DATE LE #W-TRANS-CCYYMM OR ( ISS-MODE NE #W-MODE AND ISS-MODE NE 100 ) )
                        || (ia_Mast_Rec_Iss_Mode.notEquals(pnd_W_Mode) && ia_Mast_Rec_Iss_Mode.notEquals(100)))))
                    {
                        //*  REMOVE COMMENT BEFORE PRODUCTION
                        pnd_W_Bypass.setValue("BYPASS");                                                                                                                  //Natural: MOVE 'BYPASS' TO #W-BYPASS
                        pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                               //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                    }                                                                                                                                                     //Natural: END-IF
                    //*      IF ISS-CONTR-TYPE-CD  NE 'P'  /* TEST ONLY  2/27/06
                    //*        MOVE 'BYPASS' TO #W-BYPASS  /* REMOVE CODE AFTER TEST. THIS IS
                    //*        #W-WRITE-QTRLY-SW := ' '    /* TO CREATE AN AUGMENTED FILE OF
                    //*      END-IF                        /* ALL PARTIAL DRAW DOWNS ONLY.
                    //*      IF ISS-CONTR-TYPE-CD  = 'P' /* & IPRO CONTRACTS DUE TO CONVERSION
                    //*         AND (ISS-LAST-TRANS-DATE LE #W-TRANS-CCYYMM) /* TEST ONLY
                    //*         OR (ISS-MODE NE #W-MODE AND ISS-MODE NE 100))
                    //*        MOVE 'BYPASS' TO #W-BYPASS
                    //*        #W-WRITE-QTRLY-SW := ' '
                    //*      END-IF
                    //*      IF IA-PAYEE-NO > 01        /* BYPASS ALL BENEFICIARIES
                    //*        MOVE 'BYPASS' TO #W-BYPASS
                    //*        #W-WRITE-QTRLY-SW := ' '
                    //*      END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BYPASS IF NOT TPA,IPRO OR P&I
                    pnd_W_Bypass.setValue("BYPASS");                                                                                                                      //Natural: MOVE 'BYPASS' TO #W-BYPASS
                    pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                                   //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Mast_Rec_Ia_Rec_No.equals(10)))                                                                                                              //Natural: IF IA-REC-NO = 10
            {
                if (condition(ia_Mast_Rec_Iss_Option.equals(22) && ia_Mast_Rec_Iss_Origin.equals(3)))                                                                     //Natural: IF ISS-OPTION = 22 AND ISS-ORIGIN = 03
                {
                    //*  BYPASS ORGN 3 (INSURANCE) P&I
                    pnd_W_Bypass.setValue("BYPASS");                                                                                                                      //Natural: MOVE 'BYPASS' TO #W-BYPASS
                    pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                                   //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Mast_Rec_Ia_Rec_No.equals(10)))                                                                                                              //Natural: IF IA-REC-NO = 10
            {
                pnd_W_Payee.setValue(ia_Mast_Rec_Ia_Payee_No);                                                                                                            //Natural: MOVE IA-PAYEE-NO TO #W-PAYEE
                pnd_W_Iss_Mode_Mm.setValue(ia_Mast_Rec_Iss_Mode_Mm);                                                                                                      //Natural: MOVE ISS-MODE-MM TO #W-ISS-MODE-MM
                pnd_Master_Rec_Linkage_Pnd_Rec1.setValue(ia_Mast_Rec);                                                                                                    //Natural: MOVE IA-MAST-REC TO #REC1
                //*  082017
                //*  ADDED 6/03 CALL TO READ HISTORY
                w_Tape_Out_B.getValue("*").reset();                                                                                                                       //Natural: RESET W-TAPE-OUT-B ( * ) #W-IA-PIN W-IA-FUND-PCT-B-REC ( * ) W-IA-RATE-CDE ( * ) W-IA-RATE-PER-PAY ( * ) W-IA-RATE-PER-DIV ( * ) W-RATE-FINAL-PAY ( * ) W-IA-FUND-PCT-N-REC ( * ) IA-TPA #W-TOTAL-PAYMENT-AMOUNT #W-TOTAL-TPA-PAYMENT-AMOUNT #W-TOTAL-TRANSFER-AMOUNT #W-TOTAL-GUARPYMT-AMOUNT #W-TOTAL-GUARDIVD-AMOUNT #W-TOTAL-GUARDIVD-PREV #W-TOT-FIN-PMT-AMT
                pnd_W_Ia_Pin.reset();
                w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec.getValue("*").reset();
                w_Tape_Out_B_W_Ia_Rate_Cde.getValue("*").reset();
                w_Tape_Out_B_W_Ia_Rate_Per_Pay.getValue("*").reset();
                w_Tape_Out_B_W_Ia_Rate_Per_Div.getValue("*").reset();
                w_Tape_Out_B_W_Rate_Final_Pay.getValue("*").reset();
                w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec.getValue("*").reset();
                vw_ia_Tpa.reset();
                pnd_W_Total_Payment_Amount.reset();
                pnd_W_Total_Tpa_Payment_Amount.reset();
                pnd_W_Total_Transfer_Amount.reset();
                pnd_W_Total_Guarpymt_Amount.reset();
                pnd_W_Total_Guardivd_Amount.reset();
                pnd_W_Total_Guardivd_Prev.reset();
                pnd_W_Tot_Fin_Pmt_Amt.reset();
                pnd_Call_Hist_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #CALL-HIST-SW := 'Y'
                if (condition(pnd_W_Bypass.notEquals("BYPASS")))                                                                                                          //Natural: IF #W-BYPASS NE 'BYPASS'
                {
                    pnd_W_Write_Qtrly_Sw.setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #W-WRITE-QTRLY-SW
                }                                                                                                                                                         //Natural: END-IF
                w_Tape_Out_B_W_Ia_Record_Type_Cd_B.setValue("B");                                                                                                         //Natural: MOVE 'B' TO W-IA-RECORD-TYPE-CD-B
                //*  JB01
                w_Tape_Out_B_W_Ia_Contract_Number.setValue(ia_Mast_Rec_Ia_Contract_No);                                                                                   //Natural: MOVE IA-CONTRACT-NO TO W-IA-CONTRACT-NUMBER #I-IA-CONTRACT-NAZ #IA-CONTRACT9
                pnd_Naz_Parm_Pnd_I_Ia_Contract_Naz.setValue(ia_Mast_Rec_Ia_Contract_No);
                pnd_Ia_Contract9.setValue(ia_Mast_Rec_Ia_Contract_No);
                //*  JB01
                setValueToSubstring(ia_Mast_Rec_Ia_Payee_No,w_Tape_Out_B_W_Ia_Contract_Number,9,2);                                                                       //Natural: MOVE IA-PAYEE-NO TO SUBSTRING ( W-IA-CONTRACT-NUMBER,9,2 )
                w_Tape_Out_B_W_Ia_Issu_Coll_Code.setValue(ia_Mast_Rec_Iss_College_Code);                                                                                  //Natural: MOVE ISS-COLLEGE-CODE TO W-IA-ISSU-COLL-CODE
                w_Tape_Out_B_W_Ia_Num_Of_Prod.setValue(1);                                                                                                                //Natural: MOVE 01 TO W-IA-NUM-OF-PROD
                w_Tape_Out_B_W_Ia_Product_Type_Cd.setValue("A");                                                                                                          //Natural: MOVE 'A' TO W-IA-PRODUCT-TYPE-CD
                w_Tape_Out_B_W_Ia_Optn_Code.setValue(ia_Mast_Rec_Iss_Option);                                                                                             //Natural: MOVE ISS-OPTION TO W-IA-OPTN-CODE
                pnd_Sve_Optn_Code.setValue(ia_Mast_Rec_Iss_Option);                                                                                                       //Natural: MOVE ISS-OPTION TO #SVE-OPTN-CODE
                w_Tape_Out_B_W_Ia_Mode.setValue(ia_Mast_Rec_Iss_Mode);                                                                                                    //Natural: MOVE ISS-MODE TO W-IA-MODE
                //* ADDED 9/00
                w_Tape_Out_B_W_Ia_Issue_Dte.setValue(ia_Mast_Rec_Iss_Iss_Date);                                                                                           //Natural: MOVE ISS-ISS-DATE TO W-IA-ISSUE-DTE
                //* 10/00
                w_Tape_Out_B_W_Ia_Final_Per_Pay_Date.setValue(ia_Mast_Rec_Iss_Final_Per_Pay_Date);                                                                        //Natural: MOVE ISS-FINAL-PER-PAY-DATE TO W-IA-FINAL-PER-PAY-DATE
                //*  ADDED 1/02
                pnd_Sve_Issu_Dte_Dd.setValue(ia_Mast_Rec_Iss_Dte_Dd);                                                                                                     //Natural: MOVE ISS-DTE-DD TO #SVE-ISSU-DTE-DD
                //*  FOR FULL DRAW DOWN
                pnd_Sve_Contr_Type_Cd.setValue(ia_Mast_Rec_Iss_Contr_Type_Cd);                                                                                            //Natural: MOVE ISS-CONTR-TYPE-CD TO #SVE-CONTR-TYPE-CD
                //*                                                 /* 02/06
                //*  ADDED FOLLOWING IF STMNT. 9/00
                //*  ADDED 9/00
                if (condition(ia_Mast_Rec_Iss_Pend_Date.lessOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                //Natural: IF ISS-PEND-DATE LE #W-TRANS-CCYYMM
                {
                    w_Tape_Out_B_W_Ia_Pend_Code.setValue(ia_Mast_Rec_Iss_Pend_Pay);                                                                                       //Natural: MOVE ISS-PEND-PAY TO W-IA-PEND-CODE
                    pnd_Sve_Iss_Pend_Pay.setValue(ia_Mast_Rec_Iss_Pend_Pay);                                                                                              //Natural: MOVE ISS-PEND-PAY TO #SVE-ISS-PEND-PAY
                    //* ADDED TO CORRECT BLANK PEND 5/01
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sve_Iss_Pend_Pay.setValue("0");                                                                                                                   //Natural: ASSIGN #SVE-ISS-PEND-PAY := '0'
                }                                                                                                                                                         //Natural: END-IF
                //*  COLLEGE OWNED CONTRACTS
                if (condition(ia_Mast_Rec_Ia_Contract_No.equals("IC081073") || ia_Mast_Rec_Ia_Contract_No.equals("ID629698") || ia_Mast_Rec_Ia_Contract_No.equals("ID681491")  //Natural: IF IA-CONTRACT-NO = 'IC081073' OR = 'ID629698' OR = 'ID681491' OR = 'ID681509' OR = 'ID726932' OR = 'ID743408' OR = 'ID775517'
                    || ia_Mast_Rec_Ia_Contract_No.equals("ID681509") || ia_Mast_Rec_Ia_Contract_No.equals("ID726932") || ia_Mast_Rec_Ia_Contract_No.equals("ID743408") 
                    || ia_Mast_Rec_Ia_Contract_No.equals("ID775517")))
                {
                    w_Tape_Out_B_W_Ia_College_Owned_Code.setValue("C");                                                                                                   //Natural: MOVE 'C' TO W-IA-COLLEGE-OWNED-CODE
                    w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("10");                                                                                                         //Natural: MOVE '10' TO W-IA-PULL-OUT-CD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    w_Tape_Out_B_W_Ia_College_Owned_Code.setValue("0");                                                                                                   //Natural: MOVE '0' TO W-IA-COLLEGE-OWNED-CODE
                }                                                                                                                                                         //Natural: END-IF
                w_Tape_Out_B_W_Currency_Cd.setValue(1);                                                                                                                   //Natural: MOVE 1 TO W-CURRENCY-CD
                pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                          //Natural: MOVE #W-TRANS-CCYY TO #W-NEXT-DUE-DTE-CCYY
                pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Mm.setValue(ia_Mast_Rec_Iss_Mode_Mm);                                                                      //Natural: MOVE ISS-MODE-MM TO #W-NEXT-DUE-DTE-MM
                pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Dd.setValue(1);                                                                                            //Natural: MOVE 01 TO #W-NEXT-DUE-DTE-DD
                pnd_W_Iss_1st_Pay_Due_Date.setValue(ia_Mast_Rec_Iss_1st_Pay_Due_Date);                                                                                    //Natural: ASSIGN #W-ISS-1ST-PAY-DUE-DATE := ISS-1ST-PAY-DUE-DATE
                //*   >>> LOGIC FOR TPA CONTRACTS
                if (condition(ia_Mast_Rec_Iss_Option.equals(28) || ia_Mast_Rec_Iss_Option.equals(30)))                                                                    //Natural: IF ( ISS-OPTION = 28 OR = 30 )
                {
                    pnd_Sve_Tpa_Due_Date.setValue(ia_Mast_Rec_Iss_1st_Pay_Due_Date);                                                                                      //Natural: ASSIGN #SVE-TPA-DUE-DATE := ISS-1ST-PAY-DUE-DATE
                    if (condition(ia_Mast_Rec_Iss_1st_Pay_Due_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                         //Natural: IF ISS-1ST-PAY-DUE-DATE > #W-TRANS-CCYYMM
                    {
                        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(ia_Mast_Rec_Iss_1st_Pay_Due_Date);                                                       //Natural: MOVE ISS-1ST-PAY-DUE-DATE TO #W-HOLD-DATE-CCYYMM
                        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                          //Natural: MOVE 01 TO #W-HOLD-DATE-DD
                        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy);                                                                //Natural: MOVE #W-HOLD-DATE-CCYY TO #W-WRK-CCYY
                        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm);                                                                    //Natural: MOVE #W-HOLD-DATE-MM TO #W-WRK-MM
                        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd);                                                                    //Natural: MOVE #W-HOLD-DATE-DD TO #W-WRK-DD
                        w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(pnd_W_Date);                                                                                     //Natural: MOVE #W-DATE TO W-IA-TPA-NEXT-PAYMENT-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm.lessOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                            //Natural: IF #W-NEXT-DUE-DTE-CCYYMM LE #W-TRANS-CCYYMM
                        {
                            pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm.nadd(100);                                                                              //Natural: ADD 100 TO #W-NEXT-DUE-DTE-CCYYMM
                            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm);                              //Natural: MOVE #W-NEXT-DUE-DTE-CCYYMM TO #W-HOLD-DATE-CCYYMM
                            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                      //Natural: MOVE 01 TO #W-HOLD-DATE-DD
                            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy);                                                            //Natural: MOVE #W-HOLD-DATE-CCYY TO #W-WRK-CCYY
                            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm);                                                                //Natural: MOVE #W-HOLD-DATE-MM TO #W-WRK-MM
                            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd);                                                                //Natural: MOVE #W-HOLD-DATE-DD TO #W-WRK-DD
                            w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(pnd_W_Date);                                                                                 //Natural: MOVE #W-DATE TO W-IA-TPA-NEXT-PAYMENT-DATE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm);                              //Natural: MOVE #W-NEXT-DUE-DTE-CCYYMM TO #W-HOLD-DATE-CCYYMM
                            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                      //Natural: MOVE 01 TO #W-HOLD-DATE-DD
                            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy);                                                            //Natural: MOVE #W-HOLD-DATE-CCYY TO #W-WRK-CCYY
                            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm);                                                                //Natural: MOVE #W-HOLD-DATE-MM TO #W-WRK-MM
                            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd);                                                                //Natural: MOVE #W-HOLD-DATE-DD TO #W-WRK-DD
                            w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(pnd_W_Date);                                                                                 //Natural: MOVE #W-DATE TO W-IA-TPA-NEXT-PAYMENT-DATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Iss_Iss_Date.setValue(ia_Mast_Rec_Iss_Iss_Date);                                                                                                    //Natural: MOVE ISS-ISS-DATE TO #W-ISS-ISS-DATE
                //*   ADDED FOLLOWING 1/02  FOR CALL TO ACTUARY TPA  OPEN VALUE
                pnd_W_Iss_Dte_Dd.setValue(ia_Mast_Rec_Iss_Dte_Dd);                                                                                                        //Natural: MOVE ISS-DTE-DD TO #W-ISS-DTE-DD
                pnd_W_Iss_1st_Pay_Paid_Date.setValue(ia_Mast_Rec_Iss_1st_Pay_Paid_Date);                                                                                  //Natural: MOVE ISS-1ST-PAY-PAID-DATE TO #W-ISS-1ST-PAY-PAID-DATE
                pnd_W_Iss_Final_Per_Pay_Date.setValue(ia_Mast_Rec_Iss_Final_Per_Pay_Date);                                                                                //Natural: MOVE ISS-FINAL-PER-PAY-DATE TO #W-ISS-FINAL-PER-PAY-DATE
                //*  CALCULATE NBR OF PMTS
                if (condition(ia_Mast_Rec_Iss_Option.equals(28) || ia_Mast_Rec_Iss_Option.equals(30)))                                                                    //Natural: IF ( ISS-OPTION = 28 OR = 30 )
                {
                    //*  LEFT FOR TPA CONTRACTS
                    w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.compute(new ComputeParameters(false, w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments), (pnd_W_Iss_Final_Per_Pay_Date_Pnd_W_Iss_Final_Per_Pay_Date_Ccyy).subtract((pnd_W_Trans_Date_Pnd_W_Trans_Ccyy))); //Natural: COMPUTE W-IA-TPA-REM-NBR-PAYMENTS = ( #W-ISS-FINAL-PER-PAY-DATE-CCYY ) - ( #W-TRANS-CCYY )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.equals(getZero()) && ia_Mast_Rec_Iss_Mode_Mm.lessOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Mm)))          //Natural: IF W-IA-TPA-REM-NBR-PAYMENTS = 0 AND ISS-MODE-MM LE #W-TRANS-MM
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.less(ia_Mast_Rec_Iss_Mode_Mm)))                                                                         //Natural: IF #W-TRANS-MM LT ISS-MODE-MM
                    {
                        w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.nadd(1);                                                                                                   //Natural: ADD 1 TO W-IA-TPA-REM-NBR-PAYMENTS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ia_Mast_Rec_Ia_Rec_No.equals(10)))                                                                                                          //Natural: IF IA-REC-NO = 10
                {
                    if (condition(ia_Mast_Rec_Iss_Pend_Pay.equals("A")))                                                                                                  //Natural: IF ISS-PEND-PAY = 'A'
                    {
                        pnd_W_Bypass.setValue("BYPASS");                                                                                                                  //Natural: MOVE 'BYPASS' TO #W-BYPASS
                        pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                               //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  082015 - START
                //*    RESET #W-CORE-SUPER
                //*    MOVE 'I'                           TO #W-CORE-IA-IND
                //*    MOVE IA-CONTRACT-NO                TO #W-CORE-IA-CONTRACT-NO
                pnd_Cntrct_Payee_Pnd_Cntrct_Nbr.setValue(ia_Mast_Rec_Ia_Contract_No);                                                                                     //Natural: ASSIGN #CNTRCT-NBR := IA-CONTRACT-NO
                pnd_Cntrct_Payee_Pnd_Payee_Cde.setValue(ia_Mast_Rec_Ia_Payee_No);                                                                                         //Natural: ASSIGN #PAYEE-CDE := IA-PAYEE-NO
                //*  082015 - END
                if (condition(pnd_W_Bypass.notEquals("BYPASS")))                                                                                                          //Natural: IF #W-BYPASS NE 'BYPASS'
                {
                                                                                                                                                                          //Natural: PERFORM READ-COR-FILE
                    sub_Read_Cor_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  >>> BYPASS IPRO CONTRACT PAYEE GREATER THAN 01
                //*    IF (ISS-OPTION = 25 OR = 27)
                //*      IF IA-PAYEE-NO GT 01
                //*        MOVE 'BYPASS' TO #W-BYPASS
                //*        #W-WRITE-QTRLY-SW := ' '
                //*        ESCAPE TOP
                //*      END-IF
                //*    END-IF
                //*  >>> BYPASS TPA & P&I CONTRACTS WITH NO PAYMENTS REMAINING
                if (condition(! (ia_Mast_Rec_Iss_Option.equals(25) || ia_Mast_Rec_Iss_Option.equals(27))))                                                                //Natural: IF NOT ( ISS-OPTION = 25 OR = 27 )
                {
                    if (condition(pnd_W_Iss_Final_Per_Pay_Date.less(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                //Natural: IF #W-ISS-FINAL-PER-PAY-DATE LT #W-TRANS-CCYYMM
                    {
                        pnd_W_Bypass.setValue("BYPASS");                                                                                                                  //Natural: MOVE 'BYPASS' TO #W-BYPASS
                        pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                               //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*         HANDLE FINAL-PER-PAYMENT
                //*         CREATE CONVERSION TRANS FOR P&I & PAYMENT TRANS
                //*  --- FINAL PAYMENT DATE FOR CONTRACT
                if (condition(! (ia_Mast_Rec_Iss_Option.equals(25) || ia_Mast_Rec_Iss_Option.equals(27))))                                                                //Natural: IF NOT ( ISS-OPTION = 25 OR = 27 )
                {
                    if (condition(pnd_W_Iss_Final_Per_Pay_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                              //Natural: IF #W-ISS-FINAL-PER-PAY-DATE = #W-TRANS-CCYYMM
                    {
                        pnd_W_Fin_Pay_Sw.setValue("Y");                                                                                                                   //Natural: ASSIGN #W-FIN-PAY-SW := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  --- RETRO
                if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                               //Natural: IF #W-NEW-ISSUE = 'Y'
                {
                    if (condition(ia_Mast_Rec_Iss_Iss_Date.less(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                    //Natural: IF ISS-ISS-DATE < #W-TRANS-CCYYMM
                    {
                        pnd_W_Retro_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #W-RETRO-SW
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*        BYPASS NEW-ISSUES WITH ISSUE-DATE
                //*        GREATER THAN QRTLY CYCLE DATE
                //*        BYPASS UNTIL ISSUE-DATE = W-TRANS-DATE
                //*  --- NEW ISSUE BYPASS
                if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                               //Natural: IF #W-NEW-ISSUE = 'Y'
                {
                    if (condition(ia_Mast_Rec_Iss_Iss_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                 //Natural: IF ISS-ISS-DATE > #W-TRANS-CCYYMM
                    {
                        pnd_W_Bypass.setValue("BYPASS");                                                                                                                  //Natural: MOVE 'BYPASS' TO #W-BYPASS
                        //*  3/00
                        pnd_Summary_Report_Counters_Pnd_Ia_New_Issu_Not_Selected.nadd(1);                                                                                 //Natural: ADD 1 TO #IA-NEW-ISSU-NOT-SELECTED
                        pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                               //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ia_Mast_Rec_Iss_Iss_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                      //Natural: IF ISS-ISS-DATE = #W-TRANS-CCYYMM
                {
                    pnd_W_New_Issue.setValue("Y");                                                                                                                        //Natural: ASSIGN #W-NEW-ISSUE := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ia_Mast_Rec_Iss_Rewrite.equals(1) && ia_Mast_Rec_Iss_Last_Trans_Date.greaterOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)                    //Natural: IF ISS-REWRITE = 1 AND ISS-LAST-TRANS-DATE GE #W-TRANS-CCYYMM AND ISS-DELETE-R NE '9'
                    && ia_Mast_Rec_Iss_Delete_R.notEquals("9")))
                {
                    pnd_W_Rewrite_Sw.setValue("Y");                                                                                                                       //Natural: ASSIGN #W-REWRITE-SW := 'Y'
                    if (condition(ia_Mast_Rec_Iss_Option.equals(28) || ia_Mast_Rec_Iss_Option.equals(30)))                                                                //Natural: IF ( ISS-OPTION = 28 OR = 30 )
                    {
                        //*  TPA CONTRACTS
                        w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("80");                                                                                                     //Natural: MOVE '80' TO W-IA-PULL-OUT-CD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  IPRO & P&I CONTRACTS
                        w_Tape_Out_B_W_Ia_Status_Flag.setValue("D");                                                                                                      //Natural: MOVE 'D' TO W-IA-STATUS-FLAG
                        //*  FOR REWRITES
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Sve_Iss_Last_Trans_Date.setValue(ia_Mast_Rec_Iss_Last_Trans_Date);                                                                                    //Natural: MOVE ISS-LAST-TRANS-DATE TO #SVE-ISS-LAST-TRANS-DATE
                pnd_W_Conv_Trans_Date.setValue(ia_Mast_Rec_Iss_Last_Trans_Date);                                                                                          //Natural: MOVE ISS-LAST-TRANS-DATE TO #W-CONV-TRANS-DATE
                pnd_Sve_Iss_Delete_R.setValue(ia_Mast_Rec_Iss_Delete_R);                                                                                                  //Natural: MOVE ISS-DELETE-R TO #SVE-ISS-DELETE-R
                //*  ADDED 4/02
                pnd_Sve_Tran_Code.getValue("*").setValue(ia_Mast_Rec_Iss_Tran_Code.getValue("*"));                                                                        //Natural: MOVE ISS-TRAN-CODE ( * ) TO #SVE-TRAN-CODE ( * )
                //*   HANDLE DELETED RECS FOR TPA COMMMUTED VALUE
                if (condition(pnd_Sve_Iss_Delete_R.equals("9")))                                                                                                          //Natural: IF #SVE-ISS-DELETE-R = '9'
                {
                    ia_Mast_Rec_Iss_Delete_R.setValue("1");                                                                                                               //Natural: MOVE '1' TO ISS-DELETE-R
                    pnd_Master_Rec_Linkage_Pnd_Rec1.setValue(ia_Mast_Rec);                                                                                                //Natural: MOVE IA-MAST-REC TO #REC1
                    ia_Mast_Rec_Iss_Delete_R.setValue("9");                                                                                                               //Natural: MOVE '9' TO ISS-DELETE-R
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  >>>> PROCESS IA REC 20
            if (condition(ia_Mast_Rec_Ia_Rec_No.equals(20)))                                                                                                              //Natural: IF IA-REC-NO = 20
            {
                pnd_Master_Rec_Linkage_Pnd_Rec2.setValue(ia_Mast_Rec);                                                                                                    //Natural: MOVE IA-MAST-REC TO #REC2
                if (condition(ia_Mast_Rec_Ann_1st_Ann_Sex.equals(1)))                                                                                                     //Natural: IF ANN-1ST-ANN-SEX = 1
                {
                    w_Tape_Out_B_W_Ia_Sex.setValue("M");                                                                                                                  //Natural: MOVE 'M' TO W-IA-SEX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    w_Tape_Out_B_W_Ia_Sex.setValue("F");                                                                                                                  //Natural: MOVE 'F' TO W-IA-SEX
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ia_Mast_Rec_Ia_Rec_No.equals(20)))                                                                                                          //Natural: IF IA-REC-NO = 20
                {
                    w_Tape_Out_B_W_Ia_Dob.setValue(ia_Mast_Rec_Ann_1st_Ann_Dob);                                                                                          //Natural: MOVE ANN-1ST-ANN-DOB TO W-IA-DOB
                    //*  ADDED 10/00
                    if (condition(ia_Mast_Rec_Ia_Payee_No.equals(1) && pnd_Sve_Iss_Last_Trans_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                          //Natural: IF IA-PAYEE-NO = 1 AND #SVE-ISS-LAST-TRANS-DATE = #W-TRANS-CCYYMM
                    {
                        w_Tape_Out_B_W_Ia_Dod.setValue(ia_Mast_Rec_Ann_1st_Ann_Dod);                                                                                      //Natural: MOVE ANN-1ST-ANN-DOD TO W-IA-DOD
                        //*      ELSE                                       /* COMMENTED 10/99
                        //*        MOVE ANN-BENEF-DOD      TO  W-IA-DOD     /* COMMENTED 10/99
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  THIS LOGIC TO HANDLE CENTURY ERROR
                if (condition(ia_Mast_Rec_Ia_Rec_No.equals(20)))                                                                                                          //Natural: IF IA-REC-NO = 20
                {
                    //*  IN DATE-OF-DEATH
                    if (condition(w_Tape_Out_B_W_Ia_Dod_Cc.equals(20)))                                                                                                   //Natural: IF W-IA-DOD-CC = 20
                    {
                        if (condition(w_Tape_Out_B_W_Ia_Dod_Yy.greater(60)))                                                                                              //Natural: IF W-IA-DOD-YY GT 60
                        {
                            w_Tape_Out_B_W_Ia_Dod_Cc.setValue(19);                                                                                                        //Natural: ASSIGN W-IA-DOD-CC := 19
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                w_Tape_Out_B_W_Ia_Latest_Da_Tiaa.setValue(ia_Mast_Rec_Ann_Da_Contr_Num);                                                                                  //Natural: MOVE ANN-DA-CONTR-NUM TO W-IA-LATEST-DA-TIAA
                w_Tape_Out_B_W_Ia_Paymt_Destination.setValue(ia_Mast_Rec_Ann_Curr_Tpa_Disb);                                                                              //Natural: MOVE ANN-CURR-TPA-DISB TO W-IA-PAYMT-DESTINATION
                if (condition((ia_Mast_Rec_Ann_Curr_Tpa_Disb.equals(" ") && (w_Tape_Out_B_W_Ia_Optn_Code.equals(25) || w_Tape_Out_B_W_Ia_Optn_Code.equals(27)))))         //Natural: IF ANN-CURR-TPA-DISB = ' ' AND W-IA-OPTN-CODE = 25 OR = 27
                {
                    w_Tape_Out_B_W_Ia_Paymt_Destination.setValue("CASH");                                                                                                 //Natural: MOVE 'CASH' TO W-IA-PAYMT-DESTINATION
                }                                                                                                                                                         //Natural: END-IF
                //*    DELETED RECORD CHECK FOR WITHDRAWAL & DEATH PROCESSING
                if (condition(pnd_Sve_Iss_Delete_R.equals("9")))                                                                                                          //Natural: IF #SVE-ISS-DELETE-R = '9'
                {
                                                                                                                                                                          //Natural: PERFORM INACTIVE-CONTRACT
                    sub_Inactive_Contract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_W_Bypass.equals("BYPASS")))                                                                                                         //Natural: IF #W-BYPASS = 'BYPASS'
                    {
                        pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                               //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  >>> PROCESS IA REC 50'S rate payment info
            //*  CHANGED FROM < 60 TO < 70      7/02
            if (condition((ia_Mast_Rec_Ia_Rec_No.greater(49)) && (ia_Mast_Rec_Ia_Rec_No.less(70))))                                                                       //Natural: IF ( IA-REC-NO > 49 ) AND ( IA-REC-NO < 70 )
            {
                //*  CHANGED ABOVE TO FOLLOWING 6/03 ONLY 50 REC-TYPE RATE RECORDS
                //*  IF (IA-REC-NO = 50)
                //*  TO ALLOW UP TO 90 RATES
                pnd_Sub1.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #SUB1
                pnd_Master_Rec_Linkage_Pnd_Rec5.getValue(pnd_Sub1).setValue(ia_Mast_Rec);                                                                                 //Natural: MOVE IA-MAST-REC TO #REC5 ( #SUB1 )
                pnd_W_Total_Guardivd_Prev.nadd(ia_Mast_Rec_Rate_Tot_Old_Tiaa_Div);                                                                                        //Natural: COMPUTE #W-TOTAL-GUARDIVD-PREV = #W-TOTAL-GUARDIVD-PREV + RATE-TOT-OLD-TIAA-DIV
                pnd_W_Total_Guardivd_Amount.compute(new ComputeParameters(false, pnd_W_Total_Guardivd_Amount), pnd_W_Total_Guardivd_Amount.add(ia_Mast_Rec_Rate_Per_Div_1).add(ia_Mast_Rec_Rate_Per_Div_2).add(ia_Mast_Rec_Rate_Per_Div_3)); //Natural: COMPUTE #W-TOTAL-GUARDIVD-AMOUNT = #W-TOTAL-GUARDIVD-AMOUNT + RATE-PER-DIV-1 + RATE-PER-DIV-2 + RATE-PER-DIV-3
                //*  ADDED FOLLOWING 10/00 FOR ABR
                pnd_Rtex.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RTEX
                //*  ADDED TO PASS NO MORE THAN 60 RATES 6/03
                if (condition(pnd_Rtex.less(61)))                                                                                                                         //Natural: IF #RTEX LT 61
                {
                    w_Tape_Out_B_W_Ia_Rate_Cde.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Rate_1);                                                                      //Natural: ASSIGN W-IA-RATE-CDE ( #RTEX ) := RATE-RATE-1
                    w_Tape_Out_B_W_Ia_Rate_Per_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Pay_1);                                                               //Natural: ASSIGN W-IA-RATE-PER-PAY ( #RTEX ) := RATE-PER-PAY-1
                    w_Tape_Out_B_W_Ia_Rate_Per_Div.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Div_1);                                                               //Natural: ASSIGN W-IA-RATE-PER-DIV ( #RTEX ) := RATE-PER-DIV-1
                    w_Tape_Out_B_W_Rate_Final_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Final_Pay_1);                                                              //Natural: ASSIGN W-RATE-FINAL-PAY ( #RTEX ) := RATE-FINAL-PAY-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rtex.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RTEX
                //*  ADDED TO PASS NO MORE THAN 60 RATES 6/03
                if (condition(pnd_Rtex.less(61)))                                                                                                                         //Natural: IF #RTEX LT 61
                {
                    w_Tape_Out_B_W_Ia_Rate_Cde.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Rate_2);                                                                      //Natural: ASSIGN W-IA-RATE-CDE ( #RTEX ) := RATE-RATE-2
                    w_Tape_Out_B_W_Ia_Rate_Per_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Pay_2);                                                               //Natural: ASSIGN W-IA-RATE-PER-PAY ( #RTEX ) := RATE-PER-PAY-2
                    w_Tape_Out_B_W_Ia_Rate_Per_Div.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Div_2);                                                               //Natural: ASSIGN W-IA-RATE-PER-DIV ( #RTEX ) := RATE-PER-DIV-2
                    w_Tape_Out_B_W_Rate_Final_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Final_Pay_2);                                                              //Natural: ASSIGN W-RATE-FINAL-PAY ( #RTEX ) := RATE-FINAL-PAY-2
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rtex.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #RTEX
                //*  ADDED TO PASS NO MORE THAN 60 RATES 6/03
                if (condition(pnd_Rtex.less(61)))                                                                                                                         //Natural: IF #RTEX LT 61
                {
                    w_Tape_Out_B_W_Ia_Rate_Cde.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Rate_3);                                                                      //Natural: ASSIGN W-IA-RATE-CDE ( #RTEX ) := RATE-RATE-3
                    w_Tape_Out_B_W_Ia_Rate_Per_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Pay_3);                                                               //Natural: ASSIGN W-IA-RATE-PER-PAY ( #RTEX ) := RATE-PER-PAY-3
                    w_Tape_Out_B_W_Ia_Rate_Per_Div.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Per_Div_3);                                                               //Natural: ASSIGN W-IA-RATE-PER-DIV ( #RTEX ) := RATE-PER-DIV-3
                    w_Tape_Out_B_W_Rate_Final_Pay.getValue(pnd_Rtex).setValue(ia_Mast_Rec_Rate_Final_Pay_3);                                                              //Natural: ASSIGN W-RATE-FINAL-PAY ( #RTEX ) := RATE-FINAL-PAY-3
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD 10/00 FOR ABR
                pnd_W_Total_Guarpymt_Amount.compute(new ComputeParameters(false, pnd_W_Total_Guarpymt_Amount), pnd_W_Total_Guarpymt_Amount.add(ia_Mast_Rec_Rate_Per_Pay_1).add(ia_Mast_Rec_Rate_Per_Pay_2).add(ia_Mast_Rec_Rate_Per_Pay_3)); //Natural: COMPUTE #W-TOTAL-GUARPYMT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + RATE-PER-PAY-1 + RATE-PER-PAY-2 + RATE-PER-PAY-3
                //*  >>>>  USE 50 REC  TRANS CODE FOR 42
                //*  PARTIAL WITHDRAWAL TRANS 42
                //*  CHANGE IN PMT AMT
                if (condition(ia_Mast_Rec_Rate_Trans_Code.equals(42) && ia_Mast_Rec_Rate_Trans_Date.greaterOrEqual(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)                   //Natural: IF RATE-TRANS-CODE = 042 AND RATE-TRANS-DATE GE #W-TRANS-CCYYMM AND ISS-DELETE-R NE '9'
                    && ia_Mast_Rec_Iss_Delete_R.notEquals("9")))
                {
                    pnd_W_Rewrite_Sw.setValue("Y");                                                                                                                       //Natural: ASSIGN #W-REWRITE-SW := 'Y'
                    if (condition(ia_Mast_Rec_Iss_Option.equals(28) || ia_Mast_Rec_Iss_Option.equals(30)))                                                                //Natural: IF ( ISS-OPTION = 28 OR = 30 )
                    {
                        //*  TPA CONTRACTS
                        w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("80");                                                                                                     //Natural: MOVE '80' TO W-IA-PULL-OUT-CD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  IPRO & P&I CONTRACTS
                        w_Tape_Out_B_W_Ia_Status_Flag.setValue("D");                                                                                                      //Natural: MOVE 'D' TO W-IA-STATUS-FLAG
                        //*  FOR REWRITES
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  >>>> FOLLOWING FOR DIVIDEND CHANGE BOUNDARY
                //*  >>>> WHICH OCCURS IN IA CHECK-DATE MONTH 4 FOR IPRO & TPA CONTRACTS
                //*  >>>> USING MAY IA MASTER TO REPORT MONTH 3 FOR QTRLY
                //*  >>>> USE OLD DIVIDEND AMOUNT FOR THIS MONTH
                if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25) || w_Tape_Out_B_W_Ia_Optn_Code.equals(27) || w_Tape_Out_B_W_Ia_Optn_Code.equals(28)                  //Natural: IF W-IA-OPTN-CODE = 25 OR = 27 OR = 28 OR = 30
                    || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))
                {
                    //*  DIVIDEND CHANGE IN APRIL USE OLD DIV-AMT
                    if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(3)))                                                                                             //Natural: IF #W-TRANS-MM = 03
                    {
                        //*  ADDED 6/03
                                                                                                                                                                          //Natural: PERFORM CALL-IAANHP02-FUND-HISTORY
                        sub_Call_Iaanhp02_Fund_History();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Pymnt_Amt.greater(getZero())))                                                                                                  //Natural: IF #PYMNT-AMT GT 0
                        {
                            pnd_W_Total_Payment_Amount.setValue(pnd_Pymnt_Amt);                                                                                           //Natural: ASSIGN #W-TOTAL-PAYMENT-AMOUNT := #PYMNT-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_W_Total_Guardivd_Prev.greater(getZero())))                                                                                  //Natural: IF #W-TOTAL-GUARDIVD-PREV GT 0
                            {
                                pnd_W_Total_Payment_Amount.compute(new ComputeParameters(false, pnd_W_Total_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Prev)); //Natural: COMPUTE #W-TOTAL-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-PREV
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_W_Total_Payment_Amount.compute(new ComputeParameters(false, pnd_W_Total_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Amount)); //Natural: COMPUTE #W-TOTAL-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-AMOUNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED FOLLOWING 6/03 USE CURR-DIV-AMT WHEN TRANS-MM NE 03
                if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25) || w_Tape_Out_B_W_Ia_Optn_Code.equals(27) || w_Tape_Out_B_W_Ia_Optn_Code.equals(28)                  //Natural: IF W-IA-OPTN-CODE = 25 OR = 27 OR = 28 OR = 30
                    || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))
                {
                    if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.notEquals(3)))                                                                                          //Natural: IF #W-TRANS-MM NE 03
                    {
                        pnd_W_Total_Payment_Amount.compute(new ComputeParameters(false, pnd_W_Total_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Amount)); //Natural: COMPUTE #W-TOTAL-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-AMOUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  DIVIDEND CHANGE JAN. USE OLD DIV-AMT
                if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(22)))                                                                                                    //Natural: IF W-IA-OPTN-CODE = 22
                {
                    //*  FOR P&I
                    if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(12)))                                                                                            //Natural: IF #W-TRANS-MM = 12
                    {
                        pnd_W_Total_Payment_Amount.compute(new ComputeParameters(false, pnd_W_Total_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Prev)); //Natural: COMPUTE #W-TOTAL-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-PREV
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_W_Total_Payment_Amount.compute(new ComputeParameters(false, pnd_W_Total_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Amount)); //Natural: COMPUTE #W-TOTAL-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-AMOUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  LEFT
                pnd_W_Tot_Fin_Pmt_Amt.compute(new ComputeParameters(false, pnd_W_Tot_Fin_Pmt_Amt), pnd_W_Tot_Fin_Pmt_Amt.add(ia_Mast_Rec_Rate_Final_Pay_1).add(ia_Mast_Rec_Rate_Final_Pay_2).add(ia_Mast_Rec_Rate_Final_Pay_3)); //Natural: COMPUTE #W-TOT-FIN-PMT-AMT = #W-TOT-FIN-PMT-AMT + RATE-FINAL-PAY-1 + RATE-FINAL-PAY-2 + RATE-FINAL-PAY-3
                w_Tape_Out_B_W_Ia_Next_Payment_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                               //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-NEXT-PAYMENT-AMOUNT
                //*  END OF REC TYPE 50 LOGIC
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Mast_Rec_Ia_Rec_No.equals(70) && ia_Mast_Rec_Ia_Sequence_No.equals(1)))                                                                      //Natural: IF IA-REC-NO = 70 AND IA-SEQUENCE-NO = 001
            {
                w_Tape_Out_B_W_Ia_Social_Security_No.setValue(ia_Mast_Rec_Us_Soc_Sec_No_Or_Tax_Id_Num);                                                                   //Natural: MOVE US-SOC-SEC-NO-OR-TAX-ID-NUM TO W-IA-SOCIAL-SECURITY-NO
                pnd_W_Tax_Stop_Date.setValue(ia_Mast_Rec_Us_Stop_Date);                                                                                                   //Natural: ASSIGN #W-TAX-STOP-DATE := US-STOP-DATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        pnd_W_Total_Record_W_Ia_Record_Type_Cd_X.setValue("X");                                                                                                           //Natural: MOVE 'X' TO W-IA-RECORD-TYPE-CD-X
        w_Tape_Out.getValue("*").setValue(pnd_W_Total_Record.getValue("*"));                                                                                              //Natural: MOVE #W-TOTAL-RECORD ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE TRAILER  RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM GET-LAST-DAY
        sub_Get_Last_Day();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE SUMMARY REPORT
        DbsUtil.callnat(Iadn170.class , getCurrentProcessState(), pnd_W_Date, pnd_Work1_Pnd_W_Check_Date, ia_Mast_Rec, pnd_W_Total_Record_Pnd_W_Total_Record_Parm,        //Natural: CALLNAT 'IADN170' #W-DATE #W-CHECK-DATE IA-MAST-REC #W-TOTAL-RECORD-PARM #SUMMARY-REPORT-COUNTERS
            pnd_Summary_Report_Counters);
        if (condition(Global.isEscape())) return;
        //*  ****************************************************************
        //*  DELETED RECORD PROCESS CHECK FOR WITHDRAWALS , DEATH & FIN-PMNT
        //*  ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INACTIVE-CONTRACT
        //*  3/29/17 - END
        //* *************************************************************
        //*  PROCESS TPA CONTRACTS GET OPENING & CLOSING COMMUTED VALUES
        //* *************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-RTNE
        //*  1) CALL MS MODULE FOR NEW ISSUES TO GET ACCUMULATED COMMUTED VALUE
        //*  2) IF NOT FOUND ON ADAM  CALL NON PREMIUM MODULE TO
        //*     GET ACCUMULATED VALUE FROM NON PREMIUM FILE
        //*  3) COMPARE ON FILE ADAM ACCUMULATED VALUE TO CALCULATED
        //*     OPENING COMMUTED VALUE RETURNED FROM ACTUARY MODULE FOR NEW ISSUES
        //*  #TPA-PH-UNQUE-ID-NBR := W-IA-PIN
        //*  NEW ROUTINE  1/96
        //*  *********************************************************
        //*   IF TPA DEST TIAA OR CREF (RINV)SET PART DATE TO PREV. MONTH
        //*  *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-TPA-PART-DATE
        //* * ADDED FOLLOWING 11/01 FOR NEW TPA
        //* ****************************************
        //*   CREATE TPA PMT TRANS
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TPA-PMT-TRANS
        //*                        ADDED RINV TO FOLLOWING 7/01
        //*                        TO RESOLVE WHEN NO TPA FND REC FOUND 7/01
        //*  04/06 GET THE OLD PAYMENT FROM THE BEFORE IMAGE RECORD
        //*  IF THERE ARE DRAWDOWN TRANSACTIONS FOR THE CYCLE AND PAYMENT IS DUE
        //*  END OF ADD 11/01
        //* ****************************************
        //*   ACCUMULATE TPA AMOUNTS
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-TPA-AMOUNTS
        //* ****************************
        //*  IPRO ROUTINE
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IPRO-RTNE
        //*  04/06 GET THE OLD FINAL PAYMENT FROM THE BEFORE IMAGE RECORD
        //*  IF THERE ARE DRAWDOWN TRANSACTIONS FOR THE CYCLE AND PAYMENT IS DUE
        //*  04/06 FOR PARTIAL OR FULL DRAW DOWN, GET AMOUNT FROM HISTORY
        //*  ADDED FOLLOWING ROUTINE TO CREATE I1 OR I2 TRANS 4/02
        //*        ADAM SWEEPS NEW PREMIUMS INTO CONTRACT TRANS CODE 60
        //* ***************************************
        //*  >>> CREATE I1 OT I2 IPRO N TRANSACTION
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: I1-I2-FOR-SWEEP-RTNE
        //*  END OF ADD  4/02
        //* ***************************************
        //*  >>> CREATE I3 IPRO N TRANSACTION
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: I3-RTNE
        //*  04/06  DO WE HAVE TO CHECK FOR PARTIAL/FULL DRAWDOWN INDICATOR HERE???
        //* *****************************************************
        //*  >>> CREATE IPRO TRANSACTIONS
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: I4-7-RTNE
        //*     ---- TOTAL OF ALL PER-PMT-AMT AND PER-DIV-AMT ON 50 RECS
        //*  04/06 -- DO WE HAVE TO CHECK FOR PARTIAL DRAWDOWN INDICATOR HERE????
        //* ****************************************
        //*   ACCUMULATE IPRO AMOUNTS
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IPRO-AMOUNTS
        //* ****************************************************************
        //*  >>>> ROUTINE TO HANDLE P&I(OPTION 22) CONTRACTS
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPT22-RTNE
        //* ********************************************************
        //*  >>> CHECK IF P&I PAYMENT IS DUE DETERMINED BY THE MODE
        //* ********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECIDE-IF-PAYMENT-IS-DUE
        //* *****************************************************
        //*  >>> CREATE P&I P4 TRANSACTIONS
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P4-RTNE
        //* ***************************************
        //*  >>> CREATE P3 P&I TRANSACTION
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P3-RTNE
        //* *******************************************
        //*  >>>> P&I WITHDRAWAL P8 TRANSACTION
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P8-RTNE
        //* ****************************************
        //*  >>> ACCUMULATE P&I AMOUNTS
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-OPT22-AMOUNTS
        //* ****************************************************************
        //*  CALL PIA3170 TO CALL ACTUARY MODULE AIA6236 FOR COMMUTED VALUES
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-PIA3170
        //*  END OF ADD 4/01
        //* ******************************************
        //*  ADAM NAZ  CALL
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-NAZ
        //* ******************************************
        //*  NON PREMIUM CALL
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IADN161-NON-PREMIUM
        //* *******************************************
        //*   ACCUMULATE B RECORD TRAILER TOTALS
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-B-REC-TOTALS
        //* *********************************************
        //*  ACCUMULATE (N) RECORD TOTALS
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-N-REC-TOTALS
        //* *********************************************
        //*  READ COR TO GET PIN-NBR FOR CONTRACT
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR-FILE
        //* ****************************************************
        //*  READ TPA TRANSFER FOR CURRENT PAY DEST
        //* ****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TPA-TRANSFER
        //* *#TPA-PH-UNQUE-ID-NBR := W-IA-PIN
        //*    IA-TPA.STTLMNT-DTE    = #W-TRANS-CCYYMM
        //*  ADDED FOLLOWING READ ROUTINE   1/96
        //*  ********************************************************
        //*  >>> READ TPA TRANSFER FUND RECORD FOR MIXED ALLOCATIONS
        //*  >>> FOR TRANSACTION RECORDS GET CURRENT FUND ALLOCATIONS
        //*  ********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TPA-FUND
        //* *********************************************************
        //*  READ TPA TRANSFER FUND RECORD FOR MIXED ALLOCATIONS
        //*       FOR (B)BALANCE RECORD GET NEXT PMT FUND ALLOCATIONS
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TPA-FUND-FOR-B
        //* *******************************************
        //*  IPRO WITHDRAWAL TRANSACTION
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: I8-RTNE
        //* ***************************************************
        //*  >>>>  ACCUMULATE PMT AMTS FOR ALL ACTIVE CONTRACTS
        //* ***************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-ACTIVE-CONTRACTS
        //*  END OF ADD  6/03
        //* *******************************************
        //*  >>>>  USED FOR SUMMARY REPORT
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LAST-DAY
        //*  ADDED FOLLOWING 6/03
        //* *******************************************
        //*  >>>>  READ IA FUND HISTORY RECORD
        //*  >>>>  GET OLD PMT AMOUNT
        //*  >>>>  P&I FOR JANUARY DIVIVEND CHANGE WHEN DOING DEC QTRLY
        //*  >>>>  TPA & IPRO FOR APRIL DIVIVEND CHANGE WHEN DOING MARCH QTRLY
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAANHP02-FUND-HISTORY
        //*   END OF ADD 6/03
        //*  ADDED FOLLOWING TO GET OPENING VALUE FOR OUT OF CYCLE TPA NEW ISSU9/00
        //*  QTRLY DETERMINES IF T1/T2 TRANS EXISTS IF NOT QTRLY GENERATES T9 TRANS
        //*           AND USES W-IA-OUT-OF-CYCLE-OPEN-VALUE FOR OPEN BALANCE
        //*  END OF COMMENTED 2/02
        //*  END OF ADD 9/00
        //* **********************************************************************
        //* *
        //* ** ADDED FOLLOWING CODE TO HANDLE TPA NEW-ISSUES INPUT OUT CYCLE 9/00
        //* *        TO GET OPENING VALUE FOR CONTRACT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-OUT-CYCLE-NEW-ISSUE
        //*  1) CALL ADAM MODULE(IADN171) READ ADAM RESULT FILE FOR NEW ISSUES
        //*      TO GET ACCUMULATED COMMUTED VALUE
        //*  2) IF NOT FOUND ON MS CALL NON PREMIUM MODULE TO GET ACCUMULATED VALUE
        //*      FROM NON PREMIUM FILE
        //*  3) COMPARE ON FILE ADAM ACCUMULATED VALUE TO CALCULATED
        //*     OPENING COMMUTED VALUE RETURNED FROM ACTUARY MODULE FOR NEW ISSUES
        //* * END OF ADD 9/00
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Inactive_Contract() throws Exception                                                                                                                 //Natural: INACTIVE-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //*  TO HANDLE CONVERSIONS (WITHDRAWALS)
        //*  #W-CONV-TRANS-DATE = ISS-LAST-TRANS-DATE
        //*  3/29/17 - START
        if (condition(pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm.equals(getZero()) || pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy.equals(getZero())))            //Natural: IF #W-CONV-TRANS-DATE-MM = 0 OR #W-CONV-TRANS-DATE-YYYY = 0
        {
            pnd_W_Bypass.setValue("BYPASS");                                                                                                                              //Natural: ASSIGN #W-BYPASS := 'BYPASS'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm.nsubtract(1);                                                                                                      //Natural: ASSIGN #W-CONV-TRANS-DATE-MM := #W-CONV-TRANS-DATE-MM - 1
        if (condition(pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm.equals(getZero())))                                                                                  //Natural: IF #W-CONV-TRANS-DATE-MM = 0
        {
            pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm.setValue(12);                                                                                                  //Natural: MOVE 12 TO #W-CONV-TRANS-DATE-MM
            pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy.nsubtract(1);                                                                                                //Natural: SUBTRACT 1 FROM #W-CONV-TRANS-DATE-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Conv_Trans_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) && ia_Mast_Rec_Ia_Payee_No.equals(1) && ia_Mast_Rec_Ann_1st_Ann_Dod.equals(getZero())  //Natural: IF #W-CONV-TRANS-DATE = #W-TRANS-CCYYMM AND IA-PAYEE-NO = 01 AND ANN-1ST-ANN-DOD = 0 AND ANN-BENEF-DOD = 0
            && ia_Mast_Rec_Ann_Benef_Dod.equals(getZero())))
        {
            pnd_W_Withdrawal.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #W-WITHDRAWAL
            w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("  ");                                                                                                                 //Natural: MOVE '  ' TO W-IA-PULL-OUT-CD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Conv_Trans_Date.less(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) && ia_Mast_Rec_Ia_Payee_No.equals(1) && ia_Mast_Rec_Ann_1st_Ann_Dod.equals(getZero())  //Natural: IF #W-CONV-TRANS-DATE LT #W-TRANS-CCYYMM AND IA-PAYEE-NO = 01 AND ANN-1ST-ANN-DOD = 0 AND ANN-BENEF-DOD = 0
            && ia_Mast_Rec_Ann_Benef_Dod.equals(getZero())))
        {
            pnd_W_Bypass.setValue("BYPASS");                                                                                                                              //Natural: MOVE 'BYPASS' TO #W-BYPASS
            pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                                           //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Tax_Stop_Date.notEquals(getZero())))                                                                                                          //Natural: IF #W-TAX-STOP-DATE NE 0
        {
            pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm.nadd(1);                                                                                                           //Natural: ADD 1 TO #W-TAX-STOP-DATE-MM
            if (condition(pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm.greater(12)))                                                                                        //Natural: IF #W-TAX-STOP-DATE-MM GT 12
            {
                pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Mm.setValue(1);                                                                                                   //Natural: MOVE 1 TO #W-TAX-STOP-DATE-MM
                pnd_W_Tax_Stop_Date_Pnd_W_Tax_Stop_Date_Ccyy.nadd(1);                                                                                                     //Natural: ADD 1 TO #W-TAX-STOP-DATE-CCYY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Tax_Stop_Date.greater(getZero()) && pnd_Sve_Iss_Last_Trans_Date.greater(pnd_W_Tax_Stop_Date)))                                                //Natural: IF #W-TAX-STOP-DATE GT 0 AND #SVE-ISS-LAST-TRANS-DATE GT #W-TAX-STOP-DATE
        {
            pnd_W_Bypass.setValue("BYPASS");                                                                                                                              //Natural: MOVE 'BYPASS' TO #W-BYPASS
            pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                                           //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Iss_Final_Per_Pay_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) && pnd_Sve_Iss_Last_Trans_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm))) //Natural: IF #W-ISS-FINAL-PER-PAY-DATE = #W-TRANS-CCYYMM AND #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BYPASS ANY RECORDS WHEN PROCESSING A INACTIVE PAYEE 01 CONTRACT
        //*  IF  #SVE-IA-CONTRACT  = W-IA-CONTRACT-NUMBER
        //*     AND
        //*   #SVE-IA-CONTRACT-PAYEE  =  01
        //*     AND
        //*     IA-PAYEE-NO GT 01
        //*   MOVE 'BYPASS' TO #W-BYPASS
        //*   #W-WRITE-QTRLY-SW := ' '
        //*   ESCAPE ROUTINE
        //*  END-IF
        //*  >>>HAVE CONVERTED CONTRACT TERMINATED DUE TO 20 OR 40 TRANS
        //*     ASSUME NORMAL PAYMENTS UP TO TRANS-DATE
        if (condition(pnd_Sve_Iss_Last_Trans_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                          //Natural: IF #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM
        {
            pnd_Sve_Ia_Contract_Payee.setValue(ia_Mast_Rec_Ia_Payee_No);                                                                                                  //Natural: ASSIGN #SVE-IA-CONTRACT-PAYEE := IA-PAYEE-NO
            pnd_Sve_Ia_Contract.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                              //Natural: ASSIGN #SVE-IA-CONTRACT := W-IA-CONTRACT-NUMBER
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_W_Iss_Final_Per_Pay_Date.less(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) || pnd_Sve_Iss_Last_Trans_Date.less(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm))) //Natural: IF #W-ISS-FINAL-PER-PAY-DATE < #W-TRANS-CCYYMM OR #SVE-ISS-LAST-TRANS-DATE LT #W-TRANS-CCYYMM
            {
                pnd_W_Bypass.setValue("BYPASS");                                                                                                                          //Natural: MOVE 'BYPASS' TO #W-BYPASS
                pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                                       //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition((ia_Mast_Rec_Ann_1st_Ann_Dod.greater(getZero()) && ia_Mast_Rec_Ia_Payee_No.equals(1)) && pnd_Sve_Iss_Last_Trans_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm))) //Natural: IF ( ANN-1ST-ANN-DOD > 0 AND IA-PAYEE-NO = 01 ) AND #SVE-ISS-LAST-TRANS-DATE = #W-TRANS-CCYYMM
                {
                    pnd_W_Death_Sw.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #W-DEATH-SW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((ia_Mast_Rec_Ann_Benef_Dod.greater(getZero()) && ia_Mast_Rec_Ia_Payee_No.greater(1)) && pnd_Sve_Iss_Last_Trans_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm))) //Natural: IF ( ANN-BENEF-DOD > 0 AND IA-PAYEE-NO GT 01 ) AND #SVE-ISS-LAST-TRANS-DATE = #W-TRANS-CCYYMM
                    {
                        pnd_W_Death_Sw.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #W-DEATH-SW
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Sve_Iss_Last_Trans_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) && ia_Mast_Rec_Ia_Payee_No.greater(1)))                     //Natural: IF #SVE-ISS-LAST-TRANS-DATE = #W-TRANS-CCYYMM AND IA-PAYEE-NO GT 01
                        {
                            pnd_W_Withdrawal.setValue("Y");                                                                                                               //Natural: MOVE 'Y' TO #W-WITHDRAWAL
                            w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("  ");                                                                                                 //Natural: MOVE '  ' TO W-IA-PULL-OUT-CD
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_W_Bypass.setValue("BYPASS");                                                                                                              //Natural: MOVE 'BYPASS' TO #W-BYPASS
                            pnd_W_Write_Qtrly_Sw.setValue(" ");                                                                                                           //Natural: ASSIGN #W-WRITE-QTRLY-SW := ' '
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tpa_Rtne() throws Exception                                                                                                                          //Natural: TPA-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED 4/02
        pnd_Hve_60_Trans.reset();                                                                                                                                         //Natural: RESET #HVE-60-TRANS
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm);                                                                    //Natural: MOVE #W-TRANS-CCYYMM TO #W-HOLD-DATE-CCYYMM
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                                          //Natural: MOVE 01 TO #W-HOLD-DATE-DD
        pnd_Pia3170_Comm_Date.setValue(pnd_W_Hold_Date);                                                                                                                  //Natural: MOVE #W-HOLD-DATE TO #PIA3170-COMM-DATE
        //*  ADDED 9/01
        if (condition(pnd_W_New_Issue.equals("Y") && pnd_W_Retro_Sw.notEquals("Y") && (w_Tape_Out_B_W_Ia_Issue_Dte.add(100).equals(pnd_W_Iss_1st_Pay_Due_Date))))         //Natural: IF #W-NEW-ISSUE = 'Y' AND #W-RETRO-SW NE 'Y' AND ( W-IA-ISSUE-DTE + 100 = #W-ISS-1ST-PAY-DUE-DATE )
        {
            //*  OLD TPA ISSUE
            w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.setValue(10);                                                                                                          //Natural: MOVE 10 TO W-IA-TPA-REM-NBR-PAYMENTS
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING 11/01
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            //*  MANUAL NEW ISSUE 08/05
            if (condition(pnd_W_Manual_Issue.equals(true)))                                                                                                               //Natural: IF #W-MANUAL-ISSUE = TRUE
            {
                //*  08/05
                pnd_Pia3170_Call_Type.setValue("M");                                                                                                                      //Natural: MOVE 'M' TO #PIA3170-CALL-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pia3170_Call_Type.setValue("G");                                                                                                                      //Natural: MOVE 'G' TO #PIA3170-CALL-TYPE
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_B_W_Ia_Open_Value.setValue(0);                                                                                                                     //Natural: MOVE 0 TO W-IA-OPEN-VALUE
            //*  GET OPENING VALUE
                                                                                                                                                                          //Natural: PERFORM CALL-PIA3170
            sub_Call_Pia3170();
            if (condition(Global.isEscape())) {return;}
            pnd_Sve_Open_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                                          //Natural: ASSIGN #SVE-OPEN-VALUE := #PIA3170-COMM-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pia3170_Call_Type.setValue("T");                                                                                                                          //Natural: MOVE 'T' TO #PIA3170-CALL-TYPE
            //*  GET OPENING VALUE
                                                                                                                                                                          //Natural: PERFORM CALL-PIA3170
            sub_Call_Pia3170();
            if (condition(Global.isEscape())) {return;}
            w_Tape_Out_B_W_Ia_Open_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                                //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-OPEN-VALUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR CALL TO MS
        pnd_Naz_Parm_Pnd_I_Ia_Amount_Naz.setValue(pnd_Pia3170_Comm_Value);                                                                                                //Natural: MOVE #PIA3170-COMM-VALUE TO #I-IA-AMOUNT-NAZ
        //*              FOR NEW-ISSUES  TO COMPARE CALC AMT TO AMT ON MS FILE
        //*  FOR CALL TO
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Amount_Np.setValue(pnd_Pia3170_Comm_Value);                                                                                        //Natural: MOVE #PIA3170-COMM-VALUE TO #I-IA-AMOUNT-NP
        //*                                 FOR NEW-ISSUES    NON PREMIUM (NP)
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            //*  MOVE #PIA3170-COMM-VALUE   TO W-IA-CLOSE-VALUE
            w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Pia3170_Comm_Value);                                                                                                //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-TRN-AMOUNT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Withdrawal.equals("Y") || pnd_W_Fin_Pay_Sw.equals("Y")))                                                                                      //Natural: IF #W-WITHDRAWAL = 'Y' OR #W-FIN-PAY-SW = 'Y'
        {
            w_Tape_Out_B_W_Ia_Close_Value.setValue(0);                                                                                                                    //Natural: MOVE 0 TO W-IA-CLOSE-VALUE
            w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.setValue(0);                                                                                                           //Natural: MOVE 0 TO W-IA-TPA-REM-NBR-PAYMENTS
            w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(0);                                                                                                          //Natural: MOVE 0 TO W-IA-TPA-NEXT-PAYMENT-DATE
            w_Tape_Out_B_W_Ia_Next_Payment_Amount.setValue(0);                                                                                                            //Natural: MOVE 0 TO W-IA-NEXT-PAYMENT-AMOUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm);                                                                //Natural: MOVE #W-TRANS-CCYYMM TO #W-HOLD-DATE-CCYYMM
            pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                                      //Natural: MOVE 01 TO #W-HOLD-DATE-DD
            if (condition(pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm.equals(12)))                                                                                        //Natural: IF #W-HOLD-DATE-MM = 12
            {
                pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm.setValue(1);                                                                                                  //Natural: MOVE 1 TO #W-HOLD-DATE-MM
                pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyy.nadd(1);                                                                                                    //Natural: ADD 1 TO #W-HOLD-DATE-CCYY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Mm.nadd(1);                                                                                                      //Natural: ADD 1 TO #W-HOLD-DATE-MM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pia3170_Comm_Date.setValue(pnd_W_Hold_Date);                                                                                                              //Natural: MOVE #W-HOLD-DATE TO #PIA3170-COMM-DATE
            pnd_W_Close_Val_Sw.setValue("Y");                                                                                                                             //Natural: ASSIGN #W-CLOSE-VAL-SW := 'Y'
            pnd_Pia3170_Call_Type.setValue("T");                                                                                                                          //Natural: MOVE 'T' TO #PIA3170-CALL-TYPE
            //*  GET CLOSING COMMUTED VALUE
                                                                                                                                                                          //Natural: PERFORM CALL-PIA3170
            sub_Call_Pia3170();
            if (condition(Global.isEscape())) {return;}
            w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                               //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-CLOSE-VALUE
            pnd_W_Close_Val_Sw.setValue("N");                                                                                                                             //Natural: ASSIGN #W-CLOSE-VAL-SW := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                                    //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
        //*  ADDED 4/02
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            pnd_Naz_Parm_Pnd_I_Read_Type.setValue("N");                                                                                                                   //Natural: ASSIGN #I-READ-TYPE := 'N'
            //*  CALL NAZ MODULE FOR ADAM NEW ISSUES OPEN VALUE
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
            sub_Call_Naz();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED FOLLOWING 1/02 USE ADAM TOT ACCUM AMT FOR  OPENING VALUE
            //*  OPENING VALUE FOR NEW ISSUE
            if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greater(getZero())))                                                                                          //Natural: IF #O-SETTLE-AMT-NAZ GT 0
            {
                //*  OPENING VALUE
                w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                                 //Natural: MOVE #O-SETTLE-AMT-NAZ TO W-IA-TRN-AMOUNT #SVE-OPEN-VALUE
                pnd_Sve_Open_Value.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD 1/02 USE ADAM AMT FOR  OPENING VALUE
            if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greaterOrEqual(pnd_Sve_Open_Value)))                                                                          //Natural: IF #O-SETTLE-AMT-NAZ GE #SVE-OPEN-VALUE
            {
                pnd_W_Est.compute(new ComputeParameters(false, pnd_W_Est), (pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.subtract(pnd_Sve_Open_Value)));                             //Natural: COMPUTE #W-EST = ( #O-SETTLE-AMT-NAZ - #SVE-OPEN-VALUE )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Est.compute(new ComputeParameters(false, pnd_W_Est), (pnd_Sve_Open_Value.subtract(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz)));                             //Natural: COMPUTE #W-EST = ( #SVE-OPEN-VALUE - #O-SETTLE-AMT-NAZ )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-TPA-AMOUNTS
        sub_Accum_Tpa_Amounts();
        if (condition(Global.isEscape())) {return;}
        //* * ADDED FOLLOWING 11/01 TO USE ADAM PMT-AMT FOR NEW-ISSUES 11/01
        if (condition(pnd_Naz_Parm_Pnd_O_Pmt_Amt.notEquals(getZero())))                                                                                                   //Natural: IF #O-PMT-AMT NOT = 0
        {
            //*  GUAR + DIV PMT
            pnd_W_Total_Payment_Amount.setValue(pnd_Naz_Parm_Pnd_O_Pmt_Amt);                                                                                              //Natural: MOVE #O-PMT-AMT TO #W-TOTAL-PAYMENT-AMOUNT
        }                                                                                                                                                                 //Natural: END-IF
        //* * END OF ADD 11/01 TO USE ADAM PMT-AMT FOR NEW-ISSUES 11/01
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            //*  +$5 DIFF OR NOT FOUND ON MS
            if (condition(pnd_W_Est.greater(new DbsDecimal("5.00")) || pnd_Naz_Parm_Pnd_O_Found_Naz.equals("N")))                                                         //Natural: IF #W-EST > 5.00 OR #O-FOUND-NAZ = 'N'
            {
                //*  NOT FND ON MS CK NON PREMIUM
                                                                                                                                                                          //Natural: PERFORM CALL-IADN161-NON-PREMIUM
                sub_Call_Iadn161_Non_Premium();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np.equals("N") || pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np.equals("Y")))                                 //Natural: IF #O-FOUND-NP = 'N' OR #O-25-OVER-SW-NP = 'Y'
                {
                    //*  $5 DIFF IN AMT FOR NEW-ISSU
                    w_Tape_Out_B_W_Ia_Status_Flag.setValue("N");                                                                                                          //Natural: MOVE 'N' TO W-IA-STATUS-FLAG
                    w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                       //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-CLOSE-VALUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  B-RECORD
                    w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                       //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-CLOSE-VALUE
                    //*  TRANS N-RECORD
                    w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np);                                                                     //Natural: MOVE #O-SETTLE-AMT-NP TO W-IA-TRN-AMOUNT
                    //*  OPENING VALUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_Pia3170_Comm_Value);                                                                                           //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-CLOSE-VALUE
                //*  TRAN N-RECORD
                w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                                 //Natural: MOVE #O-SETTLE-AMT-NAZ TO W-IA-TRN-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        pnd_Naz_Parm.reset();                                                                                                                                             //Natural: RESET #NAZ-PARM
        w_Tape_Out_B_W_Ia_Next_Payment_Amount.compute(new ComputeParameters(false, w_Tape_Out_B_W_Ia_Next_Payment_Amount), pnd_W_Total_Guarpymt_Amount.add(pnd_W_Total_Guardivd_Amount)); //Natural: COMPUTE W-IA-NEXT-PAYMENT-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + #W-TOTAL-GUARDIVD-AMOUNT
        if (condition(w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.equals(getZero())))                                                                                          //Natural: IF W-IA-TPA-REM-NBR-PAYMENTS = 0
        {
            w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(0);                                                                                                          //Natural: ASSIGN W-IA-TPA-NEXT-PAYMENT-DATE := 0
            w_Tape_Out_B_W_Ia_Next_Payment_Amount.setValue(0);                                                                                                            //Natural: ASSIGN W-IA-NEXT-PAYMENT-AMOUNT := 0
            w_Tape_Out_B_W_Ia_Close_Value.setValue(0);                                                                                                                    //Natural: ASSIGN W-IA-CLOSE-VALUE := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.notEquals(getZero())))                                                                                      //Natural: IF W-IA-TPA-NEXT-PAYMENT-DATE NE 0
        {
            pnd_W_Tia_Sw.reset();                                                                                                                                         //Natural: RESET #W-TIA-SW #W-CRF-SW
            pnd_W_Crf_Sw.reset();
            pnd_Sve_Dest.setValue(w_Tape_Out_B_W_Ia_Paymt_Destination);                                                                                                   //Natural: ASSIGN #SVE-DEST := W-IA-PAYMT-DESTINATION
            //*  082017
            pnd_W_Date.setValue(w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date);                                                                                                 //Natural: MOVE W-IA-TPA-NEXT-PAYMENT-DATE TO #W-DATE
            pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyy.setValue(pnd_W_Date_Pnd_W_Wrk_Ccyy);                                                                      //Natural: ASSIGN #W-NEXT-DUE-DTE-CCYY := #W-WRK-CCYY
            pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Mm.setValue(pnd_W_Date_Pnd_W_Wrk_Mm);                                                                          //Natural: ASSIGN #W-NEXT-DUE-DTE-MM := #W-WRK-MM
            pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr.setValue(pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7);                                                                                 //Natural: ASSIGN #TPA-PH-UNQUE-ID-NBR := #W-IA-PIN-7
            pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                             //Natural: ASSIGN #TPA-IA-CNTRCT-NBR := W-IA-CONTRACT-NUMBER
            pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte.setValue(pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm);                                                           //Natural: ASSIGN #TPA-STTLMNT-DTE := #W-NEXT-DUE-DTE-CCYYMM
            //*  ADDED 6/01
            if (condition(pnd_Sve_Dest.equals("RINV") || pnd_Sve_Dest.equals("CASH")))                                                                                    //Natural: IF #SVE-DEST = 'RINV' OR = 'CASH'
            {
                //*  GET B DEST FOR RINV     6/01
                                                                                                                                                                          //Natural: PERFORM READ-TPA-TRANSFER
                sub_Read_Tpa_Transfer();
                if (condition(Global.isEscape())) {return;}
                //*    PERFORM READ-TPA-FUND-FOR-B   /* COMMENTED 7/01  DTRA & RINV  6/01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ADDED EXTERNAL PMT 6/01
                if (condition(pnd_Sve_Dest.equals("DTRA")))                                                                                                               //Natural: IF #SVE-DEST = 'DTRA'
                {
                    //*  GET DEST FOR DTRA  6/01
                                                                                                                                                                          //Natural: PERFORM READ-TPA-TRANSFER
                    sub_Read_Tpa_Transfer();
                    if (condition(Global.isEscape())) {return;}
                    //*  ADDED 6/01
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED 6/01
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 7/01
            if (condition(pnd_Sve_Dest.equals("RINV")))                                                                                                                   //Natural: IF #SVE-DEST = 'RINV'
            {
                //*  GET B FUND ALLOCATION 7/01
                                                                                                                                                                          //Natural: PERFORM READ-TPA-FUND-FOR-B
                sub_Read_Tpa_Fund_For_B();
                if (condition(Global.isEscape())) {return;}
                //*  ADDED 7/01
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_B_W_Ia_Paymt_Destination.setValue(pnd_Sve_Dest);                                                                                                   //Natural: ASSIGN W-IA-PAYMT-DESTINATION := #SVE-DEST
        }                                                                                                                                                                 //Natural: END-IF
        //*  082017
        w_Tape_Out_B_W_Ia_Pin.setValue(pnd_W_Ia_Pin, MoveOption.LeftJustified);                                                                                           //Natural: MOVE LEFT #W-IA-PIN TO W-IA-PIN
        //*  WRITE TPA BALANCE (B) REC
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_B.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-B ( * ) TO W-TAPE-OUT ( * )
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
        pnd_W_Total_Record_W_Ia_Tpa_Written_B.nadd(1);                                                                                                                    //Natural: ADD 1 TO W-IA-TPA-WRITTEN-B
                                                                                                                                                                          //Natural: PERFORM ACCUM-B-REC-TOTALS
        sub_Accum_B_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W_New_Issue.equals("Y") && pnd_W_Payee.equals(1)))                                                                                              //Natural: IF #W-NEW-ISSUE = 'Y' AND #W-PAYEE = 01
        {
            w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                             //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
            if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28)))                                                                                                        //Natural: IF W-IA-OPTN-CODE = 28
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T1");                                                                                                          //Natural: MOVE 'T1' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T2");                                                                                                          //Natural: MOVE 'T2' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                            //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm);                                                                                   //Natural: MOVE #W-ISS-ISS-DATE-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy);                                                                               //Natural: MOVE #W-ISS-ISS-DATE-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                         //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
            w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                  //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
            if (condition(pnd_W_Retro_Sw.equals("Y")))                                                                                                                    //Natural: IF #W-RETRO-SW = 'Y'
            {
                pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                    //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
                pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                        //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
                pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                      //Natural: MOVE 01 TO #W-WRK-DD
                w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                              //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                             //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            //*  WRITE NEW ISSUE N (T1 OR T2) TRAN
            getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                     //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
            pnd_W_Total_Record_W_Ia_Tpa_Written_N.nadd(1);                                                                                                                //Natural: ADD 1 TO W-IA-TPA-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
            sub_Accum_N_Rec_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED FOLLOWING  4/02 FOR TRAN 60(SWEEP) GET NEW DA AMT INTO CONTRACT
            //*                   CREATE T1 TRANS FOR QTRLY TO REFLECT NEW MONEY
            pnd_Hve_60_Trans.reset();                                                                                                                                     //Natural: RESET #HVE-60-TRANS
            if (condition(pnd_Sve_Tran_Code.getValue("*").equals(60)))                                                                                                    //Natural: IF #SVE-TRAN-CODE ( * ) = 60
            {
                pnd_Hve_60_Trans.setValue("Y");                                                                                                                           //Natural: ASSIGN #HVE-60-TRANS := 'Y'
                pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                            //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
                pnd_Naz_Parm_Pnd_I_Read_Type.setValue("R");                                                                                                               //Natural: ASSIGN #I-READ-TYPE := 'R'
                //*  CALL NAZ MODULE FOR ADAM RESETTLEMENT TRN 60
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
                sub_Call_Naz();
                if (condition(Global.isEscape())) {return;}
                //*  OPENING VALUE FOR NEW ISSUE
                if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greater(getZero())))                                                                                      //Natural: IF #O-SETTLE-AMT-NAZ GT 0
                {
                    w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                             //Natural: MOVE #O-SETTLE-AMT-NAZ TO W-IA-TRN-AMOUNT
                    w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                        //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
                    //*  WRITE RESETTLE (SWEEP)
                    getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                             //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                    //*                                          N (T1 OR T2) TRAN
                    pnd_W_Total_Record_W_Ia_Tpa_Written_N.nadd(1);                                                                                                        //Natural: ADD 1 TO W-IA-TPA-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
                    sub_Accum_N_Rec_Totals();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF  ADD  4/02
            //*  ADDED 11/01 FOR NEW TPA DUE
                                                                                                                                                                          //Natural: PERFORM CREATE-TPA-PMT-TRANS
            sub_Create_Tpa_Pmt_Trans();
            if (condition(Global.isEscape())) {return;}
            //* ==> END NEW ISSUE LOGIC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CHANGED ABOVE TO FOLLOWING 6/03 TO HANDLE NEW PEND-CODE S & T FOR
            //*     SELF-REMITTER TPA CONTRACTS NOT TO BYPASS CREATING PMT TRANS
            //*  IF NOT(#SVE-ISS-PEND-PAY  = '0' OR= 'S' OR= 'T')
            if (condition(pnd_Sve_Iss_Pend_Pay.notEquals("0")))                                                                                                           //Natural: IF #SVE-ISS-PEND-PAY NE '0'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* ADDED #W-DEATH-SW 10/00
            if (condition(pnd_W_Withdrawal.equals("Y") || pnd_W_Death_Sw.equals("Y")))                                                                                    //Natural: IF #W-WITHDRAWAL = 'Y' OR #W-DEATH-SW = 'Y'
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T8");                                                                                                          //Natural: MOVE 'T8' TO W-IA-TRN-TRANS-CODE
                w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                        //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
                w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Trans_Date);                                                                                               //Natural: MOVE #W-TRANS-DATE TO W-IA-TRN-PART-DATE
                pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                    //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
                pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                        //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
                pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                      //Natural: MOVE 01 TO #W-WRK-DD
                w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                     //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
                w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                              //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
                w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                         //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
                w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Pia3170_Comm_Value);                                                                                            //Natural: MOVE #PIA3170-COMM-VALUE TO W-IA-TRN-AMOUNT
                w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));              //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
                w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                         //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
                w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                            //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
                //*                                      /* 02/06
                //*  WRITE A PAYMENT TRANSACTION FOR
                if (condition(pnd_Sve_Contr_Type_Cd.equals("F")))                                                                                                         //Natural: IF #SVE-CONTR-TYPE-CD = 'F'
                {
                    //*  FULL DRAWDOWN DUE TO CONVERSION
                                                                                                                                                                          //Natural: PERFORM CREATE-TPA-PMT-TRANS
                    sub_Create_Tpa_Pmt_Trans();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  WRITE N T8 TRANSACTION RECORD
                    getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                             //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                    pnd_W_Total_Record_W_Ia_Tpa_Written_N.nadd(1);                                                                                                        //Natural: ADD 1 TO W-IA-TPA-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
                    sub_Accum_N_Rec_Totals();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ADDED FOLLOWING  4/02 FOR TRAN 60(SWEEP) GET NEW DA AMT INTO CONTRACT
                //*                   CREATE T1 TRANS FOR QTRLY TO REFLECT NEW MONEY
                pnd_Hve_60_Trans.reset();                                                                                                                                 //Natural: RESET #HVE-60-TRANS
                if (condition(pnd_Sve_Tran_Code.getValue("*").equals(60)))                                                                                                //Natural: IF #SVE-TRAN-CODE ( * ) = 60
                {
                    pnd_Hve_60_Trans.setValue("Y");                                                                                                                       //Natural: ASSIGN #HVE-60-TRANS := 'Y'
                    pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                        //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
                    pnd_Naz_Parm_Pnd_I_Read_Type.setValue("R");                                                                                                           //Natural: ASSIGN #I-READ-TYPE := 'R'
                    //*  CALL NAZ MODULE FOR ADAM RESETTLEMENT TRN 60
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
                    sub_Call_Naz();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28)))                                                                                                //Natural: IF W-IA-OPTN-CODE = 28
                    {
                        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T1");                                                                                                  //Natural: MOVE 'T1' TO W-IA-TRN-TRANS-CODE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T2");                                                                                                  //Natural: MOVE 'T2' TO W-IA-TRN-TRANS-CODE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  OPENING VALUE FOR NEW ISSUE
                    if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greater(getZero())))                                                                                  //Natural: IF #O-SETTLE-AMT-NAZ GT 0
                    {
                        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
                        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
                        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
                        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
                        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
                        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
                        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
                        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                         //Natural: MOVE #O-SETTLE-AMT-NAZ TO W-IA-TRN-AMOUNT
                        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
                        //*  WRITE RESETTLE (SWEEP)
                        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                        //*                                          N (T1 OR T2) TRAN
                        pnd_W_Total_Record_W_Ia_Tpa_Written_N.nadd(1);                                                                                                    //Natural: ADD 1 TO W-IA-TPA-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
                        sub_Accum_N_Rec_Totals();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF  ADD  4/02
                //*  ADDED 11/01 FOR NEW TPA DUE
                                                                                                                                                                          //Natural: PERFORM CREATE-TPA-PMT-TRANS
                sub_Create_Tpa_Pmt_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Tpa_Part_Date() throws Exception                                                                                                                 //Natural: SET-TPA-PART-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Date_Pnd_W_Wrk_Mm.nsubtract(1);                                                                                                                             //Natural: SUBTRACT 1 FROM #W-WRK-MM
        if (condition(pnd_W_Date_Pnd_W_Wrk_Mm.equals(getZero())))                                                                                                         //Natural: IF #W-WRK-MM = 0
        {
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(12);                                                                                                                         //Natural: MOVE 12 TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Ccyy.nsubtract(1);                                                                                                                       //Natural: SUBTRACT 1 FROM #W-WRK-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #W-WRK-DD 31 TO 1 STEP -1
        for (pnd_W_Date_Pnd_W_Wrk_Dd.setValue(31); condition(pnd_W_Date_Pnd_W_Wrk_Dd.greaterOrEqual(1)); pnd_W_Date_Pnd_W_Wrk_Dd.nsubtract(1))
        {
            if (condition(DbsUtil.maskMatches(pnd_W_Date,"MMDDYYYY")))                                                                                                    //Natural: IF #W-DATE = MASK ( MMDDYYYY )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
    }
    //*  ADDED 11/01 FOR NEW TPA DUE
    private void sub_Create_Tpa_Pmt_Trans() throws Exception                                                                                                              //Natural: CREATE-TPA-PMT-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Hold_Date.setValue(w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date);                                                                                                //Natural: MOVE W-IA-TPA-NEXT-PAYMENT-DATE TO #W-HOLD-DATE
        if (condition(pnd_W_Iss_1st_Pay_Due_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                           //Natural: IF #W-ISS-1ST-PAY-DUE-DATE GT #W-TRANS-CCYYMM
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Iss_Mode_Mm.equals(pnd_W_Trans_Date_Pnd_W_Trans_Mm)))                                                                                         //Natural: IF #W-ISS-MODE-MM = #W-TRANS-MM
        {
            w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                             //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
            pnd_Sve_Dest.setValue(w_Tape_Out_B_W_Ia_Paymt_Destination);                                                                                                   //Natural: ASSIGN #SVE-DEST := W-IA-PAYMT-DESTINATION
            pnd_W_Tia_Sw.reset();                                                                                                                                         //Natural: RESET #W-TIA-SW #W-CRF-SW
            pnd_W_Crf_Sw.reset();
                                                                                                                                                                          //Natural: PERFORM READ-TPA-TRANSFER
            sub_Read_Tpa_Transfer();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED 6/01 COMMENTED 7/01
            if (condition(pnd_Sve_Dest.equals("RINV")))                                                                                                                   //Natural: IF #SVE-DEST = 'RINV'
            {
                                                                                                                                                                          //Natural: PERFORM READ-TPA-FUND
                sub_Read_Tpa_Fund();
                if (condition(Global.isEscape())) {return;}
                //*  ADDED 6/01 COMMENTED 7/01
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                        //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                            //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                  //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
            w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                         //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
            short decideConditionsMet2182 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SVE-DEST = 'MIXD'
            if (condition(pnd_Sve_Dest.equals("MIXD")))
            {
                decideConditionsMet2182++;
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T7");                                                                                                          //Natural: MOVE 'T7' TO W-IA-TRN-TRANS-CODE
                //*  ADDED 1/96
                                                                                                                                                                          //Natural: PERFORM SET-TPA-PART-DATE
                sub_Set_Tpa_Part_Date();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #SVE-DEST = 'CREF' OR = 'RINV'
            else if (condition(pnd_Sve_Dest.equals("CREF") || pnd_Sve_Dest.equals("RINV")))
            {
                decideConditionsMet2182++;
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T4");                                                                                                          //Natural: MOVE 'T4' TO W-IA-TRN-TRANS-CODE
                //*  ADDED 1/96
                                                                                                                                                                          //Natural: PERFORM SET-TPA-PART-DATE
                sub_Set_Tpa_Part_Date();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #SVE-DEST = 'CASH'
            else if (condition(pnd_Sve_Dest.equals("CASH")))
            {
                decideConditionsMet2182++;
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T5");                                                                                                          //Natural: MOVE 'T5' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: WHEN #SVE-DEST = 'TIAA' OR = 'REA '
            else if (condition(pnd_Sve_Dest.equals("TIAA") || pnd_Sve_Dest.equals("REA ")))
            {
                decideConditionsMet2182++;
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T6");                                                                                                          //Natural: MOVE 'T6' TO W-IA-TRN-TRANS-CODE
                //*  ADDED 1/96
                                                                                                                                                                          //Natural: PERFORM SET-TPA-PART-DATE
                sub_Set_Tpa_Part_Date();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("T3");                                                                                                          //Natural: MOVE 'T3' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: END-DECIDE
            w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                            //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
            //*  SAVE AMOUNT
            w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                             //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
            pnd_Sve_W_Total_Payment_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                          //Natural: ASSIGN #SVE-W-TOTAL-PAYMENT-AMOUNT := #W-TOTAL-PAYMENT-AMOUNT
            //* D /* IF PARTIAL OR FULL DRAW
            if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                        //Natural: IF ( #SVE-CONTR-TYPE-CD = 'P' OR = 'F' )
            {
                //*    (#SVE-MODE = #W-MODE OR #SVE-MODE = 100) AND /* DOWN AND PAYMENT DUE
                //*     #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* AND TRANS OCCURRED
                //*   CALLNAT 'IADN173' W-IA-CONTRACT-NUMBER #W-CHECK-DATE /* DURING CYCLE
                //*  JB01
                DbsUtil.callnat(Iadn173.class , getCurrentProcessState(), pnd_Ia_Contract9, pnd_Work1_Pnd_W_Check_Date, pnd_W_Total_Payment_Amount, pnd_Sve_W_Tot_Fin_Pmt_Amt); //Natural: CALLNAT 'IADN173' #IA-CONTRACT9 #W-CHECK-DATE #W-TOTAL-PAYMENT-AMOUNT #SVE-W-TOT-FIN-PMT-AMT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  GUAR+DIV AMTS
            w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                            //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-TRN-AMOUNT
            w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));                  //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            //*  WRITE N TRANSACTION RECORD
            //*  RESTORE AMT
            getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                     //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
            pnd_W_Total_Payment_Amount.setValue(pnd_Sve_W_Total_Payment_Amount);                                                                                          //Natural: ASSIGN #W-TOTAL-PAYMENT-AMOUNT := #SVE-W-TOTAL-PAYMENT-AMOUNT
            pnd_W_Total_Record_W_Ia_Tpa_Written_N.nadd(1);                                                                                                                //Natural: ADD 1 TO W-IA-TPA-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
            sub_Accum_N_Rec_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Tpa_Amounts() throws Exception                                                                                                                 //Natural: ACCUM-TPA-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Cnt.nadd(1);                                                                                                     //Natural: ADD 1 TO #TPA-NEW-ISSU-CNT
            pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                      //Natural: COMPUTE #TPA-NEW-ISSU-GUAR-PMT = #TPA-NEW-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                       //Natural: COMPUTE #TPA-NEW-ISSU-DIV-PMT = #TPA-NEW-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Tot_Pmt),              //Natural: COMPUTE #TPA-NEW-ISSU-TOT-PMT = #TPA-NEW-ISSU-GUAR-PMT + #TPA-NEW-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Tpa_New_Issu_Comm_Value.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                  //Natural: COMPUTE #TPA-NEW-ISSU-COMM-VALUE = #TPA-NEW-ISSU-COMM-VALUE + W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Cntrct_Cnt.nadd(1);                                                                                                   //Natural: ADD 1 TO #TPA-OLD-CNTRCT-CNT
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                      //Natural: COMPUTE #TPA-OLD-ISSU-GUAR-PMT = #TPA-OLD-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                       //Natural: COMPUTE #TPA-OLD-ISSU-DIV-PMT = #TPA-OLD-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Tot_Pmt),              //Natural: COMPUTE #TPA-OLD-ISSU-TOT-PMT = #TPA-OLD-ISSU-GUAR-PMT + #TPA-OLD-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Comm_Value.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                   //Natural: COMPUTE #TPA-OLD-ISSU-COMM-VALUE = #TPA-OLD-ISSU-COMM-VALUE + W-IA-OPEN-VALUE
            pnd_Summary_Report_Counters_Pnd_Tpa_Old_Issu_Cls_Value.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                   //Natural: COMPUTE #TPA-OLD-ISSU-CLS-VALUE = #TPA-OLD-ISSU-CLS-VALUE + W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ipro_Rtne() throws Exception                                                                                                                         //Natural: IPRO-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED 4/02
        pnd_Hve_60_Trans.reset();                                                                                                                                         //Natural: RESET #HVE-60-TRANS
        w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(0);                                                                                                              //Natural: MOVE 0 TO W-IA-TPA-NEXT-PAYMENT-DATE
        w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.setValue(0);                                                                                                               //Natural: MOVE 0 TO W-IA-TPA-REM-NBR-PAYMENTS
        w_Tape_Out_B_W_Ia_Next_Payment_Amount.setValue(0);                                                                                                                //Natural: MOVE 0 TO W-IA-NEXT-PAYMENT-AMOUNT
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            w_Tape_Out_B_W_Ia_Open_Value.setValue(0);                                                                                                                     //Natural: MOVE 0 TO W-IA-OPEN-VALUE
            //*  04/06 SAVE AMOUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Sve_W_Tot_Fin_Pmt_Amt.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                    //Natural: ASSIGN #SVE-W-TOT-FIN-PMT-AMT := #W-TOT-FIN-PMT-AMT
            //*  IF PARTIAL/FULL DUE FOR
            if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                        //Natural: IF #SVE-CONTR-TYPE-CD = 'P' OR = 'F'
            {
                //*     (#SVE-MODE = #W-MODE OR = 100) AND /* PAYMENT AND TRANS
                //*     #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* OCCURRED
                //*    CALLNAT 'IADN173' W-IA-CONTRACT-NUMBER #W-CHECK-DATE
                //*   JB01
                DbsUtil.callnat(Iadn173.class , getCurrentProcessState(), pnd_Ia_Contract9, pnd_Work1_Pnd_W_Check_Date, pnd_Sve_W_Total_Payment_Amount,                   //Natural: CALLNAT 'IADN173' #IA-CONTRACT9 #W-CHECK-DATE #SVE-W-TOTAL-PAYMENT-AMOUNT #W-TOT-FIN-PMT-AMT
                    pnd_W_Tot_Fin_Pmt_Amt);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_B_W_Ia_Open_Value.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                 //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-OPEN-VALUE
            //*  04/06 RESTORE AMOUNT
            pnd_W_Ia_Ipro_Open_Value_Less_Intr.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                                        //Natural: ADD W-IA-OPEN-VALUE TO #W-IA-IPRO-OPEN-VALUE-LESS-INTR
            pnd_W_Tot_Fin_Pmt_Amt.setValue(pnd_Sve_W_Tot_Fin_Pmt_Amt);                                                                                                    //Natural: ASSIGN #W-TOT-FIN-PMT-AMT := #SVE-W-TOT-FIN-PMT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Withdrawal.equals("Y") || pnd_W_Death_Sw.equals("Y") || pnd_W_Fin_Pay_Sw.equals("Y")))                                                        //Natural: IF #W-WITHDRAWAL = 'Y' OR #W-DEATH-SW = 'Y' OR #W-FIN-PAY-SW = 'Y'
        {
            w_Tape_Out_B_W_Ia_Close_Value.setValue(0);                                                                                                                    //Natural: MOVE 0 TO W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-CLOSE-VALUE
            pnd_W_Ia_Ipro_Close_Value_Less_Intr.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                                      //Natural: ADD W-IA-CLOSE-VALUE TO #W-IA-IPRO-CLOSE-VALUE-LESS-INTR
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-IPRO-AMOUNTS
        sub_Accum_Ipro_Amounts();
        if (condition(Global.isEscape())) {return;}
        //*  082017
        w_Tape_Out_B_W_Ia_Pin.setValue(pnd_W_Ia_Pin, MoveOption.LeftJustified);                                                                                           //Natural: MOVE LEFT #W-IA-PIN TO W-IA-PIN
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_B.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-B ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE TPA BALANCE (B) RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
        pnd_W_Total_Record_W_Ia_Ipro_Written_B.nadd(1);                                                                                                                   //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-B
                                                                                                                                                                          //Natural: PERFORM ACCUM-B-REC-TOTALS
        sub_Accum_B_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  NEW ISSUE LOGIC<-----------------
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                             //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
            if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25)))                                                                                                        //Natural: IF W-IA-OPTN-CODE = 25
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I1");                                                                                                          //Natural: MOVE 'I1' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I2");                                                                                                          //Natural: MOVE 'I2' TO W-IA-TRN-TRANS-CODE
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                            //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm);                                                                                   //Natural: MOVE #W-ISS-ISS-DATE-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy);                                                                               //Natural: MOVE #W-ISS-ISS-DATE-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                         //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                        //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                            //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                  //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
            //*  SAVE AMOUNT
            w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                             //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
            pnd_Sve_W_Tot_Fin_Pmt_Amt.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                    //Natural: ASSIGN #SVE-W-TOT-FIN-PMT-AMT := #W-TOT-FIN-PMT-AMT
            //* AND /* IF PARTIAL/FULL DRAW
            if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                        //Natural: IF ( #SVE-CONTR-TYPE-CD = 'P' OR = 'F' )
            {
                //*  (#SVE-MODE = #W-MODE OR = 100) AND /* DOWN AND PAYMENT DUE
                //*   #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* AND TRANS OCCURRED
                //*    CALLNAT 'IADN173' W-IA-CONTRACT-NUMBER #W-CHECK-DATE /* DURING CYCLE
                //*  JB01
                DbsUtil.callnat(Iadn173.class , getCurrentProcessState(), pnd_Ia_Contract9, pnd_Work1_Pnd_W_Check_Date, pnd_Sve_W_Total_Payment_Amount,                   //Natural: CALLNAT 'IADN173' #IA-CONTRACT9 #W-CHECK-DATE #SVE-W-TOTAL-PAYMENT-AMOUNT #W-TOT-FIN-PMT-AMT
                    pnd_W_Tot_Fin_Pmt_Amt);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                 //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-TRN-AMOUNT
            pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                               //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            //*  WRITE N TRANSACTION RECORD
            //*  RESTORE AMOUNT
            getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                     //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
            pnd_W_Tot_Fin_Pmt_Amt.setValue(pnd_Sve_W_Tot_Fin_Pmt_Amt);                                                                                                    //Natural: ASSIGN #W-TOT-FIN-PMT-AMT := #SVE-W-TOT-FIN-PMT-AMT
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
            sub_Accum_N_Rec_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED FOLLOWING  4/02 FOR TRAN 60(SWEEP) GET NEW DA AMT INTO CONTRACT
            //*                   CREATE T1 TRANS FOR QTRLY TO REFLECT NEW MONEY
            pnd_Hve_60_Trans.reset();                                                                                                                                     //Natural: RESET #HVE-60-TRANS
            if (condition(pnd_Sve_Tran_Code.getValue("*").equals(60)))                                                                                                    //Natural: IF #SVE-TRAN-CODE ( * ) = 60
            {
                pnd_Hve_60_Trans.setValue("Y");                                                                                                                           //Natural: ASSIGN #HVE-60-TRANS := 'Y'
                pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                            //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
                pnd_Naz_Parm_Pnd_I_Read_Type.setValue("R");                                                                                                               //Natural: ASSIGN #I-READ-TYPE := 'R'
                //*  CALL NAZ MODULE FOR ADAM RESETTLEMENT TRN 60
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
                sub_Call_Naz();
                if (condition(Global.isEscape())) {return;}
                //*  NEW MONEY IN CONTRACT
                if (condition(pnd_Naz_Parm_Pnd_O_Total_Accum_Amt.greater(getZero())))                                                                                     //Natural: IF #O-TOTAL-ACCUM-AMT GT 0
                {
                    w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Total_Accum_Amt);                                                                            //Natural: MOVE #O-TOTAL-ACCUM-AMT TO W-IA-TRN-AMOUNT
                    w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                        //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
                    //*  WRITE RESETTLE (SWEEP)
                    getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                             //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                    //*                                          N (I1 OR I2) TRAN
                    pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                       //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
                    sub_Accum_N_Rec_Totals();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF  ADD  4/02
            if (condition(pnd_W_Retro_Sw.equals("Y")))                                                                                                                    //Natural: IF #W-RETRO-SW = 'Y'
            {
                //*  IF PMT IS DUE OTHERWISE BYPASS
                                                                                                                                                                          //Natural: PERFORM I3-RTNE
                sub_I3_Rtne();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM I4-7-RTNE
                sub_I4_7_Rtne();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 8/00 REMOVED ELSE & ADDED END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------>END OF NEW-ISSU LOGIC
        //*  ADDED FOLLOWING 8/00 TO HANDLE WITHDRAWALS
        if (condition(pnd_W_New_Issue.notEquals("Y")))                                                                                                                    //Natural: IF #W-NEW-ISSUE NE 'Y'
        {
            if (condition(pnd_W_Withdrawal.equals("Y")))                                                                                                                  //Natural: IF #W-WITHDRAWAL = 'Y'
            {
                if (condition(pnd_W_Iss_Final_Per_Pay_Date.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) || pnd_W_Iss_Final_Per_Pay_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm))) //Natural: IF #W-ISS-FINAL-PER-PAY-DATE = #W-TRANS-CCYYMM OR #W-ISS-FINAL-PER-PAY-DATE GT #W-TRANS-CCYYMM
                {
                                                                                                                                                                          //Natural: PERFORM I3-RTNE
                    sub_I3_Rtne();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM I4-7-RTNE
                    sub_I4_7_Rtne();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM I8-RTNE
                    sub_I8_Rtne();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM I8-RTNE
                    sub_I8_Rtne();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 8/00
        if (condition(pnd_W_Death_Sw.equals("Y")))                                                                                                                        //Natural: IF #W-DEATH-SW = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM I8-RTNE
            sub_I8_Rtne();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING 4/02 FOR TRAN 60(SWEEP) GET NEW DA AMT INTO CONTRACT
        //*                   CREATE I1 TRANS FOR QTRLY TO REFLECT NEW MONEY
        if (condition(pnd_Sve_Tran_Code.getValue("*").equals(60)))                                                                                                        //Natural: IF #SVE-TRAN-CODE ( * ) = 60
        {
                                                                                                                                                                          //Natural: PERFORM I1-I2-FOR-SWEEP-RTNE
            sub_I1_I2_For_Sweep_Rtne();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF  ADD  4/02
        if (condition(pnd_Sve_Iss_Pend_Pay.notEquals("0")))                                                                                                               //Natural: IF #SVE-ISS-PEND-PAY NE '0'
        {
            if (condition(pnd_Sve_Iss_Last_Trans_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                      //Natural: IF #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM
            {
                                                                                                                                                                          //Natural: PERFORM I3-RTNE
                sub_I3_Rtne();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM I4-7-RTNE
                sub_I4_7_Rtne();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sve_Iss_Pend_Pay.equals("0") || pnd_Sve_Iss_Pend_Pay.equals(" ")))                                                                              //Natural: IF #SVE-ISS-PEND-PAY = '0' OR = ' '
        {
                                                                                                                                                                          //Natural: PERFORM I3-RTNE
            sub_I3_Rtne();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM I4-7-RTNE
            sub_I4_7_Rtne();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_I1_I2_For_Sweep_Rtne() throws Exception                                                                                                              //Natural: I1-I2-FOR-SWEEP-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(25)))                                                                                                            //Natural: IF W-IA-OPTN-CODE = 25
        {
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I1");                                                                                                              //Natural: MOVE 'I1' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I2");                                                                                                              //Natural: MOVE 'I2' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: END-IF
        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                                    //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
        pnd_Naz_Parm_Pnd_I_Read_Type.setValue("R");                                                                                                                       //Natural: ASSIGN #I-READ-TYPE := 'R'
        //*  CALL NAZ MODULE FOR ADAM RESETTLEMENT TRN 60
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
        sub_Call_Naz();
        if (condition(Global.isEscape())) {return;}
        pnd_Hve_60_Trans.setValue("Y");                                                                                                                                   //Natural: ASSIGN #HVE-60-TRANS := 'Y'
        //*  RESETTLE FOR TRAN 60 SWEEP
        if (condition(pnd_Naz_Parm_Pnd_O_Total_Accum_Amt.greater(getZero())))                                                                                             //Natural: IF #O-TOTAL-ACCUM-AMT GT 0
        {
            w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_Naz_Parm_Pnd_O_Total_Accum_Amt);                                                                                    //Natural: MOVE #O-TOTAL-ACCUM-AMT TO W-IA-TRN-AMOUNT
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                               //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            //*  WRITE RESETTLE (SWEEP)
            getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                     //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
            sub_Accum_N_Rec_Totals();
            if (condition(Global.isEscape())) {return;}
            //*                                          N (I1 OR I2) TRAN
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_I3_Rtne() throws Exception                                                                                                                           //Natural: I3-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---- INTEREST CREDITED ----
        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I3");                                                                                                                  //Natural: MOVE 'I3' TO W-IA-TRN-TRANS-CODE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        //*  >>> FOLLOWING WAS USED FOR DIVIDEND CHANGE BOUNDARY WHEN REPORTING
        //*  >>> MARCH QTRLY TO ALWAYS USE CURRENT DIVIDEND AMOUNT FOR INTEREST
        //*  >>> CURRENTLY USING 1ST OF MONTH FOR INTEREST(I3) & PAYMENT(I4)
        //*  COMPUTE W-IA-TRN-AMOUNT = #W-TOTAL-GUARPYMT-AMOUNT + USED FOR DIV
        //*   #W-TOTAL-GUARDIVD-AMOUNT CHANGE BOUNDARY WHEN REPORTING END OF
        //*                            MONTH INTEREST CREDITED
        //*   END OF COMMENTED 2/00
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        //*  USE SAME DATE INTEREST
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        //*  CREDIT(I3) & PMT TRANS
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        //*  SAVE AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        pnd_Sve_W_Total_Payment_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                              //Natural: ASSIGN #SVE-W-TOTAL-PAYMENT-AMOUNT := #W-TOTAL-PAYMENT-AMOUNT
        //* D /* IF PARTIAL OR FULL DRAW
        if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                            //Natural: IF ( #SVE-CONTR-TYPE-CD = 'P' OR = 'F' )
        {
            //*  (#SVE-MODE = #W-MODE OR #SVE-MODE = 100) AND /* DOWN AND PAYMENT DUE
            //*   #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* AND TRANS OCCURRED
            //*  CALLNAT 'IADN173' W-IA-CONTRACT-NUMBER #W-CHECK-DATE /* DURING CYCLE
            //*  JB01
            DbsUtil.callnat(Iadn173.class , getCurrentProcessState(), pnd_Ia_Contract9, pnd_Work1_Pnd_W_Check_Date, pnd_W_Total_Payment_Amount, pnd_Sve_W_Tot_Fin_Pmt_Amt); //Natural: CALLNAT 'IADN173' #IA-CONTRACT9 #W-CHECK-DATE #W-TOTAL-PAYMENT-AMOUNT #SVE-W-TOT-FIN-PMT-AMT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESTORE AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                                //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-TRN-AMOUNT
        pnd_W_Total_Payment_Amount.setValue(pnd_Sve_W_Total_Payment_Amount);                                                                                              //Natural: ASSIGN #W-TOTAL-PAYMENT-AMOUNT := #SVE-W-TOTAL-PAYMENT-AMOUNT
        pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                                   //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_I4_7_Rtne() throws Exception                                                                                                                         //Natural: I4-7-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        if (condition(pnd_W_Iss_1st_Pay_Due_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                           //Natural: IF #W-ISS-1ST-PAY-DUE-DATE GT #W-TRANS-CCYYMM
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Death_Sw.equals("Y")))                                                                                                                        //Natural: IF #W-DEATH-SW = 'Y'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  NOT CURRENTLY USED
        short decideConditionsMet2498 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN W-IA-PAYMT-DESTINATION = 'ROLL' OR = 'IRAX' OR = '03BX' OR = 'QPLX'
        if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("ROLL") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAX") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BX") 
            || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLX")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I4");                                                                                                              //Natural: MOVE 'I4' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAC'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAC")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I5");                                                                                                              //Natural: MOVE 'I5' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'CASH' OR = '    '
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("CASH") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("    ")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I6");                                                                                                              //Natural: MOVE 'I6' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAT'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAT")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I7");                                                                                                              //Natural: MOVE 'I7' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'XXXX'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("XXXX")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I8");                                                                                                              //Natural: MOVE 'I8' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'XXXX'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("XXXX")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I9");                                                                                                              //Natural: MOVE 'I9' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAC' OR = '03BC' OR = 'QPLC'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAC") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BC") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLC")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("IA");                                                                                                              //Natural: MOVE 'IA' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAT' OR = '03BT' OR = 'QPLT'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAT") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BT") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLT")))
        {
            decideConditionsMet2498++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("IB");                                                                                                              //Natural: MOVE 'IB' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("ZZ");                                                                                                              //Natural: MOVE 'ZZ' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I4");                                                                                                                  //Natural: MOVE 'I4' TO W-IA-TRN-TRANS-CODE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        //*  SAVE AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        pnd_Sve_W_Total_Payment_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                              //Natural: ASSIGN #SVE-W-TOTAL-PAYMENT-AMOUNT := #W-TOTAL-PAYMENT-AMOUNT
        //* AND /* IF PARTIAL OR FULL DRAW
        if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                            //Natural: IF ( #SVE-CONTR-TYPE-CD = 'P' OR = 'F' )
        {
            //*  (#SVE-MODE = #W-MODE OR = 100) AND /* DOWN AND PAYMENT DUE
            //*   #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* AND TRANS OCCURRED
            //*  CALLNAT 'IADN173' W-IA-CONTRACT-NUMBER #W-CHECK-DATE /* DURING CYCLE
            //*  JB01
            DbsUtil.callnat(Iadn173.class , getCurrentProcessState(), pnd_Ia_Contract9, pnd_Work1_Pnd_W_Check_Date, pnd_W_Total_Payment_Amount, pnd_Sve_W_Tot_Fin_Pmt_Amt); //Natural: CALLNAT 'IADN173' #IA-CONTRACT9 #W-CHECK-DATE #W-TOTAL-PAYMENT-AMOUNT #SVE-W-TOT-FIN-PMT-AMT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESTORE AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                                //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-TRN-AMOUNT
        pnd_W_Total_Payment_Amount.setValue(pnd_Sve_W_Total_Payment_Amount);                                                                                              //Natural: ASSIGN #W-TOTAL-PAYMENT-AMOUNT := #SVE-W-TOTAL-PAYMENT-AMOUNT
        //*                        /* 04/06 THIS IS FOR ABOVE LINE
        w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));                      //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
        pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                                   //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Accum_Ipro_Amounts() throws Exception                                                                                                                //Natural: ACCUM-IPRO-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #IPRO-NEW-ISSU-CNT
            pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                     //Natural: COMPUTE #IPRO-NEW-ISSU-GUAR-PMT = #IPRO-NEW-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                      //Natural: COMPUTE #IPRO-NEW-ISSU-DIV-PMT = #IPRO-NEW-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Tot_Pmt),            //Natural: COMPUTE #IPRO-NEW-ISSU-TOT-PMT = #IPRO-NEW-ISSU-GUAR-PMT + #IPRO-NEW-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Ipro_New_Issu_Fin_Pmt.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                    //Natural: COMPUTE #IPRO-NEW-ISSU-FIN-PMT = #IPRO-NEW-ISSU-FIN-PMT + W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Cntrct_Cnt.nadd(1);                                                                                             //Natural: ADD 1 TO #IPRO-OLD-ISSU-CNTRCT-CNT
            pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                     //Natural: COMPUTE #IPRO-OLD-ISSU-GUAR-PMT = #IPRO-OLD-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                      //Natural: COMPUTE #IPRO-OLD-ISSU-DIV-PMT = #IPRO-OLD-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Tot_Pmt),            //Natural: COMPUTE #IPRO-OLD-ISSU-TOT-PMT = #IPRO-OLD-ISSU-GUAR-PMT + #IPRO-OLD-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Ipro_Old_Issu_Fin_Pmt.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                     //Natural: COMPUTE #IPRO-OLD-ISSU-FIN-PMT = #IPRO-OLD-ISSU-FIN-PMT + W-IA-OPEN-VALUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Opt22_Rtne() throws Exception                                                                                                                        //Natural: OPT22-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        w_Tape_Out_B_W_Ia_Tpa_Next_Payment_Date.setValue(0);                                                                                                              //Natural: MOVE 0 TO W-IA-TPA-NEXT-PAYMENT-DATE
        w_Tape_Out_B_W_Ia_Tpa_Rem_Nbr_Payments.setValue(0);                                                                                                               //Natural: MOVE 0 TO W-IA-TPA-REM-NBR-PAYMENTS
        w_Tape_Out_B_W_Ia_Next_Payment_Amount.setValue(0);                                                                                                                //Natural: MOVE 0 TO W-IA-NEXT-PAYMENT-AMOUNT
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            w_Tape_Out_B_W_Ia_Open_Value.setValue(0);                                                                                                                     //Natural: MOVE 0 TO W-IA-OPEN-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            w_Tape_Out_B_W_Ia_Open_Value.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                 //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-OPEN-VALUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Withdrawal.equals("Y") || pnd_W_Death_Sw.equals("Y") || pnd_W_Fin_Pay_Sw.equals("Y")))                                                        //Natural: IF #W-WITHDRAWAL = 'Y' OR #W-DEATH-SW = 'Y' OR #W-FIN-PAY-SW = 'Y'
        {
            w_Tape_Out_B_W_Ia_Close_Value.setValue(0);                                                                                                                    //Natural: MOVE 0 TO W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            w_Tape_Out_B_W_Ia_Close_Value.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-OPT22-AMOUNTS
        sub_Accum_Opt22_Amounts();
        if (condition(Global.isEscape())) {return;}
        //*  082017
        w_Tape_Out_B_W_Ia_Pin.setValue(pnd_W_Ia_Pin, MoveOption.LeftJustified);                                                                                           //Natural: MOVE LEFT #W-IA-PIN TO W-IA-PIN
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_B.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-B ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE TPA BALANCE (B) RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_B.nadd(1);                                                                                                                 //Natural: ADD 1 TO W-IA-P&I-WRITTEN-B
                                                                                                                                                                          //Natural: PERFORM ACCUM-B-REC-TOTALS
        sub_Accum_B_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DECIDE-IF-PAYMENT-IS-DUE
        sub_Decide_If_Payment_Is_Due();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                             //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P1");                                                                                                              //Natural: MOVE 'P1' TO W-IA-TRN-TRANS-CODE
            w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                            //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Mm);                                                                                   //Natural: MOVE #W-ISS-ISS-DATE-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Iss_Iss_Date_Pnd_W_Iss_Iss_Date_Ccyy);                                                                               //Natural: MOVE #W-ISS-ISS-DATE-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                         //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
            pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                        //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
            pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                            //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
            pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                          //Natural: MOVE 01 TO #W-WRK-DD
            w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                  //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
            w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                             //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
            w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                 //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-TRN-AMOUNT
            pnd_W_Total_Record_W_Ia_Pamp_I_Written_N.nadd(1);                                                                                                             //Natural: ADD 1 TO W-IA-P&I-WRITTEN-N
            w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
            //*  WRITE N TRANSACTION RECORD
            getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                     //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
            sub_Accum_N_Rec_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_W_Pmt_Due_Sw.equals("Y") && pnd_Sve_Iss_Pend_Pay.equals("0")))                                                                              //Natural: IF #W-PMT-DUE-SW = 'Y' AND #SVE-ISS-PEND-PAY = '0'
            {
                                                                                                                                                                          //Natural: PERFORM P3-RTNE
                sub_P3_Rtne();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM P4-RTNE
                sub_P4_Rtne();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Death_Sw.equals("Y")))                                                                                                                        //Natural: IF #W-DEATH-SW = 'Y'
        {
            //* CREATE WITHDRWL TRANS FOR DTH SAME AS IPRO 10/00
                                                                                                                                                                          //Natural: PERFORM P8-RTNE
            sub_P8_Rtne();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ASSUME PMT UP TO TRANS DATE
        if (condition(pnd_Sve_Iss_Pend_Pay.notEquals("0")))                                                                                                               //Natural: IF #SVE-ISS-PEND-PAY NE '0'
        {
            //*  ASSUME PMT UP TO TRANS DATE
            if (condition(pnd_Sve_Iss_Last_Trans_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm) && pnd_W_Pmt_Due_Sw.equals("Y")))                                      //Natural: IF #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM AND #W-PMT-DUE-SW = 'Y'
            {
                //*  INTEREST CREDITED TRANS
                                                                                                                                                                          //Natural: PERFORM P3-RTNE
                sub_P3_Rtne();
                if (condition(Global.isEscape())) {return;}
                //*  INCLUDE PAY TRANS FOR P&I FINAL-PMT
                                                                                                                                                                          //Natural: PERFORM P4-RTNE
                sub_P4_Rtne();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  THIS IS NOT A NEW-ISSUED-CONTRACT
        if (condition(pnd_W_New_Issue.notEquals("Y")))                                                                                                                    //Natural: IF #W-NEW-ISSUE NE 'Y'
        {
            if (condition(pnd_W_Withdrawal.equals("Y") || pnd_W_Fin_Pay_Sw.equals("Y")))                                                                                  //Natural: IF #W-WITHDRAWAL = 'Y' OR #W-FIN-PAY-SW = 'Y'
            {
                //*  WITHDRAWAL TRANS
                                                                                                                                                                          //Natural: PERFORM P8-RTNE
                sub_P8_Rtne();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_W_Pmt_Due_Sw.equals("Y")))                                                                                                              //Natural: IF #W-PMT-DUE-SW = 'Y'
                {
                    //*  INTEREST CREDITED TRANS
                                                                                                                                                                          //Natural: PERFORM P3-RTNE
                    sub_P3_Rtne();
                    if (condition(Global.isEscape())) {return;}
                    //*  INCLUDE PAY TRANS FOR P&I FINAL-PMT
                                                                                                                                                                          //Natural: PERFORM P4-RTNE
                    sub_P4_Rtne();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Sve_Iss_Pend_Pay.notEquals("0") || pnd_W_Death_Sw.equals("Y")))                                                                         //Natural: IF #SVE-ISS-PEND-PAY NE '0' OR #W-DEATH-SW = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_W_Pmt_Due_Sw.equals("Y")))                                                                                                          //Natural: IF #W-PMT-DUE-SW = 'Y'
                    {
                        //*  INTEREST CREDITED TRANS
                                                                                                                                                                          //Natural: PERFORM P3-RTNE
                        sub_P3_Rtne();
                        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM P4-RTNE
                        sub_P4_Rtne();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Decide_If_Payment_Is_Due() throws Exception                                                                                                          //Natural: DECIDE-IF-PAYMENT-IS-DUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Pmt_Due_Sw.reset();                                                                                                                                         //Natural: RESET #W-PMT-DUE-SW
        if (condition(w_Tape_Out_B_W_Ia_Mode.equals(100)))                                                                                                                //Natural: IF W-IA-MODE = 100
        {
            pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                               //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2679 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF W-IA-MODE;//Natural: VALUE 601
        if (condition((w_Tape_Out_B_W_Ia_Mode.equals(601))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(1) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(4) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(7)             //Natural: IF #W-TRANS-MM = 1 OR = 4 OR = 7 OR = 10
                || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(10)))
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(602))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(2) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(5) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(8)             //Natural: IF #W-TRANS-MM = 2 OR = 5 OR = 8 OR = 11
                || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(11)))
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(603))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(3) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(6) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(9)             //Natural: IF #W-TRANS-MM = 3 OR = 6 OR = 9 OR = 12
                || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(12)))
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(701))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(1) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(7)))                                                        //Natural: IF #W-TRANS-MM = 1 OR = 7
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(702))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(2) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(8)))                                                        //Natural: IF #W-TRANS-MM = 2 OR = 8
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(703))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(3) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(9)))                                                        //Natural: IF #W-TRANS-MM = 3 OR = 9
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(704))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(4) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(10)))                                                       //Natural: IF #W-TRANS-MM = 4 OR = 10
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(705))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(5) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(11)))                                                       //Natural: IF #W-TRANS-MM = 5 OR = 11
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((w_Tape_Out_B_W_Ia_Mode.equals(706))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(6) || pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(12)))                                                       //Natural: IF #W-TRANS-MM = 6 OR = 12
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((w_Tape_Out_B_W_Ia_Mode.greaterOrEqual(801) && w_Tape_Out_B_W_Ia_Mode.lessOrEqual(812)))))
        {
            decideConditionsMet2679++;
            if (condition(pnd_W_Trans_Date_Pnd_W_Trans_Mm.equals(w_Tape_Out_B_W_Ia_Mode_2)))                                                                              //Natural: IF #W-TRANS-MM = W-IA-MODE-2
            {
                pnd_W_Pmt_Due_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-PMT-DUE-SW := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_P4_Rtne() throws Exception                                                                                                                           //Natural: P4-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        if (condition(pnd_W_Iss_1st_Pay_Due_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                           //Natural: IF #W-ISS-1ST-PAY-DUE-DATE GT #W-TRANS-CCYYMM
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_W_Death_Sw.equals("Y")))                                                                                                                        //Natural: IF #W-DEATH-SW = 'Y'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CURRENTLY NOT USED
        short decideConditionsMet2736 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN W-IA-PAYMT-DESTINATION = 'ROLL' OR = 'IRAX' OR = '03BX' OR = 'QPLX'
        if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("ROLL") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAX") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BX") 
            || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLX")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P4");                                                                                                              //Natural: MOVE 'P4' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAC'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAC")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P5");                                                                                                              //Natural: MOVE 'P5' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'CASH' OR = '    '
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("CASH") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("    ")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P6");                                                                                                              //Natural: MOVE 'P6' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAT'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAT")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P7");                                                                                                              //Natural: MOVE 'P7' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'XXXX'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("XXXX")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P8");                                                                                                              //Natural: MOVE 'P8' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'XXXX'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("XXXX")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P9");                                                                                                              //Natural: MOVE 'P9' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAC' OR = '03BC' OR = 'QPLC'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAC") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BC") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLC")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("IA");                                                                                                              //Natural: MOVE 'IA' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN W-IA-PAYMT-DESTINATION = 'IRAT' OR = '03BT' OR = 'QPLT'
        else if (condition(w_Tape_Out_B_W_Ia_Paymt_Destination.equals("IRAT") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("03BT") || w_Tape_Out_B_W_Ia_Paymt_Destination.equals("QPLT")))
        {
            decideConditionsMet2736++;
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("IB");                                                                                                              //Natural: MOVE 'IB' TO W-IA-TRN-TRANS-CODE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("ZZ");                                                                                                              //Natural: MOVE 'ZZ' TO W-IA-TRN-TRANS-CODE
            //*  CURRENTLY NOT USED
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   FORCE P4 INTO TRANS CODE FOR P&I
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P4");                                                                                                                  //Natural: MOVE 'P4' TO W-IA-TRN-TRANS-CODE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        //*  >>>> TOTAL OF ALL PER-PMT-AMT AND PER-DIV-AMT ON 50 RECS
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                                //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-TRN-AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));                      //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N.nadd(1);                                                                                                                 //Natural: ADD 1 TO W-IA-P&I-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_P3_Rtne() throws Exception                                                                                                                           //Natural: P3-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---- INTEREST CREDITED ----
        if (condition(pnd_W_Iss_1st_Pay_Due_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                           //Natural: IF #W-ISS-1ST-PAY-DUE-DATE GT #W-TRANS-CCYYMM
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P3");                                                                                                                  //Natural: MOVE 'P3' TO W-IA-TRN-TRANS-CODE
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        //*  >>>> TOTAL OF ALL PER-PMT-AMT AND PER-DIV-AMT ON 50 RECS
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Total_Payment_Amount);                                                                                                //Natural: MOVE #W-TOTAL-PAYMENT-AMOUNT TO W-IA-TRN-AMOUNT
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N.nadd(1);                                                                                                                 //Natural: ADD 1 TO W-IA-P&I-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_P8_Rtne() throws Exception                                                                                                                           //Natural: P8-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("P8");                                                                                                                  //Natural: MOVE 'P8' TO W-IA-TRN-TRANS-CODE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                     //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-TRN-AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));                      //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
        pnd_W_Total_Record_W_Ia_Pamp_I_Written_N.nadd(1);                                                                                                                 //Natural: ADD 1 TO W-IA-P&I-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Accum_Opt22_Amounts() throws Exception                                                                                                               //Natural: ACCUM-OPT22-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                       //Natural: IF #W-NEW-ISSUE = 'Y'
        {
            pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Cnt.nadd(1);                                                                                                  //Natural: ADD 1 TO #P&I-NEW-ISSU-CNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                   //Natural: COMPUTE #P&I-NEW-ISSU-GUAR-PMT = #P&I-NEW-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                    //Natural: COMPUTE #P&I-NEW-ISSU-DIV-PMT = #P&I-NEW-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Tot_Pmt),        //Natural: COMPUTE #P&I-NEW-ISSU-TOT-PMT = #P&I-NEW-ISSU-GUAR-PMT + #P&I-NEW-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Pamp_I_New_Issu_Fin_Pmt.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                  //Natural: COMPUTE #P&I-NEW-ISSU-FIN-PMT = #P&I-NEW-ISSU-FIN-PMT + W-IA-CLOSE-VALUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Cntrct_Cnt.nadd(1);                                                                                           //Natural: ADD 1 TO #P&I-OLD-ISSU-CNTRCT-CNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                   //Natural: COMPUTE #P&I-OLD-ISSU-GUAR-PMT = #P&I-OLD-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt.nadd(pnd_W_Total_Guardivd_Amount);                                                                    //Natural: COMPUTE #P&I-OLD-ISSU-DIV-PMT = #P&I-OLD-ISSU-DIV-PMT + #W-TOTAL-GUARDIVD-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt.compute(new ComputeParameters(false, pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Tot_Pmt),        //Natural: COMPUTE #P&I-OLD-ISSU-TOT-PMT = #P&I-OLD-ISSU-GUAR-PMT + #P&I-OLD-ISSU-DIV-PMT
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Guar_Pmt.add(pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Div_Pmt));
            pnd_Summary_Report_Counters_Pnd_Pamp_I_Old_Issu_Fin_Pmt.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                   //Natural: COMPUTE #P&I-OLD-ISSU-FIN-PMT = #P&I-OLD-ISSU-FIN-PMT + W-IA-OPEN-VALUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Pia3170() throws Exception                                                                                                                      //Natural: CALL-PIA3170
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Ccyymm.setValue(pnd_Work1_Pnd_W_Check_Ccyymm);                                                                           //Natural: MOVE #W-CHECK-CCYYMM TO #W-HOLD-DATE-CCYYMM
        pnd_W_Hold_Date_Ccyymmdd_Pnd_W_Hold_Date_Dd.setValue(1);                                                                                                          //Natural: MOVE 01 TO #W-HOLD-DATE-DD
        pnd_Pia3170_Cycle_Date.setValue(pnd_W_Hold_Date_Ccyymmdd);                                                                                                        //Natural: MOVE #W-HOLD-DATE-CCYYMMDD TO #PIA3170-CYCLE-DATE
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Ccyymm.setValue(pnd_Sve_Chk_Date);                                                                                       //Natural: MOVE #SVE-CHK-DATE TO #PIA3170-CYCLE-CCYYMM
        //*  IA CHECK-DATE
        pnd_Pia3170_Cycle_Date_Pnd_Pia3170_Cycle_Dd2.setValue(1);                                                                                                         //Natural: MOVE 1 TO #PIA3170-CYCLE-DD2
        //*  ADDED ELSE TO FOLLOWING 1/02  TO USE ISSUE-DAY FOR OPENING-VALUE
        if (condition(pnd_W_Close_Val_Sw.equals("Y")))                                                                                                                    //Natural: IF #W-CLOSE-VAL-SW = 'Y'
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd2.setValue(1);                                                                                                       //Natural: MOVE 01 TO #PIA3170-COMM-DD2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Dd2.setValue(pnd_W_Iss_Dte_Dd);                                                                                        //Natural: MOVE #W-ISS-DTE-DD TO #PIA3170-COMM-DD2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm);                                                                      //Natural: MOVE #W-TRANS-CCYYMM TO #PIA3170-COMM-CCYYMM
        if (condition(pnd_W_Iss_Iss_Date.greater(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))                                                                                   //Natural: IF #W-ISS-ISS-DATE GT #W-TRANS-CCYYMM
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm.setValue(pnd_W_Iss_Iss_Date);                                                                                   //Natural: MOVE #W-ISS-ISS-DATE TO #PIA3170-COMM-CCYYMM
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDING FOLLOWING FOR RETRO-NEW-ISSUE TO GET CORRECT CLOSING VALUE 4/01
        if (condition(pnd_W_Retro_Sw.equals("Y") && pnd_W_Close_Val_Sw.notEquals("Y")))                                                                                   //Natural: IF #W-RETRO-SW = 'Y' AND #W-CLOSE-VAL-SW NE 'Y'
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm.setValue(pnd_W_Iss_Iss_Date);                                                                                   //Natural: MOVE #W-ISS-ISS-DATE TO #PIA3170-COMM-CCYYMM
            //*  04/06 SAVE THE ORIGINAL DATA FIRST
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sve_Rec5.getValue("*").setValue(pnd_Master_Rec_Linkage_Pnd_Rec5.getValue("*"));                                                                               //Natural: ASSIGN #SVE-REC5 ( * ) := #REC5 ( * )
        if (condition(pnd_W_Close_Val_Sw.equals("Y")))                                                                                                                    //Natural: IF #W-CLOSE-VAL-SW = 'Y'
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm.nadd(1);                                                                                                        //Natural: ADD 1 TO #PIA3170-COMM-CCYYMM
            //*  04/06 CHECK FOR PARTIAL AND OVERRIDE PAYMENT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DATA WITH BEFORE
            if (condition(pnd_Sve_Contr_Type_Cd.equals("P") || pnd_Sve_Contr_Type_Cd.equals("F")))                                                                        //Natural: IF #SVE-CONTR-TYPE-CD = 'P' OR = 'F'
            {
                //*     (#SVE-MODE = #W-MODE OR #SVE-MODE = 100) AND /* IMAGE IF
                //*     #SVE-ISS-LAST-TRANS-DATE GT #W-TRANS-CCYYMM /* DRAWDOWN
                //*  TRANSACTIONS OCCURRED WITHIN THE CHECK
                //*  CYCLE AND PAYMENT IS DUE TO GET
                //*  THE CORRECT OPENING BALANCE
                DbsUtil.callnat(Iadn172.class , getCurrentProcessState(), w_Tape_Out_B_W_Ia_Contract_Number, pnd_Work1_Pnd_W_Check_Date, pnd_Master_Rec_Linkage_Pnd_Rec5.getValue("*")); //Natural: CALLNAT 'IADN172' W-IA-CONTRACT-NUMBER #W-CHECK-DATE #REC5 ( * )
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Mm.greater(12)))                                                                                             //Natural: IF #PIA3170-COMM-MM GT 12
        {
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Mm.setValue(1);                                                                                                        //Natural: MOVE 1 TO #PIA3170-COMM-MM
            pnd_Pia3170_Comm_Date_Pnd_Pia3170_Comm_Ccyymm.nadd(100);                                                                                                      //Natural: ADD 100 TO #PIA3170-COMM-CCYYMM
        }                                                                                                                                                                 //Natural: END-IF
        pia3170ReturnCode = DbsUtil.callExternalProgram("PIA3170",pnd_Pia3170_Call_Type,pnd_Pia3170_Cycle_Date,pnd_Pia3170_Comm_Date,pnd_Master_Rec_Linkage_Pnd_Rec1,     //Natural: CALL 'PIA3170' #PIA3170-CALL-TYPE #PIA3170-CYCLE-DATE #PIA3170-COMM-DATE #REC1 #PIA3170-COMM-VALUE-A #PIA3170-ERROR-01A
            pnd_Pia3170_Comm_Value_Pnd_Pia3170_Comm_Value_A,pnd_Pia3170_Error_01a);
        if (condition(pnd_Pia3170_Error_01a.notEquals("0")))                                                                                                              //Natural: IF #PIA3170-ERROR-01A NE '0'
        {
            w_Tape_Out_B_W_Ia_Pull_Out_Cd.setValue("80");                                                                                                                 //Natural: MOVE '80' TO W-IA-PULL-OUT-CD
            getReports().write(0, "**** ERROR RETURNED FROM CALL TO PIA3170 ***************");                                                                            //Natural: WRITE '**** ERROR RETURNED FROM CALL TO PIA3170 ***************'
            if (Global.isEscape()) return;
            getReports().write(0, "**** CALL TO ACTUARY MODULE FOR TPA COMMUTED VALUE *****");                                                                            //Natural: WRITE '**** CALL TO ACTUARY MODULE FOR TPA COMMUTED VALUE *****'
            if (Global.isEscape()) return;
            getReports().write(0, "**** RETURN CODE RECEIVED-------->",pnd_Pia3170_Error_01a);                                                                            //Natural: WRITE '**** RETURN CODE RECEIVED-------->' #PIA3170-ERROR-01A
            if (Global.isEscape()) return;
            getReports().write(0, "**** CONTRACT NUMBER ----->",w_Tape_Out_B_W_Ia_Contract_Number);                                                                       //Natural: WRITE '**** CONTRACT NUMBER ----->' W-IA-CONTRACT-NUMBER
            if (Global.isEscape()) return;
            //*  04/06 RESTORE THE CURRENT PAYMENT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Master_Rec_Linkage_Pnd_Rec5.getValue("*").setValue(pnd_Sve_Rec5.getValue("*"));                                                                               //Natural: ASSIGN #REC5 ( * ) := #SVE-REC5 ( * )
    }
    private void sub_Call_Naz() throws Exception                                                                                                                          //Natural: CALL-NAZ
    {
        if (BLNatReinput.isReinput()) return;

        //* *VE W-IA-PIN              TO #I-UNIQUE-ID-NAZ   /* 082017
        //*  082017
        pnd_Naz_Parm_Pnd_I_Unique_Id_Naz.setValue(pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7);                                                                                           //Natural: MOVE #W-IA-PIN-7 TO #I-UNIQUE-ID-NAZ
        pnd_Naz_Parm_Pnd_I_Ia_Contract_Naz.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                   //Natural: MOVE W-IA-CONTRACT-NUMBER TO #I-IA-CONTRACT-NAZ
        pnd_Naz_Parm_Pnd_I_Da_Contract_Naz.setValue(w_Tape_Out_B_W_Ia_Latest_Da_Tiaa);                                                                                    //Natural: MOVE W-IA-LATEST-DA-TIAA TO #I-DA-CONTRACT-NAZ
        //*  CALLNAT 'ITPN0160' USING #NAZ-PARM /* OLD MS CALL
        //*  USE ADAM FILE NAZ-IA-RSLT-DDM
        DbsUtil.callnat(Iadn171.class , getCurrentProcessState(), pnd_Naz_Parm);                                                                                          //Natural: CALLNAT 'IADN171' USING #NAZ-PARM
        if (condition(Global.isEscape())) return;
    }
    private void sub_Call_Iadn161_Non_Premium() throws Exception                                                                                                          //Natural: CALL-IADN161-NON-PREMIUM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np.reset();                                                                                                                //Natural: RESET #O-SETTLE-AMT-NP #O-FOUND-NP #O-25-OVER-SW-NP
        pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np.reset();
        pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np.reset();
        //*  ADDED 1/02
        //*  ADDED 1/02
        if (condition(pnd_Sve_Issu_Dte_Dd.notEquals(getZero())))                                                                                                          //Natural: IF #SVE-ISSU-DTE-DD NE 0
        {
            pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Dd_Np.setValue(pnd_Sve_Issu_Dte_Dd);                                                                                 //Natural: ASSIGN #I-IA-ISSUE-DTE-DD-NP := #SVE-ISSU-DTE-DD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Dd_Np.setValue(1);                                                                                                   //Natural: ASSIGN #I-IA-ISSUE-DTE-DD-NP := 01
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ia_Trn_Call_Parms_Pnd_I_Da_Contract_Np.setValue(w_Tape_Out_B_W_Ia_Latest_Da_Tiaa);                                                                            //Natural: MOVE W-IA-LATEST-DA-TIAA TO #I-DA-CONTRACT-NP
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Contract_Np.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                           //Natural: MOVE W-IA-CONTRACT-NUMBER TO #I-IA-CONTRACT-NP
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Issue_Dte_Np.setValue(pnd_W_Iss_Iss_Date);                                                                                         //Natural: MOVE #W-ISS-ISS-DATE TO #I-IA-ISSUE-DTE-NP
        DbsUtil.callnat(Iadn161.class , getCurrentProcessState(), pnd_Ia_Trn_Call_Parms);                                                                                 //Natural: CALLNAT 'IADN161' USING #IA-TRN-CALL-PARMS
        if (condition(Global.isEscape())) return;
    }
    private void sub_Accum_B_Rec_Totals() throws Exception                                                                                                                //Natural: ACCUM-B-REC-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED FOLLOWING  3/00
        if (condition(pnd_W_Death_Sw.equals("Y") || pnd_Sve_Iss_Delete_R.equals("9")))                                                                                    //Natural: IF #W-DEATH-SW = 'Y' OR #SVE-ISS-DELETE-R = '9'
        {
            pnd_Summary_Report_Counters_Pnd_Inactive_Payee_Sent_To_Qtrly.nadd(1);                                                                                         //Natural: ADD 1 TO #INACTIVE-PAYEE-SENT-TO-QTRLY
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD  3/00
        if (condition(DbsUtil.maskMatches(w_Tape_Out_B_W_Ia_Contract_Number,"'IP'")))                                                                                     //Natural: IF W-IA-CONTRACT-NUMBER = MASK ( 'IP' )
        {
            pnd_W_Total_Record_W_Ia_Ipro_Open_Value.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                                   //Natural: ASSIGN W-IA-IPRO-OPEN-VALUE := W-IA-IPRO-OPEN-VALUE + W-IA-OPEN-VALUE
            pnd_W_Total_Record_W_Ia_Ipro_Close_Value.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                                 //Natural: ASSIGN W-IA-IPRO-CLOSE-VALUE := W-IA-IPRO-CLOSE-VALUE + W-IA-CLOSE-VALUE
            if (condition(pnd_W_Rewrite_Sw.equals("Y")))                                                                                                                  //Natural: IF #W-REWRITE-SW = 'Y'
            {
                pnd_Summary_Report_Counters_Pnd_Ipro_Rewrite_Cnt.nadd(1);                                                                                                 //Natural: ADD 1 TO #IPRO-REWRITE-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28) || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))                                                                  //Natural: IF W-IA-OPTN-CODE = 28 OR = 30
        {
            pnd_W_Total_Record_W_Ia_Tpa_Open_Value.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                                    //Natural: ASSIGN W-IA-TPA-OPEN-VALUE := W-IA-TPA-OPEN-VALUE + W-IA-OPEN-VALUE
            pnd_W_Total_Record_W_Ia_Tpa_Close_Value.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                                  //Natural: ASSIGN W-IA-TPA-CLOSE-VALUE := W-IA-TPA-CLOSE-VALUE + W-IA-CLOSE-VALUE
            if (condition(pnd_W_Rewrite_Sw.equals("Y")))                                                                                                                  //Natural: IF #W-REWRITE-SW = 'Y'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_Rewrite_Cnt.nadd(1);                                                                                                  //Natural: ADD 1 TO #TPA-REWRITE-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_B_W_Ia_Status_Flag.equals("N")))                                                                                                     //Natural: IF W-IA-STATUS-FLAG = 'N'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_Flag_N_Cnt.nadd(1);                                                                                                   //Natural: ADD 1 TO #TPA-FLAG-N-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(22)))                                                                                                            //Natural: IF W-IA-OPTN-CODE = 22
        {
            pnd_W_Total_Record_W_Ia_Pamp_I_Open_Value.nadd(w_Tape_Out_B_W_Ia_Open_Value);                                                                                 //Natural: ASSIGN W-IA-P&I-OPEN-VALUE := W-IA-P&I-OPEN-VALUE + W-IA-OPEN-VALUE
            pnd_W_Total_Record_W_Ia_Pamp_I_Close_Value.nadd(w_Tape_Out_B_W_Ia_Close_Value);                                                                               //Natural: ASSIGN W-IA-P&I-CLOSE-VALUE := W-IA-P&I-CLOSE-VALUE + W-IA-CLOSE-VALUE
            if (condition(pnd_W_Rewrite_Sw.equals("Y")))                                                                                                                  //Natural: IF #W-REWRITE-SW = 'Y'
            {
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Rewrite_Cnt.nadd(1);                                                                                               //Natural: ADD 1 TO #P&I-REWRITE-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(w_Tape_Out_B_W_Ia_Contract_Number,"'IP'")))                                                                                     //Natural: IF W-IA-CONTRACT-NUMBER = MASK ( 'IP' )
        {
            if (condition(w_Tape_Out_B_W_Ia_Pull_Out_Cd.greater("  ")))                                                                                                   //Natural: IF W-IA-PULL-OUT-CD GT '  '
            {
                pnd_W_Total_Record_W_Ia_Ipro_Pullouts.nadd(1);                                                                                                            //Natural: ASSIGN W-IA-IPRO-PULLOUTS := W-IA-IPRO-PULLOUTS + 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(28) || w_Tape_Out_B_W_Ia_Optn_Code.equals(30)))                                                                  //Natural: IF W-IA-OPTN-CODE = 28 OR = 30
        {
            if (condition(w_Tape_Out_B_W_Ia_Pull_Out_Cd.greater("  ")))                                                                                                   //Natural: IF W-IA-PULL-OUT-CD GT '  '
            {
                pnd_W_Total_Record_W_Ia_Tpa_Pullouts.nadd(1);                                                                                                             //Natural: ASSIGN W-IA-TPA-PULLOUTS := W-IA-TPA-PULLOUTS + 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(w_Tape_Out_B_W_Ia_Optn_Code.equals(22)))                                                                                                            //Natural: IF W-IA-OPTN-CODE = 22
        {
            if (condition(w_Tape_Out_B_W_Ia_Pull_Out_Cd.greater("  ")))                                                                                                   //Natural: IF W-IA-PULL-OUT-CD GT '  '
            {
                pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts.compute(new ComputeParameters(false, pnd_W_Total_Record_W_Ia_Pamp_I_Pullouts), pnd_W_Total_Record_W_Ia_Tpa_Pullouts.add(1)); //Natural: ASSIGN W-IA-P&I-PULLOUTS := W-IA-TPA-PULLOUTS + 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_N_Rec_Totals() throws Exception                                                                                                                //Natural: ACCUM-N-REC-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  TPA CONTRACTS
        short decideConditionsMet2990 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN W-IA-TRN-TRANS-CODE = MASK ( 'T' )
        if (condition(DbsUtil.maskMatches(w_Tape_Out_N_W_Ia_Trn_Trans_Code,"'T'")))
        {
            decideConditionsMet2990++;
            //*   ADDED FOLLOWING 4/02
            if (condition(pnd_Hve_60_Trans.equals("Y")))                                                                                                                  //Natural: IF #HVE-60-TRANS = 'Y'
            {
                if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T1") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T2")))                                            //Natural: IF W-IA-TRN-TRANS-CODE = 'T1' OR = 'T2'
                {
                    pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Amt.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                     //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-SWEEP-AMT
                    pnd_Summary_Report_Counters_Pnd_Tpa_Sweep_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TPA-SWEEP-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF ADD 4/02
            //*  ADDED 4/02
            if (condition(pnd_Hve_60_Trans.notEquals("Y")))                                                                                                               //Natural: IF #HVE-60-TRANS NE 'Y'
            {
                if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T1") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T2")))                                            //Natural: IF W-IA-TRN-TRANS-CODE = 'T1' OR = 'T2'
                {
                    pnd_Summary_Report_Counters_Pnd_Tpa_Non_Premium_Amt_T1_T2.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                         //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-NON-PREMIUM-AMT-T1-T2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T4") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T6") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T7"))) //Natural: IF W-IA-TRN-TRANS-CODE = 'T4' OR = 'T6' OR = 'T7'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Amt.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                  //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-TO-TIAA-CREF-AMT
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Tiaa_Cref_Cnt.nadd(1);                                                                                             //Natural: ADD 1 TO #TPA-TO-TIAA-CREF-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T3")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'T3'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Carrier.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                    //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-TO-ALT-CARRIER
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Alt_Cnt.nadd(1);                                                                                                   //Natural: ADD 1 TO #TPA-TO-ALT-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T5")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'T5'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                           //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-TO-CASH
                pnd_Summary_Report_Counters_Pnd_Tpa_To_Cash_Cnt.nadd(1);                                                                                                  //Natural: ADD 1 TO #TPA-TO-CASH-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("T8")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'T8'
            {
                pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_T8.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                     //Natural: ADD W-IA-TRN-AMOUNT TO #TPA-WITHDRAWAL-T8
                pnd_Summary_Report_Counters_Pnd_Tpa_Withdrawal_Cnt.nadd(1);                                                                                               //Natural: ADD 1 TO #TPA-WITHDRAWAL-CNT
                //*  IPRO CONTRACTS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN W-IA-TRN-TRANS-CODE = MASK ( 'I' )
        else if (condition(DbsUtil.maskMatches(w_Tape_Out_N_W_Ia_Trn_Trans_Code,"'I'")))
        {
            decideConditionsMet2990++;
            //*   ADDED FOLLOWING 4/02
            if (condition(pnd_Hve_60_Trans.equals("Y")))                                                                                                                  //Natural: IF #HVE-60-TRANS = 'Y'
            {
                if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I1") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I2")))                                            //Natural: IF W-IA-TRN-TRANS-CODE = 'I1' OR = 'I2'
                {
                    pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Amt.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                    //Natural: ADD W-IA-TRN-AMOUNT TO #IPRO-SWEEP-AMT
                    pnd_Summary_Report_Counters_Pnd_Ipro_Sweep_Cnt.nadd(1);                                                                                               //Natural: ADD 1 TO #IPRO-SWEEP-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF ADD 4/02
            //*  ADDED 4/02
            if (condition(pnd_Hve_60_Trans.notEquals("Y")))                                                                                                               //Natural: IF #HVE-60-TRANS NE 'Y'
            {
                if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I1") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I2")))                                            //Natural: IF W-IA-TRN-TRANS-CODE = 'I1' OR = 'I2'
                {
                    pnd_Summary_Report_Counters_Pnd_Ipro_Non_Premium_Amt_I1_I2.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                        //Natural: ADD W-IA-TRN-AMOUNT TO #IPRO-NON-PREMIUM-AMT-I1-I2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I4")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'I4'
            {
                pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Amt.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                          //Natural: ADD W-IA-TRN-AMOUNT TO #IPRO-PMT-AMT
                pnd_Summary_Report_Counters_Pnd_Ipro_Pmt_Cnt.nadd(1);                                                                                                     //Natural: ADD 1 TO #IPRO-PMT-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I3")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'I3'
            {
                pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                    //Natural: ADD W-IA-TRN-AMOUNT TO #IPRO-INTEREST-CRED
                pnd_Summary_Report_Counters_Pnd_Ipro_Interest_Cred_Cnt.nadd(1);                                                                                           //Natural: ADD 1 TO #IPRO-INTEREST-CRED-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("I8")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'I8'
            {
                pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_I8.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                    //Natural: ADD W-IA-TRN-AMOUNT TO #IPRO-WITHDRAWAL-I8
                pnd_Summary_Report_Counters_Pnd_Ipro_Withdrawal_Cnt.nadd(1);                                                                                              //Natural: ADD 1 TO #IPRO-WITHDRAWAL-CNT
                //*  P&I CONTRACTS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN W-IA-TRN-TRANS-CODE = MASK ( 'P' )
        else if (condition(DbsUtil.maskMatches(w_Tape_Out_N_W_Ia_Trn_Trans_Code,"'P'")))
        {
            decideConditionsMet2990++;
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("P1") || w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("P2")))                                                //Natural: IF W-IA-TRN-TRANS-CODE = 'P1' OR = 'P2'
            {
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Non_Premium_Amt_P1.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                             //Natural: ADD W-IA-TRN-AMOUNT TO #P&I-NON-PREMIUM-AMT-P1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("P4")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'P4'
            {
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Amt.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                        //Natural: ADD W-IA-TRN-AMOUNT TO #P&I-PMT-AMT
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Pmt_Cnt.nadd(1);                                                                                                   //Natural: ADD 1 TO #P&I-PMT-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("P3")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'P3'
            {
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                  //Natural: ADD W-IA-TRN-AMOUNT TO #P&I-INTEREST-CRED
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Interest_Cred_Cnt.nadd(1);                                                                                         //Natural: ADD 1 TO #P&I-INTEREST-CRED-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(w_Tape_Out_N_W_Ia_Trn_Trans_Code.equals("P8")))                                                                                                 //Natural: IF W-IA-TRN-TRANS-CODE = 'P8'
            {
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_P8.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                  //Natural: ADD W-IA-TRN-AMOUNT TO #P&I-WITHDRAWAL-P8
                pnd_Summary_Report_Counters_Pnd_Pamp_I_Withdrawal_Cnt.nadd(1);                                                                                            //Natural: ADD 1 TO #P&I-WITHDRAWAL-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(DbsUtil.maskMatches(w_Tape_Out_N_W_Ia_Trn_Contract_Number,"'IP'")))                                                                                 //Natural: IF W-IA-TRN-CONTRACT-NUMBER = MASK ( 'IP' )
        {
            pnd_W_Total_Record_W_Ia_Ipro_Non_Premium_Amount.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                           //Natural: ASSIGN W-IA-IPRO-NON-PREMIUM-AMOUNT := W-IA-IPRO-NON-PREMIUM-AMOUNT + W-IA-TRN-AMOUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Sve_Optn_Code.equals(28) || pnd_Sve_Optn_Code.equals(30)))                                                                                  //Natural: IF #SVE-OPTN-CODE = 28 OR = 30
            {
                pnd_W_Total_Record_W_Ia_Tpa_Non_Premium_Amount.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                        //Natural: ASSIGN W-IA-TPA-NON-PREMIUM-AMOUNT := W-IA-TPA-NON-PREMIUM-AMOUNT + W-IA-TRN-AMOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Sve_Optn_Code.equals(22)))                                                                                                              //Natural: IF #SVE-OPTN-CODE = 22
                {
                    pnd_W_Total_Record_W_Ia_Pamp_I_Non_Premium_Amount.nadd(w_Tape_Out_N_W_Ia_Trn_Amount);                                                                 //Natural: ASSIGN W-IA-P&I-NON-PREMIUM-AMOUNT := W-IA-P&I-NON-PREMIUM-AMOUNT + W-IA-TRN-AMOUNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Cor_File() throws Exception                                                                                                                     //Natural: READ-COR-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  082017
        pnd_W_Ia_Pin.reset();                                                                                                                                             //Natural: RESET #W-IA-PIN
        //* *082015 - START
        if (condition(pnd_Core_Eof.getBoolean()))                                                                                                                         //Natural: IF #CORE-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Core_Pnd_Cor_Status_Cde.notEquals(" ")))                                                                                                    //Natural: IF #COR-STATUS-CDE NE ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Core_Pnd_Cor_Cntrct_Tot.greater(pnd_Cntrct_Payee)))                                                                                     //Natural: IF #COR-CNTRCT-TOT GT #CNTRCT-PAYEE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Core_Pnd_Cor_Cntrct_Tot.equals(pnd_Cntrct_Payee)))                                                                                      //Natural: IF #COR-CNTRCT-TOT = #CNTRCT-PAYEE
                {
                    //*      IF #COR-PIN IS (N7)                        /* 082017
                    //*  082017
                    //*  082017
                    if (condition(DbsUtil.is(pnd_Core_Pnd_Cor_Pin.getText(),"N12")))                                                                                      //Natural: IF #COR-PIN IS ( N12 )
                    {
                        pnd_W_Ia_Pin.compute(new ComputeParameters(false, pnd_W_Ia_Pin), pnd_Core_Pnd_Cor_Pin.val());                                                     //Natural: ASSIGN #W-IA-PIN := VAL ( #COR-PIN )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(4, pnd_Core);                                                                                                                             //Natural: READ WORK 4 ONCE #CORE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Core_Eof.setValue(true);                                                                                                                              //Natural: ASSIGN #CORE-EOF := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *READ CORE BY COR-SUPER-LOB-CNTRCT-PAYEE STARTING FROM
        //*     #W-CORE-SUPER
        //*   IF CORE.CNTRCT-NBR > #W-CORE-IA-CONTRACT-NO
        //*      AND
        //*      W-IA-PIN  NE MASK(NNNNNNN)
        //*    W-IA-PIN  := 0
        //*    ESCAPE ROUTINE
        //*  ELSE
        //*    IF CORE.CNTRCT-NBR > #W-CORE-IA-CONTRACT-NO
        //*      ESCAPE ROUTINE
        //*    ELSE
        //*      IF IA-CONTRACT-NO = CORE.CNTRCT-NBR AND
        //*        IA-PAYEE-NO-A  = CORE.CNTRCT-PAYEE-CDE
        //*        MOVE CORE.PH-UNIQUE-ID-NBR TO W-IA-PIN
        //*        ESCAPE ROUTINE
        //*      END-IF
        //*    END-IF
        //*  END-IF
        //* *END-READ
        //* *082015 - END
    }
    //*  082017
    //* COMMENTED 6/01 UNCOMMENTED 7/01
    private void sub_Read_Tpa_Transfer() throws Exception                                                                                                                 //Natural: READ-TPA-TRANSFER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr.setValue(pnd_W_Ia_Pin_Pnd_W_Ia_Pin_7);                                                                                     //Natural: ASSIGN #TPA-PH-UNQUE-ID-NBR := #W-IA-PIN-7
        pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                 //Natural: ASSIGN #TPA-IA-CNTRCT-NBR := W-IA-CONTRACT-NUMBER
        pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm);                                                                                 //Natural: ASSIGN #TPA-STTLMNT-DTE := #W-TRANS-CCYYMM
        vw_ia_Tpa.startDatabaseRead                                                                                                                                       //Natural: READ IA-TPA BY IA-TPA.PIN-CNTRCT-DATE STARTING FROM #IA-TPA-KEY
        (
        "READ03",
        new Wc[] { new Wc("PIN_CNTRCT_DATE", ">=", pnd_Ia_Tpa_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_DATE", "ASC") }
        );
        READ03:
        while (condition(vw_ia_Tpa.readNextRow("READ03")))
        {
            if (condition(ia_Tpa_Ia_Cntrct_Nbr.notEquals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)))                                                                          //Natural: IF IA-TPA.IA-CNTRCT-NBR NE #TPA-IA-CNTRCT-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 6/01
            if (condition(ia_Tpa_Ph_Unque_Id_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr) && ia_Tpa_Ia_Cntrct_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)      //Natural: IF IA-TPA.PH-UNQUE-ID-NBR = #TPA-PH-UNQUE-ID-NBR AND IA-TPA.IA-CNTRCT-NBR = #TPA-IA-CNTRCT-NBR AND IA-TPA.STTLMNT-DTE = #TPA-STTLMNT-DTE
                && ia_Tpa_Sttlmnt_Dte.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Sttlmnt_Dte)))
            {
                pnd_Fnd_Tpa_For_B.setValue("Y");                                                                                                                          //Natural: ASSIGN #FND-TPA-FOR-B := 'Y'
                pnd_Sve_Dest.setValue(ia_Tpa_Trnsfr_Dstntn_Cde);                                                                                                          //Natural: ASSIGN #SVE-DEST := IA-TPA.TRNSFR-DSTNTN-CDE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Tpa_Fund() throws Exception                                                                                                                     //Natural: READ-TPA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Tpa_Fund_Key.setValue(pnd_Ia_Tpa_Key);                                                                                                                     //Natural: ASSIGN #IA-TPA-FUND-KEY := #IA-TPA-KEY
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Fund_Code.setValue("   ");                                                                                                        //Natural: ASSIGN #TPA-FND-FUND-CODE := '   '
        pnd_I.setValue(1);                                                                                                                                                //Natural: ASSIGN #I := 1
        vw_ia_Tpa_Fund.startDatabaseRead                                                                                                                                  //Natural: READ IA-TPA-FUND BY IA-TPA-FUND.PIN-CNTRCT-DATE-FUND STARTING FROM #IA-TPA-FUND-KEY
        (
        "READ04",
        new Wc[] { new Wc("PIN_CNTRCT_DATE_FUND", ">=", pnd_Ia_Tpa_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_DATE_FUND", "ASC") }
        );
        READ04:
        while (condition(vw_ia_Tpa_Fund.readNextRow("READ04")))
        {
            if (condition(pnd_W_Tia_Sw.equals("Y") && pnd_W_Crf_Sw.equals("Y")))                                                                                          //Natural: IF #W-TIA-SW = 'Y' AND #W-CRF-SW = 'Y'
            {
                pnd_Sve_Dest.setValue("MIXD");                                                                                                                            //Natural: ASSIGN #SVE-DEST := 'MIXD'
                //*    ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Tpa_Fund_Ia_Cntrct_Nbr.notEquals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)))                                                                     //Natural: IF IA-TPA-FUND.IA-CNTRCT-NBR NE #TPA-IA-CNTRCT-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* ADDED 10/00
            //* ADDED 10/00
            if (condition(ia_Tpa_Fund_Ph_Unque_Id_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr) && ia_Tpa_Fund_Ia_Cntrct_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)  //Natural: IF IA-TPA-FUND.PH-UNQUE-ID-NBR = #TPA-PH-UNQUE-ID-NBR AND IA-TPA-FUND.IA-CNTRCT-NBR = #TPA-IA-CNTRCT-NBR AND IA-TPA-FUND.STTLMNT-DTE = #W-TRANS-CCYYMM
                && ia_Tpa_Fund_Sttlmnt_Dte.equals(pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm)))
            {
                w_Tape_Out_N_W_Ia_Fund_Cde_N_Rec.getValue(pnd_I).setValue(ia_Tpa_Fund_Cref_Fund_Cde);                                                                     //Natural: ASSIGN W-IA-FUND-CDE-N-REC ( #I ) := CREF-FUND-CDE
                w_Tape_Out_N_W_Ia_Fund_Pct_N_Rec.getValue(pnd_I).setValue(ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct);                                                              //Natural: ASSIGN W-IA-FUND-PCT-N-REC ( #I ) := CREF-FUND-TRNSFR-PCT
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                if (condition(ia_Tpa_Fund_Cref_Fund_Cde.equals("TIA") || ia_Tpa_Fund_Cref_Fund_Cde.equals("REA")))                                                        //Natural: IF IA-TPA-FUND.CREF-FUND-CDE = 'TIA' OR = 'REA'
                {
                    pnd_W_Tia_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-TIA-SW := 'Y'
                    pnd_Sve_Dest.setValue("TIAA");                                                                                                                        //Natural: ASSIGN #SVE-DEST := 'TIAA'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Crf_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-CRF-SW := 'Y'
                    //*      #SVE-DEST    :=  'CREF' /* ADDED 6/01 COMMENTED 7/01
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Tpa_Fund_For_B() throws Exception                                                                                                               //Natural: READ-TPA-FUND-FOR-B
    {
        if (BLNatReinput.isReinput()) return;

        pnd_I.setValue(1);                                                                                                                                                //Natural: ASSIGN #I := 1
        pnd_Ia_Tpa_Fund_Key.setValue(pnd_Ia_Tpa_Key);                                                                                                                     //Natural: ASSIGN #IA-TPA-FUND-KEY := #IA-TPA-KEY
        pnd_Ia_Tpa_Fund_Key_Pnd_Tpa_Fnd_Fund_Code.setValue("   ");                                                                                                        //Natural: ASSIGN #TPA-FND-FUND-CODE := '   '
        vw_ia_Tpa_Fund.startDatabaseRead                                                                                                                                  //Natural: READ IA-TPA-FUND BY IA-TPA-FUND.PIN-CNTRCT-DATE-FUND STARTING FROM #IA-TPA-FUND-KEY
        (
        "READ05",
        new Wc[] { new Wc("PIN_CNTRCT_DATE_FUND", ">=", pnd_Ia_Tpa_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_DATE_FUND", "ASC") }
        );
        READ05:
        while (condition(vw_ia_Tpa_Fund.readNextRow("READ05")))
        {
            if (condition(pnd_W_Tia_Sw.equals("Y") && pnd_W_Crf_Sw.equals("Y")))                                                                                          //Natural: IF #W-TIA-SW = 'Y' AND #W-CRF-SW = 'Y'
            {
                pnd_Sve_Dest.setValue("MIXD");                                                                                                                            //Natural: ASSIGN #SVE-DEST := 'MIXD'
                //*    ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ia_Tpa_Fund_Ia_Cntrct_Nbr.notEquals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)))                                                                     //Natural: IF IA-TPA-FUND.IA-CNTRCT-NBR NE #TPA-IA-CNTRCT-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* ADDED 10/00
            //* ADDED 10/00
            if (condition(ia_Tpa_Fund_Ph_Unque_Id_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ph_Unque_Id_Nbr) && ia_Tpa_Fund_Ia_Cntrct_Nbr.equals(pnd_Ia_Tpa_Key_Pnd_Tpa_Ia_Cntrct_Nbr)  //Natural: IF IA-TPA-FUND.PH-UNQUE-ID-NBR = #TPA-PH-UNQUE-ID-NBR AND IA-TPA-FUND.IA-CNTRCT-NBR = #TPA-IA-CNTRCT-NBR AND IA-TPA-FUND.STTLMNT-DTE = #W-NEXT-DUE-DTE-CCYYMM
                && ia_Tpa_Fund_Sttlmnt_Dte.equals(pnd_W_Next_Due_Dte_Ccyymmdd_Pnd_W_Next_Due_Dte_Ccyymm)))
            {
                w_Tape_Out_B_W_Ia_Fund_Cde_B_Rec.getValue(pnd_I).setValue(ia_Tpa_Fund_Cref_Fund_Cde);                                                                     //Natural: ASSIGN W-IA-FUND-CDE-B-REC ( #I ) := CREF-FUND-CDE
                w_Tape_Out_B_W_Ia_Fund_Pct_B_Rec.getValue(pnd_I).setValue(ia_Tpa_Fund_Cref_Fund_Trnsfr_Pct);                                                              //Natural: ASSIGN W-IA-FUND-PCT-B-REC ( #I ) := CREF-FUND-TRNSFR-PCT
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                if (condition(ia_Tpa_Fund_Cref_Fund_Cde.equals("TIA") || ia_Tpa_Fund_Cref_Fund_Cde.equals("REA")))                                                        //Natural: IF IA-TPA-FUND.CREF-FUND-CDE = 'TIA' OR = 'REA'
                {
                    pnd_W_Tia_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-TIA-SW := 'Y'
                    pnd_Sve_Dest.setValue("TIAA");                                                                                                                        //Natural: ASSIGN #SVE-DEST := 'TIAA'
                    //*  ADDED 6/01
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Crf_Sw.setValue("Y");                                                                                                                           //Natural: ASSIGN #W-CRF-SW := 'Y'
                    pnd_Sve_Dest.setValue("CREF");                                                                                                                        //Natural: ASSIGN #SVE-DEST := 'CREF'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_I8_Rtne() throws Exception                                                                                                                           //Natural: I8-RTNE
    {
        if (BLNatReinput.isReinput()) return;

        //*  02/06 DON't write terminate transaction
        if (condition(pnd_Sve_Contr_Type_Cd.equals("F")))                                                                                                                 //Natural: IF #SVE-CONTR-TYPE-CD = 'F'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        w_Tape_Out_N_W_Ia_Record_Type_Cd_N.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO W-IA-RECORD-TYPE-CD-N
        w_Tape_Out_N_W_Ia_Trn_Trans_Code.setValue("I8");                                                                                                                  //Natural: MOVE 'I8' TO W-IA-TRN-TRANS-CODE
        w_Tape_Out_N_W_Ia_Trn_Contract_Number.setValue(w_Tape_Out_B_W_Ia_Contract_Number);                                                                                //Natural: MOVE W-IA-CONTRACT-NUMBER TO W-IA-TRN-CONTRACT-NUMBER
        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        pnd_W_Date_Pnd_W_Wrk_Dd.setValue(1);                                                                                                                              //Natural: MOVE 01 TO #W-WRK-DD
        w_Tape_Out_N_W_Ia_Trn_Transaction_Date.setValue(pnd_W_Date);                                                                                                      //Natural: MOVE #W-DATE TO W-IA-TRN-TRANSACTION-DATE
        w_Tape_Out_N_W_Ia_Trn_Part_Date.setValue(pnd_W_Date);                                                                                                             //Natural: MOVE #W-DATE TO W-IA-TRN-PART-DATE
        w_Tape_Out_N_W_Ia_Trn_Product_Code.setValue("A");                                                                                                                 //Natural: MOVE 'A' TO W-IA-TRN-PRODUCT-CODE
        w_Tape_Out_N_W_Ia_Trn_Amount.setValue(pnd_W_Tot_Fin_Pmt_Amt);                                                                                                     //Natural: MOVE #W-TOT-FIN-PMT-AMT TO W-IA-TRN-AMOUNT
        w_Tape_Out_N_W_Ia_Trn_Amount.compute(new ComputeParameters(false, w_Tape_Out_N_W_Ia_Trn_Amount), w_Tape_Out_N_W_Ia_Trn_Amount.multiply(-1));                      //Natural: COMPUTE W-IA-TRN-AMOUNT = W-IA-TRN-AMOUNT * -1
        pnd_W_Total_Record_W_Ia_Ipro_Written_N.nadd(1);                                                                                                                   //Natural: ADD 1 TO W-IA-IPRO-WRITTEN-N
        w_Tape_Out.getValue("*").setValue(w_Tape_Out_N.getValue("*"));                                                                                                    //Natural: MOVE W-TAPE-OUT-N ( * ) TO W-TAPE-OUT ( * )
        //*  WRITE N TRANSACTION RECORD
        getWorkFiles().write(2, false, w_Tape_Out.getValue("*"));                                                                                                         //Natural: WRITE WORK FILE 02 W-TAPE-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-N-REC-TOTALS
        sub_Accum_N_Rec_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Accum_Active_Contracts() throws Exception                                                                                                            //Natural: ACCUM-ACTIVE-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet3264 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-BYPASS = 'BYPASS'
        if (condition(pnd_W_Bypass.equals("BYPASS")))
        {
            decideConditionsMet3264++;
            if (condition(((pnd_W_New_Issue.equals("Y") && pnd_Sve_Origin.notEquals(3)) && ((((pnd_Sve_Optn_Code.equals(22) || pnd_Sve_Optn_Code.equals(25))              //Natural: IF #W-NEW-ISSUE = 'Y' AND #SVE-ORIGIN NE 03 AND ( #SVE-OPTN-CODE = 22 OR = 25 OR = 27 OR = 28 OR = 30 )
                || pnd_Sve_Optn_Code.equals(27)) || pnd_Sve_Optn_Code.equals(28)) || pnd_Sve_Optn_Code.equals(30)))))
            {
                pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                    //Natural: COMPUTE #TOT-NEW-NOT-SEL-PMT = #TOT-NEW-NOT-SEL-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_New_Not_Sel_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                    //Natural: COMPUTE #TOT-NEW-NOT-SEL-DIV = #TOT-NEW-NOT-SEL-DIV + #W-TOTAL-GUARDIVD-AMOUNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Ia_Contract_Payee.greater(1) && pnd_Sve_Origin.notEquals(3)))                                                                           //Natural: IF #SVE-IA-CONTRACT-PAYEE GT 01 AND #SVE-ORIGIN NE 03
            {
                pnd_Summary_Report_Counters_Pnd_Tot_Ben_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                       //Natural: COMPUTE #TOT-BEN-GUAR-PMT = #TOT-BEN-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_Ben_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                            //Natural: COMPUTE #TOT-BEN-DIV = #TOT-BEN-DIV + #W-TOTAL-GUARDIVD-AMOUNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Optn_Code.equals(23)))                                                                                                                  //Natural: IF #SVE-OPTN-CODE = 23
            {
                pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                  //Natural: COMPUTE #TOT-FMLY-INC-GUAR-PMT = #TOT-FMLY-INC-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_Fmly_Inc_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                       //Natural: COMPUTE #TOT-FMLY-INC-DIV = #TOT-FMLY-INC-DIV + #W-TOTAL-GUARDIVD-AMOUNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Optn_Code.equals(22) && pnd_Sve_Origin.equals(3)))                                                                                      //Natural: IF #SVE-OPTN-CODE = 22 AND #SVE-ORIGIN = 03
            {
                pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                     //Natural: COMPUTE #TOT-ORGN3-GUAR-PMT = #TOT-ORGN3-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_Orgn3_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                          //Natural: COMPUTE #TOT-ORGN3-DIV = #TOT-ORGN3-DIV + #W-TOTAL-GUARDIVD-AMOUNT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED FOLLOWING CORRECT TOTAL AMT AT END OF REPORT TO MATCH TRL 6/03
            //*        TO BALANCE BACK TO TRAILER TOTAL                          6/03
            pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                      //Natural: COMPUTE #TOT-OLD-ISSU-GUAR-PMT = #TOT-OLD-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
            pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                           //Natural: COMPUTE #TOT-OLD-ISSU-DIV = #TOT-OLD-ISSU-DIV + #W-TOTAL-GUARDIVD-AMOUNT
        }                                                                                                                                                                 //Natural: WHEN #W-BYPASS NE 'BYPASS'
        else if (condition(pnd_W_Bypass.notEquals("BYPASS")))
        {
            decideConditionsMet3264++;
            if (condition(pnd_W_New_Issue.equals("Y")))                                                                                                                   //Natural: IF #W-NEW-ISSUE = 'Y'
            {
                pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                  //Natural: COMPUTE #TOT-NEW-ISSU-GUAR-PMT = #TOT-NEW-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_New_Issu_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                       //Natural: COMPUTE #TOT-NEW-ISSU-DIV = #TOT-NEW-ISSU-DIV + #W-TOTAL-GUARDIVD-AMOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Guar_Pmt.nadd(pnd_W_Total_Guarpymt_Amount);                                                                  //Natural: COMPUTE #TOT-OLD-ISSU-GUAR-PMT = #TOT-OLD-ISSU-GUAR-PMT + #W-TOTAL-GUARPYMT-AMOUNT
                pnd_Summary_Report_Counters_Pnd_Tot_Old_Issu_Div.nadd(pnd_W_Total_Guardivd_Amount);                                                                       //Natural: COMPUTE #TOT-OLD-ISSU-DIV = #TOT-OLD-ISSU-DIV + #W-TOTAL-GUARDIVD-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Last_Day() throws Exception                                                                                                                      //Natural: GET-LAST-DAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Date_Pnd_W_Wrk_Ccyy.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Ccyy);                                                                                            //Natural: MOVE #W-TRANS-CCYY TO #W-WRK-CCYY
        pnd_W_Date_Pnd_W_Wrk_Mm.setValue(pnd_W_Trans_Date_Pnd_W_Trans_Mm);                                                                                                //Natural: MOVE #W-TRANS-MM TO #W-WRK-MM
        FOR02:                                                                                                                                                            //Natural: FOR #W-WRK-DD 31 TO 1 STEP -1
        for (pnd_W_Date_Pnd_W_Wrk_Dd.setValue(31); condition(pnd_W_Date_Pnd_W_Wrk_Dd.greaterOrEqual(1)); pnd_W_Date_Pnd_W_Wrk_Dd.nsubtract(1))
        {
            if (condition(DbsUtil.maskMatches(pnd_W_Date,"MMDDYYYY")))                                                                                                    //Natural: IF #W-DATE = MASK ( MMDDYYYY )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Call_Iaanhp02_Fund_History() throws Exception                                                                                                        //Natural: CALL-IAANHP02-FUND-HISTORY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Call_Hist_Sw.equals("Y")))                                                                                                                      //Natural: IF #CALL-HIST-SW = 'Y'
        {
            pnd_Pymnt_Amt.reset();                                                                                                                                        //Natural: RESET #PYMNT-AMT
            pnd_Cntrct_Ppcn_Nbr_Parm.setValue(ia_Mast_Rec_Ia_Contract_No);                                                                                                //Natural: ASSIGN #CNTRCT-PPCN-NBR-PARM := IA-CONTRACT-NO
            pnd_Cntrct_Payee_Cde_Parm.setValue(ia_Mast_Rec_Ia_Payee_No);                                                                                                  //Natural: ASSIGN #CNTRCT-PAYEE-CDE-PARM := IA-PAYEE-NO
            pnd_Optn_Cde.setValue(w_Tape_Out_B_W_Ia_Optn_Code);                                                                                                           //Natural: ASSIGN #OPTN-CDE := W-IA-OPTN-CODE
            pnd_Issue_Dte.setValue(w_Tape_Out_B_W_Ia_Issue_Dte);                                                                                                          //Natural: ASSIGN #ISSUE-DTE := W-IA-ISSUE-DTE
            DbsUtil.callnat(Iaanhp02.class , getCurrentProcessState(), pnd_Cntrct_Ppcn_Nbr_Parm, pnd_Cntrct_Payee_Cde_Parm, w_Tape_Out_B_W_Ia_Optn_Code,                  //Natural: CALLNAT 'IAANHP02' #CNTRCT-PPCN-NBR-PARM #CNTRCT-PAYEE-CDE-PARM W-IA-OPTN-CODE W-IA-ISSUE-DTE #W-TRANS-CCYYMM-A #PYMNT-AMT
                w_Tape_Out_B_W_Ia_Issue_Dte, pnd_W_Trans_Date_Pnd_W_Trans_Ccyymm_A, pnd_Pymnt_Amt);
            if (condition(Global.isEscape())) return;
            pnd_Call_Hist_Sw.reset();                                                                                                                                     //Natural: RESET #CALL-HIST-SW
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Out_Cycle_New_Issue() throws Exception                                                                                                       //Natural: PROCESS-OUT-CYCLE-NEW-ISSUE
    {
        if (BLNatReinput.isReinput()) return;

        //*  FOR CALL TO MS FILE
        pnd_Naz_Parm_Pnd_I_Ia_Amount_Naz.setValue(pnd_Pia3170_Comm_Value);                                                                                                //Natural: MOVE #PIA3170-COMM-VALUE TO #I-IA-AMOUNT-NAZ
        //*                              TO COMPARE CALC AMT TO AMT ON MS FILE
        //*  FOR CALL TO
        pnd_Ia_Trn_Call_Parms_Pnd_I_Ia_Amount_Np.setValue(pnd_Pia3170_Comm_Value);                                                                                        //Natural: MOVE #PIA3170-COMM-VALUE TO #I-IA-AMOUNT-NP
        pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.setValue(0);                                                                                                                    //Natural: ASSIGN #O-SETTLE-AMT-NAZ := 0
        //*  CALL NAZ MODULE FOR ADAM NEW ISSUES OPEN VALUE
                                                                                                                                                                          //Natural: PERFORM CALL-NAZ
        sub_Call_Naz();
        if (condition(Global.isEscape())) {return;}
        //*  ADDED FOLLOWING 1/02 USE ADAM TOT ACCUM AMT FOR  OPENING VALUE
        //*  OPENING VALUE FOR NEW ISSUE
        if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greater(getZero())))                                                                                              //Natural: IF #O-SETTLE-AMT-NAZ GT 0
        {
            w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                        //Natural: ASSIGN W-IA-OUT-OF-CYCLE-OPEN-VALUE := #O-SETTLE-AMT-NAZ
            pnd_Pia3170_Comm_Value.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                                           //Natural: ASSIGN #PIA3170-COMM-VALUE := #O-SETTLE-AMT-NAZ
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 1/02 USE ADAM AMT FOR  OPENING VALUE
        if (condition(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.greaterOrEqual(pnd_Pia3170_Comm_Value)))                                                                          //Natural: IF #O-SETTLE-AMT-NAZ GE #PIA3170-COMM-VALUE
        {
            pnd_W_Est.compute(new ComputeParameters(false, pnd_W_Est), (pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz.subtract(pnd_Pia3170_Comm_Value)));                             //Natural: COMPUTE #W-EST = ( #O-SETTLE-AMT-NAZ - #PIA3170-COMM-VALUE )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Est.compute(new ComputeParameters(false, pnd_W_Est), (pnd_Pia3170_Comm_Value.subtract(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz)));                             //Natural: COMPUTE #W-EST = ( #PIA3170-COMM-VALUE - #O-SETTLE-AMT-NAZ )
        }                                                                                                                                                                 //Natural: END-IF
        //*  +$5 DIFF OR NOT FOUND ON MS
        if (condition(pnd_W_Est.greater(new DbsDecimal("5.00")) || pnd_Naz_Parm_Pnd_O_Found_Naz.equals("N")))                                                             //Natural: IF #W-EST > 5.00 OR #O-FOUND-NAZ = 'N'
        {
            //*  NOT FND ON MS CK NON PREMIUM
                                                                                                                                                                          //Natural: PERFORM CALL-IADN161-NON-PREMIUM
            sub_Call_Iadn161_Non_Premium();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Ia_Trn_Call_Parms_Pnd_O_Found_Np.equals("N") || pnd_Ia_Trn_Call_Parms_Pnd_O_25_Over_Sw_Np.equals("Y")))                                     //Natural: IF #O-FOUND-NP = 'N' OR #O-25-OVER-SW-NP = 'Y'
            {
                //*  $5 DIFF IN AMT FOR NEW-ISSU
                w_Tape_Out_B_W_Ia_Status_Flag.setValue("N");                                                                                                              //Natural: MOVE 'N' TO W-IA-STATUS-FLAG
                //*    MOVE #PIA3170-COMM-VALUE    TO W-IA-CLOSE-VALUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value.setValue(pnd_Ia_Trn_Call_Parms_Pnd_O_Settle_Amt_Np);                                                            //Natural: MOVE #O-SETTLE-AMT-NP TO W-IA-OUT-OF-CYCLE-OPEN-VALUE
                //*  NEW ISSU OUT CYCLE OPENING VALUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            w_Tape_Out_B_W_Ia_Out_Of_Cycle_Open_Value.setValue(pnd_Naz_Parm_Pnd_O_Settle_Amt_Naz);                                                                        //Natural: MOVE #O-SETTLE-AMT-NAZ TO W-IA-OUT-OF-CYCLE-OPEN-VALUE
            //*  NEW ISSU OUT CYCLE OPENING VALUE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().display(0, ia_Mast_Rec_Ia_Mstr_Key,ia_Mast_Rec_Iss_Last_Trans_Date,pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy,pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm); //Natural: DISPLAY IA-MSTR-KEY ISS-LAST-TRANS-DATE #W-CONV-TRANS-DATE-YYYY #W-CONV-TRANS-DATE-MM
        getReports().setDisplayColumns(0, ia_Mast_Rec_Ia_Mstr_Key,ia_Mast_Rec_Iss_Last_Trans_Date,pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Yyyy,pnd_W_Conv_Trans_Date_Pnd_W_Conv_Trans_Date_Mm);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66");
        Global.format(1, "LS=132 PS=56");
        Global.format(2, "LS=132 PS=56");
    }
}
