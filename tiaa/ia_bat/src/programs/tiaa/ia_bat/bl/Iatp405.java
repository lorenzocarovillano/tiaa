/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:38:17 PM
**        * FROM NATURAL PROGRAM : Iatp405
************************************************************
**        * FILE NAME            : Iatp405.java
**        * CLASS NAME           : Iatp405
**        * INSTANCE NAME        : Iatp405
************************************************************
************************************************************************
* PROGRAM: IATP405
* DATE   : 02/18/98
* AUTHOR : LEN B
* DESC   : THIS PROGRAM WRITES THE DAILY ERROR REPORT AND DAILY
*        : IVC ERROR REPORT
* HISTORY:
* 08/2015  RC :  COR NAAD DECOMM. USE PSGM001
* 04/2016  RC :  CHANGED TO CALL MDMN100A
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp405 extends BLNatBase
{
    // Data Areas
    private LdaIatl405 ldaIatl405;
    private PdaIaaa299a pdaIaaa299a;
    private PdaIaaa202h pdaIaaa202h;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Trnsfr_Sw_Rqst;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id;
    private DbsField iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde;
    private DbsField iaa_Trnsfr_Sw_Rqst_Ia_Super_De_01;
    private DbsField pnd_Start_Key;

    private DbsGroup pnd_Start_Key__R_Field_1;
    private DbsField pnd_Start_Key_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Start_Key_Pnd_Rcrd_Type_Cde;
    private DbsField pnd_Start_Key_Pnd_Ia_Frm_Cntrct;
    private DbsField pnd_Start_Key_Pnd_Ia_Frm_Payee;
    private DbsField pnd_End_Key;

    private DbsGroup pnd_End_Key__R_Field_2;
    private DbsField pnd_End_Key_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_End_Key_Pnd_Rcrd_Type_Cde;
    private DbsField pnd_End_Key_Pnd_Ia_Frm_Cntrct;
    private DbsField pnd_End_Key_Pnd_Ia_Frm_Payee;

    private DbsGroup pnd_L;
    private DbsField pnd_L_Pnd_First_Switch;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Trnsfr;
    private DbsField pnd_Const_Pnd_Rcrd_Type_Switch;
    private DbsField pnd_Trnsfr_Sw;
    private DbsField pnd_I;
    private DbsField pnd_Error_Remarks;
    private DbsField pnd_Tot_Err_Rec;
    private DbsField pnd_Tot_Ivc_Err;
    private DbsField pnd_Dashes_A;
    private DbsField pnd_Last_Name;
    private DbsField pnd_First_Name;
    private DbsField pnd_Middle_Name;
    private DbsField pnd_Stored_Cntrct;
    private DbsField pnd_Todays_Date;
    private DbsField pnd_First_Of_Month_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatl405 = new LdaIatl405();
        registerRecord(ldaIatl405);
        localVariables = new DbsRecord();
        pdaIaaa299a = new PdaIaaa299a(localVariables);
        pdaIaaa202h = new PdaIaaa202h(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_iaa_Trnsfr_Sw_Rqst = new DataAccessProgramView(new NameInfo("vw_iaa_Trnsfr_Sw_Rqst", "IAA-TRNSFR-SW-RQST"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO");
        iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Id", "RQST-ID", FieldType.STRING, 34, 
            RepeatingFieldStrategy.None, "RQST_ID");
        iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_EFFCTV_DTE");
        iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "RQST_LST_ACTVTY_DTE");
        iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IA_FRM_CNTRCT");
        iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "IA_UNIQUE_ID");
        iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "XFR_RJCTN_CDE");
        iaa_Trnsfr_Sw_Rqst_Ia_Super_De_01 = vw_iaa_Trnsfr_Sw_Rqst.getRecord().newFieldInGroup("iaa_Trnsfr_Sw_Rqst_Ia_Super_De_01", "IA-SUPER-DE-01", FieldType.BINARY, 
            17, RepeatingFieldStrategy.None, "IA_SUPER_DE_01");
        iaa_Trnsfr_Sw_Rqst_Ia_Super_De_01.setSuperDescriptor(true);
        registerRecord(vw_iaa_Trnsfr_Sw_Rqst);

        pnd_Start_Key = localVariables.newFieldInRecord("pnd_Start_Key", "#START-KEY", FieldType.STRING, 17);

        pnd_Start_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Start_Key__R_Field_1", "REDEFINE", pnd_Start_Key);
        pnd_Start_Key_Pnd_Rqst_Effctv_Dte = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", FieldType.DATE);
        pnd_Start_Key_Pnd_Rcrd_Type_Cde = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Start_Key_Pnd_Ia_Frm_Cntrct = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 
            10);
        pnd_Start_Key_Pnd_Ia_Frm_Payee = pnd_Start_Key__R_Field_1.newFieldInGroup("pnd_Start_Key_Pnd_Ia_Frm_Payee", "#IA-FRM-PAYEE", FieldType.STRING, 
            2);
        pnd_End_Key = localVariables.newFieldInRecord("pnd_End_Key", "#END-KEY", FieldType.STRING, 17);

        pnd_End_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_End_Key__R_Field_2", "REDEFINE", pnd_End_Key);
        pnd_End_Key_Pnd_Rqst_Effctv_Dte = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", FieldType.DATE);
        pnd_End_Key_Pnd_Rcrd_Type_Cde = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Pnd_Rcrd_Type_Cde", "#RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_End_Key_Pnd_Ia_Frm_Cntrct = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Pnd_Ia_Frm_Cntrct", "#IA-FRM-CNTRCT", FieldType.STRING, 10);
        pnd_End_Key_Pnd_Ia_Frm_Payee = pnd_End_Key__R_Field_2.newFieldInGroup("pnd_End_Key_Pnd_Ia_Frm_Payee", "#IA-FRM-PAYEE", FieldType.STRING, 2);

        pnd_L = localVariables.newGroupInRecord("pnd_L", "#L");
        pnd_L_Pnd_First_Switch = pnd_L.newFieldInGroup("pnd_L_Pnd_First_Switch", "#FIRST-SWITCH", FieldType.BOOLEAN, 1);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Pnd_Rcrd_Type_Trnsfr = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Trnsfr", "#RCRD-TYPE-TRNSFR", FieldType.STRING, 1);
        pnd_Const_Pnd_Rcrd_Type_Switch = pnd_Const.newFieldInGroup("pnd_Const_Pnd_Rcrd_Type_Switch", "#RCRD-TYPE-SWITCH", FieldType.STRING, 1);
        pnd_Trnsfr_Sw = localVariables.newFieldInRecord("pnd_Trnsfr_Sw", "#TRNSFR-SW", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Error_Remarks = localVariables.newFieldInRecord("pnd_Error_Remarks", "#ERROR-REMARKS", FieldType.STRING, 62);
        pnd_Tot_Err_Rec = localVariables.newFieldInRecord("pnd_Tot_Err_Rec", "#TOT-ERR-REC", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Ivc_Err = localVariables.newFieldInRecord("pnd_Tot_Ivc_Err", "#TOT-IVC-ERR", FieldType.PACKED_DECIMAL, 9);
        pnd_Dashes_A = localVariables.newFieldInRecord("pnd_Dashes_A", "#DASHES-A", FieldType.STRING, 62);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 19);
        pnd_First_Name = localVariables.newFieldInRecord("pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 10);
        pnd_Middle_Name = localVariables.newFieldInRecord("pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 3);
        pnd_Stored_Cntrct = localVariables.newFieldInRecord("pnd_Stored_Cntrct", "#STORED-CNTRCT", FieldType.STRING, 10);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.DATE);
        pnd_First_Of_Month_Dte = localVariables.newFieldInRecord("pnd_First_Of_Month_Dte", "#FIRST-OF-MONTH-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trnsfr_Sw_Rqst.reset();

        ldaIatl405.initializeValues();

        localVariables.reset();
        pnd_Const_Pnd_Rcrd_Type_Trnsfr.setInitialValue("1");
        pnd_Const_Pnd_Rcrd_Type_Switch.setInitialValue("2");
        pnd_Trnsfr_Sw.setInitialValue("TRANSFER");
        pnd_Stored_Cntrct.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iatp405() throws Exception
    {
        super("Iatp405");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 55;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        //*  COR NAAD MQ OPEN  04/2016
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  ======================================================================
        //*                           START OF PROGRAM
        //*  ======================================================================
        pnd_Dashes_A.moveAll("-");                                                                                                                                        //Natural: MOVE ALL '-' TO #DASHES-A
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATES
        sub_Get_Control_Dates();
        if (condition(Global.isEscape())) {return;}
        pnd_Start_Key_Pnd_Rqst_Effctv_Dte.setValue(pnd_First_Of_Month_Dte);                                                                                               //Natural: ASSIGN #START-KEY.#RQST-EFFCTV-DTE := #FIRST-OF-MONTH-DTE
        pnd_Start_Key_Pnd_Rcrd_Type_Cde.setValue(" ");                                                                                                                    //Natural: ASSIGN #START-KEY.#RCRD-TYPE-CDE := ' '
        pnd_Start_Key_Pnd_Ia_Frm_Cntrct.setValue(" ");                                                                                                                    //Natural: ASSIGN #START-KEY.#IA-FRM-CNTRCT := ' '
        pnd_Start_Key_Pnd_Ia_Frm_Payee.setValue(" ");                                                                                                                     //Natural: ASSIGN #START-KEY.#IA-FRM-PAYEE := ' '
        pnd_End_Key_Pnd_Rqst_Effctv_Dte.setValue(pnd_Todays_Date);                                                                                                        //Natural: ASSIGN #END-KEY.#RQST-EFFCTV-DTE := #TODAYS-DATE
        pnd_End_Key_Pnd_Rcrd_Type_Cde.setValue("H'FF'");                                                                                                                  //Natural: ASSIGN #END-KEY.#RCRD-TYPE-CDE := H'FF'
        pnd_End_Key_Pnd_Ia_Frm_Cntrct.setValue("H'FF'");                                                                                                                  //Natural: ASSIGN #END-KEY.#IA-FRM-CNTRCT := H'FF'
        pnd_End_Key_Pnd_Ia_Frm_Payee.setValue("H'FF'");                                                                                                                   //Natural: ASSIGN #END-KEY.#IA-FRM-PAYEE := H'FF'
        vw_iaa_Trnsfr_Sw_Rqst.startDatabaseRead                                                                                                                           //Natural: READ IAA-TRNSFR-SW-RQST WITH IA-SUPER-DE-01 EQ #START-KEY ENDING AT #END-KEY
        (
        "READ01",
        new Wc[] { new Wc("IA_SUPER_DE_01", ">=", pnd_Start_Key.getBinary(), "And", WcType.BY) ,
        new Wc("IA_SUPER_DE_01", "<=", pnd_End_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_01", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Trnsfr_Sw_Rqst.readNextRow("READ01")))
        {
            if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde.equals(" ") || iaa_Trnsfr_Sw_Rqst_Rqst_Lst_Actvty_Dte.notEquals(pnd_Todays_Date)))                             //Natural: REJECT IF XFR-RJCTN-CDE EQ ' ' OR RQST-LST-ACTVTY-DTE NE #TODAYS-DATE
            {
                continue;
            }
            getSort().writeSortInData(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde, iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct, iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte, iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee,  //Natural: END-ALL
                iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id, iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde, iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct);                                                                           //Natural: SORT BY RCRD-TYPE-CDE IA-FRM-CNTRCT USING RQST-EFFCTV-DTE IA-FRM-PAYEE IA-UNIQUE-ID XFR-RJCTN-CDE
        SORT01:
        while (condition(getSort().readSortOutData(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde, iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct, iaa_Trnsfr_Sw_Rqst_Rqst_Effctv_Dte, 
            iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee, iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id, iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde)))
        {
            //*    #IAXFR-RJCT-INVRSE-RCVD-TIME
            if (condition(iaa_Trnsfr_Sw_Rqst_Rcrd_Type_Cde.equals(pnd_Const_Pnd_Rcrd_Type_Switch) && ! (pnd_L_Pnd_First_Switch.getBoolean())))                            //Natural: IF IAA-TRNSFR-SW-RQST.RCRD-TYPE-CDE = #RCRD-TYPE-SWITCH AND NOT #FIRST-SWITCH
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_L_Pnd_First_Switch.setValue(true);                                                                                                                    //Natural: ASSIGN #FIRST-SWITCH := TRUE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-REPORT
            sub_Process_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-REPORT
            //*        01X IA-UNIQUE-ID (EM=9999999)
            //* *********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CORE
            //*         #MDMA100
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //*  04/2016 - END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
        //* ***********************************************************************
        //*  GET TODAY's date and Next Business date
        //* ***********************************************************************
        //* *=====================================================================
        //* *==================== AFTER SORT PROCESSING ==========================
        //* *=====================================================================
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        if (condition(pnd_Tot_Err_Rec.equals(getZero())))                                                                                                                 //Natural: IF #TOT-ERR-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,"No Rejected Requests found for :",pnd_Todays_Date, new ReportEditMask ("MM/DD/YYYY"));                            //Natural: WRITE ( 1 ) 'No Rejected Requests found for :' #TODAYS-DATE ( EM = MM/DD/YYYY )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,"Total Rejected Records =",pnd_Tot_Err_Rec);                                                                       //Natural: WRITE ( 1 ) 'Total Rejected Records =' #TOT-ERR-REC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        if (condition(pnd_Tot_Ivc_Err.equals(getZero())))                                                                                                                 //Natural: IF #TOT-IVC-ERR = 0
        {
            getReports().write(2, ReportOption.NOTITLE,"No IVC errors found for :",pnd_Todays_Date, new ReportEditMask ("MM/DD/YYYY"));                                   //Natural: WRITE ( 2 ) 'No IVC errors found for :' #TODAYS-DATE ( EM = MM/DD/YYYY )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,"Total IVC Rejected Records =",pnd_Tot_Ivc_Err);                                                                   //Natural: WRITE ( 2 ) 'Total IVC Rejected Records =' #TOT-IVC-ERR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Report() throws Exception                                                                                                                    //Natural: PROCESS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #ERROR-REMARKS
        pnd_Error_Remarks.reset();
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde.equals(" ")))                                                                                                      //Natural: IF XFR-RJCTN-CDE = ' '
        {
            pnd_Error_Remarks.setValue("*** NO REJECT CODE ***");                                                                                                         //Natural: MOVE '*** NO REJECT CODE ***' TO #ERROR-REMARKS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(ldaIatl405.getPnd_Iatl403_Pnd_Reject_Code().getValue(1,":",ldaIatl405.getPnd_Iatl403_Pnd_Nbr_Of_Reject_Codes())),           //Natural: EXAMINE #REJECT-CODE ( 1:#NBR-OF-REJECT-CODES ) FOR XFR-RJCTN-CDE GIVING INDEX #I
                new ExamineSearch(iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde), new ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I = 0
            {
                pnd_Error_Remarks.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*** UNKNOWN ERROR (", iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde, ") ***"));            //Natural: COMPRESS '*** UNKNOWN ERROR (' XFR-RJCTN-CDE ') ***' INTO #ERROR-REMARKS LEAVING NO SPACE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Remarks.setValue(ldaIatl405.getPnd_Iatl403_Pnd_Reject_Desc().getValue(pnd_I));                                                                  //Natural: MOVE #REJECT-DESC ( #I ) TO #ERROR-REMARKS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  IVC ERROR REPORT
        if (condition(iaa_Trnsfr_Sw_Rqst_Xfr_Rjctn_Cde.equals("IV")))                                                                                                     //Natural: IF XFR-RJCTN-CDE = 'IV'
        {
            //*  (EM=99)
            pnd_Tot_Ivc_Err.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-IVC-ERR
            getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(1),iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct,new ColumnSpacing(2),iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee,new     //Natural: WRITE ( 2 ) 01X IA-FRM-CNTRCT 02X IA-FRM-PAYEE 02X #ERROR-REMARKS
                ColumnSpacing(2),pnd_Error_Remarks);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Err_Rec.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-ERR-REC
            //*  1-8
            //*  (EM=99)
                                                                                                                                                                          //Natural: PERFORM READ-CORE
            sub_Read_Core();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),iaa_Trnsfr_Sw_Rqst_Ia_Frm_Cntrct,new ColumnSpacing(1),iaa_Trnsfr_Sw_Rqst_Ia_Frm_Payee,new     //Natural: WRITE ( 1 ) 01X IA-FRM-CNTRCT 01X IA-FRM-PAYEE 02X IA-UNIQUE-ID ( EM = 999999999999 ) 01X #LAST-NAME 01X #FIRST-NAME 01X #MIDDLE-NAME 01X #ERROR-REMARKS
                ColumnSpacing(2),iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id, new ReportEditMask ("999999999999"),new ColumnSpacing(1),pnd_Last_Name,new ColumnSpacing(1),pnd_First_Name,new 
                ColumnSpacing(1),pnd_Middle_Name,new ColumnSpacing(1),pnd_Error_Remarks);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROCESS-REPORT
    }
    private void sub_Read_Core() throws Exception                                                                                                                         //Natural: READ-CORE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        //*  04/2016 - START
        //*  082017
        pnd_Last_Name.reset();                                                                                                                                            //Natural: RESET #LAST-NAME #MDMA101 #FIRST-NAME #MIDDLE-NAME
        pdaMdma101.getPnd_Mdma101().reset();
        pnd_First_Name.reset();
        pnd_Middle_Name.reset();
        //* * MOVE EDITED IA-UNIQUE-ID (EM=9999999) TO #I-PIN-A7
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_A12().setValueEdited(iaa_Trnsfr_Sw_Rqst_Ia_Unique_Id,new ReportEditMask("999999999999"));                                     //Natural: MOVE EDITED IA-UNIQUE-ID ( EM = 999999999999 ) TO #I-PIN-A12
        //*   CALLNAT 'MDMN100A' #MDMA100
        //*  082017 END
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #O-RETURN-CODE NE '0000'
        {
            pnd_Last_Name.moveAll("***** UNKNOWN *****");                                                                                                                 //Natural: MOVE ALL '***** UNKNOWN *****' TO #LAST-NAME
            pnd_First_Name.setValue("** UNKNOWN");                                                                                                                        //Natural: MOVE '** UNKNOWN' TO #FIRST-NAME
            pnd_Middle_Name.moveAll("X");                                                                                                                                 //Natural: MOVE ALL 'X' TO #MIDDLE-NAME
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Last_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                                                              //Natural: MOVE #O-LAST-NAME TO #LAST-NAME
        pnd_First_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                                                                            //Natural: MOVE #O-FIRST-NAME TO #FIRST-NAME
        pnd_Middle_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                                                                          //Natural: MOVE #O-MIDDLE-NAME TO #MIDDLE-NAME
        //*  READ COR-XREF-PH-VIEW BY COR-SUPER-PIN-VIP
        //*      STARTING FROM IA-UNIQUE-ID
        //*    /*
        //*    IF IA-UNIQUE-ID = PH-UNIQUE-ID-NBR
        //*      MOVE PH-LAST-NME  TO #LAST-NAME
        //*      MOVE PH-FIRST-NME TO #FIRST-NAME
        //*      MOVE PH-MDDLE-NME TO #MIDDLE-NAME
        //*      ESCAPE BOTTOM
        //*    ELSE
        //*      MOVE ALL '***** UNKNOWN *****' TO #LAST-NAME
        //*      MOVE '** UNKNOWN'              TO #FIRST-NAME
        //*      MOVE ALL 'X'                   TO #MIDDLE-NAME
        //*    END-IF
        //*    /*
        //*    ESCAPE BOTTOM
        //*    /*
        //*  END-READ
        //*  READ-CORE
    }
    private void sub_Get_Control_Dates() throws Exception                                                                                                                 //Natural: GET-CONTROL-DATES
    {
        if (BLNatReinput.isReinput()) return;

        pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde().setValue("AA");                                                                                                        //Natural: ASSIGN #IAAN299A-IN.CNTRL-CDE := 'AA'
                                                                                                                                                                          //Natural: PERFORM #GET-CONTROL-REC
        sub_Pnd_Get_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte().equals(getZero()) || pdaIaaa299a.getPnd_Iaan299a_Out_Pnd_Return_Code().equals("E")))         //Natural: IF #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE = 0 OR #IAAN299A-OUT.#RETURN-CODE = 'E'
        {
            getReports().write(1, ReportOption.NOTITLE,"Control Record 'AA' returned an invalid date");                                                                   //Natural: WRITE ( 1 ) 'Control Record "AA" returned an invalid date'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Todays date   = ",pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                        //Natural: WRITE ( 1 ) 'Todays date   = ' #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Fatal error Program Terminated");                                                                                 //Natural: WRITE ( 1 ) 'Fatal error Program Terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_First_Of_Month_Dte.setValue(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                                                      //Natural: ASSIGN #FIRST-OF-MONTH-DTE := #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa299a.getPnd_Iaan299a_In_Cntrl_Cde().setValue("DC");                                                                                                        //Natural: ASSIGN #IAAN299A-IN.CNTRL-CDE := 'DC'
                                                                                                                                                                          //Natural: PERFORM #GET-CONTROL-REC
        sub_Pnd_Get_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte().equals(getZero()) || pdaIaaa299a.getPnd_Iaan299a_Out_Pnd_Return_Code().equals("E")))         //Natural: IF #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE = 0 OR #IAAN299A-OUT.#RETURN-CODE = 'E'
        {
            getReports().write(1, ReportOption.NOTITLE,"Control Record 'DC' returned an invalid date");                                                                   //Natural: WRITE ( 1 ) 'Control Record "DC" returned an invalid date'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Todays date   = ",pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                        //Natural: WRITE ( 1 ) 'Todays date   = ' #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Fatal error Program Terminated");                                                                                 //Natural: WRITE ( 1 ) 'Fatal error Program Terminated'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Todays_Date.setValue(pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte());                                                                             //Natural: ASSIGN #TODAYS-DATE := #IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTROL-DATES
    }
    private void sub_Pnd_Get_Control_Rec() throws Exception                                                                                                               //Natural: #GET-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FETCH LATEST CONTROL DATE
        DbsUtil.callnat(Iaan299a.class , getCurrentProcessState(), pdaIaaa299a.getPnd_Iaan299a_In(), pdaIaaa299a.getPnd_Iaan299a_Out(), pdaIaaa202h.getPnd_Iaa_Cntrl_Rcrd_1()); //Natural: CALLNAT 'IAAN299A' #IAAN299A-IN #IAAN299A-OUT #IAA-CNTRL-RCRD-1
        if (condition(Global.isEscape())) return;
        //*  #GET-CONTROL-REC
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_L_Pnd_First_Switch.getBoolean()))                                                                                                   //Natural: IF #FIRST-SWITCH
                    {
                        pnd_Trnsfr_Sw.setValue("SWITCH");                                                                                                                 //Natural: ASSIGN #TRNSFR-SW := 'SWITCH'
                        //*   PIN EXP 06/2017
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATU(),new ColumnSpacing(19),"IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATU 19X 'IA ADMINISTRATION - POST SETTLEMENT FLEXIBILITIES SYSTEM' 12X 'PAGE       :  ' *PAGE-NUMBER ( 1 ) / 01X 'RUN TIME : ' *TIMX 33X 'REJECT / ERROR REPORT FOR' 29X 'PROGRAM ID : ' *PROGRAM / 47X #TRNSFR-SW 'REQUESTS PROCESSED ON' #TODAYS-DATE ( EM = MM/DD/YYYY ) /// 01X '  FROM     PYE  UNIQUE' 13X ' LAST           FIRST   INI' 31X 'REASON FOR ' / 01X 'CONTRACT   CDE    ID' 16X 'NAME            NAME            ' 24X '  REJECT/ERROR'/ 02X 'NUMBER' / 01X '---------- --- ------------ ------------------- ---------- ---' 01X #DASHES-A /
                        ColumnSpacing(12),"PAGE       :  ",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new 
                        ColumnSpacing(33),"REJECT / ERROR REPORT FOR",new ColumnSpacing(29),"PROGRAM ID : ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(47),pnd_Trnsfr_Sw,"REQUESTS PROCESSED ON",pnd_Todays_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(1),"  FROM     PYE  UNIQUE",new ColumnSpacing(13)," LAST           FIRST   INI",new 
                        ColumnSpacing(31),"REASON FOR ",NEWLINE,new ColumnSpacing(1),"CONTRACT   CDE    ID",new ColumnSpacing(16),"NAME            NAME            ",new 
                        ColumnSpacing(24),"  REJECT/ERROR",NEWLINE,new ColumnSpacing(2),"NUMBER",NEWLINE,new ColumnSpacing(1),"---------- --- ------------ ------------------- ---------- ---",new 
                        ColumnSpacing(1),pnd_Dashes_A,NEWLINE);
                    //*    01X '  FROM     PYE  UNIQUE'
                    //*    08X ' LAST           FIRST   INI'
                    //*    26X 'REASON FOR '
                    //*    /
                    //*    01X 'CONTRACT   CDE    ID'
                    //*    11X 'NAME            NAME            '
                    //*    19X '  REJECT/ERROR'/
                    //*    02X 'NUMBER'  /
                    //*    01X '---------- --- ------- ------------------- ---------- ---'
                    //*    01X #DASHES-A /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(1),"RUN DATE : ",Global.getDATU(),new ColumnSpacing(18),"IVC ERROR REPORT",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR 01X 'RUN DATE : ' *DATU 18X 'IVC ERROR REPORT' 53X 'PAGE       :  ' *PAGE-NUMBER ( 2 ) / 01X 'RUN TIME : ' *TIMX 18X 'TRANSFER/SWITCH REQUESTS PROCESSED ON' #TODAYS-DATE ( EM = MM/DD/YYYY ) 21X 'PROGRAM ID : ' *PROGRAM /// 'CONTRACT   PAYEE                  ERROR DESCRIPTION' / '---------- -----' '-' ( 76 ) /
                        ColumnSpacing(53),"PAGE       :  ",getReports().getPageNumberDbs(2),NEWLINE,new ColumnSpacing(1),"RUN TIME : ",Global.getTIMX(),new 
                        ColumnSpacing(18),"TRANSFER/SWITCH REQUESTS PROCESSED ON",pnd_Todays_Date, new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(21),"PROGRAM ID : ",Global.getPROGRAM(),NEWLINE,NEWLINE,NEWLINE,"CONTRACT   PAYEE                  ERROR DESCRIPTION",NEWLINE,"---------- -----","-",new 
                        RepeatItem(76),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=55");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
    }
}
