/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:27 PM
**        * FROM NATURAL PROGRAM : Iaap730a
************************************************************
**        * FILE NAME            : Iaap730a.java
**        * CLASS NAME           : Iaap730a
**        * INSTANCE NAME        : Iaap730a
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP730A - ROLLOVER REPORTS (IAARPT40)            *
*                                                                    *
*   HISTORY                                                          *
*   9/03       - MADE CHANGES TO TOTAL REPORT FOR EGTRRA             *
*                DO SCAN ON 9/03
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap730a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup iaaa730a_Rec_In;
    private DbsField iaaa730a_Rec_In_Destination_Curr;
    private DbsField iaaa730a_Rec_In_Destination_Prev;
    private DbsField iaaa730a_Rec_In_Soc_Sec;
    private DbsField iaaa730a_Rec_In_Da_Contract;
    private DbsField iaaa730a_Rec_In_Ia_Contract;
    private DbsField iaaa730a_Rec_In_Contractual_Amt;
    private DbsField iaaa730a_Rec_In_Per_Dividend;
    private DbsField iaaa730a_Rec_In_Cash_Cd;
    private DbsField iaaa730a_Rec_In_Term_Cd;
    private DbsField iaaa730a_Rec_In_Hold_Cd;
    private DbsField iaaa730a_Rec_In_Pend_Cd;
    private DbsField iaaa730a_Rec_In_Check_Date;
    private DbsField iaaa730a_Rec_In_Mode;
    private DbsField iaaa730a_Rec_In_Coll_Cd;

    private DbsGroup detail_Line;
    private DbsField detail_Line_Pnd_Soc_Sec;
    private DbsField detail_Line_Pnd_Fiiler;
    private DbsField detail_Line_Pnd_Da_Contract;
    private DbsField detail_Line_Pnd_Fiiler1;
    private DbsField detail_Line_Pnd_Ia_Contract;
    private DbsField detail_Line_Pnd_Contractual_Amt;
    private DbsField detail_Line_Pnd_Per_Dividend;
    private DbsField detail_Line_Pnd_Fiiler2;
    private DbsField detail_Line_Pnd_Destination_Prev;
    private DbsField detail_Line_Pnd_Fiile2;
    private DbsField detail_Line_Pnd_Destination_Curr;
    private DbsField detail_Line_Pnd_Fiiler3;
    private DbsField detail_Line_Pnd_Cash_Cd;
    private DbsField detail_Line_Pnd_Fiiler4;
    private DbsField detail_Line_Pnd_Term_Cd;
    private DbsField detail_Line_Pnd_Fiiler5;
    private DbsField detail_Line_Pnd_Hold_Cd;
    private DbsField detail_Line_Filler10;
    private DbsField detail_Line_Pnd_Pend_Cd;
    private DbsField detail_Line_Filler11;
    private DbsField detail_Line_Pnd_Check_Date;
    private DbsField detail_Line_Pnd_Mode;
    private DbsField detail_Line_Filler13;
    private DbsField detail_Line_Pnd_Coll_Cd;

    private DbsGroup detail_Line_1;
    private DbsField detail_Line_1_Pnd_Soc_Sec_1;
    private DbsField detail_Line_1_Pnd_Fiiler;
    private DbsField detail_Line_1_Pnd_Ia_Contract_1;
    private DbsField detail_Line_1_Pnd_Contractual_Amt_1;
    private DbsField detail_Line_1_Pnd_Per_Dividend_1;
    private DbsField detail_Line_1_Pnd_Cntr_Div_1;
    private DbsField detail_Line_1_Pnd_Fiil2;
    private DbsField detail_Line_1_Pnd_Destination_Curr_1;
    private DbsField detail_Line_1_Pnd_Fiil3;
    private DbsField detail_Line_1_Pnd_Destination_Prev_1;
    private DbsField detail_Line_1_Pnd_Fiil5;
    private DbsField detail_Line_1_Pnd_Cash_Cd_1;
    private DbsField detail_Line_1_Pnd_Fiil6;
    private DbsField detail_Line_1_Pnd_Term_Cd_1;
    private DbsField detail_Line_1_Pnd_Fiil7;
    private DbsField detail_Line_1_Pnd_Hold_Cd_1;
    private DbsField detail_Line_1_Pnd_Fill8;
    private DbsField detail_Line_1_Pnd_Pend_Cd_1;
    private DbsField detail_Line_1_Pnd_Mode_1;

    private DbsGroup total_Line;
    private DbsField total_Line_Pnd_Total_Destination;
    private DbsField total_Line_Pnd_Fiiler;
    private DbsField total_Line_Pnd_Total_Payees;
    private DbsField total_Line_Pnd_F;
    private DbsField total_Line_Pnd_Total_Contractual_Amt;
    private DbsField total_Line_Pnd_F1;
    private DbsField total_Line_Pnd_Total_Per_Dividend;
    private DbsField total_Line_Pnd_F2;
    private DbsField total_Line_Pnd_Total_Amt;

    private DbsGroup grand_Line;
    private DbsField grand_Line_Pnd_Grand_Destination;
    private DbsField grand_Line_Pnd_Fiiler;
    private DbsField grand_Line_Pnd_Grand_Payees;
    private DbsField grand_Line_Pnd_F;
    private DbsField grand_Line_Pnd_Grand_Contractual_Amt;
    private DbsField grand_Line_Pnd_F2;
    private DbsField grand_Line_Pnd_Grand_Per_Dividend;
    private DbsField grand_Line_Pnd_F3;
    private DbsField grand_Line_Pnd_Grand_Amt;
    private DbsField tot_Destination;
    private DbsField tot_Payees;
    private DbsField tot_Contractual_Amt;
    private DbsField tot_Per_Dividend;
    private DbsField count;
    private DbsField tpa_Totals;
    private DbsField pnd_Save_Destination;
    private DbsField pnd_Ctr;
    private DbsField pnd_Check_Dt;

    private DbsGroup pnd_Check_Dt__R_Field_1;
    private DbsField pnd_Check_Dt_Pnd_Yyyymm;
    private DbsField pnd_Check_Dt_Pnd_Dd;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        iaaa730a_Rec_In = localVariables.newGroupInRecord("iaaa730a_Rec_In", "IAAA730A-REC-IN");
        iaaa730a_Rec_In_Destination_Curr = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Destination_Curr", "DESTINATION-CURR", FieldType.STRING, 4);
        iaaa730a_Rec_In_Destination_Prev = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Destination_Prev", "DESTINATION-PREV", FieldType.STRING, 4);
        iaaa730a_Rec_In_Soc_Sec = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Soc_Sec", "SOC-SEC", FieldType.NUMERIC, 9);
        iaaa730a_Rec_In_Da_Contract = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Da_Contract", "DA-CONTRACT", FieldType.STRING, 8);
        iaaa730a_Rec_In_Ia_Contract = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Ia_Contract", "IA-CONTRACT", FieldType.STRING, 10);
        iaaa730a_Rec_In_Contractual_Amt = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Contractual_Amt", "CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        iaaa730a_Rec_In_Per_Dividend = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Per_Dividend", "PER-DIVIDEND", FieldType.PACKED_DECIMAL, 9, 2);
        iaaa730a_Rec_In_Cash_Cd = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Cash_Cd", "CASH-CD", FieldType.STRING, 1);
        iaaa730a_Rec_In_Term_Cd = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Term_Cd", "TERM-CD", FieldType.STRING, 1);
        iaaa730a_Rec_In_Hold_Cd = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Hold_Cd", "HOLD-CD", FieldType.STRING, 1);
        iaaa730a_Rec_In_Pend_Cd = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Pend_Cd", "PEND-CD", FieldType.STRING, 1);
        iaaa730a_Rec_In_Check_Date = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Check_Date", "CHECK-DATE", FieldType.NUMERIC, 6);
        iaaa730a_Rec_In_Mode = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Mode", "MODE", FieldType.NUMERIC, 3);
        iaaa730a_Rec_In_Coll_Cd = iaaa730a_Rec_In.newFieldInGroup("iaaa730a_Rec_In_Coll_Cd", "COLL-CD", FieldType.STRING, 5);

        detail_Line = localVariables.newGroupInRecord("detail_Line", "DETAIL-LINE");
        detail_Line_Pnd_Soc_Sec = detail_Line.newFieldInGroup("detail_Line_Pnd_Soc_Sec", "#SOC-SEC", FieldType.NUMERIC, 9);
        detail_Line_Pnd_Fiiler = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler", "#FIILER", FieldType.STRING, 1);
        detail_Line_Pnd_Da_Contract = detail_Line.newFieldInGroup("detail_Line_Pnd_Da_Contract", "#DA-CONTRACT", FieldType.STRING, 8);
        detail_Line_Pnd_Fiiler1 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler1", "#FIILER1", FieldType.STRING, 3);
        detail_Line_Pnd_Ia_Contract = detail_Line.newFieldInGroup("detail_Line_Pnd_Ia_Contract", "#IA-CONTRACT", FieldType.STRING, 10);
        detail_Line_Pnd_Contractual_Amt = detail_Line.newFieldInGroup("detail_Line_Pnd_Contractual_Amt", "#CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        detail_Line_Pnd_Per_Dividend = detail_Line.newFieldInGroup("detail_Line_Pnd_Per_Dividend", "#PER-DIVIDEND", FieldType.PACKED_DECIMAL, 9, 2);
        detail_Line_Pnd_Fiiler2 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler2", "#FIILER2", FieldType.STRING, 2);
        detail_Line_Pnd_Destination_Prev = detail_Line.newFieldInGroup("detail_Line_Pnd_Destination_Prev", "#DESTINATION-PREV", FieldType.STRING, 4);
        detail_Line_Pnd_Fiile2 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiile2", "#FIILE2", FieldType.STRING, 1);
        detail_Line_Pnd_Destination_Curr = detail_Line.newFieldInGroup("detail_Line_Pnd_Destination_Curr", "#DESTINATION-CURR", FieldType.STRING, 4);
        detail_Line_Pnd_Fiiler3 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler3", "#FIILER3", FieldType.STRING, 2);
        detail_Line_Pnd_Cash_Cd = detail_Line.newFieldInGroup("detail_Line_Pnd_Cash_Cd", "#CASH-CD", FieldType.STRING, 1);
        detail_Line_Pnd_Fiiler4 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler4", "#FIILER4", FieldType.STRING, 4);
        detail_Line_Pnd_Term_Cd = detail_Line.newFieldInGroup("detail_Line_Pnd_Term_Cd", "#TERM-CD", FieldType.STRING, 1);
        detail_Line_Pnd_Fiiler5 = detail_Line.newFieldInGroup("detail_Line_Pnd_Fiiler5", "#FIILER5", FieldType.STRING, 2);
        detail_Line_Pnd_Hold_Cd = detail_Line.newFieldInGroup("detail_Line_Pnd_Hold_Cd", "#HOLD-CD", FieldType.STRING, 1);
        detail_Line_Filler10 = detail_Line.newFieldInGroup("detail_Line_Filler10", "FILLER10", FieldType.STRING, 4);
        detail_Line_Pnd_Pend_Cd = detail_Line.newFieldInGroup("detail_Line_Pnd_Pend_Cd", "#PEND-CD", FieldType.STRING, 2);
        detail_Line_Filler11 = detail_Line.newFieldInGroup("detail_Line_Filler11", "FILLER11", FieldType.STRING, 1);
        detail_Line_Pnd_Check_Date = detail_Line.newFieldInGroup("detail_Line_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        detail_Line_Pnd_Mode = detail_Line.newFieldInGroup("detail_Line_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        detail_Line_Filler13 = detail_Line.newFieldInGroup("detail_Line_Filler13", "FILLER13", FieldType.STRING, 1);
        detail_Line_Pnd_Coll_Cd = detail_Line.newFieldInGroup("detail_Line_Pnd_Coll_Cd", "#COLL-CD", FieldType.STRING, 5);

        detail_Line_1 = localVariables.newGroupInRecord("detail_Line_1", "DETAIL-LINE-1");
        detail_Line_1_Pnd_Soc_Sec_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Soc_Sec_1", "#SOC-SEC-1", FieldType.NUMERIC, 9);
        detail_Line_1_Pnd_Fiiler = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiiler", "#FIILER", FieldType.STRING, 1);
        detail_Line_1_Pnd_Ia_Contract_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Ia_Contract_1", "#IA-CONTRACT-1", FieldType.STRING, 8);
        detail_Line_1_Pnd_Contractual_Amt_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Contractual_Amt_1", "#CONTRACTUAL-AMT-1", FieldType.PACKED_DECIMAL, 
            9, 2);
        detail_Line_1_Pnd_Per_Dividend_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Per_Dividend_1", "#PER-DIVIDEND-1", FieldType.PACKED_DECIMAL, 
            9, 2);
        detail_Line_1_Pnd_Cntr_Div_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Cntr_Div_1", "#CNTR-DIV-1", FieldType.PACKED_DECIMAL, 10, 2);
        detail_Line_1_Pnd_Fiil2 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiil2", "#FIIL2", FieldType.STRING, 1);
        detail_Line_1_Pnd_Destination_Curr_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Destination_Curr_1", "#DESTINATION-CURR-1", FieldType.STRING, 
            4);
        detail_Line_1_Pnd_Fiil3 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiil3", "#FIIL3", FieldType.STRING, 1);
        detail_Line_1_Pnd_Destination_Prev_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Destination_Prev_1", "#DESTINATION-PREV-1", FieldType.STRING, 
            4);
        detail_Line_1_Pnd_Fiil5 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiil5", "#FIIL5", FieldType.STRING, 2);
        detail_Line_1_Pnd_Cash_Cd_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Cash_Cd_1", "#CASH-CD-1", FieldType.STRING, 1);
        detail_Line_1_Pnd_Fiil6 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiil6", "#FIIL6", FieldType.STRING, 4);
        detail_Line_1_Pnd_Term_Cd_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Term_Cd_1", "#TERM-CD-1", FieldType.STRING, 1);
        detail_Line_1_Pnd_Fiil7 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fiil7", "#FIIL7", FieldType.STRING, 2);
        detail_Line_1_Pnd_Hold_Cd_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Hold_Cd_1", "#HOLD-CD-1", FieldType.STRING, 1);
        detail_Line_1_Pnd_Fill8 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Fill8", "#FILL8", FieldType.STRING, 4);
        detail_Line_1_Pnd_Pend_Cd_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Pend_Cd_1", "#PEND-CD-1", FieldType.STRING, 2);
        detail_Line_1_Pnd_Mode_1 = detail_Line_1.newFieldInGroup("detail_Line_1_Pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 3);

        total_Line = localVariables.newGroupInRecord("total_Line", "TOTAL-LINE");
        total_Line_Pnd_Total_Destination = total_Line.newFieldInGroup("total_Line_Pnd_Total_Destination", "#TOTAL-DESTINATION", FieldType.STRING, 14);
        total_Line_Pnd_Fiiler = total_Line.newFieldInGroup("total_Line_Pnd_Fiiler", "#FIILER", FieldType.STRING, 2);
        total_Line_Pnd_Total_Payees = total_Line.newFieldInGroup("total_Line_Pnd_Total_Payees", "#TOTAL-PAYEES", FieldType.NUMERIC, 8);
        total_Line_Pnd_F = total_Line.newFieldInGroup("total_Line_Pnd_F", "#F", FieldType.STRING, 5);
        total_Line_Pnd_Total_Contractual_Amt = total_Line.newFieldInGroup("total_Line_Pnd_Total_Contractual_Amt", "#TOTAL-CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        total_Line_Pnd_F1 = total_Line.newFieldInGroup("total_Line_Pnd_F1", "#F1", FieldType.STRING, 7);
        total_Line_Pnd_Total_Per_Dividend = total_Line.newFieldInGroup("total_Line_Pnd_Total_Per_Dividend", "#TOTAL-PER-DIVIDEND", FieldType.PACKED_DECIMAL, 
            11, 2);
        total_Line_Pnd_F2 = total_Line.newFieldInGroup("total_Line_Pnd_F2", "#F2", FieldType.STRING, 1);
        total_Line_Pnd_Total_Amt = total_Line.newFieldInGroup("total_Line_Pnd_Total_Amt", "#TOTAL-AMT", FieldType.PACKED_DECIMAL, 12, 2);

        grand_Line = localVariables.newGroupInRecord("grand_Line", "GRAND-LINE");
        grand_Line_Pnd_Grand_Destination = grand_Line.newFieldInGroup("grand_Line_Pnd_Grand_Destination", "#GRAND-DESTINATION", FieldType.STRING, 11);
        grand_Line_Pnd_Fiiler = grand_Line.newFieldInGroup("grand_Line_Pnd_Fiiler", "#FIILER", FieldType.STRING, 4);
        grand_Line_Pnd_Grand_Payees = grand_Line.newFieldInGroup("grand_Line_Pnd_Grand_Payees", "#GRAND-PAYEES", FieldType.NUMERIC, 9);
        grand_Line_Pnd_F = grand_Line.newFieldInGroup("grand_Line_Pnd_F", "#F", FieldType.STRING, 5);
        grand_Line_Pnd_Grand_Contractual_Amt = grand_Line.newFieldInGroup("grand_Line_Pnd_Grand_Contractual_Amt", "#GRAND-CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        grand_Line_Pnd_F2 = grand_Line.newFieldInGroup("grand_Line_Pnd_F2", "#F2", FieldType.STRING, 7);
        grand_Line_Pnd_Grand_Per_Dividend = grand_Line.newFieldInGroup("grand_Line_Pnd_Grand_Per_Dividend", "#GRAND-PER-DIVIDEND", FieldType.PACKED_DECIMAL, 
            11, 2);
        grand_Line_Pnd_F3 = grand_Line.newFieldInGroup("grand_Line_Pnd_F3", "#F3", FieldType.STRING, 1);
        grand_Line_Pnd_Grand_Amt = grand_Line.newFieldInGroup("grand_Line_Pnd_Grand_Amt", "#GRAND-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        tot_Destination = localVariables.newFieldArrayInRecord("tot_Destination", "TOT-DESTINATION", FieldType.STRING, 4, new DbsArrayController(1, 7));
        tot_Payees = localVariables.newFieldArrayInRecord("tot_Payees", "TOT-PAYEES", FieldType.NUMERIC, 9, new DbsArrayController(1, 7));
        tot_Contractual_Amt = localVariables.newFieldArrayInRecord("tot_Contractual_Amt", "TOT-CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            7));
        tot_Per_Dividend = localVariables.newFieldArrayInRecord("tot_Per_Dividend", "TOT-PER-DIVIDEND", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            7));
        count = localVariables.newFieldInRecord("count", "COUNT", FieldType.NUMERIC, 1);
        tpa_Totals = localVariables.newFieldInRecord("tpa_Totals", "TPA-TOTALS", FieldType.STRING, 1);
        pnd_Save_Destination = localVariables.newFieldInRecord("pnd_Save_Destination", "#SAVE-DESTINATION", FieldType.STRING, 4);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Check_Dt = localVariables.newFieldInRecord("pnd_Check_Dt", "#CHECK-DT", FieldType.NUMERIC, 8);

        pnd_Check_Dt__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Dt__R_Field_1", "REDEFINE", pnd_Check_Dt);
        pnd_Check_Dt_Pnd_Yyyymm = pnd_Check_Dt__R_Field_1.newFieldInGroup("pnd_Check_Dt_Pnd_Yyyymm", "#YYYYMM", FieldType.NUMERIC, 6);
        pnd_Check_Dt_Pnd_Dd = pnd_Check_Dt__R_Field_1.newFieldInGroup("pnd_Check_Dt_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        grand_Line_Pnd_Grand_Destination.setInitialValue("GRAND TOTAL");
        tpa_Totals.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap730a() throws Exception
    {
        super("Iaap730a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 55 LS = 133;//Natural: FORMAT ( 1 ) PS = 55 LS = 133;//Natural: FORMAT ( 2 ) PS = 55 LS = 133;//Natural: FORMAT ( 3 ) PS = 55 LS = 133;//Natural: FORMAT ( 4 ) PS = 55 LS = 133;//Natural: FORMAT ( 5 ) PS = 55 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_Ctr.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #CTR
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 IAAA730A-REC-IN
        while (condition(getWorkFiles().read(1, iaaa730a_Rec_In)))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TPA-REPORT
            sub_Print_Tpa_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TPA-REPORT
            //*   TOT-DESTINATION (#I) :=          #SAVE-DESTINATION
            //* ********************
            //*  CHANGED FROM 6 TO 3 REMOVE IRA DEST 9/03
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  REMOVED FOLLOWING FROM FOR LOOP 9/03
        //*  ELSE
        //*  IF #I = 3 MOVE  'IRAC' TO #TOTAL-DESTINATION
        //*  ELSE
        //*   IF #I = 4 MOVE  'IRAT' TO #TOTAL-DESTINATION
        //*   ELSE
        //*     IF #I = 5 MOVE  'IRAX' TO #TOTAL-DESTINATION
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                total_Line_Pnd_Total_Destination.setValue("CASH");                                                                                                        //Natural: MOVE 'CASH' TO #TOTAL-DESTINATION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_I.equals(2)))                                                                                                                           //Natural: IF #I = 2
                {
                    total_Line_Pnd_Total_Destination.setValue("DTRA");                                                                                                    //Natural: MOVE 'DTRA' TO #TOTAL-DESTINATION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_I.equals(3)))                                                                                                                       //Natural: IF #I = 3
                    {
                        total_Line_Pnd_Total_Destination.setValue("RINV");                                                                                                //Natural: MOVE 'RINV' TO #TOTAL-DESTINATION
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   #TOTAL-DESTINATION        :=   TOT-DESTINATION(#I)
            total_Line_Pnd_Total_Payees.setValue(tot_Payees.getValue(pnd_I));                                                                                             //Natural: ASSIGN #TOTAL-PAYEES := TOT-PAYEES ( #I )
            total_Line_Pnd_Total_Contractual_Amt.setValue(tot_Contractual_Amt.getValue(pnd_I));                                                                           //Natural: ASSIGN #TOTAL-CONTRACTUAL-AMT := TOT-CONTRACTUAL-AMT ( #I )
            total_Line_Pnd_Total_Per_Dividend.setValue(tot_Per_Dividend.getValue(pnd_I));                                                                                 //Natural: ASSIGN #TOTAL-PER-DIVIDEND := TOT-PER-DIVIDEND ( #I )
            total_Line_Pnd_Total_Amt.compute(new ComputeParameters(false, total_Line_Pnd_Total_Amt), tot_Contractual_Amt.getValue(pnd_I).add(tot_Per_Dividend.getValue(pnd_I))); //Natural: ASSIGN #TOTAL-AMT := TOT-CONTRACTUAL-AMT ( #I ) + TOT-PER-DIVIDEND ( #I )
            //*  IF TOT-DESTINATION(#I) = ' '
            //*    IGNORE
            //*  ELSE
            getReports().write(2, ReportOption.NOTITLE,total_Line_Pnd_Total_Destination,total_Line_Pnd_Fiiler,total_Line_Pnd_Total_Payees,total_Line_Pnd_F,               //Natural: WRITE ( 2 ) TOTAL-LINE
                total_Line_Pnd_Total_Contractual_Amt,total_Line_Pnd_F1,total_Line_Pnd_Total_Per_Dividend,total_Line_Pnd_F2,total_Line_Pnd_Total_Amt);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        grand_Line_Pnd_Grand_Payees.setValue(tot_Payees.getValue(7));                                                                                                     //Natural: ASSIGN #GRAND-PAYEES := TOT-PAYEES ( 7 )
        grand_Line_Pnd_Grand_Contractual_Amt.setValue(tot_Contractual_Amt.getValue(7));                                                                                   //Natural: ASSIGN #GRAND-CONTRACTUAL-AMT := TOT-CONTRACTUAL-AMT ( 7 )
        grand_Line_Pnd_Grand_Per_Dividend.setValue(tot_Per_Dividend.getValue(7));                                                                                         //Natural: ASSIGN #GRAND-PER-DIVIDEND := TOT-PER-DIVIDEND ( 7 )
        grand_Line_Pnd_Grand_Amt.compute(new ComputeParameters(false, grand_Line_Pnd_Grand_Amt), tot_Contractual_Amt.getValue(7).add(tot_Per_Dividend.getValue(7)));      //Natural: ASSIGN #GRAND-AMT := TOT-CONTRACTUAL-AMT ( 7 ) + TOT-PER-DIVIDEND ( 7 )
        getReports().write(2, ReportOption.NOTITLE,grand_Line_Pnd_Grand_Destination,grand_Line_Pnd_Fiiler,grand_Line_Pnd_Grand_Payees,grand_Line_Pnd_F,                   //Natural: WRITE ( 2 ) GRAND-LINE
            grand_Line_Pnd_Grand_Contractual_Amt,grand_Line_Pnd_F2,grand_Line_Pnd_Grand_Per_Dividend,grand_Line_Pnd_F3,grand_Line_Pnd_Grand_Amt);
        if (Global.isEscape()) return;
    }
    private void sub_Print_Tpa_Report() throws Exception                                                                                                                  //Natural: PRINT-TPA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************
        //*    REMOVE FOLLOWING FROM IF STATEMENT 9/03
        //*        IF DESTINATION-CURR = 'IRAC' #I := 3
        //*        ELSE
        //*          IF DESTINATION-CURR = 'IRAT' #I := 4
        //*          ELSE
        //*            IF DESTINATION-CURR = 'IRAX' #I := 5
        //*            ELSE
        if (condition(pnd_Save_Destination.notEquals(iaaa730a_Rec_In_Destination_Curr)))                                                                                  //Natural: IF #SAVE-DESTINATION NOT = DESTINATION-CURR
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            if (condition(iaaa730a_Rec_In_Destination_Curr.equals("CASH")))                                                                                               //Natural: IF DESTINATION-CURR = 'CASH'
            {
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaaa730a_Rec_In_Destination_Curr.equals("DTRA")))                                                                                           //Natural: IF DESTINATION-CURR = 'DTRA'
                {
                    pnd_I.setValue(2);                                                                                                                                    //Natural: ASSIGN #I := 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* CHANGED FROM 6 TO 3 9/03
                    if (condition(iaaa730a_Rec_In_Destination_Curr.equals("RINV")))                                                                                       //Natural: IF DESTINATION-CURR = 'RINV'
                    {
                        pnd_I.setValue(3);                                                                                                                                //Natural: ASSIGN #I := 3
                        //*     ADD 1 TO #I
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*        END-IF
            //*      END-IF
            //*    END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Destination.setValue(iaaa730a_Rec_In_Destination_Curr);                                                                                                  //Natural: ASSIGN #SAVE-DESTINATION := DESTINATION-CURR
        tot_Payees.getValue(7).nadd(1);                                                                                                                                   //Natural: ADD 1 TO TOT-PAYEES ( 7 )
        //*  GRAND TOTAL LIN
        tot_Contractual_Amt.getValue(7).nadd(iaaa730a_Rec_In_Contractual_Amt);                                                                                            //Natural: ADD CONTRACTUAL-AMT TO TOT-CONTRACTUAL-AMT ( 7 )
        //*  GRAND TOTAL
        tot_Per_Dividend.getValue(7).nadd(iaaa730a_Rec_In_Per_Dividend);                                                                                                  //Natural: ADD PER-DIVIDEND TO TOT-PER-DIVIDEND ( 7 )
        tot_Payees.getValue(pnd_I).nadd(1);                                                                                                                               //Natural: ADD 1 TO TOT-PAYEES ( #I )
        tot_Contractual_Amt.getValue(pnd_I).nadd(iaaa730a_Rec_In_Contractual_Amt);                                                                                        //Natural: ADD CONTRACTUAL-AMT TO TOT-CONTRACTUAL-AMT ( #I )
        tot_Per_Dividend.getValue(pnd_I).nadd(iaaa730a_Rec_In_Per_Dividend);                                                                                              //Natural: ADD PER-DIVIDEND TO TOT-PER-DIVIDEND ( #I )
        detail_Line_Pnd_Destination_Curr.setValue(iaaa730a_Rec_In_Destination_Curr);                                                                                      //Natural: ASSIGN #DESTINATION-CURR := DESTINATION-CURR
        detail_Line_Pnd_Destination_Prev.setValue(iaaa730a_Rec_In_Destination_Prev);                                                                                      //Natural: ASSIGN #DESTINATION-PREV := DESTINATION-PREV
        detail_Line_Pnd_Soc_Sec.setValue(iaaa730a_Rec_In_Soc_Sec);                                                                                                        //Natural: ASSIGN #SOC-SEC := SOC-SEC
        detail_Line_Pnd_Da_Contract.setValue(iaaa730a_Rec_In_Da_Contract);                                                                                                //Natural: ASSIGN #DA-CONTRACT := DA-CONTRACT
        detail_Line_Pnd_Ia_Contract.setValue(iaaa730a_Rec_In_Ia_Contract);                                                                                                //Natural: ASSIGN #IA-CONTRACT := IA-CONTRACT
        detail_Line_Pnd_Contractual_Amt.setValue(iaaa730a_Rec_In_Contractual_Amt);                                                                                        //Natural: ASSIGN #CONTRACTUAL-AMT := CONTRACTUAL-AMT
        detail_Line_Pnd_Per_Dividend.setValue(iaaa730a_Rec_In_Per_Dividend);                                                                                              //Natural: ASSIGN #PER-DIVIDEND := PER-DIVIDEND
        detail_Line_Pnd_Cash_Cd.setValue(iaaa730a_Rec_In_Cash_Cd);                                                                                                        //Natural: ASSIGN #CASH-CD := CASH-CD
        detail_Line_Pnd_Term_Cd.setValue(iaaa730a_Rec_In_Term_Cd);                                                                                                        //Natural: ASSIGN #TERM-CD := TERM-CD
        detail_Line_Pnd_Hold_Cd.setValue(iaaa730a_Rec_In_Hold_Cd);                                                                                                        //Natural: ASSIGN #HOLD-CD := HOLD-CD
        detail_Line_Pnd_Pend_Cd.setValue(iaaa730a_Rec_In_Pend_Cd);                                                                                                        //Natural: ASSIGN #PEND-CD := PEND-CD
        pnd_Check_Dt_Pnd_Yyyymm.setValue(iaaa730a_Rec_In_Check_Date);                                                                                                     //Natural: ASSIGN #YYYYMM := CHECK-DATE
        pnd_Check_Dt_Pnd_Dd.setValue(1);                                                                                                                                  //Natural: ASSIGN #DD := 01
        detail_Line_Pnd_Check_Date.setValue(pnd_Check_Dt);                                                                                                                //Natural: ASSIGN #CHECK-DATE := #CHECK-DT
        detail_Line_Pnd_Mode.setValue(iaaa730a_Rec_In_Mode);                                                                                                              //Natural: ASSIGN #MODE := MODE
        detail_Line_Pnd_Coll_Cd.setValue(iaaa730a_Rec_In_Coll_Cd);                                                                                                        //Natural: ASSIGN #COLL-CD := COLL-CD
        getReports().write(1, ReportOption.NOTITLE,detail_Line_Pnd_Soc_Sec,detail_Line_Pnd_Fiiler,detail_Line_Pnd_Da_Contract,detail_Line_Pnd_Fiiler1,                    //Natural: WRITE ( 1 ) DETAIL-LINE
            detail_Line_Pnd_Ia_Contract,detail_Line_Pnd_Contractual_Amt,detail_Line_Pnd_Per_Dividend,detail_Line_Pnd_Fiiler2,detail_Line_Pnd_Destination_Prev,
            detail_Line_Pnd_Fiile2,detail_Line_Pnd_Destination_Curr,detail_Line_Pnd_Fiiler3,detail_Line_Pnd_Cash_Cd,detail_Line_Pnd_Fiiler4,detail_Line_Pnd_Term_Cd,
            detail_Line_Pnd_Fiiler5,detail_Line_Pnd_Hold_Cd,detail_Line_Filler10,detail_Line_Pnd_Pend_Cd,detail_Line_Filler11,detail_Line_Pnd_Check_Date,
            detail_Line_Pnd_Mode,detail_Line_Filler13,detail_Line_Pnd_Coll_Cd);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(25),NEWLINE,"RUN DATE   ",Global.getDATX(),  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'PROGRAM' *PROGRAM 25X / 'RUN DATE   ' *DATX ( EM = MM/DD/YYYY ) 20X 'IA TPA DETAIL REPORT FOR CHECK DATE' #CHECK-DATE ( EM = 9999/99/99 ) /55X #SAVE-DESTINATION 'DESTINATION' 42X 'PAGE :' *PAGE-NUMBER ( 1 ) / / 38X 'CONTRACTUAL' 3X'PERIODIC' 5X 'DESTINATION' 3X 'CASH' 3X 'TERM' 'HOLD' 3X 'PEND ' 3X 'CHECKT' 10X 'COLL    ' / 1X'SOC-SEC' 3X 'DA-CONTRACT' 2X 'IA-CONTRACT' 5X 'AMOUNT' 6X 'DIVIDEND' 5X 'PREV' 3X 'CURR' 3X 'CODE' 3X 'CODE' 1X 'CODE' 3X 'CODE' 4X 'DATE' 5X 'MODE' 3X 'CODE'
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(20),"IA TPA DETAIL REPORT FOR CHECK DATE",detail_Line_Pnd_Check_Date, new ReportEditMask 
                        ("9999/99/99"),NEWLINE,new ColumnSpacing(55),pnd_Save_Destination,"DESTINATION",new ColumnSpacing(42),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
                        ColumnSpacing(38),"CONTRACTUAL",new ColumnSpacing(3),"PERIODIC",new ColumnSpacing(5),"DESTINATION",new ColumnSpacing(3),"CASH",new 
                        ColumnSpacing(3),"TERM","HOLD",new ColumnSpacing(3),"PEND ",new ColumnSpacing(3),"CHECKT",new ColumnSpacing(10),"COLL    ",NEWLINE,new 
                        ColumnSpacing(1),"SOC-SEC",new ColumnSpacing(3),"DA-CONTRACT",new ColumnSpacing(2),"IA-CONTRACT",new ColumnSpacing(5),"AMOUNT",new 
                        ColumnSpacing(6),"DIVIDEND",new ColumnSpacing(5),"PREV",new ColumnSpacing(3),"CURR",new ColumnSpacing(3),"CODE",new ColumnSpacing(3),"CODE",new 
                        ColumnSpacing(1),"CODE",new ColumnSpacing(3),"CODE",new ColumnSpacing(4),"DATE",new ColumnSpacing(5),"MODE",new ColumnSpacing(3),
                        "CODE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(25),NEWLINE,"RUN DATE   ",Global.getDATX(),  //Natural: WRITE ( 2 ) NOTITLE NOHDR / 'PROGRAM' *PROGRAM 25X / 'RUN DATE   ' *DATX ( EM = MM/DD/YYYY ) 5X 'IA TPA DETAIL REPORT FOR CHECK DATE' #CHECK-DATE ( EM = 9999/99/99 ) / '           ' 30X 'TPA TOTALS BY DEST (Non Rollovers)' 42X 'PAGE :' *PAGE-NUMBER ( 2 ) / 'DEST' 18X 'PAYEES' 3X 'CONTRACTUAL AMOUNT' 6X 'PERIODIC DIVIDEND' 10X 'TOTAL-AMT' /
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(5),"IA TPA DETAIL REPORT FOR CHECK DATE",detail_Line_Pnd_Check_Date, new ReportEditMask 
                        ("9999/99/99"),NEWLINE,"           ",new ColumnSpacing(30),"TPA TOTALS BY DEST (Non Rollovers)",new ColumnSpacing(42),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,"DEST",new 
                        ColumnSpacing(18),"PAYEES",new ColumnSpacing(3),"CONTRACTUAL AMOUNT",new ColumnSpacing(6),"PERIODIC DIVIDEND",new ColumnSpacing(10),
                        "TOTAL-AMT",NEWLINE);
                    //*    40X 'TOTAL TPA SETLEMENTS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=133");
        Global.format(1, "PS=55 LS=133");
        Global.format(2, "PS=55 LS=133");
        Global.format(3, "PS=55 LS=133");
        Global.format(4, "PS=55 LS=133");
        Global.format(5, "PS=55 LS=133");
    }
}
