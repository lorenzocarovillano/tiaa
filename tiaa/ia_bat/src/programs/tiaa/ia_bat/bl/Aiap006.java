/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:04 PM
**        * FROM NATURAL PROGRAM : Aiap006
************************************************************
**        * FILE NAME            : Aiap006.java
**        * CLASS NAME           : Aiap006
**        * INSTANCE NAME        : Aiap006
************************************************************
**** AIAP006 - IA-LEDGER RECONCILIATION REPORT 12/15/98

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap006 extends BLNatBase
{
    // Data Areas
    private PdaAiaa510 pdaAiaa510;
    private PdaAial0421 pdaAial0421;
    private PdaAial0130 pdaAial0130;
    private LdaAial0595 ldaAial0595;
    private LdaAial2000 ldaAial2000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ia_Reserve;
    private DbsField pnd_Reserve_Switch;
    private DbsField pnd_Positive_Units;
    private DbsField pnd_Calculated_Units;
    private DbsField pnd_Calculated_Payment;
    private DbsField pnd_L_Calculated_Units;
    private DbsField pnd_L_Calculated_Payment;
    private DbsField pnd_Error_Code;
    private DbsField pnd_Monthly_Switch;
    private DbsField pnd_Previous_Contract;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_1;
    private DbsField pnd_Date_Pnd_Date_Yymm;
    private DbsField pnd_Date_Pnd_Date_Dd;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField pnd_Header_Rec;

    private DbsGroup pnd_Header_Rec__R_Field_2;
    private DbsField pnd_Header_Rec_Pnd_Filler_Ef1;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Date;

    private DbsGroup pnd_Header_Rec__R_Field_3;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Year;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Mon;
    private DbsField pnd_Header_Rec_Pnd_Start_Eff_Day;
    private DbsField pnd_Header_Rec_Pnd_Filler_Ef2;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Date;

    private DbsGroup pnd_Header_Rec__R_Field_4;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Year;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Mon;
    private DbsField pnd_Header_Rec_Pnd_End_Eff_Day;

    private DbsGroup pnd_Ia_Output_File;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Contract;

    private DbsGroup pnd_Ia_Output_File__R_Field_5;
    private DbsField pnd_Ia_Output_File_Pnd_I_Contract;
    private DbsField pnd_Ia_Output_File_Pnd_I_Payee;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date;

    private DbsGroup pnd_Ia_Output_File__R_Field_6;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_Y;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_M;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_D;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Units;
    private DbsField pnd_Ia_Output_File_Pnd_E_Ia_Payment;

    private DbsGroup pnd_Ledger_Output_File;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Contract;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Account;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date;

    private DbsGroup pnd_Ledger_Output_File__R_Field_7;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_Y;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_M;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_D;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Effective_Date;

    private DbsGroup pnd_Ledger_Output_File__R_Field_8;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_Y;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_M;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_D;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Fund_Code;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind;
    private DbsField pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount;
    private DbsField pnd_Mode_Change_Display;

    private DbsGroup pnd_Mode_Change_Display__R_Field_9;
    private DbsField pnd_Mode_Change_Display_Pnd_Mode_Message;
    private DbsField pnd_Mode_Change_Display_Pnd_Mode_Filler;
    private DbsField pnd_Mode_Change_Display_Pnd_Mode_Number;

    private DbsGroup pnd_Contract_Payee_S;
    private DbsField pnd_Contract_Payee_S_Pnd_Contract;
    private DbsField pnd_Contract_Payee_S_Pnd_Payee;
    private DbsField pnd_Issue_Date_S;

    private DbsGroup pnd_Issue_Date_S__R_Field_10;
    private DbsField pnd_Issue_Date_S_Pnd_Issue_Year_S;
    private DbsField pnd_Issue_Date_S_Pnd_Issue_Month_S;
    private DbsField pnd_Issue_Date_S_Pnd_Issue_Day_S;
    private DbsField pnd_Issue_Date_P;

    private DbsGroup pnd_Issue_Date_P__R_Field_11;
    private DbsField pnd_Issue_Date_P_Pnd_Issue_Year_P;
    private DbsField pnd_Issue_Date_P_Pnd_Issue_Month_P;
    private DbsField pnd_E_L_Calculated_Payment;
    private DbsField pnd_E_L_Calculated_Units;
    private DbsField pnd_E_Error_Code;
    private DbsField pnd_Space;
    private DbsField pnd_Space2;
    private DbsField pnd_Space3;
    private DbsField pnd_Ia_Final_Date;

    private DbsGroup pnd_Ia_Final_Date__R_Field_12;
    private DbsField pnd_Ia_Final_Date_Pnd_Ia_Final_Yymm;
    private DbsField pnd_Ia_Final_Date_Pnd_Ia_Final_Dd;
    private DbsField pnd_Time_Between_Dates;
    private DbsField pnd_Extra_Time;
    private DbsField pnd_Check_Date;

    private DbsGroup pnd_Check_Date__R_Field_13;
    private DbsField pnd_Check_Date_Pnd_Check_Yy;
    private DbsField pnd_Check_Date_Pnd_Check_Mm;
    private DbsField pnd_Check_Date_Pnd_Check_Dd;
    private DbsField pnd_Check_Date_6;

    private DbsGroup pnd_Check_Date_6__R_Field_14;
    private DbsField pnd_Check_Date_6_Pnd_Check_Yy_6;
    private DbsField pnd_Check_Date_6_Pnd_Check_Mm_6;
    private DbsField pnd_Lump_Sum_Switch;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAiaa510 = new PdaAiaa510(localVariables);
        pdaAial0421 = new PdaAial0421(localVariables);
        pdaAial0130 = new PdaAial0130(localVariables);
        ldaAial0595 = new LdaAial0595();
        registerRecord(ldaAial0595);
        ldaAial2000 = new LdaAial2000();
        registerRecord(ldaAial2000);

        // Local Variables
        pnd_Ia_Reserve = localVariables.newFieldInRecord("pnd_Ia_Reserve", "#IA-RESERVE", FieldType.NUMERIC, 11, 2);
        pnd_Reserve_Switch = localVariables.newFieldInRecord("pnd_Reserve_Switch", "#RESERVE-SWITCH", FieldType.STRING, 1);
        pnd_Positive_Units = localVariables.newFieldInRecord("pnd_Positive_Units", "#POSITIVE-UNITS", FieldType.NUMERIC, 7, 3);
        pnd_Calculated_Units = localVariables.newFieldInRecord("pnd_Calculated_Units", "#CALCULATED-UNITS", FieldType.NUMERIC, 8, 3);
        pnd_Calculated_Payment = localVariables.newFieldInRecord("pnd_Calculated_Payment", "#CALCULATED-PAYMENT", FieldType.NUMERIC, 9, 2);
        pnd_L_Calculated_Units = localVariables.newFieldInRecord("pnd_L_Calculated_Units", "#L-CALCULATED-UNITS", FieldType.NUMERIC, 8, 3);
        pnd_L_Calculated_Payment = localVariables.newFieldInRecord("pnd_L_Calculated_Payment", "#L-CALCULATED-PAYMENT", FieldType.NUMERIC, 7, 2);
        pnd_Error_Code = localVariables.newFieldInRecord("pnd_Error_Code", "#ERROR-CODE", FieldType.STRING, 3);
        pnd_Monthly_Switch = localVariables.newFieldInRecord("pnd_Monthly_Switch", "#MONTHLY-SWITCH", FieldType.STRING, 1);
        pnd_Previous_Contract = localVariables.newFieldInRecord("pnd_Previous_Contract", "#PREVIOUS-CONTRACT", FieldType.STRING, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Date__R_Field_1", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Yymm = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Yymm", "#DATE-YYMM", FieldType.STRING, 6);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Header_Rec = localVariables.newFieldInRecord("pnd_Header_Rec", "#HEADER-REC", FieldType.STRING, 80);

        pnd_Header_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Header_Rec__R_Field_2", "REDEFINE", pnd_Header_Rec);
        pnd_Header_Rec_Pnd_Filler_Ef1 = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Filler_Ef1", "#FILLER-EF1", FieldType.STRING, 18);
        pnd_Header_Rec_Pnd_Start_Eff_Date = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Date", "#START-EFF-DATE", FieldType.STRING, 
            8);

        pnd_Header_Rec__R_Field_3 = pnd_Header_Rec__R_Field_2.newGroupInGroup("pnd_Header_Rec__R_Field_3", "REDEFINE", pnd_Header_Rec_Pnd_Start_Eff_Date);
        pnd_Header_Rec_Pnd_Start_Eff_Year = pnd_Header_Rec__R_Field_3.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Year", "#START-EFF-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Header_Rec_Pnd_Start_Eff_Mon = pnd_Header_Rec__R_Field_3.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Mon", "#START-EFF-MON", FieldType.NUMERIC, 
            2);
        pnd_Header_Rec_Pnd_Start_Eff_Day = pnd_Header_Rec__R_Field_3.newFieldInGroup("pnd_Header_Rec_Pnd_Start_Eff_Day", "#START-EFF-DAY", FieldType.NUMERIC, 
            2);
        pnd_Header_Rec_Pnd_Filler_Ef2 = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_Filler_Ef2", "#FILLER-EF2", FieldType.STRING, 1);
        pnd_Header_Rec_Pnd_End_Eff_Date = pnd_Header_Rec__R_Field_2.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Date", "#END-EFF-DATE", FieldType.STRING, 
            8);

        pnd_Header_Rec__R_Field_4 = pnd_Header_Rec__R_Field_2.newGroupInGroup("pnd_Header_Rec__R_Field_4", "REDEFINE", pnd_Header_Rec_Pnd_End_Eff_Date);
        pnd_Header_Rec_Pnd_End_Eff_Year = pnd_Header_Rec__R_Field_4.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Year", "#END-EFF-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Header_Rec_Pnd_End_Eff_Mon = pnd_Header_Rec__R_Field_4.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Mon", "#END-EFF-MON", FieldType.NUMERIC, 
            2);
        pnd_Header_Rec_Pnd_End_Eff_Day = pnd_Header_Rec__R_Field_4.newFieldInGroup("pnd_Header_Rec_Pnd_End_Eff_Day", "#END-EFF-DAY", FieldType.NUMERIC, 
            2);

        pnd_Ia_Output_File = localVariables.newGroupInRecord("pnd_Ia_Output_File", "#IA-OUTPUT-FILE");
        pnd_Ia_Output_File_Pnd_E_Ia_Contract = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Contract", "#E-IA-CONTRACT", FieldType.STRING, 
            10);

        pnd_Ia_Output_File__R_Field_5 = pnd_Ia_Output_File.newGroupInGroup("pnd_Ia_Output_File__R_Field_5", "REDEFINE", pnd_Ia_Output_File_Pnd_E_Ia_Contract);
        pnd_Ia_Output_File_Pnd_I_Contract = pnd_Ia_Output_File__R_Field_5.newFieldInGroup("pnd_Ia_Output_File_Pnd_I_Contract", "#I-CONTRACT", FieldType.STRING, 
            8);
        pnd_Ia_Output_File_Pnd_I_Payee = pnd_Ia_Output_File__R_Field_5.newFieldInGroup("pnd_Ia_Output_File_Pnd_I_Payee", "#I-PAYEE", FieldType.STRING, 
            2);
        pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code", "#E-IA-TRANSACTION-CODE", 
            FieldType.STRING, 2);
        pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date", "#E-IA-EFFECTIVE-DATE", 
            FieldType.STRING, 6);

        pnd_Ia_Output_File__R_Field_6 = pnd_Ia_Output_File.newGroupInGroup("pnd_Ia_Output_File__R_Field_6", "REDEFINE", pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date);
        pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_Y = pnd_Ia_Output_File__R_Field_6.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_Y", "#E-IA-EFFECTIVE-DATE-Y", 
            FieldType.STRING, 2);
        pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_M = pnd_Ia_Output_File__R_Field_6.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_M", "#E-IA-EFFECTIVE-DATE-M", 
            FieldType.STRING, 2);
        pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_D = pnd_Ia_Output_File__R_Field_6.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_D", "#E-IA-EFFECTIVE-DATE-D", 
            FieldType.STRING, 2);
        pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code", "#E-IA-SEQUENCE-CODE", 
            FieldType.STRING, 2);
        pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code", "#E-IA-FUND-CODE", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind", "#E-IA-REVAL-IND", FieldType.STRING, 
            1);
        pnd_Ia_Output_File_Pnd_E_Ia_Units = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Units", "#E-IA-UNITS", FieldType.NUMERIC, 
            9, 3);
        pnd_Ia_Output_File_Pnd_E_Ia_Payment = pnd_Ia_Output_File.newFieldInGroup("pnd_Ia_Output_File_Pnd_E_Ia_Payment", "#E-IA-PAYMENT", FieldType.NUMERIC, 
            8, 2);

        pnd_Ledger_Output_File = localVariables.newGroupInRecord("pnd_Ledger_Output_File", "#LEDGER-OUTPUT-FILE");
        pnd_Ledger_Output_File_Pnd_E_L_Contract = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Contract", "#E-L-CONTRACT", FieldType.STRING, 
            8);
        pnd_Ledger_Output_File_Pnd_E_L_Account = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Account", "#E-L-ACCOUNT", FieldType.STRING, 
            8);
        pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr", "#E-L-DR-OR-CR", FieldType.STRING, 
            1);
        pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date", "#E-L-TRANSACTION-DATE", 
            FieldType.STRING, 6);

        pnd_Ledger_Output_File__R_Field_7 = pnd_Ledger_Output_File.newGroupInGroup("pnd_Ledger_Output_File__R_Field_7", "REDEFINE", pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date);
        pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_Y = pnd_Ledger_Output_File__R_Field_7.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_Y", 
            "#E-L-TRANSACTION-DATE-Y", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_M = pnd_Ledger_Output_File__R_Field_7.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_M", 
            "#E-L-TRANSACTION-DATE-M", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_D = pnd_Ledger_Output_File__R_Field_7.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_D", 
            "#E-L-TRANSACTION-DATE-D", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Effective_Date = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Effective_Date", "#E-L-EFFECTIVE-DATE", 
            FieldType.STRING, 6);

        pnd_Ledger_Output_File__R_Field_8 = pnd_Ledger_Output_File.newGroupInGroup("pnd_Ledger_Output_File__R_Field_8", "REDEFINE", pnd_Ledger_Output_File_Pnd_E_L_Effective_Date);
        pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_Y = pnd_Ledger_Output_File__R_Field_8.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_Y", 
            "#E-L-EFFECTIVE-DATE-Y", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_M = pnd_Ledger_Output_File__R_Field_8.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_M", 
            "#E-L-EFFECTIVE-DATE-M", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_D = pnd_Ledger_Output_File__R_Field_8.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_D", 
            "#E-L-EFFECTIVE-DATE-D", FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code", "#E-L-SEQUENCE-CODE", 
            FieldType.STRING, 2);
        pnd_Ledger_Output_File_Pnd_E_L_Fund_Code = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Fund_Code", "#E-L-FUND-CODE", 
            FieldType.STRING, 1);
        pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind", "#E-L-REVAL-IND", 
            FieldType.STRING, 1);
        pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount = pnd_Ledger_Output_File.newFieldInGroup("pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount", 
            "#E-L-TRANSACTION-AMOUNT", FieldType.NUMERIC, 10, 2);
        pnd_Mode_Change_Display = localVariables.newFieldInRecord("pnd_Mode_Change_Display", "#MODE-CHANGE-DISPLAY", FieldType.STRING, 15);

        pnd_Mode_Change_Display__R_Field_9 = localVariables.newGroupInRecord("pnd_Mode_Change_Display__R_Field_9", "REDEFINE", pnd_Mode_Change_Display);
        pnd_Mode_Change_Display_Pnd_Mode_Message = pnd_Mode_Change_Display__R_Field_9.newFieldInGroup("pnd_Mode_Change_Display_Pnd_Mode_Message", "#MODE-MESSAGE", 
            FieldType.STRING, 11);
        pnd_Mode_Change_Display_Pnd_Mode_Filler = pnd_Mode_Change_Display__R_Field_9.newFieldInGroup("pnd_Mode_Change_Display_Pnd_Mode_Filler", "#MODE-FILLER", 
            FieldType.STRING, 1);
        pnd_Mode_Change_Display_Pnd_Mode_Number = pnd_Mode_Change_Display__R_Field_9.newFieldInGroup("pnd_Mode_Change_Display_Pnd_Mode_Number", "#MODE-NUMBER", 
            FieldType.NUMERIC, 3);

        pnd_Contract_Payee_S = localVariables.newGroupInRecord("pnd_Contract_Payee_S", "#CONTRACT-PAYEE-S");
        pnd_Contract_Payee_S_Pnd_Contract = pnd_Contract_Payee_S.newFieldInGroup("pnd_Contract_Payee_S_Pnd_Contract", "#CONTRACT", FieldType.STRING, 8);
        pnd_Contract_Payee_S_Pnd_Payee = pnd_Contract_Payee_S.newFieldInGroup("pnd_Contract_Payee_S_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 2);
        pnd_Issue_Date_S = localVariables.newFieldInRecord("pnd_Issue_Date_S", "#ISSUE-DATE-S", FieldType.NUMERIC, 8);

        pnd_Issue_Date_S__R_Field_10 = localVariables.newGroupInRecord("pnd_Issue_Date_S__R_Field_10", "REDEFINE", pnd_Issue_Date_S);
        pnd_Issue_Date_S_Pnd_Issue_Year_S = pnd_Issue_Date_S__R_Field_10.newFieldInGroup("pnd_Issue_Date_S_Pnd_Issue_Year_S", "#ISSUE-YEAR-S", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_S_Pnd_Issue_Month_S = pnd_Issue_Date_S__R_Field_10.newFieldInGroup("pnd_Issue_Date_S_Pnd_Issue_Month_S", "#ISSUE-MONTH-S", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_S_Pnd_Issue_Day_S = pnd_Issue_Date_S__R_Field_10.newFieldInGroup("pnd_Issue_Date_S_Pnd_Issue_Day_S", "#ISSUE-DAY-S", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_P = localVariables.newFieldInRecord("pnd_Issue_Date_P", "#ISSUE-DATE-P", FieldType.NUMERIC, 6);

        pnd_Issue_Date_P__R_Field_11 = localVariables.newGroupInRecord("pnd_Issue_Date_P__R_Field_11", "REDEFINE", pnd_Issue_Date_P);
        pnd_Issue_Date_P_Pnd_Issue_Year_P = pnd_Issue_Date_P__R_Field_11.newFieldInGroup("pnd_Issue_Date_P_Pnd_Issue_Year_P", "#ISSUE-YEAR-P", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_P_Pnd_Issue_Month_P = pnd_Issue_Date_P__R_Field_11.newFieldInGroup("pnd_Issue_Date_P_Pnd_Issue_Month_P", "#ISSUE-MONTH-P", FieldType.NUMERIC, 
            2);
        pnd_E_L_Calculated_Payment = localVariables.newFieldInRecord("pnd_E_L_Calculated_Payment", "#E-L-CALCULATED-PAYMENT", FieldType.NUMERIC, 8, 2);
        pnd_E_L_Calculated_Units = localVariables.newFieldInRecord("pnd_E_L_Calculated_Units", "#E-L-CALCULATED-UNITS", FieldType.NUMERIC, 8, 3);
        pnd_E_Error_Code = localVariables.newFieldInRecord("pnd_E_Error_Code", "#E-ERROR-CODE", FieldType.STRING, 3);
        pnd_Space = localVariables.newFieldInRecord("pnd_Space", "#SPACE", FieldType.STRING, 50);
        pnd_Space2 = localVariables.newFieldInRecord("pnd_Space2", "#SPACE2", FieldType.STRING, 77);
        pnd_Space3 = localVariables.newFieldInRecord("pnd_Space3", "#SPACE3", FieldType.STRING, 2);
        pnd_Ia_Final_Date = localVariables.newFieldInRecord("pnd_Ia_Final_Date", "#IA-FINAL-DATE", FieldType.STRING, 8);

        pnd_Ia_Final_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Ia_Final_Date__R_Field_12", "REDEFINE", pnd_Ia_Final_Date);
        pnd_Ia_Final_Date_Pnd_Ia_Final_Yymm = pnd_Ia_Final_Date__R_Field_12.newFieldInGroup("pnd_Ia_Final_Date_Pnd_Ia_Final_Yymm", "#IA-FINAL-YYMM", FieldType.STRING, 
            6);
        pnd_Ia_Final_Date_Pnd_Ia_Final_Dd = pnd_Ia_Final_Date__R_Field_12.newFieldInGroup("pnd_Ia_Final_Date_Pnd_Ia_Final_Dd", "#IA-FINAL-DD", FieldType.STRING, 
            2);
        pnd_Time_Between_Dates = localVariables.newFieldInRecord("pnd_Time_Between_Dates", "#TIME-BETWEEN-DATES", FieldType.NUMERIC, 10);
        pnd_Extra_Time = localVariables.newFieldInRecord("pnd_Extra_Time", "#EXTRA-TIME", FieldType.NUMERIC, 2);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.STRING, 8);

        pnd_Check_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_Check_Date__R_Field_13", "REDEFINE", pnd_Check_Date);
        pnd_Check_Date_Pnd_Check_Yy = pnd_Check_Date__R_Field_13.newFieldInGroup("pnd_Check_Date_Pnd_Check_Yy", "#CHECK-YY", FieldType.NUMERIC, 4);
        pnd_Check_Date_Pnd_Check_Mm = pnd_Check_Date__R_Field_13.newFieldInGroup("pnd_Check_Date_Pnd_Check_Mm", "#CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Check_Date_Pnd_Check_Dd = pnd_Check_Date__R_Field_13.newFieldInGroup("pnd_Check_Date_Pnd_Check_Dd", "#CHECK-DD", FieldType.STRING, 2);
        pnd_Check_Date_6 = localVariables.newFieldInRecord("pnd_Check_Date_6", "#CHECK-DATE-6", FieldType.STRING, 6);

        pnd_Check_Date_6__R_Field_14 = localVariables.newGroupInRecord("pnd_Check_Date_6__R_Field_14", "REDEFINE", pnd_Check_Date_6);
        pnd_Check_Date_6_Pnd_Check_Yy_6 = pnd_Check_Date_6__R_Field_14.newFieldInGroup("pnd_Check_Date_6_Pnd_Check_Yy_6", "#CHECK-YY-6", FieldType.NUMERIC, 
            4);
        pnd_Check_Date_6_Pnd_Check_Mm_6 = pnd_Check_Date_6__R_Field_14.newFieldInGroup("pnd_Check_Date_6_Pnd_Check_Mm_6", "#CHECK-MM-6", FieldType.NUMERIC, 
            2);
        pnd_Lump_Sum_Switch = localVariables.newFieldInRecord("pnd_Lump_Sum_Switch", "#LUMP-SUM-SWITCH", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();

        ldaAial0595.initializeValues();
        ldaAial2000.initializeValues();

        localVariables.reset();
        pnd_Ia_Reserve.setInitialValue(0);
        pnd_Reserve_Switch.setInitialValue("N");
        pnd_Positive_Units.setInitialValue(0);
        pnd_Calculated_Units.setInitialValue(0);
        pnd_Calculated_Payment.setInitialValue(0);
        pnd_L_Calculated_Units.setInitialValue(0);
        pnd_L_Calculated_Payment.setInitialValue(0);
        pnd_Monthly_Switch.setInitialValue("N");
        pnd_Previous_Contract.setInitialValue("Y0000000");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Aiap006() throws Exception
    {
        super("Aiap006");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  READ WORK FILE 2 ONCE RECORD #HEADER-REC                                                                                                                     //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60
        //*  AT END
        //*   ESCAPE ROUTINE
        //*  END-ENDFILE
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Header_Rec_Pnd_Start_Eff_Date.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #START-EFF-DATE
            pnd_Header_Rec_Pnd_End_Eff_Date.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #END-EFF-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM READ-NEXT-CASE
        sub_Read_Next_Case();
        if (condition(Global.isEscape())) {return;}
        //*    MOVE 2 TO #ERROR-CODE
        //*    MOVE #IA-LEDGER-REC TO #LEDGER-RECORD
        //*    IF #LEDGER-IA-CONTRACT = #IA-CONTRACT
        //*      PERFORM CALCULATE-L-UNITS-WITH-IA-DATA
        //*    ELSE
        //*      PERFORM CALCULATE-L-UNITS
        //*    END-IF
        //*    PERFORM WRITE-OUTPUT-FILE
        //*    PERFORM WRITE-LEDGER-ONLY
        //* *************
        //* ******************
        //* ***********************
        //*  #E-ERROR-CODE / 36X #IA-RESERVE
        //*  #E-ERROR-CODE / 36X #IA-RESERVE
        //* **********************
        //* *******************************
        //*  36X #IA-RESERVE
        //*     36X #IA-RESERVE
        //* ******************************
        //* ******************************
        //*  *DEFINE WRITE-ERROR-FILE
        //*  *FOR #I 1 TO #ARRAY-COUNT
        //*   WRITE  #E-IA-CONTRACT #E-IA-TRANSACTION-CODE
        //*   #E-IA-EFFECTIVE-DATE  #E-IA-FUND-CODE
        //*   #E-IA-REVAL-IND  #E-IA-RESERVE-AMOUNT
        //*        #E-L-CONTRACT
        //*                       #E-L-TRANSACTION-AMOUNT
        //*  #E-ERROR-CODE
        //* ************************
        //* *****************************************************
        //* *****************************************************
        //* ***************************
        //* ************************
        //* *********************************
        //* **********************************
        //*  DISPLAY 'CALCULATE IA UNITS'
        //* *************************
        //* ****************************
        //* ************************************************
        //* ****************************
        //* *********************************
        //* *********************************
        //* ******************************************
        //* *****************************
        //* *****************************************
        //* ***********************************
        //* ************************************
        //*  T END OF PAGE (2)  SKIP (2) 1                                                                                                                                //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
        //*  ITE (2) 10X 'ERROR CODE: 1 = IA WITH NO LEDGER, 2 = LEDGER WITH NO IA'
        //*    '3 = PAYMENTS DO NOT MATCH'
        //*  D-ENDPAGE
    }
    private void sub_Read_Next_Case() throws Exception                                                                                                                    //Natural: READ-NEXT-CASE
    {
        if (BLNatReinput.isReinput()) return;

        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 1 #IA-LEDGER-REC
        while (condition(getWorkFiles().read(1, ldaAial0595.getPnd_Ia_Ledger_Rec())))
        {
            if (condition(ldaAial0595.getPnd_Ia_Ledger_Rec_Pnd_Ia_Ledger_Key().equals("I")))                                                                              //Natural: IF #IA-LEDGER-KEY = 'I'
            {
                ldaAial0595.getPnd_Ia_Record().setValue(ldaAial0595.getPnd_Ia_Ledger_Rec());                                                                              //Natural: MOVE #IA-LEDGER-REC TO #IA-RECORD
                //*    MOVE #IA-CONTRACT TO #PREVIOUS-CONTRACT
                //*    DISPLAY #IA-CONTRACT
                                                                                                                                                                          //Natural: PERFORM PROCESS-IA-RECORD
                sub_Process_Ia_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Process_Ia_Record() throws Exception                                                                                                                 //Natural: PROCESS-IA-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  MOVE 'Y' TO #RESERVE-SWITCH
        //*  * SPLAY 'IA-REC'
                                                                                                                                                                          //Natural: PERFORM CALCULATE-RESERVE
        sub_Calculate_Reserve();
        if (condition(Global.isEscape())) {return;}
        //*  IF #CALCULATED-UNITS NOT = 0
        //*  COMPUTE #IA-RESERVE = 100000 * (#IA-PER-UNITS / #CALCULATED-UNITS)
        //*  * SPLAY NAZ-ACT-CREF-IA-CERT-NMBR   #IA-PER-UNITS #CALCULATED-UNITS
        //*  #IA-RESERVE
        //*  END-IF
        //*  READ   WORK FILE 1  #IA-LEDGER-REC /* FIF
        //*   MOVE 'N' TO #RESERVE-SWITCH
        //*  COMMENT FIF
        //*  IF #IA-LEDGER-KEY = 'L'
        //*    PERFORM PROCESS-LEDGER-RECORD
        //*    ESCAPE ROUTINE
        //*  ELSE
        pnd_Error_Code.setValue(1);                                                                                                                                       //Natural: MOVE 1 TO #ERROR-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
        sub_Write_Output_File();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-ONLY
        sub_Write_Ia_Only();
        if (condition(Global.isEscape())) {return;}
        //*     MOVE #IA-LEDGER-REC TO #IA-RECORD
        //*     PERFORM CALCULATE-RESERVE
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  END-IF
        //*  END-WORK
    }
    private void sub_Process_Ledger_Record() throws Exception                                                                                                             //Natural: PROCESS-LEDGER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAial0595.getPnd_Ledger_Record().setValue(ldaAial0595.getPnd_Ia_Ledger_Rec());                                                                                  //Natural: MOVE #IA-LEDGER-REC TO #LEDGER-RECORD
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract().notEquals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract()) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date().notEquals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date())  //Natural: IF #LEDGER-IA-CONTRACT NOT = #IA-CONTRACT OR #LEDGER-EFFECTIVE-DATE NOT = #IA-EFFECTIVE-DATE OR #LEDGER-FUND-CODE NOT = #IA-FUND-CODE OR #LEDGER-REVAL-IND NOT = #IA-REVAL-IND
            || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().notEquals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code()) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().notEquals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind())))
        {
            pnd_Error_Code.setValue(1);                                                                                                                                   //Natural: MOVE 01 TO #ERROR-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-ONLY
            sub_Write_Ia_Only();
            if (condition(Global.isEscape())) {return;}
            pnd_Error_Code.setValue(2);                                                                                                                                   //Natural: MOVE 02 TO #ERROR-CODE
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract().notEquals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract())))                           //Natural: IF #LEDGER-IA-CONTRACT NOT = #IA-CONTRACT
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-L-UNITS
                sub_Calculate_L_Units();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-L-UNITS-WITH-IA-DATA
                sub_Calculate_L_Units_With_Ia_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-LEDGER-ONLY
            sub_Write_Ledger_Only();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract().equals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract()) && ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date().equals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date())  //Natural: IF #LEDGER-IA-CONTRACT = #IA-CONTRACT AND #LEDGER-EFFECTIVE-DATE = #IA-EFFECTIVE-DATE AND #LEDGER-FUND-CODE = #IA-FUND-CODE AND #LEDGER-REVAL-IND = #IA-REVAL-IND
                && ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code()) && ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().equals(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind())))
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS
                sub_Calculate_Ia_Units();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*   IF #IA-PER-PAYMENT NOT = NAZ-ACT-CREF-O-ANN-RATE-AMT (1)
            pnd_Positive_Units.reset();                                                                                                                                   //Natural: RESET #POSITIVE-UNITS
            if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units().less(getZero())))                                                                               //Natural: IF #IA-PER-UNITS < 0
            {
                pnd_Positive_Units.compute(new ComputeParameters(false, pnd_Positive_Units), DbsField.subtract(getZero(),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units())); //Natural: COMPUTE #POSITIVE-UNITS = 0 - #IA-PER-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Positive_Units.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units());                                                                             //Natural: MOVE #IA-PER-UNITS TO #POSITIVE-UNITS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Positive_Units.notEquals(pnd_E_L_Calculated_Units)))                                                                                        //Natural: IF #POSITIVE-UNITS NOT = #E-L-CALCULATED-UNITS
            {
                pnd_Error_Code.setValue(3);                                                                                                                               //Natural: MOVE 3 TO #ERROR-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
                sub_Write_Output_File();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-ENTIRE-RECORD
                sub_Write_Entire_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Code.setValue("   ");                                                                                                                           //Natural: MOVE '   ' TO #ERROR-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
                sub_Write_Output_File();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-ENTIRE-RECORD
                sub_Write_Entire_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Ia_Only() throws Exception                                                                                                                     //Natural: WRITE-IA-ONLY
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(0, pnd_Ia_Output_File_Pnd_I_Contract);                                                                                                       //Natural: DISPLAY #I-CONTRACT
        if (Global.isEscape()) return;
        if (condition(pnd_Ia_Output_File_Pnd_I_Contract.notEquals(pnd_Previous_Contract)))                                                                                //Natural: IF #I-CONTRACT NOT = #PREVIOUS-CONTRACT
        {
            if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(40) && (ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code().greater(2)               //Natural: IF #IA-TRANSACTION-CODE = 40 AND ( #IA-PAYEE-CODE > 02 OR #IA-OPTION = 21 )
                || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option().equals(21))))
            {
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 2 ) 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(40) && (ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code().greater(2)                   //Natural: IF #IA-TRANSACTION-CODE = 40 AND ( #IA-PAYEE-CODE > 02 OR #IA-OPTION = 21 )
            || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option().equals(21))))
        {
            getReports().write(2, pnd_Ia_Output_File_Pnd_E_Ia_Contract,pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code,pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date,pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code,pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code,pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind,pnd_Ia_Output_File_Pnd_E_Ia_Units,pnd_Ia_Output_File_Pnd_E_Ia_Payment,pnd_Space2,pnd_E_Error_Code,NEWLINE,pnd_Mode_Change_Display,new  //Natural: WRITE ( 2 ) #IA-OUTPUT-FILE #SPACE2 #E-ERROR-CODE / #MODE-CHANGE-DISPLAY 21X #IA-RESERVE
                ColumnSpacing(21),pnd_Ia_Reserve);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, pnd_Ia_Output_File_Pnd_E_Ia_Contract,pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code,pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date,pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code,pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code,pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind,pnd_Ia_Output_File_Pnd_E_Ia_Units,pnd_Ia_Output_File_Pnd_E_Ia_Payment,pnd_Space2,pnd_E_Error_Code,NEWLINE,pnd_Mode_Change_Display,new  //Natural: WRITE ( 1 ) #IA-OUTPUT-FILE #SPACE2 #E-ERROR-CODE / #MODE-CHANGE-DISPLAY 21X #IA-RESERVE
                ColumnSpacing(21),pnd_Ia_Reserve);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Previous_Contract.setValue(pnd_Ia_Output_File_Pnd_I_Contract);                                                                                                //Natural: MOVE #I-CONTRACT TO #PREVIOUS-CONTRACT
    }
    private void sub_Write_Ledger_Only() throws Exception                                                                                                                 //Natural: WRITE-LEDGER-ONLY
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ledger_Output_File_Pnd_E_L_Contract.equals("6N999999")))                                                                                        //Natural: IF #E-L-CONTRACT = '6N999999'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ledger_Output_File_Pnd_E_L_Contract.notEquals(pnd_Previous_Contract)))                                                                          //Natural: IF #E-L-CONTRACT NOT = #PREVIOUS-CONTRACT
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(616030) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617030)  //Natural: IF #LEDGER-ACC-NUMBER = 616030 OR = 617030 OR = 617020 OR = 617020
                || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617020) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617020)))
            {
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 2 ) 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(616030) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617030)  //Natural: IF #LEDGER-ACC-NUMBER = 616030 OR = 617030 OR = 617020 OR = 617020
            || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617020) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(617020)))
        {
            getReports().write(2, pnd_Space,pnd_Ledger_Output_File_Pnd_E_L_Contract,pnd_Ledger_Output_File_Pnd_E_L_Account,pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr,       //Natural: WRITE ( 2 ) #SPACE #LEDGER-OUTPUT-FILE #E-L-CALCULATED-UNITS #E-L-CALCULATED-PAYMENT #E-ERROR-CODE
                pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date,pnd_Ledger_Output_File_Pnd_E_L_Effective_Date,pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code,
                pnd_Ledger_Output_File_Pnd_E_L_Fund_Code,pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount,pnd_E_L_Calculated_Units,
                pnd_E_L_Calculated_Payment,pnd_E_Error_Code);
            if (Global.isEscape()) return;
            //*  21X #E-ERROR-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, pnd_Space,pnd_Ledger_Output_File_Pnd_E_L_Contract,pnd_Ledger_Output_File_Pnd_E_L_Account,pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr,       //Natural: WRITE ( 1 ) #SPACE #LEDGER-OUTPUT-FILE #E-L-CALCULATED-UNITS #E-L-CALCULATED-PAYMENT #E-ERROR-CODE
                pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date,pnd_Ledger_Output_File_Pnd_E_L_Effective_Date,pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code,
                pnd_Ledger_Output_File_Pnd_E_L_Fund_Code,pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount,pnd_E_L_Calculated_Units,
                pnd_E_L_Calculated_Payment,pnd_E_Error_Code);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Previous_Contract.setValue(pnd_Ledger_Output_File_Pnd_E_L_Contract);                                                                                          //Natural: MOVE #E-L-CONTRACT TO #PREVIOUS-CONTRACT
    }
    private void sub_Write_Entire_Record() throws Exception                                                                                                               //Natural: WRITE-ENTIRE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ia_Output_File_Pnd_E_Ia_Contract.notEquals(pnd_Previous_Contract)))                                                                             //Natural: IF #E-IA-CONTRACT NOT = #PREVIOUS-CONTRACT
        {
            if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(40) && (ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code().greater(2)               //Natural: IF #IA-TRANSACTION-CODE = 40 AND ( #IA-PAYEE-CODE > 02 OR #IA-OPTION = 21 )
                || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option().equals(21))))
            {
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 2 ) 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(40) && (ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code().greater(2)                   //Natural: IF #IA-TRANSACTION-CODE = 40 AND ( #IA-PAYEE-CODE > 02 OR #IA-OPTION = 21 )
            || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option().equals(21))))
        {
            getReports().write(2, pnd_Ia_Output_File_Pnd_E_Ia_Contract,pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code,pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date,pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code,pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code,pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind,pnd_Ia_Output_File_Pnd_E_Ia_Units,pnd_Ia_Output_File_Pnd_E_Ia_Payment,pnd_Ledger_Output_File_Pnd_E_L_Contract,pnd_Ledger_Output_File_Pnd_E_L_Account,pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date,pnd_Ledger_Output_File_Pnd_E_L_Effective_Date,pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code,pnd_Ledger_Output_File_Pnd_E_L_Fund_Code,pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount,pnd_E_L_Calculated_Units,pnd_E_L_Calculated_Payment,pnd_E_Error_Code,NEWLINE,pnd_Mode_Change_Display,new  //Natural: WRITE ( 2 ) #IA-OUTPUT-FILE #LEDGER-OUTPUT-FILE #E-L-CALCULATED-UNITS #E-L-CALCULATED-PAYMENT #E-ERROR-CODE / #MODE-CHANGE-DISPLAY 21X #IA-RESERVE
                ColumnSpacing(21),pnd_Ia_Reserve);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, pnd_Ia_Output_File_Pnd_E_Ia_Contract,pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code,pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date,pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code,pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code,pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind,pnd_Ia_Output_File_Pnd_E_Ia_Units,pnd_Ia_Output_File_Pnd_E_Ia_Payment,pnd_Ledger_Output_File_Pnd_E_L_Contract,pnd_Ledger_Output_File_Pnd_E_L_Account,pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date,pnd_Ledger_Output_File_Pnd_E_L_Effective_Date,pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code,pnd_Ledger_Output_File_Pnd_E_L_Fund_Code,pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind,pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount,pnd_E_L_Calculated_Units,pnd_E_L_Calculated_Payment,pnd_E_Error_Code,NEWLINE,pnd_Mode_Change_Display,new  //Natural: WRITE ( 1 ) #IA-OUTPUT-FILE #LEDGER-OUTPUT-FILE #E-L-CALCULATED-UNITS #E-L-CALCULATED-PAYMENT #E-ERROR-CODE / #MODE-CHANGE-DISPLAY 21X #IA-RESERVE
                ColumnSpacing(21),pnd_Ia_Reserve);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Previous_Contract.setValue(pnd_Ia_Output_File_Pnd_E_Ia_Contract);                                                                                             //Natural: MOVE #E-IA-CONTRACT TO #PREVIOUS-CONTRACT
    }
    private void sub_Write_Output_File() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Error_Code.equals("1") || pnd_Error_Code.equals("3") || pnd_Error_Code.equals("   ")))                                                          //Natural: IF #ERROR-CODE = '1' OR #ERROR-CODE = '3' OR #ERROR-CODE = '   '
        {
            pnd_Ia_Output_File_Pnd_E_Ia_Contract.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract());                                                                //Natural: MOVE #IA-CONTRACT TO #E-IA-CONTRACT
            pnd_Ia_Output_File_Pnd_I_Payee.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Pay_Code());                                                                      //Natural: MOVE #IA-PAY-CODE TO #I-PAYEE
            pnd_Ia_Output_File_Pnd_E_Ia_Payment.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Payment());                                                              //Natural: MOVE #IA-PER-PAYMENT TO #E-IA-PAYMENT
            pnd_Ia_Output_File_Pnd_E_Ia_Transaction_Code.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code());                                                //Natural: MOVE #IA-TRANSACTION-CODE TO #E-IA-TRANSACTION-CODE
            //*  MOVE #IA-EFFECTIVE-DATE TO #E-IA-EFFECTIVE-DATE
            pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_Y.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date_Y());                                                //Natural: MOVE #IA-EFFECTIVE-DATE-Y TO #E-IA-EFFECTIVE-DATE-Y
            pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_M.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date_M());                                                //Natural: MOVE #IA-EFFECTIVE-DATE-M TO #E-IA-EFFECTIVE-DATE-M
            pnd_Ia_Output_File_Pnd_E_Ia_Effective_Date_D.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date_D());                                                //Natural: MOVE #IA-EFFECTIVE-DATE-D TO #E-IA-EFFECTIVE-DATE-D
            pnd_Ia_Output_File_Pnd_E_Ia_Fund_Code.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code());                                                              //Natural: MOVE #IA-FUND-CODE TO #E-IA-FUND-CODE
            pnd_Ia_Output_File_Pnd_E_Ia_Reval_Ind.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind());                                                              //Natural: MOVE #IA-REVAL-IND TO #E-IA-REVAL-IND
            pnd_Ia_Output_File_Pnd_E_Ia_Payment.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Payment());                                                              //Natural: MOVE #IA-PER-PAYMENT TO #E-IA-PAYMENT
            pnd_Ia_Output_File_Pnd_E_Ia_Units.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units());                                                                  //Natural: MOVE #IA-PER-UNITS TO #E-IA-UNITS
            pnd_Ia_Output_File_Pnd_E_Ia_Sequence_Code.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sequence_Code());                                                      //Natural: MOVE #IA-SEQUENCE-CODE TO #E-IA-SEQUENCE-CODE
            pnd_E_Error_Code.setValue(pnd_Error_Code);                                                                                                                    //Natural: MOVE #ERROR-CODE TO #E-ERROR-CODE
            if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode_Change_Ind().equals("M")))                                                                             //Natural: IF #IA-MODE-CHANGE-IND = 'M'
            {
                pnd_Mode_Change_Display_Pnd_Mode_Message.setValue("MODE CHANGE");                                                                                         //Natural: MOVE 'MODE CHANGE' TO #MODE-MESSAGE
                pnd_Mode_Change_Display_Pnd_Mode_Number.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode());                                                             //Natural: MOVE #IA-MODE TO #MODE-NUMBER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Mode_Change_Display.setValue("               ");                                                                                                      //Natural: MOVE '               ' TO #MODE-CHANGE-DISPLAY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Error_Code.equals("2") || pnd_Error_Code.equals("3") || pnd_Error_Code.equals("   ")))                                                          //Natural: IF #ERROR-CODE = '2' OR #ERROR-CODE = '3' OR #ERROR-CODE = '   '
        {
            pnd_Ledger_Output_File_Pnd_E_L_Contract.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract());                                                  //Natural: MOVE #LEDGER-IA-CONTRACT TO #E-L-CONTRACT
            //*  MOVE #LEDGER-EFFECTIVE-DATE TO #E-L-EFFECTIVE-DATE
            pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_Y.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_Y());                                     //Natural: MOVE #LEDGER-EFFECTIVE-DATE-Y TO #E-L-EFFECTIVE-DATE-Y
            pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_M.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_M());                                     //Natural: MOVE #LEDGER-EFFECTIVE-DATE-M TO #E-L-EFFECTIVE-DATE-M
            pnd_Ledger_Output_File_Pnd_E_L_Effective_Date_D.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_D());                                     //Natural: MOVE #LEDGER-EFFECTIVE-DATE-D TO #E-L-EFFECTIVE-DATE-D
            pnd_Ledger_Output_File_Pnd_E_L_Transaction_Amount.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount());                                      //Natural: MOVE #L-TRANSACTION-AMOUNT TO #E-L-TRANSACTION-AMOUNT
            pnd_Ledger_Output_File_Pnd_E_L_Fund_Code.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code());                                                   //Natural: MOVE #LEDGER-FUND-CODE TO #E-L-FUND-CODE
            pnd_Ledger_Output_File_Pnd_E_L_Reval_Ind.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind());                                                   //Natural: MOVE #LEDGER-REVAL-IND TO #E-L-REVAL-IND
            pnd_Ledger_Output_File_Pnd_E_L_Account.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Account_Number());                                                //Natural: MOVE #LEDGER-ACCOUNT-NUMBER TO #E-L-ACCOUNT
            pnd_Ledger_Output_File_Pnd_E_L_Dr_Or_Cr.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Dr_Or_Cr());                                                     //Natural: MOVE #LEDGER-DR-OR-CR TO #E-L-DR-OR-CR
            //*  MOVE #LEDGER-TRANSACTION-DATE TO #E-L-TRANSACTION-DATE
            pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_Y.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_Y());                                 //Natural: MOVE #LEDGER-TRANSACTION-DATE-Y TO #E-L-TRANSACTION-DATE-Y
            pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_M.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_M());                                 //Natural: MOVE #LEDGER-TRANSACTION-DATE-M TO #E-L-TRANSACTION-DATE-M
            pnd_Ledger_Output_File_Pnd_E_L_Transaction_Date_D.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Transaction_Date_D());                                 //Natural: MOVE #LEDGER-TRANSACTION-DATE-D TO #E-L-TRANSACTION-DATE-D
            pnd_Ledger_Output_File_Pnd_E_L_Sequence_Code.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Sequence_Number());                                         //Natural: MOVE #LEDGER-SEQUENCE-NUMBER TO #E-L-SEQUENCE-CODE
            pnd_E_Error_Code.setValue(pnd_Error_Code);                                                                                                                    //Natural: MOVE #ERROR-CODE TO #E-ERROR-CODE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_L_Units() throws Exception                                                                                                                 //Natural: CALCULATE-L-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //*  SPLAY 'L-UNITS'
        //*  WRITE  #LEDGER-IA-CONTRACT #LEDGER-FUND-CODE  'L-UNITS'
        //*  #LEDGER-EFFECTIVE-DATE
        //*  MOVE 0 TO #AIAN042-ERROR
        pnd_E_L_Calculated_Payment.reset();                                                                                                                               //Natural: RESET #E-L-CALCULATED-PAYMENT
        pnd_E_L_Calculated_Units.reset();                                                                                                                                 //Natural: RESET #E-L-CALCULATED-UNITS
        pdaAiaa510.getNaz_Act_Calc_Input_Area().reset();                                                                                                                  //Natural: RESET NAZ-ACT-CALC-INPUT-AREA
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("N");                                                                                   //Natural: MOVE 'N' TO NAZ-ACT-TIAA-GRADED-CALL
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("N");                                                                                      //Natural: MOVE 'N' TO NAZ-ACT-TIAA-STD-CALL
        pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract());                             //Natural: MOVE #LEDGER-IA-CONTRACT TO #AIAN042-CNTRCT-PPCN-NBR
        pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Payee_Cde().setValue(1);                                                                                    //Natural: MOVE 01 TO #AIAN042-CNTRCT-PAYEE-CDE
        pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cmpny_Fund_Cde_Meth().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind());                           //Natural: MOVE #LEDGER-REVAL-IND TO #AIAN042-CMPNY-FUND-CDE-METH
        DbsUtil.callnat(Aian042s.class , getCurrentProcessState(), pdaAial0421.getPnd_Aian042_Linkage());                                                                 //Natural: CALLNAT 'AIAN042S' #AIAN042-LINKAGE
        if (condition(Global.isEscape())) return;
        //*  DISPLAY #AIAN042-CNTRCT-PPCN-NBR #AIAN042-ISSUE-DATE
        if (condition(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Error().equals(1)))                                                                                  //Natural: IF #AIAN042-ERROR = 1
        {
            getReports().display(0, pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Cntrct_Ppcn_Nbr(),"NO IA RECORD FOUND");                                               //Natural: DISPLAY #AIAN042-CNTRCT-PPCN-NBR 'NO IA RECORD FOUND'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Issue_Date().less(199804) && ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Valuation().equals(60)))) //Natural: IF ( #AIAN042-ISSUE-DATE < 199804 AND #LEDGER-ACC-VALUATION = 60 )
        {
            pnd_Monthly_Switch.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #MONTHLY-SWITCH
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE NOHDR #AIAN042-CNTRCT-PPCN-NBR #LEDGER-EFFECTIVE-DATE-N
        //*  #LEDGER-ACCOUNT-FUND #MONTHLY-SWITCH
        if (condition(pnd_Monthly_Switch.equals("N")))                                                                                                                    //Natural: IF #MONTHLY-SWITCH = 'N'
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(330000) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(312100)  //Natural: IF #LEDGER-ACC-NUMBER = 330000 OR = 312100 OR = 335000 OR #LEDGER-ACCOUNT-NUMBER = 31510070
                || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(335000) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Account_Number().equals(31510070)))
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-L-UNITS-AIAN022
                sub_Calculate_L_Units_Aian022();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-L-UNITS-AIAN013
                sub_Calculate_L_Units_Aian013();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Monthly_Switch.equals("Y")))                                                                                                                    //Natural: IF #MONTHLY-SWITCH = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-L-UNITS-AIAN013
            sub_Calculate_L_Units_Aian013();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Monthly_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #MONTHLY-SWITCH
    }
    private void sub_Calculate_L_Units_With_Ia_Data() throws Exception                                                                                                    //Natural: CALCULATE-L-UNITS-WITH-IA-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  SPLAY 'IADATA' #LEDGER-EFFECTIVE-DATE
        if (condition((ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date_N().less(19980401) && ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Valuation().equals(60))))     //Natural: IF ( #IA-ISSUE-DATE-N < 19980401 AND #LEDGER-ACC-VALUATION = 60 )
        {
            pnd_Monthly_Switch.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #MONTHLY-SWITCH
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Monthly_Switch.equals("N")))                                                                                                                    //Natural: IF #MONTHLY-SWITCH = 'N'
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(330000) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(312100)  //Natural: IF #LEDGER-ACC-NUMBER = 330000 OR = 312100 OR = 335000 OR #LEDGER-ACCOUNT-NUMBER = 31510070
                || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Acc_Number().equals(335000) || ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Account_Number().equals(31510070)))
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN022
                sub_Calculate_Ia_Units_Aian022();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN013
                sub_Calculate_Ia_Units_Aian013();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Monthly_Switch.equals("Y")))                                                                                                                    //Natural: IF #MONTHLY-SWITCH = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN013
            sub_Calculate_Ia_Units_Aian013();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Monthly_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #MONTHLY-SWITCH
    }
    private void sub_Calculate_L_Units_Aian022() throws Exception                                                                                                         //Natural: CALCULATE-L-UNITS-AIAN022
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Date_Pnd_Date_Yymm.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Issue_Date());                                                                     //Natural: MOVE #AIAN042-ISSUE-DATE TO #DATE-YYMM
        pnd_Date_Pnd_Date_Dd.setValue("01");                                                                                                                              //Natural: MOVE '01' TO #DATE-DD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date);                                                 //Natural: MOVE EDITED #DATE TO NAZ-ACT-ASD-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date);                                                 //Natural: MOVE EDITED #DATE TO NAZ-ACT-EFF-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().nsubtract(1);                                                                                            //Natural: SUBTRACT 1 FROM NAZ-ACT-EFF-DATE
        pnd_Check_Date.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date());                                                                            //Natural: MOVE #LEDGER-EFFECTIVE-DATE TO #CHECK-DATE
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_Pnd_Check_Dd.setValue("01");                                                                                                                       //Natural: MOVE '01' TO #CHECK-DD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Check_Date);                                    //Natural: MOVE EDITED #CHECK-DATE TO NAZ-ACT-CURR-BSNSS-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ia_Next_Pymt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Check_Date);                                   //Natural: MOVE EDITED #CHECK-DATE TO NAZ-ACT-IA-NEXT-PYMT-DTE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("I");                                                                                       //Natural: MOVE 'I' TO NAZ-ACT-TYPE-OF-CALL
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Option());                                     //Natural: MOVE #AIAN042-OPTION TO NAZ-ACT-OPTION-CDE
        if (condition(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Option().equals(21)))                                                                                //Natural: IF #AIAN042-OPTION = 21
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-GUAR-PERIOD-2
            sub_Calculate_Guar_Period_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Mode());                                       //Natural: MOVE #AIAN042-MODE TO NAZ-ACT-PYMNT-MODE
        if (condition(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob().notEquals(0)))                                                                       //Natural: IF #AIAN042-FIRST-ANN-DOB NOT = 00000000
        {
            pnd_Date.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob());                                                                            //Natural: MOVE #AIAN042-FIRST-ANN-DOB TO #DATE
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date);                                       //Natural: MOVE EDITED #DATE TO NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex());                                 //Natural: MOVE #AIAN042-FIRST-ANN-SEX TO NAZ-ACT-ANN-SEX
        if (condition(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob().notEquals(getZero())))                                                              //Natural: IF #AIAN042-SECOND-ANN-DOB NOT = 0
        {
            pnd_Date.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob());                                                                           //Natural: MOVE #AIAN042-SECOND-ANN-DOB TO #DATE
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date);                                   //Natural: MOVE EDITED #DATE TO NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex());                            //Natural: MOVE #AIAN042-SECOND-ANN-SEX TO NAZ-ACT-2ND-ANN-SEX
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Ia_Cert_Nmbr().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract());                            //Natural: MOVE #LEDGER-IA-CONTRACT TO NAZ-ACT-CREF-IA-CERT-NMBR
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().equals("A")))                                                                               //Natural: IF #LEDGER-REVAL-IND = 'A'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("Y");                                                                               //Natural: MOVE 'Y' TO NAZ-ACT-CREF-ANNUAL-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("N");                                                                                //Natural: MOVE 'N' TO NAZ-ACT-CREF-MTHLY-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount());       //Natural: MOVE #L-TRANSACTION-AMOUNT TO NAZ-ACT-CREF-ANNUAL-RATE-AMT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-CREF-ANNUAL-RATE-2
            sub_Get_Cref_Annual_Rate_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("Y");                                                                                //Natural: MOVE 'Y' TO NAZ-ACT-CREF-MTHLY-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("N");                                                                               //Natural: MOVE 'N' TO NAZ-ACT-CREF-ANNUAL-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount());        //Natural: MOVE #L-TRANSACTION-AMOUNT TO NAZ-ACT-CREF-MTHLY-RATE-AMT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-CREF-MTHLY-RATE-2
            sub_Get_Cref_Mthly_Rate_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  * SPLAY NAZ-ACT-CREF-IA-CERT-NMBR
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(1);                                                                                           //Natural: MOVE 01 TO NAZ-ACT-ORIGIN-CDE
        DbsUtil.callnat(Aian022.class , getCurrentProcessState(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                                                               //Natural: CALLNAT 'AIAN022' NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        //*  ITE (3)  NAZ-ACT-CREF-IA-CERT-NMBR  NAZ-ACT-EFF-DATE NAZ-ACT-ASD-DATE
        //*  NAZ-ACT-CREF-ANNUAL-RATE-AMT (1)
        //*  NAZ-ACT-CREF-ANNUAL-INFO (1)
        //*  NAZ-ACT-OPTION-CDE NAZ-ACT-GUAR-PERIOD NAZ-ACT-RETURN-CODE
        //*  IF NAZ-ACT-RETURN-CODE NOT = 0
        //*   * SPLAY NAZ-ACT-RETURN-CODE NAZ-ACT-RETURN-ERROR-MSG
        //*  NAZ-ACT-CREF-IA-CERT-NMBR
        //*  END-IF
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().equals("A")))                                                                               //Natural: IF #LEDGER-REVAL-IND = 'A'
        {
            pnd_E_L_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(1));                                         //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-AMT ( 1 ) TO #E-L-CALCULATED-PAYMENT
            pnd_E_L_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(1));                                        //Natural: MOVE NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( 1 ) TO #E-L-CALCULATED-UNITS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_E_L_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(1));                                         //Natural: MOVE NAZ-ACT-CREF-O-MTH-RATE-AMT ( 1 ) TO #E-L-CALCULATED-PAYMENT
            pnd_E_L_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(1));                                        //Natural: MOVE NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( 1 ) TO #E-L-CALCULATED-UNITS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Calculated_Units.setValue(pnd_E_L_Calculated_Units);                                                                                                          //Natural: MOVE #E-L-CALCULATED-UNITS TO #CALCULATED-UNITS
        pdaAial0421.getPnd_Aian042_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN042-LINKAGE
    }
    private void sub_Calculate_L_Units_Aian013() throws Exception                                                                                                         //Natural: CALCULATE-L-UNITS-AIAN013
    {
        if (BLNatReinput.isReinput()) return;

        pdaAial0130.getPnd_Aian013_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN013-LINKAGE
        pnd_Calculated_Units.reset();                                                                                                                                     //Natural: RESET #CALCULATED-UNITS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue(0);                                                                                       //Natural: MOVE 0 TO #TRANSFER-REVAL-SWITCH
        pnd_Issue_Date_P.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Issue_Date());                                                                           //Natural: MOVE #AIAN042-ISSUE-DATE TO #ISSUE-DATE-P
        //*  * SPLAY  #AIAN042-ISSUE-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Ia_Contract());                                     //Natural: MOVE #LEDGER-IA-CONTRACT TO #CONTRACT-NUMBER
        pnd_Check_Date.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date());                                                                            //Natural: MOVE #LEDGER-EFFECTIVE-DATE TO #CHECK-DATE
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_6_Pnd_Check_Yy_6.setValue(pnd_Check_Date_Pnd_Check_Yy);                                                                                            //Natural: MOVE #CHECK-YY TO #CHECK-YY-6
        pnd_Check_Date_6_Pnd_Check_Mm_6.setValue(pnd_Check_Date_Pnd_Check_Mm);                                                                                            //Natural: MOVE #CHECK-MM TO #CHECK-MM-6
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte().setValueEdited(new ReportEditMask("YYYYMM"),pnd_Check_Date_6);                                            //Natural: MOVE EDITED #CHECK-DATE-6 TO #NEXT-PYMNT-DTE ( EM = YYYYMM )
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().setValue(1);                                                                                                  //Natural: MOVE 01 TO #PAYEE-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code());                                             //Natural: MOVE #LEDGER-FUND-CODE TO #FUND-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Mode());                                                    //Natural: MOVE #AIAN042-MODE TO #MODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N());                        //Natural: MOVE #LEDGER-EFFECTIVE-DATE-N TO #TRANSFER-EFFECTIVE-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Year().setValue(pnd_Issue_Date_P_Pnd_Issue_Year_P);                                                                  //Natural: MOVE #ISSUE-YEAR-P TO #ISSUE-YEAR
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Month().setValue(pnd_Issue_Date_P_Pnd_Issue_Month_P);                                                                //Natural: MOVE #ISSUE-MONTH-P TO #ISSUE-MONTH
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Option());                                                //Natural: MOVE #AIAN042-OPTION TO #OPTION
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dob());                                  //Natural: MOVE #AIAN042-FIRST-ANN-DOB TO #FIRST-ANN-DOB
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date());                        //Natural: MOVE #AIAN042-FINAL-PER-PAY-DATE TO #FINAL-PER-PAY-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Dod());                                  //Natural: MOVE #AIAN042-FIRST-ANN-DOD TO #FIRST-ANN-DOD
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dod());                                //Natural: MOVE #AIAN042-SECOND-ANN-DOD TO #SECOND-ANN-DOD
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Dob());                                //Natural: MOVE #AIAN042-SECOND-ANN-DOB TO #SECOND-ANN-DOB
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_First_Ann_Sex());                                  //Natural: MOVE #AIAN042-FIRST-ANN-SEX TO #FIRST-ANN-SEX
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex().setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Second_Ann_Sex());                                //Natural: MOVE #AIAN042-SECOND-ANN-SEX TO #SECOND-ANN-SEX
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind());                                 //Natural: MOVE #LEDGER-REVAL-IND TO #REVALUATION-INDICATOR
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N());                          //Natural: MOVE #LEDGER-EFFECTIVE-DATE-N TO #ILLUSTRATION-EFF-DATE
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N().equals(19980331)))                                                                   //Natural: IF #LEDGER-EFFECTIVE-DATE-N = 19980331
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(19980401);                                                                            //Natural: MOVE 19980401 TO #ILLUSTRATION-EFF-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(19980401);                                                                          //Natural: MOVE 19980401 TO #TRANSFER-EFFECTIVE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(100);                                                                                            //Natural: MOVE 100 TO #TRANSFER-UNITS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1).setValue(100);                                                                              //Natural: MOVE 100 TO #UNITS-TO-RECEIVE ( 1 )
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code());                           //Natural: MOVE #LEDGER-FUND-CODE TO #FUND-TO-RECEIVE ( 1 )
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().setValue("I");                                                                                               //Natural: MOVE 'I' TO #TYPE-OF-RUN
        //*  DISPLAY #CONTRACT-NUMBER
        //*  CALLNAT 'AIAN013S' #AIAN013-LINKAGE
        DbsUtil.callnat(Aian013r.class , getCurrentProcessState(), pdaAial0130.getPnd_Aian013_Linkage());                                                                 //Natural: CALLNAT 'AIAN013R' #AIAN013-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1).notEquals(getZero())))                                                   //Natural: IF #TRANSFER-AMT-OUT-CREF ( 1 ) NOT = 0
        {
            pnd_Calculated_Units.compute(new ComputeParameters(true, pnd_Calculated_Units), DbsField.multiply(100,ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount()).divide(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1))); //Natural: COMPUTE ROUNDED #CALCULATED-UNITS = 100 * #L-TRANSACTION-AMOUNT / #TRANSFER-AMT-OUT-CREF ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Calculated_Payment.compute(new ComputeParameters(false, pnd_Calculated_Payment), pnd_Calculated_Units.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1))); //Natural: COMPUTE #CALCULATED-PAYMENT = #CALCULATED-UNITS * #AUV-OUT ( 1 )
        //*  ITE (6) #CONTRACT-NUMBER #ISSUE-DATE #TRANSFER-EFFECTIVE-DATE
        //*  #MONTHLY-SWITCH
        //*  #TRANSFER-AMT-OUT-CREF (1) #CALCULATED-UNITS  #AUV-OUT (1)
        //*  #CALCULATED-UNITS
        pnd_E_L_Calculated_Units.setValue(pnd_Calculated_Units);                                                                                                          //Natural: MOVE #CALCULATED-UNITS TO #E-L-CALCULATED-UNITS
        pnd_E_L_Calculated_Payment.setValue(pnd_Calculated_Payment);                                                                                                      //Natural: MOVE #CALCULATED-PAYMENT TO #E-L-CALCULATED-PAYMENT
        pdaAial0421.getPnd_Aian042_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN042-LINKAGE
    }
    private void sub_Calculate_Ia_Units() throws Exception                                                                                                                //Natural: CALCULATE-IA-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //*  * SPLAY   #IA-CONTRACT       'IA-UNITS'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(20) || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(33)))            //Natural: IF #IA-TRANSACTION-CODE = 20 OR #IA-TRANSACTION-CODE = 33
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN022
            sub_Calculate_Ia_Units_Aian022();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN013
            sub_Calculate_Ia_Units_Aian013();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Ia_Units_Aian013() throws Exception                                                                                                        //Natural: CALCULATE-IA-UNITS-AIAN013
    {
        if (BLNatReinput.isReinput()) return;

        pdaAial0130.getPnd_Aian013_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN013-LINKAGE
        pnd_Calculated_Units.reset();                                                                                                                                     //Natural: RESET #CALCULATED-UNITS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Reval_Switch().setValue(0);                                                                                       //Natural: MOVE 0 TO #TRANSFER-REVAL-SWITCH
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Contract_Number().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract());                                                //Natural: MOVE #IA-CONTRACT TO #CONTRACT-NUMBER
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Payee_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code());                                                   //Natural: MOVE #IA-PAYEE-CODE TO #PAYEE-CODE
        //*  SPLAY #RESERVE-SWITCH
        if (condition(pnd_Reserve_Switch.equals("Y")))                                                                                                                    //Natural: IF #RESERVE-SWITCH = 'Y'
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date_N());                            //Natural: MOVE #IA-EFFECTIVE-DATE-N TO #TRANSFER-EFFECTIVE-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date_N());                              //Natural: MOVE #IA-EFFECTIVE-DATE-N TO #ILLUSTRATION-EFF-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind());                                     //Natural: MOVE #IA-REVAL-IND TO #REVALUATION-INDICATOR
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code());                               //Natural: MOVE #IA-FUND-CODE TO #FUND-TO-RECEIVE ( 1 )
            //*  MOVE #IA-EFFECTIVE-DATE-N TO #TRANSFER-EFF-DATE-OUT
            pnd_Check_Date.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date());                                                                                //Natural: MOVE #IA-EFFECTIVE-DATE TO #CHECK-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code());                                                 //Natural: MOVE #IA-FUND-CODE TO #FUND-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Effective_Date().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N());                    //Natural: MOVE #LEDGER-EFFECTIVE-DATE-N TO #TRANSFER-EFFECTIVE-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Illustration_Eff_Date().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date_N());                      //Natural: MOVE #LEDGER-EFFECTIVE-DATE-N TO #ILLUSTRATION-EFF-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Revaluation_Indicator().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind());                             //Natural: MOVE #LEDGER-REVAL-IND TO #REVALUATION-INDICATOR
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_To_Receive().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code());                       //Natural: MOVE #LEDGER-FUND-CODE TO #FUND-TO-RECEIVE ( 1 )
            //*  MOVE #LEDGER-EFFECTIVE-DATE-N TO #TRANSFER-EFF-DATE-OUT
            pnd_Check_Date.setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Effective_Date());                                                                        //Natural: MOVE #LEDGER-EFFECTIVE-DATE TO #CHECK-DATE
            pdaAial0130.getPnd_Aian013_Linkage_Pnd_Fund_Code().setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code());                                         //Natural: MOVE #LEDGER-FUND-CODE TO #FUND-CODE
            //*  SPLAY #IA-CONTRACT #LEDGER-EFFECTIVE-DATE #TRANSFER-EFFECTIVE-DATE
            //*  'HELLO'
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE #LEDGER-FUND-CODE TO #FUND-CODE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Mode().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode());                                                               //Natural: MOVE #IA-MODE TO #MODE
        pnd_Issue_Date_S.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date_N());                                                                                    //Natural: MOVE #IA-ISSUE-DATE-N TO #ISSUE-DATE-S
        //*  MOVE #IA-EFFECTIVE-DATE-N TO #TRANSFER-EFFECTIVE-DATE
        //*  MOVE #LEDGER-EFFECTIVE-DATE-N TO #TRANSFER-EFFECTIVE-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Year().setValue(pnd_Issue_Date_S_Pnd_Issue_Year_S);                                                                  //Natural: MOVE #ISSUE-YEAR-S TO #ISSUE-YEAR
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Issue_Month().setValue(pnd_Issue_Date_S_Pnd_Issue_Month_S);                                                                //Natural: MOVE #ISSUE-MONTH-S TO #ISSUE-MONTH
        //*  * SPLAY 'MONTH2' * SPLAY  #ISSUE-MONTH
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Option().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option());                                                           //Natural: MOVE #IA-OPTION TO #OPTION
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Final_Per_Pay_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date_N());                                     //Natural: MOVE #IA-FINAL-PAY-DATE-N TO #FINAL-PER-PAY-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dob().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob_N());                                           //Natural: MOVE #IA-FIRST-ANN-DOB-N TO #FIRST-ANN-DOB
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Dod().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dod_Yymm());                                        //Natural: MOVE #IA-FIRST-ANN-DOD-YYMM TO #FIRST-ANN-DOD
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dod().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod_Yymm());                                         //Natural: MOVE #IA-SEC-ANN-DOD-YYMM TO #SECOND-ANN-DOD
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Dob().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob_N());                                            //Natural: MOVE #IA-SEC-ANN-DOB-N TO #SECOND-ANN-DOB
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_First_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Sex());                                             //Natural: MOVE #IA-FIRST-ANN-SEX TO #FIRST-ANN-SEX
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Second_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex());                                              //Natural: MOVE #IA-SEC-ANN-SEX TO #SECOND-ANN-SEX
        //*  MOVE #IA-REVAL-IND TO #REVALUATION-INDICATOR
        //*  MOVE #LEDGER-REVAL-IND  TO #REVALUATION-INDICATOR
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Units().setValue(100);                                                                                            //Natural: MOVE 100 TO #TRANSFER-UNITS
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Units_To_Receive().getValue(1).setValue(100);                                                                              //Natural: MOVE 100 TO #UNITS-TO-RECEIVE ( 1 )
        //*  MOVE #IA-FUND-CODE TO #FUND-TO-RECEIVE (1)
        //*  MOVE #LEDGER-FUND-CODE TO #FUND-TO-RECEIVE (1)
        //*  MOVE #IA-EFFECTIVE-DATE-N TO #TRANSFER-EFF-DATE-OUT
        //*  MOVE #LEDGER-EFFECTIVE-DATE-N TO #TRANSFER-EFF-DATE-OUT
        //*  DISPLAY #LEDGER-ACCOUNT-NUMBER
        //*  MOVE #LEDGER-EFFECTIVE-DATE TO #CHECK-DATE
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Type_Of_Run().setValue("I");                                                                                               //Natural: MOVE 'I' TO #TYPE-OF-RUN
        //*  MOVE #IA-EFFECTIVE-DATE TO #CHECK-DATE
        //*  VE 99999999 TO #ILLUSTRATION-EFF-DATE
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_Pnd_Check_Mm.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CHECK-MM
        if (condition(pnd_Check_Date_Pnd_Check_Mm.equals(13)))                                                                                                            //Natural: IF #CHECK-MM = 13
        {
            pnd_Check_Date_Pnd_Check_Mm.setValue(1);                                                                                                                      //Natural: MOVE 1 TO #CHECK-MM
            pnd_Check_Date_Pnd_Check_Yy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CHECK-YY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Date_6_Pnd_Check_Yy_6.setValue(pnd_Check_Date_Pnd_Check_Yy);                                                                                            //Natural: MOVE #CHECK-YY TO #CHECK-YY-6
        pnd_Check_Date_6_Pnd_Check_Mm_6.setValue(pnd_Check_Date_Pnd_Check_Mm);                                                                                            //Natural: MOVE #CHECK-MM TO #CHECK-MM-6
        pdaAial0130.getPnd_Aian013_Linkage_Pnd_Next_Pymnt_Dte().setValueEdited(new ReportEditMask("YYYYMM"),pnd_Check_Date_6);                                            //Natural: MOVE EDITED #CHECK-DATE-6 TO #NEXT-PYMNT-DTE ( EM = YYYYMM )
        //*  CALLNAT 'AIAN013S' #AIAN013-LINKAGE
        DbsUtil.callnat(Aian013r.class , getCurrentProcessState(), pdaAial0130.getPnd_Aian013_Linkage());                                                                 //Natural: CALLNAT 'AIAN013R' #AIAN013-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1).notEquals(getZero())))                                                   //Natural: IF #TRANSFER-AMT-OUT-CREF ( 1 ) NOT = 0
        {
            pnd_Calculated_Units.compute(new ComputeParameters(true, pnd_Calculated_Units), DbsField.multiply(100,ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount()).divide(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1))); //Natural: COMPUTE ROUNDED #CALCULATED-UNITS = 100 * #L-TRANSACTION-AMOUNT / #TRANSFER-AMT-OUT-CREF ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Calculated_Payment.compute(new ComputeParameters(false, pnd_Calculated_Payment), pnd_Calculated_Units.multiply(pdaAial0130.getPnd_Aian013_Linkage_Pnd_Auv_Out().getValue(1))); //Natural: COMPUTE #CALCULATED-PAYMENT = #CALCULATED-UNITS * #AUV-OUT ( 1 )
        //*  IF #RESERVE-SWITCH = 'Y'
        //*  WRITE (7) #CONTRACT-NUMBER #ISSUE-DATE  'AIAN032'
        //*    #FUND-TO-RECEIVE (1)  #RESERVE-SWITCH
        //*    #TRANSFER-AMT-OUT-CREF (1) #CALCULATED-UNITS #CALCULATED-PAYMENT
        //*    #TRANSFER-EFF-DATE-OUT #AUV-OUT (1) #LEDGER-EFFECTIVE-DATE
        //*    #MODE #FINAL-PER-PAY-DATE
        //*    #TRANSFER-UNITS #UNITS-TO-RECEIVE (1) #UNITS-OUT (1)
        //*  ELSE
        //*   WRITE (7) #CONTRACT-NUMBER #ISSUE-DATE  'AIAN032'
        //*     #FUND-TO-RECEIVE (1) #RESERVE-SWITCH
        //*     #TRANSFER-AMT-OUT-CREF (1) #CALCULATED-UNITS #CALCULATED-PAYMENT
        //*     #TRANSFER-EFF-DATE-OUT #AUV-OUT (1) #LEDGER-EFFECTIVE-DATE
        //*     #MODE #FINAL-PER-PAY-DATE
        //*     #TRANSFER-UNITS #UNITS-TO-RECEIVE (1) #UNITS-OUT (1)
        //*  END-IF
        //*  * SPLAY #LEDGER-EFFECTIVE-DATE-N  #ILLUSTRATION-EFF-DATE
        if (condition(pnd_Reserve_Switch.equals("N")))                                                                                                                    //Natural: IF #RESERVE-SWITCH = 'N'
        {
            pnd_E_L_Calculated_Units.setValue(pnd_Calculated_Units);                                                                                                      //Natural: MOVE #CALCULATED-UNITS TO #E-L-CALCULATED-UNITS
            pnd_E_L_Calculated_Payment.setValue(pnd_Calculated_Payment);                                                                                                  //Natural: MOVE #CALCULATED-PAYMENT TO #E-L-CALCULATED-PAYMENT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Ia_Units_Aian022() throws Exception                                                                                                        //Natural: CALCULATE-IA-UNITS-AIAN022
    {
        if (BLNatReinput.isReinput()) return;

        pnd_E_L_Calculated_Payment.reset();                                                                                                                               //Natural: RESET #E-L-CALCULATED-PAYMENT
        pdaAiaa510.getNaz_Act_Calc_Input_Area().reset();                                                                                                                  //Natural: RESET NAZ-ACT-CALC-INPUT-AREA
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("N");                                                                                   //Natural: MOVE 'N' TO NAZ-ACT-TIAA-GRADED-CALL
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("N");                                                                                      //Natural: MOVE 'N' TO NAZ-ACT-TIAA-STD-CALL
        //*  IF #IA-TRANSACTION-CODE = 50 OR #IA-TRANSACTION-CODE = 51
        //*   MOVE EDITED #IA-TRANSACTION-DATE TO NAZ-ACT-ASD-DATE (EM=YYYYMMDD)
        //*  DISPLAY #IA-ISSUE-DATE #IA-CONTRACT
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date());         //Natural: MOVE EDITED #IA-ISSUE-DATE TO NAZ-ACT-EFF-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().nsubtract(1);                                                                                            //Natural: SUBTRACT 1 FROM NAZ-ACT-EFF-DATE
        //*  ELSE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date());         //Natural: MOVE EDITED #IA-ISSUE-DATE TO NAZ-ACT-ASD-DATE ( EM = YYYYMMDD )
        //*  END-IF
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option().equals(21)))                                                                                           //Natural: IF #IA-OPTION = 21
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-GUAR-PERIOD
            sub_Calculate_Guar_Period();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date().setValue("       ");                                                                                     //Natural: MOVE '       ' TO #IA-FINAL-PAY-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Check_Date());  //Natural: MOVE EDITED #IA-CHECK-DATE TO NAZ-ACT-CURR-BSNSS-DATE ( EM = YYYYMMDD )
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ia_Next_Pymt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Check_Date()); //Natural: MOVE EDITED #IA-CHECK-DATE TO NAZ-ACT-IA-NEXT-PYMT-DTE ( EM = YYYYMMDD )
        //*  MOVE  'I'   TO NAZ-ACT-TYPE-OF-CALL
        //*  CHG TIAA-INDEX RATE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("1");                                                                                       //Natural: MOVE '1' TO NAZ-ACT-TYPE-OF-CALL
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option());                                                //Natural: MOVE #IA-OPTION TO NAZ-ACT-OPTION-CDE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode());                                                  //Natural: MOVE #IA-MODE TO NAZ-ACT-PYMNT-MODE
        //*  MOVE #IA-FINAL-PAY-DATE TO #IA-FINAL-YYMM
        //*  IF #IA-FINAL-PAY-DATE NOT = '000000'
        //*   MOVE '01' TO #IA-FINAL-DD
        //*  PERFORM CALCULATE-GUAR-PERIOD
        //*  * SPLAY 'DATE1' #IA-FINAL-PAY-DATE
        //*  ISPLAY 'DATE' #IA-FINAL-DATE
        //*  IF #IA-FINAL-DATE NOT = '00000000'
        //*    MOVE EDITED #IA-FINAL-DATE TO NAZ-ACT-FNL-PAY-DATE (EM=YYYYMMDD)
        //*  END-IF
        //*  END-IF
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob().notEquals("00000000")))                                                                         //Natural: IF #IA-FIRST-ANN-DOB NOT = '00000000'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob()); //Natural: MOVE EDITED #IA-FIRST-ANN-DOB TO NAZ-ACT-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Sex());                                            //Natural: MOVE #IA-FIRST-ANN-SEX TO NAZ-ACT-ANN-SEX
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob().notEquals("00000000")))                                                                           //Natural: IF #IA-SEC-ANN-DOB NOT = '00000000'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob()); //Natural: MOVE EDITED #IA-SEC-ANN-DOB TO NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex());                                          //Natural: MOVE #IA-SEC-ANN-SEX TO NAZ-ACT-2ND-ANN-SEX
        //*  MOVE #IA-CONTRACT TO NAZ-ACT-CREF-IA-CERT-NMBR
        //*  CHG TIAA-INDEX
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract());                                     //Natural: MOVE #IA-CONTRACT TO NAZ-ACT-TIAA-IA-CNTRCT-NMBR
        //*  IF #IA-REVAL-IND = 'A'
        if (condition(pnd_Reserve_Switch.equals("N")))                                                                                                                    //Natural: IF #RESERVE-SWITCH = 'N'
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().equals("A")))                                                                           //Natural: IF #LEDGER-REVAL-IND = 'A'
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("Y");                                                                           //Natural: MOVE 'Y' TO NAZ-ACT-CREF-ANNUAL-CALL
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("N");                                                                            //Natural: MOVE 'N' TO NAZ-ACT-CREF-MTHLY-CALL
                //*  IF #RESERVE-SWITCH = 'Y' MOVE 100000 TO
                //*      NAZ-ACT-CREF-ANNUAL-RATE-AMT (1)
                //*  ELSE
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount());   //Natural: MOVE #L-TRANSACTION-AMOUNT TO NAZ-ACT-CREF-ANNUAL-RATE-AMT ( 1 )
                //*  END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CREF-ANNUAL-RATE-2
                sub_Get_Cref_Annual_Rate_2();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("Y");                                                                            //Natural: MOVE 'Y' TO NAZ-ACT-CREF-MTHLY-CALL
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("N");                                                                           //Natural: MOVE 'N' TO NAZ-ACT-CREF-ANNUAL-CALL
                //*  IF #RESERVE-SWITCH = 'Y' MOVE 100000 TO
                //*      NAZ-ACT-CREF-MTHLY-RATE-AMT (1)
                //*  ELSE
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(1).setValue(ldaAial0595.getPnd_Ledger_Record_Pnd_L_Transaction_Amount());    //Natural: MOVE #L-TRANSACTION-AMOUNT TO NAZ-ACT-CREF-MTHLY-RATE-AMT ( 1 )
                //*  END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CREF-MTHLY-RATE-2
                sub_Get_Cref_Mthly_Rate_2();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Reserve_Switch.equals("Y")))                                                                                                                //Natural: IF #RESERVE-SWITCH = 'Y'
            {
                if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind().equals("A")))                                                                               //Natural: IF #IA-REVAL-IND = 'A'
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("Y");                                                                       //Natural: MOVE 'Y' TO NAZ-ACT-CREF-ANNUAL-CALL
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("N");                                                                        //Natural: MOVE 'N' TO NAZ-ACT-CREF-MTHLY-CALL
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(1).setValue(100000);                                                    //Natural: MOVE 100000 TO NAZ-ACT-CREF-ANNUAL-RATE-AMT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-CREF-ANNUAL-RATE
                    sub_Get_Cref_Annual_Rate();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("Y");                                                                        //Natural: MOVE 'Y' TO NAZ-ACT-CREF-MTHLY-CALL
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("N");                                                                       //Natural: MOVE 'N' TO NAZ-ACT-CREF-ANNUAL-CALL
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(1).setValue(100000);                                                     //Natural: MOVE 100000 TO NAZ-ACT-CREF-MTHLY-RATE-AMT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-CREF-MTHLY-RATE
                    sub_Get_Cref_Mthly_Rate();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  SPLAY NAZ-ACT-CREF-IA-CERT-NMBR
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(1);                                                                                           //Natural: MOVE 01 TO NAZ-ACT-ORIGIN-CDE
        DbsUtil.callnat(Aian022.class , getCurrentProcessState(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                                                               //Natural: CALLNAT 'AIAN022' NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        //*  IF #RESERVE-SWITCH = 'N'
        //*  WRITE(4)  NAZ-ACT-CREF-IA-CERT-NMBR  NAZ-ACT-EFF-DATE  #RESERVE-SWITCH
        getReports().write(4, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr(),pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(),               //Natural: WRITE ( 4 ) NAZ-ACT-TIAA-IA-CNTRCT-NMBR NAZ-ACT-EFF-DATE #RESERVE-SWITCH NAZ-ACT-CREF-ANNUAL-RATE-AMT ( 1 ) NAZ-ACT-ASD-DATE NAZ-ACT-CREF-ANNUAL-INFO ( 1 ) #IA-ISSUE-DATE #MONTHLY-SWITCH NAZ-ACT-OPTION-CDE NAZ-ACT-GUAR-PERIOD NAZ-ACT-RETURN-CODE
            pnd_Reserve_Switch,pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(1),pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Info().getValue(1),ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date(),pnd_Monthly_Switch,
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde(),pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period(),pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code());
        if (Global.isEscape()) return;
        //*  ELSE
        //*  RITE (5)  NAZ-ACT-CREF-IA-CERT-NMBR  NAZ-ACT-EFF-DATE  #RESERVE-SWITCH
        //*    NAZ-ACT-CREF-ANNUAL-RATE-AMT (1)  NAZ-ACT-ASD-DATE
        //*    NAZ-ACT-CREF-ANNUAL-INFO (1)  #IA-ISSUE-DATE
        //*    NAZ-ACT-OPTION-CDE NAZ-ACT-GUAR-PERIOD NAZ-ACT-RETURN-CODE
        //*  END-IF
        //* *****************************************************
        //* ***** NEXT LINE COMMENTED OUT ON 9/25/98 ****************
        //* ******************************************************
        if (condition(pnd_Reserve_Switch.equals("N")))                                                                                                                    //Natural: IF #RESERVE-SWITCH = 'N'
        {
            if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Reval_Ind().equals("A")))                                                                           //Natural: IF #LEDGER-REVAL-IND = 'A'
            {
                //*    IF #RESERVE-SWITCH NOT = 'Y'
                pnd_E_L_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(1));                                     //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-AMT ( 1 ) TO #E-L-CALCULATED-PAYMENT
                //*    END-IF
                pnd_E_L_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(1));                                    //Natural: MOVE NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( 1 ) TO #E-L-CALCULATED-UNITS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #RESERVE-SWITCH NOT = 'Y'
                pnd_E_L_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(1));                                     //Natural: MOVE NAZ-ACT-CREF-O-MTH-RATE-AMT ( 1 ) TO #E-L-CALCULATED-PAYMENT
                //*    END-IF
                pnd_E_L_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(1));                                    //Natural: MOVE NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( 1 ) TO #E-L-CALCULATED-UNITS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Reserve_Switch.equals("Y")))                                                                                                                //Natural: IF #RESERVE-SWITCH = 'Y'
            {
                if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind().equals("A")))                                                                               //Natural: IF #IA-REVAL-IND = 'A'
                {
                    pnd_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(1));                                     //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-AMT ( 1 ) TO #CALCULATED-PAYMENT
                    pnd_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(1));                                    //Natural: MOVE NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( 1 ) TO #CALCULATED-UNITS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Calculated_Payment.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(1));                                     //Natural: MOVE NAZ-ACT-CREF-O-MTH-RATE-AMT ( 1 ) TO #CALCULATED-PAYMENT
                    pnd_Calculated_Units.setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(1));                                    //Natural: MOVE NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( 1 ) TO #CALCULATED-UNITS
                }                                                                                                                                                         //Natural: END-IF
                //*    MOVE #E-L-CALCULATED-UNITS TO #CALCULATED-UNITS
                //*    MOVE NAZ-ACT-CREF-O-ANN-RATE-AMT (1)   TO #CALCULATED-PAYMENT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Cref_Annual_Rate() throws Exception                                                                                                              //Natural: GET-CREF-ANNUAL-RATE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1275 = 0;                                                                                                                                //Natural: DECIDE ON EVERY #IA-FUND-CODE;//Natural: VALUE 'C'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("C")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("38");                                                              //Natural: MOVE '38' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'M'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("M")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("20");                                                              //Natural: MOVE '20' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("S")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("18");                                                              //Natural: MOVE '18' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'B'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("B")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("19");                                                              //Natural: MOVE '19' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'W'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("W")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("17");                                                              //Natural: MOVE '17' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("L")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("16");                                                              //Natural: MOVE '16' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("F")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("13");                                                              //Natural: MOVE '13' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'R'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("R")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("14");                                                              //Natural: MOVE '14' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'U'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("U")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("01");                                                              //Natural: MOVE '01' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'V'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("V")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("02");                                                              //Natural: MOVE '02' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'N'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("N")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("03");                                                              //Natural: MOVE '03' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'O'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("O")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("04");                                                              //Natural: MOVE '04' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("P")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("05");                                                              //Natural: MOVE '05' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("Q")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("06");                                                              //Natural: MOVE '06' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'J'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("J")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("07");                                                              //Natural: MOVE '07' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'K'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("K")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("08");                                                              //Natural: MOVE '08' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("X")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("09");                                                              //Natural: MOVE '09' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("E")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("15");                                                              //Natural: MOVE '15' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'D'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("D")))
        {
            decideConditionsMet1275++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("LA");                                                              //Natural: MOVE 'LA' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: NONE
        if (condition(decideConditionsMet1275 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Cref_Mthly_Rate() throws Exception                                                                                                               //Natural: GET-CREF-MTHLY-RATE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1319 = 0;                                                                                                                                //Natural: DECIDE ON EVERY #IA-FUND-CODE;//Natural: VALUE 'C'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("C")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("38");                                                               //Natural: MOVE '38' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'M'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("M")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("20");                                                               //Natural: MOVE '20' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("S")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("18");                                                               //Natural: MOVE '18' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'B'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("B")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("19");                                                               //Natural: MOVE '19' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'W'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("W")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("17");                                                               //Natural: MOVE '17' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("L")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("16");                                                               //Natural: MOVE '16' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("F")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("13");                                                               //Natural: MOVE '13' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'R'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("R")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("14");                                                               //Natural: MOVE '14' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("E")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("15");                                                               //Natural: MOVE '15' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'U'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("U")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("01");                                                               //Natural: MOVE '01' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'V'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("V")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("02");                                                               //Natural: MOVE '02' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'N'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("N")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("03");                                                               //Natural: MOVE '03' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'O'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("O")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("04");                                                               //Natural: MOVE '04' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("P")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("05");                                                               //Natural: MOVE '05' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("Q")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("06");                                                               //Natural: MOVE '06' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'J'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("J")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("07");                                                               //Natural: MOVE '07' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'K'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("K")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("08");                                                               //Natural: MOVE '08' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("X")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("09");                                                               //Natural: MOVE '09' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("E")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("15");                                                               //Natural: MOVE '15' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'D'
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code().equals("D")))
        {
            decideConditionsMet1319++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("LA");                                                               //Natural: MOVE 'LA' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: NONE
        if (condition(decideConditionsMet1319 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Cref_Annual_Rate_2() throws Exception                                                                                                            //Natural: GET-CREF-ANNUAL-RATE-2
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1365 = 0;                                                                                                                                //Natural: DECIDE ON EVERY #LEDGER-FUND-CODE;//Natural: VALUE 'C'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("C")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("38");                                                              //Natural: MOVE '38' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'M'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("M")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("20");                                                              //Natural: MOVE '20' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("S")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("18");                                                              //Natural: MOVE '18' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'B'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("B")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("19");                                                              //Natural: MOVE '19' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'W'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("W")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("17");                                                              //Natural: MOVE '17' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("L")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("16");                                                              //Natural: MOVE '16' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("F")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("13");                                                              //Natural: MOVE '13' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'R'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("R")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("14");                                                              //Natural: MOVE '14' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("E")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("15");                                                              //Natural: MOVE '15' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'U'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("U")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("01");                                                              //Natural: MOVE '01' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'V'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("V")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("02");                                                              //Natural: MOVE '02' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'N'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("N")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("03");                                                              //Natural: MOVE '03' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'O'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("O")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("04");                                                              //Natural: MOVE '04' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("P")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("05");                                                              //Natural: MOVE '05' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("Q")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("06");                                                              //Natural: MOVE '06' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'J'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("J")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("07");                                                              //Natural: MOVE '07' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'K'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("K")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("08");                                                              //Natural: MOVE '08' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("X")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("09");                                                              //Natural: MOVE '09' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("E")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("15");                                                              //Natural: MOVE '15' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'D'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("D")))
        {
            decideConditionsMet1365++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(1).setValue("LA");                                                              //Natural: MOVE 'LA' TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: NONE
        if (condition(decideConditionsMet1365 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Cref_Mthly_Rate_2() throws Exception                                                                                                             //Natural: GET-CREF-MTHLY-RATE-2
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1411 = 0;                                                                                                                                //Natural: DECIDE ON EVERY #LEDGER-FUND-CODE;//Natural: VALUE 'C'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("C")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("38");                                                               //Natural: MOVE '38' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'M'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("M")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("20");                                                               //Natural: MOVE '20' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("S")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("18");                                                               //Natural: MOVE '18' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'B'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("B")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("19");                                                               //Natural: MOVE '19' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'W'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("W")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("17");                                                               //Natural: MOVE '17' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("L")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("16");                                                               //Natural: MOVE '16' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("F")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("13");                                                               //Natural: MOVE '13' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'R'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("R")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("14");                                                               //Natural: MOVE '14' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("E")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("15");                                                               //Natural: MOVE '15' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'U'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("U")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("01");                                                               //Natural: MOVE '01' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'V'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("V")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("02");                                                               //Natural: MOVE '02' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'N'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("N")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("03");                                                               //Natural: MOVE '03' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'O'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("O")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("04");                                                               //Natural: MOVE '04' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("P")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("05");                                                               //Natural: MOVE '05' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("Q")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("06");                                                               //Natural: MOVE '06' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'J'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("J")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("07");                                                               //Natural: MOVE '07' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'K'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("K")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("08");                                                               //Natural: MOVE '08' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("X")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("09");                                                               //Natural: MOVE '09' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'E'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("E")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("15");                                                               //Natural: MOVE '15' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'D'
        if (condition(ldaAial0595.getPnd_Ledger_Record_Pnd_Ledger_Fund_Code().equals("D")))
        {
            decideConditionsMet1411++;
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(1).setValue("LA");                                                               //Natural: MOVE 'LA' TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( 1 )
        }                                                                                                                                                                 //Natural: NONE
        if (condition(decideConditionsMet1411 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Calculate_Guar_Period() throws Exception                                                                                                             //Natural: CALCULATE-GUAR-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Final_Date_Pnd_Ia_Final_Yymm.setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date());                                                               //Natural: MOVE #IA-FINAL-PAY-DATE TO #IA-FINAL-YYMM
        //*  IF #IA-FINAL-PAY-DATE NOT = '000000'
        pnd_Ia_Final_Date_Pnd_Ia_Final_Dd.setValue("01");                                                                                                                 //Natural: MOVE '01' TO #IA-FINAL-DD
        //*  * SPLAY 'DATE1' #IA-FINAL-PAY-DATE
        //*  ISPLAY 'DATE' #IA-FINAL-DATE
        //*  IF #IA-FINAL-DATE NOT = '00000000'
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ia_Final_Date);                                    //Natural: MOVE EDITED #IA-FINAL-DATE TO NAZ-ACT-FNL-PAY-DATE ( EM = YYYYMMDD )
        pnd_Time_Between_Dates.compute(new ComputeParameters(false, pnd_Time_Between_Dates), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date())); //Natural: COMPUTE #TIME-BETWEEN-DATES = NAZ-ACT-FNL-PAY-DATE - NAZ-ACT-ASD-DATE
        //*  DIVIDE   365 INTO #TIME-BETWEEN-DATES
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period()),    //Natural: COMPUTE NAZ-ACT-GUAR-PERIOD = ( #TIME-BETWEEN-DATES / 365 ) + 1
            (pnd_Time_Between_Dates.divide(365)).add(1));
        //*  GIVING NAZ-ACT-GUAR-PERIOD REMAINDER  #EXTRA-TIME
        //*  DIVIDE ROUNDED  30 INTO #EXTRA-TIME  GIVING NAZ-ACT-GUAR-PERIOD-MTH
    }
    private void sub_Calculate_Guar_Period_2() throws Exception                                                                                                           //Natural: CALCULATE-GUAR-PERIOD-2
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Final_Date_Pnd_Ia_Final_Yymm.setValue(pdaAial0421.getPnd_Aian042_Linkage_Pnd_Aian042_Final_Per_Pay_Date());                                                //Natural: MOVE #AIAN042-FINAL-PER-PAY-DATE TO #IA-FINAL-YYMM
        pnd_Ia_Final_Date_Pnd_Ia_Final_Dd.setValue("01");                                                                                                                 //Natural: MOVE '01' TO #IA-FINAL-DD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ia_Final_Date);                                    //Natural: MOVE EDITED #IA-FINAL-DATE TO NAZ-ACT-FNL-PAY-DATE ( EM = YYYYMMDD )
        pnd_Time_Between_Dates.compute(new ComputeParameters(false, pnd_Time_Between_Dates), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date())); //Natural: COMPUTE #TIME-BETWEEN-DATES = NAZ-ACT-FNL-PAY-DATE - NAZ-ACT-ASD-DATE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period()),    //Natural: COMPUTE NAZ-ACT-GUAR-PERIOD = ( #TIME-BETWEEN-DATES / 365 ) + 1
            (pnd_Time_Between_Dates.divide(365)).add(1));
    }
    private void sub_Calculate_Reserve() throws Exception                                                                                                                 //Natural: CALCULATE-RESERVE
    {
        if (BLNatReinput.isReinput()) return;

        //*  WRITE 'CONTRACT' #IA-CONTRACT #IA-TRANSACTION-CODE
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(20) || ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code().equals(33)))            //Natural: IF #IA-TRANSACTION-CODE = 20 OR #IA-TRANSACTION-CODE = 33
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-RESERVE-AIAN022
            sub_Calculate_Reserve_Aian022();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-RECONNET-OUT
            sub_Write_Reconnet_Out();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-RESERVE-AIAN013
            sub_Calculate_Reserve_Aian013();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-RECONNET-OUT
            sub_Write_Reconnet_Out();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Reserve_Aian022() throws Exception                                                                                                         //Natural: CALCULATE-RESERVE-AIAN022
    {
        if (BLNatReinput.isReinput()) return;

        //*  WRITE '2#IACONTRACT' #IA-CONTRACT
        pnd_Ia_Reserve.reset();                                                                                                                                           //Natural: RESET #IA-RESERVE
        pnd_Reserve_Switch.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #RESERVE-SWITCH
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN022
        sub_Calculate_Ia_Units_Aian022();
        if (condition(Global.isEscape())) {return;}
        pnd_Reserve_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #RESERVE-SWITCH
        if (condition(pnd_Calculated_Units.notEquals(getZero())))                                                                                                         //Natural: IF #CALCULATED-UNITS NOT = 0
        {
            pnd_Ia_Reserve.compute(new ComputeParameters(false, pnd_Ia_Reserve), DbsField.multiply(100000,ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units()).divide(pnd_Calculated_Units)); //Natural: COMPUTE #IA-RESERVE = 100000 * #IA-PER-UNITS / #CALCULATED-UNITS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units().equals(getZero()) && pnd_Calculated_Payment.notEquals(getZero())))                                  //Natural: IF #IA-PER-UNITS = 0 AND #CALCULATED-PAYMENT NOT = 0
        {
            pnd_Ia_Reserve.compute(new ComputeParameters(false, pnd_Ia_Reserve), DbsField.multiply(100000,ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Payment()).divide(pnd_Calculated_Payment)); //Natural: COMPUTE #IA-RESERVE = 100000 * #IA-PER-PAYMENT / #CALCULATED-PAYMENT
        }                                                                                                                                                                 //Natural: END-IF
        //*  * SPLAY NOHDR
        //*    NAZ-ACT-CREF-IA-CERT-NMBR   #IA-PER-UNITS #CALCULATED-UNITS
        //*  WRITE NOHDR   NAZ-ACT-CREF-IA-CERT-NMBR
        //*    NAZ-ACT-CREF-O-MTH-UNIT-VALUE (1) NAZ-ACT-CREF-O-ANN-RATE-AMT (1)
        //*    NAZ-ACT-CREF-MTHLY-RATE-AMT (1) NAZ-ACT-CREF-ANNUAL-RATE-AMT (1)
        //*  D-IF
    }
    private void sub_Calculate_Reserve_Aian013() throws Exception                                                                                                         //Natural: CALCULATE-RESERVE-AIAN013
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Reserve.reset();                                                                                                                                           //Natural: RESET #IA-RESERVE
        pnd_Reserve_Switch.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #RESERVE-SWITCH
                                                                                                                                                                          //Natural: PERFORM CALCULATE-IA-UNITS-AIAN013
        sub_Calculate_Ia_Units_Aian013();
        if (condition(Global.isEscape())) {return;}
        pnd_Reserve_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #RESERVE-SWITCH
        pnd_Ia_Reserve.compute(new ComputeParameters(false, pnd_Ia_Reserve), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1).multiply(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units()).divide(100)); //Natural: COMPUTE #IA-RESERVE = #TRANSFER-AMT-OUT-CREF ( 1 ) * #IA-PER-UNITS / 100
        if (condition(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units().equals(getZero())))                                                                                 //Natural: IF #IA-PER-UNITS = 0
        {
            pnd_Ia_Reserve.compute(new ComputeParameters(false, pnd_Ia_Reserve), pdaAial0130.getPnd_Aian013_Linkage_Pnd_Transfer_Amt_Out_Cref().getValue(1).multiply(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Payment()).divide(100)); //Natural: COMPUTE #IA-RESERVE = #TRANSFER-AMT-OUT-CREF ( 1 ) * #IA-PER-PAYMENT / 100
        }                                                                                                                                                                 //Natural: END-IF
        //*  DISPLAY NOHDR #IA-CONTRACT #FUND-CODE-OUT (1) #AUV-OUT (1)
        //*   #ISSUE-DATE
        //*   #TRANSFER-AMT-OUT-CREF (1)
        //*  DISPLAY #TRANSFER-EFF-DATE-OUT #MODE #FINAL-PER-PAY-DATE
        //*  DISPLAY #TRANSFER-UNITS #UNITS-TO-RECEIVE (1) #UNITS-OUT (1)
    }
    private void sub_Write_Reconnet_Out() throws Exception                                                                                                                //Natural: WRITE-RECONNET-OUT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        ldaAial2000.getPnd_Out_Record().reset();                                                                                                                          //Natural: RESET #OUT-RECORD
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Key().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Key());                                                                  //Natural: MOVE #IA-KEY TO #OUT-KEY
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Contract().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Contract());                                                        //Natural: MOVE #IA-CONTRACT TO #OUT-CONTRACT
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Payee_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Payee_Code());                                                    //Natural: MOVE #IA-PAYEE-CODE TO #OUT-PAYEE-CODE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Da_Contract().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Da_Contract());                                                  //Natural: MOVE #IA-DA-CONTRACT TO #OUT-DA-CONTRACT
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Transaction_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Code());                                        //Natural: MOVE #IA-TRANSACTION-CODE TO #OUT-TRANSACTION-CODE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Transaction_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Transaction_Date());                                        //Natural: MOVE #IA-TRANSACTION-DATE TO #OUT-TRANSACTION-DATE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Effective_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Effective_Date());                                            //Natural: MOVE #IA-EFFECTIVE-DATE TO #OUT-EFFECTIVE-DATE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Check_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Check_Date());                                                    //Natural: MOVE #IA-CHECK-DATE TO #OUT-CHECK-DATE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Sequence_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sequence_Code());                                              //Natural: MOVE #IA-SEQUENCE-CODE TO #OUT-SEQUENCE-CODE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Fund_Code().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Fund_Code());                                                      //Natural: MOVE #IA-FUND-CODE TO #OUT-FUND-CODE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Rate_Basis().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Rate_Basis());                                                    //Natural: MOVE #IA-RATE-BASIS TO #OUT-RATE-BASIS
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Reval_Ind().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Reval_Ind());                                                      //Natural: MOVE #IA-REVAL-IND TO #OUT-REVAL-IND
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Per_Units().setValueEdited(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Units(),new ReportEditMask("S9999999.999"));             //Natural: MOVE EDITED #IA-PER-UNITS ( EM = S9999999.999 ) TO #OUT-PER-UNITS
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Per_Payment().setValueEdited(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Per_Payment(),new ReportEditMask("S9999999.99"));          //Natural: MOVE EDITED #IA-PER-PAYMENT ( EM = S9999999.99 ) TO #OUT-PER-PAYMENT
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Option().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Option());                                                            //Natural: MOVE #IA-OPTION TO #OUT-OPTION
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Mode().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode());                                                                //Natural: MOVE #IA-MODE TO #OUT-MODE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Origin().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Origin());                                                            //Natural: MOVE #IA-ORIGIN TO #OUT-ORIGIN
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Issue_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Issue_Date());                                                    //Natural: MOVE #IA-ISSUE-DATE TO #OUT-ISSUE-DATE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Final_Pay_Date().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Final_Pay_Date());                                            //Natural: MOVE #IA-FINAL-PAY-DATE TO #OUT-FINAL-PAY-DATE
        ldaAial2000.getPnd_Out_Record_Pnd_Out_First_Ann_Xref().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Xref());                                            //Natural: MOVE #IA-FIRST-ANN-XREF TO #OUT-FIRST-ANN-XREF
        ldaAial2000.getPnd_Out_Record_Pnd_Out_First_Ann_Dob().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dob());                                              //Natural: MOVE #IA-FIRST-ANN-DOB TO #OUT-FIRST-ANN-DOB
        ldaAial2000.getPnd_Out_Record_Pnd_Out_First_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Sex());                                              //Natural: MOVE #IA-FIRST-ANN-SEX TO #OUT-FIRST-ANN-SEX
        ldaAial2000.getPnd_Out_Record_Pnd_Out_First_Ann_Dod().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_First_Ann_Dod());                                              //Natural: MOVE #IA-FIRST-ANN-DOD TO #OUT-FIRST-ANN-DOD
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Sec_Ann_Xref().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Xref());                                                //Natural: MOVE #IA-SEC-ANN-XREF TO #OUT-SEC-ANN-XREF
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Sec_Ann_Dob().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dob());                                                  //Natural: MOVE #IA-SEC-ANN-DOB TO #OUT-SEC-ANN-DOB
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Sec_Ann_Sex().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Sex());                                                  //Natural: MOVE #IA-SEC-ANN-SEX TO #OUT-SEC-ANN-SEX
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Sec_Ann_Dod().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Sec_Ann_Dod());                                                  //Natural: MOVE #IA-SEC-ANN-DOD TO #OUT-SEC-ANN-DOD
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Beneficiary_Xref().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Beneficiary_Xref());                                        //Natural: MOVE #IA-BENEFICIARY-XREF TO #OUT-BENEFICIARY-XREF
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Mode_Change_Ind().setValue(ldaAial0595.getPnd_Ia_Record_Pnd_Ia_Mode_Change_Ind());                                          //Natural: MOVE #IA-MODE-CHANGE-IND TO #OUT-MODE-CHANGE-IND
        ldaAial2000.getPnd_Out_Record_Pnd_Out_Calculated_Asset().setValueEdited(pnd_Ia_Reserve,new ReportEditMask("S999999999.99"));                                      //Natural: MOVE EDITED #IA-RESERVE ( EM = S999999999.99 ) TO #OUT-CALCULATED-ASSET
        getWorkFiles().write(3, false, ldaAial2000.getPnd_Out_Record());                                                                                                  //Natural: WRITE WORK FILE 3 #OUT-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(32),"IA-LEDGER RECONCILIATION DETAILED REPORT",new ColumnSpacing(3),pnd_Header_Rec_Pnd_Start_Eff_Mon,"/",pnd_Header_Rec_Pnd_Start_Eff_Day,"/",pnd_Header_Rec_Pnd_Start_Eff_Year,NEWLINE,"IA TRANSACTION",new  //Natural: WRITE ( 1 ) 32X 'IA-LEDGER RECONCILIATION DETAILED REPORT' 3X #START-EFF-MON'/'#START-EFF-DAY'/'#START-EFF-YEAR / 'IA TRANSACTION' 27X 'PAYMENT/' 3X 'LEDGER ENTRY' 46X 'CALCULATED' / 'CONTRACT' 2X 'TYPE' 'DATE' 2X 'FUND/REVAL' 2X 'UNITS' 1X 'CALC ASSET' 'CONTRACT' 'ACCT' 'DR/CR' 'ACC DAT' 'EFF DATE' 'FUND/REVAL' 2X 'AMOUNT' 4X 'UNITS' 'PAYMENT' 'ERROR'
                        ColumnSpacing(27),"PAYMENT/",new ColumnSpacing(3),"LEDGER ENTRY",new ColumnSpacing(46),"CALCULATED",NEWLINE,"CONTRACT",new ColumnSpacing(2),"TYPE","DATE",new 
                        ColumnSpacing(2),"FUND/REVAL",new ColumnSpacing(2),"UNITS",new ColumnSpacing(1),"CALC ASSET","CONTRACT","ACCT","DR/CR","ACC DAT","EFF DATE","FUND/REVAL",new 
                        ColumnSpacing(2),"AMOUNT",new ColumnSpacing(4),"UNITS","PAYMENT","ERROR");
                    //*    '-' #END-EFF-MON'/'#END-EFF-DAY '/'#END-EFF-YEAR /
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, new ColumnSpacing(20),"IA-LEDGER RECONCILIATION LUMP SUM TRANSACTION REPORT",new ColumnSpacing(3),pnd_Header_Rec_Pnd_Start_Eff_Mon,"/",pnd_Header_Rec_Pnd_Start_Eff_Day,"/",pnd_Header_Rec_Pnd_Start_Eff_Year,NEWLINE,"IA TRANSACTION",new  //Natural: WRITE ( 2 ) 20X 'IA-LEDGER RECONCILIATION LUMP SUM TRANSACTION REPORT' 3X #START-EFF-MON'/'#START-EFF-DAY'/'#START-EFF-YEAR / 'IA TRANSACTION' 27X 'PAYMENT/' 3X 'LEDGER ENTRY' 46X 'CALCULATED' / 'CONTRACT' 2X 'TYPE' 'DATE' 2X 'FUND/REVAL' 2X 'UNITS' 1X 'CALC ASSET' 'CONTRACT' 'ACCT' 'DR/CR' 'ACC DAT' 'EFF DATE' 'FUND/REVAL' 2X 'AMOUNT' 4X 'UNITS' 'PAYMENT' 'ERROR'
                        ColumnSpacing(27),"PAYMENT/",new ColumnSpacing(3),"LEDGER ENTRY",new ColumnSpacing(46),"CALCULATED",NEWLINE,"CONTRACT",new ColumnSpacing(2),"TYPE","DATE",new 
                        ColumnSpacing(2),"FUND/REVAL",new ColumnSpacing(2),"UNITS",new ColumnSpacing(1),"CALC ASSET","CONTRACT","ACCT","DR/CR","ACC DAT","EFF DATE","FUND/REVAL",new 
                        ColumnSpacing(2),"AMOUNT",new ColumnSpacing(4),"UNITS","PAYMENT","ERROR");
                    //*    ' - ' #END-EFF-MON'/'#END-EFF-DAY'/'#END-EFF-YEAR /
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");

        getReports().setDisplayColumns(0, pnd_Ia_Output_File_Pnd_I_Contract);
    }
}
