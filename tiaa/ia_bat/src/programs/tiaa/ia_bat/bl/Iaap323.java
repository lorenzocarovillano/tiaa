/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:39 PM
**        * FROM NATURAL PROGRAM : Iaap323
************************************************************
**        * FILE NAME            : Iaap323.java
**        * CLASS NAME           : Iaap323
**        * INSTANCE NAME        : Iaap323
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: BUILD NET CHANGE FILE
**SAG SYSTEM: IASTPA
************************************************************************
* PROGRAM  : IAAP323
* SYSTEM   : IASTPA
* TITLE    : BUILD NET CHANGE FILE
* GENERATED: FEB 05,96 AT 04:59 PM
* FUNCTION : THIS PROGRAM WILL READ A MERGED CURRENT AND PRIOR
*          | CPS GROSS TO NET FILE AND BUILD A NET CHANGE FILE THAT
*          | WILL BE USED TO PRODUCE NET CHANGE STATEMENTS.
*          |
*          |
*          |
* MOD DATE   MOD BY     DESCRIPTION OF CHANGES
* ---------+---------------------------------------
* 10 18 96  ________   CORRECT EXAMINE STATEMENT  DO SCAN ON 10/96
* 09/16/97  R SALGADO  RECOMPILE WITH NEW FCPA800 (NO OTHER CHANGES)
* 03/98     R CARREON  INCLUDES PROCESSING OF VALUATION PERIOD.
* 07/13/00  J HOFSTET  PA SELECT/SPIA-INITIALIZE NEW FIELDS ON IAAA323
* 03/06/01  M MURILLO  - ADD CNTRCT-OPTION-CDE IN IAAA323
*                      - POPULATE CNTRCT-OPTION-CDE FOR USE BY TPA,
*                        IPRO AND P&I PAYMENT PROCESSING.
* 01/09/06  RAMANA ANNE - ARKANSAS,DELAWARE,KANSAS,NEBRASKA,MARYLAND
*                       - NORTH CAROLINA ADDED AS A MANDATORY STATES
* 08/13/08  ART RONDEAU - RESTOW TO PICK UP NEW FCPA800 - ROTH-MAJOR1
* 03/07/08  J BREMER    - PASS CANADIAN TAX IN DEDUCTION BUCKETS
* 12/16/11  J TINIO     - RECATALOG WITH CHANGES TO FCPA800
* 04/21/17  SAI K       - PIN EXPANSION
* 10/09/18  GHOSHJ      - ADDED CONNECTICUT AS A NEW MANDATORY STATE
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap323 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private LdaFcpl199a ldaFcpl199a;
    private LdaFcpl810 ldaFcpl810;
    private PdaIaaa323 pdaIaaa323;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Prior_Curr_Month_Flag;

    private DbsGroup pnd_Prior_Curr_Month_Flag__R_Field_1;
    private DbsField pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr;

    private DbsGroup pnd_Gtn_Rec;
    private DbsField pnd_Gtn_Rec_Pnd_Pin;
    private DbsField pnd_Gtn_Rec_Pnd_Contract;
    private DbsField pnd_Gtn_Rec_Pnd_Payee;
    private DbsField pnd_Gtn_Rec_Pnd_Month_Flag;
    private DbsField pnd_Gtn_Rec_Pnd_Gtn_Data;
    private DbsField pnd_Gtn_Rec_Pnd_Canadian_Tax;
    private DbsField pnd_Mailing_Address;

    private DbsGroup pnd_Mailing_Address__R_Field_2;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_1;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_2;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_3;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_4;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_5;
    private DbsField pnd_Mailing_Address_Pnd_Mailing_Addr_6;
    private DbsField pnd_Bad_Pin_Cnt;
    private DbsField pnd_Bad_Pin_Cnt_Prior;
    private DbsField pnd_Bad_Pin_Cnt_Curr;
    private DbsField pnd_Tpa_Cnt;
    private DbsField pnd_Tpa_Cnt_Prior;
    private DbsField pnd_Tpa_Cnt_Curr;
    private DbsField pnd_Canadian_Cnt;
    private DbsField pnd_New_Issue_Cnt;
    private DbsField pnd_Terminated_Cnt;
    private DbsField pnd_Unknown_Month_Cnt;
    private DbsField pnd_Duplicate_Cnt;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Read_Prior_Cnt;
    private DbsField pnd_Read_Curr_Cnt;
    private DbsField pnd_Group_Curr_Cnt;
    private DbsField pnd_Group_Prior_Cnt;
    private DbsField pnd_Match_Cnt;
    private DbsField pnd_Net_Chg_Cnt;
    private DbsField pnd_Mailing_Label_Cnt;

    private DbsGroup pnd_Control_Totals;
    private DbsField pnd_Control_Totals_Pnd_Tot_Desc;
    private DbsField pnd_Control_Totals_Pnd_Tot_Read_Cnt;
    private DbsField pnd_Control_Totals_Pnd_Tot_Contract_Amt;
    private DbsField pnd_Control_Totals_Pnd_Tot_Dividend_Amt;
    private DbsField pnd_Control_Totals_Pnd_Tot_Cref_Sttl_Amt;
    private DbsField pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt;
    private DbsField pnd_Control_Totals_Pnd_Tot_Net_Pymnt_Amt;
    private DbsField pnd_X;
    private DbsField pnd_Eof;
    private DbsField pnd_Sub;
    private DbsField pnd_Ded_Sub;
    private DbsField pnd_Fund_Sub;
    private DbsField pnd_Fund_How_Many;
    private DbsField pnd_Fund_Tbl_Sub;
    private DbsField pnd_Net_Chg_Flag;
    private DbsField pnd_Rpt_Cde_Ndx;
    private DbsField pnd_Y;
    private DbsField pnd_Xx;
    private DbsField pnd_Write_Fund_Ctr;
    private DbsField pnd_M;

    private DbsGroup pnd_Ws_Save_Fund_Info;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_N;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Save_Valuat;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_A;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Qty;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Value;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Settlement_Amt;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Contract_Amt;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Dividend_Amt;
    private DbsField pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Net_Pymnt_Amt;
    private DbsField pnd_Prev_Valuat_Fund_Cde;

    private DbsGroup pnd_Prev_Valuat_Fund_Cde__R_Field_3;
    private DbsField pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Valuat;
    private DbsField pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Fund_Cde_A;
    private DbsField pnd_Temp_Date;

    private DbsGroup pnd_Temp_Date__R_Field_4;
    private DbsField pnd_Temp_Date_Pnd_Temp_Date_A;
    private DbsField pnd_Temp_Ded;

    private DbsGroup pnd_Temp_Ded__R_Field_5;
    private DbsField pnd_Temp_Ded_Pnd_Temp_Ded_A;
    private DbsField pnd_Prev_Pin;

    private DbsGroup pnd_States;
    private DbsField pnd_States_Pnd_Mstate_Nbr;
    private DbsField pnd_Curr_Chk_Dte;

    private DbsGroup pnd_Curr_Chk_Dte__R_Field_6;
    private DbsField pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Yyyy;
    private DbsField pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm;
    private DbsField pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Dd;
    private DbsField pnd_Future_Dte;

    private DbsGroup pnd_Future_Dte__R_Field_7;
    private DbsField pnd_Future_Dte_Pnd_Future_Yyyy;
    private DbsField pnd_Future_Dte_Pnd_Future_Mm;
    private DbsField pnd_Future_Dte_Pnd_Future_Dd;
    private DbsField pnd_Hold_Mode;

    private DbsGroup pnd_Hold_Mode__R_Field_8;
    private DbsField pnd_Hold_Mode__Filler1;
    private DbsField pnd_Hold_Mode_Pnd_Hold_Mode_Mm;
    private DbsField pnd_Curr_Cycle_Dte;

    private DbsGroup pnd_Curr_Cycle_Dte__R_Field_9;
    private DbsField pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Mm;
    private DbsField pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler1;
    private DbsField pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Dd;
    private DbsField pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler2;
    private DbsField pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Yyyy;
    private DbsField pnd_Prior_Cycle_Dte;

    private DbsGroup pnd_Prior_Cycle_Dte__R_Field_10;
    private DbsField pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Mm;
    private DbsField pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler1;
    private DbsField pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Dd;
    private DbsField pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler2;
    private DbsField pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Yyyy;
    private DbsField pnd_Gtn_Rpt_Cnt;
    private DbsField pnd_Rpt_Code;
    private DbsField pnd_Rpt_Code_Max;
    private DbsField pnd_Got_Curr_Cycle_Dte;
    private DbsField pnd_Got_Prior_Cycle_Dte;
    private DbsField pnd_Verify_Cycle_Dte;
    private DbsField pnd_Cycle_Yyyy_Diff;
    private DbsField pnd_Cycle_Error;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);
        ldaFcpl810 = new LdaFcpl810();
        registerRecord(ldaFcpl810);
        pdaIaaa323 = new PdaIaaa323(localVariables);

        // Local Variables
        pnd_Prior_Curr_Month_Flag = localVariables.newFieldInRecord("pnd_Prior_Curr_Month_Flag", "#PRIOR-CURR-MONTH-FLAG", FieldType.NUMERIC, 1);

        pnd_Prior_Curr_Month_Flag__R_Field_1 = localVariables.newGroupInRecord("pnd_Prior_Curr_Month_Flag__R_Field_1", "REDEFINE", pnd_Prior_Curr_Month_Flag);
        pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr = pnd_Prior_Curr_Month_Flag__R_Field_1.newFieldInGroup("pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr", "#PRIOR-CURR", 
            FieldType.NUMERIC, 1);

        pnd_Gtn_Rec = localVariables.newGroupArrayInRecord("pnd_Gtn_Rec", "#GTN-REC", new DbsArrayController(1, 2));
        pnd_Gtn_Rec_Pnd_Pin = pnd_Gtn_Rec.newFieldInGroup("pnd_Gtn_Rec_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Gtn_Rec_Pnd_Contract = pnd_Gtn_Rec.newFieldInGroup("pnd_Gtn_Rec_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Gtn_Rec_Pnd_Payee = pnd_Gtn_Rec.newFieldInGroup("pnd_Gtn_Rec_Pnd_Payee", "#PAYEE", FieldType.STRING, 4);
        pnd_Gtn_Rec_Pnd_Month_Flag = pnd_Gtn_Rec.newFieldInGroup("pnd_Gtn_Rec_Pnd_Month_Flag", "#MONTH-FLAG", FieldType.NUMERIC, 1);
        pnd_Gtn_Rec_Pnd_Gtn_Data = pnd_Gtn_Rec.newFieldArrayInGroup("pnd_Gtn_Rec_Pnd_Gtn_Data", "#GTN-DATA", FieldType.STRING, 250, new DbsArrayController(1, 
            12));
        pnd_Gtn_Rec_Pnd_Canadian_Tax = pnd_Gtn_Rec.newFieldInGroup("pnd_Gtn_Rec_Pnd_Canadian_Tax", "#CANADIAN-TAX", FieldType.NUMERIC, 11, 2);
        pnd_Mailing_Address = localVariables.newFieldInRecord("pnd_Mailing_Address", "#MAILING-ADDRESS", FieldType.STRING, 210);

        pnd_Mailing_Address__R_Field_2 = localVariables.newGroupInRecord("pnd_Mailing_Address__R_Field_2", "REDEFINE", pnd_Mailing_Address);
        pnd_Mailing_Address_Pnd_Mailing_Addr_1 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_1", "#MAILING-ADDR-1", 
            FieldType.STRING, 35);
        pnd_Mailing_Address_Pnd_Mailing_Addr_2 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_2", "#MAILING-ADDR-2", 
            FieldType.STRING, 35);
        pnd_Mailing_Address_Pnd_Mailing_Addr_3 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_3", "#MAILING-ADDR-3", 
            FieldType.STRING, 35);
        pnd_Mailing_Address_Pnd_Mailing_Addr_4 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_4", "#MAILING-ADDR-4", 
            FieldType.STRING, 35);
        pnd_Mailing_Address_Pnd_Mailing_Addr_5 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_5", "#MAILING-ADDR-5", 
            FieldType.STRING, 35);
        pnd_Mailing_Address_Pnd_Mailing_Addr_6 = pnd_Mailing_Address__R_Field_2.newFieldInGroup("pnd_Mailing_Address_Pnd_Mailing_Addr_6", "#MAILING-ADDR-6", 
            FieldType.STRING, 35);
        pnd_Bad_Pin_Cnt = localVariables.newFieldInRecord("pnd_Bad_Pin_Cnt", "#BAD-PIN-CNT", FieldType.NUMERIC, 7);
        pnd_Bad_Pin_Cnt_Prior = localVariables.newFieldInRecord("pnd_Bad_Pin_Cnt_Prior", "#BAD-PIN-CNT-PRIOR", FieldType.NUMERIC, 7);
        pnd_Bad_Pin_Cnt_Curr = localVariables.newFieldInRecord("pnd_Bad_Pin_Cnt_Curr", "#BAD-PIN-CNT-CURR", FieldType.NUMERIC, 7);
        pnd_Tpa_Cnt = localVariables.newFieldInRecord("pnd_Tpa_Cnt", "#TPA-CNT", FieldType.NUMERIC, 7);
        pnd_Tpa_Cnt_Prior = localVariables.newFieldInRecord("pnd_Tpa_Cnt_Prior", "#TPA-CNT-PRIOR", FieldType.NUMERIC, 7);
        pnd_Tpa_Cnt_Curr = localVariables.newFieldInRecord("pnd_Tpa_Cnt_Curr", "#TPA-CNT-CURR", FieldType.NUMERIC, 7);
        pnd_Canadian_Cnt = localVariables.newFieldInRecord("pnd_Canadian_Cnt", "#CANADIAN-CNT", FieldType.NUMERIC, 7);
        pnd_New_Issue_Cnt = localVariables.newFieldInRecord("pnd_New_Issue_Cnt", "#NEW-ISSUE-CNT", FieldType.NUMERIC, 7);
        pnd_Terminated_Cnt = localVariables.newFieldInRecord("pnd_Terminated_Cnt", "#TERMINATED-CNT", FieldType.NUMERIC, 7);
        pnd_Unknown_Month_Cnt = localVariables.newFieldInRecord("pnd_Unknown_Month_Cnt", "#UNKNOWN-MONTH-CNT", FieldType.NUMERIC, 7);
        pnd_Duplicate_Cnt = localVariables.newFieldInRecord("pnd_Duplicate_Cnt", "#DUPLICATE-CNT", FieldType.NUMERIC, 7);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 7);
        pnd_Read_Prior_Cnt = localVariables.newFieldInRecord("pnd_Read_Prior_Cnt", "#READ-PRIOR-CNT", FieldType.NUMERIC, 7);
        pnd_Read_Curr_Cnt = localVariables.newFieldInRecord("pnd_Read_Curr_Cnt", "#READ-CURR-CNT", FieldType.NUMERIC, 7);
        pnd_Group_Curr_Cnt = localVariables.newFieldInRecord("pnd_Group_Curr_Cnt", "#GROUP-CURR-CNT", FieldType.NUMERIC, 7);
        pnd_Group_Prior_Cnt = localVariables.newFieldInRecord("pnd_Group_Prior_Cnt", "#GROUP-PRIOR-CNT", FieldType.NUMERIC, 7);
        pnd_Match_Cnt = localVariables.newFieldInRecord("pnd_Match_Cnt", "#MATCH-CNT", FieldType.NUMERIC, 7);
        pnd_Net_Chg_Cnt = localVariables.newFieldInRecord("pnd_Net_Chg_Cnt", "#NET-CHG-CNT", FieldType.NUMERIC, 7);
        pnd_Mailing_Label_Cnt = localVariables.newFieldInRecord("pnd_Mailing_Label_Cnt", "#MAILING-LABEL-CNT", FieldType.NUMERIC, 7);

        pnd_Control_Totals = localVariables.newGroupArrayInRecord("pnd_Control_Totals", "#CONTROL-TOTALS", new DbsArrayController(1, 2));
        pnd_Control_Totals_Pnd_Tot_Desc = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Desc", "#TOT-DESC", FieldType.STRING, 10);
        pnd_Control_Totals_Pnd_Tot_Read_Cnt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Read_Cnt", "#TOT-READ-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Control_Totals_Pnd_Tot_Contract_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Contract_Amt", "#TOT-CONTRACT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Totals_Pnd_Tot_Dividend_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Dividend_Amt", "#TOT-DIVIDEND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Totals_Pnd_Tot_Cref_Sttl_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Cref_Sttl_Amt", "#TOT-CREF-STTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt", "#TOT-STTLMNT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Control_Totals_Pnd_Tot_Net_Pymnt_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Tot_Net_Pymnt_Amt", "#TOT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Ded_Sub = localVariables.newFieldInRecord("pnd_Ded_Sub", "#DED-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Fund_Sub = localVariables.newFieldInRecord("pnd_Fund_Sub", "#FUND-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Fund_How_Many = localVariables.newFieldInRecord("pnd_Fund_How_Many", "#FUND-HOW-MANY", FieldType.PACKED_DECIMAL, 3);
        pnd_Fund_Tbl_Sub = localVariables.newFieldInRecord("pnd_Fund_Tbl_Sub", "#FUND-TBL-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Net_Chg_Flag = localVariables.newFieldInRecord("pnd_Net_Chg_Flag", "#NET-CHG-FLAG", FieldType.BOOLEAN, 1);
        pnd_Rpt_Cde_Ndx = localVariables.newFieldInRecord("pnd_Rpt_Cde_Ndx", "#RPT-CDE-NDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Xx = localVariables.newFieldInRecord("pnd_Xx", "#XX", FieldType.NUMERIC, 3);
        pnd_Write_Fund_Ctr = localVariables.newFieldInRecord("pnd_Write_Fund_Ctr", "#WRITE-FUND-CTR", FieldType.NUMERIC, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);

        pnd_Ws_Save_Fund_Info = localVariables.newGroupArrayInRecord("pnd_Ws_Save_Fund_Info", "#WS-SAVE-FUND-INFO", new DbsArrayController(1, 40));
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_N = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_N", "#FUND-ACCOUNT-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Save_Fund_Info_Pnd_Save_Valuat = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Save_Valuat", "#SAVE-VALUAT", FieldType.STRING, 
            1);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_A = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_A", "#FUND-ACCOUNT-CDE-A", 
            FieldType.STRING, 2);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Qty = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Qty", "#FUND-CURR-UNIT-QTY", 
            FieldType.NUMERIC, 9, 3);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Value = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Value", "#FUND-CURR-UNIT-VALUE", 
            FieldType.NUMERIC, 9, 4);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Settlement_Amt = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Settlement_Amt", 
            "#FUND-CURR-SETTLEMENT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Contract_Amt = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Contract_Amt", "#FUND-CURR-CONTRACT-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Dividend_Amt = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Dividend_Amt", "#FUND-CURR-DIVIDEND-AMT", 
            FieldType.NUMERIC, 9, 2);
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Net_Pymnt_Amt = pnd_Ws_Save_Fund_Info.newFieldInGroup("pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Net_Pymnt_Amt", 
            "#FUND-CURR-NET-PYMNT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Prev_Valuat_Fund_Cde = localVariables.newFieldInRecord("pnd_Prev_Valuat_Fund_Cde", "#PREV-VALUAT-FUND-CDE", FieldType.STRING, 3);

        pnd_Prev_Valuat_Fund_Cde__R_Field_3 = localVariables.newGroupInRecord("pnd_Prev_Valuat_Fund_Cde__R_Field_3", "REDEFINE", pnd_Prev_Valuat_Fund_Cde);
        pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Valuat = pnd_Prev_Valuat_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Valuat", "#PREV-VALUAT", 
            FieldType.STRING, 1);
        pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Fund_Cde_A = pnd_Prev_Valuat_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Fund_Cde_A", 
            "#PREV-FUND-CDE-A", FieldType.STRING, 2);
        pnd_Temp_Date = localVariables.newFieldInRecord("pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);

        pnd_Temp_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_Date__R_Field_4", "REDEFINE", pnd_Temp_Date);
        pnd_Temp_Date_Pnd_Temp_Date_A = pnd_Temp_Date__R_Field_4.newFieldInGroup("pnd_Temp_Date_Pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);
        pnd_Temp_Ded = localVariables.newFieldInRecord("pnd_Temp_Ded", "#TEMP-DED", FieldType.NUMERIC, 3);

        pnd_Temp_Ded__R_Field_5 = localVariables.newGroupInRecord("pnd_Temp_Ded__R_Field_5", "REDEFINE", pnd_Temp_Ded);
        pnd_Temp_Ded_Pnd_Temp_Ded_A = pnd_Temp_Ded__R_Field_5.newFieldInGroup("pnd_Temp_Ded_Pnd_Temp_Ded_A", "#TEMP-DED-A", FieldType.STRING, 3);
        pnd_Prev_Pin = localVariables.newFieldInRecord("pnd_Prev_Pin", "#PREV-PIN", FieldType.NUMERIC, 12);

        pnd_States = localVariables.newGroupArrayInRecord("pnd_States", "#STATES", new DbsArrayController(1, 16));
        pnd_States_Pnd_Mstate_Nbr = pnd_States.newFieldInGroup("pnd_States_Pnd_Mstate_Nbr", "#MSTATE-NBR", FieldType.STRING, 2);
        pnd_Curr_Chk_Dte = localVariables.newFieldInRecord("pnd_Curr_Chk_Dte", "#CURR-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Curr_Chk_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Chk_Dte__R_Field_6", "REDEFINE", pnd_Curr_Chk_Dte);
        pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Yyyy = pnd_Curr_Chk_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Yyyy", "#CURR-CHK-DTE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm = pnd_Curr_Chk_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm", "#CURR-CHK-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Dd = pnd_Curr_Chk_Dte__R_Field_6.newFieldInGroup("pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Dd", "#CURR-CHK-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Future_Dte = localVariables.newFieldInRecord("pnd_Future_Dte", "#FUTURE-DTE", FieldType.NUMERIC, 8);

        pnd_Future_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Future_Dte__R_Field_7", "REDEFINE", pnd_Future_Dte);
        pnd_Future_Dte_Pnd_Future_Yyyy = pnd_Future_Dte__R_Field_7.newFieldInGroup("pnd_Future_Dte_Pnd_Future_Yyyy", "#FUTURE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Future_Dte_Pnd_Future_Mm = pnd_Future_Dte__R_Field_7.newFieldInGroup("pnd_Future_Dte_Pnd_Future_Mm", "#FUTURE-MM", FieldType.NUMERIC, 2);
        pnd_Future_Dte_Pnd_Future_Dd = pnd_Future_Dte__R_Field_7.newFieldInGroup("pnd_Future_Dte_Pnd_Future_Dd", "#FUTURE-DD", FieldType.NUMERIC, 2);
        pnd_Hold_Mode = localVariables.newFieldInRecord("pnd_Hold_Mode", "#HOLD-MODE", FieldType.NUMERIC, 3);

        pnd_Hold_Mode__R_Field_8 = localVariables.newGroupInRecord("pnd_Hold_Mode__R_Field_8", "REDEFINE", pnd_Hold_Mode);
        pnd_Hold_Mode__Filler1 = pnd_Hold_Mode__R_Field_8.newFieldInGroup("pnd_Hold_Mode__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Hold_Mode_Pnd_Hold_Mode_Mm = pnd_Hold_Mode__R_Field_8.newFieldInGroup("pnd_Hold_Mode_Pnd_Hold_Mode_Mm", "#HOLD-MODE-MM", FieldType.NUMERIC, 
            2);
        pnd_Curr_Cycle_Dte = localVariables.newFieldInRecord("pnd_Curr_Cycle_Dte", "#CURR-CYCLE-DTE", FieldType.STRING, 10);

        pnd_Curr_Cycle_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Curr_Cycle_Dte__R_Field_9", "REDEFINE", pnd_Curr_Cycle_Dte);
        pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Mm = pnd_Curr_Cycle_Dte__R_Field_9.newFieldInGroup("pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Mm", "#CURR-CYCLE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler1 = pnd_Curr_Cycle_Dte__R_Field_9.newFieldInGroup("pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler1", "#CURR-CYCLE-FILLER1", 
            FieldType.STRING, 1);
        pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Dd = pnd_Curr_Cycle_Dte__R_Field_9.newFieldInGroup("pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Dd", "#CURR-CYCLE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler2 = pnd_Curr_Cycle_Dte__R_Field_9.newFieldInGroup("pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Filler2", "#CURR-CYCLE-FILLER2", 
            FieldType.STRING, 1);
        pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Yyyy = pnd_Curr_Cycle_Dte__R_Field_9.newFieldInGroup("pnd_Curr_Cycle_Dte_Pnd_Curr_Cycle_Yyyy", "#CURR-CYCLE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Prior_Cycle_Dte = localVariables.newFieldInRecord("pnd_Prior_Cycle_Dte", "#PRIOR-CYCLE-DTE", FieldType.STRING, 10);

        pnd_Prior_Cycle_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Prior_Cycle_Dte__R_Field_10", "REDEFINE", pnd_Prior_Cycle_Dte);
        pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Mm = pnd_Prior_Cycle_Dte__R_Field_10.newFieldInGroup("pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Mm", "#PRIOR-CYCLE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler1 = pnd_Prior_Cycle_Dte__R_Field_10.newFieldInGroup("pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler1", "#PRIOR-CYCLE-FILLER1", 
            FieldType.STRING, 1);
        pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Dd = pnd_Prior_Cycle_Dte__R_Field_10.newFieldInGroup("pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Dd", "#PRIOR-CYCLE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler2 = pnd_Prior_Cycle_Dte__R_Field_10.newFieldInGroup("pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Filler2", "#PRIOR-CYCLE-FILLER2", 
            FieldType.STRING, 1);
        pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Yyyy = pnd_Prior_Cycle_Dte__R_Field_10.newFieldInGroup("pnd_Prior_Cycle_Dte_Pnd_Prior_Cycle_Yyyy", "#PRIOR-CYCLE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Gtn_Rpt_Cnt = localVariables.newFieldArrayInRecord("pnd_Gtn_Rpt_Cnt", "#GTN-RPT-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            2, 1, 30));
        pnd_Rpt_Code = localVariables.newFieldInRecord("pnd_Rpt_Code", "#RPT-CODE", FieldType.NUMERIC, 2);
        pnd_Rpt_Code_Max = localVariables.newFieldInRecord("pnd_Rpt_Code_Max", "#RPT-CODE-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Got_Curr_Cycle_Dte = localVariables.newFieldInRecord("pnd_Got_Curr_Cycle_Dte", "#GOT-CURR-CYCLE-DTE", FieldType.BOOLEAN, 1);
        pnd_Got_Prior_Cycle_Dte = localVariables.newFieldInRecord("pnd_Got_Prior_Cycle_Dte", "#GOT-PRIOR-CYCLE-DTE", FieldType.BOOLEAN, 1);
        pnd_Verify_Cycle_Dte = localVariables.newFieldInRecord("pnd_Verify_Cycle_Dte", "#VERIFY-CYCLE-DTE", FieldType.BOOLEAN, 1);
        pnd_Cycle_Yyyy_Diff = localVariables.newFieldInRecord("pnd_Cycle_Yyyy_Diff", "#CYCLE-YYYY-DIFF", FieldType.PACKED_DECIMAL, 3);
        pnd_Cycle_Error = localVariables.newFieldInRecord("pnd_Cycle_Error", "#CYCLE-ERROR", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl199a.initializeValues();
        ldaFcpl810.initializeValues();

        localVariables.reset();
        pnd_Control_Totals_Pnd_Tot_Desc.getValue(1).setInitialValue("CURRENT");
        pnd_Control_Totals_Pnd_Tot_Desc.getValue(2).setInitialValue("PRIOR");
        pnd_Control_Totals_Pnd_Tot_Read_Cnt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Tot_Contract_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Tot_Dividend_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Tot_Cref_Sttl_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Tot_Net_Pymnt_Amt.getValue(1).setInitialValue(0);
        pnd_X.setInitialValue(0);
        pnd_Eof.setInitialValue(false);
        pnd_Net_Chg_Flag.setInitialValue(false);
        pnd_Prev_Pin.setInitialValue(0);
        pnd_States_Pnd_Mstate_Nbr.getValue(1).setInitialValue("04");
        pnd_States_Pnd_Mstate_Nbr.getValue(2).setInitialValue("05");
        pnd_States_Pnd_Mstate_Nbr.getValue(3).setInitialValue("08");
        pnd_States_Pnd_Mstate_Nbr.getValue(4).setInitialValue("09");
        pnd_States_Pnd_Mstate_Nbr.getValue(5).setInitialValue("12");
        pnd_States_Pnd_Mstate_Nbr.getValue(6).setInitialValue("18");
        pnd_States_Pnd_Mstate_Nbr.getValue(7).setInitialValue("19");
        pnd_States_Pnd_Mstate_Nbr.getValue(8).setInitialValue("22");
        pnd_States_Pnd_Mstate_Nbr.getValue(9).setInitialValue("23");
        pnd_States_Pnd_Mstate_Nbr.getValue(10).setInitialValue("24");
        pnd_States_Pnd_Mstate_Nbr.getValue(11).setInitialValue("30");
        pnd_States_Pnd_Mstate_Nbr.getValue(12).setInitialValue("36");
        pnd_States_Pnd_Mstate_Nbr.getValue(13).setInitialValue("39");
        pnd_States_Pnd_Mstate_Nbr.getValue(14).setInitialValue("40");
        pnd_States_Pnd_Mstate_Nbr.getValue(15).setInitialValue("50");
        pnd_States_Pnd_Mstate_Nbr.getValue(16).setInitialValue("51");
        pnd_Curr_Cycle_Dte.setInitialValue("  /  /    ");
        pnd_Prior_Cycle_Dte.setInitialValue("  /  /    ");
        pnd_Rpt_Code_Max.setInitialValue(0);
        pnd_Got_Curr_Cycle_Dte.setInitialValue(false);
        pnd_Got_Prior_Cycle_Dte.setInitialValue(false);
        pnd_Verify_Cycle_Dte.setInitialValue(true);
        pnd_Cycle_Yyyy_Diff.setInitialValue(0);
        pnd_Cycle_Error.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap323() throws Exception
    {
        super("Iaap323");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP323", onError);
        setupReports();
        //*  -------------------------------------------
        pnd_Gtn_Rpt_Cnt.getValue("*","*").setValue(0);                                                                                                                    //Natural: ASSIGN #GTN-RPT-CNT ( *,* ) := 0
        FOR01:                                                                                                                                                            //Natural: FOR #RPT-CODE = 1 TO 30
        for (pnd_Rpt_Code.setValue(1); condition(pnd_Rpt_Code.lessOrEqual(30)); pnd_Rpt_Code.nadd(1))
        {
            if (condition(ldaFcpl810.getPnd_Gtn_Rpt_Msg_Pnd_Gtn_Rpt_Desc().getValue(pnd_Rpt_Code).notEquals(" ")))                                                        //Natural: IF #GTN-RPT-DESC ( #RPT-CODE ) NE ' '
            {
                pnd_Rpt_Code_Max.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RPT-CODE-MAX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * *ERROR-TA := 'INFP9000'                                                                                                                                     //Natural: FORMAT ( 0 ) LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT ( 3 ) LS = 132 PS = 60
        //* ******************** START OF MAIN PROGRAM LOGIC AND LOOP *************
        pnd_Sub.reset();                                                                                                                                                  //Natural: RESET #SUB
        //* *                                                                                                                                                             //Natural: ON ERROR
        MAIN_LOOP:                                                                                                                                                        //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  MUST HAVE TWO CONSECUTIVE RECORDS
            READ_GTN_FILE:                                                                                                                                                //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Sub.equals(2))) {break;}                                                                                                                //Natural: UNTIL #SUB = 2
                //*  TO COMPARE
                getWorkFiles().read(1, pnd_Prior_Curr_Month_Flag, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                   //Natural: READ WORK FILE 1 ONCE #PRIOR-CURR-MONTH-FLAG WF-PYMNT-ADDR-REC
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                    pnd_Eof.setValue(true);                                                                                                                               //Natural: ASSIGN #EOF := TRUE
                    if (true) break READ_GTN_FILE;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ-GTN-FILE. )
                }                                                                                                                                                         //Natural: END-ENDFILE
                pnd_Read_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-CNT
                if (condition(pnd_Prior_Curr_Month_Flag.equals(1)))                                                                                                       //Natural: IF #PRIOR-CURR-MONTH-FLAG = 1
                {
                    pnd_Read_Curr_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ-CURR-CNT
                    if (condition(pnd_Read_Curr_Cnt.equals(1)))                                                                                                           //Natural: IF #READ-CURR-CNT = 1
                    {
                        pnd_Curr_Cycle_Dte.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("MM/DD/YYYY"));                            //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = MM/DD/YYYY ) TO #CURR-CYCLE-DTE
                        pnd_Got_Curr_Cycle_Dte.setValue(true);                                                                                                            //Natural: ASSIGN #GOT-CURR-CYCLE-DTE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Read_Prior_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #READ-PRIOR-CNT
                    if (condition(pnd_Read_Prior_Cnt.equals(1)))                                                                                                          //Natural: IF #READ-PRIOR-CNT = 1
                    {
                        pnd_Prior_Cycle_Dte.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("MM/DD/YYYY"));                           //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = MM/DD/YYYY ) TO #PRIOR-CYCLE-DTE
                        pnd_Got_Prior_Cycle_Dte.setValue(true);                                                                                                           //Natural: ASSIGN #GOT-PRIOR-CYCLE-DTE := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*        IF #VERIFY-CYCLE-DTE
                //*          IF #GOT-CURR-CYCLE-DTE AND #GOT-PRIOR-CYCLE-DTE
                //*             IF #PRIOR-CYCLE-DTE EQ #CURR-CYCLE-DTE
                //*                WRITE '******************************************'
                //*                    / '*          CYCLE DATE ERROR              *'
                //*                    / '*          ----------------              *'
                //*                    / '* CURRENT FILE CYCLE DATE IS EQUAL TO    *'
                //*                    / '* PRIOR FILE CYCLE.  PROGRAM TERMINATED. *'
                //*                    / '* ****************************************'
                //*                  /// 'PRIOR   CYCLE DATE:' #PRIOR-CYCLE-DTE
                //*                    / 'CURRENT CYCLE DATE:' #CURR-CYCLE-DTE
                //*                TERMINATE 16
                //*             END-IF
                //*   CYCLE DATES MUST BE CONSECUTIVE MONTHS
                //*             DECIDE ON FIRST VALUE #PRIOR-CYCLE-MM
                //*                VALUE 1:11
                //*                   IF #PRIOR-CYCLE-MM + 1 NE #CURR-CYCLE-MM
                //*                     #CYCLE-ERROR := TRUE
                //*                   END-IF
                //*                   IF #PRIOR-CYCLE-YYYY NE #CURR-CYCLE-YYYY
                //*                     #CYCLE-ERROR := TRUE
                //*                   END-IF
                //*                VALUE 12
                //*                   IF #CURR-CYCLE-MM NE 1
                //*                     #CYCLE-ERROR := TRUE
                //*                   END-IF
                //*                   #CYCLE-YYYY-DIFF :=
                //*                         #CURR-CYCLE-YYYY - #PRIOR-CYCLE-YYYY
                //*                   IF #CYCLE-YYYY-DIFF NE 1
                //*                     #CYCLE-ERROR := TRUE
                //*                   END-IF
                //*                NONE
                //*                   IGNORE
                //*             END-DECIDE
                //*             IF #CYCLE-ERROR
                //*                WRITE '******************************************'
                //*                    / '*          CYCLE DATE ERROR              *'
                //*                    / '*          ----------------              *'
                //*                    / '* CURRENT FILE CYCLE DATE AND THE PRIOR  *'
                //*                    / '* FILE CYCLE DATE MUST BE TWO CONSECUTIVE*'
                //*                    / '* MONTHS.  PROGRAM TERMINATED.           *'
                //*                    / '******************************************'
                //*                  /// 'PRIOR   CYCLE DATE:' #PRIOR-CYCLE-DTE
                //*                    / 'CURRENT CYCLE DATE:' #CURR-CYCLE-DTE
                //*                TERMINATE 16
                //*             END-IF
                //*             #VERIFY-CYCLE-DTE := FALSE
                //*          END-IF
                //*        END-IF
                //*    GTN REPORT CODE COUNTERS
                FOR02:                                                                                                                                                    //Natural: FOR #RPT-CDE-NDX = 1 TO 7
                for (pnd_Rpt_Cde_Ndx.setValue(1); condition(pnd_Rpt_Cde_Ndx.lessOrEqual(7)); pnd_Rpt_Cde_Ndx.nadd(1))
                {
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Rpt_Cde_Ndx).notEquals(getZero())))                                         //Natural: IF GTN-RPT-CODE ( #RPT-CDE-NDX ) NE 0
                    {
                        pnd_Rpt_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Rpt_Cde_Ndx));                                                  //Natural: ASSIGN #RPT-CODE := GTN-RPT-CODE ( #RPT-CDE-NDX )
                        pnd_Gtn_Rpt_Cnt.getValue(pnd_Prior_Curr_Month_Flag,pnd_Rpt_Code).nadd(1);                                                                         //Natural: ADD 1 TO #GTN-RPT-CNT ( #PRIOR-CURR-MONTH-FLAG,#RPT-CODE )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_GTN_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_GTN_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   INCREMENT DOLLAR AMT CONTROL TOTALS
                pnd_Control_Totals_Pnd_Tot_Read_Cnt.getValue(pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr).nadd(1);                                                           //Natural: ADD 1 TO #TOT-READ-CNT ( #PRIOR-CURR )
                FOR03:                                                                                                                                                    //Natural: FOR #X = 1 TO INV-ACCT-COUNT
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
                {
                    pnd_Control_Totals_Pnd_Tot_Contract_Amt.getValue(pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_X)); //Natural: ADD INV-ACCT-CNTRCT-AMT ( #X ) TO #TOT-CONTRACT-AMT ( #PRIOR-CURR )
                    pnd_Control_Totals_Pnd_Tot_Dividend_Amt.getValue(pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_X)); //Natural: ADD INV-ACCT-DVDND-AMT ( #X ) TO #TOT-DIVIDEND-AMT ( #PRIOR-CURR )
                    pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt.getValue(pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_X)); //Natural: ADD INV-ACCT-SETTL-AMT ( #X ) TO #TOT-STTLMNT-AMT ( #PRIOR-CURR )
                    pnd_Control_Totals_Pnd_Tot_Net_Pymnt_Amt.getValue(pnd_Prior_Curr_Month_Flag_Pnd_Prior_Curr).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_X)); //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #X ) TO #TOT-NET-PYMNT-AMT ( #PRIOR-CURR )
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_GTN_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_GTN_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   REJECT LOGIC
                //*      1) BAD ADDRESS
                //*    % 2) INVALID PIN NUMBER
                //*    % 3) CANADIAN CURRENCY
                //*      4) CONTRACT IS A NEW ISSUE
                //*      5) CONTRACT IS TERMINATED
                //*      5) BAD GTN RETURN CODE
                //*    IF CNTRCT-UNQ-ID-NBR = 0 OR = 9999999 OR
                //*        CNTRCT-UNQ-ID-NBR NE MASK (NNNNNNN)
                //*  PIN EXPANSION
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr().equals(getZero()) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr().equals(new     //Natural: IF CNTRCT-UNQ-ID-NBR = 0 OR = 999999999999 OR CNTRCT-UNQ-ID-NBR NE MASK ( NNNNNNNNNNNN )
                    DbsDecimal("999999999999")) || ! (DbsUtil.maskMatches(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(),"NNNNNNNNNNNN"))))
                {
                    pnd_Bad_Pin_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #BAD-PIN-CNT
                    if (condition(pnd_Prior_Curr_Month_Flag.equals(1)))                                                                                                   //Natural: IF #PRIOR-CURR-MONTH-FLAG = 1
                    {
                        pnd_Bad_Pin_Cnt_Curr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #BAD-PIN-CNT-CURR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Bad_Pin_Cnt_Prior.nadd(1);                                                                                                                    //Natural: ADD 1 TO #BAD-PIN-CNT-PRIOR
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(3, "PRIOR/CURR",                                                                                                                 //Natural: DISPLAY ( 3 ) 'PRIOR/CURR' #PRIOR-CURR-MONTH-FLAG '/PIN' CNTRCT-UNQ-ID-NBR '/CONTRACT' CNTRCT-PPCN-NBR '/PAYEE' CNTRCT-PAYEE-CDE '/SSN' ANNT-SOC-SEC-NBR '/LAST NAME' PH-LAST-NAME '/FIRST NAME' PH-FIRST-NAME
                    		pnd_Prior_Curr_Month_Flag,"/PIN",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(),"/CONTRACT",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/PAYEE",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde(),"/SSN",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),"/LAST NAME",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name(),"/FIRST NAME",
                    		pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_GTN_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_GTN_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        '/MIDDLE NAME' PH-MIDDLE-NAME
                    //*  GET NEXT RECORD
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde().equals("2")))                                                                           //Natural: IF CNTRCT-CRRNCY-CDE = '2'
                {
                    pnd_Canadian_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CANADIAN-CNT
                    //*  GET NEXT RECORD
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  MM 03/06/01 BEGIN
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(28) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(30)))          //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE = 28 OR = 30
                {
                    pnd_Tpa_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TPA-CNT
                    if (condition(pnd_Prior_Curr_Month_Flag.equals(1)))                                                                                                   //Natural: IF #PRIOR-CURR-MONTH-FLAG = 1
                    {
                        pnd_Tpa_Cnt_Curr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TPA-CNT-CURR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Tpa_Cnt_Prior.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TPA-CNT-PRIOR
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  MM 03/06/01 END
                pnd_Sub.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SUB
                pnd_Gtn_Rec_Pnd_Pin.getValue(pnd_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr());                                                      //Natural: MOVE CNTRCT-UNQ-ID-NBR TO #GTN-REC.#PIN ( #SUB )
                pnd_Gtn_Rec_Pnd_Contract.getValue(pnd_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                   //Natural: MOVE CNTRCT-PPCN-NBR TO #GTN-REC.#CONTRACT ( #SUB )
                pnd_Gtn_Rec_Pnd_Payee.getValue(pnd_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                                     //Natural: MOVE CNTRCT-PAYEE-CDE TO #GTN-REC.#PAYEE ( #SUB )
                pnd_Gtn_Rec_Pnd_Month_Flag.getValue(pnd_Sub).setValue(pnd_Prior_Curr_Month_Flag);                                                                         //Natural: MOVE #PRIOR-CURR-MONTH-FLAG TO #GTN-REC.#MONTH-FLAG ( #SUB )
                pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(pnd_Sub,1,":",12).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue(1,":",12));                                       //Natural: MOVE WF-PYMNT-ADDR-GRP ( 1:12 ) TO #GTN-REC.#GTN-DATA ( #SUB,1:12 )
                //*  JB01
                pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(pnd_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());                                              //Natural: MOVE CANADIAN-TAX-AMT TO #GTN-REC.#CANADIAN-TAX ( #SUB )
                //*  READ GROSS TO NET FILE
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_LOOP"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_LOOP"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   COMPARE KEYS OF CONSECUTIVE GTN RECORDS
            if (condition(pnd_Gtn_Rec_Pnd_Pin.getValue(1).notEquals(pnd_Gtn_Rec_Pnd_Pin.getValue(2)) || pnd_Gtn_Rec_Pnd_Contract.getValue(1).notEquals(pnd_Gtn_Rec_Pnd_Contract.getValue(2))  //Natural: IF #GTN-REC.#PIN ( 1 ) NE #GTN-REC.#PIN ( 2 ) OR #GTN-REC.#CONTRACT ( 1 ) NE #GTN-REC.#CONTRACT ( 2 ) OR #GTN-REC.#PAYEE ( 1 ) NE #GTN-REC.#PAYEE ( 2 )
                || pnd_Gtn_Rec_Pnd_Payee.getValue(1).notEquals(pnd_Gtn_Rec_Pnd_Payee.getValue(2))))
            {
                //*   SINCE KEYS DON't match, what kind of record is in the first
                //*   OF THE CONSECUTIVE GTN RECORDS
                //*  CURRENT MONTH
                short decideConditionsMet986 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #GTN-REC.#MONTH-FLAG ( 1 );//Natural: VALUE 1
                if (condition((pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).equals(1))))
                {
                    decideConditionsMet986++;
                    //*  PRIOR MONTH
                    pnd_New_Issue_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NEW-ISSUE-CNT
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).equals(2))))
                {
                    decideConditionsMet986++;
                    pnd_Terminated_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TERMINATED-CNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Unknown_Month_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #UNKNOWN-MONTH-CNT
                    //*  JB01
                    //*  GET NEXT RECORD
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Gtn_Rec_Pnd_Pin.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Pin.getValue(2));                                                                                //Natural: ASSIGN #GTN-REC.#PIN ( 1 ) := #GTN-REC.#PIN ( 2 )
                pnd_Gtn_Rec_Pnd_Contract.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Contract.getValue(2));                                                                      //Natural: ASSIGN #GTN-REC.#CONTRACT ( 1 ) := #GTN-REC.#CONTRACT ( 2 )
                pnd_Gtn_Rec_Pnd_Payee.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Payee.getValue(2));                                                                            //Natural: ASSIGN #GTN-REC.#PAYEE ( 1 ) := #GTN-REC.#PAYEE ( 2 )
                pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2));                                                                  //Natural: ASSIGN #GTN-REC.#MONTH-FLAG ( 1 ) := #GTN-REC.#MONTH-FLAG ( 2 )
                pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(1,1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12));                                                    //Natural: ASSIGN #GTN-REC.#GTN-DATA ( 1,1:12 ) := #GTN-REC.#GTN-DATA ( 2,1:12 )
                pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2));                                                              //Natural: ASSIGN #GTN-REC.#CANADIAN-TAX ( 1 ) := #GTN-REC.#CANADIAN-TAX ( 2 )
                pnd_Sub.setValue(1);                                                                                                                                      //Natural: ASSIGN #SUB := 1
                //*  JB01
                pnd_Gtn_Rec_Pnd_Pin.getValue(2).reset();                                                                                                                  //Natural: RESET #GTN-REC.#PIN ( 2 ) #GTN-REC.#CONTRACT ( 2 ) #GTN-REC.#PAYEE ( 2 ) #GTN-REC.#MONTH-FLAG ( 2 ) #GTN-REC.#GTN-DATA ( 2,1:12 ) #GTN-REC.#CANADIAN-TAX ( 2 )
                pnd_Gtn_Rec_Pnd_Contract.getValue(2).reset();
                pnd_Gtn_Rec_Pnd_Payee.getValue(2).reset();
                pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2).reset();
                pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12).reset();
                pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2).reset();
                //*    KEYS MATCH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).equals(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2))))                                                     //Natural: IF #GTN-REC.#MONTH-FLAG ( 1 ) = #GTN-REC.#MONTH-FLAG ( 2 )
                {
                    //*    CONSECUTIVE RECORDS ARE DUPLICATES
                    //*    REMOVE ONE AND GET THE NEXT RECORD
                    //*  JB01
                    //*  GET NEXT RECORD
                    pnd_Duplicate_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUPLICATE-CNT
                    pnd_Gtn_Rec_Pnd_Pin.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Pin.getValue(2));                                                                            //Natural: ASSIGN #GTN-REC.#PIN ( 1 ) := #GTN-REC.#PIN ( 2 )
                    pnd_Gtn_Rec_Pnd_Contract.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Contract.getValue(2));                                                                  //Natural: ASSIGN #GTN-REC.#CONTRACT ( 1 ) := #GTN-REC.#CONTRACT ( 2 )
                    pnd_Gtn_Rec_Pnd_Payee.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Payee.getValue(2));                                                                        //Natural: ASSIGN #GTN-REC.#PAYEE ( 1 ) := #GTN-REC.#PAYEE ( 2 )
                    pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2));                                                              //Natural: ASSIGN #GTN-REC.#MONTH-FLAG ( 1 ) := #GTN-REC.#MONTH-FLAG ( 2 )
                    pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(1,1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12));                                                //Natural: ASSIGN #GTN-REC.#GTN-DATA ( 1,1:12 ) := #GTN-REC.#GTN-DATA ( 2,1:12 )
                    pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(1).setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2));                                                          //Natural: ASSIGN #GTN-REC.#CANADIAN-TAX ( 1 ) := #GTN-REC.#CANADIAN-TAX ( 2 )
                    pnd_Sub.setValue(1);                                                                                                                                  //Natural: ASSIGN #SUB := 1
                    //*  JB01
                    pnd_Gtn_Rec_Pnd_Pin.getValue(2).reset();                                                                                                              //Natural: RESET #GTN-REC.#PIN ( 2 ) #GTN-REC.#CONTRACT ( 2 ) #GTN-REC.#PAYEE ( 2 ) #GTN-REC.#MONTH-FLAG ( 2 ) #GTN-REC.#GTN-DATA ( 2,1:12 ) #GTN-REC.#CANADIAN-TAX ( 2 )
                    pnd_Gtn_Rec_Pnd_Contract.getValue(2).reset();
                    pnd_Gtn_Rec_Pnd_Payee.getValue(2).reset();
                    pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2).reset();
                    pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12).reset();
                    pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2).reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Prev_Pin.notEquals(pnd_Gtn_Rec_Pnd_Pin.getValue(2))))                                                                               //Natural: IF #PREV-PIN NE #GTN-REC.#PIN ( 2 )
                    {
                        //*  PIN NET CHANGE FLAG
                        pnd_Net_Chg_Flag.reset();                                                                                                                         //Natural: RESET #NET-CHG-FLAG
                        pnd_Prev_Pin.setValue(pnd_Gtn_Rec_Pnd_Pin.getValue(2));                                                                                           //Natural: ASSIGN #PREV-PIN := #GTN-REC.#PIN ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Match_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MATCH-CNT
                                                                                                                                                                          //Natural: PERFORM RESET-NET-CHANGE-RECORD
                    sub_Reset_Net_Change_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM BUILD-NET-CHANGE-RECORD
                    sub_Build_Net_Change_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MAIN_LOOP"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(2, false, pdaIaaa323.getIaaa320());                                                                                              //Natural: WRITE WORK FILE 2 IAAA320
                    //*  GET NEXT TWO RECORDS
                    pnd_Gtn_Rec.getValue("*").reset();                                                                                                                    //Natural: RESET #GTN-REC ( * ) #WRITE-FUND-CTR
                    pnd_Write_Fund_Ctr.reset();
                    pnd_Sub.setValue(0);                                                                                                                                  //Natural: ASSIGN #SUB := 0
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Eof.getBoolean()))                                                                                                                          //Natural: IF #EOF
            {
                if (true) break MAIN_LOOP;                                                                                                                                //Natural: ESCAPE BOTTOM ( MAIN-LOOP. )
            }                                                                                                                                                             //Natural: END-IF
            //*  MAIN-LOOP.
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  --------------------------------------------
        //*   WRITE CONTROL REPORT
        FOR04:                                                                                                                                                            //Natural: FOR #RPT-CODE = 1 TO #RPT-CODE-MAX
        for (pnd_Rpt_Code.setValue(1); condition(pnd_Rpt_Code.lessOrEqual(pnd_Rpt_Code_Max)); pnd_Rpt_Code.nadd(1))
        {
            getReports().display(2, new ReportEmptyLineSuppression(true),"GTN/REPORT CODE",                                                                               //Natural: DISPLAY ( 2 ) ( ES = ON ) 'GTN/REPORT CODE' #RPT-CODE '/DESCRIPTION' #GTN-RPT-DESC ( #RPT-CODE ) '/PRIOR COUNT' #GTN-RPT-CNT ( 2,#RPT-CODE ) ( EM = Z,ZZZ,ZZ9 ) '/CURRENT COUNT' #GTN-RPT-CNT ( 1,#RPT-CODE ) ( EM = Z,ZZZ,ZZ9 )
            		pnd_Rpt_Code,"/DESCRIPTION",
            		ldaFcpl810.getPnd_Gtn_Rpt_Msg_Pnd_Gtn_Rpt_Desc().getValue(pnd_Rpt_Code),"/PRIOR COUNT",
            		pnd_Gtn_Rpt_Cnt.getValue(2,pnd_Rpt_Code), new ReportEditMask ("Z,ZZZ,ZZ9"),"/CURRENT COUNT",
            		pnd_Gtn_Rpt_Cnt.getValue(1,pnd_Rpt_Code), new ReportEditMask ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 41T 'IA NET CHANGE FILE SELECTION AND CONTROL PROCESS' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *DATX 58T 'CONTROL REPORT' 120T *TIMX // 54T 'CURRENT CYCLE:' #CURR-CYCLE-DTE / 54T 'PRIOR CYCLE:  ' #PRIOR-CYCLE-DTE ///
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 41T 'IA NET CHANGE FILE SELECTION AND CONTROL PROCESS' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / *DATX 49T 'GROSS TO NET REPORT CODE TABLE' 120T *TIMX // 54T 'CURRENT CYCLE:' #CURR-CYCLE-DTE / 54T 'PRIOR CYCLE:  ' #PRIOR-CYCLE-DTE ///
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *PROGRAM 41T 'IA NET CHANGE FILE SELECTION AND CONTROL PROCESS' 120T 'PAGE:' *PAGE-NUMBER ( 3 ) ( EM = ZZ9 ) / *DATX 61T 'BAD PINS' 120T *TIMX // 54T 'CURRENT CYCLE:' #CURR-CYCLE-DTE / 54T 'PRIOR CYCLE:  ' #PRIOR-CYCLE-DTE ///
        getReports().display(1, " ",                                                                                                                                      //Natural: DISPLAY ( 1 ) ' ' #TOT-DESC ( * ) 'Read Count' #TOT-READ-CNT ( * ) ( EM = Z,ZZZ,ZZ9 ) 'Contract Amt' #TOT-CONTRACT-AMT ( * ) ( EM = -Z,ZZZ,ZZZ,ZZ9.99 ) 'Dividend Amt' #TOT-DIVIDEND-AMT ( * ) ( EM = -Z,ZZZ,ZZZ,ZZ9.99 ) 'Gross Amt' #TOT-STTLMNT-AMT ( * ) ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_Control_Totals_Pnd_Tot_Desc.getValue("*"),"Read Count",
        		pnd_Control_Totals_Pnd_Tot_Read_Cnt.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZ9"),"Contract Amt",
        		pnd_Control_Totals_Pnd_Tot_Contract_Amt.getValue("*"), new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"Dividend Amt",
        		pnd_Control_Totals_Pnd_Tot_Dividend_Amt.getValue("*"), new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"Gross Amt",
        		pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt.getValue("*"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  'Net Amt'       #TOT-NET-PYMNT-AMT (*)(EM=-ZZZ,ZZZ,ZZZ,ZZ9.99)
        //*  MM 03/06/01
        //*  MM 03/06/01
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Prior Month Totals",NEWLINE,"  Prior Records Read:      ",pnd_Read_Prior_Cnt,NEWLINE,"  Prior Bad PINS:          ", //Natural: WRITE ( 1 ) /// / 'Prior Month Totals' / '  Prior Records Read:      ' #READ-PRIOR-CNT / '  Prior Bad PINS:          ' #BAD-PIN-CNT-PRIOR / '  Prior TPAs rejected:     ' #TPA-CNT-PRIOR / '  Terminated Contracts:    ' #TERMINATED-CNT // / 'Current Month Totals' / '  Current Records Read:    ' #READ-CURR-CNT / '  Current Bad PINS:        ' #BAD-PIN-CNT-CURR / '  Current TPAs rejected:   ' #TPA-CNT-CURR / '  New Issues:              ' #NEW-ISSUE-CNT // / 'Total Records Matched:     ' #MATCH-CNT / '  total Mailing Labels rec:' #MAILING-LABEL-CNT / '  Total Net Change Record: ' #NET-CHG-CNT
            pnd_Bad_Pin_Cnt_Prior,NEWLINE,"  Prior TPAs rejected:     ",pnd_Tpa_Cnt_Prior,NEWLINE,"  Terminated Contracts:    ",pnd_Terminated_Cnt,NEWLINE,
            NEWLINE,NEWLINE,"Current Month Totals",NEWLINE,"  Current Records Read:    ",pnd_Read_Curr_Cnt,NEWLINE,"  Current Bad PINS:        ",pnd_Bad_Pin_Cnt_Curr,
            NEWLINE,"  Current TPAs rejected:   ",pnd_Tpa_Cnt_Curr,NEWLINE,"  New Issues:              ",pnd_New_Issue_Cnt,NEWLINE,NEWLINE,NEWLINE,"Total Records Matched:     ",
            pnd_Match_Cnt,NEWLINE,"  total Mailing Labels rec:",pnd_Mailing_Label_Cnt,NEWLINE,"  Total Net Change Record: ",pnd_Net_Chg_Cnt);
        if (Global.isEscape()) return;
        //*  / 'Canadian Currency PINS:  '  #CANADIAN-CNT
        //*  / 'Duplicate Contracts:     '  #DUPLICATE-CNT
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-NET-CHANGE-RECORD
        //*    CURRENT MONTH'S FIELDS TO NET CHANGE FILE
        //*  IAAA320.COMBINED-PYMNT-IND := PYMNT-CMBNE-IND
        //*  ADDED FOLLOWING 5/96
        //*  END OF ADD      5/96
        //*  -------------------           JH 7/13/2000 PA SELECT/SPIA
        //*    CURRENT TAXES
        //*    CURRENT INVESTMENT FUNDS
        //*    FUTURE PAYMENT DATE
        //*     PRIOR STATE RESIDENCY CODE -  SET CHANGE INDICATOR
        //* ***********************************************************************
        //*     SET DEDUCTION CODE CHANGE INDICATOR
        //*    IAAA320.DED-AMT-CHG-FLAG (DEDUCTION-CNT) := 1
        //*     PRIOR TAXES  - SET TAX CHANGE INDICATORS
        //*     PRIOR INVESTMENT FUND -  SET CHANGE INDICATORS
        //*     TRY TO MATCH PRIOR FUNDS AGAINST THE FUNDS IN THE
        //*     NET CHANGE FUND TABLE
        //*  ADDED FULL TO FOLLOWING TO CORRECT ERROR IN EXAMINE 10/96
        //*        FOR GRADED & TIAA STANDARD CONTRACTS          10/96
        //*    CHECK FOR UNMATCHED CURRENT FUNDS
        //*  ADDED FOLLOWING TEST FOR TRANSFERS SET TRANSFER IND     10/96
        //*  END OF ADD 10/96
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALUATION-NOT-MATCH
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AFTER-VALUAT-NOT-EQ
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-PRIOR
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-CURR
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-NET-CHANGE-RECORD
        //*  -------------------           JH 7/13/2000 PA SELECT/SPIA
        //*  -------------------           JH 7/13/2000 PA SELECT/SPIA END
    }
    private void sub_Build_Net_Change_Record() throws Exception                                                                                                           //Natural: BUILD-NET-CHANGE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*   PROCESS CURRENT MONTH DATA
        //*  CURRENT MONTH
        short decideConditionsMet1146 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #GTN-REC.#MONTH-FLAG ( 1 ) = 1
        if (condition(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).equals(1)))
        {
            decideConditionsMet1146++;
            pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue(1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(1,1,":",12));                                                 //Natural: MOVE #GTN-REC.#GTN-DATA ( 1,1:12 ) TO WF-PYMNT-ADDR-GRP ( 1:12 )
            //*  JB01
            //*  CURRENT MONTH
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(1));                                                        //Natural: MOVE #CANADIAN-TAX ( 1 ) TO CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: WHEN #GTN-REC.#MONTH-FLAG ( 2 ) = 1
        else if (condition(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2).equals(1)))
        {
            decideConditionsMet1146++;
            pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue(1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12));                                                 //Natural: MOVE #GTN-REC.#GTN-DATA ( 2,1:12 ) TO WF-PYMNT-ADDR-GRP ( 1:12 )
            //*  JB01
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2));                                                        //Natural: MOVE #CANADIAN-TAX ( 2 ) TO CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaIaaa323.getIaaa320_Pin_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr());                                                                    //Natural: ASSIGN IAAA320.PIN-NBR := CNTRCT-UNQ-ID-NBR
        pdaIaaa323.getIaaa320_Contract_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                                 //Natural: ASSIGN IAAA320.CONTRACT-NBR := CNTRCT-PPCN-NBR
        pdaIaaa323.getIaaa320_Payee_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                                                   //Natural: ASSIGN IAAA320.PAYEE-CDE := CNTRCT-PAYEE-CDE
        pdaIaaa323.getIaaa320_Ssn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr());                                                                     //Natural: ASSIGN IAAA320.SSN-NBR := ANNT-SOC-SEC-NBR
        pdaIaaa323.getIaaa320_Company_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                                                     //Natural: ASSIGN IAAA320.COMPANY-CDE := INV-ACCT-COMPANY ( 1 )
        pdaIaaa323.getIaaa320_Hold_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Ind());                                                                     //Natural: ASSIGN IAAA320.HOLD-IND := CNTRCT-HOLD-IND
        pdaIaaa323.getIaaa320_Hold_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                                                     //Natural: ASSIGN IAAA320.HOLD-CDE := CNTRCT-HOLD-CDE
        pdaIaaa323.getIaaa320_Hold_Grp_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Grp());                                                                 //Natural: ASSIGN IAAA320.HOLD-GRP-CDE := CNTRCT-HOLD-GRP
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().getBoolean()))                                                                                  //Natural: IF OFF-MODE-CONTRACT
        {
            pdaIaaa323.getIaaa320_Off_Mode_Ind().setValue(1);                                                                                                             //Natural: ASSIGN IAAA320.OFF-MODE-IND := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIaaa323.getIaaa320_Off_Mode_Ind().setValue(0);                                                                                                             //Natural: ASSIGN IAAA320.OFF-MODE-IND := 0
            //*  ASSUMES TRANSFERS DID NOT OCCUR
            //*  N= NRA
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa323.getIaaa320_Transfer_Ind().setValue("0");                                                                                                               //Natural: ASSIGN IAAA320.TRANSFER-IND := '0'
        pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr());                                                 //Natural: ASSIGN IAAA320.COMBINED-PYMNT-IND := PYMNT-PAYEE-TX-ELCT-TRGGR
        //*  2 = DIVDN PAYED TO COLLEGE
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind().equals(2)))                                                                                //Natural: IF CNTRCT-DVDND-PAYEE-IND = 2
        {
            //*  B= DIV TO COLL & NRA CNTRCT
            if (condition(pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().equals("N")))                                                                                        //Natural: IF IAAA320.COMBINED-PYMNT-IND = 'N'
            {
                pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().setValue("B");                                                                                                 //Natural: ASSIGN IAAA320.COMBINED-PYMNT-IND := 'B'
                //*  D= DIV TO COLL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().setValue("D");                                                                                                 //Natural: ASSIGN IAAA320.COMBINED-PYMNT-IND := 'D'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa323.getIaaa320_Gtn_Return_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code());                                                                  //Natural: ASSIGN IAAA320.GTN-RETURN-CDE := GTN-RET-CODE
        pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*"));                                      //Natural: ASSIGN IAAA320.GTN-REPORT-CDE ( * ) := GTN-RPT-CODE ( * )
        pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde());                                                        //Natural: ASSIGN IAAA320.CURR-STATE-RSDNCY-CDE := ANNT-RSDNCY-CDE
        pdaIaaa323.getIaaa320_Ph_Last_Nme().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name());                                                                     //Natural: ASSIGN IAAA320.PH-LAST-NME := PH-LAST-NAME
        pdaIaaa323.getIaaa320_Ph_First_Nme().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name());                                                                   //Natural: ASSIGN IAAA320.PH-FIRST-NME := PH-FIRST-NAME
        pdaIaaa323.getIaaa320_Ph_Middle_Nme().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name());                                                                 //Natural: ASSIGN IAAA320.PH-MIDDLE-NME := PH-MIDDLE-NAME
        //*     GET CURRENT CORRESPONDENCE MAILING ADDRESS
        //*  THIS MEANS THAT CHECK MAILING ADDRESS IS IN OCCURRENCE 1
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(2).notEquals(" ")))                                                                     //Natural: IF PYMNT-ADDR-LINES ( 2 ) NE ' '
        {
            pnd_X.setValue(2);                                                                                                                                            //Natural: ASSIGN #X := 2
            //*  AND THE CORRESPONDENCE MAILING ADDRSS IS IN OCCURRENCE 2
            //*  CORRESPONDENCE MAILING ADDRESS ARE THE SAME AND IT'S IN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_X.setValue(1);                                                                                                                                            //Natural: ASSIGN #X := 1
            //*  OCCURRENCE 1
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa323.getIaaa320_Ph_Mailing_Nme().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(pnd_X));                                                     //Natural: ASSIGN IAAA320.PH-MAILING-NME := PYMNT-NME ( #X )
        pnd_Mailing_Address.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(pnd_X));                                                                 //Natural: ASSIGN #MAILING-ADDRESS := PYMNT-ADDR-LINES ( #X )
        pdaIaaa323.getIaaa320_Ph_Address_Line_1().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_1);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-1 := #MAILING-ADDR-1
        pdaIaaa323.getIaaa320_Ph_Address_Line_2().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_2);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-2 := #MAILING-ADDR-2
        pdaIaaa323.getIaaa320_Ph_Address_Line_3().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_3);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-3 := #MAILING-ADDR-3
        pdaIaaa323.getIaaa320_Ph_Address_Line_4().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_4);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-4 := #MAILING-ADDR-4
        pdaIaaa323.getIaaa320_Ph_Address_Line_5().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_5);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-5 := #MAILING-ADDR-5
        pdaIaaa323.getIaaa320_Ph_Address_Line_6().setValue(pnd_Mailing_Address_Pnd_Mailing_Addr_6);                                                                       //Natural: ASSIGN IAAA320.PH-ADDRESS-LINE-6 := #MAILING-ADDR-6
        pdaIaaa323.getIaaa320_Zip_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(pnd_X));                                                   //Natural: ASSIGN IAAA320.ZIP-CDE := PYMNT-ADDR-ZIP-CDE ( #X )
        pdaIaaa323.getIaaa320_Postal_Data().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Postl_Data().getValue(pnd_X));                                                 //Natural: ASSIGN IAAA320.POSTAL-DATA := PYMNT-POSTL-DATA ( #X )
        //*  ADDED FOLLOWING  5/96
        //*  PART OF POSTAL DATA
        if (condition(pdaIaaa323.getIaaa320_Zip_Code_5().equals("     ")))                                                                                                //Natural: IF IAAA320.ZIP-CODE-5 = '     '
        {
            pdaIaaa323.getIaaa320_Zip_Code_5().setValue("00000");                                                                                                         //Natural: ASSIGN IAAA320.ZIP-CODE-5 := '00000'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING  5/96
        //*    CHECK/CYCLE DATES
        pnd_Temp_Date_Pnd_Temp_Date_A.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A
        pdaIaaa323.getIaaa320_Curr_Check_Dte().setValue(pnd_Temp_Date);                                                                                                   //Natural: ASSIGN IAAA320.CURR-CHECK-DTE := #TEMP-DATE
        pnd_Temp_Date_Pnd_Temp_Date_A.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A
        pdaIaaa323.getIaaa320_Curr_Cycle_Dte().setValue(pnd_Temp_Date);                                                                                                   //Natural: ASSIGN IAAA320.CURR-CYCLE-DTE := #TEMP-DATE
        pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());                                                  //Natural: ASSIGN IAAA320.CNTRCT-ANNTY-INS-TYPE := WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE
        pdaIaaa323.getIaaa320_Cntrct_Annty_Type_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde());                                                  //Natural: ASSIGN IAAA320.CNTRCT-ANNTY-TYPE-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE
        pdaIaaa323.getIaaa320_Cntrct_Insurance_Option().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option());                                              //Natural: ASSIGN IAAA320.CNTRCT-INSURANCE-OPTION := WF-PYMNT-ADDR-GRP.CNTRCT-INSURANCE-OPTION
        pdaIaaa323.getIaaa320_Cntrct_Life_Contingency().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency());                                              //Natural: ASSIGN IAAA320.CNTRCT-LIFE-CONTINGENCY := WF-PYMNT-ADDR-GRP.CNTRCT-LIFE-CONTINGENCY
        //*  -------------------           JH 7/13/2000 PA SELECT/SPIA END
        //*  MM 03/07/01 BEGIN
        pdaIaaa323.getIaaa320_Cntrct_Option_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde());                                                          //Natural: ASSIGN IAAA320.CNTRCT-OPTION-CDE = WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE
        //*  MM 03/07/01 END
        //*    CURRENT DEDUCTIONS
        pdaIaaa323.getIaaa320_Deduction_Cnt().reset();                                                                                                                    //Natural: RESET IAAA320.DEDUCTION-CNT
        //*   JB01
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().notEquals(getZero())))                                                                           //Natural: IF CANADIAN-TAX-AMT NE 0
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10).notEquals(getZero())))                                                             //Natural: IF PYMNT-DED-CDE ( 10 ) NE 0
            {
                getReports().write(0, "SYSTEM ERROR - MORE THAN 10 DEDUCTIONS: CONTACT SYSTEMS");                                                                         //Natural: WRITE "SYSTEM ERROR - MORE THAN 10 DEDUCTIONS: CONTACT SYSTEMS"
                if (Global.isEscape()) return;
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            FOR05:                                                                                                                                                        //Natural: FOR #X 9 TO 1 STEP -1
            for (pnd_X.setValue(9); condition(pnd_X.greaterOrEqual(1)); pnd_X.nsubtract(1))
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X.getDec().add(1)).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X)); //Natural: MOVE PYMNT-DED-CDE ( #X ) TO PYMNT-DED-CDE ( #X + 1 )
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X.getDec().add(1)).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X)); //Natural: MOVE PYMNT-DED-AMT ( #X ) TO PYMNT-DED-AMT ( #X + 1 )
                //*  JB01
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1).setValue(12);                                                                                     //Natural: ASSIGN PYMNT-DED-CDE ( 1 ) := 12
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());                                     //Natural: ASSIGN PYMNT-DED-AMT ( 1 ) := CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #X = 1 TO 10
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(10)); pnd_X.nadd(1))
        {
            //* **
            //*    SPECIAL LOGIC FOR NYST DEDUCTION
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X).equals(getZero()) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X).notEquals(getZero()))) //Natural: IF PYMNT-DED-CDE ( #X ) = 0 AND PYMNT-DED-AMT ( #X ) NE 0
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X).setValue(6);                                                                              //Natural: ASSIGN PYMNT-DED-CDE ( #X ) := 6
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X).notEquals(getZero())))                                                          //Natural: IF PYMNT-DED-CDE ( #X ) NE 0
            {
                pnd_Temp_Ded.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X));                                                                   //Natural: ASSIGN #TEMP-DED := PYMNT-DED-CDE ( #X )
                DbsUtil.examine(new ExamineSource(pdaIaaa323.getIaaa320_Ded_Cde_A().getValue("*")), new ExamineSearch(pnd_Temp_Ded_Pnd_Temp_Ded_A), new                   //Natural: EXAMINE IAAA320.DED-CDE-A ( * ) FOR #TEMP-DED-A GIVING INDEX #DED-SUB
                    ExamineGivingIndex(pnd_Ded_Sub));
                if (condition(pnd_Ded_Sub.equals(getZero())))                                                                                                             //Natural: IF #DED-SUB = 0
                {
                    pdaIaaa323.getIaaa320_Deduction_Cnt().nadd(1);                                                                                                        //Natural: ADD 1 TO IAAA320.DEDUCTION-CNT
                    pdaIaaa323.getIaaa320_Ded_Cde().getValue(pdaIaaa323.getIaaa320_Deduction_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X)); //Natural: ASSIGN IAAA320.DED-CDE ( DEDUCTION-CNT ) := PYMNT-DED-CDE ( #X )
                    pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue(pdaIaaa323.getIaaa320_Deduction_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.DED-CURR-AMT ( DEDUCTION-CNT ) := PYMNT-DED-AMT ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue(pnd_Ded_Sub).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X));                     //Natural: ADD PYMNT-DED-AMT ( #X ) TO IAAA320.DED-CURR-AMT ( #DED-SUB )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #X = 1 TO INV-ACCT-COUNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
        {
            pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_X));                                       //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #X ) TO IAAA320.TAX-CURR-FED-AMT
            pdaIaaa323.getIaaa320_Tax_Curr_St_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_X));                                       //Natural: ADD INV-ACCT-STATE-TAX-AMT ( #X ) TO IAAA320.TAX-CURR-ST-AMT
            pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_X));                                      //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #X ) TO IAAA320.TAX-CURR-LOC-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FIX GROUP CONTRACTS ON CURRENT FILE
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0250000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0899999"))) //Natural: IF CNTRCT-PPCN-NBR = 'W0250000' THRU 'W0899999'
        {
            pnd_Group_Curr_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #GROUP-CURR-CNT
            FOR08:                                                                                                                                                        //Natural: FOR #X = 1 TO INV-ACCT-COUNT
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X).equals(1)))                                                                //Natural: IF INV-ACCT-CDE-N ( #X ) = 1
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X).setValue(21);                                                                        //Natural: ASSIGN INV-ACCT-CDE-N ( #X ) := 21
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X).setValue("G");                                                                   //Natural: ASSIGN INV-ACCT-CDE-ALPHA ( #X ) := 'G'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaIaaa323.getIaaa320_Fund_Cnt().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                                      //Natural: ASSIGN IAAA320.FUND-CNT := INV-ACCT-COUNT
        FOR09:                                                                                                                                                            //Natural: FOR #X = 1 TO IAAA320.FUND-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaIaaa323.getIaaa320_Fund_Cnt())); pnd_X.nadd(1))
        {
            //*  NEXT 6 LINES - RCC
            pnd_Write_Fund_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WRITE-FUND-CTR
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X).equals(" ")))                                                          //Natural: IF INV-ACCT-VALUAT-PERIOD ( #X ) = ' '
            {
                pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_X).setValue("A");                                                                                 //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( #X ) := 'A'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X));            //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( #X ) := INV-ACCT-VALUAT-PERIOD ( #X )
            }                                                                                                                                                             //Natural: END-IF
            pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X));                        //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-N ( #X ) := INV-ACCT-CDE-N ( #X )
            pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X));                    //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-A ( #X ) := INV-ACCT-CDE-ALPHA ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_X));                     //Natural: ASSIGN IAAA320.FUND-CURR-UNIT-QTY ( #X ) := INV-ACCT-UNIT-QTY ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_X));                 //Natural: ASSIGN IAAA320.FUND-CURR-UNIT-VALUE ( #X ) := INV-ACCT-UNIT-VALUE ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_X));              //Natural: ASSIGN IAAA320.FUND-CURR-SETTLEMENT-AMT ( #X ) := INV-ACCT-SETTL-AMT ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_X));               //Natural: ASSIGN IAAA320.FUND-CURR-CONTRACT-AMT ( #X ) := INV-ACCT-CNTRCT-AMT ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_X));                //Natural: ASSIGN IAAA320.FUND-CURR-DIVIDEND-AMT ( #X ) := INV-ACCT-DVDND-AMT ( #X )
            pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_X).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_X));           //Natural: ASSIGN IAAA320.FUND-CURR-NET-PYMNT-AMT ( #X ) := INV-ACCT-NET-PYMNT-AMT ( #X )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaIaaa323.getIaaa320_Mode_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde());                                                                     //Natural: ASSIGN IAAA320.MODE-CDE := CNTRCT-MODE-CDE
        //*  MONTHLY PAYMENT
        if (condition(pdaIaaa323.getIaaa320_Mode_Cde().equals(100)))                                                                                                      //Natural: IF IAAA320.MODE-CDE = 100
        {
            pdaIaaa323.getIaaa320_Future_Pymnt_Dte().setValue(pdaIaaa323.getIaaa320_Curr_Check_Dte());                                                                    //Natural: ASSIGN IAAA320.FUTURE-PYMNT-DTE := IAAA320.CURR-CHECK-DTE
            //*  FIRST DAY OF THE MONTH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Curr_Chk_Dte.setValue(pdaIaaa323.getIaaa320_Curr_Check_Dte());                                                                                            //Natural: ASSIGN #CURR-CHK-DTE := IAAA320.CURR-CHECK-DTE
            pnd_Future_Dte.setValue(pnd_Curr_Chk_Dte);                                                                                                                    //Natural: ASSIGN #FUTURE-DTE := #CURR-CHK-DTE
            pnd_Future_Dte_Pnd_Future_Dd.setValue(1);                                                                                                                     //Natural: ASSIGN #FUTURE-DD := 01
            //*  QUARTERLY
            short decideConditionsMet1319 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE IAAA320.MODE-CDE;//Natural: VALUE 601
            if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(601))))
            {
                decideConditionsMet1319++;
                //*  JANUARY
                short decideConditionsMet1322 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 1
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(1)))
                {
                    decideConditionsMet1322++;
                    //*  APRIL
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(1);                                                                                                             //Natural: MOVE 1 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 4
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(4)))
                {
                    decideConditionsMet1322++;
                    //*  JULY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(4);                                                                                                             //Natural: MOVE 4 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 7
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(7)))
                {
                    decideConditionsMet1322++;
                    //*  OCTOBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(7);                                                                                                             //Natural: MOVE 7 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 10
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(10)))
                {
                    decideConditionsMet1322++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(10);                                                                                                            //Natural: MOVE 10 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(1);                                                                                                             //Natural: MOVE 1 TO #FUTURE-MM
                    //*  QUARTERLY
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 602
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(602))))
            {
                decideConditionsMet1319++;
                //*  FEBRUARY
                short decideConditionsMet1341 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 2
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(2)))
                {
                    decideConditionsMet1341++;
                    //*  MAY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(2);                                                                                                             //Natural: MOVE 2 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 5
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(5)))
                {
                    decideConditionsMet1341++;
                    //*  AUGUST
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(5);                                                                                                             //Natural: MOVE 5 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 8
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(8)))
                {
                    decideConditionsMet1341++;
                    //*  NOVEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(8);                                                                                                             //Natural: MOVE 8 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 11
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(11)))
                {
                    decideConditionsMet1341++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(11);                                                                                                            //Natural: MOVE 11 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(2);                                                                                                             //Natural: MOVE 2 TO #FUTURE-MM
                    //*  QUARTERLY
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 603
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(603))))
            {
                decideConditionsMet1319++;
                //*  MARCH
                short decideConditionsMet1360 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 3
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(3)))
                {
                    decideConditionsMet1360++;
                    //*  JUNE
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(3);                                                                                                             //Natural: MOVE 3 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 6
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(6)))
                {
                    decideConditionsMet1360++;
                    //*  SEPTEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(6);                                                                                                             //Natural: MOVE 6 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 9
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(9)))
                {
                    decideConditionsMet1360++;
                    //*  DECEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(9);                                                                                                             //Natural: MOVE 9 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 12
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(12)))
                {
                    decideConditionsMet1360++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(12);                                                                                                            //Natural: MOVE 12 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(3);                                                                                                             //Natural: MOVE 3 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 701
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(701))))
            {
                decideConditionsMet1319++;
                //*  JANUARY
                short decideConditionsMet1379 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 1
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(1)))
                {
                    decideConditionsMet1379++;
                    //*  JULY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(1);                                                                                                             //Natural: MOVE 1 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 7
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(7)))
                {
                    decideConditionsMet1379++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(7);                                                                                                             //Natural: MOVE 7 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(1);                                                                                                             //Natural: MOVE 1 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 702
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(702))))
            {
                decideConditionsMet1319++;
                //*  FEBRUARY
                short decideConditionsMet1392 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 2
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(2)))
                {
                    decideConditionsMet1392++;
                    //*  AUGUST
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(2);                                                                                                             //Natural: MOVE 2 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 8
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(8)))
                {
                    decideConditionsMet1392++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(8);                                                                                                             //Natural: MOVE 8 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(2);                                                                                                             //Natural: MOVE 2 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 703
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(703))))
            {
                decideConditionsMet1319++;
                //*  MARCH
                short decideConditionsMet1405 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 3
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(3)))
                {
                    decideConditionsMet1405++;
                    //*  SEPTEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(3);                                                                                                             //Natural: MOVE 3 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 9
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(9)))
                {
                    decideConditionsMet1405++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(9);                                                                                                             //Natural: MOVE 9 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(3);                                                                                                             //Natural: MOVE 3 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 704
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(704))))
            {
                decideConditionsMet1319++;
                //*  APRIL
                short decideConditionsMet1418 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 4
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(4)))
                {
                    decideConditionsMet1418++;
                    //*  OCTOBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(4);                                                                                                             //Natural: MOVE 4 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 10
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(10)))
                {
                    decideConditionsMet1418++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(10);                                                                                                            //Natural: MOVE 10 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(4);                                                                                                             //Natural: MOVE 4 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 705
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(705))))
            {
                decideConditionsMet1319++;
                //*  MAY
                short decideConditionsMet1431 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 5
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(5)))
                {
                    decideConditionsMet1431++;
                    //*  NOVEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(5);                                                                                                             //Natural: MOVE 5 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 11
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(11)))
                {
                    decideConditionsMet1431++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(11);                                                                                                            //Natural: MOVE 11 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(5);                                                                                                             //Natural: MOVE 5 TO #FUTURE-MM
                    //*  SEMI-ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 706
            else if (condition((pdaIaaa323.getIaaa320_Mode_Cde().equals(706))))
            {
                decideConditionsMet1319++;
                //*  JUNE
                short decideConditionsMet1444 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURR-CHK-DTE-MM LE 6
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(6)))
                {
                    decideConditionsMet1444++;
                    //*  DECEMBER
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(6);                                                                                                             //Natural: MOVE 6 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN #CURR-CHK-DTE-MM LE 12
                else if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(12)))
                {
                    decideConditionsMet1444++;
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(12);                                                                                                            //Natural: MOVE 12 TO #FUTURE-MM
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(6);                                                                                                             //Natural: MOVE 6 TO #FUTURE-MM
                    //*  ANNUAL
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 801:812
            else if (condition(((pdaIaaa323.getIaaa320_Mode_Cde().greaterOrEqual(801) && pdaIaaa323.getIaaa320_Mode_Cde().lessOrEqual(812)))))
            {
                decideConditionsMet1319++;
                pnd_Hold_Mode.setValue(pdaIaaa323.getIaaa320_Mode_Cde());                                                                                                 //Natural: ASSIGN #HOLD-MODE := IAAA320.MODE-CDE
                if (condition(pnd_Curr_Chk_Dte_Pnd_Curr_Chk_Dte_Mm.lessOrEqual(pnd_Hold_Mode_Pnd_Hold_Mode_Mm)))                                                          //Natural: IF #CURR-CHK-DTE-MM LE #HOLD-MODE-MM
                {
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(pnd_Hold_Mode_Pnd_Hold_Mode_Mm);                                                                                //Natural: ASSIGN #FUTURE-MM := #HOLD-MODE-MM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Future_Dte_Pnd_Future_Mm.setValue(pnd_Hold_Mode_Pnd_Hold_Mode_Mm);                                                                                //Natural: ASSIGN #FUTURE-MM := #HOLD-MODE-MM
                    pnd_Future_Dte_Pnd_Future_Yyyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #FUTURE-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1319 > 0))
            {
                pdaIaaa323.getIaaa320_Future_Pymnt_Dte().setValue(pnd_Future_Dte);                                                                                        //Natural: ASSIGN IAAA320.FUTURE-PYMNT-DTE := #FUTURE-DTE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                getReports().write(0, "*** INVALID MODE CODE ENCOUNTERED.               ***",NEWLINE,"*** FUTURE PAYMENT DATE WAS NOT CALCULATED       ***",              //Natural: WRITE '*** INVALID MODE CODE ENCOUNTERED.               ***' / '*** FUTURE PAYMENT DATE WAS NOT CALCULATED       ***' / '*** PIN:' IAAA320.PIN-NBR 'CONTRACT:' IAAA320.CONTRACT-NBR 'PAYEE:' IAAA320.PAYEE-CDE '***' /
                    NEWLINE,"*** PIN:",pdaIaaa323.getIaaa320_Pin_Nbr(),"CONTRACT:",pdaIaaa323.getIaaa320_Contract_Nbr(),"PAYEE:",pdaIaaa323.getIaaa320_Payee_Cde(),
                    "***",NEWLINE);
                if (Global.isEscape()) return;
                pdaIaaa323.getIaaa320_Future_Pymnt_Dte().reset();                                                                                                         //Natural: RESET IAAA320.FUTURE-PYMNT-DTE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF MODE = 100  MONTHLY
        }                                                                                                                                                                 //Natural: END-IF
        //*      PROCESS PRIOR MONTH
        //*  PRIOR MONTH
        short decideConditionsMet1475 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #GTN-REC.#MONTH-FLAG ( 1 ) = 2
        if (condition(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(1).equals(2)))
        {
            decideConditionsMet1475++;
            pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue(1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(1,1,":",12));                                                 //Natural: MOVE #GTN-REC.#GTN-DATA ( 1,1:12 ) TO WF-PYMNT-ADDR-GRP ( 1:12 )
            //*  JB01
            //*  PRIOR MONTH
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(1));                                                        //Natural: MOVE #CANADIAN-TAX ( 1 ) TO CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: WHEN #GTN-REC.#MONTH-FLAG ( 2 ) = 2
        else if (condition(pnd_Gtn_Rec_Pnd_Month_Flag.getValue(2).equals(2)))
        {
            decideConditionsMet1475++;
            pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue(1,":",12).setValue(pnd_Gtn_Rec_Pnd_Gtn_Data.getValue(2,1,":",12));                                                 //Natural: MOVE #GTN-REC.#GTN-DATA ( 2,1:12 ) TO WF-PYMNT-ADDR-GRP ( 1:12 )
            //*  JB01
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().setValue(pnd_Gtn_Rec_Pnd_Canadian_Tax.getValue(2));                                                        //Natural: MOVE #CANADIAN-TAX ( 2 ) TO CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde());                                                       //Natural: ASSIGN IAAA320.PRIOR-STATE-RSDNCY-CDE := ANNT-RSDNCY-CDE
        if (condition(pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde().notEquals(pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde())))                                           //Natural: IF IAAA320.PRIOR-STATE-RSDNCY-CDE NE IAAA320.CURR-STATE-RSDNCY-CDE
        {
            if (condition((pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde().equals(pnd_States_Pnd_Mstate_Nbr.getValue("*")) || pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde().equals(pnd_States_Pnd_Mstate_Nbr.getValue("*"))))) //Natural: IF ( IAAA320.PRIOR-STATE-RSDNCY-CDE = #MSTATE-NBR ( * ) OR IAAA320.CURR-STATE-RSDNCY-CDE = #MSTATE-NBR ( * ) )
            {
                pdaIaaa323.getIaaa320_State_Rsdncy_Chg_Flag().setValue(1);                                                                                                //Natural: ASSIGN IAAA320.STATE-RSDNCY-CHG-FLAG := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*     PRIOR CHECK DATES
        pnd_Temp_Date_Pnd_Temp_Date_A.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A
        pdaIaaa323.getIaaa320_Prior_Check_Dte().setValue(pnd_Temp_Date);                                                                                                  //Natural: ASSIGN IAAA320.PRIOR-CHECK-DTE := #TEMP-DATE
        pnd_Temp_Date_Pnd_Temp_Date_A.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-A
        pdaIaaa323.getIaaa320_Prior_Cycle_Dte().setValue(pnd_Temp_Date);                                                                                                  //Natural: ASSIGN IAAA320.PRIOR-CYCLE-DTE := #TEMP-DATE
        //*     PRIOR DEDUCTIONS  -  SET CHANGE INDICATORS
        //*     TRY TO MATCH PRIOR DEDUCTIONS AGAINST DEDUCTIONS IN THE
        //*     NET CHANGE DEDUCTION TABLE.
        //*   JB01
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().notEquals(getZero())))                                                                           //Natural: IF CANADIAN-TAX-AMT NE 0
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10).notEquals(getZero())))                                                             //Natural: IF PYMNT-DED-CDE ( 10 ) NE 0
            {
                getReports().write(0, "SYSTEM ERROR - MORE THAN 10 DEDUCTIONS: CONTACT SYSTEMS");                                                                         //Natural: WRITE "SYSTEM ERROR - MORE THAN 10 DEDUCTIONS: CONTACT SYSTEMS"
                if (Global.isEscape()) return;
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            FOR10:                                                                                                                                                        //Natural: FOR #X 9 TO 1 STEP -1
            for (pnd_X.setValue(9); condition(pnd_X.greaterOrEqual(1)); pnd_X.nsubtract(1))
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X.getDec().add(1)).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X)); //Natural: MOVE PYMNT-DED-CDE ( #X ) TO PYMNT-DED-CDE ( #X + 1 )
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X.getDec().add(1)).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X)); //Natural: MOVE PYMNT-DED-AMT ( #X ) TO PYMNT-DED-AMT ( #X + 1 )
                //*  JB01
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1).setValue(12);                                                                                     //Natural: ASSIGN PYMNT-DED-CDE ( 1 ) := 12
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());                                     //Natural: ASSIGN PYMNT-DED-AMT ( 1 ) := CANADIAN-TAX-AMT
        }                                                                                                                                                                 //Natural: END-IF
        FOR11:                                                                                                                                                            //Natural: FOR #X = 1 TO 10
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(10)); pnd_X.nadd(1))
        {
            //* ***********************************************************************
            //*    SPECIAL LOGIC FOR NYST DEDUCTION
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X).equals(getZero()) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X).notEquals(getZero()))) //Natural: IF PYMNT-DED-CDE ( #X ) = 0 AND PYMNT-DED-AMT ( #X ) NE 0
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X).setValue(6);                                                                              //Natural: ASSIGN PYMNT-DED-CDE ( #X ) := 6
            }                                                                                                                                                             //Natural: END-IF
            pnd_Temp_Ded.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X));                                                                       //Natural: ASSIGN #TEMP-DED := PYMNT-DED-CDE ( #X )
            DbsUtil.examine(new ExamineSource(pdaIaaa323.getIaaa320_Ded_Cde_A().getValue("*")), new ExamineSearch(pnd_Temp_Ded_Pnd_Temp_Ded_A), new ExamineGivingIndex(pnd_Ded_Sub)); //Natural: EXAMINE IAAA320.DED-CDE-A ( * ) FOR #TEMP-DED-A GIVING INDEX #DED-SUB
            //*   FOUND A MATCHING CURRENT DEDUCTION
            if (condition(pnd_Ded_Sub.greater(getZero())))                                                                                                                //Natural: IF #DED-SUB GT 0
            {
                pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue(pnd_Ded_Sub).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X));                        //Natural: ADD PYMNT-DED-AMT ( #X ) TO IAAA320.DED-PRIOR-AMT ( #DED-SUB )
                //*  UNMATCHED PRIOR DEDUCTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaIaaa323.getIaaa320_Deduction_Cnt().greaterOrEqual(10)))                                                                                  //Natural: IF IAAA320.DEDUCTION-CNT GE 10
                {
                    getReports().write(0, "PRIOR DEDUCTION WITH NO MATCHING CURRENT DEDUCTION",NEWLINE,"WAS BYPASSED BECAUSE ALL 10 DEDUCTION BUCKETS ARE",               //Natural: WRITE 'PRIOR DEDUCTION WITH NO MATCHING CURRENT DEDUCTION' / 'WAS BYPASSED BECAUSE ALL 10 DEDUCTION BUCKETS ARE' / 'FULL.' IAAA320.PIN-NBR IAAA320.CONTRACT-NBR IAAA320.PAYEE-CDE IAAA320.SSN-NBR
                        NEWLINE,"FULL.",pdaIaaa323.getIaaa320_Pin_Nbr(),pdaIaaa323.getIaaa320_Contract_Nbr(),pdaIaaa323.getIaaa320_Payee_Cde(),pdaIaaa323.getIaaa320_Ssn_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  INSERT DEDUCTION INTO EMPTY SLOT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IN NET CHANGE DEDUCTION TABLE
                    pdaIaaa323.getIaaa320_Deduction_Cnt().nadd(1);                                                                                                        //Natural: ADD 1 TO IAAA320.DEDUCTION-CNT
                    pdaIaaa323.getIaaa320_Ded_Cde().getValue(pdaIaaa323.getIaaa320_Deduction_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_X)); //Natural: ASSIGN IAAA320.DED-CDE ( DEDUCTION-CNT ) := PYMNT-DED-CDE ( #X )
                    pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue(pdaIaaa323.getIaaa320_Deduction_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.DED-PRIOR-AMT ( DEDUCTION-CNT ) := PYMNT-DED-AMT ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR12:                                                                                                                                                            //Natural: FOR #DED-SUB = 1 TO IAAA320.DEDUCTION-CNT
        for (pnd_Ded_Sub.setValue(1); condition(pnd_Ded_Sub.lessOrEqual(pdaIaaa323.getIaaa320_Deduction_Cnt())); pnd_Ded_Sub.nadd(1))
        {
            //*  JB01
            if (condition(pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue(pnd_Ded_Sub).notEquals(pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue(pnd_Ded_Sub))))             //Natural: IF IAAA320.DED-CURR-AMT ( #DED-SUB ) NE IAAA320.DED-PRIOR-AMT ( #DED-SUB )
            {
                pdaIaaa323.getIaaa320_Ded_Amt_Chg_Flag().getValue(pnd_Ded_Sub).setValue(1);                                                                               //Natural: ASSIGN IAAA320.DED-AMT-CHG-FLAG ( #DED-SUB ) := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #X = 1 TO INV-ACCT-COUNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
        {
            pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_X));                                      //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #X ) TO IAAA320.TAX-PRIOR-FED-AMT
            pdaIaaa323.getIaaa320_Tax_Prior_St_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_X));                                      //Natural: ADD INV-ACCT-STATE-TAX-AMT ( #X ) TO IAAA320.TAX-PRIOR-ST-AMT
            pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_X));                                     //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #X ) TO IAAA320.TAX-PRIOR-LOC-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        short decideConditionsMet1566 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAAA320.TAX-PRIOR-FED-AMT NE IAAA320.TAX-CURR-FED-AMT
        if (condition(pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().notEquals(pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt())))
        {
            decideConditionsMet1566++;
            pdaIaaa323.getIaaa320_Tax_Fed_Chg_Flag().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TAX-FED-CHG-FLAG := 1
        }                                                                                                                                                                 //Natural: WHEN IAAA320.TAX-PRIOR-ST-AMT NE IAAA320.TAX-CURR-ST-AMT
        if (condition(pdaIaaa323.getIaaa320_Tax_Prior_St_Amt().notEquals(pdaIaaa323.getIaaa320_Tax_Curr_St_Amt())))
        {
            decideConditionsMet1566++;
            pdaIaaa323.getIaaa320_Tax_St_Chg_Flag().setValue(1);                                                                                                          //Natural: ASSIGN IAAA320.TAX-ST-CHG-FLAG := 1
        }                                                                                                                                                                 //Natural: WHEN IAAA320.TAX-PRIOR-LOC-AMT NE IAAA320.TAX-CURR-LOC-AMT
        if (condition(pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt().notEquals(pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt())))
        {
            decideConditionsMet1566++;
            pdaIaaa323.getIaaa320_Tax_Loc_Chg_Flag().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TAX-LOC-CHG-FLAG := 1
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1566 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  FIX GROUP CONTRACTS ON PRIOR FILE
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0250000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0899999"))) //Natural: IF CNTRCT-PPCN-NBR = 'W0250000' THRU 'W0899999'
        {
            pnd_Group_Prior_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #GROUP-PRIOR-CNT
            FOR14:                                                                                                                                                        //Natural: FOR #X = 1 TO INV-ACCT-COUNT
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X).equals(1)))                                                                //Natural: IF INV-ACCT-CDE-N ( #X ) = 1
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X).setValue(21);                                                                        //Natural: ASSIGN INV-ACCT-CDE-N ( #X ) := 21
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X).setValue("G");                                                                   //Natural: ASSIGN INV-ACCT-CDE-ALPHA ( #X ) := 'G'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR15:                                                                                                                                                            //Natural: FOR #X = 1 TO INV-ACCT-COUNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_X.nadd(1))
        {
            //*   RCC
            //*  EXAMINE FULL IAAA320.FUND-ACCOUNT-CDE-A (*)  /* CONTAINS CURRENT
            //*   FOR FULL INV-ACCT-CDE-ALPHA (#X)             /* FIND INDEX VALUE
            //*        GIVING INDEX #FUND-SUB
            //*   INCLUDE VALUATION PERIOD IN EXAMINE STATEMENT
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X).equals(" ")))                                                          //Natural: IF INV-ACCT-VALUAT-PERIOD ( #X ) = ' '
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X).setValue("A");                                                                   //Natural: ASSIGN INV-ACCT-VALUAT-PERIOD ( #X ) := 'A'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Valuat.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X));                                  //Natural: ASSIGN #PREV-VALUAT := INV-ACCT-VALUAT-PERIOD ( #X )
            pnd_Prev_Valuat_Fund_Cde_Pnd_Prev_Fund_Cde_A.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X));                                  //Natural: ASSIGN #PREV-FUND-CDE-A := INV-ACCT-CDE-ALPHA ( #X )
            DbsUtil.examine(new ExamineSource(pdaIaaa323.getIaaa320_Fund_Valuat_Fund().getValue("*"),true), new ExamineSearch(pnd_Prev_Valuat_Fund_Cde,                   //Natural: EXAMINE FULL IAAA320.FUND-VALUAT-FUND ( * ) FOR FULL #PREV-VALUAT-FUND-CDE GIVING INDEX #FUND-SUB
                true), new ExamineGivingIndex(pnd_Fund_Sub));
            //*  RCC
            //*  FOUND MATCHING CURRENT FUND FOR PRIOR FUND
            if (condition(pnd_Fund_Sub.greater(getZero())))                                                                                                               //Natural: IF #FUND-SUB GT 0
            {
                //*   NEXT 2 LINES - RCC
                //*  MATCHED VALUATION
                if (condition(pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Fund_Sub).equals(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X)))) //Natural: IF IAAA320.FUND-VALUAT-PERIOD ( #FUND-SUB ) = INV-ACCT-VALUAT-PERIOD ( #X )
                {
                    pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X));         //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-N ( #FUND-SUB ) := INV-ACCT-CDE-N ( #X )
                    pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X));     //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-A ( #FUND-SUB ) := INV-ACCT-CDE-ALPHA ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_X));     //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) := INV-ACCT-UNIT-QTY ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-VALUE ( #FUND-SUB ) := INV-ACCT-UNIT-VALUE ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( #FUND-SUB ) := INV-ACCT-SETTL-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) := INV-ACCT-CNTRCT-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-DIVIDEND-AMT ( #FUND-SUB ) := INV-ACCT-DVDND-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( #FUND-SUB ) := INV-ACCT-NET-PYMNT-AMT ( #X )
                    short decideConditionsMet1621 = 0;                                                                                                                    //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) NE IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB )
                    if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub))))
                    {
                        decideConditionsMet1621++;
                        pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                //Natural: ASSIGN IAAA320.FUND-UNIT-QTY-CHG-FLAG ( #FUND-SUB ) := 1
                    }                                                                                                                                                     //Natural: WHEN IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-SETTLEMENT-AMT ( #FUND-SUB )
                    if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Fund_Sub))))
                    {
                        decideConditionsMet1621++;
                        pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                              //Natural: ASSIGN IAAA320.FUND-SETTLEMENT-CHG-FLAG ( #FUND-SUB ) := 1
                    }                                                                                                                                                     //Natural: WHEN IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB )
                    if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub))))
                    {
                        decideConditionsMet1621++;
                        pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                //Natural: ASSIGN IAAA320.FUND-CONTRACT-CHG-FLAG ( #FUND-SUB ) := 1
                    }                                                                                                                                                     //Natural: WHEN IAAA320.FUND-PRIOR-DIVIDEND-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-DIVIDEND-AMT ( #FUND-SUB )
                    if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Fund_Sub))))
                    {
                        decideConditionsMet1621++;
                        pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                //Natural: ASSIGN IAAA320.FUND-DIVIDEND-CHG-FLAG ( #FUND-SUB ) := 1
                    }                                                                                                                                                     //Natural: WHEN IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-NET-PYMNT-AMT ( #FUND-SUB )
                    if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Fund_Sub))))
                    {
                        decideConditionsMet1621++;
                        pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                               //Natural: ASSIGN IAAA320.FUND-NET-PYMNT-CHG-FLAG ( #FUND-SUB ) := 1
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet1621 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  FUND MATCHED -UNMATCHED VALUAT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaIaaa323.getIaaa320_Fund_Cnt().greaterOrEqual(40)))                                                                                   //Natural: IF IAAA320.FUND-CNT GE 40
                    {
                        getReports().write(0, "PRIOR FUND WITH NO MATCHING CURRENT FUND",NEWLINE,"WAS BYPASSED BECAUSE ALL 10 FUND BUCKETS ARE",NEWLINE,                  //Natural: WRITE 'PRIOR FUND WITH NO MATCHING CURRENT FUND' / 'WAS BYPASSED BECAUSE ALL 10 FUND BUCKETS ARE' / 'FULL.' IAAA320.PIN-NBR IAAA320.CONTRACT-NBR IAAA320.PAYEE-CDE IAAA320.SSN-NBR
                            "FULL.",pdaIaaa323.getIaaa320_Pin_Nbr(),pdaIaaa323.getIaaa320_Contract_Nbr(),pdaIaaa323.getIaaa320_Payee_Cde(),pdaIaaa323.getIaaa320_Ssn_Nbr());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  VALUAT DID NOT MATCHED
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*   NEXT 1 LINE - RCC
                                                                                                                                                                          //Natural: PERFORM VALUATION-NOT-MATCH
                        sub_Valuation_Not_Match();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VALUAT TEST
                }                                                                                                                                                         //Natural: END-IF
                //*  NO MATCHING CURRENT FUND FOR PRIOR FUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaIaaa323.getIaaa320_Fund_Cnt().greaterOrEqual(40)))                                                                                       //Natural: IF IAAA320.FUND-CNT GE 40
                {
                    getReports().write(0, "PRIOR FUND WITH NO MATCHING CURRENT FUND",NEWLINE,"WAS BYPASSED BECAUSE ALL 10 FUND BUCKETS ARE",NEWLINE,"FULL.",              //Natural: WRITE 'PRIOR FUND WITH NO MATCHING CURRENT FUND' / 'WAS BYPASSED BECAUSE ALL 10 FUND BUCKETS ARE' / 'FULL.' IAAA320.PIN-NBR IAAA320.CONTRACT-NBR IAAA320.PAYEE-CDE IAAA320.SSN-NBR
                        pdaIaaa323.getIaaa320_Pin_Nbr(),pdaIaaa323.getIaaa320_Contract_Nbr(),pdaIaaa323.getIaaa320_Payee_Cde(),pdaIaaa323.getIaaa320_Ssn_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  INSERT PRIOR FUND INTO EMPTY SLOT IN NET CHANGE FUND TB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaIaaa323.getIaaa320_Fund_Cnt().nadd(1);                                                                                                             //Natural: ADD 1 TO IAAA320.FUND-CNT
                    pnd_Write_Fund_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WRITE-FUND-CTR
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X).equals(" ")))                                                  //Natural: IF INV-ACCT-VALUAT-PERIOD ( #X ) = ' '
                    {
                        pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue("A");                                              //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( FUND-CNT ) := 'A'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( FUND-CNT ) := INV-ACCT-VALUAT-PERIOD ( #X )
                    }                                                                                                                                                     //Natural: END-IF
                    pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-N ( FUND-CNT ) := INV-ACCT-CDE-N ( #X )
                    pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-A ( FUND-CNT ) := INV-ACCT-CDE-ALPHA ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-QTY ( FUND-CNT ) := INV-ACCT-UNIT-QTY ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-VALUE ( FUND-CNT ) := INV-ACCT-UNIT-VALUE ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( FUND-CNT ) := INV-ACCT-SETTL-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-CONTRACT-AMT ( FUND-CNT ) := INV-ACCT-CNTRCT-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-DIVIDEND-AMT ( FUND-CNT ) := INV-ACCT-DVDND-AMT ( #X )
                    pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_X)); //Natural: ASSIGN IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( FUND-CNT ) := INV-ACCT-NET-PYMNT-AMT ( #X )
                    DbsUtil.examine(new ExamineSource(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct().getValue("*")), new ExamineSearch(pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pdaIaaa323.getIaaa320_Fund_Cnt())),  //Natural: EXAMINE #INV-ACCT ( * ) FOR IAAA320.FUND-ACCOUNT-CDE-A ( FUND-CNT ) GIVING INDEX #FUND-TBL-SUB
                        new ExamineGivingIndex(pnd_Fund_Tbl_Sub));
                    if (condition(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Company_Cde().getValue(pnd_Fund_Tbl_Sub).equals("C")))                                                  //Natural: IF #COMPANY-CDE ( #FUND-TBL-SUB ) = 'C'
                    {
                        pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(1);                                            //Natural: ASSIGN IAAA320.FUND-UNIT-QTY-CHG-FLAG ( FUND-CNT ) := 1
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Company_Cde().getValue(pnd_Fund_Tbl_Sub).equals("T")))                                                  //Natural: IF #COMPANY-CDE ( #FUND-TBL-SUB ) = 'T'
                    {
                        pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(1);                                            //Natural: ASSIGN IAAA320.FUND-CONTRACT-CHG-FLAG ( FUND-CNT ) := 1
                        pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(1);                                            //Natural: ASSIGN IAAA320.FUND-DIVIDEND-CHG-FLAG ( FUND-CNT ) := 1
                    }                                                                                                                                                     //Natural: END-IF
                    pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(1);                                              //Natural: ASSIGN IAAA320.FUND-SETTLEMENT-CHG-FLAG ( FUND-CNT ) := 1
                    pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pdaIaaa323.getIaaa320_Fund_Cnt()).setValue(1);                                               //Natural: ASSIGN IAAA320.FUND-NET-PYMNT-CHG-FLAG ( FUND-CNT ) := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   NEXT 4 LINES - RCC
        if (condition(pnd_Y.greater(getZero())))                                                                                                                          //Natural: IF #Y > 0
        {
                                                                                                                                                                          //Natural: PERFORM AFTER-VALUAT-NOT-EQ
            sub_After_Valuat_Not_Eq();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Y.reset();                                                                                                                                                    //Natural: RESET #Y
        FOR16:                                                                                                                                                            //Natural: FOR #FUND-SUB = 1 TO IAAA320.FUND-CNT
        for (pnd_Fund_Sub.setValue(1); condition(pnd_Fund_Sub.lessOrEqual(pdaIaaa323.getIaaa320_Fund_Cnt())); pnd_Fund_Sub.nadd(1))
        {
            short decideConditionsMet1691 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN ( IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) NE IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB ) ) AND IAAA320.FUND-UNIT-QTY-CHG-FLAG ( #FUND-SUB ) NE 1
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub)) 
                && pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Fund_Sub).notEquals(1)))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                        //Natural: ASSIGN IAAA320.FUND-UNIT-QTY-CHG-FLAG ( #FUND-SUB ) := 1
            }                                                                                                                                                             //Natural: WHEN ( IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-SETTLEMENT-AMT ( #FUND-SUB ) ) AND IAAA320.FUND-SETTLEMENT-CHG-FLAG ( #FUND-SUB ) NE 1
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Fund_Sub)) 
                && pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Fund_Sub).notEquals(1)))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                      //Natural: ASSIGN IAAA320.FUND-SETTLEMENT-CHG-FLAG ( #FUND-SUB ) := 1
            }                                                                                                                                                             //Natural: WHEN ( IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB ) ) AND IAAA320.FUND-CONTRACT-CHG-FLAG ( #FUND-SUB ) NE 1
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub)) 
                && pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Fund_Sub).notEquals(1)))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                        //Natural: ASSIGN IAAA320.FUND-CONTRACT-CHG-FLAG ( #FUND-SUB ) := 1
            }                                                                                                                                                             //Natural: WHEN ( IAAA320.FUND-PRIOR-DIVIDEND-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-DIVIDEND-AMT ( #FUND-SUB ) ) AND IAAA320.FUND-DIVIDEND-CHG-FLAG ( #FUND-SUB ) NE 1
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Fund_Sub)) 
                && pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Fund_Sub).notEquals(1)))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                        //Natural: ASSIGN IAAA320.FUND-DIVIDEND-CHG-FLAG ( #FUND-SUB ) := 1
            }                                                                                                                                                             //Natural: WHEN ( IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( #FUND-SUB ) NE IAAA320.FUND-CURR-NET-PYMNT-AMT ( #FUND-SUB ) ) AND IAAA320.FUND-NET-PYMNT-CHG-FLAG ( #FUND-SUB ) NE 1
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Fund_Sub).notEquals(pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Fund_Sub)) 
                && pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Fund_Sub).notEquals(1)))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Fund_Sub).setValue(1);                                                                       //Natural: ASSIGN IAAA320.FUND-NET-PYMNT-CHG-FLAG ( #FUND-SUB ) := 1
            }                                                                                                                                                             //Natural: WHEN IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) = 0 AND IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB ) GT 0
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).equals(getZero()) && pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub).greater(getZero())))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Transfer_Ind().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TRANSFER-IND := 1
            }                                                                                                                                                             //Natural: WHEN IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) GT 0 AND IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB ) = 0
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).greater(getZero()) && pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub).equals(getZero())))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Transfer_Ind().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TRANSFER-IND := 1
            }                                                                                                                                                             //Natural: WHEN IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) = 0 AND IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB ) GT 0
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).equals(getZero()) && pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub).greater(getZero())))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Transfer_Ind().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TRANSFER-IND := 1
            }                                                                                                                                                             //Natural: WHEN IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) GT 0 AND IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB ) = 0
            if (condition(pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).greater(getZero()) && pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub).equals(getZero())))
            {
                decideConditionsMet1691++;
                pdaIaaa323.getIaaa320_Transfer_Ind().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TRANSFER-IND := 1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1691 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*    CHECK FOR TRANSFER  - SET TRANSFER INDICATOE
        //*  ADDED FOLLOWING IF STMNT TRANSFERS SET TRANSFER IND     10/96
        if (condition(pdaIaaa323.getIaaa320_Fund_Cnt().greater(1)))                                                                                                       //Natural: IF IAAA320.FUND-CNT GT 1
        {
            if (condition((pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue("*").equals(1) || pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue("*").equals(1)))) //Natural: IF ( IAAA320.FUND-UNIT-QTY-CHG-FLAG ( * ) = 1 OR IAAA320.FUND-CONTRACT-CHG-FLAG ( * ) = 1 )
            {
                pdaIaaa323.getIaaa320_Transfer_Ind().setValue(1);                                                                                                         //Natural: ASSIGN IAAA320.TRANSFER-IND := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   SET MAILING LABEL INDICATOR
        //*  CHANGE OCCURRED
        if (condition((pdaIaaa323.getIaaa320_Ded_Amt_Chg_Flag().getValue("*").equals(1) || pdaIaaa323.getIaaa320_Tax_Fed_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Tax_St_Chg_Flag().equals(1)  //Natural: IF ( IAAA320.DED-AMT-CHG-FLAG ( * ) = 1 OR IAAA320.TAX-FED-CHG-FLAG = 1 OR IAAA320.TAX-ST-CHG-FLAG = 1 OR IAAA320.TAX-LOC-CHG-FLAG = 1 OR IAAA320.FUND-UNIT-QTY-CHG-FLAG ( * ) = 1 OR IAAA320.FUND-CONTRACT-CHG-FLAG ( * ) = 1 OR IAAA320.FUND-SETTLEMENT-CHG-FLAG ( * ) = 1 OR IAAA320.FUND-DIVIDEND-CHG-FLAG ( * ) = 1 OR IAAA320.FUND-NET-PYMNT-CHG-FLAG ( * ) = 1 OR #NET-CHG-FLAG )
            || pdaIaaa323.getIaaa320_Tax_Loc_Chg_Flag().equals(1) || pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue("*").equals(1) || pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue("*").equals(1) 
            || pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue("*").equals(1) || pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue("*").equals(1) 
            || pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue("*").equals(1) || pnd_Net_Chg_Flag.getBoolean())))
        {
            pdaIaaa323.getIaaa320_Mailing_Label_Ind().setValue("2");                                                                                                      //Natural: ASSIGN IAAA320.MAILING-LABEL-IND := '2'
            pnd_Net_Chg_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NET-CHG-CNT
            if (condition(! (pnd_Net_Chg_Flag.getBoolean())))                                                                                                             //Natural: IF NOT #NET-CHG-FLAG
            {
                pnd_Net_Chg_Flag.setValue(true);                                                                                                                          //Natural: ASSIGN #NET-CHG-FLAG := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  NO CHANGE OCCURRED, PRODUCE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIaaa323.getIaaa320_Mailing_Label_Ind().setValue("1");                                                                                                      //Natural: ASSIGN IAAA320.MAILING-LABEL-IND := '1'
            //*  MAILING LABELS ONLY.
            pnd_Mailing_Label_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MAILING-LABEL-CNT
        }                                                                                                                                                                 //Natural: END-IF
        //*  BUILD-NET-CHANGE-RECORD
    }
    //*  RCC
    private void sub_Valuation_Not_Match() throws Exception                                                                                                               //Natural: VALUATION-NOT-MATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_Y.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #Y
        pnd_Write_Fund_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRITE-FUND-CTR
        pnd_Ws_Save_Fund_Info_Pnd_Save_Valuat.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Fund_Sub));                                //Natural: ASSIGN #SAVE-VALUAT ( #Y ) := IAAA320.FUND-VALUAT-PERIOD ( #FUND-SUB )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_N.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Fund_Sub));                         //Natural: MOVE IAAA320.FUND-ACCOUNT-CDE-N ( #FUND-SUB ) TO #FUND-ACCOUNT-CDE-N ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_A.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Fund_Sub));                         //Natural: MOVE IAAA320.FUND-ACCOUNT-CDE-A ( #FUND-SUB ) TO #FUND-ACCOUNT-CDE-A ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Qty.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub));                         //Natural: MOVE IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB ) TO #FUND-CURR-UNIT-QTY ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Value.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_Fund_Sub));                     //Natural: MOVE IAAA320.FUND-CURR-UNIT-VALUE ( #FUND-SUB ) TO #FUND-CURR-UNIT-VALUE ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Settlement_Amt.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Fund_Sub));             //Natural: MOVE IAAA320.FUND-CURR-SETTLEMENT-AMT ( #FUND-SUB ) TO #FUND-CURR-SETTLEMENT-AMT ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Contract_Amt.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub));                 //Natural: MOVE IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB ) TO #FUND-CURR-CONTRACT-AMT ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Dividend_Amt.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Fund_Sub));                 //Natural: MOVE IAAA320.FUND-CURR-DIVIDEND-AMT ( #FUND-SUB ) TO #FUND-CURR-DIVIDEND-AMT ( #Y )
        pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Net_Pymnt_Amt.getValue(pnd_Y).setValue(pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Fund_Sub));               //Natural: MOVE IAAA320.FUND-CURR-NET-PYMNT-AMT ( #FUND-SUB ) TO #FUND-CURR-NET-PYMNT-AMT ( #Y )
        //*   WRITE 1ST OCCUR
                                                                                                                                                                          //Natural: PERFORM RESET-CURR
        sub_Reset_Curr();
        if (condition(Global.isEscape())) {return;}
        pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_X));             //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( #FUND-SUB ) := INV-ACCT-VALUAT-PERIOD ( #X )
        pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_X));                     //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-N ( #FUND-SUB ) := INV-ACCT-CDE-N ( #X )
        pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_X));                 //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-A ( #FUND-SUB ) := INV-ACCT-CDE-ALPHA ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_X));                 //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-QTY ( #FUND-SUB ) := INV-ACCT-UNIT-QTY ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_X));             //Natural: ASSIGN IAAA320.FUND-PRIOR-UNIT-VALUE ( #FUND-SUB ) := INV-ACCT-UNIT-VALUE ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_X));          //Natural: ASSIGN IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( #FUND-SUB ) := INV-ACCT-SETTL-AMT ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_X));           //Natural: ASSIGN IAAA320.FUND-PRIOR-CONTRACT-AMT ( #FUND-SUB ) := INV-ACCT-CNTRCT-AMT ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_X));            //Natural: ASSIGN IAAA320.FUND-PRIOR-DIVIDEND-AMT ( #FUND-SUB ) := INV-ACCT-DVDND-AMT ( #X )
        pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Fund_Sub).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_X));       //Natural: ASSIGN IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( #FUND-SUB ) := INV-ACCT-NET-PYMNT-AMT ( #X )
    }
    //*  RCC
    private void sub_After_Valuat_Not_Eq() throws Exception                                                                                                               //Natural: AFTER-VALUAT-NOT-EQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_M.reset();                                                                                                                                                    //Natural: RESET #M #XX
        pnd_Xx.reset();
        if (condition(pdaIaaa323.getIaaa320_Fund_Cnt().greaterOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())))                                                 //Natural: IF IAAA320.FUND-CNT GE INV-ACCT-COUNT
        {
            pnd_Xx.setValue(pdaIaaa323.getIaaa320_Fund_Cnt());                                                                                                            //Natural: MOVE IAAA320.FUND-CNT TO #XX
            pnd_Y.setValue(pnd_Write_Fund_Ctr);                                                                                                                           //Natural: ASSIGN #Y := #WRITE-FUND-CTR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Xx.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                                                            //Natural: MOVE INV-ACCT-COUNT TO #XX
            pnd_Y.setValue(pnd_Xx);                                                                                                                                       //Natural: ASSIGN #Y := #XX
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Xx.greaterOrEqual(pnd_Y)))                                                                                                                  //Natural: IF #XX GE #Y
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Xx.nadd(1);                                                                                                                                               //Natural: ASSIGN #XX := #XX + 1
            pnd_M.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #M
                                                                                                                                                                          //Natural: PERFORM RESET-PRIOR
            sub_Reset_Prior();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Save_Valuat.getValue(pnd_M));                                  //Natural: ASSIGN IAAA320.FUND-VALUAT-PERIOD ( #XX ) := #SAVE-VALUAT ( #M )
            pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_N.getValue(pnd_M));                           //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-N ( #XX ) := #FUND-ACCOUNT-CDE-N ( #M )
            pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Account_Cde_A.getValue(pnd_M));                           //Natural: ASSIGN IAAA320.FUND-ACCOUNT-CDE-A ( #XX ) := #FUND-ACCOUNT-CDE-A ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Qty.getValue(pnd_M));                           //Natural: ASSIGN IAAA320.FUND-CURR-UNIT-QTY ( #XX ) := #FUND-CURR-UNIT-QTY ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Unit_Value.getValue(pnd_M));                       //Natural: ASSIGN IAAA320.FUND-CURR-UNIT-VALUE ( #XX ) := #FUND-CURR-UNIT-VALUE ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Settlement_Amt.getValue(pnd_M));               //Natural: ASSIGN IAAA320.FUND-CURR-SETTLEMENT-AMT ( #XX ) := #FUND-CURR-SETTLEMENT-AMT ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Contract_Amt.getValue(pnd_M));                   //Natural: ASSIGN IAAA320.FUND-CURR-CONTRACT-AMT ( #XX ) := #FUND-CURR-CONTRACT-AMT ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Dividend_Amt.getValue(pnd_M));                   //Natural: ASSIGN IAAA320.FUND-CURR-DIVIDEND-AMT ( #XX ) := #FUND-CURR-DIVIDEND-AMT ( #M )
            pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Xx).setValue(pnd_Ws_Save_Fund_Info_Pnd_Fund_Curr_Net_Pymnt_Amt.getValue(pnd_M));                 //Natural: ASSIGN IAAA320.FUND-CURR-NET-PYMNT-AMT ( #XX ) := #FUND-CURR-NET-PYMNT-AMT ( #M )
            //* ************
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pdaIaaa323.getIaaa320_Fund_Cnt().setValue(pnd_Y);                                                                                                                 //Natural: ASSIGN IAAA320.FUND-CNT := #Y
    }
    //*   RCC
    private void sub_Reset_Prior() throws Exception                                                                                                                       //Natural: RESET-PRIOR
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Xx).reset();                                                                                          //Natural: RESET IAAA320.FUND-UNIT-QTY-CHG-FLAG ( #XX ) IAAA320.FUND-CONTRACT-CHG-FLAG ( #XX ) IAAA320.FUND-SETTLEMENT-CHG-FLAG ( #XX ) IAAA320.FUND-DIVIDEND-CHG-FLAG ( #XX ) IAAA320.FUND-NET-PYMNT-CHG-FLAG ( #XX ) IAAA320.FUND-ACCOUNT-CDE-N ( #XX ) IAAA320.FUND-ACCOUNT-CDE-A ( #XX ) IAAA320.FUND-PRIOR-UNIT-QTY ( #XX ) IAAA320.FUND-PRIOR-UNIT-VALUE ( #XX ) IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( #XX ) IAAA320.FUND-PRIOR-CONTRACT-AMT ( #XX ) IAAA320.FUND-PRIOR-DIVIDEND-AMT ( #XX ) IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( #XX )
        pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue(pnd_Xx).reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue(pnd_Xx).reset();
    }
    //*   RCC
    private void sub_Reset_Curr() throws Exception                                                                                                                        //Natural: RESET-CURR
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue(pnd_Fund_Sub).reset();                                                                                    //Natural: RESET IAAA320.FUND-UNIT-QTY-CHG-FLAG ( #FUND-SUB ) IAAA320.FUND-CONTRACT-CHG-FLAG ( #FUND-SUB ) IAAA320.FUND-SETTLEMENT-CHG-FLAG ( #FUND-SUB ) IAAA320.FUND-DIVIDEND-CHG-FLAG ( #FUND-SUB ) IAAA320.FUND-NET-PYMNT-CHG-FLAG ( #FUND-SUB ) IAAA320.FUND-ACCOUNT-CDE-N ( #FUND-SUB ) IAAA320.FUND-ACCOUNT-CDE-A ( #FUND-SUB ) IAAA320.FUND-CURR-UNIT-QTY ( #FUND-SUB ) IAAA320.FUND-CURR-UNIT-VALUE ( #FUND-SUB ) IAAA320.FUND-CURR-SETTLEMENT-AMT ( #FUND-SUB ) IAAA320.FUND-CURR-CONTRACT-AMT ( #FUND-SUB ) IAAA320.FUND-CURR-DIVIDEND-AMT ( #FUND-SUB ) IAAA320.FUND-CURR-NET-PYMNT-AMT ( #FUND-SUB )
        pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue(pnd_Fund_Sub).reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue(pnd_Fund_Sub).reset();
    }
    private void sub_Reset_Net_Change_Record() throws Exception                                                                                                           //Natural: RESET-NET-CHANGE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  MM 03/06/01
        pdaIaaa323.getIaaa320_Pin_Nbr().reset();                                                                                                                          //Natural: RESET IAAA320.PIN-NBR IAAA320.CONTRACT-NBR IAAA320.PAYEE-CDE IAAA320.SSN-NBR IAAA320.ZIP-CDE IAAA320.COMPANY-CDE IAAA320.MAILING-LABEL-IND IAAA320.HOLD-IND IAAA320.HOLD-CDE IAAA320.HOLD-GRP-CDE IAAA320.MODE-CDE IAAA320.OFF-MODE-IND IAAA320.TRANSFER-IND IAAA320.COMBINED-PYMNT-IND IAAA320.GTN-RETURN-CDE IAAA320.GTN-REPORT-CDE ( * ) IAAA320.CURR-STATE-RSDNCY-CDE IAAA320.PRIOR-STATE-RSDNCY-CDE IAAA320.STATE-RSDNCY-CHG-FLAG IAAA320.PH-LAST-NME IAAA320.PH-FIRST-NME IAAA320.PH-MIDDLE-NME IAAA320.PH-MAILING-NME IAAA320.PH-ADDRESS-LINE-1 IAAA320.PH-ADDRESS-LINE-2 IAAA320.PH-ADDRESS-LINE-3 IAAA320.PH-ADDRESS-LINE-4 IAAA320.PH-ADDRESS-LINE-5 IAAA320.PH-ADDRESS-LINE-6 IAAA320.POSTAL-DATA IAAA320.PRIOR-CHECK-DTE IAAA320.CURR-CHECK-DTE IAAA320.PRIOR-CYCLE-DTE IAAA320.CURR-CYCLE-DTE IAAA320.FUTURE-PYMNT-DTE IAAA320.CNTRCT-ANNTY-INS-TYPE IAAA320.CNTRCT-ANNTY-TYPE-CDE IAAA320.CNTRCT-INSURANCE-OPTION IAAA320.CNTRCT-LIFE-CONTINGENCY IAAA320.CNTRCT-OPTION-CDE IAAA320.DEDUCTION-CNT IAAA320.DED-CDE ( * ) IAAA320.DED-PRIOR-AMT ( * ) IAAA320.DED-CURR-AMT ( * ) IAAA320.DED-AMT-CHG-FLAG ( * ) IAAA320.TAX-PRIOR-FED-AMT IAAA320.TAX-PRIOR-ST-AMT IAAA320.TAX-PRIOR-LOC-AMT IAAA320.TAX-CURR-FED-AMT IAAA320.TAX-CURR-ST-AMT IAAA320.TAX-CURR-LOC-AMT IAAA320.TAX-FED-CHG-FLAG IAAA320.TAX-ST-CHG-FLAG IAAA320.TAX-LOC-CHG-FLAG IAAA320.FUND-CNT IAAA320.FUND-VALUAT-PERIOD ( * ) IAAA320.FUND-ACCOUNT-CDE-N ( * ) IAAA320.FUND-ACCOUNT-CDE-A ( * ) IAAA320.FUND-PRIOR-UNIT-QTY ( * ) IAAA320.FUND-PRIOR-UNIT-VALUE ( * ) IAAA320.FUND-PRIOR-SETTLEMENT-AMT ( * ) IAAA320.FUND-PRIOR-CONTRACT-AMT ( * ) IAAA320.FUND-PRIOR-DIVIDEND-AMT ( * ) IAAA320.FUND-PRIOR-NET-PYMNT-AMT ( * ) IAAA320.FUND-CURR-UNIT-QTY ( * ) IAAA320.FUND-CURR-UNIT-VALUE ( * ) IAAA320.FUND-CURR-SETTLEMENT-AMT ( * ) IAAA320.FUND-CURR-CONTRACT-AMT ( * ) IAAA320.FUND-CURR-DIVIDEND-AMT ( * ) IAAA320.FUND-CURR-NET-PYMNT-AMT ( * ) IAAA320.FUND-UNIT-QTY-CHG-FLAG ( * ) IAAA320.FUND-UNIT-VALUE-CHG-FLAG ( * ) IAAA320.FUND-SETTLEMENT-CHG-FLAG ( * ) IAAA320.FUND-CONTRACT-CHG-FLAG ( * ) IAAA320.FUND-DIVIDEND-CHG-FLAG ( * ) IAAA320.FUND-NET-PYMNT-CHG-FLAG ( * )
        pdaIaaa323.getIaaa320_Contract_Nbr().reset();
        pdaIaaa323.getIaaa320_Payee_Cde().reset();
        pdaIaaa323.getIaaa320_Ssn_Nbr().reset();
        pdaIaaa323.getIaaa320_Zip_Cde().reset();
        pdaIaaa323.getIaaa320_Company_Cde().reset();
        pdaIaaa323.getIaaa320_Mailing_Label_Ind().reset();
        pdaIaaa323.getIaaa320_Hold_Ind().reset();
        pdaIaaa323.getIaaa320_Hold_Cde().reset();
        pdaIaaa323.getIaaa320_Hold_Grp_Cde().reset();
        pdaIaaa323.getIaaa320_Mode_Cde().reset();
        pdaIaaa323.getIaaa320_Off_Mode_Ind().reset();
        pdaIaaa323.getIaaa320_Transfer_Ind().reset();
        pdaIaaa323.getIaaa320_Combined_Pymnt_Ind().reset();
        pdaIaaa323.getIaaa320_Gtn_Return_Cde().reset();
        pdaIaaa323.getIaaa320_Gtn_Report_Cde().getValue("*").reset();
        pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde().reset();
        pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde().reset();
        pdaIaaa323.getIaaa320_State_Rsdncy_Chg_Flag().reset();
        pdaIaaa323.getIaaa320_Ph_Last_Nme().reset();
        pdaIaaa323.getIaaa320_Ph_First_Nme().reset();
        pdaIaaa323.getIaaa320_Ph_Middle_Nme().reset();
        pdaIaaa323.getIaaa320_Ph_Mailing_Nme().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_1().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_2().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_3().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_4().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_5().reset();
        pdaIaaa323.getIaaa320_Ph_Address_Line_6().reset();
        pdaIaaa323.getIaaa320_Postal_Data().reset();
        pdaIaaa323.getIaaa320_Prior_Check_Dte().reset();
        pdaIaaa323.getIaaa320_Curr_Check_Dte().reset();
        pdaIaaa323.getIaaa320_Prior_Cycle_Dte().reset();
        pdaIaaa323.getIaaa320_Curr_Cycle_Dte().reset();
        pdaIaaa323.getIaaa320_Future_Pymnt_Dte().reset();
        pdaIaaa323.getIaaa320_Cntrct_Annty_Ins_Type().reset();
        pdaIaaa323.getIaaa320_Cntrct_Annty_Type_Cde().reset();
        pdaIaaa323.getIaaa320_Cntrct_Insurance_Option().reset();
        pdaIaaa323.getIaaa320_Cntrct_Life_Contingency().reset();
        pdaIaaa323.getIaaa320_Cntrct_Option_Cde().reset();
        pdaIaaa323.getIaaa320_Deduction_Cnt().reset();
        pdaIaaa323.getIaaa320_Ded_Cde().getValue("*").reset();
        pdaIaaa323.getIaaa320_Ded_Prior_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Ded_Curr_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Ded_Amt_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Tax_Prior_Fed_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Prior_St_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Prior_Loc_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Curr_Fed_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Curr_St_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Curr_Loc_Amt().reset();
        pdaIaaa323.getIaaa320_Tax_Fed_Chg_Flag().reset();
        pdaIaaa323.getIaaa320_Tax_St_Chg_Flag().reset();
        pdaIaaa323.getIaaa320_Tax_Loc_Chg_Flag().reset();
        pdaIaaa323.getIaaa320_Fund_Cnt().reset();
        pdaIaaa323.getIaaa320_Fund_Valuat_Period().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_N().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Account_Cde_A().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Qty().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Unit_Value().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Settlement_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Contract_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Dividend_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Prior_Net_Pymnt_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Unit_Qty().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Unit_Value().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Settlement_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Contract_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Dividend_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Curr_Net_Pymnt_Amt().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Unit_Qty_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Unit_Value_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Settlement_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Contract_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Dividend_Chg_Flag().getValue("*").reset();
        pdaIaaa323.getIaaa320_Fund_Net_Pymnt_Chg_Flag().getValue("*").reset();
        //*  RESET-NET-CHANGE-RECORD
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, pnd_Read_Cnt,pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec(), new AlphanumericLength (35));                                             //Natural: WRITE #READ-CNT WF-PYMNT-ADDR-REC ( AL = 35 )
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(3, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(41),"IA NET CHANGE FILE SELECTION AND CONTROL PROCESS",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(58),"CONTROL REPORT",new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(54),"CURRENT CYCLE:",pnd_Curr_Cycle_Dte,NEWLINE,new TabSetting(54),"PRIOR CYCLE:  ",
            pnd_Prior_Cycle_Dte,NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(41),"IA NET CHANGE FILE SELECTION AND CONTROL PROCESS",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(49),"GROSS TO NET REPORT CODE TABLE",new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(54),"CURRENT CYCLE:",pnd_Curr_Cycle_Dte,NEWLINE,new TabSetting(54),"PRIOR CYCLE:  ",
            pnd_Prior_Cycle_Dte,NEWLINE,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(41),"IA NET CHANGE FILE SELECTION AND CONTROL PROCESS",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(61),"BAD PINS",new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(54),"CURRENT CYCLE:",pnd_Curr_Cycle_Dte,NEWLINE,new TabSetting(54),"PRIOR CYCLE:  ",
            pnd_Prior_Cycle_Dte,NEWLINE,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(3, "PRIOR/CURR",
        		pnd_Prior_Curr_Month_Flag,"/PIN",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(),"/CONTRACT",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/PAYEE",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde(),"/SSN",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),"/LAST NAME",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name(),"/FIRST NAME",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name());
        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),"GTN/REPORT CODE",
        		pnd_Rpt_Code,"/DESCRIPTION",
        		ldaFcpl810.getPnd_Gtn_Rpt_Msg_Pnd_Gtn_Rpt_Desc(),"/PRIOR COUNT",
        		pnd_Gtn_Rpt_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/CURRENT COUNT",
        		pnd_Gtn_Rpt_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));
        getReports().setDisplayColumns(1, " ",
        		pnd_Control_Totals_Pnd_Tot_Desc,"Read Count",
        		pnd_Control_Totals_Pnd_Tot_Read_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"Contract Amt",
        		pnd_Control_Totals_Pnd_Tot_Contract_Amt, new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"Dividend Amt",
        		pnd_Control_Totals_Pnd_Tot_Dividend_Amt, new ReportEditMask ("-Z,ZZZ,ZZZ,ZZ9.99"),"Gross Amt",
        		pnd_Control_Totals_Pnd_Tot_Sttlmnt_Amt, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"));
    }
}
