/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:04 PM
**        * FROM NATURAL PROGRAM : Iaapgrp
************************************************************
**        * FILE NAME            : Iaapgrp.java
**        * CLASS NAME           : Iaapgrp
**        * INSTANCE NAME        : Iaapgrp
************************************************************
************************************************************************
*
* PROGRAM : IAAPGRP
* DATE    : 10/08/2002
* PURPOSE : UPDATE DEDUCTION RECORDS FOR HARVARD FROM A FILE FEED.
* HISTORY :
*
* 12/04/2007 - JUN TINIO
* ADDED RESEQUENCING LOGIC AND SECONDARY UPDATE REPORT
*
* 11/2010  J BREMER  REPLACE PIN WITH SSN AND HARVARD ID ON REPORTS
*          JB01      IF NO HARVARD ID ON PAYEES > 01, GET FROM 01 REC
* 04/2017  O SOTTO   RE-STOWED ONLY FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapgrp extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Rec;
    private DbsField pnd_Input_Rec_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input_Rec__R_Field_1;
    private DbsField pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Input_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Id_Nbr;

    private DbsGroup pnd_Input_Rec__R_Field_2;
    private DbsField pnd_Input_Rec_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Rec_Pnd_Fill;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Fill;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Cde;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Pd_To_Dte_Amt;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Tot_Amt;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Strt_Dte;
    private DbsField pnd_Input_Rec_Pnd_Ddctn_Stp_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Rllvr_Cntrct_Nbr;

    private DataAccessProgramView vw_cpr2;
    private DbsField cpr2_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr2_Cntrct_Part_Payee_Cde;
    private DbsField cpr2_Rllvr_Cntrct_Nbr;
    private DbsField cpr2_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr2_Cntrct_Payee_Key;

    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;
    private DbsField iaa_Deduction_Lst_Trans_Dte;

    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Id_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;

    private DbsGroup iaa_Ddctn_Trans__R_Field_3;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ytd_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Intent_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Trans_Check_Dte;
    private DbsField iaa_Ddctn_Trans_Bfre_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Aftr_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField pnd_Ddctn_Cdes;
    private DbsField pnd_Ddctn_Seqs;
    private DbsField pnd_Idx;
    private DbsField pnd_I;
    private DbsField pnd_Old_Seq;
    private DbsField pnd_First;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_4;
    private DbsField pnd_Cpr_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cpr_Key_Pnd_Payee_Cde;
    private DbsField pnd_Cpr_Key2;

    private DbsGroup pnd_Cpr_Key2__R_Field_5;
    private DbsField pnd_Cpr_Key2_Pnd_Ppcn_Nbr2;
    private DbsField pnd_Cpr_Key2_Pnd_Payee_Cde2;
    private DbsField pnd_Save_Cntrct_Payee;

    private DbsGroup pnd_Save_Cntrct_Payee__R_Field_6;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Cntrct;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Payee;
    private DbsField pnd_Last_Cntrct_Payee;

    private DbsGroup pnd_Last_Cntrct_Payee__R_Field_7;
    private DbsField pnd_Last_Cntrct_Payee_Pnd_Last_Cntrct;
    private DbsField pnd_Last_Cntrct_Payee_Pnd_Last_Payee;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Date;
    private DbsField pnd_S_Invrse_Dte;
    private DbsField pnd_Seq_Nbr;
    private DbsField pnd_Last_Seq_Nbr;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Tot_Per_Ded;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_8;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Ddctn_Bfre_Key;

    private DbsGroup pnd_Ddctn_Bfre_Key__R_Field_9;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id;
    private DbsField pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_Rec = localVariables.newGroupInRecord("pnd_Input_Rec", "#INPUT-REC");
        pnd_Input_Rec_Pnd_Cntrct_Payee = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 10);

        pnd_Input_Rec__R_Field_1 = pnd_Input_Rec.newGroupInGroup("pnd_Input_Rec__R_Field_1", "REDEFINE", pnd_Input_Rec_Pnd_Cntrct_Payee);
        pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Input_Rec_Pnd_Cntrct_Payee_Cde = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Input_Rec_Pnd_Ddctn_Id_Nbr = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Id_Nbr", "#DDCTN-ID-NBR", FieldType.STRING, 7);

        pnd_Input_Rec__R_Field_2 = pnd_Input_Rec.newGroupInGroup("pnd_Input_Rec__R_Field_2", "REDEFINE", pnd_Input_Rec_Pnd_Ddctn_Id_Nbr);
        pnd_Input_Rec_Pnd_Ppg_Cde = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 4);
        pnd_Input_Rec_Pnd_Fill = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Fill", "#FILL", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Ddctn_Fill = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Fill", "#DDCTN-FILL", FieldType.STRING, 2);
        pnd_Input_Rec_Pnd_Ddctn_Cde = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Ddctn_Per_Amt = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Input_Rec_Pnd_Ddctn_Pd_To_Dte_Amt = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Pd_To_Dte_Amt", "#DDCTN-PD-TO-DTE-AMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Input_Rec_Pnd_Ddctn_Tot_Amt = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Tot_Amt", "#DDCTN-TOT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Input_Rec_Pnd_Ddctn_Strt_Dte = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Strt_Dte", "#DDCTN-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Rec_Pnd_Ddctn_Stp_Dte = pnd_Input_Rec.newFieldInGroup("pnd_Input_Rec_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Rllvr_Cntrct_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RLLVR_CNTRCT_NBR");
        registerRecord(vw_cpr);

        vw_cpr2 = new DataAccessProgramView(new NameInfo("vw_cpr2", "CPR2"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr2_Cntrct_Part_Ppcn_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr2_Cntrct_Part_Payee_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr2_Rllvr_Cntrct_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RLLVR_CNTRCT_NBR");
        cpr2_Prtcpnt_Tax_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr2_Cntrct_Payee_Key = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Payee_Key", "CNTRCT-PAYEE-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_KEY");
        cpr2_Cntrct_Payee_Key.setSuperDescriptor(true);
        registerRecord(vw_cpr2);

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Ytd_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_Ddctn_Pd_To_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        registerRecord(vw_iaa_Deduction);

        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Id_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");

        iaa_Ddctn_Trans__R_Field_3 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans__R_Field_3", "REDEFINE", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Seq_Nbr = iaa_Ddctn_Trans__R_Field_3.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans__R_Field_3.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Ytd_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ddctn_Trans_Ddctn_Pd_To_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Intent_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Trans_Check_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Ddctn_Trans_Bfre_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Ddctn_Trans_Aftr_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        registerRecord(vw_iaa_Ddctn_Trans);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Ddctn_Cdes = localVariables.newFieldArrayInRecord("pnd_Ddctn_Cdes", "#DDCTN-CDES", FieldType.STRING, 3, new DbsArrayController(1, 10));
        pnd_Ddctn_Seqs = localVariables.newFieldArrayInRecord("pnd_Ddctn_Seqs", "#DDCTN-SEQS", FieldType.NUMERIC, 3, new DbsArrayController(1, 10));
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Old_Seq = localVariables.newFieldInRecord("pnd_Old_Seq", "#OLD-SEQ", FieldType.NUMERIC, 3);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_4", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Ppcn_Nbr = pnd_Cpr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Payee_Cde = pnd_Cpr_Key__R_Field_4.newFieldInGroup("pnd_Cpr_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cpr_Key2 = localVariables.newFieldInRecord("pnd_Cpr_Key2", "#CPR-KEY2", FieldType.STRING, 12);

        pnd_Cpr_Key2__R_Field_5 = localVariables.newGroupInRecord("pnd_Cpr_Key2__R_Field_5", "REDEFINE", pnd_Cpr_Key2);
        pnd_Cpr_Key2_Pnd_Ppcn_Nbr2 = pnd_Cpr_Key2__R_Field_5.newFieldInGroup("pnd_Cpr_Key2_Pnd_Ppcn_Nbr2", "#PPCN-NBR2", FieldType.STRING, 10);
        pnd_Cpr_Key2_Pnd_Payee_Cde2 = pnd_Cpr_Key2__R_Field_5.newFieldInGroup("pnd_Cpr_Key2_Pnd_Payee_Cde2", "#PAYEE-CDE2", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Save_Cntrct_Payee", "#SAVE-CNTRCT-PAYEE", FieldType.STRING, 10);

        pnd_Save_Cntrct_Payee__R_Field_6 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Payee__R_Field_6", "REDEFINE", pnd_Save_Cntrct_Payee);
        pnd_Save_Cntrct_Payee_Pnd_Save_Cntrct = pnd_Save_Cntrct_Payee__R_Field_6.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Cntrct", "#SAVE-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Save_Cntrct_Payee_Pnd_Save_Payee = pnd_Save_Cntrct_Payee__R_Field_6.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Payee", "#SAVE-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Last_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Last_Cntrct_Payee", "#LAST-CNTRCT-PAYEE", FieldType.STRING, 10);

        pnd_Last_Cntrct_Payee__R_Field_7 = localVariables.newGroupInRecord("pnd_Last_Cntrct_Payee__R_Field_7", "REDEFINE", pnd_Last_Cntrct_Payee);
        pnd_Last_Cntrct_Payee_Pnd_Last_Cntrct = pnd_Last_Cntrct_Payee__R_Field_7.newFieldInGroup("pnd_Last_Cntrct_Payee_Pnd_Last_Cntrct", "#LAST-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Last_Cntrct_Payee_Pnd_Last_Payee = pnd_Last_Cntrct_Payee__R_Field_7.newFieldInGroup("pnd_Last_Cntrct_Payee_Pnd_Last_Payee", "#LAST-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_S_Invrse_Dte = localVariables.newFieldInRecord("pnd_S_Invrse_Dte", "#S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Seq_Nbr = localVariables.newFieldInRecord("pnd_Seq_Nbr", "#SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Last_Seq_Nbr = localVariables.newFieldInRecord("pnd_Last_Seq_Nbr", "#LAST-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Per_Ded = localVariables.newFieldInRecord("pnd_Tot_Per_Ded", "#TOT-PER-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_8", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_8.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ddctn_Bfre_Key = localVariables.newFieldInRecord("pnd_Ddctn_Bfre_Key", "#DDCTN-BFRE-KEY", FieldType.STRING, 26);

        pnd_Ddctn_Bfre_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Ddctn_Bfre_Key__R_Field_9", "REDEFINE", pnd_Ddctn_Bfre_Key);
        pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id = pnd_Ddctn_Bfre_Key__R_Field_9.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Bfre_Imge_Id", "#BFRE-IMGE-ID", FieldType.STRING, 
            1);
        pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Ddctn_Bfre_Key__R_Field_9.newFieldInGroup("pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_cpr.reset();
        vw_cpr2.reset();
        vw_iaa_Deduction.reset();
        vw_iaa_Ddctn_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Ddctn_Bfre_Key.setInitialValue("1");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapgrp() throws Exception
    {
        super("Iaapgrp");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *FORMAT(1) PS=0 LS=133                                                                                                                                        //Natural: FORMAT PS = 66 LS = 133;//Natural: FORMAT ( 1 ) PS = 00 LS = 133;//Natural: FORMAT ( 2 ) PS = 00 LS = 133 ZP = OFF
        //* *FORMAT(2) PS=0 LS=133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Check_Dte.compute(new ComputeParameters(false, pnd_Check_Dte), pnd_Date.val());                                                                           //Natural: ASSIGN #CHECK-DTE := VAL ( #DATE )
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Todays_Dte.compute(new ComputeParameters(false, pnd_Todays_Dte), pnd_Date.val());                                                                         //Natural: ASSIGN #TODAYS-DTE := VAL ( #DATE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  #CHECK-DTE := 20110101  /* TEST ONLY. DON'T MOVE TO PRODUCTION.
        //* *PERFORM DELETE-TRANS /* TEST ONLY IF LOAD HAS TO BE BACKED OUT
        //* *STOP
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-REC
        while (condition(getWorkFiles().read(1, pnd_Input_Rec)))
        {
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Input_Rec_Pnd_Ddctn_Strt_Dte.notEquals(pnd_Check_Dte)))                                                                                     //Natural: IF #DDCTN-STRT-DTE NE #CHECK-DTE
            {
                getReports().write(0, "ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR");                                                                           //Natural: WRITE 'ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "ERROR IN INPUT FILE - CHECK DATE NOT EQUAL TO START DATE.");                                                                       //Natural: WRITE 'ERROR IN INPUT FILE - CHECK DATE NOT EQUAL TO START DATE.'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "TERMINATING JOB.");                                                                                                                //Natural: WRITE 'TERMINATING JOB.'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RECORD
            sub_Create_Trans_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Input_Rec_Pnd_Cntrct_Payee.notEquals(pnd_Save_Cntrct_Payee)))                                                                               //Natural: IF #CNTRCT-PAYEE NE #SAVE-CNTRCT-PAYEE
            {
                if (condition(pnd_First.getBoolean()))                                                                                                                    //Natural: IF #FIRST
                {
                    pnd_First.reset();                                                                                                                                    //Natural: RESET #FIRST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM RESEQUENCE-DEDUCTIONS
                    sub_Resequence_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PIN
                sub_Get_Pin();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DELETE-100-300-710-DEDS
                sub_Delete_100_300_710_Deds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Cntrct_Payee.setValue(pnd_Input_Rec_Pnd_Cntrct_Payee);                                                                                           //Natural: ASSIGN #SAVE-CNTRCT-PAYEE := #CNTRCT-PAYEE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-DDCTN-RECORD
            sub_Create_Ddctn_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Last_Cntrct_Payee.setValue(pnd_Input_Rec_Pnd_Cntrct_Payee);                                                                                               //Natural: ASSIGN #LAST-CNTRCT-PAYEE := #CNTRCT-PAYEE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
        pnd_Save_Cntrct_Payee.setValue(pnd_Last_Cntrct_Payee);                                                                                                            //Natural: ASSIGN #SAVE-CNTRCT-PAYEE := #LAST-CNTRCT-PAYEE
                                                                                                                                                                          //Natural: PERFORM RESEQUENCE-DEDUCTIONS
        sub_Resequence_Deductions();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, ReportOption.NOTITLE,"Total Deduction Records Processed:",pnd_Rec_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Total Periodic Deduction Amount  :",pnd_Tot_Per_Ded,  //Natural: WRITE ( 1 ) 'Total Deduction Records Processed:' #REC-CNT ( EM = Z,ZZZ,ZZ9 ) / 'Total Periodic Deduction Amount  :' #TOT-PER-DED ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *FETCH 'IAAP999'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PIN
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-100-300-710-DEDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DDCTN-RECORD
        //* *IAA-DEDUCTION.DDCTN-FINAL-DTE := 99999999
        //*  'DDCTN ID' IAA-DEDUCTION.DDCTN-ID-NBR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESEQUENCE-DEDUCTIONS
        //*    'DDCTN ID' IAA-DEDUCTION.DDCTN-ID-NBR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RECORD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-TRANS
        //* ***********************************************************************
    }
    private void sub_Get_Pin() throws Exception                                                                                                                           //Natural: GET-PIN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cpr_Key_Pnd_Ppcn_Nbr.setValue(pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr);                                                                                             //Natural: ASSIGN #PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Cpr_Key_Pnd_Payee_Cde.setValue(pnd_Input_Rec_Pnd_Cntrct_Payee_Cde);                                                                                           //Natural: ASSIGN #PAYEE-CDE := #CNTRCT-PAYEE-CDE
        vw_cpr.startDatabaseFind                                                                                                                                          //Natural: FIND CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cpr.readNextRow("FIND01")))
        {
            vw_cpr.setIfNotFoundControlFlag(false);
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Delete_100_300_710_Deds() throws Exception                                                                                                           //Natural: DELETE-100-300-710-DEDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Idx.reset();                                                                                                                                                  //Natural: RESET #IDX #DDCTN-CDES ( * ) #DDCTN-SEQS ( * ) #LAST-SEQ-NBR #OLD-SEQ
        pnd_Ddctn_Cdes.getValue("*").reset();
        pnd_Ddctn_Seqs.getValue("*").reset();
        pnd_Last_Seq_Nbr.reset();
        pnd_Old_Seq.reset();
        vw_iaa_Deduction.startDatabaseRead                                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #CPR-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        R1:
        while (condition(vw_iaa_Deduction.readNextRow("R1")))
        {
            if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.notEquals(pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr) || iaa_Deduction_Ddctn_Payee_Cde.notEquals(pnd_Input_Rec_Pnd_Cntrct_Payee_Cde))) //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR NE #CNTRCT-PPCN-NBR OR IAA-DEDUCTION.DDCTN-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Idx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #IDX
            pnd_Ddctn_Cdes.getValue(pnd_Idx).setValue(iaa_Deduction_Ddctn_Cde);                                                                                           //Natural: ASSIGN #DDCTN-CDES ( #IDX ) := IAA-DEDUCTION.DDCTN-CDE
            pnd_Ddctn_Seqs.getValue(pnd_Idx).setValue(iaa_Deduction_Ddctn_Seq_Nbr);                                                                                       //Natural: ASSIGN #DDCTN-SEQS ( #IDX ) := IAA-DEDUCTION.DDCTN-SEQ-NBR
            if (condition(pnd_Last_Seq_Nbr.less(iaa_Deduction_Ddctn_Seq_Nbr)))                                                                                            //Natural: IF #LAST-SEQ-NBR LT IAA-DEDUCTION.DDCTN-SEQ-NBR
            {
                pnd_Last_Seq_Nbr.setValue(pnd_Ddctn_Seqs.getValue(pnd_Idx));                                                                                              //Natural: ASSIGN #LAST-SEQ-NBR := #DDCTN-SEQS ( #IDX )
            }                                                                                                                                                             //Natural: END-IF
            vw_iaa_Ddctn_Trans.setValuesByName(vw_iaa_Deduction);                                                                                                         //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
            iaa_Ddctn_Trans_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                        //Natural: ASSIGN IAA-DDCTN-TRANS.LST-TRANS-DTE := #TRANS-DTE
            iaa_Ddctn_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Ddctn_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                            //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE := #TRANS-DTE
            iaa_Ddctn_Trans_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                                      //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE := #CHECK-DTE
            iaa_Ddctn_Trans_Bfre_Imge_Id.setValue("1");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID := '1'
            iaa_Ddctn_Trans_Aftr_Imge_Id.setValue(" ");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID := ' '
            vw_iaa_Ddctn_Trans.insertDBRow();                                                                                                                             //Natural: STORE IAA-DDCTN-TRANS
            if (condition(iaa_Deduction_Ddctn_Cde.equals("100") || iaa_Deduction_Ddctn_Cde.equals("300") || iaa_Deduction_Ddctn_Cde.equals("710")))                       //Natural: IF DDCTN-CDE = '100' OR = '300' OR = '710'
            {
                G1:                                                                                                                                                       //Natural: GET IAA-DEDUCTION *ISN ( R1. )
                vw_iaa_Deduction.readByID(vw_iaa_Deduction.getAstISN("R1"), "G1");
                vw_iaa_Deduction.deleteDBRow("G1");                                                                                                                       //Natural: DELETE ( G1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_Ddctn_Record() throws Exception                                                                                                               //Natural: CREATE-DDCTN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        iaa_Deduction_Ddctn_Payee.reset();                                                                                                                                //Natural: RESET IAA-DEDUCTION.DDCTN-PAYEE IAA-DEDUCTION.DDCTN-YTD-AMT IAA-DEDUCTION.DDCTN-TOT-AMT #SEQ-NBR
        iaa_Deduction_Ddctn_Ytd_Amt.reset();
        iaa_Deduction_Ddctn_Tot_Amt.reset();
        pnd_Seq_Nbr.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #IDX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Idx)); pnd_I.nadd(1))
        {
            if (condition(pnd_Ddctn_Cdes.getValue(pnd_I).equals(pnd_Input_Rec_Pnd_Ddctn_Cde)))                                                                            //Natural: IF #DDCTN-CDES ( #I ) = #DDCTN-CDE
            {
                pnd_Seq_Nbr.setValue(pnd_Ddctn_Seqs.getValue(pnd_I));                                                                                                     //Natural: ASSIGN #SEQ-NBR := #DDCTN-SEQS ( #I )
                pnd_Ddctn_Cdes.getValue(pnd_I).setValue("999");                                                                                                           //Natural: ASSIGN #DDCTN-CDES ( #I ) := '999'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Seq_Nbr.equals(getZero())))                                                                                                                     //Natural: IF #SEQ-NBR = 0
        {
            pnd_Last_Seq_Nbr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LAST-SEQ-NBR
            pnd_Seq_Nbr.setValue(pnd_Last_Seq_Nbr);                                                                                                                       //Natural: ASSIGN #SEQ-NBR := #LAST-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Deduction_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                              //Natural: ASSIGN IAA-DEDUCTION.LST-TRANS-DTE := #TRANS-DTE
        iaa_Deduction_Ddctn_Ppcn_Nbr.setValue(pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr);                                                                                         //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PPCN-NBR := #CNTRCT-PPCN-NBR
        iaa_Deduction_Ddctn_Payee_Cde.setValue(pnd_Input_Rec_Pnd_Cntrct_Payee_Cde);                                                                                       //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        iaa_Deduction_Ddctn_Cde.setValue(pnd_Input_Rec_Pnd_Ddctn_Cde);                                                                                                    //Natural: ASSIGN IAA-DEDUCTION.DDCTN-CDE := #DDCTN-CDE
        iaa_Deduction_Ddctn_Seq_Nbr.setValue(pnd_Seq_Nbr);                                                                                                                //Natural: ASSIGN IAA-DEDUCTION.DDCTN-SEQ-NBR := #SEQ-NBR
        iaa_Deduction_Ddctn_Id_Nbr.setValue(cpr_Cpr_Id_Nbr);                                                                                                              //Natural: ASSIGN IAA-DEDUCTION.DDCTN-ID-NBR := CPR-ID-NBR
        iaa_Deduction_Ddctn_Per_Amt.setValue(pnd_Input_Rec_Pnd_Ddctn_Per_Amt);                                                                                            //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PER-AMT := #DDCTN-PER-AMT
        iaa_Deduction_Ddctn_Tot_Amt.setValue(pnd_Input_Rec_Pnd_Ddctn_Tot_Amt);                                                                                            //Natural: ASSIGN IAA-DEDUCTION.DDCTN-TOT-AMT := #DDCTN-TOT-AMT
        iaa_Deduction_Ddctn_Pd_To_Dte.setValue(pnd_Input_Rec_Pnd_Ddctn_Pd_To_Dte_Amt);                                                                                    //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PD-TO-DTE := #DDCTN-PD-TO-DTE-AMT
        iaa_Deduction_Ddctn_Strt_Dte.setValue(pnd_Input_Rec_Pnd_Ddctn_Strt_Dte);                                                                                          //Natural: ASSIGN IAA-DEDUCTION.DDCTN-STRT-DTE := #DDCTN-STRT-DTE
        iaa_Deduction_Ddctn_Stp_Dte.setValue(0);                                                                                                                          //Natural: ASSIGN IAA-DEDUCTION.DDCTN-STP-DTE := 0
        iaa_Deduction_Ddctn_Final_Dte.setValue(pnd_Input_Rec_Pnd_Ddctn_Stp_Dte);                                                                                          //Natural: ASSIGN IAA-DEDUCTION.DDCTN-FINAL-DTE := #DDCTN-STP-DTE
        iaa_Deduction_Ddctn_Payee.setValue(pnd_Input_Rec_Pnd_Ppg_Cde, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #PPG-CDE TO IAA-DEDUCTION.DDCTN-PAYEE
        DbsUtil.examine(new ExamineSource(iaa_Deduction_Ddctn_Payee,true), new ExamineSearch(" ", true), new ExamineReplace("0"));                                        //Natural: EXAMINE FULL IAA-DEDUCTION.DDCTN-PAYEE FOR FULL ' ' REPLACE '0'
        vw_iaa_Deduction.insertDBRow();                                                                                                                                   //Natural: STORE IAA-DEDUCTION
        pnd_Tot_Per_Ded.nadd(iaa_Deduction_Ddctn_Per_Amt);                                                                                                                //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #TOT-PER-DED
        //*  IF CPR.RLLVR-CNTRCT-NBR = ' '                         /* JB01 9 LINES
        //*  JB01 9 LINES
        if (condition(pnd_Input_Rec_Pnd_Cntrct_Payee_Cde.greater(1)))                                                                                                     //Natural: IF #CNTRCT-PAYEE-CDE > 1
        {
            pnd_Cpr_Key2.setValue(pnd_Cpr_Key);                                                                                                                           //Natural: ASSIGN #CPR-KEY2 := #CPR-KEY
            pnd_Cpr_Key2_Pnd_Payee_Cde2.setValue(1);                                                                                                                      //Natural: ASSIGN #PAYEE-CDE2 := 1
            vw_cpr2.startDatabaseFind                                                                                                                                     //Natural: FIND CPR2 WITH CNTRCT-PAYEE-KEY = #CPR-KEY2
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cpr_Key2, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_cpr2.readNextRow("FIND02")))
            {
                vw_cpr2.setIfNotFoundControlFlag(false);
                getReports().write(0, "PAYEE 01 HARVARD KEY USED","=",pnd_Cpr_Key,"=",cpr2_Rllvr_Cntrct_Nbr);                                                             //Natural: WRITE 'PAYEE 01 HARVARD KEY USED' '=' #CPR-KEY '=' CPR2.RLLVR-CNTRCT-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                cpr_Rllvr_Cntrct_Nbr.setValue(cpr2_Rllvr_Cntrct_Nbr);                                                                                                     //Natural: ASSIGN CPR.RLLVR-CNTRCT-NBR := CPR2.RLLVR-CNTRCT-NBR
                cpr_Prtcpnt_Tax_Id_Nbr.setValue(cpr2_Prtcpnt_Tax_Id_Nbr);                                                                                                 //Natural: ASSIGN CPR.PRTCPNT-TAX-ID-NBR := CPR2.PRTCPNT-TAX-ID-NBR
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  JB01 CHANGE REPORT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "/Contract",                                                                                                                              //Natural: DISPLAY ( 1 ) '/Contract' IAA-DEDUCTION.DDCTN-PPCN-NBR '/Payee' IAA-DEDUCTION.DDCTN-PAYEE-CDE '/Hrvrd Id' CPR.RLLVR-CNTRCT-NBR '/SSN     ' CPR.PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 'Ded/Cde' IAA-DEDUCTION.DDCTN-CDE '/Seq#' IAA-DEDUCTION.DDCTN-SEQ-NBR 'Ded/Py' IAA-DEDUCTION.DDCTN-PAYEE '/Per Amt' IAA-DEDUCTION.DDCTN-PER-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'Tot/Ded Amt' IAA-DEDUCTION.DDCTN-TOT-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'Ded Pd/To Dte' IAA-DEDUCTION.DDCTN-PD-TO-DTE ( EM = Z,ZZZ,ZZ9.99 ) '/Strt Dte' IAA-DEDUCTION.DDCTN-STRT-DTE '/Stop Dte' IAA-DEDUCTION.DDCTN-STP-DTE ( ZP = OFF ) '/Final Dte' IAA-DEDUCTION.DDCTN-FINAL-DTE ( ZP = OFF )
        		iaa_Deduction_Ddctn_Ppcn_Nbr,"/Payee",
        		iaa_Deduction_Ddctn_Payee_Cde,"/Hrvrd Id",
        		cpr_Rllvr_Cntrct_Nbr,"/SSN     ",
        		cpr_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"Ded/Cde",
        		iaa_Deduction_Ddctn_Cde,"/Seq#",
        		iaa_Deduction_Ddctn_Seq_Nbr,"Ded/Py",
        		iaa_Deduction_Ddctn_Payee,"/Per Amt",
        		iaa_Deduction_Ddctn_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Tot/Ded Amt",
        		iaa_Deduction_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Ded Pd/To Dte",
        		iaa_Deduction_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Strt Dte",
        		iaa_Deduction_Ddctn_Strt_Dte,"/Stop Dte",
        		iaa_Deduction_Ddctn_Stp_Dte, new ReportZeroPrint (false),"/Final Dte",
        		iaa_Deduction_Ddctn_Final_Dte, new ReportZeroPrint (false));
        if (Global.isEscape()) return;
        pnd_Rec_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-CNT
    }
    private void sub_Resequence_Deductions() throws Exception                                                                                                             //Natural: RESEQUENCE-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Old_Seq.reset();                                                                                                                                              //Natural: RESET #OLD-SEQ
        pnd_Cpr_Key_Pnd_Ppcn_Nbr.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Cntrct);                                                                                         //Natural: ASSIGN #PPCN-NBR := #SAVE-CNTRCT
        pnd_Cpr_Key_Pnd_Payee_Cde.setValue(pnd_Save_Cntrct_Payee_Pnd_Save_Payee);                                                                                         //Natural: ASSIGN #PAYEE-CDE := #SAVE-PAYEE
        vw_iaa_Deduction.startDatabaseRead                                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #CPR-KEY
        (
        "RX",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        RX:
        while (condition(vw_iaa_Deduction.readNextRow("RX")))
        {
            if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.notEquals(pnd_Save_Cntrct_Payee_Pnd_Save_Cntrct) || iaa_Deduction_Ddctn_Payee_Cde.notEquals(pnd_Save_Cntrct_Payee_Pnd_Save_Payee))) //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR NE #SAVE-CNTRCT OR IAA-DEDUCTION.DDCTN-PAYEE-CDE NE #SAVE-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            GX:                                                                                                                                                           //Natural: GET IAA-DEDUCTION *ISN ( RX. )
            vw_iaa_Deduction.readByID(vw_iaa_Deduction.getAstISN("RX"), "GX");
            pnd_Old_Seq.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #OLD-SEQ
            iaa_Deduction_Ddctn_Seq_Nbr.setValue(pnd_Old_Seq);                                                                                                            //Natural: ASSIGN IAA-DEDUCTION.DDCTN-SEQ-NBR := #OLD-SEQ
            vw_iaa_Deduction.updateDBRow("GX");                                                                                                                           //Natural: UPDATE ( GX. )
            vw_iaa_Ddctn_Trans.setValuesByName(vw_iaa_Deduction);                                                                                                         //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
            iaa_Ddctn_Trans_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                        //Natural: ASSIGN IAA-DDCTN-TRANS.LST-TRANS-DTE := #TRANS-DTE
            iaa_Ddctn_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Ddctn_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                            //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE := #TRANS-DTE
            iaa_Ddctn_Trans_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                                      //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE := #CHECK-DTE
            iaa_Ddctn_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID := ' '
            iaa_Ddctn_Trans_Aftr_Imge_Id.setValue("2");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID := '2'
            //*  JB01 CHANGTE REPORT
            vw_iaa_Ddctn_Trans.insertDBRow();                                                                                                                             //Natural: STORE IAA-DDCTN-TRANS
            getReports().display(2, "/Contract",                                                                                                                          //Natural: DISPLAY ( 2 ) '/Contract' IAA-DEDUCTION.DDCTN-PPCN-NBR '/Payee' IAA-DEDUCTION.DDCTN-PAYEE-CDE '/Hrvrd Id' CPR.RLLVR-CNTRCT-NBR '/SSN     ' CPR.PRTCPNT-TAX-ID-NBR ( EM = 999-99-9999 ) 'Ded/Cde' IAA-DEDUCTION.DDCTN-CDE '/Seq#' IAA-DEDUCTION.DDCTN-SEQ-NBR 'Ded/Py' IAA-DEDUCTION.DDCTN-PAYEE '/Per Amt' IAA-DEDUCTION.DDCTN-PER-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'Tot/Ded Amt' IAA-DEDUCTION.DDCTN-TOT-AMT ( EM = Z,ZZZ,ZZ9.99 ) 'Ded Pd/To Dte' IAA-DEDUCTION.DDCTN-PD-TO-DTE ( EM = Z,ZZZ,ZZ9.99 ) '/Strt Dte' IAA-DEDUCTION.DDCTN-STRT-DTE '/Stop Dte' IAA-DEDUCTION.DDCTN-STP-DTE ( ZP = OFF ) '/Final Dte' IAA-DEDUCTION.DDCTN-FINAL-DTE ( ZP = OFF )
            		iaa_Deduction_Ddctn_Ppcn_Nbr,"/Payee",
            		iaa_Deduction_Ddctn_Payee_Cde,"/Hrvrd Id",
            		cpr_Rllvr_Cntrct_Nbr,"/SSN     ",
            		cpr_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"Ded/Cde",
            		iaa_Deduction_Ddctn_Cde,"/Seq#",
            		iaa_Deduction_Ddctn_Seq_Nbr,"Ded/Py",
            		iaa_Deduction_Ddctn_Payee,"/Per Amt",
            		iaa_Deduction_Ddctn_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Tot/Ded Amt",
            		iaa_Deduction_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Ded Pd/To Dte",
            		iaa_Deduction_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Strt Dte",
            		iaa_Deduction_Ddctn_Strt_Dte,"/Stop Dte",
            		iaa_Deduction_Ddctn_Stp_Dte, new ReportZeroPrint (false),"/Final Dte",
            		iaa_Deduction_Ddctn_Final_Dte, new ReportZeroPrint (false));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RX"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RX"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Create_Trans_Record() throws Exception                                                                                                               //Natural: CREATE-TRANS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                         //Natural: ASSIGN #TRANS-DTE := *TIMX
        pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                                    //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
        if (condition(pnd_Invrse_Dte.equals(pnd_S_Invrse_Dte)))                                                                                                           //Natural: IF #INVRSE-DTE = #S-INVRSE-DTE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                 //Natural: ASSIGN #TRANS-DTE := *TIMX
                pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                            //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
                if (condition(pnd_Invrse_Dte.notEquals(pnd_S_Invrse_Dte)))                                                                                                //Natural: IF #INVRSE-DTE NE #S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Invrse_Dte.setValue(pnd_Invrse_Dte);                                                                                                                        //Natural: ASSIGN #S-INVRSE-DTE := #INVRSE-DTE
        iaa_Trans_Rcrd_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
        iaa_Trans_Rcrd_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        iaa_Trans_Rcrd_Lst_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(pnd_Input_Rec_Pnd_Cntrct_Ppcn_Nbr);                                                                                        //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #CNTRCT-PPCN-NBR
        iaa_Trans_Rcrd_Trans_Payee_Cde.setValue(pnd_Input_Rec_Pnd_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        iaa_Trans_Rcrd_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #CHECK-DTE
        iaa_Trans_Rcrd_Trans_Actvty_Cde.setValue("A");                                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        iaa_Trans_Rcrd_Trans_Todays_Dte.setValue(pnd_Todays_Dte);                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DTE
        iaa_Trans_Rcrd_Trans_User_Area.setValue("BATCH");                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        iaa_Trans_Rcrd_Trans_User_Id.setValue(Global.getPROGRAM());                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := *PROGRAM
        iaa_Trans_Rcrd_Trans_Cde.setValue(906);                                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 906
        vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
    }
    private void sub_Delete_Trans() throws Exception                                                                                                                      //Natural: DELETE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                               //Natural: ASSIGN #TRANS-CHECK-DTE := #CHECK-DTE
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr.setValue("W0766193");                                                                                                   //Natural: ASSIGN #TRANS-PPCN-NBR := 'W0766193'
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "RZ",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        RZ:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("RZ")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.greater("W0799999")))                                                                                             //Natural: IF IAA-TRANS-RCRD.TRANS-PPCN-NBR GT 'W0799999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Check_Dte.equals(pnd_Check_Dte) && iaa_Trans_Rcrd_Trans_User_Id.equals("IAAPGRP")))                                        //Natural: IF TRANS-CHECK-DTE = #CHECK-DTE AND TRANS-USER-ID = 'IAAPGRP'
            {
                GZ:                                                                                                                                                       //Natural: GET IAA-TRANS-RCRD *ISN ( RZ. )
                vw_iaa_Trans_Rcrd.readByID(vw_iaa_Trans_Rcrd.getAstISN("RZ"), "GZ");
                vw_iaa_Trans_Rcrd.deleteDBRow("GZ");                                                                                                                      //Natural: DELETE ( GZ. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Ddctn_Bfre_Key_Pnd_Ddctn_Ppcn_Nbr.setValue("W0766193");                                                                                                       //Natural: ASSIGN #DDCTN-PPCN-NBR := 'W0766193'
        vw_iaa_Ddctn_Trans.startDatabaseRead                                                                                                                              //Natural: READ IAA-DDCTN-TRANS BY DDCTN-BFRE-KEY STARTING FROM #DDCTN-BFRE-KEY
        (
        "RY",
        new Wc[] { new Wc("DDCTN_BFRE_KEY", ">=", pnd_Ddctn_Bfre_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DDCTN_BFRE_KEY", "ASC") }
        );
        RY:
        while (condition(vw_iaa_Ddctn_Trans.readNextRow("RY")))
        {
            if (condition(!(iaa_Ddctn_Trans_Trans_Check_Dte.equals(pnd_Check_Dte))))                                                                                      //Natural: ACCEPT IF IAA-DDCTN-TRANS.TRANS-CHECK-DTE = #CHECK-DTE
            {
                continue;
            }
            if (condition(iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr.greater("W07999999")))                                                                                           //Natural: IF DDCTN-PPCN-NBR GT 'W07999999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            GY:                                                                                                                                                           //Natural: GET IAA-DDCTN-TRANS *ISN ( RY. )
            vw_iaa_Ddctn_Trans.readByID(vw_iaa_Ddctn_Trans.getAstISN("RY"), "GY");
            vw_iaa_Ddctn_Trans.deleteDBRow("GY");                                                                                                                         //Natural: DELETE ( GY. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(40),"DEDUCTION LOAD UPDATE REPORT",NEWLINE,Global.getDATX(),         //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 40X 'DEDUCTION LOAD UPDATE REPORT' / *DATX ( EM = MM/DD/YYYY ) 100X 'Page' *PAGE-NUMBER ( 1 )
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(100),"Page",getReports().getPageNumberDbs(1));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(40),"DEDUCTION LOAD MISCELANEOUS REPORT",NEWLINE,Global.getDATX(),   //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 40X 'DEDUCTION LOAD MISCELANEOUS REPORT' / *DATX ( EM = MM/DD/YYYY ) 100X 'Page' *PAGE-NUMBER ( 2 )
                        new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(100),"Page",getReports().getPageNumberDbs(2));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=66 LS=133");
        Global.format(1, "PS=00 LS=133");
        Global.format(2, "PS=00 LS=133 ZP=OFF");

        getReports().setDisplayColumns(1, "/Contract",
        		iaa_Deduction_Ddctn_Ppcn_Nbr,"/Payee",
        		iaa_Deduction_Ddctn_Payee_Cde,"/Hrvrd Id",
        		cpr_Rllvr_Cntrct_Nbr,"/SSN     ",
        		cpr_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"Ded/Cde",
        		iaa_Deduction_Ddctn_Cde,"/Seq#",
        		iaa_Deduction_Ddctn_Seq_Nbr,"Ded/Py",
        		iaa_Deduction_Ddctn_Payee,"/Per Amt",
        		iaa_Deduction_Ddctn_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Tot/Ded Amt",
        		iaa_Deduction_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Ded Pd/To Dte",
        		iaa_Deduction_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Strt Dte",
        		iaa_Deduction_Ddctn_Strt_Dte,"/Stop Dte",
        		iaa_Deduction_Ddctn_Stp_Dte, new ReportZeroPrint (false),"/Final Dte",
        		iaa_Deduction_Ddctn_Final_Dte, new ReportZeroPrint (false));
        getReports().setDisplayColumns(2, "/Contract",
        		iaa_Deduction_Ddctn_Ppcn_Nbr,"/Payee",
        		iaa_Deduction_Ddctn_Payee_Cde,"/Hrvrd Id",
        		cpr_Rllvr_Cntrct_Nbr,"/SSN     ",
        		cpr_Prtcpnt_Tax_Id_Nbr, new ReportEditMask ("999-99-9999"),"Ded/Cde",
        		iaa_Deduction_Ddctn_Cde,"/Seq#",
        		iaa_Deduction_Ddctn_Seq_Nbr,"Ded/Py",
        		iaa_Deduction_Ddctn_Payee,"/Per Amt",
        		iaa_Deduction_Ddctn_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Tot/Ded Amt",
        		iaa_Deduction_Ddctn_Tot_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"Ded Pd/To Dte",
        		iaa_Deduction_Ddctn_Pd_To_Dte, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Strt Dte",
        		iaa_Deduction_Ddctn_Strt_Dte,"/Stop Dte",
        		iaa_Deduction_Ddctn_Stp_Dte, new ReportZeroPrint (false),"/Final Dte",
        		iaa_Deduction_Ddctn_Final_Dte, new ReportZeroPrint (false));
    }
}
