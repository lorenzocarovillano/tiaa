/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:24 PM
**        * FROM NATURAL PROGRAM : Iaapqdro
************************************************************
**        * FILE NAME            : Iaapqdro.java
**        * CLASS NAME           : Iaapqdro
**        * INSTANCE NAME        : Iaapqdro
************************************************************
**********************************************************************
* PROGRAM:  IAAPQDRO                                                 *
* DATE   :  04/07/2015                                               *
* PURPOSE:  CREATE A MONTHLY EXTRACT OF QDRO CONTRACTS FOR COMMUNITY
*           PROPERTY PROCESSING. THE FILE WILL BE SENT TO SERVER FOR
*           PROCESSING. THIS WILL BE PART OF THE MONTHLY PAYMENT CYCLE.
* HISTORY:
*
* 04/07/2015 - JT - ORIGINAL CODE
* 08/20/2015 - JT - USE MDMN100A FOR BATCH. SC ON 08/15.
* 04/2017      OS - PIN EXPANSION CHANGES MARKED 082017.
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapqdro extends BLNatBase
{
    // Data Areas
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Opt_A;
    private DbsField pnd_Input_Pnd_Orgn_Cde;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Orgn_Cde_A;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Issue_Yyyy;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Iss_St;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input__Filler1;
    private DbsField pnd_Input_Pnd_Iss;
    private DbsField pnd_Input_Pnd_F3a;
    private DbsField pnd_Input_Pnd_First_Ann_Dob_Dte;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Dob_Yyyymm;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_1st_Dob_8;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob_Dte;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_F8;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_16;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_17;
    private DbsField pnd_Input_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_18;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_19;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;

    private DbsGroup pnd_Input__R_Field_20;
    private DbsField pnd_Input__Filler2;
    private DbsField pnd_Input_Pnd_Res;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_Trmnte_Rsn;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_21;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Cde;
    private DbsField pnd_Input_Pnd_Spirt_Amt;
    private DbsField pnd_Input_Pnd_Spirt_Srce;
    private DbsField pnd_Input_Pnd_Spirt_Arr_Dte;
    private DbsField pnd_Input_Pnd_Spirt_Prcss_Dte;
    private DbsField pnd_Input_Pnd_Fed_Tax_Amt;
    private DbsField pnd_Input_Pnd_State_Cde;
    private DbsField pnd_Input_Pnd_State_Tax_Amt;
    private DbsField pnd_Input_Pnd_Local_Cde;
    private DbsField pnd_Input_Pnd_Local_Tax_Amt;
    private DbsField pnd_Input_Pnd_Lst_Chnge_Dte;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Input_Pnd_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;
    private DbsField pnd_Ws_Ticker;
    private DbsField pnd_Fnd_Idntfr;
    private DbsField pnd_S_Plan_Nmbr;
    private DbsField pnd_S_Cpr_Id_Nbr;
    private DbsField pnd_S_Ppg_Cde;
    private DbsField pnd_S_Orgn_Cde;

    private DbsGroup pnd_S_Orgn_Cde__R_Field_22;
    private DbsField pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A;
    private DbsField pnd_S_Issue_Dte;
    private DbsField pnd_S_Tax_Exmpt_Ind;
    private DbsField pnd_S_Optn;
    private DbsField pnd_Per_Ivc;
    private DbsField pnd_Per_Tot;
    private DbsField pnd_Per_Div;
    private DbsField pnd_Fund_Tot;
    private DbsField pnd_Tot_Pmt;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Bypass;
    private DbsField pnd_Cpr_Bypass;
    private DbsField pnd_Write_It;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_In_Aian026;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_23;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_24;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_25;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Out_Aian026;

    private DbsGroup aian026_Pnd_Out;
    private DbsField aian026_Pnd_Rtrn_Cde_Pgm;

    private DbsGroup aian026__R_Field_26;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_27;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Newf1;
    private DbsField aian026_Pnd_Newf2;

    private DataAccessProgramView vw_pnd_Ext_Fund;
    private DbsField pnd_Ext_Fund_Nec_Table_Cde;
    private DbsField pnd_Ext_Fund_Nec_Ticker_Symbol;
    private DbsField pnd_Ext_Fund_Nec_Investment_Grouping_Id;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_28;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol;
    private DbsField pnd_Naz_Tbl_Super1;

    private DbsGroup pnd_Naz_Tbl_Super1__R_Field_29;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Tc;
    private DbsField pnd_S_1st_Dod;
    private DbsField pnd_S_2nd_Dod;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_Type_Call;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_W_Tckr;
    private DbsField pnd_Tickers;
    private DbsField pnd_Fund_Amt;
    private DbsField pnd_Frqncy;
    private DbsField pnd_Output;

    private DbsGroup pnd_Output__R_Field_30;
    private DbsField pnd_Output_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Output_Pnd_O1;
    private DbsField pnd_Output_Pnd_S_Mode;
    private DbsField pnd_Output_Pnd_O2;
    private DbsField pnd_Output_Pnd_Cntrct_Per_Amt;
    private DbsField pnd_Output_Pnd_O3;
    private DbsField pnd_Output_Pnd_Cntrct_Div_Amt;
    private DbsField pnd_Output_Pnd_O4;
    private DbsField pnd_Output_Pnd_Tot_Amt;
    private DbsField pnd_Output_Pnd_O5;
    private DbsField pnd_Output_Pnd_Ivc_Amt;
    private DbsField pnd_Output_Pnd_O6;
    private DbsField pnd_Output_Pnd_Dpi_Amt;
    private DbsField pnd_Output_Pnd_O7;
    private DbsField pnd_Output_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Pnd_O8;
    private DbsField pnd_Output_Pnd_Ps_Typ;

    private DbsGroup pnd_Output__R_Field_31;
    private DbsField pnd_Output_Pnd_Pymnt_Typ;
    private DbsField pnd_Output_Pnd_Sttlmnt_Typ;
    private DbsField pnd_Output_Pnd_O9;
    private DbsField pnd_Output_Pnd_Crrncy;
    private DbsField pnd_Output_Pnd_10;
    private DbsField pnd_Output_Pnd_Pln_Typ;
    private DbsField pnd_Output_Pnd_11;
    private DbsField pnd_Output_Pnd_Ctznshp;
    private DbsField pnd_Output_Pnd_12;
    private DbsField pnd_Output_Pnd_Rsdncy;
    private DbsField pnd_Output_Pnd_13;
    private DbsField pnd_Output_Pnd_Money_Source;
    private DbsField pnd_Output_Pnd_14;
    private DbsField pnd_Output_Pnd_Rllvr;
    private DbsField pnd_Output_Pnd_15;
    private DbsField pnd_Output_Pnd_Db_Ind;
    private DbsField pnd_Output_Pnd_16;
    private DbsField pnd_Output_Pnd_Dob;
    private DbsField pnd_Output_Pnd_17;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;
    private DbsField pls_Tckr_Symbl;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Optn_Cde);
        pnd_Input_Pnd_Opt_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Opt_A", "#OPT-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Orgn_Cde);
        pnd_Input_Pnd_Orgn_Cde_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde_A", "#ORGN-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Yyyy = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Issue_Yyyy", "#ISSUE-YYYY", FieldType.STRING, 4);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Iss_St = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Iss_St", "#ISS-ST", FieldType.STRING, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Iss_St);
        pnd_Input__Filler1 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Input_Pnd_Iss = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Iss", "#ISS", FieldType.STRING, 2);
        pnd_Input_Pnd_F3a = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F3a", "#F3A", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob_Dte", "#FIRST-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_Dob_Yyyymm = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Dob_Yyyymm", "#DOB-YYYYMM", FieldType.STRING, 6);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_First_Ann_Dob_Dte);
        pnd_Input_Pnd_1st_Dob_8 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_1st_Dob_8", "#1ST-DOB-8", FieldType.STRING, 8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob_Dte", "#SCND-ANN-DOB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_F8 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F8", "#F8", FieldType.STRING, 45);
        pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", "#W1-CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.STRING, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.STRING, 8);

        pnd_Input__R_Field_13 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_14 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_13.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_16 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_16", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);

        pnd_Input__R_Field_17 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_17", "REDEFINE", pnd_Input_Pnd_Summ_Fund_Cde);
        pnd_Input_Fund_Cde = pnd_Input__R_Field_17.newFieldInGroup("pnd_Input_Fund_Cde", "FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_18 = pnd_Input__R_Field_16.newGroupInGroup("pnd_Input__R_Field_18", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_18.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_16.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_19 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_19", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_20 = pnd_Input__R_Field_19.newGroupInGroup("pnd_Input__R_Field_20", "REDEFINE", pnd_Input_Pnd_Rsdncy_Cde);
        pnd_Input__Filler2 = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_Res = pnd_Input__R_Field_20.newFieldInGroup("pnd_Input_Pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Trmnte_Rsn = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Trmnte_Rsn", "#TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cntrct_Cmpny_Cde = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Cmpny_Cde", "#CNTRCT-CMPNY-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_21 = pnd_Input__R_Field_19.newGroupInGroup("pnd_Input__R_Field_21", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_21.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_Spirt_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Cde", "#SPIRT-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Amt", "#SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Input_Pnd_Spirt_Srce = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Srce", "#SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Input_Pnd_Spirt_Arr_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Arr_Dte", "#SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Input_Pnd_Spirt_Prcss_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Spirt_Prcss_Dte", "#SPIRT-PRCSS-DTE", FieldType.NUMERIC, 
            6);
        pnd_Input_Pnd_Fed_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Fed_Tax_Amt", "#FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Input_Pnd_State_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_State_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Local_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Local_Cde", "#LOCAL-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Local_Tax_Amt = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Lst_Chnge_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Lst_Chnge_Dte", "#LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Cpr_Xfr_Term_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Term_Cde", "#CPR-XFR-TERM-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cpr_Lgl_Res_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Lgl_Res_Cde", "#CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_Cpr_Xfr_Iss_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Cpr_Xfr_Iss_Dte", "#CPR-XFR-ISS-DTE", FieldType.DATE);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_19.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_19.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Ticker = localVariables.newFieldInRecord("pnd_Ws_Ticker", "#WS-TICKER", FieldType.STRING, 10);
        pnd_Fnd_Idntfr = localVariables.newFieldInRecord("pnd_Fnd_Idntfr", "#FND-IDNTFR", FieldType.STRING, 5);
        pnd_S_Plan_Nmbr = localVariables.newFieldInRecord("pnd_S_Plan_Nmbr", "#S-PLAN-NMBR", FieldType.STRING, 6);
        pnd_S_Cpr_Id_Nbr = localVariables.newFieldInRecord("pnd_S_Cpr_Id_Nbr", "#S-CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_S_Ppg_Cde = localVariables.newFieldInRecord("pnd_S_Ppg_Cde", "#S-PPG-CDE", FieldType.STRING, 5);
        pnd_S_Orgn_Cde = localVariables.newFieldInRecord("pnd_S_Orgn_Cde", "#S-ORGN-CDE", FieldType.NUMERIC, 2);

        pnd_S_Orgn_Cde__R_Field_22 = localVariables.newGroupInRecord("pnd_S_Orgn_Cde__R_Field_22", "REDEFINE", pnd_S_Orgn_Cde);
        pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A = pnd_S_Orgn_Cde__R_Field_22.newFieldInGroup("pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A", "#S-ORGN-CDE-A", FieldType.STRING, 
            2);
        pnd_S_Issue_Dte = localVariables.newFieldInRecord("pnd_S_Issue_Dte", "#S-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_S_Tax_Exmpt_Ind = localVariables.newFieldInRecord("pnd_S_Tax_Exmpt_Ind", "#S-TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_S_Optn = localVariables.newFieldInRecord("pnd_S_Optn", "#S-OPTN", FieldType.NUMERIC, 2);
        pnd_Per_Ivc = localVariables.newFieldInRecord("pnd_Per_Ivc", "#PER-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Tot = localVariables.newFieldInRecord("pnd_Per_Tot", "#PER-TOT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per_Div = localVariables.newFieldInRecord("pnd_Per_Div", "#PER-DIV", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Fund_Tot = localVariables.newFieldArrayInRecord("pnd_Fund_Tot", "#FUND-TOT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20));
        pnd_Tot_Pmt = localVariables.newFieldInRecord("pnd_Tot_Pmt", "#TOT-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_Cpr_Bypass = localVariables.newFieldInRecord("pnd_Cpr_Bypass", "#CPR-BYPASS", FieldType.BOOLEAN, 1);
        pnd_Write_It = localVariables.newFieldInRecord("pnd_Write_It", "#WRITE-IT", FieldType.BOOLEAN, 1);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_In_Aian026 = aian026.newGroupInGroup("aian026_Pnd_In_Aian026", "#IN-AIAN026");
        aian026_Pnd_Call_Type = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_23 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_23", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_23.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_24 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_24", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_24.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_24.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_24.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_In_Aian026.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_25 = aian026_Pnd_In_Aian026.newGroupInGroup("aian026__R_Field_25", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_25.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_25.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Out_Aian026 = aian026.newGroupInGroup("aian026_Pnd_Out_Aian026", "#OUT-AIAN026");

        aian026_Pnd_Out = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026_Pnd_Out", "#OUT");
        aian026_Pnd_Rtrn_Cde_Pgm = aian026_Pnd_Out.newFieldInGroup("aian026_Pnd_Rtrn_Cde_Pgm", "#RTRN-CDE-PGM", FieldType.STRING, 11);

        aian026__R_Field_26 = aian026_Pnd_Out.newGroupInGroup("aian026__R_Field_26", "REDEFINE", aian026_Pnd_Rtrn_Cde_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_26.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_26.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Out_Aian026.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_27 = aian026_Pnd_Out_Aian026.newGroupInGroup("aian026__R_Field_27", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_27.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Newf1 = aian026.newFieldInGroup("aian026_Pnd_Newf1", "#NEWF1", FieldType.NUMERIC, 2);
        aian026_Pnd_Newf2 = aian026.newFieldInGroup("aian026_Pnd_Newf2", "#NEWF2", FieldType.NUMERIC, 2);

        vw_pnd_Ext_Fund = new DataAccessProgramView(new NameInfo("vw_pnd_Ext_Fund", "#EXT-FUND"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        pnd_Ext_Fund_Nec_Table_Cde = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        pnd_Ext_Fund_Nec_Ticker_Symbol = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        pnd_Ext_Fund_Nec_Investment_Grouping_Id = vw_pnd_Ext_Fund.getRecord().newFieldInGroup("pnd_Ext_Fund_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_pnd_Ext_Fund);

        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_28 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_28", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde = pnd_Nec_Fnd_Super1__R_Field_28.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol = pnd_Nec_Fnd_Super1__R_Field_28.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol", "#NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10);
        pnd_Naz_Tbl_Super1 = localVariables.newFieldInRecord("pnd_Naz_Tbl_Super1", "#NAZ-TBL-SUPER1", FieldType.STRING, 29);

        pnd_Naz_Tbl_Super1__R_Field_29 = localVariables.newGroupInRecord("pnd_Naz_Tbl_Super1__R_Field_29", "REDEFINE", pnd_Naz_Tbl_Super1);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Tbl_Super1__R_Field_29.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", "#NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Tbl_Super1__R_Field_29.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", "#NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Tbl_Super1__R_Field_29.newFieldInGroup("pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", "#NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Tc = localVariables.newFieldInRecord("pnd_Tc", "#TC", FieldType.STRING, 1);
        pnd_S_1st_Dod = localVariables.newFieldInRecord("pnd_S_1st_Dod", "#S-1ST-DOD", FieldType.NUMERIC, 6);
        pnd_S_2nd_Dod = localVariables.newFieldInRecord("pnd_S_2nd_Dod", "#S-2ND-DOD", FieldType.NUMERIC, 6);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_W_Tckr = localVariables.newFieldInRecord("pnd_W_Tckr", "#W-TCKR", FieldType.STRING, 10);
        pnd_Tickers = localVariables.newFieldArrayInRecord("pnd_Tickers", "#TICKERS", FieldType.STRING, 10, new DbsArrayController(1, 10));
        pnd_Fund_Amt = localVariables.newFieldInRecord("pnd_Fund_Amt", "#FUND-AMT", FieldType.STRING, 10);
        pnd_Frqncy = localVariables.newFieldInRecord("pnd_Frqncy", "#FRQNCY", FieldType.STRING, 13);
        pnd_Output = localVariables.newFieldInRecord("pnd_Output", "#OUTPUT", FieldType.STRING, 103);

        pnd_Output__R_Field_30 = localVariables.newGroupInRecord("pnd_Output__R_Field_30", "REDEFINE", pnd_Output);
        pnd_Output_Pnd_Cntrct_Ppcn_Nbr = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Output_Pnd_O1 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O1", "#O1", FieldType.STRING, 1);
        pnd_Output_Pnd_S_Mode = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_S_Mode", "#S-MODE", FieldType.NUMERIC, 3);
        pnd_Output_Pnd_O2 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O2", "#O2", FieldType.STRING, 1);
        pnd_Output_Pnd_Cntrct_Per_Amt = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Cntrct_Per_Amt", "#CNTRCT-PER-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_O3 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O3", "#O3", FieldType.STRING, 1);
        pnd_Output_Pnd_Cntrct_Div_Amt = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Cntrct_Div_Amt", "#CNTRCT-DIV-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_O4 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O4", "#O4", FieldType.STRING, 1);
        pnd_Output_Pnd_Tot_Amt = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Tot_Amt", "#TOT-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_O5 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O5", "#O5", FieldType.STRING, 1);
        pnd_Output_Pnd_Ivc_Amt = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.STRING, 10);
        pnd_Output_Pnd_O6 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O6", "#O6", FieldType.STRING, 1);
        pnd_Output_Pnd_Dpi_Amt = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Dpi_Amt", "#DPI-AMT", FieldType.STRING, 4);
        pnd_Output_Pnd_O7 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O7", "#O7", FieldType.STRING, 1);
        pnd_Output_Pnd_Cntrct_Payee_Cde = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Output_Pnd_O8 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O8", "#O8", FieldType.STRING, 1);
        pnd_Output_Pnd_Ps_Typ = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Ps_Typ", "#PS-TYP", FieldType.STRING, 2);

        pnd_Output__R_Field_31 = pnd_Output__R_Field_30.newGroupInGroup("pnd_Output__R_Field_31", "REDEFINE", pnd_Output_Pnd_Ps_Typ);
        pnd_Output_Pnd_Pymnt_Typ = pnd_Output__R_Field_31.newFieldInGroup("pnd_Output_Pnd_Pymnt_Typ", "#PYMNT-TYP", FieldType.STRING, 1);
        pnd_Output_Pnd_Sttlmnt_Typ = pnd_Output__R_Field_31.newFieldInGroup("pnd_Output_Pnd_Sttlmnt_Typ", "#STTLMNT-TYP", FieldType.STRING, 1);
        pnd_Output_Pnd_O9 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_O9", "#O9", FieldType.STRING, 1);
        pnd_Output_Pnd_Crrncy = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Crrncy", "#CRRNCY", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_10 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_10", "#10", FieldType.STRING, 1);
        pnd_Output_Pnd_Pln_Typ = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Pln_Typ", "#PLN-TYP", FieldType.STRING, 1);
        pnd_Output_Pnd_11 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_11", "#11", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctznshp = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Ctznshp", "#CTZNSHP", FieldType.NUMERIC, 3);
        pnd_Output_Pnd_12 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_12", "#12", FieldType.STRING, 1);
        pnd_Output_Pnd_Rsdncy = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Rsdncy", "#RSDNCY", FieldType.STRING, 2);
        pnd_Output_Pnd_13 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_13", "#13", FieldType.STRING, 1);
        pnd_Output_Pnd_Money_Source = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Money_Source", "#MONEY-SOURCE", FieldType.STRING, 5);
        pnd_Output_Pnd_14 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_14", "#14", FieldType.STRING, 1);
        pnd_Output_Pnd_Rllvr = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Rllvr", "#RLLVR", FieldType.STRING, 4);
        pnd_Output_Pnd_15 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_15", "#15", FieldType.STRING, 1);
        pnd_Output_Pnd_Db_Ind = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Db_Ind", "#DB-IND", FieldType.STRING, 1);
        pnd_Output_Pnd_16 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_16", "#16", FieldType.STRING, 1);
        pnd_Output_Pnd_Dob = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_Dob", "#DOB", FieldType.STRING, 8);
        pnd_Output_Pnd_17 = pnd_Output__R_Field_30.newFieldInGroup("pnd_Output_Pnd_17", "#17", FieldType.STRING, 1);
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pnd_Ext_Fund.reset();
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
        pnd_Fund_2.setInitialValue("09");
        pnd_Type_Call.setInitialValue("R");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapqdro() throws Exception
    {
        super("Iaapqdro");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 0 LS = 250;//Natural: FORMAT ( 1 ) PS = 0 LS = 250
        //*  08/15 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getWorkFiles().read(3, aian026_Pnd_Call_Type);                                                                                                                    //Natural: READ WORK 3 ONCE #CALL-TYPE
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        pnd_Output_Pnd_O1.setValue(",");                                                                                                                                  //Natural: MOVE ',' TO #O1 #O2 #O3 #O4 #O5 #O6 #O7 #O8 #O9 #10 #11 #12 #13 #14 #15 #16 #17
        pnd_Output_Pnd_O2.setValue(",");
        pnd_Output_Pnd_O3.setValue(",");
        pnd_Output_Pnd_O4.setValue(",");
        pnd_Output_Pnd_O5.setValue(",");
        pnd_Output_Pnd_O6.setValue(",");
        pnd_Output_Pnd_O7.setValue(",");
        pnd_Output_Pnd_O8.setValue(",");
        pnd_Output_Pnd_O9.setValue(",");
        pnd_Output_Pnd_10.setValue(",");
        pnd_Output_Pnd_11.setValue(",");
        pnd_Output_Pnd_12.setValue(",");
        pnd_Output_Pnd_13.setValue(",");
        pnd_Output_Pnd_14.setValue(",");
        pnd_Output_Pnd_15.setValue(",");
        pnd_Output_Pnd_16.setValue(",");
        pnd_Output_Pnd_17.setValue(",");
        pnd_Output_Pnd_Dpi_Amt.setValue("0.00");                                                                                                                          //Natural: ASSIGN #DPI-AMT := '0.00'
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rtrn_Cde);                                                                 //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RTRN-CDE
        if (condition(Global.isEscape())) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER")))                                                                                               //Natural: IF #PPCN-NBR = '   CHEADER'
                {
                    aian026_Pnd_Prtcptn_Dte.setValue(pnd_Input_Pnd_Header_Chk_Dte);                                                                                       //Natural: ASSIGN #PRTCPTN-DTE := #HEADER-CHK-DTE
                    aian026_Pnd_Uv_Req_Dte.setValue(aian026_Pnd_Prtcptn_Dte);                                                                                             //Natural: ASSIGN #UV-REQ-DTE := #PRTCPTN-DTE
                    getReports().write(0, "=",aian026_Pnd_Prtcptn_Dte);                                                                                                   //Natural: WRITE '=' #PRTCPTN-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.equals(10) || pnd_Input_Pnd_Record_Code.equals(20) || pnd_Input_Pnd_Record_Code.equals(30))))                       //Natural: ACCEPT IF #RECORD-CODE = 10 OR = 20 OR = 30
            {
                continue;
            }
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_Cpr_Bypass.reset();                                                                                                                                   //Natural: RESET #CPR-BYPASS
                if (condition(pnd_Write_It.getBoolean()))                                                                                                                 //Natural: IF #WRITE-IT
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-IT
                    sub_Write_It();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_S_Orgn_Cde.setValue(pnd_Input_Pnd_Orgn_Cde);                                                                                                          //Natural: ASSIGN #S-ORGN-CDE := #ORGN-CDE
                pnd_S_Optn.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                              //Natural: ASSIGN #S-OPTN := #OPTN-CDE
                pnd_S_Issue_Dte.setValue(pnd_Input_Pnd_Issue_Dte);                                                                                                        //Natural: ASSIGN #S-ISSUE-DTE := #ISSUE-DTE
                pnd_S_Tax_Exmpt_Ind.setValue(pnd_Input_Pnd_Tax_Exmpt_Ind);                                                                                                //Natural: ASSIGN #S-TAX-EXMPT-IND := #TAX-EXMPT-IND
                pnd_S_Plan_Nmbr.setValue(pnd_Input_Pnd_Plan_Nmbr);                                                                                                        //Natural: ASSIGN #S-PLAN-NMBR := #PLAN-NMBR
                pnd_Output_Pnd_Crrncy.setValue(pnd_Input_Pnd_Crrncy_Cde);                                                                                                 //Natural: ASSIGN #CRRNCY := #CRRNCY-CDE
                pnd_S_Ppg_Cde.setValue(pnd_Input_Pnd_Ppg_Cde);                                                                                                            //Natural: ASSIGN #S-PPG-CDE := #PPG-CDE
                pnd_S_1st_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                      //Natural: ASSIGN #S-1ST-DOD := #FIRST-ANN-DOD
                pnd_S_2nd_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                       //Natural: ASSIGN #S-2ND-DOD := #SCND-ANN-DOD
                pnd_Output_Pnd_Dob.reset();                                                                                                                               //Natural: RESET #DOB
                if (condition(pnd_Input_Pnd_First_Ann_Dod.greater(getZero())))                                                                                            //Natural: IF #FIRST-ANN-DOD GT 0
                {
                    pnd_Output_Pnd_Dob.setValueEdited(pnd_Input_Pnd_Scnd_Ann_Dob_Dte,new ReportEditMask("99999999"));                                                     //Natural: MOVE EDITED #SCND-ANN-DOB-DTE ( EM = 99999999 ) TO #DOB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Dob.setValue(pnd_Input_Pnd_1st_Dob_8);                                                                                                 //Natural: ASSIGN #DOB := #1ST-DOB-8
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                pnd_Cpr_Bypass.reset();                                                                                                                                   //Natural: RESET #CPR-BYPASS
                if (condition(pnd_Write_It.getBoolean()))                                                                                                                 //Natural: IF #WRITE-IT
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-IT
                    sub_Write_It();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Pnd_Status_Cde.notEquals(1) || pnd_Input_Pnd_Pend_Cde.notEquals("M")))                                                            //Natural: IF #STATUS-CDE NE 1 OR #PEND-CDE NE 'M'
                {
                    pnd_Cpr_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #CPR-BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-PAYMENT-DUE
                sub_Check_Payment_Due();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Payment_Due.equals(true)))                                                                                                              //Natural: IF #PAYMENT-DUE = TRUE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cpr_Bypass.setValue(true);                                                                                                                        //Natural: ASSIGN #CPR-BYPASS := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet517 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #S-ORGN-CDE;//Natural: VALUE 11, 13, 19, 80, 86
                if (condition((pnd_S_Orgn_Cde.equals(11) || pnd_S_Orgn_Cde.equals(13) || pnd_S_Orgn_Cde.equals(19) || pnd_S_Orgn_Cde.equals(80) || pnd_S_Orgn_Cde.equals(86))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("IRA");                                                                                                          //Natural: ASSIGN #MONEY-SOURCE := 'IRA'
                }                                                                                                                                                         //Natural: VALUE 12, 20, 21, 22, 23, 79
                else if (condition((pnd_S_Orgn_Cde.equals(12) || pnd_S_Orgn_Cde.equals(20) || pnd_S_Orgn_Cde.equals(21) || pnd_S_Orgn_Cde.equals(22) || 
                    pnd_S_Orgn_Cde.equals(23) || pnd_S_Orgn_Cde.equals(79))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("ROTH");                                                                                                         //Natural: ASSIGN #MONEY-SOURCE := 'ROTH'
                }                                                                                                                                                         //Natural: VALUE 17, 18, 35, 36, 37, 38, 43, 44, 45, 46
                else if (condition((pnd_S_Orgn_Cde.equals(17) || pnd_S_Orgn_Cde.equals(18) || pnd_S_Orgn_Cde.equals(35) || pnd_S_Orgn_Cde.equals(36) || 
                    pnd_S_Orgn_Cde.equals(37) || pnd_S_Orgn_Cde.equals(38) || pnd_S_Orgn_Cde.equals(43) || pnd_S_Orgn_Cde.equals(44) || pnd_S_Orgn_Cde.equals(45) 
                    || pnd_S_Orgn_Cde.equals(46))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("PA");                                                                                                           //Natural: ASSIGN #MONEY-SOURCE := 'PA'
                }                                                                                                                                                         //Natural: VALUE 40
                else if (condition((pnd_S_Orgn_Cde.equals(40))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("SPIA");                                                                                                         //Natural: ASSIGN #MONEY-SOURCE := 'SPIA'
                }                                                                                                                                                         //Natural: VALUE 39, 42, 47, 63, 64, 67, 68
                else if (condition((pnd_S_Orgn_Cde.equals(39) || pnd_S_Orgn_Cde.equals(42) || pnd_S_Orgn_Cde.equals(47) || pnd_S_Orgn_Cde.equals(63) || 
                    pnd_S_Orgn_Cde.equals(64) || pnd_S_Orgn_Cde.equals(67) || pnd_S_Orgn_Cde.equals(68))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("SR1");                                                                                                          //Natural: ASSIGN #MONEY-SOURCE := 'SR1'
                }                                                                                                                                                         //Natural: VALUE 51, 52, 53, 54, 55, 56, 57, 58, 71, 72, 73, 74, 75,76, 77, 78
                else if (condition((pnd_S_Orgn_Cde.equals(51) || pnd_S_Orgn_Cde.equals(52) || pnd_S_Orgn_Cde.equals(53) || pnd_S_Orgn_Cde.equals(54) || 
                    pnd_S_Orgn_Cde.equals(55) || pnd_S_Orgn_Cde.equals(56) || pnd_S_Orgn_Cde.equals(57) || pnd_S_Orgn_Cde.equals(58) || pnd_S_Orgn_Cde.equals(71) 
                    || pnd_S_Orgn_Cde.equals(72) || pnd_S_Orgn_Cde.equals(73) || pnd_S_Orgn_Cde.equals(74) || pnd_S_Orgn_Cde.equals(75) || pnd_S_Orgn_Cde.equals(76) 
                    || pnd_S_Orgn_Cde.equals(77) || pnd_S_Orgn_Cde.equals(78))))
                {
                    decideConditionsMet517++;
                    pnd_Output_Pnd_Money_Source.setValue("ROTHP");                                                                                                        //Natural: ASSIGN #MONEY-SOURCE := 'ROTHP'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Output_Pnd_Money_Source.reset();                                                                                                                  //Natural: RESET #MONEY-SOURCE
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Input_Pnd_Curr_Dist_Cde.equals("CASH") || pnd_Input_Pnd_Curr_Dist_Cde.equals(" ")))                                                     //Natural: IF #CURR-DIST-CDE = 'CASH' OR = ' '
                {
                    pnd_Output_Pnd_Rllvr.reset();                                                                                                                         //Natural: RESET #RLLVR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Rllvr.setValue(pnd_Input_Pnd_Curr_Dist_Cde);                                                                                           //Natural: ASSIGN #RLLVR := #CURR-DIST-CDE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Pnd_S_Mode.setValue(pnd_Input_Pnd_Mode_Ind);                                                                                                   //Natural: ASSIGN #S-MODE := #MODE-IND
                pnd_S_Cpr_Id_Nbr.setValue(pnd_Input_Pnd_Cpr_Id_Nbr);                                                                                                      //Natural: ASSIGN #S-CPR-ID-NBR := #CPR-ID-NBR
                pnd_Output_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Input_Pnd_Ppcn_Nbr);                                                                                          //Natural: ASSIGN #CNTRCT-PPCN-NBR := #PPCN-NBR
                pnd_Output_Pnd_Cntrct_Payee_Cde.setValue(pnd_Input_Pnd_Payee_Cde_A);                                                                                      //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #PAYEE-CDE-A
                pnd_Per_Ivc.compute(new ComputeParameters(false, pnd_Per_Ivc), DbsField.add(getZero(),pnd_Input_Pnd_Cntrct_Per_Ivc_Amt.getValue(1,":",                    //Natural: ASSIGN #PER-IVC := 0 + #CNTRCT-PER-IVC-AMT ( 1:2 )
                    2)));
                pnd_Output_Pnd_Ctznshp.setValue(pnd_Input_Pnd_Ctznshp_Cde);                                                                                               //Natural: ASSIGN #CTZNSHP := #CTZNSHP-CDE
                pnd_Output_Pnd_Rsdncy.setValue(pnd_Input_Pnd_Res);                                                                                                        //Natural: ASSIGN #RSDNCY := #RES
                if (condition(pnd_Input_Pnd_Cntrct_Cmpny_Cde.getValue(1).notEquals(" ")))                                                                                 //Natural: IF #CNTRCT-CMPNY-CDE ( 1 ) NE ' '
                {
                    pnd_Tc.setValue("T");                                                                                                                                 //Natural: ASSIGN #TC := 'T'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tc.setValue("C");                                                                                                                                 //Natural: ASSIGN #TC := 'C'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SET-SETTLEMENT-PAYMENT-TYPE
                sub_Set_Settlement_Payment_Type();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Input_Pnd_Payee_Cde.greater(2)))                                                                                                        //Natural: IF #PAYEE-CDE GT 02
                {
                                                                                                                                                                          //Natural: PERFORM GET-BENE-DOB-FROM-MDM
                    sub_Get_Bene_Dob_From_Mdm();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Output_Pnd_Db_Ind.setValue("Y");                                                                                                                  //Natural: ASSIGN #DB-IND := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_S_1st_Dod.greater(getZero())))                                                                                                      //Natural: IF #S-1ST-DOD GT 0
                    {
                        pnd_Output_Pnd_Db_Ind.setValue("Y");                                                                                                              //Natural: ASSIGN #DB-IND := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_S_Orgn_Cde.greaterOrEqual(79) && pnd_S_Orgn_Cde.lessOrEqual(82)))                                                                       //Natural: IF #S-ORGN-CDE = 79 THRU 82
                {
                    pnd_Output_Pnd_Db_Ind.setValue("Y");                                                                                                                  //Natural: ASSIGN #DB-IND := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                //*    DISPLAY(1)
                //*      'CNTRCT' #PPCN-NBR
                //*      'PY' #PAYEE-CDE-A
                //*      'MODE' #MODE-IND
                //*      'IVC' #PER-IVC(EM=ZZZZZZ9.99)
                //*      'DEST' #CURR-DIST-CDE
                //*      'TC' #TC
                //*      'PT' #PS-TYP
                //*      '$' #CRRNCY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cpr_Bypass.getBoolean()))                                                                                                                   //Natural: IF #CPR-BYPASS
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Record_Code.equals(30)))                                                                                                          //Natural: IF #RECORD-CODE = 30
            {
                if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("U") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("2")            //Natural: IF #SUMM-CMPNY-CDE = 'U' OR = 'W' OR = '2' OR = '4'
                    || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("4")))
                {
                    pnd_Input_Pnd_Summ_Per_Dvdnd.reset();                                                                                                                 //Natural: RESET #SUMM-PER-DVDND
                    if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("W") || pnd_Input_Pnd_Summ_Cmpny_Cde.equals("4")))                                                  //Natural: IF #SUMM-CMPNY-CDE = 'W' OR = '4'
                    {
                                                                                                                                                                          //Natural: PERFORM GET-AUV
                        sub_Get_Auv();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Input_Pnd_Summ_Fund_Cde.setValue("01");                                                                                                           //Natural: ASSIGN #SUMM-FUND-CDE := '01'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Write_It.setValue(true);                                                                                                                              //Natural: ASSIGN #WRITE-IT := TRUE
                pnd_Tot_Pmt.compute(new ComputeParameters(false, pnd_Tot_Pmt), pnd_Tot_Pmt.add(pnd_Input_Pnd_Summ_Per_Pymnt).add(pnd_Input_Pnd_Summ_Per_Dvdnd));          //Natural: ASSIGN #TOT-PMT := #TOT-PMT + #SUMM-PER-PYMNT + #SUMM-PER-DVDND
                pnd_Per_Tot.nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                                                                           //Natural: ASSIGN #PER-TOT := #PER-TOT + #SUMM-PER-PYMNT
                pnd_Per_Div.nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                                                                           //Natural: ASSIGN #PER-DIV := #PER-DIV + #SUMM-PER-DVDND
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 20
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                {
                    if (condition(pls_Fund_Num_Cde.getValue(pnd_I).equals(pnd_Input_Pnd_Summ_Fund_Cde.val())))                                                            //Natural: IF +FUND-NUM-CDE ( #I ) = VAL ( #SUMM-FUND-CDE )
                    {
                        pnd_W_Tckr.setValue(pls_Tckr_Symbl.getValue(pnd_I));                                                                                              //Natural: ASSIGN #W-TCKR := +TCKR-SYMBL ( #I )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_W_Tckr.equals(pnd_Tickers.getValue("*"))))                                                                                              //Natural: IF #W-TCKR = #TICKERS ( * )
                {
                    DbsUtil.examine(new ExamineSource(pnd_Tickers.getValue("*")), new ExamineSearch(pnd_W_Tckr), new ExamineGivingIndex(pnd_L));                          //Natural: EXAMINE #TICKERS ( * ) FOR #W-TCKR GIVING INDEX #L
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Tickers.getValue(pnd_J).setValue(pnd_W_Tckr);                                                                                                     //Natural: ASSIGN #TICKERS ( #J ) := #W-TCKR
                    pnd_L.setValue(pnd_J);                                                                                                                                //Natural: ASSIGN #L := #J
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fund_Tot.getValue(pnd_L).compute(new ComputeParameters(false, pnd_Fund_Tot.getValue(pnd_L)), pnd_Fund_Tot.getValue(pnd_L).add(pnd_Input_Pnd_Summ_Per_Pymnt).add(pnd_Input_Pnd_Summ_Per_Dvdnd)); //Natural: ASSIGN #FUND-TOT ( #L ) := #FUND-TOT ( #L ) + #SUMM-PER-PYMNT + #SUMM-PER-DVDND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  08/15 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUV
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-SETTLEMENT-PAYMENT-TYPE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SHORT-NAME
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BENE-DOB-FROM-MDM
        //* *#MDMA100.#I-SSN   := #TAX-ID-NBR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PAYMENT-DUE
    }
    private void sub_Write_It() throws Exception                                                                                                                          //Natural: WRITE-IT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Output_Pnd_Ivc_Amt.setValueEdited(pnd_Per_Ivc,new ReportEditMask("9999999.99"));                                                                              //Natural: MOVE EDITED #PER-IVC ( EM = 9999999.99 ) TO #IVC-AMT
        pnd_Output_Pnd_Cntrct_Per_Amt.setValueEdited(pnd_Per_Tot,new ReportEditMask("9999999.99"));                                                                       //Natural: MOVE EDITED #PER-TOT ( EM = 9999999.99 ) TO #CNTRCT-PER-AMT
        pnd_Output_Pnd_Cntrct_Div_Amt.setValueEdited(pnd_Per_Div,new ReportEditMask("9999999.99"));                                                                       //Natural: MOVE EDITED #PER-DIV ( EM = 9999999.99 ) TO #CNTRCT-DIV-AMT
        pnd_Output_Pnd_Tot_Amt.setValueEdited(pnd_Tot_Pmt,new ReportEditMask("9999999.99"));                                                                              //Natural: MOVE EDITED #TOT-PMT ( EM = 9999999.99 ) TO #TOT-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pnd_Tickers.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF #TICKERS ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Amt.setValueEdited(pnd_Fund_Tot.getValue(pnd_I),new ReportEditMask("9999999.99"));                                                                   //Natural: MOVE EDITED #FUND-TOT ( #I ) ( EM = 9999999.99 ) TO #FUND-AMT
            pnd_W_Tckr.setValue(pnd_Tickers.getValue(pnd_I));                                                                                                             //Natural: ASSIGN #W-TCKR := #TICKERS ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-SHORT-NAME
            sub_Get_Short_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, pnd_Output, pnd_Fnd_Idntfr, pnd_Output_Pnd_17, pnd_Tc, pnd_Output_Pnd_17, pnd_Fund_Amt, pnd_Output_Pnd_17,                     //Natural: WRITE WORK FILE 2 #OUTPUT #FND-IDNTFR #17 #TC #17 #FUND-AMT #17 #S-CPR-ID-NBR
                pnd_S_Cpr_Id_Nbr);
            //*  ADD 1 TO #REC-CNT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Write_It.reset();                                                                                                                                             //Natural: RESET #WRITE-IT #PER-IVC #PER-TOT #PER-DIV #TOT-PMT #PS-TYP #TICKERS ( * ) #FUND-AMT #J #FUND-TOT ( * ) #K #L #DB-IND #DOB
        pnd_Per_Ivc.reset();
        pnd_Per_Tot.reset();
        pnd_Per_Div.reset();
        pnd_Tot_Pmt.reset();
        pnd_Output_Pnd_Ps_Typ.reset();
        pnd_Tickers.getValue("*").reset();
        pnd_Fund_Amt.reset();
        pnd_J.reset();
        pnd_Fund_Tot.getValue("*").reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Output_Pnd_Db_Ind.reset();
        pnd_Output_Pnd_Dob.reset();
    }
    private void sub_Get_Auv() throws Exception                                                                                                                           //Natural: GET-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        aian026_Pnd_Ia_Fund_Code.reset();                                                                                                                                 //Natural: RESET #IA-FUND-CODE
        FOR03:                                                                                                                                                            //Natural: FOR #K 1 TO 20
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(20)); pnd_K.nadd(1))
        {
            if (condition(pls_Fund_Num_Cde.getValue(pnd_K).equals(pnd_Input_Pnd_Summ_Fund_Cde.val())))                                                                    //Natural: IF +FUND-NUM-CDE ( #K ) = VAL ( #SUMM-FUND-CDE )
            {
                aian026_Pnd_Ia_Fund_Code.setValue(pls_Fund_Alpha_Cde.getValue(pnd_K));                                                                                    //Natural: ASSIGN #IA-FUND-CODE := +FUND-ALPHA-CDE ( #K )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(aian026_Pnd_Ia_Fund_Code.equals(" ")))                                                                                                              //Natural: IF #IA-FUND-CODE = ' '
        {
            getReports().write(0, "ERROR: INVALID FUND CODE FOR:",pnd_Output_Pnd_Cntrct_Ppcn_Nbr,pnd_Output_Pnd_Cntrct_Payee_Cde);                                        //Natural: WRITE 'ERROR: INVALID FUND CODE FOR:' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE
            if (Global.isEscape()) return;
            DbsUtil.terminate(77);  if (true) return;                                                                                                                     //Natural: TERMINATE 77
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.equals(getZero())))                                                                                                             //Natural: IF #RTRN-CD = 0
        {
            pnd_Input_Pnd_Summ_Per_Pymnt.compute(new ComputeParameters(true, pnd_Input_Pnd_Summ_Per_Pymnt), pnd_Input_Pnd_Summ_Units.multiply(aian026_Pnd_Iuv));          //Natural: COMPUTE ROUNDED #SUMM-PER-PYMNT = #SUMM-UNITS * #IUV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ERROR IN CNTRCT:",pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde_A,"No unit value for fund:",aian026_Pnd_Ia_Fund_Code);                //Natural: WRITE 'ERROR IN CNTRCT:' #PPCN-NBR #PAYEE-CDE-A 'No unit value for fund:' #IA-FUND-CODE
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Settlement_Payment_Type() throws Exception                                                                                                       //Natural: SET-SETTLEMENT-PAYMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        //*  COPIED FROM IAAP700
        if (condition(pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("20") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("12")))                                                      //Natural: IF #S-ORGN-CDE-A = '20' OR = '12'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("C");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'C'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("T");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'T'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Ppg_Cde.equals("Y1650")))                                                                                                                     //Natural: IF #S-PPG-CDE = 'Y1650'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("C");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'C'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("C");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("21") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("22") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("23")        //Natural: IF #S-ORGN-CDE-A = '21' OR = '22' OR = '23' OR = '79'
            || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("79")))
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("C");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'C'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("H");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'H'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("19") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("11") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("13")        //Natural: IF #S-ORGN-CDE-A = '19' OR = '11' OR = '13' OR = '80' OR = '86'
            || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("80") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("86")))
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("C");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'C'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue(" ");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Optn.equals(28) || pnd_S_Optn.equals(30)))                                                                                                    //Natural: IF ( #S-OPTN = 28 OR = 30 )
        {
            if (condition(pnd_S_Issue_Dte.less(200111)))                                                                                                                  //Natural: IF #S-ISSUE-DTE < 200111
            {
                pnd_Output_Pnd_Pymnt_Typ.setValue("P");                                                                                                                   //Natural: ASSIGN #PYMNT-TYP := 'P'
                pnd_Output_Pnd_Sttlmnt_Typ.setValue(" ");                                                                                                                 //Natural: ASSIGN #STTLMNT-TYP := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Pnd_Pymnt_Typ.setValue("T");                                                                                                                   //Natural: ASSIGN #PYMNT-TYP := 'T'
                pnd_Output_Pnd_Sttlmnt_Typ.setValue(" ");                                                                                                                 //Natural: ASSIGN #STTLMNT-TYP := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Optn.equals(25) || pnd_S_Optn.equals(27) || pnd_S_Optn.equals(22)))                                                                           //Natural: IF #S-OPTN = 25 OR = 27 OR = 22
        {
            if (condition(pnd_Input_Pnd_Curr_Dist_Cde.equals("IRAT") || pnd_Input_Pnd_Curr_Dist_Cde.equals("IRAC") || pnd_Input_Pnd_Curr_Dist_Cde.equals("03BT")          //Natural: IF #CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                || pnd_Input_Pnd_Curr_Dist_Cde.equals("03BC") || pnd_Input_Pnd_Curr_Dist_Cde.equals("QPLT") || pnd_Input_Pnd_Curr_Dist_Cde.equals("QPLC") 
                || pnd_Input_Pnd_Curr_Dist_Cde.equals("57BT") || pnd_Input_Pnd_Curr_Dist_Cde.equals("57BC")))
            {
                pnd_Output_Pnd_Pymnt_Typ.setValue("P");                                                                                                                   //Natural: ASSIGN #PYMNT-TYP := 'P'
                pnd_Output_Pnd_Sttlmnt_Typ.setValue("R");                                                                                                                 //Natural: ASSIGN #STTLMNT-TYP := 'R'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Pnd_Pymnt_Typ.setValue("P");                                                                                                                   //Natural: ASSIGN #PYMNT-TYP := 'P'
                pnd_Output_Pnd_Sttlmnt_Typ.setValue(" ");                                                                                                                 //Natural: ASSIGN #STTLMNT-TYP := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.greaterOrEqual("51") && pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.lessOrEqual("58")) || (pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.greaterOrEqual("71")  //Natural: IF #S-ORGN-CDE-A = '51' THRU '58' OR #S-ORGN-CDE-A = '71' THRU '78'
            && pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.lessOrEqual("78")))))
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("A");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'A'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("W");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'W'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.greaterOrEqual("24") && pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.lessOrEqual("31")))                                         //Natural: IF #S-ORGN-CDE-A = '24' THRU '31'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("A");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'A'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("S");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("50") || pnd_S_Orgn_Cde_Pnd_S_Orgn_Cde_A.equals("83")))                                                      //Natural: IF #S-ORGN-CDE-A = '50' OR = '83'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("M");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'M'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("S");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Tax_Exmpt_Ind.equals("Y")))                                                                                                                   //Natural: IF #S-TAX-EXMPT-IND = 'Y'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("X");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'X'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("Z");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'Z'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_S_Plan_Nmbr.equals("TIAA05") || pnd_S_Plan_Nmbr.equals("TIAA06")))                                                                              //Natural: IF #S-PLAN-NMBR = 'TIAA05' OR = 'TIAA06'
        {
            pnd_Output_Pnd_Pymnt_Typ.setValue("M");                                                                                                                       //Natural: ASSIGN #PYMNT-TYP := 'M'
            pnd_Output_Pnd_Sttlmnt_Typ.setValue("T");                                                                                                                     //Natural: ASSIGN #STTLMNT-TYP := 'T'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Short_Name() throws Exception                                                                                                                    //Natural: GET-SHORT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Fnd_Idntfr.reset();                                                                                                                                           //Natural: RESET #FND-IDNTFR #WS-TICKER
        pnd_Ws_Ticker.reset();
        pnd_Nec_Fnd_Super1_Pnd_Nec_Table_Cde.setValue("FND");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'FND'
        pnd_Nec_Fnd_Super1_Pnd_Nec_Ticker_Symbol.setValue(pnd_W_Tckr);                                                                                                    //Natural: ASSIGN #NEC-TICKER-SYMBOL := #W-TCKR
        vw_pnd_Ext_Fund.startDatabaseFind                                                                                                                                 //Natural: FIND #EXT-FUND WITH NEC-FND-SUPER1 = #NEC-FND-SUPER1
        (
        "FIND01",
        new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_pnd_Ext_Fund.readNextRow("FIND01")))
        {
            vw_pnd_Ext_Fund.setIfNotFoundControlFlag(false);
            pnd_Ws_Ticker.setValue(pnd_Ext_Fund_Nec_Investment_Grouping_Id);                                                                                              //Natural: ASSIGN #WS-TICKER := NEC-INVESTMENT-GROUPING-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ084");                                                                                                   //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ084'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("ADS");                                                                                                      //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'ADS'
        pnd_Naz_Tbl_Super1_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Ws_Ticker);                                                                                              //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #WS-TICKER
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) NAZ-TABLE-DDM WITH NAZ-TBL-SUPER1 = #NAZ-TBL-SUPER1
        (
        "FIND02",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Tbl_Super1, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND02")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_Fnd_Idntfr.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                              //Natural: ASSIGN #FND-IDNTFR := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Fnd_Idntfr.equals(" ")))                                                                                                                        //Natural: IF #FND-IDNTFR = ' '
        {
            pnd_Fnd_Idntfr.setValue(pnd_W_Tckr);                                                                                                                          //Natural: ASSIGN #FND-IDNTFR := #W-TCKR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Bene_Dob_From_Mdm() throws Exception                                                                                                             //Natural: GET-BENE-DOB-FROM-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaMdma101.getPnd_Mdma101_Pnd_O_Pda_Data().reset();                                                                                                               //Natural: RESET #O-PDA-DATA
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pnd_Input_Pnd_Tax_Id_Nbr);                                                                                         //Natural: ASSIGN #MDMA101.#I-SSN := #TAX-ID-NBR
        //* *CALLNAT 'MDMN100A' #MDMA100 /* 08/15
        //*  08/15
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //* * #MDMA100.#O-RETURN-CODE NE '0000'
        //*  082017 END
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA101.#O-RETURN-CODE NE '0000'
        {
            pnd_Output_Pnd_Dob.reset();                                                                                                                                   //Natural: RESET #DOB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Dob.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                                 //Natural: ASSIGN #DOB := #O-DATE-OF-BIRTH
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Payment_Due() throws Exception                                                                                                                 //Natural: CHECK-PAYMENT-DUE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Payment_Due.reset();                                                                                                                                          //Natural: RESET #PAYMENT-DUE
        short decideConditionsMet805 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #MODE-IND;//Natural: VALUE 100
        if (condition((pnd_Input_Pnd_Mode_Ind.equals(100))))
        {
            decideConditionsMet805++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(601))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(1) || aian026_Pnd_Req_Mm.equals(4) || aian026_Pnd_Req_Mm.equals(7) || aian026_Pnd_Req_Mm.equals(10)))                 //Natural: IF #REQ-MM = 01 OR = 04 OR = 07 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(602))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(2) || aian026_Pnd_Req_Mm.equals(5) || aian026_Pnd_Req_Mm.equals(8) || aian026_Pnd_Req_Mm.equals(11)))                 //Natural: IF #REQ-MM = 02 OR = 05 OR = 08 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(603))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(3) || aian026_Pnd_Req_Mm.equals(6) || aian026_Pnd_Req_Mm.equals(9) || aian026_Pnd_Req_Mm.equals(12)))                 //Natural: IF #REQ-MM = 03 OR = 06 OR = 09 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(701))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(1) || aian026_Pnd_Req_Mm.equals(7)))                                                                                  //Natural: IF #REQ-MM = 01 OR = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(702))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(2) || aian026_Pnd_Req_Mm.equals(8)))                                                                                  //Natural: IF #REQ-MM = 02 OR = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(703))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(3) || aian026_Pnd_Req_Mm.equals(9)))                                                                                  //Natural: IF #REQ-MM = 03 OR = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(704))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(4) || aian026_Pnd_Req_Mm.equals(10)))                                                                                 //Natural: IF #REQ-MM = 04 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(705))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(5) || aian026_Pnd_Req_Mm.equals(11)))                                                                                 //Natural: IF #REQ-MM = 05 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(706))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(6) || aian026_Pnd_Req_Mm.equals(12)))                                                                                 //Natural: IF #REQ-MM = 06 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(801))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(1)))                                                                                                                  //Natural: IF #REQ-MM = 01
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(802))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(2)))                                                                                                                  //Natural: IF #REQ-MM = 02
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(803))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(3)))                                                                                                                  //Natural: IF #REQ-MM = 03
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(804))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(4)))                                                                                                                  //Natural: IF #REQ-MM = 04
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(805))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(5)))                                                                                                                  //Natural: IF #REQ-MM = 05
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(806))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(6)))                                                                                                                  //Natural: IF #REQ-MM = 06
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(807))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(7)))                                                                                                                  //Natural: IF #REQ-MM = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(808))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(8)))                                                                                                                  //Natural: IF #REQ-MM = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(809))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(9)))                                                                                                                  //Natural: IF #REQ-MM = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(810))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(10)))                                                                                                                 //Natural: IF #REQ-MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(811))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(11)))                                                                                                                 //Natural: IF #REQ-MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((pnd_Input_Pnd_Mode_Ind.equals(812))))
        {
            decideConditionsMet805++;
            if (condition(aian026_Pnd_Req_Mm.equals(12)))                                                                                                                 //Natural: IF #REQ-MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=0 LS=250");
        Global.format(1, "PS=0 LS=250");
    }
}
