/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:36 PM
**        * FROM NATURAL PROGRAM : Iatp120
************************************************************
**        * FILE NAME            : Iatp120.java
**        * CLASS NAME           : Iatp120
**        * INSTANCE NAME        : Iatp120
************************************************************
************************************************************************
* PROGRAM:  IATP120
* FUNCTION: REPORT GIVING NUMBER OF FROM-TO TRANSFER COMBINATIONS.
*
* CREATED:  12/04/95 BY ARI GROSSMAN
*
* HISTORY: 12/16/96 : ADDED MULTI FUND FUNCTIONALITY
*          11/25/97 : ADDED ILB FUND, MANY TO MANY PROCESSING, BREAKING
*                     BY EFFECTIVE DATE INTO SEPARATE REPORTS.
*          01/23/09 OS TIAA ACCESS CHANGES. SC 012309.
*          04/06/12 OS RATE BASE EXPANSION CHANGES. SC 040612.
*          04/2017  OS RE-STOWED ONLY FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp120 extends BLNatBase
{
    // Data Areas
    private LdaIatl100 ldaIatl100;
    private LdaIatl010 ldaIatl010;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_Max_Acct;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_To_Units;
    private DbsField pnd_W_Units;
    private DbsField pnd_D_Units;
    private DbsField pnd_E_Units;
    private DbsField pnd_To_Pymts;
    private DbsField pnd_W_Pymts;
    private DbsField pnd_D_Pymts;
    private DbsField pnd_E_Pymts;
    private DbsField pnd_Date8;
    private DbsField pnd_Auv_Dte;
    private DbsField pnd_Auv;
    private DbsField pnd_Auv_Ret_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_D;
    private DbsField pnd_W_To_Date;
    private DbsField pnd_W_From_Date;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_2;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;

    private DbsGroup pnd_Eff_Date__R_Field_3;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;
    private DbsField pnd_Eff_Date_Hold;

    private DbsGroup pnd_Eff_Date_Hold__R_Field_4;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd;
    private DbsField pnd_Cmpny_Fund_3;
    private DbsField pnd_Invrse_Recvd_Time;

    private DbsGroup pnd_Invrse_Recvd_Time__R_Field_5;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Fund;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Dte;
    private DbsField pnd_Read_File;
    private DbsField pnd_Read_File_1;
    private DbsField pnd_W_Date;
    private DbsField pnd_Acct_Cde;
    private DbsField pnd_T;
    private DbsField pnd_Tiaa_Fund_Pymt;
    private DbsField pnd_Xfr_Units;
    private DbsField pnd_Transfer_Dollars;
    private DbsField pnd_Per;
    private DbsField pnd_Next_Pay_Dte;
    private DbsField pnd_Next_Chk_Dt;
    private DbsField pnd_Print_Reports;
    private DbsField pnd_Last_Dt;
    private DbsField pnd_Next_Dt;

    private DbsGroup pnd_Next_Dt__R_Field_6;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Mm;
    private DbsField pnd_Next_Dt_Pnd_Filler_1;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Dd;
    private DbsField pnd_Next_Dt_Pnd_Filler_2;
    private DbsField pnd_Next_Dt_Pnd_Next_Dt_Yyyy;
    private DbsField pnd_Nbr_Acct;
    private DbsField pnd_Acct_Std_Alpha_Cde;
    private DbsField pnd_Acct_Nme_5;
    private DbsField pnd_Acct_Tckr;
    private DbsField pnd_Acct_Effctve_Dte;
    private DbsField pnd_Acct_Unit_Rte_Ind;
    private DbsField pnd_Acct_Std_Nm_Cde;
    private DbsField pnd_Acct_Rpt_Seq;
    private DbsField pnd_Sub_Tot;
    private DbsField pnd_Ovrall_Tot;

    private DbsGroup pnd_Totals_Table;
    private DbsField pnd_Totals_Table_Pnd_To_Fnd;
    private DbsField pnd_Totals_Table_Pnd_To_Tot;
    private DbsField pnd_Totals_Table_Pnd_Fr_Fnd;
    private DbsField pnd_Totals_Table_Pnd_Fr_Tot;

    private DbsGroup pnd_Net_Total;
    private DbsField pnd_Net_Total_Pnd_Net_Fnd;
    private DbsField pnd_Net_Total_Pnd_Net_Amt;
    private DbsField pnd_Name_From;
    private DbsField pnd_Name_To;
    private DbsField pnd_Fund_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatl100 = new LdaIatl100();
        registerRecord(ldaIatl100);
        registerRecord(ldaIatl100.getVw_iaa_Tiaa_Fund_Rcrd_View());
        ldaIatl010 = new LdaIatl010();
        registerRecord(ldaIatl010);
        registerRecord(ldaIatl010.getVw_iatl010());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Max_Acct = localVariables.newFieldInRecord("pnd_Max_Acct", "#MAX-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_To_Units = localVariables.newFieldInRecord("pnd_To_Units", "#TO-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Units = localVariables.newFieldInRecord("pnd_W_Units", "#W-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_D_Units = localVariables.newFieldInRecord("pnd_D_Units", "#D-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_E_Units = localVariables.newFieldInRecord("pnd_E_Units", "#E-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_To_Pymts = localVariables.newFieldInRecord("pnd_To_Pymts", "#TO-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W_Pymts = localVariables.newFieldInRecord("pnd_W_Pymts", "#W-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_D_Pymts = localVariables.newFieldInRecord("pnd_D_Pymts", "#D-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_E_Pymts = localVariables.newFieldInRecord("pnd_E_Pymts", "#E-PYMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);
        pnd_Auv_Dte = localVariables.newFieldInRecord("pnd_Auv_Dte", "#AUV-DTE", FieldType.NUMERIC, 8);
        pnd_Auv = localVariables.newFieldInRecord("pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_Auv_Ret_Dte = localVariables.newFieldInRecord("pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 2);
        pnd_W_To_Date = localVariables.newFieldInRecord("pnd_W_To_Date", "#W-TO-DATE", FieldType.STRING, 8);
        pnd_W_From_Date = localVariables.newFieldInRecord("pnd_W_From_Date", "#W-FROM-DATE", FieldType.STRING, 8);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_2", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Cc = pnd_Eff_Date__R_Field_2.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Cc", "#EFF-DATE-CC", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Yy = pnd_Eff_Date__R_Field_2.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yy", "#EFF-DATE-YY", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_2.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_2.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);

        pnd_Eff_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_3", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_3.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);
        pnd_Eff_Date_Hold = localVariables.newFieldInRecord("pnd_Eff_Date_Hold", "#EFF-DATE-HOLD", FieldType.STRING, 8);

        pnd_Eff_Date_Hold__R_Field_4 = localVariables.newGroupInRecord("pnd_Eff_Date_Hold__R_Field_4", "REDEFINE", pnd_Eff_Date_Hold);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc = pnd_Eff_Date_Hold__R_Field_4.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Cc", "#HLD-EFF-DATE-CC", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy = pnd_Eff_Date_Hold__R_Field_4.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy", "#HLD-EFF-DATE-YY", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm = pnd_Eff_Date_Hold__R_Field_4.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm", "#HLD-EFF-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd = pnd_Eff_Date_Hold__R_Field_4.newFieldInGroup("pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd", "#HLD-EFF-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Cmpny_Fund_3 = localVariables.newFieldInRecord("pnd_Cmpny_Fund_3", "#CMPNY-FUND-3", FieldType.STRING, 3);
        pnd_Invrse_Recvd_Time = localVariables.newFieldInRecord("pnd_Invrse_Recvd_Time", "#INVRSE-RECVD-TIME", FieldType.NUMERIC, 14);

        pnd_Invrse_Recvd_Time__R_Field_5 = localVariables.newGroupInRecord("pnd_Invrse_Recvd_Time__R_Field_5", "REDEFINE", pnd_Invrse_Recvd_Time);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Fund = pnd_Invrse_Recvd_Time__R_Field_5.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Fund", "#IRT-FUND", FieldType.NUMERIC, 
            2);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Dte = pnd_Invrse_Recvd_Time__R_Field_5.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Dte", "#IRT-DTE", FieldType.NUMERIC, 
            12);
        pnd_Read_File = localVariables.newFieldInRecord("pnd_Read_File", "#READ-FILE", FieldType.NUMERIC, 6);
        pnd_Read_File_1 = localVariables.newFieldInRecord("pnd_Read_File_1", "#READ-FILE-1", FieldType.NUMERIC, 6);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.STRING, 8);
        pnd_Acct_Cde = localVariables.newFieldInRecord("pnd_Acct_Cde", "#ACCT-CDE", FieldType.STRING, 1);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.NUMERIC, 2);
        pnd_Tiaa_Fund_Pymt = localVariables.newFieldInRecord("pnd_Tiaa_Fund_Pymt", "#TIAA-FUND-PYMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xfr_Units = localVariables.newFieldInRecord("pnd_Xfr_Units", "#XFR-UNITS", FieldType.PACKED_DECIMAL, 8, 3);
        pnd_Transfer_Dollars = localVariables.newFieldInRecord("pnd_Transfer_Dollars", "#TRANSFER-DOLLARS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Per = localVariables.newFieldInRecord("pnd_Per", "#PER", FieldType.NUMERIC, 7, 4);
        pnd_Next_Pay_Dte = localVariables.newFieldInRecord("pnd_Next_Pay_Dte", "#NEXT-PAY-DTE", FieldType.DATE);
        pnd_Next_Chk_Dt = localVariables.newFieldInRecord("pnd_Next_Chk_Dt", "#NEXT-CHK-DT", FieldType.STRING, 8);
        pnd_Print_Reports = localVariables.newFieldInRecord("pnd_Print_Reports", "#PRINT-REPORTS", FieldType.NUMERIC, 8);
        pnd_Last_Dt = localVariables.newFieldInRecord("pnd_Last_Dt", "#LAST-DT", FieldType.STRING, 10);
        pnd_Next_Dt = localVariables.newFieldInRecord("pnd_Next_Dt", "#NEXT-DT", FieldType.STRING, 10);

        pnd_Next_Dt__R_Field_6 = localVariables.newGroupInRecord("pnd_Next_Dt__R_Field_6", "REDEFINE", pnd_Next_Dt);
        pnd_Next_Dt_Pnd_Next_Dt_Mm = pnd_Next_Dt__R_Field_6.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Mm", "#NEXT-DT-MM", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_1 = pnd_Next_Dt__R_Field_6.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Dd = pnd_Next_Dt__R_Field_6.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Dd", "#NEXT-DT-DD", FieldType.STRING, 2);
        pnd_Next_Dt_Pnd_Filler_2 = pnd_Next_Dt__R_Field_6.newFieldInGroup("pnd_Next_Dt_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Next_Dt_Pnd_Next_Dt_Yyyy = pnd_Next_Dt__R_Field_6.newFieldInGroup("pnd_Next_Dt_Pnd_Next_Dt_Yyyy", "#NEXT-DT-YYYY", FieldType.STRING, 4);
        pnd_Nbr_Acct = localVariables.newFieldInRecord("pnd_Nbr_Acct", "#NBR-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Acct_Std_Alpha_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Alpha_Cde", "#ACCT-STD-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Acct_Nme_5 = localVariables.newFieldArrayInRecord("pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 6, new DbsArrayController(1, 20));
        pnd_Acct_Tckr = localVariables.newFieldArrayInRecord("pnd_Acct_Tckr", "#ACCT-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Acct_Effctve_Dte = localVariables.newFieldArrayInRecord("pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            20));
        pnd_Acct_Unit_Rte_Ind = localVariables.newFieldArrayInRecord("pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 
            20));
        pnd_Acct_Std_Nm_Cde = localVariables.newFieldArrayInRecord("pnd_Acct_Std_Nm_Cde", "#ACCT-STD-NM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pnd_Acct_Rpt_Seq = localVariables.newFieldArrayInRecord("pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 2, new DbsArrayController(1, 20));
        pnd_Sub_Tot = localVariables.newFieldInRecord("pnd_Sub_Tot", "#SUB-TOT", FieldType.NUMERIC, 10);
        pnd_Ovrall_Tot = localVariables.newFieldInRecord("pnd_Ovrall_Tot", "#OVRALL-TOT", FieldType.NUMERIC, 10);

        pnd_Totals_Table = localVariables.newGroupArrayInRecord("pnd_Totals_Table", "#TOTALS-TABLE", new DbsArrayController(1, 20));
        pnd_Totals_Table_Pnd_To_Fnd = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_To_Fnd", "#TO-FND", FieldType.STRING, 6);
        pnd_Totals_Table_Pnd_To_Tot = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_To_Tot", "#TO-TOT", FieldType.NUMERIC, 10);
        pnd_Totals_Table_Pnd_Fr_Fnd = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_Fr_Fnd", "#FR-FND", FieldType.STRING, 6);
        pnd_Totals_Table_Pnd_Fr_Tot = pnd_Totals_Table.newFieldInGroup("pnd_Totals_Table_Pnd_Fr_Tot", "#FR-TOT", FieldType.NUMERIC, 10);

        pnd_Net_Total = localVariables.newGroupArrayInRecord("pnd_Net_Total", "#NET-TOTAL", new DbsArrayController(1, 20));
        pnd_Net_Total_Pnd_Net_Fnd = pnd_Net_Total.newFieldInGroup("pnd_Net_Total_Pnd_Net_Fnd", "#NET-FND", FieldType.STRING, 6);
        pnd_Net_Total_Pnd_Net_Amt = pnd_Net_Total.newFieldInGroup("pnd_Net_Total_Pnd_Net_Amt", "#NET-AMT", FieldType.NUMERIC, 10);
        pnd_Name_From = localVariables.newFieldInRecord("pnd_Name_From", "#NAME-FROM", FieldType.STRING, 6);
        pnd_Name_To = localVariables.newFieldInRecord("pnd_Name_To", "#NAME-TO", FieldType.STRING, 6);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        ldaIatl100.initializeValues();
        ldaIatl010.initializeValues();

        localVariables.reset();
        pnd_Max_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp120() throws Exception
    {
        super("Iatp120");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*  012309
                                                                                                                                                                          //Natural: PERFORM GET-ACCT-INFO
        sub_Get_Acct_Info();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "***************************",NEWLINE,"  START OF IATP120 PROGRAM",NEWLINE,"***************************");                                  //Natural: WRITE '***************************' / '  START OF IATP120 PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #TEXT-TABLE-PARA
        sub_Pnd_Text_Table_Para();
        if (condition(Global.isEscape())) {return;}
        RW:                                                                                                                                                               //Natural: READ WORK FILE 1 IATL010
        while (condition(getWorkFiles().read(1, ldaIatl010.getVw_iatl010())))
        {
            pnd_Read_File.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-FILE
            pnd_Eff_Date.setValueEdited(ldaIatl010.getIatl010_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED IATL010.RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
            //*     WRITE '=' #EFF-DATE-N
            //*     MOVE EDITED IATL010.RQST-EFFCTV-DTE(EM=MM/DD/YY) TO #W-DATE
            //*     COMPRESS #EFF-DATE-MM '/' #EFF-DATE-DD '/' #EFF-DATE-YY
            //*       INTO #W-DATE LEAVING NO
            if (condition(pnd_Read_File.equals(1)))                                                                                                                       //Natural: IF #READ-FILE = 1
            {
                pnd_Eff_Date_Hold.setValue(pnd_Eff_Date);                                                                                                                 //Natural: MOVE #EFF-DATE TO #EFF-DATE-HOLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Eff_Date.notEquals(pnd_Eff_Date_Hold)))                                                                                                 //Natural: IF #EFF-DATE NE #EFF-DATE-HOLD
                {
                                                                                                                                                                          //Natural: PERFORM #PRINT-REPORT
                    sub_Pnd_Print_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM #RESET-PRINT-REPORT
                    sub_Pnd_Reset_Print_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RW"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Eff_Date_Hold.setValue(pnd_Eff_Date);                                                                                                             //Natural: MOVE #EFF-DATE TO #EFF-DATE-HOLD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_File_1.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #READ-FILE-1
            pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr.setValue(ldaIatl010.getIatl010_Ia_Frm_Cntrct());                                                                            //Natural: ASSIGN #PPCN-NBR := IATL010.IA-FRM-CNTRCT
            pnd_Cntrct_Payee_Key_Pnd_Payee_Cde.compute(new ComputeParameters(false, pnd_Cntrct_Payee_Key_Pnd_Payee_Cde), ldaIatl010.getIatl010_Ia_Frm_Payee().val());     //Natural: ASSIGN #PAYEE-CDE := VAL ( IATL010.IA-FRM-PAYEE )
            //*    WRITE '=' #PPCN-NBR '=' #PAYEE-CDE
            vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                  //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
            (
            "R1",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
            1
            );
            R1:
            while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("R1")))
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(pnd_Cntrct_Payee_Key_Pnd_Payee_Cde))) //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR NE #PPCN-NBR OR IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE NE #PAYEE-CDE
                {
                    //*  040612
                    getReports().write(0, NEWLINE," CONTRACT PARTICIPANT",pnd_Cntrct_Payee_Key,"NOT FOUND");                                                              //Natural: WRITE / ' CONTRACT PARTICIPANT' #CNTRCT-PAYEE-KEY 'NOT FOUND'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        #CONTRACT-NUMBER #PAYEE-CODE 'NOT FOUND'
                    //*         ESCAPE TOP(RW.)
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))                                                                                       //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
                {
                    //*  040612
                    getReports().write(0, NEWLINE," CONTRACT PARTICIPANT",pnd_Cntrct_Payee_Key,"NOT ACTIVE");                                                             //Natural: WRITE / ' CONTRACT PARTICIPANT' #CNTRCT-PAYEE-KEY 'NOT ACTIVE'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        #CONTRACT-NUMBER #PAYEE-CODE 'NOT ACTIVE'
                    //*         ESCAPE TOP(RW.)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FZ:                                                                                                                                                           //Natural: FOR #T = 1 TO 20
            for (pnd_T.setValue(1); condition(pnd_T.lessOrEqual(20)); pnd_T.nadd(1))
            {
                if (condition(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T).equals(" ")))                                                                      //Natural: IF IATL010.XFR-FRM-ACCT-CDE ( #T ) EQ ' '
                {
                    if (true) break FZ;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FZ. )
                }                                                                                                                                                         //Natural: END-IF
                ldaIatl100.getPnd_Fund_Cd_From().setValue(ldaIatl010.getIatl010_Xfr_Frm_Acct_Cde().getValue(pnd_T));                                                      //Natural: ASSIGN #FUND-CD-FROM := IATL010.XFR-FRM-ACCT-CDE ( #T )
                //*  012309
                                                                                                                                                                          //Natural: PERFORM #FROM-SUB-CONVERT
                sub_Pnd_From_Sub_Convert();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                F5:                                                                                                                                                       //Natural: FOR #C = 1 TO #MAX-ACCT
                for (ldaIatl100.getPnd_C().setValue(1); condition(ldaIatl100.getPnd_C().lessOrEqual(pnd_Max_Acct)); ldaIatl100.getPnd_C().nadd(1))
                {
                    if (condition(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(ldaIatl100.getPnd_C()).notEquals(" ")))                                                //Natural: IF IATL010.XFR-TO-ACCT-CDE ( #C ) NE ' '
                    {
                        ldaIatl100.getPnd_Fund_Cd_To().setValue(ldaIatl010.getIatl010_Xfr_To_Acct_Cde().getValue(ldaIatl100.getPnd_C()));                                 //Natural: ASSIGN #FUND-CD-TO := IATL010.XFR-TO-ACCT-CDE ( #C )
                                                                                                                                                                          //Natural: PERFORM #TO-SUB-CONVERT
                        sub_Pnd_To_Sub_Convert();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F5"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        ADD IATA001.#TRANSFER-AMT-OUT-CREF(#C)
                        ldaIatl100.getPnd_To_From_Table().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).nadd(1);                                      //Natural: ADD 1 TO #TO-FROM-TABLE ( #TO-SUB,#FROM-SUB )
                        //*  012309
                        //*  012309
                        ldaIatl100.getPnd_To_From_Funds().getValue(ldaIatl100.getPnd_To_Sub(),ldaIatl100.getPnd_From_Sub()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,  //Natural: COMPRESS #FUND-CD-TO #FUND-CD-FROM INTO #TO-FROM-FUNDS ( #TO-SUB,#FROM-SUB ) LEAVING NO
                            ldaIatl100.getPnd_Fund_Cd_To(), ldaIatl100.getPnd_Fund_Cd_From()));
                        //*        WRITE / '=' #TRANSFER-AMT-OUT-CREF(#C) '=' #TO-SUB '=' #FROM-SUB
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FZ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FZ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FZ.
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   PERFORM #RESET-PARA
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (Global.isEscape()) return;
        pnd_W_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm, "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd,            //Natural: COMPRESS #HLD-EFF-DATE-MM '/' #HLD-EFF-DATE-DD '/' #HLD-EFF-DATE-YY INTO #W-DATE LEAVING NO
            "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy));
                                                                                                                                                                          //Natural: PERFORM #PRINT-REPORT
        sub_Pnd_Print_Report();
        if (condition(Global.isEscape())) {return;}
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(0, "RECORDS READ     =",pnd_Read_File, new ReportEditMask ("ZZZ,ZZ9"));                                                                        //Natural: WRITE 'RECORDS READ     =' #READ-FILE ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "REPORTS PRINTED =>",pnd_Print_Reports, new ReportEditMask ("ZZZ,ZZ9"));                                                                    //Natural: WRITE 'REPORTS PRINTED =>' #PRINT-REPORTS ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*   SKIP (1) 1
        //*   WRITE (1) '-'(57) 59T 'END OF REPORT' 73T '-'(59)
        //* *******************************************************012309**********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ACCT-INFO
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-REPORT
        //*  WRITE (1) #TEXT-TABLE(#I)
        //*    2X #CNV-TABLE(#I,1)(EM=ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,2)(EM=Z,ZZZ,ZZ9)
        //*    3X #CNV-TABLE(#I,3)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,4)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,5)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,6)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,7)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,8)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,9)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,10)(EM=Z,ZZZ,ZZ9)
        //*    1X #CNV-TABLE(#I,11)(EM=Z,ZZZ,ZZ9)
        //*    2X #TOT-ACROSS-CNV(#I)(EM=ZZ,ZZZ,ZZ9)
        //* *FR3. FOR #I = 1 TO 11
        //* *  ADD  #CNV-TABLE(*,#I)  TO #TOT-DOWN-CNV(#I)
        //* *END-FOR
        //* *ADD #TOT-DOWN-CNV(*) TO #TOT-DOWN-TOT
        //* *WRITE (1) 'TOTAL:'
        //* *  2X #TOT-DOWN-CNV(1)(EM=ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(2)(EM=Z,ZZZ,ZZ9)
        //* *  3X #TOT-DOWN-CNV(3)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(4)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(5)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(6)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(7)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(8)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(9)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(10)(EM=Z,ZZZ,ZZ9)
        //* *  1X #TOT-DOWN-CNV(11)(EM=Z,ZZZ,ZZ9)
        //* *  2X #TOT-DOWN-TOT(EM=ZZ,ZZZ,ZZ9)
        //* *ITE (1)    'RECORDS ACCEPTED =' #READ-FILE-1(EM=ZZZ,ZZ9)
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TO-SUB-CONVERT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #TEXT-TABLE-PARA
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FROM-SUB-CONVERT
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-PRINT-REPORT
        //* ******************************************************************
    }
    private void sub_Get_Acct_Info() throws Exception                                                                                                                     //Natural: GET-ACCT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iatn216.class , getCurrentProcessState(), pnd_Nbr_Acct, pnd_Acct_Std_Alpha_Cde.getValue("*"), pnd_Acct_Nme_5.getValue("*"), pnd_Acct_Tckr.getValue("*"),  //Natural: CALLNAT 'IATN216' #NBR-ACCT #ACCT-STD-ALPHA-CDE ( * ) #ACCT-NME-5 ( * ) #ACCT-TCKR ( * ) #ACCT-EFFCTVE-DTE ( * ) #ACCT-UNIT-RTE-IND ( * ) #ACCT-STD-NM-CDE ( * ) #ACCT-RPT-SEQ ( * )
            pnd_Acct_Effctve_Dte.getValue("*"), pnd_Acct_Unit_Rte_Ind.getValue("*"), pnd_Acct_Std_Nm_Cde.getValue("*"), pnd_Acct_Rpt_Seq.getValue("*"));
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Nbr_Acct.equals(getZero())))                                                                                                                    //Natural: IF #NBR-ACCT = 0
        {
            getReports().write(0, "*** SOMETHING IS WRONG WITH EXTERNALIZATION! ***");                                                                                    //Natural: WRITE '*** SOMETHING IS WRONG WITH EXTERNALIZATION! ***'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Print_Report() throws Exception                                                                                                                  //Natural: #PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Print_Reports.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PRINT-REPORTS
        pnd_W_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Mm, "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Dd,            //Natural: COMPRESS #HLD-EFF-DATE-MM '/' #HLD-EFF-DATE-DD '/' #HLD-EFF-DATE-YY INTO #W-DATE LEAVING NO
            "/", pnd_Eff_Date_Hold_Pnd_Hld_Eff_Date_Yy));
        //*  012309
        //*  012309
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FR1:                                                                                                                                                              //Natural: FOR #I = 1 TO #MAX-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Acct)); pnd_I.nadd(1))
        {
            FR2:                                                                                                                                                          //Natural: FOR #J = 1 TO #MAX-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
            {
                ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).setValue(ldaIatl100.getPnd_To_From_Table().getValue(pnd_I,pnd_J));                                    //Natural: ASSIGN #CNV-TABLE ( #I,#J ) := #TO-FROM-TABLE ( #I,#J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIatl100.getPnd_Tot_Across_Cnv().getValue(pnd_I).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,"*"));                                                   //Natural: ADD #CNV-TABLE ( #I,* ) TO #TOT-ACROSS-CNV ( #I )
            FOR01:                                                                                                                                                        //Natural: FOR #J = 1 TO #MAX-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Acct)); pnd_J.nadd(1))
            {
                if (condition(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J).greater(getZero())))                                                                    //Natural: IF #CNV-TABLE ( #I,#J ) GT 0
                {
                    pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Funds().getValue(pnd_I,pnd_J).getSubstring(1,1));                                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-FUNDS ( #I,#J ) ,1,1 )
                    short decideConditionsMet485 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                    if (condition(pnd_Fund_Cde.equals("T")))
                    {
                        decideConditionsMet485++;
                        pnd_Name_To.setValue("STNDRD");                                                                                                                   //Natural: ASSIGN #NAME-TO := 'STNDRD'
                    }                                                                                                                                                     //Natural: WHEN #FUND-CDE = 'G'
                    else if (condition(pnd_Fund_Cde.equals("G")))
                    {
                        decideConditionsMet485++;
                        pnd_Name_To.setValue("GRADED");                                                                                                                   //Natural: ASSIGN #NAME-TO := 'GRADED'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));         //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                        if (condition(pnd_A.greater(getZero())))                                                                                                          //Natural: IF #A GT 0
                        {
                            pnd_Name_To.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                         //Natural: ASSIGN #NAME-TO := #ACCT-NME-5 ( #A )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Name_To.setValue("UNKWN");                                                                                                                //Natural: ASSIGN #NAME-TO := 'UNKWN'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-DECIDE
                    DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_To_Fnd.getValue("*")), new ExamineSearch(pnd_Name_To), new ExamineGivingIndex(pnd_B));         //Natural: EXAMINE #TO-FND ( * ) FOR #NAME-TO GIVING INDEX #B
                    if (condition(pnd_B.greater(getZero())))                                                                                                              //Natural: IF #B GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        FOR02:                                                                                                                                            //Natural: FOR #B 1 #MAX-ACCT
                        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                        {
                            if (condition(pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).equals(" ")))                                                                       //Natural: IF #TO-FND ( #B ) = ' '
                            {
                                pnd_Totals_Table_Pnd_To_Fnd.getValue(pnd_B).setValue(pnd_Name_To);                                                                        //Natural: ASSIGN #TO-FND ( #B ) := #NAME-TO
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Totals_Table_Pnd_To_Tot.getValue(pnd_B).nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                //Natural: ADD #CNV-TABLE ( #I,#J ) TO #TO-TOT ( #B )
                    pnd_Fund_Cde.setValue(ldaIatl100.getPnd_To_From_Funds().getValue(pnd_I,pnd_J).getSubstring(2,1));                                                     //Natural: ASSIGN #FUND-CDE := SUBSTR ( #TO-FROM-FUNDS ( #I,#J ) ,2,1 )
                    short decideConditionsMet511 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-CDE = 'T'
                    if (condition(pnd_Fund_Cde.equals("T")))
                    {
                        decideConditionsMet511++;
                        pnd_Name_From.setValue("STNDRD");                                                                                                                 //Natural: ASSIGN #NAME-FROM := 'STNDRD'
                    }                                                                                                                                                     //Natural: WHEN #FUND-CDE = 'G'
                    else if (condition(pnd_Fund_Cde.equals("G")))
                    {
                        decideConditionsMet511++;
                        pnd_Name_From.setValue("GRADED");                                                                                                                 //Natural: ASSIGN #NAME-FROM := 'GRADED'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Acct_Std_Alpha_Cde.getValue("*")), new ExamineSearch(pnd_Fund_Cde), new ExamineGivingIndex(pnd_A));         //Natural: EXAMINE #ACCT-STD-ALPHA-CDE ( * ) FOR #FUND-CDE GIVING INDEX #A
                        if (condition(pnd_A.greater(getZero())))                                                                                                          //Natural: IF #A GT 0
                        {
                            pnd_Name_From.setValue(pnd_Acct_Nme_5.getValue(pnd_A));                                                                                       //Natural: ASSIGN #NAME-FROM := #ACCT-NME-5 ( #A )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Name_From.setValue("UNKWN");                                                                                                              //Natural: ASSIGN #NAME-FROM := 'UNKWN'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-DECIDE
                    DbsUtil.examine(new ExamineSource(pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*")), new ExamineSearch(pnd_Name_From), new ExamineGivingIndex(pnd_B));       //Natural: EXAMINE #FR-FND ( * ) FOR #NAME-FROM GIVING INDEX #B
                    if (condition(pnd_B.greater(getZero())))                                                                                                              //Natural: IF #B GT 0
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #B 1 #MAX-ACCT
                        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Acct)); pnd_B.nadd(1))
                        {
                            if (condition(pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).equals(" ")))                                                                       //Natural: IF #FR-FND ( #B ) = ' '
                            {
                                pnd_Totals_Table_Pnd_Fr_Fnd.getValue(pnd_B).setValue(pnd_Name_From);                                                                      //Natural: ASSIGN #FR-FND ( #B ) := #NAME-FROM
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Totals_Table_Pnd_Fr_Tot.getValue(pnd_B).nsubtract(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                           //Natural: SUBTRACT #CNV-TABLE ( #I,#J ) FROM #FR-TOT ( #B )
                    pnd_Sub_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                                //Natural: ADD #CNV-TABLE ( #I,#J ) TO #SUB-TOT
                    pnd_Ovrall_Tot.nadd(ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J));                                                                             //Natural: ADD #CNV-TABLE ( #I,#J ) TO #OVRALL-TOT
                    getReports().write(1, ReportOption.NOTITLE,pnd_Name_From,new TabSetting(11),pnd_Name_To,new TabSetting(22),ldaIatl100.getPnd_Cnv_Table().getValue(pnd_I,pnd_J),  //Natural: WRITE ( 1 ) #NAME-FROM 11T #NAME-TO 22T #CNV-TABLE ( #I,#J ) ( EM = Z,ZZZ,ZZ9 )
                        new ReportEditMask ("Z,ZZZ,ZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Sub_Tot.greater(getZero())))                                                                                                                //Natural: IF #SUB-TOT GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Subtotal:",new TabSetting(18),pnd_Sub_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) / 'Subtotal:' 18T #SUB-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FR1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FR1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Sub_Tot.reset();                                                                                                                                      //Natural: RESET #SUB-TOT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total   :",new TabSetting(18),pnd_Ovrall_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) // 'Total   :' 18T #OVRALL-TOT ( EM = Z,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #SUB-TOT #OVRALL-TOT #TO-FND ( * ) #TO-TOT ( * ) #FR-FND ( * ) #FR-TOT ( * ) #NET-FND ( * ) #NET-AMT ( * )
        pnd_Sub_Tot.reset();
        pnd_Ovrall_Tot.reset();
        pnd_Totals_Table_Pnd_To_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_To_Tot.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Fnd.getValue("*").reset();
        pnd_Totals_Table_Pnd_Fr_Tot.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Fnd.getValue("*").reset();
        pnd_Net_Total_Pnd_Net_Amt.getValue("*").reset();
        //*                                                      /* 011609 END
    }
    private void sub_Pnd_To_Sub_Convert() throws Exception                                                                                                                //Natural: #TO-SUB-CONVERT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*   WRITE 'TO-SUB-CONVERT' '=' #FUND-CD-TO
        ldaIatl100.getPnd_To_Sub().reset();                                                                                                                               //Natural: RESET #TO-SUB
        short decideConditionsMet558 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #FUND-CD-TO;//Natural: VALUE 'G'
        if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("G"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(1);                                                                                                                       //Natural: MOVE 1 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("T"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(2);                                                                                                                       //Natural: MOVE 2 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("C"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(3);                                                                                                                       //Natural: MOVE 3 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("M"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(4);                                                                                                                       //Natural: MOVE 4 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("S"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(5);                                                                                                                       //Natural: MOVE 5 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("B"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(6);                                                                                                                       //Natural: MOVE 6 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("W"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(7);                                                                                                                       //Natural: MOVE 7 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("L"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(8);                                                                                                                       //Natural: MOVE 8 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("E"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(9);                                                                                                                       //Natural: MOVE 9 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("R"))))
        {
            decideConditionsMet558++;
            ldaIatl100.getPnd_To_Sub().setValue(10);                                                                                                                      //Natural: MOVE 10 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("I"))))
        {
            decideConditionsMet558++;
            //*  012309
            ldaIatl100.getPnd_To_Sub().setValue(11);                                                                                                                      //Natural: MOVE 11 TO #TO-SUB
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_To().equals("D"))))
        {
            decideConditionsMet558++;
            //*  012309
            ldaIatl100.getPnd_To_Sub().setValue(12);                                                                                                                      //Natural: MOVE 12 TO #TO-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "FUND CODE TO ERROR",ldaIatl100.getPnd_Fund_Cd_To());                                                                                   //Natural: WRITE 'FUND CODE TO ERROR' #FUND-CD-TO
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WRITE '=' #TO-SUB
    }
    private void sub_Pnd_Text_Table_Para() throws Exception                                                                                                               //Natural: #TEXT-TABLE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_Text_Table().getValue(1).setValue("GRADED");                                                                                                    //Natural: MOVE 'GRADED' TO #TEXT-TABLE ( 1 )
        ldaIatl100.getPnd_Text_Table().getValue(2).setValue("STNDRD");                                                                                                    //Natural: MOVE 'STNDRD' TO #TEXT-TABLE ( 2 )
        ldaIatl100.getPnd_Text_Table().getValue(3).setValue("STOCK ");                                                                                                    //Natural: MOVE 'STOCK ' TO #TEXT-TABLE ( 3 )
        ldaIatl100.getPnd_Text_Table().getValue(4).setValue("MMA   ");                                                                                                    //Natural: MOVE 'MMA   ' TO #TEXT-TABLE ( 4 )
        ldaIatl100.getPnd_Text_Table().getValue(5).setValue("SOCIAL");                                                                                                    //Natural: MOVE 'SOCIAL' TO #TEXT-TABLE ( 5 )
        ldaIatl100.getPnd_Text_Table().getValue(6).setValue("BOND  ");                                                                                                    //Natural: MOVE 'BOND  ' TO #TEXT-TABLE ( 6 )
        ldaIatl100.getPnd_Text_Table().getValue(7).setValue("GLOBAL");                                                                                                    //Natural: MOVE 'GLOBAL' TO #TEXT-TABLE ( 7 )
        ldaIatl100.getPnd_Text_Table().getValue(8).setValue("GROWTH");                                                                                                    //Natural: MOVE 'GROWTH' TO #TEXT-TABLE ( 8 )
        ldaIatl100.getPnd_Text_Table().getValue(9).setValue("EQUITY");                                                                                                    //Natural: MOVE 'EQUITY' TO #TEXT-TABLE ( 9 )
        ldaIatl100.getPnd_Text_Table().getValue(10).setValue("REA   ");                                                                                                   //Natural: MOVE 'REA   ' TO #TEXT-TABLE ( 10 )
        ldaIatl100.getPnd_Text_Table().getValue(11).setValue("ILB   ");                                                                                                   //Natural: MOVE 'ILB   ' TO #TEXT-TABLE ( 11 )
    }
    private void sub_Pnd_From_Sub_Convert() throws Exception                                                                                                              //Natural: #FROM-SUB-CONVERT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*   WRITE 'FROM-SUB-CONVERT' '=' #FUND-CD-FROM
        ldaIatl100.getPnd_From_Sub().reset();                                                                                                                             //Natural: RESET #FROM-SUB
        short decideConditionsMet609 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #FUND-CD-FROM;//Natural: VALUE 'G'
        if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("G"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(1);                                                                                                                     //Natural: MOVE 1 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("T"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(2);                                                                                                                     //Natural: MOVE 2 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("C"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(3);                                                                                                                     //Natural: MOVE 3 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("M"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(4);                                                                                                                     //Natural: MOVE 4 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("S"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(5);                                                                                                                     //Natural: MOVE 5 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("B"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(6);                                                                                                                     //Natural: MOVE 6 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("W"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(7);                                                                                                                     //Natural: MOVE 7 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("L"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(8);                                                                                                                     //Natural: MOVE 8 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("E"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(9);                                                                                                                     //Natural: MOVE 9 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("R"))))
        {
            decideConditionsMet609++;
            ldaIatl100.getPnd_From_Sub().setValue(10);                                                                                                                    //Natural: MOVE 10 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("I"))))
        {
            decideConditionsMet609++;
            //*  012309
            ldaIatl100.getPnd_From_Sub().setValue(11);                                                                                                                    //Natural: MOVE 11 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((ldaIatl100.getPnd_Fund_Cd_From().equals("D"))))
        {
            decideConditionsMet609++;
            //*  012309
            ldaIatl100.getPnd_From_Sub().setValue(12);                                                                                                                    //Natural: MOVE 12 TO #FROM-SUB
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, "ERROR IN FROM FUND",ldaIatl100.getPnd_Fund_Cd_From());                                                                                 //Natural: WRITE 'ERROR IN FROM FUND' #FUND-CD-FROM
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   WRITE '=' #FROM-SUB
    }
    private void sub_Pnd_Reset_Print_Report() throws Exception                                                                                                            //Natural: #RESET-PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        ldaIatl100.getPnd_Cnv_Table().getValue("*","*").reset();                                                                                                          //Natural: RESET #CNV-TABLE ( *,* ) #TO-FROM-TABLE ( *,* ) #TOT-ACROSS-CNV ( * ) #TOT-DOWN-CNV ( * ) #SUB-TOT-DOWN-ACROSS ( * ) #SUB-TOT-DOWN-ACROSS-TOT #TOT-DOWN-TOT #READ-FILE-1
        ldaIatl100.getPnd_To_From_Table().getValue("*","*").reset();
        ldaIatl100.getPnd_Tot_Across_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Tot_Down_Cnv().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across().getValue("*").reset();
        ldaIatl100.getPnd_Sub_Tot_Down_Across_Tot().reset();
        ldaIatl100.getPnd_Tot_Down_Tot().reset();
        pnd_Read_File_1.reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(49),"NUMBER OF TRANSFER REQUESTS ENTERED",new                //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 49T 'NUMBER OF TRANSFER REQUESTS ENTERED' 124T 'PAGE 1'
                        TabSetting(124),"PAGE 1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(46),"TRANSFER REQUESTS EFFECTIVE DATE",pnd_W_Date);                                         //Natural: WRITE ( 1 ) 46T 'TRANSFER REQUESTS EFFECTIVE DATE' #W-DATE
                    //* *ITE (1) / 10T '-'(43)  '-'(9) ' FROM  ' '-'(57)     /* 012309 START
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"FROM",new TabSetting(11),"TO",new TabSetting(26),"COUNT");                                        //Natural: WRITE ( 1 ) / 'FROM' 11T 'TO' 26T 'COUNT'
                    //*  WRITE (1) 001T 'TO:'
                    //*    010T 'GRADED'
                    //*    020T 'STNDRD'
                    //*    033T 'STOCK'
                    //*    045T 'MMA'
                    //*    052T 'SOCIAL'
                    //*    064T 'BOND'
                    //*    072T 'GLOBAL'
                    //*    082T 'GROWTH'
                    //*    092T 'EQUITY'
                    //*    105T 'REA'
                    //*    112T '   ILB'
                    //*    125T 'TOTAL'                                      /* 012309 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
