/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:34 PM
**        * FROM NATURAL PROGRAM : Iaap321
************************************************************
**        * FILE NAME            : Iaap321.java
**        * CLASS NAME           : Iaap321
**        * INSTANCE NAME        : Iaap321
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: NET CHANGE MAILING LABEL INDICATOR
**SAG SYSTEM: IASTPA
************************************************************************
* PROGRAM  : IAAP321
* SYSTEM   : IASTPA
* TITLE    : FLIP MAILING LABEL IND ON NET CHANGE FILE
* GENERATED: FEB 05,96 AT 04:59 PM
* FUNCTION : THIS PROGRAM WILL READ THE NET CHANGE FILE WHICH IS SORTED
*          | BY PIN (ASCENDING) AND MAILING LABEL IND (DESCENDING).
*          | MAILING LABEL VALUES ARE
*          |    1 - MAILING LABEL (NO NET CHANGE OCCURRED FOR CONTRACT)
*          |    2 - NET CHANGE    (NET CHANGE OCCURRED FOR CONTRACT)
*          | IF ANY OF THE CONTRACTS OF A PIN HAVE A NET CHANGE RECORD
*          | THEN ALL MAILING LABEL RECORDS FOR THE PIN MUST BE CHANGED
*          | TO A NET CHANGE RECORD.
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* ---------+---------+----------------------------------------------
* 07/13/00 | JHH     | PA SELECT/SPIA - NEW FIELDS ON IAAA323
* 04/21/17 | SAI K   | PIN EXPANSION
* ______________________________________________
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap321 extends BLNatBase
{
    // Data Areas
    private PdaIaaa323 pdaIaaa323;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Rec_Chgd_Cnt;
    private DbsField pnd_Rec_Not_Chgd_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Read_Lbl_Cnt;
    private DbsField pnd_Read_Chg_Cnt;
    private DbsField pnd_Write_Lbl_Cnt;
    private DbsField pnd_Write_Chg_Cnt;
    private DbsField pnd_Neg_Rec_Chgd_Cnt;
    private DbsField pnd_Prev_Pin;
    private DbsField pnd_Mailing_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa323 = new PdaIaaa323(localVariables);

        // Local Variables
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 7);
        pnd_Rec_Chgd_Cnt = localVariables.newFieldInRecord("pnd_Rec_Chgd_Cnt", "#REC-CHGD-CNT", FieldType.NUMERIC, 7);
        pnd_Rec_Not_Chgd_Cnt = localVariables.newFieldInRecord("pnd_Rec_Not_Chgd_Cnt", "#REC-NOT-CHGD-CNT", FieldType.NUMERIC, 7);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.NUMERIC, 7);
        pnd_Read_Lbl_Cnt = localVariables.newFieldInRecord("pnd_Read_Lbl_Cnt", "#READ-LBL-CNT", FieldType.NUMERIC, 7);
        pnd_Read_Chg_Cnt = localVariables.newFieldInRecord("pnd_Read_Chg_Cnt", "#READ-CHG-CNT", FieldType.NUMERIC, 7);
        pnd_Write_Lbl_Cnt = localVariables.newFieldInRecord("pnd_Write_Lbl_Cnt", "#WRITE-LBL-CNT", FieldType.NUMERIC, 7);
        pnd_Write_Chg_Cnt = localVariables.newFieldInRecord("pnd_Write_Chg_Cnt", "#WRITE-CHG-CNT", FieldType.NUMERIC, 7);
        pnd_Neg_Rec_Chgd_Cnt = localVariables.newFieldInRecord("pnd_Neg_Rec_Chgd_Cnt", "#NEG-REC-CHGD-CNT", FieldType.NUMERIC, 7);
        pnd_Prev_Pin = localVariables.newFieldInRecord("pnd_Prev_Pin", "#PREV-PIN", FieldType.NUMERIC, 12);
        pnd_Mailing_Ind = localVariables.newFieldInRecord("pnd_Mailing_Ind", "#MAILING-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Read_Cnt.setInitialValue(0);
        pnd_Rec_Chgd_Cnt.setInitialValue(0);
        pnd_Rec_Not_Chgd_Cnt.setInitialValue(0);
        pnd_Write_Cnt.setInitialValue(0);
        pnd_Read_Lbl_Cnt.setInitialValue(0);
        pnd_Read_Chg_Cnt.setInitialValue(0);
        pnd_Write_Lbl_Cnt.setInitialValue(0);
        pnd_Write_Chg_Cnt.setInitialValue(0);
        pnd_Neg_Rec_Chgd_Cnt.setInitialValue(0);
        pnd_Prev_Pin.setInitialValue(0);
        pnd_Mailing_Ind.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap321() throws Exception
    {
        super("Iaap321");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ------------------------------------------------
        //* * *ERROR-TA := 'INFP9000'                                                                                                                                     //Natural: FORMAT ( 0 ) LS = 132 PS = 66;//Natural: FORMAT ( 1 ) LS = 132 PS = 66
        //* ******************** START OF MAIN PROGRAM LOGIC & LOOP ***************
        READ_NETCHG_FILE:                                                                                                                                                 //Natural: READ WORK FILE 1 IAAA320
        while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320())))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: AT END OF DATA;//Natural: ADD 1 TO #READ-CNT
            if (condition(pdaIaaa323.getIaaa320_Mailing_Label_Ind().equals("1")))                                                                                         //Natural: IF IAAA320.MAILING-LABEL-IND = '1'
            {
                pnd_Read_Lbl_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-LBL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Read_Chg_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CHG-CNT
            }                                                                                                                                                             //Natural: END-IF
            //*  TAKE MAILING IND VAL
            //*  OF FIRST CONTRACT IN
            //*  THE SET OF PINS.
            if (condition(pnd_Prev_Pin.notEquals(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                                       //Natural: IF #PREV-PIN NE IAAA320.PIN-NBR
            {
                pnd_Mailing_Ind.setValue(pdaIaaa323.getIaaa320_Mailing_Label_Ind());                                                                                      //Natural: ASSIGN #MAILING-IND := IAAA320.MAILING-LABEL-IND
                pnd_Prev_Pin.setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                                   //Natural: ASSIGN #PREV-PIN := IAAA320.PIN-NBR
                //*  PROPAGATE THAT VALUE
            }                                                                                                                                                             //Natural: END-IF
            //*  TO OTHER RECORDS.
            if (condition(pdaIaaa323.getIaaa320_Mailing_Label_Ind().notEquals(pnd_Mailing_Ind)))                                                                          //Natural: IF IAAA320.MAILING-LABEL-IND NE #MAILING-IND
            {
                pdaIaaa323.getIaaa320_Mailing_Label_Ind().setValue(pnd_Mailing_Ind);                                                                                      //Natural: ASSIGN IAAA320.MAILING-LABEL-IND := #MAILING-IND
                pnd_Rec_Chgd_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-CHGD-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rec_Not_Chgd_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REC-NOT-CHGD-CNT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Write_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WRITE-CNT
            if (condition(pdaIaaa323.getIaaa320_Mailing_Label_Ind().equals("1")))                                                                                         //Natural: IF IAAA320.MAILING-LABEL-IND = '1'
            {
                pnd_Write_Lbl_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WRITE-LBL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Write_Chg_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WRITE-CHG-CNT
            }                                                                                                                                                             //Natural: END-IF
            //*  FINAL NET CHANGE FILE
            getWorkFiles().write(2, false, pdaIaaa323.getIaaa320());                                                                                                      //Natural: WRITE WORK FILE 2 IAAA320
        }                                                                                                                                                                 //Natural: END-WORK
        READ_NETCHG_FILE_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "READ_NETCHG_FILE");                                                                          //Natural: ESCAPE BOTTOM ( READ-NETCHG-FILE. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*   WRITE CONTROL REPORT
        pnd_Neg_Rec_Chgd_Cnt.compute(new ComputeParameters(false, pnd_Neg_Rec_Chgd_Cnt), pnd_Rec_Chgd_Cnt.multiply(-1));                                                  //Natural: ASSIGN #NEG-REC-CHGD-CNT := #REC-CHGD-CNT * -1
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 41T 'IA NET CHANGE FILE SELECTION AND CONTROL PROCESS' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *DATX 46T 'FLIP NET CHANGE INDICATOR CONTROL RPT' 120T *TIMX
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"RECORDS READ:                     ",NEWLINE,"      LABEL RECORDS READ:         ",pnd_Read_Lbl_Cnt,         //Natural: WRITE ( 1 ) /// / 'RECORDS READ:                     ' / '      LABEL RECORDS READ:         ' #READ-LBL-CNT ( EM = -Z,ZZZ,ZZ9 ) / '      NET CHANGE RECORDS READ:    ' #READ-CHG-CNT ( EM = -Z,ZZZ,ZZ9 ) / '                                    ---------' / 'TOTAL RECORDS READ:               ' #READ-CNT ( EM = -Z,ZZZ,ZZ9 ) ///'LABEL RECORDS PROCESSED:          ' / '      LABELS RECORDS READ:        ' #READ-LBL-CNT ( EM = -Z,ZZZ,ZZ9 ) / '      RECORDS CHANGED:            ' #NEG-REC-CHGD-CNT ( EM = -Z,ZZZ,ZZ9 ) / '                                    ---------' / 'TOTAL LABELS RECORDS WRITTEN:     ' #WRITE-LBL-CNT ( EM = -Z,ZZZ,ZZ9 ) ///'NET CHANGE RECORDS PROCESSED:     ' / '      NET CHANGE RECORDS READ:    ' #READ-CHG-CNT ( EM = -Z,ZZZ,ZZ9 ) / '      RECORDS CHANGED:            ' #REC-CHGD-CNT ( EM = -Z,ZZZ,ZZ9 ) / '                                    ---------' / 'TOTAL NET CHANGE RECORDS WRITTEN: ' #WRITE-CHG-CNT ( EM = -Z,ZZZ,ZZ9 ) ///'RECORDS WRITTEN:                  ' / '      LABELS RECORDS WRITTEN:     ' #WRITE-LBL-CNT ( EM = -Z,ZZZ,ZZ9 ) / '      NET CHANGE RECORDS WRITTEN: ' #WRITE-CHG-CNT ( EM = -Z,ZZZ,ZZ9 ) / '                                    ---------' / 'TOTAL RECORDS WRITTEN             ' #WRITE-CNT ( EM = -Z,ZZZ,ZZ9 )
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"      NET CHANGE RECORDS READ:    ",pnd_Read_Chg_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                                    ---------",NEWLINE,"TOTAL RECORDS READ:               ",pnd_Read_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"LABEL RECORDS PROCESSED:          ",NEWLINE,"      LABELS RECORDS READ:        ",pnd_Read_Lbl_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"      RECORDS CHANGED:            ",pnd_Neg_Rec_Chgd_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                                    ---------",NEWLINE,"TOTAL LABELS RECORDS WRITTEN:     ",pnd_Write_Lbl_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"NET CHANGE RECORDS PROCESSED:     ",NEWLINE,"      NET CHANGE RECORDS READ:    ",pnd_Read_Chg_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"      RECORDS CHANGED:            ",pnd_Rec_Chgd_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                                    ---------",NEWLINE,"TOTAL NET CHANGE RECORDS WRITTEN: ",pnd_Write_Chg_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"RECORDS WRITTEN:                  ",NEWLINE,"      LABELS RECORDS WRITTEN:     ",pnd_Write_Lbl_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"      NET CHANGE RECORDS WRITTEN: ",pnd_Write_Chg_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                                    ---------",NEWLINE,"TOTAL RECORDS WRITTEN             ",pnd_Write_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66");
        Global.format(1, "LS=132 PS=66");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(41),"IA NET CHANGE FILE SELECTION AND CONTROL PROCESS",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(46),"FLIP NET CHANGE INDICATOR CONTROL RPT",new 
            TabSetting(120),Global.getTIMX());
    }
}
