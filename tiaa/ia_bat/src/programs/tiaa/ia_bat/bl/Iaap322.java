/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:23:36 PM
**        * FROM NATURAL PROGRAM : Iaap322
************************************************************
**        * FILE NAME            : Iaap322.java
**        * CLASS NAME           : Iaap322
**        * INSTANCE NAME        : Iaap322
************************************************************
************************************************************************
* PROGRAM    : IAAP322
* SYSTEM     : IASTPA
* TITLE      : GENERATE IA MANDATORY STATE LETTERS
* GENERATION : FEB 26, 1996
* FUNCTION   : DRIVER PROGRAM FOR
*            | MANDATORY STATE LETTERS (MST)
* 12/15/00   | RCC - NORTH CAROLINA AS A NEW MANDATORY STATE
* 07/21/05   | RAMANA ANNE  - DELAWARE,KANSAS AS A NEW MANDATORY STATE
*            |                AND REMOVE SIGNATURE FROM LETTERS
* 09/26/05   | ALTHEA YOUNG - ADD MARYLAND AS A NEW MANDATORY STATE.
*            |              - ADDED TEST FLAG LOGIC TO MODULES IAAN322
*            |                AND IAAP324.  TO TEST, SET #TEST FLAGS TO
*            |                'TRUE', AND POPULATE LOCAL PRINTER ID IN
*            |                IAAN322.  #TEST MUST BE 'FALSE' IN PROD!!!
* 01/09/06     RAMANA ANNE  - NEBRASKA AND ARKANSAS AS A NEW MANDATORY
*                             STATES
* 03/25/15     MUKHERR      - FIX IAAP322 SO THAT IT DOES NOT PASS ANY
*                             REQUESTS TO POST THAT CONTAIN A BLANK/
*                             NULLVALUE FOR FROM-STATE-NME OR
*                             TO-STATE-NME. TAGGED-MUKHERR
* 04/25/17   | SAI K        - PIN EXPANSION
* 10/09/18     GHOSHJ       - ADDED CONNECTICUT AS A NEW MANDATORY STATE
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap322 extends BLNatBase
{
    // Data Areas
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaIaaa323 pdaIaaa323;
    private PdaIaaa322 pdaIaaa322;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Prev_Pin;
    private DbsField pnd_First_Time;
    private DbsField pnd_Contract_Cnt;
    private DbsField pnd_Contract_Cnt_Minus1;
    private DbsField pnd_Curr_Mstate;
    private DbsField pnd_Prior_Mstate;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Vol_Man_Cnt;
    private DbsField pnd_Man_Man_Cnt;
    private DbsField pnd_Man_Vol_Cnt;
    private DbsField pnd_Total_Letters_Cnt;
    private DbsField pnd_Post_Error_Cnt;
    private DbsField pnd_Get_State_Info_Curr;
    private DbsField pnd_Reached_State_Tbl_Max;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Warning;
    private DbsField pnd_Post_Error;
    private DbsField pnd_Hold_Cntrct;
    private DbsField pnd_Hold_Cntrct_Cmpy_Cde;

    private DataAccessProgramView vw_nst_Table_Entry;
    private DbsField nst_Table_Entry_Entry_Actve_Ind;
    private DbsField nst_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField nst_Table_Entry_Entry_Table_Sub_Id;
    private DbsField nst_Table_Entry_Entry_Cde;
    private DbsField nst_Table_Entry_Entry_Dscrptn_Txt;
    private DbsField nst_Table_Entry_Entry_User_Id;
    private DbsField pnd_Table_Key;

    private DbsGroup pnd_Table_Key__R_Field_1;
    private DbsField pnd_Table_Key_Pnd_Table_Nbr;
    private DbsField pnd_Table_Key_Pnd_Table_Sub;
    private DbsField pnd_Table_Key_Pnd_Entry_Code;

    private DbsGroup pnd_Table_Key__R_Field_2;
    private DbsField pnd_Table_Key_Pnd_Citizenship_Cde;

    private DbsGroup pnd_Mstates;
    private DbsField pnd_Mstates_Pnd_Mstate_Nbr;
    private DbsField pnd_State_Tbl_Cnt;
    private DbsField pnd_State_Tbl_Ndx;

    private DbsGroup pnd_State_Tbl;
    private DbsField pnd_State_Tbl_Pnd_State_Nbr;
    private DbsField pnd_State_Tbl_Pnd_State_Cde;
    private DbsField pnd_State_Tbl_Pnd_State_Desc;

    private DataAccessProgramView vw_table_Entry_Db48;
    private DbsField table_Entry_Db48_Entry_Actve_Ind;
    private DbsField table_Entry_Db48_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Db48_Entry_Cde;
    private DbsField table_Entry_Db48_Entry_Dscrptn_Txt;
    private DbsField table_Entry_Db48_Entry_Updte_Tmstmp_Tme;
    private DbsField table_Entry_Db48_Entry_Updte_Tmstmp_Dte;
    private DbsField table_Entry_Db48_Entry_Updte_User_Id_Nme;
    private DbsField table_Entry_Db48_Entry_Mgrtn_Ind;
    private DbsField table_Entry_Db48_Entry_Mgrtn_Tmstmp_Tme;
    private DbsField table_Entry_Db48_Entry_Mgrtn_Tmstmp_Dte;
    private DbsField table_Entry_Db48_Entry_Mgrtn_User_Id_Nme;
    private DbsField table_Entry_Db48_Entry_Mgrtn_Rqst_Tmstmp_Dte;
    private DbsField table_Entry_Db48_Entry_Mgrtn_Rqst_User_Id_Nme;
    private DbsField pnd_State_Key;

    private DbsGroup pnd_State_Key__R_Field_3;
    private DbsField pnd_State_Key_Pnd_State_Tbid_Key;
    private DbsField pnd_State_Key_Pnd_State_Nbr_Key;
    private DbsField pnd_Empty;
    private DbsField pnd_Roll;

    private DataAccessProgramView vw_pay;
    private DbsField pay_Cntrct_Ppcn_Nbr;
    private DbsField pay_Cntrct_Ac_Lt_10yrs;

    private DataAccessProgramView vw_contract;
    private DbsField contract_Cntrct_Ppcn_Nbr;
    private DbsField contract_Cntrct_Issue_Dte;

    private DataAccessProgramView vw_role;
    private DbsField role_Cntrct_Part_Ppcn_Nbr;
    private DbsField role_Cntrct_Part_Payee_Cde;
    private DbsField role_Cntrct_Final_Per_Pay_Dte;
    private DbsField role_Cntrct_Mode_Ind;
    private DbsField pnd_Contract_Payee_Key;

    private DbsGroup pnd_Contract_Payee_Key__R_Field_4;
    private DbsField pnd_Contract_Payee_Key_Pnd_Contract_Key;
    private DbsField pnd_Contract_Payee_Key_Pnd_Payee_Key;
    private DbsField pnd_Elgble;
    private DbsField pnd_Start_Dte;

    private DbsGroup pnd_Start_Dte__R_Field_5;
    private DbsField pnd_Start_Dte_Pnd_S_Yyyy;
    private DbsField pnd_Start_Dte_Pnd_S_Mm;
    private DbsField pnd_End_Dte;

    private DbsGroup pnd_End_Dte__R_Field_6;
    private DbsField pnd_End_Dte_Pnd_E_Yyyy;
    private DbsField pnd_End_Dte_Pnd_E_Mm;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaIaaa323 = new PdaIaaa323(localVariables);
        pdaIaaa322 = new PdaIaaa322(localVariables);

        // Local Variables
        pnd_Prev_Pin = localVariables.newFieldInRecord("pnd_Prev_Pin", "#PREV-PIN", FieldType.NUMERIC, 12);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Contract_Cnt = localVariables.newFieldInRecord("pnd_Contract_Cnt", "#CONTRACT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Contract_Cnt_Minus1 = localVariables.newFieldInRecord("pnd_Contract_Cnt_Minus1", "#CONTRACT-CNT-MINUS1", FieldType.PACKED_DECIMAL, 7);
        pnd_Curr_Mstate = localVariables.newFieldInRecord("pnd_Curr_Mstate", "#CURR-MSTATE", FieldType.BOOLEAN, 1);
        pnd_Prior_Mstate = localVariables.newFieldInRecord("pnd_Prior_Mstate", "#PRIOR-MSTATE", FieldType.BOOLEAN, 1);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Vol_Man_Cnt = localVariables.newFieldInRecord("pnd_Vol_Man_Cnt", "#VOL-MAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Man_Man_Cnt = localVariables.newFieldInRecord("pnd_Man_Man_Cnt", "#MAN-MAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Man_Vol_Cnt = localVariables.newFieldInRecord("pnd_Man_Vol_Cnt", "#MAN-VOL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Letters_Cnt = localVariables.newFieldInRecord("pnd_Total_Letters_Cnt", "#TOTAL-LETTERS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Post_Error_Cnt = localVariables.newFieldInRecord("pnd_Post_Error_Cnt", "#POST-ERROR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Get_State_Info_Curr = localVariables.newFieldInRecord("pnd_Get_State_Info_Curr", "#GET-STATE-INFO-CURR", FieldType.BOOLEAN, 1);
        pnd_Reached_State_Tbl_Max = localVariables.newFieldInRecord("pnd_Reached_State_Tbl_Max", "#REACHED-STATE-TBL-MAX", FieldType.BOOLEAN, 1);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 80);
        pnd_Warning = localVariables.newFieldInRecord("pnd_Warning", "#WARNING", FieldType.BOOLEAN, 1);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.BOOLEAN, 1);
        pnd_Hold_Cntrct = localVariables.newFieldInRecord("pnd_Hold_Cntrct", "#HOLD-CNTRCT", FieldType.STRING, 9);
        pnd_Hold_Cntrct_Cmpy_Cde = localVariables.newFieldInRecord("pnd_Hold_Cntrct_Cmpy_Cde", "#HOLD-CNTRCT-CMPY-CDE", FieldType.STRING, 1);

        vw_nst_Table_Entry = new DataAccessProgramView(new NameInfo("vw_nst_Table_Entry", "NST-TABLE-ENTRY"), "NST_TABLE_ENTRY", "NST_TABLE_ENTRY");
        nst_Table_Entry_Entry_Actve_Ind = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        nst_Table_Entry_Entry_Table_Id_Nbr = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        nst_Table_Entry_Entry_Table_Id_Nbr.setDdmHeader("TABLE ID");
        nst_Table_Entry_Entry_Table_Sub_Id = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        nst_Table_Entry_Entry_Cde = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        nst_Table_Entry_Entry_Cde.setDdmHeader("ENTRY CODE");
        nst_Table_Entry_Entry_Dscrptn_Txt = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        nst_Table_Entry_Entry_User_Id = vw_nst_Table_Entry.getRecord().newFieldInGroup("nst_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        registerRecord(vw_nst_Table_Entry);

        pnd_Table_Key = localVariables.newFieldInRecord("pnd_Table_Key", "#TABLE-KEY", FieldType.STRING, 28);

        pnd_Table_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Key__R_Field_1", "REDEFINE", pnd_Table_Key);
        pnd_Table_Key_Pnd_Table_Nbr = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Nbr", "#TABLE-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Key_Pnd_Table_Sub = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Sub", "#TABLE-SUB", FieldType.STRING, 2);
        pnd_Table_Key_Pnd_Entry_Code = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Entry_Code", "#ENTRY-CODE", FieldType.STRING, 20);

        pnd_Table_Key__R_Field_2 = pnd_Table_Key__R_Field_1.newGroupInGroup("pnd_Table_Key__R_Field_2", "REDEFINE", pnd_Table_Key_Pnd_Entry_Code);
        pnd_Table_Key_Pnd_Citizenship_Cde = pnd_Table_Key__R_Field_2.newFieldInGroup("pnd_Table_Key_Pnd_Citizenship_Cde", "#CITIZENSHIP-CDE", FieldType.STRING, 
            2);

        pnd_Mstates = localVariables.newGroupArrayInRecord("pnd_Mstates", "#MSTATES", new DbsArrayController(1, 18));
        pnd_Mstates_Pnd_Mstate_Nbr = pnd_Mstates.newFieldInGroup("pnd_Mstates_Pnd_Mstate_Nbr", "#MSTATE-NBR", FieldType.STRING, 2);
        pnd_State_Tbl_Cnt = localVariables.newFieldInRecord("pnd_State_Tbl_Cnt", "#STATE-TBL-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_State_Tbl_Ndx = localVariables.newFieldInRecord("pnd_State_Tbl_Ndx", "#STATE-TBL-NDX", FieldType.PACKED_DECIMAL, 3);

        pnd_State_Tbl = localVariables.newGroupArrayInRecord("pnd_State_Tbl", "#STATE-TBL", new DbsArrayController(1, 100));
        pnd_State_Tbl_Pnd_State_Nbr = pnd_State_Tbl.newFieldInGroup("pnd_State_Tbl_Pnd_State_Nbr", "#STATE-NBR", FieldType.STRING, 2);
        pnd_State_Tbl_Pnd_State_Cde = pnd_State_Tbl.newFieldInGroup("pnd_State_Tbl_Pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 2);
        pnd_State_Tbl_Pnd_State_Desc = pnd_State_Tbl.newFieldInGroup("pnd_State_Tbl_Pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 14);

        vw_table_Entry_Db48 = new DataAccessProgramView(new NameInfo("vw_table_Entry_Db48", "TABLE-ENTRY-DB48"), "TABLE_ENTRY_DB48", "TABLE_DATA_1");
        table_Entry_Db48_Entry_Actve_Ind = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Db48_Entry_Table_Id_Nbr = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Db48_Entry_Table_Id_Nbr.setDdmHeader("TABLE ID");
        table_Entry_Db48_Entry_Cde = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "ENTRY_CDE");
        table_Entry_Db48_Entry_Cde.setDdmHeader("ENTRY CODE");
        table_Entry_Db48_Entry_Dscrptn_Txt = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        table_Entry_Db48_Entry_Updte_Tmstmp_Tme = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Updte_Tmstmp_Tme", "ENTRY-UPDTE-TMSTMP-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_UPDTE_TMSTMP_TME");
        table_Entry_Db48_Entry_Updte_Tmstmp_Dte = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Updte_Tmstmp_Dte", "ENTRY-UPDTE-TMSTMP-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_UPDTE_TMSTMP_DTE");
        table_Entry_Db48_Entry_Updte_User_Id_Nme = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Updte_User_Id_Nme", "ENTRY-UPDTE-USER-ID-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_UPDTE_USER_ID_NME");
        table_Entry_Db48_Entry_Mgrtn_Ind = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_Ind", "ENTRY-MGRTN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_MGRTN_IND");
        table_Entry_Db48_Entry_Mgrtn_Tmstmp_Tme = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_Tmstmp_Tme", "ENTRY-MGRTN-TMSTMP-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_MGRTN_TMSTMP_TME");
        table_Entry_Db48_Entry_Mgrtn_Tmstmp_Dte = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_Tmstmp_Dte", "ENTRY-MGRTN-TMSTMP-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_MGRTN_TMSTMP_DTE");
        table_Entry_Db48_Entry_Mgrtn_User_Id_Nme = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_User_Id_Nme", "ENTRY-MGRTN-USER-ID-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_MGRTN_USER_ID_NME");
        table_Entry_Db48_Entry_Mgrtn_Rqst_Tmstmp_Dte = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_Rqst_Tmstmp_Dte", 
            "ENTRY-MGRTN-RQST-TMSTMP-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_MGRTN_RQST_TMSTMP_DTE");
        table_Entry_Db48_Entry_Mgrtn_Rqst_User_Id_Nme = vw_table_Entry_Db48.getRecord().newFieldInGroup("table_Entry_Db48_Entry_Mgrtn_Rqst_User_Id_Nme", 
            "ENTRY-MGRTN-RQST-USER-ID-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_MGRTN_RQST_USER_ID_NME");
        registerRecord(vw_table_Entry_Db48);

        pnd_State_Key = localVariables.newFieldInRecord("pnd_State_Key", "#STATE-KEY", FieldType.STRING, 8);

        pnd_State_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_State_Key__R_Field_3", "REDEFINE", pnd_State_Key);
        pnd_State_Key_Pnd_State_Tbid_Key = pnd_State_Key__R_Field_3.newFieldInGroup("pnd_State_Key_Pnd_State_Tbid_Key", "#STATE-TBID-KEY", FieldType.NUMERIC, 
            6);
        pnd_State_Key_Pnd_State_Nbr_Key = pnd_State_Key__R_Field_3.newFieldInGroup("pnd_State_Key_Pnd_State_Nbr_Key", "#STATE-NBR-KEY", FieldType.STRING, 
            2);
        pnd_Empty = localVariables.newFieldInRecord("pnd_Empty", "#EMPTY", FieldType.BOOLEAN, 1);
        pnd_Roll = localVariables.newFieldInRecord("pnd_Roll", "#ROLL", FieldType.BOOLEAN, 1);

        vw_pay = new DataAccessProgramView(new NameInfo("vw_pay", "PAY"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pay_Cntrct_Ppcn_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pay_Cntrct_Ac_Lt_10yrs = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_AC_LT_10YRS");
        registerRecord(vw_pay);

        vw_contract = new DataAccessProgramView(new NameInfo("vw_contract", "CONTRACT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        contract_Cntrct_Ppcn_Nbr = vw_contract.getRecord().newFieldInGroup("contract_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        contract_Cntrct_Issue_Dte = vw_contract.getRecord().newFieldInGroup("contract_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        registerRecord(vw_contract);

        vw_role = new DataAccessProgramView(new NameInfo("vw_role", "ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        role_Cntrct_Part_Ppcn_Nbr = vw_role.getRecord().newFieldInGroup("role_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        role_Cntrct_Part_Payee_Cde = vw_role.getRecord().newFieldInGroup("role_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        role_Cntrct_Final_Per_Pay_Dte = vw_role.getRecord().newFieldInGroup("role_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        role_Cntrct_Mode_Ind = vw_role.getRecord().newFieldInGroup("role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        registerRecord(vw_role);

        pnd_Contract_Payee_Key = localVariables.newFieldInRecord("pnd_Contract_Payee_Key", "#CONTRACT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Contract_Payee_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Contract_Payee_Key__R_Field_4", "REDEFINE", pnd_Contract_Payee_Key);
        pnd_Contract_Payee_Key_Pnd_Contract_Key = pnd_Contract_Payee_Key__R_Field_4.newFieldInGroup("pnd_Contract_Payee_Key_Pnd_Contract_Key", "#CONTRACT-KEY", 
            FieldType.STRING, 10);
        pnd_Contract_Payee_Key_Pnd_Payee_Key = pnd_Contract_Payee_Key__R_Field_4.newFieldInGroup("pnd_Contract_Payee_Key_Pnd_Payee_Key", "#PAYEE-KEY", 
            FieldType.STRING, 2);
        pnd_Elgble = localVariables.newFieldInRecord("pnd_Elgble", "#ELGBLE", FieldType.BOOLEAN, 1);
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.STRING, 6);

        pnd_Start_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_5", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_S_Yyyy = pnd_Start_Dte__R_Field_5.newFieldInGroup("pnd_Start_Dte_Pnd_S_Yyyy", "#S-YYYY", FieldType.NUMERIC, 4);
        pnd_Start_Dte_Pnd_S_Mm = pnd_Start_Dte__R_Field_5.newFieldInGroup("pnd_Start_Dte_Pnd_S_Mm", "#S-MM", FieldType.NUMERIC, 2);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.STRING, 6);

        pnd_End_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_6", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_E_Yyyy = pnd_End_Dte__R_Field_6.newFieldInGroup("pnd_End_Dte_Pnd_E_Yyyy", "#E-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Dte_Pnd_E_Mm = pnd_End_Dte__R_Field_6.newFieldInGroup("pnd_End_Dte_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_nst_Table_Entry.reset();
        vw_table_Entry_Db48.reset();
        vw_pay.reset();
        vw_contract.reset();
        vw_role.reset();

        localVariables.reset();
        pnd_Prev_Pin.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Contract_Cnt.setInitialValue(0);
        pnd_Contract_Cnt_Minus1.setInitialValue(0);
        pnd_Curr_Mstate.setInitialValue(false);
        pnd_Prior_Mstate.setInitialValue(false);
        pnd_Read_Cnt.setInitialValue(0);
        pnd_Vol_Man_Cnt.setInitialValue(0);
        pnd_Man_Man_Cnt.setInitialValue(0);
        pnd_Man_Vol_Cnt.setInitialValue(0);
        pnd_Total_Letters_Cnt.setInitialValue(0);
        pnd_Post_Error_Cnt.setInitialValue(0);
        pnd_Get_State_Info_Curr.setInitialValue(false);
        pnd_Reached_State_Tbl_Max.setInitialValue(false);
        pnd_Error_Msg.setInitialValue(" ");
        pnd_Warning.setInitialValue(false);
        pnd_Post_Error.setInitialValue(false);
        pnd_Hold_Cntrct.setInitialValue(" ");
        pnd_Hold_Cntrct_Cmpy_Cde.setInitialValue(" ");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(1).setInitialValue("04");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(2).setInitialValue("05");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(3).setInitialValue("08");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(4).setInitialValue("09");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(5).setInitialValue("10");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(6).setInitialValue("12");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(7).setInitialValue("18");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(8).setInitialValue("19");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(9).setInitialValue("22");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(10).setInitialValue("23");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(11).setInitialValue("24");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(12).setInitialValue("25");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(13).setInitialValue("30");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(14).setInitialValue("36");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(15).setInitialValue("39");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(16).setInitialValue("40");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(17).setInitialValue("50");
        pnd_Mstates_Pnd_Mstate_Nbr.getValue(18).setInitialValue("51");
        pnd_State_Tbl_Cnt.setInitialValue(0);
        pnd_State_Tbl_Ndx.setInitialValue(0);
        pnd_Empty.setInitialValue(true);
        pnd_Roll.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap322() throws Exception
    {
        super("Iaap322");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  -----------------------------------------
        //* * *ERROR-TA := 'INFP9000'               /* JH 7/17/2000
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60
        MSTATE_FILE:                                                                                                                                                      //Natural: READ WORK FILE 1 IAAA320
        while (condition(getWorkFiles().read(1, pdaIaaa323.getIaaa320())))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            pnd_Empty.setValue(false);                                                                                                                                    //Natural: ASSIGN #EMPTY := FALSE
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Prev_Pin.setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                                   //Natural: ASSIGN #PREV-PIN := IAAA320.PIN-NBR
                //*  JWO 08/10/2012
                pnd_Roll.reset();                                                                                                                                         //Natural: RESET #ROLL
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-POST-DATA
                sub_Initialize_Post_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME := FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK FOR NEW PIN
            if (condition(pnd_Prev_Pin.notEquals(pdaIaaa323.getIaaa320_Pin_Nbr())))                                                                                       //Natural: IF #PREV-PIN NE IAAA320.PIN-NBR
            {
                pnd_Prev_Pin.setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                                   //Natural: ASSIGN #PREV-PIN := IAAA320.PIN-NBR
                pdaIaaa322.getIaaa322_Data_C_Contracts().setValue(pnd_Contract_Cnt);                                                                                      //Natural: ASSIGN IAAA322-DATA.C-CONTRACTS := #CONTRACT-CNT
                                                                                                                                                                          //Natural: PERFORM CALL-POST
                sub_Call_Post();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Contract_Cnt.reset();                                                                                                                                 //Natural: RESET #CONTRACT-CNT
                //*  JWO 08/10/2012
                pnd_Roll.reset();                                                                                                                                         //Natural: RESET #ROLL
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-POST-DATA
                sub_Initialize_Post_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*    LOAD CONTRACT NUMBER
            pnd_Contract_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CONTRACT-CNT
            //*  JWO 08/10/2012
            if (condition(! (pnd_Roll.getBoolean())))                                                                                                                     //Natural: IF NOT #ROLL
            {
                //*  JWO 08/10/2012
                //*  JWO 08/10/2012
                //*  JWO 08/10/2012
                //*  JWO 08/10/2012
                short decideConditionsMet678 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE CNTRCT-OPTION-CDE;//Natural: VALUE 22, 25, 27, 28, 30
                if (condition((pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(22) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(25) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(27) 
                    || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(28) || pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(30))))
                {
                    decideConditionsMet678++;
                    pnd_Roll.setValue(true);                                                                                                                              //Natural: ASSIGN #ROLL := TRUE
                }                                                                                                                                                         //Natural: VALUE 21
                else if (condition((pdaIaaa323.getIaaa320_Cntrct_Option_Cde().equals(21))))
                {
                    decideConditionsMet678++;
                    //*  JWO 08/10/2012
                                                                                                                                                                          //Natural: PERFORM DETERMINE-LT-10YRS
                    sub_Determine_Lt_10yrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  JWO 08/10/2012
                    //*  JWO 08/10/2012
                    if (condition(pnd_Elgble.getBoolean()))                                                                                                               //Natural: IF #ELGBLE
                    {
                        pnd_Roll.setValue(true);                                                                                                                          //Natural: ASSIGN #ROLL := TRUE
                        //*  JWO 08/10/2012
                        //*  JWO 08/10/2012
                        //*  JWO 08/10/2012
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                    //*  JWO 08/10/2012
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  JWO 08/10/2012
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet699 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CONTRACT-CNT GE 1 AND #CONTRACT-CNT LE 10
            if (condition(pnd_Contract_Cnt.greaterOrEqual(1) && pnd_Contract_Cnt.lessOrEqual(10)))
            {
                decideConditionsMet699++;
                //*  CAN PASS POST UP TO 10 CONTRACTS
                pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(pnd_Contract_Cnt).setValueEdited(pdaIaaa323.getIaaa320_Contract_Nbr(),new ReportEditMask("XXXXXXX'-'X")); //Natural: MOVE EDITED IAAA320.CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO IAAA322-DATA.CNTRCT-NBR ( #CONTRACT-CNT )
                pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(pnd_Contract_Cnt).setValue(pdaIaaa323.getIaaa320_Company_Cde());                                 //Natural: ASSIGN IAAA322-DATA.CNTRCT-COMPANY-CDE ( #CONTRACT-CNT ) := IAAA320.COMPANY-CDE
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-CNT GT 10
            else if (condition(pnd_Contract_Cnt.greater(10)))
            {
                decideConditionsMet699++;
                getReports().write(2, "** WARNING **","PIN",pdaIaaa323.getIaaa320_Pin_Nbr(),"HAS MORE THAN 10 CONTRACTS.","CONTRACT",pdaIaaa323.getIaaa320_Contract_Nbr(), //Natural: WRITE ( 2 ) '** WARNING **' 'PIN' IAAA320.PIN-NBR 'HAS MORE THAN 10 CONTRACTS.' 'CONTRACT' IAAA320.CONTRACT-NBR 'NOT PASSED TO POST.'
                    "NOT PASSED TO POST.");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Contract_Cnt.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #CONTRACT-CNT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM SET-STATE-LETTER
            sub_Set_State_Letter();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MSTATE_FILE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MSTATE_FILE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        MSTATE_FILE_Exit:
        if (Global.isEscape()) return;
        //*   PRINT LAST PIN
        if (condition(! (pnd_Empty.getBoolean())))                                                                                                                        //Natural: IF NOT #EMPTY
        {
                                                                                                                                                                          //Natural: PERFORM CALL-POST
            sub_Call_Post();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE *PROGRAM 45T 'IA MANDATORY STATE LETTERS CONTROL RPT' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / *DATX 120T *TIMX
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE *PROGRAM 46T 'IA MANDATORY STATE LETTERS ERROR RPT' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / *DATX 120T *TIMX
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"TOTAL CONTRACT RECORDS READ:   ",pnd_Read_Cnt,NEWLINE,NEWLINE,NEWLINE,"TOTAL VOLUNTARY TO MANDATORY:  ",           //Natural: WRITE ( 1 ) /// 'TOTAL CONTRACT RECORDS READ:   ' #READ-CNT /// 'TOTAL VOLUNTARY TO MANDATORY:  ' #VOL-MAN-CNT / 'TOTAL MANDATORY TO MANDATORY:  ' #MAN-MAN-CNT / 'TOTAL MANDATORY TO VOLUNTARY:  ' #MAN-VOL-CNT / '                                 -------' / 'TOTAL LETTERS PRINTED:         ' #TOTAL-LETTERS-CNT / 'TOTAL LETTERS WITH POST ERRORS:' #POST-ERROR-CNT
            pnd_Vol_Man_Cnt,NEWLINE,"TOTAL MANDATORY TO MANDATORY:  ",pnd_Man_Man_Cnt,NEWLINE,"TOTAL MANDATORY TO VOLUNTARY:  ",pnd_Man_Vol_Cnt,NEWLINE,
            "                                 -------",NEWLINE,"TOTAL LETTERS PRINTED:         ",pnd_Total_Letters_Cnt,NEWLINE,"TOTAL LETTERS WITH POST ERRORS:",
            pnd_Post_Error_Cnt);
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(50),"***  END OF REPORT  ***");                                                                                              //Natural: WRITE ( 2 ) 50T '***  END OF REPORT  ***'
        if (Global.isEscape()) return;
        //*   DUMP STATE TRANSLATION TABLE
        getReports().display(0, new ReportEmptyLineSuppression(true),pnd_State_Tbl_Pnd_State_Nbr.getValue("*"),pnd_State_Tbl_Pnd_State_Cde.getValue("*"),                 //Natural: DISPLAY ( ES = ON ) #STATE-NBR ( * ) #STATE-CDE ( * ) #STATE-DESC ( * )
            pnd_State_Tbl_Pnd_State_Desc.getValue("*"));
        if (Global.isEscape()) return;
        //*   PROGRAM TERMINATION
        if (condition(pnd_Post_Error.getBoolean()))                                                                                                                       //Natural: IF #POST-ERROR
        {
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #WARNING /* COMMENTED 8/96 TO PREVENT RETURN CODE OF 4
        //*    TERMINATE 04 /* AT END OF JOB
        //*  END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-POST-DATA
        //*  PSTA9611
        //*   SET UP VARS IN PSTA9611 FOR POST OPEN
        //*  PSTA9611.PIN-NBR    := IAAA320.PIN-NBR
        //*  EMPL-SGNTRY-NME     := 'KAREN GIACOPELLI'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STATE-LETTER
        //*    IF IAAA322-DATA.TAX-ADDR-LTR = '2'
        //*      IAAA322-DATA.STATE-FORM-REQ  := FALSE
        //*      IAAA322-DATA.PREFILL-STATE   := FALSE
        //*    ELSE
        //*      IAAA322-DATA.STATE-FORM-REQ  := TRUE
        //*      IAAA322-DATA.PREFILL-STATE   := TRUE
        //*    END-IF
        //*  SET ADDRESS INFO
        //*  PSTA9611.ADDRSS-LINE-TXT(1):= PH-ADDRESS-LINE-1
        //*  PSTA9611.ADDRSS-LINE-TXT(2):= PH-ADDRESS-LINE-2
        //*  PSTA9611.ADDRSS-LINE-TXT(3):= PH-ADDRESS-LINE-3
        //*  PSTA9611.ADDRSS-LINE-TXT(4):= PH-ADDRESS-LINE-4
        //*  PSTA9611.ADDRSS-LINE-TXT(5):= PH-ADDRESS-LINE-5
        //*  PSTA9611.ADDRSS-LINE-TXT(6):= PH-ADDRESS-LINE-6
        //*    PSTA9611.ADDRSS-TYP-CDE         := 'U'
        //*    PSTA9611.ADDRSS-PSTL-DTA        := POSTAL-DATA
        //*    PSTA9611.ADDRSS-TYP-CDE         := ' '
        //*    PSTA9611.ADDRSS-PSTL-DTA        := ' '
        //*                                                 START JWO 08/10/2012
        //*  SET PERIODIC PAYMENT AND ROLLOVER ELIGIBLE INDICATORS
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST
        //* **************************
        //*  PSTA9611
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //*  GET STATE OR COUNTRY DESCRIPTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-LT-10YRS
        //* ***********************************************************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-RTN
        //*  'NAME'      PSTA9611.FULL-NME
    }
    private void sub_Initialize_Post_Data() throws Exception                                                                                                              //Natural: INITIALIZE-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  POST OPEN
        //*    "  /* ghoshj
        //*  MANDATORY STATE LETTERS
        //* GHOSHJ
        pdaPsta9610.getPsta9610().reset();                                                                                                                                //Natural: RESET PSTA9610 PSTA9612 IAAA322-DATA IAAA322-DATA.PSTA3005-TAX IAAA322-DATA.PSTA3005-SET-TAX IAAA322-DATA.PSTA3006-TAX
        pdaPsta9612.getPsta9612().reset();
        pdaIaaa322.getIaaa322_Data().reset();
        pdaIaaa322.getIaaa322_Data_Psta3005_Tax().reset();
        pdaIaaa322.getIaaa322_Data_Psta3005_Set_Tax().reset();
        pdaIaaa322.getIaaa322_Data_Psta3006_Tax().reset();
        pdaPsta9612.getPsta9612_Full_Nme().setValue(pdaIaaa323.getIaaa320_Ph_Mailing_Nme());                                                                              //Natural: ASSIGN FULL-NME := IAAA320.PH-MAILING-NME
        pdaPsta9612.getPsta9612_Univ_Id().setValue(pdaIaaa323.getIaaa320_Pin_Nbr());                                                                                      //Natural: ASSIGN UNIV-ID := IAAA320.PIN-NBR
        //* GHOSHJ
        if (condition(pdaPsta9612.getPsta9612_Univ_Id_Typ().equals(" ")))                                                                                                 //Natural: IF UNIV-ID-TYP = ' '
        {
            //* GHOSHJ
            //* GHOSHJ
            if (condition(pdaPsta9612.getPsta9612_Univ_Id().greater("000000000000")))                                                                                     //Natural: IF UNIV-ID > '000000000000' THEN
            {
                pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                      //Natural: ASSIGN UNIV-ID-TYP := 'P'
                //* GHOSHJ
                //* GHOSHJ
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("N");                                                                                                      //Natural: ASSIGN UNIV-ID-TYP := 'N'
                //* GHOSHJ
            }                                                                                                                                                             //Natural: END-IF
            //* GHOSHJ
            //*  F-CFF   S-CSF
            //*  WILL BE ASSIGNED LATER
            //*  WILL BE ASSIGNED LATER
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHTAX");                                                                                                      //Natural: ASSIGN SYSTM-ID-CDE := 'BATCHTAX'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PLMSTAX");                                                                                                          //Natural: ASSIGN PCKGE-CDE := 'PLMSTAX'
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: ASSIGN PCKGE-DLVRY-TYP := '   '
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue("    ");                                                                                                          //Natural: ASSIGN PRNTR-ID-CDE := '    '
        pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(false);                                                                                                      //Natural: ASSIGN DONT-USE-FINALIST := FALSE
        pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                      //Natural: ASSIGN ADDRSS-LINES-RULE := FALSE
        pdaPsta9612.getPsta9612_Last_Nme().setValue(pdaIaaa323.getIaaa320_Ph_Last_Nme());                                                                                 //Natural: ASSIGN LAST-NME := IAAA320.PH-LAST-NME
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(" ");                                                                                                           //Natural: ASSIGN LTTR-SLTTN-TXT := ' '
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue(" ");                                                                                                          //Natural: ASSIGN EMPL-SGNTRY-NME := ' '
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue(" ");                                                                                                       //Natural: ASSIGN EMPL-UNIT-WORK-NME := ' '
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue(" ");                                                                                                          //Natural: ASSIGN PRINT-FCLTY-CDE := ' '
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := TRUE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                     //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := TRUE
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: ASSIGN FORM-LBRRY-ID := ' '
        pdaPsta9612.getPsta9612_Fnshng_Cde().setValue(" ");                                                                                                               //Natural: ASSIGN FNSHNG-CDE := ' '
        pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue("*").setValue(" ");                                                                                              //Natural: ASSIGN IAAA322-DATA.CNTRCT-NBR ( * ) := ' '
        pdaIaaa322.getIaaa322_Data_C_Contracts().setValue(0);                                                                                                             //Natural: ASSIGN IAAA322-DATA.C-CONTRACTS := 0
        pdaIaaa322.getIaaa322_Data_C_Contract_Sets().setValue(1);                                                                                                         //Natural: ASSIGN IAAA322-DATA.C-CONTRACT-SETS := 1
        pnd_Curr_Mstate.reset();                                                                                                                                          //Natural: RESET #CURR-MSTATE #PRIOR-MSTATE
        pnd_Prior_Mstate.reset();
        //*  INITIALIZE POST DATA
    }
    private void sub_Set_State_Letter() throws Exception                                                                                                                  //Natural: SET-STATE-LETTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   MANDATORY STATE LETTER DATA
        if (condition(! (pnd_Curr_Mstate.getBoolean()) && ! (pnd_Prior_Mstate.getBoolean())))                                                                             //Natural: IF NOT #CURR-MSTATE AND NOT #PRIOR-MSTATE
        {
            if (condition(pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde().equals(pnd_Mstates_Pnd_Mstate_Nbr.getValue("*"))))                                                //Natural: IF IAAA320.CURR-STATE-RSDNCY-CDE = #MSTATE-NBR ( * )
            {
                pnd_Curr_Mstate.setValue(true);                                                                                                                           //Natural: ASSIGN #CURR-MSTATE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde().equals(pnd_Mstates_Pnd_Mstate_Nbr.getValue("*"))))                                               //Natural: IF IAAA320.PRIOR-STATE-RSDNCY-CDE = #MSTATE-NBR ( * )
            {
                pnd_Prior_Mstate.setValue(true);                                                                                                                          //Natural: ASSIGN #PRIOR-MSTATE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Curr_Mstate.getBoolean()) && ! (pnd_Prior_Mstate.getBoolean())))                                                                         //Natural: IF NOT #CURR-MSTATE AND NOT #PRIOR-MSTATE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pdaIaaa322.getIaaa322_Data_Tax_Addr_Ltr().reset();                                                                                                            //Natural: RESET IAAA322-DATA.TAX-ADDR-LTR
            //*  VOLUNTARY TO MANDATORY
            short decideConditionsMet875 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( NOT #PRIOR-MSTATE ) AND #CURR-MSTATE
            if (condition(! ((pnd_Prior_Mstate.getBoolean()) && pnd_Curr_Mstate.getBoolean())))
            {
                decideConditionsMet875++;
                pdaIaaa322.getIaaa322_Data_Tax_Addr_Ltr().setValue("1");                                                                                                  //Natural: ASSIGN IAAA322-DATA.TAX-ADDR-LTR := '1'
                pdaIaaa322.getIaaa322_Data_State_Form_Req().setValue(true);                                                                                               //Natural: ASSIGN IAAA322-DATA.STATE-FORM-REQ := TRUE
                pdaIaaa322.getIaaa322_Data_Prefill_State().setValue(true);                                                                                                //Natural: ASSIGN IAAA322-DATA.PREFILL-STATE := TRUE
                //*  MANDATORY TO VOLUNTARY
                pnd_Vol_Man_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #VOL-MAN-CNT
            }                                                                                                                                                             //Natural: WHEN #PRIOR-MSTATE AND ( NOT #CURR-MSTATE )
            else if (condition(pnd_Prior_Mstate.getBoolean() && ! (pnd_Curr_Mstate.getBoolean())))
            {
                decideConditionsMet875++;
                pdaIaaa322.getIaaa322_Data_Tax_Addr_Ltr().setValue("2");                                                                                                  //Natural: ASSIGN IAAA322-DATA.TAX-ADDR-LTR := '2'
                pdaIaaa322.getIaaa322_Data_State_Form_Req().setValue(false);                                                                                              //Natural: ASSIGN IAAA322-DATA.STATE-FORM-REQ := FALSE
                pdaIaaa322.getIaaa322_Data_Prefill_State().setValue(false);                                                                                               //Natural: ASSIGN IAAA322-DATA.PREFILL-STATE := FALSE
                //*  MANDATORY TO MANDATORY
                pnd_Man_Vol_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #MAN-VOL-CNT
            }                                                                                                                                                             //Natural: WHEN #PRIOR-MSTATE AND #CURR-MSTATE
            else if (condition(pnd_Prior_Mstate.getBoolean() && pnd_Curr_Mstate.getBoolean()))
            {
                decideConditionsMet875++;
                pdaIaaa322.getIaaa322_Data_Tax_Addr_Ltr().setValue("3");                                                                                                  //Natural: ASSIGN IAAA322-DATA.TAX-ADDR-LTR := '3'
                pdaIaaa322.getIaaa322_Data_State_Form_Req().setValue(true);                                                                                               //Natural: ASSIGN IAAA322-DATA.STATE-FORM-REQ := TRUE
                pdaIaaa322.getIaaa322_Data_Prefill_State().setValue(true);                                                                                                //Natural: ASSIGN IAAA322-DATA.PREFILL-STATE := TRUE
                pnd_Man_Man_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #MAN-MAN-CNT
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet875 > 0))
            {
                if (condition(pnd_Contract_Cnt.notEquals(1)))                                                                                                             //Natural: IF #CONTRACT-CNT NE 1
                {
                    pnd_Hold_Cntrct.setValue(pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(pnd_Contract_Cnt));                                                         //Natural: ASSIGN #HOLD-CNTRCT := IAAA322-DATA.CNTRCT-NBR ( #CONTRACT-CNT )
                    pnd_Hold_Cntrct_Cmpy_Cde.setValue(pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(pnd_Contract_Cnt));                                        //Natural: ASSIGN #HOLD-CNTRCT-CMPY-CDE := IAAA322-DATA.CNTRCT-COMPANY-CDE ( #CONTRACT-CNT )
                    pnd_Contract_Cnt_Minus1.compute(new ComputeParameters(false, pnd_Contract_Cnt_Minus1), pnd_Contract_Cnt.subtract(1));                                 //Natural: ASSIGN #CONTRACT-CNT-MINUS1 := #CONTRACT-CNT - 1
                    pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(2,":",pnd_Contract_Cnt).setValue(pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(1,                 //Natural: ASSIGN IAAA322-DATA.CNTRCT-NBR ( 2:#CONTRACT-CNT ) := IAAA322-DATA.CNTRCT-NBR ( 1:#CONTRACT-CNT-MINUS1 )
                        ":",pnd_Contract_Cnt_Minus1));
                    pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(1,":",pnd_Contract_Cnt).setValue(pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(pnd_Contract_Cnt_Minus1)); //Natural: ASSIGN IAAA322-DATA.CNTRCT-COMPANY-CDE ( 1:#CONTRACT-CNT ) := IAAA322-DATA.CNTRCT-COMPANY-CDE ( #CONTRACT-CNT-MINUS1 )
                    pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(1).setValue(pnd_Hold_Cntrct);                                                                        //Natural: ASSIGN IAAA322-DATA.CNTRCT-NBR ( 1 ) := #HOLD-CNTRCT
                    pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(1).setValue(pnd_Hold_Cntrct_Cmpy_Cde);                                                       //Natural: ASSIGN IAAA322-DATA.CNTRCT-COMPANY-CDE ( 1 ) := #HOLD-CNTRCT-CMPY-CDE
                    getReports().write(0, "CONTRACT SHIFT","PIN",pdaIaaa323.getIaaa320_Pin_Nbr(),"CONTRACT",pdaIaaa323.getIaaa320_Contract_Nbr(),"CNT",                   //Natural: WRITE 'CONTRACT SHIFT' 'PIN' IAAA320.PIN-NBR 'CONTRACT' IAAA320.CONTRACT-NBR 'CNT' #CONTRACT-CNT 'MINUS 1' #CONTRACT-CNT-MINUS1 'COMPANY' IAAA322-DATA.CNTRCT-COMPANY-CDE ( 1 )
                        pnd_Contract_Cnt,"MINUS 1",pnd_Contract_Cnt_Minus1,"COMPANY",pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(1));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet904 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE IAAA322-DATA.CNTRCT-COMPANY-CDE ( 1 );//Natural: VALUE 'T'
                if (condition((pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(1).equals("T"))))
                {
                    decideConditionsMet904++;
                    pdaIaaa322.getIaaa322_Data_Cntrct_Desc().setValue("TIAA Contract Number:");                                                                           //Natural: ASSIGN IAAA322-DATA.CNTRCT-DESC := 'TIAA Contract Number:'
                }                                                                                                                                                         //Natural: VALUE "C"
                else if (condition((pdaIaaa322.getIaaa322_Data_Cntrct_Company_Cde().getValue(1).equals("C"))))
                {
                    decideConditionsMet904++;
                    pdaIaaa322.getIaaa322_Data_Cntrct_Desc().setValue("CREF Contract Number:");                                                                           //Natural: ASSIGN IAAA322-DATA.CNTRCT-DESC := 'CREF Contract Number:'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pdaIaaa322.getIaaa322_Data_Cntrct_Desc().setValue("Contract Number:");                                                                                //Natural: ASSIGN IAAA322-DATA.CNTRCT-DESC := 'Contract Number:'
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Get_State_Info_Curr.setValue(false);                                                                                                                  //Natural: ASSIGN #GET-STATE-INFO-CURR := FALSE
                DbsUtil.examine(new ExamineSource(pnd_State_Tbl_Pnd_State_Nbr.getValue("*")), new ExamineSearch(pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde()),           //Natural: EXAMINE #STATE-NBR ( * ) FOR IAAA320.CURR-STATE-RSDNCY-CDE GIVING INDEX #STATE-TBL-NDX
                    new ExamineGivingIndex(pnd_State_Tbl_Ndx));
                if (condition(pnd_State_Tbl_Ndx.greater(getZero())))                                                                                                      //Natural: IF #STATE-TBL-NDX GT 0
                {
                    pdaIaaa322.getIaaa322_Data_To_State_Nme().setValue(pnd_State_Tbl_Pnd_State_Desc.getValue(pnd_State_Tbl_Ndx));                                         //Natural: ASSIGN IAAA322-DATA.TO-STATE-NME := #STATE-DESC ( #STATE-TBL-NDX )
                    pdaIaaa322.getIaaa322_Data_Addrss_State_Cde().setValue(pnd_State_Tbl_Pnd_State_Cde.getValue(pnd_State_Tbl_Ndx));                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-STATE-CDE := #STATE-CDE ( #STATE-TBL-NDX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_State_Key_Pnd_State_Nbr_Key.setValue(pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde());                                                              //Natural: ASSIGN #STATE-NBR-KEY := IAAA320.CURR-STATE-RSDNCY-CDE
                    pnd_Get_State_Info_Curr.setValue(true);                                                                                                               //Natural: ASSIGN #GET-STATE-INFO-CURR := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                    sub_Get_State_Info();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                pnd_Get_State_Info_Curr.setValue(false);                                                                                                                  //Natural: ASSIGN #GET-STATE-INFO-CURR := FALSE
                DbsUtil.examine(new ExamineSource(pnd_State_Tbl_Pnd_State_Nbr.getValue("*")), new ExamineSearch(pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde()),          //Natural: EXAMINE #STATE-NBR ( * ) FOR IAAA320.PRIOR-STATE-RSDNCY-CDE GIVING INDEX #STATE-TBL-NDX
                    new ExamineGivingIndex(pnd_State_Tbl_Ndx));
                if (condition(pnd_State_Tbl_Ndx.greater(getZero())))                                                                                                      //Natural: IF #STATE-TBL-NDX GT 0
                {
                    pdaIaaa322.getIaaa322_Data_From_State_Nme().setValue(pnd_State_Tbl_Pnd_State_Desc.getValue(pnd_State_Tbl_Ndx));                                       //Natural: ASSIGN IAAA322-DATA.FROM-STATE-NME := #STATE-DESC ( #STATE-TBL-NDX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_State_Key_Pnd_State_Nbr_Key.setValue(pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde());                                                             //Natural: ASSIGN #STATE-NBR-KEY := IAAA320.PRIOR-STATE-RSDNCY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                    sub_Get_State_Info();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(2, "NEITHER STATE IS MANDATORY FOR PIN",pdaIaaa323.getIaaa320_Pin_Nbr(),"CURR STATE",pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde(),    //Natural: WRITE ( 2 ) 'NEITHER STATE IS MANDATORY FOR PIN' IAAA320.PIN-NBR 'CURR STATE' IAAA320.CURR-STATE-RSDNCY-CDE 'PRIOR STATE' IAAA320.PRIOR-STATE-RSDNCY-CDE
                    "PRIOR STATE",pdaIaaa323.getIaaa320_Prior_State_Rsdncy_Cde());
                if (Global.isEscape()) return;
                //*  GHOSHJ
                //*  GHOSHJ
                //*  GHOSHJ
                //*  GHOSHJ
                //*  GHOSHJ
                //*  GHOSHJ
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(1).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_1());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 1 ) := PH-ADDRESS-LINE-1
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(2).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_2());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 2 ) := PH-ADDRESS-LINE-2
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(3).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_3());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 3 ) := PH-ADDRESS-LINE-3
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(4).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_4());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 4 ) := PH-ADDRESS-LINE-4
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(5).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_5());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 5 ) := PH-ADDRESS-LINE-5
            pdaPsta9612.getPsta9612_Addrss_Line_Txt().getValue(6).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_6());                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-TXT ( 6 ) := PH-ADDRESS-LINE-6
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(1).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_1());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 1 ) := PH-ADDRESS-LINE-1
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(2).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_2());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 2 ) := PH-ADDRESS-LINE-2
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(3).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_3());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 3 ) := PH-ADDRESS-LINE-3
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(4).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_4());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 4 ) := PH-ADDRESS-LINE-4
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(5).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_5());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 5 ) := PH-ADDRESS-LINE-5
            pdaIaaa322.getIaaa322_Data_Addrss_Lne().getValue(6).setValue(pdaIaaa323.getIaaa320_Ph_Address_Line_6());                                                      //Natural: ASSIGN IAAA322-DATA.ADDRSS-LNE ( 6 ) := PH-ADDRESS-LINE-6
            //*  US ADDRESS
            //*  GHOSHJ
            //*  GHOSHJ
            if (condition(DbsUtil.maskMatches(pdaIaaa323.getIaaa320_Curr_State_Rsdncy_Cde(),"NN")))                                                                       //Natural: IF IAAA320.CURR-STATE-RSDNCY-CDE = MASK ( NN )
            {
                pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                                   //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := 'U'
                pdaPsta9612.getPsta9612_Addrss_Pstl_Dta().setValue(pdaIaaa323.getIaaa320_Postal_Data());                                                                  //Natural: ASSIGN PSTA9612.ADDRSS-PSTL-DTA := POSTAL-DATA
                pdaIaaa322.getIaaa322_Data_Addrss_Type_Cde().setValue("U");                                                                                               //Natural: ASSIGN IAAA322-DATA.ADDRSS-TYPE-CDE := 'U'
                pdaIaaa322.getIaaa322_Data_Addrss_Postal_Data().setValue(pdaIaaa323.getIaaa320_Postal_Data());                                                            //Natural: ASSIGN IAAA322-DATA.ADDRSS-POSTAL-DATA := POSTAL-DATA
                //*  GHOSHJ
                //*  GHOSHJ
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(" ");                                                                                                   //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := ' '
                pdaPsta9612.getPsta9612_Addrss_Pstl_Dta().setValue(" ");                                                                                                  //Natural: ASSIGN PSTA9612.ADDRSS-PSTL-DTA := ' '
                pdaIaaa322.getIaaa322_Data_Addrss_Type_Cde().setValue(" ");                                                                                               //Natural: ASSIGN IAAA322-DATA.ADDRSS-TYPE-CDE := ' '
                pdaIaaa322.getIaaa322_Data_Addrss_Postal_Data().setValue(" ");                                                                                            //Natural: ASSIGN IAAA322-DATA.ADDRSS-POSTAL-DATA := ' '
            }                                                                                                                                                             //Natural: END-IF
            pdaIaaa322.getIaaa322_Data_Periodic_Ind().setValue("Y");                                                                                                      //Natural: ASSIGN IAAA322-DATA.PERIODIC-IND := 'Y'
            if (condition(pnd_Roll.getBoolean()))                                                                                                                         //Natural: IF #ROLL
            {
                pdaIaaa322.getIaaa322_Data_Rollover_Ind().setValue("Y");                                                                                                  //Natural: ASSIGN IAAA322-DATA.ROLLOVER-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIaaa322.getIaaa322_Data_Rollover_Ind().setValue("N");                                                                                                  //Natural: ASSIGN IAAA322-DATA.ROLLOVER-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(1),"=",pdaIaaa323.getIaaa320_Pin_Nbr(),"=",pdaIaaa322.getIaaa322_Data_Periodic_Ind(), //Natural: WRITE '=' IAAA322-DATA.CNTRCT-NBR ( 1 ) '=' IAAA320.PIN-NBR '=' IAAA322-DATA.PERIODIC-IND '=' IAAA322-DATA.ROLLOVER-IND
                "=",pdaIaaa322.getIaaa322_Data_Rollover_Ind());
            if (Global.isEscape()) return;
            //*                                                   END JWO 08/10/2012
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET STATE LETTER
    }
    private void sub_Call_Post() throws Exception                                                                                                                         //Natural: CALL-POST
    {
        if (BLNatReinput.isReinput()) return;

        pdaIaaa322.getIaaa322_Data_C_Contracts().setValue(pnd_Contract_Cnt);                                                                                              //Natural: ASSIGN IAAA322-DATA.C-CONTRACTS := #CONTRACT-CNT
        //*  USED IN ALL POST  MODULES
        //*  USED IN POST OPEN MODULE  /* GHOSHJ
        //*  CONTAINS MST DATA STRUCTURES
        //*  STANDARD CONSTRUCT
        DbsUtil.callnat(Iaan322.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaIaaa322.getIaaa322_Data(),                     //Natural: CALLNAT 'IAAN322' PSTA9610 PSTA9612 IAAA322-DATA MSG-INFO-SUB
            pdaCwfpda_M.getMsg_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(), " - ", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg())); //Natural: COMPRESS MSG-INFO-SUB.##ERROR-FIELD ' - ' MSG-INFO-SUB.##MSG INTO #ERROR-MSG LEAVING NO
            pnd_Post_Error.setValue(true);                                                                                                                                //Natural: ASSIGN #POST-ERROR := TRUE
            pnd_Post_Error_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #POST-ERROR-CNT
                                                                                                                                                                          //Natural: PERFORM ERROR-RTN
            sub_Error_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Letters_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-LETTERS-CNT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-POST
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(pnd_State_Tbl_Cnt.less(100)))                                                                                                                       //Natural: IF #STATE-TBL-CNT LT 100
        {
            pnd_State_Tbl_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #STATE-TBL-CNT
            pnd_State_Tbl_Ndx.setValue(pnd_State_Tbl_Cnt);                                                                                                                //Natural: ASSIGN #STATE-TBL-NDX := #STATE-TBL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Reached_State_Tbl_Max.setValue(true);                                                                                                                     //Natural: ASSIGN #REACHED-STATE-TBL-MAX := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_State_Key_Pnd_State_Tbid_Key.setValue(19);                                                                                                                    //Natural: ASSIGN #STATE-TBID-KEY := 19
        vw_table_Entry_Db48.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) TABLE-ENTRY-DB48 BY TABLE-ID-ENTRY-CDE STARTING FROM #STATE-KEY
        (
        "READ01",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_State_Key, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") },
        1
        );
        READ01:
        while (condition(vw_table_Entry_Db48.readNextRow("READ01")))
        {
            if (condition(table_Entry_Db48_Entry_Table_Id_Nbr.notEquals(pnd_State_Key_Pnd_State_Tbid_Key) || table_Entry_Db48_Entry_Cde.notEquals(pnd_State_Key_Pnd_State_Nbr_Key))) //Natural: IF ENTRY-TABLE-ID-NBR NE #STATE-TBID-KEY OR ENTRY-CDE NE #STATE-NBR-KEY
            {
                //* ********************************************************************
                //*  READ NST-TABLE-ENTRY AS DB48 HAS NO MATCH                /* MUKHERR
                //* ********************************************************************
                //*  MUKHERR
                pnd_Table_Key_Pnd_Table_Nbr.setValue(19);                                                                                                                 //Natural: ASSIGN #TABLE-NBR = 000019
                //*  MUKHERR
                pnd_Table_Key_Pnd_Table_Sub.setValue("RC");                                                                                                               //Natural: ASSIGN #TABLE-SUB = 'RC'
                //*  MUKHERR
                //*  MUKHERR
                //*  MUKHERR
                pnd_Table_Key_Pnd_Citizenship_Cde.setValue(pnd_State_Key_Pnd_State_Nbr_Key);                                                                              //Natural: ASSIGN #CITIZENSHIP-CDE = #STATE-NBR-KEY
                vw_nst_Table_Entry.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) NST-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-KEY
                (
                "READ02",
                new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Key, WcType.BY) },
                new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") },
                1
                );
                READ02:
                while (condition(vw_nst_Table_Entry.readNextRow("READ02")))
                {
                    //*  MUKHERR
                    //*  MUKHERR
                    //*  MUKHERR
                    if (condition(nst_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Key_Pnd_Table_Nbr) || nst_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Key_Pnd_Table_Sub)  //Natural: IF NST-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE #TABLE-NBR OR NST-TABLE-ENTRY.ENTRY-TABLE-SUB-ID NE #TABLE-SUB OR NST-TABLE-ENTRY.ENTRY-CDE NE #ENTRY-CODE
                        || nst_Table_Entry_Entry_Cde.notEquals(pnd_Table_Key_Pnd_Entry_Code)))
                    {
                        pnd_Error_Msg.setValue(DbsUtil.compress("STATE NOT FOUND ON PREDICT TABLE FILE: ", pnd_State_Key_Pnd_State_Tbid_Key, "STATE NBR: ",               //Natural: COMPRESS 'STATE NOT FOUND ON PREDICT TABLE FILE: ' #STATE-TBID-KEY 'STATE NBR: ' #STATE-NBR-KEY INTO #ERROR-MSG
                            pnd_State_Key_Pnd_State_Nbr_Key));
                        pnd_Warning.setValue(true);                                                                                                                       //Natural: ASSIGN #WARNING := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-RTN
                        sub_Error_Rtn();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  MUKHERR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  MUKHERR
                        //*  MUKHERR
                        //*  MUKHERR
                        if (condition(pnd_Get_State_Info_Curr.getBoolean()))                                                                                              //Natural: IF #GET-STATE-INFO-CURR
                        {
                            pdaIaaa322.getIaaa322_Data_To_State_Nme().setValue(nst_Table_Entry_Entry_Dscrptn_Txt);                                                        //Natural: ASSIGN IAAA322-DATA.TO-STATE-NME := NST-TABLE-ENTRY.ENTRY-DSCRPTN-TXT
                            //*  MUKHERR
                            //*  MUKHERR
                            //*  MUKHERR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaIaaa322.getIaaa322_Data_From_State_Nme().setValue(nst_Table_Entry_Entry_Dscrptn_Txt);                                                      //Natural: ASSIGN IAAA322-DATA.FROM-STATE-NME := NST-TABLE-ENTRY.ENTRY-DSCRPTN-TXT
                            //*  MUKHERR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  MUKHERR
                        //*  MUKHERR
                        //*  MUKHERR
                        //*  MUKHERR
                        if (condition(! (pnd_Reached_State_Tbl_Max.getBoolean())))                                                                                        //Natural: IF NOT #REACHED-STATE-TBL-MAX
                        {
                            pnd_State_Tbl_Pnd_State_Nbr.getValue(pnd_State_Tbl_Ndx).setValue(pnd_State_Key_Pnd_State_Nbr_Key);                                            //Natural: ASSIGN #STATE-NBR ( #STATE-TBL-NDX ) := #STATE-NBR-KEY
                            pnd_State_Tbl_Pnd_State_Desc.getValue(pnd_State_Tbl_Ndx).setValue(nst_Table_Entry_Entry_Dscrptn_Txt);                                         //Natural: ASSIGN #STATE-DESC ( #STATE-TBL-NDX ) := NST-TABLE-ENTRY.ENTRY-DSCRPTN-TXT
                            //*  MUKHERR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  MUKHERR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MUKHERR
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Get_State_Info_Curr.getBoolean()))                                                                                                      //Natural: IF #GET-STATE-INFO-CURR
                {
                    pdaIaaa322.getIaaa322_Data_To_State_Nme().setValue(table_Entry_Db48_Entry_Dscrptn_Txt);                                                               //Natural: ASSIGN IAAA322-DATA.TO-STATE-NME := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaIaaa322.getIaaa322_Data_From_State_Nme().setValue(table_Entry_Db48_Entry_Dscrptn_Txt);                                                             //Natural: ASSIGN IAAA322-DATA.FROM-STATE-NME := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Reached_State_Tbl_Max.getBoolean())))                                                                                                //Natural: IF NOT #REACHED-STATE-TBL-MAX
                {
                    pnd_State_Tbl_Pnd_State_Nbr.getValue(pnd_State_Tbl_Ndx).setValue(pnd_State_Key_Pnd_State_Nbr_Key);                                                    //Natural: ASSIGN #STATE-NBR ( #STATE-TBL-NDX ) := #STATE-NBR-KEY
                    pnd_State_Tbl_Pnd_State_Desc.getValue(pnd_State_Tbl_Ndx).setValue(table_Entry_Db48_Entry_Dscrptn_Txt);                                                //Natural: ASSIGN #STATE-DESC ( #STATE-TBL-NDX ) := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_State_Key_Pnd_State_Tbid_Key.setValue(3);                                                                                                                     //Natural: ASSIGN #STATE-TBID-KEY := 3
        vw_table_Entry_Db48.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) TABLE-ENTRY-DB48 BY TABLE-ID-ENTRY-CDE STARTING FROM #STATE-KEY
        (
        "READ03",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_State_Key, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") },
        1
        );
        READ03:
        while (condition(vw_table_Entry_Db48.readNextRow("READ03")))
        {
            if (condition(table_Entry_Db48_Entry_Table_Id_Nbr.notEquals(pnd_State_Key_Pnd_State_Tbid_Key) || table_Entry_Db48_Entry_Cde.notEquals(pnd_State_Key_Pnd_State_Nbr_Key))) //Natural: IF ENTRY-TABLE-ID-NBR NE #STATE-TBID-KEY OR ENTRY-CDE NE #STATE-NBR-KEY
            {
                pnd_Error_Msg.setValue(DbsUtil.compress("STATE NOT FOUND ON PREDICT TABLE FILE: ", pnd_State_Key_Pnd_State_Tbid_Key, "STATE NBR: ", pnd_State_Key_Pnd_State_Nbr_Key)); //Natural: COMPRESS 'STATE NOT FOUND ON PREDICT TABLE FILE: ' #STATE-TBID-KEY 'STATE NBR: ' #STATE-NBR-KEY INTO #ERROR-MSG
                pnd_Warning.setValue(true);                                                                                                                               //Natural: ASSIGN #WARNING := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-RTN
                sub_Error_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Get_State_Info_Curr.getBoolean()))                                                                                                      //Natural: IF #GET-STATE-INFO-CURR
                {
                    pdaIaaa322.getIaaa322_Data_Addrss_State_Cde().setValue(table_Entry_Db48_Entry_Dscrptn_Txt);                                                           //Natural: ASSIGN IAAA322-DATA.ADDRSS-STATE-CDE := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Reached_State_Tbl_Max.getBoolean())))                                                                                                //Natural: IF NOT #REACHED-STATE-TBL-MAX
                {
                    pnd_State_Tbl_Pnd_State_Nbr.getValue(pnd_State_Tbl_Ndx).setValue(pnd_State_Key_Pnd_State_Nbr_Key);                                                    //Natural: ASSIGN #STATE-NBR ( #STATE-TBL-NDX ) := #STATE-NBR-KEY
                    pnd_State_Tbl_Pnd_State_Cde.getValue(pnd_State_Tbl_Ndx).setValue(table_Entry_Db48_Entry_Dscrptn_Txt);                                                 //Natural: ASSIGN #STATE-CDE ( #STATE-TBL-NDX ) := ENTRY-DSCRPTN-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET STATE INFO
    }
    //*  JWO 08/10/2012
    private void sub_Determine_Lt_10yrs() throws Exception                                                                                                                //Natural: DETERMINE-LT-10YRS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Payee_Key_Pnd_Contract_Key.setValue(pdaIaaa323.getIaaa320_Contract_Nbr());                                                                           //Natural: ASSIGN #CONTRACT-KEY := IAAA320.CONTRACT-NBR
        pnd_Contract_Payee_Key_Pnd_Payee_Key.setValue(pdaIaaa323.getIaaa320_Payee_Cde());                                                                                 //Natural: ASSIGN #PAYEE-KEY := IAAA320.PAYEE-CDE
        vw_contract.startDatabaseFind                                                                                                                                     //Natural: FIND CONTRACT WITH CNTRCT-PPCN-NBR = IAAA320.CONTRACT-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaIaaa323.getIaaa320_Contract_Nbr(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_contract.readNextRow("FIND01")))
        {
            vw_contract.setIfNotFoundControlFlag(false);
            vw_role.startDatabaseFind                                                                                                                                     //Natural: FIND ROLE WITH CNTRCT-PAYEE-KEY = #CONTRACT-PAYEE-KEY
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Contract_Payee_Key, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_role.readNextRow("FIND02")))
            {
                vw_role.setIfNotFoundControlFlag(false);
                pnd_Start_Dte.setValueEdited(contract_Cntrct_Issue_Dte,new ReportEditMask("999999"));                                                                     //Natural: MOVE EDITED CONTRACT.CNTRCT-ISSUE-DTE ( EM = 999999 ) TO #START-DTE
                pnd_End_Dte.setValueEdited(role_Cntrct_Final_Per_Pay_Dte,new ReportEditMask("999999"));                                                                   //Natural: MOVE EDITED ROLE.CNTRCT-FINAL-PER-PAY-DTE ( EM = 999999 ) TO #END-DTE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        short decideConditionsMet1101 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((role_Cntrct_Mode_Ind.equals(100))))
        {
            decideConditionsMet1101++;
            pnd_End_Dte_Pnd_E_Mm.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((role_Cntrct_Mode_Ind.greaterOrEqual(601) && role_Cntrct_Mode_Ind.lessOrEqual(603)))))
        {
            decideConditionsMet1101++;
            pnd_End_Dte_Pnd_E_Mm.nadd(3);                                                                                                                                 //Natural: ADD 3 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((role_Cntrct_Mode_Ind.greaterOrEqual(701) && role_Cntrct_Mode_Ind.lessOrEqual(706)))))
        {
            decideConditionsMet1101++;
            pnd_End_Dte_Pnd_E_Mm.nadd(6);                                                                                                                                 //Natural: ADD 6 TO #E-MM
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((role_Cntrct_Mode_Ind.greaterOrEqual(801) && role_Cntrct_Mode_Ind.lessOrEqual(812)))))
        {
            decideConditionsMet1101++;
            pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                               //Natural: ADD 1 TO #E-YYYY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_End_Dte_Pnd_E_Mm.greater(12)))                                                                                                                  //Natural: IF #E-MM GT 12
        {
            pnd_End_Dte_Pnd_E_Mm.nsubtract(12);                                                                                                                           //Natural: SUBTRACT 12 FROM #E-MM
            pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                               //Natural: ADD 1 TO #E-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Start_Dte_Pnd_S_Mm.greater(pnd_End_Dte_Pnd_E_Mm)))                                                                                              //Natural: IF #S-MM GT #E-MM
        {
            pnd_End_Dte_Pnd_E_Yyyy.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #E-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_End_Dte_Pnd_E_Yyyy.subtract(pnd_Start_Dte_Pnd_S_Yyyy)).less(10)))                                                                              //Natural: IF ( #E-YYYY - #S-YYYY ) LT 10
        {
            pnd_Elgble.setValue(true);                                                                                                                                    //Natural: ASSIGN #ELGBLE := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Elgble.setValue(false);                                                                                                                                   //Natural: ASSIGN #ELGBLE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "Contract:",pdaIaaa323.getIaaa320_Contract_Nbr(),"les than 10 years:",pnd_Elgble);                                                          //Natural: WRITE 'Contract:' IAAA320.CONTRACT-NBR 'les than 10 years:' #ELGBLE
        if (Global.isEscape()) return;
    }
    private void sub_Error_Rtn() throws Exception                                                                                                                         //Natural: ERROR-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  GHOSHJ
        getReports().display(2, "NAME",                                                                                                                                   //Natural: DISPLAY ( 2 ) 'NAME' PSTA9612.FULL-NME 'CONTRACT' IAAA322-DATA.CNTRCT-NBR ( 1 ) 'ERROR MSG' #ERROR-MSG
        		pdaPsta9612.getPsta9612_Full_Nme(),"CONTRACT",
        		pdaIaaa322.getIaaa322_Data_Cntrct_Nbr().getValue(1),"ERROR MSG",
        		pnd_Error_Msg);
        if (Global.isEscape()) return;
        pnd_Error_Msg.reset();                                                                                                                                            //Natural: RESET #ERROR-MSG
        //*  ERROR-RTN
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,Global.getPROGRAM(),new TabSetting(45),"IA MANDATORY STATE LETTERS CONTROL RPT",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(120),Global.getTIMX());
        getReports().write(2, ReportOption.TITLE,Global.getPROGRAM(),new TabSetting(46),"IA MANDATORY STATE LETTERS ERROR RPT",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ9"),NEWLINE,Global.getDATX(),new TabSetting(120),Global.getTIMX());

        getReports().setDisplayColumns(0, new ReportEmptyLineSuppression(true),pnd_State_Tbl_Pnd_State_Nbr,pnd_State_Tbl_Pnd_State_Cde,pnd_State_Tbl_Pnd_State_Desc);
        getReports().setDisplayColumns(2, "NAME",
        		pdaPsta9612.getPsta9612_Full_Nme(),"CONTRACT",
        		pdaIaaa322.getIaaa322_Data_Cntrct_Nbr(),"ERROR MSG",
        		pnd_Error_Msg);
    }
}
