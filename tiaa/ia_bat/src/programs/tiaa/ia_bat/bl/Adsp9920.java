/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:01 PM
**        * FROM NATURAL PROGRAM : Adsp9920
************************************************************
**        * FILE NAME            : Adsp9920.java
**        * CLASS NAME           : Adsp9920
**        * INSTANCE NAME        : Adsp9920
************************************************************
************************************************************************
* SELECT ALL PPGS WITH ACTIVE CONTRACTS
* ALL ANNUITIES FROM ADAM AND ADAS
* OUTPUT OPTION, AMOUNT ANNUITIZED, NAME, AND SSN.
* FREQUENCY: DAILY
*
* 12/12/2013 O. SOTTO GET OPTION DESCRIPTION FROM IA. CHANGES MARKED
*                     OS - 121213.
* 04/2017    O. SOTTO PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp9920 extends BLNatBase
{
    // Data Areas
    private PdaAdspda_M pdaAdspda_M;
    private LdaIaal999 ldaIaal999;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_In_Rec;
    private DbsField pnd_In_Rec_Pnd_I_Rqst_Id;
    private DbsField pnd_In_Rec_Pnd_I_Tiaa;
    private DbsField pnd_In_Rec_Pnd_I_Cref;
    private DbsField pnd_In_Rec_Pnd_I_1st_Pin;
    private DbsField pnd_In_Rec_Pnd_I_1st_Ssn;
    private DbsField pnd_In_Rec_Pnd_I_1st_Name;
    private DbsField pnd_In_Rec_Pnd_I_1st_Dob;
    private DbsField pnd_In_Rec_Pnd_I_1st_Sex;
    private DbsField pnd_In_Rec_Pnd_I_1st_Rsdnce;
    private DbsField pnd_In_Rec_Pnd_I_2nd_Ssn;
    private DbsField pnd_In_Rec_Pnd_I_2nd_Name;
    private DbsField pnd_In_Rec_Pnd_I_2nd_Dob;
    private DbsField pnd_In_Rec_Pnd_I_2nd_Sex;
    private DbsField pnd_In_Rec_Pnd_I_Annty_Start;
    private DbsField pnd_In_Rec_Pnd_I_Ppg;
    private DbsField pnd_In_Rec_Pnd_I_Paymnt_Mode;
    private DbsField pnd_In_Rec_Pnd_I_Option;
    private DbsField pnd_In_Rec_Pnd_I_Grntee_Prd;
    private DbsField pnd_In_Rec_Pnd_I_Acct_Cde;
    private DbsField pnd_In_Rec_Pnd_I_Acct_Actl_Amt;

    private DataAccessProgramView vw_ads_P;
    private DbsField ads_P_Rqst_Id;
    private DbsField ads_P_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_P_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField ads_P_Adp_Bps_Unit;
    private DbsField ads_P_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField ads_P_Adp_Ia_Cref_Nbr;
    private DbsField ads_P_Adp_Ia_Cref_Payee_Cd;
    private DbsField ads_P_Adp_Annt_Typ_Cde;
    private DbsField ads_P_Adp_Emplymnt_Term_Dte;
    private DbsField ads_P_Adp_Apprvl_Wo_Term_Dte;
    private DbsGroup ads_P_Adp_Cntrcts_In_RqstMuGroup;
    private DbsField ads_P_Adp_Cntrcts_In_Rqst;
    private DbsField ads_P_Adp_Cntr_Prt_Issue_Dte;
    private DbsField ads_P_Adp_Entry_Dte;
    private DbsField ads_P_Adp_Lst_Actvty_Dte;
    private DbsField ads_P_Adp_Annty_Strt_Dte;
    private DbsField ads_P_Adp_Unique_Id;

    private DbsGroup ads_P__R_Field_1;
    private DbsField ads_P_Adp_Unique_Id_A;
    private DbsField ads_P_Adp_Effctv_Dte;
    private DbsField ads_P_Adp_Opn_Clsd_Ind;
    private DbsField ads_P_Adp_Stts_Cde;
    private DbsField ads_P_Adp_Plan_Nbr;
    private DbsField ads_P_Adp_Annty_Optn;
    private DbsField ads_P_Adp_Grntee_Period;

    private DbsGroup ads_P__R_Field_2;
    private DbsField ads_P_Adp_Grntee_Period_A;
    private DbsField ads_P_Adp_Pymnt_Mode;
    private DbsField ads_P_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField ads_P_Adp_Settl_All_Cref_Mon_Pct;
    private DbsField ads_P_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField ads_P_Adp_Frst_Annt_Ssn;
    private DbsField ads_P_Adp_Frst_Annt_Frst_Nme;
    private DbsField ads_P_Adp_Frst_Annt_Mid_Nme;
    private DbsField ads_P_Adp_Frst_Annt_Lst_Nme;
    private DbsField ads_P_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField ads_P_Adp_Rsttlmnt_Cnt;

    private DataAccessProgramView vw_ads_R;
    private DbsField ads_R_Rqst_Id;
    private DbsField ads_R_Adp_Unique_Id;
    private DbsField ads_R_Adp_Stts_Cde;
    private DbsField ads_R_Adp_Rsttlmnt_Cnt;
    private DbsField ads_R_Adp_Orig_Rqst_Id;

    private DataAccessProgramView vw_ads_Cntrct;
    private DbsField ads_Cntrct_Rqst_Id;
    private DbsField ads_Cntrct_Adc_Tiaa_Nbr;
    private DbsField ads_Cntrct_Adc_Cref_Nbr;

    private DbsGroup ads_Cntrct_Adc_Rqst_Info;
    private DbsField ads_Cntrct_Adc_Acct_Cde;
    private DbsField ads_Cntrct_Adc_Acct_Actl_Amt;

    private DbsGroup pnd_Ia_Area;
    private DbsField pnd_Ia_Area_Pnd_Ia_Ppcn;
    private DbsField pnd_Ia_Area_Pnd_Ia_Pay;
    private DbsField pnd_Ia_Area_Pnd_Tot_Per_Amt;
    private DbsField pnd_Ia_Area_Pnd_Stts;
    private DbsField pnd_Ia_Area_Pnd_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Ia_Area_Pnd_Cntrct_Mode;
    private DbsField pnd_Cp_Key;

    private DbsGroup pnd_Cp_Key__R_Field_3;
    private DbsField pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Desc;
    private DbsField pnd_Long_Desc;
    private DbsField pnd_Misc_Fld;
    private DbsField pnd_Table_Code;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_4;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Dup;
    private DbsField pnd_Found;
    private DbsField pnd_W_Cnt;
    private DbsField pnd_W_Eff_Dte;

    private DbsGroup pnd_W_Eff_Dte__R_Field_5;
    private DbsField pnd_W_Eff_Dte_Pnd_W_Yr;

    private DbsGroup pnd_Yr_Table;
    private DbsField pnd_Yr_Table_Pnd_Yr;
    private DbsField pnd_Yr_Table_Pnd_Cnt;
    private DbsField pnd_Overall_Tot;
    private DbsField pnd_Dated;
    private DbsField pnd_Ws_Pin;
    private DbsField pnd_First_Name;
    private DbsField pnd_Last_Name;
    private DbsField pnd_From;
    private DbsField pnd_Type_Call;
    private DbsField pnd_Code;
    private DbsField pnd_Desc_Res;

    private DbsGroup pnd_Out_Rec;
    private DbsField pnd_Out_Rec_Pnd_O_Pin;

    private DbsGroup pnd_Out_Rec__R_Field_6;
    private DbsField pnd_Out_Rec_Pnd_O_Pin_5;
    private DbsField pnd_Out_Rec_Pnd_O_Pin_7;
    private DbsField pnd_Out_Rec_Fill1;
    private DbsField pnd_Out_Rec_Pnd_O_Tiaa;
    private DbsField pnd_Out_Rec_Fill3;
    private DbsField pnd_Out_Rec_Pnd_O_Cref;
    private DbsField pnd_Out_Rec_Fill4;
    private DbsField pnd_Out_Rec_Pnd_O_Name;
    private DbsField pnd_Out_Rec_Fill5;
    private DbsField pnd_Out_Rec_Pnd_O_Age;
    private DbsField pnd_Out_Rec_Fill6;
    private DbsField pnd_Out_Rec_Pnd_O_Dob;
    private DbsField pnd_Out_Rec_Fill7;
    private DbsField pnd_Out_Rec_Pnd_O_Annty_Start;
    private DbsField pnd_Out_Rec_Fill8;
    private DbsField pnd_Out_Rec_Pnd_O_Accum_Settled;
    private DbsField pnd_Out_Rec_Fill9;
    private DbsField pnd_Out_Rec_Pnd_O_Payment;
    private DbsField pnd_Out_Rec_Fill10;
    private DbsField pnd_Out_Rec_Pnd_O_Frequency;
    private DbsField pnd_Out_Rec_Fill11;
    private DbsField pnd_Out_Rec_Pnd_O_Option;
    private DbsField pnd_Out_Rec_Fill12;

    private DbsGroup pnd_Out_Rec_Pnd_O_Plan_Ppgs_Tbl;
    private DbsField pnd_Out_Rec_Pnd_O_Plan_Ppg;
    private DbsField pnd_O_Ia_Tiaa_Status;
    private DbsField pnd_O_Ia_Cref_Status;
    private DbsField pnd_Tiaa_Amt_Settled;
    private DbsField pnd_Cref_Amt_Settled;
    private DbsField pnd_Tiaa_Total_Pymnt;
    private DbsField pnd_Cref_Total_Pymnt;
    private DbsField pnd_Frequency;
    private DbsField pnd_Dob;
    private DbsField pnd_Amt_Annuitized_Cref;
    private DbsField pnd_Payment_Amt;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_7;
    private DbsField pnd_Start_Date_Pnd_Yyyy;
    private DbsField pnd_Start_Date_Pnd_Mm;
    private DbsField pnd_Start_Date_Pnd_Dd;

    private DbsGroup pnd_Start_Date__R_Field_8;
    private DbsField pnd_Start_Date_Pnd_Start_Date_N;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_End_Date__R_Field_9;
    private DbsField pnd_End_Date_Pnd_E_Yyyy;
    private DbsField pnd_End_Date_Pnd_E_Mm;
    private DbsField pnd_End_Date_Pnd_E_Dd;

    private DbsGroup pnd_End_Date__R_Field_10;
    private DbsField pnd_End_Date_Pnd_End_Date_N;
    private DbsField pnd_Num_Years;

    private DbsGroup pnd_Num_Years__R_Field_11;
    private DbsField pnd_Num_Years_Pnd_Num_Years_A;
    private DbsField pnd_Num_Months;

    private DbsGroup pnd_Num_Months__R_Field_12;
    private DbsField pnd_Num_Months_Pnd_Num_Months_A;
    private DbsField pnd_Rc;
    private DbsField pnd_Rtn_Cde;
    private DbsField pnd_Auv;
    private DbsField pnd_Auv_Ret_Dte;
    private DbsField pnd_Tot_Amt;
    private DbsField pnd_In_Cnt;
    private DbsField pnd_Cnt_Adam;
    private DbsField pnd_Cnt_Adam_Active;
    private DbsField pnd_Cnt_Adam_Term;
    private DbsField pnd_Cnt_Adas;
    private DbsField pnd_Cnt_Adas_Active;
    private DbsField pnd_Cnt_Adas_Term;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Dep;
    private DbsField pnd_Msu;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_W_Pin;

    private DbsGroup pnd_W_Pin__R_Field_13;
    private DbsField pnd_W_Pin_Pnd_W_Pin_A;
    private DbsField pnd_Ppcn;
    private DbsField pnd_In_Option;
    private DbsField pnd_Out_Desc;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_C;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        ldaIaal999 = new LdaIaal999();
        registerRecord(ldaIaal999);
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Cref_Fund_Trans());
        registerRecord(ldaIaal999.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct());
        registerRecord(ldaIaal999.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaIaal999.getVw_cpr());
        registerRecord(ldaIaal999.getVw_iaa_Cpr_Trans());

        // Local Variables

        pnd_In_Rec = localVariables.newGroupInRecord("pnd_In_Rec", "#IN-REC");
        pnd_In_Rec_Pnd_I_Rqst_Id = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Rqst_Id", "#I-RQST-ID", FieldType.STRING, 30);
        pnd_In_Rec_Pnd_I_Tiaa = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Tiaa", "#I-TIAA", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_I_Cref = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Cref", "#I-CREF", FieldType.STRING, 10);
        pnd_In_Rec_Pnd_I_1st_Pin = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Pin", "#I-1ST-PIN", FieldType.NUMERIC, 7);
        pnd_In_Rec_Pnd_I_1st_Ssn = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Ssn", "#I-1ST-SSN", FieldType.NUMERIC, 9);
        pnd_In_Rec_Pnd_I_1st_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Name", "#I-1ST-NAME", FieldType.STRING, 30);
        pnd_In_Rec_Pnd_I_1st_Dob = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Dob", "#I-1ST-DOB", FieldType.STRING, 8);
        pnd_In_Rec_Pnd_I_1st_Sex = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Sex", "#I-1ST-SEX", FieldType.STRING, 1);
        pnd_In_Rec_Pnd_I_1st_Rsdnce = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_1st_Rsdnce", "#I-1ST-RSDNCE", FieldType.STRING, 2);
        pnd_In_Rec_Pnd_I_2nd_Ssn = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_2nd_Ssn", "#I-2ND-SSN", FieldType.NUMERIC, 9);
        pnd_In_Rec_Pnd_I_2nd_Name = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_2nd_Name", "#I-2ND-NAME", FieldType.STRING, 30);
        pnd_In_Rec_Pnd_I_2nd_Dob = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_2nd_Dob", "#I-2ND-DOB", FieldType.STRING, 8);
        pnd_In_Rec_Pnd_I_2nd_Sex = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_2nd_Sex", "#I-2ND-SEX", FieldType.STRING, 1);
        pnd_In_Rec_Pnd_I_Annty_Start = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Annty_Start", "#I-ANNTY-START", FieldType.STRING, 8);
        pnd_In_Rec_Pnd_I_Ppg = pnd_In_Rec.newFieldArrayInGroup("pnd_In_Rec_Pnd_I_Ppg", "#I-PPG", FieldType.STRING, 6, new DbsArrayController(1, 21));
        pnd_In_Rec_Pnd_I_Paymnt_Mode = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Paymnt_Mode", "#I-PAYMNT-MODE", FieldType.NUMERIC, 3);
        pnd_In_Rec_Pnd_I_Option = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Option", "#I-OPTION", FieldType.STRING, 2);
        pnd_In_Rec_Pnd_I_Grntee_Prd = pnd_In_Rec.newFieldInGroup("pnd_In_Rec_Pnd_I_Grntee_Prd", "#I-GRNTEE-PRD", FieldType.NUMERIC, 2);
        pnd_In_Rec_Pnd_I_Acct_Cde = pnd_In_Rec.newFieldArrayInGroup("pnd_In_Rec_Pnd_I_Acct_Cde", "#I-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_In_Rec_Pnd_I_Acct_Actl_Amt = pnd_In_Rec.newFieldArrayInGroup("pnd_In_Rec_Pnd_I_Acct_Actl_Amt", "#I-ACCT-ACTL-AMT", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 20));

        vw_ads_P = new DataAccessProgramView(new NameInfo("vw_ads_P", "ADS-P"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_P_Rqst_Id = vw_ads_P.getRecord().newFieldInGroup("ads_P_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        ads_P_Adp_Ia_Tiaa_Nbr = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADP_IA_TIAA_NBR");
        ads_P_Adp_Ia_Tiaa_Rea_Nbr = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADP_IA_TIAA_REA_NBR");
        ads_P_Adp_Bps_Unit = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_BPS_UNIT");
        ads_P_Adp_Ia_Tiaa_Payee_Cd = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_IA_TIAA_PAYEE_CD");
        ads_P_Adp_Ia_Cref_Nbr = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADP_IA_CREF_NBR");
        ads_P_Adp_Ia_Cref_Payee_Cd = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_IA_CREF_PAYEE_CD");
        ads_P_Adp_Annt_Typ_Cde = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_ANNT_TYP_CDE");
        ads_P_Adp_Emplymnt_Term_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Emplymnt_Term_Dte", "ADP-EMPLYMNT-TERM-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EMPLYMNT_TERM_DTE");
        ads_P_Adp_Apprvl_Wo_Term_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Apprvl_Wo_Term_Dte", "ADP-APPRVL-WO-TERM-DTE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_APPRVL_WO_TERM_DTE");
        ads_P_Adp_Cntrcts_In_RqstMuGroup = vw_ads_P.getRecord().newGroupInGroup("ADS_P_ADP_CNTRCTS_IN_RQSTMuGroup", "ADP_CNTRCTS_IN_RQSTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_PRTCPNT_ADP_CNTRCTS_IN_RQST");
        ads_P_Adp_Cntrcts_In_Rqst = ads_P_Adp_Cntrcts_In_RqstMuGroup.newFieldArrayInGroup("ads_P_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_CNTRCTS_IN_RQST");
        ads_P_Adp_Cntr_Prt_Issue_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Cntr_Prt_Issue_Dte", "ADP-CNTR-PRT-ISSUE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_CNTR_PRT_ISSUE_DTE");
        ads_P_Adp_Entry_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Entry_Dte", "ADP-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_ENTRY_DTE");
        ads_P_Adp_Lst_Actvty_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_LST_ACTVTY_DTE");
        ads_P_Adp_Annty_Strt_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_ANNTY_STRT_DTE");
        ads_P_Adp_Unique_Id = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ADP_UNIQUE_ID");

        ads_P__R_Field_1 = vw_ads_P.getRecord().newGroupInGroup("ads_P__R_Field_1", "REDEFINE", ads_P_Adp_Unique_Id);
        ads_P_Adp_Unique_Id_A = ads_P__R_Field_1.newFieldInGroup("ads_P_Adp_Unique_Id_A", "ADP-UNIQUE-ID-A", FieldType.STRING, 7);
        ads_P_Adp_Effctv_Dte = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EFFCTV_DTE");
        ads_P_Adp_Opn_Clsd_Ind = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_OPN_CLSD_IND");
        ads_P_Adp_Stts_Cde = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADP_STTS_CDE");
        ads_P_Adp_Plan_Nbr = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_PLAN_NBR");
        ads_P_Adp_Annty_Optn = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_ANNTY_OPTN");
        ads_P_Adp_Grntee_Period = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADP_GRNTEE_PERIOD");

        ads_P__R_Field_2 = vw_ads_P.getRecord().newGroupInGroup("ads_P__R_Field_2", "REDEFINE", ads_P_Adp_Grntee_Period);
        ads_P_Adp_Grntee_Period_A = ads_P__R_Field_2.newFieldInGroup("ads_P_Adp_Grntee_Period_A", "ADP-GRNTEE-PERIOD-A", FieldType.STRING, 2);
        ads_P_Adp_Pymnt_Mode = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ADP_PYMNT_MODE");
        ads_P_Adp_Settl_All_Tiaa_Grd_Pct = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_TIAA_GRD_PCT");
        ads_P_Adp_Settl_All_Cref_Mon_Pct = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Settl_All_Cref_Mon_Pct", "ADP-SETTL-ALL-CREF-MON-PCT", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_CREF_MON_PCT");
        ads_P_Adp_Frst_Annt_Rsdnc_Cde = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_RSDNC_CDE");
        ads_P_Adp_Frst_Annt_Ssn = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ADP_FRST_ANNT_SSN");
        ads_P_Adp_Frst_Annt_Frst_Nme = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_FRST_NME");
        ads_P_Adp_Frst_Annt_Mid_Nme = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "ADP_FRST_ANNT_MID_NME");
        ads_P_Adp_Frst_Annt_Lst_Nme = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "ADP_FRST_ANNT_LST_NME");
        ads_P_Adp_Frst_Annt_Dte_Of_Brth = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_FRST_ANNT_DTE_OF_BRTH");
        ads_P_Adp_Rsttlmnt_Cnt = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_RSTTLMNT_CNT");
        registerRecord(vw_ads_P);

        vw_ads_R = new DataAccessProgramView(new NameInfo("vw_ads_R", "ADS-R"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_R_Rqst_Id = vw_ads_R.getRecord().newFieldInGroup("ads_R_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        ads_R_Adp_Unique_Id = vw_ads_R.getRecord().newFieldInGroup("ads_R_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ADP_UNIQUE_ID");
        ads_R_Adp_Stts_Cde = vw_ads_R.getRecord().newFieldInGroup("ads_R_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADP_STTS_CDE");
        ads_R_Adp_Rsttlmnt_Cnt = vw_ads_R.getRecord().newFieldInGroup("ads_R_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_RSTTLMNT_CNT");
        ads_R_Adp_Orig_Rqst_Id = vw_ads_R.getRecord().newFieldInGroup("ads_R_Adp_Orig_Rqst_Id", "ADP-ORIG-RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "ADP_ORIG_RQST_ID");
        registerRecord(vw_ads_R);

        vw_ads_Cntrct = new DataAccessProgramView(new NameInfo("vw_ads_Cntrct", "ADS-CNTRCT"), "ADS_CNTRCT", "DS_CNTRCT", DdmPeriodicGroups.getInstance().getGroups("ADS_CNTRCT"));
        ads_Cntrct_Rqst_Id = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Cntrct_Adc_Tiaa_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADC_TIAA_NBR");
        ads_Cntrct_Adc_Cref_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Cref_Nbr", "ADC-CREF-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADC_CREF_NBR");

        ads_Cntrct_Adc_Rqst_Info = vw_ads_Cntrct.getRecord().newGroupInGroup("ads_Cntrct_Adc_Rqst_Info", "ADC-RQST-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DS_CNTRCT_ADC_RQST_INFO");
        ads_Cntrct_Adc_Acct_Cde = ads_Cntrct_Adc_Rqst_Info.newFieldArrayInGroup("ads_Cntrct_Adc_Acct_Cde", "ADC-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_ACCT_CDE", "DS_CNTRCT_ADC_RQST_INFO");
        ads_Cntrct_Adc_Acct_Actl_Amt = ads_Cntrct_Adc_Rqst_Info.newFieldArrayInGroup("ads_Cntrct_Adc_Acct_Actl_Amt", "ADC-ACCT-ACTL-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_ACCT_ACTL_AMT", "DS_CNTRCT_ADC_RQST_INFO");
        registerRecord(vw_ads_Cntrct);

        pnd_Ia_Area = localVariables.newGroupInRecord("pnd_Ia_Area", "#IA-AREA");
        pnd_Ia_Area_Pnd_Ia_Ppcn = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Ia_Ppcn", "#IA-PPCN", FieldType.STRING, 10);
        pnd_Ia_Area_Pnd_Ia_Pay = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Ia_Pay", "#IA-PAY", FieldType.NUMERIC, 2);
        pnd_Ia_Area_Pnd_Tot_Per_Amt = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ia_Area_Pnd_Stts = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Stts", "#STTS", FieldType.STRING, 1);
        pnd_Ia_Area_Pnd_Cntrct_Final_Per_Pay_Dte = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Cntrct_Final_Per_Pay_Dte", "#CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Ia_Area_Pnd_Cntrct_Mode = pnd_Ia_Area.newFieldInGroup("pnd_Ia_Area_Pnd_Cntrct_Mode", "#CNTRCT-MODE", FieldType.NUMERIC, 3);
        pnd_Cp_Key = localVariables.newFieldInRecord("pnd_Cp_Key", "#CP-KEY", FieldType.STRING, 12);

        pnd_Cp_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cp_Key__R_Field_3", "REDEFINE", pnd_Cp_Key);
        pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cp_Key__R_Field_3.newFieldInGroup("pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cp_Key__R_Field_3.newFieldInGroup("pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 60);
        pnd_Long_Desc = localVariables.newFieldInRecord("pnd_Long_Desc", "#LONG-DESC", FieldType.STRING, 80);
        pnd_Misc_Fld = localVariables.newFieldInRecord("pnd_Misc_Fld", "#MISC-FLD", FieldType.STRING, 80);
        pnd_Table_Code = localVariables.newFieldInRecord("pnd_Table_Code", "#TABLE-CODE", FieldType.STRING, 3);
        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 29);

        pnd_Naz_Table_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_4", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_4.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_4.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_4.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Dup = localVariables.newFieldInRecord("pnd_Dup", "#DUP", FieldType.BOOLEAN, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_W_Cnt = localVariables.newFieldInRecord("pnd_W_Cnt", "#W-CNT", FieldType.NUMERIC, 5);
        pnd_W_Eff_Dte = localVariables.newFieldInRecord("pnd_W_Eff_Dte", "#W-EFF-DTE", FieldType.STRING, 8);

        pnd_W_Eff_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_W_Eff_Dte__R_Field_5", "REDEFINE", pnd_W_Eff_Dte);
        pnd_W_Eff_Dte_Pnd_W_Yr = pnd_W_Eff_Dte__R_Field_5.newFieldInGroup("pnd_W_Eff_Dte_Pnd_W_Yr", "#W-YR", FieldType.STRING, 4);

        pnd_Yr_Table = localVariables.newGroupArrayInRecord("pnd_Yr_Table", "#YR-TABLE", new DbsArrayController(1, 5));
        pnd_Yr_Table_Pnd_Yr = pnd_Yr_Table.newFieldInGroup("pnd_Yr_Table_Pnd_Yr", "#YR", FieldType.STRING, 4);
        pnd_Yr_Table_Pnd_Cnt = pnd_Yr_Table.newFieldInGroup("pnd_Yr_Table_Pnd_Cnt", "#CNT", FieldType.NUMERIC, 5);
        pnd_Overall_Tot = localVariables.newFieldInRecord("pnd_Overall_Tot", "#OVERALL-TOT", FieldType.NUMERIC, 7);
        pnd_Dated = localVariables.newFieldInRecord("pnd_Dated", "#DATED", FieldType.DATE);
        pnd_Ws_Pin = localVariables.newFieldInRecord("pnd_Ws_Pin", "#WS-PIN", FieldType.NUMERIC, 7);
        pnd_First_Name = localVariables.newFieldInRecord("pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_From = localVariables.newFieldInRecord("pnd_From", "#FROM", FieldType.STRING, 4);
        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_Code = localVariables.newFieldInRecord("pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Desc_Res = localVariables.newFieldInRecord("pnd_Desc_Res", "#DESC-RES", FieldType.STRING, 20);

        pnd_Out_Rec = localVariables.newGroupInRecord("pnd_Out_Rec", "#OUT-REC");
        pnd_Out_Rec_Pnd_O_Pin = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Pin", "#O-PIN", FieldType.STRING, 12);

        pnd_Out_Rec__R_Field_6 = pnd_Out_Rec.newGroupInGroup("pnd_Out_Rec__R_Field_6", "REDEFINE", pnd_Out_Rec_Pnd_O_Pin);
        pnd_Out_Rec_Pnd_O_Pin_5 = pnd_Out_Rec__R_Field_6.newFieldInGroup("pnd_Out_Rec_Pnd_O_Pin_5", "#O-PIN-5", FieldType.STRING, 5);
        pnd_Out_Rec_Pnd_O_Pin_7 = pnd_Out_Rec__R_Field_6.newFieldInGroup("pnd_Out_Rec_Pnd_O_Pin_7", "#O-PIN-7", FieldType.NUMERIC, 7);
        pnd_Out_Rec_Fill1 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill1", "FILL1", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Tiaa = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Tiaa", "#O-TIAA", FieldType.STRING, 10);
        pnd_Out_Rec_Fill3 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill3", "FILL3", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Cref = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Cref", "#O-CREF", FieldType.STRING, 10);
        pnd_Out_Rec_Fill4 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill4", "FILL4", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Name = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Name", "#O-NAME", FieldType.STRING, 30);
        pnd_Out_Rec_Fill5 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill5", "FILL5", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Age = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Age", "#O-AGE", FieldType.STRING, 3);
        pnd_Out_Rec_Fill6 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill6", "FILL6", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Dob = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Dob", "#O-DOB", FieldType.STRING, 10);
        pnd_Out_Rec_Fill7 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill7", "FILL7", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Annty_Start = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Annty_Start", "#O-ANNTY-START", FieldType.STRING, 10);
        pnd_Out_Rec_Fill8 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill8", "FILL8", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Accum_Settled = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Accum_Settled", "#O-ACCUM-SETTLED", FieldType.NUMERIC, 11, 2);
        pnd_Out_Rec_Fill9 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill9", "FILL9", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Payment = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Payment", "#O-PAYMENT", FieldType.NUMERIC, 11, 2);
        pnd_Out_Rec_Fill10 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill10", "FILL10", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Frequency = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Frequency", "#O-FREQUENCY", FieldType.STRING, 20);
        pnd_Out_Rec_Fill11 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill11", "FILL11", FieldType.STRING, 1);
        pnd_Out_Rec_Pnd_O_Option = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Pnd_O_Option", "#O-OPTION", FieldType.STRING, 80);
        pnd_Out_Rec_Fill12 = pnd_Out_Rec.newFieldInGroup("pnd_Out_Rec_Fill12", "FILL12", FieldType.STRING, 1);

        pnd_Out_Rec_Pnd_O_Plan_Ppgs_Tbl = pnd_Out_Rec.newGroupArrayInGroup("pnd_Out_Rec_Pnd_O_Plan_Ppgs_Tbl", "#O-PLAN-PPGS-TBL", new DbsArrayController(1, 
            21));
        pnd_Out_Rec_Pnd_O_Plan_Ppg = pnd_Out_Rec_Pnd_O_Plan_Ppgs_Tbl.newFieldInGroup("pnd_Out_Rec_Pnd_O_Plan_Ppg", "#O-PLAN-PPG", FieldType.STRING, 7);
        pnd_O_Ia_Tiaa_Status = localVariables.newFieldInRecord("pnd_O_Ia_Tiaa_Status", "#O-IA-TIAA-STATUS", FieldType.STRING, 20);
        pnd_O_Ia_Cref_Status = localVariables.newFieldInRecord("pnd_O_Ia_Cref_Status", "#O-IA-CREF-STATUS", FieldType.STRING, 20);
        pnd_Tiaa_Amt_Settled = localVariables.newFieldInRecord("pnd_Tiaa_Amt_Settled", "#TIAA-AMT-SETTLED", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Amt_Settled = localVariables.newFieldInRecord("pnd_Cref_Amt_Settled", "#CREF-AMT-SETTLED", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Total_Pymnt = localVariables.newFieldInRecord("pnd_Tiaa_Total_Pymnt", "#TIAA-TOTAL-PYMNT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Total_Pymnt = localVariables.newFieldInRecord("pnd_Cref_Total_Pymnt", "#CREF-TOTAL-PYMNT", FieldType.NUMERIC, 11, 2);
        pnd_Frequency = localVariables.newFieldInRecord("pnd_Frequency", "#FREQUENCY", FieldType.STRING, 3);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.DATE);
        pnd_Amt_Annuitized_Cref = localVariables.newFieldInRecord("pnd_Amt_Annuitized_Cref", "#AMT-ANNUITIZED-CREF", FieldType.NUMERIC, 11, 2);
        pnd_Payment_Amt = localVariables.newFieldInRecord("pnd_Payment_Amt", "#PAYMENT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);

        pnd_Start_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_7", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Yyyy = pnd_Start_Date__R_Field_7.newFieldInGroup("pnd_Start_Date_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Start_Date_Pnd_Mm = pnd_Start_Date__R_Field_7.newFieldInGroup("pnd_Start_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Start_Date_Pnd_Dd = pnd_Start_Date__R_Field_7.newFieldInGroup("pnd_Start_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Start_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_8", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_N = pnd_Start_Date__R_Field_8.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_N", "#START-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.STRING, 8);

        pnd_End_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_9", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_E_Yyyy = pnd_End_Date__R_Field_9.newFieldInGroup("pnd_End_Date_Pnd_E_Yyyy", "#E-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Date_Pnd_E_Mm = pnd_End_Date__R_Field_9.newFieldInGroup("pnd_End_Date_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        pnd_End_Date_Pnd_E_Dd = pnd_End_Date__R_Field_9.newFieldInGroup("pnd_End_Date_Pnd_E_Dd", "#E-DD", FieldType.NUMERIC, 2);

        pnd_End_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_10", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_End_Date_N = pnd_End_Date__R_Field_10.newFieldInGroup("pnd_End_Date_Pnd_End_Date_N", "#END-DATE-N", FieldType.NUMERIC, 8);
        pnd_Num_Years = localVariables.newFieldInRecord("pnd_Num_Years", "#NUM-YEARS", FieldType.NUMERIC, 4);

        pnd_Num_Years__R_Field_11 = localVariables.newGroupInRecord("pnd_Num_Years__R_Field_11", "REDEFINE", pnd_Num_Years);
        pnd_Num_Years_Pnd_Num_Years_A = pnd_Num_Years__R_Field_11.newFieldInGroup("pnd_Num_Years_Pnd_Num_Years_A", "#NUM-YEARS-A", FieldType.STRING, 4);
        pnd_Num_Months = localVariables.newFieldInRecord("pnd_Num_Months", "#NUM-MONTHS", FieldType.NUMERIC, 2);

        pnd_Num_Months__R_Field_12 = localVariables.newGroupInRecord("pnd_Num_Months__R_Field_12", "REDEFINE", pnd_Num_Months);
        pnd_Num_Months_Pnd_Num_Months_A = pnd_Num_Months__R_Field_12.newFieldInGroup("pnd_Num_Months_Pnd_Num_Months_A", "#NUM-MONTHS-A", FieldType.STRING, 
            2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 3);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Auv = localVariables.newFieldInRecord("pnd_Auv", "#AUV", FieldType.NUMERIC, 8, 4);
        pnd_Auv_Ret_Dte = localVariables.newFieldInRecord("pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_Tot_Amt = localVariables.newFieldInRecord("pnd_Tot_Amt", "#TOT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_In_Cnt = localVariables.newFieldInRecord("pnd_In_Cnt", "#IN-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnt_Adam = localVariables.newFieldInRecord("pnd_Cnt_Adam", "#CNT-ADAM", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Adam_Active = localVariables.newFieldInRecord("pnd_Cnt_Adam_Active", "#CNT-ADAM-ACTIVE", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Adam_Term = localVariables.newFieldInRecord("pnd_Cnt_Adam_Term", "#CNT-ADAM-TERM", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Adas = localVariables.newFieldInRecord("pnd_Cnt_Adas", "#CNT-ADAS", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Adas_Active = localVariables.newFieldInRecord("pnd_Cnt_Adas_Active", "#CNT-ADAS-ACTIVE", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Adas_Term = localVariables.newFieldInRecord("pnd_Cnt_Adas_Term", "#CNT-ADAS-TERM", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Dep = localVariables.newFieldInRecord("pnd_Dep", "#DEP", FieldType.BOOLEAN, 1);
        pnd_Msu = localVariables.newFieldInRecord("pnd_Msu", "#MSU", FieldType.BOOLEAN, 1);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_W_Pin = localVariables.newFieldInRecord("pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 7);

        pnd_W_Pin__R_Field_13 = localVariables.newGroupInRecord("pnd_W_Pin__R_Field_13", "REDEFINE", pnd_W_Pin);
        pnd_W_Pin_Pnd_W_Pin_A = pnd_W_Pin__R_Field_13.newFieldInGroup("pnd_W_Pin_Pnd_W_Pin_A", "#W-PIN-A", FieldType.STRING, 7);
        pnd_Ppcn = localVariables.newFieldInRecord("pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        pnd_In_Option = localVariables.newFieldInRecord("pnd_In_Option", "#IN-OPTION", FieldType.NUMERIC, 2);
        pnd_Out_Desc = localVariables.newFieldInRecord("pnd_Out_Desc", "#OUT-DESC", FieldType.STRING, 50);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 5);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 5);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_P.reset();
        vw_ads_R.reset();
        vw_ads_Cntrct.reset();

        ldaIaal999.initializeValues();

        localVariables.reset();
        pnd_Out_Rec_Fill1.setInitialValue(";");
        pnd_Out_Rec_Fill3.setInitialValue(";");
        pnd_Out_Rec_Fill4.setInitialValue(";");
        pnd_Out_Rec_Fill5.setInitialValue(";");
        pnd_Out_Rec_Fill6.setInitialValue(";");
        pnd_Out_Rec_Fill7.setInitialValue(";");
        pnd_Out_Rec_Fill8.setInitialValue(";");
        pnd_Out_Rec_Fill9.setInitialValue(";");
        pnd_Out_Rec_Fill10.setInitialValue(";");
        pnd_Out_Rec_Fill11.setInitialValue(";");
        pnd_Out_Rec_Fill12.setInitialValue(";");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp9920() throws Exception
    {
        super("Adsp9920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133
                                                                                                                                                                          //Natural: PERFORM CHECK-ADAM
        sub_Check_Adam();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-ADAS
        sub_Check_Adas();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "TOTAL ADAM REQUESTS   ",pnd_Cnt_Adam);                                                                                                     //Natural: WRITE 'TOTAL ADAM REQUESTS   ' #CNT-ADAM
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ADAM ACTIVE     ",pnd_Cnt_Adam_Active);                                                                                              //Natural: WRITE 'TOTAL ADAM ACTIVE     ' #CNT-ADAM-ACTIVE
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ADAM TERMINATED ",pnd_Cnt_Adam_Term);                                                                                                //Natural: WRITE 'TOTAL ADAM TERMINATED ' #CNT-ADAM-TERM
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL ADAS REQUESTS   ",pnd_Cnt_Adas);                                                                                             //Natural: WRITE / 'TOTAL ADAS REQUESTS   ' #CNT-ADAS
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ADAS ACTIVE     ",pnd_Cnt_Adas_Active);                                                                                              //Natural: WRITE 'TOTAL ADAS ACTIVE     ' #CNT-ADAS-ACTIVE
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ADAS TERMINATED ",pnd_Cnt_Adas_Term);                                                                                                //Natural: WRITE 'TOTAL ADAS TERMINATED ' #CNT-ADAS-TERM
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ADAS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ADAM
        //*    END-IF
        //*  ELSE
        //*    IF SUBSTR(#I-TIAA,1,2) = 'IP'
        //*      #OUT-REC.#O-OPTION := 'IPRO'
        //*    ELSE
        //*      #OUT-REC.#O-OPTION := 'TPA'
        //*    END-IF
        //*  END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ADAS-RESETTLEMENTS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OPTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AGE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENCE-DESC
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
    }
    private void sub_Check_Adas() throws Exception                                                                                                                        //Natural: CHECK-ADAS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_ads_P.startDatabaseRead                                                                                                                                        //Natural: READ ADS-P BY ADP-SUPER1 STARTING FROM 'C'
        (
        "READ01",
        new Wc[] { new Wc("ADP_SUPER1", ">=", "C", WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER1", "ASC") }
        );
        READ01:
        while (condition(vw_ads_P.readNextRow("READ01")))
        {
            if (condition(ads_P_Adp_Opn_Clsd_Ind.notEquals("C")))                                                                                                         //Natural: IF ADP-OPN-CLSD-IND NE 'C'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ads_P_Adp_Annt_Typ_Cde.equals("M") && DbsUtil.maskMatches(ads_P_Adp_Stts_Cde,"'T'"))))                                                        //Natural: ACCEPT IF ADP-ANNT-TYP-CDE EQ 'M' AND ADP-STTS-CDE = MASK ( 'T' )
            {
                continue;
            }
            pnd_Tiaa_Amt_Settled.reset();                                                                                                                                 //Natural: RESET #TIAA-AMT-SETTLED #CREF-AMT-SETTLED #TIAA-TOTAL-PYMNT #CREF-TOTAL-PYMNT
            pnd_Cref_Amt_Settled.reset();
            pnd_Tiaa_Total_Pymnt.reset();
            pnd_Cref_Total_Pymnt.reset();
            pnd_Cnt_Adas.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-ADAS
            vw_ads_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND ADS-CNTRCT WITH ADS-CNTRCT.RQST-ID = ADS-P.RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", ads_P_Rqst_Id, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Cntrct.readNextRow("FIND01")))
            {
                vw_ads_Cntrct.setIfNotFoundControlFlag(false);
                FOR01:                                                                                                                                                    //Natural: FOR #C 1 20
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
                {
                    if (condition(ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals(" ")))                                                                                   //Natural: IF ADC-ACCT-CDE ( #C ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("T") || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("R") || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("Y")  //Natural: IF ADC-ACCT-CDE ( #C ) = 'T' OR = 'R' OR = 'Y' OR = 'D'
                        || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("D")))
                    {
                        pnd_Tiaa_Amt_Settled.nadd(ads_Cntrct_Adc_Acct_Actl_Amt.getValue(pnd_C));                                                                          //Natural: ASSIGN #TIAA-AMT-SETTLED := #TIAA-AMT-SETTLED + ADC-ACCT-ACTL-AMT ( #C )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cref_Amt_Settled.nadd(ads_Cntrct_Adc_Acct_Actl_Amt.getValue(pnd_C));                                                                          //Natural: ASSIGN #CREF-AMT-SETTLED := #CREF-AMT-SETTLED + ADC-ACCT-ACTL-AMT ( #C )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-ADAS-RESETTLEMENTS
            sub_Check_Adas_Resettlements();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Out_Rec.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #OUT-REC
            pnd_Code.setValue(ads_P_Adp_Frst_Annt_Rsdnc_Cde);                                                                                                             //Natural: ASSIGN #CODE := ADS-P.ADP-FRST-ANNT-RSDNC-CDE
            pnd_Out_Rec_Pnd_O_Name.setValue(DbsUtil.compress(ads_P_Adp_Frst_Annt_Frst_Nme, ads_P_Adp_Frst_Annt_Mid_Nme.getSubstring(1,1), ads_P_Adp_Frst_Annt_Lst_Nme));  //Natural: COMPRESS ADP-FRST-ANNT-FRST-NME SUBSTR ( ADP-FRST-ANNT-MID-NME,1,1 ) ADP-FRST-ANNT-LST-NME INTO #O-NAME
            //*  MOVE EDITED ADP-FRST-ANNT-SSN (EM=999999999)
            //*    TO #O-SSN
            //*  MOVE EDITED ADS-P.ADP-UNIQUE-ID (EM=9999999)   /* 082017 START
            //*    TO #O-PIN
            //*  082017 END
            pnd_Out_Rec_Pnd_O_Pin.setValueEdited(ads_P_Adp_Unique_Id,new ReportEditMask("ZZZZZ9999999"));                                                                 //Natural: MOVE EDITED ADS-P.ADP-UNIQUE-ID ( EM = ZZZZZ9999999 ) TO #O-PIN
            pnd_Out_Rec_Pnd_O_Annty_Start.setValueEdited(ads_P_Adp_Annty_Strt_Dte,new ReportEditMask("MM/DD/YYYY"));                                                      //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = MM/DD/YYYY ) TO #O-ANNTY-START
            pnd_Out_Rec_Pnd_O_Tiaa.setValue(ads_P_Adp_Ia_Tiaa_Nbr);                                                                                                       //Natural: ASSIGN #O-TIAA := ADP-IA-TIAA-NBR
            pnd_Out_Rec_Pnd_O_Cref.setValue(ads_P_Adp_Ia_Cref_Nbr);                                                                                                       //Natural: ASSIGN #O-CREF := ADP-IA-CREF-NBR
            pnd_Frequency.reset();                                                                                                                                        //Natural: RESET #FREQUENCY
            FOR02:                                                                                                                                                        //Natural: FOR #A = 1 TO 2
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(2)); pnd_A.nadd(1))
            {
                pnd_Ia_Area.reset();                                                                                                                                      //Natural: RESET #IA-AREA
                pnd_Ia_Area_Pnd_Ia_Pay.setValue(1);                                                                                                                       //Natural: ASSIGN #IA-PAY := 01
                if (condition(pnd_A.equals(1)))                                                                                                                           //Natural: IF #A = 1
                {
                    pnd_Ia_Area_Pnd_Ia_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Tiaa);                                                                                             //Natural: ASSIGN #IA-PPCN := #O-TIAA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ia_Area_Pnd_Ia_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Cref);                                                                                             //Natural: ASSIGN #IA-PPCN := #O-CREF
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Adsn9920.class , getCurrentProcessState(), pnd_Ia_Area);                                                                                  //Natural: CALLNAT 'ADSN9920' #IA-AREA
                if (condition(Global.isEscape())) return;
                short decideConditionsMet841 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STTS = 'T'
                if (condition(pnd_Ia_Area_Pnd_Stts.equals("T")))
                {
                    decideConditionsMet841++;
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("TERMINATED");                                                                                                      //Natural: ASSIGN #O-IA-TIAA-STATUS := 'TERMINATED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("TERMINATED");                                                                                                      //Natural: ASSIGN #O-IA-CREF-STATUS := 'TERMINATED'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #STTS = 'E'
                else if (condition(pnd_Ia_Area_Pnd_Stts.equals("E")))
                {
                    decideConditionsMet841++;
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("EXPIRED");                                                                                                         //Natural: ASSIGN #O-IA-TIAA-STATUS := 'EXPIRED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("EXPIRED");                                                                                                         //Natural: ASSIGN #O-IA-CREF-STATUS := 'EXPIRED'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #STTS = 'A'
                else if (condition(pnd_Ia_Area_Pnd_Stts.equals("A")))
                {
                    decideConditionsMet841++;
                    pnd_Frequency.setValueEdited(pnd_Ia_Area_Pnd_Cntrct_Mode,new ReportEditMask("999"));                                                                  //Natural: MOVE EDITED #CNTRCT-MODE ( EM = 999 ) TO #FREQUENCY
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("ACTIVE");                                                                                                          //Natural: ASSIGN #O-IA-TIAA-STATUS := 'ACTIVE'
                        pnd_Tiaa_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                           //Natural: ASSIGN #TIAA-TOTAL-PYMNT := #TIAA-TOTAL-PYMNT + #TOT-PER-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("ACTIVE");                                                                                                          //Natural: ASSIGN #O-IA-CREF-STATUS := 'ACTIVE'
                        pnd_Cref_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                           //Natural: ASSIGN #CREF-TOTAL-PYMNT := #CREF-TOTAL-PYMNT + #TOT-PER-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("N/A");                                                                                                             //Natural: ASSIGN #O-IA-TIAA-STATUS := 'N/A'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("N/A");                                                                                                             //Natural: ASSIGN #O-IA-CREF-STATUS := 'N/A'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_A.equals(1)))                                                                                                                           //Natural: IF #A = 1
                {
                    if (condition(pnd_O_Ia_Tiaa_Status.equals("TERMINATED")))                                                                                             //Natural: IF #O-IA-TIAA-STATUS = 'TERMINATED'
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-IA
                        sub_Check_Ia();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_O_Ia_Cref_Status.equals("TERMINATED")))                                                                                             //Natural: IF #O-IA-CREF-STATUS = 'TERMINATED'
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-IA
                        sub_Check_Ia();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Out_Rec_Pnd_O_Dob.setValueEdited(ads_P_Adp_Frst_Annt_Dte_Of_Brth,new ReportEditMask("MM/DD/YYYY"));                                                       //Natural: MOVE EDITED ADP-FRST-ANNT-DTE-OF-BRTH ( EM = MM/DD/YYYY ) TO #O-DOB
            short decideConditionsMet881 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '8'
            if (condition(pnd_Frequency.getSubstring(1,1).equals("8")))
            {
                decideConditionsMet881++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("ANNUAL");                                                                                                           //Natural: ASSIGN #O-FREQUENCY := 'ANNUAL'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '1'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("1")))
            {
                decideConditionsMet881++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("MONTHLY");                                                                                                          //Natural: ASSIGN #O-FREQUENCY := 'MONTHLY'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '6'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("6")))
            {
                decideConditionsMet881++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("QUARTERLY");                                                                                                        //Natural: ASSIGN #O-FREQUENCY := 'QUARTERLY'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '7'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("7")))
            {
                decideConditionsMet881++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("SEMI-ANNUAL");                                                                                                      //Natural: ASSIGN #O-FREQUENCY := 'SEMI-ANNUAL'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition((((pnd_O_Ia_Tiaa_Status.equals("TERMINATED") || pnd_O_Ia_Tiaa_Status.equals("EXPIRED")) || pnd_O_Ia_Tiaa_Status.equals("N/A"))                  //Natural: IF ( #O-IA-TIAA-STATUS = 'TERMINATED' OR = 'EXPIRED' OR = 'N/A' ) AND ( #O-IA-CREF-STATUS = 'TERMINATED' OR = 'EXPIRED' OR = 'N/A' )
                && ((pnd_O_Ia_Cref_Status.equals("TERMINATED") || pnd_O_Ia_Cref_Status.equals("EXPIRED")) || pnd_O_Ia_Cref_Status.equals("N/A")))))
            {
                //*    #O-AGE := 0
                pnd_Cnt_Adas_Term.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-ADAS-TERM
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Start_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #START-DATE
                pnd_End_Date.setValueEdited(ads_P_Adp_Frst_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #END-DATE
                                                                                                                                                                          //Natural: PERFORM GET-AGE
                sub_Get_Age();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Out_Rec_Pnd_O_Age.setValue(pnd_Num_Years);                                                                                                            //Natural: ASSIGN #O-AGE := #NUM-YEARS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt_Adas_Active.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CNT-ADAS-ACTIVE
            //*  OS- 121213 START
            //*  #TABLE-CODE := 'AO'
            //*  IF ADP-GRNTEE-PERIOD GT 0
            //*    COMPRESS ADP-ANNTY-OPTN ADP-GRNTEE-PERIOD
            //*      INTO #NAZ-TABLE-KEY LEAVING NO
            //*  ELSE
            //*    #NAZ-TABLE-KEY := ADP-ANNTY-OPTN
            //*  END-IF
            //*  IF ADP-ANNTY-OPTN = 'AC' OR = 'IR'
            //*    IF ADP-GRNTEE-PERIOD GT 0
            //*      COMPRESS ADP-ANNTY-OPTN ADP-GRNTEE-PERIOD
            //*        INTO #OUT-REC.#O-OPTION LEAVING NO
            //*    ELSE
            //*      #OUT-REC.#O-OPTION := ADP-ANNTY-OPTN
            //*    END-IF
            //*  ELSE
            if (condition(pnd_O_Ia_Tiaa_Status.equals("ACTIVE")))                                                                                                         //Natural: IF #O-IA-TIAA-STATUS = 'ACTIVE'
            {
                pnd_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Tiaa);                                                                                                                //Natural: ASSIGN #PPCN := #O-TIAA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Cref);                                                                                                                //Natural: ASSIGN #PPCN := #O-CREF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-OPTION
            sub_Get_Option();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Out_Rec_Pnd_O_Option.setValue(pnd_Out_Desc);                                                                                                              //Natural: ASSIGN #OUT-REC.#O-OPTION := #OUT-DESC
            //*  END-IF
            //*  OS - 121213 END
            pnd_Out_Rec_Pnd_O_Plan_Ppg.getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ads_P_Adp_Plan_Nbr, ","));                                    //Natural: COMPRESS ADP-PLAN-NBR ',' INTO #O-PLAN-PPG ( 1 ) LEAVING NO
            FOR03:                                                                                                                                                        //Natural: FOR #C 2 20
            for (pnd_C.setValue(2); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                pnd_Out_Rec_Pnd_O_Plan_Ppg.getValue(pnd_C).setValue("      ,");                                                                                           //Natural: ASSIGN #O-PLAN-PPG ( #C ) := '      ,'
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Out_Rec_Pnd_O_Accum_Settled.compute(new ComputeParameters(false, pnd_Out_Rec_Pnd_O_Accum_Settled), pnd_Tiaa_Amt_Settled.add(pnd_Cref_Amt_Settled));       //Natural: ASSIGN #O-ACCUM-SETTLED := #TIAA-AMT-SETTLED + #CREF-AMT-SETTLED
            pnd_Out_Rec_Pnd_O_Payment.compute(new ComputeParameters(false, pnd_Out_Rec_Pnd_O_Payment), pnd_Tiaa_Total_Pymnt.add(pnd_Cref_Total_Pymnt));                   //Natural: ASSIGN #O-PAYMENT := #TIAA-TOTAL-PYMNT + #CREF-TOTAL-PYMNT
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
            sub_Write_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Check_Adam() throws Exception                                                                                                                        //Natural: CHECK-ADAM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 #IN-REC
        while (condition(getWorkFiles().read(1, pnd_In_Rec)))
        {
            if (condition(!(DbsUtil.maskMatches(pnd_In_Rec_Pnd_I_Tiaa,"'Y'"))))                                                                                           //Natural: ACCEPT IF #I-TIAA EQ MASK ( 'Y' )
            {
                continue;
            }
            pnd_Out_Rec.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #OUT-REC
            pnd_Tiaa_Amt_Settled.reset();                                                                                                                                 //Natural: RESET #TIAA-AMT-SETTLED #CREF-AMT-SETTLED #TIAA-TOTAL-PYMNT #CREF-TOTAL-PYMNT #B
            pnd_Cref_Amt_Settled.reset();
            pnd_Tiaa_Total_Pymnt.reset();
            pnd_Cref_Total_Pymnt.reset();
            pnd_B.reset();
            pnd_Cnt_Adam.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-ADAM
            FOR04:                                                                                                                                                        //Natural: FOR #C 1 21
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(21)); pnd_C.nadd(1))
            {
                if (condition(pnd_In_Rec_Pnd_I_Ppg.getValue(pnd_C).equals(" ")))                                                                                          //Natural: IF #I-PPG ( #C ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_B.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #B
                pnd_Out_Rec_Pnd_O_Plan_Ppg.getValue(pnd_B).setValue(DbsUtil.compress(pnd_In_Rec_Pnd_I_Ppg.getValue(pnd_C), ","));                                         //Natural: COMPRESS #I-PPG ( #C ) ',' INTO #O-PLAN-PPG ( #B )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR05:                                                                                                                                                        //Natural: FOR #C 1 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                if (condition(pnd_In_Rec_Pnd_I_Acct_Cde.getValue(pnd_C).equals(" ")))                                                                                     //Natural: IF #I-ACCT-CDE ( #C ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_In_Rec_Pnd_I_Acct_Cde.getValue(pnd_C).equals("T") || pnd_In_Rec_Pnd_I_Acct_Cde.getValue(pnd_C).equals("R") || pnd_In_Rec_Pnd_I_Acct_Cde.getValue(pnd_C).equals("Y"))) //Natural: IF #I-ACCT-CDE ( #C ) = 'T' OR = 'R' OR = 'Y'
                {
                    pnd_Tiaa_Amt_Settled.nadd(pnd_In_Rec_Pnd_I_Acct_Actl_Amt.getValue(pnd_C));                                                                            //Natural: ASSIGN #TIAA-AMT-SETTLED := #TIAA-AMT-SETTLED + #I-ACCT-ACTL-AMT ( #C )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cref_Amt_Settled.nadd(pnd_In_Rec_Pnd_I_Acct_Actl_Amt.getValue(pnd_C));                                                                            //Natural: ASSIGN #CREF-AMT-SETTLED := #CREF-AMT-SETTLED + #I-ACCT-ACTL-AMT ( #C )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR06:                                                                                                                                                        //Natural: FOR #C 2 20
            for (pnd_C.setValue(2); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                if (condition(pnd_Out_Rec_Pnd_O_Plan_Ppg.getValue(pnd_C).equals(" ")))                                                                                    //Natural: IF #O-PLAN-PPG ( #C ) = ' '
                {
                    pnd_Out_Rec_Pnd_O_Plan_Ppg.getValue(pnd_C).setValue("      ,");                                                                                       //Natural: ASSIGN #O-PLAN-PPG ( #C ) := '      ,'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Code.setValue(pnd_In_Rec_Pnd_I_1st_Rsdnce);                                                                                                               //Natural: ASSIGN #CODE := #I-1ST-RSDNCE
            pnd_Out_Rec_Pnd_O_Name.setValue(pnd_In_Rec_Pnd_I_1st_Name);                                                                                                   //Natural: ASSIGN #O-NAME := #I-1ST-NAME
            //*  MOVE EDITED #I-1ST-SSN (EM=999999999) TO #O-SSN
            //*  MOVE EDITED #I-1ST-PIN (EM=9999999) TO #O-PIN  /* 082017
            pnd_Out_Rec_Pnd_O_Pin_5.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #O-PIN-5
            //*  082017
            pnd_Out_Rec_Pnd_O_Pin_7.setValue(pnd_In_Rec_Pnd_I_1st_Pin);                                                                                                   //Natural: MOVE #I-1ST-PIN TO #O-PIN-7
            pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_In_Rec_Pnd_I_Annty_Start);                                                                        //Natural: MOVE EDITED #I-ANNTY-START TO #DATED ( EM = YYYYMMDD )
            pnd_Out_Rec_Pnd_O_Annty_Start.setValueEdited(pnd_Dated,new ReportEditMask("MM/DD/YYYY"));                                                                     //Natural: MOVE EDITED #DATED ( EM = MM/DD/YYYY ) TO #O-ANNTY-START
            pnd_Out_Rec_Pnd_O_Tiaa.setValue(pnd_In_Rec_Pnd_I_Tiaa);                                                                                                       //Natural: ASSIGN #O-TIAA := #I-TIAA
            pnd_Out_Rec_Pnd_O_Cref.setValue(pnd_In_Rec_Pnd_I_Cref);                                                                                                       //Natural: ASSIGN #O-CREF := #I-CREF
            pnd_Frequency.reset();                                                                                                                                        //Natural: RESET #FREQUENCY
            FOR07:                                                                                                                                                        //Natural: FOR #A = 1 TO 2
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(2)); pnd_A.nadd(1))
            {
                pnd_Ia_Area.reset();                                                                                                                                      //Natural: RESET #IA-AREA
                if (condition(pnd_A.equals(1)))                                                                                                                           //Natural: IF #A = 1
                {
                    pnd_Ia_Area_Pnd_Ia_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Tiaa);                                                                                             //Natural: ASSIGN #IA-PPCN := #O-TIAA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ia_Area_Pnd_Ia_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Cref);                                                                                             //Natural: ASSIGN #IA-PPCN := #O-CREF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ia_Area_Pnd_Ia_Pay.setValue(1);                                                                                                                       //Natural: ASSIGN #IA-PAY := 01
                DbsUtil.callnat(Adsn9920.class , getCurrentProcessState(), pnd_Ia_Area);                                                                                  //Natural: CALLNAT 'ADSN9920' #IA-AREA
                if (condition(Global.isEscape())) return;
                short decideConditionsMet989 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STTS = 'T'
                if (condition(pnd_Ia_Area_Pnd_Stts.equals("T")))
                {
                    decideConditionsMet989++;
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("TERMINATED");                                                                                                      //Natural: ASSIGN #O-IA-TIAA-STATUS := 'TERMINATED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("TERMINATED");                                                                                                      //Natural: ASSIGN #O-IA-CREF-STATUS := 'TERMINATED'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #STTS = 'E'
                else if (condition(pnd_Ia_Area_Pnd_Stts.equals("E")))
                {
                    decideConditionsMet989++;
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("EXPIRED");                                                                                                         //Natural: ASSIGN #O-IA-TIAA-STATUS := 'EXPIRED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("EXPIRED");                                                                                                         //Natural: ASSIGN #O-IA-CREF-STATUS := 'EXPIRED'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #STTS = 'A'
                else if (condition(pnd_Ia_Area_Pnd_Stts.equals("A")))
                {
                    decideConditionsMet989++;
                    pnd_Frequency.setValueEdited(pnd_Ia_Area_Pnd_Cntrct_Mode,new ReportEditMask("999"));                                                                  //Natural: MOVE EDITED #CNTRCT-MODE ( EM = 999 ) TO #FREQUENCY
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("ACTIVE");                                                                                                          //Natural: ASSIGN #O-IA-TIAA-STATUS := 'ACTIVE'
                        pnd_Tiaa_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                           //Natural: ASSIGN #TIAA-TOTAL-PYMNT := #TIAA-TOTAL-PYMNT + #TOT-PER-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("ACTIVE");                                                                                                          //Natural: ASSIGN #O-IA-CREF-STATUS := 'ACTIVE'
                        pnd_Cref_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                           //Natural: ASSIGN #CREF-TOTAL-PYMNT := #CREF-TOTAL-PYMNT + #TOT-PER-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    if (condition(pnd_A.equals(1)))                                                                                                                       //Natural: IF #A = 1
                    {
                        pnd_O_Ia_Tiaa_Status.setValue("N/A");                                                                                                             //Natural: ASSIGN #O-IA-TIAA-STATUS := 'N/A'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_O_Ia_Cref_Status.setValue("N/A");                                                                                                             //Natural: ASSIGN #O-IA-CREF-STATUS := 'N/A'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_A.equals(1)))                                                                                                                           //Natural: IF #A = 1
                {
                    if (condition(pnd_O_Ia_Tiaa_Status.equals("TERMINATED")))                                                                                             //Natural: IF #O-IA-TIAA-STATUS = 'TERMINATED'
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-IA
                        sub_Check_Ia();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_O_Ia_Cref_Status.equals("TERMINATED")))                                                                                             //Natural: IF #O-IA-CREF-STATUS = 'TERMINATED'
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-IA
                        sub_Check_Ia();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Dated.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_In_Rec_Pnd_I_1st_Dob);                                                                            //Natural: MOVE EDITED #I-1ST-DOB TO #DATED ( EM = YYYYMMDD )
            pnd_Out_Rec_Pnd_O_Dob.setValueEdited(pnd_Dated,new ReportEditMask("MM/DD/YYYY"));                                                                             //Natural: MOVE EDITED #DATED ( EM = MM/DD/YYYY ) TO #O-DOB
            short decideConditionsMet1030 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '8'
            if (condition(pnd_Frequency.getSubstring(1,1).equals("8")))
            {
                decideConditionsMet1030++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("ANNUAL");                                                                                                           //Natural: ASSIGN #O-FREQUENCY := 'ANNUAL'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '1'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("1")))
            {
                decideConditionsMet1030++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("MONTHLY");                                                                                                          //Natural: ASSIGN #O-FREQUENCY := 'MONTHLY'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '6'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("6")))
            {
                decideConditionsMet1030++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("QUARTERLY");                                                                                                        //Natural: ASSIGN #O-FREQUENCY := 'QUARTERLY'
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #FREQUENCY,1,1 ) = '7'
            else if (condition(pnd_Frequency.getSubstring(1,1).equals("7")))
            {
                decideConditionsMet1030++;
                pnd_Out_Rec_Pnd_O_Frequency.setValue("SEMI-ANNUAL");                                                                                                      //Natural: ASSIGN #O-FREQUENCY := 'SEMI-ANNUAL'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition((((pnd_O_Ia_Tiaa_Status.equals("TERMINATED") || pnd_O_Ia_Tiaa_Status.equals("EXPIRED")) || pnd_O_Ia_Tiaa_Status.equals("N/A"))                  //Natural: IF ( #O-IA-TIAA-STATUS = 'TERMINATED' OR = 'EXPIRED' OR = 'N/A' ) AND ( #O-IA-CREF-STATUS = 'TERMINATED' OR = 'EXPIRED' OR = 'N/A' )
                && ((pnd_O_Ia_Cref_Status.equals("TERMINATED") || pnd_O_Ia_Cref_Status.equals("EXPIRED")) || pnd_O_Ia_Cref_Status.equals("N/A")))))
            {
                //*    #O-AGE := 0
                pnd_Cnt_Adam_Term.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-ADAM-TERM
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Start_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #START-DATE
                pnd_End_Date.setValue(pnd_In_Rec_Pnd_I_1st_Dob);                                                                                                          //Natural: MOVE #I-1ST-DOB TO #END-DATE
                                                                                                                                                                          //Natural: PERFORM GET-AGE
                sub_Get_Age();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Out_Rec_Pnd_O_Age.setValue(pnd_Num_Years);                                                                                                            //Natural: ASSIGN #O-AGE := #NUM-YEARS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnt_Adam_Active.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CNT-ADAM-ACTIVE
            //*  OS - 121213 START
            //*  IF #I-TIAA = MASK('Y')
            //*    #TABLE-CODE := 'AO'
            //*    IF #I-GRNTEE-PRD GT 0
            //*      COMPRESS #I-OPTION #I-GRNTEE-PRD
            //*        INTO #NAZ-TABLE-KEY LEAVING NO
            //*    ELSE
            //*      #NAZ-TABLE-KEY := #I-OPTION
            //*    END-IF
            //*    IF #I-OPTION = 'AC' OR = 'IR'
            //*      IF #I-GRNTEE-PRD GT 0
            //*        COMPRESS #I-OPTION #I-GRNTEE-PRD
            //*          INTO #OUT-REC.#O-OPTION LEAVING NO
            //*      ELSE
            //*        #OUT-REC.#O-OPTION := #I-OPTION
            //*      END-IF
            //*    ELSE
            if (condition(pnd_O_Ia_Tiaa_Status.equals("ACTIVE")))                                                                                                         //Natural: IF #O-IA-TIAA-STATUS = 'ACTIVE'
            {
                pnd_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Tiaa);                                                                                                                //Natural: ASSIGN #PPCN := #O-TIAA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ppcn.setValue(pnd_Out_Rec_Pnd_O_Cref);                                                                                                                //Natural: ASSIGN #PPCN := #O-CREF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-OPTION
            sub_Get_Option();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Out_Rec_Pnd_O_Option.setValue(pnd_Out_Desc);                                                                                                              //Natural: ASSIGN #OUT-REC.#O-OPTION := #OUT-DESC
            pnd_Out_Rec_Pnd_O_Accum_Settled.compute(new ComputeParameters(false, pnd_Out_Rec_Pnd_O_Accum_Settled), pnd_Tiaa_Amt_Settled.add(pnd_Cref_Amt_Settled));       //Natural: ASSIGN #O-ACCUM-SETTLED := #TIAA-AMT-SETTLED + #CREF-AMT-SETTLED
            pnd_Out_Rec_Pnd_O_Payment.compute(new ComputeParameters(false, pnd_Out_Rec_Pnd_O_Payment), pnd_Tiaa_Total_Pymnt.add(pnd_Cref_Total_Pymnt));                   //Natural: ASSIGN #O-PAYMENT := #TIAA-TOTAL-PYMNT + #CREF-TOTAL-PYMNT
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
            sub_Write_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Check_Adas_Resettlements() throws Exception                                                                                                          //Natural: CHECK-ADAS-RESETTLEMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_ads_R.startDatabaseRead                                                                                                                                        //Natural: READ ADS-R BY ADS-R.RQST-ID STARTING FROM ADS-P.ADP-UNIQUE-ID-A
        (
        "READ03",
        new Wc[] { new Wc("RQST_ID", ">=", ads_P_Adp_Unique_Id_A, WcType.BY) },
        new Oc[] { new Oc("RQST_ID", "ASC") }
        );
        READ03:
        while (condition(vw_ads_R.readNextRow("READ03")))
        {
            if (condition(ads_R_Adp_Unique_Id.notEquals(ads_P_Adp_Unique_Id)))                                                                                            //Natural: IF ADS-R.ADP-UNIQUE-ID NE ADS-P.ADP-UNIQUE-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(DbsUtil.maskMatches(ads_R_Adp_Stts_Cde,"'T'") && ads_R_Adp_Orig_Rqst_Id.equals(ads_P_Rqst_Id))))                                              //Natural: ACCEPT IF ADS-R.ADP-STTS-CDE = MASK ( 'T' ) AND ADS-R.ADP-ORIG-RQST-ID = ADS-P.RQST-ID
            {
                continue;
            }
            //*  WRITE 'ADAS RESETTLEMENT FOUND FOR' ADS-P.RQST-ID
            vw_ads_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND ADS-CNTRCT WITH ADS-CNTRCT.RQST-ID = ADS-R.RQST-ID
            (
            "FIND02",
            new Wc[] { new Wc("RQST_ID", "=", ads_R_Rqst_Id, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_ads_Cntrct.readNextRow("FIND02")))
            {
                vw_ads_Cntrct.setIfNotFoundControlFlag(false);
                FOR08:                                                                                                                                                    //Natural: FOR #C 1 20
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
                {
                    if (condition(ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals(" ")))                                                                                   //Natural: IF ADC-ACCT-CDE ( #C ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("T") || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("R") || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("Y")  //Natural: IF ADC-ACCT-CDE ( #C ) = 'T' OR = 'R' OR = 'Y' OR = 'D'
                        || ads_Cntrct_Adc_Acct_Cde.getValue(pnd_C).equals("D")))
                    {
                        pnd_Tiaa_Amt_Settled.nadd(ads_Cntrct_Adc_Acct_Actl_Amt.getValue(pnd_C));                                                                          //Natural: ASSIGN #TIAA-AMT-SETTLED := #TIAA-AMT-SETTLED + ADC-ACCT-ACTL-AMT ( #C )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cref_Amt_Settled.nadd(ads_Cntrct_Adc_Acct_Actl_Amt.getValue(pnd_C));                                                                          //Natural: ASSIGN #CREF-AMT-SETTLED := #CREF-AMT-SETTLED + ADC-ACCT-ACTL-AMT ( #C )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Check_Ia() throws Exception                                                                                                                          //Natural: CHECK-IA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IA-PPCN
        (
        "FIND03",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Ia_Area_Pnd_Ia_Ppcn, WcType.WITH) }
        );
        FIND03:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FIND03", true)))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal999.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "IAA-CNTRCT NOT FOUND FOR",pnd_Ia_Area_Pnd_Ia_Ppcn);                                                                                //Natural: WRITE 'IAA-CNTRCT NOT FOUND FOR' #IA-PPCN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaIaal999.getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte().greater(getZero())))                                                                       //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pnd_Ia_Area_Pnd_Ia_Ppcn);                                                                                            //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := #IA-PPCN
        pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(2);                                                                                                                 //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 2
        ldaIaal999.getVw_cpr().startDatabaseRead                                                                                                                          //Natural: READ CPR WITH CNTRCT-PAYEE-KEY = #CP-KEY
        (
        "READ04",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cp_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ04:
        while (condition(ldaIaal999.getVw_cpr().readNextRow("READ04")))
        {
            if (condition(pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr.notEquals(ldaIaal999.getCpr_Cntrct_Part_Ppcn_Nbr()) || ldaIaal999.getCpr_Cntrct_Part_Payee_Cde().notEquals(pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde))) //Natural: IF #CNTRCT-PART-PPCN-NBR NE CNTRCT-PART-PPCN-NBR OR CNTRCT-PART-PAYEE-CDE NE #CNTRCT-PART-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ia_Area_Pnd_Ia_Pay.setValue(ldaIaal999.getCpr_Cntrct_Part_Payee_Cde());                                                                                   //Natural: ASSIGN #IA-PAY := CNTRCT-PART-PAYEE-CDE
            DbsUtil.callnat(Adsn9920.class , getCurrentProcessState(), pnd_Ia_Area);                                                                                      //Natural: CALLNAT 'ADSN9920' #IA-AREA
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Ia_Area_Pnd_Stts.equals("A")))                                                                                                              //Natural: IF #STTS = 'A'
            {
                pnd_Frequency.setValueEdited(pnd_Ia_Area_Pnd_Cntrct_Mode,new ReportEditMask("999"));                                                                      //Natural: MOVE EDITED #CNTRCT-MODE ( EM = 999 ) TO #FREQUENCY
                if (condition(pnd_A.equals(1)))                                                                                                                           //Natural: IF #A = 1
                {
                    pnd_O_Ia_Tiaa_Status.setValue("ACTIVE");                                                                                                              //Natural: ASSIGN #O-IA-TIAA-STATUS := 'ACTIVE'
                    pnd_Tiaa_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                               //Natural: ASSIGN #TIAA-TOTAL-PYMNT := #TIAA-TOTAL-PYMNT + #TOT-PER-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_O_Ia_Cref_Status.setValue("ACTIVE");                                                                                                              //Natural: ASSIGN #O-IA-CREF-STATUS := 'ACTIVE'
                    pnd_Cref_Total_Pymnt.nadd(pnd_Ia_Area_Pnd_Tot_Per_Amt);                                                                                               //Natural: ASSIGN #CREF-TOTAL-PYMNT := #CREF-TOTAL-PYMNT + #TOT-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Option() throws Exception                                                                                                                        //Natural: GET-OPTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  OS - 121213 START
        //* *RESET #DESC #LONG-DESC #MISC-FLD
        //* *CALLNAT 'ADSN032' MSG-INFO-SUB
        //* *  #TABLE-CODE
        //* *  #NAZ-TABLE-KEY
        //* *  #DESC
        //* *  #LONG-DESC
        //* *  #MISC-FLD
        //* *IF MSG-INFO-SUB.##RETURN-CODE = 'E'
        //* *  WRITE MSG-INFO-SUB.##MSG
        //* *  WRITE 'PIN ' #O-PIN ' IA TIAA' #O-TIAA
        //* *END-IF
        pnd_Out_Desc.reset();                                                                                                                                             //Natural: RESET #OUT-DESC
        ldaIaal999.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #PPCN
        (
        "FIND04",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Ppcn, WcType.WITH) }
        );
        FIND04:
        while (condition(ldaIaal999.getVw_iaa_Cntrct().readNextRow("FIND04")))
        {
            ldaIaal999.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            pnd_In_Option.setValue(ldaIaal999.getIaa_Cntrct_Cntrct_Optn_Cde());                                                                                           //Natural: ASSIGN #IN-OPTION := CNTRCT-OPTN-CDE
            DbsUtil.callnat(Iaan011.class , getCurrentProcessState(), pnd_In_Option, pnd_Out_Desc);                                                                       //Natural: CALLNAT 'IAAN011' #IN-OPTION #OUT-DESC
            if (condition(Global.isEscape())) return;
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  OS - 121213 END
    }
    private void sub_Get_Age() throws Exception                                                                                                                           //Natural: GET-AGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Num_Years.compute(new ComputeParameters(false, pnd_Num_Years), pnd_Start_Date_Pnd_Yyyy.subtract(pnd_End_Date_Pnd_E_Yyyy));                                    //Natural: ASSIGN #NUM-YEARS := #YYYY - #E-YYYY
        if (condition(pnd_Start_Date_Pnd_Mm.less(pnd_End_Date_Pnd_E_Mm)))                                                                                                 //Natural: IF #MM < #E-MM
        {
            pnd_Num_Years.nsubtract(1);                                                                                                                                   //Natural: ASSIGN #NUM-YEARS := #NUM-YEARS - 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Start_Date_Pnd_Mm.equals(pnd_End_Date_Pnd_E_Mm)))                                                                                           //Natural: IF #MM = #E-MM
            {
                if (condition(pnd_Start_Date_Pnd_Dd.less(pnd_End_Date_Pnd_E_Dd)))                                                                                         //Natural: IF #DD < #E-DD
                {
                    pnd_Num_Years.nsubtract(1);                                                                                                                           //Natural: ASSIGN #NUM-YEARS := #NUM-YEARS - 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  RESIDENCE DESCRIPTION
    private void sub_Get_Residence_Desc() throws Exception                                                                                                                //Natural: GET-RESIDENCE-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Type_Call.setValue("R");                                                                                                                                      //Natural: ASSIGN #TYPE-CALL := 'R'
        pnd_Desc_Res.setValue(" ");                                                                                                                                       //Natural: ASSIGN #DESC-RES := ' '
        DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Code, pnd_Desc_Res);                                  //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL #CODE #DESC-RES
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(2, false, pnd_Out_Rec);                                                                                                                      //Natural: WRITE WORK FILE 2 #OUT-REC
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
