/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:28:44 PM
**        * FROM NATURAL PROGRAM : Iaap580b
************************************************************
**        * FILE NAME            : Iaap580b.java
**        * CLASS NAME           : Iaap580b
**        * INSTANCE NAME        : Iaap580b
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP580B   READ WORK FILE (CREATED IN IAAP580S)   *
*      DATE     -  02/95      AND WRITE OUT (SURV & BENE) NEW ISSUES *
*    AUTHOR     -  ARI G.     REPORT SORTED BY SSN, PAYEE CODE AND   *
*                             CONTRACT. THIS REPORT CONTAINS ONLY    *
*                             NRA INDIVIDUALS.                       *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap580b extends BLNatBase
{
    // Data Areas
    private LdaIaal580b ldaIaal580b;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal580b = new LdaIaal580b();
        registerRecord(ldaIaal580b);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal580b.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap580b() throws Exception
    {
        super("Iaap580b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "***************************",NEWLINE,"  START OF IAAP580B PROGRAM",NEWLINE,"***************************");                                 //Natural: WRITE '***************************' / '  START OF IAAP580B PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal580b.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        ldaIaal580b.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getIaa_Parm_Card_Pnd_Parm_Date_Mm(), "/",                   //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
            ldaIaal580b.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", ldaIaal580b.getIaa_Parm_Card_Pnd_Parm_Date_Yy()));
        boolean endOfDataRw = true;                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD
        boolean firstRw = true;
        RW:
        while (condition(getWorkFiles().read(1, ldaIaal580b.getPnd_Work_Record())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRw();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRw = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #PAUDIT-DOB-PH-NAME-LAST
            ldaIaal580b.getPnd_Work_Records_Read().nadd(1);                                                                                                               //Natural: ADD 1 TO #WORK-RECORDS-READ
            ldaIaal580b.getPnd_Count_Contracts().nadd(1);                                                                                                                 //Natural: ADD 1 TO #COUNT-CONTRACTS
            short decideConditionsMet168 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #PAUDIT-PAYEE-CODE;//Natural: VALUE 02
            if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Payee_Code().equals(2))))
            {
                decideConditionsMet168++;
                if (condition(ldaIaal580b.getPnd_Flag_02().notEquals("N")))                                                                                               //Natural: IF #FLAG-02 NE 'N'
                {
                    ldaIaal580b.getPnd_Count_02().nadd(1);                                                                                                                //Natural: ADD 1 TO #COUNT-02
                    ldaIaal580b.getPnd_Flag_02().setValue("N");                                                                                                           //Natural: MOVE 'N' TO #FLAG-02
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 03:99
            else if (condition(((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Payee_Code().greaterOrEqual(3) && ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Payee_Code().lessOrEqual(99)))))
            {
                decideConditionsMet168++;
                if (condition(ldaIaal580b.getPnd_Flag_03_99().notEquals("N")))                                                                                            //Natural: IF #FLAG-03-99 NE 'N'
                {
                    ldaIaal580b.getPnd_Count_03_99().nadd(1);                                                                                                             //Natural: ADD 1 TO #COUNT-03-99
                    ldaIaal580b.getPnd_Flag_03_99().setValue("N");                                                                                                        //Natural: MOVE 'N' TO #FLAG-03-99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE1
            sub_Pnd_Setup_Line1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-LINE2
            sub_Pnd_Setup_Line2();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #SETUP-OTHER-LINES
            sub_Pnd_Setup_Other_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal580b.getPnd_Lines_Left().lessOrEqual(ldaIaal580b.getPnd_Lines_To_Print())))                                                              //Natural: IF #LINES-LEFT NOT > #LINES-TO-PRINT
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaIaal580b.getPnd_Line_1());                                                                              //Natural: WRITE ( 1 ) / #LINE-1
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaIaal580b.getPnd_Line_2().notEquals(" ")))                                                                                                    //Natural: IF #LINE-2 NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal580b.getPnd_Line_2());                                                                                  //Natural: WRITE ( 1 ) #LINE-2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            F5:                                                                                                                                                           //Natural: FOR #I = 1 TO #OTHER-LINES
            for (ldaIaal580b.getPnd_I().setValue(1); condition(ldaIaal580b.getPnd_I().lessOrEqual(ldaIaal580b.getPnd_Other_Lines())); ldaIaal580b.getPnd_I().nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(ldaIaal580b.getPnd_I()));                      //Natural: WRITE ( 1 ) #TABLE-GROUP ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F5"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F5"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaIaal580b.getPnd_Lines_Left().nsubtract(ldaIaal580b.getPnd_Lines_To_Print());                                                                               //Natural: SUBTRACT #LINES-TO-PRINT FROM #LINES-LEFT
                                                                                                                                                                          //Natural: PERFORM #RESET-FIELDS
            sub_Pnd_Reset_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRw(endOfDataRw);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #PRINT-FINAL-TOTALS
        sub_Pnd_Print_Final_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "WORK FILE READS =====> ",ldaIaal580b.getPnd_Work_Records_Read());                                                                          //Natural: WRITE 'WORK FILE READS =====> ' #WORK-RECORDS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "***************************",NEWLINE,"  END OF IAAP580B PROGRAM",NEWLINE,"***************************");                                   //Natural: WRITE '***************************' / '  END OF IAAP580B PROGRAM'/ '***************************'
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE1
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-LINE2
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #SETUP-OTHER-LINES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CORR-ADDRESS-COUNT-1
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CORR-ADDRESS-COUNT-2
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PAYM-ADDRESS-COUNT
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-FINAL-TOTALS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #RESET-FIELDS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-FINANCE-INFO
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Pnd_Setup_Line1() throws Exception                                                                                                                   //Natural: #SETUP-LINE1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Dob_Dte().notEquals(" ")))                                                                                //Natural: IF #PAUDIT-DOB-DTE NE ' '
        {
            ldaIaal580b.getPnd_Date_Ccyymmdd().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Dob_Dte());                                                             //Natural: MOVE #PAUDIT-DOB-DTE TO #DATE-CCYYMMDD
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Date_Ccyymmdd_Pnd_Date_Mm(),             //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-YY INTO #LN1-DATE LEAVING NO
                "/", ldaIaal580b.getPnd_Date_Ccyymmdd_Pnd_Date_Dd(), "/", ldaIaal580b.getPnd_Date_Ccyymmdd_Pnd_Date_Yy()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Date().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #LN1-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* * WRITE #PAUDIT-PYEE-TAX-ID-NBR
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Pyee_Tax_Id_Nbr().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-PYEE-TAX-ID-NBR NE ' '
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Work_Record_Pnd_Ssn_1_3(),                //Natural: COMPRESS #SSN-1-3 '-' #SSN-4-5 '-' #SSN-6-9 INTO #LN1-SSN LEAVING NO
                "-", ldaIaal580b.getPnd_Work_Record_Pnd_Ssn_4_5(), "-", ldaIaal580b.getPnd_Work_Record_Pnd_Ssn_6_9()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Ssn().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO #LN1-SSN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr().notEquals(" ")))                                                                               //Natural: IF #PAUDIT-PPCN-NBR NE ' '
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_1_7(),  //Natural: COMPRESS #PAUDIT-PPCN-NBR-1-7 '-' #PAUDIT-PPCN-NBR-8 INTO #LN1-CONTRACT LEAVING NO
                "-", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ppcn_Nbr_8()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Contract().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO #LN1-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet281 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-SEX-CODE;//Natural: VALUE '1'
        if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Sex_Code().equals("1"))))
        {
            decideConditionsMet281++;
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue("M");                                                                                                //Natural: MOVE 'M' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Sex_Code().equals("2"))))
        {
            decideConditionsMet281++;
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue("F");                                                                                                //Natural: MOVE 'F' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Gender_Code().setValue(" ");                                                                                                //Natural: MOVE ' ' TO #LN1-GENDER-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Payee_Code().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Payee_Code());                                                  //Natural: MOVE #PAUDIT-PAYEE-CODE TO #LN1-PAYEE-CODE
        ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Operator_Id().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Oper_Id());                                                    //Natural: MOVE #PAUDIT-OPER-ID TO #LN1-OPERATOR-ID
        ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_First_Name().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ph_Name_First());                                          //Natural: MOVE #PAUDIT-PH-NAME-FIRST TO #LN1-CORR-FIRST-NAME
        ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ph_Name_Middle());                                        //Natural: MOVE #PAUDIT-PH-NAME-MIDDLE TO #LN1-CORR-MIDDLE-NAME
        ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Ph_Name_Last());                                            //Natural: MOVE #PAUDIT-PH-NAME-LAST TO #LN1-CORR-LAST-NAME
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_First_Name()), new ExamineSearch(","), new ExamineReplace(" "));                         //Natural: EXAMINE #LN1-CORR-FIRST-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_First_Name()), new ExamineSearch("."), new ExamineReplace(" "));                         //Natural: EXAMINE #LN1-CORR-FIRST-NAME FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN1-CORR-MIDDLE-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Middle_Name()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN1-CORR-MIDDLE-NAME FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name()), new ExamineSearch(","), new ExamineReplace(" "));                          //Natural: EXAMINE #LN1-CORR-LAST-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Corr_Last_Name()), new ExamineSearch("."), new ExamineReplace(" "));                          //Natural: EXAMINE #LN1-CORR-LAST-NAME FOR '.' REPLACE WITH ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1)))                                                                        //Natural: IF #PAUDIT-NAME-ADDRESS-GRP-NUM = 1
        {
            ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Payment_Name().setValue("SAME AS CORRESPONDENCE");                                                                          //Natural: MOVE 'SAME AS CORRESPONDENCE' TO #LN1-PAYMENT-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2)))                                                                    //Natural: IF #PAUDIT-NAME-ADDRESS-GRP-NUM = 2
            {
                ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Payment_Name().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name().getValue(2));                                  //Natural: MOVE #PAUDIT-NAME ( 2 ) TO #LN1-PAYMENT-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Payment_Name()), new ExamineSearch(","), new ExamineReplace(" "));                            //Natural: EXAMINE #LN1-PAYMENT-NAME FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_1_Pnd_Ln1_Payment_Name()), new ExamineSearch("."), new ExamineReplace(" "));                            //Natural: EXAMINE #LN1-PAYMENT-NAME FOR '.' REPLACE WITH ' '
        ldaIaal580b.getPnd_Lines_To_Print().nadd(2);                                                                                                                      //Natural: ADD 2 TO #LINES-TO-PRINT
    }
    private void sub_Pnd_Setup_Line2() throws Exception                                                                                                                   //Natural: #SETUP-LINE2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Operator_Unit().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Oper_User_Grp());                                            //Natural: MOVE #PAUDIT-OPER-USER-GRP TO #LN2-OPERATOR-UNIT
        short decideConditionsMet322 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-NAME-ADDRESS-GRP-NUM;//Natural: VALUE 1
        if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1))))
        {
            decideConditionsMet322++;
            ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(1));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 1 ) TO #LN2-CORR-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580b.getPnd_Lines_To_Print().nadd(1);                                                                                                                  //Natural: ADD 1 TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2))))
        {
            decideConditionsMet322++;
            ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Paym_Address().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(2));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 2 ) TO #LN2-PAYM-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Paym_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-PAYM-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Paym_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-PAYM-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_1().getValue(1));                              //Natural: MOVE #PAUDIT-ADDRSS-LNE-1 ( 1 ) TO #LN2-CORR-ADDRESS
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch(","), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR ',' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Line_2_Pnd_Ln2_Corr_Address()), new ExamineSearch("."), new ExamineReplace(" "));                        //Natural: EXAMINE #LN2-CORR-ADDRESS FOR '.' REPLACE WITH ' '
            ldaIaal580b.getPnd_Lines_To_Print().nadd(1);                                                                                                                  //Natural: ADD 1 TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Setup_Other_Lines() throws Exception                                                                                                             //Natural: #SETUP-OTHER-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet345 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PAUDIT-NAME-ADDRESS-GRP-NUM;//Natural: VALUE 1
        if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(1))))
        {
            decideConditionsMet345++;
                                                                                                                                                                          //Natural: PERFORM #CORR-ADDRESS-COUNT-1
            sub_Pnd_Corr_Address_Count_1();
            if (condition(Global.isEscape())) {return;}
            ldaIaal580b.getPnd_Other_Lines().setValue(ldaIaal580b.getPnd_Corr_Count());                                                                                   //Natural: MOVE #CORR-COUNT TO #OTHER-LINES
            ldaIaal580b.getPnd_Lines_To_Print().nadd(ldaIaal580b.getPnd_Corr_Count());                                                                                    //Natural: ADD #CORR-COUNT TO #LINES-TO-PRINT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Name_Address_Grp_Num().equals(2))))
        {
            decideConditionsMet345++;
                                                                                                                                                                          //Natural: PERFORM #CORR-ADDRESS-COUNT-2
            sub_Pnd_Corr_Address_Count_2();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #PAYM-ADDRESS-COUNT
            sub_Pnd_Paym_Address_Count();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaIaal580b.getPnd_Corr_Count().greater(ldaIaal580b.getPnd_Paym_Count())))                                                                      //Natural: IF #CORR-COUNT > #PAYM-COUNT
            {
                ldaIaal580b.getPnd_Other_Lines().setValue(ldaIaal580b.getPnd_Corr_Count());                                                                               //Natural: MOVE #CORR-COUNT TO #OTHER-LINES
                ldaIaal580b.getPnd_Lines_To_Print().nadd(ldaIaal580b.getPnd_Corr_Count());                                                                                //Natural: ADD #CORR-COUNT TO #LINES-TO-PRINT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal580b.getPnd_Other_Lines().setValue(ldaIaal580b.getPnd_Paym_Count());                                                                               //Natural: MOVE #PAYM-COUNT TO #OTHER-LINES
                ldaIaal580b.getPnd_Lines_To_Print().nadd(ldaIaal580b.getPnd_Paym_Count());                                                                                //Natural: ADD #PAYM-COUNT TO #LINES-TO-PRINT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Corr_Address_Count_1() throws Exception                                                                                                          //Natural: #CORR-ADDRESS-COUNT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet367 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1).notEquals(" ")))
        {
            decideConditionsMet367++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1).notEquals(" ")))
        {
            decideConditionsMet367++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1).notEquals(" ")))
        {
            decideConditionsMet367++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet367 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR '.' REPLACE WITH ' '
        ldaIaal580b.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1), ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 1 ) #PAUDIT-ADDRSS-STATE ( 1 ) INTO #CITY-STATE
        ldaIaal580b.getPnd_Zip_Breakdown().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #CORR-COUNT
        ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(DbsUtil.compress(ldaIaal580b.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-CORR-ADDRESS ( #CORR-COUNT )
            ldaIaal580b.getPnd_Total_Zip()));
        F1:                                                                                                                                                               //Natural: FOR #I = 1 TO #CORR-COUNT
        for (ldaIaal580b.getPnd_I().setValue(1); condition(ldaIaal580b.getPnd_I().lessOrEqual(ldaIaal580b.getPnd_Corr_Count())); ldaIaal580b.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Corr_Address_Count_2() throws Exception                                                                                                          //Natural: #CORR-ADDRESS-COUNT-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet406 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1).notEquals(" ")))
        {
            decideConditionsMet406++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1).notEquals(" ")))
        {
            decideConditionsMet406++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 1 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1).notEquals(" ")))
        {
            decideConditionsMet406++;
            ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CORR-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(1)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 1 ) TO #TAB-CORR-ADDRESS ( #CORR-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet406 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 1 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 1 ) FOR '.' REPLACE WITH ' '
        ldaIaal580b.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(1), ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(1))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 1 ) #PAUDIT-ADDRSS-STATE ( 1 ) INTO #CITY-STATE
        ldaIaal580b.getPnd_Zip_Breakdown().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(1));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 1 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580b.getPnd_Corr_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #CORR-COUNT
        ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_Corr_Count()).setValue(DbsUtil.compress(ldaIaal580b.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-CORR-ADDRESS ( #CORR-COUNT )
            ldaIaal580b.getPnd_Total_Zip()));
        F2:                                                                                                                                                               //Natural: FOR #I = 1 TO #CORR-COUNT
        for (ldaIaal580b.getPnd_I().setValue(1); condition(ldaIaal580b.getPnd_I().lessOrEqual(ldaIaal580b.getPnd_Corr_Count())); ldaIaal580b.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Corr_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-CORR-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Paym_Address_Count() throws Exception                                                                                                            //Natural: #PAYM-ADDRESS-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet446 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PAUDIT-ADDRSS-LNE-2 ( 2 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(2).notEquals(" ")))
        {
            decideConditionsMet446++;
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_2().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-2 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-3 ( 2 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(2).notEquals(" ")))
        {
            decideConditionsMet446++;
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_3().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-3 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-ADDRSS-LNE-4 ( 2 ) NOT = ' '
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(2).notEquals(" ")))
        {
            decideConditionsMet446++;
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Lne_4().getValue(2)); //Natural: MOVE #PAUDIT-ADDRSS-LNE-4 ( 2 ) TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet446 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2)), new ExamineSearch(","), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 2 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2)), new ExamineSearch("."), new ExamineReplace(" "));         //Natural: EXAMINE #PAUDIT-ADDRSS-CITY ( 2 ) FOR '.' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2)), new ExamineSearch(","), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 2 ) FOR ',' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2)), new ExamineSearch("."), new ExamineReplace(" "));        //Natural: EXAMINE #PAUDIT-ADDRSS-STATE ( 2 ) FOR '.' REPLACE WITH ' '
        ldaIaal580b.getPnd_City_State().setValue(DbsUtil.compress(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_City().getValue(2), ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_State().getValue(2))); //Natural: COMPRESS #PAUDIT-ADDRSS-CITY ( 2 ) #PAUDIT-ADDRSS-STATE ( 2 ) INTO #CITY-STATE
        ldaIaal580b.getPnd_Zip_Breakdown().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(2));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 2 ) TO #ZIP-BREAKDOWN
        if (condition(ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9().equals(" ")))                                                                                        //Natural: IF #ZIP-6-9 = ' '
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Addrss_Zip().getValue(2));                                                  //Natural: MOVE #PAUDIT-ADDRSS-ZIP ( 2 ) TO #TOTAL-ZIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaal580b.getPnd_Total_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_1_5(), "-",                  //Natural: COMPRESS #ZIP-1-5 '-' #ZIP-6-9 INTO #TOTAL-ZIP LEAVING NO
                ldaIaal580b.getPnd_Zip_Breakdown_Pnd_Zip_6_9()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                          //Natural: ADD 1 TO #PAYM-COUNT
        ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580b.getPnd_City_State(),  //Natural: COMPRESS #CITY-STATE #TOTAL-ZIP INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580b.getPnd_Total_Zip()));
        F3:                                                                                                                                                               //Natural: FOR #I = 1 TO #PAYM-COUNT
        for (ldaIaal580b.getPnd_I().setValue(1); condition(ldaIaal580b.getPnd_I().lessOrEqual(ldaIaal580b.getPnd_Paym_Count())); ldaIaal580b.getPnd_I().nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch(","),      //Natural: EXAMINE #TAB-PAYM-ADDRESS ( #I ) FOR ',' REPLACE WITH ' '
                new ExamineReplace(" "));
            DbsUtil.examine(new ExamineSource(ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_I())), new ExamineSearch("."),      //Natural: EXAMINE #TAB-PAYM-ADDRESS ( #I ) FOR '.' REPLACE WITH ' '
                new ExamineReplace(" "));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Type_Ind().equals("P")))                                                                                  //Natural: IF #PAUDIT-TYPE-IND = 'P'
        {
            ldaIaal580b.getPnd_Paym_Count().nadd(2);                                                                                                                      //Natural: ADD 2 TO #PAYM-COUNT
                                                                                                                                                                          //Natural: PERFORM #WRITE-FINANCE-INFO
            sub_Pnd_Write_Finance_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Print_Final_Totals() throws Exception                                                                                                            //Natural: #PRINT-FINAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(getReports().getAstLinesLeft(1).less(9)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 9 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"NRA REPORT TOTALS:",NEWLINE,"Total number of survivors    ",ldaIaal580b.getPnd_Count_02(),            //Natural: WRITE ( 1 ) // 'NRA REPORT TOTALS:' / 'Total number of survivors    ' #COUNT-02 / 'Total number of Beneficiaries' #COUNT-03-99 / 'Total number of contracts    ' #COUNT-CONTRACTS
            NEWLINE,"Total number of Beneficiaries",ldaIaal580b.getPnd_Count_03_99(),NEWLINE,"Total number of contracts    ",ldaIaal580b.getPnd_Count_Contracts());
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Reset_Fields() throws Exception                                                                                                                  //Natural: #RESET-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal580b.getPnd_Line_1().reset();                                                                                                                              //Natural: RESET #LINE-1 #LINE-2 #CORR-COUNT #PAYM-COUNT #LINES-TO-PRINT #OTHER-LINES #TABLE-GROUP ( 1:9 )
        ldaIaal580b.getPnd_Line_2().reset();
        ldaIaal580b.getPnd_Corr_Count().reset();
        ldaIaal580b.getPnd_Paym_Count().reset();
        ldaIaal580b.getPnd_Lines_To_Print().reset();
        ldaIaal580b.getPnd_Other_Lines().reset();
        ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Table_Group().getValue(1,":",9).reset();
    }
    private void sub_Pnd_Write_Finance_Info() throws Exception                                                                                                            //Natural: #WRITE-FINANCE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        short decideConditionsMet500 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PAUDIT-TYPE-REQ-IND = 1 AND #NEW-ACCT-ID = 'Y'
        if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(1) && ldaIaal580b.getPnd_Work_Record_Pnd_New_Acct_Id().equals("Y")))
        {
            decideConditionsMet500++;
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue("ACCT: NEW ACCOUNT");                          //Natural: MOVE 'ACCT: NEW ACCOUNT' TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-CHK-SAV-IND NOT = ' '
            {
                ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                  //Natural: ADD 1 TO #PAYM-COUNT
                ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress("TYPE",                   //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                    ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 1 AND #NEW-ACCT-ID = 'N'
        else if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(1) && ldaIaal580b.getPnd_Work_Record_Pnd_New_Acct_Id().equals("N")))
        {
            decideConditionsMet500++;
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress("ACCT:", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr())); //Natural: COMPRESS 'ACCT:' #PAUDIT-EFT-ACCT-NBR INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress("TYPE", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind())); //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 2 AND #NEW-ACCT-ID = 'Y'
        else if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(2) && ldaIaal580b.getPnd_Work_Record_Pnd_New_Acct_Id().equals("Y")))
        {
            decideConditionsMet500++;
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue("ACCT: NEW ACCOUNT");                          //Natural: MOVE 'ACCT: NEW ACCOUNT' TO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Hold_Variable_1().setValue(DbsUtil.compress("ABA:", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Eft_Transit_Id()));                          //Natural: COMPRESS 'ABA:' #PAUDIT-EFT-TRANSIT-ID INTO #HOLD-VARIABLE-1
            if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind().notEquals(" ")))                                                                        //Natural: IF #PAUDIT-CHK-SAV-IND NOT = ' '
            {
                ldaIaal580b.getPnd_Hold_Variable_2().setValue(DbsUtil.compress("TYPE", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));                         //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #HOLD-VARIABLE-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal580b.getPnd_Hold_Variable_2().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #HOLD-VARIABLE-2
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580b.getPnd_Hold_Variable_1(),  //Natural: COMPRESS #HOLD-VARIABLE-1 #HOLD-VARIABLE-2 INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                ldaIaal580b.getPnd_Hold_Variable_2()));
        }                                                                                                                                                                 //Natural: WHEN #PAUDIT-TYPE-REQ-IND = 2 AND #NEW-ACCT-ID = 'N'
        else if (condition(ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Type_Req_Ind().equals(2) && ldaIaal580b.getPnd_Work_Record_Pnd_New_Acct_Id().equals("N")))
        {
            decideConditionsMet500++;
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress("ACCT:", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Eft_Acct_Nbr())); //Natural: COMPRESS 'ACCT:' #PAUDIT-EFT-ACCT-NBR INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
            ldaIaal580b.getPnd_Paym_Count().nadd(1);                                                                                                                      //Natural: ADD 1 TO #PAYM-COUNT
            ldaIaal580b.getPnd_Hold_Variable_1().setValue(DbsUtil.compress("ABA:", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Eft_Transit_Id()));                          //Natural: COMPRESS 'ABA:' #PAUDIT-EFT-TRANSIT-ID INTO #HOLD-VARIABLE-1
            ldaIaal580b.getPnd_Hold_Variable_2().setValue(DbsUtil.compress("TYPE", ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Chk_Sav_Ind()));                             //Natural: COMPRESS 'TYPE' #PAUDIT-CHK-SAV-IND INTO #HOLD-VARIABLE-2
            ldaIaal580b.getPnd_Table_Other_Lines_Pnd_Tab_Paym_Address().getValue(ldaIaal580b.getPnd_Paym_Count()).setValue(DbsUtil.compress(ldaIaal580b.getPnd_Hold_Variable_1(),  //Natural: COMPRESS #HOLD-VARIABLE-1 #HOLD-VARIABLE-2 INTO #TAB-PAYM-ADDRESS ( #PAYM-COUNT )
                ldaIaal580b.getPnd_Hold_Variable_2()));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(DbsUtil.maskMatches(ldaIaal580b.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal580b.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    ldaIaal580b.getPnd_Page_Ctr().nadd(1);                                                                                                                //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(44),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new        //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 44T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR
                        TabSetting(119),"PAGE ",ldaIaal580b.getPnd_Page_Ctr());
                    getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(43),"SURVIVOR AND BENEFICIARY NEW ISSUES NRA REPORT                                  "); //Natural: WRITE ( 1 ) '   DATE ' *DATU 43T 'SURVIVOR AND BENEFICIARY NEW ISSUES NRA REPORT                                  '
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(57)," FOR",new TabSetting(62),ldaIaal580b.getPnd_W_Parm_Date());                            //Natural: WRITE ( 1 ) 57T ' FOR' 62T #W-PARM-DATE
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Operator",new TabSetting(11),"       NAME/ADDRESS OF BENEFICIARY      ",new             //Natural: WRITE ( 1 ) 001T 'Operator' 011T '       NAME/ADDRESS OF BENEFICIARY      ' 055T 'DATE OF' 067T 'SOCIAL' 079T 'S' 083T 'CONTRACT' 094T 'PY' 098T '          PAYMENT ADDRESS          '
                        TabSetting(55),"DATE OF",new TabSetting(67),"SOCIAL",new TabSetting(79),"S",new TabSetting(83),"CONTRACT",new TabSetting(94),"PY",new 
                        TabSetting(98),"          PAYMENT ADDRESS          ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"ID/Unit ",new TabSetting(11),"     LAST           FIRST        MIDDLE   ",new           //Natural: WRITE ( 1 ) 001T 'ID/Unit ' 011T '     LAST           FIRST        MIDDLE   ' 055T ' BIRTH ' 065T 'SECURITY NO.' 079T 'X' 083T ' NUMBER '
                        TabSetting(55)," BIRTH ",new TabSetting(65),"SECURITY NO.",new TabSetting(79),"X",new TabSetting(83)," NUMBER ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"--------",new TabSetting(11),"----------------  ----------  ------------",new           //Natural: WRITE ( 1 ) 001T '--------' 011T '----------------  ----------  ------------' 055T '--------' 065T '------------' 079T '-' 083T '---------' 094T '--' 098T '-----------------------------------'
                        TabSetting(55),"--------",new TabSetting(65),"------------",new TabSetting(79),"-",new TabSetting(83),"---------",new TabSetting(94),"--",new 
                        TabSetting(98),"-----------------------------------");
                    ldaIaal580b.getPnd_Lines_Left().setValue(49);                                                                                                         //Natural: ASSIGN #LINES-LEFT := 49
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRw() throws Exception {atBreakEventRw(false);}
    private void atBreakEventRw(boolean endOfData) throws Exception
    {
        boolean ldaIaal580b_getPnd_Work_Record_Pnd_Paudit_Dob_Ph_Name_LastIsBreak = ldaIaal580b.getPnd_Work_Record_Pnd_Paudit_Dob_Ph_Name_Last().isBreak(endOfData);
        if (condition(ldaIaal580b_getPnd_Work_Record_Pnd_Paudit_Dob_Ph_Name_LastIsBreak))
        {
            ldaIaal580b.getPnd_Flag_02().setValue("Y");                                                                                                                   //Natural: MOVE 'Y' TO #FLAG-02
            ldaIaal580b.getPnd_Flag_03_99().setValue("Y");                                                                                                                //Natural: MOVE 'Y' TO #FLAG-03-99
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
