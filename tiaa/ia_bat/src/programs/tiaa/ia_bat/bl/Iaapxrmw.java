/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:44 PM
**        * FROM NATURAL PROGRAM : Iaapxrmw
************************************************************
**        * FILE NAME            : Iaapxrmw.java
**        * CLASS NAME           : Iaapxrmw
**        * INSTANCE NAME        : Iaapxrmw
************************************************************
************************************************************************
* PROGRAM  : IAAPXRMW
* SYSTEM   : IA
* TITLE    : CREATE EXTRACT OF IA POST-SETTLEMENT TRANSFERS FOR RMW.
*
* CREATED  :
*
* HISTORY:
*
* 08/2016  : PIN EXPANSION. INCREASE PIN FROM N7 TO N12. SCAN ON 08/2016
* JUN 2017 J BREMER       PIN EXPANSION - STOW  ONLY
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapxrmw extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Plan_Nmbr;

    private DbsGroup iaa_Cntrct_Lgcy_Plan_Sub_Plan;
    private DbsField iaa_Cntrct_Lgcy_Plan_Nbr;
    private DbsField iaa_Cntrct_Lgcy_Sub_Plan_Nbr;
    private DbsField iaa_Cntrct_Lgcy_Orig_Sub_Plan;

    private DataAccessProgramView vw_cntrct_To;
    private DbsField cntrct_To_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_To_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField cntrct_To_Plan_Nmbr;

    private DbsGroup cntrct_To_Lgcy_Plan_Sub_Plan;
    private DbsField cntrct_To_Lgcy_Plan_Nbr;
    private DbsField cntrct_To_Lgcy_Sub_Plan_Nbr;
    private DbsField cntrct_To_Lgcy_Orig_Sub_Plan;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Lst_Trans_Dte;
    private DbsField cpr_Prtcpnt_Ctznshp_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Sw;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Prtcpnt_Tax_Id_Typ;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Wthdrwl_Dte;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;

    private DataAccessProgramView vw_iaa_Xfr_Audit;
    private DbsField iaa_Xfr_Audit_Rcrd_Type_Cde;
    private DbsField iaa_Xfr_Audit_Rqst_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id;
    private DbsField iaa_Xfr_Audit_Iaxfr_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Entry_Tme;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Wpid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_In_Progress_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Retry_Cnt;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data;

    private DbsGroup iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Fund_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Acct_Cd;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Unit_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Typ;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Qty;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Asset_Amt;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar;
    private DbsField iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind;
    private DbsField iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Cycle_Dte;
    private DbsField iaa_Xfr_Audit_Iaxfr_Acctng_Dte;
    private DbsField iaa_Xfr_Audit_Lst_Chnge_Dte;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Fund_1;
    private DbsField pnd_Fund_2;
    private DbsField pnd_Rc;
    private DbsField pnd_W_Assets;
    private DbsField pnd_W_Amts;
    private DbsField pnd_W_Pmt;
    private DbsField pnd_A_Pmt;
    private DbsField pnd_Entry_Dte;
    private DbsField pnd_Start;

    private DbsGroup pnd_Start__R_Field_1;
    private DbsField pnd_Start_Pnd_S_Yyyy;
    private DbsField pnd_Start_Pnd_S_Mm;
    private DbsField pnd_Start_Pnd_S_Dd;
    private DbsField pnd_End;

    private DbsGroup pnd_End__R_Field_2;
    private DbsField pnd_End_Pnd_E_Yyyy;
    private DbsField pnd_End_Pnd_E_Mm;
    private DbsField pnd_End_Pnd_E_Dd;
    private DbsField pnd_W_Iss_Dte;

    private DbsGroup pnd_W_Iss_Dte__R_Field_3;
    private DbsField pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy;
    private DbsField pnd_W_Iss_Dte_Pnd_W_Iss_Mm;
    private DbsField pnd_W_Final_Dte;

    private DbsGroup pnd_W_Final_Dte__R_Field_4;
    private DbsField pnd_W_Final_Dte_Pnd_W_Final_Yyyy;
    private DbsField pnd_W_Final_Dte_Pnd_W_Final_Mm;
    private DbsField pnd_W_Issue_Yr;
    private DbsField pnd_W_Issue_Mo;
    private DbsField pnd_Cntrct_Key;
    private DbsField pnd_Fctr;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Output_Pnd_F1;
    private DbsField pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Output_Pnd_F2;
    private DbsField pnd_Output_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Output_Pnd_F3;
    private DbsField pnd_Output_Pnd_Orig_Da_Nbr;
    private DbsField pnd_Output_Pnd_F4;
    private DbsField pnd_Output_Pnd_Fund_Ticker;
    private DbsField pnd_Output_Pnd_F5;
    private DbsField pnd_Output_Pnd_Cmpny_Fund_Cde;

    private DbsGroup pnd_Output__R_Field_6;
    private DbsField pnd_Output_Pnd_Cmpny_Cde;
    private DbsField pnd_Output_Pnd_Fund_Cde;
    private DbsField pnd_Output_Pnd_F6;
    private DbsField pnd_Output_Pnd_Units;
    private DbsField pnd_Output_Pnd_F7;
    private DbsField pnd_Output_Pnd_Guar_Amt;
    private DbsField pnd_Output_Pnd_F8;
    private DbsField pnd_Output_Pnd_Div_Amt;
    private DbsField pnd_Output_Pnd_F9;
    private DbsField pnd_Output_Pnd_Assets_Amt;
    private DbsField pnd_Output_Pnd_F0;
    private DbsField pnd_Output_Pnd_Annlzd_Pmt;
    private DbsField pnd_Output_Pnd_Fa;
    private DbsField pnd_Output_Pnd_Mode;
    private DbsField pnd_Output_Pnd_Fb;
    private DbsField pnd_Output_Pnd_1st_Dob;
    private DbsField pnd_Output_Pnd_Fc;
    private DbsField pnd_Output_Pnd_2nd_Dob;
    private DbsField pnd_Output_Pnd_Fd;
    private DbsField pnd_Output_Pnd_Optn_Cde;
    private DbsField pnd_Output_Pnd_Fe;
    private DbsField pnd_Output_Pnd_Sex_Cde;
    private DbsField pnd_Output_Pnd_Ff;
    private DbsField pnd_Output_Pnd_Iss_Dte;

    private DbsGroup pnd_Output__R_Field_7;
    private DbsField pnd_Output_Pnd_Iss_Yyyymm;
    private DbsField pnd_Output_Pnd_Iss_Dd;
    private DbsField pnd_Output_Pnd_Fg;
    private DbsField pnd_Output_Pnd_Effctv_Dte;
    private DbsField pnd_Output_Pnd_Fh;
    private DbsField pnd_Output_Pnd_Guar_Prd;
    private DbsField pnd_Output_Pnd_Fi;
    private DbsField pnd_Output_Pnd_Plan_Nmbr;
    private DbsField pnd_Output_Pnd_Fj;
    private DbsField pnd_Output_Pnd_B_Units;
    private DbsField pnd_Output_Pnd_Fk;
    private DbsField pnd_Output_Pnd_B_Guar_Amt;
    private DbsField pnd_Output_Pnd_Fl;
    private DbsField pnd_Output_Pnd_B_Div_Amt;
    private DbsField pnd_Output_Pnd_Fm;
    private DbsField pnd_Output_Pnd_B_Annlzd_Pmt;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField pls_Tckr_Symbl;
    private DbsField pls_Fund_Num_Cde;
    private DbsField pls_Fund_Alpha_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT"));
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");

        iaa_Cntrct_Lgcy_Plan_Sub_Plan = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Lgcy_Plan_Sub_Plan", "LGCY-PLAN-SUB-PLAN", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Plan_Nbr = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Plan_Nbr", "LGCY-PLAN-NBR", FieldType.STRING, 6, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Sub_Plan_Nbr = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Sub_Plan_Nbr", "LGCY-SUB-PLAN-NBR", FieldType.STRING, 
            6, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SUB_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        iaa_Cntrct_Lgcy_Orig_Sub_Plan = iaa_Cntrct_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("iaa_Cntrct_Lgcy_Orig_Sub_Plan", "LGCY-ORIG-SUB-PLAN", FieldType.STRING, 
            6, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_SUB_PLAN", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        registerRecord(vw_iaa_Cntrct);

        vw_cntrct_To = new DataAccessProgramView(new NameInfo("vw_cntrct_To", "CNTRCT-TO"), "IAA_CNTRCT", "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT"));
        cntrct_To_Cntrct_Ppcn_Nbr = vw_cntrct_To.getRecord().newFieldInGroup("cntrct_To_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_To_Cntrct_Orig_Da_Cntrct_Nbr = vw_cntrct_To.getRecord().newFieldInGroup("cntrct_To_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        cntrct_To_Plan_Nmbr = vw_cntrct_To.getRecord().newFieldInGroup("cntrct_To_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");

        cntrct_To_Lgcy_Plan_Sub_Plan = vw_cntrct_To.getRecord().newGroupInGroup("cntrct_To_Lgcy_Plan_Sub_Plan", "LGCY-PLAN-SUB-PLAN", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        cntrct_To_Lgcy_Plan_Nbr = cntrct_To_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("cntrct_To_Lgcy_Plan_Nbr", "LGCY-PLAN-NBR", FieldType.STRING, 6, new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        cntrct_To_Lgcy_Sub_Plan_Nbr = cntrct_To_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("cntrct_To_Lgcy_Sub_Plan_Nbr", "LGCY-SUB-PLAN-NBR", FieldType.STRING, 
            6, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_SUB_PLAN_NBR", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        cntrct_To_Lgcy_Orig_Sub_Plan = cntrct_To_Lgcy_Plan_Sub_Plan.newFieldArrayInGroup("cntrct_To_Lgcy_Orig_Sub_Plan", "LGCY-ORIG-SUB-PLAN", FieldType.STRING, 
            6, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "LGCY_ORIG_SUB_PLAN", "IA_CONTRACT_PART_LGCY_PLAN_SUB_PLAN");
        registerRecord(vw_cntrct_To);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Lst_Trans_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        cpr_Prtcpnt_Ctznshp_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_CTZNSHP_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Prtcpnt_Rsdncy_Sw = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_SW");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Prtcpnt_Tax_Id_Typ = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_TYP");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Wthdrwl_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_WTHDRWL_DTE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        registerRecord(vw_cpr);

        vw_iaa_Xfr_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Xfr_Audit", "IAA-XFR-AUDIT"), "IAA_XFR_AUDIT", "IA_TRANSFERS", DdmPeriodicGroups.getInstance().getGroups("IAA_XFR_AUDIT"));
        iaa_Xfr_Audit_Rcrd_Type_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYPE_CDE");
        iaa_Xfr_Audit_Rqst_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, 
            "RQST_ID");
        iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unique_Id", "IAXFR-CALC-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIQUE_ID");
        iaa_Xfr_Audit_Iaxfr_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Effctve_Dte", "IAXFR-EFFCTVE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Dte", "IAXFR-RQST-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_DTE");
        iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Rqst_Rcvd_Tme", "IAXFR-RQST-RCVD-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_RQST_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Effctve_Dte", "IAXFR-INVRSE-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "IAXFR_INVRSE_EFFCTVE_DTE");
        iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Invrse_Rcvd_Tme", "IAXFR-INVRSE-RCVD-TME", 
            FieldType.NUMERIC, 14, RepeatingFieldStrategy.None, "IAXFR_INVRSE_RCVD_TME");
        iaa_Xfr_Audit_Iaxfr_Entry_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Dte", "IAXFR-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ENTRY_DTE");
        iaa_Xfr_Audit_Iaxfr_Entry_Tme = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Entry_Tme", "IAXFR-ENTRY-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "IAXFR_ENTRY_TME");
        iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Sttmnt_Indctr", "IAXFR-CALC-STTMNT-INDCTR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_STTMNT_INDCTR");
        iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Status_Cde", "IAXFR-CALC-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_STATUS_CDE");
        iaa_Xfr_Audit_Iaxfr_Cwf_Wpid = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Wpid", "IAXFR-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "IAXFR_CWF_WPID");
        iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cwf_Log_Dte_Time", "IAXFR-CWF-LOG-DTE-TIME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "IAXFR_CWF_LOG_DTE_TIME");
        iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr", "IAXFR-FROM-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "IAXFR_FROM_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_From_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_From_Payee_Cde", "IAXFR-FROM-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_FROM_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Reject_Cde", "IAXFR-CALC-REJECT-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_REJECT_CDE");
        iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Opn_Clsd_Ind", "IAXFR-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "IAXFR_OPN_CLSD_IND");
        iaa_Xfr_Audit_Iaxfr_In_Progress_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_In_Progress_Ind", "IAXFR-IN-PROGRESS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "IAXFR_IN_PROGRESS_IND");
        iaa_Xfr_Audit_Iaxfr_Retry_Cnt = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Retry_Cnt", "IAXFR-RETRY-CNT", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "IAXFR_RETRY_CNT");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data", 
            "C*IAXFR-CALC-FROM-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data", "IAXFR-CALC-FROM-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Fund_Cde", "IAXFR-FROM-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd", "IAXFR-FRM-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ", "IAXFR-FRM-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FRM_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Typ = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Typ", "IAXFR-FROM-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_TYP", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Qty = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Qty", "IAXFR-FROM-QTY", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_QTY", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar", 
            "IAXFR-FROM-CURRENT-PMT-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid", 
            "IAXFR-FROM-CURRENT-PMT-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units", 
            "IAXFR-FROM-CURRENT-PMT-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Unit_Val", 
            "IAXFR-FROM-CURRENT-PMT-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_CURRENT_PMT_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Reval_Unit_Val", 
            "IAXFR-FROM-REVAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Amt", 
            "IAXFR-FROM-RQSTD-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units", 
            "IAXFR-FROM-RQSTD-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar", 
            "IAXFR-FROM-RQSTD-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid", 
            "IAXFR-FROM-RQSTD-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_RQSTD_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt", 
            "IAXFR-FROM-ASSET-XFR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_ASSET_XFR_AMT", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Pmt_Aftr_Xfr", 
            "IAXFR-FROM-PMT-AFTR-XFR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_FROM_PMT_AFTR_XFR", 
            "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Units", 
            "IAXFR-FROM-AFTR-XFR-UNITS", FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar", 
            "IAXFR-FROM-AFTR-XFR-GUAR", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_From_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid", 
            "IAXFR-FROM-AFTR-XFR-DIVID", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IAXFR_FROM_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_FROM_ACCT_DATA");
        iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data", 
            "C*IAXFR-CALC-TO-ACCT-DATA", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");

        iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data = vw_iaa_Xfr_Audit.getRecord().newGroupInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data", "IAXFR-CALC-TO-ACCT-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Fund_Cde = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Fund_Cde", "IAXFR-TO-FUND-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_FUND_CDE", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Acct_Cd = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Acct_Cd", "IAXFR-TO-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ACCT_CD", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Unit_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Unit_Typ", "IAXFR-TO-UNIT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_UNIT_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Fund_Rec", "IAXFR-TO-NEW-FUND-REC", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_FUND_REC", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_New_Phys_Cntrct_Issue", 
            "IAXFR-TO-NEW-PHYS-CNTRCT-ISSUE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_NEW_PHYS_CNTRCT_ISSUE", 
            "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Typ = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Typ", "IAXFR-TO-TYP", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_TYP", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Qty = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Qty", "IAXFR-TO-QTY", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_QTY", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar", "IAXFR-TO-BFR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid", "IAXFR-TO-BFR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units", "IAXFR-TO-BFR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_BFR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Reval_Unit_Val", "IAXFR-TO-REVAL-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_REVAL_UNIT_VAL", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Units", "IAXFR-TO-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar", "IAXFR-TO-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid", "IAXFR-TO-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Asset_Amt = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Asset_Amt", "IAXFR-TO-ASSET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_ASSET_AMT", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Units", "IAXFR-TO-AFTR-XFR-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_UNITS", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar", "IAXFR-TO-AFTR-XFR-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_GUAR", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid = iaa_Xfr_Audit_Iaxfr_Calc_To_Acct_Data.newFieldArrayInGroup("iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid", "IAXFR-TO-AFTR-XFR-DIVID", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IAXFR_TO_AFTR_XFR_DIVID", "IA_TRANSFERS_IAXFR_CALC_TO_ACCT_DATA");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr", "IAXFR-CALC-TO-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NBR");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde", "IAXFR-CALC-TO-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PAYEE_CDE");
        iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_New_Issue_Ind", 
            "IAXFR-CALC-TO-PPCN-NEW-ISSUE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IAXFR_CALC_TO_PPCN_NEW_ISSUE_IND");
        iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Calc_Unit_Val_Dte", "IAXFR-CALC-UNIT-VAL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "IAXFR_CALC_UNIT_VAL_DTE");
        iaa_Xfr_Audit_Iaxfr_Cycle_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Cycle_Dte", "IAXFR-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_CYCLE_DTE");
        iaa_Xfr_Audit_Iaxfr_Acctng_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Iaxfr_Acctng_Dte", "IAXFR-ACCTNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "IAXFR_ACCTNG_DTE");
        iaa_Xfr_Audit_Lst_Chnge_Dte = vw_iaa_Xfr_Audit.getRecord().newFieldInGroup("iaa_Xfr_Audit_Lst_Chnge_Dte", "LST-CHNGE-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_CHNGE_DTE");
        registerRecord(vw_iaa_Xfr_Audit);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_Fund_2 = localVariables.newFieldInRecord("pnd_Fund_2", "#FUND-2", FieldType.STRING, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_W_Assets = localVariables.newFieldInRecord("pnd_W_Assets", "#W-ASSETS", FieldType.STRING, 12);
        pnd_W_Amts = localVariables.newFieldInRecord("pnd_W_Amts", "#W-AMTS", FieldType.STRING, 10);
        pnd_W_Pmt = localVariables.newFieldInRecord("pnd_W_Pmt", "#W-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_A_Pmt = localVariables.newFieldInRecord("pnd_A_Pmt", "#A-PMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Entry_Dte = localVariables.newFieldInRecord("pnd_Entry_Dte", "#ENTRY-DTE", FieldType.STRING, 8);
        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.STRING, 8);

        pnd_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Start__R_Field_1", "REDEFINE", pnd_Start);
        pnd_Start_Pnd_S_Yyyy = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_S_Yyyy", "#S-YYYY", FieldType.NUMERIC, 4);
        pnd_Start_Pnd_S_Mm = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_S_Mm", "#S-MM", FieldType.NUMERIC, 2);
        pnd_Start_Pnd_S_Dd = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_S_Dd", "#S-DD", FieldType.NUMERIC, 2);
        pnd_End = localVariables.newFieldInRecord("pnd_End", "#END", FieldType.STRING, 8);

        pnd_End__R_Field_2 = localVariables.newGroupInRecord("pnd_End__R_Field_2", "REDEFINE", pnd_End);
        pnd_End_Pnd_E_Yyyy = pnd_End__R_Field_2.newFieldInGroup("pnd_End_Pnd_E_Yyyy", "#E-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Pnd_E_Mm = pnd_End__R_Field_2.newFieldInGroup("pnd_End_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        pnd_End_Pnd_E_Dd = pnd_End__R_Field_2.newFieldInGroup("pnd_End_Pnd_E_Dd", "#E-DD", FieldType.NUMERIC, 2);
        pnd_W_Iss_Dte = localVariables.newFieldInRecord("pnd_W_Iss_Dte", "#W-ISS-DTE", FieldType.NUMERIC, 6);

        pnd_W_Iss_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Iss_Dte__R_Field_3", "REDEFINE", pnd_W_Iss_Dte);
        pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy = pnd_W_Iss_Dte__R_Field_3.newFieldInGroup("pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy", "#W-ISS-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Iss_Dte_Pnd_W_Iss_Mm = pnd_W_Iss_Dte__R_Field_3.newFieldInGroup("pnd_W_Iss_Dte_Pnd_W_Iss_Mm", "#W-ISS-MM", FieldType.NUMERIC, 2);
        pnd_W_Final_Dte = localVariables.newFieldInRecord("pnd_W_Final_Dte", "#W-FINAL-DTE", FieldType.NUMERIC, 6);

        pnd_W_Final_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Final_Dte__R_Field_4", "REDEFINE", pnd_W_Final_Dte);
        pnd_W_Final_Dte_Pnd_W_Final_Yyyy = pnd_W_Final_Dte__R_Field_4.newFieldInGroup("pnd_W_Final_Dte_Pnd_W_Final_Yyyy", "#W-FINAL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_W_Final_Dte_Pnd_W_Final_Mm = pnd_W_Final_Dte__R_Field_4.newFieldInGroup("pnd_W_Final_Dte_Pnd_W_Final_Mm", "#W-FINAL-MM", FieldType.NUMERIC, 
            2);
        pnd_W_Issue_Yr = localVariables.newFieldInRecord("pnd_W_Issue_Yr", "#W-ISSUE-YR", FieldType.NUMERIC, 4);
        pnd_W_Issue_Mo = localVariables.newFieldInRecord("pnd_W_Issue_Mo", "#W-ISSUE-MO", FieldType.NUMERIC, 2);
        pnd_Cntrct_Key = localVariables.newFieldInRecord("pnd_Cntrct_Key", "#CNTRCT-KEY", FieldType.STRING, 10);
        pnd_Fctr = localVariables.newFieldInRecord("pnd_Fctr", "#FCTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Cpr_Id_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Output_Pnd_F1 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Output_Pnd_F2 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Output_Pnd_Cntrct_Part_Payee_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Output_Pnd_F3 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Output_Pnd_Orig_Da_Nbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Orig_Da_Nbr", "#ORIG-DA-NBR", FieldType.STRING, 8);
        pnd_Output_Pnd_F4 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Output_Pnd_Fund_Ticker = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fund_Ticker", "#FUND-TICKER", FieldType.STRING, 6);
        pnd_Output_Pnd_F5 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Output_Pnd_Cmpny_Fund_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", FieldType.STRING, 3);

        pnd_Output__R_Field_6 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_6", "REDEFINE", pnd_Output_Pnd_Cmpny_Fund_Cde);
        pnd_Output_Pnd_Cmpny_Cde = pnd_Output__R_Field_6.newFieldInGroup("pnd_Output_Pnd_Cmpny_Cde", "#CMPNY-CDE", FieldType.STRING, 1);
        pnd_Output_Pnd_Fund_Cde = pnd_Output__R_Field_6.newFieldInGroup("pnd_Output_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 2);
        pnd_Output_Pnd_F6 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Output_Pnd_Units = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Units", "#UNITS", FieldType.STRING, 11);
        pnd_Output_Pnd_F7 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F7", "#F7", FieldType.STRING, 1);
        pnd_Output_Pnd_Guar_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Guar_Amt", "#GUAR-AMT", FieldType.STRING, 11);
        pnd_Output_Pnd_F8 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F8", "#F8", FieldType.STRING, 1);
        pnd_Output_Pnd_Div_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Div_Amt", "#DIV-AMT", FieldType.STRING, 11);
        pnd_Output_Pnd_F9 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F9", "#F9", FieldType.STRING, 1);
        pnd_Output_Pnd_Assets_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Assets_Amt", "#ASSETS-AMT", FieldType.STRING, 13);
        pnd_Output_Pnd_F0 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_F0", "#F0", FieldType.STRING, 1);
        pnd_Output_Pnd_Annlzd_Pmt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Annlzd_Pmt", "#ANNLZD-PMT", FieldType.STRING, 12);
        pnd_Output_Pnd_Fa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fa", "#FA", FieldType.STRING, 1);
        pnd_Output_Pnd_Mode = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Output_Pnd_Fb = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fb", "#FB", FieldType.STRING, 1);
        pnd_Output_Pnd_1st_Dob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_1st_Dob", "#1ST-DOB", FieldType.NUMERIC, 8);
        pnd_Output_Pnd_Fc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fc", "#FC", FieldType.STRING, 1);
        pnd_Output_Pnd_2nd_Dob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_2nd_Dob", "#2ND-DOB", FieldType.NUMERIC, 8);
        pnd_Output_Pnd_Fd = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fd", "#FD", FieldType.STRING, 1);
        pnd_Output_Pnd_Optn_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Fe = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fe", "#FE", FieldType.STRING, 1);
        pnd_Output_Pnd_Sex_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Sex_Cde", "#SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ff = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ff", "#FF", FieldType.STRING, 1);
        pnd_Output_Pnd_Iss_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Iss_Dte", "#ISS-DTE", FieldType.STRING, 8);

        pnd_Output__R_Field_7 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_7", "REDEFINE", pnd_Output_Pnd_Iss_Dte);
        pnd_Output_Pnd_Iss_Yyyymm = pnd_Output__R_Field_7.newFieldInGroup("pnd_Output_Pnd_Iss_Yyyymm", "#ISS-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Output_Pnd_Iss_Dd = pnd_Output__R_Field_7.newFieldInGroup("pnd_Output_Pnd_Iss_Dd", "#ISS-DD", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Fg = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fg", "#FG", FieldType.STRING, 1);
        pnd_Output_Pnd_Effctv_Dte = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.STRING, 8);
        pnd_Output_Pnd_Fh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fh", "#FH", FieldType.STRING, 1);
        pnd_Output_Pnd_Guar_Prd = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Guar_Prd", "#GUAR-PRD", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Fi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fi", "#FI", FieldType.STRING, 1);
        pnd_Output_Pnd_Plan_Nmbr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Output_Pnd_Fj = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fj", "#FJ", FieldType.STRING, 1);
        pnd_Output_Pnd_B_Units = pnd_Output.newFieldInGroup("pnd_Output_Pnd_B_Units", "#B-UNITS", FieldType.STRING, 11);
        pnd_Output_Pnd_Fk = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fk", "#FK", FieldType.STRING, 1);
        pnd_Output_Pnd_B_Guar_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_B_Guar_Amt", "#B-GUAR-AMT", FieldType.STRING, 11);
        pnd_Output_Pnd_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fl", "#FL", FieldType.STRING, 1);
        pnd_Output_Pnd_B_Div_Amt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_B_Div_Amt", "#B-DIV-AMT", FieldType.STRING, 11);
        pnd_Output_Pnd_Fm = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Fm", "#FM", FieldType.STRING, 1);
        pnd_Output_Pnd_B_Annlzd_Pmt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_B_Annlzd_Pmt", "#B-ANNLZD-PMT", FieldType.STRING, 12);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        pls_Tckr_Symbl = WsIndependent.getInstance().newFieldArrayInRecord("pls_Tckr_Symbl", "+TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 
            20));
        pls_Fund_Num_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Num_Cde", "+FUND-NUM-CDE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            20));
        pls_Fund_Alpha_Cde = WsIndependent.getInstance().newFieldArrayInRecord("pls_Fund_Alpha_Cde", "+FUND-ALPHA-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_cntrct_To.reset();
        vw_cpr.reset();
        vw_iaa_Xfr_Audit.reset();
        vw_cntrl.reset();

        localVariables.reset();
        pnd_Fund_1.setInitialValue("R");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapxrmw() throws Exception
    {
        super("Iaapxrmw");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 250 PS = 0;//Natural: FORMAT ( 2 ) LS = 250 PS = 0
        pnd_Output_Pnd_F1.setValue(",");                                                                                                                                  //Natural: MOVE ',' TO #F1 #F2 #F3 #F4 #F5 #F6 #F7 #F8 #F9 #F0 #FA #FB #FC #FD #FE #FF #FG #FH #FI #FJ #FK #FL #FM
        pnd_Output_Pnd_F2.setValue(",");
        pnd_Output_Pnd_F3.setValue(",");
        pnd_Output_Pnd_F4.setValue(",");
        pnd_Output_Pnd_F5.setValue(",");
        pnd_Output_Pnd_F6.setValue(",");
        pnd_Output_Pnd_F7.setValue(",");
        pnd_Output_Pnd_F8.setValue(",");
        pnd_Output_Pnd_F9.setValue(",");
        pnd_Output_Pnd_F0.setValue(",");
        pnd_Output_Pnd_Fa.setValue(",");
        pnd_Output_Pnd_Fb.setValue(",");
        pnd_Output_Pnd_Fc.setValue(",");
        pnd_Output_Pnd_Fd.setValue(",");
        pnd_Output_Pnd_Fe.setValue(",");
        pnd_Output_Pnd_Ff.setValue(",");
        pnd_Output_Pnd_Fg.setValue(",");
        pnd_Output_Pnd_Fh.setValue(",");
        pnd_Output_Pnd_Fi.setValue(",");
        pnd_Output_Pnd_Fj.setValue(",");
        pnd_Output_Pnd_Fk.setValue(",");
        pnd_Output_Pnd_Fl.setValue(",");
        pnd_Output_Pnd_Fm.setValue(",");
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, pnd_Fund_2, pnd_Rc);                                                                       //Natural: CALLNAT 'IAAN0511' #FUND-1 #FUND-2 #RC
        if (condition(Global.isEscape())) return;
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            if (condition(cntrl_Cntrl_Cde.notEquals("DC")))                                                                                                               //Natural: IF CNTRL-CDE NE 'DC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Start.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                              //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #START
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Start.equals(" ")))                                                                                                                             //Natural: IF #START = ' '
        {
            getReports().write(0, "SEVERE ERROR - NO DC CONTROL RECORD FOUND");                                                                                           //Natural: WRITE 'SEVERE ERROR - NO DC CONTROL RECORD FOUND'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Start_Pnd_S_Mm.nsubtract(1);                                                                                                                                  //Natural: SUBTRACT 1 FROM #S-MM
        if (condition(pnd_Start_Pnd_S_Mm.lessOrEqual(getZero())))                                                                                                         //Natural: IF #S-MM LE 0
        {
            pnd_Start_Pnd_S_Mm.nadd(12);                                                                                                                                  //Natural: ADD 12 TO #S-MM
            pnd_Start_Pnd_S_Yyyy.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #S-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Start_Pnd_S_Dd.setValue(1);                                                                                                                                   //Natural: ASSIGN #S-DD := 01
        pnd_End.setValue(pnd_Start);                                                                                                                                      //Natural: ASSIGN #END := #START
        pnd_End_Pnd_E_Dd.setValue(31);                                                                                                                                    //Natural: ASSIGN #E-DD := 31
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(DbsUtil.maskMatches(pnd_End,"YYYYMMDD")))                                                                                                       //Natural: IF #END = MASK ( YYYYMMDD )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_End_Pnd_E_Dd.nsubtract(1);                                                                                                                                //Natural: SUBTRACT 1 FROM #E-DD
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        vw_iaa_Xfr_Audit.startDatabaseRead                                                                                                                                //Natural: READ IAA-XFR-AUDIT BY RQST-ID STARTING FROM '00000000000000000000000000000'
        (
        "READ02",
        new Wc[] { new Wc("RQST_ID", ">=", "00000000000000000000000000000", WcType.BY) },
        new Oc[] { new Oc("RQST_ID", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Xfr_Audit.readNextRow("READ02")))
        {
            pnd_Output_Pnd_Effctv_Dte.setValueEdited(iaa_Xfr_Audit_Iaxfr_Effctve_Dte,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED IAXFR-EFFCTVE-DTE ( EM = YYYYMMDD ) TO #EFFCTV-DTE
            pnd_Entry_Dte.setValueEdited(iaa_Xfr_Audit_Iaxfr_Entry_Dte,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED IAXFR-ENTRY-DTE ( EM = YYYYMMDD ) TO #ENTRY-DTE
            if (condition(!(((pnd_Output_Pnd_Effctv_Dte.greaterOrEqual(pnd_Start) && pnd_Output_Pnd_Effctv_Dte.lessOrEqual(pnd_End)) || (pnd_Entry_Dte.greaterOrEqual(pnd_Start)  //Natural: ACCEPT IF #EFFCTV-DTE = #START THRU #END OR #ENTRY-DTE = #START THRU #END
                && pnd_Entry_Dte.lessOrEqual(pnd_End))))))
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CNTRCT
            sub_Read_Cntrct();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CPR
            sub_Read_Cpr();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Output_Pnd_F1.reset();                                                                                                                                        //Natural: RESET #F1
        getWorkFiles().write(2, false, pnd_Output_Pnd_F1);                                                                                                                //Natural: WRITE WORK FILE 2 #F1
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*    '///Mode'              CNTRCT-MODE-IND
        //*    '//Entry/Date'         IAXFR-ENTRY-DTE(EM=YYYYMMDD)
        //*    '//Cycle/Date'         IAXFR-CYCLE-DTE(EM=YYYYMMDD)
        //*    '//Rqst/Id'            RQST-ID
        //*    '///Aftr Units'        IAXFR-TO-AFTR-XFR-UNITS(#I)
        //*    '///Aftr Guar'         IAXFR-TO-AFTR-XFR-GUAR(#I)
        //*    '///Aftr Div'          IAXFR-TO-AFTR-XFR-DIVID(#I)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CNTRCT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ANNUALIZE-PAYMENT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-GUARANTEED-PERIOD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DA-NBR
        //* ***********************************************************************
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data.greater(iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data)))                                        //Natural: IF C*IAXFR-CALC-TO-ACCT-DATA GT C*IAXFR-CALC-FROM-ACCT-DATA
        {
            pnd_I2.setValue(iaa_Xfr_Audit_Count_Castiaxfr_Calc_To_Acct_Data);                                                                                             //Natural: ASSIGN #I2 := C*IAXFR-CALC-TO-ACCT-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_I2.setValue(iaa_Xfr_Audit_Count_Castiaxfr_Calc_From_Acct_Data);                                                                                           //Natural: ASSIGN #I2 := C*IAXFR-CALC-FROM-ACCT-DATA
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-FILE
        sub_Write_To_File();
        if (condition(Global.isEscape())) {return;}
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            getReports().display(1, ReportOption.NOTITLE,"///Cntrt-Frm",                                                                                                  //Natural: DISPLAY ( 1 ) NOTITLE '///Cntrt-Frm' IAXFR-FROM-PPCN-NBR '//Py/Frm' IAXFR-FROM-PAYEE-CDE '///Cntrt-To' IAXFR-CALC-TO-PPCN-NBR '//Py/To' IAXFR-CALC-TO-PAYEE-CDE '//Effect/Date' #EFFCTV-DTE '//Frm/Fnd' IAXFR-FRM-ACCT-CD ( #I ) '/Frm/Rvl/Mth' IAXFR-FRM-UNIT-TYP ( #I ) '//To/fnd' IAXFR-TO-ACCT-CD ( #I ) '/To/Rvl/Mth' IAXFR-TO-UNIT-TYP ( #I ) '///Asset-frm-amt' IAXFR-FROM-ASSET-XFR-AMT ( #I ) '///Asset-to-Amt' IAXFR-TO-ASSET-AMT ( #I ) '///Xfr fr Guar' IAXFR-FROM-RQSTD-XFR-GUAR ( #I ) '///Xfr fr Div' IAXFR-FROM-RQSTD-XFR-DIVID ( #I ) '///Xfr fr Units' IAXFR-FROM-RQSTD-XFR-UNITS ( #I ) '///Xfr To Guar' IAXFR-TO-XFR-GUAR ( #I ) '///Xfr To Div' IAXFR-TO-XFR-DIVID ( #I ) '///Xfr To Units' IAXFR-TO-XFR-UNITS ( #I ) '///Bfr to Gr' IAXFR-TO-BFR-XFR-GUAR ( #I ) '///Bfr to Dv' IAXFR-TO-BFR-XFR-DIVID ( #I ) '///Bfr to Unts' IAXFR-TO-BFR-XFR-UNITS ( #I ) '///Bfr fr Gr' IAXFR-FROM-CURRENT-PMT-GUAR ( #I ) '///Bfr fr Dv' IAXFR-FROM-CURRENT-PMT-DIVID ( #I ) '///Bfr fr Unts' IAXFR-FROM-CURRENT-PMT-UNITS ( #I )
            		iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"//Py/Frm",
            		iaa_Xfr_Audit_Iaxfr_From_Payee_Cde,"///Cntrt-To",
            		iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr,"//Py/To",
            		iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde,"//Effect/Date",
            		pnd_Output_Pnd_Effctv_Dte,"//Frm/Fnd",
            		iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I),"/Frm/Rvl/Mth",
            		iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I),"//To/fnd",
            		iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I),"/To/Rvl/Mth",
            		iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I),"///Asset-frm-amt",
            		iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I),"///Asset-to-Amt",
            		iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I),"///Xfr fr Guar",
            		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_I),"///Xfr fr Div",
            		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I),"///Xfr fr Units",
            		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I),"///Xfr To Guar",
            		iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar.getValue(pnd_I),"///Xfr To Div",
            		iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue(pnd_I),"///Xfr To Units",
            		iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue(pnd_I),"///Bfr to Gr",
            		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I),"///Bfr to Dv",
            		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid.getValue(pnd_I),"///Bfr to Unts",
            		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I),"///Bfr fr Gr",
            		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I),"///Bfr fr Dv",
            		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid.getValue(pnd_I),"///Bfr fr Unts",
            		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cpr() throws Exception                                                                                                                          //Natural: READ-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                             //Natural: ASSIGN #CNTRCT-PPCN-NBR := IAXFR-FROM-PPCN-NBR
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                           //Natural: ASSIGN #CNTRCT-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_cpr.readNextRow("R1")))
        {
            pnd_Output_Pnd_Mode.setValue(cpr_Cntrct_Mode_Ind);                                                                                                            //Natural: ASSIGN #MODE := CNTRCT-MODE-IND
            pnd_Output_Pnd_Cpr_Id_Nbr.setValue(cpr_Cpr_Id_Nbr);                                                                                                           //Natural: ASSIGN #CPR-ID-NBR := CPR-ID-NBR
            if (condition(pnd_Output_Pnd_Optn_Cde.equals(21)))                                                                                                            //Natural: IF #OPTN-CDE = 21
            {
                                                                                                                                                                          //Natural: PERFORM GET-GUARANTEED-PERIOD
                sub_Get_Guaranteed_Period();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cpr_Cntrct_Part_Payee_Cde.equals(1) || iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                 //Natural: IF CNTRCT-PART-PAYEE-CDE = 1 OR CNTRCT-SCND-ANNT-DOD-DTE GT 0
            {
                pnd_Output_Pnd_Sex_Cde.setValue(iaa_Cntrct_Cntrct_First_Annt_Sex_Cde);                                                                                    //Natural: ASSIGN #SEX-CDE := CNTRCT-FIRST-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Pnd_Sex_Cde.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde);                                                                                     //Natural: ASSIGN #SEX-CDE := CNTRCT-SCND-ANNT-SEX-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Cntrct() throws Exception                                                                                                                       //Natural: READ-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Key.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                                                       //Natural: ASSIGN #CNTRCT-KEY := IAXFR-FROM-PPCN-NBR
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ ( 1 ) IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM #CNTRCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", pnd_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        READ03:
        while (condition(vw_iaa_Cntrct.readNextRow("READ03")))
        {
            pnd_Output_Pnd_Iss_Yyyymm.setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                                              //Natural: ASSIGN #ISS-YYYYMM := CNTRCT-ISSUE-DTE
            pnd_Output_Pnd_Iss_Dd.setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                                                               //Natural: ASSIGN #ISS-DD := CNTRCT-ISSUE-DTE-DD
            if (condition(pnd_Output_Pnd_Iss_Dd.equals(getZero())))                                                                                                       //Natural: IF #ISS-DD = 0
            {
                pnd_Output_Pnd_Iss_Dd.setValue(1);                                                                                                                        //Natural: ASSIGN #ISS-DD := 01
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Pnd_1st_Dob.setValue(iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);                                                                                        //Natural: ASSIGN #1ST-DOB := CNTRCT-FIRST-ANNT-DOB-DTE
            pnd_Output_Pnd_2nd_Dob.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);                                                                                         //Natural: ASSIGN #2ND-DOB := CNTRCT-SCND-ANNT-DOB-DTE
            pnd_Output_Pnd_Orig_Da_Nbr.setValue(iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr);                                                                                    //Natural: ASSIGN #ORIG-DA-NBR := CNTRCT-ORIG-DA-CNTRCT-NBR
            pnd_Output_Pnd_Optn_Cde.setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                                                 //Natural: ASSIGN #OPTN-CDE := CNTRCT-OPTN-CDE
            if (condition(iaa_Cntrct_Plan_Nmbr.equals("MLTPLN")))                                                                                                         //Natural: IF PLAN-NMBR = 'MLTPLN'
            {
                FOR02:                                                                                                                                                    //Natural: FOR #K 1 TO 20
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(20)); pnd_K.nadd(1))
                {
                    if (condition(iaa_Cntrct_Lgcy_Plan_Nbr.getValue(pnd_K).equals(" ")))                                                                                  //Natural: IF LGCY-PLAN-NBR ( #K ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(iaa_Cntrct_Lgcy_Plan_Nbr.getValue(pnd_K).notEquals(" ") && (iaa_Cntrct_Lgcy_Sub_Plan_Nbr.getValue(pnd_K).notEquals(" ")                 //Natural: IF LGCY-PLAN-NBR ( #K ) NE ' ' AND ( LGCY-SUB-PLAN-NBR ( #K ) NE ' ' OR LGCY-ORIG-SUB-PLAN ( #K ) NE ' ' )
                        || iaa_Cntrct_Lgcy_Orig_Sub_Plan.getValue(pnd_K).notEquals(" "))))
                    {
                        pnd_Output_Pnd_Plan_Nmbr.setValue(iaa_Cntrct_Lgcy_Plan_Nbr.getValue(pnd_K));                                                                      //Natural: ASSIGN #PLAN-NMBR := LGCY-PLAN-NBR ( #K )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(iaa_Cntrct_Plan_Nmbr.equals("SGLPLN")))                                                                                                     //Natural: IF PLAN-NMBR = 'SGLPLN'
                {
                    pnd_Output_Pnd_Plan_Nmbr.setValue(" ");                                                                                                               //Natural: ASSIGN #PLAN-NMBR := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Plan_Nmbr.setValue(iaa_Cntrct_Plan_Nmbr);                                                                                              //Natural: ASSIGN #PLAN-NMBR := PLAN-NMBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(21)))                                                                                                         //Natural: IF CNTRCT-OPTN-CDE = 21
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet443 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE CNTRCT-OPTN-CDE;//Natural: VALUE 50,51,52,53
                if (condition((iaa_Cntrct_Cntrct_Optn_Cde.equals(50) || iaa_Cntrct_Cntrct_Optn_Cde.equals(51) || iaa_Cntrct_Cntrct_Optn_Cde.equals(52) 
                    || iaa_Cntrct_Cntrct_Optn_Cde.equals(53))))
                {
                    decideConditionsMet443++;
                    pnd_Output_Pnd_Guar_Prd.setValue(5);                                                                                                                  //Natural: ASSIGN #GUAR-PRD := 5
                }                                                                                                                                                         //Natural: VALUE 05,08,16,17,54
                else if (condition((iaa_Cntrct_Cntrct_Optn_Cde.equals(5) || iaa_Cntrct_Cntrct_Optn_Cde.equals(8) || iaa_Cntrct_Cntrct_Optn_Cde.equals(16) 
                    || iaa_Cntrct_Cntrct_Optn_Cde.equals(17) || iaa_Cntrct_Cntrct_Optn_Cde.equals(54))))
                {
                    decideConditionsMet443++;
                    pnd_Output_Pnd_Guar_Prd.setValue(10);                                                                                                                 //Natural: ASSIGN #GUAR-PRD := 10
                }                                                                                                                                                         //Natural: VALUE 15,09,13,14,15,55
                else if (condition((iaa_Cntrct_Cntrct_Optn_Cde.equals(15) || iaa_Cntrct_Cntrct_Optn_Cde.equals(9) || iaa_Cntrct_Cntrct_Optn_Cde.equals(13) 
                    || iaa_Cntrct_Cntrct_Optn_Cde.equals(14) || iaa_Cntrct_Cntrct_Optn_Cde.equals(15) || iaa_Cntrct_Cntrct_Optn_Cde.equals(55))))
                {
                    decideConditionsMet443++;
                    pnd_Output_Pnd_Guar_Prd.setValue(15);                                                                                                                 //Natural: ASSIGN #GUAR-PRD := 15
                }                                                                                                                                                         //Natural: VALUE 06,10,11,12,56
                else if (condition((iaa_Cntrct_Cntrct_Optn_Cde.equals(6) || iaa_Cntrct_Cntrct_Optn_Cde.equals(10) || iaa_Cntrct_Cntrct_Optn_Cde.equals(11) 
                    || iaa_Cntrct_Cntrct_Optn_Cde.equals(12) || iaa_Cntrct_Cntrct_Optn_Cde.equals(56))))
                {
                    decideConditionsMet443++;
                    pnd_Output_Pnd_Guar_Prd.setValue(20);                                                                                                                 //Natural: ASSIGN #GUAR-PRD := 20
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Output_Pnd_Guar_Prd.reset();                                                                                                                      //Natural: RESET #GUAR-PRD
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Annualize_Payment() throws Exception                                                                                                                 //Natural: ANNUALIZE-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet463 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE CNTRCT-MODE-IND;//Natural: VALUE 801 : 812
        if (condition(((cpr_Cntrct_Mode_Ind.greaterOrEqual(801) && cpr_Cntrct_Mode_Ind.lessOrEqual(812)))))
        {
            decideConditionsMet463++;
            pnd_Fctr.setValue(1);                                                                                                                                         //Natural: ASSIGN #FCTR := 1
        }                                                                                                                                                                 //Natural: VALUE 601 : 603
        else if (condition(((cpr_Cntrct_Mode_Ind.greaterOrEqual(601) && cpr_Cntrct_Mode_Ind.lessOrEqual(603)))))
        {
            decideConditionsMet463++;
            pnd_Fctr.setValue(3);                                                                                                                                         //Natural: ASSIGN #FCTR := 3
        }                                                                                                                                                                 //Natural: VALUE 701 : 706
        else if (condition(((cpr_Cntrct_Mode_Ind.greaterOrEqual(701) && cpr_Cntrct_Mode_Ind.lessOrEqual(706)))))
        {
            decideConditionsMet463++;
            pnd_Fctr.setValue(6);                                                                                                                                         //Natural: ASSIGN #FCTR := 6
        }                                                                                                                                                                 //Natural: VALUE 100
        else if (condition((cpr_Cntrct_Mode_Ind.equals(100))))
        {
            decideConditionsMet463++;
            pnd_Fctr.setValue(12);                                                                                                                                        //Natural: ASSIGN #FCTR := 12
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet463 > 0))
        {
            pnd_A_Pmt.compute(new ComputeParameters(true, pnd_A_Pmt), pnd_W_Pmt.multiply(pnd_Fctr));                                                                      //Natural: COMPUTE ROUNDED #A-PMT = #W-PMT * #FCTR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Guaranteed_Period() throws Exception                                                                                                             //Natural: GET-GUARANTEED-PERIOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Output_Pnd_Guar_Prd.reset();                                                                                                                                  //Natural: RESET #GUAR-PRD
        pnd_W_Iss_Dte.setValue(pnd_Output_Pnd_Iss_Yyyymm);                                                                                                                //Natural: ASSIGN #W-ISS-DTE := #ISS-YYYYMM
        pnd_W_Final_Dte.setValue(cpr_Cntrct_Final_Per_Pay_Dte);                                                                                                           //Natural: ASSIGN #W-FINAL-DTE := CNTRCT-FINAL-PER-PAY-DTE
        if (condition(pnd_W_Iss_Dte_Pnd_W_Iss_Mm.greater(pnd_W_Final_Dte_Pnd_W_Final_Mm)))                                                                                //Natural: IF #W-ISS-MM GT #W-FINAL-MM
        {
            pnd_W_Final_Dte_Pnd_W_Final_Mm.nadd(12);                                                                                                                      //Natural: ADD 12 TO #W-FINAL-MM
            pnd_W_Final_Dte_Pnd_W_Final_Yyyy.nsubtract(1);                                                                                                                //Natural: SUBTRACT 1 FROM #W-FINAL-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_W_Issue_Yr.compute(new ComputeParameters(false, pnd_W_Issue_Yr), pnd_W_Final_Dte_Pnd_W_Final_Yyyy.subtract(pnd_W_Iss_Dte_Pnd_W_Iss_Yyyy));                    //Natural: ASSIGN #W-ISSUE-YR := #W-FINAL-YYYY - #W-ISS-YYYY
        pnd_W_Issue_Mo.compute(new ComputeParameters(false, pnd_W_Issue_Mo), pnd_W_Final_Dte_Pnd_W_Final_Mm.subtract(pnd_W_Iss_Dte_Pnd_W_Iss_Mm));                        //Natural: ASSIGN #W-ISSUE-MO := #W-FINAL-MM - #W-ISS-MM
        short decideConditionsMet492 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #MODE;//Natural: VALUE 100
        if (condition((pnd_Output_Pnd_Mode.equals(100))))
        {
            decideConditionsMet492++;
            pnd_W_Issue_Mo.nadd(1);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 1
        }                                                                                                                                                                 //Natural: VALUE 600:699
        else if (condition(((pnd_Output_Pnd_Mode.greaterOrEqual(600) && pnd_Output_Pnd_Mode.lessOrEqual(699)))))
        {
            decideConditionsMet492++;
            pnd_W_Issue_Mo.nadd(3);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 3
        }                                                                                                                                                                 //Natural: VALUE 700:799
        else if (condition(((pnd_Output_Pnd_Mode.greaterOrEqual(700) && pnd_Output_Pnd_Mode.lessOrEqual(799)))))
        {
            decideConditionsMet492++;
            pnd_W_Issue_Mo.nadd(6);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO + 6
        }                                                                                                                                                                 //Natural: VALUE 800:899
        else if (condition(((pnd_Output_Pnd_Mode.greaterOrEqual(800) && pnd_Output_Pnd_Mode.lessOrEqual(899)))))
        {
            decideConditionsMet492++;
            pnd_W_Issue_Yr.nadd(1);                                                                                                                                       //Natural: ASSIGN #W-ISSUE-YR := #W-ISSUE-YR + 1
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet492 > 0))
        {
            if (condition(pnd_W_Issue_Mo.greaterOrEqual(12)))                                                                                                             //Natural: IF #W-ISSUE-MO GE 12
            {
                pnd_W_Issue_Mo.nsubtract(12);                                                                                                                             //Natural: ASSIGN #W-ISSUE-MO := #W-ISSUE-MO - 12
                pnd_W_Issue_Yr.nadd(1);                                                                                                                                   //Natural: ASSIGN #W-ISSUE-YR := #W-ISSUE-YR + 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Pnd_Guar_Prd.compute(new ComputeParameters(true, pnd_Output_Pnd_Guar_Prd), (pnd_W_Issue_Yr.multiply(12).add(pnd_W_Issue_Mo)).divide(12));          //Natural: COMPUTE ROUNDED #GUAR-PRD = ( #W-ISSUE-YR * 12 + #W-ISSUE-MO ) / 12
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_To_File() throws Exception                                                                                                                     //Natural: WRITE-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF IAXFR-FRM-ACCT-CD ( #I ) NE ' '
            {
                pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                          //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-FROM-PPCN-NBR
                pnd_Output_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                        //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
                if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals("T") || iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I).equals("G")))                //Natural: IF IAXFR-FRM-ACCT-CD ( #I ) = 'T' OR = 'G'
                {
                    pnd_Output_Pnd_Cmpny_Fund_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "T1", iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I)));       //Natural: COMPRESS 'T1' IAXFR-FRM-ACCT-CD ( #I ) INTO #CMPNY-FUND-CDE LEAVE NO
                    DbsUtil.examine(new ExamineSource(pls_Fund_Alpha_Cde.getValue("*"),true), new ExamineSearch("T"), new ExamineGivingIndex(pnd_J));                     //Natural: EXAMINE FULL +FUND-ALPHA-CDE ( * ) FOR 'T' GIVING INDEX #J
                    iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I).reset();                                                                                     //Natural: RESET IAXFR-FROM-RQSTD-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I).reset();                                                                                     //Natural: RESET IAXFR-FROM-RQSTD-XFR-DIVID ( #I )
                    DbsUtil.examine(new ExamineSource(pls_Fund_Alpha_Cde.getValue("*"),true), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd.getValue(pnd_I)),         //Natural: EXAMINE FULL +FUND-ALPHA-CDE ( * ) FOR IAXFR-FRM-ACCT-CD ( #I ) GIVING INDEX #J
                        new ExamineGivingIndex(pnd_J));
                    pnd_Output_Pnd_Fund_Cde.setValueEdited(pls_Fund_Num_Cde.getValue(pnd_J),new ReportEditMask("99"));                                                    //Natural: MOVE EDITED +FUND-NUM-CDE ( #J ) ( EM = 99 ) TO #FUND-CDE
                    if (condition(pnd_Output_Pnd_Fund_Cde.equals("09") || pnd_Output_Pnd_Fund_Cde.equals("11") || pnd_Output_Pnd_Fund_Cde.greaterOrEqual("40")))          //Natural: IF #FUND-CDE = '09' OR = '11' OR #FUND-CDE GE '40'
                    {
                        if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals("A")))                                                                      //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = 'A'
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("U");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := 'U'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("W");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := 'W'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ.getValue(pnd_I).equals("A")))                                                                      //Natural: IF IAXFR-FRM-UNIT-TYP ( #I ) = 'A'
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("2");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := '2'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("4");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := '4'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_J.greater(getZero())))                                                                                                                  //Natural: IF #J GT 0
                {
                    pnd_Output_Pnd_Fund_Ticker.setValue(pls_Tckr_Symbl.getValue(pnd_J));                                                                                  //Natural: ASSIGN #FUND-TICKER := +TCKR-SYMBL ( #J )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Pnd_Units.reset();                                                                                                                             //Natural: RESET #UNITS #GUAR-AMT #DIV-AMT #ASSETS-AMT #ANNLZD-PMT #W-AMTS #W-ASSETS #B-UNITS #B-GUAR-AMT #B-DIV-AMT #B-ANNLZD-PMT
                pnd_Output_Pnd_Guar_Amt.reset();
                pnd_Output_Pnd_Div_Amt.reset();
                pnd_Output_Pnd_Assets_Amt.reset();
                pnd_Output_Pnd_Annlzd_Pmt.reset();
                pnd_W_Amts.reset();
                pnd_W_Assets.reset();
                pnd_Output_Pnd_B_Units.reset();
                pnd_Output_Pnd_B_Guar_Amt.reset();
                pnd_Output_Pnd_B_Div_Amt.reset();
                pnd_Output_Pnd_B_Annlzd_Pmt.reset();
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I).greater(getZero())))                                                               //Natural: IF IAXFR-FROM-RQSTD-XFR-UNITS ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units.getValue(pnd_I),new ReportEditMask("999999.999"));                                 //Natural: MOVE EDITED IAXFR-FROM-RQSTD-XFR-UNITS ( #I ) ( EM = 999999.999 ) TO #W-AMTS
                    pnd_Output_Pnd_Units.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "-", pnd_W_Amts));                                                      //Natural: COMPRESS '-' #W-AMTS INTO #UNITS LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Units.setValue("-000000.000");                                                                                                         //Natural: ASSIGN #UNITS := '-000000.000'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_I).greater(getZero())))                                                                //Natural: IF IAXFR-FROM-RQSTD-XFR-GUAR ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar.getValue(pnd_I),new ReportEditMask("9999999.99"));                                  //Natural: MOVE EDITED IAXFR-FROM-RQSTD-XFR-GUAR ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_Guar_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "-", pnd_W_Amts));                                                   //Natural: COMPRESS '-' #W-AMTS INTO #GUAR-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Guar_Amt.setValue("-0000000.00");                                                                                                      //Natural: ASSIGN #GUAR-AMT := '-0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I).greater(getZero())))                                                               //Natural: IF IAXFR-FROM-RQSTD-XFR-DIVID ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid.getValue(pnd_I),new ReportEditMask("9999999.99"));                                 //Natural: MOVE EDITED IAXFR-FROM-RQSTD-XFR-DIVID ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_Div_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "-", pnd_W_Amts));                                                    //Natural: COMPRESS '-' #W-AMTS INTO #DIV-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Div_Amt.setValue("-0000000.00");                                                                                                       //Natural: ASSIGN #DIV-AMT := '-0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I).greater(getZero())))                                                                 //Natural: IF IAXFR-FROM-ASSET-XFR-AMT ( #I ) GT 0
                {
                    pnd_W_Assets.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt.getValue(pnd_I),new ReportEditMask("999999999.99"));                               //Natural: MOVE EDITED IAXFR-FROM-ASSET-XFR-AMT ( #I ) ( EM = 999999999.99 ) TO #W-ASSETS
                    pnd_Output_Pnd_Assets_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "-", pnd_W_Assets));                                               //Natural: COMPRESS '-' #W-ASSETS INTO #ASSETS-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Assets_Amt.setValue("-000000000.00");                                                                                                  //Natural: ASSIGN #ASSETS-AMT := '-000000000.00'
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Pmt.compute(new ComputeParameters(false, pnd_W_Pmt), iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Guar.getValue(pnd_I).add(iaa_Xfr_Audit_Iaxfr_From_Aftr_Xfr_Divid.getValue(pnd_I))); //Natural: COMPUTE #W-PMT = IAXFR-FROM-AFTR-XFR-GUAR ( #I ) + IAXFR-FROM-AFTR-XFR-DIVID ( #I )
                                                                                                                                                                          //Natural: PERFORM ANNUALIZE-PAYMENT
                sub_Annualize_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Output_Pnd_Annlzd_Pmt.setValueEdited(pnd_A_Pmt,new ReportEditMask("999999999.99"));                                                                   //Natural: MOVE EDITED #A-PMT ( EM = 999999999.99 ) TO #ANNLZD-PMT
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I).greater(getZero())))                                                             //Natural: IF IAXFR-FROM-CURRENT-PMT-UNITS ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units.getValue(pnd_I),new ReportEditMask("999999.999"));                               //Natural: MOVE EDITED IAXFR-FROM-CURRENT-PMT-UNITS ( #I ) ( EM = 999999.999 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Units.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                    //Natural: COMPRESS '+' #W-AMTS INTO #B-UNITS LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Units.setValue("+000000.000");                                                                                                       //Natural: ASSIGN #B-UNITS := '+000000.000'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I).greater(getZero())))                                                              //Natural: IF IAXFR-FROM-CURRENT-PMT-GUAR ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I),new ReportEditMask("9999999.99"));                                //Natural: MOVE EDITED IAXFR-FROM-CURRENT-PMT-GUAR ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Guar_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                 //Natural: COMPRESS '+' #W-AMTS INTO #B-GUAR-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Guar_Amt.setValue("+0000000.00");                                                                                                    //Natural: ASSIGN #B-GUAR-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid.getValue(pnd_I).greater(getZero())))                                                             //Natural: IF IAXFR-FROM-CURRENT-PMT-DIVID ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid.getValue(pnd_I),new ReportEditMask("9999999.99"));                               //Natural: MOVE EDITED IAXFR-FROM-CURRENT-PMT-DIVID ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Div_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                  //Natural: COMPRESS '+' #W-AMTS INTO #B-DIV-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Div_Amt.setValue("+0000000.00");                                                                                                     //Natural: ASSIGN #B-DIV-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Pmt.compute(new ComputeParameters(false, pnd_W_Pmt), iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar.getValue(pnd_I).add(iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid.getValue(pnd_I))); //Natural: COMPUTE #W-PMT = IAXFR-FROM-CURRENT-PMT-GUAR ( #I ) + IAXFR-FROM-CURRENT-PMT-DIVID ( #I )
                                                                                                                                                                          //Natural: PERFORM ANNUALIZE-PAYMENT
                sub_Annualize_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Output_Pnd_B_Annlzd_Pmt.setValueEdited(pnd_A_Pmt,new ReportEditMask("999999999.99"));                                                                 //Natural: MOVE EDITED #A-PMT ( EM = 999999999.99 ) TO #B-ANNLZD-PMT
                getWorkFiles().write(1, false, pnd_Output);                                                                                                               //Natural: WRITE WORK FILE 1 #OUTPUT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                                 //Natural: IF IAXFR-TO-ACCT-CD ( #I ) NE ' '
            {
                if (condition(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr.notEquals(" ")))                                                                                       //Natural: IF IAXFR-CALC-TO-PPCN-NBR NE ' '
                {
                    pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr);                                                                   //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-CALC-TO-PPCN-NBR
                    pnd_Output_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde);                                                                 //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-CALC-TO-PAYEE-CDE
                    if (condition(pnd_I.equals(1)))                                                                                                                       //Natural: IF #I = 1
                    {
                                                                                                                                                                          //Natural: PERFORM GET-DA-NBR
                        sub_Get_Da_Nbr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr);                                                                      //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := IAXFR-FROM-PPCN-NBR
                    pnd_Output_Pnd_Cntrct_Part_Payee_Cde.setValue(iaa_Xfr_Audit_Iaxfr_From_Payee_Cde);                                                                    //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := IAXFR-FROM-PAYEE-CDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("T") || iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I).equals("G")))                  //Natural: IF IAXFR-TO-ACCT-CD ( #I ) = 'T' OR = 'G'
                {
                    pnd_Output_Pnd_Cmpny_Fund_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "T1", iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I)));        //Natural: COMPRESS 'T1' IAXFR-TO-ACCT-CD ( #I ) INTO #CMPNY-FUND-CDE LEAVE NO
                    DbsUtil.examine(new ExamineSource(pls_Fund_Alpha_Cde.getValue("*"),true), new ExamineSearch("T"), new ExamineGivingIndex(pnd_J));                     //Natural: EXAMINE FULL +FUND-ALPHA-CDE ( * ) FOR 'T' GIVING INDEX #J
                    iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue(pnd_I).reset();                                                                                             //Natural: RESET IAXFR-TO-XFR-UNITS ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue(pnd_I).reset();                                                                                             //Natural: RESET IAXFR-TO-XFR-DIVID ( #I ) IAXFR-TO-AFTR-XFR-DIVID ( #I )
                    iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid.getValue(pnd_I).reset();
                    DbsUtil.examine(new ExamineSource(pls_Fund_Alpha_Cde.getValue("*"),true), new ExamineSearch(iaa_Xfr_Audit_Iaxfr_To_Acct_Cd.getValue(pnd_I)),          //Natural: EXAMINE FULL +FUND-ALPHA-CDE ( * ) FOR IAXFR-TO-ACCT-CD ( #I ) GIVING INDEX #J
                        new ExamineGivingIndex(pnd_J));
                    pnd_Output_Pnd_Fund_Cde.setValueEdited(pls_Fund_Num_Cde.getValue(pnd_J),new ReportEditMask("99"));                                                    //Natural: MOVE EDITED +FUND-NUM-CDE ( #J ) ( EM = 99 ) TO #FUND-CDE
                    if (condition(pnd_Output_Pnd_Fund_Cde.equals("09") || pnd_Output_Pnd_Fund_Cde.equals("11") || pnd_Output_Pnd_Fund_Cde.greaterOrEqual("40")))          //Natural: IF #FUND-CDE = '09' OR = '11' OR #FUND-CDE GE '40'
                    {
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                       //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("U");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := 'U'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("W");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := 'W'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(iaa_Xfr_Audit_Iaxfr_To_Unit_Typ.getValue(pnd_I).equals("A")))                                                                       //Natural: IF IAXFR-TO-UNIT-TYP ( #I ) = 'A'
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("2");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := '2'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_Pnd_Cmpny_Cde.setValue("4");                                                                                                       //Natural: ASSIGN #CMPNY-CDE := '4'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_J.greater(getZero())))                                                                                                                  //Natural: IF #J GT 0
                {
                    pnd_Output_Pnd_Fund_Ticker.setValue(pls_Tckr_Symbl.getValue(pnd_J));                                                                                  //Natural: ASSIGN #FUND-TICKER := +TCKR-SYMBL ( #J )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Pnd_Units.reset();                                                                                                                             //Natural: RESET #UNITS #GUAR-AMT #DIV-AMT #ASSETS-AMT #ANNLZD-PMT #W-AMTS #W-ASSETS
                pnd_Output_Pnd_Guar_Amt.reset();
                pnd_Output_Pnd_Div_Amt.reset();
                pnd_Output_Pnd_Assets_Amt.reset();
                pnd_Output_Pnd_Annlzd_Pmt.reset();
                pnd_W_Amts.reset();
                pnd_W_Assets.reset();
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue(pnd_I).greater(getZero())))                                                                       //Natural: IF IAXFR-TO-XFR-UNITS ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Xfr_Units.getValue(pnd_I),new ReportEditMask("999999.999"));                                         //Natural: MOVE EDITED IAXFR-TO-XFR-UNITS ( #I ) ( EM = 999999.999 ) TO #W-AMTS
                    pnd_Output_Pnd_Units.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                      //Natural: COMPRESS '+' #W-AMTS INTO #UNITS LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Units.setValue("+000000.000");                                                                                                         //Natural: ASSIGN #UNITS := '+000000.000'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar.getValue(pnd_I).greater(getZero())))                                                                        //Natural: IF IAXFR-TO-XFR-GUAR ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar.getValue(pnd_I),new ReportEditMask("9999999.99"));                                          //Natural: MOVE EDITED IAXFR-TO-XFR-GUAR ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_Guar_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                   //Natural: COMPRESS '+' #W-AMTS INTO #GUAR-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Guar_Amt.setValue("+0000000.00");                                                                                                      //Natural: ASSIGN #GUAR-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue(pnd_I).greater(getZero())))                                                                       //Natural: IF IAXFR-TO-XFR-DIVID ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid.getValue(pnd_I),new ReportEditMask("9999999.99"));                                         //Natural: MOVE EDITED IAXFR-TO-XFR-DIVID ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_Div_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                    //Natural: COMPRESS '+' #W-AMTS INTO #DIV-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Div_Amt.setValue("+0000000.00");                                                                                                       //Natural: ASSIGN #DIV-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I).greater(getZero())))                                                                       //Natural: IF IAXFR-TO-ASSET-AMT ( #I ) GT 0
                {
                    pnd_W_Assets.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Asset_Amt.getValue(pnd_I),new ReportEditMask("999999999.99"));                                     //Natural: MOVE EDITED IAXFR-TO-ASSET-AMT ( #I ) ( EM = 999999999.99 ) TO #W-ASSETS
                    pnd_Output_Pnd_Assets_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Assets));                                               //Natural: COMPRESS '+' #W-ASSETS INTO #ASSETS-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Assets_Amt.setValue("+000000000.00");                                                                                                  //Natural: ASSIGN #ASSETS-AMT := '+000000000.00'
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Pmt.compute(new ComputeParameters(false, pnd_W_Pmt), iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Guar.getValue(pnd_I).add(iaa_Xfr_Audit_Iaxfr_To_Aftr_Xfr_Divid.getValue(pnd_I))); //Natural: COMPUTE #W-PMT = IAXFR-TO-AFTR-XFR-GUAR ( #I ) + IAXFR-TO-AFTR-XFR-DIVID ( #I )
                                                                                                                                                                          //Natural: PERFORM ANNUALIZE-PAYMENT
                sub_Annualize_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Output_Pnd_Annlzd_Pmt.setValueEdited(pnd_A_Pmt,new ReportEditMask("999999999.99"));                                                                   //Natural: MOVE EDITED #A-PMT ( EM = 999999999.99 ) TO #ANNLZD-PMT
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I).greater(getZero())))                                                                    //Natural: IF IAXFR-TO-BFR-XFR-GUAR ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I),new ReportEditMask("9999999.99"));                                      //Natural: MOVE EDITED IAXFR-TO-BFR-XFR-GUAR ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Guar_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                 //Natural: COMPRESS '+' #W-AMTS INTO #B-GUAR-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Guar_Amt.setValue("+0000000.00");                                                                                                    //Natural: ASSIGN #B-GUAR-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid.getValue(pnd_I).greater(getZero())))                                                                   //Natural: IF IAXFR-TO-BFR-XFR-DIVID ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid.getValue(pnd_I),new ReportEditMask("9999999.99"));                                     //Natural: MOVE EDITED IAXFR-TO-BFR-XFR-DIVID ( #I ) ( EM = 9999999.99 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Div_Amt.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                  //Natural: COMPRESS '+' #W-AMTS INTO #B-DIV-AMT LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Div_Amt.setValue("+0000000.00");                                                                                                     //Natural: ASSIGN #B-DIV-AMT := '+0000000.00'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I).greater(getZero())))                                                                   //Natural: IF IAXFR-TO-BFR-XFR-UNITS ( #I ) GT 0
                {
                    pnd_W_Amts.setValueEdited(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units.getValue(pnd_I),new ReportEditMask("999999.999"));                                     //Natural: MOVE EDITED IAXFR-TO-BFR-XFR-UNITS ( #I ) ( EM = 999999.999 ) TO #W-AMTS
                    pnd_Output_Pnd_B_Units.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+", pnd_W_Amts));                                                    //Natural: COMPRESS '+' #W-AMTS INTO #B-UNITS LEAVE NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_B_Units.setValue("+000000.000");                                                                                                       //Natural: ASSIGN #B-UNITS := '+000000.000'
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Pmt.compute(new ComputeParameters(false, pnd_W_Pmt), iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar.getValue(pnd_I).add(iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid.getValue(pnd_I))); //Natural: COMPUTE #W-PMT = IAXFR-TO-BFR-XFR-GUAR ( #I ) + IAXFR-TO-BFR-XFR-DIVID ( #I )
                                                                                                                                                                          //Natural: PERFORM ANNUALIZE-PAYMENT
                sub_Annualize_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Output_Pnd_B_Annlzd_Pmt.setValueEdited(pnd_A_Pmt,new ReportEditMask("999999999.99"));                                                                 //Natural: MOVE EDITED #A-PMT ( EM = 999999999.99 ) TO #B-ANNLZD-PMT
                getWorkFiles().write(1, false, pnd_Output);                                                                                                               //Natural: WRITE WORK FILE 1 #OUTPUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Da_Nbr() throws Exception                                                                                                                        //Natural: GET-DA-NBR
    {
        if (BLNatReinput.isReinput()) return;

        vw_cntrct_To.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) CNTRCT-TO BY CNTRCT-PPCN-NBR STARTING FROM IAXFR-CALC-TO-PPCN-NBR
        (
        "READ04",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") },
        1
        );
        READ04:
        while (condition(vw_cntrct_To.readNextRow("READ04")))
        {
            pnd_Output_Pnd_Orig_Da_Nbr.setValue(cntrct_To_Cntrct_Orig_Da_Cntrct_Nbr);                                                                                     //Natural: ASSIGN #ORIG-DA-NBR := CNTRCT-ORIG-DA-CNTRCT-NBR
            if (condition(cntrct_To_Plan_Nmbr.equals("MLTPLN")))                                                                                                          //Natural: IF PLAN-NMBR = 'MLTPLN'
            {
                FOR04:                                                                                                                                                    //Natural: FOR #K 1 TO 20
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(20)); pnd_K.nadd(1))
                {
                    if (condition(cntrct_To_Lgcy_Plan_Nbr.getValue(pnd_K).equals(" ")))                                                                                   //Natural: IF LGCY-PLAN-NBR ( #K ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(cntrct_To_Lgcy_Plan_Nbr.getValue(pnd_K).notEquals(" ") && (cntrct_To_Lgcy_Sub_Plan_Nbr.getValue(pnd_K).notEquals(" ")                   //Natural: IF LGCY-PLAN-NBR ( #K ) NE ' ' AND ( LGCY-SUB-PLAN-NBR ( #K ) NE ' ' OR LGCY-ORIG-SUB-PLAN ( #K ) NE ' ' )
                        || cntrct_To_Lgcy_Orig_Sub_Plan.getValue(pnd_K).notEquals(" "))))
                    {
                        pnd_Output_Pnd_Plan_Nmbr.setValue(cntrct_To_Lgcy_Plan_Nbr.getValue(pnd_K));                                                                       //Natural: ASSIGN #PLAN-NMBR := LGCY-PLAN-NBR ( #K )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cntrct_To_Plan_Nmbr.equals("SGLPLN")))                                                                                                      //Natural: IF PLAN-NMBR = 'SGLPLN'
                {
                    pnd_Output_Pnd_Plan_Nmbr.setValue(" ");                                                                                                               //Natural: ASSIGN #PLAN-NMBR := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Plan_Nmbr.setValue(cntrct_To_Plan_Nmbr);                                                                                               //Natural: ASSIGN #PLAN-NMBR := PLAN-NMBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=250 PS=0");
        Global.format(2, "LS=250 PS=0");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"///Cntrt-Frm",
        		iaa_Xfr_Audit_Iaxfr_From_Ppcn_Nbr,"//Py/Frm",
        		iaa_Xfr_Audit_Iaxfr_From_Payee_Cde,"///Cntrt-To",
        		iaa_Xfr_Audit_Iaxfr_Calc_To_Ppcn_Nbr,"//Py/To",
        		iaa_Xfr_Audit_Iaxfr_Calc_To_Payee_Cde,"//Effect/Date",
        		pnd_Output_Pnd_Effctv_Dte,"//Frm/Fnd",
        		iaa_Xfr_Audit_Iaxfr_Frm_Acct_Cd,"/Frm/Rvl/Mth",
        		iaa_Xfr_Audit_Iaxfr_Frm_Unit_Typ,"//To/fnd",
        		iaa_Xfr_Audit_Iaxfr_To_Acct_Cd,"/To/Rvl/Mth",
        		iaa_Xfr_Audit_Iaxfr_To_Unit_Typ,"///Asset-frm-amt",
        		iaa_Xfr_Audit_Iaxfr_From_Asset_Xfr_Amt,"///Asset-to-Amt",
        		iaa_Xfr_Audit_Iaxfr_To_Asset_Amt,"///Xfr fr Guar",
        		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Guar,"///Xfr fr Div",
        		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Divid,"///Xfr fr Units",
        		iaa_Xfr_Audit_Iaxfr_From_Rqstd_Xfr_Units,"///Xfr To Guar",
        		iaa_Xfr_Audit_Iaxfr_To_Xfr_Guar,"///Xfr To Div",
        		iaa_Xfr_Audit_Iaxfr_To_Xfr_Divid,"///Xfr To Units",
        		iaa_Xfr_Audit_Iaxfr_To_Xfr_Units,"///Bfr to Gr",
        		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Guar,"///Bfr to Dv",
        		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Divid,"///Bfr to Unts",
        		iaa_Xfr_Audit_Iaxfr_To_Bfr_Xfr_Units,"///Bfr fr Gr",
        		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Guar,"///Bfr fr Dv",
        		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Divid,"///Bfr fr Unts",
        		iaa_Xfr_Audit_Iaxfr_From_Current_Pmt_Units);
    }
}
