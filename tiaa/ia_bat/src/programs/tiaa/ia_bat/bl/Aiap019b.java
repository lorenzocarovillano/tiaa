/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:14 PM
**        * FROM NATURAL PROGRAM : Aiap019b
************************************************************
**        * FILE NAME            : Aiap019b.java
**        * CLASS NAME           : Aiap019b
**        * INSTANCE NAME        : Aiap019b
************************************************************
*
* AIAP019B - TIAA/CREF IA TRANSFER SUMMARY
*  VERSION 1.0 - 10/11/99
* UPDATED FOR RATE EXPANSION - 03/15/05 L.W.
* 07/10/08  KCD  ROTH/99 RATES IMPLEMENTATION
*
* STARTING FROM 02/02/05, THE TRANSFER RECORD IN IAA-XFR-ACTRL-RCRD-VIEW
* IS WRITTEN ON NEW FORMAT WITH RATE EXPANSION.
* 10/15/10 O. SOTTO PROD FIX - CHECK FOR VALUE '00' IN RATE CODE.
*                   SC 101510.
* 10/18/10 O. SOTTO PROD FIX - PREVENT INDEX PROBLEM WITH CREF.
*                   SC 101810.
* 11/11/10 O. SOTTO PROD FIX - SC 111110.
* 02/22/11 O. SOTTO PROD FIX - SC 022211.
* 03/14/11 O. SOTTO PROD FIX - SC 031411.
* 09/14/11 O. YAFFEE PROD FIX - SC 091411.
* 10-08-12   DY  RBE PHASE 2 PROJECT; REF AS RBE2
* 04-29-13   DY  UPDATE TO ADD GRAND TOTAL OF UNITS ON "ALL" PAGE;
*                  CHANGE IS REFERENCED AS DY2
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap019b extends BLNatBase
{
    // Data Areas
    private LdaAial0131 ldaAial0131;
    private LdaAial0191 ldaAial0191;
    private LdaAial0192 ldaAial0192;
    private LdaAial019b ldaAial019b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pgm_Subscripts_Counters_Buckets;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Cpm_Key_Previous;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Cref_Fund_Counter;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Fund_Units;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Variable_Payments;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Graded;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Standard;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Transfer_Amount;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Graded;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Standard;
    private DbsField pgm_Subscripts_Counters_Buckets_Pgm_Grand_Total_Tiaa_Gtd_Pmts;
    private DbsField pgm_Tiaa_Array_Area;

    private DbsGroup pgm_Tiaa_Array_Area__R_Field_1;

    private DbsGroup pgm_Tiaa_Array_Area_Pgm_Tiaa_Array;
    private DbsField pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code;
    private DbsField pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment;
    private DbsField pgm_Tiaa_Array_Area_Pgm_Tiaa_Dividend_Payment;
    private DbsField pgm_Tiaa_Array_Area_Pgm_Tiaa_Transfer_Date;
    private DbsField pgm_Tiaa_Array_Area_Pgm_Tiaa_Filler;
    private DbsField pgm_Cref_Array_Area;

    private DbsGroup pgm_Cref_Array_Area__R_Field_2;

    private DbsGroup pgm_Cref_Array_Area_Pgm_Cref_Array;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Fund_Letter;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Units;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Auv;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Variable_Payment;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount;
    private DbsField pgm_Cref_Array_Area_Pgm_Cref_Filler;

    private DbsGroup dtl_Line_Table;

    private DbsGroup dtl_Line_Table_Dtl_Line;
    private DbsField dtl_Line_Table_Dtl_Contract_Number;
    private DbsField dtl_Line_Table_Dtl_Payee_Code;
    private DbsField dtl_Line_Table_Dtl_Mode;
    private DbsField dtl_Line_Table_Dtl_Option;

    private DbsGroup dtl_Line_Table__R_Field_3;
    private DbsField dtl_Line_Table_Dtl_Option_N;
    private DbsField dtl_Line_Table_Dtl_Graded_Standard_Ind;
    private DbsField dtl_Line_Table_Dtl_Rate_Code;

    private DbsGroup dtl_Line_Table__R_Field_4;
    private DbsField dtl_Line_Table_Dtl_Rate_Code_N;
    private DbsField dtl_Line_Table_Dtl_Guaranteed_Payment;
    private DbsField dtl_Line_Table_Dtl_Fund_Name;
    private DbsField dtl_Line_Table_Dtl_Cref_Reval_Method;
    private DbsField dtl_Line_Table_Dtl_Units;
    private DbsField dtl_Line_Table_Dtl_Variable_Payment;
    private DbsField dtl_Line_Table_Dtl_Transfer_Amount;
    private DbsField dtl_Line_Table_Dtl_Transfer_Effective_Date;

    private DbsGroup dtl_Line_Table__R_Field_5;
    private DbsField dtl_Line_Table_Dtl_Transfer_Effective_Date_N;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_6;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_Begin;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_End;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_2;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date;

    private DbsGroup pnd_Input_Record__R_Field_7;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date_A;

    private DbsGroup pnd_Input_Record__R_Field_8;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Year;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Month;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Day;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_3;
    private DbsField pnd_Input_Record_Pnd_Input_End_Date;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_4;
    private DbsField pnd_Total_Pmts_Out;
    private DbsField pnd_Total_Units_Out;
    private DbsField pnd_Pmts_Out;
    private DbsField pnd_Cntrl_Sd_Cde;
    private DbsField pnd_Date1;
    private DbsField pnd_Nonzero_Switch;
    private DbsField pnd_Prt_Msg;
    private DbsField pnd_Reval_Switch;

    private DbsGroup pnd_Eff_Date_Array;
    private DbsField pnd_Eff_Date_Array_Pnd_Eff_Date;
    private DbsField pnd_Save_Eff_Date;

    private DbsGroup pnd_Save_Eff_Date__R_Field_9;
    private DbsField pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Max_Seq_Number;
    private DbsField pnd_Date_Count;
    private DbsField pnd_Eof_Flag;
    private DbsField pnd_First_Return;
    private DbsField pnd_First_Sort;
    private DbsField pnd_Save_Transfer_Eff_Date;
    private DbsField pnd_Prt_Transfer_Eff_Date;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Revaluation_Ind_Out;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Print_Ppcn;

    private DbsGroup pnd_Print_Ppcn__R_Field_10;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_7;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Dash;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_8;
    private DbsField pnd_Print_Fund;
    private DbsField pnd_Print_Fund_Out;

    private DbsGroup pnd_Fund_Name_Array;
    private DbsField pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Stock_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Mma_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Social_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Bond_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Global_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Growth_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Index_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Ilb_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Rea_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Access_Name;

    private DbsGroup pnd_Fund_Name_Array__R_Field_11;

    private DbsGroup pnd_Fund_Name_Array_Pnd_Filler9;
    private DbsField pnd_Fund_Name_Array_Pnd_Fund_Name;

    private DbsGroup pnd_Fund_Name_Array__R_Field_12;
    private DbsField pnd_Fund_Name_Array_Pnd_Fund_Name_1_6;
    private DbsField pnd_Fund_Name_Array_Pnd_Filler10;

    private DbsGroup pnd_Fund_Letter_Array;
    private DbsField pnd_Fund_Letter_Array_Pnd_Std_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Grd_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Stock_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Mma_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Social_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Bond_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Global_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Growth_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Index_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Ilb_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Rea_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Access_Letter;

    private DbsGroup pnd_Fund_Letter_Array__R_Field_13;

    private DbsGroup pnd_Fund_Letter_Array_Pnd_Filler;
    private DbsField pnd_Fund_Letter_Array_Pnd_Fund_Letter;
    private DbsField pnd_Fund_Input_Pos;
    private DbsField pnd_Fund_Output_Pos;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A;

    private DbsGroup pnd_Transfer_Tiaa_By_Method;
    private DbsField pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa;
    private DbsField pnd_Net_Units;
    private DbsField pnd_Net_Pmts;
    private DbsField pnd_Prt_Switch;
    private DbsField pnd_Num_Lines;
    private DbsField pnd_Max_Detail;
    private DbsField pnd_Max_Cref;
    private DbsField pnd_Max_Cref2;
    private DbsField pnd_Tiaa_Pos;
    private DbsField pnd_Cref_Rea_Pos;
    private DbsField pnd_Pmt_Index;
    private DbsField pnd_Total_Num_Transfers;
    private DbsField pnd_Grand_Total_Num_Transfers;
    private DbsField pnd_First_Write_Switch;
    private DbsField pnd_Grand_Total_Switch;
    private DbsField pnd_Net_Trans_Reval;
    private DbsField pnd_Tot_Guar_Pmt_Std;
    private DbsField pnd_Grand_Tot_Guar_Pmt_Std;
    private DbsField pnd_Tot_Guar_Pmt_Grd;
    private DbsField pnd_Grand_Tot_Guar_Pmt_Grd;
    private DbsField pnd_Tot_Guar_Pmt;
    private DbsField pnd_Grand_Tot_Guar_Pmt;

    private DbsGroup pnd_Tot_Cref_Arrays;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts;
    private DbsField pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets;
    private DbsField pnd_Line_Count;
    private DbsField pnd_Valid_Count;
    private DbsField pnd_Prt_Reval;
    private DbsField pnd_Grd_Std_Ind;
    private DbsField pnd_Tot_Var_Pmt;
    private DbsField pnd_Tot_Trans_Amt;
    private DbsField pnd_Grand_Tot_Trans_Amt;
    private DbsField pnd_Tot_Trans_Amt_Std;
    private DbsField pnd_Grand_Tot_Trans_Amt_Std;
    private DbsField pnd_Tot_Trans_Amt_Grd;
    private DbsField pnd_Grand_Tot_Trans_Amt_Grd;
    private DbsField pnd_Total_Cref_Pmts;
    private DbsField pnd_Grand_Total_Cref_Pmts;
    private DbsField pnd_Total_Cref_Assets;
    private DbsField pnd_Grand_Total_Cref_Assets;
    private DbsField pnd_Grand_Total_Cref_Units;
    private DbsField pnd_Save_Dtl_Grd_Std_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        ldaAial0191 = new LdaAial0191();
        registerRecord(ldaAial0191);
        ldaAial0192 = new LdaAial0192();
        registerRecord(ldaAial0192);
        ldaAial019b = new LdaAial019b();
        registerRecord(ldaAial019b);

        // Local Variables
        localVariables = new DbsRecord();

        pgm_Subscripts_Counters_Buckets = localVariables.newGroupInRecord("pgm_Subscripts_Counters_Buckets", "PGM-SUBSCRIPTS-COUNTERS-BUCKETS");
        pgm_Subscripts_Counters_Buckets_Pgm_Cpm_Key_Previous = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Cpm_Key_Previous", 
            "PGM-CPM-KEY-PREVIOUS", FieldType.STRING, 11);
        pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub", 
            "PGM-DETAIL-LINE-SUB", FieldType.NUMERIC, 3);
        pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max", 
            "PGM-DETAIL-LINE-SUB-MAX", FieldType.NUMERIC, 3);
        pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub", 
            "PGM-TIAA-ARRAY-SUB", FieldType.NUMERIC, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub", 
            "PGM-CREF-ARRAY-SUB", FieldType.NUMERIC, 1);
        pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator", 
            "PGM-TIAA-ARRAY-DESIGNATOR", FieldType.NUMERIC, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator", 
            "PGM-CREF-ARRAY-DESIGNATOR", FieldType.NUMERIC, 1);
        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Fund_Counter = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Cref_Fund_Counter", 
            "PGM-CREF-FUND-COUNTER", FieldType.NUMERIC, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number", 
            "PGM-CURRENT-RECORD-NUMBER", FieldType.NUMERIC, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Fund_Units = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Fund_Units", 
            "PGM-TOTAL-CREF-FUND-UNITS", FieldType.NUMERIC, 10, 3);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Variable_Payments = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Variable_Payments", 
            "PGM-TOTAL-CREF-VARIABLE-PAYMENTS", FieldType.NUMERIC, 13, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Graded = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Graded", 
            "PGM-TOTAL-TIAA-ASSETS-GRADED", FieldType.NUMERIC, 14, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Standard = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Standard", 
            "PGM-TOTAL-TIAA-ASSETS-STANDARD", FieldType.NUMERIC, 14, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Transfer_Amount = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Transfer_Amount", 
            "PGM-TOTAL-CREF-TRANSFER-AMOUNT", FieldType.NUMERIC, 13, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Graded = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Graded", 
            "PGM-TOTAL-TIAA-GTD-PMTS-GRADED", FieldType.NUMERIC, 13, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Standard = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Standard", 
            "PGM-TOTAL-TIAA-GTD-PMTS-STANDARD", FieldType.NUMERIC, 13, 2);
        pgm_Subscripts_Counters_Buckets_Pgm_Grand_Total_Tiaa_Gtd_Pmts = pgm_Subscripts_Counters_Buckets.newFieldInGroup("pgm_Subscripts_Counters_Buckets_Pgm_Grand_Total_Tiaa_Gtd_Pmts", 
            "PGM-GRAND-TOTAL-TIAA-GTD-PMTS", FieldType.NUMERIC, 13, 2);
        pgm_Tiaa_Array_Area = localVariables.newFieldInRecord("pgm_Tiaa_Array_Area", "PGM-TIAA-ARRAY-AREA", FieldType.STRING, 250);

        pgm_Tiaa_Array_Area__R_Field_1 = localVariables.newGroupInRecord("pgm_Tiaa_Array_Area__R_Field_1", "REDEFINE", pgm_Tiaa_Array_Area);

        pgm_Tiaa_Array_Area_Pgm_Tiaa_Array = pgm_Tiaa_Array_Area__R_Field_1.newGroupArrayInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Array", "PGM-TIAA-ARRAY", 
            new DbsArrayController(1, 10));
        pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code = pgm_Tiaa_Array_Area_Pgm_Tiaa_Array.newFieldInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code", "PGM-TIAA-RATE-CODE", 
            FieldType.STRING, 2);
        pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment = pgm_Tiaa_Array_Area_Pgm_Tiaa_Array.newFieldInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment", 
            "PGM-TIAA-GUARANTEED-PAYMENT", FieldType.NUMERIC, 9, 2);
        pgm_Tiaa_Array_Area_Pgm_Tiaa_Dividend_Payment = pgm_Tiaa_Array_Area_Pgm_Tiaa_Array.newFieldInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Dividend_Payment", 
            "PGM-TIAA-DIVIDEND-PAYMENT", FieldType.NUMERIC, 9, 2);
        pgm_Tiaa_Array_Area_Pgm_Tiaa_Transfer_Date = pgm_Tiaa_Array_Area_Pgm_Tiaa_Array.newFieldInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Transfer_Date", 
            "PGM-TIAA-TRANSFER-DATE", FieldType.DATE);
        pgm_Tiaa_Array_Area_Pgm_Tiaa_Filler = pgm_Tiaa_Array_Area__R_Field_1.newFieldInGroup("pgm_Tiaa_Array_Area_Pgm_Tiaa_Filler", "PGM-TIAA-FILLER", 
            FieldType.STRING, 10);
        pgm_Cref_Array_Area = localVariables.newFieldInRecord("pgm_Cref_Array_Area", "PGM-CREF-ARRAY-AREA", FieldType.STRING, 250);

        pgm_Cref_Array_Area__R_Field_2 = localVariables.newGroupInRecord("pgm_Cref_Array_Area__R_Field_2", "REDEFINE", pgm_Cref_Array_Area);

        pgm_Cref_Array_Area_Pgm_Cref_Array = pgm_Cref_Array_Area__R_Field_2.newGroupArrayInGroup("pgm_Cref_Array_Area_Pgm_Cref_Array", "PGM-CREF-ARRAY", 
            new DbsArrayController(1, 5));
        pgm_Cref_Array_Area_Pgm_Cref_Fund_Letter = pgm_Cref_Array_Area_Pgm_Cref_Array.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Fund_Letter", "PGM-CREF-FUND-LETTER", 
            FieldType.STRING, 1);
        pgm_Cref_Array_Area_Pgm_Cref_Units = pgm_Cref_Array_Area_Pgm_Cref_Array.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Units", "PGM-CREF-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pgm_Cref_Array_Area_Pgm_Cref_Auv = pgm_Cref_Array_Area_Pgm_Cref_Array.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Auv", "PGM-CREF-AUV", FieldType.NUMERIC, 
            8, 4);
        pgm_Cref_Array_Area_Pgm_Cref_Variable_Payment = pgm_Cref_Array_Area_Pgm_Cref_Array.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Variable_Payment", 
            "PGM-CREF-VARIABLE-PAYMENT", FieldType.NUMERIC, 11, 2);
        pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount = pgm_Cref_Array_Area_Pgm_Cref_Array.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount", 
            "PGM-CREF-TRANSFER-AMOUNT", FieldType.NUMERIC, 14, 2);
        pgm_Cref_Array_Area_Pgm_Cref_Filler = pgm_Cref_Array_Area__R_Field_2.newFieldInGroup("pgm_Cref_Array_Area_Pgm_Cref_Filler", "PGM-CREF-FILLER", 
            FieldType.STRING, 35);

        dtl_Line_Table = localVariables.newGroupArrayInRecord("dtl_Line_Table", "DTL-LINE-TABLE", new DbsArrayController(1, 251));

        dtl_Line_Table_Dtl_Line = dtl_Line_Table.newGroupInGroup("dtl_Line_Table_Dtl_Line", "DTL-LINE");
        dtl_Line_Table_Dtl_Contract_Number = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Contract_Number", "DTL-CONTRACT-NUMBER", FieldType.STRING, 
            8);
        dtl_Line_Table_Dtl_Payee_Code = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Payee_Code", "DTL-PAYEE-CODE", FieldType.STRING, 2);
        dtl_Line_Table_Dtl_Mode = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Mode", "DTL-MODE", FieldType.STRING, 3);
        dtl_Line_Table_Dtl_Option = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Option", "DTL-OPTION", FieldType.STRING, 2);

        dtl_Line_Table__R_Field_3 = dtl_Line_Table_Dtl_Line.newGroupInGroup("dtl_Line_Table__R_Field_3", "REDEFINE", dtl_Line_Table_Dtl_Option);
        dtl_Line_Table_Dtl_Option_N = dtl_Line_Table__R_Field_3.newFieldInGroup("dtl_Line_Table_Dtl_Option_N", "DTL-OPTION-N", FieldType.NUMERIC, 2);
        dtl_Line_Table_Dtl_Graded_Standard_Ind = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Graded_Standard_Ind", "DTL-GRADED-STANDARD-IND", 
            FieldType.STRING, 1);
        dtl_Line_Table_Dtl_Rate_Code = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Rate_Code", "DTL-RATE-CODE", FieldType.STRING, 2);

        dtl_Line_Table__R_Field_4 = dtl_Line_Table_Dtl_Line.newGroupInGroup("dtl_Line_Table__R_Field_4", "REDEFINE", dtl_Line_Table_Dtl_Rate_Code);
        dtl_Line_Table_Dtl_Rate_Code_N = dtl_Line_Table__R_Field_4.newFieldInGroup("dtl_Line_Table_Dtl_Rate_Code_N", "DTL-RATE-CODE-N", FieldType.NUMERIC, 
            2);
        dtl_Line_Table_Dtl_Guaranteed_Payment = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Guaranteed_Payment", "DTL-GUARANTEED-PAYMENT", 
            FieldType.NUMERIC, 9, 2);
        dtl_Line_Table_Dtl_Fund_Name = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Fund_Name", "DTL-FUND-NAME", FieldType.STRING, 6);
        dtl_Line_Table_Dtl_Cref_Reval_Method = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Cref_Reval_Method", "DTL-CREF-REVAL-METHOD", 
            FieldType.STRING, 1);
        dtl_Line_Table_Dtl_Units = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Units", "DTL-UNITS", FieldType.NUMERIC, 9, 3);
        dtl_Line_Table_Dtl_Variable_Payment = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Variable_Payment", "DTL-VARIABLE-PAYMENT", FieldType.NUMERIC, 
            11, 2);
        dtl_Line_Table_Dtl_Transfer_Amount = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Transfer_Amount", "DTL-TRANSFER-AMOUNT", FieldType.NUMERIC, 
            13, 2);
        dtl_Line_Table_Dtl_Transfer_Effective_Date = dtl_Line_Table_Dtl_Line.newFieldInGroup("dtl_Line_Table_Dtl_Transfer_Effective_Date", "DTL-TRANSFER-EFFECTIVE-DATE", 
            FieldType.STRING, 8);

        dtl_Line_Table__R_Field_5 = dtl_Line_Table_Dtl_Line.newGroupInGroup("dtl_Line_Table__R_Field_5", "REDEFINE", dtl_Line_Table_Dtl_Transfer_Effective_Date);
        dtl_Line_Table_Dtl_Transfer_Effective_Date_N = dtl_Line_Table__R_Field_5.newFieldInGroup("dtl_Line_Table_Dtl_Transfer_Effective_Date_N", "DTL-TRANSFER-EFFECTIVE-DATE-N", 
            FieldType.NUMERIC, 8);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_cntrl);

        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 80);

        pnd_Input_Record__R_Field_6 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_6", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Input_Contract_Payee_Begin = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_Begin", 
            "#INPUT-CONTRACT-PAYEE-BEGIN", FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_1 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_1", "#INPUT-FILLER-1", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Contract_Payee_End = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_End", "#INPUT-CONTRACT-PAYEE-END", 
            FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_2 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_2", "#INPUT-FILLER-2", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Begin_Date = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date", "#INPUT-BEGIN-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Input_Record__R_Field_7 = pnd_Input_Record__R_Field_6.newGroupInGroup("pnd_Input_Record__R_Field_7", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Date_A = pnd_Input_Record__R_Field_7.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date_A", "#INPUT-BEGIN-DATE-A", 
            FieldType.STRING, 8);

        pnd_Input_Record__R_Field_8 = pnd_Input_Record__R_Field_6.newGroupInGroup("pnd_Input_Record__R_Field_8", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Year = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Year", "#INPUT-BEGIN-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Input_Record_Pnd_Input_Begin_Month = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Month", "#INPUT-BEGIN-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Begin_Day = pnd_Input_Record__R_Field_8.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Day", "#INPUT-BEGIN-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Filler_3 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_3", "#INPUT-FILLER-3", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_End_Date = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_End_Date", "#INPUT-END-DATE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Input_Filler_4 = pnd_Input_Record__R_Field_6.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_4", "#INPUT-FILLER-4", FieldType.STRING, 
            33);
        pnd_Total_Pmts_Out = localVariables.newFieldInRecord("pnd_Total_Pmts_Out", "#TOTAL-PMTS-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Total_Units_Out = localVariables.newFieldInRecord("pnd_Total_Units_Out", "#TOTAL-UNITS-OUT", FieldType.NUMERIC, 9, 3);
        pnd_Pmts_Out = localVariables.newFieldInRecord("pnd_Pmts_Out", "#PMTS-OUT", FieldType.NUMERIC, 9, 2);
        pnd_Cntrl_Sd_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Sd_Cde", "#CNTRL-SD-CDE", FieldType.STRING, 2);
        pnd_Date1 = localVariables.newFieldInRecord("pnd_Date1", "#DATE1", FieldType.STRING, 10);
        pnd_Nonzero_Switch = localVariables.newFieldInRecord("pnd_Nonzero_Switch", "#NONZERO-SWITCH", FieldType.STRING, 1);
        pnd_Prt_Msg = localVariables.newFieldInRecord("pnd_Prt_Msg", "#PRT-MSG", FieldType.STRING, 19);
        pnd_Reval_Switch = localVariables.newFieldInRecord("pnd_Reval_Switch", "#REVAL-SWITCH", FieldType.STRING, 2);

        pnd_Eff_Date_Array = localVariables.newGroupArrayInRecord("pnd_Eff_Date_Array", "#EFF-DATE-ARRAY", new DbsArrayController(1, 5));
        pnd_Eff_Date_Array_Pnd_Eff_Date = pnd_Eff_Date_Array.newFieldInGroup("pnd_Eff_Date_Array_Pnd_Eff_Date", "#EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Save_Eff_Date = localVariables.newFieldInRecord("pnd_Save_Eff_Date", "#SAVE-EFF-DATE", FieldType.NUMERIC, 8);

        pnd_Save_Eff_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Save_Eff_Date__R_Field_9", "REDEFINE", pnd_Save_Eff_Date);
        pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X = pnd_Save_Eff_Date__R_Field_9.newFieldInGroup("pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X", "#SAVE-EFF-DATE-X", 
            FieldType.STRING, 8);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 3);
        pnd_Max_Seq_Number = localVariables.newFieldInRecord("pnd_Max_Seq_Number", "#MAX-SEQ-NUMBER", FieldType.NUMERIC, 1);
        pnd_Date_Count = localVariables.newFieldInRecord("pnd_Date_Count", "#DATE-COUNT", FieldType.NUMERIC, 3);
        pnd_Eof_Flag = localVariables.newFieldInRecord("pnd_Eof_Flag", "#EOF-FLAG", FieldType.STRING, 1);
        pnd_First_Return = localVariables.newFieldInRecord("pnd_First_Return", "#FIRST-RETURN", FieldType.STRING, 1);
        pnd_First_Sort = localVariables.newFieldInRecord("pnd_First_Sort", "#FIRST-SORT", FieldType.NUMERIC, 1);
        pnd_Save_Transfer_Eff_Date = localVariables.newFieldInRecord("pnd_Save_Transfer_Eff_Date", "#SAVE-TRANSFER-EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Prt_Transfer_Eff_Date = localVariables.newFieldInRecord("pnd_Prt_Transfer_Eff_Date", "#PRT-TRANSFER-EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.NUMERIC, 8);
        pnd_Revaluation_Ind_Out = localVariables.newFieldInRecord("pnd_Revaluation_Ind_Out", "#REVALUATION-IND-OUT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Print_Ppcn = localVariables.newFieldInRecord("pnd_Print_Ppcn", "#PRINT-PPCN", FieldType.STRING, 9);

        pnd_Print_Ppcn__R_Field_10 = localVariables.newGroupInRecord("pnd_Print_Ppcn__R_Field_10", "REDEFINE", pnd_Print_Ppcn);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_7 = pnd_Print_Ppcn__R_Field_10.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_7", "#PRINT-PPCN-7", FieldType.STRING, 
            7);
        pnd_Print_Ppcn_Pnd_Print_Dash = pnd_Print_Ppcn__R_Field_10.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Dash", "#PRINT-DASH", FieldType.STRING, 1);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_8 = pnd_Print_Ppcn__R_Field_10.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_8", "#PRINT-PPCN-8", FieldType.STRING, 
            1);
        pnd_Print_Fund = localVariables.newFieldInRecord("pnd_Print_Fund", "#PRINT-FUND", FieldType.STRING, 7);
        pnd_Print_Fund_Out = localVariables.newFieldInRecord("pnd_Print_Fund_Out", "#PRINT-FUND-OUT", FieldType.STRING, 7);

        pnd_Fund_Name_Array = localVariables.newGroupInRecord("pnd_Fund_Name_Array", "#FUND-NAME-ARRAY");
        pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name", "#TIAA-STD-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name", "#TIAA-GTD-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Stock_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Stock_Name", "#STOCK-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Mma_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Mma_Name", "#MMA-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Social_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Social_Name", "#SOCIAL-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Bond_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Bond_Name", "#BOND-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Global_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Global_Name", "#GLOBAL-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Growth_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Growth_Name", "#GROWTH-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Index_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Index_Name", "#INDEX-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Ilb_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Ilb_Name", "#ILB-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Rea_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Rea_Name", "#REA-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Access_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Access_Name", "#ACCESS-NAME", FieldType.STRING, 
            8);

        pnd_Fund_Name_Array__R_Field_11 = localVariables.newGroupInRecord("pnd_Fund_Name_Array__R_Field_11", "REDEFINE", pnd_Fund_Name_Array);

        pnd_Fund_Name_Array_Pnd_Filler9 = pnd_Fund_Name_Array__R_Field_11.newGroupArrayInGroup("pnd_Fund_Name_Array_Pnd_Filler9", "#FILLER9", new DbsArrayController(1, 
            12));
        pnd_Fund_Name_Array_Pnd_Fund_Name = pnd_Fund_Name_Array_Pnd_Filler9.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Fund_Name", "#FUND-NAME", FieldType.STRING, 
            8);

        pnd_Fund_Name_Array__R_Field_12 = pnd_Fund_Name_Array_Pnd_Filler9.newGroupInGroup("pnd_Fund_Name_Array__R_Field_12", "REDEFINE", pnd_Fund_Name_Array_Pnd_Fund_Name);
        pnd_Fund_Name_Array_Pnd_Fund_Name_1_6 = pnd_Fund_Name_Array__R_Field_12.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Fund_Name_1_6", "#FUND-NAME-1-6", 
            FieldType.STRING, 6);
        pnd_Fund_Name_Array_Pnd_Filler10 = pnd_Fund_Name_Array__R_Field_12.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Filler10", "#FILLER10", FieldType.STRING, 
            2);

        pnd_Fund_Letter_Array = localVariables.newGroupInRecord("pnd_Fund_Letter_Array", "#FUND-LETTER-ARRAY");
        pnd_Fund_Letter_Array_Pnd_Std_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Std_Letter", "#STD-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Grd_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Grd_Letter", "#GRD-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Stock_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Stock_Letter", "#STOCK-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Mma_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Mma_Letter", "#MMA-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Social_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Social_Letter", "#SOCIAL-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Bond_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Bond_Letter", "#BOND-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Global_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Global_Letter", "#GLOBAL-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Growth_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Growth_Letter", "#GROWTH-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Index_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Index_Letter", "#INDEX-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Ilb_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Ilb_Letter", "#ILB-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Rea_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Rea_Letter", "#REA-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Access_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Access_Letter", "#ACCESS-LETTER", FieldType.STRING, 
            1);

        pnd_Fund_Letter_Array__R_Field_13 = localVariables.newGroupInRecord("pnd_Fund_Letter_Array__R_Field_13", "REDEFINE", pnd_Fund_Letter_Array);

        pnd_Fund_Letter_Array_Pnd_Filler = pnd_Fund_Letter_Array__R_Field_13.newGroupArrayInGroup("pnd_Fund_Letter_Array_Pnd_Filler", "#FILLER", new DbsArrayController(1, 
            12));
        pnd_Fund_Letter_Array_Pnd_Fund_Letter = pnd_Fund_Letter_Array_Pnd_Filler.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Fund_Letter", "#FUND-LETTER", 
            FieldType.STRING, 1);
        pnd_Fund_Input_Pos = localVariables.newFieldInRecord("pnd_Fund_Input_Pos", "#FUND-INPUT-POS", FieldType.NUMERIC, 2);
        pnd_Fund_Output_Pos = localVariables.newFieldInRecord("pnd_Fund_Output_Pos", "#FUND-OUTPUT-POS", FieldType.NUMERIC, 2);

        pnd_Transfer_Amt_By_Fund_In_1 = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_1", "#TRANSFER-AMT-BY-FUND-IN-1", new DbsArrayController(1, 
            11));
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M", 
            "#UNITS-OUTGOING-M", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A", 
            "#UNITS-OUTGOING-A", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1 = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1", 
            "#UNITS-OUTGOING-M-1", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1 = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1", 
            "#UNITS-OUTGOING-A-1", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M", 
            "#PMTS-OUTGOING-M", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A", 
            "#PMTS-OUTGOING-A", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming", 
            "#GTD-INCOMING", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming", 
            "#DVD-INCOMING", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A", 
            "#UNITS-INCOMING-A", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M", 
            "#UNITS-INCOMING-M", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M", 
            "#PMTS-INCOMING-M", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A", 
            "#PMTS-INCOMING-A", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers", 
            "#TOTAL-NUM-FROM-TRANSFERS", FieldType.NUMERIC, 6);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M", 
            "#NET-TRANSFER-M", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A", 
            "#NET-TRANSFER-A", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A", 
            "#NET-REVAL-A", FieldType.NUMERIC, 11, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M", 
            "#NET-REVAL-M", FieldType.NUMERIC, 11, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_1 = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_1", "#TOT-TRANS-AMT-BY-FUND-IN-1", new DbsArrayController(1, 
            11));
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M", 
            "#TOT-UNITS-OUTGOING-M", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1 = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1", 
            "#TOT-UNITS-OUTGOING-M-1", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A", 
            "#TOT-UNITS-OUTGOING-A", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1 = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1", 
            "#TOT-UNITS-OUTGOING-A-1", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M", 
            "#TOT-PMTS-OUTGOING-M", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A", 
            "#TOT-PMTS-OUTGOING-A", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming", 
            "#TOT-GTD-INCOMING", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming", 
            "#TOT-DVD-INCOMING", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A", 
            "#TOT-UNITS-INCOMING-A", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M", 
            "#TOT-UNITS-INCOMING-M", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M", 
            "#TOT-PMTS-INCOMING-M", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A", 
            "#TOT-PMTS-INCOMING-A", FieldType.NUMERIC, 11, 2);

        pnd_Transfer_Tiaa_By_Method = localVariables.newGroupArrayInRecord("pnd_Transfer_Tiaa_By_Method", "#TRANSFER-TIAA-BY-METHOD", new DbsArrayController(1, 
            2));
        pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa = pnd_Transfer_Tiaa_By_Method.newFieldInGroup("pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa", 
            "#NET-TRANSFER-TIAA", FieldType.NUMERIC, 11, 2);
        pnd_Net_Units = localVariables.newFieldInRecord("pnd_Net_Units", "#NET-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Net_Pmts = localVariables.newFieldInRecord("pnd_Net_Pmts", "#NET-PMTS", FieldType.NUMERIC, 11, 2);
        pnd_Prt_Switch = localVariables.newFieldInRecord("pnd_Prt_Switch", "#PRT-SWITCH", FieldType.STRING, 1);
        pnd_Num_Lines = localVariables.newFieldInRecord("pnd_Num_Lines", "#NUM-LINES", FieldType.NUMERIC, 2);
        pnd_Max_Detail = localVariables.newFieldInRecord("pnd_Max_Detail", "#MAX-DETAIL", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Cref = localVariables.newFieldInRecord("pnd_Max_Cref", "#MAX-CREF", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Cref2 = localVariables.newFieldInRecord("pnd_Max_Cref2", "#MAX-CREF2", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Pos = localVariables.newFieldInRecord("pnd_Tiaa_Pos", "#TIAA-POS", FieldType.NUMERIC, 1);
        pnd_Cref_Rea_Pos = localVariables.newFieldInRecord("pnd_Cref_Rea_Pos", "#CREF-REA-POS", FieldType.NUMERIC, 1);
        pnd_Pmt_Index = localVariables.newFieldInRecord("pnd_Pmt_Index", "#PMT-INDEX", FieldType.NUMERIC, 2);
        pnd_Total_Num_Transfers = localVariables.newFieldInRecord("pnd_Total_Num_Transfers", "#TOTAL-NUM-TRANSFERS", FieldType.NUMERIC, 6);
        pnd_Grand_Total_Num_Transfers = localVariables.newFieldInRecord("pnd_Grand_Total_Num_Transfers", "#GRAND-TOTAL-NUM-TRANSFERS", FieldType.NUMERIC, 
            6);
        pnd_First_Write_Switch = localVariables.newFieldInRecord("pnd_First_Write_Switch", "#FIRST-WRITE-SWITCH", FieldType.STRING, 1);
        pnd_Grand_Total_Switch = localVariables.newFieldInRecord("pnd_Grand_Total_Switch", "#GRAND-TOTAL-SWITCH", FieldType.STRING, 1);
        pnd_Net_Trans_Reval = localVariables.newFieldInRecord("pnd_Net_Trans_Reval", "#NET-TRANS-REVAL", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Guar_Pmt_Std = localVariables.newFieldInRecord("pnd_Tot_Guar_Pmt_Std", "#TOT-GUAR-PMT-STD", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Tot_Guar_Pmt_Std = localVariables.newFieldInRecord("pnd_Grand_Tot_Guar_Pmt_Std", "#GRAND-TOT-GUAR-PMT-STD", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Guar_Pmt_Grd = localVariables.newFieldInRecord("pnd_Tot_Guar_Pmt_Grd", "#TOT-GUAR-PMT-GRD", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Tot_Guar_Pmt_Grd = localVariables.newFieldInRecord("pnd_Grand_Tot_Guar_Pmt_Grd", "#GRAND-TOT-GUAR-PMT-GRD", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Guar_Pmt = localVariables.newFieldInRecord("pnd_Tot_Guar_Pmt", "#TOT-GUAR-PMT", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Tot_Guar_Pmt = localVariables.newFieldInRecord("pnd_Grand_Tot_Guar_Pmt", "#GRAND-TOT-GUAR-PMT", FieldType.NUMERIC, 11, 2);

        pnd_Tot_Cref_Arrays = localVariables.newGroupArrayInRecord("pnd_Tot_Cref_Arrays", "#TOT-CREF-ARRAYS", new DbsArrayController(1, 2, 3, 11));
        pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units", "#TOT-CREF-UNITS", FieldType.NUMERIC, 
            9, 3);
        pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts", "#TOT-CREF-PMTS", FieldType.NUMERIC, 
            11, 2);
        pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets", "#TOT-CREF-ASSETS", FieldType.NUMERIC, 
            13, 2);
        pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units", "#GRAND-TOT-CREF-UNITS", 
            FieldType.NUMERIC, 9, 3);
        pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts", "#GRAND-TOT-CREF-PMTS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets = pnd_Tot_Cref_Arrays.newFieldInGroup("pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets", "#GRAND-TOT-CREF-ASSETS", 
            FieldType.NUMERIC, 13, 2);
        pnd_Line_Count = localVariables.newFieldInRecord("pnd_Line_Count", "#LINE-COUNT", FieldType.NUMERIC, 2);
        pnd_Valid_Count = localVariables.newFieldInRecord("pnd_Valid_Count", "#VALID-COUNT", FieldType.NUMERIC, 3);
        pnd_Prt_Reval = localVariables.newFieldInRecord("pnd_Prt_Reval", "#PRT-REVAL", FieldType.STRING, 7);
        pnd_Grd_Std_Ind = localVariables.newFieldInRecord("pnd_Grd_Std_Ind", "#GRD-STD-IND", FieldType.STRING, 1);
        pnd_Tot_Var_Pmt = localVariables.newFieldInRecord("pnd_Tot_Var_Pmt", "#TOT-VAR-PMT", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt", "#TOT-TRANS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Grand_Tot_Trans_Amt", "#GRAND-TOT-TRANS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_Std = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt_Std", "#TOT-TRANS-AMT-STD", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Trans_Amt_Std = localVariables.newFieldInRecord("pnd_Grand_Tot_Trans_Amt_Std", "#GRAND-TOT-TRANS-AMT-STD", FieldType.NUMERIC, 13, 
            2);
        pnd_Tot_Trans_Amt_Grd = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt_Grd", "#TOT-TRANS-AMT-GRD", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Trans_Amt_Grd = localVariables.newFieldInRecord("pnd_Grand_Tot_Trans_Amt_Grd", "#GRAND-TOT-TRANS-AMT-GRD", FieldType.NUMERIC, 13, 
            2);
        pnd_Total_Cref_Pmts = localVariables.newFieldInRecord("pnd_Total_Cref_Pmts", "#TOTAL-CREF-PMTS", FieldType.NUMERIC, 11, 2);
        pnd_Grand_Total_Cref_Pmts = localVariables.newFieldInRecord("pnd_Grand_Total_Cref_Pmts", "#GRAND-TOTAL-CREF-PMTS", FieldType.NUMERIC, 11, 2);
        pnd_Total_Cref_Assets = localVariables.newFieldInRecord("pnd_Total_Cref_Assets", "#TOTAL-CREF-ASSETS", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Total_Cref_Assets = localVariables.newFieldInRecord("pnd_Grand_Total_Cref_Assets", "#GRAND-TOTAL-CREF-ASSETS", FieldType.NUMERIC, 13, 
            2);
        pnd_Grand_Total_Cref_Units = localVariables.newFieldInRecord("pnd_Grand_Total_Cref_Units", "#GRAND-TOTAL-CREF-UNITS", FieldType.NUMERIC, 9, 3);
        pnd_Save_Dtl_Grd_Std_Ind = localVariables.newFieldInRecord("pnd_Save_Dtl_Grd_Std_Ind", "#SAVE-DTL-GRD-STD-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaAial0131.initializeValues();
        ldaAial0191.initializeValues();
        ldaAial0192.initializeValues();
        ldaAial019b.initializeValues();

        localVariables.reset();
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Fund_Units.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Variable_Payments.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Graded.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Standard.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Transfer_Amount.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Graded.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Standard.setInitialValue(0);
        pgm_Subscripts_Counters_Buckets_Pgm_Grand_Total_Tiaa_Gtd_Pmts.setInitialValue(0);
        pnd_Save_Eff_Date.setInitialValue(0);
        pnd_Max_Rates.setInitialValue(250);
        pnd_Max_Seq_Number.setInitialValue(0);
        pnd_Date_Count.setInitialValue(0);
        pnd_Eof_Flag.setInitialValue("N");
        pnd_First_Return.setInitialValue("Y");
        pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name.setInitialValue("TIAA STD");
        pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name.setInitialValue("TIAA GRD");
        pnd_Fund_Name_Array_Pnd_Stock_Name.setInitialValue("STOCK   ");
        pnd_Fund_Name_Array_Pnd_Mma_Name.setInitialValue("MMA     ");
        pnd_Fund_Name_Array_Pnd_Social_Name.setInitialValue("SOCIAL  ");
        pnd_Fund_Name_Array_Pnd_Bond_Name.setInitialValue("BOND    ");
        pnd_Fund_Name_Array_Pnd_Global_Name.setInitialValue("GLOBAL  ");
        pnd_Fund_Name_Array_Pnd_Growth_Name.setInitialValue("GROWTH  ");
        pnd_Fund_Name_Array_Pnd_Index_Name.setInitialValue("INDEX   ");
        pnd_Fund_Name_Array_Pnd_Ilb_Name.setInitialValue("ILB     ");
        pnd_Fund_Name_Array_Pnd_Rea_Name.setInitialValue("REA     ");
        pnd_Fund_Name_Array_Pnd_Access_Name.setInitialValue("T ACCESS");
        pnd_Fund_Letter_Array_Pnd_Std_Letter.setInitialValue("T");
        pnd_Fund_Letter_Array_Pnd_Grd_Letter.setInitialValue("2");
        pnd_Fund_Letter_Array_Pnd_Stock_Letter.setInitialValue("C");
        pnd_Fund_Letter_Array_Pnd_Mma_Letter.setInitialValue("M");
        pnd_Fund_Letter_Array_Pnd_Social_Letter.setInitialValue("S");
        pnd_Fund_Letter_Array_Pnd_Bond_Letter.setInitialValue("B");
        pnd_Fund_Letter_Array_Pnd_Global_Letter.setInitialValue("W");
        pnd_Fund_Letter_Array_Pnd_Growth_Letter.setInitialValue("L");
        pnd_Fund_Letter_Array_Pnd_Index_Letter.setInitialValue("E");
        pnd_Fund_Letter_Array_Pnd_Ilb_Letter.setInitialValue("I");
        pnd_Fund_Letter_Array_Pnd_Rea_Letter.setInitialValue("R");
        pnd_Fund_Letter_Array_Pnd_Access_Letter.setInitialValue("D");
        pnd_Num_Lines.setInitialValue(57);
        pnd_Max_Detail.setInitialValue(250);
        pnd_Max_Cref.setInitialValue(9);
        pnd_Max_Cref2.setInitialValue(20);
        pnd_Tiaa_Pos.setInitialValue(0);
        pnd_Cref_Rea_Pos.setInitialValue(0);
        pnd_Pmt_Index.setInitialValue(0);
        pnd_Total_Num_Transfers.setInitialValue(0);
        pnd_Grand_Total_Num_Transfers.setInitialValue(0);
        pnd_Grand_Total_Switch.setInitialValue("N");
        pnd_Line_Count.setInitialValue(0);
        pnd_Valid_Count.setInitialValue(0);
        pnd_Tot_Trans_Amt.setInitialValue(0);
        pnd_Grand_Tot_Trans_Amt.setInitialValue(0);
        pnd_Tot_Trans_Amt_Std.setInitialValue(0);
        pnd_Grand_Tot_Trans_Amt_Std.setInitialValue(0);
        pnd_Tot_Trans_Amt_Grd.setInitialValue(0);
        pnd_Grand_Tot_Trans_Amt_Grd.setInitialValue(0);
        pnd_Total_Cref_Pmts.setInitialValue(0);
        pnd_Grand_Total_Cref_Pmts.setInitialValue(0);
        pnd_Total_Cref_Assets.setInitialValue(0);
        pnd_Grand_Total_Cref_Assets.setInitialValue(0);
        pnd_Grand_Total_Cref_Units.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Aiap019b() throws Exception
    {
        super("Aiap019b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().write(0, "**** AIAP019B ****");                                                                                                                      //Natural: WRITE '**** AIAP019B ****'
        if (Global.isEscape()) return;
        //*  RBE2                                                                                                                                                         //Natural: FORMAT LS = 132 PS = 66 ES = ON
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 66 ES = ON
        pnd_Print_Ppcn_Pnd_Print_Dash.setValue("-");                                                                                                                      //Natural: MOVE '-' TO #PRINT-DASH
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 #SORTB-RECORD
        while (condition(getWorkFiles().read(2, ldaAial019b.getPnd_Sortb_Record())))
        {
            getSort().writeSortInData(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Contract_Payee(),      //Natural: END-ALL
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Payment_Method(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area1(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area1(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area2(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area3(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area4(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area5(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area6(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area7(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area8(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area9(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area10(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area11(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area12(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area13(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area14(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area15(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area16(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area17(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area18(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area19(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Record_Number(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte(), 
                ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  FOR RBE2 FIX
        //*  ADD #SB-ARRAY-AREA7 THRU #SB-ARRAY-AREA19 ON THE SORT CLAUSE
        //*  **************************************************************
        //*  RBE2
        //*  RBE2
        //*  RBE2
        getSort().sortData(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Contract_Payee(),                 //Natural: SORT BY #SBC1-TRANSFER-EFFECTIVE-DATE #SBC1-CONTRACT-PAYEE #SB-KEY-LOGC-TRANSACTION-KEY #SBC1-PAYMENT-METHOD #SBC1-CURRENT-RECORD-NUMBER USING #SB-COMMON-DATA-AREA1 #SB-COMMON-DATA-AREA2 #SB-ARRAY-AREA1 #SB-ARRAY-AREA2 #SB-ARRAY-AREA3 #SB-ARRAY-AREA4 #SB-ARRAY-AREA5 #SB-ARRAY-AREA6 #SB-ARRAY-AREA7 #SB-ARRAY-AREA8 #SB-ARRAY-AREA9 #SB-ARRAY-AREA10 #SB-ARRAY-AREA11 #SB-ARRAY-AREA12 #SB-ARRAY-AREA13 #SB-ARRAY-AREA14 #SB-ARRAY-AREA15 #SB-ARRAY-AREA16 #SB-ARRAY-AREA17 #SB-ARRAY-AREA18 #SB-ARRAY-AREA19 #SB-KEY-CNTRCT-PY #SB-KEY-RECORD-NUMBER #SB-KEY-PRCSS-DTE #SB-KEY-SEQUENCE-NUMBER
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Payment_Method(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number());
        SORT01:
        while (condition(getSort().readSortOutData(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Contract_Payee(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Logc_Transaction_Key(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Payment_Method(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area1(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Common_Data_Area2(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area1(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area2(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area3(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area4(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area5(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area6(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area7(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area8(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area9(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area10(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area11(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area12(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area13(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area14(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area15(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area16(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area17(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area18(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area19(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Cntrct_Py(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Record_Number(), ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte(), 
            ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number())))
        {
            //*  AT END OF DATA
            //*    MOVE 'Y' TO #EOF-FLAG
            //*  END-ENDDATA
            //*  WRITE WORK FILE 3 #SORTB-RECORD
            //*  RBE2                                                                                                                                                     //Natural: AT END OF DATA
            getWorkFiles().write(3, false, ldaAial019b.getPnd_Sortb_Record());                                                                                            //Natural: WRITE WORK FILE 3 #SORTB-RECORD
                                                                                                                                                                          //Natural: PERFORM AFTER-SORT
            sub_After_Sort();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAtEndOfData()))
        {
            //*  RBE2
            pnd_Eof_Flag.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #EOF-FLAG
            //*  RBE2
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS-BY-EFF-DATE
        sub_Write_Totals_By_Eff_Date();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
        sub_Write_Grand_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.setValue("ALL     ");                                                                                                       //Natural: MOVE 'ALL     ' TO #SAVE-EFF-DATE-X
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AFTER-SORT
        //*        ADD 1 TO PGM-CREF-FUND-COUNTER
        //*        FOR #I = 1 TO 11
        //*          TO DTL-GUARANTEED-PAYMENT (61)
        //*         FOR #J = 3 TO 10
        //*  WRITE '=' #MAX-DETAIL
        //*  FOR #I = 2 TO 60
        //*  FOR #I = 1 TO 60
        //*    FOR #J = 3 TO 10
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //*   FOR #I = 3 TO 10
        //*  'CREF GRAND TOTALS'                                14X
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS-BY-EFF-DATE
        //*  FOR #I = 3 TO 10
    }
    private void sub_After_Sort() throws Exception                                                                                                                        //Natural: AFTER-SORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pnd_Prt_Transfer_Eff_Date.setValue(pnd_Save_Eff_Date);                                                                                                            //Natural: MOVE #SAVE-EFF-DATE TO #PRT-TRANSFER-EFF-DATE
        //*  RECORD-NUMBER 00 MEANS TIAA STANDARD
        //*  RECORD-NUMBER 50 MEANS TIAA GRADED
        getReports().write(0, "============================");                                                                                                            //Natural: WRITE '============================'
        if (Global.isEscape()) return;
        getReports().write(0, ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Contract_Payee(),"; CURR RECORD NBR: ",ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number()); //Natural: WRITE #SBC1-CONTRACT-PAYEE '; CURR RECORD NBR: ' #SBC1-CURRENT-RECORD-NUMBER
        if (Global.isEscape()) return;
        if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().equals(0) || ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().equals(50))) //Natural: IF ( #SBC1-CURRENT-RECORD-NUMBER = 00 OR = 50 )
        {
            //*  RBE2 FIX BEGINS >>
            //*  FOR PGM-DETAIL-LINE-SUB 1 #MAX-DETAIL     /* OS - 111110
            //*    MOVE ' ' TO DTL-CONTRACT-NUMBER (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-PAYEE-CODE (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-MODE (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-OPTION (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-GRADED-STANDARD-IND (PGM-DETAIL-LINE-SUB)
            //* *  MOVE 0   TO DTL-RATE-CODE (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' '  TO DTL-RATE-CODE (PGM-DETAIL-LINE-SUB)
            //*    MOVE 0   TO DTL-GUARANTEED-PAYMENT (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-FUND-NAME (PGM-DETAIL-LINE-SUB)
            //*    MOVE ' ' TO DTL-CREF-REVAL-METHOD (PGM-DETAIL-LINE-SUB)
            //*    MOVE 0   TO DTL-UNITS (PGM-DETAIL-LINE-SUB)
            //*    MOVE 0   TO DTL-VARIABLE-PAYMENT (PGM-DETAIL-LINE-SUB)
            //*    MOVE 0   TO DTL-TRANSFER-AMOUNT (PGM-DETAIL-LINE-SUB)
            //*    MOVE 'GTDTOTAL' TO DTL-TRANSFER-EFFECTIVE-DATE (PGM-DETAIL-LINE-SUB)
            //*    ADD 1 TO PGM-DETAIL-LINE-SUB       /* 031411
            //*  END-FOR                              /* OS - 111110
            dtl_Line_Table_Dtl_Contract_Number.getValue("*").reset();                                                                                                     //Natural: RESET DTL-CONTRACT-NUMBER ( * )
            dtl_Line_Table_Dtl_Payee_Code.getValue("*").reset();                                                                                                          //Natural: RESET DTL-PAYEE-CODE ( * )
            dtl_Line_Table_Dtl_Mode.getValue("*").reset();                                                                                                                //Natural: RESET DTL-MODE ( * )
            dtl_Line_Table_Dtl_Option.getValue("*").reset();                                                                                                              //Natural: RESET DTL-OPTION ( * )
            dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue("*").reset();                                                                                                 //Natural: RESET DTL-GRADED-STANDARD-IND ( * )
            dtl_Line_Table_Dtl_Rate_Code.getValue("*").reset();                                                                                                           //Natural: RESET DTL-RATE-CODE ( * )
            dtl_Line_Table_Dtl_Guaranteed_Payment.getValue("*").reset();                                                                                                  //Natural: RESET DTL-GUARANTEED-PAYMENT ( * )
            dtl_Line_Table_Dtl_Fund_Name.getValue("*").reset();                                                                                                           //Natural: RESET DTL-FUND-NAME ( * )
            dtl_Line_Table_Dtl_Cref_Reval_Method.getValue("*").reset();                                                                                                   //Natural: RESET DTL-CREF-REVAL-METHOD ( * )
            dtl_Line_Table_Dtl_Units.getValue("*").reset();                                                                                                               //Natural: RESET DTL-UNITS ( * )
            dtl_Line_Table_Dtl_Variable_Payment.getValue("*").reset();                                                                                                    //Natural: RESET DTL-VARIABLE-PAYMENT ( * )
            dtl_Line_Table_Dtl_Transfer_Amount.getValue("*").reset();                                                                                                     //Natural: RESET DTL-TRANSFER-AMOUNT ( * )
            pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.setValue(1);                                                                                              //Natural: MOVE 1 TO PGM-DETAIL-LINE-SUB
            dtl_Line_Table_Dtl_Transfer_Effective_Date.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue("GTDTOTAL");                                //Natural: MOVE 'GTDTOTAL' TO DTL-TRANSFER-EFFECTIVE-DATE ( PGM-DETAIL-LINE-SUB )
            pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.nadd(1);                                                                                                  //Natural: ADD 1 TO PGM-DETAIL-LINE-SUB
            //*  RBE2 FIX ENDS   <<
            pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.setValue(0);                                                                                              //Natural: MOVE 0 TO PGM-DETAIL-LINE-SUB
            pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max.setValue(0);                                                                                          //Natural: MOVE 0 TO PGM-DETAIL-LINE-SUB-MAX
            dtl_Line_Table_Dtl_Contract_Number.getValue(1).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Contract_Number());                                          //Natural: MOVE #SBC1-CONTRACT-NUMBER TO DTL-CONTRACT-NUMBER ( 1 )
            dtl_Line_Table_Dtl_Payee_Code.getValue(1).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Payee_Code());                                                    //Natural: MOVE #SBC1-PAYEE-CODE TO DTL-PAYEE-CODE ( 1 )
            dtl_Line_Table_Dtl_Mode.getValue(1).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Mode());                                                                //Natural: MOVE #SBC1-MODE TO DTL-MODE ( 1 )
            dtl_Line_Table_Dtl_Option_N.getValue(1).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Option());                                                          //Natural: MOVE #SBC1-OPTION TO DTL-OPTION-N ( 1 )
            if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().equals(0)))                                                                    //Natural: IF #SBC1-CURRENT-RECORD-NUMBER = 00
            {
                dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).setValue("S");                                                                                         //Natural: MOVE 'S' TO DTL-GRADED-STANDARD-IND ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).setValue("G");                                                                                         //Natural: MOVE 'G' TO DTL-GRADED-STANDARD-IND ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.setValue(1);                                                                                               //Natural: MOVE 1 TO PGM-CREF-ARRAY-SUB
            pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.setValue(1);                                                                                        //Natural: MOVE 1 TO PGM-CREF-ARRAY-DESIGNATOR
            //*  RESET PGM-CREF-FUND-COUNTER  /* 031411               RBE2
            //* *********************
            //*  #SB-ARRAY-AREA1 HAS THE FIRST 5 CREF ARRAY ELEMENTS
            //*  #SB-ARRAY-AREA2 HAS NEXT 5 CREF ARRAY ELEMENTS
            //*  #SB-ARRAY-AREA3 HAS NEXT 5 CREF ARRAY ELEMENTS, ETC
            //* *********************
            //*  FOR PGM-DETAIL-LINE-SUB 1 #MAX-DETAIL           /* OS - 111110
            //*  FOR PGM-DETAIL-LINE-SUB 1 #MAX-CREF2            /* RBE2
            //*  RBE2
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.greater(pnd_Max_Cref2))) {break;}                                                       //Natural: UNTIL PGM-DETAIL-LINE-SUB > #MAX-CREF2
                //*  OS - 111110
                REPEAT02:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.equals(pnd_Max_Cref))) {break;}                                                      //Natural: UNTIL PGM-CREF-ARRAY-SUB = #MAX-CREF
                    //*      ADD 1 TO PGM-CREF-FUND-COUNTER
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.equals(1)))                                                                   //Natural: IF PGM-CREF-ARRAY-DESIGNATOR = 1
                    {
                        pgm_Cref_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area1());                                                               //Natural: MOVE #SB-ARRAY-AREA1 TO PGM-CREF-ARRAY-AREA
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.equals(2)))                                                                   //Natural: IF PGM-CREF-ARRAY-DESIGNATOR = 2
                    {
                        pgm_Cref_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area2());                                                               //Natural: MOVE #SB-ARRAY-AREA2 TO PGM-CREF-ARRAY-AREA
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.equals(3)))                                                                   //Natural: IF PGM-CREF-ARRAY-DESIGNATOR = 3
                    {
                        pgm_Cref_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area3());                                                               //Natural: MOVE #SB-ARRAY-AREA3 TO PGM-CREF-ARRAY-AREA
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.equals(4)))                                                                   //Natural: IF PGM-CREF-ARRAY-DESIGNATOR = 4
                    {
                        pgm_Cref_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area4());                                                               //Natural: MOVE #SB-ARRAY-AREA4 TO PGM-CREF-ARRAY-AREA
                    }                                                                                                                                                     //Natural: END-IF
                    //*      IF PGM-CREF-ARRAY-DESIGNATOR = 5
                    //*        MOVE #SB-ARRAY-AREA5 TO PGM-CREF-ARRAY-AREA
                    //*      END-IF
                    if (condition(pgm_Cref_Array_Area_Pgm_Cref_Fund_Letter.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub).notEquals(" ")))                  //Natural: IF PGM-CREF-FUND-LETTER ( PGM-CREF-ARRAY-SUB ) <> ' '
                    {
                        //*  OS - 111110    RBE2
                        //*  RBE2
                        pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.nadd(1);                                                                                      //Natural: ADD 1 TO PGM-DETAIL-LINE-SUB
                        FOR01:                                                                                                                                            //Natural: FOR #I = 1 TO 12
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
                        {
                            if (condition(pnd_Fund_Letter_Array_Pnd_Fund_Letter.getValue(pnd_I).equals(pgm_Cref_Array_Area_Pgm_Cref_Fund_Letter.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)))) //Natural: IF #FUND-LETTER ( #I ) = PGM-CREF-FUND-LETTER ( PGM-CREF-ARRAY-SUB )
                            {
                                pnd_Fund_Input_Pos.setValue(pnd_I);                                                                                                       //Natural: MOVE #I TO #FUND-INPUT-POS
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  022211
                        dtl_Line_Table_Dtl_Fund_Name.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Input_Pos)); //Natural: MOVE #FUND-NAME ( #FUND-INPUT-POS ) TO DTL-FUND-NAME ( PGM-DETAIL-LINE-SUB )
                        //*          TO DTL-FUND-NAME (PGM-CREF-FUND-COUNTER)   /* 022211  RBE2
                        //*  022211
                        dtl_Line_Table_Dtl_Units.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(pgm_Cref_Array_Area_Pgm_Cref_Units.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: MOVE PGM-CREF-UNITS ( PGM-CREF-ARRAY-SUB ) TO DTL-UNITS ( PGM-DETAIL-LINE-SUB )
                        //*          TO DTL-UNITS (PGM-CREF-FUND-COUNTER)       /* 022211  RBE2
                        //*  022211
                        dtl_Line_Table_Dtl_Variable_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(pgm_Cref_Array_Area_Pgm_Cref_Variable_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: MOVE PGM-CREF-VARIABLE-PAYMENT ( PGM-CREF-ARRAY-SUB ) TO DTL-VARIABLE-PAYMENT ( PGM-DETAIL-LINE-SUB )
                        //*          TO DTL-VARIABLE-PAYMENT (PGM-CREF-FUND-COUNTER) /* 022211 RBE2
                        //*  022211
                        dtl_Line_Table_Dtl_Transfer_Amount.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: MOVE PGM-CREF-TRANSFER-AMOUNT ( PGM-CREF-ARRAY-SUB ) TO DTL-TRANSFER-AMOUNT ( PGM-DETAIL-LINE-SUB )
                        //*          TO DTL-TRANSFER-AMOUNT (PGM-CREF-FUND-COUNTER)  /* 022211 RBE2
                        if (condition(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).equals("S")))                                                                    //Natural: IF DTL-GRADED-STANDARD-IND ( 1 ) = 'S'
                        {
                            pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Standard.nadd(pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: ADD PGM-CREF-TRANSFER-AMOUNT ( PGM-CREF-ARRAY-SUB ) TO PGM-TOTAL-TIAA-ASSETS-STANDARD
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).equals("G")))                                                                    //Natural: IF DTL-GRADED-STANDARD-IND ( 1 ) = 'G'
                        {
                            pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Assets_Graded.nadd(pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: ADD PGM-CREF-TRANSFER-AMOUNT ( PGM-CREF-ARRAY-SUB ) TO PGM-TOTAL-TIAA-ASSETS-GRADED
                        }                                                                                                                                                 //Natural: END-IF
                        dtl_Line_Table_Dtl_Transfer_Effective_Date.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date()); //Natural: MOVE #SBC1-TRANSFER-EFFECTIVE-DATE TO DTL-TRANSFER-EFFECTIVE-DATE ( PGM-DETAIL-LINE-SUB )
                        //*          TO DTL-TRANSFER-EFFECTIVE-DATE (PGM-CREF-FUND-COUNTER)   RBE2
                        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Fund_Units.nadd(pgm_Cref_Array_Area_Pgm_Cref_Units.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: ADD PGM-CREF-UNITS ( PGM-CREF-ARRAY-SUB ) TO PGM-TOTAL-CREF-FUND-UNITS
                        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Variable_Payments.nadd(pgm_Cref_Array_Area_Pgm_Cref_Variable_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: ADD PGM-CREF-VARIABLE-PAYMENT ( PGM-CREF-ARRAY-SUB ) TO PGM-TOTAL-CREF-VARIABLE-PAYMENTS
                        pgm_Subscripts_Counters_Buckets_Pgm_Total_Cref_Transfer_Amount.nadd(pgm_Cref_Array_Area_Pgm_Cref_Transfer_Amount.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub)); //Natural: ADD PGM-CREF-TRANSFER-AMOUNT ( PGM-CREF-ARRAY-SUB ) TO PGM-TOTAL-CREF-TRANSFER-AMOUNT
                        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.nadd(1);                                                                                       //Natural: ADD 1 TO PGM-CREF-ARRAY-SUB
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  101810
                        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.setValue(9);                                                                                   //Natural: MOVE 9 TO PGM-CREF-ARRAY-SUB
                        //*        MOVE  5 TO PGM-CREF-ARRAY-SUB       /* 101810
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.greater(5)))                                                                         //Natural: IF PGM-CREF-ARRAY-SUB > 5
                    {
                        pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.setValue(1);                                                                                   //Natural: MOVE 1 TO PGM-CREF-ARRAY-SUB
                        if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.less(4)))                                                                 //Natural: IF PGM-CREF-ARRAY-DESIGNATOR < 4
                        {
                            pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Designator.nadd(1);                                                                            //Natural: ADD 1 TO PGM-CREF-ARRAY-DESIGNATOR
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  101810
                            pgm_Subscripts_Counters_Buckets_Pgm_Cref_Array_Sub.setValue(9);                                                                               //Natural: MOVE 9 TO PGM-CREF-ARRAY-SUB
                            //*          MOVE 5 TO PGM-CREF-ARRAY-SUB      /* 101810
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-REPEAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    IF PGM-DETAIL-LINE-SUB > PGM-DETAIL-LINE-SUB-MAX        /* RBE2
                //*  RBE2
                //*  RBE2
                if (condition((pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.less(99) && pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.greater(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max)))) //Natural: IF ( PGM-DETAIL-LINE-SUB < 99 AND PGM-DETAIL-LINE-SUB > PGM-DETAIL-LINE-SUB-MAX )
                {
                    pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub_Max.setValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub);                                //Natural: MOVE PGM-DETAIL-LINE-SUB TO PGM-DETAIL-LINE-SUB-MAX
                }                                                                                                                                                         //Natural: END-IF
                pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.setValue(99);                                                                                         //Natural: MOVE 99 TO PGM-DETAIL-LINE-SUB
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*   END-FOR                                                  /* RBE2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *** RBE2 FIX BEGINS >>
            if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number().equals(1)))                                                                        //Natural: IF #SB-KEY-SEQUENCE-NUMBER = 1
            {
                pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.setValue(0);                                                                                          //Natural: MOVE 0 TO PGM-DETAIL-LINE-SUB
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE 0 TO PGM-DETAIL-LINE-SUB
            if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number().greater(pnd_Max_Seq_Number)))                                                      //Natural: IF #SB-KEY-SEQUENCE-NUMBER > #MAX-SEQ-NUMBER
            {
                pnd_Max_Seq_Number.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number());                                                                //Natural: MOVE #SB-KEY-SEQUENCE-NUMBER TO #MAX-SEQ-NUMBER
            }                                                                                                                                                             //Natural: END-IF
            //* *** RBE2 FIX ENDS   <<
            pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.setValue(1);                                                                                               //Natural: MOVE 1 TO PGM-TIAA-ARRAY-SUB
            pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.setValue(1);                                                                                        //Natural: MOVE 1 TO PGM-TIAA-ARRAY-DESIGNATOR
            pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number());                         //Natural: MOVE #SBC2-CURRENT-RECORD-NUMBER TO PGM-CURRENT-RECORD-NUMBER
            if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Current_Record_Number().greater(50)))                                                                  //Natural: IF #SBC2-CURRENT-RECORD-NUMBER > 50
            {
                pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number.nsubtract(50);                                                                                  //Natural: SUBTRACT 50 FROM PGM-CURRENT-RECORD-NUMBER
            }                                                                                                                                                             //Natural: END-IF
            dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc2_Cref_Reval_Method()); //Natural: MOVE #SBC2-CREF-REVAL-METHOD TO DTL-CREF-REVAL-METHOD ( PGM-CURRENT-RECORD-NUMBER )
            getReports().write(0, "DTL-C-REVAL-MTHD (",pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number,")=",dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Current_Record_Number)); //Natural: WRITE 'DTL-C-REVAL-MTHD (' PGM-CURRENT-RECORD-NUMBER ')=' DTL-CREF-REVAL-METHOD ( PGM-CURRENT-RECORD-NUMBER )
            if (Global.isEscape()) return;
            //*  REPEAT UNTIL PGM-DETAIL-LINE-SUB > 60
            //*  FOR PGM-DETAIL-LINE-SUB 1 #MAX-DETAIL          /* 022211
            //*  REPEAT UNTIL PGM-DETAIL-LINE-SUB = #MAX-DETAIL /* 022211       RBE2
            //*    REPEAT UNTIL PGM-TIAA-ARRAY-SUB = 15         /* OS - 111110  RBE2
            //*  RBE2
            REPEAT03:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.equals(99))) {break;}                                                                    //Natural: UNTIL PGM-TIAA-ARRAY-SUB = 99
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(1)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 1
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area1());                                                                   //Natural: MOVE #SB-ARRAY-AREA1 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(2)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 2
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area2());                                                                   //Natural: MOVE #SB-ARRAY-AREA2 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(3)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 3
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area3());                                                                   //Natural: MOVE #SB-ARRAY-AREA3 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(4)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 4
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area4());                                                                   //Natural: MOVE #SB-ARRAY-AREA4 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(5)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 5
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area5());                                                                   //Natural: MOVE #SB-ARRAY-AREA5 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(6)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 6
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area6());                                                                   //Natural: MOVE #SB-ARRAY-AREA6 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                //*   RBE2 FIX BEGINS >>
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(7)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 7
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area7());                                                                   //Natural: MOVE #SB-ARRAY-AREA7 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(8)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 8
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area8());                                                                   //Natural: MOVE #SB-ARRAY-AREA8 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(9)))                                                                       //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 9
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area9());                                                                   //Natural: MOVE #SB-ARRAY-AREA9 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(10)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 10
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area10());                                                                  //Natural: MOVE #SB-ARRAY-AREA10 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(11)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 11
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area11());                                                                  //Natural: MOVE #SB-ARRAY-AREA11 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(12)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 12
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area12());                                                                  //Natural: MOVE #SB-ARRAY-AREA12 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(13)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 13
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area13());                                                                  //Natural: MOVE #SB-ARRAY-AREA13 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(14)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 14
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area14());                                                                  //Natural: MOVE #SB-ARRAY-AREA14 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(15)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 15
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area15());                                                                  //Natural: MOVE #SB-ARRAY-AREA15 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(16)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 16
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area16());                                                                  //Natural: MOVE #SB-ARRAY-AREA16 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(17)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 17
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area17());                                                                  //Natural: MOVE #SB-ARRAY-AREA17 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(18)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 18
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area18());                                                                  //Natural: MOVE #SB-ARRAY-AREA18 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.equals(19)))                                                                      //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR = 19
                {
                    pgm_Tiaa_Array_Area.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Array_Area19());                                                                  //Natural: MOVE #SB-ARRAY-AREA19 TO PGM-TIAA-ARRAY-AREA
                }                                                                                                                                                         //Natural: END-IF
                //*   RBE2 FIX ENDS   <<
                //*      IF PGM-TIAA-RATE-CODE (PGM-TIAA-ARRAY-SUB) �= ' '  /* 101510
                if (condition(! (pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub).equals(" ") || pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub).equals("00")))) //Natural: IF NOT PGM-TIAA-RATE-CODE ( PGM-TIAA-ARRAY-SUB ) = ' ' OR = '00'
                {
                    //*  OS - 111110 ADDED CHECK BELOW TO MAKE SURE NO GARBAGE AMOUNT
                    //*  RBE2 FIX BEGINS >>
                    //*          AND PGM-TIAA-GUARANTEED-PAYMENT-A (PGM-TIAA-ARRAY-SUB) IS (N9)
                    //*  RBE2 FIX ENDS   <<
                    //*  022211
                    pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub.nadd(1);                                                                                          //Natural: ADD 1 TO PGM-DETAIL-LINE-SUB
                    dtl_Line_Table_Dtl_Rate_Code.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(pgm_Tiaa_Array_Area_Pgm_Tiaa_Rate_Code.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub)); //Natural: MOVE PGM-TIAA-RATE-CODE ( PGM-TIAA-ARRAY-SUB ) TO DTL-RATE-CODE ( PGM-DETAIL-LINE-SUB )
                    dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).nadd(pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub)); //Natural: ADD PGM-TIAA-GUARANTEED-PAYMENT ( PGM-TIAA-ARRAY-SUB ) TO DTL-GUARANTEED-PAYMENT ( PGM-DETAIL-LINE-SUB )
                    dtl_Line_Table_Dtl_Transfer_Effective_Date.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Detail_Line_Sub).setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Transfer_Effective_Date()); //Natural: MOVE #SBC1-TRANSFER-EFFECTIVE-DATE TO DTL-TRANSFER-EFFECTIVE-DATE ( PGM-DETAIL-LINE-SUB )
                    //*  RBE2
                    dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(251).nadd(pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub)); //Natural: ADD PGM-TIAA-GUARANTEED-PAYMENT ( PGM-TIAA-ARRAY-SUB ) TO DTL-GUARANTEED-PAYMENT ( 251 )
                    if (condition(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).equals("S")))                                                                        //Natural: IF DTL-GRADED-STANDARD-IND ( 1 ) = 'S'
                    {
                        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Standard.nadd(pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub)); //Natural: ADD PGM-TIAA-GUARANTEED-PAYMENT ( PGM-TIAA-ARRAY-SUB ) TO PGM-TOTAL-TIAA-GTD-PMTS-STANDARD
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1).equals("G")))                                                                        //Natural: IF DTL-GRADED-STANDARD-IND ( 1 ) = 'G'
                    {
                        pgm_Subscripts_Counters_Buckets_Pgm_Total_Tiaa_Gtd_Pmts_Graded.nadd(pgm_Tiaa_Array_Area_Pgm_Tiaa_Guaranteed_Payment.getValue(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub)); //Natural: ADD PGM-TIAA-GUARANTEED-PAYMENT ( PGM-TIAA-ARRAY-SUB ) TO PGM-TOTAL-TIAA-GTD-PMTS-GRADED
                    }                                                                                                                                                     //Natural: END-IF
                    pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.nadd(1);                                                                                           //Natural: ADD 1 TO PGM-TIAA-ARRAY-SUB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  OS - 111110
                    pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.setValue(99);                                                                                      //Natural: MOVE 99 TO PGM-TIAA-ARRAY-SUB
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //* *    IF PGM-TIAA-ARRAY-SUB > 15             /* OS - 111110  RBE2
                //*      IF PGM-TIAA-ARRAY-SUB = 15             /* OS - 111110  RBE2
                //*   RBE2
                if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.greater(10)))                                                                            //Natural: IF PGM-TIAA-ARRAY-SUB > 10
                {
                    pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.setValue(1);                                                                                       //Natural: MOVE 1 TO PGM-TIAA-ARRAY-SUB
                    //*        IF PGM-TIAA-ARRAY-DESIGNATOR < 6                 /*  RBE2
                    //*   RBE2
                    if (condition(pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.less(19)))                                                                    //Natural: IF PGM-TIAA-ARRAY-DESIGNATOR < 19
                    {
                        pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Designator.nadd(1);                                                                                //Natural: ADD 1 TO PGM-TIAA-ARRAY-DESIGNATOR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  OS - 111110
                        pgm_Subscripts_Counters_Buckets_Pgm_Tiaa_Array_Sub.setValue(99);                                                                                  //Natural: MOVE 99 TO PGM-TIAA-ARRAY-SUB
                        //*          ESCAPE BOTTOM                          /* OS - 111110
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*    IF (PGM-DETAIL-LINE-SUB < 99                       /* OS - 111110
            //*        AND PGM-DETAIL-LINE-SUB > PGM-DETAIL-LINE-SUB-MAX)
            //*  RBE2 FIX BEGINS >>
            //*    IF PGM-DETAIL-LINE-SUB > PGM-DETAIL-LINE-SUB-MAX   /* OS - 111110
            //*      MOVE PGM-DETAIL-LINE-SUB TO PGM-DETAIL-LINE-SUB-MAX
            //*    END-IF
            //*  RBE2 FIX ENDS   <<
            //*     WRITE '2PGM-DETAIL-LINE-SUB=' PGM-DETAIL-LINE-SUB
            //*    MOVE 99 TO PGM-DETAIL-LINE-SUB                     /* OS - 111110
            //*    ESCAPE BOTTOM                          RBE2        /* OS - 111110
            //*  END-FOR                                              /* 022211
            //*  END-REPEAT                               RBE2        /* 022211
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '---------------------------'
        //*  WRITE '=' #SBC1-CURRENT-RECORD-NUMBER
        //*  WRITE '=' #SBC1-FINAL-RECORD-NUMBER
        //*  WRITE '=' #SB-KEY-SEQUENCE-NUMBER
        //*  WRITE '=' #MAX-SEQ-NUMBER
        //*  RBE2
        if (condition((ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().equals(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number()))           //Natural: IF ( #SBC1-CURRENT-RECORD-NUMBER = #SBC1-FINAL-RECORD-NUMBER ) AND ( #SB-KEY-SEQUENCE-NUMBER = #MAX-SEQ-NUMBER )
            && (ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Sequence_Number().equals(pnd_Max_Seq_Number))))
        {
            if (condition(dtl_Line_Table_Dtl_Transfer_Effective_Date_N.getValue(1).notEquals(pnd_Save_Eff_Date)))                                                         //Natural: IF DTL-TRANSFER-EFFECTIVE-DATE-N ( 1 ) <> #SAVE-EFF-DATE
            {
                if (condition(pnd_First_Return.equals("Y")))                                                                                                              //Natural: IF #FIRST-RETURN = 'Y'
                {
                    pnd_First_Return.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #FIRST-RETURN
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  RBE2
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS-BY-EFF-DATE
                    sub_Write_Totals_By_Eff_Date();
                    if (condition(Global.isEscape())) {return;}
                    FOR02:                                                                                                                                                //Natural: FOR #I = 1 TO 2
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #J = 3 TO 11
                        for (pnd_J.setValue(3); condition(pnd_J.lessOrEqual(11)); pnd_J.nadd(1))
                        {
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(pnd_I,pnd_J).setValue(0);                                                                     //Natural: MOVE 0 TO #TOT-CREF-UNITS ( #I, #J ) #TOT-CREF-PMTS ( #I, #J ) #TOT-CREF-ASSETS ( #I, #J )
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(pnd_I,pnd_J).setValue(0);
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(pnd_I,pnd_J).setValue(0);
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                sub_Write_Headings();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Eff_Date.setValue(dtl_Line_Table_Dtl_Transfer_Effective_Date_N.getValue(1));                                                                     //Natural: MOVE DTL-TRANSFER-EFFECTIVE-DATE-N ( 1 ) TO #SAVE-EFF-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Valid_Count.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #VALID-COUNT
            FOR04:                                                                                                                                                        //Natural: FOR #I = 2 TO #MAX-DETAIL
            for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Detail)); pnd_I.nadd(1))
            {
                //*    IF DTL-RATE-CODE (#I) = '  ' OR DTL-RATE-CODE-N (#I) = 0    RBE2
                //*  RBE2
                if (condition(dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).equals("  ") || dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).equals("0")))                     //Natural: IF DTL-RATE-CODE ( #I ) = '  ' OR = '0'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(pnd_I).setValue(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1));                                      //Natural: MOVE DTL-GRADED-STANDARD-IND ( 1 ) TO DTL-GRADED-STANDARD-IND ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR05:                                                                                                                                                        //Natural: FOR #I = 1 TO #MAX-DETAIL
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Detail)); pnd_I.nadd(1))
            {
                //*  WRITE (2) 'DTL-CONTRACT-NUMBER (' #I ') = ' DTL-CONTRACT-NUMBER (#I)
                //*  WRITE (2) 'DTL-RATE-CODE (' #I ') = ' DTL-RATE-CODE (#I)
                //*  WRITE (2) 'DTL-FUND-NAME (' #I ') = ' DTL-FUND-NAME (#I)
                //*     IF (DTL-RATE-CODE (#I) = '  ' OR DTL-RATE-CODE-N (#I) = 0) RBE2
                //*  RBE2
                if (condition(((dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).equals("  ") || dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).equals("0"))                    //Natural: IF ( DTL-RATE-CODE ( #I ) = '  ' OR = '0' ) AND DTL-FUND-NAME ( #I ) = '      '
                    && dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I).equals("      "))))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Valid_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #VALID-COUNT
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pnd_Grd_Std_Ind.setValue(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(pnd_I));                                                                     //Natural: MOVE DTL-GRADED-STANDARD-IND ( #I ) TO #GRD-STD-IND #SAVE-DTL-GRD-STD-IND
                    pnd_Save_Dtl_Grd_Std_Ind.setValue(dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(pnd_I));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Grd_Std_Ind.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #GRD-STD-IND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Current_Record_Number().equals(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Final_Record_Number())))   //Natural: IF #SBC1-CURRENT-RECORD-NUMBER = #SBC1-FINAL-RECORD-NUMBER
                {
                    //*      WRITE 'DTL-CONTRACT-NUMBER (' #I ')= ' DTL-CONTRACT-NUMBER (#I)
                    //*      WRITE 'DTL-GRADED-STANDARD-IND (' #I ') = '
                    //*        DTL-GRADED-STANDARD-IND (#I)
                    //*      WRITE 'DTL-UNITS (' #I ') = ' DTL-UNITS (#I)
                    //*      WRITE 'DTL-VARIABLE-PAYMENT (' #I ') = ' DTL-VARIABLE-PAYMENT (#I)
                    //*      WRITE 'DTL-TRANSFER-AMOUNT  (' #I ') = ' DTL-TRANSFER-AMOUNT  (#I)
                    //*  RBE2
                    if (condition(dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).equals("00")))                                                                             //Natural: IF DTL-RATE-CODE ( #I ) = '00'
                    {
                        //*  RBE2
                        dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I).setValue("  ");                                                                                      //Natural: MOVE '  ' TO DTL-RATE-CODE ( #I )
                        //*  RBE2
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Line_Count.greater(50)))                                                                                                            //Natural: IF #LINE-COUNT > 50
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                        sub_Write_Headings();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Line_Count.setValue(0);                                                                                                                       //Natural: MOVE 0 TO #LINE-COUNT
                        if (condition(dtl_Line_Table_Dtl_Units.getValue(pnd_I).equals(getZero()) && dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I).equals(getZero()) //Natural: IF DTL-UNITS ( #I ) = 0 AND DTL-VARIABLE-PAYMENT ( #I ) = 0 AND DTL-TRANSFER-AMOUNT ( #I ) = 0
                            && dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I).equals(getZero())))
                        {
                            //*  RBE2    WRITE 6X DTL-CONTRACT-NUMBER (1)  2X DTL-PAYEE-CODE (1) 3X
                            getReports().write(1, new ColumnSpacing(6),dtl_Line_Table_Dtl_Contract_Number.getValue(1),new ColumnSpacing(2),dtl_Line_Table_Dtl_Payee_Code.getValue(1),new  //Natural: WRITE ( 1 ) 6X DTL-CONTRACT-NUMBER ( 1 ) 2X DTL-PAYEE-CODE ( 1 ) 3X DTL-MODE ( 1 ) 5X DTL-OPTION ( 1 ) 4X DTL-GRADED-STANDARD-IND ( 1 ) 5X DTL-RATE-CODE ( #I ) 1X DTL-GUARANTEED-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 2X DTL-FUND-NAME ( #I ) 5X DTL-CREF-REVAL-METHOD ( #I )
                                ColumnSpacing(3),dtl_Line_Table_Dtl_Mode.getValue(1),new ColumnSpacing(5),dtl_Line_Table_Dtl_Option.getValue(1),new ColumnSpacing(4),dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1),new 
                                ColumnSpacing(5),dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I),new ColumnSpacing(1),dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I),new ColumnSpacing(5),
                                dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pnd_I));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  RBE2    WRITE 6X DTL-CONTRACT-NUMBER (1)  2X DTL-PAYEE-CODE (1) 3X
                            getReports().write(1, new ColumnSpacing(6),dtl_Line_Table_Dtl_Contract_Number.getValue(1),new ColumnSpacing(2),dtl_Line_Table_Dtl_Payee_Code.getValue(1),new  //Natural: WRITE ( 1 ) 6X DTL-CONTRACT-NUMBER ( 1 ) 2X DTL-PAYEE-CODE ( 1 ) 3X DTL-MODE ( 1 ) 5X DTL-OPTION ( 1 ) 4X DTL-GRADED-STANDARD-IND ( 1 ) 5X DTL-RATE-CODE ( #I ) 1X DTL-GUARANTEED-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 2X DTL-FUND-NAME ( #I ) 3X DTL-CREF-REVAL-METHOD ( #I ) 1X DTL-UNITS ( #I ) ( EM = ZZ,ZZ9.999 ) 1X DTL-VARIABLE-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 1X DTL-TRANSFER-AMOUNT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                                ColumnSpacing(3),dtl_Line_Table_Dtl_Mode.getValue(1),new ColumnSpacing(5),dtl_Line_Table_Dtl_Option.getValue(1),new ColumnSpacing(4),dtl_Line_Table_Dtl_Graded_Standard_Ind.getValue(1),new 
                                ColumnSpacing(5),dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I),new ColumnSpacing(1),dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I),new ColumnSpacing(3),dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pnd_I),new 
                                ColumnSpacing(1),dtl_Line_Table_Dtl_Units.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.999"),new ColumnSpacing(1),dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I), new ReportEditMask 
                                ("ZZZ,ZZZ,ZZ9.99"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(dtl_Line_Table_Dtl_Units.getValue(pnd_I).equals(getZero()) && dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I).equals(getZero()) //Natural: IF DTL-UNITS ( #I ) = 0 AND DTL-VARIABLE-PAYMENT ( #I ) = 0 AND DTL-TRANSFER-AMOUNT ( #I ) = 0
                            && dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I).equals(getZero())))
                        {
                            //*  RBE2    WRITE 6X DTL-CONTRACT-NUMBER (#I)  2X DTL-PAYEE-CODE (#I) 3X
                            getReports().write(1, new ColumnSpacing(6),dtl_Line_Table_Dtl_Contract_Number.getValue(pnd_I),new ColumnSpacing(2),dtl_Line_Table_Dtl_Payee_Code.getValue(pnd_I),new  //Natural: WRITE ( 1 ) 6X DTL-CONTRACT-NUMBER ( #I ) 2X DTL-PAYEE-CODE ( #I ) 3X DTL-MODE ( #I ) 5X DTL-OPTION ( #I ) 4X #GRD-STD-IND 5X DTL-RATE-CODE ( #I ) 1X DTL-GUARANTEED-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 2X DTL-FUND-NAME ( #I ) 5X DTL-CREF-REVAL-METHOD ( #I )
                                ColumnSpacing(3),dtl_Line_Table_Dtl_Mode.getValue(pnd_I),new ColumnSpacing(5),dtl_Line_Table_Dtl_Option.getValue(pnd_I),new 
                                ColumnSpacing(4),pnd_Grd_Std_Ind,new ColumnSpacing(5),dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I),new ColumnSpacing(1),dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I),new ColumnSpacing(5),
                                dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pnd_I));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  RBE2     WRITE 6X DTL-CONTRACT-NUMBER (#I)  2X DTL-PAYEE-CODE (#I) 3X
                            getReports().write(1, new ColumnSpacing(6),dtl_Line_Table_Dtl_Contract_Number.getValue(pnd_I),new ColumnSpacing(2),dtl_Line_Table_Dtl_Payee_Code.getValue(pnd_I),new  //Natural: WRITE ( 1 ) 6X DTL-CONTRACT-NUMBER ( #I ) 2X DTL-PAYEE-CODE ( #I ) 3X DTL-MODE ( #I ) 5X DTL-OPTION ( #I ) 4X #GRD-STD-IND 5X DTL-RATE-CODE ( #I ) 1X DTL-GUARANTEED-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 2X DTL-FUND-NAME ( #I ) 5X DTL-CREF-REVAL-METHOD ( #I ) 1X DTL-UNITS ( #I ) ( EM = ZZ,ZZ9.999 ) 1X DTL-VARIABLE-PAYMENT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 1X DTL-TRANSFER-AMOUNT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                                ColumnSpacing(3),dtl_Line_Table_Dtl_Mode.getValue(pnd_I),new ColumnSpacing(5),dtl_Line_Table_Dtl_Option.getValue(pnd_I),new 
                                ColumnSpacing(4),pnd_Grd_Std_Ind,new ColumnSpacing(5),dtl_Line_Table_Dtl_Rate_Code.getValue(pnd_I),new ColumnSpacing(1),dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I),new ColumnSpacing(5),dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pnd_I),new 
                                ColumnSpacing(1),dtl_Line_Table_Dtl_Units.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.999"),new ColumnSpacing(1),dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I), 
                                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I), new ReportEditMask 
                                ("ZZZ,ZZZ,ZZ9.99"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Line_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LINE-COUNT
                if (condition(pnd_Save_Dtl_Grd_Std_Ind.equals("S")))                                                                                                      //Natural: IF #SAVE-DTL-GRD-STD-IND = 'S'
                {
                    pnd_Tot_Guar_Pmt_Std.nadd(dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I));                                                                     //Natural: ADD DTL-GUARANTEED-PAYMENT ( #I ) TO #TOT-GUAR-PMT-STD
                    pnd_Tot_Trans_Amt_Std.nadd(dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I));                                                                       //Natural: ADD DTL-TRANSFER-AMOUNT ( #I ) TO #TOT-TRANS-AMT-STD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Guar_Pmt_Grd.nadd(dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(pnd_I));                                                                     //Natural: ADD DTL-GUARANTEED-PAYMENT ( #I ) TO #TOT-GUAR-PMT-GRD
                    pnd_Tot_Trans_Amt_Grd.nadd(dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I));                                                                       //Natural: ADD DTL-TRANSFER-AMOUNT ( #I ) TO #TOT-TRANS-AMT-GRD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Var_Pmt.nadd(dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I));                                                                                //Natural: ADD DTL-VARIABLE-PAYMENT ( #I ) TO #TOT-VAR-PMT
                pnd_Tot_Trans_Amt.nadd(dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I));                                                                               //Natural: ADD DTL-TRANSFER-AMOUNT ( #I ) TO #TOT-TRANS-AMT
                if (condition(dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I).equals("      ")))                                                                             //Natural: IF DTL-FUND-NAME ( #I ) = '      '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  RBE2
                }                                                                                                                                                         //Natural: END-IF
                FOR06:                                                                                                                                                    //Natural: FOR #J = 3 TO 11
                for (pnd_J.setValue(3); condition(pnd_J.lessOrEqual(11)); pnd_J.nadd(1))
                {
                    if (condition(dtl_Line_Table_Dtl_Fund_Name.getValue(pnd_I).equals(pnd_Fund_Name_Array_Pnd_Fund_Name_1_6.getValue(pnd_J))))                            //Natural: IF DTL-FUND-NAME ( #I ) = #FUND-NAME-1-6 ( #J )
                    {
                        if (condition(dtl_Line_Table_Dtl_Cref_Reval_Method.getValue(pnd_I).equals("A")))                                                                  //Natural: IF DTL-CREF-REVAL-METHOD ( #I ) = 'A'
                        {
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(1,pnd_J).nadd(dtl_Line_Table_Dtl_Units.getValue(pnd_I));                                      //Natural: ADD DTL-UNITS ( #I ) TO #TOT-CREF-UNITS ( 1, #J )
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(1,pnd_J).nadd(dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I));                            //Natural: ADD DTL-VARIABLE-PAYMENT ( #I ) TO #TOT-CREF-PMTS ( 1, #J )
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(1,pnd_J).nadd(dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I));                           //Natural: ADD DTL-TRANSFER-AMOUNT ( #I ) TO #TOT-CREF-ASSETS ( 1, #J )
                            //*        ESCAPE TOP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(2,pnd_J).nadd(dtl_Line_Table_Dtl_Units.getValue(pnd_I));                                      //Natural: ADD DTL-UNITS ( #I ) TO #TOT-CREF-UNITS ( 2, #J )
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(2,pnd_J).nadd(dtl_Line_Table_Dtl_Variable_Payment.getValue(pnd_I));                            //Natural: ADD DTL-VARIABLE-PAYMENT ( #I ) TO #TOT-CREF-PMTS ( 2, #J )
                            pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(2,pnd_J).nadd(dtl_Line_Table_Dtl_Transfer_Amount.getValue(pnd_I));                           //Natural: ADD DTL-TRANSFER-AMOUNT ( #I ) TO #TOT-CREF-ASSETS ( 2, #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Valid_Count.greater(getZero())))                                                                                                            //Natural: IF #VALID-COUNT > 0
            {
                //*   RBE2 FIX BEGINS >>
                //*    WRITE  40X 'TOT ' DTL-GUARANTEED-PAYMENT (61) (EM=Z,ZZZ,ZZZZ.99) 26X
                //*  RBE2    WRITE  40X 'TOT '
                getReports().write(1, new ColumnSpacing(40),"TOT ",dtl_Line_Table_Dtl_Guaranteed_Payment.getValue(251), new ReportEditMask ("ZZZ,ZZZZ.99"),new            //Natural: WRITE ( 1 ) 40X 'TOT ' DTL-GUARANTEED-PAYMENT ( 251 ) ( EM = Z,ZZZ,ZZZZ.99 ) 26X #TOT-VAR-PMT ( EM = Z,ZZZ,ZZZ.99 ) #TOT-TRANS-AMT ( EM = ZZZ,ZZZ,ZZZ.99 ) /
                    ColumnSpacing(26),pnd_Tot_Var_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),pnd_Tot_Trans_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE);
                if (Global.isEscape()) return;
                //*   RBE2 FIX ENDS   <<
            }                                                                                                                                                             //Natural: END-IF
            pnd_Line_Count.nadd(2);                                                                                                                                       //Natural: ADD 2 TO #LINE-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        pgm_Subscripts_Counters_Buckets_Pgm_Cpm_Key_Previous.setValue(ldaAial019b.getPnd_Sortb_Record_Pnd_Sbc1_Cpm_Key());                                                //Natural: MOVE #SBC1-CPM-KEY TO PGM-CPM-KEY-PREVIOUS
        pnd_Tot_Var_Pmt.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #TOT-VAR-PMT #TOT-TRANS-AMT
        pnd_Tot_Trans_Amt.setValue(0);
    }
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  RBE2 FIX BEGINS >>
        //*  ITE 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE '
        getReports().write(1, "ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ",ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte(),                   //Natural: WRITE ( 1 ) 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ' #SB-KEY-PRCSS-DTE ' EFFECTIVE DATE ' DTL-TRANSFER-EFFECTIVE-DATE ( 1 ) /
            " EFFECTIVE DATE ",dtl_Line_Table_Dtl_Transfer_Effective_Date.getValue(1),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "1) SERIATIM DETAIL",NEWLINE);                                                                                                              //Natural: WRITE ( 1 ) '1) SERIATIM DETAIL' /
        if (Global.isEscape()) return;
        getReports().write(1, "       FROM:",new ColumnSpacing(47),"TO:",NEWLINE);                                                                                        //Natural: WRITE ( 1 ) '       FROM:' 47X 'TO:' /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(7),"TIAA",new ColumnSpacing(3),"PAYEE",new ColumnSpacing(15),"GRD",new ColumnSpacing(30),"REVAL",new ColumnSpacing(5),"NEW",new  //Natural: WRITE ( 1 ) 7X 'TIAA' 3X 'PAYEE' 15X 'GRD' 30X 'REVAL' 5X 'NEW' 6X 'NEW VARIABLE' 1X 'ASSET TRANSFER'
            ColumnSpacing(6),"NEW VARIABLE",new ColumnSpacing(1),"ASSET TRANSFER");
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(6),"CONTRACT",new ColumnSpacing(1),"CODE",new ColumnSpacing(1),"MODE",new ColumnSpacing(2),"OPTION",new                   //Natural: WRITE ( 1 ) 6X 'CONTRACT' 1X 'CODE' 1X 'MODE' 2X 'OPTION' 2X 'STD' 4X 'RB' 8X 'GUAR' 4X 'FUND' 5X 'IND' 5X 'UNITS' 8X 'PMTS' 9X 'AMTS'
            ColumnSpacing(2),"STD",new ColumnSpacing(4),"RB",new ColumnSpacing(8),"GUAR",new ColumnSpacing(4),"FUND",new ColumnSpacing(5),"IND",new ColumnSpacing(5),"UNITS",new 
            ColumnSpacing(8),"PMTS",new ColumnSpacing(9),"AMTS");
        if (Global.isEscape()) return;
        pnd_Line_Count.setValue(8);                                                                                                                                       //Natural: MOVE 8 TO #LINE-COUNT
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  RBE2 FIX BEGINS >>
        //*  ITE 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE '
        getReports().write(1, "ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ",ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte(),                   //Natural: WRITE ( 1 ) 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ' #SB-KEY-PRCSS-DTE ' EFFECTIVE DATE ALL' /
            " EFFECTIVE DATE ALL",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "2) SUMMARY",NEWLINE);                                                                                                                      //Natural: WRITE ( 1 ) '2) SUMMARY' /
        if (Global.isEscape()) return;
        getReports().write(1, "FROM:",new ColumnSpacing(19),"GUARANTEED",new ColumnSpacing(17),"ASSETS");                                                                 //Natural: WRITE ( 1 ) 'FROM:' 19X 'GUARANTEED' 17X 'ASSETS'
        if (Global.isEscape()) return;
        getReports().write(1, "TIAA STANDARD",new ColumnSpacing(7),pnd_Grand_Tot_Guar_Pmt_Std, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Grand_Tot_Trans_Amt_Std,  //Natural: WRITE ( 1 ) 'TIAA STANDARD' 7X #GRAND-TOT-GUAR-PMT-STD ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #GRAND-TOT-TRANS-AMT-STD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, "TIAA GRADED",new ColumnSpacing(9),pnd_Grand_Tot_Guar_Pmt_Grd, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Grand_Tot_Trans_Amt_Grd,  //Natural: WRITE ( 1 ) 'TIAA GRADED' 9X #GRAND-TOT-GUAR-PMT-GRD ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #GRAND-TOT-TRANS-AMT-GRD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Grand_Tot_Guar_Pmt.compute(new ComputeParameters(true, pnd_Grand_Tot_Guar_Pmt), pnd_Grand_Tot_Guar_Pmt_Std.add(pnd_Grand_Tot_Guar_Pmt_Grd));                  //Natural: COMPUTE ROUNDED #GRAND-TOT-GUAR-PMT = #GRAND-TOT-GUAR-PMT-STD + #GRAND-TOT-GUAR-PMT-GRD
        pnd_Grand_Tot_Trans_Amt.compute(new ComputeParameters(true, pnd_Grand_Tot_Trans_Amt), pnd_Grand_Tot_Trans_Amt_Std.add(pnd_Grand_Tot_Trans_Amt_Grd));              //Natural: COMPUTE ROUNDED #GRAND-TOT-TRANS-AMT = #GRAND-TOT-TRANS-AMT-STD + #GRAND-TOT-TRANS-AMT-GRD
        getReports().write(1, "TIAA GRAND TOTALS",new ColumnSpacing(4),pnd_Grand_Tot_Guar_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Grand_Tot_Trans_Amt,  //Natural: WRITE ( 1 ) 'TIAA GRAND TOTALS' 4X #GRAND-TOT-GUAR-PMT ( EM = ZZ,ZZZ,ZZ9.99 ) 6X #GRAND-TOT-TRANS-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  RBE2
        getReports().write(1, new ColumnSpacing(4),"TO:",new ColumnSpacing(5),"REVAL METHOD",new ColumnSpacing(4),"UNITS",new ColumnSpacing(7),"PAYMENTS",new             //Natural: WRITE ( 1 ) 4X 'TO:' 5X 'REVAL METHOD' 4X 'UNITS' 7X 'PAYMENTS' 12X 'ASSETS'
            ColumnSpacing(12),"ASSETS");
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #J = 1 TO 2
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
        {
            FOR08:                                                                                                                                                        //Natural: FOR #I = 3 TO 11
            for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
            {
                if (condition(pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units.getValue(pnd_J,pnd_I).equals(getZero()) && pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts.getValue(pnd_J,pnd_I).equals(getZero())  //Natural: IF #GRAND-TOT-CREF-UNITS ( #J, #I ) = 0 AND #GRAND-TOT-CREF-PMTS ( #J, #I ) = 0 AND #GRAND-TOT-CREF-ASSETS ( #J, #I ) = 0
                    && pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets.getValue(pnd_J,pnd_I).equals(getZero())))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_J.equals(1)))                                                                                                                           //Natural: IF #J = 1
                {
                    pnd_Prt_Reval.setValue("ANNUAL ");                                                                                                                    //Natural: MOVE 'ANNUAL ' TO #PRT-REVAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prt_Reval.setValue("MONTHLY");                                                                                                                    //Natural: MOVE 'MONTHLY' TO #PRT-REVAL
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, new ColumnSpacing(4),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I),new ColumnSpacing(2),pnd_Prt_Reval,new ColumnSpacing(1),pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units.getValue(pnd_J,pnd_I),  //Natural: WRITE ( 1 ) 4X #FUND-NAME ( #I ) 2X #PRT-REVAL 1X #GRAND-TOT-CREF-UNITS ( #J, #I ) ( EM = ZZZ,ZZ9.999 ) 2X #GRAND-TOT-CREF-PMTS ( #J, #I ) ( EM = ZZ,ZZZ,ZZ9.99 ) 2X #GRAND-TOT-CREF-ASSETS ( #J, #I ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts.getValue(pnd_J,pnd_I), new ReportEditMask 
                    ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets.getValue(pnd_J,pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE ' GRAND TOTALS' #J 'I' #I
                //*  DY2 FIX BEGINS >>
                pnd_Grand_Total_Cref_Units.nadd(pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units.getValue(pnd_J,pnd_I));                                                      //Natural: ADD #GRAND-TOT-CREF-UNITS ( #J, #I ) TO #GRAND-TOTAL-CREF-UNITS
                //*  DY2 FIX ENDS   <<
                pnd_Grand_Total_Cref_Pmts.nadd(pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts.getValue(pnd_J,pnd_I));                                                        //Natural: ADD #GRAND-TOT-CREF-PMTS ( #J, #I ) TO #GRAND-TOTAL-CREF-PMTS
                pnd_Grand_Total_Cref_Assets.nadd(pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets.getValue(pnd_J,pnd_I));                                                    //Natural: ADD #GRAND-TOT-CREF-ASSETS ( #J, #I ) TO #GRAND-TOTAL-CREF-ASSETS
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DY2
        //*  DY2
        getReports().write(1, new ColumnSpacing(4),"CREF GRAND TOTALS",new ColumnSpacing(1),pnd_Grand_Total_Cref_Units, new ReportEditMask ("ZZZ,ZZ9.999"),new            //Natural: WRITE ( 1 ) 4X 'CREF GRAND TOTALS' 1X #GRAND-TOTAL-CREF-UNITS ( EM = ZZZ,ZZ9.999 ) 2X #GRAND-TOTAL-CREF-PMTS ( EM = ZZ,ZZZ,ZZ9.99 ) 1X #GRAND-TOTAL-CREF-ASSETS ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(2),pnd_Grand_Total_Cref_Pmts, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Grand_Total_Cref_Assets, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Totals_By_Eff_Date() throws Exception                                                                                                          //Natural: WRITE-TOTALS-BY-EFF-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  RBE2 FIX BEGINS > ADD A WRITE TO REPORT 1
        //*  ITE 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE '
        getReports().write(1, "ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ",ldaAial019b.getPnd_Sortb_Record_Pnd_Sb_Key_Prcss_Dte(),                   //Natural: WRITE ( 1 ) 'ACTUARIAL TIAA TO CREF IA TRANSFER SUMMARY FOR PROCESSING DATE ' #SB-KEY-PRCSS-DTE ' EFFECTIVE DATE ' #SAVE-EFF-DATE /
            " EFFECTIVE DATE ",pnd_Save_Eff_Date,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "2) SUMMARY",NEWLINE);                                                                                                                      //Natural: WRITE ( 1 ) '2) SUMMARY' /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(4),"FROM:",new ColumnSpacing(15),"GUARANTEED",new ColumnSpacing(17),"ASSETS");                                            //Natural: WRITE ( 1 ) 4X 'FROM:' 15X 'GUARANTEED' 17X 'ASSETS'
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(4),"TIAA STANDARD",new ColumnSpacing(4),pnd_Tot_Guar_Pmt_Std, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new                    //Natural: WRITE ( 1 ) 4X 'TIAA STANDARD' 4X #TOT-GUAR-PMT-STD ( EM = ZZ,ZZZ,ZZ9.99 ) 6X #TOT-TRANS-AMT-STD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(6),pnd_Tot_Trans_Amt_Std, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(4),"TIAA GRADED",new ColumnSpacing(6),pnd_Tot_Guar_Pmt_Grd, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pnd_Tot_Trans_Amt_Grd,  //Natural: WRITE ( 1 ) 4X 'TIAA GRADED' 6X #TOT-GUAR-PMT-GRD ( EM = ZZ,ZZZ,ZZ9.99 ) 6X #TOT-TRANS-AMT-GRD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Tot_Guar_Pmt.compute(new ComputeParameters(true, pnd_Tot_Guar_Pmt), pnd_Tot_Guar_Pmt_Std.add(pnd_Tot_Guar_Pmt_Grd));                                          //Natural: COMPUTE ROUNDED #TOT-GUAR-PMT = #TOT-GUAR-PMT-STD + #TOT-GUAR-PMT-GRD
        pnd_Grand_Tot_Guar_Pmt_Std.nadd(pnd_Tot_Guar_Pmt_Std);                                                                                                            //Natural: ADD #TOT-GUAR-PMT-STD TO #GRAND-TOT-GUAR-PMT-STD
        pnd_Grand_Tot_Guar_Pmt_Grd.nadd(pnd_Tot_Guar_Pmt_Grd);                                                                                                            //Natural: ADD #TOT-GUAR-PMT-GRD TO #GRAND-TOT-GUAR-PMT-GRD
        pnd_Grand_Tot_Trans_Amt_Std.nadd(pnd_Tot_Trans_Amt_Std);                                                                                                          //Natural: ADD #TOT-TRANS-AMT-STD TO #GRAND-TOT-TRANS-AMT-STD
        pnd_Grand_Tot_Trans_Amt_Grd.nadd(pnd_Tot_Trans_Amt_Grd);                                                                                                          //Natural: ADD #TOT-TRANS-AMT-GRD TO #GRAND-TOT-TRANS-AMT-GRD
        pnd_Tot_Trans_Amt.compute(new ComputeParameters(true, pnd_Tot_Trans_Amt), pnd_Tot_Trans_Amt_Std.add(pnd_Tot_Trans_Amt_Grd));                                      //Natural: COMPUTE ROUNDED #TOT-TRANS-AMT = #TOT-TRANS-AMT-STD + #TOT-TRANS-AMT-GRD
        getReports().write(1, new ColumnSpacing(4),"TIAA TOTALS",new ColumnSpacing(6),pnd_Tot_Guar_Pmt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(6),pnd_Tot_Trans_Amt,  //Natural: WRITE ( 1 ) 4X 'TIAA TOTALS' 6X #TOT-GUAR-PMT ( EM = ZZ,ZZZ,ZZZ.99 ) 6X #TOT-TRANS-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) //
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  RBE2
        getReports().write(1, new ColumnSpacing(4),"TO:",new ColumnSpacing(5),"REVAL METHOD",new ColumnSpacing(4),"UNITS",new ColumnSpacing(7),"PAYMENTS",new             //Natural: WRITE ( 1 ) 4X 'TO:' 5X 'REVAL METHOD' 4X 'UNITS' 7X 'PAYMENTS' 12X 'ASSETS'
            ColumnSpacing(12),"ASSETS");
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #J = 1 TO 2
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
        {
            FOR10:                                                                                                                                                        //Natural: FOR #I = 3 TO 11
            for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
            {
                if (condition(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(pnd_J,pnd_I).equals(getZero()) && pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(pnd_J,pnd_I).equals(getZero())  //Natural: IF #TOT-CREF-UNITS ( #J, #I ) = 0 AND #TOT-CREF-PMTS ( #J, #I ) = 0 AND #TOT-CREF-ASSETS ( #J, #I ) = 0
                    && pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(pnd_J,pnd_I).equals(getZero())))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_J.equals(1)))                                                                                                                           //Natural: IF #J = 1
                {
                    pnd_Prt_Reval.setValue("ANNUAL ");                                                                                                                    //Natural: MOVE 'ANNUAL ' TO #PRT-REVAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prt_Reval.setValue("MONTHLY");                                                                                                                    //Natural: MOVE 'MONTHLY' TO #PRT-REVAL
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, new ColumnSpacing(4),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I),new ColumnSpacing(1),pnd_Prt_Reval,new ColumnSpacing(2),pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(pnd_J,pnd_I),  //Natural: WRITE ( 1 ) 4X #FUND-NAME ( #I ) 1X #PRT-REVAL 2X #TOT-CREF-UNITS ( #J, #I ) ( EM = ZZZ,ZZ9.999 ) 2X #TOT-CREF-PMTS ( #J, #I ) ( EM = ZZ,ZZZ,ZZ9.99 ) 1X #TOT-CREF-ASSETS ( #J, #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(pnd_J,pnd_I), new ReportEditMask 
                    ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(pnd_J,pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Units.getValue(pnd_J,pnd_I).nadd(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Units.getValue(pnd_J,pnd_I));                    //Natural: ADD #TOT-CREF-UNITS ( #J, #I ) TO #GRAND-TOT-CREF-UNITS ( #J, #I )
                pnd_Total_Cref_Pmts.nadd(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(pnd_J,pnd_I));                                                                    //Natural: ADD #TOT-CREF-PMTS ( #J, #I ) TO #TOTAL-CREF-PMTS
                pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Pmts.getValue(pnd_J,pnd_I).nadd(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Pmts.getValue(pnd_J,pnd_I));                      //Natural: ADD #TOT-CREF-PMTS ( #J, #I ) TO #GRAND-TOT-CREF-PMTS ( #J, #I )
                pnd_Total_Cref_Assets.nadd(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(pnd_J,pnd_I));                                                                //Natural: ADD #TOT-CREF-ASSETS ( #J, #I ) TO #TOTAL-CREF-ASSETS
                pnd_Tot_Cref_Arrays_Pnd_Grand_Tot_Cref_Assets.getValue(pnd_J,pnd_I).nadd(pnd_Tot_Cref_Arrays_Pnd_Tot_Cref_Assets.getValue(pnd_J,pnd_I));                  //Natural: ADD #TOT-CREF-ASSETS ( #J, #I ) TO #GRAND-TOT-CREF-ASSETS ( #J, #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,new ColumnSpacing(4),"CREF TOTALS",new ColumnSpacing(20),pnd_Total_Cref_Pmts, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new              //Natural: WRITE ( 1 ) / 4X 'CREF TOTALS' 20X #TOTAL-CREF-PMTS ( EM = ZZ,ZZZ,ZZ9.99 ) 1X #TOTAL-CREF-ASSETS ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(1),pnd_Total_Cref_Assets, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Tot_Guar_Pmt_Std.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #TOT-GUAR-PMT-STD #TOT-GUAR-PMT-GRD #TOT-TRANS-AMT-STD #TOT-TRANS-AMT-GRD #TOT-TRANS-AMT #TOTAL-CREF-PMTS #TOTAL-CREF-ASSETS
        pnd_Tot_Guar_Pmt_Grd.setValue(0);
        pnd_Tot_Trans_Amt_Std.setValue(0);
        pnd_Tot_Trans_Amt_Grd.setValue(0);
        pnd_Tot_Trans_Amt.setValue(0);
        pnd_Total_Cref_Pmts.setValue(0);
        pnd_Total_Cref_Assets.setValue(0);
        //* *******************************************************
        //*  WRITE (2)
        //*   #FUND-NAME (1:10) /
        //*   #TOT-CREF-UNITS (2,3:10) /
        //*   #TOT-CREF-PMTS (2,3:10) /
        //*   #TOT-CREF-ASSETS (2,3:10) /
        //* *******************************************************
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66 ES=ON");
        Global.format(1, "LS=132 PS=66 ES=ON");
    }
}
