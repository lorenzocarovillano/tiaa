/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:56 PM
**        * FROM NATURAL PROGRAM : Iaap775
************************************************************
**        * FILE NAME            : Iaap775.java
**        * CLASS NAME           : Iaap775
**        * INSTANCE NAME        : Iaap775
************************************************************
************************************************************************
*
* PROGRAM  : IAAP775
* DATE     : MAY 5, 1998
* FUNCTION : READS THE LATEST AA AND DC CONTROL RECORDS AND GETS THE
*            FACTOR FOR ALL CREF FUNDS USING CHECK DATE FROM CONTROL
*            RECORD.
*
* HISTORY  : ORIGINAL CODE - JUN F. TINIO
*
*
*
*
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap775 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_2;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_2_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_2_Cntrl_Next_Bus_Dte;

    private DbsGroup iaan390;
    private DbsField iaan390_Pnd_Call_Type;
    private DbsField iaan390_Pnd_Ia_Fund_Cde;
    private DbsField iaan390_Pnd_Reval_Method;
    private DbsField iaan390_Pnd_Uv_Req_Dte;

    private DbsGroup iaan390__R_Field_1;
    private DbsField iaan390_Pnd_Uv_Req_Dte_A;
    private DbsField iaan390_Pnd_Issue_Dte;

    private DbsGroup iaan390__R_Field_2;
    private DbsField iaan390_Pnd_Issue_Dte_A;
    private DbsField iaan390_Pnd_Iuv;
    private DbsField pnd_Cmpny_Fund_Cde;

    private DbsGroup pnd_Cmpny_Fund_Cde__R_Field_3;
    private DbsField pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde;
    private DbsField pnd_Cmpny_Fund_Cde_Pnd_Fund_Cde;

    private DbsGroup pnd_Factor_Table;
    private DbsField pnd_Factor_Table_Pnd_Afslash_M;
    private DbsField pnd_Factor_Table_Pnd_Funds;
    private DbsField pnd_Factor_Table_Pnd_Fund_Desc;
    private DbsField pnd_Fctr_Dte;
    private DbsField pnd_Desc;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Len;
    private DbsField pnd_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrl_Rcrd_2 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_2", "IAA-CNTRL-RCRD-2"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_2_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_2_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_2_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_2_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_2_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_2_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_2_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_2_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_2_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_2_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_2_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_2_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_2_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_2_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_2.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_2_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_2_Cntrl_Units = iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_2_Cntrl_Amt = iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_2_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_2_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_2_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_2_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_2.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_2_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_2);

        iaan390 = localVariables.newGroupInRecord("iaan390", "IAAN390");
        iaan390_Pnd_Call_Type = iaan390.newFieldInGroup("iaan390_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        iaan390_Pnd_Ia_Fund_Cde = iaan390.newFieldInGroup("iaan390_Pnd_Ia_Fund_Cde", "#IA-FUND-CDE", FieldType.STRING, 1);
        iaan390_Pnd_Reval_Method = iaan390.newFieldInGroup("iaan390_Pnd_Reval_Method", "#REVAL-METHOD", FieldType.STRING, 1);
        iaan390_Pnd_Uv_Req_Dte = iaan390.newFieldInGroup("iaan390_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        iaan390__R_Field_1 = iaan390.newGroupInGroup("iaan390__R_Field_1", "REDEFINE", iaan390_Pnd_Uv_Req_Dte);
        iaan390_Pnd_Uv_Req_Dte_A = iaan390__R_Field_1.newFieldInGroup("iaan390_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);
        iaan390_Pnd_Issue_Dte = iaan390.newFieldInGroup("iaan390_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 8);

        iaan390__R_Field_2 = iaan390.newGroupInGroup("iaan390__R_Field_2", "REDEFINE", iaan390_Pnd_Issue_Dte);
        iaan390_Pnd_Issue_Dte_A = iaan390__R_Field_2.newFieldInGroup("iaan390_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 8);
        iaan390_Pnd_Iuv = iaan390.newFieldInGroup("iaan390_Pnd_Iuv", "#IUV", FieldType.PACKED_DECIMAL, 8, 4);
        pnd_Cmpny_Fund_Cde = localVariables.newFieldInRecord("pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", FieldType.STRING, 3);

        pnd_Cmpny_Fund_Cde__R_Field_3 = localVariables.newGroupInRecord("pnd_Cmpny_Fund_Cde__R_Field_3", "REDEFINE", pnd_Cmpny_Fund_Cde);
        pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde = pnd_Cmpny_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde", "#CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Cmpny_Fund_Cde_Pnd_Fund_Cde = pnd_Cmpny_Fund_Cde__R_Field_3.newFieldInGroup("pnd_Cmpny_Fund_Cde_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 
            2);

        pnd_Factor_Table = localVariables.newGroupInRecord("pnd_Factor_Table", "#FACTOR-TABLE");
        pnd_Factor_Table_Pnd_Afslash_M = pnd_Factor_Table.newFieldInGroup("pnd_Factor_Table_Pnd_Afslash_M", "#A/M", FieldType.STRING, 7);
        pnd_Factor_Table_Pnd_Funds = pnd_Factor_Table.newFieldInGroup("pnd_Factor_Table_Pnd_Funds", "#FUNDS", FieldType.STRING, 1);
        pnd_Factor_Table_Pnd_Fund_Desc = pnd_Factor_Table.newFieldInGroup("pnd_Factor_Table_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 12);
        pnd_Fctr_Dte = localVariables.newFieldInRecord("pnd_Fctr_Dte", "#FCTR-DTE", FieldType.DATE);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 35);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrl_Rcrd_2.reset();

        localVariables.reset();
        pnd_Len.setInitialValue(10);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap775() throws Exception
    {
        super("Iaap775");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "'CMPRT01'");                                                                                                                       //Natural: DEFINE PRINTER ( 1 ) OUTPUT 'CMPRT01'
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        iaan390_Pnd_Call_Type.setValue("P");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'P'
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "R1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("R1")))
        {
            iaan390_Pnd_Uv_Req_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #UV-REQ-DTE-A
            pnd_Fctr_Dte.setValue(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte);                                                                                                      //Natural: ASSIGN #FCTR-DTE := IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE
            iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte.nsubtract(1);                                                                                                                //Natural: SUBTRACT 1 FROM IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE
            iaan390_Pnd_Issue_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #ISSUE-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Cntrl_Rcrd_2.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-2 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "R2",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R2:
        while (condition(vw_iaa_Cntrl_Rcrd_2.readNextRow("R2")))
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 80
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM GET-UNIT-VALUES
                sub_Get_Unit_Values();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getWorkFiles().write(1, false, iaan390_Pnd_Call_Type);                                                                                                            //Natural: WRITE WORK FILE 1 #CALL-TYPE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-UNIT-VALUES
        //* ***********************************************************************
    }
    private void sub_Get_Unit_Values() throws Exception                                                                                                                   //Natural: GET-UNIT-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cmpny_Fund_Cde.setValue(iaa_Cntrl_Rcrd_2_Cntrl_Fund_Cde.getValue(pnd_I));                                                                                     //Natural: ASSIGN #CMPNY-FUND-CDE := IAA-CNTRL-RCRD-2.CNTRL-FUND-CDE ( #I )
        if (condition(pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde.equals("2") || pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde.equals("U")))                                                      //Natural: IF #CMPNY-CDE = '2' OR = 'U'
        {
            iaan390_Pnd_Reval_Method.setValue("A");                                                                                                                       //Natural: ASSIGN #REVAL-METHOD := 'A'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde.equals("4") || pnd_Cmpny_Fund_Cde_Pnd_Cmpny_Cde.equals("W")))                                                  //Natural: IF #CMPNY-CDE = '4' OR = 'W'
            {
                iaan390_Pnd_Reval_Method.setValue("M");                                                                                                                   //Natural: ASSIGN #REVAL-METHOD := 'M'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaan390_Pnd_Ia_Fund_Cde.reset();                                                                                                                                  //Natural: RESET #IA-FUND-CDE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), iaan390_Pnd_Ia_Fund_Cde, pnd_Cmpny_Fund_Cde_Pnd_Fund_Cde, pnd_Rc);                                     //Natural: CALLNAT 'IAAN0511' #IA-FUND-CDE #FUND-CDE #RC
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Rc.equals(getZero())))                                                                                                                          //Natural: IF #RC = 0
        {
            DbsUtil.callnat(Iaan390.class , getCurrentProcessState(), iaan390);                                                                                           //Natural: CALLNAT 'IAAN390' IAAN390
            if (condition(Global.isEscape())) return;
            if (condition(iaan390_Pnd_Iuv.greater(getZero())))                                                                                                            //Natural: IF #IUV GT 0
            {
                if (condition(iaan390_Pnd_Reval_Method.equals("A")))                                                                                                      //Natural: IF #REVAL-METHOD = 'A'
                {
                    pnd_Factor_Table_Pnd_Afslash_M.setValue("Annual");                                                                                                    //Natural: ASSIGN #A/M := 'Annual'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Factor_Table_Pnd_Afslash_M.setValue("Monthly");                                                                                                   //Natural: ASSIGN #A/M := 'Monthly'
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Iaan051b.class , getCurrentProcessState(), iaan390_Pnd_Ia_Fund_Cde, pnd_Desc, pnd_Cmpny_Desc, pnd_Len);                                   //Natural: CALLNAT 'IAAN051B' #IA-FUND-CDE #DESC #CMPNY-DESC #LEN
                if (condition(Global.isEscape())) return;
                pnd_Factor_Table_Pnd_Fund_Desc.setValue(pnd_Desc);                                                                                                        //Natural: ASSIGN #FUND-DESC := #DESC
                getReports().display(1, new TabSetting(20),"Fund",                                                                                                        //Natural: DISPLAY ( 1 ) 20T 'Fund' #FUND-DESC 'Method' #A/M 'Factor' #IUV ( EM = ZZZ9.9999 )
                		pnd_Factor_Table_Pnd_Fund_Desc,"Method",
                		pnd_Factor_Table_Pnd_Afslash_M,"Factor",
                		iaan390_Pnd_Iuv, new ReportEditMask ("ZZZ9.9999"));
                if (Global.isEscape()) return;
                pnd_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(11),"Unit Value By Fund Report",new               //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 11X 'Unit Value By Fund Report' 60T 'Date:' *DATX / *TIMX 10X 'For Factor Date:' #FCTR-DTE ( EM = MM/DD/YYYY ) 60T 'Page:' *PAGE-NUMBER ( 1 ) /
                        TabSetting(60),"Date:",Global.getDATX(),NEWLINE,Global.getTIMX(),new ColumnSpacing(10),"For Factor Date:",pnd_Fctr_Dte, new ReportEditMask 
                        ("MM/DD/YYYY"),new TabSetting(60),"Page:",getReports().getPageNumberDbs(1),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=132");

        getReports().setDisplayColumns(1, new TabSetting(20),"Fund",
        		pnd_Factor_Table_Pnd_Fund_Desc,"Method",
        		pnd_Factor_Table_Pnd_Afslash_M,"Factor",
        		iaan390_Pnd_Iuv, new ReportEditMask ("ZZZ9.9999"));
    }
}
