/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:07 PM
**        * FROM NATURAL PROGRAM : Iaap723
************************************************************
**        * FILE NAME            : Iaap723.java
**        * CLASS NAME           : Iaap723
**        * INSTANCE NAME        : Iaap723
************************************************************
************************************************************************
* PROGRAM : IAAP723
* DATE    : 3/13/2006
* PURPOSE : CREATES A REPORT OF IA ISSUES FOR A GIVEN MONTH RANGE TO BE
*           ROUTED TO MOBIUS. ANOTHER OUTPUT WILL BE A SEQUENTIAL
*           DATASET. THIS WILL RUN ON THE MONTHEND. TPA, IPRO AND P&I
*           CONTRACTS ARE EXCLUDED.
*
*           INCREASED RATE ARRAY TO 99   DO SCAN 4/08
* 3/12      RATE BASE EXPANSION  DO SCAN 3/12
* 04/2017 OS RE-STOWED ONLY FOR PIN EXPANSION
************************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap723 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_trans;
    private DbsField trans_Trans_Dte;
    private DbsField trans_Invrse_Trans_Dte;
    private DbsField trans_Lst_Trans_Dte;
    private DbsField trans_Trans_Ppcn_Nbr;
    private DbsField trans_Trans_Payee_Cde;
    private DbsField trans_Trans_Sub_Cde;
    private DbsField trans_Trans_Cde;
    private DbsField trans_Trans_Actvty_Cde;
    private DbsField trans_Trans_Check_Dte;
    private DbsField trans_Trans_Todays_Dte;
    private DbsField trans_Trans_User_Area;
    private DbsField trans_Trans_Verify_Cde;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;
    private DbsField cpr_Cntrct_Curr_Dist_Cde;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cntrct_Final_Pay_Dte;
    private DbsField cpr_Cpr_Id_Nbr;

    private DataAccessProgramView vw_tiaa_Fund;
    private DbsField tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField tiaa_Fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField tiaa_Fund_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup tiaa_Fund__R_Field_1;
    private DbsField tiaa_Fund_Cmpny;
    private DbsField tiaa_Fund_Fund;
    private DbsField tiaa_Fund_Tiaa_Tot_Per_Amt;
    private DbsField tiaa_Fund_Tiaa_Tot_Div_Amt;

    private DbsGroup tiaa_Fund_Tiaa_Rate_Data_Grp;
    private DbsField tiaa_Fund_Tiaa_Units_Cnt;
    private DbsField tiaa_Fund_Tiaa_Rate_Cde;
    private DbsField tiaa_Fund_Tiaa_Per_Pay_Amt;
    private DbsField tiaa_Fund_Tiaa_Per_Div_Amt;
    private DbsField tiaa_Fund_Tiaa_Rate_Final_Pay_Amt;
    private DbsField tiaa_Fund_Tiaa_Rate_Final_Div_Amt;
    private DbsGroup tiaa_Fund_Tiaa_Rate_GicMuGroup;
    private DbsField tiaa_Fund_Tiaa_Rate_Gic;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_2;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_3;
    private DbsField pnd_Fund_Key_Pnd_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Payee;
    private DbsField pnd_Fund_Key_Pnd_Cmpny_Fund_Cde;

    private DbsGroup pnd_Fund_Key__R_Field_4;
    private DbsField pnd_Fund_Key_Pnd_Cpr_Key;
    private DbsField pnd_Cnt;
    private DbsField pnd_Rtrn_Cde;

    private DbsGroup aian026;

    private DbsGroup aian026_Pnd_Input;
    private DbsField aian026_Pnd_Call_Type;
    private DbsField aian026_Pnd_Ia_Fund_Code;
    private DbsField aian026_Pnd_Revaluation_Method;
    private DbsField aian026_Pnd_Uv_Req_Dte;

    private DbsGroup aian026__R_Field_5;
    private DbsField aian026_Pnd_Uv_Req_Dte_A;

    private DbsGroup aian026__R_Field_6;
    private DbsField aian026_Pnd_Req_Yyyy;
    private DbsField aian026_Pnd_Req_Mm;
    private DbsField aian026_Pnd_Req_Dd;
    private DbsField aian026_Pnd_Prtcptn_Dte;

    private DbsGroup aian026__R_Field_7;
    private DbsField aian026_Pnd_Prtcptn_Dte_A;
    private DbsField aian026_Pnd_Prtcptn_Dd;

    private DbsGroup aian026_Pnd_Output;
    private DbsField aian026_Pnd_Rtrn_Cd_Pgm;

    private DbsGroup aian026__R_Field_8;
    private DbsField aian026_Pnd_Rtrn_Pgm;
    private DbsField aian026_Pnd_Rtrn_Cd;
    private DbsField aian026_Pnd_Iuv;
    private DbsField aian026_Pnd_Iuv_Dt;

    private DbsGroup aian026__R_Field_9;
    private DbsField aian026_Pnd_Iuv_Dt_A;
    private DbsField aian026_Pnd_Days_In_Request_Month;
    private DbsField aian026_Pnd_Days_In_Particip_Month;
    private DbsField pnd_H_Trans_Ppcn_Nbr;
    private DbsField pnd_H_Trans_Payee_Cde;

    private DbsGroup pnd_Out;
    private DbsField pnd_Out_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Out_Pnd_Payee_Cde;
    private DbsField pnd_Out_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Out_Pnd_Issue_Dte;

    private DbsGroup pnd_Out__R_Field_10;
    private DbsField pnd_Out_Pnd_Iss_Yyyy_Mm;
    private DbsField pnd_Out_Pnd_Iss_Dd;
    private DbsField pnd_Out_Pnd_Final_Per_Pay;
    private DbsField pnd_Out_Pnd_Optn_Cde;
    private DbsField pnd_Out_Pnd_Orgn_Cde;
    private DbsField pnd_Out_Pnd_Mode;
    private DbsField pnd_Out_Pnd_1st_Dob;
    private DbsField pnd_Out_Pnd_2nd_Dob;
    private DbsField pnd_Out_Pnd_1st_Sex;
    private DbsField pnd_Out_Pnd_2nd_Sex;
    private DbsField pnd_Out_Pnd_Sys;

    private DbsGroup pnd_Out_Pnd_Fund_Data;
    private DbsField pnd_Out_Pnd_Fund;
    private DbsField pnd_Out_Pnd_A_M;
    private DbsField pnd_Out_Pnd_Guar_Amt;
    private DbsField pnd_Out_Pnd_Per_Amt;
    private DbsField pnd_Out_Pnd_Units;

    private DbsGroup pnd_Out_Pnd_Std;
    private DbsField pnd_Out_Pnd_Rb;
    private DbsField pnd_Out_Pnd_Gtd;
    private DbsField pnd_Out_Pnd_Div;
    private DbsField pnd_Out_Pnd_Final_Gtd;
    private DbsField pnd_Out_Pnd_Final_Div;

    private DbsGroup pnd_Out_Pnd_Grd;
    private DbsField pnd_Out_Pnd_G_Rb;
    private DbsField pnd_Out_Pnd_G_Gtd;
    private DbsField pnd_Out_Pnd_G_Div;
    private DbsField pnd_Out_Pnd_G_Final_Gtd;
    private DbsField pnd_Out_Pnd_G_Final_Div;
    private DbsField pnd_Pmt_Amt;
    private DbsField pnd_Frm_Dte;

    private DbsGroup pnd_Frm_Dte__R_Field_11;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Yyyy;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Mm;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Dd;
    private DbsField pnd_Thru_Dte;

    private DbsGroup pnd_Thru_Dte__R_Field_12;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Yyyy;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Mm;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Dd;
    private DbsField pnd_Mnthly;
    private DbsField pnd_Annual;
    private DbsField pnd_Mnthly_Amts;
    private DbsField pnd_Annual_Amts;
    private DbsField pnd_Gtd_Std;
    private DbsField pnd_Gtd_Grd;
    private DbsField pnd_First;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Run_Dte;
    private DbsField pnd_Run_Tme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_trans = new DataAccessProgramView(new NameInfo("vw_trans", "TRANS"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trans_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        trans_Invrse_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "INVRSE_TRANS_DTE");
        trans_Lst_Trans_Dte = vw_trans.getRecord().newFieldInGroup("trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        trans_Trans_Ppcn_Nbr = vw_trans.getRecord().newFieldInGroup("trans_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        trans_Trans_Payee_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TRANS_PAYEE_CDE");
        trans_Trans_Sub_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TRANS_SUB_CDE");
        trans_Trans_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trans_Trans_Actvty_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_ACTVTY_CDE");
        trans_Trans_Check_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");
        trans_Trans_Todays_Dte = vw_trans.getRecord().newFieldInGroup("trans_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_TODAYS_DTE");
        trans_Trans_User_Area = vw_trans.getRecord().newFieldInGroup("trans_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TRANS_USER_AREA");
        trans_Trans_Verify_Cde = vw_trans.getRecord().newFieldInGroup("trans_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TRANS_VERIFY_CDE");
        registerRecord(vw_trans);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        cpr_Cntrct_Curr_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cntrct_Final_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_FINAL_PAY_DTE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        registerRecord(vw_cpr);

        vw_tiaa_Fund = new DataAccessProgramView(new NameInfo("vw_tiaa_Fund", "TIAA-FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr = vw_tiaa_Fund.getRecord().newFieldInGroup("TIAA_FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        tiaa_Fund_Tiaa_Cntrct_Payee_Cde = vw_tiaa_Fund.getRecord().newFieldInGroup("tiaa_Fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        tiaa_Fund_Tiaa_Cmpny_Fund_Cde = vw_tiaa_Fund.getRecord().newFieldInGroup("tiaa_Fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        tiaa_Fund__R_Field_1 = vw_tiaa_Fund.getRecord().newGroupInGroup("tiaa_Fund__R_Field_1", "REDEFINE", tiaa_Fund_Tiaa_Cmpny_Fund_Cde);
        tiaa_Fund_Cmpny = tiaa_Fund__R_Field_1.newFieldInGroup("tiaa_Fund_Cmpny", "CMPNY", FieldType.STRING, 1);
        tiaa_Fund_Fund = tiaa_Fund__R_Field_1.newFieldInGroup("tiaa_Fund_Fund", "FUND", FieldType.STRING, 2);
        tiaa_Fund_Tiaa_Tot_Per_Amt = vw_tiaa_Fund.getRecord().newFieldInGroup("TIAA_FUND_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        tiaa_Fund_Tiaa_Tot_Div_Amt = vw_tiaa_Fund.getRecord().newFieldInGroup("tiaa_Fund_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");

        tiaa_Fund_Tiaa_Rate_Data_Grp = vw_tiaa_Fund.getRecord().newGroupInGroup("tiaa_Fund_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Units_Cnt = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("TIAA_FUND_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Rate_Cde = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("TIAA_FUND_TIAA_RATE_CDE", "TIAA-RATE-CDE", FieldType.STRING, 2, new 
            DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Per_Pay_Amt = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("tiaa_Fund_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Per_Div_Amt = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("tiaa_Fund_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Rate_Final_Pay_Amt = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("tiaa_Fund_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Rate_Final_Div_Amt = tiaa_Fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("tiaa_Fund_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        tiaa_Fund_Tiaa_Rate_GicMuGroup = vw_tiaa_Fund.getRecord().newGroupInGroup("TIAA_FUND_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        tiaa_Fund_Tiaa_Rate_Gic = tiaa_Fund_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("tiaa_Fund_Tiaa_Rate_Gic", "TIAA-RATE-GIC", FieldType.NUMERIC, 11, 
            new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        registerRecord(vw_tiaa_Fund);

        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_2", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", 
            FieldType.STRING, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde", "#TRANS-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Chck_Dte_Key__R_Field_2.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Invrse_Trans_Dte", 
            "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_3", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Ppcn = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Payee = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Payee", "#PAYEE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Cmpny_Fund_Cde = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Cmpny_Fund_Cde", "#CMPNY-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Fund_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_4", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Cpr_Key = pnd_Fund_Key__R_Field_4.newFieldInGroup("pnd_Fund_Key_Pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rtrn_Cde = localVariables.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);

        aian026 = localVariables.newGroupInRecord("aian026", "AIAN026");

        aian026_Pnd_Input = aian026.newGroupInGroup("aian026_Pnd_Input", "#INPUT");
        aian026_Pnd_Call_Type = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 1);
        aian026_Pnd_Ia_Fund_Code = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Ia_Fund_Code", "#IA-FUND-CODE", FieldType.STRING, 1);
        aian026_Pnd_Revaluation_Method = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Revaluation_Method", "#REVALUATION-METHOD", FieldType.STRING, 
            1);
        aian026_Pnd_Uv_Req_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Uv_Req_Dte", "#UV-REQ-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_5 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_5", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Uv_Req_Dte_A = aian026__R_Field_5.newFieldInGroup("aian026_Pnd_Uv_Req_Dte_A", "#UV-REQ-DTE-A", FieldType.STRING, 8);

        aian026__R_Field_6 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_6", "REDEFINE", aian026_Pnd_Uv_Req_Dte);
        aian026_Pnd_Req_Yyyy = aian026__R_Field_6.newFieldInGroup("aian026_Pnd_Req_Yyyy", "#REQ-YYYY", FieldType.NUMERIC, 4);
        aian026_Pnd_Req_Mm = aian026__R_Field_6.newFieldInGroup("aian026_Pnd_Req_Mm", "#REQ-MM", FieldType.NUMERIC, 2);
        aian026_Pnd_Req_Dd = aian026__R_Field_6.newFieldInGroup("aian026_Pnd_Req_Dd", "#REQ-DD", FieldType.NUMERIC, 2);
        aian026_Pnd_Prtcptn_Dte = aian026_Pnd_Input.newFieldInGroup("aian026_Pnd_Prtcptn_Dte", "#PRTCPTN-DTE", FieldType.NUMERIC, 8);

        aian026__R_Field_7 = aian026_Pnd_Input.newGroupInGroup("aian026__R_Field_7", "REDEFINE", aian026_Pnd_Prtcptn_Dte);
        aian026_Pnd_Prtcptn_Dte_A = aian026__R_Field_7.newFieldInGroup("aian026_Pnd_Prtcptn_Dte_A", "#PRTCPTN-DTE-A", FieldType.STRING, 6);
        aian026_Pnd_Prtcptn_Dd = aian026__R_Field_7.newFieldInGroup("aian026_Pnd_Prtcptn_Dd", "#PRTCPTN-DD", FieldType.STRING, 2);

        aian026_Pnd_Output = aian026.newGroupInGroup("aian026_Pnd_Output", "#OUTPUT");
        aian026_Pnd_Rtrn_Cd_Pgm = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Rtrn_Cd_Pgm", "#RTRN-CD-PGM", FieldType.STRING, 11);

        aian026__R_Field_8 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_8", "REDEFINE", aian026_Pnd_Rtrn_Cd_Pgm);
        aian026_Pnd_Rtrn_Pgm = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Rtrn_Pgm", "#RTRN-PGM", FieldType.STRING, 8);
        aian026_Pnd_Rtrn_Cd = aian026__R_Field_8.newFieldInGroup("aian026_Pnd_Rtrn_Cd", "#RTRN-CD", FieldType.NUMERIC, 3);
        aian026_Pnd_Iuv = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv", "#IUV", FieldType.NUMERIC, 8, 4);
        aian026_Pnd_Iuv_Dt = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Iuv_Dt", "#IUV-DT", FieldType.NUMERIC, 8);

        aian026__R_Field_9 = aian026_Pnd_Output.newGroupInGroup("aian026__R_Field_9", "REDEFINE", aian026_Pnd_Iuv_Dt);
        aian026_Pnd_Iuv_Dt_A = aian026__R_Field_9.newFieldInGroup("aian026_Pnd_Iuv_Dt_A", "#IUV-DT-A", FieldType.STRING, 8);
        aian026_Pnd_Days_In_Request_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Request_Month", "#DAYS-IN-REQUEST-MONTH", FieldType.NUMERIC, 
            2);
        aian026_Pnd_Days_In_Particip_Month = aian026_Pnd_Output.newFieldInGroup("aian026_Pnd_Days_In_Particip_Month", "#DAYS-IN-PARTICIP-MONTH", FieldType.NUMERIC, 
            2);
        pnd_H_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_H_Trans_Ppcn_Nbr", "#H-TRANS-PPCN-NBR", FieldType.STRING, 10);
        pnd_H_Trans_Payee_Cde = localVariables.newFieldInRecord("pnd_H_Trans_Payee_Cde", "#H-TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Out = localVariables.newGroupInRecord("pnd_Out", "#OUT");
        pnd_Out_Pnd_Cntrct_Ppcn_Nbr = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Out_Pnd_Payee_Cde = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Out_Pnd_Lst_Trans_Dte = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.STRING, 8);
        pnd_Out_Pnd_Issue_Dte = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.STRING, 8);

        pnd_Out__R_Field_10 = pnd_Out.newGroupInGroup("pnd_Out__R_Field_10", "REDEFINE", pnd_Out_Pnd_Issue_Dte);
        pnd_Out_Pnd_Iss_Yyyy_Mm = pnd_Out__R_Field_10.newFieldInGroup("pnd_Out_Pnd_Iss_Yyyy_Mm", "#ISS-YYYY-MM", FieldType.NUMERIC, 6);
        pnd_Out_Pnd_Iss_Dd = pnd_Out__R_Field_10.newFieldInGroup("pnd_Out_Pnd_Iss_Dd", "#ISS-DD", FieldType.NUMERIC, 2);
        pnd_Out_Pnd_Final_Per_Pay = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Final_Per_Pay", "#FINAL-PER-PAY", FieldType.STRING, 6);
        pnd_Out_Pnd_Optn_Cde = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.STRING, 2);
        pnd_Out_Pnd_Orgn_Cde = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Out_Pnd_Mode = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Mode", "#MODE", FieldType.STRING, 3);
        pnd_Out_Pnd_1st_Dob = pnd_Out.newFieldInGroup("pnd_Out_Pnd_1st_Dob", "#1ST-DOB", FieldType.STRING, 8);
        pnd_Out_Pnd_2nd_Dob = pnd_Out.newFieldInGroup("pnd_Out_Pnd_2nd_Dob", "#2ND-DOB", FieldType.STRING, 8);
        pnd_Out_Pnd_1st_Sex = pnd_Out.newFieldInGroup("pnd_Out_Pnd_1st_Sex", "#1ST-SEX", FieldType.STRING, 1);
        pnd_Out_Pnd_2nd_Sex = pnd_Out.newFieldInGroup("pnd_Out_Pnd_2nd_Sex", "#2ND-SEX", FieldType.STRING, 1);
        pnd_Out_Pnd_Sys = pnd_Out.newFieldInGroup("pnd_Out_Pnd_Sys", "#SYS", FieldType.STRING, 4);

        pnd_Out_Pnd_Fund_Data = pnd_Out.newGroupArrayInGroup("pnd_Out_Pnd_Fund_Data", "#FUND-DATA", new DbsArrayController(1, 22));
        pnd_Out_Pnd_Fund = pnd_Out_Pnd_Fund_Data.newFieldInGroup("pnd_Out_Pnd_Fund", "#FUND", FieldType.STRING, 1);
        pnd_Out_Pnd_A_M = pnd_Out_Pnd_Fund_Data.newFieldInGroup("pnd_Out_Pnd_A_M", "#A-M", FieldType.STRING, 1);
        pnd_Out_Pnd_Guar_Amt = pnd_Out_Pnd_Fund_Data.newFieldInGroup("pnd_Out_Pnd_Guar_Amt", "#GUAR-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Out_Pnd_Per_Amt = pnd_Out_Pnd_Fund_Data.newFieldInGroup("pnd_Out_Pnd_Per_Amt", "#PER-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Out_Pnd_Units = pnd_Out_Pnd_Fund_Data.newFieldInGroup("pnd_Out_Pnd_Units", "#UNITS", FieldType.NUMERIC, 9, 3);

        pnd_Out_Pnd_Std = pnd_Out.newGroupArrayInGroup("pnd_Out_Pnd_Std", "#STD", new DbsArrayController(1, 250));
        pnd_Out_Pnd_Rb = pnd_Out_Pnd_Std.newFieldInGroup("pnd_Out_Pnd_Rb", "#RB", FieldType.STRING, 2);
        pnd_Out_Pnd_Gtd = pnd_Out_Pnd_Std.newFieldInGroup("pnd_Out_Pnd_Gtd", "#GTD", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_Div = pnd_Out_Pnd_Std.newFieldInGroup("pnd_Out_Pnd_Div", "#DIV", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_Final_Gtd = pnd_Out_Pnd_Std.newFieldInGroup("pnd_Out_Pnd_Final_Gtd", "#FINAL-GTD", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_Final_Div = pnd_Out_Pnd_Std.newFieldInGroup("pnd_Out_Pnd_Final_Div", "#FINAL-DIV", FieldType.NUMERIC, 9, 2);

        pnd_Out_Pnd_Grd = pnd_Out.newGroupArrayInGroup("pnd_Out_Pnd_Grd", "#GRD", new DbsArrayController(1, 250));
        pnd_Out_Pnd_G_Rb = pnd_Out_Pnd_Grd.newFieldInGroup("pnd_Out_Pnd_G_Rb", "#G-RB", FieldType.STRING, 2);
        pnd_Out_Pnd_G_Gtd = pnd_Out_Pnd_Grd.newFieldInGroup("pnd_Out_Pnd_G_Gtd", "#G-GTD", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_G_Div = pnd_Out_Pnd_Grd.newFieldInGroup("pnd_Out_Pnd_G_Div", "#G-DIV", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_G_Final_Gtd = pnd_Out_Pnd_Grd.newFieldInGroup("pnd_Out_Pnd_G_Final_Gtd", "#G-FINAL-GTD", FieldType.NUMERIC, 9, 2);
        pnd_Out_Pnd_G_Final_Div = pnd_Out_Pnd_Grd.newFieldInGroup("pnd_Out_Pnd_G_Final_Div", "#G-FINAL-DIV", FieldType.NUMERIC, 9, 2);
        pnd_Pmt_Amt = localVariables.newFieldInRecord("pnd_Pmt_Amt", "#PMT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Frm_Dte = localVariables.newFieldInRecord("pnd_Frm_Dte", "#FRM-DTE", FieldType.STRING, 8);

        pnd_Frm_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_Frm_Dte__R_Field_11", "REDEFINE", pnd_Frm_Dte);
        pnd_Frm_Dte_Pnd_Frm_Yyyy = pnd_Frm_Dte__R_Field_11.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Yyyy", "#FRM-YYYY", FieldType.NUMERIC, 4);
        pnd_Frm_Dte_Pnd_Frm_Mm = pnd_Frm_Dte__R_Field_11.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Mm", "#FRM-MM", FieldType.NUMERIC, 2);
        pnd_Frm_Dte_Pnd_Frm_Dd = pnd_Frm_Dte__R_Field_11.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Dd", "#FRM-DD", FieldType.NUMERIC, 2);
        pnd_Thru_Dte = localVariables.newFieldInRecord("pnd_Thru_Dte", "#THRU-DTE", FieldType.STRING, 8);

        pnd_Thru_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_Thru_Dte__R_Field_12", "REDEFINE", pnd_Thru_Dte);
        pnd_Thru_Dte_Pnd_Thru_Yyyy = pnd_Thru_Dte__R_Field_12.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Yyyy", "#THRU-YYYY", FieldType.NUMERIC, 4);
        pnd_Thru_Dte_Pnd_Thru_Mm = pnd_Thru_Dte__R_Field_12.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Mm", "#THRU-MM", FieldType.NUMERIC, 2);
        pnd_Thru_Dte_Pnd_Thru_Dd = pnd_Thru_Dte__R_Field_12.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Dd", "#THRU-DD", FieldType.NUMERIC, 2);
        pnd_Mnthly = localVariables.newFieldArrayInRecord("pnd_Mnthly", "#MNTHLY", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_Annual = localVariables.newFieldArrayInRecord("pnd_Annual", "#ANNUAL", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_Mnthly_Amts = localVariables.newFieldArrayInRecord("pnd_Mnthly_Amts", "#MNTHLY-AMTS", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));
        pnd_Annual_Amts = localVariables.newFieldArrayInRecord("pnd_Annual_Amts", "#ANNUAL-AMTS", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));
        pnd_Gtd_Std = localVariables.newFieldInRecord("pnd_Gtd_Std", "#GTD-STD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Gtd_Grd = localVariables.newFieldInRecord("pnd_Gtd_Grd", "#GTD-GRD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Run_Dte = localVariables.newFieldInRecord("pnd_Run_Dte", "#RUN-DTE", FieldType.TIME);
        pnd_Run_Tme = localVariables.newFieldInRecord("pnd_Run_Tme", "#RUN-TME", FieldType.TIME);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trans.reset();
        vw_cntrl.reset();
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();
        vw_tiaa_Fund.reset();

        localVariables.reset();
        pnd_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap723() throws Exception
    {
        super("Iaap723");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133 ZP = OFF
        pnd_Run_Dte.setValue(Global.getTIMX());                                                                                                                           //Natural: ASSIGN #RUN-DTE := *TIMX
        pnd_Run_Tme.setValue(Global.getTIMX());                                                                                                                           //Natural: ASSIGN #RUN-TME := *TIMX
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 2 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        2
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            aian026_Pnd_Uv_Req_Dte_A.setValueEdited(cntrl_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #UV-REQ-DTE-A
            pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte.setValueEdited(cntrl_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #TRANS-CHECK-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cntrl.readNextRow("READ02")))
        {
            if (condition(cntrl_Cntrl_Cde.notEquals("DC")))                                                                                                               //Natural: IF CNTRL-CDE NE 'DC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Thru_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #THRU-DTE
                if (condition(pnd_Thru_Dte_Pnd_Thru_Dd.lessOrEqual(20)))                                                                                                  //Natural: IF #THRU-DD LE 20
                {
                    DbsUtil.terminate(0);  if (true) return;                                                                                                              //Natural: TERMINATE 00
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Frm_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #FRM-DTE
            if (condition(pnd_Frm_Dte_Pnd_Frm_Mm.notEquals(pnd_Thru_Dte_Pnd_Thru_Mm)))                                                                                    //Natural: IF #FRM-MM NE #THRU-MM
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_trans.startDatabaseRead                                                                                                                                        //Natural: READ TRANS BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_trans.readNextRow("READ03")))
        {
            pnd_Out_Pnd_Lst_Trans_Dte.setValueEdited(trans_Lst_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED TRANS.LST-TRANS-DTE ( EM = YYYYMMDD ) TO #LST-TRANS-DTE
            if (condition(!(((((trans_Trans_Cde.equals(30) || trans_Trans_Cde.equals(31)) || trans_Trans_Cde.equals(33)) && (pnd_Out_Pnd_Lst_Trans_Dte.greaterOrEqual(pnd_Frm_Dte)  //Natural: ACCEPT IF ( TRANS-CDE = 30 OR = 31 OR = 33 ) AND #LST-TRANS-DTE = #FRM-DTE THRU #THRU-DTE AND TRANS-VERIFY-CDE NE 'V'
                && pnd_Out_Pnd_Lst_Trans_Dte.lessOrEqual(pnd_Thru_Dte))) && trans_Trans_Verify_Cde.notEquals("V")))))
            {
                continue;
            }
            short decideConditionsMet264 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE TRANS-CDE;//Natural: VALUE 30
            if (condition((trans_Trans_Cde.equals(30))))
            {
                decideConditionsMet264++;
                pnd_Out_Pnd_Sys.setValue("ADAM");                                                                                                                         //Natural: ASSIGN #SYS := 'ADAM'
            }                                                                                                                                                             //Natural: VALUE 31
            else if (condition((trans_Trans_Cde.equals(31))))
            {
                decideConditionsMet264++;
                pnd_Out_Pnd_Sys.setValue("ADAS");                                                                                                                         //Natural: ASSIGN #SYS := 'ADAS'
            }                                                                                                                                                             //Natural: VALUE 33
            else if (condition((trans_Trans_Cde.equals(33))))
            {
                decideConditionsMet264++;
                if (condition(trans_Trans_Ppcn_Nbr.equals(pnd_H_Trans_Ppcn_Nbr) && trans_Trans_Payee_Cde.equals(pnd_H_Trans_Payee_Cde)))                                  //Natural: IF TRANS-PPCN-NBR = #H-TRANS-PPCN-NBR AND TRANS-PAYEE-CDE = #H-TRANS-PAYEE-CDE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Out_Pnd_Sys.setValue("IAIQ");                                                                                                                         //Natural: ASSIGN #SYS := 'IAIQ'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_H_Trans_Ppcn_Nbr.setValue(trans_Trans_Ppcn_Nbr);                                                                                                          //Natural: ASSIGN #H-TRANS-PPCN-NBR := TRANS-PPCN-NBR
            pnd_H_Trans_Payee_Cde.setValue(trans_Trans_Payee_Cde);                                                                                                        //Natural: ASSIGN #H-TRANS-PAYEE-CDE := TRANS-PAYEE-CDE
            vw_iaa_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = TRANS-PPCN-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", trans_Trans_Ppcn_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_iaa_Cntrct.readNextRow("FIND01")))
            {
                vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
                if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(25) || iaa_Cntrct_Cntrct_Optn_Cde.equals(27) || iaa_Cntrct_Cntrct_Optn_Cde.equals(22)                     //Natural: IF CNTRCT-OPTN-CDE = 25 OR = 27 OR = 22 OR = 28 OR = 30
                    || iaa_Cntrct_Cntrct_Optn_Cde.equals(28) || iaa_Cntrct_Cntrct_Optn_Cde.equals(30)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Out_Pnd_Iss_Yyyy_Mm.setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                                            //Natural: ASSIGN #ISS-YYYY-MM := CNTRCT-ISSUE-DTE
                pnd_Out_Pnd_Iss_Dd.setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                                                              //Natural: ASSIGN #ISS-DD := CNTRCT-ISSUE-DTE-DD
                pnd_Out_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                         //Natural: ASSIGN #CNTRCT-PPCN-NBR := CNTRCT-PPCN-NBR
                pnd_Out_Pnd_Payee_Cde.setValue(trans_Trans_Payee_Cde);                                                                                                    //Natural: ASSIGN #PAYEE-CDE := TRANS-PAYEE-CDE
                pnd_Out_Pnd_Orgn_Cde.setValueEdited(iaa_Cntrct_Cntrct_Orgn_Cde,new ReportEditMask("99"));                                                                 //Natural: MOVE EDITED CNTRCT-ORGN-CDE ( EM = 99 ) TO #ORGN-CDE
                pnd_Out_Pnd_Optn_Cde.setValueEdited(iaa_Cntrct_Cntrct_Optn_Cde,new ReportEditMask("99"));                                                                 //Natural: MOVE EDITED CNTRCT-OPTN-CDE ( EM = 99 ) TO #OPTN-CDE
                pnd_Out_Pnd_1st_Dob.setValue(iaa_Cntrct_Cntrct_First_Annt_Dob_Dte);                                                                                       //Natural: ASSIGN #1ST-DOB := CNTRCT-FIRST-ANNT-DOB-DTE
                pnd_Out_Pnd_2nd_Dob.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte);                                                                                        //Natural: ASSIGN #2ND-DOB := CNTRCT-SCND-ANNT-DOB-DTE
                pnd_Out_Pnd_1st_Sex.setValue(iaa_Cntrct_Cntrct_First_Annt_Sex_Cde);                                                                                       //Natural: ASSIGN #1ST-SEX := CNTRCT-FIRST-ANNT-SEX-CDE
                pnd_Out_Pnd_2nd_Sex.setValue(iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde);                                                                                        //Natural: ASSIGN #2ND-SEX := CNTRCT-SCND-ANNT-SEX-CDE
                                                                                                                                                                          //Natural: PERFORM GET-CPR-FUND-RCRD
                sub_Get_Cpr_Fund_Rcrd();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,"TOTAL IA NEW ISSUE SETTLEMENTS FROM ",pnd_Frm_Dte,"THRU",pnd_Thru_Dte,":",pnd_Cnt, //Natural: WRITE ( 1 ) NOTITLE NOHDR // 'TOTAL IA NEW ISSUE SETTLEMENTS FROM ' #FRM-DTE 'THRU' #THRU-DTE ':' #CNT ( EM = Z,ZZZ,ZZ9 ) //
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pnd_Gtd_Std.greater(getZero())))                                                                                                                    //Natural: IF #GTD-STD GT 0
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL GUARANTEED PAYMENT - STD METHOD:",pnd_Gtd_Std, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL GUARANTEED PAYMENT - STD METHOD:' #GTD-STD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Gtd_Grd.greater(getZero())))                                                                                                                    //Natural: IF #GTD-GRD GT 0
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL GUARANTEED PAYMENT - GRD METHOD:",pnd_Gtd_Grd, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL GUARANTEED PAYMENT - GRD METHOD:' #GTD-GRD ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(pnd_Annual_Amts.getValue(pnd_I).greater(getZero())))                                                                                            //Natural: IF #ANNUAL-AMTS ( #I ) GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL PAYMENT AMOUNT FOR",pnd_Annual.getValue(pnd_I),"- ANNUAL:  ",pnd_Annual_Amts.getValue(pnd_I),  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL PAYMENT AMOUNT FOR' #ANNUAL ( #I ) '- ANNUAL:  ' #ANNUAL-AMTS ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(pnd_Mnthly_Amts.getValue(pnd_I).greater(getZero())))                                                                                            //Natural: IF #MNTHLY-AMTS ( #I ) GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL PAYMENT AMOUNT FOR",pnd_Mnthly.getValue(pnd_I),"- MONTHLY: ",pnd_Mnthly_Amts.getValue(pnd_I),  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL PAYMENT AMOUNT FOR' #MNTHLY ( #I ) '- MONTHLY: ' #MNTHLY-AMTS ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR-FUND-RCRD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FACTOR
        //* ***********************************************************************
    }
    private void sub_Get_Cpr_Fund_Rcrd() throws Exception                                                                                                                 //Natural: GET-CPR-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Key_Pnd_Ppcn.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                                       //Natural: ASSIGN #PPCN := CNTRCT-PPCN-NBR
        pnd_Fund_Key_Pnd_Payee.setValue(pnd_Out_Pnd_Payee_Cde);                                                                                                           //Natural: ASSIGN #PAYEE := #PAYEE-CDE
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) CPR WITH CNTRCT-PAYEE-KEY = #CPR-KEY
        (
        "READ04",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Fund_Key_Pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_cpr.readNextRow("READ04")))
        {
            if (condition(cpr_Cntrct_Actvty_Cde.notEquals(9)))                                                                                                            //Natural: IF CNTRCT-ACTVTY-CDE NE 9
            {
                pnd_Out_Pnd_Final_Per_Pay.setValue(cpr_Cntrct_Final_Per_Pay_Dte);                                                                                         //Natural: ASSIGN #FINAL-PER-PAY := CNTRCT-FINAL-PER-PAY-DTE
                pnd_Out_Pnd_Mode.setValue(cpr_Cntrct_Mode_Ind);                                                                                                           //Natural: ASSIGN #MODE := CNTRCT-MODE-IND
                                                                                                                                                                          //Natural: PERFORM GET-FUND-DATA
                sub_Get_Fund_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Fund_Data() throws Exception                                                                                                                     //Natural: GET-FUND-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #FUND ( * ) #PER-AMT ( * ) #A-M ( * ) #GUAR-AMT ( * ) #UNITS ( * ) #PER-AMT ( * ) #RB ( * ) #GTD ( * ) #DIV ( * ) #FINAL-GTD ( * ) #FINAL-DIV ( * ) #G-RB ( * ) #G-GTD ( * ) #G-DIV ( * ) #G-FINAL-GTD ( * ) #G-FINAL-DIV ( * )
        pnd_Out_Pnd_Fund.getValue("*").reset();
        pnd_Out_Pnd_Per_Amt.getValue("*").reset();
        pnd_Out_Pnd_A_M.getValue("*").reset();
        pnd_Out_Pnd_Guar_Amt.getValue("*").reset();
        pnd_Out_Pnd_Units.getValue("*").reset();
        pnd_Out_Pnd_Per_Amt.getValue("*").reset();
        pnd_Out_Pnd_Rb.getValue("*").reset();
        pnd_Out_Pnd_Gtd.getValue("*").reset();
        pnd_Out_Pnd_Div.getValue("*").reset();
        pnd_Out_Pnd_Final_Gtd.getValue("*").reset();
        pnd_Out_Pnd_Final_Div.getValue("*").reset();
        pnd_Out_Pnd_G_Rb.getValue("*").reset();
        pnd_Out_Pnd_G_Gtd.getValue("*").reset();
        pnd_Out_Pnd_G_Div.getValue("*").reset();
        pnd_Out_Pnd_G_Final_Gtd.getValue("*").reset();
        pnd_Out_Pnd_G_Final_Div.getValue("*").reset();
        vw_tiaa_Fund.startDatabaseRead                                                                                                                                    //Natural: READ TIAA-FUND BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
        (
        "READ05",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ05:
        while (condition(vw_tiaa_Fund.readNextRow("READ05")))
        {
            if (condition(tiaa_Fund_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Fund_Key_Pnd_Ppcn) || tiaa_Fund_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Fund_Key_Pnd_Payee)))          //Natural: IF TIAA-CNTRCT-PPCN-NBR NE #PPCN OR TIAA-CNTRCT-PAYEE-CDE NE #PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            if (condition(pnd_I.greater(22)))                                                                                                                             //Natural: IF #I GT 22
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Out_Pnd_Fund.getValue(pnd_I), tiaa_Fund_Fund, pnd_Rtrn_Cde);                                   //Natural: CALLNAT 'IAAN0511' #FUND ( #I ) FUND #RTRN-CDE
            if (condition(Global.isEscape())) return;
            if (condition(tiaa_Fund_Cmpny.equals("4") || tiaa_Fund_Cmpny.equals("W")))                                                                                    //Natural: IF CMPNY = '4' OR = 'W'
            {
                pnd_Out_Pnd_A_M.getValue(pnd_I).setValue("M");                                                                                                            //Natural: ASSIGN #A-M ( #I ) := 'M'
                pnd_Out_Pnd_Units.getValue(pnd_I).setValue(tiaa_Fund_Tiaa_Units_Cnt.getValue(1));                                                                         //Natural: ASSIGN #UNITS ( #I ) := TIAA-UNITS-CNT ( 1 )
                if (condition(pnd_Out_Pnd_Fund.getValue(pnd_I).equals(pnd_Mnthly.getValue("*"))))                                                                         //Natural: IF #FUND ( #I ) = #MNTHLY ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Mnthly.getValue(pnd_J).setValue(pnd_Out_Pnd_Fund.getValue(pnd_I));                                                                                //Natural: ASSIGN #MNTHLY ( #J ) := #FUND ( #I )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-FACTOR
                sub_Get_Factor();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Mnthly_Amts.getValue(pnd_J).nadd(tiaa_Fund_Tiaa_Tot_Per_Amt);                                                                                         //Natural: ADD TIAA-TOT-PER-AMT TO #MNTHLY-AMTS ( #J )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(tiaa_Fund_Cmpny.equals("2") || tiaa_Fund_Cmpny.equals("U")))                                                                                    //Natural: IF CMPNY = '2' OR = 'U'
            {
                pnd_Out_Pnd_A_M.getValue(pnd_I).setValue("A");                                                                                                            //Natural: ASSIGN #A-M ( #I ) := 'A'
                pnd_Out_Pnd_Units.getValue(pnd_I).setValue(tiaa_Fund_Tiaa_Units_Cnt.getValue(1));                                                                         //Natural: ASSIGN #UNITS ( #I ) := TIAA-UNITS-CNT ( 1 )
                if (condition(pnd_Out_Pnd_Fund.getValue(pnd_I).equals(pnd_Annual.getValue("*"))))                                                                         //Natural: IF #FUND ( #I ) = #ANNUAL ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                    pnd_Annual.getValue(pnd_K).setValue(pnd_Out_Pnd_Fund.getValue(pnd_I));                                                                                //Natural: ASSIGN #ANNUAL ( #K ) := #FUND ( #I )
                }                                                                                                                                                         //Natural: END-IF
                tiaa_Fund_Tiaa_Tot_Div_Amt.reset();                                                                                                                       //Natural: RESET TIAA-TOT-DIV-AMT
                pnd_Annual_Amts.getValue(pnd_K).nadd(tiaa_Fund_Tiaa_Tot_Per_Amt);                                                                                         //Natural: ADD TIAA-TOT-PER-AMT TO #ANNUAL-AMTS ( #K )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Out_Pnd_Per_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Out_Pnd_Per_Amt.getValue(pnd_I)), tiaa_Fund_Tiaa_Tot_Per_Amt.add(tiaa_Fund_Tiaa_Tot_Div_Amt)); //Natural: ASSIGN #PER-AMT ( #I ) := TIAA-TOT-PER-AMT + TIAA-TOT-DIV-AMT
            if (condition(tiaa_Fund_Cmpny.equals("T")))                                                                                                                   //Natural: IF CMPNY = 'T'
            {
                pnd_Out_Pnd_Guar_Amt.getValue(pnd_I).setValue(tiaa_Fund_Tiaa_Tot_Per_Amt);                                                                                //Natural: ASSIGN #GUAR-AMT ( #I ) := TIAA-TOT-PER-AMT
                if (condition(tiaa_Fund_Fund.equals("1G")))                                                                                                               //Natural: IF FUND = '1G'
                {
                    pnd_Gtd_Grd.nadd(tiaa_Fund_Tiaa_Tot_Per_Amt);                                                                                                         //Natural: ADD TIAA-TOT-PER-AMT TO #GTD-GRD
                    pnd_Out_Pnd_G_Rb.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Cde.getValue("*"));                                                                       //Natural: ASSIGN #G-RB ( * ) := TIAA-RATE-CDE ( * )
                    pnd_Out_Pnd_G_Gtd.getValue("*").setValue(tiaa_Fund_Tiaa_Per_Pay_Amt.getValue("*"));                                                                   //Natural: ASSIGN #G-GTD ( * ) := TIAA-PER-PAY-AMT ( * )
                    pnd_Out_Pnd_G_Div.getValue("*").setValue(tiaa_Fund_Tiaa_Per_Div_Amt.getValue("*"));                                                                   //Natural: ASSIGN #G-DIV ( * ) := TIAA-PER-DIV-AMT ( * )
                    pnd_Out_Pnd_G_Final_Gtd.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                      //Natural: ASSIGN #G-FINAL-GTD ( * ) := TIAA-RATE-FINAL-PAY-AMT ( * )
                    pnd_Out_Pnd_G_Final_Div.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                      //Natural: ASSIGN #G-FINAL-DIV ( * ) := TIAA-RATE-FINAL-DIV-AMT ( * )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Gtd_Std.nadd(tiaa_Fund_Tiaa_Tot_Per_Amt);                                                                                                         //Natural: ADD TIAA-TOT-PER-AMT TO #GTD-STD
                    pnd_Out_Pnd_Rb.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Cde.getValue("*"));                                                                         //Natural: ASSIGN #RB ( * ) := TIAA-RATE-CDE ( * )
                    pnd_Out_Pnd_Gtd.getValue("*").setValue(tiaa_Fund_Tiaa_Per_Pay_Amt.getValue("*"));                                                                     //Natural: ASSIGN #GTD ( * ) := TIAA-PER-PAY-AMT ( * )
                    pnd_Out_Pnd_Div.getValue("*").setValue(tiaa_Fund_Tiaa_Per_Div_Amt.getValue("*"));                                                                     //Natural: ASSIGN #DIV ( * ) := TIAA-PER-DIV-AMT ( * )
                    pnd_Out_Pnd_Final_Gtd.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Final_Pay_Amt.getValue("*"));                                                        //Natural: ASSIGN #FINAL-GTD ( * ) := TIAA-RATE-FINAL-PAY-AMT ( * )
                    pnd_Out_Pnd_Final_Div.getValue("*").setValue(tiaa_Fund_Tiaa_Rate_Final_Div_Amt.getValue("*"));                                                        //Natural: ASSIGN #FINAL-DIV ( * ) := TIAA-RATE-FINAL-DIV-AMT ( * )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  3/12
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            getReports().display(1, "/Contract",                                                                                                                          //Natural: DISPLAY ( 1 ) '/Contract' #CNTRCT-PPCN-NBR '/Pye' #PAYEE-CDE ( EM = 99 ) 'Lst Trans/Dte' #LST-TRANS-DTE 'Issue/Dte' #ISSUE-DTE 'Final Per/Pay Dte' #FINAL-PER-PAY 'Opt/Cde' #OPTN-CDE 'Orgn/Cde' #ORGN-CDE '/Mde' #MODE '1st/DOB' #1ST-DOB '2nd/DOB' #2ND-DOB '1st/Sex' #1ST-SEX '2nd/Sex' #2ND-SEX '/Sys' #SYS 'Acct/Cde' #FUND ( 1:#I ) 'A/M' #A-M ( 1:#I ) 'Guar/Amount' #GUAR-AMT ( 1:#I ) ( EM = Z,ZZZ,ZZ9.99 ) '/Units' #UNITS ( 1:#I ) ( EM = ZZZ,ZZ9.999 ) 'Paymnt/Amount' #PER-AMT ( 1:#I ) ( EM = Z,ZZZ,ZZ9.99 )
            		pnd_Out_Pnd_Cntrct_Ppcn_Nbr,"/Pye",
            		pnd_Out_Pnd_Payee_Cde, new ReportEditMask ("99"),"Lst Trans/Dte",
            		pnd_Out_Pnd_Lst_Trans_Dte,"Issue/Dte",
            		pnd_Out_Pnd_Issue_Dte,"Final Per/Pay Dte",
            		pnd_Out_Pnd_Final_Per_Pay,"Opt/Cde",
            		pnd_Out_Pnd_Optn_Cde,"Orgn/Cde",
            		pnd_Out_Pnd_Orgn_Cde,"/Mde",
            		pnd_Out_Pnd_Mode,"1st/DOB",
            		pnd_Out_Pnd_1st_Dob,"2nd/DOB",
            		pnd_Out_Pnd_2nd_Dob,"1st/Sex",
            		pnd_Out_Pnd_1st_Sex,"2nd/Sex",
            		pnd_Out_Pnd_2nd_Sex,"/Sys",
            		pnd_Out_Pnd_Sys,"Acct/Cde",
            		pnd_Out_Pnd_Fund.getValue(1,":",pnd_I),"A/M",
            		pnd_Out_Pnd_A_M.getValue(1,":",pnd_I),"Guar/Amount",
            		pnd_Out_Pnd_Guar_Amt.getValue(1,":",pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Units",
            		pnd_Out_Pnd_Units.getValue(1,":",pnd_I), new ReportEditMask ("ZZZ,ZZ9.999"),"Paymnt/Amount",
            		pnd_Out_Pnd_Per_Amt.getValue(1,":",pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            getWorkFiles().write(1, false, pnd_Out);                                                                                                                      //Natural: WRITE WORK FILE 1 #OUT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Factor() throws Exception                                                                                                                        //Natural: GET-FACTOR
    {
        if (BLNatReinput.isReinput()) return;

        aian026_Pnd_Call_Type.setValue("L");                                                                                                                              //Natural: ASSIGN #CALL-TYPE := 'L'
        aian026_Pnd_Ia_Fund_Code.setValue(pnd_Out_Pnd_Fund.getValue(pnd_I));                                                                                              //Natural: ASSIGN #IA-FUND-CODE := #FUND ( #I )
        aian026_Pnd_Revaluation_Method.setValue("M");                                                                                                                     //Natural: ASSIGN #REVALUATION-METHOD := 'M'
        aian026_Pnd_Prtcptn_Dte.compute(new ComputeParameters(false, aian026_Pnd_Prtcptn_Dte), pnd_Out_Pnd_Issue_Dte.val());                                              //Natural: ASSIGN #PRTCPTN-DTE := VAL ( #ISSUE-DTE )
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), aian026);                                                                                               //Natural: CALLNAT 'AIAN026' AIAN026
        if (condition(Global.isEscape())) return;
        if (condition(aian026_Pnd_Rtrn_Cd.equals(getZero())))                                                                                                             //Natural: IF #RTRN-CD = 0
        {
            tiaa_Fund_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(true, tiaa_Fund_Tiaa_Tot_Per_Amt), tiaa_Fund_Tiaa_Units_Cnt.getValue(1).multiply(aian026_Pnd_Iuv));  //Natural: COMPUTE ROUNDED TIAA-TOT-PER-AMT = TIAA-UNITS-CNT ( 1 ) * #IUV
            tiaa_Fund_Tiaa_Tot_Div_Amt.reset();                                                                                                                           //Natural: RESET TIAA-TOT-DIV-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,"Run Date:",pnd_Run_Dte, new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(10),"IA NEW ISSUE SETTLEMENTS",new  //Natural: WRITE ( 1 ) NOTITLE 'Run Date:' #RUN-DTE 10X 'IA NEW ISSUE SETTLEMENTS' 10X 'Program Id:' *PROGRAM / 'Run Time:' #RUN-TME 13X #FRM-DTE 'Thru' #THRU-DTE 11X 'Page:      ' *PAGE-NUMBER ( 1 ) //
                        ColumnSpacing(10),"Program Id:",Global.getPROGRAM(),NEWLINE,"Run Time:",pnd_Run_Tme, new ReportEditMask ("HH:II:SS"),new ColumnSpacing(13),pnd_Frm_Dte,"Thru",pnd_Thru_Dte,new 
                        ColumnSpacing(11),"Page:      ",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133 ZP=OFF");

        getReports().setDisplayColumns(1, "/Contract",
        		pnd_Out_Pnd_Cntrct_Ppcn_Nbr,"/Pye",
        		pnd_Out_Pnd_Payee_Cde, new ReportEditMask ("99"),"Lst Trans/Dte",
        		pnd_Out_Pnd_Lst_Trans_Dte,"Issue/Dte",
        		pnd_Out_Pnd_Issue_Dte,"Final Per/Pay Dte",
        		pnd_Out_Pnd_Final_Per_Pay,"Opt/Cde",
        		pnd_Out_Pnd_Optn_Cde,"Orgn/Cde",
        		pnd_Out_Pnd_Orgn_Cde,"/Mde",
        		pnd_Out_Pnd_Mode,"1st/DOB",
        		pnd_Out_Pnd_1st_Dob,"2nd/DOB",
        		pnd_Out_Pnd_2nd_Dob,"1st/Sex",
        		pnd_Out_Pnd_1st_Sex,"2nd/Sex",
        		pnd_Out_Pnd_2nd_Sex,"/Sys",
        		pnd_Out_Pnd_Sys,"Acct/Cde",
        		pnd_Out_Pnd_Fund,"A/M",
        		pnd_Out_Pnd_A_M,"Guar/Amount",
        		pnd_Out_Pnd_Guar_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Units",
        		pnd_Out_Pnd_Units, new ReportEditMask ("ZZZ,ZZ9.999"),"Paymnt/Amount",
        		pnd_Out_Pnd_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
    }
}
