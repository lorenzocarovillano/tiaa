/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:28 PM
**        * FROM NATURAL PROGRAM : Iaaprsa
************************************************************
**        * FILE NAME            : Iaaprsa.java
**        * CLASS NAME           : Iaaprsa
**        * INSTANCE NAME        : Iaaprsa
************************************************************
************************************************************************
* PROGRAM:  IAAPRSA
* DATE   :  09/08/2014
* PURPOSE:  CREATE A RSA FILE FROM AN INPUT FILE FROM SC.
*
* HISTORY
* 9/8/14 -  JUN TINIO - ORIGINAL CODE
* 12/2/14 - JUN TINIO - ADDED ALL PAYEES ASSOCIATED WITH CONTRSCT.
*           SEE 12/14 FOR CHANGES.
* 03/3/15 - JUN TINIO - CHANGED 999999 FINAL PER PAY DATE TO 999912.
*           SEE 03/15 FOR CHANGES.
* 07/13/15 - JUN TINIO - FIXED THE PIN BEING WRITTEN TO WORK FILE WHEN
*           UPDATING STATUS ONLY. SEE 07/15 FOR CHANGES.
*
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaaprsa extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_In_Ssna;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_In_Ssn;
    private DbsField pnd_Input_Pnd_Filler;
    private DbsField pnd_Input_Pnd_In_Pina;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_In_Pin_7;
    private DbsField pnd_Input_Pnd_In_Pin_5;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_In_Pin;
    private DbsField pnd_Input_Pnd_Filler2;
    private DbsField pnd_Input_Pnd_In_Cntrct;
    private DbsField pnd_Input_Pnd_Filler3;
    private DbsField pnd_Input_Pnd_In_Payee;

    private DataAccessProgramView vw_cntrct;
    private DbsField cntrct_Cntrct_Ppcn_Nbr;
    private DbsField cntrct_Cntrct_Optn_Cde;
    private DbsField cntrct_Cntrct_Orgn_Cde;
    private DbsField cntrct_Cntrct_Issue_Dte;
    private DbsField cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField cntrct_Cntrct_Type;
    private DbsField cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField cntrct_Cntrct_Scnd_Annt_Xref_Ind;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr_Cntrct_Curr_Dist_Cde;
    private DbsField cpr_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField cpr_Cntrct_Pend_Cde;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Bnfcry_Xref_Ind;
    private DbsField cpr_Bnfcry_Dod_Dte;

    private DataAccessProgramView vw_cpr1;
    private DbsField cpr1_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr1_Cntrct_Part_Payee_Cde;
    private DbsField cpr1_Cntrct_Actvty_Cde;
    private DbsField cpr1_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr1_Cntrct_Curr_Dist_Cde;
    private DbsField cpr1_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr1_Cpr_Id_Nbr;
    private DbsField cpr1_Cntrct_Pend_Cde;
    private DbsField cpr1_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr1_Bnfcry_Xref_Ind;
    private DbsField cpr1_Bnfcry_Dod_Dte;

    private DataAccessProgramView vw_cpr2;
    private DbsField cpr2_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr2_Cntrct_Part_Payee_Cde;
    private DbsField cpr2_Cntrct_Actvty_Cde;
    private DbsField cpr2_Prtcpnt_Rsdncy_Cde;
    private DbsField cpr2_Cntrct_Curr_Dist_Cde;
    private DbsField cpr2_Cntrct_Final_Per_Pay_Dte;
    private DbsField cpr2_Cpr_Id_Nbr;
    private DbsField cpr2_Cntrct_Pend_Cde;
    private DbsField cpr2_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr2_Bnfcry_Xref_Ind;
    private DbsField cpr2_Bnfcry_Dod_Dte;

    private DataAccessProgramView vw_settl;
    private DbsField settl_Sttlmnt_Id_Nbr;
    private DbsField settl_Sttlmnt_Tax_Id_Nbr;
    private DbsField settl_Sttlmnt_Req_Seq_Nbr;
    private DbsField settl_Sttlmnt_Process_Type;
    private DbsField settl_Sttlmnt_Timestamp;
    private DbsField settl_Sttlmnt_Status_Cde;
    private DbsField settl_Sttlmnt_Status_Timestamp;
    private DbsField settl_Sttlmnt_Cwf_Wpid;
    private DbsField settl_Sttlmnt_Decedent_Name;
    private DbsField settl_Sttlmnt_Decedent_Type;
    private DbsField settl_Sttlmnt_Dod_Dte;

    private DbsGroup settl__R_Field_4;
    private DbsField settl_Pnd_1st_Dod_6;
    private DbsField settl_Sttlmnt_2nd_Dod_Dte;

    private DbsGroup settl__R_Field_5;
    private DbsField settl_Pnd_2nd_Dod_6;
    private DbsField pnd_Pin_Taxid_Seq_Key;

    private DbsGroup pnd_Pin_Taxid_Seq_Key__R_Field_6;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr;
    private DbsField pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr;
    private DbsField pnd_S_Cntrct;
    private DbsField pnd_1st_Dod;

    private DbsGroup pnd_1st_Dod__R_Field_7;
    private DbsField pnd_1st_Dod_Pnd_1st_Yyyymm;
    private DbsField pnd_1st_Dod_Pnd_1st_Dd;
    private DbsField pnd_2nd_Dod;

    private DbsGroup pnd_2nd_Dod__R_Field_8;
    private DbsField pnd_2nd_Dod_Pnd_2nd_Yyyymm;
    private DbsField pnd_2nd_Dod_Pnd_2nd_Dd;
    private DbsField pnd_Fin_Per_Pay_Dte;

    private DbsGroup pnd_Fin_Per_Pay_Dte__R_Field_9;
    private DbsField pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm;
    private DbsField pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd;
    private DbsField pnd_Pin_Cpr_Key;

    private DbsGroup pnd_Pin_Cpr_Key__R_Field_10;
    private DbsField pnd_Pin_Cpr_Key_Pnd_Pin;
    private DbsField pnd_Cpr_Key;

    private DbsGroup pnd_Cpr_Key__R_Field_11;
    private DbsField pnd_Cpr_Key_Pnd_Cp_Cntrct;
    private DbsField pnd_Cpr_Key_Pnd_Cp_Payee;
    private DbsField pnd_Status;
    private DbsField pnd_Status_Dte;

    private DbsGroup pnd_Status_Dte__R_Field_12;
    private DbsField pnd_Status_Dte_Pnd_Syy;
    private DbsField pnd_Status_Dte_Pnd_Smm;
    private DbsField pnd_Status_Dte_Pnd_Sdd;

    private DbsGroup pnd_Cntrl;
    private DbsField pnd_Cntrl_Pnd_Srce;
    private DbsField pnd_Cntrl_Pnd_Cntrl_Dte;
    private DbsField pnd_Cntrl_Pnd_Rec_Cnt;
    private DbsField pnd_O_Cntrct;
    private DbsField pnd_O_Payee;
    private DbsField pnd_O_Pin;
    private DbsField pnd_O_Ssn;
    private DbsField pnd_Trg;
    private DbsField pnd_01_Pin;
    private DbsField pnd_02_Pin;
    private DbsField pnd_W_Pin;
    private DbsField pnd_Oth_Pin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_In_Ssna = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Ssna", "#IN-SSNA", FieldType.STRING, 9);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_In_Ssna);
        pnd_Input_Pnd_In_Ssn = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_In_Ssn", "#IN-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_Filler = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Pina = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Pina", "#IN-PINA", FieldType.STRING, 12);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_In_Pina);
        pnd_Input_Pnd_In_Pin_7 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_In_Pin_7", "#IN-PIN-7", FieldType.STRING, 7);
        pnd_Input_Pnd_In_Pin_5 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_In_Pin_5", "#IN-PIN-5", FieldType.STRING, 5);

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_In_Pina);
        pnd_Input_Pnd_In_Pin = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_In_Pin", "#IN-PIN", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_Filler2 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Cntrct = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Cntrct", "#IN-CNTRCT", FieldType.STRING, 10);
        pnd_Input_Pnd_Filler3 = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        pnd_Input_Pnd_In_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_In_Payee", "#IN-PAYEE", FieldType.STRING, 2);

        vw_cntrct = new DataAccessProgramView(new NameInfo("vw_cntrct", "CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        cntrct_Cntrct_Ppcn_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cntrct_Cntrct_Optn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        cntrct_Cntrct_Orgn_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        cntrct_Cntrct_Issue_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "CNTRCT_ISSUE_DTE");
        cntrct_Cntrct_Issue_Dte_Dd = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        cntrct_Cntrct_First_Pymnt_Due_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        cntrct_Cntrct_Fp_Due_Dte_Dd = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        cntrct_Cntrct_Type = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        cntrct_Cntrct_First_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        cntrct_Cntrct_Scnd_Annt_Ssn = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        cntrct_Cntrct_First_Annt_Xref_Ind = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_cntrct.getRecord().newFieldInGroup("cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        registerRecord(vw_cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Prtcpnt_Rsdncy_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr_Cntrct_Curr_Dist_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr_Cntrct_Final_Per_Pay_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr_Cntrct_Pend_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Bnfcry_Xref_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr_Bnfcry_Dod_Dte = vw_cpr.getRecord().newFieldInGroup("cpr_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        registerRecord(vw_cpr);

        vw_cpr1 = new DataAccessProgramView(new NameInfo("vw_cpr1", "CPR1"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr1_Cntrct_Part_Ppcn_Nbr = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr1_Cntrct_Part_Payee_Cde = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr1_Cntrct_Actvty_Cde = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr1_Prtcpnt_Rsdncy_Cde = vw_cpr1.getRecord().newFieldInGroup("cpr1_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr1_Cntrct_Curr_Dist_Cde = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr1_Cntrct_Final_Per_Pay_Dte = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr1_Cpr_Id_Nbr = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr1_Cntrct_Pend_Cde = vw_cpr1.getRecord().newFieldInGroup("cpr1_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr1_Prtcpnt_Tax_Id_Nbr = vw_cpr1.getRecord().newFieldInGroup("cpr1_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr1_Bnfcry_Xref_Ind = vw_cpr1.getRecord().newFieldInGroup("cpr1_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr1_Bnfcry_Dod_Dte = vw_cpr1.getRecord().newFieldInGroup("cpr1_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        registerRecord(vw_cpr1);

        vw_cpr2 = new DataAccessProgramView(new NameInfo("vw_cpr2", "CPR2"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr2_Cntrct_Part_Ppcn_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr2_Cntrct_Part_Payee_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cpr2_Cntrct_Actvty_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr2_Prtcpnt_Rsdncy_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PRTCPNT_RSDNCY_CDE");
        cpr2_Cntrct_Curr_Dist_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_CURR_DIST_CDE");
        cpr2_Cntrct_Final_Per_Pay_Dte = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        cpr2_Cpr_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        cpr2_Cntrct_Pend_Cde = vw_cpr2.getRecord().newFieldInGroup("cpr2_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_PEND_CDE");
        cpr2_Prtcpnt_Tax_Id_Nbr = vw_cpr2.getRecord().newFieldInGroup("cpr2_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr2_Bnfcry_Xref_Ind = vw_cpr2.getRecord().newFieldInGroup("cpr2_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BNFCRY_XREF_IND");
        cpr2_Bnfcry_Dod_Dte = vw_cpr2.getRecord().newFieldInGroup("cpr2_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "BNFCRY_DOD_DTE");
        registerRecord(vw_cpr2);

        vw_settl = new DataAccessProgramView(new NameInfo("vw_settl", "SETTL"), "IAA_DC_STTLMNT_REQ", "IA_DEATH_CLAIMS");
        settl_Sttlmnt_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Id_Nbr", "STTLMNT-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "STTLMNT_ID_NBR");
        settl_Sttlmnt_Tax_Id_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Tax_Id_Nbr", "STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "STTLMNT_TAX_ID_NBR");
        settl_Sttlmnt_Req_Seq_Nbr = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Req_Seq_Nbr", "STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "STTLMNT_REQ_SEQ_NBR");
        settl_Sttlmnt_Process_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Process_Type", "STTLMNT-PROCESS-TYPE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "STTLMNT_PROCESS_TYPE");
        settl_Sttlmnt_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Timestamp", "STTLMNT-TIMESTAMP", FieldType.TIME, RepeatingFieldStrategy.None, 
            "STTLMNT_TIMESTAMP");
        settl_Sttlmnt_Status_Cde = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Cde", "STTLMNT-STATUS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "STTLMNT_STATUS_CDE");
        settl_Sttlmnt_Status_Timestamp = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Status_Timestamp", "STTLMNT-STATUS-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STTLMNT_STATUS_TIMESTAMP");
        settl_Sttlmnt_Cwf_Wpid = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Cwf_Wpid", "STTLMNT-CWF-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STTLMNT_CWF_WPID");
        settl_Sttlmnt_Decedent_Name = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Decedent_Name", "STTLMNT-DECEDENT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_NAME");
        settl_Sttlmnt_Decedent_Type = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Decedent_Type", "STTLMNT-DECEDENT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "STTLMNT_DECEDENT_TYPE");
        settl_Sttlmnt_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_Dod_Dte", "STTLMNT-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_DOD_DTE");

        settl__R_Field_4 = vw_settl.getRecord().newGroupInGroup("settl__R_Field_4", "REDEFINE", settl_Sttlmnt_Dod_Dte);
        settl_Pnd_1st_Dod_6 = settl__R_Field_4.newFieldInGroup("settl_Pnd_1st_Dod_6", "#1ST-DOD-6", FieldType.NUMERIC, 6);
        settl_Sttlmnt_2nd_Dod_Dte = vw_settl.getRecord().newFieldInGroup("settl_Sttlmnt_2nd_Dod_Dte", "STTLMNT-2ND-DOD-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "STTLMNT_2ND_DOD_DTE");

        settl__R_Field_5 = vw_settl.getRecord().newGroupInGroup("settl__R_Field_5", "REDEFINE", settl_Sttlmnt_2nd_Dod_Dte);
        settl_Pnd_2nd_Dod_6 = settl__R_Field_5.newFieldInGroup("settl_Pnd_2nd_Dod_6", "#2ND-DOD-6", FieldType.NUMERIC, 6);
        registerRecord(vw_settl);

        pnd_Pin_Taxid_Seq_Key = localVariables.newFieldInRecord("pnd_Pin_Taxid_Seq_Key", "#PIN-TAXID-SEQ-KEY", FieldType.BINARY, 24);

        pnd_Pin_Taxid_Seq_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Pin_Taxid_Seq_Key__R_Field_6", "REDEFINE", pnd_Pin_Taxid_Seq_Key);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_6.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr", "#STTLMNT-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_6.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Tax_Id_Nbr", 
            "#STTLMNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr = pnd_Pin_Taxid_Seq_Key__R_Field_6.newFieldInGroup("pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Req_Seq_Nbr", 
            "#STTLMNT-REQ-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_S_Cntrct = localVariables.newFieldInRecord("pnd_S_Cntrct", "#S-CNTRCT", FieldType.STRING, 10);
        pnd_1st_Dod = localVariables.newFieldInRecord("pnd_1st_Dod", "#1ST-DOD", FieldType.STRING, 8);

        pnd_1st_Dod__R_Field_7 = localVariables.newGroupInRecord("pnd_1st_Dod__R_Field_7", "REDEFINE", pnd_1st_Dod);
        pnd_1st_Dod_Pnd_1st_Yyyymm = pnd_1st_Dod__R_Field_7.newFieldInGroup("pnd_1st_Dod_Pnd_1st_Yyyymm", "#1ST-YYYYMM", FieldType.NUMERIC, 6);
        pnd_1st_Dod_Pnd_1st_Dd = pnd_1st_Dod__R_Field_7.newFieldInGroup("pnd_1st_Dod_Pnd_1st_Dd", "#1ST-DD", FieldType.NUMERIC, 2);
        pnd_2nd_Dod = localVariables.newFieldInRecord("pnd_2nd_Dod", "#2ND-DOD", FieldType.STRING, 8);

        pnd_2nd_Dod__R_Field_8 = localVariables.newGroupInRecord("pnd_2nd_Dod__R_Field_8", "REDEFINE", pnd_2nd_Dod);
        pnd_2nd_Dod_Pnd_2nd_Yyyymm = pnd_2nd_Dod__R_Field_8.newFieldInGroup("pnd_2nd_Dod_Pnd_2nd_Yyyymm", "#2ND-YYYYMM", FieldType.NUMERIC, 6);
        pnd_2nd_Dod_Pnd_2nd_Dd = pnd_2nd_Dod__R_Field_8.newFieldInGroup("pnd_2nd_Dod_Pnd_2nd_Dd", "#2ND-DD", FieldType.NUMERIC, 2);
        pnd_Fin_Per_Pay_Dte = localVariables.newFieldInRecord("pnd_Fin_Per_Pay_Dte", "#FIN-PER-PAY-DTE", FieldType.STRING, 8);

        pnd_Fin_Per_Pay_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Fin_Per_Pay_Dte__R_Field_9", "REDEFINE", pnd_Fin_Per_Pay_Dte);
        pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm = pnd_Fin_Per_Pay_Dte__R_Field_9.newFieldInGroup("pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm", "#FIN-YYYYMM", FieldType.NUMERIC, 
            6);
        pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd = pnd_Fin_Per_Pay_Dte__R_Field_9.newFieldInGroup("pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd", "#FIN-DD", FieldType.NUMERIC, 
            2);
        pnd_Pin_Cpr_Key = localVariables.newFieldInRecord("pnd_Pin_Cpr_Key", "#PIN-CPR-KEY", FieldType.STRING, 24);

        pnd_Pin_Cpr_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Pin_Cpr_Key__R_Field_10", "REDEFINE", pnd_Pin_Cpr_Key);
        pnd_Pin_Cpr_Key_Pnd_Pin = pnd_Pin_Cpr_Key__R_Field_10.newFieldInGroup("pnd_Pin_Cpr_Key_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Cpr_Key = localVariables.newFieldInRecord("pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Cpr_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Cpr_Key__R_Field_11", "REDEFINE", pnd_Cpr_Key);
        pnd_Cpr_Key_Pnd_Cp_Cntrct = pnd_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Key_Pnd_Cp_Cntrct", "#CP-CNTRCT", FieldType.STRING, 10);
        pnd_Cpr_Key_Pnd_Cp_Payee = pnd_Cpr_Key__R_Field_11.newFieldInGroup("pnd_Cpr_Key_Pnd_Cp_Payee", "#CP-PAYEE", FieldType.NUMERIC, 2);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 10);
        pnd_Status_Dte = localVariables.newFieldInRecord("pnd_Status_Dte", "#STATUS-DTE", FieldType.STRING, 8);

        pnd_Status_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_Status_Dte__R_Field_12", "REDEFINE", pnd_Status_Dte);
        pnd_Status_Dte_Pnd_Syy = pnd_Status_Dte__R_Field_12.newFieldInGroup("pnd_Status_Dte_Pnd_Syy", "#SYY", FieldType.NUMERIC, 4);
        pnd_Status_Dte_Pnd_Smm = pnd_Status_Dte__R_Field_12.newFieldInGroup("pnd_Status_Dte_Pnd_Smm", "#SMM", FieldType.NUMERIC, 2);
        pnd_Status_Dte_Pnd_Sdd = pnd_Status_Dte__R_Field_12.newFieldInGroup("pnd_Status_Dte_Pnd_Sdd", "#SDD", FieldType.NUMERIC, 2);

        pnd_Cntrl = localVariables.newGroupInRecord("pnd_Cntrl", "#CNTRL");
        pnd_Cntrl_Pnd_Srce = pnd_Cntrl.newFieldInGroup("pnd_Cntrl_Pnd_Srce", "#SRCE", FieldType.STRING, 6);
        pnd_Cntrl_Pnd_Cntrl_Dte = pnd_Cntrl.newFieldInGroup("pnd_Cntrl_Pnd_Cntrl_Dte", "#CNTRL-DTE", FieldType.STRING, 8);
        pnd_Cntrl_Pnd_Rec_Cnt = pnd_Cntrl.newFieldInGroup("pnd_Cntrl_Pnd_Rec_Cnt", "#REC-CNT", FieldType.NUMERIC, 13);
        pnd_O_Cntrct = localVariables.newFieldInRecord("pnd_O_Cntrct", "#O-CNTRCT", FieldType.STRING, 10);
        pnd_O_Payee = localVariables.newFieldInRecord("pnd_O_Payee", "#O-PAYEE", FieldType.NUMERIC, 2);
        pnd_O_Pin = localVariables.newFieldInRecord("pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 12);
        pnd_O_Ssn = localVariables.newFieldInRecord("pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Trg = localVariables.newFieldInRecord("pnd_Trg", "#TRG", FieldType.STRING, 1);
        pnd_01_Pin = localVariables.newFieldInRecord("pnd_01_Pin", "#01-PIN", FieldType.NUMERIC, 12);
        pnd_02_Pin = localVariables.newFieldInRecord("pnd_02_Pin", "#02-PIN", FieldType.NUMERIC, 12);
        pnd_W_Pin = localVariables.newFieldInRecord("pnd_W_Pin", "#W-PIN", FieldType.NUMERIC, 12);
        pnd_Oth_Pin = localVariables.newFieldInRecord("pnd_Oth_Pin", "#OTH-PIN", FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrct.reset();
        vw_cpr.reset();
        vw_cpr1.reset();
        vw_cpr2.reset();
        vw_settl.reset();

        localVariables.reset();
        pnd_Cntrl_Pnd_Srce.setInitialValue("IAIQ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaaprsa() throws Exception
    {
        super("Iaaprsa");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 133 ZP = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        pnd_Cntrl_Pnd_Cntrl_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                          //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #CNTRL-DTE
        pnd_Status_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #STATUS-DTE
        pnd_Status_Dte_Pnd_Smm.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #SMM
        if (condition(pnd_Status_Dte_Pnd_Smm.equals(getZero())))                                                                                                          //Natural: IF #SMM = 0
        {
            pnd_Status_Dte_Pnd_Smm.setValue(12);                                                                                                                          //Natural: ASSIGN #SMM := 12
            pnd_Status_Dte_Pnd_Syy.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #SYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Status_Dte_Pnd_Sdd.setValue(31);                                                                                                                              //Natural: ASSIGN #SDD := 31
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(DbsUtil.maskMatches(pnd_Status_Dte,"YYYYMMDD")))                                                                                                //Natural: IF #STATUS-DTE = MASK ( YYYYMMDD )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Status_Dte_Pnd_Sdd.nsubtract(1);                                                                                                                          //Natural: SUBTRACT 1 FROM #SDD
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_In_Cntrct.greater(" ") && pnd_Input_Pnd_In_Cntrct.notEquals("|")))                                                                //Natural: IF #IN-CNTRCT GT ' ' AND #IN-CNTRCT NE '|'
            {
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT-STATUS
                sub_Get_Contract_Status();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #IN-PINA = MASK(NNNNNNN)                  /* 082017
                //*  082017
                if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_In_Pina,"NNNNNNNNNNNN") || DbsUtil.maskMatches(pnd_Input_Pnd_In_Pin_7,"NNNNNNN")))                        //Natural: IF #IN-PINA = MASK ( NNNNNNNNNNNN ) OR #IN-PIN-7 = MASK ( NNNNNNN )
                {
                                                                                                                                                                          //Natural: PERFORM GET-CPR
                    sub_Get_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_1st_Dod.reset();                                                                                                                                          //Natural: RESET #1ST-DOD #2ND-DOD #FIN-PER-PAY-DTE #STATUS #S-CNTRCT #01-PIN #02-PIN
            pnd_2nd_Dod.reset();
            pnd_Fin_Per_Pay_Dte.reset();
            pnd_Status.reset();
            pnd_S_Cntrct.reset();
            pnd_01_Pin.reset();
            pnd_02_Pin.reset();
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Cntrl_Pnd_Rec_Cnt.equals(getZero())))                                                                                                           //Natural: IF #REC-CNT = 0
        {
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(3, false, pnd_Cntrl);                                                                                                                        //Natural: WRITE WORK FILE 3 #CNTRL
        getWorkFiles().write(4, false, pnd_Trg);                                                                                                                          //Natural: WRITE WORK FILE 4 #TRG
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CPR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTRACT-STATUS
        //*  12/14 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DOD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OTHER-DOD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* ***********************************************************************
        //*  12/14 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALL-PAYEES
        //*    #IN-SSN '|' #PIN '|' CNTRCT-OPTN-CDE '|' CNTRCT-ORGN-CDE
        //*  12/14 - START
        //*  12/14 - END
    }
    private void sub_Get_Contract() throws Exception                                                                                                                      //Natural: GET-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_01_Pin.reset();                                                                                                                                               //Natural: RESET #01-PIN #02-PIN
        pnd_02_Pin.reset();
        vw_cpr2.startDatabaseRead                                                                                                                                         //Natural: READ ( 2 ) CPR2 BY CNTRCT-PAYEE-KEY STARTING FROM CPR.CNTRCT-PART-PPCN-NBR
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", cpr_Cntrct_Part_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        2
        );
        READ02:
        while (condition(vw_cpr2.readNextRow("READ02")))
        {
            if (condition(cpr2_Cntrct_Part_Ppcn_Nbr.notEquals(cpr_Cntrct_Part_Ppcn_Nbr)))                                                                                 //Natural: IF CNTRCT-PART-PPCN-NBR NE CPR.CNTRCT-PART-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cpr2_Cntrct_Part_Payee_Cde.equals(1)))                                                                                                          //Natural: IF CNTRCT-PART-PAYEE-CDE = 01
            {
                pnd_01_Pin.setValue(cpr2_Cpr_Id_Nbr);                                                                                                                     //Natural: ASSIGN #01-PIN := CPR-ID-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cpr2_Cntrct_Part_Payee_Cde.equals(2)))                                                                                                          //Natural: IF CNTRCT-PART-PAYEE-CDE = 02
            {
                pnd_02_Pin.setValue(cpr2_Cpr_Id_Nbr);                                                                                                                     //Natural: ASSIGN #02-PIN := CPR-ID-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_cntrct.startDatabaseFind                                                                                                                                       //Natural: FIND CNTRCT WITH CNTRCT-PPCN-NBR = CPR.CNTRCT-PART-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", cpr_Cntrct_Part_Ppcn_Nbr, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cntrct.readNextRow("FIND01")))
        {
            vw_cntrct.setIfNotFoundControlFlag(false);
            if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero()) || cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                     //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0 OR CNTRCT-SCND-ANNT-DOD-DTE GT 0
            {
                if (condition(pnd_1st_Dod.equals(" ") && pnd_2nd_Dod.equals(" ")))                                                                                        //Natural: IF #1ST-DOD = ' ' AND #2ND-DOD = ' '
                {
                                                                                                                                                                          //Natural: PERFORM GET-DOD
                    sub_Get_Dod();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cpr() throws Exception                                                                                                                           //Natural: GET-CPR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  082017
        if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_In_Pina,"NNNNNNNNNNNN")))                                                                                         //Natural: IF #IN-PINA = MASK ( NNNNNNNNNNNN )
        {
            pnd_Pin_Cpr_Key_Pnd_Pin.setValue(pnd_Input_Pnd_In_Pin);                                                                                                       //Natural: ASSIGN #PIN := #IN-PIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pin_Cpr_Key_Pnd_Pin.compute(new ComputeParameters(false, pnd_Pin_Cpr_Key_Pnd_Pin), pnd_Input_Pnd_In_Pin_7.val());                                         //Natural: ASSIGN #PIN := VAL ( #IN-PIN-7 )
            //*  082017
        }                                                                                                                                                                 //Natural: END-IF
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ CPR BY PIN-CNTRCT-PAYEE-KEY STARTING FROM #PIN-CPR-KEY
        (
        "READ03",
        new Wc[] { new Wc("PIN_CNTRCT_PAYEE_KEY", ">=", pnd_Pin_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_cpr.readNextRow("READ03")))
        {
            if (condition(cpr_Cpr_Id_Nbr.notEquals(pnd_Pin_Cpr_Key_Pnd_Pin)))                                                                                             //Natural: IF CPR-ID-NBR NE #PIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_S_Cntrct.notEquals(cpr_Cntrct_Part_Ppcn_Nbr)))                                                                                              //Natural: IF #S-CNTRCT NE CNTRCT-PART-PPCN-NBR
            {
                pnd_S_Cntrct.setValue(cpr_Cntrct_Part_Ppcn_Nbr);                                                                                                          //Natural: ASSIGN #S-CNTRCT := CNTRCT-PART-PPCN-NBR
                pnd_1st_Dod.reset();                                                                                                                                      //Natural: RESET #1ST-DOD #2ND-DOD
                pnd_2nd_Dod.reset();
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT
                sub_Get_Contract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  12/14 NEW
                                                                                                                                                                          //Natural: PERFORM GET-ALL-PAYEES
                sub_Get_All_Payees();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  12/14
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  12/14
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  12/14 - START - MOVED THIS BLOCK TO GET-ALL-PAYEES SUBROUTINE
            //*  RESET #FIN-PER-PAY-DTE
            //*  IF CNTRCT-FINAL-PER-PAY-DTE GT 0
            //*    #FIN-PER-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
            //*    #FIN-DD := 01
            //*  END-IF
            //*  DECIDE FOR FIRST CONDITION
            //*    WHEN CNTRCT-ACTVTY-CDE = 9
            //*      #STATUS := 'TERMINATED'
            //*    WHEN CNTRCT-PEND-CDE = 'R'
            //*      #STATUS := 'EXPIRED'
            //*    WHEN CNTRCT-PEND-CDE NE '0'
            //*      #STATUS := 'PENDING'
            //*    WHEN NONE
            //*      #STATUS := 'ACTIVE'
            //*  END-DECIDE
            //*  WRITE WORK 2
            //*    #IN-SSN '|' #PIN '|' CNTRCT-OPTN-CDE '|' CNTRCT-ORGN-CDE '|'
            //*    #1ST-DOD '|' #2ND-DOD '|' #FIN-PER-PAY-DTE '|'
            //*    CNTRCT-PART-PPCN-NBR '|' CNTRCT-PART-PAYEE-CDE '|'
            //*    #STATUS '|' #STATUS-DTE
            //*  PERFORM WRITE-REPORT
            //*  12/14 - END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Contract_Status() throws Exception                                                                                                               //Natural: GET-CONTRACT-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(DbsUtil.maskMatches(pnd_Input_Pnd_In_Payee,"NN")))                                                                                                  //Natural: IF #IN-PAYEE = MASK ( NN )
        {
            pnd_Cpr_Key_Pnd_Cp_Payee.compute(new ComputeParameters(false, pnd_Cpr_Key_Pnd_Cp_Payee), pnd_Input_Pnd_In_Payee.val());                                       //Natural: ASSIGN #CP-PAYEE := VAL ( #IN-PAYEE )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cpr_Key_Pnd_Cp_Cntrct.setValue(pnd_Input_Pnd_In_Cntrct);                                                                                                      //Natural: ASSIGN #CP-CNTRCT := #IN-CNTRCT
        vw_cpr.startDatabaseRead                                                                                                                                          //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CPR-KEY
        (
        "READ04",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cpr_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_cpr.readNextRow("READ04")))
        {
            if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(pnd_Cpr_Key_Pnd_Cp_Cntrct) || cpr_Cntrct_Part_Payee_Cde.notEquals(pnd_Cpr_Key_Pnd_Cp_Payee)))                //Natural: IF CNTRCT-PART-PPCN-NBR NE #CP-CNTRCT OR CNTRCT-PART-PAYEE-CDE NE #CP-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CONTRACT
            sub_Get_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fin_Per_Pay_Dte.reset();                                                                                                                                  //Natural: RESET #FIN-PER-PAY-DTE
            if (condition(cpr_Cntrct_Final_Per_Pay_Dte.greater(getZero())))                                                                                               //Natural: IF CNTRCT-FINAL-PER-PAY-DTE GT 0
            {
                pnd_Fin_Per_Pay_Dte.setValue(cpr_Cntrct_Final_Per_Pay_Dte);                                                                                               //Natural: ASSIGN #FIN-PER-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd.setValue(1);                                                                                                               //Natural: ASSIGN #FIN-DD := 01
            }                                                                                                                                                             //Natural: END-IF
            //*  03/15 - START
            if (condition(pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm.equals(999999)))                                                                                             //Natural: IF #FIN-YYYYMM = 999999
            {
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm.setValue(999912);                                                                                                      //Natural: ASSIGN #FIN-YYYYMM := 999912
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd.setValue(31);                                                                                                              //Natural: ASSIGN #FIN-DD := 31
            }                                                                                                                                                             //Natural: END-IF
            //*  03/15 - END
            short decideConditionsMet389 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-ACTVTY-CDE = 9
            if (condition(cpr_Cntrct_Actvty_Cde.equals(9)))
            {
                decideConditionsMet389++;
                pnd_Status.setValue("TERMINATED");                                                                                                                        //Natural: ASSIGN #STATUS := 'TERMINATED'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PEND-CDE = 'R'
            else if (condition(cpr_Cntrct_Pend_Cde.equals("R")))
            {
                decideConditionsMet389++;
                pnd_Status.setValue("EXPIRED");                                                                                                                           //Natural: ASSIGN #STATUS := 'EXPIRED'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PEND-CDE NE '0'
            else if (condition(cpr_Cntrct_Pend_Cde.notEquals("0")))
            {
                decideConditionsMet389++;
                pnd_Status.setValue("PENDING");                                                                                                                           //Natural: ASSIGN #STATUS := 'PENDING'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Status.setValue("ACTIVE");                                                                                                                            //Natural: ASSIGN #STATUS := 'ACTIVE'
                //*  07/15
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Pin_Cpr_Key_Pnd_Pin.setValue(cpr_Cpr_Id_Nbr);                                                                                                             //Natural: ASSIGN #PIN := CPR.CPR-ID-NBR
            getWorkFiles().write(2, false, pnd_Input_Pnd_In_Ssn, "|", pnd_Pin_Cpr_Key_Pnd_Pin, "|", cntrct_Cntrct_Optn_Cde, "|", cntrct_Cntrct_Orgn_Cde,                  //Natural: WRITE WORK FILE 2 #IN-SSN '|' #PIN '|' CNTRCT-OPTN-CDE '|' CNTRCT-ORGN-CDE '|' #1ST-DOD '|' #2ND-DOD '|' #FIN-PER-PAY-DTE '|' CNTRCT-PART-PPCN-NBR '|' CNTRCT-PART-PAYEE-CDE '|' #STATUS '|' #STATUS-DTE
                "|", pnd_1st_Dod, "|", pnd_2nd_Dod, "|", pnd_Fin_Per_Pay_Dte, "|", cpr_Cntrct_Part_Ppcn_Nbr, "|", cpr_Cntrct_Part_Payee_Cde, "|", pnd_Status, 
                "|", pnd_Status_Dte);
            pnd_O_Pin.setValue(pnd_Pin_Cpr_Key_Pnd_Pin);                                                                                                                  //Natural: ASSIGN #O-PIN := #PIN
            pnd_O_Ssn.setValue(pnd_Input_Pnd_In_Ssn);                                                                                                                     //Natural: ASSIGN #O-SSN := #IN-SSN
            pnd_O_Cntrct.setValue(cpr_Cntrct_Part_Ppcn_Nbr);                                                                                                              //Natural: ASSIGN #O-CNTRCT := CNTRCT-PART-PPCN-NBR
            pnd_O_Payee.setValue(cpr_Cntrct_Part_Payee_Cde);                                                                                                              //Natural: ASSIGN #O-PAYEE := CNTRCT-PART-PAYEE-CDE
            //*  12/14 - END
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Dod() throws Exception                                                                                                                           //Natural: GET-DOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr.setValue(pnd_Input_Pnd_In_Pin);                                                                                          //Natural: ASSIGN #STTLMNT-ID-NBR := #IN-PIN
        vw_settl.startDatabaseRead                                                                                                                                        //Natural: READ SETTL BY PIN-TAXID-SEQ-KEY STARTING FROM #PIN-TAXID-SEQ-KEY
        (
        "READ05",
        new Wc[] { new Wc("PIN_TAXID_SEQ_KEY", ">=", pnd_Pin_Taxid_Seq_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_KEY", "ASC") }
        );
        READ05:
        while (condition(vw_settl.readNextRow("READ05")))
        {
            if (condition(settl_Sttlmnt_Id_Nbr.notEquals(pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr)))                                                                      //Natural: IF STTLMNT-ID-NBR NE #STTLMNT-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(DbsUtil.maskMatches(settl_Sttlmnt_Process_Type,"'ID'"))))                                                                                     //Natural: ACCEPT IF STTLMNT-PROCESS-TYPE = MASK ( 'ID' )
            {
                continue;
            }
            short decideConditionsMet421 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE STTLMNT-PROCESS-TYPE;//Natural: VALUE 'ID1A'
            if (condition((settl_Sttlmnt_Process_Type.equals("ID1A"))))
            {
                decideConditionsMet421++;
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                              //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #1ST-DOD-6
                {
                    pnd_1st_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                          //Natural: ASSIGN #1ST-DOD := STTLMNT-DOD-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'ID2A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID2A"))))
            {
                decideConditionsMet421++;
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                           //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_2nd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                       //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #1ST-DOD-6
                        {
                            pnd_2nd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-DOD-DTE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'ID3A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID3A"))))
            {
                decideConditionsMet421++;
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                              //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #1ST-DOD-6
                {
                    pnd_1st_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                          //Natural: ASSIGN #1ST-DOD := STTLMNT-DOD-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                          //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_1st_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #1ST-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                           //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_2nd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                       //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #1ST-DOD-6
                        {
                            pnd_2nd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-DOD-DTE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_1st_Dod.equals(" ")))                                                                                                                           //Natural: IF #1ST-DOD = ' '
        {
            if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero())))                                                                                           //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE GT 0
            {
                if (condition(pnd_Input_Pnd_In_Pin.notEquals(pnd_01_Pin)))                                                                                                //Natural: IF #IN-PIN NE #01-PIN
                {
                    pnd_Oth_Pin.setValue(pnd_01_Pin);                                                                                                                     //Natural: ASSIGN #OTH-PIN := #01-PIN
                                                                                                                                                                          //Natural: PERFORM GET-OTHER-DOD
                    sub_Get_Other_Dod();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_1st_Dod.equals(" ")))                                                                                                                   //Natural: IF #1ST-DOD = ' '
                {
                    pnd_1st_Dod_Pnd_1st_Yyyymm.setValue(cntrct_Cntrct_First_Annt_Dod_Dte);                                                                                //Natural: ASSIGN #1ST-YYYYMM := CNTRCT-FIRST-ANNT-DOD-DTE
                    pnd_1st_Dod_Pnd_1st_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN #1ST-DD := 01
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_2nd_Dod.equals(" ")))                                                                                                                           //Natural: IF #2ND-DOD = ' '
        {
            if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                            //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
            {
                if (condition(pnd_Input_Pnd_In_Pin.notEquals(pnd_02_Pin)))                                                                                                //Natural: IF #IN-PIN NE #02-PIN
                {
                    pnd_Oth_Pin.setValue(pnd_02_Pin);                                                                                                                     //Natural: ASSIGN #OTH-PIN := #02-PIN
                                                                                                                                                                          //Natural: PERFORM GET-OTHER-DOD
                    sub_Get_Other_Dod();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_2nd_Dod.equals(" ")))                                                                                                                   //Natural: IF #2ND-DOD = ' '
                {
                    pnd_2nd_Dod_Pnd_2nd_Yyyymm.setValue(cntrct_Cntrct_Scnd_Annt_Dod_Dte);                                                                                 //Natural: ASSIGN #2ND-YYYYMM := CNTRCT-SCND-ANNT-DOD-DTE
                    pnd_2nd_Dod_Pnd_2nd_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN #2ND-DD := 01
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Other_Dod() throws Exception                                                                                                                     //Natural: GET-OTHER-DOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr.setValue(pnd_Oth_Pin);                                                                                                   //Natural: ASSIGN #STTLMNT-ID-NBR := #OTH-PIN
        vw_settl.startDatabaseRead                                                                                                                                        //Natural: READ SETTL BY PIN-TAXID-SEQ-KEY STARTING FROM #PIN-TAXID-SEQ-KEY
        (
        "READ06",
        new Wc[] { new Wc("PIN_TAXID_SEQ_KEY", ">=", pnd_Pin_Taxid_Seq_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_KEY", "ASC") }
        );
        READ06:
        while (condition(vw_settl.readNextRow("READ06")))
        {
            if (condition(settl_Sttlmnt_Id_Nbr.notEquals(pnd_Pin_Taxid_Seq_Key_Pnd_Sttlmnt_Id_Nbr)))                                                                      //Natural: IF STTLMNT-ID-NBR NE #STTLMNT-ID-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(DbsUtil.maskMatches(settl_Sttlmnt_Process_Type,"'ID'"))))                                                                                     //Natural: ACCEPT IF STTLMNT-PROCESS-TYPE = MASK ( 'ID' )
            {
                continue;
            }
            short decideConditionsMet493 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE STTLMNT-PROCESS-TYPE;//Natural: VALUE 'ID1A'
            if (condition((settl_Sttlmnt_Process_Type.equals("ID1A"))))
            {
                decideConditionsMet493++;
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                              //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #1ST-DOD-6
                {
                    pnd_1st_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                          //Natural: ASSIGN #1ST-DOD := STTLMNT-DOD-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'ID2A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID2A"))))
            {
                decideConditionsMet493++;
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                           //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_2nd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                       //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #1ST-DOD-6
                        {
                            pnd_2nd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-DOD-DTE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'ID3A'
            else if (condition((settl_Sttlmnt_Process_Type.equals("ID3A"))))
            {
                decideConditionsMet493++;
                if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                              //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #1ST-DOD-6
                {
                    pnd_1st_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                          //Natural: ASSIGN #1ST-DOD := STTLMNT-DOD-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(cntrct_Cntrct_First_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                          //Natural: IF CNTRCT-FIRST-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_1st_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #1ST-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())))                                                                                        //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE GT 0
                {
                    if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_2nd_Dod_6)))                                                                           //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #2ND-DOD-6
                    {
                        pnd_2nd_Dod.setValue(settl_Sttlmnt_2nd_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-2ND-DOD-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(cntrct_Cntrct_Scnd_Annt_Dod_Dte.equals(settl_Pnd_1st_Dod_6)))                                                                       //Natural: IF CNTRCT-SCND-ANNT-DOD-DTE = #1ST-DOD-6
                        {
                            pnd_2nd_Dod.setValue(settl_Sttlmnt_Dod_Dte);                                                                                                  //Natural: ASSIGN #2ND-DOD := STTLMNT-DOD-DTE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  12/14 FROM #IN-SSN
    //*  12/14 FROM #PIN
    //*  12/14
    //*  12/14
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "SSN",                                                                                                                                    //Natural: DISPLAY ( 1 ) 'SSN' #O-SSN 'PIN' #O-PIN 'OPTN' CNTRCT-OPTN-CDE 'ORGN' CNTRCT-ORGN-CDE '1ST DOD' #1ST-DOD '2ND DOD' #2ND-DOD 'FIN PER PAY DTE' #FIN-PER-PAY-DTE 'CONTRACT' #O-CNTRCT 'PY' #O-PAYEE 'STATUS' #STATUS 'STTS DTE' #STATUS-DTE
        		pnd_O_Ssn,"PIN",
        		pnd_O_Pin,"OPTN",
        		cntrct_Cntrct_Optn_Cde,"ORGN",
        		cntrct_Cntrct_Orgn_Cde,"1ST DOD",
        		pnd_1st_Dod,"2ND DOD",
        		pnd_2nd_Dod,"FIN PER PAY DTE",
        		pnd_Fin_Per_Pay_Dte,"CONTRACT",
        		pnd_O_Cntrct,"PY",
        		pnd_O_Payee,"STATUS",
        		pnd_Status,"STTS DTE",
        		pnd_Status_Dte);
        if (Global.isEscape()) return;
        pnd_Cntrl_Pnd_Rec_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-CNT
    }
    private void sub_Get_All_Payees() throws Exception                                                                                                                    //Natural: GET-ALL-PAYEES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_cpr1.startDatabaseRead                                                                                                                                         //Natural: READ CPR1 BY CNTRCT-PAYEE-KEY STARTING FROM CPR.CNTRCT-PART-PPCN-NBR
        (
        "READ07",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", cpr_Cntrct_Part_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ07:
        while (condition(vw_cpr1.readNextRow("READ07")))
        {
            if (condition(cpr1_Cntrct_Part_Ppcn_Nbr.notEquals(cpr_Cntrct_Part_Ppcn_Nbr)))                                                                                 //Natural: IF CPR1.CNTRCT-PART-PPCN-NBR NE CPR.CNTRCT-PART-PPCN-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fin_Per_Pay_Dte.reset();                                                                                                                                  //Natural: RESET #FIN-PER-PAY-DTE
            if (condition(cpr1_Cntrct_Final_Per_Pay_Dte.greater(getZero())))                                                                                              //Natural: IF CNTRCT-FINAL-PER-PAY-DTE GT 0
            {
                pnd_Fin_Per_Pay_Dte.setValue(cpr1_Cntrct_Final_Per_Pay_Dte);                                                                                              //Natural: ASSIGN #FIN-PER-PAY-DTE := CNTRCT-FINAL-PER-PAY-DTE
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd.setValue(1);                                                                                                               //Natural: ASSIGN #FIN-DD := 01
            }                                                                                                                                                             //Natural: END-IF
            //*  03/15 - START
            if (condition(pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm.equals(999999)))                                                                                             //Natural: IF #FIN-YYYYMM = 999999
            {
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Yyyymm.setValue(999912);                                                                                                      //Natural: ASSIGN #FIN-YYYYMM := 999912
                pnd_Fin_Per_Pay_Dte_Pnd_Fin_Dd.setValue(31);                                                                                                              //Natural: ASSIGN #FIN-DD := 31
            }                                                                                                                                                             //Natural: END-IF
            //*  03/15 - END
            short decideConditionsMet562 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-ACTVTY-CDE = 9
            if (condition(cpr1_Cntrct_Actvty_Cde.equals(9)))
            {
                decideConditionsMet562++;
                pnd_Status.setValue("TERMINATED");                                                                                                                        //Natural: ASSIGN #STATUS := 'TERMINATED'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PEND-CDE = 'R'
            else if (condition(cpr1_Cntrct_Pend_Cde.equals("R")))
            {
                decideConditionsMet562++;
                pnd_Status.setValue("EXPIRED");                                                                                                                           //Natural: ASSIGN #STATUS := 'EXPIRED'
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PEND-CDE NE '0'
            else if (condition(cpr1_Cntrct_Pend_Cde.notEquals("0")))
            {
                decideConditionsMet562++;
                pnd_Status.setValue("PENDING");                                                                                                                           //Natural: ASSIGN #STATUS := 'PENDING'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Status.setValue("ACTIVE");                                                                                                                            //Natural: ASSIGN #STATUS := 'ACTIVE'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  12/14 - START
            if (condition(cpr1_Prtcpnt_Tax_Id_Nbr.equals(getZero())))                                                                                                     //Natural: IF PRTCPNT-TAX-ID-NBR = 0
            {
                cpr1_Prtcpnt_Tax_Id_Nbr.setValue(pnd_Input_Pnd_In_Ssn);                                                                                                   //Natural: ASSIGN PRTCPNT-TAX-ID-NBR := #IN-SSN
            }                                                                                                                                                             //Natural: END-IF
            //*  12/14 - END
            getWorkFiles().write(2, false, cpr1_Prtcpnt_Tax_Id_Nbr, "|", cpr1_Cpr_Id_Nbr, "|", cntrct_Cntrct_Optn_Cde, "|", cntrct_Cntrct_Orgn_Cde, "|",                  //Natural: WRITE WORK FILE 2 PRTCPNT-TAX-ID-NBR '|' CPR-ID-NBR '|' CNTRCT-OPTN-CDE '|' CNTRCT-ORGN-CDE '|' #1ST-DOD '|' #2ND-DOD '|' #FIN-PER-PAY-DTE '|' CNTRCT-PART-PPCN-NBR '|' CNTRCT-PART-PAYEE-CDE '|' #STATUS '|' #STATUS-DTE
                pnd_1st_Dod, "|", pnd_2nd_Dod, "|", pnd_Fin_Per_Pay_Dte, "|", cpr1_Cntrct_Part_Ppcn_Nbr, "|", cpr1_Cntrct_Part_Payee_Cde, "|", pnd_Status, 
                "|", pnd_Status_Dte);
            pnd_O_Ssn.setValue(cpr1_Prtcpnt_Tax_Id_Nbr);                                                                                                                  //Natural: ASSIGN #O-SSN := PRTCPNT-TAX-ID-NBR
            pnd_O_Pin.setValue(cpr1_Cpr_Id_Nbr);                                                                                                                          //Natural: ASSIGN #O-PIN := CPR-ID-NBR
            pnd_O_Cntrct.setValue(cpr1_Cntrct_Part_Ppcn_Nbr);                                                                                                             //Natural: ASSIGN #O-CNTRCT := CNTRCT-PART-PPCN-NBR
            pnd_O_Payee.setValue(cpr1_Cntrct_Part_Payee_Cde);                                                                                                             //Natural: ASSIGN #O-PAYEE := CNTRCT-PART-PAYEE-CDE
            //*  12/14 - END
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(20),"MONTHLY IA-RMW DEATH NOTIFICATION",new ColumnSpacing(20),Global.getDATX(),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 20X 'MONTHLY IA-RMW DEATH NOTIFICATION' 20X *DATX ( EM = MM/DD/YYYY )
                        new ReportEditMask ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=133 ZP=OFF");

        getReports().setDisplayColumns(1, "SSN",
        		pnd_O_Ssn,"PIN",
        		pnd_O_Pin,"OPTN",
        		cntrct_Cntrct_Optn_Cde,"ORGN",
        		cntrct_Cntrct_Orgn_Cde,"1ST DOD",
        		pnd_1st_Dod,"2ND DOD",
        		pnd_2nd_Dod,"FIN PER PAY DTE",
        		pnd_Fin_Per_Pay_Dte,"CONTRACT",
        		pnd_O_Cntrct,"PY",
        		pnd_O_Payee,"STATUS",
        		pnd_Status,"STTS DTE",
        		pnd_Status_Dte);
    }
}
