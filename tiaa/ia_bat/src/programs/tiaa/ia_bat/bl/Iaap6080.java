/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:23 PM
**        * FROM NATURAL PROGRAM : Iaap6080
************************************************************
**        * FILE NAME            : Iaap6080.java
**        * CLASS NAME           : Iaap6080
**        * INSTANCE NAME        : Iaap6080
************************************************************
***********************************************************************
*                                                                     *
*  PROGRAM      -           : REPLACES COBOL PROGRAM PIA6080          *
*  HISTORY:
*
*  5/2015 RC    - COR/NAAD DECOMMISSION.
*  8/2017 RC    - PIN EXPANSION
*
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap6080 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Inp_Low_Cont_Work;

    private DbsGroup pnd_Inp_Low_Cont_Work__R_Field_1;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Low_Cont_No_Lc;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc;

    private DbsGroup pnd_Inp_Low_Cont_Work__R_Field_2;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc;

    private DbsGroup pnd_Inp_Low_Cont_Work__R_Field_3;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Prefix_Lc;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Numbers_Lc;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Check_Dg_Lc;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Filler;

    private DbsGroup pnd_Inp_Low_Cont_Work__R_Field_4;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_1;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc;

    private DbsGroup pnd_Inp_Low_Cont_Work__R_Field_5;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_2;
    private DbsField pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc_8;
    private DbsField pnd_Out_Low_Seq_Rec;

    private DbsGroup pnd_Out_Low_Seq_Rec__R_Field_6;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls;

    private DbsGroup pnd_Out_Low_Seq_Rec__R_Field_7;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls;

    private DbsGroup pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls;
    private DbsField pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls;

    private DbsGroup pnd_Name_Check_Out;
    private DbsField pnd_Name_Check_Out_Pnd_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Check_Out_Pnd_Cntrct_Nmbr;
    private DbsField pnd_Name_Check_Out_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Check_Out_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Check_Out__R_Field_8;
    private DbsField pnd_Name_Check_Out_Pnd_Cntrct_Name_Free;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_1;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_2;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_3;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_4;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_5;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Lne_6;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Check_Out__R_Field_9;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Walk_Rte;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Check_Out_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Check_Out__R_Field_10;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Type_Cde;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Check_Out_Pnd_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Check_Out_Pnd_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Check_Out_Pnd_Eft_Status_Ind;
    private DbsField pnd_Name_Check_Out_Pnd_Checking_Saving_Cde;
    private DbsField pnd_Name_Check_Out_Pnd_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Check_Out_Pnd_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Pnd_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Check_Out_Pnd_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Check_Out_Pnd_Check_Mailing_Addrss_Ind;
    private DbsField pnd_Name_Check_Out_Addrss_Geographic_Cde;
    private DbsField pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde;
    private DbsField pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr;
    private DbsField pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Check_Out_Eft_Pre_Note_Status_Ind;

    private DbsGroup pnd_Name_Corr_Out;
    private DbsField pnd_Name_Corr_Out_Ph_Unque_Id_Nmbr;
    private DbsField pnd_Name_Corr_Out_Cntrct_Nmbr;
    private DbsField pnd_Name_Corr_Out_Cntrct_Payee_Cde;
    private DbsField pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add;

    private DbsGroup pnd_Name_Corr_Out__R_Field_11;
    private DbsField pnd_Name_Corr_Out_Cntrct_Name_Free;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_1;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_2;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_3;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_4;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_5;
    private DbsField pnd_Name_Corr_Out_Addrss_Lne_6;
    private DbsField pnd_Name_Corr_Out_Addrss_Postal_Data;

    private DbsGroup pnd_Name_Corr_Out__R_Field_12;
    private DbsField pnd_Name_Corr_Out_Addrss_Zip_Plus_4;
    private DbsField pnd_Name_Corr_Out_Addrss_Carrier_Rte;
    private DbsField pnd_Name_Corr_Out_Addrss_Walk_Rte;
    private DbsField pnd_Name_Corr_Out_Addrss_Usps_Future_Use;
    private DbsField pnd_Name_Corr_Out_Pnd_Rest_Of_Record;

    private DbsGroup pnd_Name_Corr_Out__R_Field_13;
    private DbsField pnd_Name_Corr_Out_Addrss_Type_Cde;
    private DbsField pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Addrss_Last_Chnge_Time;
    private DbsField pnd_Name_Corr_Out_Permanent_Addrss_Ind;
    private DbsField pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Out_Eft_Status_Ind;
    private DbsField pnd_Name_Corr_Out_Checking_Saving_Cde;
    private DbsField pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte;
    private DbsField pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte;
    private DbsField pnd_Name_Corr_Out_Correspondence_Addrss_Ind;
    private DbsField pnd_Name_Corr_Out_Addrss_Geographic_Cde;

    private DbsGroup pnd_Master_Work_Rec;
    private DbsField pnd_Master_Work_Rec_Pnd_Contract_Payee_M;

    private DbsGroup pnd_Master_Work_Rec__R_Field_14;
    private DbsField pnd_Master_Work_Rec_Pnd_Contract_No_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Payee_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Rec_Type_M;

    private DbsGroup pnd_Master_Work_Rec_Pnd_Rec_Type_10;
    private DbsField pnd_Master_Work_Rec_Filler_1;
    private DbsField pnd_Master_Work_Rec_Pnd_Currency_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Mode_M;
    private DbsField pnd_Master_Work_Rec_Filler_2;
    private DbsField pnd_Master_Work_Rec_Pnd_Option_M;
    private DbsField pnd_Master_Work_Rec_Filler_3;
    private DbsField pnd_Master_Work_Rec_Pnd_J_C_Code_M;
    private DbsField pnd_Master_Work_Rec_Filler_4;

    private DbsGroup pnd_Master_Work_Rec__R_Field_15;

    private DbsGroup pnd_Master_Work_Rec_Pnd_Rec_Type_20;
    private DbsField pnd_Master_Work_Rec_Filler_5;
    private DbsField pnd_Master_Work_Rec_Pnd_X_Ref_1_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Sex_1_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Dob_1_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Dod_1_M;
    private DbsField pnd_Master_Work_Rec_Filler_6;
    private DbsField pnd_Master_Work_Rec_Pnd_X_Ref_2_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Sex_2_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Dob_2_M;
    private DbsField pnd_Master_Work_Rec_Pnd_Dod_2_M;
    private DbsField pnd_Master_Work_Rec_Filler_7;

    private DbsGroup pnd_Master_Work_Rec__R_Field_16;

    private DbsGroup pnd_Master_Work_Rec_Pnd_Rec_Type_70_1;
    private DbsField pnd_Master_Work_Rec_Filler_8;
    private DbsField pnd_Master_Work_Rec_Pnd_Soc_Sec_No_M;
    private DbsField pnd_Master_Work_Rec_Filler_9;

    private DbsGroup pnd_Master_Work_Rec__R_Field_17;

    private DbsGroup pnd_Master_Work_Rec_Pnd_Rec_Type_70_2;
    private DbsField pnd_Master_Work_Rec_Filler_10;
    private DbsField pnd_Master_Work_Rec_Pnd_Soc_Ins_No_M;
    private DbsField pnd_Master_Work_Rec_Filler_11;
    private DbsField pnd_Hold_File;

    private DbsGroup pnd_Hold_File__R_Field_18;
    private DbsField pnd_Hold_File_Pnd_Hold_Low_Seq_H;

    private DbsGroup pnd_Hold_File__R_Field_19;
    private DbsField pnd_Hold_File_Pnd_Hold_Filler;
    private DbsField pnd_Hold_File_Pnd_Hold_Low_Ck_H;
    private DbsField pnd_Hold_File_Pnd_Hold_Contract_No_H;
    private DbsField pnd_Ws_Addr_Lne8_Info;

    private DbsGroup pnd_Ws_Addr_Lne8_Info__R_Field_20;
    private DbsField pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Ph_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Checking_Saving_Cde;
    private DbsField pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Status_Ind;
    private DbsField pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Pre_Note_Status_Ind;
    private DbsField pnd_Ws_Addr_Lne8_Info_Ws_Fill;
    private DbsField pnd_Check_Pmt_Date_Link;

    private DbsGroup pnd_Check_Pmt_Date_Link__R_Field_21;
    private DbsField pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Cc;
    private DbsField pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Yy;
    private DbsField pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Mm;
    private DbsField pnd_Ppcn;
    private DbsField pnd_Dup;
    private DbsField pnd_Key_Saved;
    private DbsField pnd_Ms_End;
    private DbsField pnd_Master_Found;
    private DbsField pnd_Naad_Etl_Record;
    private DbsField pnd_Naad_Etl_Payee_01;
    private DbsField pnd_Etl_Eof;
    private DbsField pnd_Input_Rec;
    private DbsField pnd_Total_No_Ms;
    private DbsField pnd_Total_No_Na;
    private DbsField pnd_Total_Opt;
    private DbsField pnd_Total_J;
    private DbsField pnd_Total_Can_Cref;
    private DbsField pnd_Total_Group;
    private DbsField pnd_Output_Rec_Cnt;
    private DbsField pnd_Dont_Process;
    private DbsField pnd_Etl_Found;
    private DbsField pnd_Etl_Ppcn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Inp_Low_Cont_Work = localVariables.newFieldInRecord("pnd_Inp_Low_Cont_Work", "#INP-LOW-CONT-WORK", FieldType.STRING, 18);

        pnd_Inp_Low_Cont_Work__R_Field_1 = localVariables.newGroupInRecord("pnd_Inp_Low_Cont_Work__R_Field_1", "REDEFINE", pnd_Inp_Low_Cont_Work);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Low_Cont_No_Lc = pnd_Inp_Low_Cont_Work__R_Field_1.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Low_Cont_No_Lc", 
            "#INP-LOW-CONT-NO-LC", FieldType.STRING, 8);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc = pnd_Inp_Low_Cont_Work__R_Field_1.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc", "#INP-KEY-LC", 
            FieldType.STRING, 10);

        pnd_Inp_Low_Cont_Work__R_Field_2 = pnd_Inp_Low_Cont_Work__R_Field_1.newGroupInGroup("pnd_Inp_Low_Cont_Work__R_Field_2", "REDEFINE", pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id = pnd_Inp_Low_Cont_Work__R_Field_2.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id", "#INP-FILE-ID", 
            FieldType.STRING, 1);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc = pnd_Inp_Low_Cont_Work__R_Field_2.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc", 
            "#INP-CONTRACT-NO-LC", FieldType.STRING, 8);

        pnd_Inp_Low_Cont_Work__R_Field_3 = pnd_Inp_Low_Cont_Work__R_Field_1.newGroupInGroup("pnd_Inp_Low_Cont_Work__R_Field_3", "REDEFINE", pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc = pnd_Inp_Low_Cont_Work__R_Field_3.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc", "#INP-FILE-ID-LC", 
            FieldType.STRING, 1);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Prefix_Lc = pnd_Inp_Low_Cont_Work__R_Field_3.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Prefix_Lc", "#INP-PREFIX-LC", 
            FieldType.STRING, 2);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Numbers_Lc = pnd_Inp_Low_Cont_Work__R_Field_3.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Numbers_Lc", "#INP-NUMBERS-LC", 
            FieldType.NUMERIC, 5);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Check_Dg_Lc = pnd_Inp_Low_Cont_Work__R_Field_3.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Check_Dg_Lc", "#INP-CHECK-DG-LC", 
            FieldType.STRING, 1);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Filler = pnd_Inp_Low_Cont_Work__R_Field_3.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Filler", "#INP-FILLER", 
            FieldType.STRING, 1);

        pnd_Inp_Low_Cont_Work__R_Field_4 = pnd_Inp_Low_Cont_Work__R_Field_1.newGroupInGroup("pnd_Inp_Low_Cont_Work__R_Field_4", "REDEFINE", pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_1 = pnd_Inp_Low_Cont_Work__R_Field_4.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_1", "#INP-FILE-ID-LC-1", 
            FieldType.STRING, 1);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc = pnd_Inp_Low_Cont_Work__R_Field_4.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc", "#INP-CONTRACT-LC", 
            FieldType.STRING, 7);

        pnd_Inp_Low_Cont_Work__R_Field_5 = pnd_Inp_Low_Cont_Work__R_Field_1.newGroupInGroup("pnd_Inp_Low_Cont_Work__R_Field_5", "REDEFINE", pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_2 = pnd_Inp_Low_Cont_Work__R_Field_5.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_File_Id_Lc_2", "#INP-FILE-ID-LC-2", 
            FieldType.STRING, 1);
        pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc_8 = pnd_Inp_Low_Cont_Work__R_Field_5.newFieldInGroup("pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc_8", 
            "#INP-CONTRACT-LC-8", FieldType.STRING, 8);
        pnd_Out_Low_Seq_Rec = localVariables.newFieldInRecord("pnd_Out_Low_Seq_Rec", "#OUT-LOW-SEQ-REC", FieldType.STRING, 334);

        pnd_Out_Low_Seq_Rec__R_Field_6 = localVariables.newGroupInRecord("pnd_Out_Low_Seq_Rec__R_Field_6", "REDEFINE", pnd_Out_Low_Seq_Rec);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls", "#OUT-LOW-SEQ-NO-LS", 
            FieldType.STRING, 8);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls", "#OUT-PAYEE-1-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls", "#OUT-CONTRACT-NO-LS", 
            FieldType.STRING, 8);

        pnd_Out_Low_Seq_Rec__R_Field_7 = pnd_Out_Low_Seq_Rec__R_Field_6.newGroupInGroup("pnd_Out_Low_Seq_Rec__R_Field_7", "REDEFINE", pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls = pnd_Out_Low_Seq_Rec__R_Field_7.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls", "#OUT-CON-PRE-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls = pnd_Out_Low_Seq_Rec__R_Field_7.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls", "#OUT-CON-NUM-LS", 
            FieldType.STRING, 6);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls", "#OUT-PAYEE-2-LS", 
            FieldType.STRING, 2);

        pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newGroupInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls", "#OUT-NAME-ADDR-LS");
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1", "#OUT-ADDR-LINE1", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2", "#OUT-ADDR-LINE2", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3", "#OUT-ADDR-LINE3", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4", "#OUT-ADDR-LINE4", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5", "#OUT-ADDR-LINE5", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6", "#OUT-ADDR-LINE6", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7", "#OUT-ADDR-LINE7", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8 = pnd_Out_Low_Seq_Rec_Pnd_Out_Name_Addr_Ls.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8", "#OUT-ADDR-LINE8", 
            FieldType.STRING, 35);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls", "#OUT-CURRENCY-LS", 
            FieldType.STRING, 1);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls", "#OUT-SEX-LS", FieldType.STRING, 
            1);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls", "#OUT-DOB-LS", FieldType.NUMERIC, 
            8);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls", "#OUT-MODE-LS", FieldType.STRING, 
            3);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls", "#OUT-SOC-SEC-NO-LS", 
            FieldType.STRING, 9);
        pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls", "#OUT-X-REF-LS", 
            FieldType.STRING, 9);
        pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls", "#OUT-OPTION-LS", 
            FieldType.STRING, 2);
        pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls = pnd_Out_Low_Seq_Rec__R_Field_6.newFieldInGroup("pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls", "#OUT-J-C-CODE-LS", 
            FieldType.STRING, 1);

        pnd_Name_Check_Out = localVariables.newGroupInRecord("pnd_Name_Check_Out", "#NAME-CHECK-OUT");
        pnd_Name_Check_Out_Pnd_Ph_Unque_Id_Nmbr = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Ph_Unque_Id_Nmbr", "#PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 
            12);
        pnd_Name_Check_Out_Pnd_Cntrct_Nmbr = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Cntrct_Nmbr", "#CNTRCT-NMBR", FieldType.STRING, 
            10);
        pnd_Name_Check_Out_Pnd_Cntrct_Payee_Cde = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Check_Out_Pnd_Cntrct_Name_Add = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);

        pnd_Name_Check_Out__R_Field_8 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_8", "REDEFINE", pnd_Name_Check_Out_Pnd_Cntrct_Name_Add);
        pnd_Name_Check_Out_Pnd_Cntrct_Name_Free = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Cntrct_Name_Free", "#CNTRCT-NAME-FREE", 
            FieldType.STRING, 35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_1 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_1", "#ADDRSS-LNE-1", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_2 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_2", "#ADDRSS-LNE-2", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_3 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_3", "#ADDRSS-LNE-3", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_4 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_4", "#ADDRSS-LNE-4", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_5 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_5", "#ADDRSS-LNE-5", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Lne_6 = pnd_Name_Check_Out__R_Field_8.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Lne_6", "#ADDRSS-LNE-6", FieldType.STRING, 
            35);
        pnd_Name_Check_Out_Pnd_Addrss_Postal_Data = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Postal_Data", "#ADDRSS-POSTAL-DATA", 
            FieldType.STRING, 32);

        pnd_Name_Check_Out__R_Field_9 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_9", "REDEFINE", pnd_Name_Check_Out_Pnd_Addrss_Postal_Data);
        pnd_Name_Check_Out_Pnd_Addrss_Zip_Plus_4 = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Zip_Plus_4", "#ADDRSS-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pnd_Name_Check_Out_Pnd_Addrss_Carrier_Rte = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Carrier_Rte", "#ADDRSS-CARRIER-RTE", 
            FieldType.STRING, 4);
        pnd_Name_Check_Out_Pnd_Addrss_Walk_Rte = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Walk_Rte", "#ADDRSS-WALK-RTE", 
            FieldType.STRING, 4);
        pnd_Name_Check_Out_Pnd_Addrss_Usps_Future_Use = pnd_Name_Check_Out__R_Field_9.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Usps_Future_Use", 
            "#ADDRSS-USPS-FUTURE-USE", FieldType.STRING, 15);
        pnd_Name_Check_Out_Pnd_Rest_Of_Record = pnd_Name_Check_Out.newFieldInGroup("pnd_Name_Check_Out_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            149);

        pnd_Name_Check_Out__R_Field_10 = pnd_Name_Check_Out.newGroupInGroup("pnd_Name_Check_Out__R_Field_10", "REDEFINE", pnd_Name_Check_Out_Pnd_Rest_Of_Record);
        pnd_Name_Check_Out_Pnd_Addrss_Type_Cde = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Type_Cde", "#ADDRSS-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Dte = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Dte", 
            "#ADDRSS-LAST-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Time = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Addrss_Last_Chnge_Time", 
            "#ADDRSS-LAST-CHNGE-TIME", FieldType.NUMERIC, 7);
        pnd_Name_Check_Out_Pnd_Permanent_Addrss_Ind = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Permanent_Addrss_Ind", "#PERMANENT-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Pnd_Bank_Aba_Acct_Nmbr = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Bank_Aba_Acct_Nmbr", "#BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Name_Check_Out_Pnd_Eft_Status_Ind = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Eft_Status_Ind", "#EFT-STATUS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Pnd_Checking_Saving_Cde = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Checking_Saving_Cde", "#CHECKING-SAVING-CDE", 
            FieldType.STRING, 1);
        pnd_Name_Check_Out_Pnd_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Ph_Bank_Pymnt_Acct_Nmbr", 
            "#PH-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21);
        pnd_Name_Check_Out_Pnd_Pending_Addrss_Chnge_Dte = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Pending_Addrss_Chnge_Dte", 
            "#PENDING-ADDRSS-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pnd_Pending_Addrss_Restore_Dte = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Pending_Addrss_Restore_Dte", 
            "#PENDING-ADDRSS-RESTORE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pnd_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Pending_Perm_Addrss_Chnge_Dte", 
            "#PENDING-PERM-ADDRSS-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Check_Out_Pnd_Check_Mailing_Addrss_Ind = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Pnd_Check_Mailing_Addrss_Ind", 
            "#CHECK-MAILING-ADDRSS-IND", FieldType.STRING, 1);
        pnd_Name_Check_Out_Addrss_Geographic_Cde = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Intl_Eft_Pay_Type_Cde", "INTL-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Intl_Bank_Pymnt_Eft_Nmbr", "INTL-BANK-PYMNT-EFT-NMBR", 
            FieldType.STRING, 35);
        pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Intl_Bank_Pymnt_Acct_Nmbr", 
            "INTL-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 35);
        pnd_Name_Check_Out_Eft_Pre_Note_Status_Ind = pnd_Name_Check_Out__R_Field_10.newFieldInGroup("pnd_Name_Check_Out_Eft_Pre_Note_Status_Ind", "EFT-PRE-NOTE-STATUS-IND", 
            FieldType.STRING, 1);

        pnd_Name_Corr_Out = localVariables.newGroupInRecord("pnd_Name_Corr_Out", "#NAME-CORR-OUT");
        pnd_Name_Corr_Out_Ph_Unque_Id_Nmbr = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Ph_Unque_Id_Nmbr", "PH-UNQUE-ID-NMBR", FieldType.NUMERIC, 
            12);
        pnd_Name_Corr_Out_Cntrct_Nmbr = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Cntrct_Nmbr", "CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Name_Corr_Out_Cntrct_Payee_Cde = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add", "#CNTRCT-NAME-ADD", FieldType.STRING, 
            245);

        pnd_Name_Corr_Out__R_Field_11 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_11", "REDEFINE", pnd_Name_Corr_Out_Pnd_Cntrct_Name_Add);
        pnd_Name_Corr_Out_Cntrct_Name_Free = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Cntrct_Name_Free", "CNTRCT-NAME-FREE", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_1 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_1", "ADDRSS-LNE-1", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_2 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_2", "ADDRSS-LNE-2", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_3 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_3", "ADDRSS-LNE-3", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_4 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_4", "ADDRSS-LNE-4", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_5 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_5", "ADDRSS-LNE-5", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Lne_6 = pnd_Name_Corr_Out__R_Field_11.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Lne_6", "ADDRSS-LNE-6", FieldType.STRING, 
            35);
        pnd_Name_Corr_Out_Addrss_Postal_Data = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Postal_Data", "ADDRSS-POSTAL-DATA", FieldType.STRING, 
            32);

        pnd_Name_Corr_Out__R_Field_12 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_12", "REDEFINE", pnd_Name_Corr_Out_Addrss_Postal_Data);
        pnd_Name_Corr_Out_Addrss_Zip_Plus_4 = pnd_Name_Corr_Out__R_Field_12.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Zip_Plus_4", "ADDRSS-ZIP-PLUS-4", 
            FieldType.STRING, 9);
        pnd_Name_Corr_Out_Addrss_Carrier_Rte = pnd_Name_Corr_Out__R_Field_12.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Carrier_Rte", "ADDRSS-CARRIER-RTE", 
            FieldType.STRING, 4);
        pnd_Name_Corr_Out_Addrss_Walk_Rte = pnd_Name_Corr_Out__R_Field_12.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Walk_Rte", "ADDRSS-WALK-RTE", FieldType.STRING, 
            4);
        pnd_Name_Corr_Out_Addrss_Usps_Future_Use = pnd_Name_Corr_Out__R_Field_12.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Usps_Future_Use", "ADDRSS-USPS-FUTURE-USE", 
            FieldType.STRING, 15);
        pnd_Name_Corr_Out_Pnd_Rest_Of_Record = pnd_Name_Corr_Out.newFieldInGroup("pnd_Name_Corr_Out_Pnd_Rest_Of_Record", "#REST-OF-RECORD", FieldType.STRING, 
            76);

        pnd_Name_Corr_Out__R_Field_13 = pnd_Name_Corr_Out.newGroupInGroup("pnd_Name_Corr_Out__R_Field_13", "REDEFINE", pnd_Name_Corr_Out_Pnd_Rest_Of_Record);
        pnd_Name_Corr_Out_Addrss_Type_Cde = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Type_Cde", "ADDRSS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Last_Chnge_Dte", "ADDRSS-LAST-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Addrss_Last_Chnge_Time = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Last_Chnge_Time", "ADDRSS-LAST-CHNGE-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Name_Corr_Out_Permanent_Addrss_Ind = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Permanent_Addrss_Ind", "PERMANENT-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Bank_Aba_Acct_Nmbr", "BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Name_Corr_Out_Eft_Status_Ind = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Eft_Status_Ind", "EFT-STATUS-IND", FieldType.STRING, 
            1);
        pnd_Name_Corr_Out_Checking_Saving_Cde = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Checking_Saving_Cde", "CHECKING-SAVING-CDE", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Ph_Bank_Pymnt_Acct_Nmbr", "PH-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Pending_Addrss_Chnge_Dte", "PENDING-ADDRSS-CHNGE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Pending_Addrss_Restore_Dte", "PENDING-ADDRSS-RESTORE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Pending_Perm_Addrss_Chnge_Dte", 
            "PENDING-PERM-ADDRSS-CHNGE-DTE", FieldType.NUMERIC, 8);
        pnd_Name_Corr_Out_Correspondence_Addrss_Ind = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Correspondence_Addrss_Ind", "CORRESPONDENCE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Name_Corr_Out_Addrss_Geographic_Cde = pnd_Name_Corr_Out__R_Field_13.newFieldInGroup("pnd_Name_Corr_Out_Addrss_Geographic_Cde", "ADDRSS-GEOGRAPHIC-CDE", 
            FieldType.STRING, 2);

        pnd_Master_Work_Rec = localVariables.newGroupInRecord("pnd_Master_Work_Rec", "#MASTER-WORK-REC");
        pnd_Master_Work_Rec_Pnd_Contract_Payee_M = pnd_Master_Work_Rec.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Contract_Payee_M", "#CONTRACT-PAYEE-M", 
            FieldType.STRING, 10);

        pnd_Master_Work_Rec__R_Field_14 = pnd_Master_Work_Rec.newGroupInGroup("pnd_Master_Work_Rec__R_Field_14", "REDEFINE", pnd_Master_Work_Rec_Pnd_Contract_Payee_M);
        pnd_Master_Work_Rec_Pnd_Contract_No_M = pnd_Master_Work_Rec__R_Field_14.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Contract_No_M", "#CONTRACT-NO-M", 
            FieldType.STRING, 8);
        pnd_Master_Work_Rec_Pnd_Payee_M = pnd_Master_Work_Rec__R_Field_14.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Payee_M", "#PAYEE-M", FieldType.STRING, 
            2);
        pnd_Master_Work_Rec_Pnd_Rec_Type_M = pnd_Master_Work_Rec.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Rec_Type_M", "#REC-TYPE-M", FieldType.STRING, 
            2);

        pnd_Master_Work_Rec_Pnd_Rec_Type_10 = pnd_Master_Work_Rec.newGroupInGroup("pnd_Master_Work_Rec_Pnd_Rec_Type_10", "#REC-TYPE-10");
        pnd_Master_Work_Rec_Filler_1 = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_1", "FILLER-1", FieldType.STRING, 
            4);
        pnd_Master_Work_Rec_Pnd_Currency_M = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Currency_M", "#CURRENCY-M", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Pnd_Mode_M = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Mode_M", "#MODE-M", FieldType.STRING, 
            3);
        pnd_Master_Work_Rec_Filler_2 = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_2", "FILLER-2", FieldType.STRING, 
            6);
        pnd_Master_Work_Rec_Pnd_Option_M = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Option_M", "#OPTION-M", FieldType.STRING, 
            2);
        pnd_Master_Work_Rec_Filler_3 = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_3", "FILLER-3", FieldType.STRING, 
            52);
        pnd_Master_Work_Rec_Pnd_J_C_Code_M = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Pnd_J_C_Code_M", "#J-C-CODE-M", 
            FieldType.STRING, 1);
        pnd_Master_Work_Rec_Filler_4 = pnd_Master_Work_Rec_Pnd_Rec_Type_10.newFieldInGroup("pnd_Master_Work_Rec_Filler_4", "FILLER-4", FieldType.STRING, 
            19);

        pnd_Master_Work_Rec__R_Field_15 = pnd_Master_Work_Rec.newGroupInGroup("pnd_Master_Work_Rec__R_Field_15", "REDEFINE", pnd_Master_Work_Rec_Pnd_Rec_Type_10);

        pnd_Master_Work_Rec_Pnd_Rec_Type_20 = pnd_Master_Work_Rec__R_Field_15.newGroupInGroup("pnd_Master_Work_Rec_Pnd_Rec_Type_20", "#REC-TYPE-20");
        pnd_Master_Work_Rec_Filler_5 = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Filler_5", "FILLER-5", FieldType.STRING, 
            3);
        pnd_Master_Work_Rec_Pnd_X_Ref_1_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_X_Ref_1_M", "#X-REF-1-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Pnd_Sex_1_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Sex_1_M", "#SEX-1-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Pnd_Dob_1_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Dob_1_M", "#DOB-1-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Pnd_Dod_1_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Dod_1_M", "#DOD-1-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Filler_6 = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Filler_6", "FILLER-6", FieldType.STRING, 
            10);
        pnd_Master_Work_Rec_Pnd_X_Ref_2_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_X_Ref_2_M", "#X-REF-2-M", FieldType.STRING, 
            9);
        pnd_Master_Work_Rec_Pnd_Sex_2_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Sex_2_M", "#SEX-2-M", FieldType.STRING, 
            1);
        pnd_Master_Work_Rec_Pnd_Dob_2_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Dob_2_M", "#DOB-2-M", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Master_Work_Rec_Pnd_Dod_2_M = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Dod_2_M", "#DOD-2-M", FieldType.PACKED_DECIMAL, 
            6);
        pnd_Master_Work_Rec_Filler_7 = pnd_Master_Work_Rec_Pnd_Rec_Type_20.newFieldInGroup("pnd_Master_Work_Rec_Filler_7", "FILLER-7", FieldType.STRING, 
            37);

        pnd_Master_Work_Rec__R_Field_16 = pnd_Master_Work_Rec.newGroupInGroup("pnd_Master_Work_Rec__R_Field_16", "REDEFINE", pnd_Master_Work_Rec_Pnd_Rec_Type_10);

        pnd_Master_Work_Rec_Pnd_Rec_Type_70_1 = pnd_Master_Work_Rec__R_Field_16.newGroupInGroup("pnd_Master_Work_Rec_Pnd_Rec_Type_70_1", "#REC-TYPE-70-1");
        pnd_Master_Work_Rec_Filler_8 = pnd_Master_Work_Rec_Pnd_Rec_Type_70_1.newFieldInGroup("pnd_Master_Work_Rec_Filler_8", "FILLER-8", FieldType.STRING, 
            57);
        pnd_Master_Work_Rec_Pnd_Soc_Sec_No_M = pnd_Master_Work_Rec_Pnd_Rec_Type_70_1.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Soc_Sec_No_M", "#SOC-SEC-NO-M", 
            FieldType.STRING, 9);
        pnd_Master_Work_Rec_Filler_9 = pnd_Master_Work_Rec_Pnd_Rec_Type_70_1.newFieldInGroup("pnd_Master_Work_Rec_Filler_9", "FILLER-9", FieldType.STRING, 
            22);

        pnd_Master_Work_Rec__R_Field_17 = pnd_Master_Work_Rec.newGroupInGroup("pnd_Master_Work_Rec__R_Field_17", "REDEFINE", pnd_Master_Work_Rec_Pnd_Rec_Type_10);

        pnd_Master_Work_Rec_Pnd_Rec_Type_70_2 = pnd_Master_Work_Rec__R_Field_17.newGroupInGroup("pnd_Master_Work_Rec_Pnd_Rec_Type_70_2", "#REC-TYPE-70-2");
        pnd_Master_Work_Rec_Filler_10 = pnd_Master_Work_Rec_Pnd_Rec_Type_70_2.newFieldInGroup("pnd_Master_Work_Rec_Filler_10", "FILLER-10", FieldType.STRING, 
            49);
        pnd_Master_Work_Rec_Pnd_Soc_Ins_No_M = pnd_Master_Work_Rec_Pnd_Rec_Type_70_2.newFieldInGroup("pnd_Master_Work_Rec_Pnd_Soc_Ins_No_M", "#SOC-INS-NO-M", 
            FieldType.STRING, 9);
        pnd_Master_Work_Rec_Filler_11 = pnd_Master_Work_Rec_Pnd_Rec_Type_70_2.newFieldInGroup("pnd_Master_Work_Rec_Filler_11", "FILLER-11", FieldType.STRING, 
            30);
        pnd_Hold_File = localVariables.newFieldInRecord("pnd_Hold_File", "#HOLD-FILE", FieldType.STRING, 16);

        pnd_Hold_File__R_Field_18 = localVariables.newGroupInRecord("pnd_Hold_File__R_Field_18", "REDEFINE", pnd_Hold_File);
        pnd_Hold_File_Pnd_Hold_Low_Seq_H = pnd_Hold_File__R_Field_18.newFieldInGroup("pnd_Hold_File_Pnd_Hold_Low_Seq_H", "#HOLD-LOW-SEQ-H", FieldType.STRING, 
            8);

        pnd_Hold_File__R_Field_19 = pnd_Hold_File__R_Field_18.newGroupInGroup("pnd_Hold_File__R_Field_19", "REDEFINE", pnd_Hold_File_Pnd_Hold_Low_Seq_H);
        pnd_Hold_File_Pnd_Hold_Filler = pnd_Hold_File__R_Field_19.newFieldInGroup("pnd_Hold_File_Pnd_Hold_Filler", "#HOLD-FILLER", FieldType.STRING, 7);
        pnd_Hold_File_Pnd_Hold_Low_Ck_H = pnd_Hold_File__R_Field_19.newFieldInGroup("pnd_Hold_File_Pnd_Hold_Low_Ck_H", "#HOLD-LOW-CK-H", FieldType.STRING, 
            1);
        pnd_Hold_File_Pnd_Hold_Contract_No_H = pnd_Hold_File__R_Field_18.newFieldInGroup("pnd_Hold_File_Pnd_Hold_Contract_No_H", "#HOLD-CONTRACT-NO-H", 
            FieldType.STRING, 8);
        pnd_Ws_Addr_Lne8_Info = localVariables.newFieldInRecord("pnd_Ws_Addr_Lne8_Info", "#WS-ADDR-LNE8-INFO", FieldType.STRING, 35);

        pnd_Ws_Addr_Lne8_Info__R_Field_20 = localVariables.newGroupInRecord("pnd_Ws_Addr_Lne8_Info__R_Field_20", "REDEFINE", pnd_Ws_Addr_Lne8_Info);
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Ph_Bank_Pymnt_Acct_Nmbr = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Ph_Bank_Pymnt_Acct_Nmbr", 
            "#WS-PH-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21);
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Bank_Aba_Acct_Nmbr = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Bank_Aba_Acct_Nmbr", 
            "#WS-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9);
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Checking_Saving_Cde = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Checking_Saving_Cde", 
            "#WS-CHECKING-SAVING-CDE", FieldType.STRING, 1);
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Status_Ind = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Status_Ind", 
            "#WS-EFT-STATUS-IND", FieldType.STRING, 1);
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Pre_Note_Status_Ind = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Pre_Note_Status_Ind", 
            "#WS-EFT-PRE-NOTE-STATUS-IND", FieldType.STRING, 1);
        pnd_Ws_Addr_Lne8_Info_Ws_Fill = pnd_Ws_Addr_Lne8_Info__R_Field_20.newFieldInGroup("pnd_Ws_Addr_Lne8_Info_Ws_Fill", "WS-FILL", FieldType.STRING, 
            2);
        pnd_Check_Pmt_Date_Link = localVariables.newFieldInRecord("pnd_Check_Pmt_Date_Link", "#CHECK-PMT-DATE-LINK", FieldType.NUMERIC, 6);

        pnd_Check_Pmt_Date_Link__R_Field_21 = localVariables.newGroupInRecord("pnd_Check_Pmt_Date_Link__R_Field_21", "REDEFINE", pnd_Check_Pmt_Date_Link);
        pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Cc = pnd_Check_Pmt_Date_Link__R_Field_21.newFieldInGroup("pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Cc", "#CHECK-PMT-CC", 
            FieldType.NUMERIC, 2);
        pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Yy = pnd_Check_Pmt_Date_Link__R_Field_21.newFieldInGroup("pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Yy", "#CHECK-PMT-YY", 
            FieldType.NUMERIC, 2);
        pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Mm = pnd_Check_Pmt_Date_Link__R_Field_21.newFieldInGroup("pnd_Check_Pmt_Date_Link_Pnd_Check_Pmt_Mm", "#CHECK-PMT-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ppcn = localVariables.newFieldInRecord("pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        pnd_Dup = localVariables.newFieldInRecord("pnd_Dup", "#DUP", FieldType.NUMERIC, 9);
        pnd_Key_Saved = localVariables.newFieldInRecord("pnd_Key_Saved", "#KEY-SAVED", FieldType.STRING, 10);
        pnd_Ms_End = localVariables.newFieldInRecord("pnd_Ms_End", "#MS-END", FieldType.STRING, 5);
        pnd_Master_Found = localVariables.newFieldInRecord("pnd_Master_Found", "#MASTER-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naad_Etl_Record = localVariables.newFieldInRecord("pnd_Naad_Etl_Record", "#NAAD-ETL-RECORD", FieldType.BOOLEAN, 1);
        pnd_Naad_Etl_Payee_01 = localVariables.newFieldInRecord("pnd_Naad_Etl_Payee_01", "#NAAD-ETL-PAYEE-01", FieldType.BOOLEAN, 1);
        pnd_Etl_Eof = localVariables.newFieldInRecord("pnd_Etl_Eof", "#ETL-EOF", FieldType.BOOLEAN, 1);
        pnd_Input_Rec = localVariables.newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.NUMERIC, 9);
        pnd_Total_No_Ms = localVariables.newFieldInRecord("pnd_Total_No_Ms", "#TOTAL-NO-MS", FieldType.NUMERIC, 9);
        pnd_Total_No_Na = localVariables.newFieldInRecord("pnd_Total_No_Na", "#TOTAL-NO-NA", FieldType.NUMERIC, 9);
        pnd_Total_Opt = localVariables.newFieldInRecord("pnd_Total_Opt", "#TOTAL-OPT", FieldType.NUMERIC, 9);
        pnd_Total_J = localVariables.newFieldInRecord("pnd_Total_J", "#TOTAL-J", FieldType.NUMERIC, 9);
        pnd_Total_Can_Cref = localVariables.newFieldInRecord("pnd_Total_Can_Cref", "#TOTAL-CAN-CREF", FieldType.NUMERIC, 9);
        pnd_Total_Group = localVariables.newFieldInRecord("pnd_Total_Group", "#TOTAL-GROUP", FieldType.NUMERIC, 9);
        pnd_Output_Rec_Cnt = localVariables.newFieldInRecord("pnd_Output_Rec_Cnt", "#OUTPUT-REC-CNT", FieldType.NUMERIC, 9);
        pnd_Dont_Process = localVariables.newFieldInRecord("pnd_Dont_Process", "#DONT-PROCESS", FieldType.NUMERIC, 1);
        pnd_Etl_Found = localVariables.newFieldInRecord("pnd_Etl_Found", "#ETL-FOUND", FieldType.BOOLEAN, 1);
        pnd_Etl_Ppcn = localVariables.newFieldInRecord("pnd_Etl_Ppcn", "#ETL-PPCN", FieldType.STRING, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap6080() throws Exception
    {
        super("Iaap6080");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Iaap6080|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Check_Pmt_Date_Link);                                                                              //Natural: INPUT #CHECK-PMT-DATE-LINK
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 #INP-LOW-CONT-WORK
                while (condition(getWorkFiles().read(1, pnd_Inp_Low_Cont_Work)))
                {
                    pnd_Input_Rec.nadd(1);                                                                                                                                //Natural: ADD 1 TO #INPUT-REC
                    if (condition(pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc.equals(pnd_Key_Saved)))                                                                            //Natural: IF #INP-KEY-LC = #KEY-SAVED
                    {
                        pnd_Dup.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Key_Saved.setValue(pnd_Inp_Low_Cont_Work_Pnd_Inp_Key_Lc);                                                                                         //Natural: MOVE #INP-KEY-LC TO #KEY-SAVED
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls.setValue(pnd_Inp_Low_Cont_Work_Pnd_Inp_Low_Cont_No_Lc);                                                     //Natural: MOVE #INP-LOW-CONT-NO-LC TO #OUT-LOW-SEQ-NO-LS
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls.setValue(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc);                                                    //Natural: MOVE #INP-CONTRACT-NO-LC TO #OUT-CONTRACT-NO-LS
                    pnd_Etl_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #ETL-FOUND := TRUE
                    if (condition(! (pnd_Etl_Eof.getBoolean())))                                                                                                          //Natural: IF NOT #ETL-EOF
                    {
                        //*   PNM1140 - STARTING FROM CONTRACT
                                                                                                                                                                          //Natural: PERFORM GET-NA-ETL-FILE
                        sub_Get_Na_Etl_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-MASTER-FILE
                    sub_Read_Master_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(! (pnd_Master_Found.getBoolean())))                                                                                                     //Natural: IF NOT #MASTER-FOUND
                    {
                        pnd_Total_No_Ms.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-NO-MS
                        pnd_Dont_Process.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #DONT-PROCESS
                        pnd_Hold_File_Pnd_Hold_Contract_No_H.setValue(pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls);                                                        //Natural: MOVE #OUT-CONTRACT-NO-LS TO #HOLD-CONTRACT-NO-H
                        pnd_Hold_File_Pnd_Hold_Low_Seq_H.setValue(pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls);                                                             //Natural: MOVE #OUT-LOW-SEQ-NO-LS TO #HOLD-LOW-SEQ-H
                        getWorkFiles().write(5, false, pnd_Hold_File);                                                                                                    //Natural: WRITE WORK FILE 5 #HOLD-FILE
                        //*     COMPRESS #INP-CONTRACT-LC-8 '-' #INP-CHECK-DG-LC INTO #PPCN LEAVING
                        //*      COMPRESS #INP-LOW-CONT-NO-LC           /* OR #OUT-LOW-SEQ-NO-LS
                        //*               #INP-CONTRACT-LC-8
                        //*   HOLD - SYS013
                        getReports().display(1, pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls,pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls);                                    //Natural: DISPLAY ( 01 ) #OUT-CONTRACT-NO-LS #OUT-LOW-SEQ-NO-LS
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* **           #PPCN
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  NOT IN ETL
                    if (condition(! (pnd_Etl_Found.getBoolean())))                                                                                                        //Natural: IF NOT #ETL-FOUND
                    {
                        pnd_Total_No_Na.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-NO-NA
                        pnd_Etl_Ppcn.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc, "-", pnd_Inp_Low_Cont_Work_Pnd_Inp_Check_Dg_Lc)); //Natural: COMPRESS #INP-CONTRACT-LC '-' #INP-CHECK-DG-LC INTO #ETL-PPCN LEAVING NO
                        getReports().display(2, pnd_Etl_Ppcn);                                                                                                            //Natural: DISPLAY ( 02 ) #ETL-PPCN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  AND NOT #ETL-01-FOUND
                    if (condition(! (pnd_Etl_Found.getBoolean())))                                                                                                        //Natural: IF NOT #ETL-FOUND
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("21") || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("22") || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("23")  //Natural: IF #OUT-OPTION-LS = '21' OR = '22' OR = '23' OR = '25' OR = '26' OR = '27' OR = '28' OR = '30'
                        || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("25") || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("26") || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("27") 
                        || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("28") || pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.equals("30")))
                    {
                        pnd_Total_Opt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-OPT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls.equals("J")))                                                                                   //Natural: IF #OUT-J-C-CODE-LS = 'J'
                    {
                        pnd_Total_J.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-J
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls.equals("2")))                                                                                   //Natural: IF #OUT-CURRENCY-LS = '2'
                    {
                        pnd_Total_Can_Cref.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-CAN-CREF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(((pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls.equals("DP") || pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls.equals("NP")) || (pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Pre_Ls.equals("W0")  //Natural: IF ( #OUT-CON-PRE-LS = 'DP' OR = 'NP' ) OR ( #OUT-CON-PRE-LS = 'W0' AND ( #OUT-CON-NUM-LS > '249999' AND #OUT-CON-NUM-LS < '900000' ) )
                        && (pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls.greater("249999") && pnd_Out_Low_Seq_Rec_Pnd_Out_Con_Num_Ls.less("900000"))))))
                    {
                        pnd_Total_Group.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-GROUP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(4, false, pnd_Out_Low_Seq_Rec);                                                                                                  //Natural: WRITE WORK FILE 4 #OUT-LOW-SEQ-REC
                    pnd_Output_Rec_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #OUTPUT-REC-CNT
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"INPUT RECORD COUNT          ",pnd_Input_Rec,NEWLINE,"OUTPUT LOWSEQ COUNT         ",pnd_Output_Rec_Cnt,        //Natural: WRITE ( 1 ) 'INPUT RECORD COUNT          ' #INPUT-REC / 'OUTPUT LOWSEQ COUNT         ' #OUTPUT-REC-CNT / 'DISCARDED - NO IA MASTER    ' #TOTAL-NO-MS / 'DISCARDED - DUPLICATE INPUT ' #DUP / 'DISCARDED - INVALID OPTION  ' #TOTAL-OPT / 'DISCARDED - JOINT CONTRACT  ' #TOTAL-J / 'DISCARDED - CANADIAN CREF   ' #TOTAL-CAN-CREF / 'DISCARDED - GROUP CONTRACTS ' #TOTAL-GROUP
                    NEWLINE,"DISCARDED - NO IA MASTER    ",pnd_Total_No_Ms,NEWLINE,"DISCARDED - DUPLICATE INPUT ",pnd_Dup,NEWLINE,"DISCARDED - INVALID OPTION  ",
                    pnd_Total_Opt,NEWLINE,"DISCARDED - JOINT CONTRACT  ",pnd_Total_J,NEWLINE,"DISCARDED - CANADIAN CREF   ",pnd_Total_Can_Cref,NEWLINE,"DISCARDED - GROUP CONTRACTS ",
                    pnd_Total_Group);
                if (Global.isEscape()) return;
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 )
                //*  --------------------------------------------------------------------                                                                                 //Natural: AT TOP OF PAGE ( 02 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NA-ETL-FILE
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDR-01
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MASTER-FILE
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MASTER
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Na_Etl_File() throws Exception                                                                                                                   //Natural: GET-NA-ETL-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naad_Etl_Record.setValue(false);                                                                                                                              //Natural: ASSIGN #NAAD-ETL-RECORD := FALSE
        pnd_Naad_Etl_Payee_01.setValue(true);                                                                                                                             //Natural: ASSIGN #NAAD-ETL-PAYEE-01 := TRUE
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc.less(pnd_Name_Check_Out_Pnd_Cntrct_Nmbr)))                                                         //Natural: IF #INP-CONTRACT-NO-LC < #CNTRCT-NMBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Name_Check_Out);                                                                                                                   //Natural: READ WORK FILE 2 ONCE #NAME-CHECK-OUT
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Etl_Eof.setValue(true);                                                                                                                               //Natural: ASSIGN #ETL-EOF := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc.greater(pnd_Name_Check_Out_Pnd_Cntrct_Nmbr)))                                                      //Natural: IF #INP-CONTRACT-NO-LC > #CNTRCT-NMBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc_8.equals(pnd_Name_Check_Out_Pnd_Cntrct_Nmbr)))                                                        //Natural: IF #INP-CONTRACT-LC-8 EQ #CNTRCT-NMBR
            {
                pnd_Naad_Etl_Record.setValue(true);                                                                                                                       //Natural: ASSIGN #NAAD-ETL-RECORD := TRUE
                if (condition(pnd_Name_Check_Out_Pnd_Cntrct_Payee_Cde.equals(1)))                                                                                         //Natural: IF #CNTRCT-PAYEE-CDE = 01
                {
                    pnd_Naad_Etl_Payee_01.setValue(true);                                                                                                                 //Natural: ASSIGN #NAAD-ETL-PAYEE-01 := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-NAME-ADDR-01
                    sub_Get_Name_Addr_01();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_Lc_8.less(pnd_Name_Check_Out_Pnd_Cntrct_Nmbr)))                                                          //Natural: IF #INP-CONTRACT-LC-8 < #CNTRCT-NMBR
            {
                pnd_Etl_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #ETL-FOUND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Name_Addr_01() throws Exception                                                                                                                  //Natural: GET-NAME-ADDR-01
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line1.setValue(pnd_Name_Check_Out_Pnd_Cntrct_Name_Free);                                                                         //Natural: MOVE #CNTRCT-NAME-FREE TO #OUT-ADDR-LINE1
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line2.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_1);                                                                             //Natural: MOVE #ADDRSS-LNE-1 TO #OUT-ADDR-LINE2
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line3.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_2);                                                                             //Natural: MOVE #ADDRSS-LNE-2 TO #OUT-ADDR-LINE3
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line4.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_3);                                                                             //Natural: MOVE #ADDRSS-LNE-3 TO #OUT-ADDR-LINE4
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line5.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_4);                                                                             //Natural: MOVE #ADDRSS-LNE-4 TO #OUT-ADDR-LINE5
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line6.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_5);                                                                             //Natural: MOVE #ADDRSS-LNE-5 TO #OUT-ADDR-LINE6
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line7.setValue(pnd_Name_Check_Out_Pnd_Addrss_Lne_6);                                                                             //Natural: MOVE #ADDRSS-LNE-6 TO #OUT-ADDR-LINE7
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Bank_Aba_Acct_Nmbr.setValue(pnd_Name_Check_Out_Pnd_Bank_Aba_Acct_Nmbr);                                                              //Natural: MOVE #BANK-ABA-ACCT-NMBR TO #WS-BANK-ABA-ACCT-NMBR
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Status_Ind.setValue(pnd_Name_Check_Out_Pnd_Eft_Status_Ind);                                                                      //Natural: MOVE #EFT-STATUS-IND TO #WS-EFT-STATUS-IND
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Checking_Saving_Cde.setValue(pnd_Name_Check_Out_Pnd_Checking_Saving_Cde);                                                            //Natural: MOVE #CHECKING-SAVING-CDE TO #WS-CHECKING-SAVING-CDE
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Ph_Bank_Pymnt_Acct_Nmbr.setValue(pnd_Name_Check_Out_Pnd_Ph_Bank_Pymnt_Acct_Nmbr);                                                    //Natural: MOVE #PH-BANK-PYMNT-ACCT-NMBR TO #WS-PH-BANK-PYMNT-ACCT-NMBR
        pnd_Ws_Addr_Lne8_Info_Pnd_Ws_Eft_Pre_Note_Status_Ind.setValue(pnd_Name_Check_Out_Eft_Pre_Note_Status_Ind);                                                        //Natural: MOVE EFT-PRE-NOTE-STATUS-IND TO #WS-EFT-PRE-NOTE-STATUS-IND
        pnd_Out_Low_Seq_Rec_Pnd_Out_Addr_Line8.setValue(pnd_Ws_Addr_Lne8_Info);                                                                                           //Natural: MOVE #WS-ADDR-LNE8-INFO TO #OUT-ADDR-LINE8
    }
    private void sub_Read_Master_File() throws Exception                                                                                                                  //Natural: READ-MASTER-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Master_Found.setValue(false);                                                                                                                                 //Natural: ASSIGN #MASTER-FOUND := FALSE
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ms_End.equals("END") || pnd_Master_Work_Rec_Pnd_Contract_No_M.greater(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc)))                       //Natural: IF #MS-END = 'END' OR #CONTRACT-NO-M > #INP-CONTRACT-NO-LC
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Master_Work_Rec_Pnd_Contract_No_M.less(pnd_Inp_Low_Cont_Work_Pnd_Inp_Contract_No_Lc)))                                                      //Natural: IF #CONTRACT-NO-M < #INP-CONTRACT-NO-LC
            {
                                                                                                                                                                          //Natural: PERFORM READ-MASTER
                sub_Read_Master();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Master_Found.setValue(true);                                                                                                                              //Natural: ASSIGN #MASTER-FOUND := TRUE
            short decideConditionsMet399 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE-M;//Natural: VALUE '10'
            if (condition((pnd_Master_Work_Rec_Pnd_Rec_Type_M.equals("10"))))
            {
                decideConditionsMet399++;
                if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls.equals(pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls)))                                              //Natural: IF #OUT-LOW-SEQ-NO-LS = #OUT-CONTRACT-NO-LS
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls.setValue(pnd_Master_Work_Rec_Pnd_Payee_M);                                                                     //Natural: MOVE #PAYEE-M TO #OUT-PAYEE-1-LS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_1_Ls.setValue("00");                                                                                                //Natural: MOVE '00' TO #OUT-PAYEE-1-LS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Out_Low_Seq_Rec_Pnd_Out_Payee_2_Ls.setValue(pnd_Master_Work_Rec_Pnd_Payee_M);                                                                         //Natural: MOVE #PAYEE-M TO #OUT-PAYEE-2-LS
                pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls.setValue(pnd_Master_Work_Rec_Pnd_Currency_M);                                                                     //Natural: MOVE #CURRENCY-M TO #OUT-CURRENCY-LS
                pnd_Out_Low_Seq_Rec_Pnd_Out_Mode_Ls.setValue(pnd_Master_Work_Rec_Pnd_Mode_M);                                                                             //Natural: MOVE #MODE-M TO #OUT-MODE-LS
                pnd_Out_Low_Seq_Rec_Pnd_Out_Option_Ls.setValue(pnd_Master_Work_Rec_Pnd_Option_M);                                                                         //Natural: MOVE #OPTION-M TO #OUT-OPTION-LS
                pnd_Out_Low_Seq_Rec_Pnd_Out_J_C_Code_Ls.setValue(pnd_Master_Work_Rec_Pnd_J_C_Code_M);                                                                     //Natural: MOVE #J-C-CODE-M TO #OUT-J-C-CODE-LS
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((pnd_Master_Work_Rec_Pnd_Rec_Type_M.equals("20"))))
            {
                decideConditionsMet399++;
                if (condition(pnd_Master_Work_Rec_Pnd_Dod_1_M.equals(getZero())))                                                                                         //Natural: IF #DOD-1-M = 0
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls.setValue(pnd_Master_Work_Rec_Pnd_X_Ref_1_M);                                                                     //Natural: MOVE #X-REF-1-M TO #OUT-X-REF-LS
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls.setValue(pnd_Master_Work_Rec_Pnd_Sex_1_M);                                                                         //Natural: MOVE #SEX-1-M TO #OUT-SEX-LS
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls.setValue(pnd_Master_Work_Rec_Pnd_Dob_1_M);                                                                         //Natural: MOVE #DOB-1-M TO #OUT-DOB-LS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_X_Ref_Ls.setValue(pnd_Master_Work_Rec_Pnd_X_Ref_2_M);                                                                     //Natural: MOVE #X-REF-2-M TO #OUT-X-REF-LS
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Sex_Ls.setValue(pnd_Master_Work_Rec_Pnd_Sex_2_M);                                                                         //Natural: MOVE #SEX-2-M TO #OUT-SEX-LS
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Dob_Ls.setValue(pnd_Master_Work_Rec_Pnd_Dob_2_M);                                                                         //Natural: MOVE #DOB-2-M TO #OUT-DOB-LS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '70'
            else if (condition((pnd_Master_Work_Rec_Pnd_Rec_Type_M.equals("70"))))
            {
                decideConditionsMet399++;
                if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls.equals("1")))                                                                                       //Natural: IF #OUT-CURRENCY-LS = '1'
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls.setValue(pnd_Master_Work_Rec_Pnd_Soc_Sec_No_M);                                                             //Natural: MOVE #SOC-SEC-NO-M TO #OUT-SOC-SEC-NO-LS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '80'
            else if (condition((pnd_Master_Work_Rec_Pnd_Rec_Type_M.equals("80"))))
            {
                decideConditionsMet399++;
                if (condition(pnd_Out_Low_Seq_Rec_Pnd_Out_Currency_Ls.equals("2")))                                                                                       //Natural: IF #OUT-CURRENCY-LS = '2'
                {
                    pnd_Out_Low_Seq_Rec_Pnd_Out_Soc_Sec_No_Ls.setValue(pnd_Master_Work_Rec_Pnd_Soc_Ins_No_M);                                                             //Natural: MOVE #SOC-INS-NO-M TO #OUT-SOC-SEC-NO-LS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  GET OTHER REC TYPES
                                                                                                                                                                          //Natural: PERFORM READ-MASTER
            sub_Read_Master();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Read_Master() throws Exception                                                                                                                       //Natural: READ-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        getWorkFiles().read(3, pnd_Master_Work_Rec);                                                                                                                      //Natural: READ WORK FILE 3 ONCE #MASTER-WORK-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Ms_End.setValue("END");                                                                                                                                   //Natural: MOVE 'END' TO #MS-END
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"JOB NAME:",Global.getINIT_USER(),NEWLINE,"PROGRAM: IAAP6080","     CONTRACT NUMBERS NOT FOUND ON THE IA MASTER FILE     ",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'JOB NAME:' *INIT-USER / 'PROGRAM: IAAP6080' '     CONTRACT NUMBERS NOT FOUND ON THE IA MASTER FILE     ' 10X 'PAGE:   ' *PAGE-NUMBER ( 01 ) / 'DATE: '*DATU //
                        ColumnSpacing(10),"PAGE:   ",getReports().getPageNumberDbs(1),NEWLINE,"DATE: ",Global.getDATU(),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"JOB NAME:",Global.getINIT_USER(),NEWLINE,"PROGRAM: IAAP6080","     CONTRACT NUMBERS NOT FOUND ON THE NAME AND ADDRESS FILE  ",new  //Natural: WRITE ( 02 ) NOTITLE NOHDR / 'JOB NAME:' *INIT-USER / 'PROGRAM: IAAP6080' '     CONTRACT NUMBERS NOT FOUND ON THE NAME AND ADDRESS FILE  ' 10X 'PAGE:   ' *PAGE-NUMBER ( 02 ) / 'DATE: '*DATU //
                        ColumnSpacing(10),"PAGE:   ",getReports().getPageNumberDbs(2),NEWLINE,"DATE: ",Global.getDATU(),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(1, pnd_Out_Low_Seq_Rec_Pnd_Out_Contract_No_Ls,pnd_Out_Low_Seq_Rec_Pnd_Out_Low_Seq_No_Ls);
        getReports().setDisplayColumns(2, pnd_Etl_Ppcn);
    }
}
