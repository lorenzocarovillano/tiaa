/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:42 PM
**        * FROM NATURAL PROGRAM : Iaap200
************************************************************
**        * FILE NAME            : Iaap200.java
**        * CLASS NAME           : Iaap200
**        * INSTANCE NAME        : Iaap200
************************************************************
************************************************************************
*
* PROGRAM: IAAP200
* DESC   : THIS PROGRAM WILL CREATE A LIST OF SELF-REMITTER CONTRACTS
*          ISSUED FOR A GIVEN MONTH. THIS WILL BE RUN MONTHLY.
*
*
* HISTORY:
* --------
* 08/11/15 : JT COR/NAAD SUNSET. SCAN ON 08/15 FOR CHANGES
* 04/2017    OS PIN EXPANSION CHANGES MARKED 082017.
*
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap200 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;

    private DbsGroup iaa_Cntrct__R_Field_1;
    private DbsField iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Yy;
    private DbsField iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Mm;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField pnd_Name_Free;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_2;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler;
    private DbsField iaa_Tiaa_Fund_Rcrd_Company;
    private DbsField iaa_Tiaa_Fund_Rcrd_Filler1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_3;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_Cntrct_Fund;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_Paye_Fund;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_Fund;
    private DbsField pnd_Total_Records_Read;
    private DbsField pnd_Total_Sr_Records;
    private DbsField pnd_State;
    private DbsField pnd_A;
    private DbsField pnd_Mode_Name;
    private DbsField pnd_Mode_Ctr;
    private DbsField pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Date;
    private DbsField pnd_Tot_Per_Amt;
    private DbsField pnd_State_Code;
    private DbsField pnd_Last_Updte_Dte;
    private DbsField pnd_Next_Updte_Dte;
    private DbsField pnd_Todays_Date;

    private DbsGroup pnd_Todays_Date__R_Field_4;
    private DbsField pnd_Todays_Date_Pnd_Today_Mm;
    private DbsField pnd_Todays_Date_Pnd_Filler_1;
    private DbsField pnd_Todays_Date_Pnd_Today_Dd;
    private DbsField pnd_Todays_Date_Pnd_Filler_2;
    private DbsField pnd_Todays_Date_Pnd_Today_Yyyy;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");

        iaa_Cntrct__R_Field_1 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct__R_Field_1", "REDEFINE", iaa_Cntrct_Cntrct_Issue_Dte);
        iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Yy = iaa_Cntrct__R_Field_1.newFieldInGroup("iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Yy", "#CNTRCT-ISSUE-DTE-YY", FieldType.NUMERIC, 
            4);
        iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Mm = iaa_Cntrct__R_Field_1.newFieldInGroup("iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Mm", "#CNTRCT-ISSUE-DTE-MM", FieldType.NUMERIC, 
            2);
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Name_Free = localVariables.newFieldInRecord("pnd_Name_Free", "#NAME-FREE", FieldType.STRING, 35);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_2 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_2", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Filler = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler", "FILLER", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Company = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Company", "COMPANY", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Filler1 = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Filler1", "FILLER1", FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_Cntrct_Fund = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_Cntrct_Fund", "#CNTRCT-FUND", FieldType.STRING, 
            10);
        pnd_Cntrct_Fund_Key_Pnd_Paye_Fund = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_Paye_Fund", "#PAYE-FUND", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Fund_Key_Pnd_Fund = pnd_Cntrct_Fund_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_Fund", "#FUND", FieldType.STRING, 3);
        pnd_Total_Records_Read = localVariables.newFieldInRecord("pnd_Total_Records_Read", "#TOTAL-RECORDS-READ", FieldType.NUMERIC, 9);
        pnd_Total_Sr_Records = localVariables.newFieldInRecord("pnd_Total_Sr_Records", "#TOTAL-SR-RECORDS", FieldType.NUMERIC, 9);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_Mode_Name = localVariables.newFieldArrayInRecord("pnd_Mode_Name", "#MODE-NAME", FieldType.STRING, 3, new DbsArrayController(1, 22));
        pnd_Mode_Ctr = localVariables.newFieldArrayInRecord("pnd_Mode_Ctr", "#MODE-CTR", FieldType.NUMERIC, 4, new DbsArrayController(1, 22));
        pnd_Trans_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", FieldType.STRING, 11);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 7);
        pnd_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.NUMERIC, 9, 2);
        pnd_State_Code = localVariables.newFieldInRecord("pnd_State_Code", "#STATE-CODE", FieldType.STRING, 3);
        pnd_Last_Updte_Dte = localVariables.newFieldInRecord("pnd_Last_Updte_Dte", "#LAST-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Next_Updte_Dte = localVariables.newFieldInRecord("pnd_Next_Updte_Dte", "#NEXT-UPDTE-DTE", FieldType.STRING, 10);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.STRING, 10);

        pnd_Todays_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Todays_Date__R_Field_4", "REDEFINE", pnd_Todays_Date);
        pnd_Todays_Date_Pnd_Today_Mm = pnd_Todays_Date__R_Field_4.newFieldInGroup("pnd_Todays_Date_Pnd_Today_Mm", "#TODAY-MM", FieldType.NUMERIC, 2);
        pnd_Todays_Date_Pnd_Filler_1 = pnd_Todays_Date__R_Field_4.newFieldInGroup("pnd_Todays_Date_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Todays_Date_Pnd_Today_Dd = pnd_Todays_Date__R_Field_4.newFieldInGroup("pnd_Todays_Date_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);
        pnd_Todays_Date_Pnd_Filler_2 = pnd_Todays_Date__R_Field_4.newFieldInGroup("pnd_Todays_Date_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Todays_Date_Pnd_Today_Yyyy = pnd_Todays_Date__R_Field_4.newFieldInGroup("pnd_Todays_Date_Pnd_Today_Yyyy", "#TODAY-YYYY", FieldType.NUMERIC, 
            4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap200() throws Exception
    {
        super("Iaap200");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP200", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133;//Natural: FORMAT ( 1 ) PS = 60 LS = 133
        //*  08/15 - MQ OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Iaan0020.class , getCurrentProcessState(), pnd_Last_Updte_Dte, pnd_Next_Updte_Dte);                                                               //Natural: CALLNAT 'IAAN0020' #LAST-UPDTE-DTE #NEXT-UPDTE-DTE
        if (condition(Global.isEscape())) return;
        pnd_Todays_Date.setValue(pnd_Last_Updte_Dte);                                                                                                                     //Natural: MOVE #LAST-UPDTE-DTE TO #TODAYS-DATE
        pnd_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Todays_Date_Pnd_Today_Mm, "/", pnd_Todays_Date_Pnd_Today_Yyyy));                            //Natural: COMPRESS #TODAY-MM '/' #TODAY-YYYY INTO #DATE LEAVING NO SPACE
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        pnd_Mode_Name.getValue(1).setValue("100");                                                                                                                        //Natural: MOVE '100' TO #MODE-NAME ( 1 )
        pnd_Mode_Name.getValue(2).setValue("601");                                                                                                                        //Natural: MOVE '601' TO #MODE-NAME ( 2 )
        pnd_Mode_Name.getValue(3).setValue("602");                                                                                                                        //Natural: MOVE '602' TO #MODE-NAME ( 3 )
        pnd_Mode_Name.getValue(4).setValue("603");                                                                                                                        //Natural: MOVE '603' TO #MODE-NAME ( 4 )
        pnd_Mode_Name.getValue(5).setValue("701");                                                                                                                        //Natural: MOVE '701' TO #MODE-NAME ( 5 )
        pnd_Mode_Name.getValue(6).setValue("702");                                                                                                                        //Natural: MOVE '702' TO #MODE-NAME ( 6 )
        pnd_Mode_Name.getValue(7).setValue("703");                                                                                                                        //Natural: MOVE '703' TO #MODE-NAME ( 7 )
        pnd_Mode_Name.getValue(8).setValue("704");                                                                                                                        //Natural: MOVE '704' TO #MODE-NAME ( 8 )
        pnd_Mode_Name.getValue(9).setValue("705");                                                                                                                        //Natural: MOVE '705' TO #MODE-NAME ( 9 )
        pnd_Mode_Name.getValue(10).setValue("706");                                                                                                                       //Natural: MOVE '706' TO #MODE-NAME ( 10 )
        pnd_Mode_Name.getValue(11).setValue("801");                                                                                                                       //Natural: MOVE '801' TO #MODE-NAME ( 11 )
        pnd_Mode_Name.getValue(12).setValue("802");                                                                                                                       //Natural: MOVE '802' TO #MODE-NAME ( 12 )
        pnd_Mode_Name.getValue(13).setValue("803");                                                                                                                       //Natural: MOVE '803' TO #MODE-NAME ( 13 )
        pnd_Mode_Name.getValue(14).setValue("804");                                                                                                                       //Natural: MOVE '804' TO #MODE-NAME ( 14 )
        pnd_Mode_Name.getValue(15).setValue("805");                                                                                                                       //Natural: MOVE '805' TO #MODE-NAME ( 15 )
        pnd_Mode_Name.getValue(16).setValue("806");                                                                                                                       //Natural: MOVE '806' TO #MODE-NAME ( 16 )
        pnd_Mode_Name.getValue(17).setValue("807");                                                                                                                       //Natural: MOVE '807' TO #MODE-NAME ( 17 )
        pnd_Mode_Name.getValue(18).setValue("808");                                                                                                                       //Natural: MOVE '808' TO #MODE-NAME ( 18 )
        pnd_Mode_Name.getValue(19).setValue("809");                                                                                                                       //Natural: MOVE '809' TO #MODE-NAME ( 19 )
        pnd_Mode_Name.getValue(20).setValue("810");                                                                                                                       //Natural: MOVE '810' TO #MODE-NAME ( 20 )
        pnd_Mode_Name.getValue(21).setValue("811");                                                                                                                       //Natural: MOVE '811' TO #MODE-NAME ( 21 )
        pnd_Mode_Name.getValue(22).setValue("812");                                                                                                                       //Natural: MOVE '812' TO #MODE-NAME ( 22 )
        //* ***********************************************************************
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR
        (
        "READ01",
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Cntrct.readNextRow("READ01")))
        {
            pnd_Total_Records_Read.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-RECORDS-READ
            //*  SELF REMITTER
            if (condition(!(iaa_Cntrct_Cntrct_Orgn_Cde.equals(42) && pnd_Todays_Date_Pnd_Today_Mm.equals(iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Mm) && iaa_Cntrct_Pnd_Cntrct_Issue_Dte_Yy.equals(pnd_Todays_Date_Pnd_Today_Yyyy)))) //Natural: ACCEPT IF IAA-CNTRCT.CNTRCT-ORGN-CDE = 42 AND #TODAY-MM = #CNTRCT-ISSUE-DTE-MM AND #CNTRCT-ISSUE-DTE-YY = #TODAY-YYYY
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CPR-RECORD
            sub_Read_Cpr_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  08/15 - MQ CLOSE
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #A = 1 TO 22
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(22)); pnd_A.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,"TOTAL RECORDS FOR MODE     ",pnd_Mode_Name.getValue(pnd_A)," ",pnd_Mode_Ctr.getValue(pnd_A), new                  //Natural: WRITE ( 1 ) 'TOTAL RECORDS FOR MODE     ' #MODE-NAME ( #A ) ' ' #MODE-CTR ( #A ) ( EM = ZZZ,ZZZ,ZZ9 )
                ReportEditMask ("Z,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL-RECORDS-READ         ",pnd_Total_Records_Read, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 'TOTAL-RECORDS-READ         ' #TOTAL-RECORDS-READ ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL NUMBER SELF REMITTER ",pnd_Total_Sr_Records, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                               //Natural: WRITE ( 1 ) 'TOTAL NUMBER SELF REMITTER ' #TOTAL-SR-RECORDS ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR-RECORD
        //*  MOVE 'O'  TO  #CORRESPONDENCE-ADDRSS-IND
        //*  FIND NA-VIEW WITH  CNTRCT-TYPE-CORR-CNTRCT-KEY
        //*      =  #CONTRACT-KEY
        //*    IF NO RECORD
        //*      MOVE 'NOT/AVAILABLE' TO NA-VIEW.CNTRCT-NAME-FREE
        //*    END-NOREC
        //*  END-FIND
        //* *#I-PIN := CPR-ID-NBR
        //*  08/15 - START
        //*    'NAME'              NA-VIEW.CNTRCT-NAME-FREE
        //*  08/15 - END
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Read_Cpr_Record() throws Exception                                                                                                                   //Natural: READ-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM CNTRCT-PPCN-NBR
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", iaa_Cntrct_Cntrct_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ02")))
        {
            if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cntrct_Cntrct_Ppcn_Nbr)) || (iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9)))) //Natural: IF ( CNTRCT-PART-PPCN-NBR NE CNTRCT-PPCN-NBR ) OR ( IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  08/15 - START
            //*  MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR  TO #CNTRCT-NMBR
            pnd_Cntrct_Fund_Key_Pnd_Cntrct_Fund.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                   //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR TO #CNTRCT-FUND
            //*  MOVE 'I'                                           TO #CNTRCT-TYPE-CDE
            //*  MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE TO #CNTRCT-PAY-CDE
            pnd_Cntrct_Fund_Key_Pnd_Paye_Fund.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                    //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE TO #PAYE-FUND
            pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr);                                                                       //Natural: ASSIGN #I-PIN-N12 := CPR-ID-NBR
            //* *CALLNAT 'MDMN100A' #MDMA100
            //*  082017 END
            DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                      //Natural: CALLNAT 'MDMN101A' #MDMA101
            if (condition(Global.isEscape())) return;
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                  //Natural: IF #O-RETURN-CODE = '0000'
            {
                pnd_Name_Free.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(),  //Natural: COMPRESS #O-PREFIX #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME #O-SUFFIX INTO #NAME-FREE
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Name_Free.setValue("NOT AVAILABLE");                                                                                                                  //Natural: ASSIGN #NAME-FREE := 'NOT AVAILABLE'
            }                                                                                                                                                             //Natural: END-IF
            //*  08/15 - END
            pnd_Tot_Per_Amt.reset();                                                                                                                                      //Natural: RESET #TOT-PER-AMT
            vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                       //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
            (
            "READ03",
            new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
            new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
            );
            READ03:
            while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ03")))
            {
                if (condition((iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.notEquals(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Fund)) || (iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.notEquals(pnd_Cntrct_Fund_Key_Pnd_Paye_Fund)))) //Natural: IF ( IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #CNTRCT-FUND ) OR ( IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #PAYE-FUND )
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Tot_Per_Amt.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                            //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #TOT-PER-AMT
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaNeca4000.getNeca4000_Function_Cde().setValue("STT");                                                                                                       //Natural: ASSIGN FUNCTION-CDE := 'STT'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                 //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
            pdaNeca4000.getNeca4000_Stt_Key_Table_Cde().setValue("STT");                                                                                                  //Natural: ASSIGN STT-KEY-TABLE-CDE := 'STT'
            pnd_State_Code.setValue(iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde);                                                                                               //Natural: ASSIGN #STATE-CODE := IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
            pdaNeca4000.getNeca4000_Stt_Key_State_Cde().setValue(pnd_State_Code);                                                                                         //Natural: ASSIGN STT-KEY-STATE-CDE := #STATE-CODE
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            pnd_State.setValue(pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1));                                                                                 //Natural: MOVE STT-ABBR-STATE-CDE ( 1 ) TO #STATE
            pnd_Trans_Ppcn_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr, "-", iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde)); //Natural: COMPRESS IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR '-' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE INTO #TRANS-PPCN-NBR LEAVING NO SPACE
            getReports().display(1, "PPCN/PAYEE",                                                                                                                         //Natural: DISPLAY ( 1 ) 'PPCN/PAYEE' #TRANS-PPCN-NBR 'ISSUE/STATE' IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE 'STATE/NAME' #STATE 'PIN' IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR 'ORGN/CODE' IAA-CNTRCT.CNTRCT-ORGN-CDE 'OPTN/CODE' IAA-CNTRCT.CNTRCT-OPTN-CDE 'ISSUE/DATE' IAA-CNTRCT.CNTRCT-ISSUE-DTE ( EM = 9999/99 ) 'ORIG./DA/CONTRACT' IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR 'MODE' IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND 'NAME' #NAME-FREE 'ORIG./PPG' IAA-CNTRCT.CNTRCT-INST-ISS-CDE 'PERIODIC/PAYMENT' #TOT-PER-AMT ( EM = Z,ZZZ,ZZZ.99 )
            		pnd_Trans_Ppcn_Nbr,"ISSUE/STATE",
            		iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde,"STATE/NAME",
            		pnd_State,"PIN",
            		iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr,"ORGN/CODE",
            		iaa_Cntrct_Cntrct_Orgn_Cde,"OPTN/CODE",
            		iaa_Cntrct_Cntrct_Optn_Cde,"ISSUE/DATE",
            		iaa_Cntrct_Cntrct_Issue_Dte, new ReportEditMask ("9999/99"),"ORIG./DA/CONTRACT",
            		iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr,"MODE",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind,"NAME",
            		pnd_Name_Free,"ORIG./PPG",
            		iaa_Cntrct_Cntrct_Inst_Iss_Cde,"PERIODIC/PAYMENT",
            		pnd_Tot_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Sr_Records.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-SR-RECORDS
            short decideConditionsMet1357 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND;//Natural: VALUE 100
            if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(100))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(1).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 1 )
            }                                                                                                                                                             //Natural: VALUE 601
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(601))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(2).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 2 )
            }                                                                                                                                                             //Natural: VALUE 602
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(602))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(3).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 3 )
            }                                                                                                                                                             //Natural: VALUE 603
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(603))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(4).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 4 )
            }                                                                                                                                                             //Natural: VALUE 701
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(701))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(5).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 5 )
            }                                                                                                                                                             //Natural: VALUE 702
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(702))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(6).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 6 )
            }                                                                                                                                                             //Natural: VALUE 703
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(703))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(7).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 7 )
            }                                                                                                                                                             //Natural: VALUE 704
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(704))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(8).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 8 )
            }                                                                                                                                                             //Natural: VALUE 705
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(705))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(9).nadd(1);                                                                                                                         //Natural: ADD 1 TO #MODE-CTR ( 9 )
            }                                                                                                                                                             //Natural: VALUE 706
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(706))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(10).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 10 )
            }                                                                                                                                                             //Natural: VALUE 801
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(801))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(11).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 11 )
            }                                                                                                                                                             //Natural: VALUE 802
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(802))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(12).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 12 )
            }                                                                                                                                                             //Natural: VALUE 803
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(803))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(13).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 13 )
            }                                                                                                                                                             //Natural: VALUE 804
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(804))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(14).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 14 )
            }                                                                                                                                                             //Natural: VALUE 805
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(805))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(15).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 15 )
            }                                                                                                                                                             //Natural: VALUE 806
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(806))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(16).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 16 )
            }                                                                                                                                                             //Natural: VALUE 807
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(807))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(17).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 17 )
            }                                                                                                                                                             //Natural: VALUE 808
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(808))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(18).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 18 )
            }                                                                                                                                                             //Natural: VALUE 809
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(809))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(19).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 19 )
            }                                                                                                                                                             //Natural: VALUE 810
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(810))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(20).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 20 )
            }                                                                                                                                                             //Natural: VALUE 811
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(811))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(21).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 21 )
            }                                                                                                                                                             //Natural: VALUE 812
            else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(812))))
            {
                decideConditionsMet1357++;
                pnd_Mode_Ctr.getValue(22).nadd(1);                                                                                                                        //Natural: ADD 1 TO #MODE-CTR ( 22 )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(3),"RUN DATE : ",Global.getDATU(),new ColumnSpacing(12),"IA ADMINISTRATION SELF-REMITTER REPORT",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 03X 'RUN DATE : ' *DATU 12X 'IA ADMINISTRATION SELF-REMITTER REPORT' 45X 'PAGE :' *PAGE-NUMBER ( 1 ) / 37X 'ACTIVE CONTRACTS ISSUED IN ' #DATE //
                        ColumnSpacing(45),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(37),"ACTIVE CONTRACTS ISSUED IN ",pnd_Date,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"==================================",NEWLINE);                                                      //Natural: WRITE ( 1 ) NOHDR '==================================' /
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                       //Natural: WRITE ( 1 ) NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                      //Natural: WRITE ( 1 ) NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                    //Natural: WRITE ( 1 ) NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"==================================",NEWLINE);                                                      //Natural: WRITE ( 1 ) NOHDR '==================================' /
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "PPCN/PAYEE",
        		pnd_Trans_Ppcn_Nbr,"ISSUE/STATE",
        		iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde,"STATE/NAME",
        		pnd_State,"PIN",
        		iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr,"ORGN/CODE",
        		iaa_Cntrct_Cntrct_Orgn_Cde,"OPTN/CODE",
        		iaa_Cntrct_Cntrct_Optn_Cde,"ISSUE/DATE",
        		iaa_Cntrct_Cntrct_Issue_Dte, new ReportEditMask ("9999/99"),"ORIG./DA/CONTRACT",
        		iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr,"MODE",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind,"NAME",
        		pnd_Name_Free,"ORIG./PPG",
        		iaa_Cntrct_Cntrct_Inst_Iss_Cde,"PERIODIC/PAYMENT",
        		pnd_Tot_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
    }
}
