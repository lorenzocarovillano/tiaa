/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:38 PM
**        * FROM NATURAL PROGRAM : Iatp120s
************************************************************
**        * FILE NAME            : Iatp120s.java
**        * CLASS NAME           : Iatp120s
**        * INSTANCE NAME        : Iatp120s
************************************************************
************************************************************************
* PROGRAM:  IATP120S
* FUNCTION: ETNAT SELECTION THAT WILL FEED INTO # OF FROM-TO REPORT
*           (IATP120) SORTD BY EFFECTIVE DATE. ONLY DIFF BET. THIS PROG.
*           AND IATP100S IS THIS PROGRAM ALSO SELECTS FROM GRADED TO
*           STANDARD TRANSFERS.
* CREATED:  11/10/97 BY ARI GROSSMAN
* 04/2017 OS PIN EXPANSION CHNAGES MARKED 082017.
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp120s extends BLNatBase
{
    // Data Areas
    private LdaIatl010 ldaIatl010;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Payee_Cde;
    private DbsField pnd_I;
    private DbsField pnd_To_Units;
    private DbsField pnd_W_Units;
    private DbsField pnd_D_Units;
    private DbsField pnd_E_Units;
    private DbsField pnd_Date8;
    private DbsField pnd_Auv_Dte;
    private DbsField pnd_Auv;
    private DbsField pnd_Auv_Ret_Dte;
    private DbsField pnd_Rc;
    private DbsField pnd_W_To_Date;
    private DbsField pnd_W_From_Date;

    private DbsGroup pnd_From_To_Dates;
    private DbsField pnd_From_To_Dates_Pnd_From_Date;

    private DbsGroup pnd_From_To_Dates__R_Field_2;
    private DbsField pnd_From_To_Dates_Pnd_From_Date_Cc;
    private DbsField pnd_From_To_Dates_Pnd_From_Date_Yy;
    private DbsField pnd_From_To_Dates_Pnd_From_Date_Mm;
    private DbsField pnd_From_To_Dates_Pnd_From_Date_Dd;
    private DbsField pnd_From_To_Dates_Pnd_To_Date;

    private DbsGroup pnd_From_To_Dates__R_Field_3;
    private DbsField pnd_From_To_Dates_Pnd_To_Date_A;

    private DbsGroup pnd_From_To_Dates__R_Field_4;
    private DbsField pnd_From_To_Dates_Pnd_To_Date_Cc;
    private DbsField pnd_From_To_Dates_Pnd_To_Date_Yy;
    private DbsField pnd_From_To_Dates_Pnd_To_Date_Mm;
    private DbsField pnd_From_To_Dates_Pnd_To_Date_Dd;
    private DbsField pnd_Cmpny_Fund_3;
    private DbsField pnd_Invrse_Recvd_Time;

    private DbsGroup pnd_Invrse_Recvd_Time__R_Field_5;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Fund;
    private DbsField pnd_Invrse_Recvd_Time_Pnd_Irt_Dte;
    private DbsField pnd_Ia_Super_De_02;

    private DbsGroup pnd_Ia_Super_De_02__R_Field_6;
    private DbsField pnd_Ia_Super_De_02_Pnd_De_02_Type_Cde;
    private DbsField pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_1;
    private DbsField pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_2;
    private DbsField pnd_Ia_Super_De_02_Pnd_De_02_Rqst_Id;
    private DbsField pnd_Stts_Cde;

    private DbsGroup pnd_Stts_Cde__R_Field_7;
    private DbsField pnd_Stts_Cde_Pnd_Stts_Cde_1;
    private DbsField pnd_Stts_Cde_Pnd_Stts_Cde_2;
    private DbsField pnd_J;
    private DbsField pnd_Rec_Acpt;
    private DbsField pnd_Rec_Read;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIatl010 = new LdaIatl010();
        registerRecord(ldaIatl010);
        registerRecord(ldaIatl010.getVw_iatl010());

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cntrct_Payee_Key_Pnd_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_To_Units = localVariables.newFieldInRecord("pnd_To_Units", "#TO-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Units = localVariables.newFieldInRecord("pnd_W_Units", "#W-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_D_Units = localVariables.newFieldInRecord("pnd_D_Units", "#D-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_E_Units = localVariables.newFieldInRecord("pnd_E_Units", "#E-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Date8 = localVariables.newFieldInRecord("pnd_Date8", "#DATE8", FieldType.STRING, 8);
        pnd_Auv_Dte = localVariables.newFieldInRecord("pnd_Auv_Dte", "#AUV-DTE", FieldType.NUMERIC, 8);
        pnd_Auv = localVariables.newFieldInRecord("pnd_Auv", "#AUV", FieldType.NUMERIC, 5, 2);
        pnd_Auv_Ret_Dte = localVariables.newFieldInRecord("pnd_Auv_Ret_Dte", "#AUV-RET-DTE", FieldType.NUMERIC, 8);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_W_To_Date = localVariables.newFieldInRecord("pnd_W_To_Date", "#W-TO-DATE", FieldType.STRING, 8);
        pnd_W_From_Date = localVariables.newFieldInRecord("pnd_W_From_Date", "#W-FROM-DATE", FieldType.STRING, 8);

        pnd_From_To_Dates = localVariables.newGroupInRecord("pnd_From_To_Dates", "#FROM-TO-DATES");
        pnd_From_To_Dates_Pnd_From_Date = pnd_From_To_Dates.newFieldInGroup("pnd_From_To_Dates_Pnd_From_Date", "#FROM-DATE", FieldType.NUMERIC, 8);

        pnd_From_To_Dates__R_Field_2 = pnd_From_To_Dates.newGroupInGroup("pnd_From_To_Dates__R_Field_2", "REDEFINE", pnd_From_To_Dates_Pnd_From_Date);
        pnd_From_To_Dates_Pnd_From_Date_Cc = pnd_From_To_Dates__R_Field_2.newFieldInGroup("pnd_From_To_Dates_Pnd_From_Date_Cc", "#FROM-DATE-CC", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_From_Date_Yy = pnd_From_To_Dates__R_Field_2.newFieldInGroup("pnd_From_To_Dates_Pnd_From_Date_Yy", "#FROM-DATE-YY", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_From_Date_Mm = pnd_From_To_Dates__R_Field_2.newFieldInGroup("pnd_From_To_Dates_Pnd_From_Date_Mm", "#FROM-DATE-MM", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_From_Date_Dd = pnd_From_To_Dates__R_Field_2.newFieldInGroup("pnd_From_To_Dates_Pnd_From_Date_Dd", "#FROM-DATE-DD", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_To_Date = pnd_From_To_Dates.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date", "#TO-DATE", FieldType.NUMERIC, 8);

        pnd_From_To_Dates__R_Field_3 = pnd_From_To_Dates.newGroupInGroup("pnd_From_To_Dates__R_Field_3", "REDEFINE", pnd_From_To_Dates_Pnd_To_Date);
        pnd_From_To_Dates_Pnd_To_Date_A = pnd_From_To_Dates__R_Field_3.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date_A", "#TO-DATE-A", FieldType.STRING, 
            8);

        pnd_From_To_Dates__R_Field_4 = pnd_From_To_Dates.newGroupInGroup("pnd_From_To_Dates__R_Field_4", "REDEFINE", pnd_From_To_Dates_Pnd_To_Date);
        pnd_From_To_Dates_Pnd_To_Date_Cc = pnd_From_To_Dates__R_Field_4.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date_Cc", "#TO-DATE-CC", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_To_Date_Yy = pnd_From_To_Dates__R_Field_4.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date_Yy", "#TO-DATE-YY", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_To_Date_Mm = pnd_From_To_Dates__R_Field_4.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date_Mm", "#TO-DATE-MM", FieldType.STRING, 
            2);
        pnd_From_To_Dates_Pnd_To_Date_Dd = pnd_From_To_Dates__R_Field_4.newFieldInGroup("pnd_From_To_Dates_Pnd_To_Date_Dd", "#TO-DATE-DD", FieldType.STRING, 
            2);
        pnd_Cmpny_Fund_3 = localVariables.newFieldInRecord("pnd_Cmpny_Fund_3", "#CMPNY-FUND-3", FieldType.STRING, 3);
        pnd_Invrse_Recvd_Time = localVariables.newFieldInRecord("pnd_Invrse_Recvd_Time", "#INVRSE-RECVD-TIME", FieldType.NUMERIC, 14);

        pnd_Invrse_Recvd_Time__R_Field_5 = localVariables.newGroupInRecord("pnd_Invrse_Recvd_Time__R_Field_5", "REDEFINE", pnd_Invrse_Recvd_Time);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Fund = pnd_Invrse_Recvd_Time__R_Field_5.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Fund", "#IRT-FUND", FieldType.NUMERIC, 
            2);
        pnd_Invrse_Recvd_Time_Pnd_Irt_Dte = pnd_Invrse_Recvd_Time__R_Field_5.newFieldInGroup("pnd_Invrse_Recvd_Time_Pnd_Irt_Dte", "#IRT-DTE", FieldType.NUMERIC, 
            12);
        pnd_Ia_Super_De_02 = localVariables.newFieldInRecord("pnd_Ia_Super_De_02", "#IA-SUPER-DE-02", FieldType.STRING, 37);

        pnd_Ia_Super_De_02__R_Field_6 = localVariables.newGroupInRecord("pnd_Ia_Super_De_02__R_Field_6", "REDEFINE", pnd_Ia_Super_De_02);
        pnd_Ia_Super_De_02_Pnd_De_02_Type_Cde = pnd_Ia_Super_De_02__R_Field_6.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_De_02_Type_Cde", "#DE-02-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_1 = pnd_Ia_Super_De_02__R_Field_6.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_1", "#DE-02-STTS-CDE-1", 
            FieldType.STRING, 1);
        pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_2 = pnd_Ia_Super_De_02__R_Field_6.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_2", "#DE-02-STTS-CDE-2", 
            FieldType.STRING, 1);
        pnd_Ia_Super_De_02_Pnd_De_02_Rqst_Id = pnd_Ia_Super_De_02__R_Field_6.newFieldInGroup("pnd_Ia_Super_De_02_Pnd_De_02_Rqst_Id", "#DE-02-RQST-ID", 
            FieldType.STRING, 34);
        pnd_Stts_Cde = localVariables.newFieldInRecord("pnd_Stts_Cde", "#STTS-CDE", FieldType.STRING, 2);

        pnd_Stts_Cde__R_Field_7 = localVariables.newGroupInRecord("pnd_Stts_Cde__R_Field_7", "REDEFINE", pnd_Stts_Cde);
        pnd_Stts_Cde_Pnd_Stts_Cde_1 = pnd_Stts_Cde__R_Field_7.newFieldInGroup("pnd_Stts_Cde_Pnd_Stts_Cde_1", "#STTS-CDE-1", FieldType.STRING, 1);
        pnd_Stts_Cde_Pnd_Stts_Cde_2 = pnd_Stts_Cde__R_Field_7.newFieldInGroup("pnd_Stts_Cde_Pnd_Stts_Cde_2", "#STTS-CDE-2", FieldType.STRING, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Rec_Acpt = localVariables.newFieldInRecord("pnd_Rec_Acpt", "#REC-ACPT", FieldType.NUMERIC, 6);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();

        ldaIatl010.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp120s() throws Exception
    {
        super("Iatp120s");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*  WRITE '***************************' /
        //*        '  START OF IATP120S PROGRAM'/
        //*        '***************************'
        pnd_Ia_Super_De_02_Pnd_De_02_Type_Cde.setValue("1");                                                                                                              //Natural: MOVE '1' TO #DE-02-TYPE-CDE
        pnd_Ia_Super_De_02_Pnd_De_02_Stts_Cde_1.setValue("F");                                                                                                            //Natural: MOVE 'F' TO #DE-02-STTS-CDE-1
        //*  R1. READ IATL010 PHYSICAL
        ldaIatl010.getVw_iatl010().startDatabaseRead                                                                                                                      //Natural: READ IATL010 BY IA-SUPER-DE-02 STARTING FROM #IA-SUPER-DE-02
        (
        "R1",
        new Wc[] { new Wc("IA_SUPER_DE_02", ">=", pnd_Ia_Super_De_02, WcType.BY) },
        new Oc[] { new Oc("IA_SUPER_DE_02", "ASC") }
        );
        R1:
        while (condition(ldaIatl010.getVw_iatl010().readNextRow("R1")))
        {
            //*  WRITE 'BEFORE ACCEPT' '=' IA-FRM-CNTRCT '=' XFR-STTS-CDE
            //*      ACCEPT IF IA-FRM-CNTRCT
            //*                              = '6M600166  ' OR
            //*                              = 'Z0000662  '
            //*      WRITE '=' IA-FRM-CNTRCT '=' XFR-STTS-CDE /
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            pnd_Stts_Cde.setValue(ldaIatl010.getIatl010_Xfr_Stts_Cde());                                                                                                  //Natural: MOVE XFR-STTS-CDE TO #STTS-CDE
            if (condition(ldaIatl010.getIatl010_Rcrd_Type_Cde().equals("1") && pnd_Stts_Cde_Pnd_Stts_Cde_1.equals("F")))                                                  //Natural: IF IATL010.RCRD-TYPE-CDE EQ '1' AND #STTS-CDE-1 EQ 'F'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaIatl010.getIatl010_Xfr_Stts_Cde().equals("F6") || ldaIatl010.getIatl010_Xfr_Stts_Cde().equals("F0") || ldaIatl010.getIatl010_Xfr_Stts_Cde().equals("F2")  //Natural: ACCEPT IF IATL010.XFR-STTS-CDE = 'F6' OR = 'F0' OR = 'F2' OR = 'F4'
                || ldaIatl010.getIatl010_Xfr_Stts_Cde().equals("F4"))))
            {
                continue;
            }
            //*  WRITE 'AFTER ACCEPT' '=' IA-FRM-CNTRCT
            getWorkFiles().write(1, false, ldaIatl010.getVw_iatl010());                                                                                                   //Natural: WRITE WORK FILE 1 IATL010
            pnd_Rec_Acpt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-ACPT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS READ ==========> ",pnd_Rec_Read);                                                                                                  //Natural: WRITE 'RECORDS READ ==========> ' #REC-READ
        if (Global.isEscape()) return;
        getReports().write(0, "RECORDS ACCEPTED  =====> ",pnd_Rec_Acpt);                                                                                                  //Natural: WRITE 'RECORDS ACCEPTED  =====> ' #REC-ACPT
        if (Global.isEscape()) return;
        //* ******************************************************************
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
