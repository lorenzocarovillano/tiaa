/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:27 PM
**        * FROM NATURAL PROGRAM : Iaap590
************************************************************
**        * FILE NAME            : Iaap590.java
**        * CLASS NAME           : Iaap590
**        * INSTANCE NAME        : Iaap590
************************************************************
************************************************************************
* PROGRAM  : IAAP590
* SYSTEM   : IA
* TITLE    : REPORT
* DATE     : DEC 09,2003
* FUNCTION : PRODUCE REPORT DEATH CLAIMS WITH OVERPAYMENTS AND
*            NO RECOVERY DEDUCTION SETUP FOR CONTRACTS
*
* HISTORY
* 11/04    ADDED LOGIC TO READ DEDUCTION-TRANS FILE TO PREVENT
*          REPORTING OVERPAYMENTS NOT RECOVERED AT END OF YEAR
*          WHEN ANY DEDUCTIONS THAT ARE NOT ACTIVE GET DELETED OFF FILE
*          WHERE OVERPAYMENT HAS BEEN RECOVERED
* 08/15    COR/NAAD SUNSET. SC 08/15
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap590 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Dc_Pmt_Audit;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Id_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Payee_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Timestamp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Status_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Status_Timestamp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Typ;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ctznshp_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Rsdncy_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Locality_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ph_Name_First;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Middle;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Last;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Decedent_Name;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Dob_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Dod_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Proof_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Lst_Pymnt_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Notify_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Cycle_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Acctg_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte;

    private DbsGroup iaa_Dc_Pmt_Audit__R_Field_1;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Yyyymm;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Dd;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Intrfce_Dte;

    private DbsGroup iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Name;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_1;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_2;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_3;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_4;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_5;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_6;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_City;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_State;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Addrss_Zip;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Type_Req_Ind;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Type_Ind;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Roll_Dest_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Eft_Acct_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Eft_Transit_Id;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Chk_Sav_Ind;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Hold_Cde;

    private DbsGroup iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Rate_Basis;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Per_Pymnt;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Per_Dvdnd;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Per_Units;
    private DbsField iaa_Dc_Pmt_Audit_Count_Castpaudit_Installments;

    private DbsGroup iaa_Dc_Pmt_Audit_Paudit_Installments;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dte;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Gross;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Guar;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Divd;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ivc;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dci;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Units;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ovrpymnt;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Futr_Ind;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Oper_Id;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Oper_Timestamp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Oper_User_Grp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Verf_Id;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Verf_Timestamp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Verf_User_Grp;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Prefix;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Suffix;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Inv_Acct_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Surv_Bene_Sex_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_New_Acct_Ind;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ovr_Prcnt_This_Pye;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ovr_Method_Of_Rcvry_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ovr_Pymnt_Rcvry_Per_Amt;

    private DataAccessProgramView vw_iaa_Dc_Ovrpymnt;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_Id;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_Timestamp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_User_Grp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_Id;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_Timestamp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_User_Grp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Id_Nbr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Tax_Id_Nbr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Process_Type;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Req_Seq_Nbr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Timestamp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Ppcn_Nbr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Payee_Cde;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Cde;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Timestamp;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Annt_Type;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Ind;
    private DbsField iaa_Dc_Ovrpymnt_Count_Castovrpymnt_Data;

    private DbsGroup iaa_Dc_Ovrpymnt_Ovrpymnt_Data;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Mo;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Yr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Mo;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Nbr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Dcdnt_Per_Amt;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Surv_Ben_Per_Amt;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Ovr_Per_Amt;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Yr_Ovr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Grand_Ttl_Ovr_Amt;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Amt_Rtrnd;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Rmn_Amt_To_Rcvr;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Optn_Cde;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Pymnt_Mode;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date;

    private DbsGroup iaa_Dc_Ovrpymnt__R_Field_2;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Yy;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Mm;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Dd;
    private DbsField iaa_Dc_Ovrpymnt_Ovrpymnt_Rcvry_Amt_Accounted_For;
    private DbsField pnd_Dc_Ovrpymnt_Key;

    private DbsGroup pnd_Dc_Ovrpymnt_Key__R_Field_3;
    private DbsField pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Id_Nbr;
    private DbsField pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Bypass_This_Cntrct;

    private DbsGroup iaa_Parm_Card;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date;

    private DbsGroup iaa_Parm_Card__R_Field_4;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_N;

    private DbsGroup iaa_Parm_Card__R_Field_5;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Yyyy;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Mm;
    private DbsField iaa_Parm_Card_Pnd_Parm_Date_Dd;
    private DbsField pnd_Paud_Dte_Yyyymmdd_Alph;

    private DbsGroup pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_6;
    private DbsField pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyymmdd_Num;

    private DbsGroup pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7;
    private DbsField pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyy;
    private DbsField pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Mm;
    private DbsField pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Dd;

    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Lst_Trans_Dte;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;
    private DbsField pnd_Ded_Key;

    private DbsGroup pnd_Ded_Key__R_Field_8;
    private DbsField pnd_Ded_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Ded_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Ded_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Ded_Key_Pnd_Ddctn_Cde;

    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Id_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;

    private DbsGroup iaa_Ddctn_Trans__R_Field_9;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde2;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ytd_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Intent_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Trans_Check_Dte;
    private DbsField ddctn_Bfre_Key;

    private DbsGroup ddctn_Bfre_Key__R_Field_10;
    private DbsField ddctn_Bfre_Key_Bfr_Id;
    private DbsField ddctn_Bfre_Key_Pnd_Cntrct_Nbr10;

    private DbsGroup ddctn_Bfre_Key__R_Field_11;
    private DbsField ddctn_Bfre_Key_Pnd_Cntrct_Nbr;
    private DbsField ddctn_Bfre_Key_Pnd_Cntrct_Hex;
    private DbsField ddctn_Bfre_Key_Pnd_Cntrct_Payee;
    private DbsField ddctn_Bfre_Key_Pnd_Ded_Seq_Cde;
    private DbsField ddctn_Bfre_Key_Pnd_Ded_Trn_Dte;
    private DbsField pnd_Sve_Cntrct_Nbr;
    private DbsField pnd_Sve_Payee;
    private DbsField pnd_Cnt;
    private DbsField pnd_Wth_3_Ded;
    private DbsField pnd_Wth_2_Ded;
    private DbsField pnd_Wth_1_Ded;
    private DbsField pnd_Tot_Ded_Rem;
    private DbsField pnd_Wrk_Name;
    private DbsField pnd_Fnd_Nme_Sw;
    private DbsField pnd_Fnd_Ded_Sw;
    private DbsField pnd_Fnd_Actv_Cpr;
    private DbsField pnd_Wrt_Nme_Sw;
    private DbsField pnd_Name_Key;

    private DbsGroup pnd_Name_Key__R_Field_12;
    private DbsField pnd_Name_Key_Pnd_Type_N;
    private DbsField pnd_Name_Key_Pnd_Contrct_N;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_13;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;

    private DbsGroup iaa_Trans_Rcrd__R_Field_14;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;

    private DbsGroup iaa_Trans_Rcrd__R_Field_15;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr8;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr2;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_16;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Sve_Trns_User_Id;
    private DbsField pnd_Sve_Ssn;
    private DbsField pnd_Sve_Dc_Pin;
    private DbsField pnd_Frst_Read;
    private DbsField pnd_Frst_Prt;
    private DbsField pnd_I;
    private DbsField pnd_Dcx;

    private DbsGroup pnd_Dc_Cntrct_Tab;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Id_Nbr;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Name;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Amt_To_Rcvr;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt;
    private DbsField pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte;
    private DbsField pnd_W_Hex_High_Value;
    private DbsField pnd_Rpt_Written;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Dc_Pmt_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Pmt_Audit", "IAA-DC-PMT-AUDIT"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_DC_PMT_AUDIT"));
        iaa_Dc_Pmt_Audit_Paudit_Id_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Id_Nbr", "PAUDIT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PAUDIT_ID_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr", "PAUDIT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PAUDIT_TAX_ID_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Payee_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key", "PAUDIT-PPCN-PYE-KEY", 
            FieldType.STRING, 12, RepeatingFieldStrategy.None, "PAUDIT_PPCN_PYE_KEY");
        iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key.setSuperDescriptor(true);
        iaa_Dc_Pmt_Audit_Paudit_Timestamp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Timestamp", "PAUDIT-TIMESTAMP", FieldType.TIME, 
            RepeatingFieldStrategy.None, "PAUDIT_TIMESTAMP");
        iaa_Dc_Pmt_Audit_Paudit_Status_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Status_Cde", "PAUDIT-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAUDIT_STATUS_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Status_Timestamp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Status_Timestamp", "PAUDIT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PAUDIT_STATUS_TIMESTAMP");
        iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Nbr", "PAUDIT-PYEE-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PAUDIT_PYEE_TAX_ID_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Typ = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Typ", "PAUDIT-PYEE-TAX-ID-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_PYEE_TAX_ID_TYP");
        iaa_Dc_Pmt_Audit_Paudit_Ctznshp_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ctznshp_Cde", "PAUDIT-CTZNSHP-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAUDIT_CTZNSHP_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Rsdncy_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Rsdncy_Cde", "PAUDIT-RSDNCY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAUDIT_RSDNCY_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Locality_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Locality_Cde", "PAUDIT-LOCALITY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAUDIT_LOCALITY_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Ph_Name_First = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ph_Name_First", "PAUDIT-PH-NAME-FIRST", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_FIRST");
        iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Middle = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Middle", "PAUDIT-PH-NAME-MIDDLE", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_MIDDLE");
        iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Last = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Last", "PAUDIT-PH-NAME-LAST", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_LAST");
        iaa_Dc_Pmt_Audit_Paudit_Decedent_Name = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Decedent_Name", "PAUDIT-DECEDENT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "PAUDIT_DECEDENT_NAME");
        iaa_Dc_Pmt_Audit_Paudit_Dob_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Dob_Dte", "PAUDIT-DOB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_DOB_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Dod_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Dod_Dte", "PAUDIT-DOD-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_DOD_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Proof_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Proof_Dte", "PAUDIT-PROOF-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PROOF_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Lst_Pymnt_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Lst_Pymnt_Dte", "PAUDIT-LST-PYMNT-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "PAUDIT_LST_PYMNT_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Notify_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Notify_Dte", "PAUDIT-NOTIFY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "PAUDIT_NOTIFY_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Cycle_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Cycle_Dte", "PAUDIT-CYCLE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_CYCLE_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Acctg_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Acctg_Dte", "PAUDIT-ACCTG-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_ACCTG_DTE");
        iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte", "PAUDIT-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PYMNT_DTE");

        iaa_Dc_Pmt_Audit__R_Field_1 = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupInGroup("iaa_Dc_Pmt_Audit__R_Field_1", "REDEFINE", iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte);
        iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Yyyymm = iaa_Dc_Pmt_Audit__R_Field_1.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Yyyymm", "PAUDIT-PYMNT-DTE-YYYYMM", 
            FieldType.NUMERIC, 6);
        iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Dd = iaa_Dc_Pmt_Audit__R_Field_1.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Dd", "PAUDIT-PYMNT-DTE-DD", 
            FieldType.NUMERIC, 2);
        iaa_Dc_Pmt_Audit_Paudit_Intrfce_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Intrfce_Dte", "PAUDIT-INTRFCE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "PAUDIT_INTRFCE_DTE");

        iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupArrayInGroup("iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp", "PAUDIT-NAME-ADDRSS-GRP", 
            new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Name = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Name", "PAUDIT-NAME", FieldType.STRING, 
            35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_NAME", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_1 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_1", "PAUDIT-ADDRSS-LNE-1", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_1", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_2 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_2", "PAUDIT-ADDRSS-LNE-2", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_2", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_3 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_3", "PAUDIT-ADDRSS-LNE-3", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_3", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_4 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_4", "PAUDIT-ADDRSS-LNE-4", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_4", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_5 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_5", "PAUDIT-ADDRSS-LNE-5", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_5", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_6 = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Lne_6", "PAUDIT-ADDRSS-LNE-6", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_LNE_6", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_City = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_City", "PAUDIT-ADDRSS-CITY", 
            FieldType.STRING, 21, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_CITY", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_State = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_State", "PAUDIT-ADDRSS-STATE", 
            FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_STATE", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Addrss_Zip = iaa_Dc_Pmt_Audit_Paudit_Name_Addrss_Grp.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Addrss_Zip", "PAUDIT-ADDRSS-ZIP", 
            FieldType.STRING, 9, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_ADDRSS_ZIP", "IA_DEATH_CLAIMS_PAUDIT_NAME_ADDRSS_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Type_Req_Ind = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Type_Req_Ind", "PAUDIT-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PAUDIT_TYPE_REQ_IND");
        iaa_Dc_Pmt_Audit_Paudit_Type_Ind = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Type_Ind", "PAUDIT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PAUDIT_TYPE_IND");
        iaa_Dc_Pmt_Audit_Paudit_Roll_Dest_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Roll_Dest_Cde", "PAUDIT-ROLL-DEST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PAUDIT_ROLL_DEST_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Eft_Acct_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Eft_Acct_Nbr", "PAUDIT-EFT-ACCT-NBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "PAUDIT_EFT_ACCT_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Eft_Transit_Id = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Eft_Transit_Id", "PAUDIT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PAUDIT_EFT_TRANSIT_ID");
        iaa_Dc_Pmt_Audit_Paudit_Chk_Sav_Ind = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Chk_Sav_Ind", "PAUDIT-CHK-SAV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_CHK_SAV_IND");
        iaa_Dc_Pmt_Audit_Paudit_Hold_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Hold_Cde", "PAUDIT-HOLD-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PAUDIT_HOLD_CDE");

        iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupArrayInGroup("iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl", "PAUDIT-RATE-TBL", 
            new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        iaa_Dc_Pmt_Audit_Paudit_Rate_Basis = iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Rate_Basis", "PAUDIT-RATE-BASIS", 
            FieldType.NUMERIC, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_RATE_BASIS", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        iaa_Dc_Pmt_Audit_Paudit_Per_Pymnt = iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Per_Pymnt", "PAUDIT-PER-PYMNT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_PYMNT", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        iaa_Dc_Pmt_Audit_Paudit_Per_Dvdnd = iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Per_Dvdnd", "PAUDIT-PER-DVDND", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_DVDND", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        iaa_Dc_Pmt_Audit_Paudit_Per_Units = iaa_Dc_Pmt_Audit_Paudit_Rate_Tbl.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Per_Units", "PAUDIT-PER-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_PER_UNITS", "IA_DEATH_CLAIMS_PAUDIT_RATE_TBL");
        iaa_Dc_Pmt_Audit_Count_Castpaudit_Installments = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Count_Castpaudit_Installments", 
            "C*PAUDIT-INSTALLMENTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");

        iaa_Dc_Pmt_Audit_Paudit_Installments = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupArrayInGroup("iaa_Dc_Pmt_Audit_Paudit_Installments", "PAUDIT-INSTALLMENTS", 
            new DbsArrayController(1, 84) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dte = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dte", "PAUDIT-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DTE", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Typ", "PAUDIT-INSTLLMNT-TYP", 
            FieldType.STRING, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_TYP", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Gross = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Gross", "PAUDIT-INSTLLMNT-GROSS", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GROSS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Guar = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Guar", "PAUDIT-INSTLLMNT-GUAR", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_GUAR", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Divd = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Divd", "PAUDIT-INSTLLMNT-DIVD", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DIVD", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ivc = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ivc", "PAUDIT-INSTLLMNT-IVC", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_IVC", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dci = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Dci", "PAUDIT-INSTLLMNT-DCI", 
            FieldType.PACKED_DECIMAL, 11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_DCI", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Units = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Units", "PAUDIT-INSTLLMNT-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_UNITS", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ovrpymnt = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ovrpymnt", 
            "PAUDIT-INSTLLMNT-OVRPYMNT", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_OVRPYMNT", 
            "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Futr_Ind = iaa_Dc_Pmt_Audit_Paudit_Installments.newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Futr_Ind", 
            "PAUDIT-INSTLLMNT-FUTR-IND", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAUDIT_INSTLLMNT_FUTR_IND", "IA_DEATH_CLAIMS_PAUDIT_INSTALLMENTS");
        iaa_Dc_Pmt_Audit_Paudit_Oper_Id = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_OPER_ID", "PAUDIT-OPER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_ID");
        iaa_Dc_Pmt_Audit_Paudit_Oper_Timestamp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_OPER_TIMESTAMP", "PAUDIT-OPER-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FETV_OPER_TIMESTAMP");
        iaa_Dc_Pmt_Audit_Paudit_Oper_User_Grp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_OPER_USER_GRP", "PAUDIT-OPER-USER-GRP", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_USER_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Verf_Id = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_VERF_ID", "PAUDIT-VERF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "FETV_VERF_ID");
        iaa_Dc_Pmt_Audit_Paudit_Verf_Timestamp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_VERF_TIMESTAMP", "PAUDIT-VERF-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "BNFCRY_VERF_TIMESTAMP");
        iaa_Dc_Pmt_Audit_Paudit_Verf_User_Grp = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_VERF_USER_GRP", "PAUDIT-VERF-USER-GRP", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "FETV_VERF_USER_GRP");
        iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr", 
            "PAUDIT-REMAIN-OVRPYMNT-TO-RCVR", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "PAUDIT_REMAIN_OVRPYMNT_TO_RCVR");
        iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Prefix = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Prefix", "PAUDIT-PH-NAME-PREFIX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_PREFIX");
        iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Suffix = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ph_Name_Suffix", "PAUDIT-PH-NAME-SUFFIX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PAUDIT_PH_NAME_SUFFIX");
        iaa_Dc_Pmt_Audit_Paudit_Inv_Acct_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Inv_Acct_Cde", "PAUDIT-INV-ACCT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_INV_ACCT_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Surv_Bene_Sex_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("IAA_DC_PMT_AUDIT_PAUDIT_SURV_BENE_SEX_CDE", "PAUDIT-SURV-BENE-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PAYMNT_SURV_BENE_SEX_CDE");
        iaa_Dc_Pmt_Audit_Paudit_New_Acct_Ind = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_New_Acct_Ind", "PAUDIT-NEW-ACCT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_NEW_ACCT_IND");
        iaa_Dc_Pmt_Audit_Paudit_Ovr_Prcnt_This_Pye = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ovr_Prcnt_This_Pye", "PAUDIT-OVR-PRCNT-THIS-PYE", 
            FieldType.NUMERIC, 7, 4, RepeatingFieldStrategy.None, "PAUDIT_OVR_PRCNT_THIS_PYE");
        iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye", "PAUDIT-OVR-AMT-THIS-PYE", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "PAUDIT_OVR_AMT_THIS_PYE");
        iaa_Dc_Pmt_Audit_Paudit_Ovr_Method_Of_Rcvry_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ovr_Method_Of_Rcvry_Cde", 
            "PAUDIT-OVR-METHOD-OF-RCVRY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PAUDIT_OVR_METHOD_OF_RCVRY_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Ovr_Pymnt_Rcvry_Per_Amt = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ovr_Pymnt_Rcvry_Per_Amt", 
            "PAUDIT-OVR-PYMNT-RCVRY-PER-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "PAUDIT_OVR_PYMNT_RCVRY_PER_AMT");
        registerRecord(vw_iaa_Dc_Pmt_Audit);

        vw_iaa_Dc_Ovrpymnt = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Ovrpymnt", "IAA-DC-OVRPYMNT"), "IAA_DC_OVRPYMNT", "IA_DEATH_CLAIMS", DdmPeriodicGroups.getInstance().getGroups("IAA_DC_OVRPYMNT"));
        iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_Id = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_Id", "OVRPYMNT-OPER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_ID");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_Timestamp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_OPER_TIMESTAMP", "OVRPYMNT-OPER-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "FETV_OPER_TIMESTAMP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_User_Grp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Oper_User_Grp", "OVRPYMNT-OPER-USER-GRP", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "OVRPYMNT_OPER_USER_GRP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_Id = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_VERF_ID", "OVRPYMNT-VERF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "FETV_VERF_ID");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_Timestamp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_VERF_TIMESTAMP", "OVRPYMNT-VERF-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "BNFCRY_VERF_TIMESTAMP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Verf_User_Grp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_VERF_USER_GRP", "OVRPYMNT-VERF-USER-GRP", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "FETV_VERF_USER_GRP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Id_Nbr = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Id_Nbr", "OVRPYMNT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "OVRPYMNT_ID_NBR");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Tax_Id_Nbr = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Tax_Id_Nbr", "OVRPYMNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "OVRPYMNT_TAX_ID_NBR");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Process_Type = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Process_Type", "OVRPYMNT-PROCESS-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OVRPYMNT_PROCESS_TYPE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Req_Seq_Nbr = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Req_Seq_Nbr", "OVRPYMNT-REQ-SEQ-NBR", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "OVRPYMNT_REQ_SEQ_NBR");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Timestamp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Timestamp", "OVRPYMNT-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "OVRPYMNT_TIMESTAMP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Ppcn_Nbr = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Ppcn_Nbr", "OVRPYMNT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "OVRPYMNT_PPCN_NBR");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Payee_Cde = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Payee_Cde", "OVRPYMNT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "OVRPYMNT_PAYEE_CDE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Cde = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Cde", "OVRPYMNT-STATUS-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "OVRPYMNT_STATUS_CDE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Timestamp = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Status_Timestamp", "OVRPYMNT-STATUS-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "OVRPYMNT_STATUS_TIMESTAMP");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Annt_Type = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Annt_Type", "OVRPYMNT-ANNT-TYPE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "OVRPYMNT_ANNT_TYPE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Ind = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Ind", "OVRPYMNT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "OVRPYMNT_IND");
        iaa_Dc_Ovrpymnt_Count_Castovrpymnt_Data = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Count_Castovrpymnt_Data", "C*OVRPYMNT-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_DEATH_CLAIMS_OVRPYMNT_DATA");

        iaa_Dc_Ovrpymnt_Ovrpymnt_Data = vw_iaa_Dc_Ovrpymnt.getRecord().newGroupArrayInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Data", "OVRPYMNT-DATA", new DbsArrayController(1, 
            7) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Mo = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Mo", "OVRPYMNT-BEGIN-FISCAL-MO", 
            FieldType.NUMERIC, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_BEGIN_FISCAL_MO", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Yr = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Begin_Fiscal_Yr", "OVRPYMNT-BEGIN-FISCAL-YR", 
            FieldType.NUMERIC, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_BEGIN_FISCAL_YR", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Mo = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Mo", "OVRPYMNT-END-FISCAL-MO", 
            FieldType.NUMERIC, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_END_FISCAL_MO", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_End_Fiscal_Yr", "OVRPYMNT-END-FISCAL-YR", 
            FieldType.NUMERIC, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_END_FISCAL_YR", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Nbr = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Nbr", "OVRPYMNT-NBR", FieldType.NUMERIC, 
            2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_NBR", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Dcdnt_Per_Amt = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Dcdnt_Per_Amt", "OVRPYMNT-DCDNT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_DCDNT_PER_AMT", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Surv_Ben_Per_Amt = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Surv_Ben_Per_Amt", "OVRPYMNT-SURV-BEN-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_SURV_BEN_PER_AMT", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Ovr_Per_Amt = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Ovr_Per_Amt", "OVRPYMNT-OVR-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_OVR_PER_AMT", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Yr_Ovr = iaa_Dc_Ovrpymnt_Ovrpymnt_Data.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Yr_Ovr", "OVRPYMNT-TTL-YR-OVR", 
            FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "OVRPYMNT_TTL_YR_OVR", "IA_DEATH_CLAIMS_OVRPYMNT_DATA");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Grand_Ttl_Ovr_Amt = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Grand_Ttl_Ovr_Amt", "OVRPYMNT-GRAND-TTL-OVR-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "OVRPYMNT_GRAND_TTL_OVR_AMT");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Amt_Rtrnd = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Amt_Rtrnd", "OVRPYMNT-AMT-RTRND", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "OVRPYMNT_AMT_RTRND");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Rmn_Amt_To_Rcvr = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Ttl_Rmn_Amt_To_Rcvr", 
            "OVRPYMNT-TTL-RMN-AMT-TO-RCVR", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "OVRPYMNT_TTL_RMN_AMT_TO_RCVR");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Optn_Cde = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_OPTN_CDE", "OVRPYMNT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Pymnt_Mode = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("IAA_DC_OVRPYMNT_OVRPYMNT_PYMNT_MODE", "OVRPYMNT-PYMNT-MODE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MODE");
        iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date", 
            "OVRPYMNT-LAST-PYMNT-RCVD-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "OVRPYMNT_LAST_PYMNT_RCVD_DATE");

        iaa_Dc_Ovrpymnt__R_Field_2 = vw_iaa_Dc_Ovrpymnt.getRecord().newGroupInGroup("iaa_Dc_Ovrpymnt__R_Field_2", "REDEFINE", iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date);
        iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Yy = iaa_Dc_Ovrpymnt__R_Field_2.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Yy", 
            "OVRPYMNT-LAST-PYMNT-RCVD-DATE-YY", FieldType.NUMERIC, 4);
        iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Mm = iaa_Dc_Ovrpymnt__R_Field_2.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Mm", 
            "OVRPYMNT-LAST-PYMNT-RCVD-DATE-MM", FieldType.NUMERIC, 2);
        iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Dd = iaa_Dc_Ovrpymnt__R_Field_2.newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Last_Pymnt_Rcvd_Date_Dd", 
            "OVRPYMNT-LAST-PYMNT-RCVD-DATE-DD", FieldType.NUMERIC, 2);
        iaa_Dc_Ovrpymnt_Ovrpymnt_Rcvry_Amt_Accounted_For = vw_iaa_Dc_Ovrpymnt.getRecord().newFieldInGroup("iaa_Dc_Ovrpymnt_Ovrpymnt_Rcvry_Amt_Accounted_For", 
            "OVRPYMNT-RCVRY-AMT-ACCOUNTED-FOR", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "OVRPYMNT_RCVRY_AMT_ACCOUNTED_FOR");
        registerRecord(vw_iaa_Dc_Ovrpymnt);

        pnd_Dc_Ovrpymnt_Key = localVariables.newFieldInRecord("pnd_Dc_Ovrpymnt_Key", "#DC-OVRPYMNT-KEY", FieldType.STRING, 21);

        pnd_Dc_Ovrpymnt_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Dc_Ovrpymnt_Key__R_Field_3", "REDEFINE", pnd_Dc_Ovrpymnt_Key);
        pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Id_Nbr = pnd_Dc_Ovrpymnt_Key__R_Field_3.newFieldInGroup("pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Id_Nbr", "#OVRPYMNT-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Tax_Id_Nbr = pnd_Dc_Ovrpymnt_Key__R_Field_3.newFieldInGroup("pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Tax_Id_Nbr", "#OVRPYMNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Bypass_This_Cntrct = localVariables.newFieldInRecord("pnd_Bypass_This_Cntrct", "#BYPASS-THIS-CNTRCT", FieldType.STRING, 1);

        iaa_Parm_Card = localVariables.newGroupInRecord("iaa_Parm_Card", "IAA-PARM-CARD");
        iaa_Parm_Card_Pnd_Parm_Date = iaa_Parm_Card.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date", "#PARM-DATE", FieldType.STRING, 8);

        iaa_Parm_Card__R_Field_4 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card__R_Field_4", "REDEFINE", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_N = iaa_Parm_Card__R_Field_4.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_N", "#PARM-DATE-N", FieldType.NUMERIC, 8);

        iaa_Parm_Card__R_Field_5 = iaa_Parm_Card.newGroupInGroup("iaa_Parm_Card__R_Field_5", "REDEFINE", iaa_Parm_Card_Pnd_Parm_Date);
        iaa_Parm_Card_Pnd_Parm_Date_Yyyy = iaa_Parm_Card__R_Field_5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Yyyy", "#PARM-DATE-YYYY", FieldType.NUMERIC, 
            4);
        iaa_Parm_Card_Pnd_Parm_Date_Mm = iaa_Parm_Card__R_Field_5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Mm", "#PARM-DATE-MM", FieldType.NUMERIC, 
            2);
        iaa_Parm_Card_Pnd_Parm_Date_Dd = iaa_Parm_Card__R_Field_5.newFieldInGroup("iaa_Parm_Card_Pnd_Parm_Date_Dd", "#PARM-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Paud_Dte_Yyyymmdd_Alph = localVariables.newFieldInRecord("pnd_Paud_Dte_Yyyymmdd_Alph", "#PAUD-DTE-YYYYMMDD-ALPH", FieldType.STRING, 8);

        pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_6 = localVariables.newGroupInRecord("pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_6", "REDEFINE", pnd_Paud_Dte_Yyyymmdd_Alph);
        pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyymmdd_Num = pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_6.newFieldInGroup("pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyymmdd_Num", 
            "#PAUD-DTE-YYYYMMDD-NUM", FieldType.NUMERIC, 8);

        pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7 = localVariables.newGroupInRecord("pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7", "REDEFINE", pnd_Paud_Dte_Yyyymmdd_Alph);
        pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyy = pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7.newFieldInGroup("pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyy", 
            "#PAUD-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Mm = pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7.newFieldInGroup("pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Mm", 
            "#PAUD-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Dd = pnd_Paud_Dte_Yyyymmdd_Alph__R_Field_7.newFieldInGroup("pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Dd", 
            "#PAUD-DTE-DD", FieldType.NUMERIC, 2);

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Ytd_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_Ddctn_Pd_To_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Deduction);

        pnd_Ded_Key = localVariables.newFieldInRecord("pnd_Ded_Key", "#DED-KEY", FieldType.STRING, 18);

        pnd_Ded_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Ded_Key__R_Field_8", "REDEFINE", pnd_Ded_Key);
        pnd_Ded_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Ded_Key__R_Field_8.newFieldInGroup("pnd_Ded_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ded_Key_Pnd_Ddctn_Payee_Cde = pnd_Ded_Key__R_Field_8.newFieldInGroup("pnd_Ded_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ded_Key_Pnd_Ddctn_Seq_Nbr = pnd_Ded_Key__R_Field_8.newFieldInGroup("pnd_Ded_Key_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Ded_Key_Pnd_Ddctn_Cde = pnd_Ded_Key__R_Field_8.newFieldInGroup("pnd_Ded_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.NUMERIC, 3);

        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Id_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");

        iaa_Ddctn_Trans__R_Field_9 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans__R_Field_9", "REDEFINE", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans__R_Field_9.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Cde2 = iaa_Ddctn_Trans__R_Field_9.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde2", "DDCTN-CDE2", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Ytd_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ddctn_Trans_Ddctn_Pd_To_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Intent_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Trans_Check_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        registerRecord(vw_iaa_Ddctn_Trans);

        ddctn_Bfre_Key = localVariables.newFieldInRecord("ddctn_Bfre_Key", "DDCTN-BFRE-KEY", FieldType.STRING, 26);

        ddctn_Bfre_Key__R_Field_10 = localVariables.newGroupInRecord("ddctn_Bfre_Key__R_Field_10", "REDEFINE", ddctn_Bfre_Key);
        ddctn_Bfre_Key_Bfr_Id = ddctn_Bfre_Key__R_Field_10.newFieldInGroup("ddctn_Bfre_Key_Bfr_Id", "BFR-ID", FieldType.NUMERIC, 1);
        ddctn_Bfre_Key_Pnd_Cntrct_Nbr10 = ddctn_Bfre_Key__R_Field_10.newFieldInGroup("ddctn_Bfre_Key_Pnd_Cntrct_Nbr10", "#CNTRCT-NBR10", FieldType.STRING, 
            10);

        ddctn_Bfre_Key__R_Field_11 = ddctn_Bfre_Key__R_Field_10.newGroupInGroup("ddctn_Bfre_Key__R_Field_11", "REDEFINE", ddctn_Bfre_Key_Pnd_Cntrct_Nbr10);
        ddctn_Bfre_Key_Pnd_Cntrct_Nbr = ddctn_Bfre_Key__R_Field_11.newFieldInGroup("ddctn_Bfre_Key_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8);
        ddctn_Bfre_Key_Pnd_Cntrct_Hex = ddctn_Bfre_Key__R_Field_11.newFieldInGroup("ddctn_Bfre_Key_Pnd_Cntrct_Hex", "#CNTRCT-HEX", FieldType.STRING, 2);
        ddctn_Bfre_Key_Pnd_Cntrct_Payee = ddctn_Bfre_Key__R_Field_10.newFieldInGroup("ddctn_Bfre_Key_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.NUMERIC, 
            2);
        ddctn_Bfre_Key_Pnd_Ded_Seq_Cde = ddctn_Bfre_Key__R_Field_10.newFieldInGroup("ddctn_Bfre_Key_Pnd_Ded_Seq_Cde", "#DED-SEQ-CDE", FieldType.STRING, 
            6);
        ddctn_Bfre_Key_Pnd_Ded_Trn_Dte = ddctn_Bfre_Key__R_Field_10.newFieldInGroup("ddctn_Bfre_Key_Pnd_Ded_Trn_Dte", "#DED-TRN-DTE", FieldType.TIME);
        pnd_Sve_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Sve_Cntrct_Nbr", "#SVE-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Sve_Payee = localVariables.newFieldInRecord("pnd_Sve_Payee", "#SVE-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 7);
        pnd_Wth_3_Ded = localVariables.newFieldInRecord("pnd_Wth_3_Ded", "#WTH-3-DED", FieldType.NUMERIC, 7);
        pnd_Wth_2_Ded = localVariables.newFieldInRecord("pnd_Wth_2_Ded", "#WTH-2-DED", FieldType.NUMERIC, 7);
        pnd_Wth_1_Ded = localVariables.newFieldInRecord("pnd_Wth_1_Ded", "#WTH-1-DED", FieldType.NUMERIC, 7);
        pnd_Tot_Ded_Rem = localVariables.newFieldInRecord("pnd_Tot_Ded_Rem", "#TOT-DED-REM", FieldType.NUMERIC, 9, 2);
        pnd_Wrk_Name = localVariables.newFieldInRecord("pnd_Wrk_Name", "#WRK-NAME", FieldType.STRING, 30);
        pnd_Fnd_Nme_Sw = localVariables.newFieldInRecord("pnd_Fnd_Nme_Sw", "#FND-NME-SW", FieldType.STRING, 1);
        pnd_Fnd_Ded_Sw = localVariables.newFieldInRecord("pnd_Fnd_Ded_Sw", "#FND-DED-SW", FieldType.STRING, 1);
        pnd_Fnd_Actv_Cpr = localVariables.newFieldInRecord("pnd_Fnd_Actv_Cpr", "#FND-ACTV-CPR", FieldType.STRING, 1);
        pnd_Wrt_Nme_Sw = localVariables.newFieldInRecord("pnd_Wrt_Nme_Sw", "#WRT-NME-SW", FieldType.STRING, 1);
        pnd_Name_Key = localVariables.newFieldInRecord("pnd_Name_Key", "#NAME-KEY", FieldType.STRING, 12);

        pnd_Name_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Name_Key__R_Field_12", "REDEFINE", pnd_Name_Key);
        pnd_Name_Key_Pnd_Type_N = pnd_Name_Key__R_Field_12.newFieldInGroup("pnd_Name_Key_Pnd_Type_N", "#TYPE-N", FieldType.STRING, 2);
        pnd_Name_Key_Pnd_Contrct_N = pnd_Name_Key__R_Field_12.newFieldInGroup("pnd_Name_Key_Pnd_Contrct_N", "#CONTRCT-N", FieldType.STRING, 10);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_13 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_13", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_13.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_13.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");

        iaa_Trans_Rcrd__R_Field_14 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_14", "REDEFINE", iaa_Trans_Rcrd_Lst_Trans_Dte);
        iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd = iaa_Trans_Rcrd__R_Field_14.newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte_Rcrd", "LST-TRANS-DTE-RCRD", FieldType.TIME);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");

        iaa_Trans_Rcrd__R_Field_15 = vw_iaa_Trans_Rcrd.getRecord().newGroupInGroup("iaa_Trans_Rcrd__R_Field_15", "REDEFINE", iaa_Trans_Rcrd_Trans_Ppcn_Nbr);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr8 = iaa_Trans_Rcrd__R_Field_15.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr8", "TRANS-PPCN-NBR8", FieldType.STRING, 
            8);
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr2 = iaa_Trans_Rcrd__R_Field_15.newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr2", "TRANS-PPCN-NBR2", FieldType.STRING, 
            2);
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 24);

        pnd_Trans_Cntrct_Key__R_Field_16 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_16", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_16.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_16.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_16.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Sve_Trns_User_Id = localVariables.newFieldInRecord("pnd_Sve_Trns_User_Id", "#SVE-TRNS-USER-ID", FieldType.STRING, 8);
        pnd_Sve_Ssn = localVariables.newFieldInRecord("pnd_Sve_Ssn", "#SVE-SSN", FieldType.NUMERIC, 9);
        pnd_Sve_Dc_Pin = localVariables.newFieldInRecord("pnd_Sve_Dc_Pin", "#SVE-DC-PIN", FieldType.NUMERIC, 12);
        pnd_Frst_Read = localVariables.newFieldInRecord("pnd_Frst_Read", "#FRST-READ", FieldType.STRING, 1);
        pnd_Frst_Prt = localVariables.newFieldInRecord("pnd_Frst_Prt", "#FRST-PRT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 4);
        pnd_Dcx = localVariables.newFieldInRecord("pnd_Dcx", "#DCX", FieldType.NUMERIC, 3);

        pnd_Dc_Cntrct_Tab = localVariables.newGroupArrayInRecord("pnd_Dc_Cntrct_Tab", "#DC-CNTRCT-TAB", new DbsArrayController(1, 40));
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Id_Nbr = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Id_Nbr", "#DC-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye", "#DC-CNTRCT-PAYE", FieldType.STRING, 
            12);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr", "#DC-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee", "#DC-PAYEE", FieldType.NUMERIC, 2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id", "#DC-OPER-ID", FieldType.STRING, 8);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id", "#DC-TAX-ID", FieldType.NUMERIC, 9);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Name = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Name", "#DC-NAME", FieldType.STRING, 35);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt", "#DC-PER-AMT", FieldType.NUMERIC, 9, 
            2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over", "#DC-TOT-OVER", FieldType.NUMERIC, 
            9, 2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered", "#DC-TOT-RCOVERED", FieldType.NUMERIC, 
            9, 2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Amt_To_Rcvr = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Amt_To_Rcvr", "#DC-AMT-TO-RCVR", FieldType.NUMERIC, 
            9, 2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt", "#DC-REM-OVR-PMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte = pnd_Dc_Cntrct_Tab.newFieldInGroup("pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte", "#DC-PYMNT-DTE", FieldType.NUMERIC, 
            6);
        pnd_W_Hex_High_Value = localVariables.newFieldInRecord("pnd_W_Hex_High_Value", "#W-HEX-HIGH-VALUE", FieldType.STRING, 2);
        pnd_Rpt_Written = localVariables.newFieldInRecord("pnd_Rpt_Written", "#RPT-WRITTEN", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Dc_Pmt_Audit.reset();
        vw_iaa_Dc_Ovrpymnt.reset();
        vw_iaa_Deduction.reset();
        vw_iaa_Ddctn_Trans.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
        pnd_Frst_Read.setInitialValue("Y");
        pnd_Frst_Prt.setInitialValue("Y");
        pnd_W_Hex_High_Value.setInitialValue("H'FFFF'");
        pnd_Rpt_Written.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap590() throws Exception
    {
        super("Iaap590");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 133 PS = 56;//Natural: FORMAT ( 1 ) LS = 133 PS = 56;//Natural: FORMAT ( 2 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 1 IAA-PARM-CARD
        while (condition(getWorkFiles().read(1, iaa_Parm_Card)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
        vw_iaa_Dc_Pmt_Audit.startDatabaseRead                                                                                                                             //Natural: READ IAA-DC-PMT-AUDIT BY PIN-TAXID-SEQ-CNTRCT-PYE-KEY STARTING FROM 0000000
        (
        "READ01",
        new Wc[] { new Wc("PIN_TAXID_SEQ_CNTRCT_PYE_KEY", ">=", 0, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_CNTRCT_PYE_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_iaa_Dc_Pmt_Audit.readNextRow("READ01")))
        {
            pnd_Paud_Dte_Yyyymmdd_Alph.setValueEdited(iaa_Dc_Pmt_Audit_Paudit_Timestamp,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED PAUDIT-TIMESTAMP ( EM = YYYYMMDD ) TO #PAUD-DTE-YYYYMMDD-ALPH
            //*  ACCEPT IF #PAUD-DTE-YYYYMMDD-NUM EQ #PARM-DATE-N
            //*  ACCEPT IF #PAUD-DTE-YYYYMMDD-NUM GT 20031231
            if (condition(!(pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyy.equals(iaa_Parm_Card_Pnd_Parm_Date_Yyyy))))                                                      //Natural: ACCEPT IF #PAUD-DTE-YYYY = #PARM-DATE-YYYY
            {
                continue;
            }
            if (condition(iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr.equals(getZero())))                                                                             //Natural: IF PAUDIT-REMAIN-OVRPYMNT-TO-RCVR = 0
            {
                //*      AND
                //*    PAUDIT-OVR-AMT-THIS-PYE =  0
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-IAA-DC-OVRPYMNT
            sub_Read_Iaa_Dc_Ovrpymnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Bypass_This_Cntrct.equals("Y")))                                                                                                            //Natural: IF #BYPASS-THIS-CNTRCT = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Frst_Read.equals("Y")))                                                                                                                     //Natural: IF #FRST-READ = 'Y'
            {
                pnd_Frst_Read.setValue("N");                                                                                                                              //Natural: ASSIGN #FRST-READ := 'N'
                pnd_Sve_Dc_Pin.setValue(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr);                                                                                                  //Natural: ASSIGN #SVE-DC-PIN := PAUDIT-ID-NBR
                pnd_Dcx.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #DCX
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered.getValue(pnd_Dcx).compute(new ComputeParameters(false, pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered.getValue(pnd_Dcx)),    //Natural: ASSIGN #DC-TOT-RCOVERED ( #DCX ) := PAUDIT-OVR-AMT-THIS-PYE - PAUDIT-REMAIN-OVRPYMNT-TO-RCVR
                    iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye.subtract(iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr));
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Id_Nbr.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr);                                                               //Natural: ASSIGN #DC-ID-NBR ( #DCX ) := PAUDIT-ID-NBR
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key);                                                    //Natural: ASSIGN #DC-CNTRCT-PAYE ( #DCX ) := PAUDIT-PPCN-PYE-KEY
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr);                                                         //Natural: ASSIGN #DC-CNTRCT-NBR ( #DCX ) := PAUDIT-PPCN-NBR
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Payee_Cde);                                                             //Natural: ASSIGN #DC-PAYEE ( #DCX ) := PAUDIT-PAYEE-CDE
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Oper_Id);                                                             //Natural: ASSIGN #DC-OPER-ID ( #DCX ) := PAUDIT-OPER-ID
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Nbr);                                                      //Natural: ASSIGN #DC-TAX-ID ( #DCX ) := PAUDIT-PYEE-TAX-ID-NBR
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Name.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Name.getValue(1));                                                       //Natural: ASSIGN #DC-NAME ( #DCX ) := PAUDIT-NAME ( 1 )
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ovrpymnt.getValue(1));                                      //Natural: ASSIGN #DC-PER-AMT ( #DCX ) := PAUDIT-INSTLLMNT-OVRPYMNT ( 1 )
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye);                                                   //Natural: ASSIGN #DC-TOT-OVER ( #DCX ) := PAUDIT-OVR-AMT-THIS-PYE
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr);                                         //Natural: ASSIGN #DC-REM-OVR-PMT ( #DCX ) := PAUDIT-REMAIN-OVRPYMNT-TO-RCVR
                pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Yyyymm);                                                  //Natural: ASSIGN #DC-PYMNT-DTE ( #DCX ) := PAUDIT-PYMNT-DTE-YYYYMM
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sve_Dc_Pin.equals(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr)))                                                                                         //Natural: IF #SVE-DC-PIN = PAUDIT-ID-NBR
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  PROCESS RECORDS FOUND ON DC
                                                                                                                                                                          //Natural: PERFORM READ-CPR
                sub_Read_Cpr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fnd_Actv_Cpr.equals("Y")))                                                                                                              //Natural: IF #FND-ACTV-CPR = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DEDUCTION
                    sub_Process_Deduction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fnd_Actv_Cpr.reset();                                                                                                                                 //Natural: RESET #FND-ACTV-CPR
                pnd_Sve_Dc_Pin.setValue(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr);                                                                                                  //Natural: ASSIGN #SVE-DC-PIN := PAUDIT-ID-NBR
                pnd_Dcx.reset();                                                                                                                                          //Natural: RESET #DCX
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dcx.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #DCX
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered.getValue(pnd_Dcx).compute(new ComputeParameters(false, pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered.getValue(pnd_Dcx)),        //Natural: ASSIGN #DC-TOT-RCOVERED ( #DCX ) := PAUDIT-OVR-AMT-THIS-PYE - PAUDIT-REMAIN-OVRPYMNT-TO-RCVR
                iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye.subtract(iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr));
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Id_Nbr.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr);                                                                   //Natural: ASSIGN #DC-ID-NBR ( #DCX ) := PAUDIT-ID-NBR
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ppcn_Pye_Key);                                                        //Natural: ASSIGN #DC-CNTRCT-PAYE ( #DCX ) := PAUDIT-PPCN-PYE-KEY
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr);                                                             //Natural: ASSIGN #DC-CNTRCT-NBR ( #DCX ) := PAUDIT-PPCN-NBR
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Payee_Cde);                                                                 //Natural: ASSIGN #DC-PAYEE ( #DCX ) := PAUDIT-PAYEE-CDE
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Oper_Id);                                                                 //Natural: ASSIGN #DC-OPER-ID ( #DCX ) := PAUDIT-OPER-ID
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Pyee_Tax_Id_Nbr);                                                          //Natural: ASSIGN #DC-TAX-ID ( #DCX ) := PAUDIT-PYEE-TAX-ID-NBR
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Name.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Name.getValue(1));                                                           //Natural: ASSIGN #DC-NAME ( #DCX ) := PAUDIT-NAME ( 1 )
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Instllmnt_Ovrpymnt.getValue(1));                                          //Natural: ASSIGN #DC-PER-AMT ( #DCX ) := PAUDIT-INSTLLMNT-OVRPYMNT ( 1 )
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Ovr_Amt_This_Pye);                                                       //Natural: ASSIGN #DC-TOT-OVER ( #DCX ) := PAUDIT-OVR-AMT-THIS-PYE
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Remain_Ovrpymnt_To_Rcvr);                                             //Natural: ASSIGN #DC-REM-OVR-PMT ( #DCX ) := PAUDIT-REMAIN-OVRPYMNT-TO-RCVR
            pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte.getValue(pnd_Dcx).setValue(iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte_Yyyymm);                                                      //Natural: ASSIGN #DC-PYMNT-DTE ( #DCX ) := PAUDIT-PYMNT-DTE-YYYYMM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Rpt_Written.notEquals("Y")))                                                                                                                    //Natural: IF #RPT-WRITTEN NE 'Y'
        {
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"****************************************");                                                 //Natural: WRITE ( 1 ) 10X '****************************************'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"*** NO TRANSACTIONS IN THIS RUN *******");                                                  //Natural: WRITE ( 1 ) 10X '*** NO TRANSACTIONS IN THIS RUN *******'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(10),"****************************************");                                                 //Natural: WRITE ( 1 ) 10X '****************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ==> READ CPR RECORD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CPR
        //* *
        //*  READ IAA-DC-OVRPYMNT CHECK IF OVERPAYMENT AMOUNT HAS BEEN RESOVLED
        //* *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-DC-OVRPYMNT
        //* * =>PROCESS PMT RELEASE RECORDS READ DEDUCTIONS FOR OVR-PMT DED
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DEDUCTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DEDUCTION
        //*  ADDED FOLLOWING 11/04
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-DED-TRANS
        //*  #CNTRCT-HEX  :=  #W-HEX-HIGH-VALUE
    }
    //*    WRITE '=' #DC-CNTRCT-NBR (#I)
    //*    WRITE '=' #DC-PAYEE      (#I)
    //*    WRITE '='      IAA-DDCTN-TRANS.DDCTN-PPCN-NBR
    //*      '='  IAA-DDCTN-TRANS.DDCTN-PAYEE-CDE
    //*      '=' IAA-DDCTN-TRANS.DDCTN-CDE
    //*      '=' IAA-DDCTN-TRANS.DDCTN-CDE
    //*      '='   IAA-DDCTN-TRANS.DDCTN-STP-DTE
    //*      '=' #FND-DED-SW
    private void sub_Read_Cpr() throws Exception                                                                                                                          //Natural: READ-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fnd_Actv_Cpr.reset();                                                                                                                                         //Natural: RESET #FND-ACTV-CPR
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_Dcx));                                                         //Natural: ASSIGN #CNTRCT-PPCN-NBR := #DC-CNTRCT-NBR ( #DCX )
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_Dcx));                                                             //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #DC-PAYEE ( #DCX )
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") },
        1
        );
        F1:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("F1")))
        {
            //*    ACCEPT IF  CNTRCT-ACTVTY-CDE =  1
            if (condition(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr) || pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.notEquals(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde))) //Natural: IF #CNTRCT-PPCN-NBR NE CNTRCT-PART-PPCN-NBR OR #CNTRCT-PAYEE-CDE NE CNTRCT-PART-PAYEE-CDE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(1)))                                                                                           //Natural: IF CNTRCT-ACTVTY-CDE = 1
            {
                pnd_Fnd_Actv_Cpr.setValue("Y");                                                                                                                           //Natural: ASSIGN #FND-ACTV-CPR := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Dc_Ovrpymnt() throws Exception                                                                                                              //Natural: READ-IAA-DC-OVRPYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bypass_This_Cntrct.reset();                                                                                                                                   //Natural: RESET #BYPASS-THIS-CNTRCT
        pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Id_Nbr.setValue(iaa_Dc_Pmt_Audit_Paudit_Id_Nbr);                                                                                 //Natural: ASSIGN #OVRPYMNT-ID-NBR := PAUDIT-ID-NBR
        pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Tax_Id_Nbr.setValue(iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr);                                                                         //Natural: ASSIGN #OVRPYMNT-TAX-ID-NBR := PAUDIT-TAX-ID-NBR
        //*     #OVRPYMNT-REQ-SEQ-NBR (N3)
        //*     #OVRPYMNT-PPCN-NBR    (A8)
        //*     #OVRPYMNT-PAYEE-CDE   (N2)
        vw_iaa_Dc_Ovrpymnt.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) IAA-DC-OVRPYMNT BY PIN-TAXID-SEQ-PPCN-PYE-KEY FROM #DC-OVRPYMNT-KEY
        (
        "READ02",
        new Wc[] { new Wc("PIN_TAXID_SEQ_PPCN_PYE_KEY", ">=", pnd_Dc_Ovrpymnt_Key, WcType.BY) },
        new Oc[] { new Oc("PIN_TAXID_SEQ_PPCN_PYE_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Dc_Ovrpymnt.readNextRow("READ02")))
        {
            //*   MOVE EDITED OVRPYMNT-TIMESTAMP (EM=YYYYMMDD)
            //*     TO  #PAUD-DTE-YYYYMMDD-ALPH
            if (condition(!(pnd_Paud_Dte_Yyyymmdd_Alph_Pnd_Paud_Dte_Yyyy.equals(iaa_Parm_Card_Pnd_Parm_Date_Yyyy))))                                                      //Natural: ACCEPT IF #PAUD-DTE-YYYY = #PARM-DATE-YYYY
            {
                continue;
            }
            if (condition(iaa_Dc_Ovrpymnt_Ovrpymnt_Id_Nbr.notEquals(pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Id_Nbr) || iaa_Dc_Ovrpymnt_Ovrpymnt_Tax_Id_Nbr.notEquals(pnd_Dc_Ovrpymnt_Key_Pnd_Ovrpymnt_Tax_Id_Nbr))) //Natural: IF OVRPYMNT-ID-NBR NE #OVRPYMNT-ID-NBR OR OVRPYMNT-TAX-ID-NBR NE #OVRPYMNT-TAX-ID-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Dc_Ovrpymnt_Ovrpymnt_Grand_Ttl_Ovr_Amt.equals(getZero())))                                                                                  //Natural: IF OVRPYMNT-GRAND-TTL-OVR-AMT = 0
            {
                pnd_Bypass_This_Cntrct.setValue("Y");                                                                                                                     //Natural: ASSIGN #BYPASS-THIS-CNTRCT := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Deduction() throws Exception                                                                                                                 //Natural: PROCESS-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #DCX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Dcx)); pnd_I.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM READ-DEDUCTION
            sub_Read_Deduction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Fnd_Ded_Sw.notEquals("Y")))                                                                                                                 //Natural: IF #FND-DED-SW NE 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM READ-IAA-DED-TRANS
                sub_Read_Iaa_Ded_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Fnd_Ded_Sw.notEquals("Y")))                                                                                                             //Natural: IF #FND-DED-SW NE 'Y'
                {
                    pnd_Rpt_Written.setValue("Y");                                                                                                                        //Natural: ASSIGN #RPT-WRITTEN := 'Y'
                    getReports().display(1, ReportOption.NOTITLE,"Surviving/Annuitant/Beneficiary/Name",                                                                  //Natural: DISPLAY ( 1 ) NOTITLE 'Surviving/Annuitant/Beneficiary/Name' #DC-NAME ( #I ) 'Surviving/Annuitant/Beneficiary/SSN' #DC-TAX-ID ( #I ) ( EM = 999-99-9999 ) '///Cntract Py ' #DC-CNTRCT-PAYE ( #I ) '//Last-Ded/Date' #DC-PYMNT-DTE ( #I ) ( EM = 9999/99 ) '/Deduction/Amount Per/Payment' #DC-PER-AMT ( #I ) ( EM = ZZ,ZZZ.99 ) '/Total/Overpayment/Amount' #DC-TOT-OVER ( #I ) ( EM = Z,ZZZ,ZZZ.99 ) 'Total/OverPayment/Amount/Recovered' #DC-TOT-RCOVERED ( #I ) ( EM = Z,ZZZ,ZZZ.99 ) '/Remaining/Overpayment/Amount' #DC-REM-OVR-PMT ( #I ) ( EM = Z,ZZZ,ZZZ.99 ) '//Associate/Name' #DC-OPER-ID ( #I )
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Name.getValue(pnd_I),"Surviving/Annuitant/Beneficiary/SSN",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id.getValue(pnd_I), new ReportEditMask ("999-99-9999"),"///Cntract Py ",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye.getValue(pnd_I),"//Last-Ded/Date",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte.getValue(pnd_I), new ReportEditMask ("9999/99"),"/Deduction/Amount Per/Payment",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ.99"),"/Total/Overpayment/Amount",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Total/OverPayment/Amount/Recovered",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ.99"),"/Remaining/Overpayment/Amount",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ.99"),"//Associate/Name",
                    		pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id.getValue(pnd_I));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Deduction() throws Exception                                                                                                                    //Natural: READ-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        //*   READ DEDUCTION FILE
        //* *****************************************************
        pnd_Fnd_Ded_Sw.reset();                                                                                                                                           //Natural: RESET #FND-DED-SW
        pnd_Ded_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_I));                                                                     //Natural: ASSIGN #DDCTN-PPCN-NBR := #DC-CNTRCT-NBR ( #I )
        pnd_Ded_Key_Pnd_Ddctn_Payee_Cde.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_I));                                                                         //Natural: ASSIGN #DDCTN-PAYEE-CDE := #DC-PAYEE ( #I )
        pnd_Ded_Key_Pnd_Ddctn_Seq_Nbr.setValue(1);                                                                                                                        //Natural: ASSIGN #DDCTN-SEQ-NBR := #DDCTN-CDE := 001
        pnd_Ded_Key_Pnd_Ddctn_Cde.setValue(1);
        vw_iaa_Deduction.startDatabaseRead                                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #DED-KEY
        (
        "READ_FILE",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Ded_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ_FILE:
        while (condition(vw_iaa_Deduction.readNextRow("READ_FILE")))
        {
            if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.notEquals(pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_I)) || iaa_Deduction_Ddctn_Payee_Cde.notEquals(pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_I)))) //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR NE #DC-CNTRCT-NBR ( #I ) OR IAA-DEDUCTION.DDCTN-PAYEE-CDE NE #DC-PAYEE ( #I )
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Deduction_Ddctn_Cde.notEquals("001")))                                                                                                      //Natural: IF IAA-DEDUCTION.DDCTN-CDE NE '001'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fnd_Ded_Sw.setValue("Y");                                                                                                                                 //Natural: ASSIGN #FND-DED-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Ded_Trans() throws Exception                                                                                                                //Natural: READ-IAA-DED-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fnd_Ded_Sw.reset();                                                                                                                                           //Natural: RESET #FND-DED-SW
        ddctn_Bfre_Key_Bfr_Id.setValue(2);                                                                                                                                //Natural: ASSIGN BFR-ID := 2
        ddctn_Bfre_Key_Pnd_Cntrct_Nbr.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_I));                                                                      //Natural: ASSIGN #CNTRCT-NBR := #DC-CNTRCT-NBR ( #I )
        ddctn_Bfre_Key_Pnd_Cntrct_Payee.setValue(pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_I));                                                                         //Natural: ASSIGN #CNTRCT-PAYEE := #DC-PAYEE ( #I )
        ddctn_Bfre_Key_Pnd_Ded_Seq_Cde.setValue("000000");                                                                                                                //Natural: ASSIGN #DED-SEQ-CDE := '000000'
        vw_iaa_Ddctn_Trans.startDatabaseRead                                                                                                                              //Natural: READ IAA-DDCTN-TRANS BY DDCTN-AFTR-KEY STARTING FROM DDCTN-BFRE-KEY
        (
        "READ03",
        new Wc[] { new Wc("DDCTN_AFTR_KEY", ">=", ddctn_Bfre_Key, WcType.BY) },
        new Oc[] { new Oc("DDCTN_AFTR_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Ddctn_Trans.readNextRow("READ03")))
        {
            if (condition(iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr.greater(ddctn_Bfre_Key_Pnd_Cntrct_Nbr)))                                                                         //Natural: IF IAA-DDCTN-TRANS.DDCTN-PPCN-NBR GT #CNTRCT-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr.equals(pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Nbr.getValue(pnd_I)) && iaa_Ddctn_Trans_Ddctn_Payee_Cde.equals(pnd_Dc_Cntrct_Tab_Pnd_Dc_Payee.getValue(pnd_I))  //Natural: IF IAA-DDCTN-TRANS.DDCTN-PPCN-NBR = #DC-CNTRCT-NBR ( #I ) AND IAA-DDCTN-TRANS.DDCTN-PAYEE-CDE = #DC-PAYEE ( #I ) AND IAA-DDCTN-TRANS.DDCTN-CDE = '001' AND IAA-DDCTN-TRANS.DDCTN-STP-DTE GT 0
                && iaa_Ddctn_Trans_Ddctn_Cde.equals("001") && iaa_Ddctn_Trans_Ddctn_Stp_Dte.greater(getZero())))
            {
                pnd_Fnd_Ded_Sw.setValue("Y");                                                                                                                             //Natural: ASSIGN #FND-DED-SW := 'Y'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"                  Recovery Greater than Receivables ",new  //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X '                  Recovery Greater than Receivables ' 10X 'PAGE: ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 )
                        ColumnSpacing(10),"PAGE: ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,"Run Date: ",Global.getDATU(),NEWLINE,NEWLINE);                                                            //Natural: WRITE ( 1 ) 'Run Date: ' *DATU //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=56");
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"Surviving/Annuitant/Beneficiary/Name",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Name,"Surviving/Annuitant/Beneficiary/SSN",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tax_Id, new ReportEditMask ("999-99-9999"),"///Cntract Py ",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Cntrct_Paye,"//Last-Ded/Date",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Pymnt_Dte, new ReportEditMask ("9999/99"),"/Deduction/Amount Per/Payment",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Per_Amt, new ReportEditMask ("ZZ,ZZZ.99"),"/Total/Overpayment/Amount",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Over, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"Total/OverPayment/Amount/Recovered",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Tot_Rcovered, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"/Remaining/Overpayment/Amount",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Rem_Ovr_Pmt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"//Associate/Name",
        		pnd_Dc_Cntrct_Tab_Pnd_Dc_Oper_Id);
    }
}
