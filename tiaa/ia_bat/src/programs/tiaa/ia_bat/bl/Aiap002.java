/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:56 PM
**        * FROM NATURAL PROGRAM : Aiap002
************************************************************
**        * FILE NAME            : Aiap002.java
**        * CLASS NAME           : Aiap002
**        * INSTANCE NAME        : Aiap002
************************************************************
*
* AIAP002 - CREF/REA IA TRANSFER SUMMARY
*  VERSION 1.9 - 11/25/98
* 07/10/08  KCD  ROTH/99 RATES IMPLEMENTATION
************************************************************************
* 04/15/2009 OS TIAA ACCESS CHANGES. SC 041509.
* 10/18/2010 OS PROD FIX. STANDARD IS 'T' AND GRADED IS '2'. SC 101810
* 10-08-2012 DY RBE PHASE 2 PROJECT; REF AS RBE2
* 01-10-2013 DY ADD GRAND TOTAL TO THE "EFFECTIVE DATE ALL" ASSET
*                 TRANSFER SUMMARY REPORT PAGE; REF AS DY1
* 04-02-2013 DY FIX ARRAY INDEX PROBLEM WITH SWITCHES; REF AS DY2
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aiap002 extends BLNatBase
{
    // Data Areas
    private LdaAial0131 ldaAial0131;
    private LdaAial0132 ldaAial0132;
    private LdaAial0133 ldaAial0133;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField cntrl_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_Begin;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_1;
    private DbsField pnd_Input_Record_Pnd_Input_Contract_Payee_End;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_2;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Date_A;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Year;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Month;
    private DbsField pnd_Input_Record_Pnd_Input_Begin_Day;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_3;
    private DbsField pnd_Input_Record_Pnd_Input_End_Date;
    private DbsField pnd_Input_Record_Pnd_Input_Filler_4;

    private DbsGroup pnd_Aian013b_Record;
    private DbsField pnd_Aian013b_Record_Pnd_Contract_Payee;

    private DbsGroup pnd_Aian013b_Record__R_Field_4;
    private DbsField pnd_Aian013b_Record_Pnd_Contract_Number;

    private DbsGroup pnd_Aian013b_Record__R_Field_5;
    private DbsField pnd_Aian013b_Record_Pnd_Ppcn_7;
    private DbsField pnd_Aian013b_Record_Pnd_Ppcn_8;
    private DbsField pnd_Aian013b_Record_Pnd_Payee_Code;
    private DbsField pnd_Aian013b_Record_Pnd_Fund_Code;
    private DbsField pnd_Aian013b_Record_Pnd_Mode;

    private DbsGroup pnd_Aian013b_Record__R_Field_6;
    private DbsField pnd_Aian013b_Record_Pnd_Mode_1;
    private DbsField pnd_Aian013b_Record_Pnd_Filler1;
    private DbsField pnd_Aian013b_Record_Pnd_Option;
    private DbsField pnd_Aian013b_Record_Pnd_Issue_Date;

    private DbsGroup pnd_Aian013b_Record__R_Field_7;
    private DbsField pnd_Aian013b_Record_Pnd_Issue_Year;
    private DbsField pnd_Aian013b_Record_Pnd_Issue_Month;
    private DbsField pnd_Aian013b_Record_Pnd_Final_Per_Pay_Date;

    private DbsGroup pnd_Aian013b_Record__R_Field_8;
    private DbsField pnd_Aian013b_Record_Pnd_Final_Per_Pay_Year;
    private DbsField pnd_Aian013b_Record_Pnd_Final_Per_Pay_Month;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dob;

    private DbsGroup pnd_Aian013b_Record__R_Field_9;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dob_Year;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dob_Month;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dob_Day;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Sex;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dod;

    private DbsGroup pnd_Aian013b_Record__R_Field_10;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dod_Year;
    private DbsField pnd_Aian013b_Record_Pnd_First_Ann_Dod_Month;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dob;

    private DbsGroup pnd_Aian013b_Record__R_Field_11;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Year;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Month;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Day;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Sex;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dod;

    private DbsGroup pnd_Aian013b_Record__R_Field_12;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Year;
    private DbsField pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Month;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Effective_Date;

    private DbsGroup pnd_Aian013b_Record__R_Field_13;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Eff_Year;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Eff_Month;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Eff_Day;
    private DbsField pnd_Aian013b_Record_Pnd_Type_Of_Run;
    private DbsField pnd_Aian013b_Record_Pnd_Processing_Date;
    private DbsField pnd_Aian013b_Record_Pnd_Illustration_Eff_Date;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Units;

    private DbsGroup pnd_Aian013b_Record_Pnd_Fund_To_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Fund_To_Receive;

    private DbsGroup pnd_Aian013b_Record_Pnd_Units_To_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Units_To_Receive;

    private DbsGroup pnd_Aian013b_Record_Pnd_Pmts_To_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Pmts_To_Receive;
    private DbsField pnd_Aian013b_Record_Pnd_Return_Code;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Eff_Date_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Fund_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Fund_Code_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Units_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Units_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Auv_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Auv_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Transfer_Amt_Cref_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref;

    private DbsGroup pnd_Aian013b_Record_Pnd_Pmt_Method_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Pmt_Method_Code_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Rate_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Rate_Code_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Gtd_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Gtd_Pmt_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Dvd_Out_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Dvd_Pmt_Out;

    private DbsGroup pnd_Aian013b_Record_Pnd_Transfer_Amt_Tiaa_Array;
    private DbsField pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Tiaa;
    private DbsField pnd_Aian013b_Record_Pnd_Total_Transfer_Amt_Out;
    private DbsField pnd_Transfer_Amt_Array_Tot;
    private DbsField pnd_Total_Pmts_Out;
    private DbsField pnd_Total_Units_Out;
    private DbsField pnd_Pmts_Out;
    private DbsField pnd_Cntrl_Sd_Cde;
    private DbsField pnd_Date1;
    private DbsField pnd_Nonzero_Switch;
    private DbsField pnd_Prt_Msg;
    private DbsField pnd_Reval_Switch;

    private DbsGroup pnd_Eff_Date_Array;
    private DbsField pnd_Eff_Date_Array_Pnd_Eff_Date;
    private DbsField pnd_Save_Eff_Date;

    private DbsGroup pnd_Save_Eff_Date__R_Field_14;
    private DbsField pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X;
    private DbsField pnd_Date_Count;
    private DbsField pnd_Eof_Flag;
    private DbsField pnd_First_Return;
    private DbsField pnd_First_Sort;
    private DbsField pnd_Save_Transfer_Eff_Date;
    private DbsField pnd_Prt_Transfer_Eff_Date;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Revaluation_Ind_Out;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Print_Ppcn;

    private DbsGroup pnd_Print_Ppcn__R_Field_15;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_7;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Dash;
    private DbsField pnd_Print_Ppcn_Pnd_Print_Ppcn_8;
    private DbsField pnd_Print_Fund;
    private DbsField pnd_Print_Fund_Out;

    private DbsGroup pnd_Fund_Name_Array;
    private DbsField pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Stock_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Mma_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Social_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Bond_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Global_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Growth_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Index_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Ilb_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Rea_Name;
    private DbsField pnd_Fund_Name_Array_Pnd_Acc_Name;

    private DbsGroup pnd_Fund_Name_Array__R_Field_16;

    private DbsGroup pnd_Fund_Name_Array_Pnd_Filler9;
    private DbsField pnd_Fund_Name_Array_Pnd_Fund_Name;

    private DbsGroup pnd_Fund_Letter_Array;
    private DbsField pnd_Fund_Letter_Array_Pnd_Std_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Grd_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Stock_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Mma_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Social_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Bond_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Global_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Growth_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Index_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Ilb_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Rea_Letter;
    private DbsField pnd_Fund_Letter_Array_Pnd_Acc_Letter;

    private DbsGroup pnd_Fund_Letter_Array__R_Field_17;

    private DbsGroup pnd_Fund_Letter_Array_Pnd_Filler;
    private DbsField pnd_Fund_Letter_Array_Pnd_Fund_Letter;
    private DbsField pnd_Fund_Input_Pos;
    private DbsField pnd_Fund_Output_Pos;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A;

    private DbsGroup pnd_Transfer_Tiaa_By_Method;
    private DbsField pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa;

    private DbsGroup pnd_Transfer_Num_Mm;

    private DbsGroup pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_In_Mm;

    private DbsGroup pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_Out_Mm;
    private DbsField pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Ma;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Transfer_Num_By_Fund_Out_Ma;
    private DbsField pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Am;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Am_Pnd_Transfer_Num_By_Fund_Out_Am;
    private DbsField pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Aa;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Transfer_Num_By_Fund_Out_Aa;
    private DbsField pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa;

    private DbsGroup pnd_Tot_Transfer_Num_Mm;

    private DbsGroup pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Transfer_Num_By_Fund_In_Mm;

    private DbsGroup pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Trans_Num_By_Fund_Out_Mm;
    private DbsField pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Num_Transfers_Mm;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Ma;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Trans_Num_By_Fund_Out_Ma;
    private DbsField pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Num_Transfers_Ma;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Am;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Trans_Num_By_Fund_Out_Am;
    private DbsField pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Num_Transfers_Am;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Aa;

    private DbsGroup pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Transfer_Num_By_Fund_Out_Aa;
    private DbsField pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Num_Transfers_Aa;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Tiaa;

    private DbsGroup pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Transfer_Num_By_Fund_Out_Tiaa;
    private DbsField pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa;

    private DbsGroup pnd_Transfer_Num;

    private DbsGroup pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_In;

    private DbsGroup pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_Out;
    private DbsField pnd_Transfer_Num_Pnd_Num_Transfers;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Mm;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_By_Fund_Out_Mm;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Ma;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_By_Fund_Out_Ma;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Am;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_By_Fund_Out_Am;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Aa;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_By_Fund_Out_Aa;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Mm;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Trans_Amt_By_Fund_Out_Mm;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Transfer_Amt_Array_Mm;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Ma;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Trans_Amt_By_Fund_Out_Ma;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Transfer_Amt_Array_Ma;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Am;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Trans_Amt_By_Fund_Out_Am;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Transfer_Amt_Array_Am;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Aa;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Trans_Amt_By_Fund_Out_Aa;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Transfer_Amt_Array_Aa;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Tiaa;

    private DbsGroup pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_By_Fund_Out_Tiaa;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A;
    private DbsField pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Tiaa;

    private DbsGroup pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Tran_Amt_By_Fund_Out_Tiaa;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_A;
    private DbsField pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_M;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number;

    private DbsGroup pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_19;
    private DbsField pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2;
    private DbsField pnd_Net_Units;
    private DbsField pnd_Net_Pmts;
    private DbsField pnd_Prt_Switch;
    private DbsField pnd_Num_Lines;
    private DbsField pnd_Tiaa_Pos;
    private DbsField pnd_Cref_Rea_Pos;
    private DbsField pnd_Pmt_Index;
    private DbsField pnd_Total_Num_Transfers;
    private DbsField pnd_Grand_Total_Num_Transfers;
    private DbsField pnd_First_Write_Switch;
    private DbsField pnd_Grand_Total_Switch;
    private DbsField pnd_Net_Trans_Reval;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial0131 = new LdaAial0131();
        registerRecord(ldaAial0131);
        registerRecord(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2());
        ldaAial0132 = new LdaAial0132();
        registerRecord(ldaAial0132);
        ldaAial0133 = new LdaAial0133();
        registerRecord(ldaAial0133);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        cntrl_Cntrl_Next_Bus_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_cntrl);

        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 80);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Filler_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 4);
        pnd_Input_Record_Pnd_Input_Contract_Payee_Begin = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_Begin", 
            "#INPUT-CONTRACT-PAYEE-BEGIN", FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_1 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_1", "#INPUT-FILLER-1", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Contract_Payee_End = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Contract_Payee_End", "#INPUT-CONTRACT-PAYEE-END", 
            FieldType.STRING, 12);
        pnd_Input_Record_Pnd_Input_Filler_2 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_2", "#INPUT-FILLER-2", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_Begin_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date", "#INPUT-BEGIN-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Date_A = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Date_A", "#INPUT-BEGIN-DATE-A", 
            FieldType.STRING, 8);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Input_Begin_Date);
        pnd_Input_Record_Pnd_Input_Begin_Year = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Year", "#INPUT-BEGIN-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Input_Record_Pnd_Input_Begin_Month = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Month", "#INPUT-BEGIN-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Begin_Day = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Input_Begin_Day", "#INPUT-BEGIN-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Input_Record_Pnd_Input_Filler_3 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_3", "#INPUT-FILLER-3", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Input_End_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_End_Date", "#INPUT-END-DATE", FieldType.NUMERIC, 
            8);
        pnd_Input_Record_Pnd_Input_Filler_4 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Input_Filler_4", "#INPUT-FILLER-4", FieldType.STRING, 
            33);

        pnd_Aian013b_Record = localVariables.newGroupInRecord("pnd_Aian013b_Record", "#AIAN013B-RECORD");
        pnd_Aian013b_Record_Pnd_Contract_Payee = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Contract_Payee", "#CONTRACT-PAYEE", FieldType.STRING, 
            10);

        pnd_Aian013b_Record__R_Field_4 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_4", "REDEFINE", pnd_Aian013b_Record_Pnd_Contract_Payee);
        pnd_Aian013b_Record_Pnd_Contract_Number = pnd_Aian013b_Record__R_Field_4.newFieldInGroup("pnd_Aian013b_Record_Pnd_Contract_Number", "#CONTRACT-NUMBER", 
            FieldType.STRING, 8);

        pnd_Aian013b_Record__R_Field_5 = pnd_Aian013b_Record__R_Field_4.newGroupInGroup("pnd_Aian013b_Record__R_Field_5", "REDEFINE", pnd_Aian013b_Record_Pnd_Contract_Number);
        pnd_Aian013b_Record_Pnd_Ppcn_7 = pnd_Aian013b_Record__R_Field_5.newFieldInGroup("pnd_Aian013b_Record_Pnd_Ppcn_7", "#PPCN-7", FieldType.STRING, 
            7);
        pnd_Aian013b_Record_Pnd_Ppcn_8 = pnd_Aian013b_Record__R_Field_5.newFieldInGroup("pnd_Aian013b_Record_Pnd_Ppcn_8", "#PPCN-8", FieldType.STRING, 
            1);
        pnd_Aian013b_Record_Pnd_Payee_Code = pnd_Aian013b_Record__R_Field_4.newFieldInGroup("pnd_Aian013b_Record_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Aian013b_Record_Pnd_Fund_Code = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Fund_Code", "#FUND-CODE", FieldType.STRING, 1);
        pnd_Aian013b_Record_Pnd_Mode = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Aian013b_Record__R_Field_6 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_6", "REDEFINE", pnd_Aian013b_Record_Pnd_Mode);
        pnd_Aian013b_Record_Pnd_Mode_1 = pnd_Aian013b_Record__R_Field_6.newFieldInGroup("pnd_Aian013b_Record_Pnd_Mode_1", "#MODE-1", FieldType.NUMERIC, 
            1);
        pnd_Aian013b_Record_Pnd_Filler1 = pnd_Aian013b_Record__R_Field_6.newFieldInGroup("pnd_Aian013b_Record_Pnd_Filler1", "#FILLER1", FieldType.NUMERIC, 
            2);
        pnd_Aian013b_Record_Pnd_Option = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Option", "#OPTION", FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Issue_Date = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Issue_Date", "#ISSUE-DATE", FieldType.NUMERIC, 
            6);

        pnd_Aian013b_Record__R_Field_7 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_7", "REDEFINE", pnd_Aian013b_Record_Pnd_Issue_Date);
        pnd_Aian013b_Record_Pnd_Issue_Year = pnd_Aian013b_Record__R_Field_7.newFieldInGroup("pnd_Aian013b_Record_Pnd_Issue_Year", "#ISSUE-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Aian013b_Record_Pnd_Issue_Month = pnd_Aian013b_Record__R_Field_7.newFieldInGroup("pnd_Aian013b_Record_Pnd_Issue_Month", "#ISSUE-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Aian013b_Record_Pnd_Final_Per_Pay_Date = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Final_Per_Pay_Date", "#FINAL-PER-PAY-DATE", 
            FieldType.NUMERIC, 6);

        pnd_Aian013b_Record__R_Field_8 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_8", "REDEFINE", pnd_Aian013b_Record_Pnd_Final_Per_Pay_Date);
        pnd_Aian013b_Record_Pnd_Final_Per_Pay_Year = pnd_Aian013b_Record__R_Field_8.newFieldInGroup("pnd_Aian013b_Record_Pnd_Final_Per_Pay_Year", "#FINAL-PER-PAY-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_Final_Per_Pay_Month = pnd_Aian013b_Record__R_Field_8.newFieldInGroup("pnd_Aian013b_Record_Pnd_Final_Per_Pay_Month", "#FINAL-PER-PAY-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_First_Ann_Dob = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 
            8);

        pnd_Aian013b_Record__R_Field_9 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_9", "REDEFINE", pnd_Aian013b_Record_Pnd_First_Ann_Dob);
        pnd_Aian013b_Record_Pnd_First_Ann_Dob_Year = pnd_Aian013b_Record__R_Field_9.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dob_Year", "#FIRST-ANN-DOB-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_First_Ann_Dob_Month = pnd_Aian013b_Record__R_Field_9.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dob_Month", "#FIRST-ANN-DOB-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_First_Ann_Dob_Day = pnd_Aian013b_Record__R_Field_9.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dob_Day", "#FIRST-ANN-DOB-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_First_Ann_Sex = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Sex", "#FIRST-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Aian013b_Record_Pnd_First_Ann_Dod = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.NUMERIC, 
            6);

        pnd_Aian013b_Record__R_Field_10 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_10", "REDEFINE", pnd_Aian013b_Record_Pnd_First_Ann_Dod);
        pnd_Aian013b_Record_Pnd_First_Ann_Dod_Year = pnd_Aian013b_Record__R_Field_10.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dod_Year", "#FIRST-ANN-DOD-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_First_Ann_Dod_Month = pnd_Aian013b_Record__R_Field_10.newFieldInGroup("pnd_Aian013b_Record_Pnd_First_Ann_Dod_Month", "#FIRST-ANN-DOD-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dob = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dob", "#SECOND-ANN-DOB", FieldType.NUMERIC, 
            8);

        pnd_Aian013b_Record__R_Field_11 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_11", "REDEFINE", pnd_Aian013b_Record_Pnd_Second_Ann_Dob);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Year = pnd_Aian013b_Record__R_Field_11.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Year", "#SECOND-ANN-DOB-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Month = pnd_Aian013b_Record__R_Field_11.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Month", 
            "#SECOND-ANN-DOB-MONTH", FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Day = pnd_Aian013b_Record__R_Field_11.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dob_Day", "#SECOND-ANN-DOB-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Second_Ann_Sex = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Sex", "#SECOND-ANN-SEX", FieldType.NUMERIC, 
            1);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dod = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dod", "#SECOND-ANN-DOD", FieldType.NUMERIC, 
            6);

        pnd_Aian013b_Record__R_Field_12 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_12", "REDEFINE", pnd_Aian013b_Record_Pnd_Second_Ann_Dod);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Year = pnd_Aian013b_Record__R_Field_12.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Year", "#SECOND-ANN-DOD-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Month = pnd_Aian013b_Record__R_Field_12.newFieldInGroup("pnd_Aian013b_Record_Pnd_Second_Ann_Dod_Month", 
            "#SECOND-ANN-DOD-MONTH", FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Transfer_Effective_Date = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Effective_Date", "#TRANSFER-EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Aian013b_Record__R_Field_13 = pnd_Aian013b_Record.newGroupInGroup("pnd_Aian013b_Record__R_Field_13", "REDEFINE", pnd_Aian013b_Record_Pnd_Transfer_Effective_Date);
        pnd_Aian013b_Record_Pnd_Transfer_Eff_Year = pnd_Aian013b_Record__R_Field_13.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Eff_Year", "#TRANSFER-EFF-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Aian013b_Record_Pnd_Transfer_Eff_Month = pnd_Aian013b_Record__R_Field_13.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Eff_Month", "#TRANSFER-EFF-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Transfer_Eff_Day = pnd_Aian013b_Record__R_Field_13.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Eff_Day", "#TRANSFER-EFF-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Aian013b_Record_Pnd_Type_Of_Run = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Type_Of_Run", "#TYPE-OF-RUN", FieldType.STRING, 
            1);
        pnd_Aian013b_Record_Pnd_Processing_Date = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Processing_Date", "#PROCESSING-DATE", FieldType.NUMERIC, 
            8);
        pnd_Aian013b_Record_Pnd_Illustration_Eff_Date = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Illustration_Eff_Date", "#ILLUSTRATION-EFF-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Aian013b_Record_Pnd_Transfer_Units = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Units", "#TRANSFER-UNITS", FieldType.NUMERIC, 
            9, 3);

        pnd_Aian013b_Record_Pnd_Fund_To_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Fund_To_Array", "#FUND-TO-ARRAY", new 
            DbsArrayController(1, 23));
        pnd_Aian013b_Record_Pnd_Fund_To_Receive = pnd_Aian013b_Record_Pnd_Fund_To_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Fund_To_Receive", "#FUND-TO-RECEIVE", 
            FieldType.STRING, 1);

        pnd_Aian013b_Record_Pnd_Units_To_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Units_To_Array", "#UNITS-TO-ARRAY", 
            new DbsArrayController(1, 23));
        pnd_Aian013b_Record_Pnd_Units_To_Receive = pnd_Aian013b_Record_Pnd_Units_To_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Units_To_Receive", 
            "#UNITS-TO-RECEIVE", FieldType.NUMERIC, 9, 3);

        pnd_Aian013b_Record_Pnd_Pmts_To_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Pmts_To_Array", "#PMTS-TO-ARRAY", new 
            DbsArrayController(1, 23));
        pnd_Aian013b_Record_Pnd_Pmts_To_Receive = pnd_Aian013b_Record_Pnd_Pmts_To_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Pmts_To_Receive", "#PMTS-TO-RECEIVE", 
            FieldType.NUMERIC, 11, 2);
        pnd_Aian013b_Record_Pnd_Return_Code = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 
            2);
        pnd_Aian013b_Record_Pnd_Transfer_Eff_Date_Out = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Eff_Date_Out", "#TRANSFER-EFF-DATE-OUT", 
            FieldType.NUMERIC, 8);

        pnd_Aian013b_Record_Pnd_Fund_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Fund_Out_Array", "#FUND-OUT-ARRAY", 
            new DbsArrayController(1, 20));
        pnd_Aian013b_Record_Pnd_Fund_Code_Out = pnd_Aian013b_Record_Pnd_Fund_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Fund_Code_Out", "#FUND-CODE-OUT", 
            FieldType.STRING, 1);

        pnd_Aian013b_Record_Pnd_Units_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Units_Out_Array", "#UNITS-OUT-ARRAY", 
            new DbsArrayController(1, 20));
        pnd_Aian013b_Record_Pnd_Units_Out = pnd_Aian013b_Record_Pnd_Units_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Units_Out", "#UNITS-OUT", 
            FieldType.NUMERIC, 8, 3);

        pnd_Aian013b_Record_Pnd_Auv_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Auv_Out_Array", "#AUV-OUT-ARRAY", new 
            DbsArrayController(1, 20));
        pnd_Aian013b_Record_Pnd_Auv_Out = pnd_Aian013b_Record_Pnd_Auv_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Auv_Out", "#AUV-OUT", FieldType.NUMERIC, 
            8, 4);

        pnd_Aian013b_Record_Pnd_Transfer_Amt_Cref_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Transfer_Amt_Cref_Array", 
            "#TRANSFER-AMT-CREF-ARRAY", new DbsArrayController(1, 20));
        pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref = pnd_Aian013b_Record_Pnd_Transfer_Amt_Cref_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref", 
            "#TRANSFER-AMT-OUT-CREF", FieldType.NUMERIC, 13, 2);

        pnd_Aian013b_Record_Pnd_Pmt_Method_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Pmt_Method_Out_Array", "#PMT-METHOD-OUT-ARRAY", 
            new DbsArrayController(1, 2));
        pnd_Aian013b_Record_Pnd_Pmt_Method_Code_Out = pnd_Aian013b_Record_Pnd_Pmt_Method_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Pmt_Method_Code_Out", 
            "#PMT-METHOD-CODE-OUT", FieldType.STRING, 1);

        pnd_Aian013b_Record_Pnd_Rate_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Rate_Out_Array", "#RATE-OUT-ARRAY", 
            new DbsArrayController(1, 2));
        pnd_Aian013b_Record_Pnd_Rate_Code_Out = pnd_Aian013b_Record_Pnd_Rate_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Rate_Code_Out", "#RATE-CODE-OUT", 
            FieldType.NUMERIC, 2);

        pnd_Aian013b_Record_Pnd_Gtd_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Gtd_Out_Array", "#GTD-OUT-ARRAY", new 
            DbsArrayController(1, 2));
        pnd_Aian013b_Record_Pnd_Gtd_Pmt_Out = pnd_Aian013b_Record_Pnd_Gtd_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Gtd_Pmt_Out", "#GTD-PMT-OUT", 
            FieldType.NUMERIC, 9, 2);

        pnd_Aian013b_Record_Pnd_Dvd_Out_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Dvd_Out_Array", "#DVD-OUT-ARRAY", new 
            DbsArrayController(1, 2));
        pnd_Aian013b_Record_Pnd_Dvd_Pmt_Out = pnd_Aian013b_Record_Pnd_Dvd_Out_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Dvd_Pmt_Out", "#DVD-PMT-OUT", 
            FieldType.NUMERIC, 9, 2);

        pnd_Aian013b_Record_Pnd_Transfer_Amt_Tiaa_Array = pnd_Aian013b_Record.newGroupArrayInGroup("pnd_Aian013b_Record_Pnd_Transfer_Amt_Tiaa_Array", 
            "#TRANSFER-AMT-TIAA-ARRAY", new DbsArrayController(1, 2));
        pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Tiaa = pnd_Aian013b_Record_Pnd_Transfer_Amt_Tiaa_Array.newFieldInGroup("pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Tiaa", 
            "#TRANSFER-AMT-OUT-TIAA", FieldType.NUMERIC, 13, 2);
        pnd_Aian013b_Record_Pnd_Total_Transfer_Amt_Out = pnd_Aian013b_Record.newFieldInGroup("pnd_Aian013b_Record_Pnd_Total_Transfer_Amt_Out", "#TOTAL-TRANSFER-AMT-OUT", 
            FieldType.NUMERIC, 14, 2);
        pnd_Transfer_Amt_Array_Tot = localVariables.newFieldInRecord("pnd_Transfer_Amt_Array_Tot", "#TRANSFER-AMT-ARRAY-TOT", FieldType.NUMERIC, 15, 2);
        pnd_Total_Pmts_Out = localVariables.newFieldInRecord("pnd_Total_Pmts_Out", "#TOTAL-PMTS-OUT", FieldType.NUMERIC, 13, 2);
        pnd_Total_Units_Out = localVariables.newFieldInRecord("pnd_Total_Units_Out", "#TOTAL-UNITS-OUT", FieldType.NUMERIC, 9, 3);
        pnd_Pmts_Out = localVariables.newFieldInRecord("pnd_Pmts_Out", "#PMTS-OUT", FieldType.NUMERIC, 11, 2);
        pnd_Cntrl_Sd_Cde = localVariables.newFieldInRecord("pnd_Cntrl_Sd_Cde", "#CNTRL-SD-CDE", FieldType.STRING, 2);
        pnd_Date1 = localVariables.newFieldInRecord("pnd_Date1", "#DATE1", FieldType.STRING, 10);
        pnd_Nonzero_Switch = localVariables.newFieldInRecord("pnd_Nonzero_Switch", "#NONZERO-SWITCH", FieldType.STRING, 1);
        pnd_Prt_Msg = localVariables.newFieldInRecord("pnd_Prt_Msg", "#PRT-MSG", FieldType.STRING, 19);
        pnd_Reval_Switch = localVariables.newFieldInRecord("pnd_Reval_Switch", "#REVAL-SWITCH", FieldType.STRING, 2);

        pnd_Eff_Date_Array = localVariables.newGroupArrayInRecord("pnd_Eff_Date_Array", "#EFF-DATE-ARRAY", new DbsArrayController(1, 5));
        pnd_Eff_Date_Array_Pnd_Eff_Date = pnd_Eff_Date_Array.newFieldInGroup("pnd_Eff_Date_Array_Pnd_Eff_Date", "#EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Save_Eff_Date = localVariables.newFieldInRecord("pnd_Save_Eff_Date", "#SAVE-EFF-DATE", FieldType.NUMERIC, 8);

        pnd_Save_Eff_Date__R_Field_14 = localVariables.newGroupInRecord("pnd_Save_Eff_Date__R_Field_14", "REDEFINE", pnd_Save_Eff_Date);
        pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X = pnd_Save_Eff_Date__R_Field_14.newFieldInGroup("pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X", "#SAVE-EFF-DATE-X", 
            FieldType.STRING, 8);
        pnd_Date_Count = localVariables.newFieldInRecord("pnd_Date_Count", "#DATE-COUNT", FieldType.NUMERIC, 3);
        pnd_Eof_Flag = localVariables.newFieldInRecord("pnd_Eof_Flag", "#EOF-FLAG", FieldType.STRING, 1);
        pnd_First_Return = localVariables.newFieldInRecord("pnd_First_Return", "#FIRST-RETURN", FieldType.STRING, 1);
        pnd_First_Sort = localVariables.newFieldInRecord("pnd_First_Sort", "#FIRST-SORT", FieldType.NUMERIC, 1);
        pnd_Save_Transfer_Eff_Date = localVariables.newFieldInRecord("pnd_Save_Transfer_Eff_Date", "#SAVE-TRANSFER-EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Prt_Transfer_Eff_Date = localVariables.newFieldInRecord("pnd_Prt_Transfer_Eff_Date", "#PRT-TRANSFER-EFF-DATE", FieldType.NUMERIC, 8);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.NUMERIC, 8);
        pnd_Revaluation_Ind_Out = localVariables.newFieldInRecord("pnd_Revaluation_Ind_Out", "#REVALUATION-IND-OUT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Print_Ppcn = localVariables.newFieldInRecord("pnd_Print_Ppcn", "#PRINT-PPCN", FieldType.STRING, 9);

        pnd_Print_Ppcn__R_Field_15 = localVariables.newGroupInRecord("pnd_Print_Ppcn__R_Field_15", "REDEFINE", pnd_Print_Ppcn);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_7 = pnd_Print_Ppcn__R_Field_15.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_7", "#PRINT-PPCN-7", FieldType.STRING, 
            7);
        pnd_Print_Ppcn_Pnd_Print_Dash = pnd_Print_Ppcn__R_Field_15.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Dash", "#PRINT-DASH", FieldType.STRING, 1);
        pnd_Print_Ppcn_Pnd_Print_Ppcn_8 = pnd_Print_Ppcn__R_Field_15.newFieldInGroup("pnd_Print_Ppcn_Pnd_Print_Ppcn_8", "#PRINT-PPCN-8", FieldType.STRING, 
            1);
        pnd_Print_Fund = localVariables.newFieldInRecord("pnd_Print_Fund", "#PRINT-FUND", FieldType.STRING, 7);
        pnd_Print_Fund_Out = localVariables.newFieldInRecord("pnd_Print_Fund_Out", "#PRINT-FUND-OUT", FieldType.STRING, 7);

        pnd_Fund_Name_Array = localVariables.newGroupInRecord("pnd_Fund_Name_Array", "#FUND-NAME-ARRAY");
        pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name", "#TIAA-STD-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name", "#TIAA-GTD-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Stock_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Stock_Name", "#STOCK-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Mma_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Mma_Name", "#MMA-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Social_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Social_Name", "#SOCIAL-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Bond_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Bond_Name", "#BOND-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Global_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Global_Name", "#GLOBAL-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Growth_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Growth_Name", "#GROWTH-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Index_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Index_Name", "#INDEX-NAME", FieldType.STRING, 
            8);
        pnd_Fund_Name_Array_Pnd_Ilb_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Ilb_Name", "#ILB-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Rea_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Rea_Name", "#REA-NAME", FieldType.STRING, 8);
        pnd_Fund_Name_Array_Pnd_Acc_Name = pnd_Fund_Name_Array.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Acc_Name", "#ACC-NAME", FieldType.STRING, 8);

        pnd_Fund_Name_Array__R_Field_16 = localVariables.newGroupInRecord("pnd_Fund_Name_Array__R_Field_16", "REDEFINE", pnd_Fund_Name_Array);

        pnd_Fund_Name_Array_Pnd_Filler9 = pnd_Fund_Name_Array__R_Field_16.newGroupArrayInGroup("pnd_Fund_Name_Array_Pnd_Filler9", "#FILLER9", new DbsArrayController(1, 
            12));
        pnd_Fund_Name_Array_Pnd_Fund_Name = pnd_Fund_Name_Array_Pnd_Filler9.newFieldInGroup("pnd_Fund_Name_Array_Pnd_Fund_Name", "#FUND-NAME", FieldType.STRING, 
            8);

        pnd_Fund_Letter_Array = localVariables.newGroupInRecord("pnd_Fund_Letter_Array", "#FUND-LETTER-ARRAY");
        pnd_Fund_Letter_Array_Pnd_Std_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Std_Letter", "#STD-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Grd_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Grd_Letter", "#GRD-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Stock_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Stock_Letter", "#STOCK-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Mma_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Mma_Letter", "#MMA-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Social_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Social_Letter", "#SOCIAL-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Bond_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Bond_Letter", "#BOND-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Global_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Global_Letter", "#GLOBAL-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Growth_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Growth_Letter", "#GROWTH-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Index_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Index_Letter", "#INDEX-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Ilb_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Ilb_Letter", "#ILB-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Rea_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Rea_Letter", "#REA-LETTER", FieldType.STRING, 
            1);
        pnd_Fund_Letter_Array_Pnd_Acc_Letter = pnd_Fund_Letter_Array.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Acc_Letter", "#ACC-LETTER", FieldType.STRING, 
            1);

        pnd_Fund_Letter_Array__R_Field_17 = localVariables.newGroupInRecord("pnd_Fund_Letter_Array__R_Field_17", "REDEFINE", pnd_Fund_Letter_Array);

        pnd_Fund_Letter_Array_Pnd_Filler = pnd_Fund_Letter_Array__R_Field_17.newGroupArrayInGroup("pnd_Fund_Letter_Array_Pnd_Filler", "#FILLER", new DbsArrayController(1, 
            12));
        pnd_Fund_Letter_Array_Pnd_Fund_Letter = pnd_Fund_Letter_Array_Pnd_Filler.newFieldInGroup("pnd_Fund_Letter_Array_Pnd_Fund_Letter", "#FUND-LETTER", 
            FieldType.STRING, 1);
        pnd_Fund_Input_Pos = localVariables.newFieldInRecord("pnd_Fund_Input_Pos", "#FUND-INPUT-POS", FieldType.NUMERIC, 2);
        pnd_Fund_Output_Pos = localVariables.newFieldInRecord("pnd_Fund_Output_Pos", "#FUND-OUTPUT-POS", FieldType.NUMERIC, 2);

        pnd_Transfer_Amt_By_Fund_In_1 = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_1", "#TRANSFER-AMT-BY-FUND-IN-1", new DbsArrayController(1, 
            12));
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M", 
            "#UNITS-OUTGOING-M", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A", 
            "#UNITS-OUTGOING-A", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1 = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1", 
            "#UNITS-OUTGOING-M-1", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1 = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1", 
            "#UNITS-OUTGOING-A-1", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M", 
            "#PMTS-OUTGOING-M", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A", 
            "#PMTS-OUTGOING-A", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming", 
            "#GTD-INCOMING", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming", 
            "#DVD-INCOMING", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A", 
            "#UNITS-INCOMING-A", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M", 
            "#UNITS-INCOMING-M", FieldType.NUMERIC, 11, 3);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M", 
            "#PMTS-INCOMING-M", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A", 
            "#PMTS-INCOMING-A", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers", 
            "#TOTAL-NUM-FROM-TRANSFERS", FieldType.NUMERIC, 6);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M", 
            "#NET-TRANSFER-M", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A", 
            "#NET-TRANSFER-A", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A", 
            "#NET-REVAL-A", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M = pnd_Transfer_Amt_By_Fund_In_1.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M", 
            "#NET-REVAL-M", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_1 = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_1", "#TOT-TRANS-AMT-BY-FUND-IN-1", new DbsArrayController(1, 
            12));
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M", 
            "#TOT-UNITS-OUTGOING-M", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1 = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1", 
            "#TOT-UNITS-OUTGOING-M-1", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A", 
            "#TOT-UNITS-OUTGOING-A", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1 = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1", 
            "#TOT-UNITS-OUTGOING-A-1", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M", 
            "#TOT-PMTS-OUTGOING-M", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A", 
            "#TOT-PMTS-OUTGOING-A", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming", 
            "#TOT-GTD-INCOMING", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming", 
            "#TOT-DVD-INCOMING", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A", 
            "#TOT-UNITS-INCOMING-A", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M", 
            "#TOT-UNITS-INCOMING-M", FieldType.NUMERIC, 11, 3);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M", 
            "#TOT-PMTS-INCOMING-M", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A = pnd_Tot_Trans_Amt_By_Fund_In_1.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A", 
            "#TOT-PMTS-INCOMING-A", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Tiaa_By_Method = localVariables.newGroupArrayInRecord("pnd_Transfer_Tiaa_By_Method", "#TRANSFER-TIAA-BY-METHOD", new DbsArrayController(1, 
            2));
        pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa = pnd_Transfer_Tiaa_By_Method.newFieldInGroup("pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa", 
            "#NET-TRANSFER-TIAA", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Num_Mm = localVariables.newGroupInRecord("pnd_Transfer_Num_Mm", "#TRANSFER-NUM-MM");

        pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_In_Mm = pnd_Transfer_Num_Mm.newGroupArrayInGroup("pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_In_Mm", 
            "#TRANSFER-NUM-BY-FUND-IN-MM", new DbsArrayController(1, 12));

        pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_Out_Mm = pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_In_Mm.newGroupArrayInGroup("pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_Out_Mm", 
            "#TRANSFER-NUM-BY-FUND-OUT-MM", new DbsArrayController(1, 12));
        pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm = pnd_Transfer_Num_Mm_Pnd_Transfer_Num_By_Fund_Out_Mm.newFieldInGroup("pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm", 
            "#NUM-TRANSFERS-MM", FieldType.NUMERIC, 6);

        pnd_Transfer_Num_By_Fund_In_Ma = localVariables.newGroupArrayInRecord("pnd_Transfer_Num_By_Fund_In_Ma", "#TRANSFER-NUM-BY-FUND-IN-MA", new DbsArrayController(1, 
            12));

        pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Transfer_Num_By_Fund_Out_Ma = pnd_Transfer_Num_By_Fund_In_Ma.newGroupArrayInGroup("pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Transfer_Num_By_Fund_Out_Ma", 
            "#TRANSFER-NUM-BY-FUND-OUT-MA", new DbsArrayController(1, 12));
        pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma = pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Transfer_Num_By_Fund_Out_Ma.newFieldInGroup("pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma", 
            "#NUM-TRANSFERS-MA", FieldType.NUMERIC, 6);

        pnd_Transfer_Num_By_Fund_In_Am = localVariables.newGroupArrayInRecord("pnd_Transfer_Num_By_Fund_In_Am", "#TRANSFER-NUM-BY-FUND-IN-AM", new DbsArrayController(1, 
            12));

        pnd_Transfer_Num_By_Fund_In_Am_Pnd_Transfer_Num_By_Fund_Out_Am = pnd_Transfer_Num_By_Fund_In_Am.newGroupArrayInGroup("pnd_Transfer_Num_By_Fund_In_Am_Pnd_Transfer_Num_By_Fund_Out_Am", 
            "#TRANSFER-NUM-BY-FUND-OUT-AM", new DbsArrayController(1, 12));
        pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am = pnd_Transfer_Num_By_Fund_In_Am_Pnd_Transfer_Num_By_Fund_Out_Am.newFieldInGroup("pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am", 
            "#NUM-TRANSFERS-AM", FieldType.NUMERIC, 6);

        pnd_Transfer_Num_By_Fund_In_Aa = localVariables.newGroupArrayInRecord("pnd_Transfer_Num_By_Fund_In_Aa", "#TRANSFER-NUM-BY-FUND-IN-AA", new DbsArrayController(1, 
            12));

        pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Transfer_Num_By_Fund_Out_Aa = pnd_Transfer_Num_By_Fund_In_Aa.newGroupArrayInGroup("pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Transfer_Num_By_Fund_Out_Aa", 
            "#TRANSFER-NUM-BY-FUND-OUT-AA", new DbsArrayController(1, 12));
        pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa = pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Transfer_Num_By_Fund_Out_Aa.newFieldInGroup("pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa", 
            "#NUM-TRANSFERS-AA", FieldType.NUMERIC, 6);

        pnd_Tot_Transfer_Num_Mm = localVariables.newGroupInRecord("pnd_Tot_Transfer_Num_Mm", "#TOT-TRANSFER-NUM-MM");

        pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Transfer_Num_By_Fund_In_Mm = pnd_Tot_Transfer_Num_Mm.newGroupArrayInGroup("pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Transfer_Num_By_Fund_In_Mm", 
            "#TOT-TRANSFER-NUM-BY-FUND-IN-MM", new DbsArrayController(1, 12));

        pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Trans_Num_By_Fund_Out_Mm = pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Transfer_Num_By_Fund_In_Mm.newGroupArrayInGroup("pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Trans_Num_By_Fund_Out_Mm", 
            "#TOT-TRANS-NUM-BY-FUND-OUT-MM", new DbsArrayController(1, 12));
        pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Num_Transfers_Mm = pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Trans_Num_By_Fund_Out_Mm.newFieldInGroup("pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Num_Transfers_Mm", 
            "#TOT-NUM-TRANSFERS-MM", FieldType.NUMERIC, 6);

        pnd_Tot_Trans_Num_By_Fund_In_Ma = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Num_By_Fund_In_Ma", "#TOT-TRANS-NUM-BY-FUND-IN-MA", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Trans_Num_By_Fund_Out_Ma = pnd_Tot_Trans_Num_By_Fund_In_Ma.newGroupArrayInGroup("pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Trans_Num_By_Fund_Out_Ma", 
            "#TOT-TRANS-NUM-BY-FUND-OUT-MA", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Num_Transfers_Ma = pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Trans_Num_By_Fund_Out_Ma.newFieldInGroup("pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Num_Transfers_Ma", 
            "#TOT-NUM-TRANSFERS-MA", FieldType.NUMERIC, 6);

        pnd_Tot_Trans_Num_By_Fund_In_Am = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Num_By_Fund_In_Am", "#TOT-TRANS-NUM-BY-FUND-IN-AM", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Trans_Num_By_Fund_Out_Am = pnd_Tot_Trans_Num_By_Fund_In_Am.newGroupArrayInGroup("pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Trans_Num_By_Fund_Out_Am", 
            "#TOT-TRANS-NUM-BY-FUND-OUT-AM", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Num_Transfers_Am = pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Trans_Num_By_Fund_Out_Am.newFieldInGroup("pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Num_Transfers_Am", 
            "#TOT-NUM-TRANSFERS-AM", FieldType.NUMERIC, 6);

        pnd_Tot_Trans_Num_By_Fund_In_Aa = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Num_By_Fund_In_Aa", "#TOT-TRANS-NUM-BY-FUND-IN-AA", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Transfer_Num_By_Fund_Out_Aa = pnd_Tot_Trans_Num_By_Fund_In_Aa.newGroupArrayInGroup("pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Transfer_Num_By_Fund_Out_Aa", 
            "#TOT-TRANSFER-NUM-BY-FUND-OUT-AA", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Num_Transfers_Aa = pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Transfer_Num_By_Fund_Out_Aa.newFieldInGroup("pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Num_Transfers_Aa", 
            "#TOT-NUM-TRANSFERS-AA", FieldType.NUMERIC, 6);

        pnd_Transfer_Num_By_Fund_In_Tiaa = localVariables.newGroupArrayInRecord("pnd_Transfer_Num_By_Fund_In_Tiaa", "#TRANSFER-NUM-BY-FUND-IN-TIAA", new 
            DbsArrayController(1, 12));

        pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Transfer_Num_By_Fund_Out_Tiaa = pnd_Transfer_Num_By_Fund_In_Tiaa.newGroupArrayInGroup("pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Transfer_Num_By_Fund_Out_Tiaa", 
            "#TRANSFER-NUM-BY-FUND-OUT-TIAA", new DbsArrayController(1, 12));
        pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa = pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Transfer_Num_By_Fund_Out_Tiaa.newFieldInGroup("pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa", 
            "#NUM-TRANSFERS-TIAA", FieldType.NUMERIC, 6);

        pnd_Transfer_Num = localVariables.newGroupInRecord("pnd_Transfer_Num", "#TRANSFER-NUM");

        pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_In = pnd_Transfer_Num.newGroupArrayInGroup("pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_In", "#TRANSFER-NUM-BY-FUND-IN", 
            new DbsArrayController(1, 12));

        pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_Out = pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_In.newGroupArrayInGroup("pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_Out", 
            "#TRANSFER-NUM-BY-FUND-OUT", new DbsArrayController(1, 12));
        pnd_Transfer_Num_Pnd_Num_Transfers = pnd_Transfer_Num_Pnd_Transfer_Num_By_Fund_Out.newFieldInGroup("pnd_Transfer_Num_Pnd_Num_Transfers", "#NUM-TRANSFERS", 
            FieldType.NUMERIC, 6);

        pnd_Transfer_Amt_By_Fund_In_Mm = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_Mm", "#TRANSFER-AMT-BY-FUND-IN-MM", new DbsArrayController(1, 
            12));

        pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_By_Fund_Out_Mm = pnd_Transfer_Amt_By_Fund_In_Mm.newGroupArrayInGroup("pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_By_Fund_Out_Mm", 
            "#TRANSFER-AMT-BY-FUND-OUT-MM", new DbsArrayController(1, 12));
        pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm = pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_By_Fund_Out_Mm.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm", 
            "#TRANSFER-AMT-ARRAY-MM", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Amt_By_Fund_In_Ma = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_Ma", "#TRANSFER-AMT-BY-FUND-IN-MA", new DbsArrayController(1, 
            12));

        pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_By_Fund_Out_Ma = pnd_Transfer_Amt_By_Fund_In_Ma.newGroupArrayInGroup("pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_By_Fund_Out_Ma", 
            "#TRANSFER-AMT-BY-FUND-OUT-MA", new DbsArrayController(1, 12));
        pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma = pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_By_Fund_Out_Ma.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma", 
            "#TRANSFER-AMT-ARRAY-MA", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Amt_By_Fund_In_Am = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_Am", "#TRANSFER-AMT-BY-FUND-IN-AM", new DbsArrayController(1, 
            12));

        pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_By_Fund_Out_Am = pnd_Transfer_Amt_By_Fund_In_Am.newGroupArrayInGroup("pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_By_Fund_Out_Am", 
            "#TRANSFER-AMT-BY-FUND-OUT-AM", new DbsArrayController(1, 12));
        pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am = pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_By_Fund_Out_Am.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am", 
            "#TRANSFER-AMT-ARRAY-AM", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Amt_By_Fund_In_Aa = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_Aa", "#TRANSFER-AMT-BY-FUND-IN-AA", new DbsArrayController(1, 
            12));

        pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_By_Fund_Out_Aa = pnd_Transfer_Amt_By_Fund_In_Aa.newGroupArrayInGroup("pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_By_Fund_Out_Aa", 
            "#TRANSFER-AMT-BY-FUND-OUT-AA", new DbsArrayController(1, 12));
        pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa = pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_By_Fund_Out_Aa.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa", 
            "#TRANSFER-AMT-ARRAY-AA", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_Mm = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_Mm", "#TOT-TRANS-AMT-BY-FUND-IN-MM", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Trans_Amt_By_Fund_Out_Mm = pnd_Tot_Trans_Amt_By_Fund_In_Mm.newGroupArrayInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Trans_Amt_By_Fund_Out_Mm", 
            "#TOT-TRANS-AMT-BY-FUND-OUT-MM", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Transfer_Amt_Array_Mm = pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Trans_Amt_By_Fund_Out_Mm.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Transfer_Amt_Array_Mm", 
            "#TOT-TRANSFER-AMT-ARRAY-MM", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_Ma = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_Ma", "#TOT-TRANS-AMT-BY-FUND-IN-MA", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Trans_Amt_By_Fund_Out_Ma = pnd_Tot_Trans_Amt_By_Fund_In_Ma.newGroupArrayInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Trans_Amt_By_Fund_Out_Ma", 
            "#TOT-TRANS-AMT-BY-FUND-OUT-MA", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Transfer_Amt_Array_Ma = pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Trans_Amt_By_Fund_Out_Ma.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Transfer_Amt_Array_Ma", 
            "#TOT-TRANSFER-AMT-ARRAY-MA", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_Am = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_Am", "#TOT-TRANS-AMT-BY-FUND-IN-AM", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Trans_Amt_By_Fund_Out_Am = pnd_Tot_Trans_Amt_By_Fund_In_Am.newGroupArrayInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Trans_Amt_By_Fund_Out_Am", 
            "#TOT-TRANS-AMT-BY-FUND-OUT-AM", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Transfer_Amt_Array_Am = pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Trans_Amt_By_Fund_Out_Am.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Transfer_Amt_Array_Am", 
            "#TOT-TRANSFER-AMT-ARRAY-AM", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_Aa = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_Aa", "#TOT-TRANS-AMT-BY-FUND-IN-AA", new 
            DbsArrayController(1, 12));

        pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Trans_Amt_By_Fund_Out_Aa = pnd_Tot_Trans_Amt_By_Fund_In_Aa.newGroupArrayInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Trans_Amt_By_Fund_Out_Aa", 
            "#TOT-TRANS-AMT-BY-FUND-OUT-AA", new DbsArrayController(1, 12));
        pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Transfer_Amt_Array_Aa = pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Trans_Amt_By_Fund_Out_Aa.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Transfer_Amt_Array_Aa", 
            "#TOT-TRANSFER-AMT-ARRAY-AA", FieldType.NUMERIC, 13, 2);

        pnd_Transfer_Amt_By_Fund_In_Tiaa = localVariables.newGroupArrayInRecord("pnd_Transfer_Amt_By_Fund_In_Tiaa", "#TRANSFER-AMT-BY-FUND-IN-TIAA", new 
            DbsArrayController(1, 12));

        pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_By_Fund_Out_Tiaa = pnd_Transfer_Amt_By_Fund_In_Tiaa.newGroupArrayInGroup("pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_By_Fund_Out_Tiaa", 
            "#TRANSFER-AMT-BY-FUND-OUT-TIAA", new DbsArrayController(1, 2));
        pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A = pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_By_Fund_Out_Tiaa.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A", 
            "#TRANSFER-AMT-TIAA-ARRAY-A", FieldType.NUMERIC, 13, 2);
        pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M = pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_By_Fund_Out_Tiaa.newFieldInGroup("pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M", 
            "#TRANSFER-AMT-TIAA-ARRAY-M", FieldType.NUMERIC, 13, 2);

        pnd_Tot_Trans_Amt_By_Fund_In_Tiaa = localVariables.newGroupArrayInRecord("pnd_Tot_Trans_Amt_By_Fund_In_Tiaa", "#TOT-TRANS-AMT-BY-FUND-IN-TIAA", 
            new DbsArrayController(1, 12));

        pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Tran_Amt_By_Fund_Out_Tiaa = pnd_Tot_Trans_Amt_By_Fund_In_Tiaa.newGroupArrayInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Tran_Amt_By_Fund_Out_Tiaa", 
            "#TOT-TRAN-AMT-BY-FUND-OUT-TIAA", new DbsArrayController(1, 2));
        pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_A = pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Tran_Amt_By_Fund_Out_Tiaa.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_A", 
            "#TOT-TRANSFER-AMT-TIAA-ARRAY-A", FieldType.NUMERIC, 13, 2);
        pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_M = pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Tran_Amt_By_Fund_Out_Tiaa.newFieldInGroup("pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_M", 
            "#TOT-TRANSFER-AMT-TIAA-ARRAY-M", FieldType.NUMERIC, 13, 2);
        pnd_Iaxfr_Actrl_Cntrct_Py = localVariables.newFieldInRecord("pnd_Iaxfr_Actrl_Cntrct_Py", "#IAXFR-ACTRL-CNTRCT-PY", FieldType.STRING, 12);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18 = localVariables.newGroupInRecord("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18", "REDEFINE", pnd_Iaxfr_Actrl_Cntrct_Py);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Cntrct_Py_A10", 
            "#IAXFR-ACTRL-CNTRCT-PY-A10", FieldType.STRING, 10);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number", 
            "#IAXFR-ACTRL-RECORD-NUMBER", FieldType.NUMERIC, 2);

        pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_19 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_18.newGroupInGroup("pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_19", "REDEFINE", 
            pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number);
        pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2 = pnd_Iaxfr_Actrl_Cntrct_Py__R_Field_19.newFieldInGroup("pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2", 
            "#IAXFR-ACTRL-RECORD-NUMBER-A2", FieldType.STRING, 2);
        pnd_Net_Units = localVariables.newFieldInRecord("pnd_Net_Units", "#NET-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Net_Pmts = localVariables.newFieldInRecord("pnd_Net_Pmts", "#NET-PMTS", FieldType.NUMERIC, 13, 2);
        pnd_Prt_Switch = localVariables.newFieldInRecord("pnd_Prt_Switch", "#PRT-SWITCH", FieldType.STRING, 1);
        pnd_Num_Lines = localVariables.newFieldInRecord("pnd_Num_Lines", "#NUM-LINES", FieldType.NUMERIC, 2);
        pnd_Tiaa_Pos = localVariables.newFieldInRecord("pnd_Tiaa_Pos", "#TIAA-POS", FieldType.NUMERIC, 1);
        pnd_Cref_Rea_Pos = localVariables.newFieldInRecord("pnd_Cref_Rea_Pos", "#CREF-REA-POS", FieldType.NUMERIC, 1);
        pnd_Pmt_Index = localVariables.newFieldInRecord("pnd_Pmt_Index", "#PMT-INDEX", FieldType.NUMERIC, 2);
        pnd_Total_Num_Transfers = localVariables.newFieldInRecord("pnd_Total_Num_Transfers", "#TOTAL-NUM-TRANSFERS", FieldType.NUMERIC, 6);
        pnd_Grand_Total_Num_Transfers = localVariables.newFieldInRecord("pnd_Grand_Total_Num_Transfers", "#GRAND-TOTAL-NUM-TRANSFERS", FieldType.NUMERIC, 
            6);
        pnd_First_Write_Switch = localVariables.newFieldInRecord("pnd_First_Write_Switch", "#FIRST-WRITE-SWITCH", FieldType.STRING, 1);
        pnd_Grand_Total_Switch = localVariables.newFieldInRecord("pnd_Grand_Total_Switch", "#GRAND-TOTAL-SWITCH", FieldType.STRING, 1);
        pnd_Net_Trans_Reval = localVariables.newFieldInRecord("pnd_Net_Trans_Reval", "#NET-TRANS-REVAL", FieldType.NUMERIC, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaAial0131.initializeValues();
        ldaAial0132.initializeValues();
        ldaAial0133.initializeValues();

        localVariables.reset();
        pnd_Transfer_Amt_Array_Tot.setInitialValue(0);
        pnd_Save_Eff_Date.setInitialValue(0);
        pnd_Date_Count.setInitialValue(0);
        pnd_Eof_Flag.setInitialValue("N");
        pnd_First_Return.setInitialValue("Y");
        pnd_Fund_Name_Array_Pnd_Tiaa_Std_Name.setInitialValue("TIAA STD");
        pnd_Fund_Name_Array_Pnd_Tiaa_Gtd_Name.setInitialValue("TIAA GRD");
        pnd_Fund_Name_Array_Pnd_Stock_Name.setInitialValue("STOCK   ");
        pnd_Fund_Name_Array_Pnd_Mma_Name.setInitialValue("MMA     ");
        pnd_Fund_Name_Array_Pnd_Social_Name.setInitialValue("SOCIAL  ");
        pnd_Fund_Name_Array_Pnd_Bond_Name.setInitialValue("BOND    ");
        pnd_Fund_Name_Array_Pnd_Global_Name.setInitialValue("GLOBAL  ");
        pnd_Fund_Name_Array_Pnd_Growth_Name.setInitialValue("GROWTH  ");
        pnd_Fund_Name_Array_Pnd_Index_Name.setInitialValue("INDEX   ");
        pnd_Fund_Name_Array_Pnd_Ilb_Name.setInitialValue("ILB     ");
        pnd_Fund_Name_Array_Pnd_Rea_Name.setInitialValue("REA     ");
        pnd_Fund_Name_Array_Pnd_Acc_Name.setInitialValue("ACCESS  ");
        pnd_Fund_Letter_Array_Pnd_Std_Letter.setInitialValue("1");
        pnd_Fund_Letter_Array_Pnd_Grd_Letter.setInitialValue("2");
        pnd_Fund_Letter_Array_Pnd_Stock_Letter.setInitialValue("C");
        pnd_Fund_Letter_Array_Pnd_Mma_Letter.setInitialValue("M");
        pnd_Fund_Letter_Array_Pnd_Social_Letter.setInitialValue("S");
        pnd_Fund_Letter_Array_Pnd_Bond_Letter.setInitialValue("B");
        pnd_Fund_Letter_Array_Pnd_Global_Letter.setInitialValue("W");
        pnd_Fund_Letter_Array_Pnd_Growth_Letter.setInitialValue("L");
        pnd_Fund_Letter_Array_Pnd_Index_Letter.setInitialValue("E");
        pnd_Fund_Letter_Array_Pnd_Ilb_Letter.setInitialValue("I");
        pnd_Fund_Letter_Array_Pnd_Rea_Letter.setInitialValue("R");
        pnd_Fund_Letter_Array_Pnd_Acc_Letter.setInitialValue("D");
        pnd_Num_Lines.setInitialValue(57);
        pnd_Tiaa_Pos.setInitialValue(0);
        pnd_Cref_Rea_Pos.setInitialValue(0);
        pnd_Pmt_Index.setInitialValue(0);
        pnd_Total_Num_Transfers.setInitialValue(0);
        pnd_Grand_Total_Num_Transfers.setInitialValue(0);
        pnd_Grand_Total_Switch.setInitialValue("N");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Aiap002() throws Exception
    {
        super("Aiap002");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getReports().write(0, "****** AIAP002 *********");                                                                                                                //Natural: WRITE '****** AIAP002 *********'
        if (Global.isEscape()) return;
        //*  RBE2                                                                                                                                                         //Natural: FORMAT LS = 132 PS = 66 ES = ON
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 0 ES = ON
        pnd_Print_Ppcn_Pnd_Print_Dash.setValue("-");                                                                                                                      //Natural: MOVE '-' TO #PRINT-DASH
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TRANSFER-ARRAYS
        sub_Initialize_Transfer_Arrays();
        if (condition(Global.isEscape())) {return;}
        //* ********************************************************
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        //*    20121105    20121018              /* REMOVE BFORE PROD
        //*  MOVE 20130401 TO #INPUT-BEGIN-DATE  /* REMOVE BFORE PROD
        //*  MOVE 20130401 TO #INPUT-END-DATE    /* REMOVE BFORE PROD
        getReports().write(0, "#INPUT-BEGIN-DATE = ",pnd_Input_Record_Pnd_Input_Begin_Date);                                                                              //Natural: WRITE '#INPUT-BEGIN-DATE = ' #INPUT-BEGIN-DATE
        if (Global.isEscape()) return;
        //* ********************************************************
        //*  READ WORK FILE 1 RECORD #INPUT-DATE
        //*  WRITE '#INPUT-BEGIN-DATE = ' #INPUT-BEGIN-DATE
        //*  READ IAA-XFR-ACTRL-RCRD-VIEW
        //*  FIND IAA-XFR-ACTRL-RCRD-VIEW            /* RBE2
        //*  RBE2
        ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().startDatabaseFind                                                                                                    //Natural: FIND IAA-XFR-ACTRL-RCRD-VIEW2 WITH IAXFR-ACTRL-PRCSS-DTE = #INPUT-BEGIN-DATE
        (
        "FIND01",
        new Wc[] { new Wc("IAXFR_ACTRL_PRCSS_DTE", "=", pnd_Input_Record_Pnd_Input_Begin_Date, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().readNextRow("FIND01", true)))
        {
            ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().setIfNotFoundControlFlag(false);
            //*    WITH IAXFR-ACTRL-PRCSS-DTE = 19980706
            //*   WITH IAXFR-ACTRL-CNTRCT-PY-A10 = '0U08021401'
            if (condition(ldaAial0131.getVw_iaa_Xfr_Actrl_Rcrd_View2().getAstCOUNTER().equals(0)))                                                                        //Natural: IF NO RECORDS FOUND
            {
                getReports().write(1, "NO IA RECORDS FOUND ");                                                                                                            //Natural: WRITE ( 1 ) 'NO IA RECORDS FOUND '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAial0132.getPnd_Store_Actrl_Fld_1().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-1 TO #STORE-ACTRL-FLD-1
            ldaAial0132.getPnd_Store_Actrl_Fld_2().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_2());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-2 TO #STORE-ACTRL-FLD-2
            ldaAial0132.getPnd_Store_Actrl_Fld_3().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_3());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-3 TO #STORE-ACTRL-FLD-3
            ldaAial0132.getPnd_Store_Actrl_Fld_4().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_4());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-4 TO #STORE-ACTRL-FLD-4
            ldaAial0132.getPnd_Store_Actrl_Fld_5().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_5());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-5 TO #STORE-ACTRL-FLD-5
            ldaAial0132.getPnd_Store_Actrl_Fld_6().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_6());                                                 //Natural: MOVE IAXFR-ACTRL-FLD-6 TO #STORE-ACTRL-FLD-6
            //*  RBE2
            pnd_Iaxfr_Actrl_Cntrct_Py.setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Cntrct_Py());                                                          //Natural: MOVE IAXFR-ACTRL-CNTRCT-PY TO #IAXFR-ACTRL-CNTRCT-PY
            //*   IF IAXFR-ACTRL-RECORD-NUMBER-A2 �= '  '                 /* RBE2
            //*  RBE2
            if (condition(pnd_Iaxfr_Actrl_Cntrct_Py_Pnd_Iaxfr_Actrl_Record_Number_A2.notEquals("  ")))                                                                    //Natural: IF #IAXFR-ACTRL-RECORD-NUMBER-A2 <> '  '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1().equals("T") || ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Cref_Rea_Indicator_S1().equals(" "))) //Natural: IF #CREF-REA-INDICATOR-S1 = 'T' OR = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_1());                  //Natural: MOVE IAXFR-ACTRL-FLD-1 TO #IAXFR-ACTRL-FLD-1-OUT
            //*    MOVE #CREF-REA-INDICATOR-S1-OUT TO #CREF-REA-INDICATOR-S1-TEMP
            //*    MOVE #TRANSFER-REVAL-SWITCH-S1-OUT TO #TRANSFER-REVAL-SWITCH-S1-TEMP
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_2());                  //Natural: MOVE IAXFR-ACTRL-FLD-2 TO #IAXFR-ACTRL-FLD-2-OUT
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_3());                  //Natural: MOVE IAXFR-ACTRL-FLD-3 TO #IAXFR-ACTRL-FLD-3-OUT
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_4());                  //Natural: MOVE IAXFR-ACTRL-FLD-4 TO #IAXFR-ACTRL-FLD-4-OUT
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_5());                  //Natural: MOVE IAXFR-ACTRL-FLD-5 TO #IAXFR-ACTRL-FLD-5-OUT
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Fld_6());                  //Natural: MOVE IAXFR-ACTRL-FLD-6 TO #IAXFR-ACTRL-FLD-6-OUT
            ldaAial0133.getPnd_Iaxfr_Actrl_Prcss_Dte_Out().setValue(ldaAial0131.getIaa_Xfr_Actrl_Rcrd_View2_Iaxfr_Actrl_Prcss_Dte());                                     //Natural: MOVE IAXFR-ACTRL-PRCSS-DTE TO #IAXFR-ACTRL-PRCSS-DTE-OUT
            getWorkFiles().write(2, false, ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out());                                                                                  //Natural: WRITE WORK FILE 2 #IAA-XFR-ACTRL-RCRD-OUT
            //*    WRITE WORK FILE 2 #IAA-XFR-ACTRL-RCRD-TEMP
            //*    WRITE 'AFTER WRITE'
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE 'AFTER END-FIND'
        //*  END-READ
        //*  WRITE 'AFTER END-WORK'
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 #IAA-XFR-ACTRL-RCRD-OUT
        while (condition(getWorkFiles().read(2, ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out())))
        {
            //*  AD WORK 2 #IAA-XFR-ACTRL-RCRD-TEMP
            //*  WRITE 'BEFORE SORT'
            //*  WRITE '#CONTRACT-PAYEE-S1-OUT = ' #CONTRACT-PAYEE-S1-OUT
            //*  WRITE '#TRANSFER-EFF-DATE-OUT-S2-OUT = ' #TRANSFER-EFF-DATE-OUT-S2-OUT
            //*  D-ALL
            getSort().writeSortInData(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out(),  //Natural: END-ALL
                ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out(), 
                ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out(), 
                ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out(), ldaAial0133.getPnd_Iaxfr_Actrl_Prcss_Dte_Out());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out());                                                                 //Natural: SORT BY #TRANSFER-EFF-DATE-OUT-S3-OUT USING #IAXFR-ACTRL-FLD-1-OUT #IAXFR-ACTRL-FLD-2-OUT #IAXFR-ACTRL-FLD-3-OUT #IAXFR-ACTRL-FLD-4-OUT #IAXFR-ACTRL-FLD-5-OUT #IAXFR-ACTRL-FLD-6-OUT #IAXFR-ACTRL-PRCSS-DTE-OUT
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out(), 
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out(), 
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out(), ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out(), 
            ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out(), ldaAial0133.getPnd_Iaxfr_Actrl_Prcss_Dte_Out())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            ldaAial0132.getPnd_Store_Actrl_Fld_1().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out());                                       //Natural: MOVE #IAXFR-ACTRL-FLD-1-OUT TO #STORE-ACTRL-FLD-1
            //*  WRITE '#CONTRACT-PAYEE-S1 = ' #CONTRACT-PAYEE-S1
            //*  WRITE 'AFTER SORT'
            //*  WRITE '#CONTRACT-PAYEE-S1-OUT = ' #CONTRACT-PAYEE-S1-OUT
            //*  WRITE '#TRANSFER-EFF-DATE-OUT-S2-OUT = ' #TRANSFER-EFF-DATE-OUT-S2-OUT
            //*  WRITE '#EOF-FLAG = ' #EOF-FLAG                                                                                                                           //Natural: AT END OF DATA
                                                                                                                                                                          //Natural: AT BREAK OF #TRANSFER-EFF-DATE-OUT-S3-OUT;//Natural: PERFORM AFTER-SORT
            sub_After_Sort();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   WRITE '#CREF-REA-INDICATOR-S1-TEMP = ' #CREF-REA-INDICATOR-S1-TEMP
            //*  ITE '#TRANSFER-REVAL-SWITCH-S1-TEMP = ' #TRANSFER-REVAL-SWITCH-S1-TEMP
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            pnd_Eof_Flag.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #EOF-FLAG
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.setValue("ALL     ");                                                                                                       //Natural: MOVE 'ALL     ' TO #SAVE-EFF-DATE-X
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TRANSFER-ARRAYS
        sub_Initialize_Transfer_Arrays();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
        sub_Write_Grand_Totals();
        if (condition(Global.isEscape())) {return;}
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AFTER-SORT
        //*  WRITE '#PRINT-PPCN = ' #PRINT-PPCN
        //* *MOVE #FUND-CODE-OUT-S5         TO #FUND-CODE-OUT (19)
        //* *MOVE #UNITS-OUT-S5             TO #UNITS-OUT (19)
        //* *MOVE #TRANSFER-AMT-OUT-CREF-S5 TO #TRANSFER-AMT-OUT-CREF (19)
        //*  WRITE '#FUND-CODE-S1 = ' #FUND-CODE-S1
        //* *FOR #I = 1 TO 11
        //* *FOR #J = 1 TO 21
        //*      #FUND-NAME (#FUND-OUTPUT-POS) 2X #REVALUATION-IND-OUT 3X
        //* *FOR #J = 1 TO 19
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORTS
        //* *FOR #I = 1 TO 11
        //* *FOR #I = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //*      VALUE 3:11
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //*      VALUE 3:11
        //* *FOR #I = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*      WRITE '#NUM-LINES = ' #NUM-LINES
        //*  FOR #J = 1 TO 11
        //*  IF #GRAND-TOTAL-SWITCH = 'Y'
        //*   FOR #J = 1 TO 2
        //*   WRITE '#NET-TRANSFER-TIAA (' #J ') = ' #NET-TRANSFER-TIAA (#J)
        //*  END-FOR
        //*  END-IF
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 3 TO 11
        //* *FOR #I = 3 TO 11
        //* *FOR #I = 3 TO 11
        //* *FOR #I = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*  MOVE #TRANSFER-NUM-BY-DATE-IN-MM TO #TRANSFER-NUM-BY-DATE
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-NUM-TRANSFERS-C
        //*  'GROWTH       EQX      ILB      REA    TOTAL'
        //*  FOR #J = 1 TO 10
        //* *FOR #J = 1 TO 11
        //*  FOR #I = 1 TO 11
        //* *FOR #I = 3 TO 11
        //* *FOR #I = 3 TO 11
        //*  FOR #J = 1 TO 10
        //*  FOR #J = 1 TO 11
        //* **********************************************
        //* *** THIS ROUTINE NOT EXECUTED       **********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-NUM-TRANSFERS-T
        //*  WRITE (2) '5) SUMMARY OF TRANSFER REQUESTS PROCESSED' 60X /
        //*  FOR #J = 1 TO 10
        //*  FOR #J = 1 TO 10
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-FUND-POS
        //*  WRITE 'ASSIGN-FUND-POS'
        //*  WRITE '#FUND-TO-RECEIVE (' #J ') = ' #FUND-TO-RECEIVE (#J)
        //* *FOR #I = 1 TO 19
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-HEADINGS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CREF-TRANSFER-AMT
        //*    WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-REA-POS ') = '
        //*     #TRANSFER-AMT-OUT-CREF (#CREF-REA-POS)
        //*    WRITE '#TRANSFER-AMT-ARRAY-AM (' #FUND-INPUT-POS ','
        //*     #FUND-OUTPUT-POS ') = '
        //*      #TRANSFER-AMT-ARRAY-AM (#FUND-INPUT-POS, #FUND-OUTPUT-POS)
        //*    WRITE '#TRANSFER-AMT-OUT-CREF (' #CREF-REA-POS ') = '
        //*     #TRANSFER-AMT-OUT-CREF (#CREF-REA-POS)
        //*    WRITE '#TRANSFER-AMT-ARRAY-MA (' #FUND-INPUT-POS ','
        //*     #FUND-OUTPUT-POS ') = '
        //*      #TRANSFER-AMT-ARRAY-MA (#FUND-INPUT-POS, #FUND-OUTPUT-POS)
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TIAA-TRANSFER-AMT
        //*    WRITE '#CONTRACT-PAYEE = ' #CONTRACT-PAYEE
        //*    WRITE '#TRANSFER-AMT-OUT-TIAA-S5 (' #TIAA-POS ') = '
        //*     #TRANSFER-AMT-OUT-TIAA-S5 (#TIAA-POS)
        //*    WRITE '#TOT-TRANSFER-AMT-TIAA-ARRAY-A (' #FUND-INPUT-POS ','
        //*     #FUND-OUTPUT-POS ') = '
        //*     #TOT-TRANSFER-AMT-TIAA-ARRAY-A (#FUND-INPUT-POS, #FUND-OUTPUT-POS)
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-INCOMING-AMTS
        //*  VALUE 3:21
        //*        WRITE '#UNITS-INCOMING-A (' #FUND-OUTPUT-POS ') = '
        //*         #UNITS-INCOMING-A (#FUND-OUTPUT-POS)
        //*        WRITE '#UNITS-INCOMING-M (' #FUND-OUTPUT-POS ') = '
        //*         #UNITS-INCOMING-M (#FUND-OUTPUT-POS)
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-OUTGOING-AMTS
        //*  WRITE #CONTRACT-PAYEE
        //* *FOR #I = 1 TO 21
        //*     WRITE '#UNITS-OUTGOING-M (' #FUND-INPUT-POS ') = '
        //*      #UNITS-OUTGOING-M (#FUND-INPUT-POS)
        //*     WRITE '#UNITS-OUTGOING-A (' #FUND-INPUT-POS ') = '
        //*      #UNITS-OUTGOING-A (#FUND-INPUT-POS)
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TRANSFER-ARRAYS
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //* *FOR #I = 1 TO 11
        //*  FOR #J = 1 TO 11
    }
    private void sub_After_Sort() throws Exception                                                                                                                        //Natural: AFTER-SORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        //*  WRITE 'AFTER-SORT'
        ldaAial0132.getPnd_Store_Actrl_Fld_1().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_1_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-1-OUT TO #STORE-ACTRL-FLD-1
        ldaAial0132.getPnd_Store_Actrl_Fld_2().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_2_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-2-OUT TO #STORE-ACTRL-FLD-2
        ldaAial0132.getPnd_Store_Actrl_Fld_3().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_3_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-3-OUT TO #STORE-ACTRL-FLD-3
        ldaAial0132.getPnd_Store_Actrl_Fld_4().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_4_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-4-OUT TO #STORE-ACTRL-FLD-4
        ldaAial0132.getPnd_Store_Actrl_Fld_5().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_5_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-5-OUT TO #STORE-ACTRL-FLD-5
        ldaAial0132.getPnd_Store_Actrl_Fld_6().setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Iaxfr_Actrl_Fld_6_Out());                                           //Natural: MOVE #IAXFR-ACTRL-FLD-6-OUT TO #STORE-ACTRL-FLD-6
        if (condition(pnd_First_Return.equals("Y")))                                                                                                                      //Natural: IF #FIRST-RETURN = 'Y'
        {
            pnd_Save_Eff_Date.setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out());                                                     //Natural: MOVE #TRANSFER-EFF-DATE-OUT-S3-OUT TO #SAVE-EFF-DATE
            pnd_First_Return.setValue("N");                                                                                                                               //Natural: MOVE 'N' TO #FIRST-RETURN
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prt_Transfer_Eff_Date.setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out());                                                 //Natural: MOVE #TRANSFER-EFF-DATE-OUT-S3-OUT TO #PRT-TRANSFER-EFF-DATE
        if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                                  //Natural: IF #NUM-LINES NOT < 57
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
            sub_Write_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Pos.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #TIAA-POS #CREF-REA-POS
        pnd_Cref_Rea_Pos.setValue(0);
        pnd_Aian013b_Record_Pnd_Contract_Payee.setValue(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Contract_Payee_S1());                                                    //Natural: MOVE #CONTRACT-PAYEE-S1 TO #CONTRACT-PAYEE
        //*  WRITE '#CONTRACT-PAYEE-S1 = ' #CONTRACT-PAYEE-S1
        pnd_Print_Ppcn_Pnd_Print_Ppcn_7.setValue(pnd_Aian013b_Record_Pnd_Ppcn_7);                                                                                         //Natural: MOVE #PPCN-7 TO #PRINT-PPCN-7
        pnd_Print_Ppcn_Pnd_Print_Ppcn_8.setValue(pnd_Aian013b_Record_Pnd_Ppcn_8);                                                                                         //Natural: MOVE #PPCN-8 TO #PRINT-PPCN-8
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 11
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
        {
            pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(pnd_I));              //Natural: MOVE #FUND-TO-RECEIVE-S2 ( #I ) TO #FUND-TO-RECEIVE ( #I )
            pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Units_To_Receive_S2().getValue(pnd_I));            //Natural: MOVE #UNITS-TO-RECEIVE-S2 ( #I ) TO #UNITS-TO-RECEIVE ( #I )
            pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Pmts_To_Receive_S2().getValue(pnd_I));              //Natural: MOVE #PMTS-TO-RECEIVE-S2 ( #I ) TO #PMTS-TO-RECEIVE ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_J.setValue(11);                                                                                                                                               //Natural: MOVE 11 TO #J
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_3_Pnd_Fund_To_Receive_S3().getValue(pnd_I));              //Natural: MOVE #FUND-TO-RECEIVE-S3 ( #I ) TO #FUND-TO-RECEIVE ( #J )
            pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_3_Pnd_Units_To_Receive_S3().getValue(pnd_I));            //Natural: MOVE #UNITS-TO-RECEIVE-S3 ( #I ) TO #UNITS-TO-RECEIVE ( #J )
            pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_3_Pnd_Pmts_To_Receive_S3().getValue(pnd_I));              //Natural: MOVE #PMTS-TO-RECEIVE-S3 ( #I ) TO #PMTS-TO-RECEIVE ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 8
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
        {
            //*  WRITE '#FUND-CODE-OUT-S3 (' #I ') = ' #FUND-CODE-OUT-S3 (#I)
            //*  WRITE '#UNITS-OUT-S3 (' #I ') = ' #UNITS-OUT-S3 (#I)
            //*  WRITE '#TRANSFER-AMT-OUT-CREF-S3 (' #I ') = '
            //*    #TRANSFER-AMT-OUT-CREF-S3 (#I)
            pnd_Aian013b_Record_Pnd_Fund_Code_Out.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_4_Pnd_Fund_Code_Out_S4().getValue(pnd_I));                  //Natural: MOVE #FUND-CODE-OUT-S4 ( #I ) TO #FUND-CODE-OUT ( #I )
            pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_4_Pnd_Units_Out_S4().getValue(pnd_I));                          //Natural: MOVE #UNITS-OUT-S4 ( #I ) TO #UNITS-OUT ( #I )
            pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_I).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_4_Pnd_Transfer_Amt_Out_Cref_S4().getValue(pnd_I));  //Natural: MOVE #TRANSFER-AMT-OUT-CREF-S4 ( #I ) TO #TRANSFER-AMT-OUT-CREF ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_J.setValue(8);                                                                                                                                                //Natural: MOVE 8 TO #J
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 8
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(8)); pnd_I.nadd(1))
        {
            //*  ADDED
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pnd_Aian013b_Record_Pnd_Fund_Code_Out.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_5_Pnd_Fund_Code_Out_S5().getValue(pnd_I));                  //Natural: MOVE #FUND-CODE-OUT-S5 ( #I ) TO #FUND-CODE-OUT ( #J )
            pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_5_Pnd_Units_Out_S5().getValue(pnd_I));                          //Natural: MOVE #UNITS-OUT-S5 ( #I ) TO #UNITS-OUT ( #J )
            pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_5_Pnd_Transfer_Amt_Out_Cref_S5().getValue(pnd_I));  //Natural: MOVE #TRANSFER-AMT-OUT-CREF-S5 ( #I ) TO #TRANSFER-AMT-OUT-CREF ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pnd_Aian013b_Record_Pnd_Fund_Code_Out.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Fund_Code_Out_S6().getValue(pnd_I));                  //Natural: MOVE #FUND-CODE-OUT-S6 ( #I ) TO #FUND-CODE-OUT ( #J )
            pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Units_Out_S6().getValue(pnd_I));                          //Natural: MOVE #UNITS-OUT-S6 ( #I ) TO #UNITS-OUT ( #J )
            pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_J).setValue(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Cref_S6().getValue(pnd_I));  //Natural: MOVE #TRANSFER-AMT-OUT-CREF-S6 ( #I ) TO #TRANSFER-AMT-OUT-CREF ( #J )
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            if (condition(pnd_Fund_Letter_Array_Pnd_Fund_Letter.getValue(pnd_I).equals(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Fund_Code_S1())))                         //Natural: IF #FUND-LETTER ( #I ) = #FUND-CODE-S1
            {
                pnd_Fund_Input_Pos.setValue(pnd_I);                                                                                                                       //Natural: MOVE #I TO #FUND-INPUT-POS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE '#FUND-LETTER (' #I ') = ' #FUND-LETTER (#I)
        if (condition(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out().notEquals(pnd_Save_Eff_Date)))                                         //Natural: IF #TRANSFER-EFF-DATE-OUT-S3-OUT <> #SAVE-EFF-DATE
        {
            pnd_Date_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DATE-COUNT
            //*   MOVE #TRANSFER-EFF-DATE-OUT-S2-OUT TO #EFF-DATE (#DATE-COUNT)
            //*     #SAVE-EFF-DATE
            pnd_Save_Eff_Date.setValue(ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out());                                                     //Natural: MOVE #TRANSFER-EFF-DATE-OUT-S3-OUT TO #SAVE-EFF-DATE
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TRANSFER-ARRAYS
            sub_Initialize_Transfer_Arrays();
            if (condition(Global.isEscape())) {return;}
            //*  041509
        }                                                                                                                                                                 //Natural: END-IF
        FOR07:                                                                                                                                                            //Natural: FOR #J = 1 TO 23
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(23)); pnd_J.nadd(1))
        {
            if (condition(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals(" ")))                                                                           //Natural: IF #FUND-TO-RECEIVE ( #J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FUND-POS
            sub_Assign_Fund_Pos();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet865 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #TRANSFER-REVAL-SWITCH-S1;//Natural: VALUE '0'
            if (condition((ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1().equals("0"))))
            {
                decideConditionsMet865++;
                pnd_Revaluation_Ind_Out.setValue(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1());                                                    //Natural: MOVE #REVALUATION-INDICATOR-S1 TO #REVALUATION-IND-OUT
                if (condition(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1().equals("A")))                                                           //Natural: IF #REVALUATION-INDICATOR-S1 = 'A'
                {
                    pnd_Reval_Switch.setValue("AA");                                                                                                                      //Natural: MOVE 'AA' TO #REVAL-SWITCH
                    pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                         //Natural: ADD 1 TO #NUM-TRANSFERS-AA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                    //*        WRITE '#NUM-TRANSFERS-AA (' #FUND-INPUT-POS ',' #FUND-OUTPUT-POS
                    //*          ') = ' #NUM-TRANSFERS-AA (#FUND-INPUT-POS, #FUND-OUTPUT-POS)
                    pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Num_Transfers_Aa.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                    //Natural: ADD 1 TO #TOT-NUM-TRANSFERS-AA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Reval_Switch.setValue("MM");                                                                                                                      //Natural: MOVE 'MM' TO #REVAL-SWITCH
                    pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                                    //Natural: ADD 1 TO #NUM-TRANSFERS-MM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                    pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Num_Transfers_Mm.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                            //Natural: ADD 1 TO #TOT-NUM-TRANSFERS-MM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '1'
            else if (condition((ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1().equals("1"))))
            {
                decideConditionsMet865++;
                pnd_Revaluation_Ind_Out.setValue("A");                                                                                                                    //Natural: MOVE 'A' TO #REVALUATION-IND-OUT
                pnd_Reval_Switch.setValue("MA");                                                                                                                          //Natural: MOVE 'MA' TO #REVAL-SWITCH
                pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                             //Natural: ADD 1 TO #NUM-TRANSFERS-MA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Num_Transfers_Ma.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                        //Natural: ADD 1 TO #TOT-NUM-TRANSFERS-MA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Reval_Switch_S1().equals("2"))))
            {
                decideConditionsMet865++;
                pnd_Revaluation_Ind_Out.setValue("M");                                                                                                                    //Natural: MOVE 'M' TO #REVALUATION-IND-OUT
                pnd_Reval_Switch.setValue("AM");                                                                                                                          //Natural: MOVE 'AM' TO #REVAL-SWITCH
                pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                             //Natural: ADD 1 TO #NUM-TRANSFERS-AM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
                pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Num_Transfers_Am.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(1);                                        //Natural: ADD 1 TO #TOT-NUM-TRANSFERS-AM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                getReports().write(0, "INVALID TRANSFER REVAL SWITCH");                                                                                                   //Natural: WRITE 'INVALID TRANSFER REVAL SWITCH'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_J.setValue(1);                                                                                                                                                //Natural: MOVE 1 TO #J
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FUND-POS
        sub_Assign_Fund_Pos();
        if (condition(Global.isEscape())) {return;}
        //*  101810 ADDED T AND G TO MAKE SURE
        short decideConditionsMet897 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #FUND-TO-RECEIVE-S2 ( 1 );//Natural: VALUE '1','2','T','G'
        if (condition((ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("1") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("2") 
            || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("T") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("G"))))
        {
            decideConditionsMet897++;
            //*  STANDARD AND GRADED ARE RECOGNIZED
            pnd_Tiaa_Pos.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TIAA-POS
            //*    WRITE / 6X #PRINT-PPCN #PAYEE-CODE (EM=99) 1X      /* RBE2
            //*  RBE2
            getReports().write(1, NEWLINE,new ColumnSpacing(6),pnd_Print_Ppcn,pnd_Aian013b_Record_Pnd_Payee_Code, new ReportEditMask ("99"),new ColumnSpacing(1),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Input_Pos),new  //Natural: WRITE ( 1 ) / 6X #PRINT-PPCN #PAYEE-CODE ( EM = 99 ) 1X #FUND-NAME ( #FUND-INPUT-POS ) 1X #REVALUATION-INDICATOR-S1 4X #MODE-S1 3X #OPTION-S1 7X #FUND-NAME ( #FUND-OUTPUT-POS ) 6X #UNITS-TO-RECEIVE ( 1 ) 3X #PMTS-TO-RECEIVE ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 1X #GTD-PMT-OUT-S6 ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 2X #DVD-PMT-OUT-S6 ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) #TRANSFER-AMT-OUT-TIAA-S6 ( 1 ) ( EM = ZZ,ZZZ,ZZ9.99 )
                ColumnSpacing(1),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1(),new ColumnSpacing(4),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Mode_S1(),new 
                ColumnSpacing(3),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Option_S1(),new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Output_Pos),new 
                ColumnSpacing(6),pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(1),new ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(1), 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Gtd_Pmt_Out_S6().getValue(1), new ReportEditMask 
                ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Dvd_Pmt_Out_S6().getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(1), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            //*    ADD #TRANSFER-AMT-OUT-TIAA-S5 (1) TO
            //*      #TRANSFER-AMT-OUT-TIAA
            //*      (#DATE-COUNT, #FUND-INPUT-POS, #FUND-OUTPUT-POS)
            pnd_Num_Lines.nadd(2);                                                                                                                                        //Natural: ADD 2 TO #NUM-LINES
            //*  041509
                                                                                                                                                                          //Natural: PERFORM ADD-TIAA-TRANSFER-AMT
            sub_Add_Tiaa_Transfer_Amt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'C','M','S','B','W','L','E','R','I', 'D'
        else if (condition((ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("C") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("M") 
            || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("S") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("B") 
            || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("W") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("L") 
            || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("E") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("R") 
            || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("I") || ldaAial0132.getPnd_Store_Actrl_Fld_2_Pnd_Fund_To_Receive_S2().getValue(1).equals("D"))))
        {
            decideConditionsMet897++;
            pnd_Cref_Rea_Pos.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CREF-REA-POS
            pnd_Pmts_Out.setValue(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(1));                                                                                   //Natural: MOVE #PMTS-TO-RECEIVE ( 1 ) TO #PMTS-OUT
            //*     WRITE / 6X #PRINT-PPCN #PAYEE-CODE (EM=99) 1X       /* RBE2
            //*  RBE2
            getReports().write(1, NEWLINE,new ColumnSpacing(6),pnd_Print_Ppcn,pnd_Aian013b_Record_Pnd_Payee_Code, new ReportEditMask ("99"),new ColumnSpacing(1),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Input_Pos),new  //Natural: WRITE ( 1 ) / 6X #PRINT-PPCN #PAYEE-CODE ( EM = 99 ) 1X #FUND-NAME ( #FUND-INPUT-POS ) 1X #REVALUATION-INDICATOR-S1 4X #MODE-S1 3X #OPTION-S1 7X #FUND-NAME ( #FUND-OUTPUT-POS ) 2X #REVALUATION-IND-OUT 3X #UNITS-TO-RECEIVE ( 1 ) 3X #PMTS-TO-RECEIVE ( 1 ) ( EM = Z,ZZZ,ZZ9.99 ) 3X #UNITS-OUT ( 1 ) 3X #PMTS-OUT ( EM = Z,ZZZ,ZZ9.99 ) 1X #TRANSFER-AMT-OUT-CREF ( 1 ) ( EM = Z,ZZZ,ZZ9.99 )
                ColumnSpacing(1),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Revaluation_Indicator_S1(),new ColumnSpacing(4),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Mode_S1(),new 
                ColumnSpacing(3),ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Option_S1(),new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Output_Pos),new 
                ColumnSpacing(2),pnd_Revaluation_Ind_Out,new ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(1),new ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(1), 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Units_Out.getValue(1),new ColumnSpacing(3),pnd_Pmts_Out, 
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(1),pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Num_Lines.nadd(2);                                                                                                                                        //Natural: ADD 2 TO #NUM-LINES
            //*    WRITE '#FUND-INPUT-POS  = ' #FUND-INPUT-POS
            //*    WRITE '#FUND-OUTPUT-POS = ' #FUND-OUTPUT-POS
                                                                                                                                                                          //Natural: PERFORM ADD-CREF-TRANSFER-AMT
            sub_Add_Cref_Transfer_Amt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_J.setValue(2);                                                                                                                                                //Natural: MOVE 2 TO #J
        //*    WRITE '#CONTRACT-PAYEE = ' #CONTRACT-PAYEE
        //* *REPEAT UNTIL #FUND-TO-RECEIVE (#J) = ' ' OR #J > 21      /* 041509
        //*  041509
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals(" ") || pnd_J.greater(23))) {break;}                                             //Natural: UNTIL #FUND-TO-RECEIVE ( #J ) = ' ' OR #J > 23
            //*  WRITE '#J = ' #J
            //*  WRITE '#FUND-TO-RECEIVE (' #J ') = ' #FUND-TO-RECEIVE (#J)
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FUND-POS
            sub_Assign_Fund_Pos();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    WRITE '#NUM-TRANSFERS (' #FUND-INPUT-POS ',' #FUND-OUTPUT-POS ') = '
            //*      #NUM-TRANSFERS (#FUND-INPUT-POS, #FUND-OUTPUT-POS)
            //*  101810
            short decideConditionsMet935 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #FUND-TO-RECEIVE ( #J );//Natural: VALUE '1','2','T','G'
            if (condition((pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("1") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("2") 
                || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("T") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("G"))))
            {
                decideConditionsMet935++;
                pnd_Tiaa_Pos.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TIAA-POS
                //*       WRITE '#FUND-NAME (' #FUND-OUTPUT-POS ') = '
                //*       #FUND-NAME (#FUND-OUTPUT-POS)
                //*       WRITE '#TIAA-POS = ' #TIAA-POS
                //*       WRITE '#GTD-PMT-OUT (' #TIAA-POS ') = ' #GTD-PMT-OUT (#TIAA-POS)
                //*       WRITE '#DVD-PMT-OUT (' #TIAA-POS ') = ' #DVD-PMT-OUT (#TIAA-POS)
                //*       WRITE '#TRANSFER-AMT-OUT (' #TIAA-POS ') = '
                //*        #TRANSFER-AMT-OUT-TIAA (#TIAA-POS)
                //*      WRITE 50X #FUND-NAME (#FUND-OUTPUT-POS) 6X            /* RBE2
                //*  RBE2
                getReports().write(1, new ColumnSpacing(50),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Output_Pos),new ColumnSpacing(6),pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(pnd_J),new  //Natural: WRITE ( 1 ) 50X #FUND-NAME ( #FUND-OUTPUT-POS ) 6X #UNITS-TO-RECEIVE ( #J ) 3X #PMTS-TO-RECEIVE ( #J ) ( EM = Z,ZZZ,ZZZ.99 ) 1X 1X #GTD-PMT-OUT-S6 ( #TIAA-POS ) ( EM = Z,ZZZ,ZZZ.99 ) 3X #DVD-PMT-OUT-S6 ( #TIAA-POS ) ( EM = Z,ZZZ,ZZZ.99 ) 2X #TRANSFER-AMT-OUT-TIAA-S6 ( #TIAA-POS ) ( EM = Z,ZZZ,ZZZ.99 )
                    ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new ColumnSpacing(1),new 
                    ColumnSpacing(1),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Gtd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(3),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Dvd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(2),ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(pnd_Tiaa_Pos), new ReportEditMask ("Z,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      ADD #TRANSFER-AMT-OUT-TIAA-S5 (#TIAA-POS) TO
                //*        #TRANSFER-AMT-OUT-TIAA
                //*        (#DATE-COUNT, #FUND-INPUT-POS, #FUND-OUTPUT-POS)
                pnd_Num_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-LINES
                                                                                                                                                                          //Natural: PERFORM ADD-TIAA-TRANSFER-AMT
                sub_Add_Tiaa_Transfer_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                          //Natural: IF #NUM-LINES NOT < 57
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                    sub_Write_Headings();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  041509
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'C','M','S','B','W','L','E','R','I','D'
            else if (condition((pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("C") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("M") 
                || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("S") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("B") 
                || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("W") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("L") 
                || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("E") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("R") 
                || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("I") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("D"))))
            {
                decideConditionsMet935++;
                pnd_Cref_Rea_Pos.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CREF-REA-POS
                pnd_Pmts_Out.setValue(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_J));                                                                           //Natural: MOVE #PMTS-TO-RECEIVE ( #J ) TO #PMTS-OUT
                //*       WRITE 50X #FUND-NAME (#FUND-OUTPUT-POS) 6X           /* RBE2
                //*  RBE2
                getReports().write(1, new ColumnSpacing(50),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_Fund_Output_Pos),new ColumnSpacing(6),pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(pnd_J),new  //Natural: WRITE ( 1 ) 50X #FUND-NAME ( #FUND-OUTPUT-POS ) 6X #UNITS-TO-RECEIVE ( #J ) 3X #PMTS-TO-RECEIVE ( #J ) ( EM = Z,ZZZ,ZZZ.99 ) 3X #UNITS-OUT ( #CREF-REA-POS ) 3X #PMTS-OUT ( EM = Z,ZZZ,ZZZ.99 ) 1X #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) ( EM = Z,ZZZ,ZZZ.99 )
                    ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZZ.99"),new ColumnSpacing(3),pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_Cref_Rea_Pos),new 
                    ColumnSpacing(3),pnd_Pmts_Out, new ReportEditMask ("Z,ZZZ,ZZZ.99"),new ColumnSpacing(1),pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos), 
                    new ReportEditMask ("Z,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      WRITE '#FUND-INPUT-POS  = ' #FUND-INPUT-POS
                //*      WRITE '#FUND-OUTPUT-POS = ' #FUND-OUTPUT-POS
                pnd_Num_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-LINES
                                                                                                                                                                          //Natural: PERFORM ADD-CREF-TRANSFER-AMT
                sub_Add_Cref_Transfer_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                          //Natural: IF #NUM-LINES NOT < 57
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                    sub_Write_Headings();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  IF #FUND-INPUT-POS = 3
        //*    WRITE '#TRANSFER-UNITS = ' #TRANSFER-UNITS
        //*    WRITE '#UNITS-OUTGOING (' #FUND-INPUT-POS ') = '
        //*      #UNITS-OUTGOING (#FUND-INPUT-POS)
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM ADD-OUTGOING-AMTS
        sub_Add_Outgoing_Amts();
        if (condition(Global.isEscape())) {return;}
        //*  041509
        pnd_Tiaa_Pos.setValue(0);                                                                                                                                         //Natural: MOVE 0 TO #TIAA-POS #CREF-REA-POS #PMT-INDEX
        pnd_Cref_Rea_Pos.setValue(0);
        pnd_Pmt_Index.setValue(0);
        FOR08:                                                                                                                                                            //Natural: FOR #J = 1 TO 23
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(23)); pnd_J.nadd(1))
        {
            if (condition(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals(" ")))                                                                           //Natural: IF #FUND-TO-RECEIVE ( #J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FUND-POS
            sub_Assign_Fund_Pos();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ADD-INCOMING-AMTS
            sub_Add_Incoming_Amts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  END-FIND
        pnd_Save_Transfer_Eff_Date.setValue(ldaAial0132.getPnd_Store_Actrl_Fld_3_Pnd_Transfer_Eff_Date_Out_S3());                                                         //Natural: MOVE #TRANSFER-EFF-DATE-OUT-S3 TO #SAVE-TRANSFER-EFF-DATE
        //*  END-WORK
    }
    private void sub_Write_Summary_Reports() throws Exception                                                                                                             //Natural: WRITE-SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date," EFFECTIVE DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X, //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE ' EFFECTIVE DATE ' #SAVE-EFF-DATE-X /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "1) PERIODIC PAYMENT/UNIT TRANSFER SUMMARY");                                                                                               //Natural: WRITE ( 1 ) '1) PERIODIC PAYMENT/UNIT TRANSFER SUMMARY'
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(40),"FROM:",new ColumnSpacing(30),"TO:",new ColumnSpacing(33),"NET:");                                                    //Natural: WRITE ( 1 ) 40X 'FROM:' 30X 'TO:' 33X 'NET:'
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(31),"UNITS",new ColumnSpacing(11),"VAR PMTS",new ColumnSpacing(10),"UNITS",new ColumnSpacing(11),"VAR PMTS",new           //Natural: WRITE ( 1 ) 31X 'UNITS' 11X 'VAR PMTS' 10X 'UNITS' 11X 'VAR PMTS' 9X 'UNITS' 14X 'VAR PMTS'
            ColumnSpacing(9),"UNITS",new ColumnSpacing(14),"VAR PMTS");
        if (Global.isEscape()) return;
        //*  041509
        getReports().write(1, new ColumnSpacing(29),"OR TIAA GUAR",new ColumnSpacing(5),"OR TIAA DIV",new ColumnSpacing(6),"OR TIAA GUAR",new ColumnSpacing(5),"OR TIAA DIV",new  //Natural: WRITE ( 1 ) 29X 'OR TIAA GUAR' 5X 'OR TIAA DIV' 6X 'OR TIAA GUAR' 5X 'OR TIAA DIV' 6X 'OR TIAA GUAR' 7X 'OR TIAA DIV'
            ColumnSpacing(6),"OR TIAA GUAR",new ColumnSpacing(7),"OR TIAA DIV");
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1) || pnd_I.equals(2)))                                                                                                            //Natural: IF #I = 1 OR = 2
            {
                getReports().write(1, new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I),new ColumnSpacing(46),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_I),  //Natural: WRITE ( 1 ) 7X #FUND-NAME ( #I ) 46X #GTD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 3X #DVD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 2X #GTD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 5X #DVD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new 
                    ColumnSpacing(5),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Net_Units.compute(new ComputeParameters(true, pnd_Net_Units), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-UNITS = #UNITS-INCOMING-A ( #I ) - #UNITS-OUTGOING-A-1 ( #I )
                pnd_Net_Pmts.compute(new ComputeParameters(true, pnd_Net_Pmts), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-PMTS = #PMTS-INCOMING-A ( #I ) - #PMTS-OUTGOING-A ( #I )
                //*    WRITE '#UNITS-INCOMING-A (' #I ') = ' #UNITS-INCOMING-A (#I)
                //*    WRITE '#UNITS-OUTGOING-A (' #I ') = ' #UNITS-OUTGOING-A (#I)
                getReports().write(1, new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL ",new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1.getValue(pnd_I),  //Natural: WRITE ( 1 ) 7X #FUND-NAME ( #I ) ' ANNUAL ' 4X #UNITS-OUTGOING-A-1 ( #I ) ( EM = -ZZ,ZZZ,ZZZ.999 ) 2X #PMTS-OUTGOING-A ( #I ) ( EM = -ZZ,ZZZ,ZZZ.99 ) 2X #UNITS-INCOMING-A ( #I ) ( EM = -ZZ,ZZZ,ZZZ.999 ) 2X #PMTS-INCOMING-A ( #I ) ( EM = -ZZ,ZZZ,ZZZ.99 ) 2X #NET-UNITS ( EM = -ZZ,ZZZ,ZZZ.999 ) 4X #NET-PMTS ( EM = -ZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_I), new ReportEditMask 
                    ("-ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new 
                    ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Net_Units, 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Net_Pmts, new ReportEditMask ("-ZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            pnd_Net_Units.compute(new ComputeParameters(true, pnd_Net_Units), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-UNITS = #UNITS-INCOMING-M ( #I ) - #UNITS-OUTGOING-M-1 ( #I )
            pnd_Net_Pmts.compute(new ComputeParameters(true, pnd_Net_Pmts), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-PMTS = #PMTS-INCOMING-M ( #I ) - #PMTS-OUTGOING-M ( #I )
            //*    WRITE '#UNITS-INCOMING-M (' #I ') = ' #UNITS-INCOMING-M (#I)
            //*    WRITE '#UNITS-OUTGOING-M (' #I ') = ' #UNITS-OUTGOING-M (#I)
            //*  WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY' 4X
            getReports().write(1, new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY",new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1.getValue(pnd_I),  //Natural: WRITE ( 1 ) 7X #FUND-NAME ( #I ) ' MONTHLY' 4X #UNITS-OUTGOING-M-1 ( #I ) ( EM = -ZZ,ZZZ,ZZZ.999 ) 2X #PMTS-OUTGOING-M ( #I ) ( EM = -ZZ,ZZZ,ZZZ.99 ) 2X #UNITS-INCOMING-M ( #I ) ( EM = -ZZ,ZZZ,ZZZ.999 ) 2X #PMTS-INCOMING-M ( #I ) ( EM = -ZZ,ZZZ,ZZZ.99 ) 2X #NET-UNITS ( EM = -ZZ,ZZZ,ZZZ.999 ) 4X #NET-PMTS ( EM = -ZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_I), new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new 
                ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ.99"),new ColumnSpacing(2),pnd_Net_Units, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(4),pnd_Net_Pmts, new ReportEditMask ("-ZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Num_Lines.setValue(57);                                                                                                                                       //Natural: MOVE 57 TO #NUM-LINES
        //*  041509
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-HEADINGS
        sub_Write_Summary_Headings();
        if (condition(Global.isEscape())) {return;}
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  041509
            pnd_Prt_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #PRT-SWITCH
            FOR12:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                //*      WRITE '#NUM-LINES = ' #NUM-LINES
                if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                          //Natural: IF #NUM-LINES NOT < 57
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-HEADINGS
                    sub_Write_Summary_Headings();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1037 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #J;//Natural: VALUE 1, 2
                if (condition((pnd_J.equals(1) || pnd_J.equals(2))))
                {
                    decideConditionsMet1037++;
                    if (condition(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,pnd_J).notEquals(getZero())))                             //Natural: IF #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) NOT = 0
                    {
                        pnd_Prt_Switch.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #PRT-SWITCH
                        //*      WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY TO ' #FUND-NAME (#J)
                        getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' MONTHLY TO ' #FUND-NAME ( #J ) 19X #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                            ColumnSpacing(19),pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  DY1
                        if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                          //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                        {
                            //*  DY1
                            //*  DY1
                            pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,pnd_J));                        //Natural: ADD #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) TO #TRANSFER-AMT-ARRAY-TOT
                            //*  DY1
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Num_Lines.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NUM-LINES
                        //*  041509
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 3:12
                else if (condition(((pnd_J.greaterOrEqual(3) && pnd_J.lessOrEqual(12)))))
                {
                    decideConditionsMet1037++;
                    if (condition(pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,pnd_J).notEquals(getZero())))                                   //Natural: IF #TRANSFER-AMT-ARRAY-MM ( #I, #J ) NOT = 0
                    {
                        pnd_Prt_Switch.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #PRT-SWITCH
                        getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J)," MONTHLY",new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' MONTHLY TO ' #FUND-NAME ( #J ) ' MONTHLY' 10X #TRANSFER-AMT-ARRAY-MM ( #I, #J ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                            ColumnSpacing(10),pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  DY1
                        if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                          //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                        {
                            //*  DY1
                            //*  DY1
                            pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,pnd_J));                              //Natural: ADD #TRANSFER-AMT-ARRAY-MM ( #I, #J ) TO #TRANSFER-AMT-ARRAY-TOT
                            //*  DY1
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Num_Lines.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NUM-LINES
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Prt_Switch.equals("Y")))                                                                                                                    //Natural: IF #PRT-SWITCH = 'Y'
            {
                getReports().write(1, NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Num_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-LINES
            }                                                                                                                                                             //Natural: END-IF
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  041509
            pnd_Prt_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #PRT-SWITCH
            FOR14:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                //*      WRITE '#NUM-LINES = ' #NUM-LINES
                if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                          //Natural: IF #NUM-LINES NOT < 57
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-HEADINGS
                    sub_Write_Summary_Headings();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1084 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #J;//Natural: VALUE 1, 2
                if (condition((pnd_J.equals(1) || pnd_J.equals(2))))
                {
                    decideConditionsMet1084++;
                    if (condition(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,pnd_J).notEquals(getZero())))                             //Natural: IF #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) NOT = 0
                    {
                        pnd_Prt_Switch.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #PRT-SWITCH
                        //*      WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY TO ' #FUND-NAME (#J)
                        getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL  TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' ANNUAL  TO ' #FUND-NAME ( #J ) 19X #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                            ColumnSpacing(19),pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  DY1
                        if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                          //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                        {
                            //*  DY1
                            //*  DY1
                            pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,pnd_J));                        //Natural: ADD #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) TO #TRANSFER-AMT-ARRAY-TOT
                            //*  DY1
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Num_Lines.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NUM-LINES
                        //*  041509
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 3:12
                else if (condition(((pnd_J.greaterOrEqual(3) && pnd_J.lessOrEqual(12)))))
                {
                    decideConditionsMet1084++;
                    //*        WRITE '#TRANSFER-AMT-ARRAY-AA (' #I ',' #J ') = '
                    //*          #TRANSFER-AMT-ARRAY-AA (#I, #J)
                    if (condition(pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,pnd_J).notEquals(getZero())))                                   //Natural: IF #TRANSFER-AMT-ARRAY-AA ( #I, #J ) NOT = 0
                    {
                        pnd_Prt_Switch.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #PRT-SWITCH
                        //*      WRITE (2) 10X #FUND-NAME (#I) ' ANNUAL  TO ' #FUND-NAME (#J)
                        getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL  TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J)," ANNUAL ",new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' ANNUAL  TO ' #FUND-NAME ( #J ) ' ANNUAL ' 10X #TRANSFER-AMT-ARRAY-AA ( #I, #J ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                            ColumnSpacing(10),pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  DY1
                        if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                          //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                        {
                            //*  DY1
                            //*  DY1
                            pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,pnd_J));                              //Natural: ADD #TRANSFER-AMT-ARRAY-AA ( #I, #J ) TO #TRANSFER-AMT-ARRAY-TOT
                            //*  DY1
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Num_Lines.nadd(1);                                                                                                                            //Natural: ADD 1 TO #NUM-LINES
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Prt_Switch.equals("Y")))                                                                                                                    //Natural: IF #PRT-SWITCH = 'Y'
            {
                getReports().write(1, NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Num_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-LINES
            }                                                                                                                                                             //Natural: END-IF
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR15:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            pnd_Prt_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #PRT-SWITCH
            //*      WRITE '#NUM-LINES = ' #NUM-LINES
            if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                              //Natural: IF #NUM-LINES NOT < 57
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-HEADINGS
                sub_Write_Summary_Headings();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,pnd_I).notEquals(getZero())))                                           //Natural: IF #TRANSFER-AMT-ARRAY-MA ( #I, #I ) NOT = 0
            {
                pnd_Prt_Switch.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #PRT-SWITCH
                //*    WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY TO ' #FUND-NAME (#J)
                getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL ",new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' MONTHLY TO ' #FUND-NAME ( #I ) ' ANNUAL ' 10X #TRANSFER-AMT-ARRAY-MA ( #I, #I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    ColumnSpacing(10),pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  DY1
                if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                                  //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                {
                    //*      ADD #TRANSFER-AMT-ARRAY-MA (#I, #J) TO            /* DY1 & DY2
                    //*  DY2
                    //*  DY1
                    pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,pnd_I));                                      //Natural: ADD #TRANSFER-AMT-ARRAY-MA ( #I, #I ) TO #TRANSFER-AMT-ARRAY-TOT
                    //*  DY1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Num_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #NUM-LINES
            }                                                                                                                                                             //Natural: END-IF
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR16:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  041509
            pnd_Prt_Switch.setValue("N");                                                                                                                                 //Natural: MOVE 'N' TO #PRT-SWITCH
            FOR17:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                if (condition(pnd_Num_Lines.greaterOrEqual(57)))                                                                                                          //Natural: IF #NUM-LINES NOT < 57
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-HEADINGS
                    sub_Write_Summary_Headings();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  WRITE '#TRANSFER-AMT-ARRAY-AM (' #I ',' #J ') = '
                //*   #TRANSFER-AMT-ARRAY-MA (#I, #J)
                if (condition(pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,pnd_J).notEquals(getZero())))                                       //Natural: IF #TRANSFER-AMT-ARRAY-AM ( #I, #J ) NOT = 0
                {
                    pnd_Prt_Switch.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #PRT-SWITCH
                    //*    WRITE (2) 10X #FUND-NAME (#I) ' ANNUAL  TO ' #FUND-NAME (#J)
                    getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL  TO ",pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J)," MONTHLY",new  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' ANNUAL  TO ' #FUND-NAME ( #J ) ' MONTHLY' 10X #TRANSFER-AMT-ARRAY-AM ( #I, #J ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                        ColumnSpacing(10),pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DY1
                    if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                              //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
                    {
                        //*  DY1
                        //*  DY1
                        pnd_Transfer_Amt_Array_Tot.nadd(pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,pnd_J));                                  //Natural: ADD #TRANSFER-AMT-ARRAY-AM ( #I, #J ) TO #TRANSFER-AMT-ARRAY-TOT
                        //*  DY1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Num_Lines.nadd(1);                                                                                                                                //Natural: ADD 1 TO #NUM-LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DY1
        if (condition(pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X.equals("ALL     ")))                                                                                          //Natural: IF #SAVE-EFF-DATE-X = 'ALL     '
        {
            //*  DY1
            getReports().write(1, "  ");                                                                                                                                  //Natural: WRITE ( 1 ) '  '
            if (Global.isEscape()) return;
            //*  DY1
            //*  DY1
            getReports().write(1, new ColumnSpacing(10),"GRAND TOTAL:",new ColumnSpacing(33),pnd_Transfer_Amt_Array_Tot, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ.99"));      //Natural: WRITE ( 1 ) 10X 'GRAND TOTAL:' 33X #TRANSFER-AMT-ARRAY-TOT ( EM = ZZZ,ZZZ,ZZZ,ZZZ.99 )
            if (Global.isEscape()) return;
            //*  DY1
        }                                                                                                                                                                 //Natural: END-IF
        //*  041509
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR18:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR19:                                                                                                                                                        //Natural: FOR #J = 1 TO 2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
            {
                pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,     //Natural: ADD #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) TO #NET-TRANSFER-TIAA ( #J )
                    pnd_J));
                pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,     //Natural: ADD #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) TO #NET-TRANSFER-TIAA ( #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I, //Natural: SUBTRACT #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) FROM #NET-TRANSFER-M ( #I )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I, //Natural: SUBTRACT #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) FROM #NET-TRANSFER-A ( #I )
                    pnd_J));
                //*    WRITE '#TRANSFER-AMT-ARRAY-AA (' #I ',' #J ') = '
                //*     #TRANSFER-AMT-ARRAY-AA (#I, #J)
                //*    WRITE '#NET-TRANSFER-TIAA (' #J ') = ' #NET-TRANSFER-TIAA (#J)
                //*  041509
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR20:                                                                                                                                                        //Natural: FOR #J = 3 TO 12
            for (pnd_J.setValue(3); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,               //Natural: ADD #TRANSFER-AMT-ARRAY-MA ( #I, #J ) TO #NET-REVAL-A ( #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,          //Natural: SUBTRACT #TRANSFER-AMT-ARRAY-MA ( #I, #J ) FROM #NET-REVAL-M ( #I )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,               //Natural: ADD #TRANSFER-AMT-ARRAY-AM ( #I, #J ) TO #NET-REVAL-M ( #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,          //Natural: SUBTRACT #TRANSFER-AMT-ARRAY-AM ( #I, #J ) FROM #NET-REVAL-A ( #I )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,            //Natural: ADD #TRANSFER-AMT-ARRAY-AA ( #I, #J ) TO #NET-TRANSFER-A ( #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,       //Natural: SUBTRACT #TRANSFER-AMT-ARRAY-AA ( #I, #J ) FROM #NET-TRANSFER-A ( #I )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_J).nadd(pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,            //Natural: ADD #TRANSFER-AMT-ARRAY-MM ( #I, #J ) TO #NET-TRANSFER-M ( #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_I).nsubtract(pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,       //Natural: SUBTRACT #TRANSFER-AMT-ARRAY-MM ( #I, #J ) FROM #NET-TRANSFER-M ( #I )
                    pnd_J));
                //*    WRITE '#TRANSFER-AMT-ARRAY-AA (' #I ',' #J ') = '
                //*     #TRANSFER-AMT-ARRAY-AA (#I, #J)
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE (2) '3/31/96 ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR '
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date," EFFECTIVE DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X, //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE ' EFFECTIVE DATE ' #SAVE-EFF-DATE-X /
            NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE (2) /'3) NET ASSET TRANSFER SUMMARY REPORT'//49X 'AMOUNT'/
        getReports().write(1, NEWLINE,"3) NET ASSET TRANSFER SUMMARY REPORT",NEWLINE,NEWLINE,new ColumnSpacing(67),"REVALUATION");                                        //Natural: WRITE ( 1 ) /'3) NET ASSET TRANSFER SUMMARY REPORT'//67X 'REVALUATION'
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(49),"TRANSFERS",new ColumnSpacing(10),"SWITCHES",new ColumnSpacing(19),"TOTAL",NEWLINE);                                  //Natural: WRITE ( 1 ) 49X 'TRANSFERS' 10X 'SWITCHES' 19X 'TOTAL'/
        if (Global.isEscape()) return;
        FOR21:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            //*  WRITE (2) 10X #FUND-NAME (#I) 26X
            getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I),new ColumnSpacing(27),pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa.getValue(pnd_I),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) 27X #NET-TRANSFER-TIAA ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 ) 31X #NET-TRANSFER-TIAA ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("-ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(31),pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa.getValue(pnd_I), new ReportEditMask 
                ("-ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR22:                                                                                                                                                            //Natural: FOR #I = 3 TO 12
        for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  WRITE (2) 10X #FUND-NAME (#I) ' ANNUAL ' 18X
            pnd_Net_Trans_Reval.compute(new ComputeParameters(true, pnd_Net_Trans_Reval), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_I).add(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-TRANS-REVAL = #NET-TRANSFER-A ( #I ) + #NET-REVAL-A ( #I )
            getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL ",new ColumnSpacing(18),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_I),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' ANNUAL ' 18X #NET-TRANSFER-A ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 ) 5X #NET-REVAL-A ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 ) 11X #NET-TRANS-REVAL ( EM = -ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("-ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(5),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A.getValue(pnd_I), new ReportEditMask 
                ("-ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(11),pnd_Net_Trans_Reval, new ReportEditMask ("-ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR23:                                                                                                                                                            //Natural: FOR #I = 3 TO 12
        for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY ' 17X
            pnd_Net_Trans_Reval.compute(new ComputeParameters(true, pnd_Net_Trans_Reval), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_I).add(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-TRANS-REVAL = #NET-TRANSFER-M ( #I ) + #NET-REVAL-M ( #I )
            getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY ",new ColumnSpacing(17),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_I),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' MONTHLY ' 17X #NET-TRANSFER-M ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 ) 5X #NET-REVAL-M ( #I ) ( EM = -ZZZ,ZZZ,ZZZ.99 ) 11X #NET-TRANS-REVAL ( EM = -ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("-ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(5),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M.getValue(pnd_I), new ReportEditMask 
                ("-ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(11),pnd_Net_Trans_Reval, new ReportEditMask ("-ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  WRITE (2) '4) NET PERIODIC PAYMENT/UNIT TRANSFER SUMMARY FOR '
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date," EFFECTIVE DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X, //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE ' EFFECTIVE DATE ' #SAVE-EFF-DATE-X /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "4) NET PERIODIC PAYMENT/UNIT TRANSFER SUMMARY");                                                                                           //Natural: WRITE ( 1 ) '4) NET PERIODIC PAYMENT/UNIT TRANSFER SUMMARY'
        if (Global.isEscape()) return;
        //*  WRITE (2) 39X 'UNITS'/
        getReports().write(1, new ColumnSpacing(39),"UNITS",new ColumnSpacing(14),"VAR PMTS",NEWLINE);                                                                    //Natural: WRITE ( 1 ) 39X 'UNITS'14X 'VAR PMTS'/
        if (Global.isEscape()) return;
        //*  WRITE (2) 38X 'OR TIAA GUAR' 7X 'OR TIAA DIV'/
        //*  041509
        getReports().write(1, new ColumnSpacing(38),"OR TIAA GUAR",new ColumnSpacing(7),"OR TIAA DIV",NEWLINE);                                                           //Natural: WRITE ( 1 ) 38X 'OR TIAA GUAR' 7X 'OR TIAA DIV'/
        if (Global.isEscape()) return;
        FOR24:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1) || pnd_I.equals(2)))                                                                                                            //Natural: IF #I = 1 OR = 2
            {
                getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I),new ColumnSpacing(15),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_I),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) 15X #GTD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 ) 5X #DVD-INCOMING ( #I ) ( EM = ZZZ,ZZZ,ZZZ.99 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(5),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Net_Units.compute(new ComputeParameters(true, pnd_Net_Units), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-UNITS = #UNITS-INCOMING-A ( #I ) - #UNITS-OUTGOING-A ( #I )
                pnd_Net_Pmts.compute(new ComputeParameters(true, pnd_Net_Pmts), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-PMTS = #PMTS-INCOMING-A ( #I ) - #PMTS-OUTGOING-A ( #I )
                //*    WRITE '#UNITS-INCOMING-A (' #I ') = ' #UNITS-INCOMING-A (#I)
                //*    WRITE '#UNITS-OUTGOING-A (' #I ') = ' #UNITS-OUTGOING-A (#I)
                getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," ANNUAL ",new ColumnSpacing(5),pnd_Net_Units,              //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' ANNUAL ' 5X #NET-UNITS ( EM = -ZZ,ZZZ,ZZZ.999 ) 5X #NET-PMTS ( EM = -ZZ,ZZZ,ZZZ.999 )
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(5),pnd_Net_Pmts, new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR25:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            pnd_Net_Units.compute(new ComputeParameters(true, pnd_Net_Units), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-UNITS = #UNITS-INCOMING-M ( #I ) - #UNITS-OUTGOING-M ( #I )
            pnd_Net_Pmts.compute(new ComputeParameters(true, pnd_Net_Pmts), pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_I).subtract(pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_I))); //Natural: COMPUTE ROUNDED #NET-PMTS = #PMTS-INCOMING-M ( #I ) - #PMTS-OUTGOING-M ( #I )
            //*    WRITE '#UNITS-INCOMING-M (' #I ') = ' #UNITS-INCOMING-M (#I)
            //*    WRITE '#UNITS-OUTGOING-M (' #I ') = ' #UNITS-OUTGOING-M (#I)
            //*  WRITE (2) 10X #FUND-NAME (#I) ' MONTHLY' 4X
            getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_I)," MONTHLY",new ColumnSpacing(5),pnd_Net_Units,                  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #I ) ' MONTHLY' 5X #NET-UNITS ( EM = -ZZ,ZZZ,ZZZ.999 ) 5X #NET-PMTS ( EM = -ZZ,ZZZ,ZZZ.999 )
                new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"),new ColumnSpacing(5),pnd_Net_Pmts, new ReportEditMask ("-ZZ,ZZZ,ZZZ.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  041509
        //*  041509
        pnd_Prt_Msg.setValue("MONTHLY TO MONTHLY ");                                                                                                                      //Natural: MOVE 'MONTHLY TO MONTHLY ' TO #PRT-MSG
        FOR26:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR27:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                if (condition(pnd_I.equals(pnd_J)))                                                                                                                       //Natural: IF #I = #J
                {
                    pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(0);                                                                                 //Natural: MOVE 0 TO #NUM-TRANSFERS ( #I, #J )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm.getValue(pnd_I,pnd_J));                    //Natural: MOVE #NUM-TRANSFERS-MM ( #I, #J ) TO #NUM-TRANSFERS ( #I, #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-NUM-TRANSFERS-C
        sub_Write_Summary_Num_Transfers_C();
        if (condition(Global.isEscape())) {return;}
        //*  041509
        //*  041509
        pnd_Prt_Msg.setValue("ANNUAL  TO ANNUAL  ");                                                                                                                      //Natural: MOVE 'ANNUAL  TO ANNUAL  ' TO #PRT-MSG
        FOR28:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR29:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                if (condition(pnd_I.equals(pnd_J)))                                                                                                                       //Natural: IF #I = #J
                {
                    pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(0);                                                                                 //Natural: MOVE 0 TO #NUM-TRANSFERS ( #I, #J )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa.getValue(pnd_I,                 //Natural: MOVE #NUM-TRANSFERS-AA ( #I, #J ) TO #NUM-TRANSFERS ( #I, #J )
                        pnd_J));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-NUM-TRANSFERS-C
        sub_Write_Summary_Num_Transfers_C();
        if (condition(Global.isEscape())) {return;}
        //*  041509
        //*  041509
        pnd_Prt_Msg.setValue("MONTHLY TO ANNUAL  ");                                                                                                                      //Natural: MOVE 'MONTHLY TO ANNUAL  ' TO #PRT-MSG
        FOR30:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR31:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma.getValue(pnd_I,pnd_J));             //Natural: MOVE #NUM-TRANSFERS-MA ( #I, #J ) TO #NUM-TRANSFERS ( #I, #J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-NUM-TRANSFERS-C
        sub_Write_Summary_Num_Transfers_C();
        if (condition(Global.isEscape())) {return;}
        //*  041509
        //*  041509
        pnd_Prt_Msg.setValue("ANNUAL  TO MONTHLY ");                                                                                                                      //Natural: MOVE 'ANNUAL  TO MONTHLY ' TO #PRT-MSG
        FOR32:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR33:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).setValue(pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am.getValue(pnd_I,pnd_J));             //Natural: MOVE #NUM-TRANSFERS-AM ( #I, #J ) TO #NUM-TRANSFERS ( #I, #J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-NUM-TRANSFERS-C
        sub_Write_Summary_Num_Transfers_C();
        if (condition(Global.isEscape())) {return;}
        //*  FOR #J = 1 TO 2
        //*   FOR #I = 3 TO 11
        //*     COMPUTE #NUM-TRANSFERS-TIAA (#I, #J) =
        //*       #NUM-TRANSFERS-TIAA (#I, #J) + #NUM-TRANSFERS-MM (#I, #J)
        //*       + #NUM-TRANSFERS-AA (#I, #J)
        //*   END-FOR
        //*  END-FOR
        //*  FOR #I = 1 TO 11
        //*   COMPUTE ROUNDED #TOTAL-NUM-TRANSFERS =
        //*     #TOTAL-NUM-TRANSFERS + #NUM-TRANSFERS-TIAA (#I, #J)
        //*   IF #NUM-TRANSFERS (#I, #J) �= 0
        //*     MOVE 'Y' TO #NONZERO-SWITCH
        //*   END-IF
        //*  END-FOR
        //*  IF #NONZERO-SWITCH = 'Y'
        //*    PERFORM WRITE-SUMMARY-NUM-TRANSFERS-T
        //*  END-IF
    }
    private void sub_Write_Summary_Num_Transfers_C() throws Exception                                                                                                     //Natural: WRITE-SUMMARY-NUM-TRANSFERS-C
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_First_Write_Switch.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #FIRST-WRITE-SWITCH
        //*  WRITE (2) '3/31/96 ACTUARIAL CREF/REA IA TRANSFER SUMMARY' 60X /
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date,new ColumnSpacing(32),                    //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE 32X/ '5) SUMMARY OF TRANSFER REQUESTS PROCESSED'
            NEWLINE,"5) SUMMARY OF TRANSFER REQUESTS PROCESSED");
        if (Global.isEscape()) return;
        //*  WRITE (2) '5) SUMMARY OF TRANSFER REQUESTS PROCESSED' 60X /
        getReports().write(1, new ColumnSpacing(60),NEWLINE);                                                                                                             //Natural: WRITE ( 1 ) 60X /
        if (Global.isEscape()) return;
        //*  ITE (2) 'NUMBER' 29X 'NUMBER OF ' #PRT-MSG ' TRANSFERS FOR EFFECTIVE '
        getReports().write(1, "NUMBER",new ColumnSpacing(29),"NUMBER OF ",pnd_Prt_Msg," TRANSFERS FOR EFFECTIVE ","DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X,          //Natural: WRITE ( 1 ) 'NUMBER' 29X 'NUMBER OF ' #PRT-MSG ' TRANSFERS FOR EFFECTIVE ' 'DATE ' #SAVE-EFF-DATE-X ' FROM:'
            " FROM:");
        if (Global.isEscape()) return;
        //*  WRITE (2) 'OF'
        getReports().write(1, "OF");                                                                                                                                      //Natural: WRITE ( 1 ) 'OF'
        if (Global.isEscape()) return;
        //*  WRITE (2) 'TRANSFERS' 12X 'STOCK      MMA     SOCIAL     BOND     '
        //*  041509
        getReports().write(1, "TRANSFERS",new ColumnSpacing(12),"STOCK      MMA    SOCIAL     BOND     GLOBAL   ","GROWTH       EQX      ILB      REA    ACCESS     TOTAL"); //Natural: WRITE ( 1 ) 'TRANSFERS' 12X 'STOCK      MMA    SOCIAL     BOND     GLOBAL   ' 'GROWTH       EQX      ILB      REA    ACCESS     TOTAL'
        if (Global.isEscape()) return;
        FOR34:                                                                                                                                                            //Natural: FOR #J = 1 TO 12
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
        {
            pnd_Total_Num_Transfers.setValue(0);                                                                                                                          //Natural: MOVE 0 TO #TOTAL-NUM-TRANSFERS
            //*  WRITE '#NUM-TRANSFERS (1, ' #J ') = ' #NUM-TRANSFERS (1, #J)
            //*  WRITE '#NUM-TRANSFERS (2, ' #J ') = ' #NUM-TRANSFERS (2, #J)
            //*  WRITE '#NUM-TRANSFERS (3, ' #J ') = ' #NUM-TRANSFERS (3, #J)
            //*  WRITE '#NUM-TRANSFERS (4, ' #J ') = ' #NUM-TRANSFERS (4, #J)
            //*  WRITE '#NUM-TRANSFERS (5, ' #J ') = ' #NUM-TRANSFERS (5, #J)
            //*  WRITE '#NUM-TRANSFERS (6, ' #J ') = ' #NUM-TRANSFERS (6, #J)
            //*  WRITE '#NUM-TRANSFERS (7, ' #J ') = ' #NUM-TRANSFERS (7, #J)
            //*  WRITE '#NUM-TRANSFERS (8, ' #J ') = ' #NUM-TRANSFERS (8, #J)
            //*  041509
            pnd_Nonzero_Switch.setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #NONZERO-SWITCH
            FOR35:                                                                                                                                                        //Natural: FOR #I = 1 TO 12
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
            {
                pnd_Total_Num_Transfers.nadd(pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J));                                                                   //Natural: COMPUTE ROUNDED #TOTAL-NUM-TRANSFERS = #TOTAL-NUM-TRANSFERS + #NUM-TRANSFERS ( #I, #J )
                //*    WRITE '#NUM-TRANSFERS (' #I ',' #J ') = ' #NUM-TRANSFERS (#I, #J)
                if (condition(pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J).notEquals(getZero())))                                                             //Natural: IF #NUM-TRANSFERS ( #I, #J ) <> 0
                {
                    pnd_Nonzero_Switch.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #NONZERO-SWITCH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Nonzero_Switch.equals("N")))                                                                                                                //Natural: IF #NONZERO-SWITCH = 'N'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*    IF #J = 1
            if (condition(pnd_J.equals(1)))                                                                                                                               //Natural: IF #J = 1
            {
                //*    WRITE (2) 'TO:' 7X #FUND-NAME (#J) 1X
                //*  041509
                getReports().write(1, "TO:",new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new ColumnSpacing(1),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(3,pnd_J),  //Natural: WRITE ( 1 ) 'TO:' 7X #FUND-NAME ( #J ) 1X #NUM-TRANSFERS ( 3, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 4, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 5, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 6, #J ) ( EM = ZZZ,ZZZ ) 4X #NUM-TRANSFERS ( 7, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 8, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 9, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 10, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 11, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 12, #J ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ ) /
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(4,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(5,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(6,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(7,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(8,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(9,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(10,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(11,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(12,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Total_Num_Transfers, new ReportEditMask ("ZZZ,ZZZ"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First_Write_Switch.setValue("N");                                                                                                                     //Natural: MOVE 'N' TO #FIRST-WRITE-SWITCH
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_First_Write_Switch.equals("Y")))                                                                                                        //Natural: IF #FIRST-WRITE-SWITCH = 'Y'
                {
                    //*  041509
                    getReports().write(1, "TO:",new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new ColumnSpacing(1),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(3,pnd_J),  //Natural: WRITE ( 1 ) 'TO:' 7X #FUND-NAME ( #J ) 1X #NUM-TRANSFERS ( 3, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 4, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 5, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 6, #J ) ( EM = ZZZ,ZZZ ) 4X #NUM-TRANSFERS ( 7, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 8, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 9, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 10, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 11, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 12, #J ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ ) /
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(4,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(5,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(6,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(7,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(8,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(9,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(10,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(11,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(12,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Total_Num_Transfers, new ReportEditMask ("ZZZ,ZZZ"),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_First_Write_Switch.setValue("N");                                                                                                                 //Natural: MOVE 'N' TO #FIRST-WRITE-SWITCH
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  041509
                    getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new ColumnSpacing(1),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(3,pnd_J),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #J ) 1X #NUM-TRANSFERS ( 3, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 4, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 5, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 6, #J ) ( EM = ZZZ,ZZZ ) 4X #NUM-TRANSFERS ( 7, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 8, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS ( 9, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 10, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 11, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS ( 12, #J ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ ) /
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(4,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(5,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(6,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(7,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(8,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(9,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(10,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                        ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(11,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_Pnd_Num_Transfers.getValue(12,pnd_J), 
                        new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Total_Num_Transfers, new ReportEditMask ("ZZZ,ZZZ"),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  041509
        pnd_Grand_Total_Num_Transfers.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #GRAND-TOTAL-NUM-TRANSFERS
        FOR36:                                                                                                                                                            //Natural: FOR #I = 3 TO 12
        for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(pnd_I).setValue(0);                                                                       //Natural: MOVE 0 TO #TOTAL-NUM-FROM-TRANSFERS ( #I )
            //*  041509
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR37:                                                                                                                                                            //Natural: FOR #I = 3 TO 12
        for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  041509
            pnd_Total_Num_Transfers.setValue(0);                                                                                                                          //Natural: MOVE 0 TO #TOTAL-NUM-TRANSFERS
            FOR38:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(pnd_I).nadd(pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J));                //Natural: COMPUTE ROUNDED #TOTAL-NUM-FROM-TRANSFERS ( #I ) = #TOTAL-NUM-FROM-TRANSFERS ( #I ) + #NUM-TRANSFERS ( #I, #J )
                pnd_Grand_Total_Num_Transfers.nadd(pnd_Transfer_Num_Pnd_Num_Transfers.getValue(pnd_I,pnd_J));                                                             //Natural: COMPUTE ROUNDED #GRAND-TOTAL-NUM-TRANSFERS = #GRAND-TOTAL-NUM-TRANSFERS + #NUM-TRANSFERS ( #I, #J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  041509
        getReports().write(1, new ColumnSpacing(10),"TOTAL",new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(3),                  //Natural: WRITE ( 1 ) 10X 'TOTAL'4X #TOTAL-NUM-FROM-TRANSFERS ( 3 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 4 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 5 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 6 ) ( EM = ZZZ,ZZZ ) 4X #TOTAL-NUM-FROM-TRANSFERS ( 7 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 8 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 9 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 10 ) ( EM = ZZZ,ZZZ ) 1X #TOTAL-NUM-FROM-TRANSFERS ( 11 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 12 ) ( EM = ZZZ,ZZZ ) 2X #GRAND-TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ )
            new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(4), new ReportEditMask 
            ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(5), new ReportEditMask ("ZZZ,ZZZ"),new 
            ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(6), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(7), 
            new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(8), new ReportEditMask 
            ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(9), new ReportEditMask ("ZZZ,ZZZ"),new 
            ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(10), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(1),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(11), 
            new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(12), new ReportEditMask 
            ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Grand_Total_Num_Transfers, new ReportEditMask ("ZZZ,ZZZ"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Summary_Num_Transfers_T() throws Exception                                                                                                     //Natural: WRITE-SUMMARY-NUM-TRANSFERS-T
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  WRITE (2) '3/31/96 ACTUARIAL CREF/REA IA TRANSFER SUMMARY' 60X /
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date,new ColumnSpacing(40),                    //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE 40X/ '5) SUMMARY OF TRANSFER REQUESTS PROCESSED'
            NEWLINE,"5) SUMMARY OF TRANSFER REQUESTS PROCESSED");
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(60),NEWLINE);                                                                                                             //Natural: WRITE ( 1 ) 60X /
        if (Global.isEscape()) return;
        //*  WRITE (2) 'NUMBER' 29X 'NUMBER OF TRANSFERS FOR EFFECTIVE DATE '
        getReports().write(1, "NUMBER",new ColumnSpacing(29),"NUMBER OF TRANSFERS FOR EFFECTIVE DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X," FROM:");                   //Natural: WRITE ( 1 ) 'NUMBER' 29X 'NUMBER OF TRANSFERS FOR EFFECTIVE DATE ' #SAVE-EFF-DATE-X ' FROM:'
        if (Global.isEscape()) return;
        //*  WRITE (2) 'OF'
        getReports().write(1, "OF");                                                                                                                                      //Natural: WRITE ( 1 ) 'OF'
        if (Global.isEscape()) return;
        //*  WRITE (2) 'TRANSFERS' 13X 'STOCK     MMA     SOCIAL     BOND     '
        getReports().write(1, "TRANSFERS",new ColumnSpacing(13),"TIAA STD TIAA GRD STOCK     MMA     SOCIAL     ","BOND     GLOBAL   GROWTH     EQX     REA     TOTAL");  //Natural: WRITE ( 1 ) 'TRANSFERS' 13X 'TIAA STD TIAA GRD STOCK     MMA     SOCIAL     ' 'BOND     GLOBAL   GROWTH     EQX     REA     TOTAL'
        if (Global.isEscape()) return;
        FOR39:                                                                                                                                                            //Natural: FOR #J = 1 TO 2
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
        {
            pnd_Total_Num_Transfers.setValue(0);                                                                                                                          //Natural: MOVE 0 TO #TOTAL-NUM-TRANSFERS
            //*  WRITE '#NUM-TRANSFERS (1, ' #J ') = ' #NUM-TRANSFERS (1, #J)
            //*  WRITE '#NUM-TRANSFERS (2, ' #J ') = ' #NUM-TRANSFERS (2, #J)
            //*  WRITE '#NUM-TRANSFERS (3, ' #J ') = ' #NUM-TRANSFERS (3, #J)
            //*  WRITE '#NUM-TRANSFERS (4, ' #J ') = ' #NUM-TRANSFERS (4, #J)
            //*  WRITE '#NUM-TRANSFERS (5, ' #J ') = ' #NUM-TRANSFERS (5, #J)
            //*  WRITE '#NUM-TRANSFERS (6, ' #J ') = ' #NUM-TRANSFERS (6, #J)
            //*  WRITE '#NUM-TRANSFERS (7, ' #J ') = ' #NUM-TRANSFERS (7, #J)
            //*  WRITE '#NUM-TRANSFERS (8, ' #J ') = ' #NUM-TRANSFERS (8, #J)
            pnd_Nonzero_Switch.setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #NONZERO-SWITCH
            //*    IF #J = 1
            if (condition(pnd_J.equals(1)))                                                                                                                               //Natural: IF #J = 1
            {
                //*    WRITE (2) 'TO:' 7X #FUND-NAME (#J) 1X
                getReports().write(1, "TO:",new ColumnSpacing(7),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new ColumnSpacing(1),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(1,pnd_J),  //Natural: WRITE ( 1 ) 'TO:' 7X #FUND-NAME ( #J ) 1X #NUM-TRANSFERS-TIAA ( 1, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 2, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 3, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 4, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 5, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 6, #J ) ( EM = ZZZ,ZZZ ) 4X #NUM-TRANSFERS-TIAA ( 7, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 8, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 9, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 10, #J ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ ) /
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(2,pnd_J), new ReportEditMask 
                    ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(3,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(4,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(5,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(6,pnd_J), new ReportEditMask 
                    ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(7,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(8,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(9,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(10,pnd_J), new 
                    ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Total_Num_Transfers, new ReportEditMask ("ZZZ,ZZZ"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    WRITE (2) 10X #FUND-NAME (#J) 1X
                getReports().write(1, new ColumnSpacing(10),pnd_Fund_Name_Array_Pnd_Fund_Name.getValue(pnd_J),new ColumnSpacing(1),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(3,pnd_J),  //Natural: WRITE ( 1 ) 10X #FUND-NAME ( #J ) 1X #NUM-TRANSFERS-TIAA ( 3, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 4, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 5, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 6, #J ) ( EM = ZZZ,ZZZ ) 4X #NUM-TRANSFERS-TIAA ( 7, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 8, #J ) ( EM = ZZZ,ZZZ ) 3X #NUM-TRANSFERS-TIAA ( 9, #J ) ( EM = ZZZ,ZZZ ) 2X #NUM-TRANSFERS-TIAA ( 10, #J ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ ) /
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(4,pnd_J), new ReportEditMask 
                    ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(5,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(6,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(7,pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(8,pnd_J), new ReportEditMask 
                    ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(9,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new 
                    ColumnSpacing(2),pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(10,pnd_J), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Total_Num_Transfers, 
                    new ReportEditMask ("ZZZ,ZZZ"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR40:                                                                                                                                                            //Natural: FOR #I = 3 TO 11
        for (pnd_I.setValue(3); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
        {
            pnd_Total_Num_Transfers.setValue(0);                                                                                                                          //Natural: MOVE 0 TO #TOTAL-NUM-TRANSFERS
            FOR41:                                                                                                                                                        //Natural: FOR #J = 3 TO 11
            for (pnd_J.setValue(3); condition(pnd_J.lessOrEqual(11)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(pnd_I).nadd(pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(pnd_I,   //Natural: COMPUTE ROUNDED #TOTAL-NUM-FROM-TRANSFERS ( #I ) = #TOTAL-NUM-FROM-TRANSFERS ( #I ) + #NUM-TRANSFERS-TIAA ( #I, #J )
                    pnd_J));
                pnd_Grand_Total_Num_Transfers.nadd(pnd_Transfer_Num_By_Fund_In_Tiaa_Pnd_Num_Transfers_Tiaa.getValue(pnd_I,pnd_J));                                        //Natural: COMPUTE ROUNDED #GRAND-TOTAL-NUM-TRANSFERS = #GRAND-TOTAL-NUM-TRANSFERS + #NUM-TRANSFERS-TIAA ( #I, #J )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE (2) 10X 'TOTAL'4X
        getReports().write(1, new ColumnSpacing(10),"TOTAL",new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(3),                  //Natural: WRITE ( 1 ) 10X 'TOTAL'4X #TOTAL-NUM-FROM-TRANSFERS ( 3 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 4 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 5 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 6 ) ( EM = ZZZ,ZZZ ) 4X #TOTAL-NUM-FROM-TRANSFERS ( 7 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 8 ) ( EM = ZZZ,ZZZ ) 3X #TOTAL-NUM-FROM-TRANSFERS ( 9 ) ( EM = ZZZ,ZZZ ) 2X #TOTAL-NUM-FROM-TRANSFERS ( 10 ) ( EM = ZZZ,ZZZ ) 2X #GRAND-TOTAL-NUM-TRANSFERS ( EM = ZZZ,ZZZ )
            new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(4), new ReportEditMask 
            ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(5), new ReportEditMask ("ZZZ,ZZZ"),new 
            ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(6), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(4),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(7), 
            new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(8), new ReportEditMask 
            ("ZZZ,ZZZ"),new ColumnSpacing(3),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(9), new ReportEditMask ("ZZZ,ZZZ"),new 
            ColumnSpacing(2),pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(10), new ReportEditMask ("ZZZ,ZZZ"),new ColumnSpacing(2),pnd_Grand_Total_Num_Transfers, 
            new ReportEditMask ("ZZZ,ZZZ"));
        if (Global.isEscape()) return;
    }
    private void sub_Assign_Fund_Pos() throws Exception                                                                                                                   //Natural: ASSIGN-FUND-POS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  101810 START
        if (condition(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("1") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("T")))        //Natural: IF #FUND-TO-RECEIVE ( #J ) = '1' OR = 'T'
        {
            pnd_Fund_Output_Pos.setValue(1);                                                                                                                              //Natural: ASSIGN #FUND-OUTPUT-POS := 1
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("2") || pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J).equals("G")))        //Natural: IF #FUND-TO-RECEIVE ( #J ) = '2' OR = 'G'
        {
            pnd_Fund_Output_Pos.setValue(2);                                                                                                                              //Natural: ASSIGN #FUND-OUTPUT-POS := 2
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  101810 END
            //*  041509
        }                                                                                                                                                                 //Natural: END-IF
        FOR42:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  WRITE '#FUND-LETTER (' #I ') = ' #FUND-LETTER (#I)
            if (condition(pnd_Fund_Letter_Array_Pnd_Fund_Letter.getValue(pnd_I).equals(pnd_Aian013b_Record_Pnd_Fund_To_Receive.getValue(pnd_J))))                         //Natural: IF #FUND-LETTER ( #I ) = #FUND-TO-RECEIVE ( #J )
            {
                pnd_Fund_Output_Pos.setValue(pnd_I);                                                                                                                      //Natural: MOVE #I TO #FUND-OUTPUT-POS
                //*    WRITE '#FUND-OUTPUT-POS = ' #FUND-OUTPUT-POS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date," EFFECTIVE DATE ",ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out(), //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE ' EFFECTIVE DATE ' #TRANSFER-EFF-DATE-OUT-S3-OUT /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, "1) SERIATIM DETAIL",NEWLINE);                                                                                                              //Natural: WRITE ( 1 ) '1) SERIATIM DETAIL' /
        if (Global.isEscape()) return;
        getReports().write(1, "    FROM:",new ColumnSpacing(41),"TO:",new ColumnSpacing(20),"RESULT:",NEWLINE);                                                           //Natural: WRITE ( 1 ) '    FROM:' 41X 'TO:' 20X 'RESULT:'/
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(25),"REVAL",new ColumnSpacing(28),"REVAL",new ColumnSpacing(31),"NEW UNITS",new ColumnSpacing(6),"NEW VAR PMTS",new       //Natural: WRITE ( 1 ) 25X 'REVAL' 28X 'REVAL' 31X 'NEW UNITS' 6X 'NEW VAR PMTS' 2X 'ASSET'
            ColumnSpacing(2),"ASSET");
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(6),"CONTRACT",new ColumnSpacing(4),"FUND",new ColumnSpacing(4),"IND",new ColumnSpacing(3),"MODE",new ColumnSpacing(2),"OPTION",new  //Natural: WRITE ( 1 ) 6X 'CONTRACT' 4X 'FUND' 4X 'IND' 3X 'MODE' 2X 'OPTION' 6X 'FUND' 5X 'IND' 4X 'OLD UNITS' 6X 'OLD PMTS' 5X 'OR TIAA GUAR' 3X 'OR TIAA DIV' 2X 'TRANSFER'/
            ColumnSpacing(6),"FUND",new ColumnSpacing(5),"IND",new ColumnSpacing(4),"OLD UNITS",new ColumnSpacing(6),"OLD PMTS",new ColumnSpacing(5),"OR TIAA GUAR",new 
            ColumnSpacing(3),"OR TIAA DIV",new ColumnSpacing(2),"TRANSFER",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Num_Lines.setValue(11);                                                                                                                                       //Natural: MOVE 11 TO #NUM-LINES
    }
    private void sub_Write_Summary_Headings() throws Exception                                                                                                            //Natural: WRITE-SUMMARY-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  WRITE (2) '3/31/96 ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR '
        getReports().write(1, "ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ",pnd_Input_Record_Pnd_Input_End_Date," EFFECTIVE DATE ",pnd_Save_Eff_Date_Pnd_Save_Eff_Date_X, //Natural: WRITE ( 1 ) 'ACTUARIAL CREF/REA IA TRANSFER SUMMARY FOR PROCESSING DATE ' #INPUT-END-DATE ' EFFECTIVE DATE ' #SAVE-EFF-DATE-X /
            NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE (2) /'2) ASSET TRANSFER SUMMARY REPORT'//16X 'DIRECTION' 22X
        getReports().write(1, NEWLINE,"2) ASSET TRANSFER SUMMARY REPORT",NEWLINE,NEWLINE,new ColumnSpacing(25),"DIRECTION",new ColumnSpacing(31),"AMOUNT",                //Natural: WRITE ( 1 ) /'2) ASSET TRANSFER SUMMARY REPORT'//25X 'DIRECTION' 31X 'AMOUNT'/
            NEWLINE);
        if (Global.isEscape()) return;
        pnd_Num_Lines.setValue(11);                                                                                                                                       //Natural: MOVE 11 TO #NUM-LINES
    }
    private void sub_Add_Cref_Transfer_Amt() throws Exception                                                                                                             //Natural: ADD-CREF-TRANSFER-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  WRITE 'ADD-CREF-TRANSFER-AMT'
        short decideConditionsMet1482 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #REVAL-SWITCH;//Natural: VALUE 'AA'
        if (condition((pnd_Reval_Switch.equals("AA"))))
        {
            decideConditionsMet1482++;
            pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TRANSFER-AMT-ARRAY-AA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Transfer_Amt_Array_Aa.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TOT-TRANSFER-AMT-ARRAY-AA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: VALUE 'MM'
        else if (condition((pnd_Reval_Switch.equals("MM"))))
        {
            decideConditionsMet1482++;
            pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TRANSFER-AMT-ARRAY-MM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Transfer_Amt_Array_Mm.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TOT-TRANSFER-AMT-ARRAY-MM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: VALUE 'AM'
        else if (condition((pnd_Reval_Switch.equals("AM"))))
        {
            decideConditionsMet1482++;
            pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TRANSFER-AMT-ARRAY-AM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Transfer_Amt_Array_Am.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TOT-TRANSFER-AMT-ARRAY-AM ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: VALUE 'MA'
        else if (condition((pnd_Reval_Switch.equals("MA"))))
        {
            decideConditionsMet1482++;
            pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TRANSFER-AMT-ARRAY-MA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Transfer_Amt_Array_Ma.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Transfer_Amt_Out_Cref.getValue(pnd_Cref_Rea_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-CREF ( #CREF-REA-POS ) TO #TOT-TRANSFER-AMT-ARRAY-MA ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, "INVALID TRANSFER REVAL SWITCH");                                                                                                       //Natural: WRITE 'INVALID TRANSFER REVAL SWITCH'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Add_Tiaa_Transfer_Amt() throws Exception                                                                                                             //Natural: ADD-TIAA-TRANSFER-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        short decideConditionsMet1502 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #REVAL-SWITCH;//Natural: VALUE 'AA', 'AM'
        if (condition((pnd_Reval_Switch.equals("AA") || pnd_Reval_Switch.equals("AM"))))
        {
            decideConditionsMet1502++;
            pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(pnd_Tiaa_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-TIAA-S6 ( #TIAA-POS ) TO #TRANSFER-AMT-TIAA-ARRAY-A ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_A.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(pnd_Tiaa_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-TIAA-S6 ( #TIAA-POS ) TO #TOT-TRANSFER-AMT-TIAA-ARRAY-A ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: VALUE 'MM', 'MA'
        else if (condition((pnd_Reval_Switch.equals("MM") || pnd_Reval_Switch.equals("MA"))))
        {
            decideConditionsMet1502++;
            pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(pnd_Tiaa_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-TIAA-S6 ( #TIAA-POS ) TO #TRANSFER-AMT-TIAA-ARRAY-M ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_M.getValue(pnd_Fund_Input_Pos,pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Transfer_Amt_Out_Tiaa_S6().getValue(pnd_Tiaa_Pos)); //Natural: ADD #TRANSFER-AMT-OUT-TIAA-S6 ( #TIAA-POS ) TO #TOT-TRANSFER-AMT-TIAA-ARRAY-M ( #FUND-INPUT-POS, #FUND-OUTPUT-POS )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Add_Incoming_Amts() throws Exception                                                                                                                 //Natural: ADD-INCOMING-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE 'ADD-INCOMING-AMTS'
        pnd_Pmt_Index.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #PMT-INDEX
        short decideConditionsMet1517 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #FUND-OUTPUT-POS;//Natural: VALUE 1, 2
        if (condition((pnd_Fund_Output_Pos.equals(1) || pnd_Fund_Output_Pos.equals(2))))
        {
            decideConditionsMet1517++;
            pnd_Tiaa_Pos.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TIAA-POS
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Gtd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos)); //Natural: COMPUTE ROUNDED #GTD-INCOMING ( #FUND-OUTPUT-POS ) = #GTD-INCOMING ( #FUND-OUTPUT-POS ) + #GTD-PMT-OUT-S6 ( #TIAA-POS )
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming.getValue(pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Gtd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos)); //Natural: COMPUTE ROUNDED #TOT-GTD-INCOMING ( #FUND-OUTPUT-POS ) = #TOT-GTD-INCOMING ( #FUND-OUTPUT-POS ) + #GTD-PMT-OUT-S6 ( #TIAA-POS )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Dvd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos)); //Natural: COMPUTE ROUNDED #DVD-INCOMING ( #FUND-OUTPUT-POS ) = #DVD-INCOMING ( #FUND-OUTPUT-POS ) + #DVD-PMT-OUT-S6 ( #TIAA-POS )
            //*  041509
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming.getValue(pnd_Fund_Output_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_6_Pnd_Dvd_Pmt_Out_S6().getValue(pnd_Tiaa_Pos)); //Natural: COMPUTE ROUNDED #TOT-DVD-INCOMING ( #FUND-OUTPUT-POS ) = #TOT-DVD-INCOMING ( #FUND-OUTPUT-POS ) + #DVD-PMT-OUT-S6 ( #TIAA-POS )
        }                                                                                                                                                                 //Natural: VALUE 3:12
        else if (condition(((pnd_Fund_Output_Pos.greaterOrEqual(3) && pnd_Fund_Output_Pos.lessOrEqual(12)))))
        {
            decideConditionsMet1517++;
            pnd_Cref_Rea_Pos.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CREF-REA-POS
            //*    WRITE '#CREF-REA-POS = ' #CREF-REA-POS
            //*    WRITE '#REVAL-SWITCH = ' #REVAL-SWITCH
            //*    WRITE '#UNITS-OUT (' #CREF-REA-POS ') = '
            //*     #UNITS-OUT (#CREF-REA-POS)
            //*    WRITE '#PMTS-TO-RECEIVE (' #CREF-REA-POS ') = '
            //*      #PMTS-TO-RECEIVE (#CREF-REA-POS)
            short decideConditionsMet1533 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #REVAL-SWITCH;//Natural: VALUE 'MA', 'AA'
            if (condition((pnd_Reval_Switch.equals("MA") || pnd_Reval_Switch.equals("AA"))))
            {
                decideConditionsMet1533++;
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_Cref_Rea_Pos));      //Natural: COMPUTE ROUNDED #UNITS-INCOMING-A ( #FUND-OUTPUT-POS ) = #UNITS-INCOMING-A ( #FUND-OUTPUT-POS ) + #UNITS-OUT ( #CREF-REA-POS )
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_Pmt_Index));    //Natural: COMPUTE ROUNDED #PMTS-INCOMING-A ( #FUND-OUTPUT-POS ) = #PMTS-INCOMING-A ( #FUND-OUTPUT-POS ) + #PMTS-TO-RECEIVE ( #PMT-INDEX )
                pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_Cref_Rea_Pos)); //Natural: COMPUTE ROUNDED #TOT-UNITS-INCOMING-A ( #FUND-OUTPUT-POS ) = #TOT-UNITS-INCOMING-A ( #FUND-OUTPUT-POS ) + #UNITS-OUT ( #CREF-REA-POS )
                pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_Pmt_Index)); //Natural: COMPUTE ROUNDED #TOT-PMTS-INCOMING-A ( #FUND-OUTPUT-POS ) = #TOT-PMTS-INCOMING-A ( #FUND-OUTPUT-POS ) + #PMTS-TO-RECEIVE ( #PMT-INDEX )
            }                                                                                                                                                             //Natural: VALUE 'AM', 'MM'
            else if (condition((pnd_Reval_Switch.equals("AM") || pnd_Reval_Switch.equals("MM"))))
            {
                decideConditionsMet1533++;
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_Cref_Rea_Pos));      //Natural: COMPUTE ROUNDED #UNITS-INCOMING-M ( #FUND-OUTPUT-POS ) = #UNITS-INCOMING-M ( #FUND-OUTPUT-POS ) + #UNITS-OUT ( #CREF-REA-POS )
                pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_Pmt_Index));    //Natural: COMPUTE ROUNDED #PMTS-INCOMING-M ( #FUND-OUTPUT-POS ) = #PMTS-INCOMING-M ( #FUND-OUTPUT-POS ) + #PMTS-TO-RECEIVE ( #PMT-INDEX )
                pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Units_Out.getValue(pnd_Cref_Rea_Pos)); //Natural: COMPUTE ROUNDED #TOT-UNITS-INCOMING-M ( #FUND-OUTPUT-POS ) = #TOT-UNITS-INCOMING-M ( #FUND-OUTPUT-POS ) + #UNITS-OUT ( #CREF-REA-POS )
                pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M.getValue(pnd_Fund_Output_Pos).nadd(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_Pmt_Index)); //Natural: COMPUTE ROUNDED #TOT-PMTS-INCOMING-M ( #FUND-OUTPUT-POS ) = #TOT-PMTS-INCOMING-M ( #FUND-OUTPUT-POS ) + #PMTS-TO-RECEIVE ( #PMT-INDEX )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Add_Outgoing_Amts() throws Exception                                                                                                                 //Natural: ADD-OUTGOING-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE 'ADD-OUTGOING-AMTS'
        //*  WRITE '#REVAL-SWITCH = ' #REVAL-SWITCH
        //*  WRITE '#FIRST-ANN-DOB- = ' #TRANSFER-UNITS
        //*  WRITE '#TRANSFER-UNITS = ' #TRANSFER-UNITS
        //*  041509
        pnd_Total_Pmts_Out.setValue(0);                                                                                                                                   //Natural: MOVE 0 TO #TOTAL-PMTS-OUT #TOTAL-UNITS-OUT
        pnd_Total_Units_Out.setValue(0);
        FOR43:                                                                                                                                                            //Natural: FOR #I = 1 TO 23
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(23)); pnd_I.nadd(1))
        {
            pnd_Total_Pmts_Out.nadd(pnd_Aian013b_Record_Pnd_Pmts_To_Receive.getValue(pnd_I));                                                                             //Natural: COMPUTE ROUNDED #TOTAL-PMTS-OUT = #TOTAL-PMTS-OUT + #PMTS-TO-RECEIVE ( #I )
            pnd_Total_Units_Out.nadd(pnd_Aian013b_Record_Pnd_Units_To_Receive.getValue(pnd_I));                                                                           //Natural: COMPUTE ROUNDED #TOTAL-UNITS-OUT = #TOTAL-UNITS-OUT + #UNITS-TO-RECEIVE ( #I )
            //*   WRITE '#PMTS-TO-RECEIVE (' #I ') = ' #PMTS-TO-RECEIVE (#I)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE '#TOTAL-PMTS-OUT = ' #TOTAL-PMTS-OUT
        short decideConditionsMet1565 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #REVAL-SWITCH;//Natural: VALUE 'MA', 'MM'
        if (condition((pnd_Reval_Switch.equals("MA") || pnd_Reval_Switch.equals("MM"))))
        {
            decideConditionsMet1565++;
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M.getValue(pnd_Fund_Input_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1());           //Natural: COMPUTE ROUNDED #UNITS-OUTGOING-M ( #FUND-INPUT-POS ) = #UNITS-OUTGOING-M ( #FUND-INPUT-POS ) + #TRANSFER-UNITS-S1
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Units_Out);                                                  //Natural: COMPUTE ROUNDED #UNITS-OUTGOING-M-1 ( #FUND-INPUT-POS ) = #UNITS-OUTGOING-M-1 ( #FUND-INPUT-POS ) + #TOTAL-UNITS-OUT
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Pmts_Out);                                                      //Natural: COMPUTE ROUNDED #PMTS-OUTGOING-M ( #FUND-INPUT-POS ) = #PMTS-OUTGOING-M ( #FUND-INPUT-POS ) + #TOTAL-PMTS-OUT
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M.getValue(pnd_Fund_Input_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1());      //Natural: COMPUTE ROUNDED #TOT-UNITS-OUTGOING-M ( #FUND-INPUT-POS ) = #TOT-UNITS-OUTGOING-M ( #FUND-INPUT-POS ) + #TRANSFER-UNITS-S1
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Units_Out);                                             //Natural: COMPUTE ROUNDED #TOT-UNITS-OUTGOING-M-1 ( #FUND-INPUT-POS ) = #TOT-UNITS-OUTGOING-M-1 ( #FUND-INPUT-POS ) + #TOTAL-UNITS-OUT
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Pmts_Out);                                                 //Natural: COMPUTE ROUNDED #TOT-PMTS-OUTGOING-M ( #FUND-INPUT-POS ) = #TOT-PMTS-OUTGOING-M ( #FUND-INPUT-POS ) + #TOTAL-PMTS-OUT
        }                                                                                                                                                                 //Natural: VALUE 'AM', 'AA'
        else if (condition((pnd_Reval_Switch.equals("AM") || pnd_Reval_Switch.equals("AA"))))
        {
            decideConditionsMet1565++;
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A.getValue(pnd_Fund_Input_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1());           //Natural: COMPUTE ROUNDED #UNITS-OUTGOING-A ( #FUND-INPUT-POS ) = #UNITS-OUTGOING-A ( #FUND-INPUT-POS ) + #TRANSFER-UNITS-S1
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Units_Out);                                                  //Natural: COMPUTE ROUNDED #UNITS-OUTGOING-A-1 ( #FUND-INPUT-POS ) = #UNITS-OUTGOING-A-1 ( #FUND-INPUT-POS ) + #TOTAL-UNITS-OUT
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Pmts_Out);                                                      //Natural: COMPUTE ROUNDED #PMTS-OUTGOING-A ( #FUND-INPUT-POS ) = #PMTS-OUTGOING-A ( #FUND-INPUT-POS ) + #TOTAL-PMTS-OUT
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A.getValue(pnd_Fund_Input_Pos).nadd(ldaAial0132.getPnd_Store_Actrl_Fld_1_Pnd_Transfer_Units_S1());      //Natural: COMPUTE ROUNDED #TOT-UNITS-OUTGOING-A ( #FUND-INPUT-POS ) = #TOT-UNITS-OUTGOING-A ( #FUND-INPUT-POS ) + #TRANSFER-UNITS-S1
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Units_Out);                                             //Natural: COMPUTE ROUNDED #TOT-UNITS-OUTGOING-A-1 ( #FUND-INPUT-POS ) = #TOT-UNITS-OUTGOING-A-1 ( #FUND-INPUT-POS ) + #TOTAL-UNITS-OUT
            pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A.getValue(pnd_Fund_Input_Pos).nadd(pnd_Total_Pmts_Out);                                                 //Natural: COMPUTE ROUNDED #TOT-PMTS-OUTGOING-A ( #FUND-INPUT-POS ) = #TOT-PMTS-OUTGOING-A ( #FUND-INPUT-POS ) + #TOTAL-PMTS-OUT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Cntrl_Sd_Cde.setValue("DC");                                                                                                                                  //Natural: MOVE 'DC' TO #CNTRL-SD-CDE
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-SD-CDE
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Sd_Cde, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_cntrl.readNextRow("READ02")))
        {
            //*  WRITE 'CNTRL = ' CNTRL
            pnd_Input_Record_Pnd_Input_Begin_Date_A.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #INPUT-BEGIN-DATE-A
            pnd_Input_Record_Pnd_Input_End_Date.setValue(pnd_Input_Record_Pnd_Input_Begin_Date);                                                                          //Natural: MOVE #INPUT-BEGIN-DATE TO #INPUT-END-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WRITE 'BEGIN-DATE' #INPUT-BEGIN-DATE
        pnd_Date1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Input_Begin_Month, "/", pnd_Input_Record_Pnd_Input_Begin_Day,             //Natural: COMPRESS #INPUT-BEGIN-MONTH '/' #INPUT-BEGIN-DAY '/' #INPUT-BEGIN-YEAR INTO #DATE1 LEAVING NO SPACE
            "/", pnd_Input_Record_Pnd_Input_Begin_Year));
    }
    private void sub_Initialize_Transfer_Arrays() throws Exception                                                                                                        //Natural: INITIALIZE-TRANSFER-ARRAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  041509
        pnd_Grand_Total_Num_Transfers.setValue(0);                                                                                                                        //Natural: MOVE 0 TO #GRAND-TOTAL-NUM-TRANSFERS
        FOR44:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            //*  041509
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M.getValue(pnd_I).setValue(0);                                                                               //Natural: MOVE 0 TO #UNITS-OUTGOING-M ( #I ) #UNITS-OUTGOING-A ( #I ) #UNITS-OUTGOING-A-1 ( #I ) #UNITS-OUTGOING-M ( #I ) #UNITS-OUTGOING-M-1 ( #I ) #PMTS-OUTGOING-M ( #I ) #PMTS-OUTGOING-A ( #I ) #GTD-INCOMING ( #I ) #DVD-INCOMING ( #I ) #UNITS-INCOMING-A ( #I ) #UNITS-INCOMING-M ( #I ) #PMTS-INCOMING-M ( #I ) #PMTS-INCOMING-A ( #I ) #TOTAL-NUM-FROM-TRANSFERS ( #I ) #NET-TRANSFER-M ( #I ) #NET-TRANSFER-A ( #I ) #NET-REVAL-A ( #I ) #NET-REVAL-M ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Total_Num_From_Transfers.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_M.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Transfer_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_A.getValue(pnd_I).setValue(0);
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Net_Reval_M.getValue(pnd_I).setValue(0);
            FOR45:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,pnd_J).setValue(0);                                                               //Natural: MOVE 0 TO #TRANSFER-AMT-ARRAY-MM ( #I, #J ) #TRANSFER-AMT-ARRAY-MA ( #I, #J ) #TRANSFER-AMT-ARRAY-AM ( #I, #J ) #TRANSFER-AMT-ARRAY-AA ( #I, #J ) #NUM-TRANSFERS-MM ( #I, #J ) #NUM-TRANSFERS-MA ( #I, #J ) #NUM-TRANSFERS-AM ( #I, #J ) #NUM-TRANSFERS-AA ( #I, #J )
                pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am.getValue(pnd_I,pnd_J).setValue(0);
                pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa.getValue(pnd_I,pnd_J).setValue(0);
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR46:                                                                                                                                                        //Natural: FOR #J = 1 TO 2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,pnd_J).setValue(0);                                                         //Natural: MOVE 0 TO #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J )
                pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,pnd_J).setValue(0);
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR47:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pnd_Transfer_Tiaa_By_Method_Pnd_Net_Transfer_Tiaa.getValue(pnd_I).setValue(0);                                                                                //Natural: MOVE 0 TO #NET-TRANSFER-TIAA ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  WRITE 'WRITE-GRAND-TOTALS'
        //*  041509
        pnd_Grand_Total_Switch.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #GRAND-TOTAL-SWITCH
        FOR48:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            FOR49:                                                                                                                                                        //Natural: FOR #J = 1 TO 2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(2)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_A.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J ) TO #TRANSFER-AMT-TIAA-ARRAY-A ( #I, #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_Tiaa_Pnd_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Tiaa_Pnd_Tot_Transfer_Amt_Tiaa_Array_M.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J ) TO #TRANSFER-AMT-TIAA-ARRAY-M ( #I, #J )
                    pnd_J));
                //*  041509
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR50:                                                                                                                                                        //Natural: FOR #J = 1 TO 12
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
            {
                pnd_Transfer_Amt_By_Fund_In_Mm_Pnd_Transfer_Amt_Array_Mm.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Mm_Pnd_Tot_Transfer_Amt_Array_Mm.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-ARRAY-MM ( #I, #J ) TO #TRANSFER-AMT-ARRAY-MM ( #I, #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_Ma_Pnd_Transfer_Amt_Array_Ma.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Ma_Pnd_Tot_Transfer_Amt_Array_Ma.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-ARRAY-MA ( #I, #J ) TO #TRANSFER-AMT-ARRAY-MA ( #I, #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_Am_Pnd_Transfer_Amt_Array_Am.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Am_Pnd_Tot_Transfer_Amt_Array_Am.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-ARRAY-AM ( #I, #J ) TO #TRANSFER-AMT-ARRAY-AM ( #I, #J )
                    pnd_J));
                pnd_Transfer_Amt_By_Fund_In_Aa_Pnd_Transfer_Amt_Array_Aa.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Amt_By_Fund_In_Aa_Pnd_Tot_Transfer_Amt_Array_Aa.getValue(pnd_I, //Natural: MOVE #TOT-TRANSFER-AMT-ARRAY-AA ( #I, #J ) TO #TRANSFER-AMT-ARRAY-AA ( #I, #J )
                    pnd_J));
                pnd_Transfer_Num_Mm_Pnd_Num_Transfers_Mm.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Transfer_Num_Mm_Pnd_Tot_Num_Transfers_Mm.getValue(pnd_I,                  //Natural: MOVE #TOT-NUM-TRANSFERS-MM ( #I, #J ) TO #NUM-TRANSFERS-MM ( #I, #J )
                    pnd_J));
                pnd_Transfer_Num_By_Fund_In_Ma_Pnd_Num_Transfers_Ma.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Num_By_Fund_In_Ma_Pnd_Tot_Num_Transfers_Ma.getValue(pnd_I, //Natural: MOVE #TOT-NUM-TRANSFERS-MA ( #I, #J ) TO #NUM-TRANSFERS-MA ( #I, #J )
                    pnd_J));
                pnd_Transfer_Num_By_Fund_In_Am_Pnd_Num_Transfers_Am.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Num_By_Fund_In_Am_Pnd_Tot_Num_Transfers_Am.getValue(pnd_I, //Natural: MOVE #TOT-NUM-TRANSFERS-AM ( #I, #J ) TO #NUM-TRANSFERS-AM ( #I, #J )
                    pnd_J));
                pnd_Transfer_Num_By_Fund_In_Aa_Pnd_Num_Transfers_Aa.getValue(pnd_I,pnd_J).setValue(pnd_Tot_Trans_Num_By_Fund_In_Aa_Pnd_Tot_Num_Transfers_Aa.getValue(pnd_I, //Natural: MOVE #TOT-NUM-TRANSFERS-AA ( #I, #J ) TO #NUM-TRANSFERS-AA ( #I, #J )
                    pnd_J));
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Gtd_Incoming.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Gtd_Incoming.getValue(pnd_I));                 //Natural: MOVE #TOT-GTD-INCOMING ( #I ) TO #GTD-INCOMING ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Dvd_Incoming.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Dvd_Incoming.getValue(pnd_I));                 //Natural: MOVE #TOT-DVD-INCOMING ( #I ) TO #DVD-INCOMING ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_A.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_A.getValue(pnd_I));         //Natural: MOVE #TOT-UNITS-INCOMING-A ( #I ) TO #UNITS-INCOMING-A ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Incoming_M.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Incoming_M.getValue(pnd_I));         //Natural: MOVE #TOT-UNITS-INCOMING-M ( #I ) TO #UNITS-INCOMING-M ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A.getValue(pnd_I));         //Natural: MOVE #TOT-UNITS-OUTGOING-A ( #I ) TO #UNITS-OUTGOING-A ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_A_1.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_A_1.getValue(pnd_I));     //Natural: MOVE #TOT-UNITS-OUTGOING-A-1 ( #I ) TO #UNITS-OUTGOING-A-1 ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M.getValue(pnd_I));         //Natural: MOVE #TOT-UNITS-OUTGOING-M ( #I ) TO #UNITS-OUTGOING-M ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Units_Outgoing_M_1.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Units_Outgoing_M_1.getValue(pnd_I));     //Natural: MOVE #TOT-UNITS-OUTGOING-M-1 ( #I ) TO #UNITS-OUTGOING-M-1 ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_M.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_M.getValue(pnd_I));           //Natural: MOVE #TOT-PMTS-INCOMING-M ( #I ) TO #PMTS-INCOMING-M ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Incoming_A.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Incoming_A.getValue(pnd_I));           //Natural: MOVE #TOT-PMTS-INCOMING-A ( #I ) TO #PMTS-INCOMING-A ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_M.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_M.getValue(pnd_I));           //Natural: MOVE #TOT-PMTS-OUTGOING-M ( #I ) TO #PMTS-OUTGOING-M ( #I )
            pnd_Transfer_Amt_By_Fund_In_1_Pnd_Pmts_Outgoing_A.getValue(pnd_I).setValue(pnd_Tot_Trans_Amt_By_Fund_In_1_Pnd_Tot_Pmts_Outgoing_A.getValue(pnd_I));           //Natural: MOVE #TOT-PMTS-OUTGOING-A ( #I ) TO #PMTS-OUTGOING-A ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORTS
        sub_Write_Summary_Reports();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaAial0133_getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_OutIsBreak = ldaAial0133.getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_Out().isBreak(endOfData);
        if (condition(ldaAial0133_getPnd_Iaa_Xfr_Actrl_Rcrd_Out_Pnd_Transfer_Eff_Date_Out_S3_OutIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORTS
            sub_Write_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            //*    WRITE '#EOF-FLAG = ' #EOF-FLAG
            if (condition(pnd_Eof_Flag.equals("N")))                                                                                                                      //Natural: IF #EOF-FLAG = 'N'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                sub_Write_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=66 ES=ON");
        Global.format(1, "LS=132 PS=0 ES=ON");
    }
}
