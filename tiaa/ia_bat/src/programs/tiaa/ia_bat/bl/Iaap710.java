/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:53 PM
**        * FROM NATURAL PROGRAM : Iaap710
************************************************************
**        * FILE NAME            : Iaap710.java
**        * CLASS NAME           : Iaap710
**        * INSTANCE NAME        : Iaap710
************************************************************
************************************************************************
* PROGRAM   : IAAP710
* FUNCTION  : THIS BATCH PROGRAM HAS 2 FUNCTIONS:
*
*             1. UPDATE A DAILY CONTROL RECORD BY COMPARING THE
*                DAILY RECORD WITH THE IA MASTER RECORDS.
*             2. PRODUCE A REPORT SHOWING THE END OF THE DAILY RUN
*                AND THE LAST MONTHLY RUN.
*
* DATE      :
* HISTORY   :
* MODIFIED  : 8/96     USE EXTERN FILE FOR CREF PRODUCT DECRIPTIONS
*                      FOR CHANGES DO SCAN ON 8/96
*             3/97     ADD LINES FOR DOLLAR AMOUNTS AS WELL AS UNITS
*             LB 10/97 REPORT CHANGED TO SHOW THE TOTALS FOR ANNUAL
*                      AND MONTHLY FUNDS
*             4/02     CHANGED IAAN0510 TO IAAN0510Z FOR NEW OIA SYSTEM
*             7/02     ADDED NEW PA FUND CODES TO INITIALIZE DC
*                      RECORDS WITH NEW FUNDS
*                      DO SCAN ON 7/02 FOR CHANGES
*             9/08     ADDED NEW TIAA ACCESS FUND
*                      DO SCAN ON 9/08 FOR CHANGES
*             3/12     RBE PROJECT. INCREASED OCCURRENCES TO 250.
*                      DO SCAN ON 3/12 FOR CHANGES
* J. TINIO   11/13     REWRITTEN TO REDUCE IO FOR SAG PERFORMANCE
*                      MONITORING. THE DAILY MASTER EXTRACT WILL BE USED
*                      TO REPLACE DIRECT ADABAS READ.
* O. SOTTO   04/2017   PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap710 extends BLNatBase
{
    // Data Areas
    private LdaIaaa090a ldaIaaa090a;
    private PdaIaaa090f pdaIaaa090f;
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Desc;

    private DbsGroup pnd_Parm_Desc__R_Field_1;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_6;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_Rem;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;

    private DbsGroup pnd_Reprot_Headings;
    private DbsField pnd_Reprot_Headings_Pnd_Report1_Heading1;
    private DbsField pnd_Reprot_Headings_Pnd_Report2_Heading1;
    private DbsField pnd_Reprot_Headings_Pnd_Report_Heading2;
    private DbsField pnd_Reprot_Headings_Pnd_Page_1_Heading2;
    private DbsField pnd_Reprot_Headings_Pnd_Page_2_Heading2;
    private DbsField pnd_Reprot_Headings_Pnd_Page_3_Heading2;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_F3;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input_Pnd_F4;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_F5;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input_Pnd_F6;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_F7;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Type;
    private DbsField pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;
    private DbsField pnd_Input_Pnd_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Cntrct_Nmbr;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Fund_Cde;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_14;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Company_Cd;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_F20;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_15;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_F22;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input_Pnd_F23;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Last_1;
    private DbsField iaa_Cntrl_Last_1_Cntrl_Check_Dte;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_Month;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees;
    private DbsField pnd_I;
    private DbsField pnd_I1;
    private DbsField pnd_J;
    private DbsField pnd_R;
    private DbsField pnd_Prod_Counter_Pa;
    private DbsField pnd_Max_Prd_Cd;
    private DbsField pnd_Max_Prd_Cde;

    private DbsGroup pnd_Max_Prd_Cde__R_Field_16;
    private DbsField pnd_Max_Prd_Cde_Pnd_Max_Prd_Cde_Alph;
    private DbsField total_Ivc_Amt;
    private DbsField total_Ivc_Amt_Used;
    private DbsField total_Ivc_Amt_Remained;
    private DbsField total_Rinv;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_Occ_A;

    private DbsGroup pnd_Occ_A__R_Field_17;
    private DbsField pnd_Occ_A_Pnd_Occ_1a;
    private DbsField pnd_Occ_A_Pnd_Occ;
    private DbsField pnd_Temp_Fund_Cde;

    private DbsGroup pnd_Temp_Fund_Cde__R_Field_18;
    private DbsField pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_A1;
    private DbsField pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2;

    private DbsGroup pnd_Fund_Desc_Lne;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Desc;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Fill;
    private DbsField pnd_Fund_Desc_Lne_Pnd_Fund_Txt;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_19;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_20;
    private DbsField pnd_Today_Pnd_Today_N;

    private DbsGroup pnd_Today__R_Field_21;
    private DbsField pnd_Today_Pnd_Today_Yyyymm;

    private DbsGroup pnd_Today__R_Field_22;
    private DbsField pnd_Today_Pnd_Today_Yyyy;
    private DbsField pnd_Today_Pnd_Today_Mm;
    private DbsField pnd_Today_Pnd_Today_Dd;
    private DbsField pnd_Today_D;
    private DbsField pnd_Begin_Month_D;
    private DbsField pnd_Begin_Month;

    private DbsGroup pnd_Begin_Month__R_Field_23;
    private DbsField pnd_Begin_Month_Pnd_Begin_Month_N;

    private DbsGroup pnd_Begin_Month__R_Field_24;
    private DbsField pnd_Begin_Month_Pnd_Begin_Yyyy;
    private DbsField pnd_Begin_Month_Pnd_Begin_Mm;
    private DbsField pnd_Begin_Month_Pnd_Begin_Dd;
    private DbsField pnd_Isn_Cntrl_Rec;
    private DbsField pnd_Compare_D;

    private DbsGroup pnd_Compare_D__R_Field_25;
    private DbsField pnd_Compare_D_Pnd_Compare_D_Yyyy;
    private DbsField pnd_Compare_D_Pnd_Compare_D_Mm;

    private DbsGroup pnd_Tpa;
    private DbsField pnd_Tpa_Pnd_Fund;
    private DbsField pnd_Tpa_Pnd_Pay_Amt;
    private DbsField pnd_Tpa_Pnd_Div_Amt;
    private DbsField pnd_Tpa_Pnd_Fin_Pay;
    private DbsField pnd_Tpa_Pnd_Fin_Div;

    private DbsGroup pnd_Payess_1;
    private DbsField pnd_Payess_1_Pnd_Fund_1;
    private DbsField pnd_Payess_1_Pnd_Pay_Amt_1;
    private DbsField pnd_Payess_1_Pnd_Div_Amt_1;
    private DbsField pnd_Payess_1_Pnd_Fin_Pay_1;
    private DbsField pnd_Payess_1_Pnd_Fin_Div_1;

    private DbsGroup pnd_Payess_2;
    private DbsField pnd_Payess_2_Pnd_Fund_2;
    private DbsField pnd_Payess_2_Pnd_Pay_Amt_2;
    private DbsField pnd_Payess_2_Pnd_Div_Amt_2;
    private DbsField pnd_Payess_2_Pnd_Fin_Pay_2;
    private DbsField pnd_Payess_2_Pnd_Fin_Div_2;

    private DbsGroup pnd_Tpa_Net;
    private DbsField pnd_Tpa_Net_Pnd_Fund1;
    private DbsField pnd_Tpa_Net_Pnd_Pay_Amt1;
    private DbsField pnd_Tpa_Net_Pnd_Div_Amt1;
    private DbsField pnd_Tpa_Net_Pnd_Fin_Pay1;
    private DbsField pnd_Tpa_Net_Pnd_Fin_Div1;
    private DbsField pnd_Tpa_Net_Ws_Fin_Pay_Amt;
    private DbsField pnd_Tpa_Net_Ws_Fin_Div_Amt;

    private DbsGroup pnd_Payess_Net_1;
    private DbsField pnd_Payess_Net_1_Pnd_Fund1_1;
    private DbsField pnd_Payess_Net_1_Pnd_Pay_Amt1_1;
    private DbsField pnd_Payess_Net_1_Pnd_Div_Amt1_1;
    private DbsField pnd_Payess_Net_1_Pnd_Fin_Pay1_1;
    private DbsField pnd_Payess_Net_1_Pnd_Fin_Div1_1;

    private DbsGroup pnd_Tpa_Net_2;
    private DbsField pnd_Tpa_Net_2_Pnd_Fund1_2;
    private DbsField pnd_Tpa_Net_2_Pnd_Pay_Amt1_2;
    private DbsField pnd_Tpa_Net_2_Pnd_Div_Amt1_2;
    private DbsField pnd_Tpa_Net_2_Pnd_Fin_Pay1_2;
    private DbsField pnd_Tpa_Net_2_Pnd_Fin_Div1_2;
    private DbsField pnd_S_Optn;
    private DbsField pnd_S_Status_Cde;
    private DbsField pnd_S_Cntrct_Company_Cd;
    private DbsField pnd_S_Curr_Dist_Cde;
    private DbsField pnd_S_Cntrct_Ivc_Amt;
    private DbsField pnd_S_Cntrct_Ivc_Used_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaaa090a = new LdaIaaa090a();
        registerRecord(ldaIaaa090a);
        localVariables = new DbsRecord();
        pdaIaaa090f = new PdaIaaa090f(localVariables);
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_Parm_Fund_2 = localVariables.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Desc = localVariables.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);

        pnd_Parm_Desc__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Desc__R_Field_1", "REDEFINE", pnd_Parm_Desc);
        pnd_Parm_Desc_Pnd_Parm_Desc_6 = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_6", "#PARM-DESC-6", FieldType.STRING, 6);
        pnd_Parm_Desc_Pnd_Parm_Desc_Rem = pnd_Parm_Desc__R_Field_1.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_Rem", "#PARM-DESC-REM", FieldType.STRING, 
            29);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Parm_Len = localVariables.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);

        pnd_Reprot_Headings = localVariables.newGroupInRecord("pnd_Reprot_Headings", "#REPROT-HEADINGS");
        pnd_Reprot_Headings_Pnd_Report1_Heading1 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Report1_Heading1", "#REPORT1-HEADING1", 
            FieldType.STRING, 50);
        pnd_Reprot_Headings_Pnd_Report2_Heading1 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Report2_Heading1", "#REPORT2-HEADING1", 
            FieldType.STRING, 50);
        pnd_Reprot_Headings_Pnd_Report_Heading2 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Report_Heading2", "#REPORT-HEADING2", FieldType.STRING, 
            30);
        pnd_Reprot_Headings_Pnd_Page_1_Heading2 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Page_1_Heading2", "#PAGE-1-HEADING2", FieldType.STRING, 
            30);
        pnd_Reprot_Headings_Pnd_Page_2_Heading2 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Page_2_Heading2", "#PAGE-2-HEADING2", FieldType.STRING, 
            30);
        pnd_Reprot_Headings_Pnd_Page_3_Heading2 = pnd_Reprot_Headings.newFieldInGroup("pnd_Reprot_Headings_Pnd_Page_3_Heading2", "#PAGE-3-HEADING2", FieldType.STRING, 
            30);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_3 = pnd_Input__R_Field_2.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_7 = pnd_Input__R_Field_5.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F3 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F3", "#F3", FieldType.STRING, 21);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_F4 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F4", "#F4", FieldType.STRING, 6);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F5 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F5", "#F5", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_F6 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F6", "#F6", FieldType.STRING, 5);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_F7 = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_F7", "#F7", FieldType.STRING, 11);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_Cntrct_Type = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re", "#CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_5.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte", "#CNTRCT-FNL-PRM-DTE", FieldType.DATE, 
            new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Cntrct_Annty_Strt_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Annty_Strt_Dte", "#CNTRCT-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd", "#CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd", "#CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Sub_Plan_Nmbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Sub_Plan_Nmbr", "#SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr", "#ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Input_Pnd_Orgntng_Cntrct_Nmbr = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Orgntng_Cntrct_Nmbr", "#ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10);

        pnd_Input__R_Field_8 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_10 = pnd_Input__R_Field_8.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_11 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Fund_Cde = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Fund_Cde", "#SUMM-CMPNY-FUND-CDE", FieldType.STRING, 
            3);

        pnd_Input__R_Field_12 = pnd_Input__R_Field_11.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_11.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_14 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_14", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Cntrct_Company_Cd = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Company_Cd", "#CNTRCT-COMPANY-CD", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_F20 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F20", "#F20", FieldType.STRING, 23);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_15 = pnd_Input__R_Field_14.newGroupInGroup("pnd_Input__R_Field_15", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_15.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_F22 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F22", "#F22", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_F23 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F23", "#F23", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_14.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_14.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        vw_iaa_Cntrl_Last_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Last_1", "IAA-CNTRL-LAST-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Last_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Last_1.getRecord().newFieldInGroup("iaa_Cntrl_Last_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Last_1);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrl_Rcrd_Month = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_Month", "IAA-CNTRL-RCRD-MONTH"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_MONTH_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Count_Castcntrl_Fund_Cnts", 
            "C*CNTRL-FUND-CNTS", RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_Month.getRecord().newGroupArrayInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", 
            new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Units = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Amt = iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Cnts.newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_Month.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        registerRecord(vw_iaa_Cntrl_Rcrd_Month);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Prod_Counter_Pa = localVariables.newFieldInRecord("pnd_Prod_Counter_Pa", "#PROD-COUNTER-PA", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cd = localVariables.newFieldInRecord("pnd_Max_Prd_Cd", "#MAX-PRD-CD", FieldType.NUMERIC, 2);
        pnd_Max_Prd_Cde = localVariables.newFieldInRecord("pnd_Max_Prd_Cde", "#MAX-PRD-CDE", FieldType.NUMERIC, 2);

        pnd_Max_Prd_Cde__R_Field_16 = localVariables.newGroupInRecord("pnd_Max_Prd_Cde__R_Field_16", "REDEFINE", pnd_Max_Prd_Cde);
        pnd_Max_Prd_Cde_Pnd_Max_Prd_Cde_Alph = pnd_Max_Prd_Cde__R_Field_16.newFieldInGroup("pnd_Max_Prd_Cde_Pnd_Max_Prd_Cde_Alph", "#MAX-PRD-CDE-ALPH", 
            FieldType.STRING, 2);
        total_Ivc_Amt = localVariables.newFieldInRecord("total_Ivc_Amt", "TOTAL-IVC-AMT", FieldType.NUMERIC, 11, 2);
        total_Ivc_Amt_Used = localVariables.newFieldInRecord("total_Ivc_Amt_Used", "TOTAL-IVC-AMT-USED", FieldType.NUMERIC, 11, 2);
        total_Ivc_Amt_Remained = localVariables.newFieldInRecord("total_Ivc_Amt_Remained", "TOTAL-IVC-AMT-REMAINED", FieldType.NUMERIC, 11, 2);
        total_Rinv = localVariables.newFieldInRecord("total_Rinv", "TOTAL-RINV", FieldType.NUMERIC, 11, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.PACKED_DECIMAL, 3);
        pnd_Occ_A = localVariables.newFieldInRecord("pnd_Occ_A", "#OCC-A", FieldType.STRING, 3);

        pnd_Occ_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Occ_A__R_Field_17", "REDEFINE", pnd_Occ_A);
        pnd_Occ_A_Pnd_Occ_1a = pnd_Occ_A__R_Field_17.newFieldInGroup("pnd_Occ_A_Pnd_Occ_1a", "#OCC-1A", FieldType.STRING, 1);
        pnd_Occ_A_Pnd_Occ = pnd_Occ_A__R_Field_17.newFieldInGroup("pnd_Occ_A_Pnd_Occ", "#OCC", FieldType.NUMERIC, 2);
        pnd_Temp_Fund_Cde = localVariables.newFieldInRecord("pnd_Temp_Fund_Cde", "#TEMP-FUND-CDE", FieldType.STRING, 3);

        pnd_Temp_Fund_Cde__R_Field_18 = localVariables.newGroupInRecord("pnd_Temp_Fund_Cde__R_Field_18", "REDEFINE", pnd_Temp_Fund_Cde);
        pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_A1 = pnd_Temp_Fund_Cde__R_Field_18.newFieldInGroup("pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_A1", "#TEMP-FUND-CDE-A1", 
            FieldType.STRING, 1);
        pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2 = pnd_Temp_Fund_Cde__R_Field_18.newFieldInGroup("pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2", "#TEMP-FUND-CDE-N2", 
            FieldType.NUMERIC, 2);

        pnd_Fund_Desc_Lne = localVariables.newGroupInRecord("pnd_Fund_Desc_Lne", "#FUND-DESC-LNE");
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con", "#FUND-CREF-CON", FieldType.STRING, 
            13);
        pnd_Fund_Desc_Lne_Pnd_Fund_Desc = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 6);
        pnd_Fund_Desc_Lne_Pnd_Fund_Fill = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Fill", "#FUND-FILL", FieldType.STRING, 1);
        pnd_Fund_Desc_Lne_Pnd_Fund_Txt = pnd_Fund_Desc_Lne.newFieldInGroup("pnd_Fund_Desc_Lne_Pnd_Fund_Txt", "#FUND-TXT", FieldType.STRING, 15);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_19", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_19.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_19.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_20 = localVariables.newGroupInRecord("pnd_Today__R_Field_20", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_N = pnd_Today__R_Field_20.newFieldInGroup("pnd_Today_Pnd_Today_N", "#TODAY-N", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_21 = pnd_Today__R_Field_20.newGroupInGroup("pnd_Today__R_Field_21", "REDEFINE", pnd_Today_Pnd_Today_N);
        pnd_Today_Pnd_Today_Yyyymm = pnd_Today__R_Field_21.newFieldInGroup("pnd_Today_Pnd_Today_Yyyymm", "#TODAY-YYYYMM", FieldType.NUMERIC, 6);

        pnd_Today__R_Field_22 = pnd_Today__R_Field_21.newGroupInGroup("pnd_Today__R_Field_22", "REDEFINE", pnd_Today_Pnd_Today_Yyyymm);
        pnd_Today_Pnd_Today_Yyyy = pnd_Today__R_Field_22.newFieldInGroup("pnd_Today_Pnd_Today_Yyyy", "#TODAY-YYYY", FieldType.NUMERIC, 4);
        pnd_Today_Pnd_Today_Mm = pnd_Today__R_Field_22.newFieldInGroup("pnd_Today_Pnd_Today_Mm", "#TODAY-MM", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Dd = pnd_Today__R_Field_21.newFieldInGroup("pnd_Today_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);
        pnd_Today_D = localVariables.newFieldInRecord("pnd_Today_D", "#TODAY-D", FieldType.STRING, 10);
        pnd_Begin_Month_D = localVariables.newFieldInRecord("pnd_Begin_Month_D", "#BEGIN-MONTH-D", FieldType.STRING, 10);
        pnd_Begin_Month = localVariables.newFieldInRecord("pnd_Begin_Month", "#BEGIN-MONTH", FieldType.STRING, 8);

        pnd_Begin_Month__R_Field_23 = localVariables.newGroupInRecord("pnd_Begin_Month__R_Field_23", "REDEFINE", pnd_Begin_Month);
        pnd_Begin_Month_Pnd_Begin_Month_N = pnd_Begin_Month__R_Field_23.newFieldInGroup("pnd_Begin_Month_Pnd_Begin_Month_N", "#BEGIN-MONTH-N", FieldType.NUMERIC, 
            8);

        pnd_Begin_Month__R_Field_24 = pnd_Begin_Month__R_Field_23.newGroupInGroup("pnd_Begin_Month__R_Field_24", "REDEFINE", pnd_Begin_Month_Pnd_Begin_Month_N);
        pnd_Begin_Month_Pnd_Begin_Yyyy = pnd_Begin_Month__R_Field_24.newFieldInGroup("pnd_Begin_Month_Pnd_Begin_Yyyy", "#BEGIN-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Begin_Month_Pnd_Begin_Mm = pnd_Begin_Month__R_Field_24.newFieldInGroup("pnd_Begin_Month_Pnd_Begin_Mm", "#BEGIN-MM", FieldType.NUMERIC, 2);
        pnd_Begin_Month_Pnd_Begin_Dd = pnd_Begin_Month__R_Field_24.newFieldInGroup("pnd_Begin_Month_Pnd_Begin_Dd", "#BEGIN-DD", FieldType.NUMERIC, 2);
        pnd_Isn_Cntrl_Rec = localVariables.newFieldInRecord("pnd_Isn_Cntrl_Rec", "#ISN-CNTRL-REC", FieldType.PACKED_DECIMAL, 10);
        pnd_Compare_D = localVariables.newFieldInRecord("pnd_Compare_D", "#COMPARE-D", FieldType.STRING, 8);

        pnd_Compare_D__R_Field_25 = localVariables.newGroupInRecord("pnd_Compare_D__R_Field_25", "REDEFINE", pnd_Compare_D);
        pnd_Compare_D_Pnd_Compare_D_Yyyy = pnd_Compare_D__R_Field_25.newFieldInGroup("pnd_Compare_D_Pnd_Compare_D_Yyyy", "#COMPARE-D-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Compare_D_Pnd_Compare_D_Mm = pnd_Compare_D__R_Field_25.newFieldInGroup("pnd_Compare_D_Pnd_Compare_D_Mm", "#COMPARE-D-MM", FieldType.NUMERIC, 
            2);

        pnd_Tpa = localVariables.newGroupInRecord("pnd_Tpa", "#TPA");
        pnd_Tpa_Pnd_Fund = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fund", "#FUND", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 4));
        pnd_Tpa_Pnd_Pay_Amt = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Pay_Amt", "#PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Div_Amt = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Div_Amt", "#DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Pay = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Pay", "#FIN-PAY", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Pnd_Fin_Div = pnd_Tpa.newFieldArrayInGroup("pnd_Tpa_Pnd_Fin_Div", "#FIN-DIV", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));

        pnd_Payess_1 = localVariables.newGroupInRecord("pnd_Payess_1", "#PAYESS-1");
        pnd_Payess_1_Pnd_Fund_1 = pnd_Payess_1.newFieldArrayInGroup("pnd_Payess_1_Pnd_Fund_1", "#FUND-1", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            4));
        pnd_Payess_1_Pnd_Pay_Amt_1 = pnd_Payess_1.newFieldArrayInGroup("pnd_Payess_1_Pnd_Pay_Amt_1", "#PAY-AMT-1", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_1_Pnd_Div_Amt_1 = pnd_Payess_1.newFieldArrayInGroup("pnd_Payess_1_Pnd_Div_Amt_1", "#DIV-AMT-1", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_1_Pnd_Fin_Pay_1 = pnd_Payess_1.newFieldArrayInGroup("pnd_Payess_1_Pnd_Fin_Pay_1", "#FIN-PAY-1", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_1_Pnd_Fin_Div_1 = pnd_Payess_1.newFieldArrayInGroup("pnd_Payess_1_Pnd_Fin_Div_1", "#FIN-DIV-1", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));

        pnd_Payess_2 = localVariables.newGroupInRecord("pnd_Payess_2", "#PAYESS-2");
        pnd_Payess_2_Pnd_Fund_2 = pnd_Payess_2.newFieldArrayInGroup("pnd_Payess_2_Pnd_Fund_2", "#FUND-2", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            4));
        pnd_Payess_2_Pnd_Pay_Amt_2 = pnd_Payess_2.newFieldArrayInGroup("pnd_Payess_2_Pnd_Pay_Amt_2", "#PAY-AMT-2", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_2_Pnd_Div_Amt_2 = pnd_Payess_2.newFieldArrayInGroup("pnd_Payess_2_Pnd_Div_Amt_2", "#DIV-AMT-2", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_2_Pnd_Fin_Pay_2 = pnd_Payess_2.newFieldArrayInGroup("pnd_Payess_2_Pnd_Fin_Pay_2", "#FIN-PAY-2", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));
        pnd_Payess_2_Pnd_Fin_Div_2 = pnd_Payess_2.newFieldArrayInGroup("pnd_Payess_2_Pnd_Fin_Div_2", "#FIN-DIV-2", FieldType.PACKED_DECIMAL, 13, 2, new 
            DbsArrayController(1, 4));

        pnd_Tpa_Net = localVariables.newGroupInRecord("pnd_Tpa_Net", "#TPA-NET");
        pnd_Tpa_Net_Pnd_Fund1 = pnd_Tpa_Net.newFieldArrayInGroup("pnd_Tpa_Net_Pnd_Fund1", "#FUND1", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_Pnd_Pay_Amt1 = pnd_Tpa_Net.newFieldArrayInGroup("pnd_Tpa_Net_Pnd_Pay_Amt1", "#PAY-AMT1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_Pnd_Div_Amt1 = pnd_Tpa_Net.newFieldArrayInGroup("pnd_Tpa_Net_Pnd_Div_Amt1", "#DIV-AMT1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_Pnd_Fin_Pay1 = pnd_Tpa_Net.newFieldArrayInGroup("pnd_Tpa_Net_Pnd_Fin_Pay1", "#FIN-PAY1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_Pnd_Fin_Div1 = pnd_Tpa_Net.newFieldArrayInGroup("pnd_Tpa_Net_Pnd_Fin_Div1", "#FIN-DIV1", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_Ws_Fin_Pay_Amt = pnd_Tpa_Net.newFieldInGroup("pnd_Tpa_Net_Ws_Fin_Pay_Amt", "WS-FIN-PAY-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tpa_Net_Ws_Fin_Div_Amt = pnd_Tpa_Net.newFieldInGroup("pnd_Tpa_Net_Ws_Fin_Div_Amt", "WS-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Payess_Net_1 = localVariables.newGroupInRecord("pnd_Payess_Net_1", "#PAYESS-NET-1");
        pnd_Payess_Net_1_Pnd_Fund1_1 = pnd_Payess_Net_1.newFieldArrayInGroup("pnd_Payess_Net_1_Pnd_Fund1_1", "#FUND1-1", FieldType.PACKED_DECIMAL, 9, new 
            DbsArrayController(1, 4));
        pnd_Payess_Net_1_Pnd_Pay_Amt1_1 = pnd_Payess_Net_1.newFieldArrayInGroup("pnd_Payess_Net_1_Pnd_Pay_Amt1_1", "#PAY-AMT1-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));
        pnd_Payess_Net_1_Pnd_Div_Amt1_1 = pnd_Payess_Net_1.newFieldArrayInGroup("pnd_Payess_Net_1_Pnd_Div_Amt1_1", "#DIV-AMT1-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));
        pnd_Payess_Net_1_Pnd_Fin_Pay1_1 = pnd_Payess_Net_1.newFieldArrayInGroup("pnd_Payess_Net_1_Pnd_Fin_Pay1_1", "#FIN-PAY1-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));
        pnd_Payess_Net_1_Pnd_Fin_Div1_1 = pnd_Payess_Net_1.newFieldArrayInGroup("pnd_Payess_Net_1_Pnd_Fin_Div1_1", "#FIN-DIV1-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 4));

        pnd_Tpa_Net_2 = localVariables.newGroupInRecord("pnd_Tpa_Net_2", "#TPA-NET-2");
        pnd_Tpa_Net_2_Pnd_Fund1_2 = pnd_Tpa_Net_2.newFieldArrayInGroup("pnd_Tpa_Net_2_Pnd_Fund1_2", "#FUND1-2", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            4));
        pnd_Tpa_Net_2_Pnd_Pay_Amt1_2 = pnd_Tpa_Net_2.newFieldArrayInGroup("pnd_Tpa_Net_2_Pnd_Pay_Amt1_2", "#PAY-AMT1-2", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 4));
        pnd_Tpa_Net_2_Pnd_Div_Amt1_2 = pnd_Tpa_Net_2.newFieldArrayInGroup("pnd_Tpa_Net_2_Pnd_Div_Amt1_2", "#DIV-AMT1-2", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 4));
        pnd_Tpa_Net_2_Pnd_Fin_Pay1_2 = pnd_Tpa_Net_2.newFieldArrayInGroup("pnd_Tpa_Net_2_Pnd_Fin_Pay1_2", "#FIN-PAY1-2", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 4));
        pnd_Tpa_Net_2_Pnd_Fin_Div1_2 = pnd_Tpa_Net_2.newFieldArrayInGroup("pnd_Tpa_Net_2_Pnd_Fin_Div1_2", "#FIN-DIV1-2", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 4));
        pnd_S_Optn = localVariables.newFieldInRecord("pnd_S_Optn", "#S-OPTN", FieldType.NUMERIC, 2);
        pnd_S_Status_Cde = localVariables.newFieldInRecord("pnd_S_Status_Cde", "#S-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_S_Cntrct_Company_Cd = localVariables.newFieldArrayInRecord("pnd_S_Cntrct_Company_Cd", "#S-CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            5));
        pnd_S_Curr_Dist_Cde = localVariables.newFieldInRecord("pnd_S_Curr_Dist_Cde", "#S-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_S_Cntrct_Ivc_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cntrct_Ivc_Amt", "#S-CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            5));
        pnd_S_Cntrct_Ivc_Used_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cntrct_Ivc_Used_Amt", "#S-CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Last_1.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrl_Rcrd_Month.reset();

        ldaIaaa090a.initializeValues();

        localVariables.reset();
        pnd_Reprot_Headings_Pnd_Report1_Heading1.setInitialValue("IA ADMINISTRATION FILE CONTROL - DIFFERENCES");
        pnd_Reprot_Headings_Pnd_Report2_Heading1.setInitialValue("IA ADMINISTRATION FILE CONTROL - NET CHANGES");
        pnd_Reprot_Headings_Pnd_Page_1_Heading2.setInitialValue("TIAA TOTALS");
        pnd_Reprot_Headings_Pnd_Page_2_Heading2.setInitialValue("CREF ANNUAL TOTALS");
        pnd_Reprot_Headings_Pnd_Page_3_Heading2.setInitialValue("CREF MONTHLY TOTALS");
        pnd_Prod_Counter_Pa.setInitialValue(0);
        pnd_Max_Prd_Cd.setInitialValue(0);
        pnd_Max_Prd_Cde.setInitialValue(0);
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setInitialValue("CREF ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap710() throws Exception
    {
        super("Iaap710");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP710", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_1_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-1-HEADING2
        //*  8/96                                                                                                                                                         //Natural: FORMAT PS = 55 LS = 132;//Natural: FORMAT ( 1 ) PS = 55 LS = 132;//Natural: FORMAT ( 2 ) PS = 55 LS = 132
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN0500-READ-EXTERN-FILE
        sub_Call_Iaan0500_Read_Extern_File();
        if (condition(Global.isEscape())) {return;}
        //*  REPORT 1 PAGE HEADINGS
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  REPORT 2 PAGE HEADINGS
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  START MAIN PROCESS
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.equals("   CHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   FHEADER") || pnd_Input_Pnd_Ppcn_Nbr.equals("   SHEADER")       //Natural: IF #PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || pnd_Input_Pnd_Ppcn_Nbr.equals("99CTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99FTRAILER") || pnd_Input_Pnd_Ppcn_Nbr.equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CONTRACT
            if (condition(pnd_Input_Pnd_Record_Code.equals(10)))                                                                                                          //Natural: IF #RECORD-CODE = 10
            {
                pnd_S_Optn.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                              //Natural: ASSIGN #S-OPTN := #OPTN-CDE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CPR
            if (condition(pnd_Input_Pnd_Record_Code.equals(20)))                                                                                                          //Natural: IF #RECORD-CODE = 20
            {
                pnd_S_Status_Cde.setValue(pnd_Input_Pnd_Status_Cde);                                                                                                      //Natural: ASSIGN #S-STATUS-CDE := #STATUS-CDE
                pnd_S_Cntrct_Company_Cd.getValue("*").setValue(pnd_Input_Pnd_Cntrct_Company_Cd.getValue("*"));                                                            //Natural: ASSIGN #S-CNTRCT-COMPANY-CD ( * ) := #CNTRCT-COMPANY-CD ( * )
                pnd_S_Cntrct_Ivc_Amt.getValue("*").setValue(pnd_Input_Pnd_Cntrct_Ivc_Amt.getValue("*"));                                                                  //Natural: ASSIGN #S-CNTRCT-IVC-AMT ( * ) := #CNTRCT-IVC-AMT ( * )
                pnd_S_Cntrct_Ivc_Used_Amt.getValue("*").setValue(pnd_Input_Pnd_Cntrct_Ivc_Used_Amt.getValue("*"));                                                        //Natural: ASSIGN #S-CNTRCT-IVC-USED-AMT ( * ) := #CNTRCT-IVC-USED-AMT ( * )
                pnd_S_Curr_Dist_Cde.setValue(pnd_Input_Pnd_Curr_Dist_Cde);                                                                                                //Natural: ASSIGN #S-CURR-DIST-CDE := #CURR-DIST-CDE
                if (condition(pnd_S_Status_Cde.notEquals(9)))                                                                                                             //Natural: IF #S-STATUS-CDE NE 9
                {
                    if (condition(pnd_S_Cntrct_Company_Cd.getValue(1).equals("T")))                                                                                       //Natural: IF #S-CNTRCT-COMPANY-CD ( 1 ) = 'T'
                    {
                        ldaIaaa090a.getIaaa090_Cntrl_Actve_Tiaa_Pys().nadd(1);                                                                                            //Natural: ADD 1 TO IAAA090.CNTRL-ACTVE-TIAA-PYS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaIaaa090a.getIaaa090_Cntrl_Actve_Cref_Pys().nadd(1);                                                                                            //Natural: ADD 1 TO IAAA090.CNTRL-ACTVE-CREF-PYS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaaa090a.getIaaa090_Cntrl_Inactve_Payees().nadd(1);                                                                                                //Natural: ADD 1 TO IAAA090.CNTRL-INACTVE-PAYEES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  FUNDS
            if (condition(pnd_Input_Pnd_Record_Code.equals(30)))                                                                                                          //Natural: IF #RECORD-CODE = 30
            {
                if (condition(pnd_S_Status_Cde.notEquals(9)))                                                                                                             //Natural: IF #S-STATUS-CDE NE 9
                {
                    if (condition(pnd_S_Cntrct_Company_Cd.getValue(1).equals("T")))                                                                                       //Natural: IF #S-CNTRCT-COMPANY-CD ( 1 ) = 'T'
                    {
                        if (condition(pnd_Input_Pnd_Summ_Fund_Cde.equals("09") || pnd_Input_Pnd_Summ_Fund_Cde.equals("11")))                                              //Natural: IF #SUMM-FUND-CDE = '09' OR = '11'
                        {
                            pnd_Temp_Fund_Cde.setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                                                                                //Natural: ASSIGN #TEMP-FUND-CDE := #SUMM-CMPNY-FUND-CDE
                            //*  ANNUAL FUND
                            if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("U")))                                                                                      //Natural: IF #SUMM-CMPNY-CDE = 'U'
                            {
                                pnd_J.setValue(pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2);                                                                                   //Natural: ASSIGN #J := #TEMP-FUND-CDE-N2
                                ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                    //Natural: ADD #SUMM-PER-PYMNT TO IAAA090.CNTRL-AMT ( #J )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2.add(20));                                       //Natural: ASSIGN #J := #TEMP-FUND-CDE-N2 + 20
                                //*  LB 10/97
                            }                                                                                                                                             //Natural: END-IF
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J).nadd(1);                                                                           //Natural: ADD 1 TO IAAA090.CNTRL-FUND-PAYEES ( #J )
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J).setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                                          //Natural: ASSIGN IAAA090.CNTRL-FUND-CDE ( #J ) := #SUMM-CMPNY-FUND-CDE
                            ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J).nadd(pnd_Input_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO IAAA090.CNTRL-UNITS ( #J )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Pnd_Summ_Fund_Cde.equals("1G") || pnd_Input_Pnd_Summ_Fund_Cde.equals("1S") || pnd_Input_Pnd_Summ_Fund_Cde.equals("1 "))) //Natural: IF #SUMM-FUND-CDE = '1G' OR = '1S' OR = '1 '
                            {
                                                                                                                                                                          //Natural: PERFORM GET-OPTN-CODE
                                sub_Get_Optn_Code();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                ldaIaaa090a.getIaaa090_Cntrl_Tiaa_Payees().nadd(1);                                                                                       //Natural: ADD 1 TO IAAA090.CNTRL-TIAA-PAYEES
                                ldaIaaa090a.getIaaa090_Cntrl_Per_Pay_Amt().nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                            //Natural: ADD #SUMM-PER-PYMNT TO IAAA090.CNTRL-PER-PAY-AMT
                                ldaIaaa090a.getIaaa090_Cntrl_Per_Div_Amt().nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                            //Natural: ADD #SUMM-PER-DVDND TO IAAA090.CNTRL-PER-DIV-AMT
                                ldaIaaa090a.getIaaa090_Cntrl_Final_Pay_Amt().nadd(pnd_Input_Pnd_Summ_Fin_Pymnt);                                                          //Natural: ADD #SUMM-FIN-PYMNT TO IAAA090.CNTRL-FINAL-PAY-AMT
                                ldaIaaa090a.getIaaa090_Cntrl_Final_Div_Amt().nadd(pnd_Input_Pnd_Summ_Fin_Dvdnd);                                                          //Natural: ADD #SUMM-FIN-DVDND TO IAAA090.CNTRL-FINAL-DIV-AMT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Input_Pnd_Summ_Fund_Cde.greater("40")))                                                                                         //Natural: IF #SUMM-FUND-CDE > '40'
                        {
                            ldaIaaa090a.getIaaa090_Cntrl_Tiaa_Payees().nadd(1);                                                                                           //Natural: ADD 1 TO IAAA090.CNTRL-TIAA-PAYEES
                            pnd_Temp_Fund_Cde.setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                                                                                //Natural: ASSIGN #TEMP-FUND-CDE := #SUMM-CMPNY-FUND-CDE
                            if (condition(pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_A1.equals("U") || pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_A1.equals("2")))                      //Natural: IF #TEMP-FUND-CDE-A1 = 'U' OR = '2'
                            {
                                pnd_J.setValue(pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2);                                                                                   //Natural: ASSIGN #J = #TEMP-FUND-CDE-N2
                                //*  MONTHLY
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_Temp_Fund_Cde_Pnd_Temp_Fund_Cde_N2.add(20));                                       //Natural: ASSIGN #J = #TEMP-FUND-CDE-N2 + 20
                            }                                                                                                                                             //Natural: END-IF
                            ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                        //Natural: ADD #SUMM-PER-PYMNT TO IAAA090.CNTRL-AMT ( #J )
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J).nadd(1);                                                                           //Natural: ADD 1 TO IAAA090.CNTRL-FUND-PAYEES ( #J )
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J).setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                                          //Natural: ASSIGN IAAA090.CNTRL-FUND-CDE ( #J ) := #SUMM-CMPNY-FUND-CDE
                            ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J).nadd(pnd_Input_Pnd_Summ_Units);                                                          //Natural: ADD #SUMM-UNITS TO IAAA090.CNTRL-UNITS ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Occ_A.setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                                                                                            //Natural: ASSIGN #OCC-A := #SUMM-CMPNY-FUND-CDE
                        //*  ANNUAL FUND
                        if (condition(pnd_Input_Pnd_Summ_Cmpny_Cde.equals("2")))                                                                                          //Natural: IF #SUMM-CMPNY-CDE = '2'
                        {
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_Occ_A_Pnd_Occ).setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                              //Natural: ASSIGN IAAA090.CNTRL-FUND-CDE ( #OCC ) := #SUMM-CMPNY-FUND-CDE
                            ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_Occ_A_Pnd_Occ).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                            //Natural: ADD #SUMM-PER-PYMNT TO IAAA090.CNTRL-AMT ( #OCC )
                            //*  MONTHLY FUND
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Occ_A_Pnd_Occ.nadd(20);                                                                                                                   //Natural: ASSIGN #OCC = #OCC + 20
                            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_Occ_A_Pnd_Occ).setValue(pnd_Input_Pnd_Summ_Cmpny_Fund_Cde);                              //Natural: ASSIGN IAAA090.CNTRL-FUND-CDE ( #OCC ) := #SUMM-CMPNY-FUND-CDE
                        }                                                                                                                                                 //Natural: END-IF
                        ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_Occ_A_Pnd_Occ).nadd(pnd_Input_Pnd_Summ_Units);                                                  //Natural: ADD #SUMM-UNITS TO IAAA090.CNTRL-UNITS ( #OCC )
                        ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_Occ_A_Pnd_Occ).nadd(1);                                                                   //Natural: ADD 1 TO IAAA090.CNTRL-FUND-PAYEES ( #OCC )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  DEDUCTIONS
            if (condition(pnd_Input_Pnd_Record_Code.equals(40)))                                                                                                          //Natural: IF #RECORD-CODE = 40
            {
                if (condition(pnd_S_Status_Cde.notEquals(9)))                                                                                                             //Natural: IF #S-STATUS-CDE NE 9
                {
                    if (condition(pnd_Input_Pnd_Ddctn_Stp_Dte.equals(getZero())))                                                                                         //Natural: IF #DDCTN-STP-DTE = 0
                    {
                        getReports().display(3, pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde,pnd_Input_Pnd_Ddctn_Cde,pnd_Input_Pnd_Ddctn_Per_Amt);                      //Natural: DISPLAY ( 3 ) #PPCN-NBR #PAYEE-CDE #DDCTN-CDE #DDCTN-PER-AMT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Cnt().nadd(1);                                                                                                 //Natural: ADD 1 TO IAAA090.CNTRL-DDCTN-CNT
                        ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Amt().nadd(pnd_Input_Pnd_Ddctn_Per_Amt);                                                                       //Natural: ADD #DDCTN-PER-AMT TO IAAA090.CNTRL-DDCTN-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-DAILY-REPORT-1
        sub_Print_Daily_Report_1();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DAILY-REPORT-2
        sub_Print_Daily_Report_2();
        if (condition(Global.isEscape())) {return;}
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OPTN-CODE-1
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OPTN-CODE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DAILY-REPORT-1
        //* **********************************************************************
        //* ****************************************************'
        //*  WRITE TIAA ANNUAL REAL ESTATE AND ACCESS FUNDS
        //*  WRITE ACTIVE AND INACTIVE TOTALS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DAILY-REPORT-2
        //* **********************************************************************
        //*  GET THE BEGINNING OF THE MONTH RECORD
        //*  SUBTRACT 1 FROM TODAYS MONTH AND THEN GET THE LAST RECORD FOR THAT
        //*  MONTH
        //*   ADDED FOLLOWING HANDLE CREF PRODUCTS
        //*  #OCC-A := IAAA090F.CNTRL-FUND-CDE(#I)
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN0500-READ-EXTERN-FILE
        //* ************************  O N   E R R O R  ***************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Get_Optn_Code_1() throws Exception                                                                                                                   //Natural: GET-OPTN-CODE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        short decideConditionsMet693 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #S-OPTN;//Natural: VALUE 22
        if (condition((pnd_S_Optn.equals(22))))
        {
            decideConditionsMet693++;
            pnd_I.setValue(2);                                                                                                                                            //Natural: ASSIGN #I := 2
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_S_Optn.equals(25) || pnd_S_Optn.equals(27))))
        {
            decideConditionsMet693++;
            pnd_I.setValue(3);                                                                                                                                            //Natural: ASSIGN #I := 3
        }                                                                                                                                                                 //Natural: VALUE 28:30
        else if (condition(((pnd_S_Optn.greaterOrEqual(28) && pnd_S_Optn.lessOrEqual(30)))))
        {
            decideConditionsMet693++;
            pnd_I.setValue(4);                                                                                                                                            //Natural: ASSIGN #I := 4
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_I.setValue(1);                                                                                                                                            //Natural: ASSIGN #I := 1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Tpa_Net_Pnd_Fund1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees());                                                                          //Natural: ADD IAAA090F.CNTRL-TIAA-PAYEES TO #FUND1 ( #I )
        pnd_Tpa_Net_Pnd_Pay_Amt1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt());                                                                       //Natural: ADD IAAA090F.CNTRL-PER-PAY-AMT TO #PAY-AMT1 ( #I )
        pnd_Tpa_Net_Pnd_Div_Amt1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt());                                                                       //Natural: ADD IAAA090F.CNTRL-PER-DIV-AMT TO #DIV-AMT1 ( #I )
        pnd_Tpa_Net_Pnd_Fin_Pay1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt());                                                                     //Natural: ADD IAAA090F.CNTRL-FINAL-PAY-AMT TO #FIN-PAY1 ( #I )
        pnd_Tpa_Net_Pnd_Fin_Div1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt());                                                                     //Natural: ADD IAAA090F.CNTRL-FINAL-DIV-AMT TO #FIN-DIV1 ( #I )
        if (condition(pnd_Input_Pnd_Payee_Cde.equals(1)))                                                                                                                 //Natural: IF #PAYEE-CDE = 1
        {
            pnd_Payess_Net_1_Pnd_Fund1_1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees());                                                               //Natural: ADD IAAA090F.CNTRL-TIAA-PAYEES TO #FUND1-1 ( #I )
            pnd_Payess_Net_1_Pnd_Pay_Amt1_1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt());                                                            //Natural: ADD IAAA090F.CNTRL-PER-PAY-AMT TO #PAY-AMT1-1 ( #I )
            pnd_Payess_Net_1_Pnd_Div_Amt1_1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt());                                                            //Natural: ADD IAAA090F.CNTRL-PER-DIV-AMT TO #DIV-AMT1-1 ( #I )
            pnd_Payess_Net_1_Pnd_Fin_Pay1_1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt());                                                          //Natural: ADD IAAA090F.CNTRL-FINAL-PAY-AMT TO #FIN-PAY1-1 ( #I )
            pnd_Payess_Net_1_Pnd_Fin_Div1_1.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt());                                                          //Natural: ADD IAAA090F.CNTRL-FINAL-DIV-AMT TO #FIN-DIV1-1 ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Pnd_Payee_Cde.greater(1)))                                                                                                                //Natural: IF #PAYEE-CDE > 1
        {
            pnd_Tpa_Net_2_Pnd_Fund1_2.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees());                                                                  //Natural: ADD IAAA090F.CNTRL-TIAA-PAYEES TO #FUND1-2 ( #I )
            pnd_Tpa_Net_2_Pnd_Pay_Amt1_2.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt());                                                               //Natural: ADD IAAA090F.CNTRL-PER-PAY-AMT TO #PAY-AMT1-2 ( #I )
            pnd_Tpa_Net_2_Pnd_Div_Amt1_2.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt());                                                               //Natural: ADD IAAA090F.CNTRL-PER-DIV-AMT TO #DIV-AMT1-2 ( #I )
            pnd_Tpa_Net_2_Pnd_Fin_Pay1_2.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt());                                                             //Natural: ADD IAAA090F.CNTRL-FINAL-PAY-AMT TO #FIN-PAY1-2 ( #I )
            pnd_Tpa_Net_2_Pnd_Fin_Div1_2.getValue(pnd_I).nadd(pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt());                                                             //Natural: ADD IAAA090F.CNTRL-FINAL-DIV-AMT TO #FIN-DIV1-2 ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUBROUTINE GET-OPTN-CODE
    }
    private void sub_Get_Optn_Code() throws Exception                                                                                                                     //Natural: GET-OPTN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        short decideConditionsMet727 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #S-OPTN;//Natural: VALUE 22
        if (condition((pnd_S_Optn.equals(22))))
        {
            decideConditionsMet727++;
            pnd_I.setValue(2);                                                                                                                                            //Natural: ASSIGN #I := 2
        }                                                                                                                                                                 //Natural: VALUE 25,27
        else if (condition((pnd_S_Optn.equals(25) || pnd_S_Optn.equals(27))))
        {
            decideConditionsMet727++;
            pnd_I.setValue(3);                                                                                                                                            //Natural: ASSIGN #I := 3
        }                                                                                                                                                                 //Natural: VALUE 28:30
        else if (condition(((pnd_S_Optn.greaterOrEqual(28) && pnd_S_Optn.lessOrEqual(30)))))
        {
            decideConditionsMet727++;
            pnd_I.setValue(4);                                                                                                                                            //Natural: ASSIGN #I := 4
            total_Ivc_Amt.nadd(pnd_S_Cntrct_Ivc_Amt.getValue(1,":",2));                                                                                                   //Natural: ADD #S-CNTRCT-IVC-AMT ( 1:2 ) TO TOTAL-IVC-AMT
            total_Ivc_Amt_Used.nadd(pnd_S_Cntrct_Ivc_Used_Amt.getValue(1,":",2));                                                                                         //Natural: ADD #S-CNTRCT-IVC-USED-AMT ( 1:2 ) TO TOTAL-IVC-AMT-USED
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_I.setValue(1);                                                                                                                                            //Natural: ASSIGN #I := 1
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_I.equals(4)))                                                                                                                                   //Natural: IF #I = 4
        {
            if (condition(pnd_S_Curr_Dist_Cde.equals("RINV")))                                                                                                            //Natural: IF #S-CURR-DIST-CDE = 'RINV'
            {
                total_Rinv.nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                                                                            //Natural: ADD #SUMM-PER-PYMNT TO TOTAL-RINV
                total_Rinv.nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                                                                            //Natural: ADD #SUMM-PER-DVDND TO TOTAL-RINV
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tpa_Pnd_Fund.getValue(pnd_I).nadd(1);                                                                                                                         //Natural: ADD 1 TO #FUND ( #I )
        pnd_Tpa_Pnd_Pay_Amt.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                                                           //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT ( #I )
        pnd_Tpa_Pnd_Div_Amt.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                                                           //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT ( #I )
        pnd_Tpa_Pnd_Fin_Pay.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Pymnt);                                                                                           //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY ( #I )
        pnd_Tpa_Pnd_Fin_Div.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Dvdnd);                                                                                           //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV ( #I )
        if (condition(pnd_Input_Pnd_Payee_Cde.equals(1)))                                                                                                                 //Natural: IF #PAYEE-CDE = 1
        {
            pnd_Payess_1_Pnd_Fund_1.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #FUND-1 ( #I )
            pnd_Payess_1_Pnd_Pay_Amt_1.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                                                //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-1 ( #I )
            pnd_Payess_1_Pnd_Div_Amt_1.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                                                //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-1 ( #I )
            pnd_Payess_1_Pnd_Fin_Pay_1.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Pymnt);                                                                                //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-1 ( #I )
            pnd_Payess_1_Pnd_Fin_Div_1.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Dvdnd);                                                                                //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-1 ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Pnd_Payee_Cde.greater(1)))                                                                                                                //Natural: IF #PAYEE-CDE > 1
        {
            pnd_Payess_2_Pnd_Fund_2.getValue(pnd_I).nadd(1);                                                                                                              //Natural: ADD 1 TO #FUND-2 ( #I )
            pnd_Payess_2_Pnd_Pay_Amt_2.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Pymnt);                                                                                //Natural: ADD #SUMM-PER-PYMNT TO #PAY-AMT-2 ( #I )
            pnd_Payess_2_Pnd_Div_Amt_2.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Per_Dvdnd);                                                                                //Natural: ADD #SUMM-PER-DVDND TO #DIV-AMT-2 ( #I )
            pnd_Payess_2_Pnd_Fin_Pay_2.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Pymnt);                                                                                //Natural: ADD #SUMM-FIN-PYMNT TO #FIN-PAY-2 ( #I )
            pnd_Payess_2_Pnd_Fin_Div_2.getValue(pnd_I).nadd(pnd_Input_Pnd_Summ_Fin_Dvdnd);                                                                                //Natural: ADD #SUMM-FIN-DVDND TO #FIN-DIV-2 ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUBROUTINE GET-OPTN-CODE
    }
    private void sub_Print_Daily_Report_1() throws Exception                                                                                                              //Natural: PRINT-DAILY-REPORT-1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("DC");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'DC'
        pnd_Today.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #TODAY
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.compute(new ComputeParameters(false, pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte), DbsField.subtract(100000000,               //Natural: COMPUTE #CNTRL-INVRSE-DTE = 100000000 - VAL ( #TODAY )
            pnd_Today.val()));
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 WITH CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "R1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("R1")))
        {
            pnd_Isn_Cntrl_Rec.setValue(vw_iaa_Cntrl_Rcrd_1.getAstISN("R1"));                                                                                              //Natural: ASSIGN #ISN-CNTRL-REC := *ISN ( R1. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees()), iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees.subtract(ldaIaaa090a.getIaaa090_Cntrl_Tiaa_Payees())); //Natural: SUBTRACT IAAA090.CNTRL-TIAA-PAYEES FROM IAA-CNTRL-RCRD-1.CNTRL-TIAA-PAYEES GIVING IAAA090F.CNTRL-TIAA-PAYEES
        pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Per_Pay_Amt())); //Natural: SUBTRACT IAAA090.CNTRL-PER-PAY-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-PER-PAY-AMT GIVING IAAA090F.CNTRL-PER-PAY-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Per_Div_Amt())); //Natural: SUBTRACT IAAA090.CNTRL-PER-DIV-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-PER-DIV-AMT GIVING IAAA090F.CNTRL-PER-DIV-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Final_Pay_Amt())); //Natural: SUBTRACT IAAA090.CNTRL-FINAL-PAY-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-FINAL-PAY-AMT GIVING IAAA090F.CNTRL-FINAL-PAY-AMT
        //*  ADDED 8/96
        pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Final_Div_Amt())); //Natural: SUBTRACT IAAA090.CNTRL-FINAL-DIV-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-FINAL-DIV-AMT GIVING IAAA090F.CNTRL-FINAL-DIV-AMT
        FOR01:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_I).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_I));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #I ) := IAAA090.CNTRL-FUND-CDE ( #I )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I)),  //Natural: SUBTRACT IAAA090.CNTRL-FUND-PAYEES ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #I ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_I).subtract(ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_I)));
            //*  END OF ADD 8/96
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I)),            //Natural: SUBTRACT IAAA090.CNTRL-UNITS ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #I ) GIVING IAAA090F.CNTRL-UNITS ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_I).subtract(ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_I)));
            //*  ADDED 3/97
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I)),                //Natural: SUBTRACT IAAA090.CNTRL-AMT ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #I ) GIVING IAAA090F.CNTRL-AMT ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_I).subtract(ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_I)));
            //*  CALCULATION FOR MONTHLY FUNDS                        /* LB 10/97
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(20));                                                                                            //Natural: COMPUTE #J = #I + 20
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAAA090.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAAA090.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAAA090.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J)));
            //*  CALCULATION FOR ANNUAL T/L FUNDS                        /* LB 10/97
            //*  ADDED 8/96
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: COMPUTE #J = #I + 40
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAAA090.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAAA090.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAAA090.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J)));
            //*  CALCULATION FOR MONTHLY FUNDS                        /* LB 10/97
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(60));                                                                                            //Natural: COMPUTE #J = #I + 60
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAAA090.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAAA090.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAAA090.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt()), iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Cnt())); //Natural: SUBTRACT IAAA090.CNTRL-DDCTN-CNT FROM IAA-CNTRL-RCRD-1.CNTRL-DDCTN-CNT GIVING IAAA090F.CNTRL-DDCTN-CNT
        pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt.subtract(ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Amt())); //Natural: SUBTRACT IAAA090.CNTRL-DDCTN-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-DDCTN-AMT GIVING IAAA090F.CNTRL-DDCTN-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys()), iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys.subtract(ldaIaaa090a.getIaaa090_Cntrl_Actve_Tiaa_Pys())); //Natural: SUBTRACT IAAA090.CNTRL-ACTVE-TIAA-PYS FROM IAA-CNTRL-RCRD-1.CNTRL-ACTVE-TIAA-PYS GIVING IAAA090F.CNTRL-ACTVE-TIAA-PYS
        pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys()), iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys.subtract(ldaIaaa090a.getIaaa090_Cntrl_Actve_Cref_Pys())); //Natural: SUBTRACT IAAA090.CNTRL-ACTVE-CREF-PYS FROM IAA-CNTRL-RCRD-1.CNTRL-ACTVE-CREF-PYS GIVING IAAA090F.CNTRL-ACTVE-CREF-PYS
        pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees()), iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees.subtract(ldaIaaa090a.getIaaa090_Cntrl_Inactve_Payees())); //Natural: SUBTRACT IAAA090.CNTRL-INACTVE-PAYEES FROM IAA-CNTRL-RCRD-1.CNTRL-INACTVE-PAYEES GIVING IAAA090F.CNTRL-INACTVE-PAYEES
        //* *************************************
        //*  START OF REPORT 1, PAGE 1
        //*  TIAA, DEDUCTIONS AND ACTIVE TOTALS
        //* *************************************
        getReports().write(1, ReportOption.NOTITLE,"TIAA TRADITIONAL FUND RECORDS               ",iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees, new ReportEditMask                  //Natural: WRITE ( 1 ) 'TIAA TRADITIONAL FUND RECORDS               ' IAA-CNTRL-RCRD-1.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9+ ) / 'TIAA TRADITIONAL PERIODIC PAYMENTS    ' IAA-CNTRL-RCRD-1.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X IAAA090F.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) / 'TIAA TRADITIONAL PERIODIC DIVIDEND    ' IAA-CNTRL-RCRD-1.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X IAAA090F.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) / 'TIAA TRADITIONAL FINAL PAYMENTS       ' IAA-CNTRL-RCRD-1.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X IAAA090F.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) / 'TIAA TRADITIONAL FINAL DIVIDENDS      ' IAA-CNTRL-RCRD-1.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X IAAA090F.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ )
            ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Tiaa_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TIAA TRADITIONAL PERIODIC PAYMENTS    ",iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaIaaa090a.getIaaa090_Cntrl_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),NEWLINE,"TIAA TRADITIONAL PERIODIC DIVIDEND    ",iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaIaaa090a.getIaaa090_Cntrl_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),NEWLINE,"TIAA TRADITIONAL FINAL PAYMENTS       ",iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaIaaa090a.getIaaa090_Cntrl_Final_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),NEWLINE,"TIAA TRADITIONAL FINAL DIVIDENDS      ",iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaIaaa090a.getIaaa090_Cntrl_Final_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"));
        if (Global.isEscape()) return;
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  TRADITIONAL FUND RECORDS                ",new ColumnSpacing(22),pnd_Tpa_Pnd_Fund.getValue(4),                    //Natural: WRITE ( 1 ) 'TPA  TRADITIONAL FUND RECORDS                ' 22X #FUND ( 4 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  TRADITIONAL PERIODIC PAYMENTS     ' 22X #PAY-AMT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL PERIODIC DIVIDEND     ' 22X #DIV-AMT ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  TRADITIONAL FINAL PAYMENTS        ' 22X #FIN-PAY ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  TRADITIONAL PERIODIC PAYMENTS     ",new ColumnSpacing(22),pnd_Tpa_Pnd_Pay_Amt.getValue(4), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL PERIODIC DIVIDEND     ",new ColumnSpacing(22),pnd_Tpa_Pnd_Div_Amt.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  TRADITIONAL FINAL PAYMENTS        ",new ColumnSpacing(22),pnd_Tpa_Pnd_Fin_Pay.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  /     'TPA  PER IVC AMOUNT                    '
        //*   24X TOTAL-IVC-AMT           (EM=Z,ZZZ,ZZZ,ZZ9.99)
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  PAYEES = 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_1_Pnd_Fund_1.getValue(4),                  //Natural: WRITE ( 1 ) 'TPA  PAYEES = 1  FUND RECORDS           ' 27X #FUND-1 ( 4 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  PAYEES = 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-1 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES = 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-1 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES = 1  FINAL PAYMENTS         ' 21X #FIN-PAY-1 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  PAYEES = 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Pay_Amt_1.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES = 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Div_Amt_1.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES = 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Fin_Pay_1.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"TPA  PAYEES > 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_2_Pnd_Fund_2.getValue(4),                  //Natural: WRITE ( 1 ) 'TPA  PAYEES > 1  FUND RECORDS           ' 27X #FUND-2 ( 4 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TPA  PAYEES > 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-2 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES > 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-2 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TPA  PAYEES > 1  FINAL PAYMENTS         ' 21X #FIN-PAY-2 ( 4 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TPA  PAYEES > 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Pay_Amt_2.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES > 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Div_Amt_2.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TPA  PAYEES > 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Fin_Pay_2.getValue(4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ******************************************************'
        getReports().write(1, ReportOption.NOTITLE,"P&I  TRADIONAL FUND RECORDS                   ",new ColumnSpacing(21),pnd_Tpa_Pnd_Fund.getValue(2),                   //Natural: WRITE ( 1 ) 'P&I  TRADIONAL FUND RECORDS                   ' 21X #FUND ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  TRADITIONAL PERIODIC PAYMENTS      ' 21X #PAY-AMT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL PERIODIC DIVIDEND      ' 21X #DIV-AMT ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  TRADITIONAL FINAL PAYMENTS         ' 21X #FIN-PAY ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  TRADITIONAL PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Tpa_Pnd_Pay_Amt.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Tpa_Pnd_Div_Amt.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  TRADITIONAL FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Tpa_Pnd_Fin_Pay.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  PAYEES = 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_1_Pnd_Fund_1.getValue(2),                  //Natural: WRITE ( 1 ) 'P&I  PAYEES = 1  FUND RECORDS           ' 27X #FUND-1 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  PAYEES = 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES = 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES = 1  FINAL PAYMENTS         ' 21X #FIN-PAY-1 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  PAYEES = 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Pay_Amt_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES = 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Div_Amt_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES = 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Fin_Pay_1.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"P&I  PAYEES > 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_2_Pnd_Fund_2.getValue(2),                  //Natural: WRITE ( 1 ) 'P&I  PAYEES > 1  FUND RECORDS           ' 27X #FUND-2 ( 2 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'P&I  PAYEES > 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES > 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'P&I  PAYEES > 1  FINAL PAYMENTS         ' 21X #FIN-PAY-2 ( 2 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"P&I  PAYEES > 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Pay_Amt_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES > 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Div_Amt_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"P&I  PAYEES > 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Fin_Pay_2.getValue(2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *****************************************************'
        getReports().write(1, ReportOption.NOTITLE,"IPRO TRADIONAL FUND RECORDS                  ",new ColumnSpacing(22),pnd_Tpa_Pnd_Fund.getValue(3),                    //Natural: WRITE ( 1 ) 'IPRO TRADIONAL FUND RECORDS                  ' 22X #FUND ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO TRADITIONAL PERIODIC PAYMENTS    ' 23X #PAY-AMT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL PERIODIC DIVIDEND    ' 23X #DIV-AMT ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO TRADITIONAL FINAL PAYMENTS       ' 23X #FIN-PAY ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO TRADITIONAL PERIODIC PAYMENTS    ",new ColumnSpacing(23),pnd_Tpa_Pnd_Pay_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL PERIODIC DIVIDEND    ",new ColumnSpacing(23),pnd_Tpa_Pnd_Div_Amt.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO TRADITIONAL FINAL PAYMENTS       ",new ColumnSpacing(23),pnd_Tpa_Pnd_Fin_Pay.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ******************************************************'
        getReports().write(1, ReportOption.NOTITLE,"IPRO PAYEES = 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_1_Pnd_Fund_1.getValue(3),                  //Natural: WRITE ( 1 ) 'IPRO PAYEES = 1  FUND RECORDS           ' 27X #FUND-1 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO PAYEES = 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES = 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES = 1  FINAL PAYMENTS         ' 21X #FIN-PAY-1 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO PAYEES = 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Pay_Amt_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES = 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Div_Amt_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES = 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_1_Pnd_Fin_Pay_1.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *******************************************************
        getReports().write(1, ReportOption.NOTITLE,"IPRO PAYEES > 1  FUND RECORDS           ",new ColumnSpacing(27),pnd_Payess_2_Pnd_Fund_2.getValue(3),                  //Natural: WRITE ( 1 ) 'IPRO PAYEES > 1  FUND RECORDS           ' 27X #FUND-2 ( 3 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'IPRO PAYEES > 1  PERIODIC PAYMENTS      ' 21X #PAY-AMT-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES > 1  PERIODIC DIVIDEND      ' 21X #DIV-AMT-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'IPRO PAYEES > 1  FINAL PAYMENTS         ' 21X #FIN-PAY-2 ( 3 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"IPRO PAYEES > 1  PERIODIC PAYMENTS      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Pay_Amt_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES > 1  PERIODIC DIVIDEND      ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Div_Amt_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IPRO PAYEES > 1  FINAL PAYMENTS         ",new ColumnSpacing(21),pnd_Payess_2_Pnd_Fin_Pay_2.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* ****************************************************'
        getReports().write(1, ReportOption.NOTITLE,"OTHER TRADIONAL FUND RECORDS                 ",new ColumnSpacing(22),pnd_Tpa_Pnd_Fund.getValue(1),                    //Natural: WRITE ( 1 ) 'OTHER TRADIONAL FUND RECORDS                 ' 22X #FUND ( 1 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'OTHER TRADITIONAL PERIODIC PAYMENTS  ' 24X #PAY-AMT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL PERIODIC DIVIDEND  ' 24X #DIV-AMT ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL FINAL PAYMENTS     ' 24X #FIN-PAY ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OTHER TRADITIONAL FINAL DIVIDEND      ' 23X #FIN-DIV ( 1 ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"OTHER TRADITIONAL PERIODIC PAYMENTS  ",new ColumnSpacing(24),pnd_Tpa_Pnd_Pay_Amt.getValue(1), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL PERIODIC DIVIDEND  ",new ColumnSpacing(24),pnd_Tpa_Pnd_Div_Amt.getValue(1), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL FINAL PAYMENTS     ",new ColumnSpacing(24),pnd_Tpa_Pnd_Fin_Pay.getValue(1), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OTHER TRADITIONAL FINAL DIVIDEND      ",new ColumnSpacing(23),pnd_Tpa_Pnd_Fin_Div.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
        //*  3/97
        getReports().write(1, ReportOption.NOTITLE,"TIAA ANNUAL REAL-ESTATE FUND RECORDS        ",iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(9), new                     //Natural: WRITE ( 1 ) 'TIAA ANNUAL REAL-ESTATE FUND RECORDS        ' IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9+ ) / 'TIAA ANNUAL REAL-ESTATE UNITS          ' IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090F.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) / 'TIAA ANNUAL REAL-ESTATE DOLLAR AMOUNTS  ' IAA-CNTRL-RCRD-1.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X IAAA090.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 9X IAAA090F.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99+ )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(9), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(9), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TIAA ANNUAL REAL-ESTATE UNITS          ",iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),NEWLINE,"TIAA ANNUAL REAL-ESTATE DOLLAR AMOUNTS  ",iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(9),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99+"));
        if (Global.isEscape()) return;
        //*  9/08
        //*  9/08
        getReports().write(1, ReportOption.NOTITLE,"TIAA ANNUAL ACCESS FUND RECORDS             ",iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(11), new                    //Natural: WRITE ( 1 ) 'TIAA ANNUAL ACCESS FUND RECORDS             ' IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9+ ) / 'TIAA ANNUAL ACCESS UNITS               ' IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090F.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) / 'TIAA ANNUAL ACCESS DOLLAR AMOUNTS       ' IAA-CNTRL-RCRD-1.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X IAAA090.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 9X IAAA090F.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99+ )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(11), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(11), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TIAA ANNUAL ACCESS UNITS               ",iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(11), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),NEWLINE,"TIAA ANNUAL ACCESS DOLLAR AMOUNTS       ",iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(11), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(9),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99+"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA MONTHLY REAL ESTATE FUNDS                      /*  LB 10/97
        getReports().write(1, ReportOption.NOTITLE,"TIAA MONTHLY REAL-ESTATE FUND RECORDS       ",iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(29), new                    //Natural: WRITE ( 1 ) 'TIAA MONTHLY REAL-ESTATE FUND RECORDS       ' IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9+ ) / 'TIAA MONTHLY REAL-ESTATE UNITS         ' IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090F.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999+ )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(29), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(29), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TIAA MONTHLY REAL-ESTATE UNITS         ",iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(29), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA MONTHLY ACCESS FUNDS                        /* 9/08
        //*  9/08
        //*  9/08
        getReports().write(1, ReportOption.NOTITLE,"TIAA MONTHLY ACCESS FUND RECORDS            ",iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(31), new                    //Natural: WRITE ( 1 ) 'TIAA MONTHLY ACCESS FUND RECORDS            ' IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9+ ) / 'TIAA MONTHLY ACCESS UNITS              ' IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090F.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999+ )
            ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(31), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(31), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TIAA MONTHLY ACCESS UNITS              ",iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(31), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"ACTIVE DEDUCTIONS                           ",iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt, new ReportEditMask                    //Natural: WRITE ( 1 ) 'ACTIVE DEDUCTIONS                           ' IAA-CNTRL-RCRD-1.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9+ ) / 'DEDUCTIONS                              ' IAA-CNTRL-RCRD-1.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.999 ) 9X IAAA090F.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.999+ ) // 'TIAA ACTIVE PAYEES                          ' IAA-CNTRL-RCRD-1.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9+ ) / 'CREF ACTIVE PAYEES                          ' IAA-CNTRL-RCRD-1.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9+ ) / 'INACTIVE PAYEES                             ' IAA-CNTRL-RCRD-1.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 13X IAAA090F.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9+ ) / 'TOTAL IVC AMT' 50X TOTAL-IVC-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL IVC USED AMT' 45X TOTAL-IVC-AMT-USED ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Cnt(), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"DEDUCTIONS                              ",iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(8),ldaIaaa090a.getIaaa090_Cntrl_Ddctn_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(9),pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),NEWLINE,NEWLINE,"TIAA ACTIVE PAYEES                          ",iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Actve_Tiaa_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"CREF ACTIVE PAYEES                          ",iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Actve_Cref_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"INACTIVE PAYEES                             ",iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),ldaIaaa090a.getIaaa090_Cntrl_Inactve_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9"),new 
            ColumnSpacing(13),pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),NEWLINE,"TOTAL IVC AMT",new ColumnSpacing(50),total_Ivc_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL IVC USED AMT",new ColumnSpacing(45),total_Ivc_Amt_Used, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        total_Ivc_Amt_Remained.compute(new ComputeParameters(false, total_Ivc_Amt_Remained), total_Ivc_Amt.subtract(total_Ivc_Amt_Used));                                 //Natural: ASSIGN TOTAL-IVC-AMT-REMAINED := TOTAL-IVC-AMT - TOTAL-IVC-AMT-USED
        getReports().write(1, ReportOption.NOTITLE,"TOTAL IVC REMAIN AMT",new ColumnSpacing(43),total_Ivc_Amt_Remained, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));           //Natural: WRITE ( 1 ) 'TOTAL IVC REMAIN AMT' 43X TOTAL-IVC-AMT-REMAINED ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL RINV AMT      ",new ColumnSpacing(43),total_Rinv, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));                       //Natural: WRITE ( 1 ) 'TOTAL RINV AMT      ' 43X TOTAL-RINV ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 1, PAGE 2
        //*  CREF ANNUAL FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_2_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-2-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF ANNUAL");                                                                                                      //Natural: ASSIGN #FUND-CREF-CON = 'CREF ANNUAL'
        FOR03:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            pnd_Occ_A_Pnd_Occ.setValue(pnd_I);                                                                                                                            //Natural: ASSIGN #OCC := #I
            //*  9/08 ==> 11 IS TIAA ACCESS
            if (condition(pnd_Occ_A_Pnd_Occ.notEquals(9) && pnd_Occ_A_Pnd_Occ.notEquals(11)))                                                                             //Natural: IF #OCC NE 9 AND #OCC NE 11
            {
                pnd_Parm_Desc.reset();                                                                                                                                    //Natural: RESET #PARM-DESC
                pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                              //Natural: ASSIGN #PARM-FUND-2 := #OCC
                pnd_Parm_Len.setValue(6);                                                                                                                                 //Natural: ASSIGN #PARM-LEN := 6
                //*  ADDED 3/97
                DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                 //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
                if (condition(Global.isEscape())) return;
                pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                  //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9 ) 67T IAAA090.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9 ) 90T IAAA090F.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9+ )
                    TabSetting(45),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(67),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(90),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZ9+"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'UNITS       '
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 40T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 62T IAAA090.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 85T IAAA090F.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999+ )
                    TabSetting(40),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(62),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_I), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(85),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I), new ReportEditMask 
                    ("ZZZZ,ZZZ,ZZ9.999+"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("DOLLAR AMT  ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'DOLLAR AMT  '
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 41T IAA-CNTRL-RCRD-1.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 63T IAAA090.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 86T IAAA090F.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99+ )
                    TabSetting(41),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(63),ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_I), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(86),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99+"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF ADD  8/96
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 1, PAGE 3                         /* LB 10/97
        //*  CREF MONTHLY FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_3_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-3-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF MONTHLY");                                                                                                     //Natural: ASSIGN #FUND-CREF-CON = 'CREF MONTHLY'
        FOR04:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(20));                                                                                            //Natural: ASSIGN #J := #I + 20
            pnd_Occ_A_Pnd_Occ.setValue(pnd_I);                                                                                                                            //Natural: ASSIGN #OCC := #I
            //*  9/08 ==> 11 IS TIAA ACCESS
            if (condition(pnd_Occ_A_Pnd_Occ.notEquals(9) && pnd_Occ_A_Pnd_Occ.notEquals(11)))                                                                             //Natural: IF #OCC NE 9 AND #OCC NE 11
            {
                pnd_Parm_Desc.reset();                                                                                                                                    //Natural: RESET #PARM-DESC
                pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                              //Natural: ASSIGN #PARM-FUND-2 := #OCC
                pnd_Parm_Len.setValue(6);                                                                                                                                 //Natural: ASSIGN #PARM-LEN := 6
                DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                 //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
                if (condition(Global.isEscape())) return;
                pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                  //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 67T IAAA090.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 90T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ )
                    TabSetting(45),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(67),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(90),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZ9+"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'UNITS       '
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 40T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 62T IAAA090.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 85T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ )
                    TabSetting(40),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(62),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(85),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), new ReportEditMask 
                    ("ZZZZ,ZZZ,ZZ9.999+"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 1, PAGE 4                         /* LB 10/97
        //*   T/L ANNUAL FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue("T/L ANNUAL TOTALS");                                                                                            //Natural: MOVE 'T/L ANNUAL TOTALS' TO #REPORT-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L ANNUAL");                                                                                                       //Natural: ASSIGN #FUND-CREF-CON = 'T/L ANNUAL'
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J := #I + 40
            pnd_Occ_A_Pnd_Occ.setValue(pnd_J);                                                                                                                            //Natural: ASSIGN #OCC := #J
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                                  //Natural: ASSIGN #PARM-FUND-2 := #OCC
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc.equals(" ") && pnd_Cmpny_Desc.equals(" ")))                                                                                       //Natural: IF #PARM-DESC = ' ' AND #CMPNY-DESC = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 67T IAAA090.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 90T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ )
                TabSetting(45),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(67),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(90),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9+"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'UNITS       '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 40T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 62T IAAA090.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 85T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ )
                TabSetting(40),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(62),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(85),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("DOLLAR AMT  ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'DOLLAR AMT  '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 41T IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 63T IAAA090.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 86T IAAA090F.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99+ )
                TabSetting(41),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(63),ldaIaaa090a.getIaaa090_Cntrl_Amt().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(86),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99+"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 1, PAGE 5                         /* LB 10/97
        //*  T/L  MONTHLY FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue("T/L MONTHLY TOTALS");                                                                                           //Natural: MOVE 'T/L MONTHLY TOTALS' TO #REPORT-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L  MONTHLY");                                                                                                     //Natural: ASSIGN #FUND-CREF-CON = 'T/L  MONTHLY'
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J := #I + 40
            pnd_Occ_A_Pnd_Occ.setValue(pnd_J);                                                                                                                            //Natural: ASSIGN #OCC := #J
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                                  //Natural: ASSIGN #PARM-FUND-2 := #OCC
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc.equals(" ") && pnd_Cmpny_Desc.equals(" ")))                                                                                       //Natural: IF #PARM-DESC = ' ' AND #CMPNY-DESC = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(60));                                                                                            //Natural: ASSIGN #J := #I + 60
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) / 1T #FUND-DESC-LNE 45T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 67T IAAA090.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 90T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ )
                TabSetting(45),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(67),ldaIaaa090a.getIaaa090_Cntrl_Fund_Payees().getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(90),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9+"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'UNITS       '
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 1 ) 1T #FUND-DESC-LNE 40T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 62T IAAA090.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 85T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ )
                TabSetting(40),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(62),ldaIaaa090a.getIaaa090_Cntrl_Units().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(85),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  UPDATE THE CONTROL RECORD
        if (condition(pnd_Isn_Cntrl_Rec.greater(getZero())))                                                                                                              //Natural: IF #ISN-CNTRL-REC > 0
        {
            G1:                                                                                                                                                           //Natural: GET IAA-CNTRL-RCRD-1 #ISN-CNTRL-REC
            vw_iaa_Cntrl_Rcrd_1.readByID(pnd_Isn_Cntrl_Rec.getLong(), "G1");
            //* ***ADD 12/1/2000 IK
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(41).setValue("U41");                                                                                         //Natural: MOVE 'U41' TO IAAA090.CNTRL-FUND-CDE ( 41 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(42).setValue("U42");                                                                                         //Natural: MOVE 'U42' TO IAAA090.CNTRL-FUND-CDE ( 42 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(43).setValue("U43");                                                                                         //Natural: MOVE 'U43' TO IAAA090.CNTRL-FUND-CDE ( 43 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(44).setValue("U44");                                                                                         //Natural: MOVE 'U44' TO IAAA090.CNTRL-FUND-CDE ( 44 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(45).setValue("U45");                                                                                         //Natural: MOVE 'U45' TO IAAA090.CNTRL-FUND-CDE ( 45 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(46).setValue("U46");                                                                                         //Natural: MOVE 'U46' TO IAAA090.CNTRL-FUND-CDE ( 46 )
            //*  ADDED 7/02 NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(47).setValue("U47");                                                                                         //Natural: MOVE 'U47' TO IAAA090.CNTRL-FUND-CDE ( 47 )
            //*  ADDED 7/02NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(48).setValue("U48");                                                                                         //Natural: MOVE 'U48' TO IAAA090.CNTRL-FUND-CDE ( 48 )
            //*  ADDED 7/02NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(49).setValue("U49");                                                                                         //Natural: MOVE 'U49' TO IAAA090.CNTRL-FUND-CDE ( 49 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(61).setValue("W41");                                                                                         //Natural: MOVE 'W41' TO IAAA090.CNTRL-FUND-CDE ( 61 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(62).setValue("W42");                                                                                         //Natural: MOVE 'W42' TO IAAA090.CNTRL-FUND-CDE ( 62 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(63).setValue("W43");                                                                                         //Natural: MOVE 'W43' TO IAAA090.CNTRL-FUND-CDE ( 63 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(64).setValue("W44");                                                                                         //Natural: MOVE 'W44' TO IAAA090.CNTRL-FUND-CDE ( 64 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(65).setValue("W45");                                                                                         //Natural: MOVE 'W45' TO IAAA090.CNTRL-FUND-CDE ( 65 )
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(66).setValue("W46");                                                                                         //Natural: MOVE 'W46' TO IAAA090.CNTRL-FUND-CDE ( 66 )
            //*  ADDED 7/02 NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(67).setValue("W47");                                                                                         //Natural: MOVE 'W47' TO IAAA090.CNTRL-FUND-CDE ( 67 )
            //*  ADDED 7/02NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(68).setValue("W48");                                                                                         //Natural: MOVE 'W48' TO IAAA090.CNTRL-FUND-CDE ( 68 )
            //*  ADDED 7/02NEW PA SELCT
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(69).setValue("W49");                                                                                         //Natural: MOVE 'W49' TO IAAA090.CNTRL-FUND-CDE ( 69 )
            //*  9/08 TIAA ACCESS ANNUAL
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(11).setValue("U11");                                                                                         //Natural: MOVE 'U11' TO IAAA090.CNTRL-FUND-CDE ( 11 )
            //*  9/08 TIAA ACCESS MONTHLY
            ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(31).setValue("W11");                                                                                         //Natural: MOVE 'W11' TO IAAA090.CNTRL-FUND-CDE ( 31 )
            //* ***ADD 12/1/2000 IK
            vw_iaa_Cntrl_Rcrd_1.setValuesByName(ldaIaaa090a.getIaaa090());                                                                                                //Natural: MOVE BY NAME IAAA090 TO IAA-CNTRL-RCRD-1
            iaa_Cntrl_Rcrd_1_Cntrl_Cde.setValue("DC");                                                                                                                    //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-CDE := 'DC'
            vw_iaa_Cntrl_Rcrd_1.updateDBRow("G1");                                                                                                                        //Natural: UPDATE ( G1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "NO PREVIOUS CONTROL RECORD");                                                                                                          //Natural: WRITE 'NO PREVIOUS CONTROL RECORD'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUBROUTINE PRINT-DAILY-REPORT-1
    }
    private void sub_Print_Daily_Report_2() throws Exception                                                                                                              //Natural: PRINT-DAILY-REPORT-2
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 WITH CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ02")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET PRESENT MONTH
        pnd_Begin_Month.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #BEGIN-MONTH
        pnd_Begin_Month_Pnd_Begin_Mm.nsubtract(1);                                                                                                                        //Natural: ASSIGN #BEGIN-MM := #BEGIN-MM - 1
        if (condition(pnd_Begin_Month_Pnd_Begin_Mm.equals(getZero())))                                                                                                    //Natural: IF #BEGIN-MM = 0
        {
            pnd_Begin_Month_Pnd_Begin_Mm.setValue(12);                                                                                                                    //Natural: ASSIGN #BEGIN-MM := 12
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ IAA-CNTRL-RCRD-1 WITH CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ03",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ03")))
        {
            if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Cde.notEquals("DC")))                                                                                                    //Natural: REJECT IF IAA-CNTRL-RCRD-1.CNTRL-CDE NE 'DC'
            {
                continue;
            }
            pnd_Compare_D.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #COMPARE-D
            if (condition(pnd_Compare_D_Pnd_Compare_D_Mm.equals(pnd_Begin_Month_Pnd_Begin_Mm)))                                                                           //Natural: IF #COMPARE-D-MM = #BEGIN-MM
            {
                vw_iaa_Cntrl_Rcrd_Month.setValuesByName(vw_iaa_Cntrl_Rcrd_1);                                                                                             //Natural: MOVE BY NAME IAA-CNTRL-RCRD-1 TO IAA-CNTRL-RCRD-MONTH
                if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte.greater(getZero())))                                                                                  //Natural: IF IAA-CNTRL-RCRD-1.CNTRL-FRST-TRANS-DTE GT 0
                {
                    pnd_Begin_Month.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-FRST-TRANS-DTE ( EM = YYYYMMDD ) TO #BEGIN-MONTH
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Today.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #TODAY
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte.compute(new ComputeParameters(false, pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte), DbsField.subtract(100000000,               //Natural: COMPUTE #CNTRL-INVRSE-DTE = 100000000 - VAL ( #TODAY )
            pnd_Today.val()));
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 WITH CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ04",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ04:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ04")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees()), iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-TIAA-PAYEES FROM IAA-CNTRL-RCRD-1.CNTRL-TIAA-PAYEES GIVING IAAA090F.CNTRL-TIAA-PAYEES
        pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-PER-PAY-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-PER-PAY-AMT GIVING IAAA090F.CNTRL-PER-PAY-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-PER-DIV-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-PER-DIV-AMT GIVING IAAA090F.CNTRL-PER-DIV-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FINAL-PAY-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-FINAL-PAY-AMT GIVING IAAA090F.CNTRL-FINAL-PAY-AMT
        //*  ADDED 8/96
        pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FINAL-DIV-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-FINAL-DIV-AMT GIVING IAAA090F.CNTRL-FINAL-DIV-AMT
        FOR07:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_I).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_I));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #I ) := IAAA090.CNTRL-FUND-CDE ( #I )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I)),  //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #I ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_I).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_I)));
            //*  END OF ADD 8/96
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I)),            //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #I ) GIVING IAAA090F.CNTRL-UNITS ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_I).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_I)));
            //*  ADD 3/97
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I)),                //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #I ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #I ) GIVING IAAA090F.CNTRL-AMT ( #I )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_I).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_I)));
            //*  CALCULATE THE CREF MONTHLY FUND TOTALS           /* LB 10/97
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(20));                                                                                            //Natural: ASSIGN #J = #I + 20
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_J)));
            //*  ADDED 8/96
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J = #I + 40
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_J)));
            //*  CALCULATE THE T/L  MONTHLY FUND TOTALS
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(60));                                                                                            //Natural: ASSIGN #J = #I + 60
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Cde().getValue(pnd_J).setValue(ldaIaaa090a.getIaaa090_Cntrl_Fund_Cde().getValue(pnd_J));                                   //Natural: ASSIGN IAAA090F.CNTRL-FUND-CDE ( #J ) := IAAA090.CNTRL-FUND-CDE ( #J )
            pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J)),  //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) GIVING IAAA090F.CNTRL-FUND-PAYEES ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J)),            //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) GIVING IAAA090F.CNTRL-UNITS ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J)));
            pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J).compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J)),                //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #J ) FROM IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) GIVING IAAA090F.CNTRL-AMT ( #J )
                iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J).subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_J)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt()), iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-DDCTN-CNT FROM IAA-CNTRL-RCRD-1.CNTRL-DDCTN-CNT GIVING IAAA090F.CNTRL-DDCTN-CNT
        pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt()), iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-DDCTN-AMT FROM IAA-CNTRL-RCRD-1.CNTRL-DDCTN-AMT GIVING IAAA090F.CNTRL-DDCTN-AMT
        pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys()), iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-ACTVE-TIAA-PYS FROM IAA-CNTRL-RCRD-1.CNTRL-ACTVE-TIAA-PYS GIVING IAAA090F.CNTRL-ACTVE-TIAA-PYS
        pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys()), iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-ACTVE-CREF-PYS FROM IAA-CNTRL-RCRD-1.CNTRL-ACTVE-CREF-PYS GIVING IAAA090F.CNTRL-ACTVE-CREF-PYS
        pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees().compute(new ComputeParameters(false, pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees()), iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees.subtract(iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees)); //Natural: SUBTRACT IAA-CNTRL-RCRD-MONTH.CNTRL-INACTVE-PAYEES FROM IAA-CNTRL-RCRD-1.CNTRL-INACTVE-PAYEES GIVING IAAA090F.CNTRL-INACTVE-PAYEES
        //* *************************************
        //*  START OF REPORT 2, PAGE 1
        //*  TIAA, DEDUCTIONS AND ACTIVE TOTALS
        //* *************************************
        //*  PERFORM  GET-OPTN-CODE-1
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_1_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-1-HEADING2
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"TIAA TRADITIONAL FUND RECORDS                  ",iaa_Cntrl_Rcrd_Month_Cntrl_Tiaa_Payees,              //Natural: WRITE ( 2 ) // 'TIAA TRADITIONAL FUND RECORDS                  ' IAA-CNTRL-RCRD-MONTH.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-TIAA-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) / 'TIAA TRADITIONAL PERIODIC PAYMENTS       ' IAA-CNTRL-RCRD-MONTH.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090F.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) 6X IAA-CNTRL-RCRD-1.CNTRL-PER-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL PERIODIC DIVIDEND       ' IAA-CNTRL-RCRD-MONTH.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090F.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) 6X IAA-CNTRL-RCRD-1.CNTRL-PER-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL FINAL PAYMENTS          ' IAA-CNTRL-RCRD-MONTH.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090F.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) 6X IAA-CNTRL-RCRD-1.CNTRL-FINAL-PAY-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TRADITIONAL FINAL DIVIDENDS         ' IAA-CNTRL-RCRD-MONTH.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 6X IAAA090F.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99+ ) 6X IAA-CNTRL-RCRD-1.CNTRL-FINAL-DIV-AMT ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Tiaa_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TIAA TRADITIONAL PERIODIC PAYMENTS       ",iaa_Cntrl_Rcrd_Month_Cntrl_Per_Pay_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pdaIaaa090f.getIaaa090f_Cntrl_Per_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(6),iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL PERIODIC DIVIDEND       ",iaa_Cntrl_Rcrd_Month_Cntrl_Per_Div_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pdaIaaa090f.getIaaa090f_Cntrl_Per_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(6),iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL FINAL PAYMENTS          ",iaa_Cntrl_Rcrd_Month_Cntrl_Final_Pay_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pdaIaaa090f.getIaaa090f_Cntrl_Final_Pay_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(6),iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TRADITIONAL FINAL DIVIDENDS         ",iaa_Cntrl_Rcrd_Month_Cntrl_Final_Div_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),pdaIaaa090f.getIaaa090f_Cntrl_Final_Div_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(6),iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA ANNUAL REAL ESTATE FUNDS
        //*  3/97
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, ReportOption.NOTITLE,"TIAA ANNUAL REAL-ESTATE FUND RECORDS           ",iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(9),                  //Natural: WRITE ( 2 ) 'TIAA ANNUAL REAL-ESTATE FUND RECORDS           ' IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 9 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TIAA ANNUAL REAL-ESTATE UNITS             ' IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090F.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) 7X IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) / 'TIAA ANNUAL REAL-ESTATE DOLLAR AMOUNTS     ' IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X IAAA090F.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99+ ) 8X IAA-CNTRL-RCRD-1.CNTRL-AMT ( 9 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(9), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(9), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TIAA ANNUAL REAL-ESTATE UNITS             ",iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),new 
            ColumnSpacing(7),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ANNUAL REAL-ESTATE DOLLAR AMOUNTS     ",iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(8),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(9), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA ANNUAL ACCESS FUNDS                            /* 9/08
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, ReportOption.NOTITLE,"TIAA ANNUAL ACCESS FUND RECORDS                ",iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(11),                 //Natural: WRITE ( 2 ) 'TIAA ANNUAL ACCESS FUND RECORDS                ' IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 11 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TIAA ANNUAL ACCESS UNITS                  ' IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090F.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) 7X IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) / 'TIAA ANNUAL ACCESS DOLLAR AMOUNTS          ' IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X IAAA090F.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99+ ) 8X IAA-CNTRL-RCRD-1.CNTRL-AMT ( 11 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(11), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(11), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TIAA ANNUAL ACCESS UNITS                  ",iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(11), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),new 
            ColumnSpacing(7),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ANNUAL ACCESS DOLLAR AMOUNTS          ",iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(11), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99+"),new 
            ColumnSpacing(8),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(11), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA MONTHLY REAL ESTATE FUNDS                       /* LB 10/97
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().write(2, ReportOption.NOTITLE,"TIAA MONTHLY REAL-ESTATE FUND RECORDS          ",iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(29),                 //Natural: WRITE ( 2 ) 'TIAA MONTHLY REAL-ESTATE FUND RECORDS          ' IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 29 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TIAA MONTHLY REAL-ESTATE UNITS            ' IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090F.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) 7X IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 29 ) ( EM = ZZZ,ZZZ,ZZ9.999 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(29), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(29), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TIAA MONTHLY REAL-ESTATE UNITS            ",iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(29), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),new 
            ColumnSpacing(7),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(29), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        //*  WRITE TIAA MONTHLY ACCESS FUNDS                            /* 9/08
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().write(2, ReportOption.NOTITLE,"TIAA MONTHLY ACCESS FUND RECORDS               ",iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(31),                 //Natural: WRITE ( 2 ) 'TIAA MONTHLY ACCESS FUND RECORDS               ' IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( 31 ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TIAA MONTHLY ACCESS UNITS                 ' IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999 ) 7X IAAA090F.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999+ ) 7X IAA-CNTRL-RCRD-1.CNTRL-UNITS ( 31 ) ( EM = ZZZ,ZZZ,ZZ9.999 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(31), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(31), new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TIAA MONTHLY ACCESS UNITS                 ",iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(31), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999+"),new 
            ColumnSpacing(7),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(31), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        //*  WRITE DEDUCTIONS AND ACTIVE TOTALS
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().write(2, ReportOption.NOTITLE,"ACTIVE DEDUCTIONS                              ",iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Cnt, new ReportEditMask             //Natural: WRITE ( 2 ) 'ACTIVE DEDUCTIONS                              ' IAA-CNTRL-RCRD-MONTH.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-DDCTN-CNT ( EM = ZZ,ZZZ,ZZ9 ) / 'DEDUCTIONS                                 ' IAA-CNTRL-RCRD-MONTH.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.999 ) 8X IAAA090F.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.99+ ) 8X IAA-CNTRL-RCRD-1.CNTRL-DDCTN-AMT ( EM = ZZZ,ZZZ,ZZ9.999 ) // 'TIAA ACTIVE PAYEES                             ' IAA-CNTRL-RCRD-MONTH.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-ACTVE-TIAA-PYS ( EM = ZZ,ZZZ,ZZ9 ) / 'CREF ACTIVE PAYEES                             ' IAA-CNTRL-RCRD-MONTH.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-ACTVE-CREF-PYS ( EM = ZZ,ZZZ,ZZ9 ) / 'INACTIVE PAYEES                                ' IAA-CNTRL-RCRD-MONTH.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 ) 12X IAAA090F.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9+ ) 12X IAA-CNTRL-RCRD-1.CNTRL-INACTVE-PAYEES ( EM = ZZ,ZZZ,ZZ9 )
            ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Cnt(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"DEDUCTIONS                                 ",iaa_Cntrl_Rcrd_Month_Cntrl_Ddctn_Amt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),pdaIaaa090f.getIaaa090f_Cntrl_Ddctn_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99+"),new ColumnSpacing(8),iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"TIAA ACTIVE PAYEES                             ",iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Tiaa_Pys, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Actve_Tiaa_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"CREF ACTIVE PAYEES                             ",iaa_Cntrl_Rcrd_Month_Cntrl_Actve_Cref_Pys, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Actve_Cref_Pys(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"INACTIVE PAYEES                                ",iaa_Cntrl_Rcrd_Month_Cntrl_Inactve_Payees, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(12),pdaIaaa090f.getIaaa090f_Cntrl_Inactve_Payees(), new ReportEditMask ("ZZ,ZZZ,ZZ9+"),new 
            ColumnSpacing(12),iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees, new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 2, PAGE 2
        //*  CREF ANNUAL FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_2_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-2-HEADING2
        //*  8/96
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF ANNUAL");                                                                                                      //Natural: ASSIGN #FUND-CREF-CON = 'CREF ANNUAL'
        FOR09:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            pnd_Occ_A_Pnd_Occ.setValue(pnd_I);                                                                                                                            //Natural: ASSIGN #OCC := #I
            //*  9/08 ==> 11 IS TIAA ACCESS
            if (condition(pnd_Occ_A_Pnd_Occ.notEquals(9) && pnd_Occ_A_Pnd_Occ.notEquals(11)))                                                                             //Natural: IF #OCC NE 9 AND #OCC NE 11
            {
                pnd_Parm_Desc.reset();                                                                                                                                    //Natural: RESET #PARM-DESC
                pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                              //Natural: ASSIGN #PARM-FUND-2 := #OCC
                pnd_Parm_Len.setValue(6);                                                                                                                                 //Natural: ASSIGN #PARM-LEN := 6
                //*  3/97
                DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                 //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
                if (condition(Global.isEscape())) return;
                pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                  //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) / 1T #FUND-DESC-LNE 48T IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9 ) 70T IAAA090F.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9+ ) 93T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9 )
                    TabSetting(48),iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(70),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9+"),new TabSetting(93),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'UNITS       '
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 43T IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 65T IAAA090F.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999+ ) 88T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                    TabSetting(43),iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(65),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_I), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"),new TabSetting(88),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("DOLLAR AMT  ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'DOLLAR AMT  '
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 44T IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 66T IAAA090F.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99+ ) 89T IAA-CNTRL-RCRD-1.CNTRL-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                    TabSetting(44),iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_I), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99+"),new TabSetting(89),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 2, PAGE 3                                  /* LB 10/97
        //*  CREF MONTHLY FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue(pnd_Reprot_Headings_Pnd_Page_3_Heading2);                                                                        //Natural: ASSIGN #REPORT-HEADING2 = #PAGE-3-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("CREF MONTHLY");                                                                                                     //Natural: ASSIGN #FUND-CREF-CON = 'CREF MONTHLY'
        FOR10:                                                                                                                                                            //Natural: FOR #I = 2 TO #MAX-PRD-CDE
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(pnd_Max_Prd_Cde)); pnd_I.nadd(1))
        {
            //*  9/08 ==> 11 IS TIAA ACCESS
            if (condition(pnd_I.notEquals(9) && pnd_I.notEquals(11)))                                                                                                     //Natural: IF #I NE 9 AND #I NE 11
            {
                pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(20));                                                                                        //Natural: ASSIGN #J := #I + 20
                pnd_Occ_A_Pnd_Occ.setValue(pnd_I);                                                                                                                        //Natural: ASSIGN #OCC := #I
                pnd_Parm_Desc.reset();                                                                                                                                    //Natural: RESET #PARM-DESC
                pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                              //Natural: ASSIGN #PARM-FUND-2 := #OCC
                pnd_Parm_Len.setValue(6);                                                                                                                                 //Natural: ASSIGN #PARM-LEN := 6
                DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                 //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
                if (condition(Global.isEscape())) return;
                pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                  //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) / 1T #FUND-DESC-LNE 48T IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 70T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ ) 93T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 )
                    TabSetting(48),iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(70),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9+"),new TabSetting(93),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                  //Natural: ASSIGN #FUND-TXT := 'UNITS       '
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 43T IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 65T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ ) 88T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                    TabSetting(43),iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(65),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), 
                    new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"),new TabSetting(88),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 2, PAGE 4
        //*  CREF ANNUAL FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue("T/L ANNUAL TOTALS");                                                                                            //Natural: MOVE 'T/L ANNUAL TOTALS' TO #REPORT-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L  ANNUAL");                                                                                                      //Natural: ASSIGN #FUND-CREF-CON = 'T/L  ANNUAL'
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J := #I + 40
            pnd_Occ_A_Pnd_Occ.setValue(pnd_J);                                                                                                                            //Natural: ASSIGN #OCC := #J
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                                  //Natural: ASSIGN #PARM-FUND-2 := #OCC
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc_Pnd_Parm_Desc_6.equals(" ")))                                                                                                     //Natural: IF #PARM-DESC-6 = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  3/97
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) / 1T #FUND-DESC-LNE 48T IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 70T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ ) 93T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 )
                TabSetting(48),iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(70),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9+"),new TabSetting(93),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'UNITS       '
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 43T IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 65T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ ) 88T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                TabSetting(43),iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(65),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"),new TabSetting(88),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("DOLLAR AMT  ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'DOLLAR AMT  '
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 44T IAA-CNTRL-RCRD-MONTH.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99 ) 66T IAAA090F.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99+ ) 89T IAA-CNTRL-RCRD-1.CNTRL-AMT ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.99 )
                TabSetting(44),iaa_Cntrl_Rcrd_Month_Cntrl_Amt.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pdaIaaa090f.getIaaa090f_Cntrl_Amt().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99+"),new TabSetting(89),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************
        //*  START OF REPORT 2, PAGE 5                                  /* LB 10/97
        //*  T/L  MONTHLY FUNDS
        //* ****************************
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Reprot_Headings_Pnd_Report_Heading2.setValue("T/L MONTHLY TOTALS");                                                                                           //Natural: MOVE 'T/L MONTHLY TOTALS' TO #REPORT-HEADING2
        pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con.setValue("T/L  MONTHLY");                                                                                                     //Natural: ASSIGN #FUND-CREF-CON = 'T/L  MONTHLY'
        FOR12:                                                                                                                                                            //Natural: FOR #I = 1 TO #PROD-COUNTER-PA
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Prod_Counter_Pa)); pnd_I.nadd(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(40));                                                                                            //Natural: ASSIGN #J := #I + 40
            pnd_Occ_A_Pnd_Occ.setValue(pnd_J);                                                                                                                            //Natural: ASSIGN #OCC := #J
            pnd_Parm_Desc.reset();                                                                                                                                        //Natural: RESET #PARM-DESC
            pnd_Parm_Fund_2.setValue(pnd_Occ_A_Pnd_Occ);                                                                                                                  //Natural: ASSIGN #PARM-FUND-2 := #OCC
            pnd_Parm_Len.setValue(6);                                                                                                                                     //Natural: ASSIGN #PARM-LEN := 6
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), pnd_Parm_Fund_2, pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                                     //Natural: CALLNAT 'IAAN051A' #PARM-FUND-2 #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Parm_Desc_Pnd_Parm_Desc_6.equals(" ")))                                                                                                     //Natural: IF #PARM-DESC-6 = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Desc_Lne_Pnd_Fund_Desc.setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                      //Natural: ASSIGN #FUND-DESC := #PARM-DESC-6
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("FUND RECORDS");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'FUND RECORDS'
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(60));                                                                                            //Natural: ASSIGN #J := #I + 60
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) / 1T #FUND-DESC-LNE 48T IAA-CNTRL-RCRD-MONTH.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 ) 70T IAAA090F.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9+ ) 93T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #J ) ( EM = ZZZ,ZZZ,ZZ9 )
                TabSetting(48),iaa_Cntrl_Rcrd_Month_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(70),pdaIaaa090f.getIaaa090f_Cntrl_Fund_Payees().getValue(pnd_J), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9+"),new TabSetting(93),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_J), new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Fund_Desc_Lne_Pnd_Fund_Txt.setValue("UNITS       ");                                                                                                      //Natural: ASSIGN #FUND-TXT := 'UNITS       '
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Fund_Desc_Lne_Pnd_Fund_Cref_Con,pnd_Fund_Desc_Lne_Pnd_Fund_Desc,pnd_Fund_Desc_Lne_Pnd_Fund_Fill,pnd_Fund_Desc_Lne_Pnd_Fund_Txt,new  //Natural: WRITE ( 2 ) 1T #FUND-DESC-LNE 43T IAA-CNTRL-RCRD-MONTH.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 ) 65T IAAA090F.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999+ ) 88T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #J ) ( EM = ZZZZ,ZZZ,ZZ9.999 )
                TabSetting(43),iaa_Cntrl_Rcrd_Month_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"),new TabSetting(65),pdaIaaa090f.getIaaa090f_Cntrl_Units().getValue(pnd_J), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999+"),new TabSetting(88),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_J), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PRINT-DAILY-REPORT-2
    }
    private void sub_Call_Iaan0500_Read_Extern_File() throws Exception                                                                                                    //Natural: CALL-IAAN0500-READ-EXTERN-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  READS EXTERNALIZATION FILE FOR VALID PRODUCT CODES
        //*  UNCOMMENT FOR OIA CHANGE 5/02
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Prod_Counter_Pa.setValue(pdaIaaa051z.getIaaa051z_Pnd_Pa_Sel_Cnt());                                                                                           //Natural: ASSIGN #PROD-COUNTER-PA := #PA-SEL-CNT
        pnd_Max_Prd_Cde.setValue(pdaIaaa051z.getIaaa051z_Pnd_Cref_Cnt());                                                                                                 //Natural: ASSIGN #MAX-PRD-CDE := #CREF-CNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Today_D.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Today_Pnd_Today_Mm, "/", pnd_Today_Pnd_Today_Dd, "/", pnd_Today_Pnd_Today_Yyyy)); //Natural: COMPRESS #TODAY-MM '/' #TODAY-DD '/' #TODAY-YYYY INTO #TODAY-D LEAVING NO
                    getReports().skip(1, 3);                                                                                                                              //Natural: SKIP ( 1 ) 3
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM ",Global.getPROGRAM(),new TabSetting(35),pnd_Reprot_Headings_Pnd_Report1_Heading1,new  //Natural: WRITE ( 1 ) NOTITLE 1T 'PROGRAM ' *PROGRAM 35T #REPORT1-HEADING1 120T 'Page' *PAGE-NUMBER ( 1 ) ( AD = L )
                        TabSetting(120),"Page",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),       //Natural: WRITE ( 1 ) 1T 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 35T #REPORT-HEADING2
                        pnd_Reprot_Headings_Pnd_Report_Heading2);
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(46),"DAILY",new ColumnSpacing(15),"IA ADMIN",NEWLINE,"CONTROL FIELDS",new                //Natural: WRITE ( 1 ) 46X 'DAILY' 15X 'IA ADMIN' / 'CONTROL FIELDS' 27X 'CONTROL RECORD' 8X 'MASTER FILE(S)' 12X 'DIFFERENCES' / '------------------------------------' 5X '--------------' 8X '--------------' 12X '-----------' / 'Date                                        ' #TODAY-D 12X #TODAY-D 13X #TODAY-D
                        ColumnSpacing(27),"CONTROL RECORD",new ColumnSpacing(8),"MASTER FILE(S)",new ColumnSpacing(12),"DIFFERENCES",NEWLINE,"------------------------------------",new 
                        ColumnSpacing(5),"--------------",new ColumnSpacing(8),"--------------",new ColumnSpacing(12),"-----------",NEWLINE,"Date                                        ",pnd_Today_D,new 
                        ColumnSpacing(12),pnd_Today_D,new ColumnSpacing(13),pnd_Today_D);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Begin_Month_D.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Begin_Month_Pnd_Begin_Mm, "/", pnd_Begin_Month_Pnd_Begin_Dd,           //Natural: COMPRESS #BEGIN-MM '/' #BEGIN-DD '/' #BEGIN-YYYY INTO #BEGIN-MONTH-D LEAVING NO
                        "/", pnd_Begin_Month_Pnd_Begin_Yyyy));
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE);                                                                                  //Natural: WRITE ( 2 ) ///
                    //*  -2
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"PROGRAM ",Global.getPROGRAM(),new TabSetting(35),pnd_Reprot_Headings_Pnd_Report2_Heading1,new  //Natural: WRITE ( 2 ) NOTITLE 1T 'PROGRAM ' *PROGRAM 35T #REPORT2-HEADING1 120T 'Page' *PAGE-NUMBER ( 2 ) ( AD = L )
                        TabSetting(120),"Page",getReports().getPageNumberDbs(2), new FieldAttributes ("AD=L"));
                    getReports().write(2, ReportOption.NOTITLE,"DATE    ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),pnd_Reprot_Headings_Pnd_Report_Heading2); //Natural: WRITE ( 2 ) 'DATE    ' *DATX ( EM = MM/DD/YYYY ) 35T #REPORT-HEADING2
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 2 ) 2
                    getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(48),"BEGINNING",NEWLINE,"CONTROL FIELDS",new ColumnSpacing(35),"OF MONTH",new            //Natural: WRITE ( 2 ) 48X 'BEGINNING' / 'CONTROL FIELDS' 35X 'OF MONTH' 12X 'NET CHANGES' 12X 'NEW TOTALS' / '----------------------------------' 10X '--------------' 8X '--------------' 12X '-----------' / 'DATE                                           ' #BEGIN-MONTH-D 12X *DATX ( EM = MM/DD/YYYY ) 13X *DATX ( EM = MM/DD/YYYY )
                        ColumnSpacing(12),"NET CHANGES",new ColumnSpacing(12),"NEW TOTALS",NEWLINE,"----------------------------------",new ColumnSpacing(10),"--------------",new 
                        ColumnSpacing(8),"--------------",new ColumnSpacing(12),"-----------",NEWLINE,"DATE                                           ",pnd_Begin_Month_D,new 
                        ColumnSpacing(12),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(13),Global.getDATX(), new ReportEditMask 
                        ("MM/DD/YYYY"));
                    //*  REPORT 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"********************************",NEWLINE);                                                                             //Natural: WRITE NOHDR '********************************' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=132");
        Global.format(1, "PS=55 LS=132");
        Global.format(2, "PS=55 LS=132");

        getReports().setDisplayColumns(3, pnd_Input_Pnd_Ppcn_Nbr,pnd_Input_Pnd_Payee_Cde,pnd_Input_Pnd_Ddctn_Cde,pnd_Input_Pnd_Ddctn_Per_Amt);
    }
}
