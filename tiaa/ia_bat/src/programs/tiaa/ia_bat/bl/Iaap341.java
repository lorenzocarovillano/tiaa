/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:25:02 PM
**        * FROM NATURAL PROGRAM : Iaap341
************************************************************
**        * FILE NAME            : Iaap341.java
**        * CLASS NAME           : Iaap341
**        * INSTANCE NAME        : Iaap341
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP341    READS IA EXTRACT FLAT FILE             *
*      DATE     -  3/01       & GENERATES AUTO TRANS 006(TERMINATION)*
*                             700(IVC TAX) & 900(DEDUCTION)          *
*                             FOR TPA,IPRO, & P/I                    *
*  MODIFICATION DATE                                                 *
*                                                                    *
* 04/29/02  TD   RESTOWED FOR OIA CHANGES MADE IN IAAN0511           *
*                                                                    *
*   6/02       ADDED LOGIC TO HANDLE NOT TERMINATING PAYEE GT 02     *
*              WITH  PEND CODE 'A' OR 'C'                            *
*              GENERATE 102 TRANS WITH  PEND CODE 'R'                *
*              FOR P/I PAYEE 01 WITH PEND A OR C GENERATE (R) PEND   *
*              DO SCAN ON 6/02                                       *
*   3/06       CHANGE REFERENCE TO #W-PAGE-CNTR WITH *PAGE-NUMBER(1) *
*              DO SCAN ON 3/06
*   5/06       SUSPEND AUTO PEND OF IPRO CONTRACTS WHEN THE FINAL PER*
*              PAY DTE IS REACHED. DO SCAN ON 5/06
*   4/08       ADDED CODE FOR ROTH -- DO NOT UPDATE IVC USED
*              DO SCAN 4/08
*   3/12       RATE BASE EXPANSION - CHANGES TO ACTUARIAL PDA
*              DO SCAN 3/12
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap341 extends BLNatBase
{
    // Data Areas
    private LdaIaal140 ldaIaal140;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm_Fund_1;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Rtn_Cde;

    private DbsGroup ia_Aian026_Linkage;
    private DbsField ia_Aian026_Linkage_Ia_Call_Type;
    private DbsField ia_Aian026_Linkage_Ia_Fund_Code;
    private DbsField ia_Aian026_Linkage_Ia_Reval_Methd;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_1;
    private DbsField ia_Aian026_Linkage_Ia_Check_Date_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Check_Dd;

    private DbsGroup ia_Aian026_Linkage__R_Field_2;
    private DbsField ia_Aian026_Linkage_Ia_Check_Year;
    private DbsField ia_Aian026_Linkage_Ia_Check_Month;
    private DbsField ia_Aian026_Linkage_Ia_Check_Day;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date;

    private DbsGroup ia_Aian026_Linkage__R_Field_3;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Ccyymm;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Day;

    private DbsGroup ia_Aian026_Linkage__R_Field_4;
    private DbsField ia_Aian026_Linkage_Ia_Issue_Date_A;
    private DbsField ia_Aian026_Linkage_Return_Code_Pgm;

    private DbsGroup ia_Aian026_Linkage__R_Field_5;
    private DbsField ia_Aian026_Linkage_Return_Pgm;
    private DbsField ia_Aian026_Linkage_Return_Code;
    private DbsField ia_Aian026_Linkage_Auv_Returned;
    private DbsField ia_Aian026_Linkage_Auv_Date_Return;
    private DbsField ia_Aian026_Linkage_Days_In_Request_Month;
    private DbsField ia_Aian026_Linkage_Days_In_Particip_Month;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_6;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd;

    private DbsGroup pnd_Curr_Check_Date_Ccyymmdd__R_Field_7;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm;
    private DbsField pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2;
    private DbsField pnd_Next_Check_Date_Ccyymmdd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_8;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd;

    private DbsGroup pnd_Next_Check_Date_Ccyymmdd__R_Field_9;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm;
    private DbsField pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2;
    private DbsField pnd_W_Date_Out;
    private DbsField pnd_W_Page_Ctr;

    private DbsGroup pnd_Tran_Rec_Out;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_10;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Payee;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_11;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_2;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Filler1;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Xref;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Amt;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Int_Code;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_12;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_13;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_14;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Ccyy;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm;

    private DbsGroup pnd_Tran_Rec_Out__R_Field_15;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Pend_Dte;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Fill;
    private DbsField pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt;
    private DbsField pnd_Work_Cpr_Ivc;

    private DbsGroup pnd_Work_Cpr_Ivc__R_Field_16;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Amt;
    private DbsField pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Percent;
    private DbsField pnd_Save_Payee_Key;

    private DbsGroup pnd_Save_Payee_Key__R_Field_17;
    private DbsField pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Payee_Key_Pnd_Save_Payee_Cde;
    private DbsField pnd_Sve_Pend_Cde;
    private DbsField pnd_Sve_Opt;
    private DbsField pnd_Sve_Filler_1;

    private DbsGroup pnd_Sve_Filler_1__R_Field_18;
    private DbsField pnd_Sve_Filler_1_Pnd_Sve_Orgn;
    private DbsField pnd_Sve_Mode;

    private DbsGroup pnd_Sve_Mode__R_Field_19;
    private DbsField pnd_Sve_Mode_Pnd_Sve_Mode_Pos1;
    private DbsField pnd_Sve_Mode_Pnd_Sve_Mode_Pos2;
    private DbsField pnd_Sve_Issu_Dte;
    private DbsField pnd_Sve_Ck_Dte;

    private DbsGroup pnd_Sve_Ck_Dte__R_Field_20;
    private DbsField pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Ccyymm;
    private DbsField pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Mm;
    private DbsField pnd_Ws_Cntrct_Final_Pay_Dte;

    private DbsGroup pnd_Ws_Cntrct_Final_Pay_Dte__R_Field_21;
    private DbsField pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyymm;
    private DbsField pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyydd;
    private DbsField pnd_Ws_Cntrct_Pend_Dte;

    private DbsGroup pnd_Ws_Cntrct_Pend_Dte__R_Field_22;
    private DbsField pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Ccyy;
    private DbsField pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Dd;
    private DbsField pnd_Ws_Wrk_Field2;

    private DbsGroup pnd_Ws_Wrk_Field2__R_Field_23;
    private DbsField pnd_Ws_Wrk_Field2_Pnd_Fillera;
    private DbsField pnd_Ws_Wrk_Field2_Pnd_Cntrct_Csh_Cde;
    private DbsField pnd_Ws_Wrk_Field2_Pnd_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Sve_Crrncy_Cde;
    private DbsField pnd_Sve_Xref;
    private DbsField pnd_Bypass_Cntrct;
    private DbsField pnd_Wrte_700_Sw;
    private DbsField pnd_Gen_700_Trn_Sw;
    private DbsField pnd_Gen_006_Trn_Sw;
    private DbsField pnd_Gen_102_Trn_Sw;
    private DbsField pnd_Tot_Trans_Out;
    private DbsField pnd_Tot_Tran_006;
    private DbsField pnd_Tot_Tran_102;
    private DbsField pnd_Tot_Tran_700;
    private DbsField pnd_Tot_Tran_900;
    private DbsField pnd_Tot_Per_Pmt;
    private DbsField pnd_W_Percnt;
    private DbsField pnd_I;
    private DbsField pnd_Fst_Tme;
    private DbsField pnd_Fst_Tme_Ded;

    private DbsGroup pnd_Prog_Misc_Area;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Int_Code;
    private DbsField pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read;
    private DbsField pnd_Prog_Misc_Area_Pnd_Cpr_Fnd;
    private DbsField pnd_Prog_Misc_Area_Pnd_Sve_Isn;
    private DbsField pnd_Prog_Misc_Area_Pnd_Total_Trans;
    private DbsField pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt;
    private DbsField pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading2;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Heading3;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_102;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl;
    private DbsField pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal140 = new LdaIaal140();
        registerRecord(ldaIaal140);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm_Fund_1 = localVariables.newFieldInRecord("pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);
        pnd_Parm_Fund_2 = localVariables.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Rtn_Cde = localVariables.newFieldInRecord("pnd_Parm_Rtn_Cde", "#PARM-RTN-CDE", FieldType.NUMERIC, 2);

        ia_Aian026_Linkage = localVariables.newGroupInRecord("ia_Aian026_Linkage", "IA-AIAN026-LINKAGE");
        ia_Aian026_Linkage_Ia_Call_Type = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Call_Type", "IA-CALL-TYPE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Fund_Code = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Fund_Code", "IA-FUND-CODE", FieldType.STRING, 1);
        ia_Aian026_Linkage_Ia_Reval_Methd = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Reval_Methd", "IA-REVAL-METHD", FieldType.STRING, 
            1);
        ia_Aian026_Linkage_Ia_Check_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date", "IA-CHECK-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_1 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_1", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Date_Ccyymm = ia_Aian026_Linkage__R_Field_1.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Date_Ccyymm", "IA-CHECK-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        ia_Aian026_Linkage_Ia_Check_Dd = ia_Aian026_Linkage__R_Field_1.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Dd", "IA-CHECK-DD", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_2 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_2", "REDEFINE", ia_Aian026_Linkage_Ia_Check_Date);
        ia_Aian026_Linkage_Ia_Check_Year = ia_Aian026_Linkage__R_Field_2.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Year", "IA-CHECK-YEAR", FieldType.NUMERIC, 
            4);
        ia_Aian026_Linkage_Ia_Check_Month = ia_Aian026_Linkage__R_Field_2.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Month", "IA-CHECK-MONTH", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Check_Day = ia_Aian026_Linkage__R_Field_2.newFieldInGroup("ia_Aian026_Linkage_Ia_Check_Day", "IA-CHECK-DAY", FieldType.NUMERIC, 
            2);
        ia_Aian026_Linkage_Ia_Issue_Date = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date", "IA-ISSUE-DATE", FieldType.NUMERIC, 
            8);

        ia_Aian026_Linkage__R_Field_3 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_3", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Ccyymm = ia_Aian026_Linkage__R_Field_3.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Ccyymm", "IA-ISSUE-CCYYMM", FieldType.NUMERIC, 
            6);
        ia_Aian026_Linkage_Ia_Issue_Day = ia_Aian026_Linkage__R_Field_3.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Day", "IA-ISSUE-DAY", FieldType.NUMERIC, 
            2);

        ia_Aian026_Linkage__R_Field_4 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_4", "REDEFINE", ia_Aian026_Linkage_Ia_Issue_Date);
        ia_Aian026_Linkage_Ia_Issue_Date_A = ia_Aian026_Linkage__R_Field_4.newFieldInGroup("ia_Aian026_Linkage_Ia_Issue_Date_A", "IA-ISSUE-DATE-A", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code_Pgm = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Return_Code_Pgm", "RETURN-CODE-PGM", FieldType.STRING, 
            11);

        ia_Aian026_Linkage__R_Field_5 = ia_Aian026_Linkage.newGroupInGroup("ia_Aian026_Linkage__R_Field_5", "REDEFINE", ia_Aian026_Linkage_Return_Code_Pgm);
        ia_Aian026_Linkage_Return_Pgm = ia_Aian026_Linkage__R_Field_5.newFieldInGroup("ia_Aian026_Linkage_Return_Pgm", "RETURN-PGM", FieldType.STRING, 
            8);
        ia_Aian026_Linkage_Return_Code = ia_Aian026_Linkage__R_Field_5.newFieldInGroup("ia_Aian026_Linkage_Return_Code", "RETURN-CODE", FieldType.NUMERIC, 
            3);
        ia_Aian026_Linkage_Auv_Returned = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Returned", "AUV-RETURNED", FieldType.NUMERIC, 8, 
            4);
        ia_Aian026_Linkage_Auv_Date_Return = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Auv_Date_Return", "AUV-DATE-RETURN", FieldType.NUMERIC, 
            8);
        ia_Aian026_Linkage_Days_In_Request_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Days_In_Request_Month", "DAYS-IN-REQUEST-MONTH", 
            FieldType.NUMERIC, 2);
        ia_Aian026_Linkage_Days_In_Particip_Month = ia_Aian026_Linkage.newFieldInGroup("ia_Aian026_Linkage_Days_In_Particip_Month", "DAYS-IN-PARTICIP-MONTH", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Curr_Check_Date_Ccyymmdd", "#CURR-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_6", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy = pnd_Curr_Check_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Ccyy", 
            "#CURR-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm", 
            "#CURR-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd = pnd_Curr_Check_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Dd", 
            "#CURR-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Curr_Check_Date_Ccyymmdd__R_Field_7 = localVariables.newGroupInRecord("pnd_Curr_Check_Date_Ccyymmdd__R_Field_7", "REDEFINE", pnd_Curr_Check_Date_Ccyymmdd);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm = pnd_Curr_Check_Date_Ccyymmdd__R_Field_7.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm", 
            "#CURR-CHECK-DATE-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2 = pnd_Curr_Check_Date_Ccyymmdd__R_Field_7.newFieldInGroup("pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Dd2", 
            "#CURR-CHECK-DATE-DD2", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Next_Check_Date_Ccyymmdd", "#NEXT-CHECK-DATE-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_8 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_8", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy = pnd_Next_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy", 
            "#NEXT-CHECK-CCYY", FieldType.NUMERIC, 4);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm = pnd_Next_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm", 
            "#NEXT-CHECK-MM", FieldType.NUMERIC, 2);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd = pnd_Next_Check_Date_Ccyymmdd__R_Field_8.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd", 
            "#NEXT-CHECK-DD", FieldType.NUMERIC, 2);

        pnd_Next_Check_Date_Ccyymmdd__R_Field_9 = localVariables.newGroupInRecord("pnd_Next_Check_Date_Ccyymmdd__R_Field_9", "REDEFINE", pnd_Next_Check_Date_Ccyymmdd);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm = pnd_Next_Check_Date_Ccyymmdd__R_Field_9.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyymm", 
            "#NEXT-CHECK-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2 = pnd_Next_Check_Date_Ccyymmdd__R_Field_9.newFieldInGroup("pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Dd2", 
            "#NEXT-CHECK-DD2", FieldType.NUMERIC, 2);
        pnd_W_Date_Out = localVariables.newFieldInRecord("pnd_W_Date_Out", "#W-DATE-OUT", FieldType.STRING, 8);
        pnd_W_Page_Ctr = localVariables.newFieldInRecord("pnd_W_Page_Ctr", "#W-PAGE-CTR", FieldType.PACKED_DECIMAL, 7);

        pnd_Tran_Rec_Out = localVariables.newGroupInRecord("pnd_Tran_Rec_Out", "#TRAN-REC-OUT");
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code", "#TR-TRAN-CODE", FieldType.NUMERIC, 
            3);
        pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee", "#TR-PPCN-NBR-PAYEE", FieldType.STRING, 
            12);

        pnd_Tran_Rec_Out__R_Field_10 = pnd_Tran_Rec_Out.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_10", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee);
        pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr = pnd_Tran_Rec_Out__R_Field_10.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr", "#TR-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Tran_Rec_Out_Pnd_Tr_Payee = pnd_Tran_Rec_Out__R_Field_10.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Payee", "#TR-PAYEE", FieldType.STRING, 2);

        pnd_Tran_Rec_Out__R_Field_11 = pnd_Tran_Rec_Out.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_11", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee);
        pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8 = pnd_Tran_Rec_Out__R_Field_11.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8", "#TR-PPCN-NBR-8", FieldType.STRING, 
            8);
        pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_2 = pnd_Tran_Rec_Out__R_Field_11.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_2", "#TR-PPCN-NBR-2", FieldType.STRING, 
            2);
        pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde = pnd_Tran_Rec_Out__R_Field_11.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde", "#TR-PAYEE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed", "#TR-CHK-DTE-PROCESSED", 
            FieldType.NUMERIC, 8);
        pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due", "#TR-CHK-DTE-DUE", FieldType.NUMERIC, 
            8);
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde", "#TR-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde", "#TR-FUND-CDE", FieldType.STRING, 2);
        pnd_Tran_Rec_Out_Pnd_Tr_Filler1 = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Filler1", "#TR-FILLER1", FieldType.STRING, 1);
        pnd_Tran_Rec_Out_Pnd_Tr_Xref = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Xref", "#TR-XREF", FieldType.STRING, 9);
        pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde = pnd_Tran_Rec_Out.newFieldArrayInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde", "#TR-CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type = pnd_Tran_Rec_Out.newFieldArrayInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type", "#TR-RCVRY-TYPE", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free = pnd_Tran_Rec_Out.newFieldArrayInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free", "#TR-PER-TAX-FREE", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Amt = pnd_Tran_Rec_Out.newFieldArrayInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Amt", "#TR-PER-TAX-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units", "#TR-TOT-UNITS", FieldType.NUMERIC, 
            13, 3);
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt", "#TR-TOT-PER-PMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div", "#TR-TOT-PER-DIV", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt", "#TR-TOT-FIN-PMT", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div", "#TR-TOT-FIN-DIV", FieldType.NUMERIC, 
            9, 2);
        pnd_Tran_Rec_Out_Pnd_Tr_Int_Code = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Int_Code", "#TR-INT-CODE", FieldType.NUMERIC, 1);

        pnd_Tran_Rec_Out__R_Field_12 = pnd_Tran_Rec_Out.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_12", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Int_Code);
        pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde = pnd_Tran_Rec_Out__R_Field_12.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde", "#TR-PEND-CDE", FieldType.STRING, 
            1);
        pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code", "#TR-DED-CODE", FieldType.STRING, 3);
        pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr", "#TR-DED-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt", "#TR-PER-DED-AMT", FieldType.NUMERIC, 
            7, 2);

        pnd_Tran_Rec_Out__R_Field_13 = pnd_Tran_Rec_Out.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_13", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt);
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte = pnd_Tran_Rec_Out__R_Field_13.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte", "#TR-FIN-PER-DTE", FieldType.NUMERIC, 
            6);

        pnd_Tran_Rec_Out__R_Field_14 = pnd_Tran_Rec_Out__R_Field_13.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_14", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte);
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Ccyy = pnd_Tran_Rec_Out__R_Field_14.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Ccyy", "#TR-FIN-PER-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm = pnd_Tran_Rec_Out__R_Field_14.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm", "#TR-FIN-PER-MM", FieldType.NUMERIC, 
            2);

        pnd_Tran_Rec_Out__R_Field_15 = pnd_Tran_Rec_Out__R_Field_13.newGroupInGroup("pnd_Tran_Rec_Out__R_Field_15", "REDEFINE", pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte);
        pnd_Tran_Rec_Out_Pnd_Tr_Pend_Dte = pnd_Tran_Rec_Out__R_Field_15.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Pend_Dte", "#TR-PEND-DTE", FieldType.NUMERIC, 
            6);
        pnd_Tran_Rec_Out_Pnd_Tr_Fill = pnd_Tran_Rec_Out__R_Field_13.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Fill", "#TR-FILL", FieldType.NUMERIC, 1);
        pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt = pnd_Tran_Rec_Out.newFieldInGroup("pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt", "#TR-OLD-DED-AMT", FieldType.NUMERIC, 
            7, 2);
        pnd_Work_Cpr_Ivc = localVariables.newFieldInRecord("pnd_Work_Cpr_Ivc", "#WORK-CPR-IVC", FieldType.STRING, 155);

        pnd_Work_Cpr_Ivc__R_Field_16 = localVariables.newGroupInRecord("pnd_Work_Cpr_Ivc__R_Field_16", "REDEFINE", pnd_Work_Cpr_Ivc);
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd", "#W-CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind", 
            "#W-CNTRCT-RCVRY-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Per_Ivc_Amt = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Per_Ivc_Amt", "#W-CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Resdl_Ivc_Amt = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Resdl_Ivc_Amt", 
            "#W-CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt", "#W-CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt", "#W-CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Amt = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Amt", "#W-CNTRCT-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Percent = pnd_Work_Cpr_Ivc__R_Field_16.newFieldArrayInGroup("pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rtb_Percent", "#W-CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5));
        pnd_Save_Payee_Key = localVariables.newFieldInRecord("pnd_Save_Payee_Key", "#SAVE-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Save_Payee_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Save_Payee_Key__R_Field_17", "REDEFINE", pnd_Save_Payee_Key);
        pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr = pnd_Save_Payee_Key__R_Field_17.newFieldInGroup("pnd_Save_Payee_Key_Pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Save_Payee_Key_Pnd_Save_Payee_Cde = pnd_Save_Payee_Key__R_Field_17.newFieldInGroup("pnd_Save_Payee_Key_Pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Sve_Pend_Cde = localVariables.newFieldInRecord("pnd_Sve_Pend_Cde", "#SVE-PEND-CDE", FieldType.STRING, 1);
        pnd_Sve_Opt = localVariables.newFieldInRecord("pnd_Sve_Opt", "#SVE-OPT", FieldType.NUMERIC, 3);
        pnd_Sve_Filler_1 = localVariables.newFieldInRecord("pnd_Sve_Filler_1", "#SVE-FILLER-1", FieldType.STRING, 4);

        pnd_Sve_Filler_1__R_Field_18 = localVariables.newGroupInRecord("pnd_Sve_Filler_1__R_Field_18", "REDEFINE", pnd_Sve_Filler_1);
        pnd_Sve_Filler_1_Pnd_Sve_Orgn = pnd_Sve_Filler_1__R_Field_18.newFieldInGroup("pnd_Sve_Filler_1_Pnd_Sve_Orgn", "#SVE-ORGN", FieldType.NUMERIC, 
            2);
        pnd_Sve_Mode = localVariables.newFieldInRecord("pnd_Sve_Mode", "#SVE-MODE", FieldType.NUMERIC, 3);

        pnd_Sve_Mode__R_Field_19 = localVariables.newGroupInRecord("pnd_Sve_Mode__R_Field_19", "REDEFINE", pnd_Sve_Mode);
        pnd_Sve_Mode_Pnd_Sve_Mode_Pos1 = pnd_Sve_Mode__R_Field_19.newFieldInGroup("pnd_Sve_Mode_Pnd_Sve_Mode_Pos1", "#SVE-MODE-POS1", FieldType.NUMERIC, 
            1);
        pnd_Sve_Mode_Pnd_Sve_Mode_Pos2 = pnd_Sve_Mode__R_Field_19.newFieldInGroup("pnd_Sve_Mode_Pnd_Sve_Mode_Pos2", "#SVE-MODE-POS2", FieldType.NUMERIC, 
            2);
        pnd_Sve_Issu_Dte = localVariables.newFieldInRecord("pnd_Sve_Issu_Dte", "#SVE-ISSU-DTE", FieldType.NUMERIC, 6);
        pnd_Sve_Ck_Dte = localVariables.newFieldInRecord("pnd_Sve_Ck_Dte", "#SVE-CK-DTE", FieldType.NUMERIC, 8);

        pnd_Sve_Ck_Dte__R_Field_20 = localVariables.newGroupInRecord("pnd_Sve_Ck_Dte__R_Field_20", "REDEFINE", pnd_Sve_Ck_Dte);
        pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Ccyymm = pnd_Sve_Ck_Dte__R_Field_20.newFieldInGroup("pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Ccyymm", "#SVE-CK-DTE-CCYYMM", 
            FieldType.NUMERIC, 4);
        pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Mm = pnd_Sve_Ck_Dte__R_Field_20.newFieldInGroup("pnd_Sve_Ck_Dte_Pnd_Sve_Ck_Dte_Mm", "#SVE-CK-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Ws_Cntrct_Final_Pay_Dte = localVariables.newFieldInRecord("pnd_Ws_Cntrct_Final_Pay_Dte", "#WS-CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8);

        pnd_Ws_Cntrct_Final_Pay_Dte__R_Field_21 = localVariables.newGroupInRecord("pnd_Ws_Cntrct_Final_Pay_Dte__R_Field_21", "REDEFINE", pnd_Ws_Cntrct_Final_Pay_Dte);
        pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyymm = pnd_Ws_Cntrct_Final_Pay_Dte__R_Field_21.newFieldInGroup("pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyymm", 
            "#WS-CNTRCT-FINAL-PAY-CCYYMM", FieldType.NUMERIC, 6);
        pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyydd = pnd_Ws_Cntrct_Final_Pay_Dte__R_Field_21.newFieldInGroup("pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyydd", 
            "#WS-CNTRCT-FINAL-PAY-CCYYDD", FieldType.NUMERIC, 2);
        pnd_Ws_Cntrct_Pend_Dte = localVariables.newFieldInRecord("pnd_Ws_Cntrct_Pend_Dte", "#WS-CNTRCT-PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Ws_Cntrct_Pend_Dte__R_Field_22 = localVariables.newGroupInRecord("pnd_Ws_Cntrct_Pend_Dte__R_Field_22", "REDEFINE", pnd_Ws_Cntrct_Pend_Dte);
        pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Ccyy = pnd_Ws_Cntrct_Pend_Dte__R_Field_22.newFieldInGroup("pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Ccyy", 
            "#WS-CNTRCT-PEND-CCYY", FieldType.NUMERIC, 4);
        pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Dd = pnd_Ws_Cntrct_Pend_Dte__R_Field_22.newFieldInGroup("pnd_Ws_Cntrct_Pend_Dte_Pnd_Ws_Cntrct_Pend_Dd", 
            "#WS-CNTRCT-PEND-DD", FieldType.NUMERIC, 2);
        pnd_Ws_Wrk_Field2 = localVariables.newFieldInRecord("pnd_Ws_Wrk_Field2", "#WS-WRK-FIELD2", FieldType.STRING, 5);

        pnd_Ws_Wrk_Field2__R_Field_23 = localVariables.newGroupInRecord("pnd_Ws_Wrk_Field2__R_Field_23", "REDEFINE", pnd_Ws_Wrk_Field2);
        pnd_Ws_Wrk_Field2_Pnd_Fillera = pnd_Ws_Wrk_Field2__R_Field_23.newFieldInGroup("pnd_Ws_Wrk_Field2_Pnd_Fillera", "#FILLERA", FieldType.STRING, 3);
        pnd_Ws_Wrk_Field2_Pnd_Cntrct_Csh_Cde = pnd_Ws_Wrk_Field2__R_Field_23.newFieldInGroup("pnd_Ws_Wrk_Field2_Pnd_Cntrct_Csh_Cde", "#CNTRCT-CSH-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Wrk_Field2_Pnd_Cntrct_Emplymnt_Trmnt_Cde = pnd_Ws_Wrk_Field2__R_Field_23.newFieldInGroup("pnd_Ws_Wrk_Field2_Pnd_Cntrct_Emplymnt_Trmnt_Cde", 
            "#CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1);
        pnd_Sve_Crrncy_Cde = localVariables.newFieldInRecord("pnd_Sve_Crrncy_Cde", "#SVE-CRRNCY-CDE", FieldType.NUMERIC, 1);
        pnd_Sve_Xref = localVariables.newFieldInRecord("pnd_Sve_Xref", "#SVE-XREF", FieldType.STRING, 9);
        pnd_Bypass_Cntrct = localVariables.newFieldInRecord("pnd_Bypass_Cntrct", "#BYPASS-CNTRCT", FieldType.STRING, 1);
        pnd_Wrte_700_Sw = localVariables.newFieldInRecord("pnd_Wrte_700_Sw", "#WRTE-700-SW", FieldType.STRING, 1);
        pnd_Gen_700_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_700_Trn_Sw", "#GEN-700-TRN-SW", FieldType.STRING, 1);
        pnd_Gen_006_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_006_Trn_Sw", "#GEN-006-TRN-SW", FieldType.STRING, 1);
        pnd_Gen_102_Trn_Sw = localVariables.newFieldInRecord("pnd_Gen_102_Trn_Sw", "#GEN-102-TRN-SW", FieldType.STRING, 1);
        pnd_Tot_Trans_Out = localVariables.newFieldInRecord("pnd_Tot_Trans_Out", "#TOT-TRANS-OUT", FieldType.NUMERIC, 6);
        pnd_Tot_Tran_006 = localVariables.newFieldInRecord("pnd_Tot_Tran_006", "#TOT-TRAN-006", FieldType.NUMERIC, 6);
        pnd_Tot_Tran_102 = localVariables.newFieldInRecord("pnd_Tot_Tran_102", "#TOT-TRAN-102", FieldType.NUMERIC, 6);
        pnd_Tot_Tran_700 = localVariables.newFieldInRecord("pnd_Tot_Tran_700", "#TOT-TRAN-700", FieldType.NUMERIC, 6);
        pnd_Tot_Tran_900 = localVariables.newFieldInRecord("pnd_Tot_Tran_900", "#TOT-TRAN-900", FieldType.NUMERIC, 6);
        pnd_Tot_Per_Pmt = localVariables.newFieldInRecord("pnd_Tot_Per_Pmt", "#TOT-PER-PMT", FieldType.NUMERIC, 11, 2);
        pnd_W_Percnt = localVariables.newFieldInRecord("pnd_W_Percnt", "#W-PERCNT", FieldType.NUMERIC, 3, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Fst_Tme = localVariables.newFieldInRecord("pnd_Fst_Tme", "#FST-TME", FieldType.STRING, 1);
        pnd_Fst_Tme_Ded = localVariables.newFieldInRecord("pnd_Fst_Tme_Ded", "#FST-TME-DED", FieldType.STRING, 1);

        pnd_Prog_Misc_Area = localVariables.newGroupInRecord("pnd_Prog_Misc_Area", "#PROG-MISC-AREA");
        pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code", "#SVE-TRAN-CODE", FieldType.NUMERIC, 
            3);
        pnd_Prog_Misc_Area_Pnd_Sve_Int_Code = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Int_Code", "#SVE-INT-CODE", FieldType.NUMERIC, 
            1);
        pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read", "#FST-TRN-READ", FieldType.STRING, 
            1);
        pnd_Prog_Misc_Area_Pnd_Cpr_Fnd = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Cpr_Fnd", "#CPR-FND", FieldType.STRING, 1);
        pnd_Prog_Misc_Area_Pnd_Sve_Isn = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Sve_Isn", "#SVE-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prog_Misc_Area_Pnd_Total_Trans = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Total_Trans", "#TOTAL-TRANS", FieldType.NUMERIC, 
            5);
        pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_W_New_Ded_Amt", "#W-NEW-DED-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc", "#TYPE-TRANS-DESC", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Term", "#DETAIL-HEAD-TERM", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax", "#DETAIL-HEAD-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng", "#DETAIL-HEAD-DED-CHNG", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop", "#DETAIL-HEAD-DED-STOP", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend", "#DETAIL-HEAD-102-PEND", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading", "#DETAIL-HEADING", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading2 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading2", "#DETAIL-HEADING2", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Heading3 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Heading3", "#DETAIL-HEADING3", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term", "#DETAIL-HEAD1-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term", "#DETAIL-HEAD2-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term", "#DETAIL-HEAD3-TERM", 
            FieldType.STRING, 90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_102", "#DETAIL-HEAD1-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_102", "#DETAIL-HEAD2-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_102 = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_102", "#DETAIL-HEAD3-102", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax", "#DETAIL-HEAD1-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax", "#DETAIL-HEAD2-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax", "#DETAIL-HEAD3-TAX", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded", "#DETAIL-HEAD1-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded", "#DETAIL-HEAD2-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded", "#DETAIL-HEAD3-DED", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl", "#DETAIL-HEAD1-CTL", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl", "#DETAIL-HEAD2-CTL", FieldType.STRING, 
            90);
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl = pnd_Prog_Misc_Area.newFieldInGroup("pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl", "#DETAIL-HEAD3-CTL", FieldType.STRING, 
            90);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal140.initializeValues();

        localVariables.reset();
        ia_Aian026_Linkage_Ia_Call_Type.setInitialValue("P");
        ia_Aian026_Linkage_Ia_Reval_Methd.setInitialValue("M");
        pnd_Wrte_700_Sw.setInitialValue("Y");
        pnd_Fst_Tme.setInitialValue("Y");
        pnd_Fst_Tme_Ded.setInitialValue("Y");
        pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setInitialValue(999);
        pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setInitialValue(0);
        pnd_Prog_Misc_Area_Pnd_Fst_Trn_Read.setInitialValue("N");
        pnd_Prog_Misc_Area_Pnd_Cpr_Fnd.setInitialValue("N");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Term.setInitialValue("  TERMINATIONS (TRANS 006)                             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax.setInitialValue("  TAX TRANSACTIONS (TRANS 700)                         ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng.setInitialValue("  DEDUCTIONS FROM NET (TRANS 900) CHANGES              ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop.setInitialValue("  DEDUCTIONS FROM NET (TRANS 900) STOP DEDUCTIONS      ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend.setInitialValue("  SUSPEND-PAYMENT (TRANS 102)                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term.setInitialValue(" CONTRACT PAYEE    CROSS-REF                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term.setInitialValue("  NUMBER   CODE    NUMBER                             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term.setInitialValue("--------- ------   ---------                          ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_102.setInitialValue(" CONTRACT PAYEE    CROSS-REF  PEND      EFFECTIVE            ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_102.setInitialValue("  NUMBER   CODE    NUMBER     CODE      DATE                 ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_102.setInitialValue("--------- ------   ---------  ------    -------              ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax.setInitialValue(" CONTRACT PAYEE    CROSS-REF  RECVRY    PER TAX       PER TAX");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax.setInitialValue("  NUMBER   CODE    NUMBER     TYPE      DEDUCT        FREE   ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax.setInitialValue("--------- ------   ---------  ------    -------       -------");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded.setInitialValue(" CONTRACT PAYEE    CROSS-REF    NEW       SEQ    DED    OLD    ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded.setInitialValue("  NUMBER   CODE    NUMBER       AMT       NO     CODE   AMT    ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded.setInitialValue("--------- ------   ---------    ------    ----   -----  -------");
        pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ctl.setInitialValue("                                         CONTROL RECORD      ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ctl.setInitialValue("                                         CHANGES             ");
        pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ctl.setInitialValue("                                         --------------      ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap341() throws Exception
    {
        super("Iaap341");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP341", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_W_Date_Out.setValue(Global.getDATU());                                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 56;//Natural: ASSIGN #W-DATE-OUT := *DATU
        //*  ---------------------------------------------------------                                                                                                    //Natural: AT TOP OF PAGE ( 1 )
        //*  1) MAIN LOGIC TO GENERATE IAIQ AUTO TRANSACTIONS
        //*       TERMINATIONS(006), TAX(700), & DEDUCTION(900) TRANS
        //*  ---------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal140.getPnd_Input_Record())))
        {
            short decideConditionsMet440 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RECORD-CDE;//Natural: VALUE 00
            if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Record_Cde().equals(0))))
            {
                decideConditionsMet440++;
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   CHEADER")))                                                                 //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER'
                {
                    pnd_Curr_Check_Date_Ccyymmdd.setValue(ldaIaal140.getPnd_Input_Record_Pnd_W_Check_Date());                                                             //Natural: ASSIGN #CURR-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd.setValue(ldaIaal140.getPnd_Input_Record_Pnd_W_Check_Date());                                                             //Natural: ASSIGN #NEXT-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd.setValue(ldaIaal140.getPnd_Input_Record_Pnd_W_Check_Date());                                                             //Natural: ASSIGN #NEXT-CHECK-DATE-CCYYMMDD := #W-CHECK-DATE
                    pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.nadd(1);                                                                                               //Natural: ADD 1 TO #NEXT-CHECK-MM
                    pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(0);                                                                                                        //Natural: ASSIGN #TR-TRAN-CODE := 000
                    if (condition(pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.greater(12)))                                                                            //Natural: IF #NEXT-CHECK-MM GT 12
                    {
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Mm.setValue(1);                                                                                       //Natural: MOVE 1 TO #NEXT-CHECK-MM
                        pnd_Next_Check_Date_Ccyymmdd_Pnd_Next_Check_Ccyy.nadd(1);                                                                                         //Natural: ADD 1 TO #NEXT-CHECK-CCYY
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                        sub_Write_Tran_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                        sub_Write_Tran_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Record_Cde().equals(10))))
            {
                decideConditionsMet440++;
                //*  4/08 -- ORIGIN CODE
                pnd_Bypass_Cntrct.reset();                                                                                                                                //Natural: RESET #BYPASS-CNTRCT #GEN-700-TRN-SW #GEN-006-TRN-SW
                pnd_Gen_700_Trn_Sw.reset();
                pnd_Gen_006_Trn_Sw.reset();
                pnd_Sve_Filler_1.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Filler_1());                                                                                 //Natural: ASSIGN #SVE-FILLER-1 := #FILLER-1
                pnd_Sve_Opt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde());                                                                               //Natural: ASSIGN #SVE-OPT := #CNTRCT-OPTN-CDE
                pnd_Sve_Issu_Dte.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Issue_Dte());                                                                         //Natural: ASSIGN #SVE-ISSU-DTE := #CNTRCT-ISSUE-DTE
                pnd_Sve_Crrncy_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Crrncy_Cde());                                                                      //Natural: ASSIGN #SVE-CRRNCY-CDE := #CNTRCT-CRRNCY-CDE
                if (condition(! (ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde().equals(22) || ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde().equals(25)       //Natural: IF NOT ( #CNTRCT-OPTN-CDE = 22 OR = 25 OR = 27 OR = 28 OR = 30 )
                    || ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde().equals(27) || ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde().equals(28) 
                    || ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde().equals(30))))
                {
                    pnd_Bypass_Cntrct.setValue("Y");                                                                                                                      //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_1st_Dod().equals(getZero())))                                                                     //Natural: IF #CNTRCT-1ST-DOD = 0
                {
                    pnd_Sve_Xref.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_1st_Xref());                                                                          //Natural: ASSIGN #SVE-XREF := #CNTRCT-1ST-XREF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_2nd_Dod().equals(getZero())))                                                                 //Natural: IF #CNTRCT-2ND-DOD = 0
                    {
                        pnd_Sve_Xref.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_2nd_Xref());                                                                      //Natural: ASSIGN #SVE-XREF := #CNTRCT-2ND-XREF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CPR INFO
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Record_Cde().equals(20))))
            {
                decideConditionsMet440++;
                pnd_Gen_006_Trn_Sw.reset();                                                                                                                               //Natural: RESET #GEN-006-TRN-SW #GEN-102-TRN-SW
                pnd_Gen_102_Trn_Sw.reset();
                if (condition(! (pnd_Sve_Opt.equals(22) || pnd_Sve_Opt.equals(25) || pnd_Sve_Opt.equals(27) || pnd_Sve_Opt.equals(28) || pnd_Sve_Opt.equals(30))))        //Natural: IF NOT ( #SVE-OPT = 22 OR = 25 OR = 27 OR = 28 OR = 30 )
                {
                    pnd_Bypass_Cntrct.setValue("Y");                                                                                                                      //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass_Cntrct.reset();                                                                                                                            //Natural: RESET #BYPASS-CNTRCT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde().equals(9)))                                                                          //Natural: IF #CNTRCT-ACTVTY-CDE = 9
                {
                    pnd_Bypass_Cntrct.setValue("Y");                                                                                                                      //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Bypass_Cntrct.setValue("N");                                                                                                                      //Natural: ASSIGN #BYPASS-CNTRCT := 'N'
                    //*  ADDED 6/02
                    //*  ADDED 6/02
                }                                                                                                                                                         //Natural: END-IF
                pnd_Sve_Mode.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Mode_Ind());                                                                              //Natural: ASSIGN #SVE-MODE := #CNTRCT-MODE-IND
                pnd_Sve_Pend_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Pend_Cde());                                                                          //Natural: ASSIGN #SVE-PEND-CDE := #CNTRCT-PEND-CDE
                //*      #WS-CNTRCT-FINAL-PER-DTE := #CNTRCT-FINAL-PER-DTE
                pnd_Ws_Cntrct_Pend_Dte.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Pend_Dte());                                                                    //Natural: ASSIGN #WS-CNTRCT-PEND-DTE := #CNTRCT-PEND-DTE
                short decideConditionsMet505 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #SVE-MODE-POS1;//Natural: VALUE 1
                if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos1.equals(1))))
                {
                    decideConditionsMet505++;
                    ignore();
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos1.equals(8))))
                {
                    decideConditionsMet505++;
                    if (condition(pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.notEquals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                              //Natural: IF #SVE-MODE-POS2 NE #CURR-CHECK-MM
                    {
                        pnd_Bypass_Cntrct.setValue("Y");                                                                                                                  //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos1.equals(7))))
                {
                    decideConditionsMet505++;
                    if (condition(pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                                 //Natural: IF #SVE-MODE-POS2 = #CURR-CHECK-MM
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.add(6)).equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                    //Natural: IF ( #SVE-MODE-POS2 + 6 ) = #CURR-CHECK-MM
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Bypass_Cntrct.setValue("Y");                                                                                                              //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 6
                else if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos1.equals(6))))
                {
                    decideConditionsMet505++;
                    if (condition(pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                                 //Natural: IF #SVE-MODE-POS2 = #CURR-CHECK-MM
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.add(3)).equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                    //Natural: IF ( #SVE-MODE-POS2 + 3 ) = #CURR-CHECK-MM
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.add(6)).equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                                //Natural: IF ( #SVE-MODE-POS2 + 6 ) = #CURR-CHECK-MM
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition((pnd_Sve_Mode_Pnd_Sve_Mode_Pos2.add(9)).equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Mm)))                            //Natural: IF ( #SVE-MODE-POS2 + 9 ) = #CURR-CHECK-MM
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Bypass_Cntrct.setValue("Y");                                                                                                      //Natural: ASSIGN #BYPASS-CNTRCT := 'Y'
                                    if (condition(true)) continue;                                                                                                        //Natural: ESCAPE TOP
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Work_Cpr_Ivc.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cpr_Ivc_Info_Alpha());                                                                       //Natural: ASSIGN #WORK-CPR-IVC := #CPR-IVC-INFO-ALPHA
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde().greater(2)))                                                                          //Natural: IF #CNTRCT-PAYEE-CDE GT 02
                {
                    pnd_Sve_Xref.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Bnfcry_Xref());                                                                              //Natural: ASSIGN #SVE-XREF := #BNFCRY-XREF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde().equals(7)))                                                                          //Natural: IF #CNTRCT-ACTVTY-CDE = 7
                {
                    pnd_Gen_006_Trn_Sw.setValue("Y");                                                                                                                     //Natural: ASSIGN #GEN-006-TRN-SW := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde().greater(2) || pnd_Sve_Opt.equals(22) || pnd_Sve_Opt.equals(23) ||                     //Natural: IF #CNTRCT-PAYEE-CDE GT 02 OR #SVE-OPT = 22 OR = 23 OR = 25 OR = 27 OR = 28 OR = 30
                    pnd_Sve_Opt.equals(25) || pnd_Sve_Opt.equals(27) || pnd_Sve_Opt.equals(28) || pnd_Sve_Opt.equals(30)))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Gen_700_Trn_Sw.setValue("Y");                                                                                                                     //Natural: ASSIGN #GEN-700-TRN-SW := 'Y'
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED FOLLOWING FOR IPRO CONTRACTS 102 LOGIC   2/01
                //*  COMMENT OUT THE FOLLOWING LINES TO DISABLE AUTO PEND OF IPROS 5/06 =>
                //*  NO LONGER NECESSARY SINCE THE BUSINESS RULE TO FORCE ANNUITIZATION
                //*  OF IPRO CONTRACTS AT AGE 70.5 HAS BEEN ABOLISHED. 5/06
                //*      IF #SVE-OPT =  25 OR= 27
                //*        #WS-WRK-FIELD2 := #FILLER-3A   /* EMPLOY-CDE
                //*        #WS-CNTRCT-FINAL-PAY-DTE  :=   #CNTRCT-FINAL-PAY-DTE
                //*        IF #CNTRCT-PEND-CDE     = 'Q'
                //*            OR
                //*          #CNTRCT-EMPLYMNT-TRMNT-CDE = 'Y'
                //*            OR
                //*            (#CNTRCT-PAYEE-CDE GT 1 AND #CNTRCT-PEND-CDE  NE '0')
                //*            OR
                //*          #CNTRCT-FINAL-PAY-DTE  =  99999999
                //*          ESCAPE TOP
                //*        ELSE
                //*          IF #CURR-CHECK-DATE-CCYYMMDD  = #CNTRCT-FINAL-PAY-DTE
                //*              OR
                //*              #CURR-CHECK-DATE-CCYYMMDD GT #CNTRCT-FINAL-PAY-DTE
                //*            #GEN-102-TRN-SW  := 'Y'
                //*            ESCAPE TOP
                //*          END-IF
                //*        END-IF
                //*      END-IF
                //*  END OF ADD  102 LOGIC FOR IPRO 2/01  /* 5/06 END OF CODE
                if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Per_Dte().equals(getZero()) && ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte().equals(getZero()))) //Natural: IF #CNTRCT-FINAL-PER-DTE = 0 AND #CNTRCT-FINAL-PAY-DTE = 0
                {
                    pnd_Gen_700_Trn_Sw.setValue("Y");                                                                                                                     //Natural: ASSIGN #GEN-700-TRN-SW := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Per_Dte().equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm)               //Natural: IF #CNTRCT-FINAL-PER-DTE = #CURR-CHECK-DATE-CCYYMM AND #CNTRCT-FINAL-PAY-DTE = 0
                        && ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte().equals(getZero())))
                    {
                        //*  BYPASS IPRO TERMINATION 04/07
                        if (condition(pnd_Sve_Opt.equals(25) || pnd_Sve_Opt.equals(27)))                                                                                  //Natural: IF #SVE-OPT = 25 OR = 27
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Gen_006_Trn_Sw.setValue("Y");                                                                                                             //Natural: ASSIGN #GEN-006-TRN-SW := 'Y'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Per_Dte().equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm)           //Natural: IF #CNTRCT-FINAL-PER-DTE = #CURR-CHECK-DATE-CCYYMM AND #CNTRCT-FINAL-PAY-DTE GT #CURR-CHECK-DATE-CCYYMMDD
                            && ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte().greater(pnd_Curr_Check_Date_Ccyymmdd)))
                        {
                            pnd_Gen_700_Trn_Sw.setValue("Y");                                                                                                             //Natural: ASSIGN #GEN-700-TRN-SW := 'Y'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Per_Dte().equals(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm)       //Natural: IF #CNTRCT-FINAL-PER-DTE = #CURR-CHECK-DATE-CCYYMM AND #CNTRCT-FINAL-PAY-DTE = #CURR-CHECK-DATE-CCYYMMDD
                                && ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte().equals(pnd_Curr_Check_Date_Ccyymmdd)))
                            {
                                //*  BYPASS IPRO TERMINATION 04/07
                                if (condition(pnd_Sve_Opt.equals(25) || pnd_Sve_Opt.equals(27)))                                                                          //Natural: IF #SVE-OPT = 25 OR = 27
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Gen_006_Trn_Sw.setValue("Y");                                                                                                     //Natural: ASSIGN #GEN-006-TRN-SW := 'Y'
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Gen_700_Trn_Sw.setValue("Y");                                                                                                         //Natural: ASSIGN #GEN-700-TRN-SW := 'Y'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FUND RECORD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Record_Cde().equals(30))))
            {
                decideConditionsMet440++;
                //*  ADDED FOR IPRO CONTRACTS  2/01
                if (condition(pnd_Gen_102_Trn_Sw.equals("Y")))                                                                                                            //Natural: IF #GEN-102-TRN-SW = 'Y'
                {
                    //*  GEN PEND-CODE 'Q'
                                                                                                                                                                          //Natural: PERFORM GEN-102-TRANS
                    sub_Gen_102_Trans();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Gen_006_Trn_Sw.equals("Y")))                                                                                                            //Natural: IF #GEN-006-TRN-SW = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM GEN-006-TRANS
                    sub_Gen_006_Trans();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition((pnd_Gen_700_Trn_Sw.equals("Y") && ! (((pnd_Sve_Filler_1_Pnd_Sve_Orgn.greaterOrEqual(51) && pnd_Sve_Filler_1_Pnd_Sve_Orgn.lessOrEqual(58))  //Natural: IF #GEN-700-TRN-SW = 'Y' AND NOT ( #SVE-ORGN = 51 THRU 58 OR #SVE-ORGN = 71 THRU 78 )
                    || (pnd_Sve_Filler_1_Pnd_Sve_Orgn.greaterOrEqual(71) && pnd_Sve_Filler_1_Pnd_Sve_Orgn.lessOrEqual(78)))))))
                {
                    //*  4/08 -- BYPASS UPDATE OF IVC FOR ROTH CONTRACTS
                    //*  4/08 -- DO NOT UPDATE IVC USED
                                                                                                                                                                          //Natural: PERFORM GEN-700-TRANS
                    sub_Gen_700_Trans();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Gen_700_Trn_Sw.reset();                                                                                                                               //Natural: RESET #GEN-700-TRN-SW
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Record_Cde().equals(40))))
            {
                decideConditionsMet440++;
                if (condition(pnd_Bypass_Cntrct.equals("Y")))                                                                                                             //Natural: IF #BYPASS-CNTRCT = 'Y'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GEN-900-TRANS
                sub_Gen_900_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ADDED FOLLOWING                  1/98
        //*  -----------------------------------------------------------------
        //*  THIS LOGIC DONE AFTER ALL TRANSACTIONS WRITTEN
        //*     SORT TRANS FILE IN TRAN-CODE ,INTENT-CODE & CNTRCT/PY ORDER
        //*    & PRINT DETAIL REPORT OF TRANSACTIONS GENERATED
        //*  -----------------------------------------------------------------
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #TRAN-REC-OUT
        while (condition(getWorkFiles().read(2, pnd_Tran_Rec_Out)))
        {
            if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(0)))                                                                                                   //Natural: IF #TR-TRAN-CODE = 000
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code, pnd_Tran_Rec_Out_Pnd_Tr_Int_Code, pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee, pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde,  //Natural: END-ALL
                pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed, pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due, pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde, pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde, 
                pnd_Tran_Rec_Out_Pnd_Tr_Xref, pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(1), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(2), 
                pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(3), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(4), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(5), 
                pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(1), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(2), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(3), 
                pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(4), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(5), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(1), 
                pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(2), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(3), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(4), 
                pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(5), pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div, 
                pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div, pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code, pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr, 
                pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt, pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  (A1/1:5)
        //*  (N1/1:5)
        //*  (N7.2/1:5)
        getSort().sortData(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code, pnd_Tran_Rec_Out_Pnd_Tr_Int_Code, pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee, pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde); //Natural: SORT BY #TR-TRAN-CODE ASCENDING #TR-INT-CODE ASCENDING #TR-PPCN-NBR-PAYEE ASCENDING #TR-PAYEE-CDE ASCENDING USING #TR-CHK-DTE-PROCESSED #TR-CHK-DTE-DUE #TR-CMPNY-CDE #TR-FUND-CDE #TR-XREF #TR-CNTRCT-COMPANY-CDE ( * ) #TR-RCVRY-TYPE ( * ) #TR-PER-TAX-FREE ( * ) #TR-TOT-UNITS #TR-TOT-PER-PMT #TR-TOT-PER-DIV #TR-TOT-FIN-PMT #TR-TOT-FIN-DIV #TR-DED-CODE #TR-DED-SEQ-NBR #TR-PER-DED-AMT #TR-OLD-DED-AMT
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code, pnd_Tran_Rec_Out_Pnd_Tr_Int_Code, pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee, 
            pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde, pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed, pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due, pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde, 
            pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde, pnd_Tran_Rec_Out_Pnd_Tr_Xref, pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(1), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(2), 
            pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(3), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(4), pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(5), 
            pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(1), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(2), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(3), 
            pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(4), pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(5), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(1), 
            pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(2), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(3), pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(4), 
            pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(5), pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div, 
            pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt, pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div, pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code, pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr, 
            pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt, pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt)))
        {
            //*   700 TRANS
            //*    #TR-PER-TAX-AMT        (*)
            //*   900 TRANS
            //*  FIRST TIME THRU INITIALIZED TO 999
            if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(999)))                                                                                              //Natural: IF #SVE-TRAN-CODE = 999
            {
                pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setValue(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code);                                                                         //Natural: ASSIGN #SVE-TRAN-CODE := #TR-TRAN-CODE
                pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setValue(pnd_Tran_Rec_Out_Pnd_Tr_Int_Code);                                                                           //Natural: ASSIGN #SVE-INT-CODE := #TR-INT-CODE
                                                                                                                                                                          //Natural: PERFORM SET-HEADING-LINES
                sub_Set_Heading_Lines();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.notEquals(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code) || pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.notEquals(pnd_Tran_Rec_Out_Pnd_Tr_Int_Code))) //Natural: IF #SVE-TRAN-CODE NE #TR-TRAN-CODE OR #SVE-INT-CODE NE #TR-INT-CODE
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-LINE
                    sub_Print_Total_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM SET-HEADING-LINES
                    sub_Set_Heading_Lines();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 ) EVEN IF TOP OF PAGE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) ' '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.setValue(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code);                                                                     //Natural: ASSIGN #SVE-TRAN-CODE := #TR-TRAN-CODE
                    pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.setValue(pnd_Tran_Rec_Out_Pnd_Tr_Int_Code);                                                                       //Natural: ASSIGN #SVE-INT-CODE := #TR-INT-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TWO 006 TRANS
            if (condition(pnd_Save_Payee_Key.notEquals(pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee)))                                                                          //Natural: IF #SAVE-PAYEE-KEY NE #TR-PPCN-NBR-PAYEE
            {
                pnd_Prog_Misc_Area_Pnd_Total_Trans.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-TRANS
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINE
                sub_Print_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Save_Payee_Key.setValue(pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_Payee);                                                                                      //Natural: ASSIGN #SAVE-PAYEE-KEY := #TR-PPCN-NBR-PAYEE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-LINE
        sub_Print_Total_Line();
        if (condition(Global.isEscape())) {return;}
        //*  END OF ADD  1/98
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE," ");                                                                                          //Natural: WRITE ( 1 ) /// ' '
        if (Global.isEscape()) return;
        //*  WRITE (1) ' TOTAL IPRO PND (Q) (TRANS 102) --------->'
        getReports().write(1, ReportOption.NOTITLE," TOTAL IPRO PEND (Q) & DTH CLAIM (R) (TRANS 102) --------->",pnd_Tot_Tran_102, new ReportEditMask                     //Natural: WRITE ( 1 ) ' TOTAL IPRO PEND (Q) & DTH CLAIM (R) (TRANS 102) --------->' #TOT-TRAN-102 ( EM = ZZZ,ZZ9 )
            ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL TERMINATIONS (TRANS 006) -------------------------->",pnd_Tot_Tran_006, new ReportEditMask                     //Natural: WRITE ( 1 ) ' TOTAL TERMINATIONS (TRANS 006) -------------------------->' #TOT-TRAN-006 ( EM = ZZZ,ZZ9 )
            ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL TAX TRANSACTIONS (TRANS 700)----------------------->",pnd_Tot_Tran_700, new ReportEditMask                     //Natural: WRITE ( 1 ) ' TOTAL TAX TRANSACTIONS (TRANS 700)----------------------->' #TOT-TRAN-700 ( EM = ZZZ,ZZ9 )
            ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL DEDUCTION FROM NET (TRANS 900) -------------------->",pnd_Tot_Tran_900, new ReportEditMask                     //Natural: WRITE ( 1 ) ' TOTAL DEDUCTION FROM NET (TRANS 900) -------------------->' #TOT-TRAN-900 ( EM = ZZZ,ZZ9 )
            ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TOTAL RECORDS WRITTEN------------------------------------>",pnd_Tot_Trans_Out, new ReportEditMask                    //Natural: WRITE ( 1 ) ' TOTAL RECORDS WRITTEN------------------------------------>' #TOT-TRANS-OUT ( EM = ZZZ,ZZ9 )
            ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMX());                                                         //Natural: WRITE ( 1 ) / *PROGRAM 'FINISHED AT: ' *TIMX
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  ----------------------------------------------
        //*  GEN 006 TRANS TERMINATE CONTRACT
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-006-TRANS
        //*  END ADD 6/02
        //*  ADDED FOLLOWING    5/97
        //*  ----------------------------------------------
        //*  GEN 700 TAX TRANS
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-700-TRANS
        //*  ----------------------------------------------
        //*  700 TRANS FOR RECOVERY-TYPE = 1
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECOVERY-TYPE-1
        //*  ----------------------------------------------
        //*  700 TRANS FOR RECOVERY-TYPE = 5
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECOVERY-TYPE-5
        //*  ----------------------------------------------
        //*  GEN 900 DEDUCTION TRANS
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-900-TRANS
        //*  ----------------------------------------------
        //*  GEN 102 TRANS 'Q' PEND IPRO CONTRACTS
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-102-TRANS
        //*  -------------------------------------------------
        //*  GEN 102 TRANS 'R' GENERATE NEW PEND CODE FOR
        //*  CONTRACTS THAT ARE DUE TO TERMINATE AND ARE CURRENTLY
        //*  PENDED WITH PEND CODE A OR C DUE TO DEATH
        //*  -------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-102-TRANS-FOR-PENDED-CNTRCTS
        //*  ----------------------------------------------
        //*  WRITE TRAN RECORD
        //*  ----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAN-RECORD
        //* ********************************************************************
        //*  CONVERT 2 BYTE FUND CODE TO 1 BYTE CODE
        //*  FOR CALL TO AIAN026
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IAAN0511-CONV-FUND-CDE
        //*  *******************************************************************
        //*  CALL ACTURIAL MODULE FOR CREF UNIT VALUE
        //*       FOR CREF MONTLY UNIT VALUE CONTRACTS
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-AIAN026-GET-CREF-UNIT-VALUE
        //*  IA-CALL-TYPE   := 'L'
        //*  IA-CHECK-DATE  := 99999999
        //*  IA-ISSUE-DATE  := 00000000
        //*  ADDED ALL OF FOLLOWING ROUTINES   1/98
        //*  **************************************************************
        //*  **************************************************************
        //*  THE FOLLOWING ROUTINES DONE AFTER ALL TRANS ARE GENERATED
        //*  PRODUCE DETAIL REPORT OF TRANSACTIONS GENERATED
        //*  **************************************************************
        //*  **************************************************************
        //*  -------------------------------------------------
        //*  SET UP PROPER HEADINGS FOR TRANSACTION REPORT
        //*  -------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-HEADING-LINES
        //*  ------------------------------------
        //*  PRINT DETAIL LINES
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINE
        //*  ------------------------------------
        //*  PRINT TOTAL FOR TRANS & NEWPAGE
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTAL-LINE
    }
    private void sub_Gen_006_Trans() throws Exception                                                                                                                     //Natural: GEN-006-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out.reset();                                                                                                                                         //Natural: RESET #TRAN-REC-OUT
        //*  ADDED FOLLOWING TO HANDLE NOT TERMINATING PAYEE GT 02   6/02
        //*               WITH  PEND CODE 'A' OR 'C'
        //*               GENERATE 102 TRANS WITH  PEND CODE 'R'  6/02
        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde().greater(2) || pnd_Sve_Opt.equals(22)))                                                        //Natural: IF #CNTRCT-PAYEE-CDE GT 02 OR #SVE-OPT = 22
        {
            if (condition(pnd_Sve_Pend_Cde.equals("A") || pnd_Sve_Pend_Cde.equals("C")))                                                                                  //Natural: IF #SVE-PEND-CDE = 'A' OR = 'C'
            {
                                                                                                                                                                          //Natural: PERFORM GEN-102-TRANS-FOR-PENDED-CNTRCTS
                sub_Gen_102_Trans_For_Pended_Cntrcts();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(6);                                                                                                                    //Natural: ASSIGN #TR-TRAN-CODE := 006
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                                                  //Natural: ASSIGN #TR-CMPNY-CDE := #TIAA-CMPNY-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                    //Natural: ASSIGN #TR-FUND-CDE := #TIAA-FUND-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Xref.setValue(pnd_Sve_Xref);                                                                                                              //Natural: ASSIGN #TR-XREF := #SVE-XREF
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt());                                                              //Natural: ASSIGN #TR-TOT-PER-PMT := #TIAA-TOT-PER-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt());                                                              //Natural: ASSIGN #TR-TOT-PER-DIV := #TIAA-TOT-DIV-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt());                                                       //Natural: ASSIGN #TR-TOT-FIN-PMT := #TIAA-RATE-FINAL-PAY-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt());                                                       //Natural: ASSIGN #TR-TOT-FIN-DIV := #TIAA-RATE-FINAL-DIV-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Units.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Units_Cnt());                                                                  //Natural: ASSIGN #TR-TOT-UNITS := #TIAA-UNITS-CNT
        //*  ADDED THIS IF  1/98
        if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr.notEquals(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr())))                                                  //Natural: IF #TR-PPCN-NBR NE #CNTRCT-PPCN-NBR
        {
            //*  CHECK FOR MULT 006 TRANS
            pnd_Tot_Tran_006.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOT-TRAN-006
            //*  FOR MULTI FUNDED CNTRCTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ADDED THIS IF 1/98
            if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde.notEquals(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde())))                                            //Natural: IF #TR-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                pnd_Tot_Tran_006.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-TRAN-006
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).setValue(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd.getValue(pnd_I));                                //Natural: ASSIGN #TR-CNTRCT-COMPANY-CDE ( #I ) := #W-CNTRCT-COMPANY-CD ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  END OF ADD 5/97
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
        sub_Write_Tran_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_700_Trans() throws Exception                                                                                                                     //Natural: GEN-700-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out.reset();                                                                                                                                         //Natural: RESET #TRAN-REC-OUT
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(700);                                                                                                                  //Natural: ASSIGN #TR-TRAN-CODE := 700
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                                                  //Natural: ASSIGN #TR-CMPNY-CDE := #TIAA-CMPNY-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                    //Natural: ASSIGN #TR-FUND-CDE := #TIAA-FUND-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Xref.setValue(pnd_Sve_Xref);                                                                                                              //Natural: ASSIGN #TR-XREF := #SVE-XREF
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Pmt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt());                                                              //Natural: ASSIGN #TR-TOT-PER-PMT := #TIAA-TOT-PER-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Per_Div.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt());                                                              //Natural: ASSIGN #TR-TOT-PER-DIV := #TIAA-TOT-DIV-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Pmt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt());                                                       //Natural: ASSIGN #TR-TOT-FIN-PMT := #TIAA-RATE-FINAL-PAY-AMT
        pnd_Tran_Rec_Out_Pnd_Tr_Tot_Fin_Div.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt());                                                       //Natural: ASSIGN #TR-TOT-FIN-DIV := #TIAA-RATE-FINAL-DIV-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Wrte_700_Sw.resetInitial();                                                                                                                               //Natural: RESET INITIAL #WRTE-700-SW
            pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).setValue(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd.getValue(pnd_I));                                //Natural: ASSIGN #TR-CNTRCT-COMPANY-CDE ( #I ) := #W-CNTRCT-COMPANY-CD ( #I )
            if (condition(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind.getValue(pnd_I).equals("1")))                                                                      //Natural: IF #W-CNTRCT-RCVRY-TYPE-IND ( #I ) = '1'
            {
                                                                                                                                                                          //Natural: PERFORM RECOVERY-TYPE-1
                sub_Recovery_Type_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Wrte_700_Sw.notEquals("N")))                                                                                                            //Natural: IF #WRTE-700-SW NE 'N'
                {
                    pnd_Tot_Tran_700.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-TRAN-700
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind.getValue(pnd_I).equals("5")))                                                                      //Natural: IF #W-CNTRCT-RCVRY-TYPE-IND ( #I ) = '5'
            {
                                                                                                                                                                          //Natural: PERFORM RECOVERY-TYPE-5
                sub_Recovery_Type_5();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Wrte_700_Sw.notEquals("N")))                                                                                                            //Natural: IF #WRTE-700-SW NE 'N'
                {
                    pnd_Tot_Tran_700.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-TRAN-700
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Rcvry_Type_Ind.getValue(pnd_I).equals("0") || pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Per_Ivc_Amt.getValue(pnd_I).equals(getZero())  //Natural: IF #W-CNTRCT-RCVRY-TYPE-IND ( #I ) = '0' OR #W-CNTRCT-PER-IVC-AMT ( #I ) = 0 OR #SVE-ISSU-DTE LT 198701 OR #W-CNTRCT-IVC-USED-AMT ( #I ) LT #W-CNTRCT-IVC-AMT ( #I )
                || pnd_Sve_Issu_Dte.less(198701) || pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt.getValue(pnd_I).less(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt.getValue(pnd_I))))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM RECOVERY-TYPE-5
                sub_Recovery_Type_5();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Wrte_700_Sw.notEquals("N")))                                                                                                            //Natural: IF #WRTE-700-SW NE 'N'
                {
                    pnd_Tot_Tran_700.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-TRAN-700
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF CONTRACT
            if (condition(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd.getValue(pnd_I).equals("C")))                                                                          //Natural: IF #W-CNTRCT-COMPANY-CD ( #I ) = 'C'
            {
                                                                                                                                                                          //Natural: PERFORM RECOVERY-TYPE-5
                sub_Recovery_Type_5();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Wrte_700_Sw.notEquals("N")))                                                                                                            //Natural: IF #WRTE-700-SW NE 'N'
                {
                    pnd_Tot_Tran_700.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOT-TRAN-700
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                    sub_Write_Tran_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Recovery_Type_1() throws Exception                                                                                                                   //Natural: RECOVERY-TYPE-1
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt.getValue(pnd_I).greaterOrEqual(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt.getValue(pnd_I))))                  //Natural: IF #W-CNTRCT-IVC-USED-AMT ( #I ) GE #W-CNTRCT-IVC-AMT ( #I )
        {
                                                                                                                                                                          //Natural: PERFORM RECOVERY-TYPE-5
            sub_Recovery_Type_5();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #W-CNTRCT-COMPANY-CD(#I) = 'C' OR =  'U'     /* COMMENTED 5/97
        //*  ADDED 1/98
        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde().equals("W") || ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde().equals("4")))                //Natural: IF #TIAA-CMPNY-CDE = 'W' OR = '4'
        {
                                                                                                                                                                          //Natural: PERFORM CALL-IAAN0511-CONV-FUND-CDE
            sub_Call_Iaan0511_Conv_Fund_Cde();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-AIAN026-GET-CREF-UNIT-VALUE
            sub_Call_Aian026_Get_Cref_Unit_Value();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Per_Pmt.compute(new ComputeParameters(false, pnd_Tot_Per_Pmt), ia_Aian026_Linkage_Auv_Returned.multiply(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Units_Cnt())); //Natural: ASSIGN #TOT-PER-PMT := AUV-RETURNED * #TIAA-UNITS-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Per_Pmt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Tot_Per_Amt());                                                                              //Natural: ASSIGN #TOT-PER-PMT := #TIAA-TOT-PER-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  #TOT-PER-PMT :=  #TIAA-TOT-PER-AMT  /* COMMENTED 1/98
        if (condition((pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt.getValue(pnd_I).subtract(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt.getValue(pnd_I))).greaterOrEqual(pnd_Tot_Per_Pmt))) //Natural: IF ( #W-CNTRCT-IVC-AMT ( #I ) - #W-CNTRCT-IVC-USED-AMT ( #I ) ) GE #TOT-PER-PMT
        {
            pnd_Wrte_700_Sw.setValue("N");                                                                                                                                //Natural: ASSIGN #WRTE-700-SW := 'N'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(pnd_I).setValue(5);                                                                                                   //Natural: MOVE 5 TO #TR-RCVRY-TYPE ( #I )
        pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(pnd_I)),                  //Natural: COMPUTE #TR-PER-TAX-FREE ( #I ) = #W-CNTRCT-IVC-AMT ( #I ) - #W-CNTRCT-IVC-USED-AMT ( #I )
            pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Amt.getValue(pnd_I).subtract(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Ivc_Used_Amt.getValue(pnd_I)));
        //*  #TOT-UPD-PMT :=  #TOT-PER-PMT  /* COMMENTED  5/97
    }
    private void sub_Recovery_Type_5() throws Exception                                                                                                                   //Natural: RECOVERY-TYPE-5
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(pnd_I).setValue(2);                                                                                                   //Natural: MOVE 2 TO #TR-RCVRY-TYPE ( #I )
        pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(pnd_I).setValue(0);                                                                                                 //Natural: ASSIGN #TR-PER-TAX-FREE ( #I ) := 0
    }
    private void sub_Gen_900_Trans() throws Exception                                                                                                                     //Natural: GEN-900-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out.reset();                                                                                                                                         //Natural: RESET #TRAN-REC-OUT
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(900);                                                                                                                  //Natural: ASSIGN #TR-TRAN-CODE := 900
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                                                  //Natural: ASSIGN #TR-CMPNY-CDE := #TIAA-CMPNY-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                    //Natural: ASSIGN #TR-FUND-CDE := #TIAA-FUND-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Xref.setValue(pnd_Sve_Xref);                                                                                                              //Natural: ASSIGN #TR-XREF := #SVE-XREF
        pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Seq_Nbr());                                                                 //Natural: ASSIGN #TR-DED-SEQ-NBR := #DDCTN-SEQ-NBR
        pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Cde());                                                                        //Natural: ASSIGN #TR-DED-CODE := #DDCTN-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Per_Amt());                                                                 //Natural: ASSIGN #TR-OLD-DED-AMT := #DDCTN-PER-AMT
        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Stp_Dte().notEquals(getZero())))                                                                           //Natural: IF #DDCTN-STP-DTE NE 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Final_Dte().equals(pnd_Curr_Check_Date_Ccyymmdd) || (ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Final_Dte().equals(getZero())  //Natural: IF #DDCTN-FINAL-DTE = #CURR-CHECK-DATE-CCYYMMDD OR ( #DDCTN-FINAL-DTE = 0 AND #DDCTN-PD-TO-DTE = #DDCTN-TOT-AMT )
            && ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Pd_To_Dte().equals(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Tot_Amt()))))
        {
            pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt.setValue(0);                                                                                                              //Natural: ASSIGN #TR-PER-DED-AMT := 0
            pnd_Tran_Rec_Out_Pnd_Tr_Int_Code.setValue(3);                                                                                                                 //Natural: ASSIGN #TR-INT-CODE := 3
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
            sub_Write_Tran_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Tran_900.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOT-TRAN-900
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Final_Dte().equals(getZero())))                                                                            //Natural: IF #DDCTN-FINAL-DTE = 0
        {
            if (condition((ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Per_Amt().add(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Pd_To_Dte())).greater(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Tot_Amt()))) //Natural: IF ( #DDCTN-PER-AMT + #DDCTN-PD-TO-DTE ) GT #DDCTN-TOT-AMT
            {
                pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt.compute(new ComputeParameters(false, pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt), ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Tot_Amt().subtract(ldaIaal140.getPnd_Input_Record_Pnd_Ddctn_Pd_To_Dte())); //Natural: COMPUTE #TR-PER-DED-AMT = #DDCTN-TOT-AMT - #DDCTN-PD-TO-DTE
                pnd_Tran_Rec_Out_Pnd_Tr_Int_Code.setValue(2);                                                                                                             //Natural: ASSIGN #TR-INT-CODE := 2
                pnd_Tot_Tran_900.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-TRAN-900
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
                sub_Write_Tran_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_102_Trans() throws Exception                                                                                                                     //Natural: GEN-102-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out.reset();                                                                                                                                         //Natural: RESET #TRAN-REC-OUT
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(102);                                                                                                                  //Natural: ASSIGN #TR-TRAN-CODE := 102
        pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde.setValue("Q");                                                                                                                   //Natural: ASSIGN #TR-PEND-CDE := 'Q'
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                                                  //Natural: ASSIGN #TR-CMPNY-CDE := #TIAA-CMPNY-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                    //Natural: ASSIGN #TR-FUND-CDE := #TIAA-FUND-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Xref.setValue(pnd_Sve_Xref);                                                                                                              //Natural: ASSIGN #TR-XREF := #SVE-XREF
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte.setValue(pnd_Ws_Cntrct_Final_Pay_Dte_Pnd_Ws_Cntrct_Final_Pay_Ccyymm);                                                         //Natural: ASSIGN #TR-FIN-PER-DTE := #WS-CNTRCT-FINAL-PAY-CCYYMM
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TR-FIN-PER-MM
        if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm.greater(12)))                                                                                                    //Natural: IF #TR-FIN-PER-MM GT 12
        {
            pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Mm.setValue(1);                                                                                                               //Natural: MOVE 1 TO #TR-FIN-PER-MM
            pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Ccyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TR-FIN-PER-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tot_Tran_102.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOT-TRAN-102
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).setValue(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd.getValue(pnd_I));                                //Natural: ASSIGN #TR-CNTRCT-COMPANY-CDE ( #I ) := #W-CNTRCT-COMPANY-CD ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
        sub_Write_Tran_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_102_Trans_For_Pended_Cntrcts() throws Exception                                                                                                  //Natural: GEN-102-TRANS-FOR-PENDED-CNTRCTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  KEEP ORIGINAL PEND DATE
        pnd_Tran_Rec_Out.reset();                                                                                                                                         //Natural: RESET #TRAN-REC-OUT
        pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.setValue(102);                                                                                                                  //Natural: ASSIGN #TR-TRAN-CODE := 102
        pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde.setValue("R");                                                                                                                   //Natural: ASSIGN #TR-PEND-CDE := 'R'
        pnd_Tran_Rec_Out_Pnd_Tr_Cmpny_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                                                  //Natural: ASSIGN #TR-CMPNY-CDE := #TIAA-CMPNY-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Fund_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                    //Natural: ASSIGN #TR-FUND-CDE := #TIAA-FUND-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Xref.setValue(pnd_Sve_Xref);                                                                                                              //Natural: ASSIGN #TR-XREF := #SVE-XREF
        pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte.setValue(pnd_Ws_Cntrct_Pend_Dte);                                                                                             //Natural: ASSIGN #TR-FIN-PER-DTE := #WS-CNTRCT-PEND-DTE
        //*  ADDED THIS IF  1/98
        if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr.notEquals(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr())))                                                  //Natural: IF #TR-PPCN-NBR NE #CNTRCT-PPCN-NBR
        {
            //*  CHECK FOR MULT 102 TRANS
            pnd_Tot_Tran_102.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOT-TRAN-102
            //*  FOR MULTI FUNDED CNTRCTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ADDED THIS IF 1/98
            if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde.notEquals(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde())))                                            //Natural: IF #TR-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                pnd_Tot_Tran_102.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-TRAN-102
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).setValue(pnd_Work_Cpr_Ivc_Pnd_W_Cntrct_Company_Cd.getValue(pnd_I));                                //Natural: ASSIGN #TR-CNTRCT-COMPANY-CDE ( #I ) := #W-CNTRCT-COMPANY-CD ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAN-RECORD
        sub_Write_Tran_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Tran_Record() throws Exception                                                                                                                 //Natural: WRITE-TRAN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr());                                                                  //Natural: ASSIGN #TR-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde());                                                                //Natural: ASSIGN #TR-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Processed.setValue(pnd_Curr_Check_Date_Ccyymmdd);                                                                                 //Natural: ASSIGN #TR-CHK-DTE-PROCESSED := #CURR-CHECK-DATE-CCYYMMDD
        pnd_Tran_Rec_Out_Pnd_Tr_Chk_Dte_Due.setValue(pnd_Next_Check_Date_Ccyymmdd);                                                                                       //Natural: ASSIGN #TR-CHK-DTE-DUE := #NEXT-CHECK-DATE-CCYYMMDD
        getWorkFiles().write(2, false, pnd_Tran_Rec_Out);                                                                                                                 //Natural: WRITE WORK FILE 2 #TRAN-REC-OUT
        pnd_Tot_Trans_Out.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-TRANS-OUT
    }
    private void sub_Call_Iaan0511_Conv_Fund_Cde() throws Exception                                                                                                       //Natural: CALL-IAAN0511-CONV-FUND-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Fund_1.reset();                                                                                                                                          //Natural: RESET #PARM-FUND-1
        pnd_Parm_Fund_2.setValue(ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                                                                     //Natural: ASSIGN #PARM-FUND-2 := #TIAA-FUND-CDE
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Parm_Fund_1, pnd_Parm_Fund_2, pnd_Parm_Rtn_Cde);                                                   //Natural: CALLNAT 'IAAN0511' #PARM-FUND-1 #PARM-FUND-2 #PARM-RTN-CDE
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Parm_Rtn_Cde.notEquals(getZero())))                                                                                                             //Natural: IF #PARM-RTN-CDE NE 0
        {
            getReports().write(1, ReportOption.NOTITLE,"FUND CODE NOT CONVERTED IN CALL TO IAAN0511");                                                                    //Natural: WRITE ( 1 ) 'FUND CODE NOT CONVERTED IN CALL TO IAAN0511'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"FOR #CNTRCT-PPCN-NBR->",ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr());                                    //Natural: WRITE ( 1 ) 'FOR #CNTRCT-PPCN-NBR->' #CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"CHECK IVC AMOUNT FOR THIS CONTRACT");                                                                             //Natural: WRITE ( 1 ) 'CHECK IVC AMOUNT FOR THIS CONTRACT'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"IAAN0511 RETURN-CODE-->",pnd_Parm_Rtn_Cde);                                                                       //Natural: WRITE ( 1 ) 'IAAN0511 RETURN-CODE-->' #PARM-RTN-CDE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"FUND-CODE PASSED-->",ldaIaal140.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                         //Natural: WRITE ( 1 ) 'FUND-CODE PASSED-->' #TIAA-FUND-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  MONTHLY UNIT VALUE FOR END OF MONTH
    private void sub_Call_Aian026_Get_Cref_Unit_Value() throws Exception                                                                                                  //Natural: CALL-AIAN026-GET-CREF-UNIT-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        ia_Aian026_Linkage_Ia_Call_Type.setValue("P");                                                                                                                    //Natural: ASSIGN IA-CALL-TYPE := 'P'
        ia_Aian026_Linkage_Ia_Reval_Methd.setValue("M");                                                                                                                  //Natural: ASSIGN IA-REVAL-METHD := 'M'
        ia_Aian026_Linkage_Ia_Check_Date.setValue(pnd_Curr_Check_Date_Ccyymmdd_Pnd_Curr_Check_Date_Ccyymm);                                                               //Natural: ASSIGN IA-CHECK-DATE := #CURR-CHECK-DATE-CCYYMM
        ia_Aian026_Linkage_Ia_Issue_Date.setValue(pnd_Sve_Issu_Dte);                                                                                                      //Natural: ASSIGN IA-ISSUE-DATE := #SVE-ISSU-DTE
        ia_Aian026_Linkage_Ia_Issue_Day.setValue(1);                                                                                                                      //Natural: ASSIGN IA-ISSUE-DAY := 01
        //*  NEW ACTUARY MODULE 4/97
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ia_Aian026_Linkage);                                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ia_Aian026_Linkage_Return_Code.notEquals(getZero())))                                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(1, ReportOption.NOTITLE,"NON ZERO RETURN CODE RETURNED FROM CALL TO AIAN026");                                                             //Natural: WRITE ( 1 ) 'NON ZERO RETURN CODE RETURNED FROM CALL TO AIAN026'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"AIAN026 RETURN-CODE-->",ia_Aian026_Linkage_Return_Code,NEWLINE);                                                  //Natural: WRITE ( 1 ) 'AIAN026 RETURN-CODE-->' RETURN-CODE /
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"FOR CREF MONTHLY UNIT VALUE         ");                                                                           //Natural: WRITE ( 1 ) 'FOR CREF MONTHLY UNIT VALUE         '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"FOR #CNTRCT-PPCN-NBR->",ldaIaal140.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr());                                    //Natural: WRITE ( 1 ) 'FOR #CNTRCT-PPCN-NBR->' #CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"IA-FUND-CODE->",ia_Aian026_Linkage_Ia_Fund_Code);                                                                 //Natural: WRITE ( 1 ) 'IA-FUND-CODE->' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"CHECK IVC AMOUNT FOR THIS CONTRACT",NEWLINE);                                                                     //Natural: WRITE ( 1 ) 'CHECK IVC AMOUNT FOR THIS CONTRACT' /
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"IA-FUND-CODE->",ia_Aian026_Linkage_Ia_Fund_Code);                                                                 //Natural: WRITE ( 1 ) 'IA-FUND-CODE->' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"PARM VALUES PASSED TO AIAN026 FOLLOW",NEWLINE,"=",ia_Aian026_Linkage_Ia_Call_Type,NEWLINE,"=",                    //Natural: WRITE ( 1 ) 'PARM VALUES PASSED TO AIAN026 FOLLOW'/ '=' IA-CALL-TYPE / '=' IA-REVAL-METHD / '=' IA-CHECK-DATE / '=' IA-ISSUE-DATE
                ia_Aian026_Linkage_Ia_Reval_Methd,NEWLINE,"=",ia_Aian026_Linkage_Ia_Check_Date,NEWLINE,"=",ia_Aian026_Linkage_Ia_Issue_Date);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Heading_Lines() throws Exception                                                                                                                 //Natural: SET-HEADING-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED FOLLOWING FOR IPRO 2/01
        if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(102)))                                                                                                     //Natural: IF #TR-TRAN-CODE = 102
        {
            pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_102_Pend);                                                                 //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-102-PEND
            pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_102);                                                                      //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-102
            pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_102);                                                                     //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-102
            pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_102);                                                                     //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-102
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 2/01
        if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(6)))                                                                                                       //Natural: IF #TR-TRAN-CODE = 006
        {
            pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Term);                                                                     //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Term);                                                                     //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Term);                                                                    //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-TERM
            pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Term);                                                                    //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-TERM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(700)))                                                                                                 //Natural: IF #TR-TRAN-CODE = 700
            {
                pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Tax);                                                                  //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Tax);                                                                  //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Tax);                                                                 //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-TAX
                pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Tax);                                                                 //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-TAX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(900) && pnd_Tran_Rec_Out_Pnd_Tr_Int_Code.equals(2)))                                               //Natural: IF #TR-TRAN-CODE = 900 AND #TR-INT-CODE = 2
                {
                    pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng);                                                         //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-CHNG
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded);                                                              //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-DED
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded);                                                             //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-DED
                    pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded);                                                             //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-DED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(900) && pnd_Tran_Rec_Out_Pnd_Tr_Int_Code.equals(3)))                                           //Natural: IF #TR-TRAN-CODE = 900 AND #TR-INT-CODE = 3
                    {
                        pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Stop);                                                     //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-STOP
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head1_Ded);                                                          //Natural: ASSIGN #DETAIL-HEADING := #DETAIL-HEAD1-DED
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading2.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head2_Ded);                                                         //Natural: ASSIGN #DETAIL-HEADING2 := #DETAIL-HEAD2-DED
                        pnd_Prog_Misc_Area_Pnd_Detail_Heading3.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head3_Ded);                                                         //Natural: ASSIGN #DETAIL-HEADING3 := #DETAIL-HEAD3-DED
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_Line() throws Exception                                                                                                                 //Natural: PRINT-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED FOLLOWING FOR IPRO 102 PEND-CODE Q TRANS 2/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(102)))                                                                                                  //Natural: IF #SVE-TRAN-CODE = 102
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 4X #TR-XREF 4X #TR-PEND-CDE 9X #TR-FIN-PER-DTE ( EM = 9999/99 )
                new ReportEditMask ("99"),new ColumnSpacing(4),pnd_Tran_Rec_Out_Pnd_Tr_Xref,new ColumnSpacing(4),pnd_Tran_Rec_Out_Pnd_Tr_Pend_Cde,new ColumnSpacing(9),pnd_Tran_Rec_Out_Pnd_Tr_Fin_Per_Dte, 
                new ReportEditMask ("9999/99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 102 PEND-CODE Q TRANS 2/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(6)))                                                                                                    //Natural: IF #SVE-TRAN-CODE = 006
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 4X #TR-XREF
                new ReportEditMask ("99"),new ColumnSpacing(4),pnd_Tran_Rec_Out_Pnd_Tr_Xref);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(700)))                                                                                                 //Natural: IF #TR-TRAN-CODE = 700
            {
                FOR05:                                                                                                                                                    //Natural: FOR #I = 1 TO 5
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Cntrct_Company_Cde.getValue(pnd_I).notEquals(" ")))                                                             //Natural: IF #TR-CNTRCT-COMPANY-CDE ( #I ) NE ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 5X #TR-XREF 3X#TR-RCVRY-TYPE ( #I ) 3X#TR-PER-TAX-AMT ( #I ) 4X#TR-PER-TAX-FREE ( #I )
                            new ReportEditMask ("99"),new ColumnSpacing(5),pnd_Tran_Rec_Out_Pnd_Tr_Xref,new ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Rcvry_Type.getValue(pnd_I),new 
                            ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Amt.getValue(pnd_I),new ColumnSpacing(4),pnd_Tran_Rec_Out_Pnd_Tr_Per_Tax_Free.getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tran_Rec_Out_Pnd_Tr_Tran_Code.equals(900)))                                                                                             //Natural: IF #TR-TRAN-CODE = 900
                {
                    pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc.setValue(pnd_Prog_Misc_Area_Pnd_Detail_Head_Ded_Chng);                                                         //Natural: ASSIGN #TYPE-TRANS-DESC := #DETAIL-HEAD-DED-CHNG
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Tran_Rec_Out_Pnd_Tr_Ppcn_Nbr_8, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),pnd_Tran_Rec_Out_Pnd_Tr_Payee_Cde,  //Natural: WRITE ( 1 ) / #TR-PPCN-NBR-8 ( EM = XXXXXXX-X ) 3X#TR-PAYEE-CDE ( EM = 99 ) 5X#TR-XREF 1X#TR-PER-DED-AMT ( EM = ZZ,ZZZ.99- ) 2X#TR-DED-SEQ-NBR 4X#TR-DED-CODE 2X#TR-OLD-DED-AMT ( EM = ZZ,ZZZ.99- )
                        new ReportEditMask ("99"),new ColumnSpacing(5),pnd_Tran_Rec_Out_Pnd_Tr_Xref,new ColumnSpacing(1),pnd_Tran_Rec_Out_Pnd_Tr_Per_Ded_Amt, 
                        new ReportEditMask ("ZZ,ZZZ.99-"),new ColumnSpacing(2),pnd_Tran_Rec_Out_Pnd_Tr_Ded_Seq_Nbr,new ColumnSpacing(4),pnd_Tran_Rec_Out_Pnd_Tr_Ded_Code,new 
                        ColumnSpacing(2),pnd_Tran_Rec_Out_Pnd_Tr_Old_Ded_Amt, new ReportEditMask ("ZZ,ZZZ.99-"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Total_Line() throws Exception                                                                                                                  //Natural: PRINT-TOTAL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ADDED THE FOLLOWING FOR IPRO 2/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(102)))                                                                                                  //Natural: IF #SVE-TRAN-CODE = 102
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL PENDED Q & R 102 TRANS.....:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask                //Natural: WRITE ( 1 ) / 'TOTAL PENDED Q & R 102 TRANS.....:' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
                ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR IPRO 2/01
        if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(6)))                                                                                                    //Natural: IF #SVE-TRAN-CODE = 006
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TOTAL TERMINATION TRANSACTIONS...:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask                //Natural: WRITE ( 1 ) / 'TOTAL TERMINATION TRANSACTIONS...:' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
                ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(700)))                                                                                              //Natural: IF #SVE-TRAN-CODE = 700
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"        TOTAL TAX TRANSACTIONS...:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask            //Natural: WRITE ( 1 ) / '        TOTAL TAX TRANSACTIONS...:' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
                    ("ZZ,ZZ9"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(900) && pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.equals(2)))                                         //Natural: IF #SVE-TRAN-CODE = 900 AND #SVE-INT-CODE = 2
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,"       TOTAL DEDUCTION CHANGES...:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new ReportEditMask        //Natural: WRITE ( 1 ) / '       TOTAL DEDUCTION CHANGES...:' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
                        ("ZZ,ZZ9"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Prog_Misc_Area_Pnd_Sve_Tran_Code.equals(900) && pnd_Prog_Misc_Area_Pnd_Sve_Int_Code.equals(3)))                                     //Natural: IF #SVE-TRAN-CODE = 900 AND #SVE-INT-CODE = 3
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"         TOTAL STOP DEDUCTIONS...:",pnd_Prog_Misc_Area_Pnd_Total_Trans, new                   //Natural: WRITE ( 1 ) / '         TOTAL STOP DEDUCTIONS...:' #TOTAL-TRANS ( EM = ZZ,ZZ9 )
                            ReportEditMask ("ZZ,ZZ9"));
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prog_Misc_Area_Pnd_Total_Trans.reset();                                                                                                                       //Natural: RESET #TOTAL-TRANS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  ADD 1 TO #W-PAGE-CTR
                    //*  3/06 USE SYSTEM VARIABLE INSTEAD
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new ColumnSpacing(5),"IAIQ AUTO GENERATED TRANSACTIONS TO BE APPLIED",new   //Natural: WRITE ( 1 ) NOTITLE 'PROGRAM ' *PROGRAM 5X 'IAIQ AUTO GENERATED TRANSACTIONS TO BE APPLIED' 4X 'PAGE: ' *PAGE-NUMBER ( 1 )
                        ColumnSpacing(4),"PAGE: ",getReports().getPageNumberDbs(1));
                    //*    4X 'PAGE: ' #W-PAGE-CTR (EM=Z9) 3/96 REPLACE WITH *PAGE-NUMBER(1)
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE",pnd_W_Date_Out,new ColumnSpacing(9)," FOR PAYMENTS DUE ON CHECK DATE: ",pnd_Next_Check_Date_Ccyymmdd,  //Natural: WRITE ( 1 ) 'RUN DATE' #W-DATE-OUT 9X ' FOR PAYMENTS DUE ON CHECK DATE: ' #NEXT-CHECK-DATE-CCYYMMDD ( EM = 9999/99/99 ) //
                        new ReportEditMask ("9999/99/99"),NEWLINE,NEWLINE);
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Type_Trans_Desc,NEWLINE);                                                           //Natural: WRITE ( 1 ) #TYPE-TRANS-DESC /
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading);                                                                    //Natural: WRITE ( 1 ) #DETAIL-HEADING
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading2);                                                                   //Natural: WRITE ( 1 ) #DETAIL-HEADING2
                    getReports().write(1, ReportOption.NOTITLE,pnd_Prog_Misc_Area_Pnd_Detail_Heading3);                                                                   //Natural: WRITE ( 1 ) #DETAIL-HEADING3
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " ERROR IN PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                       //Natural: WRITE ' ERROR IN PROCESSING PROGRAM:' *PROGRAM
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=56");
    }
}
