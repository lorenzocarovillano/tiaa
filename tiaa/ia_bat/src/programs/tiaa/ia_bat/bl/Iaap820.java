/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:54 PM
**        * FROM NATURAL PROGRAM : Iaap820
************************************************************
**        * FILE NAME            : Iaap820.java
**        * CLASS NAME           : Iaap820
**        * INSTANCE NAME        : Iaap820
************************************************************
************************************************************************
* PROGRAM  : IAAP820
* SYSTEM   : IAA
* CREATED  : MAR 20, 97
* FUNCTION : CHANGES IN MONTHLY PREMIUMS
* TITLE    :
* HISTORY
* -------
* 01/99 DO SCAN ON 1/99 FOR CHANGES MADE TO HANDLE VOLUNTARY DEDUCTION
*         CHANGES FOR GROUP CONTRACTS
* 04/98 LB CHANGES MADE FOR MAY 98
*
* 12/08 JT CHANGES FOR 02/2009 PAYMENT
* 12/09 JT CHANGES FOR 02/2010 PAYMENT
* 01/11 JT CHANGES FOR 02/2011 PAYMENT - PUT RATES IN ADAT SO NO MORE
*          CODING CHANGE WILL BE REQUIRED FOR FUTURE RUNS.
* 01/12 JT CHANGES FOR 02/2012 PAYMENT - ADD NEW CONTRACT RANGE AND
*          EXTERNALIZE THE CONTRACT RANGES.
* 03/12 JT RATE BASES EXPANSION - RECATALOG DUE TO CHANGES IN IAAL201E
* JUN 2017 J BREMER       PIN EXPANSION      06/2017- STOW ONLY
* 04/2017  O SOTTO  RE-STOWED FOR IAAL200B, IAAL200C, IAAL202C
*                   AND IAAL202G.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap820 extends BLNatBase
{
    // Data Areas
    private LdaIaal200b ldaIaal200b;
    private LdaIaal201e ldaIaal201e;
    private LdaIaal200c ldaIaal200c;
    private LdaIaal202h ldaIaal202h;
    private LdaIaal202c ldaIaal202c;
    private LdaIaal202g ldaIaal202g;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_2;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cnt_4;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cnt_6;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_A;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_3;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_4;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code;
    private DbsField pnd_W_Cntrct_Tot;

    private DbsGroup pnd_W_Cntrct_Tot__R_Field_5;
    private DbsField pnd_W_Cntrct_Tot_Pnd_W_Cnt_4;
    private DbsField pnd_W_Cntrct_Tot_Pnd_W_Cnt_6;

    private DbsGroup pnd_W_Cntrct_Tot__R_Field_6;
    private DbsField pnd_W_Cntrct_Tot_Pnd_W_Cnt_1_5;
    private DbsField pnd_W_Cntrct_Tot_Pnd_W_Cnt_5_10;
    private DbsField pnd_Iaa_Ddctn_Key;

    private DbsGroup pnd_Iaa_Ddctn_Key__R_Field_7;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Iaa_Ddctn_Key__R_Field_8;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee;
    private DbsField pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_S_Invrse_Dte;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr;

    private DbsGroup pnd_W_Ded_Cde_Seq_Nbr__R_Field_9;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr;
    private DbsField pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code;
    private DbsField pnd_Prt_Ded_Seq_Nbr;
    private DbsField pnd_Prt_Ded_Code;
    private DbsField pnd_I;
    private DbsField pnd_Found_Ded_Amt;
    private DbsField pnd_Cnt_Ded_Read;
    private DbsField pnd_Cnt_Ded_Acpt;
    private DbsField pnd_Cnt_Found_Ded_Amt;
    private DbsField pnd_Cnt_Not_Found_Ded_Amt;
    private DbsField pnd_Cnt_Ded_200;
    private DbsField pnd_Cnt_Cpr_Read;
    private DbsField pnd_Cnt_Cpr_Read_Acpt;
    private DbsField pnd_Cnt_Read_Tiaa_Fund;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Total_Pay_Amt;
    private DbsField pnd_Cnt_Ded_Amt;
    private DbsField pnd_W_Ded_Per_Amt;
    private DbsField pnd_Prt_Old_Amt;
    private DbsField pnd_Prt_Message;
    private DbsField pnd_Prt_Pend_Cde;
    private DbsField pnd_Prt_New_Amt;
    private DbsField pnd_Tab_Old_Amt;
    private DbsField pnd_Tab_New_Amt;
    private DbsField pnd_Grand_Old_Amt;
    private DbsField pnd_Grand_New_Amt;
    private DbsField pnd_Page_Ctr;
    private DbsField pnd_Time;
    private DbsField pnd_Date_Time_P;
    private DbsField pnd_Save_Todays_Dte_A;

    private DbsGroup pnd_Save_Todays_Dte_A__R_Field_10;
    private DbsField pnd_Save_Todays_Dte_A_Pnd_Save_Todays_Dte_N;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_15;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm_A;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_16;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_17;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_18;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_19;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde;
    private DbsField pnd_Check_D;
    private DbsField pnd_Check_A;
    private DbsField pnd_Net_Diff;
    private DbsField pnd_Trans_Cmbne_Cde;

    private DbsGroup pnd_Trans_Cmbne_Cde__R_Field_20;
    private DbsField pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Seq_Nbr;
    private DbsField pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Cde;
    private DbsField pnd_Rates;
    private DbsField pnd_Rnges;
    private DbsField pnd_Rc;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Rngea;

    private DbsGroup pnd_Rngea__R_Field_21;
    private DbsField pnd_Rngea_Pnd_F_Rnge;
    private DbsField pnd_Rngea__Filler1;
    private DbsField pnd_Rngea_Pnd_L_Rnge;
    private DbsField pnd_B_Rnge;

    private DbsGroup pnd_B_Rnge__R_Field_22;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge1;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge2;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge3;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge4;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge5;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge6;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge7;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge8;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge9;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge10;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge11;
    private DbsField pnd_B_Rnge_Pnd_B_Rnge12;
    private DbsField pnd_E_Rnge;

    private DbsGroup pnd_E_Rnge__R_Field_23;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge1;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge2;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge3;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge4;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge5;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge6;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge7;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge8;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge9;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge10;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge11;
    private DbsField pnd_E_Rnge_Pnd_E_Rnge12;
    private DbsField pnd_Ratea;

    private DbsGroup pnd_Ratea__R_Field_24;
    private DbsField pnd_Ratea_Pnd_O_Rate;
    private DbsField pnd_Ratea_Pnd_O_Filler;
    private DbsField pnd_Ratea_Pnd_N_Rate;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal200b = new LdaIaal200b();
        registerRecord(ldaIaal200b);
        registerRecord(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role());
        ldaIaal201e = new LdaIaal201e();
        registerRecord(ldaIaal201e);
        registerRecord(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd());
        ldaIaal200c = new LdaIaal200c();
        registerRecord(ldaIaal200c);
        registerRecord(ldaIaal200c.getVw_iaa_Deduction());
        ldaIaal202h = new LdaIaal202h();
        registerRecord(ldaIaal202h);
        registerRecord(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1());
        ldaIaal202c = new LdaIaal202c();
        registerRecord(ldaIaal202c);
        registerRecord(ldaIaal202c.getVw_iaa_Ddctn_Trans());
        ldaIaal202g = new LdaIaal202g();
        registerRecord(ldaIaal202g);
        registerRecord(ldaIaal202g.getVw_iaa_Trans_Rcrd());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);

        pnd_Cntrct_Payee_Key__R_Field_2 = pnd_Cntrct_Payee_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Payee_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);
        pnd_Cntrct_Payee_Key_Pnd_Cnt_4 = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cnt_4", "#CNT-4", FieldType.STRING, 
            4);
        pnd_Cntrct_Payee_Key_Pnd_Cnt_6 = pnd_Cntrct_Payee_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cnt_6", "#CNT-6", FieldType.STRING, 
            6);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_A = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_A", "#CNTRCT-PAYEE-CDE-A", 
            FieldType.STRING, 2);

        pnd_Cntrct_Payee_Key__R_Field_3 = pnd_Cntrct_Payee_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Payee_Key__R_Field_3", "REDEFINE", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde_A);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_3.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_4", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr", "#W-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee", "#W-CNTRCT-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code = pnd_Cntrct_Fund_Key__R_Field_4.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code", "#W-FUND-CODE", FieldType.STRING, 
            3);
        pnd_W_Cntrct_Tot = localVariables.newFieldInRecord("pnd_W_Cntrct_Tot", "#W-CNTRCT-TOT", FieldType.STRING, 10);

        pnd_W_Cntrct_Tot__R_Field_5 = localVariables.newGroupInRecord("pnd_W_Cntrct_Tot__R_Field_5", "REDEFINE", pnd_W_Cntrct_Tot);
        pnd_W_Cntrct_Tot_Pnd_W_Cnt_4 = pnd_W_Cntrct_Tot__R_Field_5.newFieldInGroup("pnd_W_Cntrct_Tot_Pnd_W_Cnt_4", "#W-CNT-4", FieldType.STRING, 4);
        pnd_W_Cntrct_Tot_Pnd_W_Cnt_6 = pnd_W_Cntrct_Tot__R_Field_5.newFieldInGroup("pnd_W_Cntrct_Tot_Pnd_W_Cnt_6", "#W-CNT-6", FieldType.STRING, 6);

        pnd_W_Cntrct_Tot__R_Field_6 = localVariables.newGroupInRecord("pnd_W_Cntrct_Tot__R_Field_6", "REDEFINE", pnd_W_Cntrct_Tot);
        pnd_W_Cntrct_Tot_Pnd_W_Cnt_1_5 = pnd_W_Cntrct_Tot__R_Field_6.newFieldInGroup("pnd_W_Cntrct_Tot_Pnd_W_Cnt_1_5", "#W-CNT-1-5", FieldType.STRING, 
            5);
        pnd_W_Cntrct_Tot_Pnd_W_Cnt_5_10 = pnd_W_Cntrct_Tot__R_Field_6.newFieldInGroup("pnd_W_Cntrct_Tot_Pnd_W_Cnt_5_10", "#W-CNT-5-10", FieldType.STRING, 
            5);
        pnd_Iaa_Ddctn_Key = localVariables.newFieldInRecord("pnd_Iaa_Ddctn_Key", "#IAA-DDCTN-KEY", FieldType.STRING, 18);

        pnd_Iaa_Ddctn_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Iaa_Ddctn_Key__R_Field_7", "REDEFINE", pnd_Iaa_Ddctn_Key);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr", "#DDCTN-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde", "#DDCTN-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde = pnd_Iaa_Ddctn_Key__R_Field_7.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 
            3);

        pnd_Iaa_Ddctn_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Iaa_Ddctn_Key__R_Field_8", "REDEFINE", pnd_Iaa_Ddctn_Key);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee = pnd_Iaa_Ddctn_Key__R_Field_8.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr_Payee", "#DDCTN-PPCN-NBR-PAYEE", 
            FieldType.STRING, 12);
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key = pnd_Iaa_Ddctn_Key__R_Field_8.newFieldInGroup("pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Rest_Of_Key", "#DDCTN-REST-OF-KEY", 
            FieldType.STRING, 6);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_S_Invrse_Dte = localVariables.newFieldInRecord("pnd_S_Invrse_Dte", "#S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_W_Ded_Cde_Seq_Nbr = localVariables.newFieldInRecord("pnd_W_Ded_Cde_Seq_Nbr", "#W-DED-CDE-SEQ-NBR", FieldType.STRING, 6);

        pnd_W_Ded_Cde_Seq_Nbr__R_Field_9 = localVariables.newGroupInRecord("pnd_W_Ded_Cde_Seq_Nbr__R_Field_9", "REDEFINE", pnd_W_Ded_Cde_Seq_Nbr);
        pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr = pnd_W_Ded_Cde_Seq_Nbr__R_Field_9.newFieldInGroup("pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Seq_Nbr", "#W-DED-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code = pnd_W_Ded_Cde_Seq_Nbr__R_Field_9.newFieldInGroup("pnd_W_Ded_Cde_Seq_Nbr_Pnd_W_Ded_Code", "#W-DED-CODE", 
            FieldType.STRING, 3);
        pnd_Prt_Ded_Seq_Nbr = localVariables.newFieldInRecord("pnd_Prt_Ded_Seq_Nbr", "#PRT-DED-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Prt_Ded_Code = localVariables.newFieldInRecord("pnd_Prt_Ded_Code", "#PRT-DED-CODE", FieldType.STRING, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Found_Ded_Amt = localVariables.newFieldInRecord("pnd_Found_Ded_Amt", "#FOUND-DED-AMT", FieldType.STRING, 1);
        pnd_Cnt_Ded_Read = localVariables.newFieldInRecord("pnd_Cnt_Ded_Read", "#CNT-DED-READ", FieldType.NUMERIC, 6);
        pnd_Cnt_Ded_Acpt = localVariables.newFieldInRecord("pnd_Cnt_Ded_Acpt", "#CNT-DED-ACPT", FieldType.NUMERIC, 6);
        pnd_Cnt_Found_Ded_Amt = localVariables.newFieldInRecord("pnd_Cnt_Found_Ded_Amt", "#CNT-FOUND-DED-AMT", FieldType.NUMERIC, 6);
        pnd_Cnt_Not_Found_Ded_Amt = localVariables.newFieldInRecord("pnd_Cnt_Not_Found_Ded_Amt", "#CNT-NOT-FOUND-DED-AMT", FieldType.NUMERIC, 6);
        pnd_Cnt_Ded_200 = localVariables.newFieldInRecord("pnd_Cnt_Ded_200", "#CNT-DED-200", FieldType.NUMERIC, 6);
        pnd_Cnt_Cpr_Read = localVariables.newFieldInRecord("pnd_Cnt_Cpr_Read", "#CNT-CPR-READ", FieldType.NUMERIC, 6);
        pnd_Cnt_Cpr_Read_Acpt = localVariables.newFieldInRecord("pnd_Cnt_Cpr_Read_Acpt", "#CNT-CPR-READ-ACPT", FieldType.NUMERIC, 6);
        pnd_Cnt_Read_Tiaa_Fund = localVariables.newFieldInRecord("pnd_Cnt_Read_Tiaa_Fund", "#CNT-READ-TIAA-FUND", FieldType.NUMERIC, 6);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 6);
        pnd_Total_Pay_Amt = localVariables.newFieldInRecord("pnd_Total_Pay_Amt", "#TOTAL-PAY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cnt_Ded_Amt = localVariables.newFieldInRecord("pnd_Cnt_Ded_Amt", "#CNT-DED-AMT", FieldType.NUMERIC, 7, 2);
        pnd_W_Ded_Per_Amt = localVariables.newFieldInRecord("pnd_W_Ded_Per_Amt", "#W-DED-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Prt_Old_Amt = localVariables.newFieldInRecord("pnd_Prt_Old_Amt", "#PRT-OLD-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Prt_Message = localVariables.newFieldInRecord("pnd_Prt_Message", "#PRT-MESSAGE", FieldType.STRING, 34);
        pnd_Prt_Pend_Cde = localVariables.newFieldInRecord("pnd_Prt_Pend_Cde", "#PRT-PEND-CDE", FieldType.STRING, 1);
        pnd_Prt_New_Amt = localVariables.newFieldInRecord("pnd_Prt_New_Amt", "#PRT-NEW-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Tab_Old_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_Old_Amt", "#TAB-OLD-AMT", FieldType.NUMERIC, 7, 2, new DbsArrayController(1, 15));
        pnd_Tab_New_Amt = localVariables.newFieldArrayInRecord("pnd_Tab_New_Amt", "#TAB-NEW-AMT", FieldType.NUMERIC, 7, 2, new DbsArrayController(1, 15));
        pnd_Grand_Old_Amt = localVariables.newFieldInRecord("pnd_Grand_Old_Amt", "#GRAND-OLD-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Grand_New_Amt = localVariables.newFieldInRecord("pnd_Grand_New_Amt", "#GRAND-NEW-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Page_Ctr = localVariables.newFieldInRecord("pnd_Page_Ctr", "#PAGE-CTR", FieldType.NUMERIC, 4);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Date_Time_P = localVariables.newFieldInRecord("pnd_Date_Time_P", "#DATE-TIME-P", FieldType.PACKED_DECIMAL, 12);
        pnd_Save_Todays_Dte_A = localVariables.newFieldInRecord("pnd_Save_Todays_Dte_A", "#SAVE-TODAYS-DTE-A", FieldType.STRING, 8);

        pnd_Save_Todays_Dte_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Save_Todays_Dte_A__R_Field_10", "REDEFINE", pnd_Save_Todays_Dte_A);
        pnd_Save_Todays_Dte_A_Pnd_Save_Todays_Dte_N = pnd_Save_Todays_Dte_A__R_Field_10.newFieldInGroup("pnd_Save_Todays_Dte_A_Pnd_Save_Todays_Dte_N", 
            "#SAVE-TODAYS-DTE-N", FieldType.NUMERIC, 8);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_13 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_13.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yy);
        pnd_Save_Check_Dte_A_Pnd_Yy_A = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy_A", "#YY-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_15 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_15", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Mm);
        pnd_Save_Check_Dte_A_Pnd_Mm_A = pnd_Save_Check_Dte_A__R_Field_15.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm_A", "#MM-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_16 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_16", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Dd);
        pnd_Save_Check_Dte_A_Pnd_Dd_A = pnd_Save_Check_Dte_A__R_Field_16.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd_A", "#DD-A", FieldType.STRING, 2);

        pnd_Save_Check_Dte_A__R_Field_17 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_17", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_17.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_18 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_18", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_18.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_18.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_19 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_19", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_19.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", 
            "#TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Chck_Dte_Key__R_Field_19.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Chck_Dte_Key__R_Field_19.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Payee_Cde", 
            "#TRANS-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde = pnd_Trans_Chck_Dte_Key__R_Field_19.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Cde", "#TRANS-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Check_D = localVariables.newFieldInRecord("pnd_Check_D", "#CHECK-D", FieldType.DATE);
        pnd_Check_A = localVariables.newFieldInRecord("pnd_Check_A", "#CHECK-A", FieldType.STRING, 8);
        pnd_Net_Diff = localVariables.newFieldInRecord("pnd_Net_Diff", "#NET-DIFF", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Trans_Cmbne_Cde = localVariables.newFieldInRecord("pnd_Trans_Cmbne_Cde", "#TRANS-CMBNE-CDE", FieldType.STRING, 12);

        pnd_Trans_Cmbne_Cde__R_Field_20 = localVariables.newGroupInRecord("pnd_Trans_Cmbne_Cde__R_Field_20", "REDEFINE", pnd_Trans_Cmbne_Cde);
        pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Seq_Nbr = pnd_Trans_Cmbne_Cde__R_Field_20.newFieldInGroup("pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Seq_Nbr", "#TR-DDCTN-SEQ-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Cde = pnd_Trans_Cmbne_Cde__R_Field_20.newFieldInGroup("pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Cde", "#TR-DDCTN-CDE", 
            FieldType.STRING, 3);
        pnd_Rates = localVariables.newFieldArrayInRecord("pnd_Rates", "#RATES", FieldType.STRING, 13, new DbsArrayController(1, 24));
        pnd_Rnges = localVariables.newFieldArrayInRecord("pnd_Rnges", "#RNGES", FieldType.STRING, 17, new DbsArrayController(1, 12));
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Rngea = localVariables.newFieldInRecord("pnd_Rngea", "#RNGEA", FieldType.STRING, 17);

        pnd_Rngea__R_Field_21 = localVariables.newGroupInRecord("pnd_Rngea__R_Field_21", "REDEFINE", pnd_Rngea);
        pnd_Rngea_Pnd_F_Rnge = pnd_Rngea__R_Field_21.newFieldInGroup("pnd_Rngea_Pnd_F_Rnge", "#F-RNGE", FieldType.STRING, 8);
        pnd_Rngea__Filler1 = pnd_Rngea__R_Field_21.newFieldInGroup("pnd_Rngea__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Rngea_Pnd_L_Rnge = pnd_Rngea__R_Field_21.newFieldInGroup("pnd_Rngea_Pnd_L_Rnge", "#L-RNGE", FieldType.STRING, 8);
        pnd_B_Rnge = localVariables.newFieldArrayInRecord("pnd_B_Rnge", "#B-RNGE", FieldType.STRING, 8, new DbsArrayController(1, 12));

        pnd_B_Rnge__R_Field_22 = localVariables.newGroupInRecord("pnd_B_Rnge__R_Field_22", "REDEFINE", pnd_B_Rnge);
        pnd_B_Rnge_Pnd_B_Rnge1 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge1", "#B-RNGE1", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge2 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge2", "#B-RNGE2", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge3 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge3", "#B-RNGE3", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge4 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge4", "#B-RNGE4", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge5 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge5", "#B-RNGE5", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge6 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge6", "#B-RNGE6", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge7 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge7", "#B-RNGE7", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge8 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge8", "#B-RNGE8", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge9 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge9", "#B-RNGE9", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge10 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge10", "#B-RNGE10", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge11 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge11", "#B-RNGE11", FieldType.STRING, 8);
        pnd_B_Rnge_Pnd_B_Rnge12 = pnd_B_Rnge__R_Field_22.newFieldInGroup("pnd_B_Rnge_Pnd_B_Rnge12", "#B-RNGE12", FieldType.STRING, 8);
        pnd_E_Rnge = localVariables.newFieldArrayInRecord("pnd_E_Rnge", "#E-RNGE", FieldType.STRING, 8, new DbsArrayController(1, 12));

        pnd_E_Rnge__R_Field_23 = localVariables.newGroupInRecord("pnd_E_Rnge__R_Field_23", "REDEFINE", pnd_E_Rnge);
        pnd_E_Rnge_Pnd_E_Rnge1 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge1", "#E-RNGE1", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge2 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge2", "#E-RNGE2", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge3 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge3", "#E-RNGE3", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge4 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge4", "#E-RNGE4", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge5 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge5", "#E-RNGE5", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge6 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge6", "#E-RNGE6", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge7 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge7", "#E-RNGE7", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge8 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge8", "#E-RNGE8", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge9 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge9", "#E-RNGE9", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge10 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge10", "#E-RNGE10", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge11 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge11", "#E-RNGE11", FieldType.STRING, 8);
        pnd_E_Rnge_Pnd_E_Rnge12 = pnd_E_Rnge__R_Field_23.newFieldInGroup("pnd_E_Rnge_Pnd_E_Rnge12", "#E-RNGE12", FieldType.STRING, 8);
        pnd_Ratea = localVariables.newFieldInRecord("pnd_Ratea", "#RATEA", FieldType.STRING, 13);

        pnd_Ratea__R_Field_24 = localVariables.newGroupInRecord("pnd_Ratea__R_Field_24", "REDEFINE", pnd_Ratea);
        pnd_Ratea_Pnd_O_Rate = pnd_Ratea__R_Field_24.newFieldInGroup("pnd_Ratea_Pnd_O_Rate", "#O-RATE", FieldType.NUMERIC, 6, 2);
        pnd_Ratea_Pnd_O_Filler = pnd_Ratea__R_Field_24.newFieldInGroup("pnd_Ratea_Pnd_O_Filler", "#O-FILLER", FieldType.STRING, 1);
        pnd_Ratea_Pnd_N_Rate = pnd_Ratea__R_Field_24.newFieldInGroup("pnd_Ratea_Pnd_N_Rate", "#N-RATE", FieldType.NUMERIC, 6, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200b.initializeValues();
        ldaIaal201e.initializeValues();
        ldaIaal200c.initializeValues();
        ldaIaal202h.initializeValues();
        ldaIaal202c.initializeValues();
        ldaIaal202g.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap820() throws Exception
    {
        super("Iaap820");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "**********************************",NEWLINE,"  START OF IAAP820 REPORT PROGRAM",NEWLINE,"**********************************");             //Natural: WRITE '**********************************' / '  START OF IAAP820 REPORT PROGRAM'/ '**********************************'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #FILL-TABLES
        sub_Pnd_Fill_Tables();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #DATES-AND-TIMES
        sub_Pnd_Dates_And_Times();
        if (condition(Global.isEscape())) {return;}
        pnd_Cntrct_Payee_Key_Pnd_Cnt_4.setValue("W070");                                                                                                                  //Natural: ASSIGN #CNT-4 := 'W070'
        pnd_Cntrct_Payee_Key_Pnd_Cnt_6.setValue(" ");                                                                                                                     //Natural: ASSIGN #CNT-6 := ' '
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(0);                                                                                                            //Natural: ASSIGN #CNTRCT-PAYEE-CDE := 0
        getReports().write(0, pnd_Cntrct_Payee_Key);                                                                                                                      //Natural: WRITE #CNTRCT-PAYEE-KEY
        if (Global.isEscape()) return;
        ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseRead                                                                                                     //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        (
        "R1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Cntrct_Payee_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        R1:
        while (condition(ldaIaal200b.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("R1")))
        {
            pnd_Cnt_Cpr_Read.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CNT-CPR-READ
            pnd_W_Cntrct_Tot.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr());                                                                     //Natural: MOVE CNTRCT-PART-PPCN-NBR TO #W-CNTRCT-TOT #CNTRCT-PPCN-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr());
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde());                                           //Natural: MOVE CNTRCT-PART-PAYEE-CDE TO #CNTRCT-PAYEE-CDE
            //*  IF #W-CNT-1-5 GT 'W0753' /* 1/99
            //*  IF #W-CNT-1-5 GT 'W0754' /* 1/03
            //*  1/06
            if (condition(pnd_W_Cntrct_Tot_Pnd_W_Cnt_1_5.greater("W0759")))                                                                                               //Natural: IF #W-CNT-1-5 GT 'W0759'
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  1/99
            if (condition(pnd_W_Cntrct_Tot_Pnd_W_Cnt_4.less("W070")))                                                                                                     //Natural: IF #W-CNT-4 LT 'W070'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  IF CNTRCT-PART-PPCN-NBR = 'W0700000' THRU 'W0739999' OR /* 1/12
            //*     CNTRCT-PART-PPCN-NBR = 'W0750000' THRU 'W0759999' /* 1/12
            //*     CNTRCT-PART-PPCN-NBR = 'W0741000' THRU 'W0759999' /* 1/12
            //*    IGNORE
            //*  ELSE
            //*    ESCAPE TOP
            //*  END-IF                                               /* 1/12
            //*  1/12 START
            if (condition(((((((((((((ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge1) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge1))  //Natural: IF CNTRCT-PART-PPCN-NBR = #B-RNGE1 THRU #E-RNGE1 OR CNTRCT-PART-PPCN-NBR = #B-RNGE2 THRU #E-RNGE2 OR CNTRCT-PART-PPCN-NBR = #B-RNGE3 THRU #E-RNGE3 OR CNTRCT-PART-PPCN-NBR = #B-RNGE4 THRU #E-RNGE4 OR CNTRCT-PART-PPCN-NBR = #B-RNGE5 THRU #E-RNGE5 OR CNTRCT-PART-PPCN-NBR = #B-RNGE6 THRU #E-RNGE6 OR CNTRCT-PART-PPCN-NBR = #B-RNGE7 THRU #E-RNGE7 OR CNTRCT-PART-PPCN-NBR = #B-RNGE8 THRU #E-RNGE8 OR CNTRCT-PART-PPCN-NBR = #B-RNGE9 THRU #E-RNGE9 OR CNTRCT-PART-PPCN-NBR = #B-RNGE10 THRU #E-RNGE10 OR CNTRCT-PART-PPCN-NBR = #B-RNGE11 THRU #E-RNGE11 OR CNTRCT-PART-PPCN-NBR = #B-RNGE12 THRU #E-RNGE12
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge2) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge2))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge3) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge3))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge4) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge4))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge5) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge5))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge6) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge6))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge7) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge7))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge8) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge8))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge9) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge9))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge10) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge10))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge11) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge11))) 
                || (ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().greaterOrEqual(pnd_B_Rnge_Pnd_B_Rnge12) && ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().lessOrEqual(pnd_E_Rnge_Pnd_E_Rnge12)))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  1/12 END
            }                                                                                                                                                             //Natural: END-IF
            //*  1/03
            if (condition(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().equals("W0718087") || ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().equals("W0750460"))) //Natural: IF CNTRCT-PART-PPCN-NBR = 'W0718087' OR = 'W0750460'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().notEquals(9))))                                                                    //Natural: ACCEPT IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE NE 9
            {
                continue;
            }
            pnd_Cnt_Cpr_Read_Acpt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-CPR-READ-ACPT
            pnd_Prt_Pend_Cde.setValue(ldaIaal200b.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde());                                                                          //Natural: MOVE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE TO #PRT-PEND-CDE
                                                                                                                                                                          //Natural: PERFORM #READ-TIAA-FUND
            sub_Pnd_Read_Tiaa_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #READ-DEDUCTION
            sub_Pnd_Read_Deduction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"GRAND OLD AMOUNT ==> ",pnd_Grand_Old_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),NEWLINE,new  //Natural: WRITE ( 1 ) / 010T 'GRAND OLD AMOUNT ==> ' #GRAND-OLD-AMT ( EM = Z,ZZZ,ZZ9.99 ) / 010T 'GRAND NEW AMOUNT ==> ' #GRAND-NEW-AMT ( EM = Z,ZZZ,ZZ9.99 )
            TabSetting(10),"GRAND NEW AMOUNT ==> ",pnd_Grand_New_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(0, "CPR RECORDS READ ==============>",pnd_Cnt_Cpr_Read);                                                                                       //Natural: WRITE 'CPR RECORDS READ ==============>' #CNT-CPR-READ
        if (Global.isEscape()) return;
        getReports().write(0, "CPR RECORDS ACCEPTED ==========>",pnd_Cnt_Cpr_Read_Acpt);                                                                                  //Natural: WRITE 'CPR RECORDS ACCEPTED ==========>' #CNT-CPR-READ-ACPT
        if (Global.isEscape()) return;
        getReports().write(0, "FUND RECORDS READ =============>",pnd_Cnt_Read_Tiaa_Fund);                                                                                 //Natural: WRITE 'FUND RECORDS READ =============>' #CNT-READ-TIAA-FUND
        if (Global.isEscape()) return;
        getReports().write(0, "DED RECORDS READ ==============>",pnd_Cnt_Ded_Read);                                                                                       //Natural: WRITE 'DED RECORDS READ ==============>' #CNT-DED-READ
        if (Global.isEscape()) return;
        //*  CHANGE HARD CODED DATE IN FOLLOWING IF NEEDED  1/99
        //*  12/08 JT
        getReports().write(0, "DED RECORDS > 20090201 OR 0 == >",pnd_Cnt_Ded_Acpt);                                                                                       //Natural: WRITE 'DED RECORDS > 20090201 OR 0 == >' #CNT-DED-ACPT
        if (Global.isEscape()) return;
        getReports().write(0, "DED (200) =====================>",pnd_Cnt_Ded_200);                                                                                        //Natural: WRITE 'DED (200) =====================>' #CNT-DED-200
        if (Global.isEscape()) return;
        getReports().write(0, "DEDUCTIONS AMT FOUND ==========>",pnd_Cnt_Found_Ded_Amt);                                                                                  //Natural: WRITE 'DEDUCTIONS AMT FOUND ==========>' #CNT-FOUND-DED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "DEDUCTIONS AMT NOT FOUND ======>",pnd_Cnt_Not_Found_Ded_Amt);                                                                              //Natural: WRITE 'DEDUCTIONS AMT NOT FOUND ======>' #CNT-NOT-FOUND-DED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "UPDATED DEDUCTION RECORDS =====>",pnd_Et_Count);                                                                                           //Natural: WRITE 'UPDATED DEDUCTION RECORDS =====>' #ET-COUNT
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));                  //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-TIAA-FUND
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-DEDUCTION
        //* ******************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHANGE-DEDUCTION
        //*  MADE CHANGES IN FOLLOWING TABLES   1/99
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #FILL-TABLES
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-DETAIL-LINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-BEFORE-IMAGE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-AFTER-IMAGE
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TRANSACTION
        //* **********************************************************************
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #DATES-AND-TIMES
        //* ******************************************************************
    }
    private void sub_Pnd_Read_Tiaa_Fund() throws Exception                                                                                                                //Natural: #READ-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);                                                                     //Natural: ASSIGN #W-CNTRCT-PPCN-NBR = #CNTRCT-PPCN-NBR
        pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde);                                                                       //Natural: ASSIGN #W-CNTRCT-PAYEE = #CNTRCT-PAYEE-CDE
        pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code.setValue("T1 ");                                                                                                              //Natural: ASSIGN #W-FUND-CODE = 'T1 '
        ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                          //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R1B",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R1B:
        while (condition(ldaIaal201e.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R1B")))
        {
            if (condition(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Ppcn_Nbr) && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Cntrct_Payee)  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR = #W-CNTRCT-PPCN-NBR AND IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE = #W-CNTRCT-PAYEE AND IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE = #W-FUND-CODE
                && ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().equals(pnd_Cntrct_Fund_Key_Pnd_W_Fund_Code)))
            {
                pnd_Cnt_Read_Tiaa_Fund.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNT-READ-TIAA-FUND
                pnd_Total_Pay_Amt.compute(new ComputeParameters(false, pnd_Total_Pay_Amt), ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().add(ldaIaal201e.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt())); //Natural: COMPUTE #TOTAL-PAY-AMT = IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT + IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                //*          WRITE '=' #TOTAL-PAY-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R1B;                                                                                                                                      //Natural: ESCAPE BOTTOM ( R1B. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_Deduction() throws Exception                                                                                                                //Natural: #READ-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr);                                                                          //Natural: ASSIGN #DDCTN-PPCN-NBR := #CNTRCT-PPCN-NBR
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde.setValue(pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde);                                                                        //Natural: ASSIGN #DDCTN-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Seq_Nbr.setValue(0);                                                                                                                  //Natural: ASSIGN #DDCTN-SEQ-NBR := 0
        pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Cde.setValue(" ");                                                                                                                    //Natural: ASSIGN #DDCTN-CDE := ' '
        pnd_Cnt_Ded_Amt.reset();                                                                                                                                          //Natural: RESET #CNT-DED-AMT #PRT-NEW-AMT #PRT-OLD-AMT
        pnd_Prt_New_Amt.reset();
        pnd_Prt_Old_Amt.reset();
        ldaIaal200c.getVw_iaa_Deduction().startDatabaseRead                                                                                                               //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #IAA-DDCTN-KEY
        (
        "R3",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Iaa_Ddctn_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        R3:
        while (condition(ldaIaal200c.getVw_iaa_Deduction().readNextRow("R3")))
        {
            if (condition(ldaIaal200c.getIaa_Deduction_Ddctn_Ppcn_Nbr().equals(pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr) && ldaIaal200c.getIaa_Deduction_Ddctn_Payee_Cde().equals(pnd_Iaa_Ddctn_Key_Pnd_Ddctn_Payee_Cde))) //Natural: IF IAA-DEDUCTION.DDCTN-PPCN-NBR = #DDCTN-PPCN-NBR AND IAA-DEDUCTION.DDCTN-PAYEE-CDE = #DDCTN-PAYEE-CDE
            {
                pnd_Cnt_Ded_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CNT-DED-READ
                //*  FOLLOWING ACCEPT LINE MAY HAVE TO CHANGE      1/99
                //*  12/06
                if (condition(!(ldaIaal200c.getIaa_Deduction_Ddctn_Stp_Dte().greater(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte) || ldaIaal200c.getIaa_Deduction_Ddctn_Stp_Dte().equals(getZero())))) //Natural: ACCEPT IF DDCTN-STP-DTE > #SAVE-CHECK-DTE OR DDCTN-STP-DTE = 0
                {
                    continue;
                }
                pnd_Cnt_Ded_Acpt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CNT-DED-ACPT
                pnd_Cnt_Ded_Amt.reset();                                                                                                                                  //Natural: RESET #CNT-DED-AMT
                if (condition(ldaIaal200c.getIaa_Deduction_Ddctn_Cde().equals("200") || ldaIaal200c.getIaa_Deduction_Ddctn_Cde().equals("300")))                          //Natural: IF DDCTN-CDE = '200' OR DDCTN-CDE = '300'
                {
                    pnd_Cnt_Ded_200.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-DED-200
                                                                                                                                                                          //Natural: PERFORM #CHANGE-DEDUCTION
                    sub_Pnd_Change_Deduction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Found_Ded_Amt.equals("Y")))                                                                                                         //Natural: IF #FOUND-DED-AMT = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM #WRITE-BEFORE-IMAGE
                        sub_Pnd_Write_Before_Image();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM #WRITE-TRANSACTION
                        sub_Pnd_Write_Transaction();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt().setValue(pnd_Tab_New_Amt.getValue(pnd_I));                                                           //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PER-AMT := #TAB-NEW-AMT ( #I )
                        ldaIaal200c.getIaa_Deduction_Lst_Trans_Dte().setValue(pnd_Time);                                                                                  //Natural: ASSIGN IAA-DEDUCTION.LST-TRANS-DTE := #TIME
                        ldaIaal200c.getVw_iaa_Deduction().updateDBRow("R3");                                                                                              //Natural: UPDATE
                                                                                                                                                                          //Natural: PERFORM #WRITE-AFTER-IMAGE
                        sub_Pnd_Write_After_Image();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Et_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prt_New_Amt.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                               //Natural: MOVE DDCTN-PER-AMT TO #PRT-NEW-AMT #PRT-OLD-AMT
                    pnd_Prt_Old_Amt.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());
                    pnd_Cnt_Ded_Amt.nadd(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                   //Natural: ADD DDCTN-PER-AMT TO #CNT-DED-AMT
                    pnd_Grand_New_Amt.nadd(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                 //Natural: ADD DDCTN-PER-AMT TO #GRAND-NEW-AMT
                    pnd_Grand_Old_Amt.nadd(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                 //Natural: ADD DDCTN-PER-AMT TO #GRAND-OLD-AMT
                    pnd_Prt_Message.setValue("DEDUCTION OTHER THAN 200 OR 300");                                                                                          //Natural: MOVE 'DEDUCTION OTHER THAN 200 OR 300' TO #PRT-MESSAGE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cnt_Ded_Amt.greater(pnd_Total_Pay_Amt)))                                                                                                //Natural: IF #CNT-DED-AMT > #TOTAL-PAY-AMT
                {
                    //*  NO DEDUCTION CHANGE WAS MADE
                    pnd_Prt_Message.setValue("DEDUCTION EXCEEDS PAYMENT     ");                                                                                           //Natural: MOVE 'DEDUCTION EXCEEDS PAYMENT     ' TO #PRT-MESSAGE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prt_Ded_Seq_Nbr.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Seq_Nbr());                                                                               //Natural: ASSIGN #PRT-DED-SEQ-NBR := DDCTN-SEQ-NBR
                pnd_Prt_Ded_Code.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Cde());                                                                                      //Natural: ASSIGN #PRT-DED-CODE := DDCTN-CDE
                                                                                                                                                                          //Natural: PERFORM #WRITE-DETAIL-LINE
                sub_Pnd_Write_Detail_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) break R3;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R3. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Change_Deduction() throws Exception                                                                                                              //Natural: #CHANGE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        pnd_Found_Ded_Amt.reset();                                                                                                                                        //Natural: RESET #FOUND-DED-AMT
        pnd_W_Ded_Per_Amt.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                         //Natural: MOVE IAA-DEDUCTION.DDCTN-PER-AMT TO #W-DED-PER-AMT
        FR:                                                                                                                                                               //Natural: FOR #I = 1 TO 15
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(15)); pnd_I.nadd(1))
        {
            //*  LB 04/98
            if (condition(pnd_W_Ded_Per_Amt.equals(pnd_Tab_Old_Amt.getValue(pnd_I)) && pnd_Tab_New_Amt.getValue(pnd_I).lessOrEqual(pnd_Total_Pay_Amt)))                   //Natural: IF #W-DED-PER-AMT = #TAB-OLD-AMT ( #I ) AND #TAB-NEW-AMT ( #I ) LE #TOTAL-PAY-AMT
            {
                pnd_Prt_Old_Amt.setValue(pnd_W_Ded_Per_Amt);                                                                                                              //Natural: MOVE #W-DED-PER-AMT TO #PRT-OLD-AMT
                pnd_Prt_New_Amt.setValue(pnd_Tab_New_Amt.getValue(pnd_I));                                                                                                //Natural: MOVE #TAB-NEW-AMT ( #I ) TO #PRT-NEW-AMT
                pnd_Cnt_Ded_Amt.nadd(pnd_Tab_New_Amt.getValue(pnd_I));                                                                                                    //Natural: ADD #TAB-NEW-AMT ( #I ) TO #CNT-DED-AMT
                pnd_Grand_Old_Amt.nadd(pnd_W_Ded_Per_Amt);                                                                                                                //Natural: ADD #W-DED-PER-AMT TO #GRAND-OLD-AMT
                pnd_Grand_New_Amt.nadd(pnd_Tab_New_Amt.getValue(pnd_I));                                                                                                  //Natural: ADD #TAB-NEW-AMT ( #I ) TO #GRAND-NEW-AMT
                pnd_Found_Ded_Amt.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #FOUND-DED-AMT
                pnd_Cnt_Found_Ded_Amt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-FOUND-DED-AMT
                if (true) break FR;                                                                                                                                       //Natural: ESCAPE BOTTOM ( FR. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Found_Ded_Amt.equals("Y")))                                                                                                                     //Natural: IF #FOUND-DED-AMT = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prt_New_Amt.setValue(pnd_W_Ded_Per_Amt);                                                                                                                  //Natural: MOVE #W-DED-PER-AMT TO #PRT-NEW-AMT #PRT-OLD-AMT
            pnd_Prt_Old_Amt.setValue(pnd_W_Ded_Per_Amt);
            pnd_Cnt_Ded_Amt.nadd(pnd_W_Ded_Per_Amt);                                                                                                                      //Natural: ADD #W-DED-PER-AMT TO #CNT-DED-AMT
            pnd_Grand_New_Amt.nadd(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                         //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #GRAND-NEW-AMT
            pnd_Grand_Old_Amt.nadd(ldaIaal200c.getIaa_Deduction_Ddctn_Per_Amt());                                                                                         //Natural: ADD IAA-DEDUCTION.DDCTN-PER-AMT TO #GRAND-OLD-AMT
            pnd_Cnt_Not_Found_Ded_Amt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-NOT-FOUND-DED-AMT
            pnd_Prt_Message.setValue("NO CHANGE IN DEDUCTION AMT");                                                                                                       //Natural: MOVE 'NO CHANGE IN DEDUCTION AMT' TO #PRT-MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
        //*  #CHANGE-DEDUCTION
    }
    private void sub_Pnd_Fill_Tables() throws Exception                                                                                                                   //Natural: #FILL-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        //*  01/11 START
        DbsUtil.callnat(Iaan820r.class , getCurrentProcessState(), pnd_Rates.getValue("*"), pnd_Rc);                                                                      //Natural: CALLNAT 'IAAN820R' #RATES ( * ) #RC
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Rc.notEquals(getZero())))                                                                                                                       //Natural: IF #RC NE 0
        {
            getReports().write(0, "No rates entered -- job will terminate");                                                                                              //Natural: WRITE 'No rates entered -- job will terminate'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tab_Old_Amt.getValue("*").reset();                                                                                                                            //Natural: RESET #TAB-OLD-AMT ( * ) #TAB-NEW-AMT ( * )
        pnd_Tab_New_Amt.getValue("*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #J 1 TO 15
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(15)); pnd_J.nadd(1))
        {
            if (condition(pnd_Rates.getValue(pnd_J).notEquals(" ")))                                                                                                      //Natural: IF #RATES ( #J ) NE ' '
            {
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
                pnd_Ratea.setValue(pnd_Rates.getValue(pnd_J));                                                                                                            //Natural: ASSIGN #RATEA := #RATES ( #J )
                pnd_Tab_Old_Amt.getValue(pnd_K).setValue(pnd_Ratea_Pnd_O_Rate);                                                                                           //Natural: ASSIGN #TAB-OLD-AMT ( #K ) := #O-RATE
                pnd_Tab_New_Amt.getValue(pnd_K).setValue(pnd_Ratea_Pnd_N_Rate);                                                                                           //Natural: ASSIGN #TAB-NEW-AMT ( #K ) := #N-RATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  01/12 START
        DbsUtil.callnat(Iaan820c.class , getCurrentProcessState(), pnd_Rnges.getValue("*"), pnd_Rc);                                                                      //Natural: CALLNAT 'IAAN820C' #RNGES ( * ) #RC
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Rc.notEquals(getZero())))                                                                                                                       //Natural: IF #RC NE 0
        {
            getReports().write(0, "No range entered -- job will terminate");                                                                                              //Natural: WRITE 'No range entered -- job will terminate'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        FOR02:                                                                                                                                                            //Natural: FOR #J 1 TO 12
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(12)); pnd_J.nadd(1))
        {
            if (condition(pnd_Rnges.getValue(pnd_J).notEquals(" ")))                                                                                                      //Natural: IF #RNGES ( #J ) NE ' '
            {
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
                pnd_Rngea.setValue(pnd_Rnges.getValue(pnd_J));                                                                                                            //Natural: ASSIGN #RNGEA := #RNGES ( #J )
                pnd_B_Rnge.getValue(pnd_K).setValue(pnd_Rngea_Pnd_F_Rnge);                                                                                                //Natural: ASSIGN #B-RNGE ( #K ) := #F-RNGE
                pnd_E_Rnge.getValue(pnd_K).setValue(pnd_Rngea_Pnd_L_Rnge);                                                                                                //Natural: ASSIGN #E-RNGE ( #K ) := #L-RNGE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  01/12 END
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  BYPASS HARDCODING
        //*                                      /*    2010  2011     12/08 JT
        //*     $389  $390
        pnd_Tab_Old_Amt.getValue(1).setValue(389);                                                                                                                        //Natural: MOVE 00389.00 TO #TAB-OLD-AMT ( 1 )
        pnd_Tab_New_Amt.getValue(1).setValue(390);                                                                                                                        //Natural: MOVE 00390.00 TO #TAB-NEW-AMT ( 1 )
        //*     $152  $153
        pnd_Tab_Old_Amt.getValue(2).setValue(152);                                                                                                                        //Natural: MOVE 00152.00 TO #TAB-OLD-AMT ( 2 )
        pnd_Tab_New_Amt.getValue(2).setValue(153);                                                                                                                        //Natural: MOVE 00153.00 TO #TAB-NEW-AMT ( 2 )
        //*     $541  $543
        pnd_Tab_Old_Amt.getValue(3).setValue(541);                                                                                                                        //Natural: MOVE 00541.00 TO #TAB-OLD-AMT ( 3 )
        pnd_Tab_New_Amt.getValue(3).setValue(543);                                                                                                                        //Natural: MOVE 00543.00 TO #TAB-NEW-AMT ( 3 )
        //*     $777  $780
        pnd_Tab_Old_Amt.getValue(4).setValue(777);                                                                                                                        //Natural: MOVE 00777.00 TO #TAB-OLD-AMT ( 4 )
        pnd_Tab_New_Amt.getValue(4).setValue(780);                                                                                                                        //Natural: MOVE 00780.00 TO #TAB-NEW-AMT ( 4 )
        //*     $304  $306
        pnd_Tab_Old_Amt.getValue(5).setValue(304);                                                                                                                        //Natural: MOVE 00304.00 TO #TAB-OLD-AMT ( 5 )
        pnd_Tab_New_Amt.getValue(5).setValue(306);                                                                                                                        //Natural: MOVE 00306.00 TO #TAB-NEW-AMT ( 5 )
        //*     $1166 $1170
        pnd_Tab_Old_Amt.getValue(6).setValue(1166);                                                                                                                       //Natural: MOVE 01166.00 TO #TAB-OLD-AMT ( 6 )
        pnd_Tab_New_Amt.getValue(6).setValue(1170);                                                                                                                       //Natural: MOVE 01170.00 TO #TAB-NEW-AMT ( 6 )
        //*     $929  $933
        pnd_Tab_Old_Amt.getValue(7).setValue(929);                                                                                                                        //Natural: MOVE 00929.00 TO #TAB-OLD-AMT ( 7 )
        pnd_Tab_New_Amt.getValue(7).setValue(933);                                                                                                                        //Natural: MOVE 00933.00 TO #TAB-NEW-AMT ( 7 )
        //*     $693  $696
        pnd_Tab_Old_Amt.getValue(8).setValue(693);                                                                                                                        //Natural: MOVE 00693.00 TO #TAB-OLD-AMT ( 8 )
        //*                  12/08 JT
        pnd_Tab_New_Amt.getValue(8).setValue(696);                                                                                                                        //Natural: MOVE 00696.00 TO #TAB-NEW-AMT ( 8 )
    }
    private void sub_Pnd_Write_Detail_Line() throws Exception                                                                                                             //Natural: #WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Cntrct_Payee_Key_Pnd_Cntrct_Ppcn_Nbr,new TabSetting(13),pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde,new      //Natural: WRITE ( 1 ) / #CNTRCT-PPCN-NBR 013T #CNTRCT-PAYEE-CDE 026T #PRT-PEND-CDE 034T #PRT-DED-SEQ-NBR 045T #PRT-DED-CODE 054T #TOTAL-PAY-AMT ( EM = Z,ZZZ,ZZ9.99 ) 070T #PRT-OLD-AMT ( EM = ZZ,ZZ9.99 ) 084T #PRT-NEW-AMT ( EM = ZZ,ZZ9.99 ) 099T #PRT-MESSAGE
            TabSetting(26),pnd_Prt_Pend_Cde,new TabSetting(34),pnd_Prt_Ded_Seq_Nbr,new TabSetting(45),pnd_Prt_Ded_Code,new TabSetting(54),pnd_Total_Pay_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Prt_Old_Amt, new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(84),pnd_Prt_New_Amt, 
            new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(99),pnd_Prt_Message);
        if (Global.isEscape()) return;
        pnd_Prt_Message.reset();                                                                                                                                          //Natural: RESET #PRT-MESSAGE
    }
    private void sub_Pnd_Write_Before_Image() throws Exception                                                                                                            //Natural: #WRITE-BEFORE-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaIaal202c.getVw_iaa_Ddctn_Trans().reset();                                                                                                                      //Natural: RESET IAA-DDCTN-TRANS
        ldaIaal202c.getVw_iaa_Ddctn_Trans().setValuesByName(ldaIaal200c.getVw_iaa_Deduction());                                                                           //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
        ldaIaal202c.getIaa_Ddctn_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE = #TIME
        pnd_Date_Time_P.setValue(ldaIaal202c.getIaa_Ddctn_Trans_Trans_Dte());                                                                                             //Natural: ASSIGN #DATE-TIME-P = IAA-DDCTN-TRANS.TRANS-DTE
        ldaIaal202c.getIaa_Ddctn_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202c.getIaa_Ddctn_Trans_Invrse_Trans_Dte()), new                    //Natural: COMPUTE IAA-DDCTN-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
            DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
        ldaIaal202c.getIaa_Ddctn_Trans_Bfre_Imge_Id().setValue("1");                                                                                                      //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID = '1'
        ldaIaal202c.getIaa_Ddctn_Trans_Trans_Check_Dte().setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                               //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE = #SAVE-CHECK-DTE
        ldaIaal202c.getVw_iaa_Ddctn_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-DDCTN-TRANS
    }
    private void sub_Pnd_Write_After_Image() throws Exception                                                                                                             //Natural: #WRITE-AFTER-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaIaal202c.getVw_iaa_Ddctn_Trans().reset();                                                                                                                      //Natural: RESET IAA-DDCTN-TRANS
        ldaIaal202c.getVw_iaa_Ddctn_Trans().setValuesByName(ldaIaal200c.getVw_iaa_Deduction());                                                                           //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
        ldaIaal202c.getIaa_Ddctn_Trans_Trans_Dte().setValue(pnd_Time);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE = #TIME
        pnd_Date_Time_P.setValue(ldaIaal202c.getIaa_Ddctn_Trans_Trans_Dte());                                                                                             //Natural: ASSIGN #DATE-TIME-P = IAA-DDCTN-TRANS.TRANS-DTE
        ldaIaal202c.getIaa_Ddctn_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaIaal202c.getIaa_Ddctn_Trans_Invrse_Trans_Dte()), new                    //Natural: COMPUTE IAA-DDCTN-TRANS.INVRSE-TRANS-DTE = 1000000000000 - #DATE-TIME-P
            DbsDecimal("1000000000000").subtract(pnd_Date_Time_P));
        ldaIaal202c.getIaa_Ddctn_Trans_Aftr_Imge_Id().setValue("2");                                                                                                      //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID = '2'
        ldaIaal202c.getIaa_Ddctn_Trans_Trans_Check_Dte().setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                               //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE = #SAVE-CHECK-DTE
        ldaIaal202c.getIaa_Ddctn_Trans_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                //Natural: ASSIGN IAA-DDCTN-TRANS.LST-TRANS-DTE = #TIME
        ldaIaal202c.getVw_iaa_Ddctn_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-DDCTN-TRANS
    }
    private void sub_Pnd_Write_Transaction() throws Exception                                                                                                             //Natural: #WRITE-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal202g.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(pnd_Time);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TIME
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Time);                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TIME
        pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(ldaIaal202g.getIaa_Trans_Rcrd_Lst_Trans_Dte()));    //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - IAA-TRANS-RCRD.LST-TRANS-DTE
        //*  ADDED TO ENSURE UNIQUENESS 12/08
        if (condition(pnd_Invrse_Dte.equals(pnd_S_Invrse_Dte)))                                                                                                           //Natural: IF #INVRSE-DTE = #S-INVRSE-DTE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                 //Natural: ASSIGN #TRANS-DTE := *TIMX
                pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                            //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
                if (condition(pnd_Invrse_Dte.notEquals(pnd_S_Invrse_Dte)))                                                                                                //Natural: IF #INVRSE-DTE NE #S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*  END 12/08
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Invrse_Dte.setValue(pnd_Invrse_Dte);                                                                                                                        //Natural: ASSIGN #S-INVRSE-DTE := #INVRSE-DTE
        ldaIaal202g.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Invrse_Dte);                                                                                        //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Ppcn_Nbr());                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := IAA-DEDUCTION.DDCTN-PPCN-NBR
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Payee_Cde());                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := IAA-DEDUCTION.DDCTN-PAYEE-CDE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Cde());                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := IAA-DEDUCTION.DDCTN-CDE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cde().setValue(902);                                                                                                          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 902
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Actvty_Cde().setValue("A");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Check_Dte().setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #SAVE-CHECK-DTE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Todays_Dte().setValue(pnd_Save_Todays_Dte_A_Pnd_Save_Todays_Dte_N);                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #SAVE-TODAYS-DTE-N
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Area().setValue(" ");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_User_Id().setValue("IAAP820");                                                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'IAAP820'
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Cde().setValue(" ");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-CDE := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Verify_Id().setValue(" ");                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-VERIFY-ID := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Wpid().setValue(" ");                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-WPID := ' '
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr().setValue(0);                                                                                                     //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CWF-ID-NBR := 0
        pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Seq_Nbr.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Seq_Nbr());                                                                  //Natural: ASSIGN #TR-DDCTN-SEQ-NBR := IAA-DEDUCTION.DDCTN-SEQ-NBR
        pnd_Trans_Cmbne_Cde_Pnd_Tr_Ddctn_Cde.setValue(ldaIaal200c.getIaa_Deduction_Ddctn_Cde());                                                                          //Natural: ASSIGN #TR-DDCTN-CDE := IAA-DEDUCTION.DDCTN-CDE
        ldaIaal202g.getIaa_Trans_Rcrd_Trans_Cmbne_Cde().setValue(pnd_Trans_Cmbne_Cde);                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CMBNE-CDE := #TRANS-CMBNE-CDE
        ldaIaal202g.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                 //Natural: STORE IAA-TRANS-RCRD
    }
    private void sub_Pnd_Dates_And_Times() throws Exception                                                                                                               //Natural: #DATES-AND-TIMES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  LB 04/98
        pnd_Time.setValue(Global.getTIMX());                                                                                                                              //Natural: MOVE *TIMX TO #TIME
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("DC");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'DC'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("READ01")))
        {
            pnd_Save_Todays_Dte_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #SAVE-TODAYS-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(ldaIaal202h.getVw_iaa_Cntrl_Rcrd_1().readNextRow("READ02")))
        {
            pnd_Save_Check_Dte_A.setValueEdited(ldaIaal202h.getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 1 ) NOTITLE ' '
                    pnd_Page_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PAGE-CTR
                    getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(50),"GROUP HEALTH DEDUCTION REPORT",new TabSetting(119),     //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 50T 'GROUP HEALTH DEDUCTION REPORT' 119T 'PAGE ' #PAGE-CTR
                        "PAGE ",pnd_Page_Ctr);
                    //*  WRITE (1) '   DATE ' *DATU  53T 'DETAILED DAILY PAYMENT REPORT'
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"CONTRACT ",new TabSetting(13),"PAYEE",new TabSetting(22),"PEND CDE",new                 //Natural: WRITE ( 1 ) 001T 'CONTRACT ' 013T 'PAYEE' 022T 'PEND CDE' 034T 'SEQ #' 043T 'DED CDE' 054T ' PAYMENT AMT' 070T 'OLD AMOUNT' 084T 'NEW AMOUNT' 099T '         MESSAGE AREA         '
                        TabSetting(34),"SEQ #",new TabSetting(43),"DED CDE",new TabSetting(54)," PAYMENT AMT",new TabSetting(70),"OLD AMOUNT",new TabSetting(84),"NEW AMOUNT",new 
                        TabSetting(99),"         MESSAGE AREA         ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(13),"-----",new TabSetting(22),"--------",new                 //Natural: WRITE ( 1 ) 001T '---------' 013T '-----' 022T '--------' 034T '-----' 043T '-------' 054T '------------' 070T '------------' 084T '------------' 099T '------------------------------'
                        TabSetting(34),"-----",new TabSetting(43),"-------",new TabSetting(54),"------------",new TabSetting(70),"------------",new TabSetting(84),"------------",new 
                        TabSetting(99),"------------------------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
