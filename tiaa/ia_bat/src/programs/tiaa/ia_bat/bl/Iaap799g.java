/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:40 PM
**        * FROM NATURAL PROGRAM : Iaap799g
************************************************************
**        * FILE NAME            : Iaap799g.java
**        * CLASS NAME           : Iaap799g
**        * INSTANCE NAME        : Iaap799g
************************************************************
************************************************************************
* PROGRAM : IAAP799G - ORIGINAL SAVED AS IAAP799S
*
* DATE    : 04/26/2012
*
* PURPOSE : DAILY REPORT EXTRACT FOR GROUP CONTRACTS. CONTRACT RANGE:
*           W0250000 - W0899999
*           INPUT FILE IS THE DAILY IA MASTER EXTRACT
*           PPDT.P2210IAD.SORT20.IATRANS(0)
*
* HISTORY :
* 04/26/12  JUN TINIO - ORIGINAL CODE
* 10/08/2014 - JT REPLACED COR WITH MDM. SCAN ON 10/2014 FOR CHANGES.
* 08/2017   RC  : PIN EPXANSION
* 04/2017   OS  : ADDITIONAL PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap799g extends BLNatBase
{
    // Data Areas
    private LdaIaalpsgm ldaIaalpsgm;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Cntrct_Payee;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Ppcn_Nbr;
    private DbsField pnd_Input_Pnd_Payee_Cde;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_Payee_Cde_A;
    private DbsField pnd_Input_Pnd_Record_Code;
    private DbsField pnd_Input_Pnd_Rest_Of_Record_353;

    private DbsGroup pnd_Input__R_Field_3;
    private DbsField pnd_Input_Pnd_Header_Chk_Dte;

    private DbsGroup pnd_Input__R_Field_4;
    private DbsField pnd_Input_Pnd_Optn_Cde;
    private DbsField pnd_Input_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Pnd_F1;
    private DbsField pnd_Input_Pnd_Issue_Dte;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Issue_Dte_A;
    private DbsField pnd_Input_Pnd_1st_Due_Dte;
    private DbsField pnd_Input_Pnd_1st_Pd_Dte;
    private DbsField pnd_Input_Pnd_Crrncy_Cde;

    private DbsGroup pnd_Input__R_Field_6;
    private DbsField pnd_Input_Pnd_Crrncy_Cde_A;
    private DbsField pnd_Input_Pnd_Type_Cde;
    private DbsField pnd_Input_Pnd_F2;
    private DbsField pnd_Input_Pnd_Pnsn_Pln_Cde;
    private DbsField pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Input_Pnd_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Input_Pnd_1st_Xref_Ind;
    private DbsField pnd_Input_Pnd_First_Ann_Dob;
    private DbsField pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Input_Pnd_1st_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_1st_Annt_Life_Cnt;
    private DbsField pnd_Input_Pnd_First_Ann_Dod;
    private DbsField pnd_Input_Pnd_2nd_Xref_Ind;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dob;
    private DbsField pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Input_Pnd_2nd_Annt_Sex_Cde;
    private DbsField pnd_Input_Pnd_Scnd_Ann_Dod;
    private DbsField pnd_Input_Pnd_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Input_Pnd_Scnd_Annt_Ssn;
    private DbsField pnd_Input_Pnd_Div_Payee_Cde;
    private DbsField pnd_Input_Pnd_Div_Coll_Cde;
    private DbsField pnd_Input_Pnd_Ppg_Cde;
    private DbsField pnd_Input_Pnd_Lst_Trans_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Type;
    private DbsField pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Input_Pnd_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Input_Pnd_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Input_Pnd_Roth_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Cntrct_Ssnng_Dte;
    private DbsField pnd_Input_Pnd_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Tax_Exmpt_Ind;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dob;
    private DbsField pnd_Input_Pnd_Orig_Ownr_Dod;
    private DbsField pnd_Input_Pnd_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Input_Pnd_Orgntng_Cntrct_Nmbr;

    private DbsGroup pnd_Input__R_Field_7;
    private DbsField pnd_Input_Pnd_F9;
    private DbsField pnd_Input_Pnd_Ddctn_Cde;

    private DbsGroup pnd_Input__R_Field_8;
    private DbsField pnd_Input_Pnd_Ddctn_Cde_N;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr;

    private DbsGroup pnd_Input__R_Field_9;
    private DbsField pnd_Input_Pnd_Ddctn_Seq_Nbr_A;
    private DbsField pnd_Input_Pnd_Ddctn_Payee;
    private DbsField pnd_Input_Pnd_Ddctn_Per_Amt;
    private DbsField pnd_Input_Pnd_F10;
    private DbsField pnd_Input_Pnd_Ddctn_Stp_Dte;

    private DbsGroup pnd_Input__R_Field_10;
    private DbsField pnd_Input_Pnd_Summ_Cmpny_Cde;
    private DbsField pnd_Input_Pnd_Summ_Fund_Cde;
    private DbsField pnd_Input_Pnd_Summ_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_F11;
    private DbsField pnd_Input_Pnd_Summ_Per_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd;

    private DbsGroup pnd_Input__R_Field_11;
    private DbsField pnd_Input_Pnd_Summ_Per_Dvdnd_R;
    private DbsField pnd_Input_Pnd_F12;
    private DbsField pnd_Input_Pnd_Summ_Units;
    private DbsField pnd_Input_Pnd_Summ_Fin_Pymnt;
    private DbsField pnd_Input_Pnd_Summ_Fin_Dvdnd;

    private DbsGroup pnd_Input__R_Field_12;
    private DbsField pnd_Input_Pnd_Cpr_Id_Nbr;
    private DbsField pnd_Input_Pnd_F13;
    private DbsField pnd_Input_Pnd_Ctznshp_Cde;
    private DbsField pnd_Input_Pnd_Rsdncy_Cde;
    private DbsField pnd_Input_Pnd_F14;
    private DbsField pnd_Input_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Input_Pnd_F15;
    private DbsField pnd_Input_Pnd_Status_Cde;
    private DbsField pnd_Input_Pnd_F16;
    private DbsField pnd_Input_Pnd_Cash_Cde;
    private DbsField pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Input_Pnd_F17;
    private DbsField pnd_Input_Pnd_Rcvry_Type_Ind;
    private DbsField pnd_Input_Pnd_Cntrct_Per_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Input_Pnd_Cntrct_Ivc_Used_Amt;
    private DbsField pnd_Input_Pnd_F18;
    private DbsField pnd_Input_Pnd_Mode_Ind;
    private DbsField pnd_Input_Pnd_F19;
    private DbsField pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Input_Pnd_Final_Pay_Dte;
    private DbsField pnd_Input_Pnd_Bnfcry_Xref_Ind;
    private DbsField pnd_Input_Pnd_Bnfcry_Dod_Dte;
    private DbsField pnd_Input_Pnd_Pend_Cde;
    private DbsField pnd_Input_Pnd_Hold_Cde;
    private DbsField pnd_Input_Pnd_Pend_Dte;

    private DbsGroup pnd_Input__R_Field_13;
    private DbsField pnd_Input_Pnd_Pend_Dte_A;
    private DbsField pnd_Input_Pnd_F21;
    private DbsField pnd_Input_Pnd_Curr_Dist_Cde;
    private DbsField pnd_Input_Pnd_Cmbne_Cde;
    private DbsField pnd_Input_Pnd_F22;
    private DbsField pnd_Input_Pnd_Cntrct_Local_Cde;
    private DbsField pnd_Input_Pnd_F23;
    private DbsField pnd_Input_Pnd_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Input_Pnd_Rllvr_Ivc_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Elgble_Ind;
    private DbsField pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Input_Pnd_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Input_Pnd_F24;
    private DbsField pnd_Input_Pnd_Roth_Dsblty_Dte;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_14;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Wrk_Rnges;
    private DbsField pnd_Ded_Rnges;

    private DbsGroup pnd_Ded_Rnges__R_Field_15;

    private DbsGroup pnd_Ded_Rnges_Pnd_Rnge_Hold;
    private DbsField pnd_Ded_Rnges_Pnd_Strt_Rnge;
    private DbsField pnd_Ded_Rnges__Filler1;
    private DbsField pnd_Ded_Rnges_Pnd_End_Rnge;
    private DbsField pnd_Ded_Rnges__Filler2;
    private DbsField pnd_I;
    private DbsField pnd_Max;
    private DbsField pnd_S_Optn;
    private DbsField pnd_S_Orgn;
    private DbsField pnd_S_Mode_Ind;
    private DbsField pnd_S_Issue_Dte_8;

    private DbsGroup pnd_S_Issue_Dte_8__R_Field_16;
    private DbsField pnd_S_Issue_Dte_8_Pnd_S_Issue_Dte;
    private DbsField pnd_S_Issue_Dte_8_Pnd_S_Issue_Dd;
    private DbsField pnd_S_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_S_First_Ann_Dob;
    private DbsField pnd_S_First_Ann_Dod;
    private DbsField pnd_S_Scnd_Ann_Dob;
    private DbsField pnd_S_Scnd_Ann_Dod;
    private DbsField pnd_S_Status_Cde;
    private DbsField pnd_S_1st_Xref_Ind;
    private DbsField pnd_S_2nd_Xref_Ind;
    private DbsField pnd_S_Cntrct_Type;
    private DbsField pnd_S_Bnfcry_Xref_Ind;
    private DbsField pnd_S_Rllvr_Cntrct_Nbr;
    private DbsField pnd_S_Tax_Id_Nbr;
    private DbsField pnd_S_1st_Annt_Sex_Cde;
    private DbsField pnd_S_2nd_Annt_Sex_Cde;
    private DbsField pnd_S_Scnd_Annt_Ssn;
    private DbsField pnd_Per_Pmt;
    private DbsField pnd_Skip;
    private DbsField pnd_I_Pin;

    private DbsGroup pnd_I_Pin__R_Field_17;
    private DbsField pnd_I_Pin_Pnd_I_Pin_A;
    private DbsField pnd_I_Ssn;

    private DbsGroup pnd_I_Ssn__R_Field_18;
    private DbsField pnd_I_Ssn_Pnd_I_Ssn_A;
    private DbsField pnd_Fname;
    private DbsField pnd_Mname;
    private DbsField pnd_Lname;
    private DbsField pnd_1st_Fname;
    private DbsField pnd_1st_Mname;
    private DbsField pnd_1st_Lname;
    private DbsField pnd_2nd_Fname;
    private DbsField pnd_2nd_Mname;
    private DbsField pnd_2nd_Lname;
    private DbsField pnd_Cor_Rec;

    private DbsGroup pnd_Cor_Rec__R_Field_19;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct_Py;

    private DbsGroup pnd_Cor_Rec__R_Field_20;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Payee;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Pin;

    private DbsGroup pnd_Cor_Rec__R_Field_21;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Pin_N;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Dob;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Dod;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_Rec_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Cor_Hold;

    private DbsGroup pnd_Cor_Hold__R_Field_22;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Cntrct_Py;

    private DbsGroup pnd_Cor_Hold__R_Field_23;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Payee;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Pin;

    private DbsGroup pnd_Cor_Hold__R_Field_24;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Pin_N;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Dob;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Dod;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_Hold_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_Hold_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Cor_01;

    private DbsGroup pnd_Cor_01__R_Field_25;
    private DbsField pnd_Cor_01_Pnd_Cor_Cntrct_Py;

    private DbsGroup pnd_Cor_01__R_Field_26;
    private DbsField pnd_Cor_01_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_01_Pnd_Cor_Payee;
    private DbsField pnd_Cor_01_Pnd_Cor_Pin;

    private DbsGroup pnd_Cor_01__R_Field_27;
    private DbsField pnd_Cor_01_Pnd_Cor_Pin_N;
    private DbsField pnd_Cor_01_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_01_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_01_Pnd_Cor_Dob;
    private DbsField pnd_Cor_01_Pnd_Cor_Dod;
    private DbsField pnd_Cor_01_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_01_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_01_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_01_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_01_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Cor_Pend;

    private DbsGroup pnd_Cor_Pend__R_Field_28;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Cntrct_Py;

    private DbsGroup pnd_Cor_Pend__R_Field_29;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Cntrct;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Payee;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Pin;

    private DbsGroup pnd_Cor_Pend__R_Field_30;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Pin_N;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Type_Cde;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Ssn;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Dob;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Dod;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Last_Nm;
    private DbsField pnd_Cor_Pend_Pnd_Cor_First_Nm;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Middle_Nm;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Status_Cde;
    private DbsField pnd_Cor_Pend_Pnd_Cor_Sex_Cde;
    private DbsField pnd_Core_End_Of_File;
    private DbsField pnd_No_Name;
    private int psgm001ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaalpsgm = new LdaIaalpsgm();
        registerRecord(ldaIaalpsgm);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Cntrct_Payee = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Cntrct_Payee", "#CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_Cntrct_Payee);
        pnd_Input_Pnd_Ppcn_Nbr = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Ppcn_Nbr", "#PPCN-NBR", FieldType.STRING, 10);
        pnd_Input_Pnd_Payee_Cde = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Input__R_Field_2 = pnd_Input__R_Field_1.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_Payee_Cde);
        pnd_Input_Pnd_Payee_Cde_A = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_Payee_Cde_A", "#PAYEE-CDE-A", FieldType.STRING, 2);
        pnd_Input_Pnd_Record_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Record_Code", "#RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Rest_Of_Record_353 = pnd_Input.newFieldArrayInGroup("pnd_Input_Pnd_Rest_Of_Record_353", "#REST-OF-RECORD-353", FieldType.STRING, 
            1, new DbsArrayController(1, 353));

        pnd_Input__R_Field_3 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_3", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Header_Chk_Dte = pnd_Input__R_Field_3.newFieldInGroup("pnd_Input_Pnd_Header_Chk_Dte", "#HEADER-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_4 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_4", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Optn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Optn_Cde", "#OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Input_Pnd_Orgn_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_F1 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F1", "#F1", FieldType.STRING, 2);
        pnd_Input_Pnd_Issue_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_5 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Issue_Dte);
        pnd_Input_Pnd_Issue_Dte_A = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_1st_Due_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Due_Dte", "#1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_1st_Pd_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Pd_Dte", "#1ST-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Crrncy_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde", "#CRRNCY-CDE", FieldType.NUMERIC, 1);

        pnd_Input__R_Field_6 = pnd_Input__R_Field_4.newGroupInGroup("pnd_Input__R_Field_6", "REDEFINE", pnd_Input_Pnd_Crrncy_Cde);
        pnd_Input_Pnd_Crrncy_Cde_A = pnd_Input__R_Field_6.newFieldInGroup("pnd_Input_Pnd_Crrncy_Cde_A", "#CRRNCY-CDE-A", FieldType.STRING, 1);
        pnd_Input_Pnd_Type_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Type_Cde", "#TYPE-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_F2 = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Input_Pnd_Pnsn_Pln_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Pnsn_Pln_Cde", "#PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Joint_Cnvrt_Rcrcd_Ind", "#JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Orig_Da_Cntrct_Nbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Da_Cntrct_Nbr", "#ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Input_Pnd_Rsdncy_At_Issue_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Rsdncy_At_Issue_Cde", "#RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_1st_Xref_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Xref_Ind", "#1ST-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_First_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dob", "#FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Mrtlty_Yob_Dte", "#1ST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Input_Pnd_1st_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Sex_Cde", "#1ST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_1st_Annt_Life_Cnt = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_1st_Annt_Life_Cnt", "#1ST-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_First_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_First_Ann_Dod", "#FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_2nd_Xref_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Xref_Ind", "#2ND-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_Scnd_Ann_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dob", "#SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Annt_Mrtlty_Yob_Dte", "#2ND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Input_Pnd_2nd_Annt_Sex_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_2nd_Annt_Sex_Cde", "#2ND-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Scnd_Ann_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Ann_Dod", "#SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Input_Pnd_Scnd_Annt_Life_Cnt = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Annt_Life_Cnt", "#SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 
            1);
        pnd_Input_Pnd_Scnd_Annt_Ssn = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Scnd_Annt_Ssn", "#SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_Div_Payee_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Payee_Cde", "#DIV-PAYEE-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_Div_Coll_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Div_Coll_Cde", "#DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ppg_Cde = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Ppg_Cde", "#PPG-CDE", FieldType.STRING, 5);
        pnd_Input_Pnd_Lst_Trans_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Lst_Trans_Dte", "#LST-TRANS-DTE", FieldType.TIME);
        pnd_Input_Pnd_Cntrct_Type = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Rsdncy_At_Iss_Re", "#CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3);
        pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte = pnd_Input__R_Field_4.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Fnl_Prm_Dte", "#CNTRCT-FNL-PRM-DTE", FieldType.DATE, 
            new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Mtch_Ppcn = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Mtch_Ppcn", "#CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Cntrct_Annty_Strt_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Annty_Strt_Dte", "#CNTRCT-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Input_Pnd_Cntrct_Issue_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Issue_Dte_Dd", "#CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Due_Dte_Dd", "#CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fp_Pd_Dte_Dd", "#CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Pnd_Roth_Frst_Cntrb_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Frst_Cntrb_Dte", "#ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Roth_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Cntrct_Ssnng_Dte = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Cntrct_Ssnng_Dte", "#CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 
            8);
        pnd_Input_Pnd_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Plan_Nmbr", "#PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Tax_Exmpt_Ind = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Tax_Exmpt_Ind", "#TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Orig_Ownr_Dob = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dob", "#ORIG-OWNR-DOB", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Orig_Ownr_Dod = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orig_Ownr_Dod", "#ORIG-OWNR-DOD", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Sub_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Sub_Plan_Nmbr", "#SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr", "#ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Input_Pnd_Orgntng_Cntrct_Nmbr = pnd_Input__R_Field_4.newFieldInGroup("pnd_Input_Pnd_Orgntng_Cntrct_Nmbr", "#ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10);

        pnd_Input__R_Field_7 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_7", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_F9 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_F9", "#F9", FieldType.STRING, 12);
        pnd_Input_Pnd_Ddctn_Cde = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.STRING, 3);

        pnd_Input__R_Field_8 = pnd_Input__R_Field_7.newGroupInGroup("pnd_Input__R_Field_8", "REDEFINE", pnd_Input_Pnd_Ddctn_Cde);
        pnd_Input_Pnd_Ddctn_Cde_N = pnd_Input__R_Field_8.newFieldInGroup("pnd_Input_Pnd_Ddctn_Cde_N", "#DDCTN-CDE-N", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Ddctn_Seq_Nbr = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr", "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);

        pnd_Input__R_Field_9 = pnd_Input__R_Field_7.newGroupInGroup("pnd_Input__R_Field_9", "REDEFINE", pnd_Input_Pnd_Ddctn_Seq_Nbr);
        pnd_Input_Pnd_Ddctn_Seq_Nbr_A = pnd_Input__R_Field_9.newFieldInGroup("pnd_Input_Pnd_Ddctn_Seq_Nbr_A", "#DDCTN-SEQ-NBR-A", FieldType.STRING, 3);
        pnd_Input_Pnd_Ddctn_Payee = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Payee", "#DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Input_Pnd_Ddctn_Per_Amt = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Per_Amt", "#DDCTN-PER-AMT", FieldType.NUMERIC, 7, 2);
        pnd_Input_Pnd_F10 = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_F10", "#F10", FieldType.STRING, 24);
        pnd_Input_Pnd_Ddctn_Stp_Dte = pnd_Input__R_Field_7.newFieldInGroup("pnd_Input_Pnd_Ddctn_Stp_Dte", "#DDCTN-STP-DTE", FieldType.NUMERIC, 8);

        pnd_Input__R_Field_10 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_10", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Summ_Cmpny_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Cmpny_Cde", "#SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Summ_Fund_Cde = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fund_Cde", "#SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_Summ_Per_Ivc_Amt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Ivc_Amt", "#SUMM-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_F11 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F11", "#F11", FieldType.STRING, 5);
        pnd_Input_Pnd_Summ_Per_Pymnt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Pymnt", "#SUMM-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Per_Dvdnd = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd", "#SUMM-PER-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_11 = pnd_Input__R_Field_10.newGroupInGroup("pnd_Input__R_Field_11", "REDEFINE", pnd_Input_Pnd_Summ_Per_Dvdnd);
        pnd_Input_Pnd_Summ_Per_Dvdnd_R = pnd_Input__R_Field_11.newFieldInGroup("pnd_Input_Pnd_Summ_Per_Dvdnd_R", "#SUMM-PER-DVDND-R", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Input_Pnd_F12 = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_F12", "#F12", FieldType.STRING, 26);
        pnd_Input_Pnd_Summ_Units = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Units", "#SUMM-UNITS", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Input_Pnd_Summ_Fin_Pymnt = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Pymnt", "#SUMM-FIN-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Input_Pnd_Summ_Fin_Dvdnd = pnd_Input__R_Field_10.newFieldInGroup("pnd_Input_Pnd_Summ_Fin_Dvdnd", "#SUMM-FIN-DVDND", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Input__R_Field_12 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_12", "REDEFINE", pnd_Input_Pnd_Rest_Of_Record_353);
        pnd_Input_Pnd_Cpr_Id_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cpr_Id_Nbr", "#CPR-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_F13 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F13", "#F13", FieldType.STRING, 7);
        pnd_Input_Pnd_Ctznshp_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Ctznshp_Cde", "#CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_Rsdncy_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rsdncy_Cde", "#RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Input_Pnd_F14 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Input_Pnd_Tax_Id_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Input_Pnd_F15 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Input_Pnd_Status_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Status_Cde", "#STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Input_Pnd_F16 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F16", "#F16", FieldType.STRING, 3);
        pnd_Input_Pnd_Cash_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cash_Cde", "#CASH-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde", "#CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F17 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F17", "#F17", FieldType.STRING, 5);
        pnd_Input_Pnd_Rcvry_Type_Ind = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Rcvry_Type_Ind", "#RCVRY-TYPE-IND", FieldType.NUMERIC, 
            1, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Per_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Per_Ivc_Amt", "#CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Resdl_Ivc_Amt", "#CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_Cntrct_Ivc_Used_Amt = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Cntrct_Ivc_Used_Amt", "#CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Input_Pnd_F18 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F18", "#F18", FieldType.STRING, 45);
        pnd_Input_Pnd_Mode_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Mode_Ind", "#MODE-IND", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_F19 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F19", "#F19", FieldType.STRING, 6);
        pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte", "#CNTRCT-FIN-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Final_Pay_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Final_Pay_Dte", "#FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Input_Pnd_Bnfcry_Xref_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Bnfcry_Xref_Ind", "#BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_Input_Pnd_Bnfcry_Dod_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Bnfcry_Dod_Dte", "#BNFCRY-DOD-DTE", FieldType.NUMERIC, 6);
        pnd_Input_Pnd_Pend_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Pend_Cde", "#PEND-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Hold_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 1);
        pnd_Input_Pnd_Pend_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Pend_Dte", "#PEND-DTE", FieldType.NUMERIC, 6);

        pnd_Input__R_Field_13 = pnd_Input__R_Field_12.newGroupInGroup("pnd_Input__R_Field_13", "REDEFINE", pnd_Input_Pnd_Pend_Dte);
        pnd_Input_Pnd_Pend_Dte_A = pnd_Input__R_Field_13.newFieldInGroup("pnd_Input_Pnd_Pend_Dte_A", "#PEND-DTE-A", FieldType.STRING, 6);
        pnd_Input_Pnd_F21 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F21", "#F21", FieldType.STRING, 4);
        pnd_Input_Pnd_Curr_Dist_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Curr_Dist_Cde", "#CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Input_Pnd_Cmbne_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cmbne_Cde", "#CMBNE-CDE", FieldType.STRING, 12);
        pnd_Input_Pnd_F22 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F22", "#F22", FieldType.STRING, 29);
        pnd_Input_Pnd_Cntrct_Local_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Cntrct_Local_Cde", "#CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Input_Pnd_F23 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F23", "#F23", FieldType.STRING, 19);
        pnd_Input_Pnd_Rllvr_Cntrct_Nbr = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Cntrct_Nbr", "#RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10);
        pnd_Input_Pnd_Rllvr_Ivc_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Ivc_Ind", "#RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_Rllvr_Elgble_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Elgble_Ind", "#RLLVR-ELGBLE-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde = pnd_Input__R_Field_12.newFieldArrayInGroup("pnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde", "#RLLVR-DSTRBTNG-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Accptng_Irc_Cde", "#RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Input_Pnd_Rllvr_Pln_Admn_Ind = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Rllvr_Pln_Admn_Ind", "#RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1);
        pnd_Input_Pnd_F24 = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_F24", "#F24", FieldType.STRING, 8);
        pnd_Input_Pnd_Roth_Dsblty_Dte = pnd_Input__R_Field_12.newFieldInGroup("pnd_Input_Pnd_Roth_Dsblty_Dte", "#ROTH-DSBLTY-DTE", FieldType.NUMERIC, 
            8);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_14", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_14.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_14.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_14.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_14.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Wrk_Rnges = localVariables.newFieldArrayInRecord("pnd_Wrk_Rnges", "#WRK-RNGES", FieldType.STRING, 18, new DbsArrayController(1, 10));
        pnd_Ded_Rnges = localVariables.newFieldArrayInRecord("pnd_Ded_Rnges", "#DED-RNGES", FieldType.STRING, 18, new DbsArrayController(1, 100));

        pnd_Ded_Rnges__R_Field_15 = localVariables.newGroupInRecord("pnd_Ded_Rnges__R_Field_15", "REDEFINE", pnd_Ded_Rnges);

        pnd_Ded_Rnges_Pnd_Rnge_Hold = pnd_Ded_Rnges__R_Field_15.newGroupArrayInGroup("pnd_Ded_Rnges_Pnd_Rnge_Hold", "#RNGE-HOLD", new DbsArrayController(1, 
            100));
        pnd_Ded_Rnges_Pnd_Strt_Rnge = pnd_Ded_Rnges_Pnd_Rnge_Hold.newFieldInGroup("pnd_Ded_Rnges_Pnd_Strt_Rnge", "#STRT-RNGE", FieldType.STRING, 8);
        pnd_Ded_Rnges__Filler1 = pnd_Ded_Rnges_Pnd_Rnge_Hold.newFieldInGroup("pnd_Ded_Rnges__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ded_Rnges_Pnd_End_Rnge = pnd_Ded_Rnges_Pnd_Rnge_Hold.newFieldInGroup("pnd_Ded_Rnges_Pnd_End_Rnge", "#END-RNGE", FieldType.STRING, 8);
        pnd_Ded_Rnges__Filler2 = pnd_Ded_Rnges_Pnd_Rnge_Hold.newFieldInGroup("pnd_Ded_Rnges__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.INTEGER, 2);
        pnd_S_Optn = localVariables.newFieldInRecord("pnd_S_Optn", "#S-OPTN", FieldType.NUMERIC, 2);
        pnd_S_Orgn = localVariables.newFieldInRecord("pnd_S_Orgn", "#S-ORGN", FieldType.STRING, 2);
        pnd_S_Mode_Ind = localVariables.newFieldInRecord("pnd_S_Mode_Ind", "#S-MODE-IND", FieldType.NUMERIC, 3);
        pnd_S_Issue_Dte_8 = localVariables.newFieldInRecord("pnd_S_Issue_Dte_8", "#S-ISSUE-DTE-8", FieldType.STRING, 8);

        pnd_S_Issue_Dte_8__R_Field_16 = localVariables.newGroupInRecord("pnd_S_Issue_Dte_8__R_Field_16", "REDEFINE", pnd_S_Issue_Dte_8);
        pnd_S_Issue_Dte_8_Pnd_S_Issue_Dte = pnd_S_Issue_Dte_8__R_Field_16.newFieldInGroup("pnd_S_Issue_Dte_8_Pnd_S_Issue_Dte", "#S-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_S_Issue_Dte_8_Pnd_S_Issue_Dd = pnd_S_Issue_Dte_8__R_Field_16.newFieldInGroup("pnd_S_Issue_Dte_8_Pnd_S_Issue_Dd", "#S-ISSUE-DD", FieldType.NUMERIC, 
            2);
        pnd_S_Cntrct_Fin_Per_Pay_Dte = localVariables.newFieldInRecord("pnd_S_Cntrct_Fin_Per_Pay_Dte", "#S-CNTRCT-FIN-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        pnd_S_First_Ann_Dob = localVariables.newFieldInRecord("pnd_S_First_Ann_Dob", "#S-FIRST-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_S_First_Ann_Dod = localVariables.newFieldInRecord("pnd_S_First_Ann_Dod", "#S-FIRST-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_S_Scnd_Ann_Dob = localVariables.newFieldInRecord("pnd_S_Scnd_Ann_Dob", "#S-SCND-ANN-DOB", FieldType.NUMERIC, 8);
        pnd_S_Scnd_Ann_Dod = localVariables.newFieldInRecord("pnd_S_Scnd_Ann_Dod", "#S-SCND-ANN-DOD", FieldType.NUMERIC, 6);
        pnd_S_Status_Cde = localVariables.newFieldInRecord("pnd_S_Status_Cde", "#S-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_S_1st_Xref_Ind = localVariables.newFieldInRecord("pnd_S_1st_Xref_Ind", "#S-1ST-XREF-IND", FieldType.STRING, 9);
        pnd_S_2nd_Xref_Ind = localVariables.newFieldInRecord("pnd_S_2nd_Xref_Ind", "#S-2ND-XREF-IND", FieldType.STRING, 9);
        pnd_S_Cntrct_Type = localVariables.newFieldInRecord("pnd_S_Cntrct_Type", "#S-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_S_Bnfcry_Xref_Ind = localVariables.newFieldInRecord("pnd_S_Bnfcry_Xref_Ind", "#S-BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_S_Rllvr_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_S_Rllvr_Cntrct_Nbr", "#S-RLLVR-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_S_Tax_Id_Nbr = localVariables.newFieldInRecord("pnd_S_Tax_Id_Nbr", "#S-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_S_1st_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_S_1st_Annt_Sex_Cde", "#S-1ST-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_S_2nd_Annt_Sex_Cde = localVariables.newFieldInRecord("pnd_S_2nd_Annt_Sex_Cde", "#S-2ND-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_S_Scnd_Annt_Ssn = localVariables.newFieldInRecord("pnd_S_Scnd_Annt_Ssn", "#S-SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Per_Pmt = localVariables.newFieldInRecord("pnd_Per_Pmt", "#PER-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Skip = localVariables.newFieldInRecord("pnd_Skip", "#SKIP", FieldType.BOOLEAN, 1);
        pnd_I_Pin = localVariables.newFieldInRecord("pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 12);

        pnd_I_Pin__R_Field_17 = localVariables.newGroupInRecord("pnd_I_Pin__R_Field_17", "REDEFINE", pnd_I_Pin);
        pnd_I_Pin_Pnd_I_Pin_A = pnd_I_Pin__R_Field_17.newFieldInGroup("pnd_I_Pin_Pnd_I_Pin_A", "#I-PIN-A", FieldType.STRING, 12);
        pnd_I_Ssn = localVariables.newFieldInRecord("pnd_I_Ssn", "#I-SSN", FieldType.NUMERIC, 9);

        pnd_I_Ssn__R_Field_18 = localVariables.newGroupInRecord("pnd_I_Ssn__R_Field_18", "REDEFINE", pnd_I_Ssn);
        pnd_I_Ssn_Pnd_I_Ssn_A = pnd_I_Ssn__R_Field_18.newFieldInGroup("pnd_I_Ssn_Pnd_I_Ssn_A", "#I-SSN-A", FieldType.STRING, 9);
        pnd_Fname = localVariables.newFieldInRecord("pnd_Fname", "#FNAME", FieldType.STRING, 15);
        pnd_Mname = localVariables.newFieldInRecord("pnd_Mname", "#MNAME", FieldType.STRING, 12);
        pnd_Lname = localVariables.newFieldInRecord("pnd_Lname", "#LNAME", FieldType.STRING, 15);
        pnd_1st_Fname = localVariables.newFieldInRecord("pnd_1st_Fname", "#1ST-FNAME", FieldType.STRING, 15);
        pnd_1st_Mname = localVariables.newFieldInRecord("pnd_1st_Mname", "#1ST-MNAME", FieldType.STRING, 12);
        pnd_1st_Lname = localVariables.newFieldInRecord("pnd_1st_Lname", "#1ST-LNAME", FieldType.STRING, 15);
        pnd_2nd_Fname = localVariables.newFieldInRecord("pnd_2nd_Fname", "#2ND-FNAME", FieldType.STRING, 15);
        pnd_2nd_Mname = localVariables.newFieldInRecord("pnd_2nd_Mname", "#2ND-MNAME", FieldType.STRING, 12);
        pnd_2nd_Lname = localVariables.newFieldInRecord("pnd_2nd_Lname", "#2ND-LNAME", FieldType.STRING, 15);
        pnd_Cor_Rec = localVariables.newFieldInRecord("pnd_Cor_Rec", "#COR-REC", FieldType.STRING, 150);

        pnd_Cor_Rec__R_Field_19 = localVariables.newGroupInRecord("pnd_Cor_Rec__R_Field_19", "REDEFINE", pnd_Cor_Rec);
        pnd_Cor_Rec_Pnd_Cor_Cntrct_Py = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct_Py", "#COR-CNTRCT-PY", FieldType.STRING, 12);

        pnd_Cor_Rec__R_Field_20 = pnd_Cor_Rec__R_Field_19.newGroupInGroup("pnd_Cor_Rec__R_Field_20", "REDEFINE", pnd_Cor_Rec_Pnd_Cor_Cntrct_Py);
        pnd_Cor_Rec_Pnd_Cor_Cntrct = pnd_Cor_Rec__R_Field_20.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_Rec_Pnd_Cor_Payee = pnd_Cor_Rec__R_Field_20.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Pin = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);

        pnd_Cor_Rec__R_Field_21 = pnd_Cor_Rec__R_Field_19.newGroupInGroup("pnd_Cor_Rec__R_Field_21", "REDEFINE", pnd_Cor_Rec_Pnd_Cor_Pin);
        pnd_Cor_Rec_Pnd_Cor_Pin_N = pnd_Cor_Rec__R_Field_21.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Pin_N", "#COR-PIN-N", FieldType.NUMERIC, 12);
        pnd_Cor_Rec_Pnd_Cor_Type_Cde = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Cor_Rec_Pnd_Cor_Ssn = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_Rec_Pnd_Cor_Dob = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Dod = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_Rec_Pnd_Cor_Last_Nm = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_First_Nm = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Middle_Nm = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 30);
        pnd_Cor_Rec_Pnd_Cor_Status_Cde = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Cor_Rec_Pnd_Cor_Sex_Cde = pnd_Cor_Rec__R_Field_19.newFieldInGroup("pnd_Cor_Rec_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Cor_Hold = localVariables.newFieldInRecord("pnd_Cor_Hold", "#COR-HOLD", FieldType.STRING, 155);

        pnd_Cor_Hold__R_Field_22 = localVariables.newGroupInRecord("pnd_Cor_Hold__R_Field_22", "REDEFINE", pnd_Cor_Hold);
        pnd_Cor_Hold_Pnd_Cor_Cntrct_Py = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Cntrct_Py", "#COR-CNTRCT-PY", FieldType.STRING, 
            12);

        pnd_Cor_Hold__R_Field_23 = pnd_Cor_Hold__R_Field_22.newGroupInGroup("pnd_Cor_Hold__R_Field_23", "REDEFINE", pnd_Cor_Hold_Pnd_Cor_Cntrct_Py);
        pnd_Cor_Hold_Pnd_Cor_Cntrct = pnd_Cor_Hold__R_Field_23.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_Hold_Pnd_Cor_Payee = pnd_Cor_Hold__R_Field_23.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_Hold_Pnd_Cor_Pin = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);

        pnd_Cor_Hold__R_Field_24 = pnd_Cor_Hold__R_Field_22.newGroupInGroup("pnd_Cor_Hold__R_Field_24", "REDEFINE", pnd_Cor_Hold_Pnd_Cor_Pin);
        pnd_Cor_Hold_Pnd_Cor_Pin_N = pnd_Cor_Hold__R_Field_24.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Pin_N", "#COR-PIN-N", FieldType.NUMERIC, 12);
        pnd_Cor_Hold_Pnd_Cor_Type_Cde = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cor_Hold_Pnd_Cor_Ssn = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_Hold_Pnd_Cor_Dob = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_Hold_Pnd_Cor_Dod = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_Hold_Pnd_Cor_Last_Nm = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_Hold_Pnd_Cor_First_Nm = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_Hold_Pnd_Cor_Middle_Nm = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 
            30);
        pnd_Cor_Hold_Pnd_Cor_Status_Cde = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Cor_Hold_Pnd_Cor_Sex_Cde = pnd_Cor_Hold__R_Field_22.newFieldInGroup("pnd_Cor_Hold_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Cor_01 = localVariables.newFieldInRecord("pnd_Cor_01", "#COR-01", FieldType.STRING, 155);

        pnd_Cor_01__R_Field_25 = localVariables.newGroupInRecord("pnd_Cor_01__R_Field_25", "REDEFINE", pnd_Cor_01);
        pnd_Cor_01_Pnd_Cor_Cntrct_Py = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Cntrct_Py", "#COR-CNTRCT-PY", FieldType.STRING, 12);

        pnd_Cor_01__R_Field_26 = pnd_Cor_01__R_Field_25.newGroupInGroup("pnd_Cor_01__R_Field_26", "REDEFINE", pnd_Cor_01_Pnd_Cor_Cntrct_Py);
        pnd_Cor_01_Pnd_Cor_Cntrct = pnd_Cor_01__R_Field_26.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_01_Pnd_Cor_Payee = pnd_Cor_01__R_Field_26.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_01_Pnd_Cor_Pin = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);

        pnd_Cor_01__R_Field_27 = pnd_Cor_01__R_Field_25.newGroupInGroup("pnd_Cor_01__R_Field_27", "REDEFINE", pnd_Cor_01_Pnd_Cor_Pin);
        pnd_Cor_01_Pnd_Cor_Pin_N = pnd_Cor_01__R_Field_27.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Pin_N", "#COR-PIN-N", FieldType.NUMERIC, 12);
        pnd_Cor_01_Pnd_Cor_Type_Cde = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Cor_01_Pnd_Cor_Ssn = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_01_Pnd_Cor_Dob = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_01_Pnd_Cor_Dod = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_01_Pnd_Cor_Last_Nm = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_01_Pnd_Cor_First_Nm = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_01_Pnd_Cor_Middle_Nm = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 30);
        pnd_Cor_01_Pnd_Cor_Status_Cde = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 1);
        pnd_Cor_01_Pnd_Cor_Sex_Cde = pnd_Cor_01__R_Field_25.newFieldInGroup("pnd_Cor_01_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Cor_Pend = localVariables.newFieldInRecord("pnd_Cor_Pend", "#COR-PEND", FieldType.STRING, 155);

        pnd_Cor_Pend__R_Field_28 = localVariables.newGroupInRecord("pnd_Cor_Pend__R_Field_28", "REDEFINE", pnd_Cor_Pend);
        pnd_Cor_Pend_Pnd_Cor_Cntrct_Py = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Cntrct_Py", "#COR-CNTRCT-PY", FieldType.STRING, 
            12);

        pnd_Cor_Pend__R_Field_29 = pnd_Cor_Pend__R_Field_28.newGroupInGroup("pnd_Cor_Pend__R_Field_29", "REDEFINE", pnd_Cor_Pend_Pnd_Cor_Cntrct_Py);
        pnd_Cor_Pend_Pnd_Cor_Cntrct = pnd_Cor_Pend__R_Field_29.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Cntrct", "#COR-CNTRCT", FieldType.STRING, 10);
        pnd_Cor_Pend_Pnd_Cor_Payee = pnd_Cor_Pend__R_Field_29.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Payee", "#COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Cor_Pend_Pnd_Cor_Pin = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Pin", "#COR-PIN", FieldType.STRING, 12);

        pnd_Cor_Pend__R_Field_30 = pnd_Cor_Pend__R_Field_28.newGroupInGroup("pnd_Cor_Pend__R_Field_30", "REDEFINE", pnd_Cor_Pend_Pnd_Cor_Pin);
        pnd_Cor_Pend_Pnd_Cor_Pin_N = pnd_Cor_Pend__R_Field_30.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Pin_N", "#COR-PIN-N", FieldType.NUMERIC, 12);
        pnd_Cor_Pend_Pnd_Cor_Type_Cde = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Type_Cde", "#COR-TYPE-CDE", FieldType.NUMERIC, 
            2);
        pnd_Cor_Pend_Pnd_Cor_Ssn = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Ssn", "#COR-SSN", FieldType.NUMERIC, 9);
        pnd_Cor_Pend_Pnd_Cor_Dob = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Dob", "#COR-DOB", FieldType.NUMERIC, 8);
        pnd_Cor_Pend_Pnd_Cor_Dod = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Dod", "#COR-DOD", FieldType.NUMERIC, 8);
        pnd_Cor_Pend_Pnd_Cor_Last_Nm = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Last_Nm", "#COR-LAST-NM", FieldType.STRING, 30);
        pnd_Cor_Pend_Pnd_Cor_First_Nm = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_First_Nm", "#COR-FIRST-NM", FieldType.STRING, 30);
        pnd_Cor_Pend_Pnd_Cor_Middle_Nm = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Middle_Nm", "#COR-MIDDLE-NM", FieldType.STRING, 
            30);
        pnd_Cor_Pend_Pnd_Cor_Status_Cde = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Status_Cde", "#COR-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Cor_Pend_Pnd_Cor_Sex_Cde = pnd_Cor_Pend__R_Field_28.newFieldInGroup("pnd_Cor_Pend_Pnd_Cor_Sex_Cde", "#COR-SEX-CDE", FieldType.STRING, 1);
        pnd_Core_End_Of_File = localVariables.newFieldInRecord("pnd_Core_End_Of_File", "#CORE-END-OF-FILE", FieldType.BOOLEAN, 1);
        pnd_No_Name = localVariables.newFieldInRecord("pnd_No_Name", "#NO-NAME", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        ldaIaalpsgm.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap799g() throws Exception
    {
        super("Iaap799g");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 0 LS = 250 ZP = OFF;//Natural: FORMAT ( 2 ) PS = 0 LS = 250 ZP = OFF;//Natural: FORMAT ( 0 ) PS = 0 LS = 250 ZP = OFF
                                                                                                                                                                          //Natural: PERFORM GET-HARVARD-RANGES
        sub_Get_Harvard_Ranges();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.greater("W0899999")))                                                                                                    //Natural: IF #PPCN-NBR GT 'W0899999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.greaterOrEqual("W0250000") && pnd_Input_Pnd_Ppcn_Nbr.lessOrEqual("W0899999")))                                           //Natural: IF #PPCN-NBR = 'W0250000' THRU 'W0899999'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(pnd_Input_Pnd_Record_Code.lessOrEqual(30))))                                                                                                  //Natural: ACCEPT #RECORD-CODE LE 30
            {
                continue;
            }
            short decideConditionsMet431 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #RECORD-CODE;//Natural: VALUE 10
            if (condition((pnd_Input_Pnd_Record_Code.equals(10))))
            {
                decideConditionsMet431++;
                pnd_S_Rllvr_Cntrct_Nbr.reset();                                                                                                                           //Natural: RESET #S-RLLVR-CNTRCT-NBR #S-CNTRCT-TYPE #PSGM001-COMMON-AREA
                pnd_S_Cntrct_Type.reset();
                ldaIaalpsgm.getPnd_Psgm001_Common_Area().reset();
                pnd_S_Optn.setValue(pnd_Input_Pnd_Optn_Cde);                                                                                                              //Natural: ASSIGN #S-OPTN := #OPTN-CDE
                pnd_S_Orgn.setValue(pnd_Input_Pnd_Orgn_Cde);                                                                                                              //Natural: ASSIGN #S-ORGN := #ORGN-CDE
                pnd_S_Issue_Dte_8_Pnd_S_Issue_Dte.setValue(pnd_Input_Pnd_Issue_Dte);                                                                                      //Natural: ASSIGN #S-ISSUE-DTE := #ISSUE-DTE
                pnd_S_Issue_Dte_8_Pnd_S_Issue_Dd.setValue(pnd_Input_Pnd_Cntrct_Issue_Dte_Dd);                                                                             //Natural: ASSIGN #S-ISSUE-DD := #CNTRCT-ISSUE-DTE-DD
                pnd_S_First_Ann_Dob.setValue(pnd_Input_Pnd_First_Ann_Dob);                                                                                                //Natural: ASSIGN #S-FIRST-ANN-DOB := #FIRST-ANN-DOB
                pnd_S_First_Ann_Dod.setValue(pnd_Input_Pnd_First_Ann_Dod);                                                                                                //Natural: ASSIGN #S-FIRST-ANN-DOD := #FIRST-ANN-DOD
                pnd_S_Scnd_Ann_Dob.setValue(pnd_Input_Pnd_Scnd_Ann_Dob);                                                                                                  //Natural: ASSIGN #S-SCND-ANN-DOB := #SCND-ANN-DOB
                pnd_S_Scnd_Ann_Dod.setValue(pnd_Input_Pnd_Scnd_Ann_Dod);                                                                                                  //Natural: ASSIGN #S-SCND-ANN-DOD := #SCND-ANN-DOD
                pnd_S_1st_Xref_Ind.setValue(pnd_Input_Pnd_1st_Xref_Ind);                                                                                                  //Natural: ASSIGN #S-1ST-XREF-IND := #1ST-XREF-IND
                pnd_S_2nd_Xref_Ind.setValue(pnd_Input_Pnd_2nd_Xref_Ind);                                                                                                  //Natural: ASSIGN #S-2ND-XREF-IND := #2ND-XREF-IND
                pnd_S_1st_Annt_Sex_Cde.setValue(pnd_Input_Pnd_1st_Annt_Sex_Cde);                                                                                          //Natural: ASSIGN #S-1ST-ANNT-SEX-CDE := #1ST-ANNT-SEX-CDE
                pnd_S_2nd_Annt_Sex_Cde.setValue(pnd_Input_Pnd_2nd_Annt_Sex_Cde);                                                                                          //Natural: ASSIGN #S-2ND-ANNT-SEX-CDE := #2ND-ANNT-SEX-CDE
                pnd_S_Scnd_Annt_Ssn.setValue(pnd_Input_Pnd_Scnd_Annt_Ssn);                                                                                                //Natural: ASSIGN #S-SCND-ANNT-SSN := #SCND-ANNT-SSN
                pnd_2nd_Fname.reset();                                                                                                                                    //Natural: RESET #2ND-FNAME #2ND-MNAME #2ND-LNAME
                pnd_2nd_Mname.reset();
                pnd_2nd_Lname.reset();
                if (condition(pnd_Input_Pnd_Scnd_Annt_Ssn.greater(getZero())))                                                                                            //Natural: IF #SCND-ANNT-SSN GT 0
                {
                    //*        #SSN := #SCND-ANNT-SSN   /* 10/2014
                    //*  10/2014
                    //*  10/2014
                    pnd_I_Pin.reset();                                                                                                                                    //Natural: RESET #I-PIN
                    pnd_I_Ssn.setValue(pnd_Input_Pnd_Scnd_Annt_Ssn);                                                                                                      //Natural: ASSIGN #I-SSN := #SCND-ANNT-SSN
                                                                                                                                                                          //Natural: PERFORM GET-NAME-SSN
                    sub_Get_Name_Ssn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_2nd_Fname.setValue(pnd_Fname);                                                                                                                    //Natural: ASSIGN #2ND-FNAME := #FNAME
                    pnd_2nd_Mname.setValue(pnd_Mname);                                                                                                                    //Natural: ASSIGN #2ND-MNAME := #MNAME
                    pnd_2nd_Lname.setValue(pnd_Lname);                                                                                                                    //Natural: ASSIGN #2ND-LNAME := #LNAME
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-IF-HARVARD
                sub_Determine_If_Harvard();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Input_Pnd_Record_Code.equals(20))))
            {
                decideConditionsMet431++;
                pnd_1st_Fname.reset();                                                                                                                                    //Natural: RESET #1ST-FNAME #1ST-MNAME #1ST-LNAME #PSGM001-COMMON-AREA
                pnd_1st_Mname.reset();
                pnd_1st_Lname.reset();
                ldaIaalpsgm.getPnd_Psgm001_Common_Area().reset();
                //* *    IF #TAX-ID-NBR GT 0
                //*        #SSN := #TAX-ID-NBR   /* 10/2014
                //* *      #I-SSN := #TAX-ID-NBR /* 10/2014
                //* *      RESET #I-PIN          /* 10/2014
                //* *      PERFORM GET-NAME-SSN
                //* *    END-IF
                //* *    IF #LNAME = ' '
                //* *      IF #CPR-ID-NBR GT 0
                //*          #PIN := #CPR-ID-NBR /* 10/2014
                //* *        #I-PIN := #CPR-ID-NBR /* 10/2014
                //* *        RESET #I-SSN          /* 10/2014
                //* *        PERFORM GET-NAME-PIN
                //* *      END-IF
                //* *    END-IF
                //* *    #1ST-FNAME := #FNAME
                //* *    #1ST-MNAME := #MNAME
                //* *    #1ST-LNAME := #LNAME
                                                                                                                                                                          //Natural: PERFORM READ-COR
                sub_Read_Cor();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_1st_Fname.equals(" ") && pnd_1st_Mname.equals(" ") && pnd_1st_Lname.equals(" ")))                                                       //Natural: IF #1ST-FNAME = ' ' AND #1ST-MNAME = ' ' AND #1ST-LNAME = ' '
                {
                    pnd_1st_Fname.setValue("NAME NOT ON COR");                                                                                                            //Natural: ASSIGN #1ST-FNAME := 'NAME NOT ON COR'
                    pnd_No_Name.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NO-NAME
                    //*        WRITE WORK 3 #CNTRCT-PAYEE
                }                                                                                                                                                         //Natural: END-IF
                pnd_S_Tax_Id_Nbr.setValue(pnd_Input_Pnd_Tax_Id_Nbr);                                                                                                      //Natural: ASSIGN #S-TAX-ID-NBR := #TAX-ID-NBR
                pnd_S_Mode_Ind.setValue(pnd_Input_Pnd_Mode_Ind);                                                                                                          //Natural: ASSIGN #S-MODE-IND := #MODE-IND
                pnd_S_Status_Cde.setValue(pnd_Input_Pnd_Status_Cde);                                                                                                      //Natural: ASSIGN #S-STATUS-CDE := #STATUS-CDE
                pnd_S_Cntrct_Fin_Per_Pay_Dte.setValue(pnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte);                                                                              //Natural: ASSIGN #S-CNTRCT-FIN-PER-PAY-DTE := #CNTRCT-FIN-PER-PAY-DTE
                if (condition(pnd_Input_Pnd_Payee_Cde.equals(1)))                                                                                                         //Natural: IF #PAYEE-CDE = 01
                {
                    pnd_S_Rllvr_Cntrct_Nbr.setValue(pnd_Input_Pnd_Rllvr_Cntrct_Nbr);                                                                                      //Natural: ASSIGN #S-RLLVR-CNTRCT-NBR := #RLLVR-CNTRCT-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Input_Pnd_Record_Code.equals(30))))
            {
                decideConditionsMet431++;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL NO NAME:",pnd_No_Name);                                                                                                              //Natural: WRITE ( 1 ) 'TOTAL NO NAME:' #NO-NAME
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  '/1st XREF'            #S-1ST-XREF-IND
        //*  '/2nd XREF'            #S-2ND-XREF-IND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-HARVARD-RANGES
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-IF-HARVARD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-SSN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Per_Pmt.compute(new ComputeParameters(false, pnd_Per_Pmt), pnd_Input_Pnd_Summ_Per_Pymnt.add(pnd_Input_Pnd_Summ_Per_Dvdnd));                                   //Natural: COMPUTE #PER-PMT := #SUMM-PER-PYMNT + #SUMM-PER-DVDND
        getReports().display(1, ReportOption.NOTITLE,"/Contract",                                                                                                         //Natural: DISPLAY ( 1 ) NOTITLE '/Contract' #PPCN-NBR ( AL = 8 ) '/Py' #PAYEE-CDE ( EM = 99 ) 'Prtcpnt/First Name' #1ST-FNAME 'Prtcpnt/Middle Name' #1ST-MNAME 'Prtcpnt/Last Name' #1ST-LNAME '2nd Annt/First Name' #2ND-FNAME '2nd Annt/Middle Name' #2ND-MNAME '2nd Annt/Last Name' #2ND-LNAME '/Optn' #S-OPTN ( EM = 99 ) 'Fin Per/Pay Dte' #S-CNTRCT-FIN-PER-PAY-DTE 'Issue/Dte' #S-ISSUE-DTE-8 '1st/Sex' #S-1ST-ANNT-SEX-CDE '/1st DOB' #S-FIRST-ANN-DOB '/1st DOD' #S-FIRST-ANN-DOD '2nd/Sex' #S-2ND-ANNT-SEX-CDE '/2nd DOB' #S-SCND-ANN-DOB '/2nd DOD' #S-SCND-ANN-DOD '/Part SSN' #S-TAX-ID-NBR ( EM = 999999999 ZP = OFF ) '/2nd SSN' #S-SCND-ANNT-SSN ( EM = 999999999 ZP = OFF ) '/Mode' #S-MODE-IND 'Periodic/Payment' #PER-PMT ( EM = Z,ZZZ,ZZ9.99 ) '/Stts' #S-STATUS-CDE 'Harvard/Id' #S-RLLVR-CNTRCT-NBR '/COLA' #S-CNTRCT-TYPE
        		pnd_Input_Pnd_Ppcn_Nbr, new AlphanumericLength (8),"/Py",
        		pnd_Input_Pnd_Payee_Cde, new ReportEditMask ("99"),"Prtcpnt/First Name",
        		pnd_1st_Fname,"Prtcpnt/Middle Name",
        		pnd_1st_Mname,"Prtcpnt/Last Name",
        		pnd_1st_Lname,"2nd Annt/First Name",
        		pnd_2nd_Fname,"2nd Annt/Middle Name",
        		pnd_2nd_Mname,"2nd Annt/Last Name",
        		pnd_2nd_Lname,"/Optn",
        		pnd_S_Optn, new ReportEditMask ("99"),"Fin Per/Pay Dte",
        		pnd_S_Cntrct_Fin_Per_Pay_Dte,"Issue/Dte",
        		pnd_S_Issue_Dte_8,"1st/Sex",
        		pnd_S_1st_Annt_Sex_Cde,"/1st DOB",
        		pnd_S_First_Ann_Dob,"/1st DOD",
        		pnd_S_First_Ann_Dod,"2nd/Sex",
        		pnd_S_2nd_Annt_Sex_Cde,"/2nd DOB",
        		pnd_S_Scnd_Ann_Dob,"/2nd DOD",
        		pnd_S_Scnd_Ann_Dod,"/Part SSN",
        		pnd_S_Tax_Id_Nbr, new ReportEditMask ("999999999"), new ReportZeroPrint (false),"/2nd SSN",
        		pnd_S_Scnd_Annt_Ssn, new ReportEditMask ("999999999"), new ReportZeroPrint (false),"/Mode",
        		pnd_S_Mode_Ind,"Periodic/Payment",
        		pnd_Per_Pmt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Stts",
        		pnd_S_Status_Cde,"Harvard/Id",
        		pnd_S_Rllvr_Cntrct_Nbr,"/COLA",
        		pnd_S_Cntrct_Type);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Harvard_Ranges() throws Exception                                                                                                                //Natural: GET-HARVARD-RANGES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ041");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ041'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("IAD");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'IAD'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(" ");                                                                                                            //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ' '
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ02",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ02:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ02")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)  //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID OR NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Wrk_Rnges.getValue("*").reset();                                                                                                                          //Natural: RESET #WRK-RNGES ( * )
            naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.separate(SeparateOption.WithAnyDelimiters, ",", pnd_Wrk_Rnges.getValue("*"));                                          //Natural: SEPARATE NAZ-TBL-RCRD-DSCRPTN-TXT INTO #WRK-RNGES ( * ) WITH DELIMITER ','
            if (condition(pnd_Wrk_Rnges.getValue("*").greater(" ")))                                                                                                      //Natural: IF #WRK-RNGES ( * ) GT ' '
            {
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 TO 10
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Wrk_Rnges.getValue(pnd_I).equals(" ")))                                                                                             //Natural: IF #WRK-RNGES ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Max.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #MAX
                    pnd_Ded_Rnges.getValue(pnd_Max).setValue(pnd_Wrk_Rnges.getValue(pnd_I));                                                                              //Natural: ASSIGN #DED-RNGES ( #MAX ) := #WRK-RNGES ( #I )
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Determine_If_Harvard() throws Exception                                                                                                              //Natural: DETERMINE-IF-HARVARD
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max)); pnd_I.nadd(1))
        {
            if (condition(pnd_Input_Pnd_Ppcn_Nbr.greaterOrEqual(pnd_Ded_Rnges_Pnd_Strt_Rnge.getValue(pnd_I)) && pnd_Input_Pnd_Ppcn_Nbr.lessOrEqual(pnd_Ded_Rnges_Pnd_End_Rnge.getValue(pnd_I)))) //Natural: IF #PPCN-NBR = #STRT-RNGE ( #I ) THRU #END-RNGE ( #I )
            {
                if (condition(pnd_Input_Pnd_Cntrct_Type.equals("B") || pnd_Input_Pnd_Cntrct_Type.equals("C")))                                                            //Natural: IF #CNTRCT-TYPE = 'B' OR = 'C'
                {
                    pnd_S_Cntrct_Type.setValue("Y");                                                                                                                      //Natural: ASSIGN #S-CNTRCT-TYPE := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_S_Cntrct_Type.setValue("N");                                                                                                                      //Natural: ASSIGN #S-CNTRCT-TYPE := 'N'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Name_Ssn() throws Exception                                                                                                                      //Natural: GET-NAME-SSN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  10/2014 - START
        //* *#S-TYP := 01
        pnd_Fname.reset();                                                                                                                                                //Natural: RESET #FNAME #MNAME #LNAME
        pnd_Mname.reset();
        pnd_Lname.reset();
        //* *IF #SSN NE 0 AND #SSN NE 999999999
        if (condition(pnd_I_Ssn.notEquals(getZero()) && pnd_I_Ssn.notEquals(999999999)))                                                                                  //Natural: IF #I-SSN NE 0 AND #I-SSN NE 999999999
        {
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function().setValue("PR");                                                                                      //Natural: ASSIGN #IN-FUNCTION := 'PR'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                  //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type().setValue("SSN");                                                                                         //Natural: ASSIGN #IN-TYPE := 'SSN'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn().setValue(pnd_I_Ssn_Pnd_I_Ssn_A);                                                                      //Natural: ASSIGN #IN-PIN-SSN := #I-SSN-A
            psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ(), //Natural: CALL 'PSGM001' #PSGM001-COMMON-AREA
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Input_String(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Pref(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_1(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_2(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_3(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_4(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_5(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_City(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Country(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_State_Nm(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status());
            if (condition(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code().equals("00")))                                                                         //Natural: IF #RT-RET-CODE = '00'
            {
                pnd_Lname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name());                                                                            //Natural: ASSIGN #LNAME := #RT-LAST-NAME
                pnd_Mname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name());                                                                          //Natural: ASSIGN #MNAME := #RT-SECOND-NAME
                pnd_Fname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name());                                                                           //Natural: ASSIGN #FNAME := #RT-FIRST-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "SSN NOT FOUND ",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn());                                                          //Natural: WRITE ( 0 ) 'SSN NOT FOUND ' #IN-PIN-SSN
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2014 - END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Name_Pin() throws Exception                                                                                                                      //Natural: GET-NAME-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  10/2014 - START
        //* *#P-TYP := 01
        pnd_Fname.reset();                                                                                                                                                //Natural: RESET #FNAME #MNAME #LNAME
        pnd_Mname.reset();
        pnd_Lname.reset();
        //* *IF #PIN NE 0 AND #PIN NE 9999999               /* 082017
        //*  082017
        if (condition(pnd_I_Pin.notEquals(getZero()) && pnd_I_Pin.notEquals(new DbsDecimal("999999999999"))))                                                             //Natural: IF #I-PIN NE 0 AND #I-PIN NE 999999999999
        {
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function().setValue("PR");                                                                                      //Natural: ASSIGN #IN-FUNCTION := 'PR'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ().setValue("RS");                                                                                  //Natural: ASSIGN #IN-ADDR-USG-TYP := 'RS'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type().setValue("PIN");                                                                                         //Natural: ASSIGN #IN-TYPE := 'PIN'
            ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn().setValue(pnd_I_Pin_Pnd_I_Pin_A);                                                                      //Natural: ASSIGN #IN-PIN-SSN := #I-PIN-A
            psgm001ReturnCode = DbsUtil.callExternalProgram("PSGM001",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Function(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Addr_Usg_Typ(), //Natural: CALL 'PSGM001' #PSGM001-COMMON-AREA
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Type(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Input_String(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Pin(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Out_Ssn(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Inact_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Birth_Date(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Dec_Date(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Gender_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Pref(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Suffix_Desc(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Person_Org_Cd(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_1(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_2(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_3(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_4(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_5(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_City(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Postal_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Iso_Code(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Country(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Irs_Country_Nm(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_State_Nm(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Geo_Code(),ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Addr_Type_Cd(),
                ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Rcrd_Status());
            if (condition(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Ret_Code().equals("00")))                                                                         //Natural: IF #RT-RET-CODE = '00'
            {
                pnd_Lname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Last_Name());                                                                            //Natural: ASSIGN #LNAME := #RT-LAST-NAME
                pnd_Mname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_Second_Name());                                                                          //Natural: ASSIGN #MNAME := #RT-SECOND-NAME
                pnd_Fname.setValue(ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_Rt_First_Name());                                                                           //Natural: ASSIGN #FNAME := #RT-FIRST-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "PIN NOT FOUND ",ldaIaalpsgm.getPnd_Psgm001_Common_Area_Pnd_In_Pin_Ssn());                                                          //Natural: WRITE ( 0 ) 'PIN NOT FOUND ' #IN-PIN-SSN
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Cor() throws Exception                                                                                                                          //Natural: READ-COR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  LAST COR PAYEE 02 HAS BEEN READ
        if (condition(pnd_Skip.getBoolean()))                                                                                                                             //Natural: IF #SKIP
        {
            pnd_Skip.reset();                                                                                                                                             //Natural: RESET #SKIP
            if (condition(pnd_Cor_Hold_Pnd_Cor_Cntrct_Py.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                             //Natural: IF #COR-HOLD.#COR-CNTRCT-PY = #CNTRCT-PAYEE
            {
                pnd_1st_Fname.setValue(pnd_Cor_Hold_Pnd_Cor_First_Nm);                                                                                                    //Natural: ASSIGN #1ST-FNAME := #COR-HOLD.#COR-FIRST-NM
                pnd_1st_Mname.setValue(pnd_Cor_Hold_Pnd_Cor_Middle_Nm);                                                                                                   //Natural: ASSIGN #1ST-MNAME := #COR-HOLD.#COR-MIDDLE-NM
                pnd_1st_Lname.setValue(pnd_Cor_Hold_Pnd_Cor_Last_Nm);                                                                                                     //Natural: ASSIGN #1ST-LNAME := #COR-HOLD.#COR-LAST-NM
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Core_End_Of_File.getBoolean()))                                                                                                                 //Natural: IF #CORE-END-OF-FILE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  GET NEXT COR PAYEE 02
            if (condition(pnd_Skip.getBoolean()))                                                                                                                         //Natural: IF #SKIP
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Py.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                         //Natural: IF #COR-REC.#COR-CNTRCT-PY GT #CNTRCT-PAYEE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Py.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                              //Natural: IF #COR-REC.#COR-CNTRCT-PY = #CNTRCT-PAYEE
            {
                pnd_1st_Fname.setValue(pnd_Cor_Rec_Pnd_Cor_First_Nm);                                                                                                     //Natural: ASSIGN #1ST-FNAME := #COR-REC.#COR-FIRST-NM
                pnd_1st_Mname.setValue(pnd_Cor_Rec_Pnd_Cor_Middle_Nm);                                                                                                    //Natural: ASSIGN #1ST-MNAME := #COR-REC.#COR-MIDDLE-NM
                pnd_1st_Lname.setValue(pnd_Cor_Rec_Pnd_Cor_Last_Nm);                                                                                                      //Natural: ASSIGN #1ST-LNAME := #COR-REC.#COR-LAST-NM
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, pnd_Cor_Rec);                                                                                                                          //Natural: READ WORK FILE 2 ONCE #COR-REC
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Core_End_Of_File.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #CORE-END-OF-FILE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Skip.getBoolean()))                                                                                                                         //Natural: IF #SKIP
            {
                if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr)))                                                                                 //Natural: IF #COR-REC.#COR-CNTRCT = #PPCN-NBR
                {
                    if (condition(pnd_Cor_Rec_Pnd_Cor_Payee.equals(2) && pnd_Input_Pnd_Payee_Cde.equals(1)))                                                              //Natural: IF #COR-REC.#COR-PAYEE = 02 AND #PAYEE-CDE = 01
                    {
                        pnd_1st_Fname.setValue(pnd_Cor_Rec_Pnd_Cor_First_Nm);                                                                                             //Natural: ASSIGN #1ST-FNAME := #COR-REC.#COR-FIRST-NM
                        pnd_1st_Mname.setValue(pnd_Cor_Rec_Pnd_Cor_Middle_Nm);                                                                                            //Natural: ASSIGN #1ST-MNAME := #COR-REC.#COR-MIDDLE-NM
                        pnd_1st_Lname.setValue(pnd_Cor_Rec_Pnd_Cor_Last_Nm);                                                                                              //Natural: ASSIGN #1ST-LNAME := #COR-REC.#COR-LAST-NM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Cor_Rec_Pnd_Cor_Payee.equals(1)))                                                                                                   //Natural: IF #COR-REC.#COR-PAYEE = 01
                    {
                        //*  SAVE 01
                        //*  IF 02 DECEASED
                        if (condition(pnd_S_Scnd_Ann_Dod.greater(getZero())))                                                                                             //Natural: IF #S-SCND-ANN-DOD GT 0
                        {
                            pnd_Cor_01.setValue(pnd_Cor_Rec);                                                                                                             //Natural: ASSIGN #COR-01 := #COR-REC
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SAVE PEND
                        //*  IF 01 DECEASED
                        if (condition(pnd_S_First_Ann_Dod.greater(getZero())))                                                                                            //Natural: IF #S-FIRST-ANN-DOD GT 0
                        {
                            pnd_Cor_Pend.setValue(pnd_Cor_Rec);                                                                                                           //Natural: ASSIGN #COR-PEND := #COR-REC
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Py.equals(pnd_Input_Pnd_Cntrct_Payee)))                                                                              //Natural: IF #COR-REC.#COR-CNTRCT-PY = #CNTRCT-PAYEE
            {
                pnd_1st_Fname.setValue(pnd_Cor_Rec_Pnd_Cor_First_Nm);                                                                                                     //Natural: ASSIGN #1ST-FNAME := #COR-REC.#COR-FIRST-NM
                pnd_1st_Mname.setValue(pnd_Cor_Rec_Pnd_Cor_Middle_Nm);                                                                                                    //Natural: ASSIGN #1ST-MNAME := #COR-REC.#COR-MIDDLE-NM
                pnd_1st_Lname.setValue(pnd_Cor_Rec_Pnd_Cor_Last_Nm);                                                                                                      //Natural: ASSIGN #1ST-LNAME := #COR-REC.#COR-LAST-NM
                if (condition(pnd_Cor_Rec_Pnd_Cor_Payee.equals(1)))                                                                                                       //Natural: IF #COR-REC.#COR-PAYEE = 01
                {
                    //*  SAVE 01
                    //*  IF 02 DECEASED
                    if (condition(pnd_S_Scnd_Ann_Dod.greater(getZero())))                                                                                                 //Natural: IF #S-SCND-ANN-DOD GT 0
                    {
                        pnd_Cor_01.setValue(pnd_Cor_Rec);                                                                                                                 //Natural: ASSIGN #COR-01 := #COR-REC
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SAVE PEND
                    //*  IF 01 DECEASED
                    if (condition(pnd_S_First_Ann_Dod.greater(getZero())))                                                                                                //Natural: IF #S-FIRST-ANN-DOD GT 0
                    {
                        pnd_Cor_Pend.setValue(pnd_Cor_Rec);                                                                                                               //Natural: ASSIGN #COR-PEND := #COR-REC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr)))                                                                                     //Natural: IF #COR-REC.#COR-CNTRCT = #PPCN-NBR
            {
                if (condition(pnd_Cor_Rec_Pnd_Cor_Payee.equals(2) && pnd_Input_Pnd_Payee_Cde.equals(1)))                                                                  //Natural: IF #COR-REC.#COR-PAYEE = 02 AND #PAYEE-CDE = 01
                {
                    if (condition(pnd_Cor_Rec_Pnd_Cor_Pin_N.equals(pnd_Input_Pnd_Cpr_Id_Nbr) || pnd_Cor_Rec_Pnd_Cor_Ssn.equals(pnd_Input_Pnd_Tax_Id_Nbr)))                //Natural: IF #COR-REC.#COR-PIN-N = #CPR-ID-NBR OR #COR-REC.#COR-SSN = #TAX-ID-NBR
                    {
                        pnd_1st_Fname.setValue(pnd_Cor_Rec_Pnd_Cor_First_Nm);                                                                                             //Natural: ASSIGN #1ST-FNAME := #COR-REC.#COR-FIRST-NM
                        pnd_1st_Mname.setValue(pnd_Cor_Rec_Pnd_Cor_Middle_Nm);                                                                                            //Natural: ASSIGN #1ST-MNAME := #COR-REC.#COR-MIDDLE-NM
                        pnd_1st_Lname.setValue(pnd_Cor_Rec_Pnd_Cor_Last_Nm);                                                                                              //Natural: ASSIGN #1ST-LNAME := #COR-REC.#COR-LAST-NM
                        //*  GET THE NEXT PAYEE 02 IF EXISTS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cor_Hold.setValue(pnd_Cor_Rec);                                                                                                               //Natural: ASSIGN #COR-HOLD := #COR-REC
                        pnd_Skip.setValue(true);                                                                                                                          //Natural: ASSIGN #SKIP := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Pnd_Payee_Cde.equals(2)))                                                                                                             //Natural: IF #PAYEE-CDE = 02
            {
                if (condition(pnd_Cor_01_Pnd_Cor_Payee.equals(1) && pnd_Cor_01_Pnd_Cor_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr)))                                            //Natural: IF #COR-01.#COR-PAYEE = 01 AND #COR-01.#COR-CNTRCT = #PPCN-NBR
                {
                    pnd_1st_Fname.setValue(pnd_Cor_01_Pnd_Cor_First_Nm);                                                                                                  //Natural: ASSIGN #1ST-FNAME := #COR-01.#COR-FIRST-NM
                    pnd_1st_Mname.setValue(pnd_Cor_01_Pnd_Cor_Middle_Nm);                                                                                                 //Natural: ASSIGN #1ST-MNAME := #COR-01.#COR-MIDDLE-NM
                    pnd_1st_Lname.setValue(pnd_Cor_01_Pnd_Cor_Last_Nm);                                                                                                   //Natural: ASSIGN #1ST-LNAME := #COR-01.#COR-LAST-NM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((pnd_Cor_Pend_Pnd_Cor_Payee.equals(1) && pnd_Cor_Pend_Pnd_Cor_Cntrct.equals(pnd_Input_Pnd_Ppcn_Nbr)) && (pnd_Input_Pnd_Pend_Cde.equals("A")  //Natural: IF #COR-PEND.#COR-PAYEE = 01 AND #COR-PEND.#COR-CNTRCT = #PPCN-NBR AND #PEND-CDE = 'A' OR = 'C'
                    || pnd_Input_Pnd_Pend_Cde.equals("C")))))
                {
                    pnd_1st_Fname.setValue(pnd_Cor_Pend_Pnd_Cor_First_Nm);                                                                                                //Natural: ASSIGN #1ST-FNAME := #COR-PEND.#COR-FIRST-NM
                    pnd_1st_Mname.setValue(pnd_Cor_Pend_Pnd_Cor_Middle_Nm);                                                                                               //Natural: ASSIGN #1ST-MNAME := #COR-PEND.#COR-MIDDLE-NM
                    pnd_1st_Lname.setValue(pnd_Cor_Pend_Pnd_Cor_Last_Nm);                                                                                                 //Natural: ASSIGN #1ST-LNAME := #COR-PEND.#COR-LAST-NM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Rec_Pnd_Cor_Payee.equals(1)))                                                                                                           //Natural: IF #COR-REC.#COR-PAYEE = 01
            {
                //*  SAVE 01
                //*  IF 02 DECEASED
                if (condition(pnd_S_Scnd_Ann_Dod.greater(getZero())))                                                                                                     //Natural: IF #S-SCND-ANN-DOD GT 0
                {
                    pnd_Cor_01.setValue(pnd_Cor_Rec);                                                                                                                     //Natural: ASSIGN #COR-01 := #COR-REC
                }                                                                                                                                                         //Natural: END-IF
                //*  SAVE PEND
                //*  IF 01 DECEASED
                if (condition(pnd_S_First_Ann_Dod.greater(getZero())))                                                                                                    //Natural: IF #S-FIRST-ANN-DOD GT 0
                {
                    pnd_Cor_Pend.setValue(pnd_Cor_Rec);                                                                                                                   //Natural: ASSIGN #COR-PEND := #COR-REC
                }                                                                                                                                                         //Natural: END-IF
                //*    WRITE WORK 4 #CNTRCT-PAYEE '  ' #COR-REC.#COR-CNTRCT-PY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cor_Rec_Pnd_Cor_Cntrct_Py.greater(pnd_Input_Pnd_Cntrct_Payee)))                                                                             //Natural: IF #COR-REC.#COR-CNTRCT-PY GT #CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=0 LS=250 ZP=OFF");
        Global.format(2, "PS=0 LS=250 ZP=OFF");
        Global.format(0, "PS=0 LS=250 ZP=OFF");

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"/Contract",
        		pnd_Input_Pnd_Ppcn_Nbr, new AlphanumericLength (8),"/Py",
        		pnd_Input_Pnd_Payee_Cde, new ReportEditMask ("99"),"Prtcpnt/First Name",
        		pnd_1st_Fname,"Prtcpnt/Middle Name",
        		pnd_1st_Mname,"Prtcpnt/Last Name",
        		pnd_1st_Lname,"2nd Annt/First Name",
        		pnd_2nd_Fname,"2nd Annt/Middle Name",
        		pnd_2nd_Mname,"2nd Annt/Last Name",
        		pnd_2nd_Lname,"/Optn",
        		pnd_S_Optn, new ReportEditMask ("99"),"Fin Per/Pay Dte",
        		pnd_S_Cntrct_Fin_Per_Pay_Dte,"Issue/Dte",
        		pnd_S_Issue_Dte_8,"1st/Sex",
        		pnd_S_1st_Annt_Sex_Cde,"/1st DOB",
        		pnd_S_First_Ann_Dob,"/1st DOD",
        		pnd_S_First_Ann_Dod,"2nd/Sex",
        		pnd_S_2nd_Annt_Sex_Cde,"/2nd DOB",
        		pnd_S_Scnd_Ann_Dob,"/2nd DOD",
        		pnd_S_Scnd_Ann_Dod,"/Part SSN",
        		pnd_S_Tax_Id_Nbr, new ReportEditMask ("999999999"), new ReportZeroPrint (false),"/2nd SSN",
        		pnd_S_Scnd_Annt_Ssn, new ReportEditMask ("999999999"), new ReportZeroPrint (false),"/Mode",
        		pnd_S_Mode_Ind,"Periodic/Payment",
        		pnd_Per_Pmt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),"/Stts",
        		pnd_S_Status_Cde,"Harvard/Id",
        		pnd_S_Rllvr_Cntrct_Nbr,"/COLA",
        		pnd_S_Cntrct_Type);
    }
}
