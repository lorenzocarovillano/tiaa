/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:34:30 PM
**        * FROM NATURAL PROGRAM : Iaap975
************************************************************
**        * FILE NAME            : Iaap975.java
**        * CLASS NAME           : Iaap975
**        * INSTANCE NAME        : Iaap975
************************************************************
**********************************************************************
* PROGRAM   : IAAP975
* FUNCTION  : CREATE 1 DC TYPE CONTROL RECORD WITH CURRENT-BUSINESS-DAY
*             & NEXT-BUSINESS-DAY UPDATED
*
*  BUSINESS-DAY'S ARE OBTAINED BY CALLING CPWN119
*
* DATE      : NOV 18, 1997
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap975 extends BLNatBase
{
    // Data Areas
    private PdaCpwa115 pdaCpwa115;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;

    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte;
    private DbsField pnd_Next_Bus_Dte;

    private DbsGroup pnd_Next_Bus_Dte__R_Field_1;
    private DbsField pnd_Next_Bus_Dte_Pnd_Next_Bus_Dte_Redf;
    private DbsField pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Todays_Dte;

    private DbsGroup pnd_Todays_Dte__R_Field_2;
    private DbsField pnd_Todays_Dte_Pnd_Todays_Dte_Redf;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpwa115 = new PdaCpwa115(localVariables);

        // Local Variables

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("IAA_CNTRL_RCRD_1_CNTRL_TIAA_PAYEES", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");

        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 80) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");
        iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte", "CNTRL-NEXT-BUS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_NEXT_BUS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Next_Bus_Dte = localVariables.newFieldInRecord("pnd_Next_Bus_Dte", "#NEXT-BUS-DTE", FieldType.NUMERIC, 8);

        pnd_Next_Bus_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Next_Bus_Dte__R_Field_1", "REDEFINE", pnd_Next_Bus_Dte);
        pnd_Next_Bus_Dte_Pnd_Next_Bus_Dte_Redf = pnd_Next_Bus_Dte__R_Field_1.newFieldInGroup("pnd_Next_Bus_Dte_Pnd_Next_Bus_Dte_Redf", "#NEXT-BUS-DTE-REDF", 
            FieldType.STRING, 8);
        pnd_Cntrl_Invrse_Dte = localVariables.newFieldInRecord("pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);

        pnd_Todays_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Todays_Dte__R_Field_2", "REDEFINE", pnd_Todays_Dte);
        pnd_Todays_Dte_Pnd_Todays_Dte_Redf = pnd_Todays_Dte__R_Field_2.newFieldInGroup("pnd_Todays_Dte_Pnd_Todays_Dte_Redf", "#TODAYS-DTE-REDF", FieldType.STRING, 
            8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap975() throws Exception
    {
        super("Iaap975");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        getReports().definePrinter(2, "CMPRT01");                                                                                                                         //Natural: DEFINE PRINTER ( CMPRT01 = 1 ) OUTPUT 'CMPRT01'
        //*  ***************************************************                                                                                                          //Natural: FORMAT ( 1 ) PS = 60 LS = 133
        //*  READ AA CONTROL RECORD TO GET CURRENT CHECK-DATE
        //*  ***************************************************
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ************************************************************
        //*   CREATE NEXT CNTRL-RCRD-1 WITH CURRENT & NEXT BUSINESS DAY
        //*  ************************************************************
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ02")))
        {
            //*  MOVE NEXT BUS DTE TO CURRENT BUS DATE
            iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte.setValue(iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte);                                                                              //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE := CNTRL-NEXT-BUS-DTE
            pnd_Next_Bus_Dte_Pnd_Next_Bus_Dte_Redf.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte,new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-NEXT-BUS-DTE ( EM = YYYYMMDD ) TO #NEXT-BUS-DTE-REDF
            pdaCpwa115.getCpwa115_For_Date().setValue(pnd_Next_Bus_Dte);                                                                                                  //Natural: ASSIGN FOR-DATE := #NEXT-BUS-DTE
            pdaCpwa115.getCpwa115_For_Time().setValue(Global.getTIMN());                                                                                                  //Natural: ASSIGN FOR-TIME := *TIMN
            //*  GET NEXT BUSSINES DAY
                                                                                                                                                                          //Natural: PERFORM CALL-CPWN119
            sub_Call_Cpwn119();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Todays_Dte_Pnd_Todays_Dte_Redf.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #TODAYS-DTE-REDF
            //*  DEF IN CPWA115
            pnd_Cntrl_Invrse_Dte.compute(new ComputeParameters(false, pnd_Cntrl_Invrse_Dte), DbsField.subtract(100000000,pnd_Todays_Dte));                                //Natural: COMPUTE #CNTRL-INVRSE-DTE = 100000000 - #TODAYS-DTE
            iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte.setValue(pnd_Cntrl_Invrse_Dte);                                                                                             //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-INVRSE-DTE := #CNTRL-INVRSE-DTE
            iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte.setValue(iaa_Cntrl_Rcrd_Cntrl_Check_Dte);                                                                                    //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE := IAA-CNTRL-RCRD.CNTRL-CHECK-DTE
            iaa_Cntrl_Rcrd_1_Cntrl_Cde.setValue("DC");                                                                                                                    //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-CDE := 'DC'
            iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte.setValue(Global.getTIMX());                                                                                             //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-FRST-TRANS-DTE := *TIMX
            iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde.setValue("A");                                                                                                              //Natural: ASSIGN IAA-CNTRL-RCRD-1.CNTRL-ACTVTY-CDE := 'A'
            pnd_Next_Bus_Dte.setValue(pdaCpwa115.getCpwa115_Next_Business_Date());                                                                                        //Natural: ASSIGN #NEXT-BUS-DTE := NEXT-BUSINESS-DATE
            iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Next_Bus_Dte_Pnd_Next_Bus_Dte_Redf);                                    //Natural: MOVE EDITED #NEXT-BUS-DTE-REDF TO IAA-CNTRL-RCRD-1.CNTRL-NEXT-BUS-DTE ( EM = YYYYMMDD )
            vw_iaa_Cntrl_Rcrd_1.insertDBRow();                                                                                                                            //Natural: STORE IAA-CNTRL-RCRD-1
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CONTROL RECORD TYPE 'DC' CREATED FOR BUSINESS DATE:",iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,               //Natural: WRITE ( 1 ) // 'CONTROL RECORD TYPE "DC" CREATED FOR BUSINESS DATE:' IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY )
            new ReportEditMask ("MM/DD/YYYY"));
        if (Global.isEscape()) return;
        //*     IAA-CNTRL-RCRD.CNTRL-CHECK-DTE(EM=MM/DD/YYYY)
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," CNTRL CHECK DATE..............",iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte, new ReportEditMask ("MM/DD/YYYY"),NEWLINE," CNTRL CURRENT BUSINESS DATE...",iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,  //Natural: WRITE ( 1 ) / ' CNTRL CHECK DATE..............' IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = MM/DD/YYYY ) / ' CNTRL CURRENT BUSINESS DATE...' IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY ) / ' CNTRL NEXT BUSINESS DATE......' IAA-CNTRL-RCRD-1.CNTRL-NEXT-BUS-DTE ( EM = MM/DD/YYYY ) / ' CNTRL CDE (RECORD TYPE).......' IAA-CNTRL-RCRD-1.CNTRL-CDE / ' CNTRL INVRSE DATE.............' IAA-CNTRL-RCRD-1.CNTRL-INVRSE-DTE / ' CNTRL ACTIVITY CODE...........' IAA-CNTRL-RCRD-1.CNTRL-ACTVTY-CDE / ' CNTRL TIAA PAYEES.............      ' IAA-CNTRL-RCRD-1.CNTRL-TIAA-PAYEES ( EM = ZZZ,ZZZ,ZZ9 ) / ' CNTRL PER PAY AMOUNT..........' IAA-CNTRL-RCRD-1.CNTRL-PER-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) / ' CNTRL PER DIV AMOUNT..........' IAA-CNTRL-RCRD-1.CNTRL-PER-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) / ' CNTRL FINAL PAY AMOUNT........' IAA-CNTRL-RCRD-1.CNTRL-FINAL-PAY-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) / ' CNTRL FINAL DIV AMOUNT........' IAA-CNTRL-RCRD-1.CNTRL-FINAL-DIV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) / ' CNTRL UNITS COUNT.............' IAA-CNTRL-RCRD-1.CNTRL-UNITS-CNT ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) /
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE," CNTRL NEXT BUSINESS DATE......",iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte, new ReportEditMask ("MM/DD/YYYY"),NEWLINE," CNTRL CDE (RECORD TYPE).......",iaa_Cntrl_Rcrd_1_Cntrl_Cde,NEWLINE," CNTRL INVRSE DATE.............",iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte,NEWLINE," CNTRL ACTIVITY CODE...........",iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde,NEWLINE," CNTRL TIAA PAYEES.............      ",iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE," CNTRL PER PAY AMOUNT..........",iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE," CNTRL PER DIV AMOUNT..........",iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE," CNTRL FINAL PAY AMOUNT........",iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE," CNTRL FINAL DIV AMOUNT........",iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE," CNTRL UNITS COUNT.............",iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"FUND       PAYEES         UNITS             AMOUNT     ",NEWLINE,new TabSetting(3),         //Natural: WRITE ( 1 ) / 3T'FUND       PAYEES         UNITS             AMOUNT     ' / 3T'---- -------------- ----------------- -----------------'
            "---- -------------- ----------------- -----------------");
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C*CNTRL-FUND-CNTS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts)); pnd_I.nadd(1))
        {
            //*  FOR #I = 1 TO 40
            if (condition(! (iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde.getValue(pnd_I).equals("   ") || iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde.getValue(pnd_I).equals("000"))))            //Natural: IF NOT ( IAA-CNTRL-RCRD-1.CNTRL-FUND-CDE ( #I ) = '   ' OR = '000' )
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde.getValue(pnd_I),new TabSetting(9),iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees.getValue(pnd_I),  //Natural: WRITE ( 1 ) 3T IAA-CNTRL-RCRD-1.CNTRL-FUND-CDE ( #I ) 9T IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES ( #I ) ( EM = ZZZ,ZZZ,ZZ9 ) 23T IAA-CNTRL-RCRD-1.CNTRL-UNITS ( #I ) ( EM = Z,ZZZ,ZZZ,ZZZ.999 ) 41T IAA-CNTRL-RCRD-1.CNTRL-AMT ( #I ) ( EM = ZZ,ZZZ,ZZZ,ZZZ.99 ) /
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(23),iaa_Cntrl_Rcrd_1_Cntrl_Units.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.999"),new 
                    TabSetting(41),iaa_Cntrl_Rcrd_1_Cntrl_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE (1) /1T 'FUND CODE....:'  IAA-CNTRL-RCRD-1.CNTRL-FUND-CDE(#I)
                //*      25T 'PAYESS .......      ' IAA-CNTRL-RCRD-1.CNTRL-FUND-PAYEES(#I)
                //*      (EM=ZZZ,ZZZ,ZZ9)
                //*      /25T 'UNITS.........' IAA-CNTRL-RCRD-1.CNTRL-UNITS(#I)
                //*      (EM=Z,ZZZ,ZZZ,ZZZ.999)
                //*      /25T 'AMT...........' IAA-CNTRL-RCRD-1.CNTRL-AMT(#I)
                //*      (EM=ZZ,ZZZ,ZZZ,ZZZ.99)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," CNTRL DEDUCTION CNT...........",iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt,NEWLINE," CNTRL DEDUCTION AMT...........",  //Natural: WRITE ( 1 ) / ' CNTRL DEDUCTION CNT...........' CNTRL-DDCTN-CNT / ' CNTRL DEDUCTION AMT...........' CNTRL-DDCTN-AMT / ' CNTRL TIAA ACTIVE PAYEES......' CNTRL-ACTVE-TIAA-PYS / ' CNTRL CREF ACTIVE PAYEES......' CNTRL-ACTVE-CREF-PYS / ' CNTRL INACTIVE PAYEES.........' CNTRL-INACTVE-PAYEES
            iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt,NEWLINE," CNTRL TIAA ACTIVE PAYEES......",iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys,NEWLINE," CNTRL CREF ACTIVE PAYEES......",
            iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys,NEWLINE," CNTRL INACTIVE PAYEES.........",iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees);
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  GET NEXT BUSINESS DATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CPWN119
    }
    private void sub_Call_Cpwn119() throws Exception                                                                                                                      //Natural: CALL-CPWN119
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.callnat(Cpwn119.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN119' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Return_Code().notEquals(getZero())))                                                                                    //Natural: IF ERROR-RETURN-CODE NE 0
        {
            getReports().write(0, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                                                 //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, "ERROR IN CALL TO CPWN119 TO DETERMINE BUSINESS DATES ");                                                                               //Natural: WRITE 'ERROR IN CALL TO CPWN119 TO DETERMINE BUSINESS DATES '
            if (Global.isEscape()) return;
            getReports().write(0, "DATE PASSED TO PROGRAM CPWN119: ",iaa_Cntrl_Rcrd_1_Cntrl_Next_Bus_Dte, new ReportEditMask ("MM-DD-YYYY"));                             //Natural: WRITE 'DATE PASSED TO PROGRAM CPWN119: ' IAA-CNTRL-RCRD-1.CNTRL-NEXT-BUS-DTE ( EM = MM-DD-YYYY )
            if (Global.isEscape()) return;
            getReports().write(0, "CPWA115.RETURN-CODE: ",pdaCpwa115.getCpwa115_Error_Return_Code());                                                                     //Natural: WRITE 'CPWA115.RETURN-CODE: ' CPWA115.ERROR-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                                                 //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, "!   1 =  FOR-DATE  ENTERED IS INVALID            ! ");                                                                                 //Natural: WRITE '!   1 =  FOR-DATE  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, "!   2 =  FOR-TIME  ENTERED IS INVALID            ! ");                                                                                 //Natural: WRITE '!   2 =  FOR-TIME  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, "!   3 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! ");                                                                                 //Natural: WRITE '!   3 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! '
            if (Global.isEscape()) return;
            getReports().write(0, "!!  DO NOT PROCEED WITH IAIQ PROCESSING         !! ");                                                                                 //Natural: WRITE '!!  DO NOT PROCEED WITH IAIQ PROCESSING         !! '
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                                                 //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(5),"CONTROL RECORD REPORT FOR TYPE 'DC' FOR BUSINESS DATE:",iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 5X 'CONTROL RECORD REPORT FOR TYPE "DC" FOR BUSINESS DATE:' IAA-CNTRL-RCRD-1.CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY )
                        new ReportEditMask ("MM/DD/YYYY"));
                    getReports().write(1, ReportOption.NOTITLE,"RUN DATE:",Global.getDATX(),new ColumnSpacing(1),"TIME:",Global.getTIMX(),NEWLINE,NEWLINE);               //Natural: WRITE ( 1 ) 'RUN DATE:' *DATX 1X 'TIME:' *TIMX / /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
