/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:30:28 PM
**        * FROM NATURAL PROGRAM : Iaap700
************************************************************
**        * FILE NAME            : Iaap700.java
**        * CLASS NAME           : Iaap700
**        * INSTANCE NAME        : Iaap700
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM    -   IAAP700                                           *
*        01/05    - ADDED LOGIC FOR 3 NEW ORIGIN CODES 48,49 & 50    *
*                   DO SCAN ON 1/05 FOR CHANGES                     *
*
*        12/03    - DO SCAN ON 12/03 CHANGE FOR PEND-CODE T          *
*         5/03    - CHANGES FOR EGTRRA PROCESS                       *
*                 - ADDED OPTION 21 TO TPA ROLL IVC LOGIC            *
*                 - REMOVED OPTION 21 TO TPA ROLL IVC LOGIC  11/03   *
*                   DO SCAN ON 05/03 FOR CHANGES AND 11/03           *
*                                                                    *
*         3/03    - CHANGED ADDED FIELDS TO END OF CPS PMT FILE      *
*                 - FOR SELF REMITTER ORIGIN(42) ON IA               *
*                   DO SCAN ON 03/03 FOR CHANGES                     *
*                                                                    *
*        12/02    - CHANGED TO USED INTL-ACC-NBR FOR                 *
*                 - FORGN & CANAD                                    *
*                   PH-BANK-PYMNT-ACCT-NMBR-R                        *
*                  INTL-BANK-PYMNT-EFT-NMBR USE THIS FOR FOR & CANAD *
*                   DO SCAN ON 12/02 FOR CHANGES                     *
*                                                                    *
*        04/30/02 - CHANGED IAAN0510 TO IAAN051Z OIA CHANGE          *
*        06/26/02 - CHANGED COMPARE LOGIC FOR TPA (IRAT)             *
*                 - TO FIX MISSING IVC FOR CONTRACTS                 *
*                   DO SCAN ON 6/02 FOR CHANGES                      *
*                                                                    *
*        11/01    - CHANGES FOR TO GET SSN FROM COR                  *
*                   DO SCAN ON 11/01 FOR CHANGES                     *
*        10/01    - CHANGES FOR IPRO & P/I ROLLOVER PAYMENTS         *
*                   DO SCAN ON 10/01 FOR CHANGES                     *
*        03/23/01 - TPA ROLLOVER WITH IVC                            *
*        12/04/00 - IPRO CONTRACTS                                   *
*   RM - 01/12/99 - ADDED LOGIC TO HANDLE NEW PA ORIGIN CODES 37 & 38*
*                   DO SCAN ON 1/99 FOR CHANGES                      *
*   KN - 08/07/98 - ADDED 'CT' AND 'CH' PAYMENT/SETTLEMENT TYPES     *
*   KN - 04/15/98 - MADE W0580000 - W0589999 NON-PAR                 *
*   KN - 12/01/97 - MONTHLY UNITS                                    *
*                   ADD CALL TO AIAN026                              *
*   KN - 08/29/97 - FIX TO SET CONTRACT TYPE FOR                     *
*                   GW '00001' THRU '86999'                          *
*                   REMOVE CALL TO AIAN003                           *
*                   CREF AMTS STORED IN FILE                         *
*   KN - 06/25/97 - ACCOUNTING FOR W037000-37999 RANGE               *
*   KN - 04/14/97 - DOWNSTREAM CHANGES FOR PA CONTRACTS              *
*   JT - 11/16/06 - NEW ORGN CODES 43,44,45,46 DO SCAN 11/06         *
*   JT - 03/19/07 - NEW ORGN CODES 39,41,47,51,63,64,67,68 SCAN 3/07 *
*   JT - 04/11/08 - ADDED NEW ROTH FIELDS                  SCAN 4/08 *
*   JT - 06/01/09 - NEW CONTRACT RANGE                     SCAN 6/09 *
*   JT - 07/01/09 - ADDED IAAG700 TO CONTAIN THE COR/NAAD/CPS LDA    *
*                 - USE PAYEE 01 NAAD IF 2ND ANNT IS DECEASED AND NO
*                 - PAYEE 02 NAAD IS PRESENT               SCAN 7/09
*   JT - 09/24/09 - ADDED LOGIC TO FORCE CA FOR RES CDE 96 SCAN 9/09
*   JT - 11/30/09 - FIX LOGIC FOR PAYEE 01 ADDRESS WHEN 2ND ANNT
*                   IS DECEASED * SCAN 11/09
*   JT - 02/23/10 - SVSAA,INDEXED RATE GUARANTEE,PCPOP,IHA/IVA 02/10
*   JT - 10/29/10 - PCPOP - AC LESS THAN 10 YEARS              10/10
*   JT - 01/12/12 - SUNY/CUNY/DATA WAREHOUSE                   01/12
*   JT - 03/28/12 - RATE BASE EXPANSION - RECATALOG - SCAN FOR 3/12
*   JT - 12/07/12 - COR LOOKUP ERROR FOR DEATH OF 2ND - SCAN 122012
*   JT - 05/06/13 - RECATALOG DUE TO CHANGES IN IAAG700 - ADDED ADDRESS
*                   FIELDS TO #NAME. ADDITIONAL CHANGE REQUESTED BY
*                   BUSINESS TO RESET DESTINATION CODE FOR PENDED
*                   CONTRACTS.  SCAN 5/13 FOR CHANGES
*   JT - 09/24/13 - DISALLOW NON-NUMERIC TRANSIT ID. 9/13
*   JT - 12/05/13 - IF PERIODIC IVC > GROSS PAYMENT, MAKE GROSS PAYMENT
*                   = PERIODIC IVC. 12/13
*   JT - 08/29/14 - CHANGED W0290000-W0299999 TO PARTICPATING AND
*                 - W0430000-W0449999 TO NON-PARTICIPATING. 8/14
*   JT - 11/17/14 - ADDED OPTION 21 FOR OVERRIDE FOR PREFIX 'S08'. 11/14
*   JT - 02/27/15 - MODIFY FOR COR/NAAD SUNSET. 02/15
*   RC - 08/2017  - PIN EXPANSION - SC 082017 FOR CHANGES.
*   RC - 08/2020  - REMOVE CHECK OVERRIDE FOR HOLDS.
*                   SC 082020 FOR CHANGES.
*
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap700 extends BLNatBase
{
    // Data Areas
    private GdaIaag700 gdaIaag700;
    private PdaCpwa115 pdaCpwa115;
    private LdaFcpl800 ldaFcpl800;
    private PdaIaaa051z pdaIaaa051z;
    private LdaIaal050 ldaIaal050;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Save_Cor_Cntrct_Payee;

    private DbsGroup pnd_Save_Cor_Cntrct_Payee__R_Field_1;
    private DbsField pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Cntrct;
    private DbsField pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Payee;
    private DbsField pnd_Save_Cor_Ph_Last_Name;
    private DbsField pnd_Save_Cor_Ph_Middle_Name;
    private DbsField pnd_Save_Cor_Ph_First_Name;
    private DbsField pnd_Save_Soc_Sec_Nbr;
    private DbsField pnd_Save_Cor_Status;
    private DbsField pnd_Save_Cor_Dob;
    private DbsField pnd_Hold;

    private DbsGroup pnd_Hold__R_Field_2;
    private DbsField pnd_Hold_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Hold_Cntrct_Payee_Cde;
    private DbsField pnd_Hold_Cntrct_Hold_User_Grp;
    private DbsField pnd_Hold_Cntrct_Hold_User_Id;
    private DbsField pnd_Hold__Filler1;
    private DbsField pnd_Audit_Hold;

    private DbsGroup pnd_Audit_Hold__R_Field_3;
    private DbsField pnd_Audit_Hold_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Audit_Hold__Filler2;
    private DbsField pnd_Audit_Hold_Pnd_Cntrct_Pay_Cde;
    private DbsField pnd_Save_Ppcn;
    private DbsField cmbn_Nbr;

    private DbsGroup cmbn_Nbr__R_Field_4;
    private DbsField cmbn_Nbr_Pnd_Cmbn_Nbr;
    private DbsField pnd_Return_Code;

    private DbsGroup pnd_Control_Rec_Array;
    private DbsField pnd_Control_Rec_Array_Pnd_Control_Rec_Id;

    private DbsGroup pnd_Control_Rec_Array_Pnd_Control_Counts;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Rec_Count;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Cref_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Gross_Amt;
    private DbsField pnd_Control_Rec_Array_Pnd_C_Ded_Amt;
    private DbsField pnd_S_Rec_Count;
    private DbsField pnd_S_Cntrct_Amt;
    private DbsField pnd_S_Gross_Amt;
    private DbsField pnd_S_Dvdnd_Amt;
    private DbsField pnd_S_Cref_Amt;
    private DbsField pnd_S_Ded_Amt;
    private DbsField pnd_T_Rec_Count;
    private DbsField pnd_T_Cntrct_Amt;
    private DbsField pnd_T_Dvdnd_Amt;
    private DbsField pnd_T_Cref_Amt;
    private DbsField pnd_T_Gross_Amt;
    private DbsField pnd_T_Ded_Amt;
    private DbsField pnd_Total_Per_Pymnt_Amt;
    private DbsField pnd_Remain_Ivc;
    private DbsField pnd_Error_Code_1;
    private DbsField pnd_Error_Code_2;
    private DbsField pnd_Bypass;
    private DbsField pnd_New_Fund;
    private DbsField pnd_First;
    private DbsField pnd_Have_Core;
    private DbsField pnd_Input_Eof;
    private DbsField pnd_Core_Eof;
    private DbsField pnd_Hold_Eof;
    private DbsField pnd_Have_Name;
    private DbsField pnd_Name_Eof;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Use_Payee_01;
    private DbsField pnd_Terminated_No_Cmbne;
    private DbsField pnd_Bypass_Record;
    private DbsField pnd_Log_Dte;
    private DbsField pnd_Prev_Cntrct_Payee;
    private DbsField pnd_Save_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Save_Rllvr_Ivc_Ind;
    private DbsField pnd_Save_Rllvr_Elgble_Ind;
    private DbsField pnd_Save_Roth_Dsblty_Dte;
    private DbsField pnd_Save_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Prtcpnt_Dob;
    private DbsField pnd_Save_Cntrct_Payee;

    private DbsGroup pnd_Save_Cntrct_Payee__R_Field_5;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde;
    private DbsField pnd_Save_Cntrct_Record_Code;
    private DbsField pnd_Save_Cntrct_Optn_Cde;
    private DbsField pnd_Save_Cntrct_Orgn_Cde;
    private DbsField pnd_Save_Cntrct_Type_Cde;
    private DbsField pnd_Save_Cntrct_Crrncy_Cde;
    private DbsField pnd_Save_Cntrct_Dvdnd_Pay_Cde;
    private DbsField pnd_Save_Cntrct_Pnsn_Pln_Cde;
    private DbsField pnd_Save_Cntrct_Dob;

    private DbsGroup pnd_Save_Cntrct_Dob__R_Field_6;
    private DbsField pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_Cc;

    private DbsGroup pnd_Save_Cntrct_Dob__R_Field_7;
    private DbsField pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_A;
    private DbsField pnd_Save_Cpr_Cmpny_Cde;
    private DbsField pnd_Save_Cpr_Pay_Mode;
    private DbsField pnd_Save_Status_Cde;
    private DbsField pnd_Save_Cmbne_Cde;
    private DbsField pnd_Save_Cntrct_Ivc_Tot_T;
    private DbsField pnd_Save_Cntrct_Ivc_Tot_C;
    private DbsField pnd_Save_Cntrct_Ivc_Amt_Ik;
    private DbsField pnd_Save_Cntrct_Ivc_Amt_T;
    private DbsField pnd_Save_Cntrct_Ivc_Amt_C;
    private DbsField pnd_Save_Cntrct_Ivc_Used_Amt_T;
    private DbsField pnd_Save_Cntrct_Ivc_Used_Amt_C;
    private DbsField pnd_Save_Cntrct_Hold_Cde;
    private DbsField pnd_Save_Rcvry_Type_Ind_T;
    private DbsField pnd_Save_Rcvry_Type_Ind_C;
    private DbsField pnd_Save_Summ_Cmpny_Cde;
    private DbsField pnd_Save_Summ_Fund_Cde;
    private DbsField pnd_Save_Strt_Dte;
    private DbsField pnd_Save_Stp_Dte;
    private DbsField pnd_Save_Cash_Cde;
    private DbsField pnd_Save_Pend_Cde;
    private DbsField pnd_Save_Curr_Dist_Cde;
    private DbsField pnd_Save_Cntrct_Emp_Trmnt_Cde;
    private DbsField pnd_Save_Cntrct_Fin_Per_Pay_Dte;
    private DbsField pnd_Save_First_Ann_Dod;
    private DbsField pnd_Save_Scnd_Ann_Dod;
    private DbsField pnd_Save_Issue_Dte;

    private DbsGroup pnd_Save_Issue_Dte__R_Field_8;
    private DbsField pnd_Save_Issue_Dte_Pnd_Save_Issue_Dte_A;
    private DbsField pnd_Ws_Issue_Dte;

    private DbsGroup pnd_Ws_Issue_Dte__R_Field_9;
    private DbsField pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Ccyy;
    private DbsField pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm;
    private DbsField pnd_Save_Issue_Dte_Dd;

    private DbsGroup pnd_Save_Issue_Dte_Dd__R_Field_10;
    private DbsField pnd_Save_Issue_Dte_Dd_Pnd_Save_Issue_Dte_Dd_A;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_11;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_12;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_13;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Cc;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_14;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Save_1st_Due_Dte;
    private DbsField pnd_Save_Ppg_Cde;
    private DbsField pnd_Save_Tax_Exmpt_Ind;
    private DbsField pnd_Save_Plan_Nmbr;
    private DbsField pnd_Save_Sub_Plan_Nmbr;
    private DbsField pnd_Save_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Save_Orgntng_Cntrct_Nmbr;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_15;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy;
    private DbsField pnd_C_Indx;
    private DbsField pnd_F_Indx;
    private DbsField pnd_D_Indx;
    private DbsField pnd_Uv_Indx;
    private DbsField pnd_Indx1;
    private DbsField pnd_Ctr;
    private DbsField pnd_Proc_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Bypass_Ctr_Reinv;
    private DbsField pnd_Core_Ctr;
    private DbsField pnd_Hold_Ctr;
    private DbsField pnd_Name_Ctr;
    private DbsField pnd_Cps_Ctr;
    private DbsField pnd_Parm_Fund_1;
    private DbsField pnd_Parm_Fund_2;
    private DbsField pnd_Parm_Rtn_Cde;
    private DbsField pnd_Amount;
    private DbsField pnd_Cref_Amount;
    private DbsField pnd_Hold_Dte;

    private DbsGroup pnd_Hold_Dte__R_Field_16;
    private DbsField pnd_Hold_Dte_Pnd_Hold_Dte_Cc;
    private DbsField pnd_Hold_Dte_Pnd_Hold_Dte_6;
    private DbsField pnd_Hold_Accounting_Date;

    private DbsGroup pnd_Hold_Accounting_Date__R_Field_17;
    private DbsField pnd_Hold_Accounting_Date_Pnd_Hold_Accounting_Date_A;
    private DbsField pnd_Hold_Interface_Date;

    private DbsGroup pnd_Hold_Interface_Date__R_Field_18;
    private DbsField pnd_Hold_Interface_Date_Pnd_Hold_Interface_Date_A;
    private DbsField pnd_Cntrct_Hold_Cde;

    private DbsGroup pnd_Cntrct_Hold_Cde__R_Field_19;
    private DbsField pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1;
    private DbsField pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2;
    private DbsField pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_3;
    private DbsField pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_4;
    private DbsField pnd_Indx2;
    private DbsField pnd_Fund_Ctr;
    private DbsField pnd_Tiaafslash_Cref;
    private DbsField pnd_Date_Out;
    private DbsField pnd_I;
    private DbsField iaan019_Rllvr_Cntrct;
    private DbsField iaan019_Lob_Prdct_Cde;
    private DbsField iaan019_Ret_Cde;

    private DbsGroup pnd_Input_Record_Work_9;
    private DbsField pnd_Input_Record_Work_9_Pnd_Call_Type;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaIaag700 = GdaIaag700.getInstance(getCallnatLevel());
        registerRecord(gdaIaag700);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCpwa115 = new PdaCpwa115(localVariables);
        ldaFcpl800 = new LdaFcpl800();
        registerRecord(ldaFcpl800);
        pdaIaaa051z = new PdaIaaa051z(localVariables);
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);

        // Local Variables
        pnd_Save_Cor_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Save_Cor_Cntrct_Payee", "#SAVE-COR-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Save_Cor_Cntrct_Payee__R_Field_1 = localVariables.newGroupInRecord("pnd_Save_Cor_Cntrct_Payee__R_Field_1", "REDEFINE", pnd_Save_Cor_Cntrct_Payee);
        pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Cntrct = pnd_Save_Cor_Cntrct_Payee__R_Field_1.newFieldInGroup("pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Cntrct", 
            "#SAVE-COR-CNTRCT", FieldType.STRING, 10);
        pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Payee = pnd_Save_Cor_Cntrct_Payee__R_Field_1.newFieldInGroup("pnd_Save_Cor_Cntrct_Payee_Pnd_Save_Cor_Payee", 
            "#SAVE-COR-PAYEE", FieldType.NUMERIC, 2);
        pnd_Save_Cor_Ph_Last_Name = localVariables.newFieldInRecord("pnd_Save_Cor_Ph_Last_Name", "#SAVE-COR-PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Save_Cor_Ph_Middle_Name = localVariables.newFieldInRecord("pnd_Save_Cor_Ph_Middle_Name", "#SAVE-COR-PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Save_Cor_Ph_First_Name = localVariables.newFieldInRecord("pnd_Save_Cor_Ph_First_Name", "#SAVE-COR-PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Save_Soc_Sec_Nbr = localVariables.newFieldInRecord("pnd_Save_Soc_Sec_Nbr", "#SAVE-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Save_Cor_Status = localVariables.newFieldInRecord("pnd_Save_Cor_Status", "#SAVE-COR-STATUS", FieldType.STRING, 1);
        pnd_Save_Cor_Dob = localVariables.newFieldInRecord("pnd_Save_Cor_Dob", "#SAVE-COR-DOB", FieldType.NUMERIC, 8);
        pnd_Hold = localVariables.newFieldInRecord("pnd_Hold", "#HOLD", FieldType.STRING, 82);

        pnd_Hold__R_Field_2 = localVariables.newGroupInRecord("pnd_Hold__R_Field_2", "REDEFINE", pnd_Hold);
        pnd_Hold_Cntrct_Ppcn_Nbr = pnd_Hold__R_Field_2.newFieldInGroup("pnd_Hold_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Hold_Cntrct_Payee_Cde = pnd_Hold__R_Field_2.newFieldInGroup("pnd_Hold_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Hold_Cntrct_Hold_User_Grp = pnd_Hold__R_Field_2.newFieldInGroup("pnd_Hold_Cntrct_Hold_User_Grp", "CNTRCT-HOLD-USER-GRP", FieldType.STRING, 
            3);
        pnd_Hold_Cntrct_Hold_User_Id = pnd_Hold__R_Field_2.newFieldInGroup("pnd_Hold_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", FieldType.STRING, 3);
        pnd_Hold__Filler1 = pnd_Hold__R_Field_2.newFieldInGroup("pnd_Hold__Filler1", "_FILLER1", FieldType.STRING, 64);
        pnd_Audit_Hold = localVariables.newFieldInRecord("pnd_Audit_Hold", "#AUDIT-HOLD", FieldType.STRING, 12);

        pnd_Audit_Hold__R_Field_3 = localVariables.newGroupInRecord("pnd_Audit_Hold__R_Field_3", "REDEFINE", pnd_Audit_Hold);
        pnd_Audit_Hold_Pnd_Cntrct_Ppcn_Nbr = pnd_Audit_Hold__R_Field_3.newFieldInGroup("pnd_Audit_Hold_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Audit_Hold__Filler2 = pnd_Audit_Hold__R_Field_3.newFieldInGroup("pnd_Audit_Hold__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Audit_Hold_Pnd_Cntrct_Pay_Cde = pnd_Audit_Hold__R_Field_3.newFieldInGroup("pnd_Audit_Hold_Pnd_Cntrct_Pay_Cde", "#CNTRCT-PAY-CDE", FieldType.NUMERIC, 
            2);
        pnd_Save_Ppcn = localVariables.newFieldInRecord("pnd_Save_Ppcn", "#SAVE-PPCN", FieldType.STRING, 10);
        cmbn_Nbr = localVariables.newFieldInRecord("cmbn_Nbr", "CMBN-NBR", FieldType.STRING, 14);

        cmbn_Nbr__R_Field_4 = localVariables.newGroupInRecord("cmbn_Nbr__R_Field_4", "REDEFINE", cmbn_Nbr);
        cmbn_Nbr_Pnd_Cmbn_Nbr = cmbn_Nbr__R_Field_4.newFieldInGroup("cmbn_Nbr_Pnd_Cmbn_Nbr", "#CMBN-NBR", FieldType.STRING, 8);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);

        pnd_Control_Rec_Array = localVariables.newGroupInRecord("pnd_Control_Rec_Array", "#CONTROL-REC-ARRAY");
        pnd_Control_Rec_Array_Pnd_Control_Rec_Id = pnd_Control_Rec_Array.newFieldInGroup("pnd_Control_Rec_Array_Pnd_Control_Rec_Id", "#CONTROL-REC-ID", 
            FieldType.STRING, 6);

        pnd_Control_Rec_Array_Pnd_Control_Counts = pnd_Control_Rec_Array.newGroupArrayInGroup("pnd_Control_Rec_Array_Pnd_Control_Counts", "#CONTROL-COUNTS", 
            new DbsArrayController(1, 19));
        pnd_Control_Rec_Array_Pnd_C_Rec_Count = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Rec_Count", "#C-REC-COUNT", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt", "#C-CNTRCT-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt", "#C-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Cref_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Cref_Amt", "#C-CREF-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Gross_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Gross_Amt", "#C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Rec_Array_Pnd_C_Ded_Amt = pnd_Control_Rec_Array_Pnd_Control_Counts.newFieldInGroup("pnd_Control_Rec_Array_Pnd_C_Ded_Amt", "#C-DED-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_S_Rec_Count = localVariables.newFieldArrayInRecord("pnd_S_Rec_Count", "#S-REC-COUNT", FieldType.PACKED_DECIMAL, 6, new DbsArrayController(1, 
            2));
        pnd_S_Cntrct_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cntrct_Amt", "#S-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Gross_Amt = localVariables.newFieldArrayInRecord("pnd_S_Gross_Amt", "#S-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Dvdnd_Amt = localVariables.newFieldArrayInRecord("pnd_S_Dvdnd_Amt", "#S-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Cref_Amt = localVariables.newFieldArrayInRecord("pnd_S_Cref_Amt", "#S-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_S_Ded_Amt = localVariables.newFieldArrayInRecord("pnd_S_Ded_Amt", "#S-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_T_Rec_Count = localVariables.newFieldInRecord("pnd_T_Rec_Count", "#T-REC-COUNT", FieldType.PACKED_DECIMAL, 6);
        pnd_T_Cntrct_Amt = localVariables.newFieldInRecord("pnd_T_Cntrct_Amt", "#T-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_T_Dvdnd_Amt", "#T-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Cref_Amt = localVariables.newFieldInRecord("pnd_T_Cref_Amt", "#T-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Gross_Amt = localVariables.newFieldInRecord("pnd_T_Gross_Amt", "#T-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Ded_Amt = localVariables.newFieldInRecord("pnd_T_Ded_Amt", "#T-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Per_Pymnt_Amt = localVariables.newFieldInRecord("pnd_Total_Per_Pymnt_Amt", "#TOTAL-PER-PYMNT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Remain_Ivc = localVariables.newFieldInRecord("pnd_Remain_Ivc", "#REMAIN-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Error_Code_1 = localVariables.newFieldArrayInRecord("pnd_Error_Code_1", "#ERROR-CODE-1", FieldType.STRING, 1, new DbsArrayController(1, 9));
        pnd_Error_Code_2 = localVariables.newFieldArrayInRecord("pnd_Error_Code_2", "#ERROR-CODE-2", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_New_Fund = localVariables.newFieldInRecord("pnd_New_Fund", "#NEW-FUND", FieldType.BOOLEAN, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Have_Core = localVariables.newFieldInRecord("pnd_Have_Core", "#HAVE-CORE", FieldType.BOOLEAN, 1);
        pnd_Input_Eof = localVariables.newFieldInRecord("pnd_Input_Eof", "#INPUT-EOF", FieldType.BOOLEAN, 1);
        pnd_Core_Eof = localVariables.newFieldInRecord("pnd_Core_Eof", "#CORE-EOF", FieldType.BOOLEAN, 1);
        pnd_Hold_Eof = localVariables.newFieldInRecord("pnd_Hold_Eof", "#HOLD-EOF", FieldType.BOOLEAN, 1);
        pnd_Have_Name = localVariables.newFieldInRecord("pnd_Have_Name", "#HAVE-NAME", FieldType.BOOLEAN, 1);
        pnd_Name_Eof = localVariables.newFieldInRecord("pnd_Name_Eof", "#NAME-EOF", FieldType.BOOLEAN, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Use_Payee_01 = localVariables.newFieldInRecord("pnd_Use_Payee_01", "#USE-PAYEE-01", FieldType.BOOLEAN, 1);
        pnd_Terminated_No_Cmbne = localVariables.newFieldInRecord("pnd_Terminated_No_Cmbne", "#TERMINATED-NO-CMBNE", FieldType.BOOLEAN, 1);
        pnd_Bypass_Record = localVariables.newFieldInRecord("pnd_Bypass_Record", "#BYPASS-RECORD", FieldType.STRING, 1);
        pnd_Log_Dte = localVariables.newFieldInRecord("pnd_Log_Dte", "#LOG-DTE", FieldType.STRING, 15);
        pnd_Prev_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Prev_Cntrct_Payee", "#PREV-CNTRCT-PAYEE", FieldType.STRING, 12);
        pnd_Save_Rllvr_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Save_Rllvr_Cntrct_Nbr", "#SAVE-RLLVR-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Save_Rllvr_Ivc_Ind = localVariables.newFieldInRecord("pnd_Save_Rllvr_Ivc_Ind", "#SAVE-RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Save_Rllvr_Elgble_Ind = localVariables.newFieldInRecord("pnd_Save_Rllvr_Elgble_Ind", "#SAVE-RLLVR-ELGBLE-IND", FieldType.STRING, 1);
        pnd_Save_Roth_Dsblty_Dte = localVariables.newFieldInRecord("pnd_Save_Roth_Dsblty_Dte", "#SAVE-ROTH-DSBLTY-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Roth_Frst_Cntrb_Dte = localVariables.newFieldInRecord("pnd_Save_Roth_Frst_Cntrb_Dte", "#SAVE-ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 
            8);
        pnd_Prtcpnt_Dob = localVariables.newFieldInRecord("pnd_Prtcpnt_Dob", "#PRTCPNT-DOB", FieldType.NUMERIC, 8);
        pnd_Save_Cntrct_Payee = localVariables.newFieldInRecord("pnd_Save_Cntrct_Payee", "#SAVE-CNTRCT-PAYEE", FieldType.STRING, 12);

        pnd_Save_Cntrct_Payee__R_Field_5 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Payee__R_Field_5", "REDEFINE", pnd_Save_Cntrct_Payee);
        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr = pnd_Save_Cntrct_Payee__R_Field_5.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde = pnd_Save_Cntrct_Payee__R_Field_5.newFieldInGroup("pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Record_Code = localVariables.newFieldInRecord("pnd_Save_Cntrct_Record_Code", "#SAVE-CNTRCT-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Optn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Optn_Cde", "#SAVE-CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Save_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Orgn_Cde", "#SAVE-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Save_Cntrct_Type_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Type_Cde", "#SAVE-CNTRCT-TYPE-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Crrncy_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Crrncy_Cde", "#SAVE-CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Save_Cntrct_Dvdnd_Pay_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Dvdnd_Pay_Cde", "#SAVE-CNTRCT-DVDND-PAY-CDE", FieldType.STRING, 
            5);
        pnd_Save_Cntrct_Pnsn_Pln_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Pnsn_Pln_Cde", "#SAVE-CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1);
        pnd_Save_Cntrct_Dob = localVariables.newFieldInRecord("pnd_Save_Cntrct_Dob", "#SAVE-CNTRCT-DOB", FieldType.NUMERIC, 8);

        pnd_Save_Cntrct_Dob__R_Field_6 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Dob__R_Field_6", "REDEFINE", pnd_Save_Cntrct_Dob);
        pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_Cc = pnd_Save_Cntrct_Dob__R_Field_6.newFieldInGroup("pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_Cc", "#SAVE-CNTRCT-DOB-CC", 
            FieldType.NUMERIC, 2);

        pnd_Save_Cntrct_Dob__R_Field_7 = localVariables.newGroupInRecord("pnd_Save_Cntrct_Dob__R_Field_7", "REDEFINE", pnd_Save_Cntrct_Dob);
        pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_A = pnd_Save_Cntrct_Dob__R_Field_7.newFieldInGroup("pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_A", "#SAVE-CNTRCT-DOB-A", 
            FieldType.STRING, 8);
        pnd_Save_Cpr_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Save_Cpr_Cmpny_Cde", "#SAVE-CPR-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Cpr_Pay_Mode = localVariables.newFieldInRecord("pnd_Save_Cpr_Pay_Mode", "#SAVE-CPR-PAY-MODE", FieldType.NUMERIC, 3);
        pnd_Save_Status_Cde = localVariables.newFieldInRecord("pnd_Save_Status_Cde", "#SAVE-STATUS-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Cmbne_Cde = localVariables.newFieldInRecord("pnd_Save_Cmbne_Cde", "#SAVE-CMBNE-CDE", FieldType.STRING, 12);
        pnd_Save_Cntrct_Ivc_Tot_T = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Tot_T", "#SAVE-CNTRCT-IVC-TOT-T", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Save_Cntrct_Ivc_Tot_C = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Tot_C", "#SAVE-CNTRCT-IVC-TOT-C", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Save_Cntrct_Ivc_Amt_Ik = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Amt_Ik", "#SAVE-CNTRCT-IVC-AMT-IK", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Save_Cntrct_Ivc_Amt_T = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Amt_T", "#SAVE-CNTRCT-IVC-AMT-T", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Save_Cntrct_Ivc_Amt_C = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Amt_C", "#SAVE-CNTRCT-IVC-AMT-C", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Save_Cntrct_Ivc_Used_Amt_T = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Used_Amt_T", "#SAVE-CNTRCT-IVC-USED-AMT-T", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Save_Cntrct_Ivc_Used_Amt_C = localVariables.newFieldInRecord("pnd_Save_Cntrct_Ivc_Used_Amt_C", "#SAVE-CNTRCT-IVC-USED-AMT-C", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Save_Cntrct_Hold_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Hold_Cde", "#SAVE-CNTRCT-HOLD-CDE", FieldType.STRING, 1);
        pnd_Save_Rcvry_Type_Ind_T = localVariables.newFieldInRecord("pnd_Save_Rcvry_Type_Ind_T", "#SAVE-RCVRY-TYPE-IND-T", FieldType.STRING, 1);
        pnd_Save_Rcvry_Type_Ind_C = localVariables.newFieldInRecord("pnd_Save_Rcvry_Type_Ind_C", "#SAVE-RCVRY-TYPE-IND-C", FieldType.STRING, 1);
        pnd_Save_Summ_Cmpny_Cde = localVariables.newFieldInRecord("pnd_Save_Summ_Cmpny_Cde", "#SAVE-SUMM-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Save_Summ_Fund_Cde = localVariables.newFieldInRecord("pnd_Save_Summ_Fund_Cde", "#SAVE-SUMM-FUND-CDE", FieldType.STRING, 2);
        pnd_Save_Strt_Dte = localVariables.newFieldInRecord("pnd_Save_Strt_Dte", "#SAVE-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Stp_Dte = localVariables.newFieldInRecord("pnd_Save_Stp_Dte", "#SAVE-STP-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Cash_Cde = localVariables.newFieldInRecord("pnd_Save_Cash_Cde", "#SAVE-CASH-CDE", FieldType.STRING, 1);
        pnd_Save_Pend_Cde = localVariables.newFieldInRecord("pnd_Save_Pend_Cde", "#SAVE-PEND-CDE", FieldType.STRING, 1);
        pnd_Save_Curr_Dist_Cde = localVariables.newFieldInRecord("pnd_Save_Curr_Dist_Cde", "#SAVE-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Save_Cntrct_Emp_Trmnt_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Emp_Trmnt_Cde", "#SAVE-CNTRCT-EMP-TRMNT-CDE", FieldType.STRING, 
            1);
        pnd_Save_Cntrct_Fin_Per_Pay_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_Fin_Per_Pay_Dte", "#SAVE-CNTRCT-FIN-PER-PAY-DTE", FieldType.NUMERIC, 
            6);
        pnd_Save_First_Ann_Dod = localVariables.newFieldInRecord("pnd_Save_First_Ann_Dod", "#SAVE-FIRST-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Scnd_Ann_Dod = localVariables.newFieldInRecord("pnd_Save_Scnd_Ann_Dod", "#SAVE-SCND-ANN-DOD", FieldType.PACKED_DECIMAL, 6);
        pnd_Save_Issue_Dte = localVariables.newFieldInRecord("pnd_Save_Issue_Dte", "#SAVE-ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Save_Issue_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Save_Issue_Dte__R_Field_8", "REDEFINE", pnd_Save_Issue_Dte);
        pnd_Save_Issue_Dte_Pnd_Save_Issue_Dte_A = pnd_Save_Issue_Dte__R_Field_8.newFieldInGroup("pnd_Save_Issue_Dte_Pnd_Save_Issue_Dte_A", "#SAVE-ISSUE-DTE-A", 
            FieldType.STRING, 6);
        pnd_Ws_Issue_Dte = localVariables.newFieldInRecord("pnd_Ws_Issue_Dte", "#WS-ISSUE-DTE", FieldType.NUMERIC, 6);

        pnd_Ws_Issue_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Issue_Dte__R_Field_9", "REDEFINE", pnd_Ws_Issue_Dte);
        pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Ccyy = pnd_Ws_Issue_Dte__R_Field_9.newFieldInGroup("pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Ccyy", "#WS-ISSUE-DTE-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm = pnd_Ws_Issue_Dte__R_Field_9.newFieldInGroup("pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm", "#WS-ISSUE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Save_Issue_Dte_Dd = localVariables.newFieldInRecord("pnd_Save_Issue_Dte_Dd", "#SAVE-ISSUE-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Save_Issue_Dte_Dd__R_Field_10 = localVariables.newGroupInRecord("pnd_Save_Issue_Dte_Dd__R_Field_10", "REDEFINE", pnd_Save_Issue_Dte_Dd);
        pnd_Save_Issue_Dte_Dd_Pnd_Save_Issue_Dte_Dd_A = pnd_Save_Issue_Dte_Dd__R_Field_10.newFieldInGroup("pnd_Save_Issue_Dte_Dd_Pnd_Save_Issue_Dte_Dd_A", 
            "#SAVE-ISSUE-DTE-DD-A", FieldType.STRING, 2);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_11", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_11.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_12 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_12", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 
            4);

        pnd_Save_Check_Dte_A__R_Field_13 = pnd_Save_Check_Dte_A__R_Field_12.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_13", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Yyyy);
        pnd_Save_Check_Dte_A_Pnd_Cc = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Yy = pnd_Save_Check_Dte_A__R_Field_13.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_12.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_14 = pnd_Save_Check_Dte_A__R_Field_11.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_14", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_14.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Save_1st_Due_Dte = localVariables.newFieldInRecord("pnd_Save_1st_Due_Dte", "#SAVE-1ST-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Save_Ppg_Cde = localVariables.newFieldInRecord("pnd_Save_Ppg_Cde", "#SAVE-PPG-CDE", FieldType.STRING, 5);
        pnd_Save_Tax_Exmpt_Ind = localVariables.newFieldInRecord("pnd_Save_Tax_Exmpt_Ind", "#SAVE-TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Save_Plan_Nmbr = localVariables.newFieldInRecord("pnd_Save_Plan_Nmbr", "#SAVE-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Save_Sub_Plan_Nmbr = localVariables.newFieldInRecord("pnd_Save_Sub_Plan_Nmbr", "#SAVE-SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Save_Orgntng_Sub_Plan_Nmbr = localVariables.newFieldInRecord("pnd_Save_Orgntng_Sub_Plan_Nmbr", "#SAVE-ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6);
        pnd_Save_Orgntng_Cntrct_Nmbr = localVariables.newFieldInRecord("pnd_Save_Orgntng_Cntrct_Nmbr", "#SAVE-ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 8);

        pnd_Payment_Due_Dte__R_Field_15 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_15", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_15.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_15.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_15.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_15.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy = pnd_Payment_Due_Dte__R_Field_15.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yy", "#PAYMENT-DUE-YY", 
            FieldType.NUMERIC, 2);
        pnd_C_Indx = localVariables.newFieldInRecord("pnd_C_Indx", "#C-INDX", FieldType.NUMERIC, 2);
        pnd_F_Indx = localVariables.newFieldInRecord("pnd_F_Indx", "#F-INDX", FieldType.NUMERIC, 3);
        pnd_D_Indx = localVariables.newFieldInRecord("pnd_D_Indx", "#D-INDX", FieldType.NUMERIC, 3);
        pnd_Uv_Indx = localVariables.newFieldInRecord("pnd_Uv_Indx", "#UV-INDX", FieldType.NUMERIC, 3);
        pnd_Indx1 = localVariables.newFieldInRecord("pnd_Indx1", "#INDX1", FieldType.NUMERIC, 3);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Proc_Ctr = localVariables.newFieldInRecord("pnd_Proc_Ctr", "#PROC-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr_Reinv = localVariables.newFieldInRecord("pnd_Bypass_Ctr_Reinv", "#BYPASS-CTR-REINV", FieldType.PACKED_DECIMAL, 9);
        pnd_Core_Ctr = localVariables.newFieldInRecord("pnd_Core_Ctr", "#CORE-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Hold_Ctr = localVariables.newFieldInRecord("pnd_Hold_Ctr", "#HOLD-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Name_Ctr = localVariables.newFieldInRecord("pnd_Name_Ctr", "#NAME-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Cps_Ctr = localVariables.newFieldInRecord("pnd_Cps_Ctr", "#CPS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Parm_Fund_1 = localVariables.newFieldInRecord("pnd_Parm_Fund_1", "#PARM-FUND-1", FieldType.STRING, 1);
        pnd_Parm_Fund_2 = localVariables.newFieldInRecord("pnd_Parm_Fund_2", "#PARM-FUND-2", FieldType.STRING, 2);
        pnd_Parm_Rtn_Cde = localVariables.newFieldInRecord("pnd_Parm_Rtn_Cde", "#PARM-RTN-CDE", FieldType.NUMERIC, 2);
        pnd_Amount = localVariables.newFieldInRecord("pnd_Amount", "#AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Amount = localVariables.newFieldInRecord("pnd_Cref_Amount", "#CREF-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Hold_Dte = localVariables.newFieldInRecord("pnd_Hold_Dte", "#HOLD-DTE", FieldType.STRING, 8);

        pnd_Hold_Dte__R_Field_16 = localVariables.newGroupInRecord("pnd_Hold_Dte__R_Field_16", "REDEFINE", pnd_Hold_Dte);
        pnd_Hold_Dte_Pnd_Hold_Dte_Cc = pnd_Hold_Dte__R_Field_16.newFieldInGroup("pnd_Hold_Dte_Pnd_Hold_Dte_Cc", "#HOLD-DTE-CC", FieldType.STRING, 2);
        pnd_Hold_Dte_Pnd_Hold_Dte_6 = pnd_Hold_Dte__R_Field_16.newFieldInGroup("pnd_Hold_Dte_Pnd_Hold_Dte_6", "#HOLD-DTE-6", FieldType.STRING, 6);
        pnd_Hold_Accounting_Date = localVariables.newFieldInRecord("pnd_Hold_Accounting_Date", "#HOLD-ACCOUNTING-DATE", FieldType.NUMERIC, 8);

        pnd_Hold_Accounting_Date__R_Field_17 = localVariables.newGroupInRecord("pnd_Hold_Accounting_Date__R_Field_17", "REDEFINE", pnd_Hold_Accounting_Date);
        pnd_Hold_Accounting_Date_Pnd_Hold_Accounting_Date_A = pnd_Hold_Accounting_Date__R_Field_17.newFieldInGroup("pnd_Hold_Accounting_Date_Pnd_Hold_Accounting_Date_A", 
            "#HOLD-ACCOUNTING-DATE-A", FieldType.STRING, 8);
        pnd_Hold_Interface_Date = localVariables.newFieldInRecord("pnd_Hold_Interface_Date", "#HOLD-INTERFACE-DATE", FieldType.NUMERIC, 8);

        pnd_Hold_Interface_Date__R_Field_18 = localVariables.newGroupInRecord("pnd_Hold_Interface_Date__R_Field_18", "REDEFINE", pnd_Hold_Interface_Date);
        pnd_Hold_Interface_Date_Pnd_Hold_Interface_Date_A = pnd_Hold_Interface_Date__R_Field_18.newFieldInGroup("pnd_Hold_Interface_Date_Pnd_Hold_Interface_Date_A", 
            "#HOLD-INTERFACE-DATE-A", FieldType.STRING, 8);
        pnd_Cntrct_Hold_Cde = localVariables.newFieldInRecord("pnd_Cntrct_Hold_Cde", "#CNTRCT-HOLD-CDE", FieldType.STRING, 4);

        pnd_Cntrct_Hold_Cde__R_Field_19 = localVariables.newGroupInRecord("pnd_Cntrct_Hold_Cde__R_Field_19", "REDEFINE", pnd_Cntrct_Hold_Cde);
        pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1 = pnd_Cntrct_Hold_Cde__R_Field_19.newFieldInGroup("pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1", "#CNTRCT-HOLD-CDE-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2 = pnd_Cntrct_Hold_Cde__R_Field_19.newFieldInGroup("pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2", "#CNTRCT-HOLD-CDE-2", 
            FieldType.STRING, 1);
        pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_3 = pnd_Cntrct_Hold_Cde__R_Field_19.newFieldInGroup("pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_3", "#CNTRCT-HOLD-CDE-3", 
            FieldType.STRING, 1);
        pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_4 = pnd_Cntrct_Hold_Cde__R_Field_19.newFieldInGroup("pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_4", "#CNTRCT-HOLD-CDE-4", 
            FieldType.STRING, 1);
        pnd_Indx2 = localVariables.newFieldInRecord("pnd_Indx2", "#INDX2", FieldType.NUMERIC, 2);
        pnd_Fund_Ctr = localVariables.newFieldInRecord("pnd_Fund_Ctr", "#FUND-CTR", FieldType.NUMERIC, 2);
        pnd_Tiaafslash_Cref = localVariables.newFieldInRecord("pnd_Tiaafslash_Cref", "#TIAA/CREF", FieldType.STRING, 1);
        pnd_Date_Out = localVariables.newFieldInRecord("pnd_Date_Out", "#DATE-OUT", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        iaan019_Rllvr_Cntrct = localVariables.newFieldInRecord("iaan019_Rllvr_Cntrct", "IAAN019-RLLVR-CNTRCT", FieldType.STRING, 10);
        iaan019_Lob_Prdct_Cde = localVariables.newFieldInRecord("iaan019_Lob_Prdct_Cde", "IAAN019-LOB-PRDCT-CDE", FieldType.STRING, 10);
        iaan019_Ret_Cde = localVariables.newFieldInRecord("iaan019_Ret_Cde", "IAAN019-RET-CDE", FieldType.NUMERIC, 2);

        pnd_Input_Record_Work_9 = localVariables.newGroupInRecord("pnd_Input_Record_Work_9", "#INPUT-RECORD-WORK-9");
        pnd_Input_Record_Work_9_Pnd_Call_Type = pnd_Input_Record_Work_9.newFieldInGroup("pnd_Input_Record_Work_9_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl800.initializeValues();
        ldaIaal050.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap700() throws Exception
    {
        super("Iaap700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP700", onError);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT LS = 133 PS = 60
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 133 PS = 0
                                                                                                                                                                          //Natural: FORMAT ( 2 ) LS = 133 PS = 56;//Natural: PERFORM INITIALIZATION
        sub_Initialization();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT
        while (condition(getWorkFiles().read(1, gdaIaag700.getPnd_Input())))
        {
            if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("   CHEADER")))                                                                                   //Natural: IF #PPCN-NBR = '   CHEADER'
            {
                pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_Header_Chk_Dte());                                                           //Natural: ASSIGN #SAVE-CHECK-DTE := #HEADER-CHK-DTE
                pdaCpwa115.getCpwa115_For_Date().setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                                       //Natural: ASSIGN CPWA115.FOR-DATE := #SAVE-CHECK-DTE
                pdaCpwa115.getCpwa115_For_Time().setValue(100000);                                                                                                        //Natural: ASSIGN CPWA115.FOR-TIME := 0100000
                DbsUtil.callnat(Cpwn119.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                       //Natural: CALLNAT 'CPWN119' CPWA115
                if (condition(Global.isEscape())) return;
                if (condition(pdaCpwa115.getCpwa115_Error_Code().equals(1)))                                                                                              //Natural: IF ERROR-CODE = 1
                {
                    getReports().write(0, "DATE NOT IN TABLE: ",pdaCpwa115.getCpwa115_For_Date());                                                                        //Natural: WRITE 'DATE NOT IN TABLE: ' CPWA115.FOR-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaCpwa115.getCpwa115_Error_Code().equals(2)))                                                                                              //Natural: IF ERROR-CODE = 2
                {
                    getReports().write(0, "INVALID TIME VALUE: ",pdaCpwa115.getCpwa115_For_Time());                                                                       //Natural: WRITE 'INVALID TIME VALUE: ' CPWA115.FOR-TIME
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("   FHEADER") || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("   SHEADER") ||                   //Natural: IF #PPCN-NBR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("99CTRAILER") || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("99FTRAILER") || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals("99STRAILER")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            //*  BYPASS ANY TPA RINV     3/2001
            //*  ACTIVE CONTRACTS
            if (condition((((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(20) && (pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(30)))        //Natural: IF ( #RECORD-CODE = 20 ) AND ( #SAVE-CNTRCT-OPTN-CDE = 28 OR = 30 ) AND ( #INPUT.#CURR-DIST-CDE = 'RINV' ) AND ( #INPUT.#STATUS-CDE NE 9 )
                && gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("RINV")) && gdaIaag700.getPnd_Input_Pnd_Status_Cde().notEquals(9))))
            {
                pnd_Bypass_Record.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #BYPASS-RECORD
                pnd_Bypass_Ctr_Reinv.nadd(1);                                                                                                                             //Natural: ADD 1 TO #BYPASS-CTR-REINV
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(20)))                                                                                      //Natural: IF #RECORD-CODE = 20
                {
                    pnd_Bypass_Record.setValue("N");                                                                                                                      //Natural: MOVE 'N' TO #BYPASS-RECORD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Bypass_Record.equals("Y") && ((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(30)) || (gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(40))))) //Natural: IF #BYPASS-RECORD = 'Y' AND ( ( #RECORD-CODE = 30 ) OR ( #RECORD-CODE = 40 ) )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   CONTRACT RECORD
            short decideConditionsMet1520 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #RECORD-CODE;//Natural: VALUE 10
            if (condition((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(10))))
            {
                decideConditionsMet1520++;
                pnd_Save_Cntrct_Dvdnd_Pay_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Div_Coll_Cde());                                                                       //Natural: ASSIGN #SAVE-CNTRCT-DVDND-PAY-CDE := #INPUT.#DIV-COLL-CDE
                pnd_Save_Cntrct_Pnsn_Pln_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Pnsn_Pln_Cde());                                                                        //Natural: ASSIGN #SAVE-CNTRCT-PNSN-PLN-CDE := #INPUT.#PNSN-PLN-CDE
                pnd_Save_Cntrct_Optn_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Optn_Cde());                                                                                //Natural: ASSIGN #SAVE-CNTRCT-OPTN-CDE := #INPUT.#OPTN-CDE
                pnd_Save_Cntrct_Orgn_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Orgn_Cde());                                                                                //Natural: ASSIGN #SAVE-CNTRCT-ORGN-CDE := #INPUT.#ORGN-CDE
                pnd_Save_Cntrct_Crrncy_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Crrncy_Cde_A());                                                                          //Natural: ASSIGN #SAVE-CNTRCT-CRRNCY-CDE := #INPUT.#CRRNCY-CDE-A
                pnd_Save_Issue_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_Issue_Dte());                                                                                     //Natural: ASSIGN #SAVE-ISSUE-DTE := #INPUT.#ISSUE-DTE
                pnd_Save_Issue_Dte_Dd.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Issue_Dte_Dd());                                                                        //Natural: ASSIGN #SAVE-ISSUE-DTE-DD := #INPUT.#CNTRCT-ISSUE-DTE-DD
                if (condition(pnd_Save_Issue_Dte_Dd.equals(getZero())))                                                                                                   //Natural: IF #SAVE-ISSUE-DTE-DD = 0
                {
                    pnd_Save_Issue_Dte_Dd.setValue(1);                                                                                                                    //Natural: ASSIGN #SAVE-ISSUE-DTE-DD := 01
                    //*  ROTH 4/08
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_1st_Due_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_1st_Due_Dte());                                                                                 //Natural: ASSIGN #SAVE-1ST-DUE-DTE := #INPUT.#1ST-DUE-DTE
                pnd_Save_Cntrct_Type_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Type_Cde());                                                                                //Natural: ASSIGN #SAVE-CNTRCT-TYPE-CDE := #INPUT.#TYPE-CDE
                pnd_Save_First_Ann_Dod.setValue(gdaIaag700.getPnd_Input_Pnd_First_Ann_Dod());                                                                             //Natural: ASSIGN #SAVE-FIRST-ANN-DOD := #INPUT.#FIRST-ANN-DOD
                pnd_Save_Scnd_Ann_Dod.setValue(gdaIaag700.getPnd_Input_Pnd_Scnd_Ann_Dod());                                                                               //Natural: ASSIGN #SAVE-SCND-ANN-DOD := #INPUT.#SCND-ANN-DOD
                pnd_Prtcpnt_Dob.setValue(gdaIaag700.getPnd_Input_Pnd_First_Ann_Dob());                                                                                    //Natural: ASSIGN #PRTCPNT-DOB := #INPUT.#FIRST-ANN-DOB
                if (condition(gdaIaag700.getPnd_Input_Pnd_First_Ann_Dod().greater(getZero())))                                                                            //Natural: IF #INPUT.#FIRST-ANN-DOD GT 0
                {
                    pnd_Save_Cntrct_Dob.setValue(gdaIaag700.getPnd_Input_Pnd_Scnd_Ann_Dob());                                                                             //Natural: ASSIGN #SAVE-CNTRCT-DOB := #INPUT.#SCND-ANN-DOB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Save_Cntrct_Dob.setValue(gdaIaag700.getPnd_Input_Pnd_First_Ann_Dob());                                                                            //Natural: ASSIGN #SAVE-CNTRCT-DOB := #INPUT.#FIRST-ANN-DOB
                    //*  4/08
                    //*  02/10
                    //*  02/10
                    //*  02/10
                    //*  01/12
                    //*  01/12
                    //*  01/12
                    //*   CPR  RECORD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_Roth_Frst_Cntrb_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_Roth_Frst_Cntrb_Dte());                                                                 //Natural: ASSIGN #SAVE-ROTH-FRST-CNTRB-DTE := #INPUT.#ROTH-FRST-CNTRB-DTE
                pnd_Save_Ppg_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Ppg_Cde());                                                                                         //Natural: ASSIGN #SAVE-PPG-CDE := #INPUT.#PPG-CDE
                pnd_Save_Tax_Exmpt_Ind.setValue(gdaIaag700.getPnd_Input_Pnd_Tax_Exmpt_Ind());                                                                             //Natural: ASSIGN #SAVE-TAX-EXMPT-IND := #INPUT.#TAX-EXMPT-IND
                pnd_Save_Plan_Nmbr.setValue(gdaIaag700.getPnd_Input_Pnd_Plan_Nmbr());                                                                                     //Natural: ASSIGN #SAVE-PLAN-NMBR := #INPUT.#PLAN-NMBR
                pnd_Save_Sub_Plan_Nmbr.setValue(gdaIaag700.getPnd_Input_Pnd_Sub_Plan_Nmbr());                                                                             //Natural: ASSIGN #SAVE-SUB-PLAN-NMBR := #INPUT.#SUB-PLAN-NMBR
                pnd_Save_Orgntng_Sub_Plan_Nmbr.setValue(gdaIaag700.getPnd_Input_Pnd_Orgntng_Sub_Plan_Nmbr());                                                             //Natural: ASSIGN #SAVE-ORGNTNG-SUB-PLAN-NMBR := #ORGNTNG-SUB-PLAN-NMBR
                pnd_Save_Orgntng_Cntrct_Nmbr.setValue(gdaIaag700.getPnd_Input_Pnd_Orgntng_Cntrct_Nmbr());                                                                 //Natural: ASSIGN #SAVE-ORGNTNG-CNTRCT-NMBR := #ORGNTNG-CNTRCT-NMBR
                //* **>>>>>>>>>>
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(20))))
            {
                decideConditionsMet1520++;
                pnd_Terminated_No_Cmbne.setValue(false);                                                                                                                  //Natural: ASSIGN #TERMINATED-NO-CMBNE := FALSE
                if (condition(gdaIaag700.getPnd_Input_Pnd_Status_Cde().equals(9) && gdaIaag700.getPnd_Input_Pnd_Cmbne_Cde().equals(" ")))                                 //Natural: IF #STATUS-CDE = 9 AND #CMBNE-CDE = ' '
                {
                    pnd_Terminated_No_Cmbne.setValue(true);                                                                                                               //Natural: ASSIGN #TERMINATED-NO-CMBNE := TRUE
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        pnd_First.setValue(false);                                                                                                                        //Natural: ASSIGN #FIRST := FALSE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*          PERFORM WRITE-CPS-RECORD
                        //*  ADDED  5/03 CHECK IF SEPARATE CHECK FOR IVC TPA/AC ROLLOVER CONTRACTS
                        //*        ADDED OPTION 21 TO THIS LOGIC 5/03
                        //*  REMOVED OPTION 21 FROM FOLLOWING LOGIC 11/03
                        //*          IF  (GTN-PDA-E.CNTRCT-OPTION-CDE = 21 OR= 28 OR= 30) AND
                        //*  ADDED 5/03
                        //*  ADDED 5/03
                        //*  ADDED 5/03
                        if (condition((((gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(28) || gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(30))                //Natural: IF ( GTN-PDA-E.CNTRCT-OPTION-CDE = 28 OR = 30 ) AND ( GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = '57BT' OR = '57BC' OR = '57BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' ) AND #SAVE-CNTRCT-IVC-AMT-IK > 0
                            && (((((((((((gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BX")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLX"))) 
                            && pnd_Save_Cntrct_Ivc_Amt_Ik.greater(getZero()))))
                        {
                                                                                                                                                                          //Natural: PERFORM CHECK-IVCROLL-FOR-O1IV-CHECK
                            sub_Check_Ivcroll_For_O1iv_Check();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  END OF ADD 5/03
                        //*  ====> WRITE CPS RECORD NORMAL PMT PROCESS
                                                                                                                                                                          //Natural: PERFORM WRITE-CPS-RECORD
                        sub_Write_Cps_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*   ADDED FOLLOWING 5/03 TO CREATE SEPARATE IVC CHECK FOR ROLLS
                        //*        ADDED OPTION 21 TO THIS LOGIC 5/03
                        //*  REMOVED OPTION 21 FROM FOLLOWING LOGIC 11/03
                        //*          IF  (GTN-PDA-E.CNTRCT-OPTION-CDE = 21 OR= 28 OR= 30) AND
                        //*  ADDED 5/03
                        //*  ADDED 5/03
                        //*  ADDED 5/03
                        if (condition((((gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(28) || gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(30))                //Natural: IF ( GTN-PDA-E.CNTRCT-OPTION-CDE = 28 OR = 30 ) AND ( GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = '57BT' OR = '57BC' OR = '57BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' ) AND #SAVE-CNTRCT-IVC-AMT-IK > 0
                            && (((((((((((gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BX")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT")) 
                            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLX"))) 
                            && pnd_Save_Cntrct_Ivc_Amt_Ik.greater(getZero()))))
                        {
                            //*            GTN-PDA-E.CNTRCT-CMBN-NBR-PAYEE := '01IV' /* ADDED 12/02
                            setValueToSubstring("IV",gdaIaag700.getGtn_Pda_E_Cntrct_Cmbn_Nbr_Payee(),3,2);                                                                //Natural: MOVE 'IV' TO SUBSTR ( GTN-PDA-E.CNTRCT-CMBN-NBR-PAYEE,3,2 )
                            //*  ADDED 12/02
                            //*  ADDED 12/02
                            gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().reset();                                                                                       //Natural: RESET GTN-PDA-E.CNTRCT-ROLL-DEST-CDE
                            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                 //Natural: ASSIGN PYMNT-PAY-TYPE-REQ-IND := 1
                            //*            GTN-PDA-E.CNTRCT-PAYEE-CDE        := '01IV'
                            setValueToSubstring("IV",gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde(),3,2);                                                                     //Natural: MOVE 'IV' TO SUBSTR ( GTN-PDA-E.CNTRCT-PAYEE-CDE,3,2 )
                            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("8");                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := '8'
                            gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Save_Cntrct_Ivc_Amt_Ik);                                                  //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #SAVE-CNTRCT-IVC-AMT-IK
                            gdaIaag700.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(1).setValue(gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1));                   //Natural: ASSIGN GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( 1 ) := GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 )
                            gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(1).setValue(gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1));                    //Natural: ASSIGN GTN-PDA-E.INV-ACCT-SETTL-AMT ( 1 ) := GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 )
                            gdaIaag700.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(1).setValue(0);                                                                         //Natural: ASSIGN GTN-PDA-E.INV-ACCT-DVDND-AMT ( 1 ) := 0
                            //*  ADDED 5/03 IF VALID NAME
                            if (condition(pnd_Error_Code_1.getValue(1).notEquals("1")))                                                                                   //Natural: IF #ERROR-CODE-1 ( 1 ) NE '1'
                            {
                                gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(1).setValue(gdaIaag700.getPnd_Hld_Cntrct_Name_Free_R());                                     //Natural: ASSIGN GTN-PDA-E.PYMNT-NME ( 1 ) := #HLD.CNTRCT-NAME-FREE-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_1_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( 1 ) := #HLD.ADDRSS-LNE-1-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_2_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( 1 ) := #HLD.ADDRSS-LNE-2-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_3_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( 1 ) := #HLD.ADDRSS-LNE-3-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_4_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( 1 ) := #HLD.ADDRSS-LNE-4-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_5_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( 1 ) := #HLD.ADDRSS-LNE-5-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_6_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( 1 ) := #HLD.ADDRSS-LNE-6-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Zip_Cde().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_R().getSubstring(1,            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-ZIP-CDE ( 1 ) := SUBSTR ( #HLD.ADDRSS-POSTAL-DATA-R,1,9 )
                                    9));
                                gdaIaag700.getGtn_Pda_E_Pymnt_Postl_Data().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_R());                            //Natural: ASSIGN GTN-PDA-E.PYMNT-POSTL-DATA ( 1 ) := #HLD.ADDRSS-POSTAL-DATA-R
                                gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(1).setValue(gdaIaag700.getPnd_Hld_Addrss_Type_Cde_R());                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( 1 ) := #HLD.ADDRSS-TYPE-CDE-R
                            }                                                                                                                                             //Natural: END-IF
                            //*  WRITE CPS 01IV IVC CHECK
                                                                                                                                                                          //Natural: PERFORM WRITE-CPS-RECORD
                            sub_Write_Cps_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  END OF ADD 5/03 FOR NEW EGTRRA IVC RULES FOR TPA ROLL 5/03
                                                                                                                                                                          //Natural: PERFORM RESET
                        sub_Reset();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-GTN-PDA
                    sub_Process_Gtn_Pda();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM SAVE-CPR
                    sub_Save_Cpr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   TIAA FUND RECORD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(30))))
            {
                decideConditionsMet1520++;
                if (condition(pnd_Terminated_No_Cmbne.getBoolean()))                                                                                                      //Natural: IF #TERMINATED-NO-CMBNE
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUND
                    sub_Process_Fund();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   DEDUCTION RECORD
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Cntrct_Payee.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee());                                                                               //Natural: ASSIGN #PREV-CNTRCT-PAYEE := #INPUT.#CNTRCT-PAYEE
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(40))))
            {
                decideConditionsMet1520++;
                if (condition(pnd_Terminated_No_Cmbne.getBoolean()))                                                                                                      //Natural: IF #TERMINATED-NO-CMBNE
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Proc_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PROC-CTR
                    if (condition(gdaIaag700.getPnd_Input_Pnd_Ddctn_Stp_Dte().equals(getZero()) && gdaIaag700.getPnd_Input_Pnd_Ddctn_Per_Amt().greater(getZero())))       //Natural: IF #DDCTN-STP-DTE = 0 AND #DDCTN-PER-AMT > 0
                    {
                        pnd_D_Indx.nadd(1);                                                                                                                               //Natural: ADD 1 TO #D-INDX
                        gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Cde().getValue(pnd_D_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Ddctn_Cde_N());                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-DED-CDE ( #D-INDX ) := #DDCTN-CDE-N
                        gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Amt().getValue(pnd_D_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Ddctn_Per_Amt());                                   //Natural: ADD #DDCTN-PER-AMT TO GTN-PDA-E.PYMNT-DED-AMT ( #D-INDX )
                        gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Payee_Cde_5().getValue(pnd_D_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Ddctn_Payee());                         //Natural: ASSIGN GTN-PDA-E.PYMNT-DED-PAYEE-CDE-5 ( #D-INDX ) := #DDCTN-PAYEE
                        if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Cde().getValue(pnd_D_Indx).equals(400)))                                                          //Natural: IF GTN-PDA-E.PYMNT-DED-CDE ( #D-INDX ) = 400
                        {
                            gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Payee_Cde_3().getValue(pnd_D_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Ddctn_Seq_Nbr_A());                 //Natural: ASSIGN GTN-PDA-E.PYMNT-DED-PAYEE-CDE-3 ( #D-INDX ) := #DDCTN-SEQ-NBR-A
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
                        sub_Check_Mode();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM CONTROL-RECORD
                        sub_Control_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Cntrct_Payee.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee());                                                                               //Natural: ASSIGN #PREV-CNTRCT-PAYEE := #INPUT.#CNTRCT-PAYEE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CPS-RECORD
        sub_Write_Cps_Record();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Check_Dte_A);                                                   //Natural: MOVE EDITED #SAVE-CHECK-DTE-A TO IA-CNTL-REC.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        FOR01:                                                                                                                                                            //Natural: FOR #INDX1 = 1 TO 19
        for (pnd_Indx1.setValue(1); condition(pnd_Indx1.lessOrEqual(19)); pnd_Indx1.nadd(1))
        {
            ldaFcpl800.getIa_Cntl_Rec_C_Rec_Count().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_Indx1));                              //Natural: ASSIGN IA-CNTL-REC.C-REC-COUNT ( #INDX1 ) := #C-REC-COUNT ( #INDX1 )
            ldaFcpl800.getIa_Cntl_Rec_C_Cntrct_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_Indx1));                            //Natural: ASSIGN IA-CNTL-REC.C-CNTRCT-AMT ( #INDX1 ) := #C-CNTRCT-AMT ( #INDX1 )
            ldaFcpl800.getIa_Cntl_Rec_C_Dvdnd_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_Indx1));                              //Natural: ASSIGN IA-CNTL-REC.C-DVDND-AMT ( #INDX1 ) := #C-DVDND-AMT ( #INDX1 )
            ldaFcpl800.getIa_Cntl_Rec_C_Cref_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_Indx1));                                //Natural: ASSIGN IA-CNTL-REC.C-CREF-AMT ( #INDX1 ) := #C-CREF-AMT ( #INDX1 )
            ldaFcpl800.getIa_Cntl_Rec_C_Gross_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_Indx1));                              //Natural: ASSIGN IA-CNTL-REC.C-GROSS-AMT ( #INDX1 ) := #C-GROSS-AMT ( #INDX1 )
            ldaFcpl800.getIa_Cntl_Rec_C_Ded_Amt().getValue(pnd_Indx1).setValue(pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_Indx1));                                  //Natural: ASSIGN IA-CNTL-REC.C-DED-AMT ( #INDX1 ) := #C-DED-AMT ( #INDX1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaFcpl800.getIa_Cntl_Rec_Cntl_Rec_Id().setValue("999999");                                                                                                       //Natural: ASSIGN CNTL-REC-ID := '999999'
        //*    CONTROL RECORD WRITEEN HERE
        getWorkFiles().write(5, false, ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte(), ldaFcpl800.getIa_Cntl_Rec_Cntl_Rec_Id(), ldaFcpl800.getIa_Cntl_Rec_Cntl_Counts().getValue("*")); //Natural: WRITE WORK FILE 5 IA-CNTL-REC.CNTL-CHECK-DTE IA-CNTL-REC.CNTL-REC-ID IA-CNTL-REC.CNTL-COUNTS ( * )
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",pnd_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                    //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",pnd_Proc_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                               //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #PROC-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Bypass_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                             //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #BYPASS-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED REINV.     : ",pnd_Bypass_Ctr_Reinv, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                       //Natural: WRITE 'NUMBER OF RECORDS BYPASSED REINV.     : ' #BYPASS-CTR-REINV ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF CORE RECORDS READ           : ",pnd_Core_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                               //Natural: WRITE 'NUMBER OF CORE RECORDS READ           : ' #CORE-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF NAME ADDRESS RECORDS READ   : ",pnd_Name_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                               //Natural: WRITE 'NUMBER OF NAME ADDRESS RECORDS READ   : ' #NAME-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF CPS RECORDS                 : ",pnd_Cps_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                //Natural: WRITE 'NUMBER OF CPS RECORDS                 : ' #CPS-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF AUDIT HOLD RECORDS READ     : ",pnd_Hold_Ctr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                               //Natural: WRITE 'NUMBER OF AUDIT HOLD RECORDS READ     : ' #HOLD-CTR ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  02/15
        getReports().write(0, "NUMBER OF MISSING NAME                : ",pnd_T_Rec_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                            //Natural: WRITE 'NUMBER OF MISSING NAME                : ' #T-REC-COUNT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-GTN-PDA
        //*  === CHANGED FOLLOWING 10/01 =======
        //*  GTN-PDA-E.CNTRCT-HOLD-IND := #SAVE-CNTRCT-HOLD-CDE
        //*  END OF COMMENTED CODE 10/01
        //*  ADDED FOLLOWING FOR EGTRRA PROCESS 5/03
        //*    WRITE WORK 8
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CPS-RECORD
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET
        //* ***************************************************************
        //*  ADDED FOLLOWING 5/03
        //*  END OF ADD 5/03
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-CPR
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-ERROR-CODE
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ANNT-INS
        //*  NEW IPRO CONTRACTS                                     12/04/2000
        //*    CNTRCT-ANNTY-TYPE-CDE := 'N'
        //*  KN 06/25/97
        //*  KN 04/15/98
        //*  KN 06/25/97
        //*  8/14 END
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FUND
        //*  **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IVC
        //*  GTN-PDA-E.CNTRCT-PAYEE-CDE = '01IV'
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TOTAL
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-RECORD
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUMULATE-CONTROL-RECORD
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MODE
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AIAN026
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CORE
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AUDIT-HOLD
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDRESS
        //*      GTN-PDA-E.CNTRCT-UNQ-ID-NBR := 9999999
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-NAME-ADDR
        //* ***************************************************************
        //* *GTN-PDA-E.PYMNT-NME(#INDX1) := #HLD.CNTRCT-NAME-FREE-K 8/13
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-VAC-NAME-ADDR
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PAYMENT-INDICATOR
        //*  082020 - START ==> NO DEFAULTING TO CHECK FOR HOLDS
        //*  WHEN GTN-PDA-E.CNTRCT-HOLD-IND NE ' ' AND
        //*      GTN-PDA-E.CNTRCT-HOLD-IND NE '0'
        //*    GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 1
        //*  082020 - END
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HOLD-CODE
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAX-EXEMPT
        //* ***************************************************************
        //* ***************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*                                                                                                                                                               //Natural: ON ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-COMPANY
        //* ***********************************************************************
        //*   ADDED FOLLOWING ROUTINE 5/03 HANDLE IVC FOR ROLL OVER CONTRACTS
        //* *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IVCROLL-FOR-O1IV-CHECK
        //* *
        //* * SUBTRACT IVC AMT FROM CHECK-AMT CREATE SEPARATE CHECK FOR IVC
        //*  END OF ADD 5/03 FOR NEW EGTRRA IVC RULES FOR TPA ROLL 5/03
    }
    //*  HAVE NEW CPR RECORD
    private void sub_Process_Gtn_Pda() throws Exception                                                                                                                   //Natural: PROCESS-GTN-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        gdaIaag700.getGtn_Pda_E_Pymnt_Reqst_Log_Dte_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                        //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO GTN-PDA-E.PYMNT-REQST-LOG-DTE-TIME
        gdaIaag700.getGtn_Pda_E_Cntrct_Ppcn_Nbr().setValue(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr());                                                                       //Natural: ASSIGN GTN-PDA-E.CNTRCT-PPCN-NBR := #INPUT.#PPCN-NBR
        gdaIaag700.getGtn_Pda_E_Cntrct_Check_Crrncy_Cde().setValue(pnd_Save_Cntrct_Crrncy_Cde);                                                                           //Natural: ASSIGN GTN-PDA-E.CNTRCT-CHECK-CRRNCY-CDE := #SAVE-CNTRCT-CRRNCY-CDE
        gdaIaag700.getGtn_Pda_E_Cntrct_Orgn_Cde().setValue("AP");                                                                                                         //Natural: ASSIGN GTN-PDA-E.CNTRCT-ORGN-CDE := 'AP'
        gdaIaag700.getGtn_Pda_E_Cntrct_Type_Cde().setValue("PP");                                                                                                         //Natural: ASSIGN GTN-PDA-E.CNTRCT-TYPE-CDE := 'PP'
        //*  08/98 KN
        gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().reset();                                                                                                          //Natural: RESET GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND
        gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().reset();
        //*  02/10
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("20") || pnd_Save_Cntrct_Orgn_Cde.equals("12")))                                                                    //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '20' OR = '12'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'T'
        }                                                                                                                                                                 //Natural: END-IF
        //*  02/10 CANADIAN CONVERTED
        if (condition(pnd_Save_Ppg_Cde.equals("Y1650")))                                                                                                                  //Natural: IF #SAVE-PPG-CDE = 'Y1650'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("C");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ====== 07/01 NEW TAX CALCULATIONS ==================
        //*   CALCULATION REQUIRES LESS THAN 10 YEAR FOR ANNUITY CERTAIN
        //*  04/06 ROTH IRA
        //*  01/12
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("21") || pnd_Save_Cntrct_Orgn_Cde.equals("22") || pnd_Save_Cntrct_Orgn_Cde.equals("23") || pnd_Save_Cntrct_Orgn_Cde.equals("79"))) //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '21' OR = '22' OR = '23' OR = '79'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("H");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'H'
            //*  07/06 ROTH SURVIVOR
            //*  01/12
            if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("23") || pnd_Save_Cntrct_Orgn_Cde.equals("79")))                                                                //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '23' OR = '79'
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Ia_Lob_Cde().setValue("23");                                                                                               //Natural: ASSIGN GTN-PDA-E.CNTRCT-IA-LOB-CDE := '23'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   RECOGNIZE CONTRACT MONEY AS IRA
        //*  02/10
        //*  01/12
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("19") || pnd_Save_Cntrct_Orgn_Cde.equals("11") || pnd_Save_Cntrct_Orgn_Cde.equals("13") || pnd_Save_Cntrct_Orgn_Cde.equals("80")  //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '19' OR = '11' OR = '13' OR = '80' OR = '86'
            || pnd_Save_Cntrct_Orgn_Cde.equals("86")))
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue(" ");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  ====== END OF 07/01 CHANGES =========================
        //*  TPA
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(30)))                                                                        //Natural: IF ( #SAVE-CNTRCT-OPTN-CDE = 28 OR = 30 )
        {
            //*  CHANGED 10/01    /* OLD STYLE
            if (condition(pnd_Save_Issue_Dte.less(200111)))                                                                                                               //Natural: IF #SAVE-ISSUE-DTE < 200111
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("P");                                                                                            //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'P'
                gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue(" ");                                                                                          //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := ' '
                //*  NEW STYLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("T");                                                                                            //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'T'
                gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue(" ");                                                                                          //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Spouse_Pay_Stats().setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde());                                                    //Natural: ASSIGN GTN-PDA-E.PYMNT-SPOUSE-PAY-STATS := #INPUT.#CNTRCT-EMP-TRMNT-CDE
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(22)))                                                                                                               //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 22
        {
            if (condition(gdaIaag700.getPnd_Input_Pnd_Cash_Cde().equals("M")))                                                                                            //Natural: IF #CASH-CDE = 'M'
            {
                gdaIaag700.getGtn_Pda_E_Pymnt_Spouse_Pay_Stats().setValue("B");                                                                                           //Natural: ASSIGN GTN-PDA-E.PYMNT-SPOUSE-PAY-STATS := 'B'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  IPRO & P/I
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(25) || pnd_Save_Cntrct_Optn_Cde.equals(27) || pnd_Save_Cntrct_Optn_Cde.equals(22)))                                 //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 25 OR = 27 OR = 22
        {
            //*  ADDED 10/01
            //*  ADDED 10/01
            //*  ADDED 10/01
            //*  ADDED 5/03
            if (condition(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAC") ||                     //Natural: IF #INPUT.#CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BC") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLT") 
                || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLC") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BC")))
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("P");                                                                                            //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'P'
                gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("R");                                                                                          //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'R'
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                    //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-DEST-CDE := #INPUT.#CURR-DIST-CDE
                gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                     //Natural: ASSIGN GTN-PDA-E.CNTRCT-ROLL-DEST-CDE := #INPUT.#CURR-DIST-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("P");                                                                                            //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'P'
                gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue(" ");                                                                                          //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := ' '
                gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().setValue(" ");                                                                                             //Natural: ASSIGN GTN-PDA-E.CNTRCT-ROLL-DEST-CDE := ' '
                gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                    //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-DEST-CDE := #INPUT.#CURR-DIST-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING FOR 3 NEW TNT PRODUCTS  1/05
        //*  =================================================
        //*  04/06 -- ICAP
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("48") || pnd_Save_Cntrct_Orgn_Cde.equals("65") || pnd_Save_Cntrct_Orgn_Cde.equals("66")))                           //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '48' OR = '65' OR = '66'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Lob_Cde().setValue("RS");                                                                                                      //Natural: ASSIGN GTN-PDA-E.CNTRCT-LOB-CDE := 'RS'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("49")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '49'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Lob_Cde().setValue("RSP");                                                                                                     //Natural: ASSIGN GTN-PDA-E.CNTRCT-LOB-CDE := 'RSP'
            //*  DATA WAREHOUSE
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().setValue(pnd_Save_Cntrct_Optn_Cde);                                                                                   //Natural: ASSIGN GTN-PDA-E.CNTRCT-OPTION-CDE := #SAVE-CNTRCT-OPTN-CDE
        gdaIaag700.getGtn_Pda_E_Ia_Orgn_Cde().setValue(pnd_Save_Cntrct_Orgn_Cde);                                                                                         //Natural: ASSIGN GTN-PDA-E.IA-ORGN-CDE := #SAVE-CNTRCT-ORGN-CDE
        gdaIaag700.getGtn_Pda_E_Cntrct_Dvdnd_Payee_Cde().setValue(pnd_Save_Cntrct_Dvdnd_Pay_Cde);                                                                         //Natural: ASSIGN GTN-PDA-E.CNTRCT-DVDND-PAYEE-CDE := #SAVE-CNTRCT-DVDND-PAY-CDE
        gdaIaag700.getGtn_Pda_E_Cntrct_Qlfied_Cde().setValue(pnd_Save_Cntrct_Pnsn_Pln_Cde);                                                                               //Natural: ASSIGN GTN-PDA-E.CNTRCT-QLFIED-CDE := #SAVE-CNTRCT-PNSN-PLN-CDE
        if (condition(pnd_Save_Cntrct_Dob.greater(getZero())))                                                                                                            //Natural: IF #SAVE-CNTRCT-DOB GT 0
        {
            if (condition(pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_Cc.equals(0)))                                                                                          //Natural: IF #SAVE-CNTRCT-DOB-CC = 00
            {
                pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_Cc.setValue(19);                                                                                                  //Natural: ASSIGN #SAVE-CNTRCT-DOB-CC := 19
            }                                                                                                                                                             //Natural: END-IF
            gdaIaag700.getGtn_Pda_E_Pymnt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Cntrct_Dob_Pnd_Save_Cntrct_Dob_A);                                 //Natural: MOVE EDITED #SAVE-CNTRCT-DOB-A TO GTN-PDA-E.PYMNT-DOB ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaIaag700.getPnd_Input_Pnd_Cmbne_Cde().equals(" ")))                                                                                               //Natural: IF #INPUT.#CMBNE-CDE = ' '
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Cmbn_Nbr().setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee());                                                               //Natural: ASSIGN GTN-PDA-E.CNTRCT-CMBN-NBR := #INPUT.#CNTRCT-PAYEE
            gdaIaag700.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().setValue(1);                                                                                                     //Natural: ASSIGN GTN-PDA-E.##THIS-PYMNT := 1
            gdaIaag700.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts().setValue(1);                                                                                                  //Natural: ASSIGN GTN-PDA-E.##NBR-OF-PYMNTS := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Cmbn_Nbr().setValue(gdaIaag700.getPnd_Input_Pnd_Cmbne_Cde());                                                                  //Natural: ASSIGN GTN-PDA-E.CNTRCT-CMBN-NBR := #INPUT.#CMBNE-CDE
            gdaIaag700.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().setValue(0);                                                                                                     //Natural: ASSIGN GTN-PDA-E.##THIS-PYMNT := 0
            gdaIaag700.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts().setValue(0);                                                                                                  //Natural: ASSIGN GTN-PDA-E.##NBR-OF-PYMNTS := 0
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Ind().setValue(0);                                                                                                           //Natural: ASSIGN GTN-PDA-E.ANNT-SOC-SEC-IND := 0
        gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr().setValue(gdaIaag700.getPnd_Input_Pnd_Tax_Id_Nbr());                                                                    //Natural: ASSIGN GTN-PDA-E.ANNT-SOC-SEC-NBR := #INPUT.#TAX-ID-NBR
        gdaIaag700.getGtn_Pda_E_Annt_Ctznshp_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Ctznshp_Cde());                                                                   //Natural: ASSIGN GTN-PDA-E.ANNT-CTZNSHP-CDE := #INPUT.#CTZNSHP-CDE
        gdaIaag700.getGtn_Pda_E_Annt_Rsdncy_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Rsdncy_Cde().getSubstring(2,2));                                                   //Natural: ASSIGN GTN-PDA-E.ANNT-RSDNCY-CDE := SUBSTR ( #INPUT.#RSDNCY-CDE,2,2 )
        //*  9/09 FORCE 96/74 RES
        //*  9/09 CODES TO USE CA SO TAXWARS
        if (condition(gdaIaag700.getGtn_Pda_E_Annt_Rsdncy_Cde().equals("96") || gdaIaag700.getGtn_Pda_E_Annt_Rsdncy_Cde().equals("74")))                                  //Natural: IF GTN-PDA-E.ANNT-RSDNCY-CDE = '96' OR = '74'
        {
            gdaIaag700.getGtn_Pda_E_Annt_Rsdncy_Cde().setValue("CA");                                                                                                     //Natural: ASSIGN GTN-PDA-E.ANNT-RSDNCY-CDE := 'CA'
            //*  9/09 WON't reject
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Annt_Locality_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Local_Cde().getSubstring(2,2));                                           //Natural: ASSIGN GTN-PDA-E.ANNT-LOCALITY-CDE := SUBSTR ( #INPUT.#CNTRCT-LOCAL-CDE,2,2 )
        gdaIaag700.getGtn_Pda_E_Pymnt_Settlmnt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Check_Dte_A);                                                 //Natural: MOVE EDITED #SAVE-CHECK-DTE-A TO GTN-PDA-E.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD )
        gdaIaag700.getGtn_Pda_E_Pymnt_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Check_Dte_A);                                                    //Natural: MOVE EDITED #SAVE-CHECK-DTE-A TO GTN-PDA-E.PYMNT-CHECK-DTE ( EM = YYYYMMDD )
        gdaIaag700.getGtn_Pda_E_Pymnt_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Check_Dte_A);                                                    //Natural: MOVE EDITED #SAVE-CHECK-DTE-A TO GTN-PDA-E.PYMNT-CYCLE-DTE ( EM = YYYYMMDD )
        if (condition(pdaCpwa115.getCpwa115_Accounting_Date().notEquals(getZero())))                                                                                      //Natural: IF ACCOUNTING-DATE NE 0
        {
            pnd_Hold_Accounting_Date.setValue(pdaCpwa115.getCpwa115_Accounting_Date());                                                                                   //Natural: ASSIGN #HOLD-ACCOUNTING-DATE := ACCOUNTING-DATE
            gdaIaag700.getGtn_Pda_E_Pymnt_Acctg_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Hold_Accounting_Date_Pnd_Hold_Accounting_Date_A);                 //Natural: MOVE EDITED #HOLD-ACCOUNTING-DATE-A TO GTN-PDA-E.PYMNT-ACCTG-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Issue_Dte.greater(getZero())))                                                                                                             //Natural: IF #SAVE-ISSUE-DTE GT 0
        {
            //*  COMPRESS #SAVE-ISSUE-DTE-A '01' INTO #HOLD-DTE
            pnd_Hold_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Save_Issue_Dte_Pnd_Save_Issue_Dte_A, pnd_Save_Issue_Dte_Dd_Pnd_Save_Issue_Dte_Dd_A)); //Natural: COMPRESS #SAVE-ISSUE-DTE-A #SAVE-ISSUE-DTE-DD-A INTO #HOLD-DTE LEAVING NO
            gdaIaag700.getGtn_Pda_E_Pymnt_Ia_Issue_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Hold_Dte);                                                     //Natural: MOVE EDITED #HOLD-DTE TO GTN-PDA-E.PYMNT-IA-ISSUE-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCpwa115.getCpwa115_Interface_Date().notEquals(getZero())))                                                                                       //Natural: IF INTERFACE-DATE NE 0
        {
            pnd_Hold_Interface_Date.setValue(pdaCpwa115.getCpwa115_Interface_Date());                                                                                     //Natural: ASSIGN #HOLD-INTERFACE-DATE := INTERFACE-DATE
            gdaIaag700.getGtn_Pda_E_Pymnt_Intrfce_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Hold_Interface_Date_Pnd_Hold_Interface_Date_A);                 //Natural: MOVE EDITED #HOLD-INTERFACE-DATE-A TO GTN-PDA-E.PYMNT-INTRFCE-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Payee_Cde_A());                                                                   //Natural: ASSIGN GTN-PDA-E.CNTRCT-PAYEE-CDE := #INPUT.#PAYEE-CDE-A
        if (condition(gdaIaag700.getPnd_Input_Pnd_Payee_Cde().equals(2)))                                                                                                 //Natural: IF #INPUT.#PAYEE-CDE = 2
        {
            if (condition(pnd_Save_First_Ann_Dod.equals(getZero())))                                                                                                      //Natural: IF #SAVE-FIRST-ANN-DOD = 0
            {
                gdaIaag700.getGtn_Pda_E_Pymnt_Payee_Tax_Ind().setValue("Y");                                                                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-PAYEE-TAX-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Pymnt_Payee_Tax_Ind().setValue("N");                                                                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-PAYEE-TAX-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Payee_Tax_Ind().setValue(" ");                                                                                                  //Natural: ASSIGN GTN-PDA-E.PYMNT-PAYEE-TAX-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Cntrct_Mode_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Mode_Ind());                                                                       //Natural: ASSIGN GTN-PDA-E.CNTRCT-MODE-CDE := #INPUT.#MODE-IND
        //*  SET CPS ROLL CODE  10/01
        //*  ADDED 5/03
        if (condition(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAC") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BT")  //Natural: IF #INPUT.#CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = 'IRAX' OR = '03BX' OR = 'QPLX' OR = 'DTRA' OR = '57BT' OR = '57BC' OR = '57BX'
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BC") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLC") 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAX") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BX") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLX") 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("DTRA") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BT") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BC") 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BX")))
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde().setValue(" ");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-DEST-CDE := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                         //Natural: ASSIGN GTN-PDA-E.CNTRCT-ROLL-DEST-CDE := #INPUT.#CURR-DIST-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().setValue(" ");                                                                                                 //Natural: ASSIGN GTN-PDA-E.CNTRCT-ROLL-DEST-CDE := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                        //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-DEST-CDE := #INPUT.#CURR-DIST-CDE
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind().setValue(gdaIaag700.getPnd_Input_Pnd_Hold_Cde());                                                                       //Natural: ASSIGN GTN-PDA-E.CNTRCT-HOLD-IND := #INPUT.#HOLD-CDE
        gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Pend_Cde());                                                                     //Natural: ASSIGN GTN-PDA-E.PYMNT-SUSPEND-CDE := #INPUT.#PEND-CDE
        if (condition(gdaIaag700.getPnd_Input_Pnd_Pend_Dte().notEquals(getZero())))                                                                                       //Natural: IF #INPUT.#PEND-DTE NE 0
        {
            pnd_Hold_Dte.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, gdaIaag700.getPnd_Input_Pnd_Pend_Dte_A(), "01"));                                       //Natural: COMPRESS #INPUT.#PEND-DTE-A '01' INTO #HOLD-DTE LEAVING NO
            gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Hold_Dte);                                                      //Natural: MOVE EDITED #HOLD-DTE TO GTN-PDA-E.PYMNT-SUSPEND-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Egtrra_Eligibility_Ind().setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Elgble_Ind());                                                        //Natural: ASSIGN GTN-PDA-E.EGTRRA-ELIGIBILITY-IND := #INPUT.#RLLVR-ELGBLE-IND
        gdaIaag700.getGtn_Pda_E_Originating_Irs_Cde().getValue(1,":",4).setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Dstrbtng_Irc_Cde().getValue("*"));                     //Natural: ASSIGN GTN-PDA-E.ORIGINATING-IRS-CDE ( 1:4 ) := #INPUT.#RLLVR-DSTRBTNG-IRC-CDE ( * )
        gdaIaag700.getGtn_Pda_E_Receiving_Irc_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Accptng_Irc_Cde());                                                        //Natural: ASSIGN GTN-PDA-E.RECEIVING-IRC-CDE := #INPUT.#RLLVR-ACCPTNG-IRC-CDE
        //*  GET LOB FOR CPS
        if (condition(gdaIaag700.getPnd_Input_Pnd_Rllvr_Cntrct_Nbr().notEquals("          ")))                                                                            //Natural: IF #INPUT.#RLLVR-CNTRCT-NBR NE '          '
        {
            iaan019_Rllvr_Cntrct.setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Cntrct_Nbr());                                                                                //Natural: ASSIGN IAAN019-RLLVR-CNTRCT := #INPUT.#RLLVR-CNTRCT-NBR
            DbsUtil.callnat(Iaan019.class , getCurrentProcessState(), iaan019_Rllvr_Cntrct, iaan019_Lob_Prdct_Cde, iaan019_Ret_Cde);                                      //Natural: CALLNAT 'IAAN019' IAAN019-RLLVR-CNTRCT IAAN019-LOB-PRDCT-CDE IAAN019-RET-CDE
            if (condition(Global.isEscape())) return;
            gdaIaag700.getGtn_Pda_E_Cntrct_Lob_Cde().setValue(iaan019_Lob_Prdct_Cde);                                                                                     //Natural: ASSIGN GTN-PDA-E.CNTRCT-LOB-CDE := IAAN019-LOB-PRDCT-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD FOR EGTRRA PROCESS 5/03
        //*  ADDED FOR ROTH 4/08
        if (condition(((pnd_Save_Cntrct_Orgn_Cde.greaterOrEqual("51") && pnd_Save_Cntrct_Orgn_Cde.lessOrEqual("58")) || (pnd_Save_Cntrct_Orgn_Cde.greaterOrEqual("71")    //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '51' THRU '58' OR #SAVE-CNTRCT-ORGN-CDE = '71' THRU '78'
            && pnd_Save_Cntrct_Orgn_Cde.lessOrEqual("78")))))
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("W");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'W'
            gdaIaag700.getGtn_Pda_E_Roth_First_Contrib_Dte().setValue(pnd_Save_Roth_Frst_Cntrb_Dte);                                                                      //Natural: MOVE #SAVE-ROTH-FRST-CNTRB-DTE TO GTN-PDA-E.ROTH-FIRST-CONTRIB-DTE
            if (condition(pnd_Save_Roth_Dsblty_Dte.notEquals(getZero())))                                                                                                 //Natural: IF #SAVE-ROTH-DSBLTY-DTE NE 0
            {
                gdaIaag700.getGtn_Pda_E_Roth_Disability_Dte().setValue(pnd_Save_Roth_Dsblty_Dte);                                                                         //Natural: MOVE #SAVE-ROTH-DSBLTY-DTE TO GTN-PDA-E.ROTH-DISABILITY-DTE
            }                                                                                                                                                             //Natural: END-IF
            gdaIaag700.getGtn_Pda_E_Roth_Dob().setValue(pnd_Prtcpnt_Dob);                                                                                                 //Natural: ASSIGN GTN-PDA-E.ROTH-DOB := #PRTCPNT-DOB
            if (condition(pnd_Save_First_Ann_Dod.greater(getZero())))                                                                                                     //Natural: IF #SAVE-FIRST-ANN-DOD GT 0
            {
                gdaIaag700.getGtn_Pda_E_Roth_Death_Dte_N6().setValue(pnd_Save_First_Ann_Dod);                                                                             //Natural: ASSIGN GTN-PDA-E.ROTH-DEATH-DTE-N6 := #SAVE-FIRST-ANN-DOD
                gdaIaag700.getGtn_Pda_E_Roth_Death_Dte_Dd().setValue("01");                                                                                               //Natural: ASSIGN GTN-PDA-E.ROTH-DEATH-DTE-DD := '01'
            }                                                                                                                                                             //Natural: END-IF
            gdaIaag700.getGtn_Pda_E_Roth_Money_Source().setValue("ROTHP");                                                                                                //Natural: ASSIGN GTN-PDA-E.ROTH-MONEY-SOURCE := 'ROTHP'
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR STABLE RETURN 02/10
        if (condition(pnd_Save_Cntrct_Orgn_Cde.greaterOrEqual("24") && pnd_Save_Cntrct_Orgn_Cde.lessOrEqual("31")))                                                       //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '24' THRU '31'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("S");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'S'
            //*  FOR STABLE RETURN ROTH
            if (condition(pnd_Save_Cntrct_Orgn_Cde.greaterOrEqual("24") && pnd_Save_Cntrct_Orgn_Cde.lessOrEqual("27")))                                                   //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '24' THRU '27'
            {
                gdaIaag700.getGtn_Pda_E_Roth_First_Contrib_Dte().setValue(pnd_Save_Roth_Frst_Cntrb_Dte);                                                                  //Natural: MOVE #SAVE-ROTH-FRST-CNTRB-DTE TO GTN-PDA-E.ROTH-FIRST-CONTRIB-DTE
                if (condition(pnd_Save_Roth_Dsblty_Dte.notEquals(getZero())))                                                                                             //Natural: IF #SAVE-ROTH-DSBLTY-DTE NE 0
                {
                    gdaIaag700.getGtn_Pda_E_Roth_Disability_Dte().setValue(pnd_Save_Roth_Dsblty_Dte);                                                                     //Natural: MOVE #SAVE-ROTH-DSBLTY-DTE TO GTN-PDA-E.ROTH-DISABILITY-DTE
                }                                                                                                                                                         //Natural: END-IF
                gdaIaag700.getGtn_Pda_E_Roth_Dob().setValue(pnd_Prtcpnt_Dob);                                                                                             //Natural: ASSIGN GTN-PDA-E.ROTH-DOB := #PRTCPNT-DOB
                if (condition(pnd_Save_First_Ann_Dod.greater(getZero())))                                                                                                 //Natural: IF #SAVE-FIRST-ANN-DOD GT 0
                {
                    gdaIaag700.getGtn_Pda_E_Roth_Death_Dte_N6().setValue(pnd_Save_First_Ann_Dod);                                                                         //Natural: ASSIGN GTN-PDA-E.ROTH-DEATH-DTE-N6 := #SAVE-FIRST-ANN-DOD
                    gdaIaag700.getGtn_Pda_E_Roth_Death_Dte_Dd().setValue("01");                                                                                           //Natural: ASSIGN GTN-PDA-E.ROTH-DEATH-DTE-DD := '01'
                }                                                                                                                                                         //Natural: END-IF
                gdaIaag700.getGtn_Pda_E_Roth_Money_Source().setValue("ROTHP");                                                                                            //Natural: ASSIGN GTN-PDA-E.ROTH-MONEY-SOURCE := 'ROTHP'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ROTH CODE 4/08
        //*  457B  6/08
        //*  457B  01/12
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("50") || pnd_Save_Cntrct_Orgn_Cde.equals("83")))                                                                    //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '50' OR = '83'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("S");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'S'
            //*                           457B END
        }                                                                                                                                                                 //Natural: END-IF
        //*  BYPASS CALL TO TAXWARS BY CPS  02/10
        if (condition(pnd_Save_Tax_Exmpt_Ind.equals("Y")))                                                                                                                //Natural: IF #SAVE-TAX-EXMPT-IND = 'Y'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("X");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'X'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("Z");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'Z'
        }                                                                                                                                                                 //Natural: END-IF
        //*  02/10
        //*  02/10
        //*  02/10
        if (condition(pnd_Save_Plan_Nmbr.equals("TIAA05") || pnd_Save_Plan_Nmbr.equals("TIAA06")))                                                                        //Natural: IF #SAVE-PLAN-NMBR = 'TIAA05' OR = 'TIAA06'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                                //Natural: ASSIGN GTN-PDA-E.CNTRCT-PYMNT-TYPE-IND := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                              //Natural: ASSIGN GTN-PDA-E.CNTRCT-STTLMNT-TYPE-IND := 'T'
            //*  02/10
            //*  01/12
            //*  01/12
            //*  01/12
            //*  01/12
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Plan_Number().setValue(pnd_Save_Plan_Nmbr);                                                                                               //Natural: ASSIGN GTN-PDA-E.PLAN-NUMBER := #SAVE-PLAN-NMBR
        gdaIaag700.getGtn_Pda_E_Sub_Plan().setValue(pnd_Save_Sub_Plan_Nmbr);                                                                                              //Natural: ASSIGN GTN-PDA-E.SUB-PLAN := #SAVE-SUB-PLAN-NMBR
        gdaIaag700.getGtn_Pda_E_Orig_Cntrct_Nbr().setValue(pnd_Save_Orgntng_Cntrct_Nmbr);                                                                                 //Natural: ASSIGN GTN-PDA-E.ORIG-CNTRCT-NBR := #SAVE-ORGNTNG-CNTRCT-NMBR
        gdaIaag700.getGtn_Pda_E_Orig_Sub_Plan().setValue(pnd_Save_Orgntng_Sub_Plan_Nmbr);                                                                                 //Natural: ASSIGN GTN-PDA-E.ORIG-SUB-PLAN := #SAVE-ORGNTNG-SUB-PLAN-NMBR
        //*  5/13 PER BUSINESS
        if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals(" ") || gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0")))                                //Natural: IF GTN-PDA-E.PYMNT-SUSPEND-CDE = ' ' OR = '0'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Dest_Cde().reset();                                                                                                      //Natural: RESET GTN-PDA-E.CNTRCT-PYMNT-DEST-CDE GTN-PDA-E.CNTRCT-ROLL-DEST-CDE
            gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().reset();
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-ANNT-INS
        sub_Get_Annt_Ins();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-CORE
        sub_Get_Core();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-NAME-ADDRESS
        sub_Get_Name_Address();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-AUDIT-HOLD
        sub_Get_Audit_Hold();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT-INDICATOR
        sub_Get_Payment_Indicator();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HOLD-CODE
        sub_Determine_Hold_Code();
        if (condition(Global.isEscape())) {return;}
        //*  TAX-EXEMPT  10/10
        if (condition(pnd_Save_Tax_Exmpt_Ind.equals("Y")))                                                                                                                //Natural: IF #SAVE-TAX-EXMPT-IND = 'Y'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Tax_Exempt_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-TAX-EXEMPT-IND := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21)))                                                                                                               //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21
        {
                                                                                                                                                                          //Natural: PERFORM TAX-EXEMPT
            sub_Tax_Exempt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Proc_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
        //*  GET NAME FROM KOR IF NAME IS BLANK /* 122012
        //*  02/15 - START
        //*  ONLY ACTIVE CONTRACTS
        //*  ONLY NON-PENDED CONTRACTS
        if (condition(gdaIaag700.getGtn_Pda_E_Ph_Last_Name().equals(" ") && gdaIaag700.getGtn_Pda_E_Ph_Middle_Name().equals(" ") && gdaIaag700.getGtn_Pda_E_Ph_First_Name().equals(" ")  //Natural: IF GTN-PDA-E.PH-LAST-NAME = ' ' AND GTN-PDA-E.PH-MIDDLE-NAME = ' ' AND GTN-PDA-E.PH-FIRST-NAME = ' ' AND GTN-PDA-E.ANNT-SOC-SEC-NBR GT 0 AND #INPUT.#STATUS-CDE NE 9 AND #INPUT.#PEND-CDE = ' 0'
            && gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr().greater(getZero()) && gdaIaag700.getPnd_Input_Pnd_Status_Cde().notEquals(9) && gdaIaag700.getPnd_Input_Pnd_Pend_Cde().equals(" 0")))
        {
            pnd_T_Rec_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #T-REC-COUNT
            if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Ppcn_Nbr().equals(pnd_Save_Ppcn) && gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde().equals("02")))                    //Natural: IF GTN-PDA-E.CNTRCT-PPCN-NBR = #SAVE-PPCN AND GTN-PDA-E.CNTRCT-PAYEE-CDE = '02'
            {
                gdaIaag700.getGtn_Pda_E_Ph_Last_Name().setValue(pnd_Save_Cor_Ph_Last_Name);                                                                               //Natural: ASSIGN GTN-PDA-E.PH-LAST-NAME := #SAVE-COR-PH-LAST-NAME
                gdaIaag700.getGtn_Pda_E_Ph_Middle_Name().setValue(pnd_Save_Cor_Ph_Middle_Name);                                                                           //Natural: ASSIGN GTN-PDA-E.PH-MIDDLE-NAME := #SAVE-COR-PH-MIDDLE-NAME
                gdaIaag700.getGtn_Pda_E_Ph_First_Name().setValue(pnd_Save_Cor_Ph_First_Name);                                                                             //Natural: ASSIGN GTN-PDA-E.PH-FIRST-NAME := #SAVE-COR-PH-FIRST-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(1, gdaIaag700.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),";",                                                                                    //Natural: DISPLAY ( 1 ) GTN-PDA-E.CNTRCT-PPCN-NBR ';' GTN-PDA-E.CNTRCT-PAYEE-CDE ';' GTN-PDA-E.ANNT-SOC-SEC-NBR
                		gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde(),";",
                		gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr());
                if (Global.isEscape()) return;
                //*  A16
                //*  A12
                //*  A10
                //*  A9
                DbsUtil.callnat(Iaan700b.class , getCurrentProcessState(), gdaIaag700.getGtn_Pda_E_Ph_Last_Name(), gdaIaag700.getGtn_Pda_E_Ph_Middle_Name(),              //Natural: CALLNAT 'IAAN700B' GTN-PDA-E.PH-LAST-NAME GTN-PDA-E.PH-MIDDLE-NAME GTN-PDA-E.PH-FIRST-NAME GTN-PDA-E.ANNT-SOC-SEC-NBR
                    gdaIaag700.getGtn_Pda_E_Ph_First_Name(), gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr());
                if (condition(Global.isEscape())) return;
                //*  02/15 - END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Cps_Record() throws Exception                                                                                                                  //Natural: WRITE-CPS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Save_Status_Cde.equals(9) && pnd_Save_Cmbne_Cde.notEquals(" ")))                                                                                //Natural: IF #SAVE-STATUS-CDE = 9 AND #SAVE-CMBNE-CDE NE ' '
        {
            gdaIaag700.getGtn_Pda_E_Error_Code_2().setValue("2");                                                                                                         //Natural: ASSIGN ERROR-CODE-2 := '2'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Error_Code_2().setValue("0");                                                                                                         //Natural: ASSIGN ERROR-CODE-2 := '0'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SET-ERROR-CODE
        sub_Set_Error_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IVC
        sub_Ivc();
        if (condition(Global.isEscape())) {return;}
        if (condition(gdaIaag700.getGtn_Pda_E_Error_Code_1().notEquals("1")))                                                                                             //Natural: IF ERROR-CODE-1 NE '1'
        {
            if (condition(gdaIaag700.getPnd_Hld_Addrss_Zip_Plus_4_K().equals("FORGN") || gdaIaag700.getPnd_Hld_Addrss_Zip_Plus_4_K().equals("CANAD") ||                   //Natural: IF #HLD.ADDRSS-ZIP-PLUS-4-K = 'FORGN' OR = 'CANAD' OR PYMNT-PAY-TYPE-REQ-IND = 3 OR = 4 OR = 9
                gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(3) || gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(4) || gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(9)))
            {
                gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Acct_Nbr().setValue(gdaIaag700.getPnd_Hld_Intl_Bank_Pymnt_Acct_Nmbr());                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-ACCT-NBR := #HLD.INTL-BANK-PYMNT-ACCT-NMBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Save_Status_Cde.notEquals(9)) || (pnd_Save_Status_Cde.equals(9) && pnd_Save_Cmbne_Cde.notEquals(" "))))                                        //Natural: IF ( #SAVE-STATUS-CDE NE 9 ) OR ( #SAVE-STATUS-CDE = 9 AND #SAVE-CMBNE-CDE NE ' ' )
        {
            cmbn_Nbr.setValue(gdaIaag700.getGtn_Pda_E_Cntrct_Cmbn_Nbr());                                                                                                 //Natural: ASSIGN CMBN-NBR := CNTRCT-CMBN-NBR
            if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Ppcn_Nbr().equals(cmbn_Nbr_Pnd_Cmbn_Nbr)))                                                                       //Natural: IF GTN-PDA-E.CNTRCT-PPCN-NBR = #CMBN-NBR
            {
                getWorkFiles().write(4, false, gdaIaag700.getGtn_Pda_E());                                                                                                //Natural: WRITE WORK FILE 4 GTN-PDA-E
                pnd_Cps_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CPS-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(7, false, gdaIaag700.getGtn_Pda_E());                                                                                                //Natural: WRITE WORK FILE 7 GTN-PDA-E
                pnd_Cps_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CPS-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Reset() throws Exception                                                                                                                             //Natural: RESET
    {
        if (BLNatReinput.isReinput()) return;

        pnd_New_Fund.setValue(true);                                                                                                                                      //Natural: ASSIGN #NEW-FUND := TRUE
        pnd_Indx1.setValue(0);                                                                                                                                            //Natural: ASSIGN #INDX1 := 0
        pnd_Indx2.setValue(0);                                                                                                                                            //Natural: ASSIGN #INDX2 := 0
        pnd_Fund_Ctr.setValue(0);                                                                                                                                         //Natural: ASSIGN #FUND-CTR := 0
        pnd_F_Indx.setValue(0);                                                                                                                                           //Natural: ASSIGN #F-INDX := 0
        pnd_D_Indx.setValue(0);                                                                                                                                           //Natural: ASSIGN #D-INDX := 0
        pnd_Error_Code_1.getValue("*").setValue("0");                                                                                                                     //Natural: ASSIGN #ERROR-CODE-1 ( * ) := '0'
        gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("0");                                                                                                             //Natural: ASSIGN ERROR-CODE-1 := '0'
        pnd_Cref_Amount.setValue(0);                                                                                                                                      //Natural: ASSIGN #CREF-AMOUNT := 0
        pnd_Total_Per_Pymnt_Amt.setValue(0);                                                                                                                              //Natural: ASSIGN #TOTAL-PER-PYMNT-AMT := 0
        //*  4/08
        //*  END ROTH 4/08
        //*  02/10
        gdaIaag700.getGtn_Pda_E().reset();                                                                                                                                //Natural: RESET GTN-PDA-E GTN-PDA-E.CNTRCT-UNQ-ID-NBR GTN-PDA-E.ANNT-SOC-SEC-IND ANNT-SOC-SEC-NBR ANNT-CTZNSHP-CDE PYMNT-DOB PYMNT-ADDR-LAST-CHG-DTE ( * ) PYMNT-ADDR-LAST-CHG-TME ( * ) INV-ACCT-SETTL-AMT ( * ) INV-ACCT-CNTRCT-AMT ( * ) INV-ACCT-DVDND-AMT ( * ) INV-ACCT-UNIT-VALUE ( * ) INV-ACCT-UNIT-QTY ( * ) INV-ACCT-IVC-AMT ( * ) PYMNT-DED-CDE ( * ) PYMNT-DED-AMT ( * ) PYMNT-SETTLMNT-DTE PYMNT-CHECK-DTE PYMNT-CYCLE-DTE PYMNT-ACCTG-DTE PYMNT-IA-ISSUE-DTE PYMNT-INTRFCE-DTE CNTRCT-OPTION-CDE CNTRCT-MODE-CDE PYMNT-PAY-TYPE-REQ-IND PYMNT-EFT-TRANSIT-ID PYMNT-SUSPEND-DTE ##THIS-PYMNT ##NBR-OF-PYMNTS CNTRCT-LOB-CDE CNTRCT-SUB-LOB-CDE CNTRCT-IA-LOB-CDE EGTRRA-ELIGIBILITY-IND DISTRIBUTING-IRC-CDE RECEIVING-IRC-CDE CNTRCT-MONEY-SOURCE ROTH-DOB ROTH-FIRST-CONTRIB-DTE ROTH-DEATH-DTE ROTH-DISABILITY-DTE ROTH-MONEY-SOURCE ROTH-QUAL-NON-QUAL-DISTRIB PYMNT-TAX-EXEMPT-IND
        gdaIaag700.getGtn_Pda_E_Cntrct_Unq_Id_Nbr().reset();
        gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Ind().reset();
        gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr().reset();
        gdaIaag700.getGtn_Pda_E_Annt_Ctznshp_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Dob().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Unit_Value().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Unit_Qty().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Cde().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Ded_Amt().getValue("*").reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Settlmnt_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Check_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Cycle_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Acctg_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Ia_Issue_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Intrfce_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Mode_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().reset();
        gdaIaag700.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Lob_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Sub_Lob_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Ia_Lob_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Egtrra_Eligibility_Ind().reset();
        gdaIaag700.getGtn_Pda_E_Distributing_Irc_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Receiving_Irc_Cde().reset();
        gdaIaag700.getGtn_Pda_E_Cntrct_Money_Source().reset();
        gdaIaag700.getGtn_Pda_E_Roth_Dob().reset();
        gdaIaag700.getGtn_Pda_E_Roth_First_Contrib_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Roth_Death_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Roth_Disability_Dte().reset();
        gdaIaag700.getGtn_Pda_E_Roth_Money_Source().reset();
        gdaIaag700.getGtn_Pda_E_Roth_Qual_Non_Qual_Distrib().reset();
        gdaIaag700.getGtn_Pda_E_Pymnt_Tax_Exempt_Ind().reset();
    }
    private void sub_Save_Cpr() throws Exception                                                                                                                          //Natural: SAVE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr.setValue(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr());                                                                         //Natural: ASSIGN #SAVE-PPCN-NBR := #PPCN-NBR
        pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Payee_Cde());                                                                       //Natural: ASSIGN #SAVE-PAYEE-CDE := #PAYEE-CDE
        pnd_Save_Status_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Status_Cde());                                                                                           //Natural: ASSIGN #SAVE-STATUS-CDE := #STATUS-CDE
        pnd_Save_Cmbne_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Cmbne_Cde());                                                                                             //Natural: ASSIGN #SAVE-CMBNE-CDE := #CMBNE-CDE
        pnd_Save_Cntrct_Ivc_Tot_T.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Ivc_Amt().getValue(1));                                                                     //Natural: ASSIGN #SAVE-CNTRCT-IVC-TOT-T := #CNTRCT-IVC-AMT ( 1 )
        pnd_Save_Cntrct_Ivc_Tot_C.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Ivc_Amt().getValue(2));                                                                     //Natural: ASSIGN #SAVE-CNTRCT-IVC-TOT-C := #CNTRCT-IVC-AMT ( 2 )
        //*           TPA CHANGE
        //*  ADDED 5/02
        //*  ADDED 5/02
        pnd_Save_Cntrct_Ivc_Amt_T.reset();                                                                                                                                //Natural: RESET #SAVE-CNTRCT-IVC-AMT-T #SAVE-CNTRCT-IVC-AMT-IK
        pnd_Save_Cntrct_Ivc_Amt_Ik.reset();
        //*  ADDED 5/03 NEW ROLL CODES
        //*  ADDED 5/03
        //*  ADDED 5/03
        if (condition(((pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(30)) && (((((((((((gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAT")  //Natural: IF ( #SAVE-CNTRCT-OPTN-CDE = 28 OR = 30 ) AND ( #INPUT.#CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = '57BT' OR = '57BC' OR = '57BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' )
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLX")))))
        {
            pnd_Save_Cntrct_Ivc_Amt_Ik.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Per_Ivc_Amt().getValue(1));                                                            //Natural: ASSIGN #SAVE-CNTRCT-IVC-AMT-IK := #CNTRCT-PER-IVC-AMT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Save_Cntrct_Ivc_Amt_T.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Per_Ivc_Amt().getValue(1));                                                             //Natural: ASSIGN #SAVE-CNTRCT-IVC-AMT-T := #CNTRCT-PER-IVC-AMT ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Cntrct_Ivc_Amt_T.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Per_Ivc_Amt().getValue(1));                                                                 //Natural: ASSIGN #SAVE-CNTRCT-IVC-AMT-T := #CNTRCT-PER-IVC-AMT ( 1 )
        pnd_Save_Cntrct_Ivc_Amt_C.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Per_Ivc_Amt().getValue(2));                                                                 //Natural: ASSIGN #SAVE-CNTRCT-IVC-AMT-C := #CNTRCT-PER-IVC-AMT ( 2 )
        pnd_Save_Cntrct_Ivc_Used_Amt_T.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Ivc_Used_Amt().getValue(1));                                                           //Natural: ASSIGN #SAVE-CNTRCT-IVC-USED-AMT-T := #CNTRCT-IVC-USED-AMT ( 1 )
        pnd_Save_Cntrct_Ivc_Used_Amt_C.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Ivc_Used_Amt().getValue(2));                                                           //Natural: ASSIGN #SAVE-CNTRCT-IVC-USED-AMT-C := #CNTRCT-IVC-USED-AMT ( 2 )
        pnd_Save_Rcvry_Type_Ind_T.setValue(gdaIaag700.getPnd_Input_Pnd_Rcvry_Type_Ind().getValue(1));                                                                     //Natural: ASSIGN #SAVE-RCVRY-TYPE-IND-T := #RCVRY-TYPE-IND ( 1 )
        pnd_Save_Rcvry_Type_Ind_C.setValue(gdaIaag700.getPnd_Input_Pnd_Rcvry_Type_Ind().getValue(2));                                                                     //Natural: ASSIGN #SAVE-RCVRY-TYPE-IND-C := #RCVRY-TYPE-IND ( 2 )
        pnd_Save_Cntrct_Hold_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Hold_Cde());                                                                                        //Natural: ASSIGN #SAVE-CNTRCT-HOLD-CDE := #HOLD-CDE
        pnd_Save_Cpr_Pay_Mode.setValue(gdaIaag700.getPnd_Input_Pnd_Mode_Ind());                                                                                           //Natural: ASSIGN #SAVE-CPR-PAY-MODE := #MODE-IND
        pnd_Save_Cntrct_Emp_Trmnt_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Emp_Trmnt_Cde());                                                                       //Natural: ASSIGN #SAVE-CNTRCT-EMP-TRMNT-CDE := #CNTRCT-EMP-TRMNT-CDE
        pnd_Save_Cntrct_Fin_Per_Pay_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte());                                                                   //Natural: ASSIGN #SAVE-CNTRCT-FIN-PER-PAY-DTE := #CNTRCT-FIN-PER-PAY-DTE
        pnd_Save_Curr_Dist_Cde.reset();                                                                                                                                   //Natural: RESET #SAVE-CURR-DIST-CDE
        //*  ADDED 5/02
        //*  ADDED 5/02
        //*  ADDED 5/02
        //*  ADDED 5/03
        //*  ADDED 5/03
        //*  ADDED 5/03
        if (condition(((pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(30)) && (((((((((((gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAT")  //Natural: IF ( #SAVE-CNTRCT-OPTN-CDE = 28 OR = 30 ) AND ( #INPUT.#CURR-DIST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = '57BT' OR = '57BC' OR = '57BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' )
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("IRAX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("03BX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("57BX")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLT")) 
            || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLC")) || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("QPLX")))))
        {
            pnd_Save_Curr_Dist_Cde.setValue(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde());                                                                                 //Natural: ASSIGN #SAVE-CURR-DIST-CDE := #CURR-DIST-CDE
            //*  ADDED 5/02
            //*  ADDED 5/03
            //*  ADDED 5/03
            //*  ADDED 5/03
            //*  ROTH 4/08
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Rllvr_Cntrct_Nbr.setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Cntrct_Nbr());                                                                               //Natural: ASSIGN #SAVE-RLLVR-CNTRCT-NBR := #RLLVR-CNTRCT-NBR
        pnd_Save_Rllvr_Ivc_Ind.setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Ivc_Ind());                                                                                     //Natural: ASSIGN #SAVE-RLLVR-IVC-IND := #RLLVR-IVC-IND
        pnd_Save_Rllvr_Elgble_Ind.setValue(gdaIaag700.getPnd_Input_Pnd_Rllvr_Elgble_Ind());                                                                               //Natural: ASSIGN #SAVE-RLLVR-ELGBLE-IND := #RLLVR-ELGBLE-IND
        pnd_Save_Roth_Dsblty_Dte.setValue(gdaIaag700.getPnd_Input_Pnd_Roth_Dsblty_Dte());                                                                                 //Natural: ASSIGN #SAVE-ROTH-DSBLTY-DTE := #INPUT.#ROTH-DSBLTY-DTE
    }
    private void sub_Set_Error_Code() throws Exception                                                                                                                    //Natural: SET-ERROR-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        short decideConditionsMet2247 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #ERROR-CODE-1 ( 1 ) = '1'
        if (condition(pnd_Error_Code_1.getValue(1).equals("1")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("1");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := '1'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 2 ) = '2'
        if (condition(pnd_Error_Code_1.getValue(2).equals("2")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("2");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := '2'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 3 ) = '3'
        if (condition(pnd_Error_Code_1.getValue(3).equals("3")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("3");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := '3'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 1 ) = '1' AND #ERROR-CODE-1 ( 2 ) = '2'
        if (condition(pnd_Error_Code_1.getValue(1).equals("1") && pnd_Error_Code_1.getValue(2).equals("2")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("A");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := 'A'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 1 ) = '1' AND #ERROR-CODE-1 ( 3 ) = '3'
        if (condition(pnd_Error_Code_1.getValue(1).equals("1") && pnd_Error_Code_1.getValue(3).equals("3")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("B");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := 'B'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 2 ) = '2' AND #ERROR-CODE-1 ( 3 ) = '3'
        if (condition(pnd_Error_Code_1.getValue(2).equals("2") && pnd_Error_Code_1.getValue(3).equals("3")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("C");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := 'C'
        }                                                                                                                                                                 //Natural: WHEN #ERROR-CODE-1 ( 1 ) = '1' AND #ERROR-CODE-1 ( 2 ) = '2' AND #ERROR-CODE-1 ( 3 ) = '3'
        if (condition(pnd_Error_Code_1.getValue(1).equals("1") && pnd_Error_Code_1.getValue(2).equals("2") && pnd_Error_Code_1.getValue(3).equals("3")))
        {
            decideConditionsMet2247++;
            gdaIaag700.getGtn_Pda_E_Error_Code_1().setValue("D");                                                                                                         //Natural: ASSIGN ERROR-CODE-1 := 'D'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2247 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Annt_Ins() throws Exception                                                                                                                      //Natural: GET-ANNT-INS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        //*  8/14 CHANGED TO PARTICIPATING
        short decideConditionsMet2270 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'IA' THRU 'IJ' OR SUBSTR ( #INPUT.#PPCN-NBR,1,1 ) = 'Y'
        if (condition(((gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).compareTo("IA") >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).compareTo("IJ") 
            <= 0) || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,1).equals("Y"))))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'GA'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("GA")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'IP'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("IP")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'GW' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '00001' THRU '86999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("GW") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("00001") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("86999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'GW' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '87000' THRU '87999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("GW") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("87000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("87999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("I");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'GW' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '88000' THRU '88999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("GW") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("88000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("88999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("C");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'GW' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '89000' THRU '89999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("GW") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("89000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("89999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("G");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '00001' THRU '86999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("00001") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("86999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '87000' THRU '87999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("87000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("87999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("I");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '88000' THRU '88999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("88000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("88999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("C");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '89000' THRU '89999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("89000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("89999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("G");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '00001' THRU '21999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("00001") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("21999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '22000' THRU '22999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("22000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("22999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("I");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '23000' THRU '23999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("23000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("23999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("C");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'C'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '24000' THRU '24999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("24000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("24999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("G");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '29000' THRU '29999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("29000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("29999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '37000' THRU '40999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("37000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("40999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '42000' THRU '42999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("42000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("42999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '47000' THRU '47999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("47000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("47999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '52000' THRU '52999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("52000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("52999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '58000' THRU '58999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("58000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("58999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '59000' THRU '59999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("59000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("59999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '80000' THRU '89999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("80000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("89999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '25000' THRU '28999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("25000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("28999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '30000' THRU '36999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("30000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("36999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '41000' THRU '41999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("41000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("41999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '43000' THRU '46999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("43000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("46999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            //*  8/14 START
            //*    CNTRCT-ANNTY-TYPE-CDE := 'P' /* SPLIT THE RANGE TO PART AND NON-PART
            if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("43000") >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("44999")  //Natural: IF SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '43000' THRU '44999'
                <= 0))
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("N");                                                                                            //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                            //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            }                                                                                                                                                             //Natural: END-IF
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '48000' THRU '51999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("48000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("51999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '53000' THRU '57999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("53000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("57999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'W0' AND SUBSTR ( #INPUT.#PPCN-NBR,3,5 ) = '60000' THRU '79999'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("W0") && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("60000") 
            >= 0 && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(3,5).compareTo("79999") <= 0))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("G");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'G'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '0L'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("0L")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '0M' OR = '0T' OR = '0U' OR SUBSTR ( #INPUT.#PPCN-NBR,1,1 ) = 'Z'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("0M") || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("0T") 
            || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("0U") || gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,1).equals("Z")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '0N'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("0N")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '6L'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("6L")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '6M'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("6M")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: WHEN SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = '6N'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("6N")))
        {
            decideConditionsMet2270++;
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  TPA CHANGES
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(22) || pnd_Save_Cntrct_Optn_Cde.equals(23)))                                                                        //Natural: IF ( #SAVE-CNTRCT-OPTN-CDE = 22 OR = 23 )
        {
            if (condition((gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0"))))                                                                       //Natural: IF ( SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' )
            {
                //*  11/06
                if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("03") || pnd_Save_Cntrct_Orgn_Cde.equals("46")))                                                            //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '03' OR = '46'
                {
                    gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                        //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("U");                                                                                        //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'U'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("C");                                                                                            //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'C'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/14 - START
        if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21)))                                                                                                               //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21
        {
            if (condition((gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().getSubstring(1,2).equals("S0"))))                                                                       //Natural: IF ( SUBSTR ( #INPUT.#PPCN-NBR,1,2 ) = 'S0' )
            {
                if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("03") || pnd_Save_Cntrct_Orgn_Cde.equals("46")))                                                            //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '03' OR = '46'
                {
                    gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("I");                                                                                        //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'I'
                    if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().equals(" ")))                                                                         //Natural: IF CNTRCT-INSURANCE-OPTION = ' '
                    {
                        gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue("I");                                                                                  //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := 'I'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  11/14 - END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("17")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '17'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21)))                                                                                                           //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                          //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                          //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("18")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '18'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("P");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'P'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING NEW PA ORIGIN CODES 37 & 38  1/99
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("37")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '37'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("S");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'S'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("D");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'D'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            if (condition(pnd_Save_Cntrct_Optn_Cde.equals(21)))                                                                                                           //Natural: IF #SAVE-CNTRCT-OPTN-CDE = 21
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("N");                                                                                          //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue("Y");                                                                                          //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/06
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("38") || pnd_Save_Cntrct_Orgn_Cde.equals("43") || pnd_Save_Cntrct_Orgn_Cde.equals("45")))                           //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '38' OR = '43' OR = '45'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("S");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'S'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 1/99
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("40")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '40'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-INSURANCE-OPTION := ' '
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().setValue(" ");                                                                                              //Natural: ASSIGN CNTRCT-LIFE-CONTINGENCY := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 2/2000
        //*  02/10 BEGIN
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("35") || pnd_Save_Cntrct_Orgn_Cde.equals("36") || pnd_Save_Cntrct_Orgn_Cde.equals("44")))                           //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '35' OR = '36' OR = '44'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Ins_Type().setValue("S");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-INS-TYPE := 'S'
            gdaIaag700.getGtn_Pda_E_Cntrct_Annty_Type_Cde().setValue("M");                                                                                                //Natural: ASSIGN CNTRCT-ANNTY-TYPE-CDE := 'M'
            gdaIaag700.getGtn_Pda_E_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                                //Natural: ASSIGN CNTRCT-PYMNT-TYPE-IND := 'A'
            gdaIaag700.getGtn_Pda_E_Cntrct_Insurance_Option().reset();                                                                                                    //Natural: RESET CNTRCT-INSURANCE-OPTION CNTRCT-LIFE-CONTINGENCY
            gdaIaag700.getGtn_Pda_E_Cntrct_Life_Contingency().reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  02/10 BEGIN
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("35")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '35'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("1");                                                                                              //Natural: ASSIGN CNTRCT-STTLMNT-TYPE-IND := '1'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("36")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '36'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("2");                                                                                              //Natural: ASSIGN CNTRCT-STTLMNT-TYPE-IND := '2'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("44")))                                                                                                             //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '44'
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Sttlmnt_Type_Ind().setValue("3");                                                                                              //Natural: ASSIGN CNTRCT-STTLMNT-TYPE-IND := '3'
            //*  02/10 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOLLOWING FOR SELF-REMITTER ORIGIN 42  3/03
        //*  3/07
        if (condition(pnd_Save_Cntrct_Orgn_Cde.equals("42") || pnd_Save_Cntrct_Orgn_Cde.equals("39") || pnd_Save_Cntrct_Orgn_Cde.equals("47") || pnd_Save_Cntrct_Orgn_Cde.equals("63")  //Natural: IF #SAVE-CNTRCT-ORGN-CDE = '42' OR = '39' OR = '47' OR = '63' OR = '64' OR = '67' OR = '68'
            || pnd_Save_Cntrct_Orgn_Cde.equals("64") || pnd_Save_Cntrct_Orgn_Cde.equals("67") || pnd_Save_Cntrct_Orgn_Cde.equals("68")))
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Money_Source().setValue("SR1");                                                                                                //Natural: ASSIGN CNTRCT-MONEY-SOURCE := 'SR1'
            //*  ADDED BLANK 12/03
            if (condition(gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("CASH") || gdaIaag700.getPnd_Input_Pnd_Curr_Dist_Cde().equals("    ")))                      //Natural: IF #INPUT.#CURR-DIST-CDE = 'CASH' OR = '    '
            {
                if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("S") || gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("T")))                        //Natural: IF GTN-PDA-E.PYMNT-SUSPEND-CDE = 'S' OR = 'T'
                {
                    gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().setValue("S");                                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-SUSPEND-CDE := 'S'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF ADD 3/03
    }
    private void sub_Process_Fund() throws Exception                                                                                                                      //Natural: PROCESS-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Proc_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
        sub_Check_Mode();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                      //Natural: IF #PAYMENT-DUE
        {
            gdaIaag700.getGtn_Pda_E_Current_Mode().setValue("1");                                                                                                         //Natural: ASSIGN GTN-PDA-E.CURRENT-MODE := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Current_Mode().setValue("0");                                                                                                         //Natural: ASSIGN GTN-PDA-E.CURRENT-MODE := '0'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Fund_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #FUND-CTR
        pnd_F_Indx.setValue(pnd_Fund_Ctr);                                                                                                                                //Natural: ASSIGN #F-INDX := #FUND-CTR
                                                                                                                                                                          //Natural: PERFORM GET-FUND-COMPANY
        sub_Get_Fund_Company();
        if (condition(Global.isEscape())) {return;}
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Valuat_Period().getValue(pnd_F_Indx).setValue("A");                                                                              //Natural: ASSIGN GTN-PDA-E.INV-ACCT-VALUAT-PERIOD ( #F-INDX ) := 'A'
        short decideConditionsMet2584 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SUMM-CMPNY-CDE = 'T'
        if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("T")))
        {
            decideConditionsMet2584++;
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde());                                            //Natural: ASSIGN GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) := #SUMM-FUND-CDE
            //*  CHANGE MADE 05/2001 TPA CHANGE
            //*    ADD #SUMM-PER-IVC-AMT TO GTN-PDA-E.INV-ACCT-SETTL-AMT(#F-INDX)
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(pnd_F_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                         //Natural: ADD #SUMM-PER-PYMNT TO GTN-PDA-E.INV-ACCT-SETTL-AMT ( #F-INDX )
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(pnd_F_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                         //Natural: ADD #SUMM-PER-DVDND TO GTN-PDA-E.INV-ACCT-SETTL-AMT ( #F-INDX )
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_F_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                        //Natural: ADD #SUMM-PER-PYMNT TO GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #F-INDX )
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_F_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                         //Natural: ADD #SUMM-PER-DVDND TO GTN-PDA-E.INV-ACCT-DVDND-AMT ( #F-INDX )
            pnd_Total_Per_Pymnt_Amt.nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                                   //Natural: ADD #SUMM-PER-PYMNT TO #TOTAL-PER-PYMNT-AMT
        }                                                                                                                                                                 //Natural: WHEN #SUMM-CMPNY-CDE = 'U' OR = 'W' OR = '2' OR = '4'
        else if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("U") || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("W") || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("2") 
            || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("4")))
        {
            decideConditionsMet2584++;
            gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde(), MoveOption.RightJustified);                                 //Natural: MOVE RIGHT #SUMM-FUND-CDE TO #SUMM-FUND-CDE
            DbsUtil.examine(new ExamineSource(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde()), new ExamineSearch(" "), new ExamineReplace("0"));                             //Natural: EXAMINE #SUMM-FUND-CDE FOR ' ' REPLACE '0'
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde());                                            //Natural: ASSIGN GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) := #SUMM-FUND-CDE
            if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().equals("07")))                                                                                      //Natural: IF #SUMM-FUND-CDE = '07'
            {
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).setValue("08");                                                                               //Natural: ASSIGN GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) := '08'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().equals("08")))                                                                                  //Natural: IF #SUMM-FUND-CDE = '08'
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_F_Indx).setValue("07");                                                                           //Natural: ASSIGN GTN-PDA-E.INV-ACCT-CDE ( #F-INDX ) := '07'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("W") || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("4")))                          //Natural: IF #SUMM-CMPNY-CDE = 'W' OR = '4'
            {
                                                                                                                                                                          //Natural: PERFORM AIAN026
                sub_Aian026();
                if (condition(Global.isEscape())) {return;}
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Valuat_Period().getValue(pnd_F_Indx).setValue("M");                                                                      //Natural: ASSIGN GTN-PDA-E.INV-ACCT-VALUAT-PERIOD ( #F-INDX ) := 'M'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Amount.setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                                            //Natural: ASSIGN #AMOUNT := #SUMM-PER-PYMNT
            pnd_Cref_Amount.nadd(pnd_Amount);                                                                                                                             //Natural: ADD #AMOUNT TO #CREF-AMOUNT
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Unit_Value().getValue(pnd_F_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd_R());                                  //Natural: ASSIGN GTN-PDA-E.INV-ACCT-UNIT-VALUE ( #F-INDX ) := #SUMM-PER-DVDND-R
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(pnd_F_Indx).setValue(pnd_Amount);                                                                       //Natural: ASSIGN GTN-PDA-E.INV-ACCT-SETTL-AMT ( #F-INDX ) := #AMOUNT
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Unit_Qty().getValue(pnd_F_Indx).setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Units());                                          //Natural: ASSIGN GTN-PDA-E.INV-ACCT-UNIT-QTY ( #F-INDX ) := #SUMM-UNITS
            pnd_Total_Per_Pymnt_Amt.nadd(pnd_Amount);                                                                                                                     //Natural: ADD #AMOUNT TO #TOTAL-PER-PYMNT-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM CONTROL-RECORD
        sub_Control_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Ivc() throws Exception                                                                                                                               //Natural: IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Tiaafslash_Cref.equals("T")))                                                                                                                   //Natural: IF #TIAA/CREF = 'T'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Settl_Ivc_Ind().setValue(pnd_Save_Rcvry_Type_Ind_T);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-SETTL-IVC-IND := #SAVE-RCVRY-TYPE-IND-T
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Ind().getValue(1).setValue(pnd_Save_Rcvry_Type_Ind_T);                                                                   //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-IND ( 1 ) := #SAVE-RCVRY-TYPE-IND-T
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Settl_Ivc_Ind().setValue(pnd_Save_Rcvry_Type_Ind_C);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-SETTL-IVC-IND := #SAVE-RCVRY-TYPE-IND-C
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Ind().getValue(1).setValue(pnd_Save_Rcvry_Type_Ind_C);                                                                   //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-IND ( 1 ) := #SAVE-RCVRY-TYPE-IND-C
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED 5/03
        //*  ADDED 5/03
        //*  ADDED 5/03
        //*  10/2010
        if (condition((((gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(28) || gdaIaag700.getGtn_Pda_E_Cntrct_Option_Cde().equals(30)) && (((((((((((gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT")  //Natural: IF ( GTN-PDA-E.CNTRCT-OPTION-CDE = 28 OR = 30 ) AND ( GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = 'IRAX' OR = '03BT' OR = '03BC' OR = '03BX' OR = '57BT' OR = '57BC' OR = '57BX' OR = 'QPLT' OR = 'QPLC' OR = 'QPLX' ) OR SUBSTR ( GTN-PDA-E.CNTRCT-PAYEE-CDE,3,2 ) = 'IV'
            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT")) 
            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT")) 
            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BX")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT")) 
            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC")) || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLX"))) || gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde().getSubstring(3,
            2).equals("IV"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Rcvry_Type_Ind_C.equals("0") && pnd_Save_Rcvry_Type_Ind_T.equals("0")))                                                                    //Natural: IF #SAVE-RCVRY-TYPE-IND-C = '0' AND #SAVE-RCVRY-TYPE-IND-T = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2644 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA/CREF = 'C' AND #SAVE-RCVRY-TYPE-IND-C = '1'
        if (condition(pnd_Tiaafslash_Cref.equals("C") && pnd_Save_Rcvry_Type_Ind_C.equals("1")))
        {
            decideConditionsMet2644++;
            if (condition(pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde.greater(2)))                                                                                           //Natural: IF #SAVE-PAYEE-CDE GT 02
            {
                pnd_Remain_Ivc.compute(new ComputeParameters(false, pnd_Remain_Ivc), pnd_Save_Cntrct_Ivc_Tot_C.subtract(pnd_Save_Cntrct_Ivc_Used_Amt_C));                 //Natural: COMPUTE #REMAIN-IVC = #SAVE-CNTRCT-IVC-TOT-C - #SAVE-CNTRCT-IVC-USED-AMT-C
                if (condition(pnd_Remain_Ivc.less(getZero())))                                                                                                            //Natural: IF #REMAIN-IVC LT 0
                {
                    pnd_Remain_Ivc.setValue(0);                                                                                                                           //Natural: ASSIGN #REMAIN-IVC := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Total_Per_Pymnt_Amt.lessOrEqual(pnd_Remain_Ivc)))                                                                                       //Natural: IF #TOTAL-PER-PYMNT-AMT LE #REMAIN-IVC
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                             //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Remain_Ivc);                                                                      //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #REMAIN-IVC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                                 //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #TIAA/CREF = 'T' AND #SAVE-RCVRY-TYPE-IND-T = '1'
        else if (condition(pnd_Tiaafslash_Cref.equals("T") && pnd_Save_Rcvry_Type_Ind_T.equals("1")))
        {
            decideConditionsMet2644++;
            if (condition(pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde.greater(2)))                                                                                           //Natural: IF #SAVE-PAYEE-CDE GT 02
            {
                pnd_Remain_Ivc.compute(new ComputeParameters(false, pnd_Remain_Ivc), pnd_Save_Cntrct_Ivc_Tot_T.subtract(pnd_Save_Cntrct_Ivc_Used_Amt_T));                 //Natural: COMPUTE #REMAIN-IVC = #SAVE-CNTRCT-IVC-TOT-T - #SAVE-CNTRCT-IVC-USED-AMT-T
                if (condition(pnd_Remain_Ivc.less(getZero())))                                                                                                            //Natural: IF #REMAIN-IVC LT 0
                {
                    pnd_Remain_Ivc.setValue(0);                                                                                                                           //Natural: ASSIGN #REMAIN-IVC := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Total_Per_Pymnt_Amt.lessOrEqual(pnd_Remain_Ivc)))                                                                                       //Natural: IF #TOTAL-PER-PYMNT-AMT LE #REMAIN-IVC
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                             //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Remain_Ivc);                                                                      //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #REMAIN-IVC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                                 //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #TIAA/CREF = 'C' AND #SAVE-RCVRY-TYPE-IND-C NE '1'
        else if (condition(pnd_Tiaafslash_Cref.equals("C") && pnd_Save_Rcvry_Type_Ind_C.notEquals("1")))
        {
            decideConditionsMet2644++;
            if (condition(pnd_Save_Cntrct_Ivc_Amt_C.less(getZero())))                                                                                                     //Natural: IF #SAVE-CNTRCT-IVC-AMT-C LT 0
            {
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(0);                                                                                       //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := 0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  12/13 - START
                if (condition(pnd_Total_Per_Pymnt_Amt.less(pnd_Save_Cntrct_Ivc_Amt_C)))                                                                                   //Natural: IF #TOTAL-PER-PYMNT-AMT LT #SAVE-CNTRCT-IVC-AMT-C
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                             //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Save_Cntrct_Ivc_Amt_C);                                                           //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #SAVE-CNTRCT-IVC-AMT-C
                }                                                                                                                                                         //Natural: END-IF
                //*  12/13 - END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #TIAA/CREF = 'T' AND #SAVE-RCVRY-TYPE-IND-C NE '1'
        else if (condition(pnd_Tiaafslash_Cref.equals("T") && pnd_Save_Rcvry_Type_Ind_C.notEquals("1")))
        {
            decideConditionsMet2644++;
            if (condition(pnd_Save_Cntrct_Ivc_Amt_T.less(getZero())))                                                                                                     //Natural: IF #SAVE-CNTRCT-IVC-AMT-T LT 0
            {
                gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(0);                                                                                       //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := 0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  12/13 - START
                if (condition(pnd_Total_Per_Pymnt_Amt.less(pnd_Save_Cntrct_Ivc_Amt_T)))                                                                                   //Natural: IF #TOTAL-PER-PYMNT-AMT LT #SAVE-CNTRCT-IVC-AMT-T
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Total_Per_Pymnt_Amt);                                                             //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #TOTAL-PER-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Save_Cntrct_Ivc_Amt_T);                                                           //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #SAVE-CNTRCT-IVC-AMT-T
                }                                                                                                                                                         //Natural: END-IF
                //*  12/13 - END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Total() throws Exception                                                                                                                     //Natural: PROCESS-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
        sub_Check_Mode();
        if (condition(Global.isEscape())) {return;}
        if (condition(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee().notEquals(pnd_Prev_Cntrct_Payee)))                                                                       //Natural: IF #INPUT.#CNTRCT-PAYEE NE #PREV-CNTRCT-PAYEE
        {
            pnd_S_Rec_Count.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #S-REC-COUNT ( 1 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_S_Rec_Count.getValue(2).nadd(1);                                                                                                                      //Natural: ADD 1 TO #S-REC-COUNT ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(30)))                                                                                              //Natural: IF #RECORD-CODE = 30
        {
                                                                                                                                                                          //Natural: PERFORM GET-FUND-COMPANY
            sub_Get_Fund_Company();
            if (condition(Global.isEscape())) {return;}
            short decideConditionsMet2718 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SUMM-CMPNY-CDE = 'T'
            if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("T")))
            {
                decideConditionsMet2718++;
                pnd_S_Cntrct_Amt.getValue(1).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                          //Natural: ADD #SUMM-PER-PYMNT TO #S-CNTRCT-AMT ( 1 )
                pnd_S_Gross_Amt.getValue(1).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                           //Natural: ADD #SUMM-PER-PYMNT TO #S-GROSS-AMT ( 1 )
                pnd_S_Dvdnd_Amt.getValue(1).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                                                           //Natural: ADD #SUMM-PER-DVDND TO #S-DVDND-AMT ( 1 )
                pnd_S_Gross_Amt.getValue(1).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                                                           //Natural: ADD #SUMM-PER-DVDND TO #S-GROSS-AMT ( 1 )
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_S_Cntrct_Amt.getValue(2).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                      //Natural: ADD #SUMM-PER-PYMNT TO #S-CNTRCT-AMT ( 2 )
                    pnd_S_Gross_Amt.getValue(2).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                       //Natural: ADD #SUMM-PER-PYMNT TO #S-GROSS-AMT ( 2 )
                    pnd_S_Dvdnd_Amt.getValue(2).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                                                       //Natural: ADD #SUMM-PER-DVDND TO #S-DVDND-AMT ( 2 )
                    pnd_S_Gross_Amt.getValue(2).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                                                       //Natural: ADD #SUMM-PER-DVDND TO #S-GROSS-AMT ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #SUMM-CMPNY-CDE = 'U' OR = 'W' OR = '2' OR = '4'
            else if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("U") || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("W") || 
                gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("2") || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("4")))
            {
                decideConditionsMet2718++;
                pnd_Amount.setValue(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                                                                        //Natural: ASSIGN #AMOUNT := #SUMM-PER-PYMNT
                pnd_S_Cref_Amt.getValue(1).nadd(pnd_Amount);                                                                                                              //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 1 )
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_S_Cref_Amt.getValue(2).nadd(pnd_Amount);                                                                                                          //Natural: ADD #AMOUNT TO #S-CREF-AMT ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_S_Ded_Amt.getValue(1).nadd(gdaIaag700.getPnd_Input_Pnd_Ddctn_Per_Amt());                                                                                  //Natural: ADD #DDCTN-PER-AMT TO #S-DED-AMT ( 1 )
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_S_Ded_Amt.getValue(2).nadd(gdaIaag700.getPnd_Input_Pnd_Ddctn_Per_Amt());                                                                              //Natural: ADD #DDCTN-PER-AMT TO #S-DED-AMT ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Control_Record() throws Exception                                                                                                                    //Natural: CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals(" ")))                                                                                           //Natural: IF PYMNT-SUSPEND-CDE = ' '
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().setValue("0");                                                                                                    //Natural: ASSIGN PYMNT-SUSPEND-CDE := '0'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaIaag700.getGtn_Pda_E_Error_Code_1().equals("1") && gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0")))                                     //Natural: IF ERROR-CODE-1 = '1' AND PYMNT-SUSPEND-CDE = '0'
        {
            pnd_C_Indx.setValue(7);                                                                                                                                       //Natural: ASSIGN #C-INDX := 7
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(16);                                                                                                                                  //Natural: ASSIGN #C-INDX := 16
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0")))                                                                                           //Natural: IF PYMNT-SUSPEND-CDE = '0'
        {
            //*  ADDED 5/03
            if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC")                  //Natural: IF GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
                || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT") 
                || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")))
            {
                pnd_C_Indx.setValue(4);                                                                                                                                   //Natural: ASSIGN #C-INDX := 4
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(13);                                                                                                                              //Natural: ASSIGN #C-INDX := 13
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  INTERNAL ROLLOVER
        short decideConditionsMet2778 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF PYMNT-SUSPEND-CDE;//Natural: VALUE '0'
        if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0"))))
        {
            decideConditionsMet2778++;
            //*  CHECK
            short decideConditionsMet2781 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet2781++;
                pnd_C_Indx.setValue(1);                                                                                                                                   //Natural: ASSIGN #C-INDX := 1
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(10);                                                                                                                              //Natural: ASSIGN #C-INDX := 10
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  EFT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet2781++;
                pnd_C_Indx.setValue(2);                                                                                                                                   //Natural: ASSIGN #C-INDX := 2
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(11);                                                                                                                              //Natural: ASSIGN #C-INDX := 11
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  GLOBAL PAY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(3) || gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet2781++;
                pnd_C_Indx.setValue(3);                                                                                                                                   //Natural: ASSIGN #C-INDX := 3
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(12);                                                                                                                              //Natural: ASSIGN #C-INDX := 12
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                    //*  MISCELLANEOUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                pnd_C_Indx.setValue(9);                                                                                                                                   //Natural: ASSIGN #C-INDX := 9
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Payment_Due.getBoolean()))                                                                                                              //Natural: IF #PAYMENT-DUE
                {
                    pnd_C_Indx.setValue(18);                                                                                                                              //Natural: ASSIGN #C-INDX := 18
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                    sub_Accumulate_Control_Record();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  INVALID COMBINES
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("2"))))
        {
            decideConditionsMet2778++;
            pnd_C_Indx.setValue(8);                                                                                                                                       //Natural: ASSIGN #C-INDX := 8
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(17);                                                                                                                                  //Natural: ASSIGN #C-INDX := 17
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                //*  BLOCKED ACCOUNTS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("I"))))
        {
            decideConditionsMet2778++;
            pnd_C_Indx.setValue(5);                                                                                                                                       //Natural: ASSIGN #C-INDX := 5
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(14);                                                                                                                                  //Natural: ASSIGN #C-INDX := 14
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                //*  UNCLAIMED SUMS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'K'
        else if (condition((gdaIaag700.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("K"))))
        {
            decideConditionsMet2778++;
            pnd_C_Indx.setValue(6);                                                                                                                                       //Natural: ASSIGN #C-INDX := 6
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(15);                                                                                                                                  //Natural: ASSIGN #C-INDX := 15
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
                //*  MISCELLANEOUS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            pnd_C_Indx.setValue(9);                                                                                                                                       //Natural: ASSIGN #C-INDX := 9
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
            sub_Accumulate_Control_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Payment_Due.getBoolean()))                                                                                                                  //Natural: IF #PAYMENT-DUE
            {
                pnd_C_Indx.setValue(18);                                                                                                                                  //Natural: ASSIGN #C-INDX := 18
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-CONTROL-RECORD
                sub_Accumulate_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Accumulate_Control_Record() throws Exception                                                                                                         //Natural: ACCUMULATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee().notEquals(pnd_Prev_Cntrct_Payee)))                                                                       //Natural: IF #INPUT.#CNTRCT-PAYEE NE #PREV-CNTRCT-PAYEE
        {
            pnd_Control_Rec_Array_Pnd_C_Rec_Count.getValue(pnd_C_Indx).nadd(1);                                                                                           //Natural: ADD 1 TO #C-REC-COUNT ( #C-INDX )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaIaag700.getPnd_Input_Pnd_Record_Code().equals(30)))                                                                                              //Natural: IF #RECORD-CODE = 30
        {
            if (condition(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().equals("1 ") || gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().equals("1G") || gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde().equals("1S"))) //Natural: IF #SUMM-FUND-CDE = '1 ' OR = '1G' OR = '1S'
            {
                pnd_Control_Rec_Array_Pnd_C_Cntrct_Amt.getValue(pnd_C_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                           //Natural: ADD #SUMM-PER-PYMNT TO #C-CNTRCT-AMT ( #C-INDX )
                pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt());                                            //Natural: ADD #SUMM-PER-PYMNT TO #C-GROSS-AMT ( #C-INDX )
                pnd_Control_Rec_Array_Pnd_C_Dvdnd_Amt.getValue(pnd_C_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                            //Natural: ADD #SUMM-PER-DVDND TO #C-DVDND-AMT ( #C-INDX )
                pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd());                                            //Natural: ADD #SUMM-PER-DVDND TO #C-GROSS-AMT ( #C-INDX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Control_Rec_Array_Pnd_C_Cref_Amt.getValue(pnd_C_Indx).nadd(pnd_Cref_Amount);                                                                          //Natural: ADD #CREF-AMOUNT TO #C-CREF-AMT ( #C-INDX )
                pnd_Control_Rec_Array_Pnd_C_Gross_Amt.getValue(pnd_C_Indx).nadd(pnd_Cref_Amount);                                                                         //Natural: ADD #CREF-AMOUNT TO #C-GROSS-AMT ( #C-INDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Control_Rec_Array_Pnd_C_Ded_Amt.getValue(pnd_C_Indx).nadd(gdaIaag700.getPnd_Input_Pnd_Ddctn_Per_Amt());                                                   //Natural: ADD #DDCTN-PER-AMT TO #C-DED-AMT ( #C-INDX )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Mode() throws Exception                                                                                                                        //Natural: CHECK-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Payment_Due.reset();                                                                                                                                          //Natural: RESET #PAYMENT-DUE
        //*  FOR OLD TPA CNT
        if (condition(((pnd_Save_Cntrct_Optn_Cde.equals(28) || pnd_Save_Cntrct_Optn_Cde.equals(30)) && pnd_Save_1st_Due_Dte.greater(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm)))) //Natural: IF ( #SAVE-CNTRCT-OPTN-CDE = 28 OR = 30 ) AND #SAVE-1ST-DUE-DTE > #SAVE-CHECK-DTE-YYYYMM
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet2881 = 0;                                                                                                                            //Natural: DECIDE ON FIRST #SAVE-CPR-PAY-MODE;//Natural: VALUE 100
            if (condition((pnd_Save_Cpr_Pay_Mode.equals(100))))
            {
                decideConditionsMet2881++;
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: VALUE 601
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(601))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)                     //Natural: IF #MM = 01 OR = 04 OR = 07 OR = 10
                    || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 602
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(602))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)                     //Natural: IF #MM = 02 OR = 05 OR = 08 OR = 11
                    || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 603
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(603))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)                     //Natural: IF #MM = 03 OR = 06 OR = 09 OR = 12
                    || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 701
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(701))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                            //Natural: IF #MM = 01 OR = 07
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 702
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(702))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                            //Natural: IF #MM = 02 OR = 08
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 703
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(703))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                            //Natural: IF #MM = 03 OR = 09
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 704
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(704))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                           //Natural: IF #MM = 04 OR = 10
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 705
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(705))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                           //Natural: IF #MM = 05 OR = 11
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 706
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(706))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                           //Natural: IF #MM = 06 OR = 12
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 801
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(801))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1)))                                                                                                     //Natural: IF #MM = 01
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 802
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(802))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2)))                                                                                                     //Natural: IF #MM = 02
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 803
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(803))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3)))                                                                                                     //Natural: IF #MM = 03
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 804
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(804))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4)))                                                                                                     //Natural: IF #MM = 04
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 805
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(805))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5)))                                                                                                     //Natural: IF #MM = 05
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 806
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(806))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6)))                                                                                                     //Natural: IF #MM = 06
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 807
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(807))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                                                     //Natural: IF #MM = 07
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 808
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(808))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                                                     //Natural: IF #MM = 08
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 809
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(809))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                                                     //Natural: IF #MM = 09
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 810
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(810))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                                                                    //Natural: IF #MM = 10
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 811
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(811))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                                                                    //Natural: IF #MM = 11
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 812
            else if (condition((pnd_Save_Cpr_Pay_Mode.equals(812))))
            {
                decideConditionsMet2881++;
                if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                                                                    //Natural: IF #MM = 12
                {
                    pnd_Payment_Due.setValue(true);                                                                                                                       //Natural: ASSIGN #PAYMENT-DUE := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Aian026() throws Exception                                                                                                                           //Natural: AIAN026
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Parm_Fund_1.setValue(" ");                                                                                                                                    //Natural: ASSIGN #PARM-FUND-1 := ' '
        DbsUtil.examine(new ExamineSource(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue("*"),true), new ExamineSearch(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde(),  //Natural: EXAMINE FULL IAAA051Z.#IA-STD-NM-CD ( * ) FOR FULL #SUMM-FUND-CDE GIVING INDEX #I
            true), new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            pnd_Parm_Fund_1.setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                                                      //Natural: ASSIGN #PARM-FUND-1 := IAAA051Z.#IA-STD-ALPHA-CD ( #I )
            ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();                                                                                                      //Natural: RESET AUV-RETURNED RETURN-CODE AUV-DATE-RETURN
            ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();
            ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
            ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_9_Pnd_Call_Type);                                                              //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(pnd_Parm_Fund_1);                                                                                    //Natural: ASSIGN IA-FUND-CODE := #PARM-FUND-1
            ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                              //Natural: ASSIGN IA-REVAL-METHD := 'M'
            if (condition(pnd_Input_Record_Work_9_Pnd_Call_Type.equals("L")))                                                                                             //Natural: IF #CALL-TYPE = 'L'
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                      //Natural: ASSIGN IA-CHECK-DATE := 99999999
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);                                                       //Natural: ASSIGN IA-CHECK-DATE := #SAVE-CHECK-DTE
            }                                                                                                                                                             //Natural: END-IF
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Ccyymm().setValue(pnd_Save_Issue_Dte);                                                                              //Natural: ASSIGN IA-ISSUE-CCYYMM := #SAVE-ISSUE-DTE
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(1);                                                                                                  //Natural: ASSIGN IA-ISSUE-DAY := 01
            DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
            if (condition(Global.isEscape())) return;
            if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().equals(getZero())))                                                                              //Natural: IF RETURN-CODE = 0
            {
                gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt().compute(new ComputeParameters(true, gdaIaag700.getPnd_Input_Pnd_Summ_Per_Pymnt()), ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().multiply(gdaIaag700.getPnd_Input_Pnd_Summ_Units())); //Natural: COMPUTE ROUNDED #SUMM-PER-PYMNT = AUV-RETURNED * #SUMM-UNITS
                gdaIaag700.getPnd_Input_Pnd_Summ_Per_Dvdnd_R().setValue(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                 //Natural: ASSIGN #SUMM-PER-DVDND-R := AUV-RETURNED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "AIAN026 RETURN CODE NE 0 FOR: ",pnd_Save_Cntrct_Payee_Pnd_Save_Ppcn_Nbr,pnd_Save_Cntrct_Payee_Pnd_Save_Payee_Cde,                  //Natural: WRITE 'AIAN026 RETURN CODE NE 0 FOR: ' #SAVE-PPCN-NBR #SAVE-PAYEE-CDE '=' IA-CALL-TYPE '=' IA-FUND-CODE '=' IA-REVAL-METHD '=' IA-CHECK-DATE '=' RETURN-CODE
                    "=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type(),"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code(),"=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd(),
                    "=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date(),"=",ldaIaal050.getIa_Aian026_Linkage_Return_Code());
                if (Global.isEscape()) return;
                DbsUtil.terminate(64);  if (true) return;                                                                                                                 //Natural: TERMINATE 64
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "INVALID FUND FOR: ",gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr(),gdaIaag700.getPnd_Input_Pnd_Payee_Cde(),"=",gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde()); //Natural: WRITE 'INVALID FUND FOR: ' #PPCN-NBR #PAYEE-CDE '=' #SUMM-FUND-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Core() throws Exception                                                                                                                          //Natural: GET-CORE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Core_Eof.getBoolean()))                                                                                                                         //Natural: IF #CORE-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  02/15 REJECT IF NOT ACTIVE
            if (condition(gdaIaag700.getPnd_Core_Cntrct_Status_Cde().notEquals(" ")))                                                                                     //Natural: IF #CORE.CNTRCT-STATUS-CDE NE ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaIaag700.getPnd_Core_Cntrct_Payee().greater(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee())))                                                 //Natural: IF #CORE.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gdaIaag700.getPnd_Core_Cntrct_Payee().equals(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee())))                                                  //Natural: IF #CORE.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
                {
                    gdaIaag700.getGtn_Pda_E_Ph_Last_Name().setValue(gdaIaag700.getPnd_Core_Ph_Last_Nme_16());                                                             //Natural: ASSIGN GTN-PDA-E.PH-LAST-NAME := #CORE.PH-LAST-NME-16
                    gdaIaag700.getGtn_Pda_E_Ph_Middle_Name().setValue(gdaIaag700.getPnd_Core_Ph_Mddle_Nme_12());                                                          //Natural: ASSIGN GTN-PDA-E.PH-MIDDLE-NAME := #CORE.PH-MDDLE-NME-12
                    gdaIaag700.getGtn_Pda_E_Ph_First_Name().setValue(gdaIaag700.getPnd_Core_Ph_First_Nme_10());                                                           //Natural: ASSIGN GTN-PDA-E.PH-FIRST-NAME := #CORE.PH-FIRST-NME-10
                    if (condition(gdaIaag700.getPnd_Core_Ph_Social_Security_No().notEquals(getZero())))                                                                   //Natural: IF #CORE.PH-SOCIAL-SECURITY-NO NE 0
                    {
                        gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr().setValue(gdaIaag700.getPnd_Core_Ph_Social_Security_No());                                              //Natural: ASSIGN GTN-PDA-E.ANNT-SOC-SEC-NBR := #CORE.PH-SOCIAL-SECURITY-NO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(2, gdaIaag700.getPnd_Core());                                                                                                             //Natural: READ WORK 2 ONCE #CORE
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Core_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #CORE-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  4/09
            pnd_Core_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CORE-CTR
            //*  IF #CORE.CNTRCT-STATUS-CDE NE ' ' /* 02/15 REJECT IF NOT ACTIVE
            //*    ESCAPE TOP
            //*  END-IF
            //*  IF #CORE.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE
            //*    GTN-PDA-E.PH-LAST-NAME := #CORE.PH-LAST-NME-16
            //*    GTN-PDA-E.PH-MIDDLE-NAME := #CORE.PH-MDDLE-NME-12
            //*    GTN-PDA-E.PH-FIRST-NAME := #CORE.PH-FIRST-NME-10
            //*    IF #CORE.PH-SOCIAL-SECURITY-NO NE 0
            //*      GTN-PDA-E.ANNT-SOC-SEC-NBR := #CORE.PH-SOCIAL-SECURITY-NO
            //*    END-IF
            //*    ESCAPE BOTTOM
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Audit_Hold() throws Exception                                                                                                                    //Natural: GET-AUDIT-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Hold_Eof.getBoolean()))                                                                                                                         //Natural: IF #HOLD-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Audit_Hold.greater(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee())))                                                                            //Natural: IF #AUDIT-HOLD GT #INPUT.#CNTRCT-PAYEE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Audit_Hold.equals(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee())))                                                                             //Natural: IF #AUDIT-HOLD = #INPUT.#CNTRCT-PAYEE
            {
                gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Grp().setValue(pnd_Hold_Cntrct_Hold_User_Grp);                                                                        //Natural: ASSIGN GTN-PDA-E.CNTRCT-HOLD-GRP := #HOLD.CNTRCT-HOLD-USER-GRP
                gdaIaag700.getGtn_Pda_E_Cntrct_Hold_User_Id().setValue(pnd_Hold_Cntrct_Hold_User_Id);                                                                     //Natural: ASSIGN GTN-PDA-E.CNTRCT-HOLD-USER-ID := #HOLD.CNTRCT-HOLD-USER-ID
                gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().setValue("1");                                                                                                    //Natural: ASSIGN GTN-PDA-E.TEMP-PEND-CDE := '1'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().setValue("0");                                                                                                    //Natural: ASSIGN GTN-PDA-E.TEMP-PEND-CDE := '0'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().read(6, pnd_Hold);                                                                                                                             //Natural: READ WORK 6 ONCE #HOLD
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Hold_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #HOLD-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            pnd_Audit_Hold_Pnd_Cntrct_Ppcn_Nbr.setValue(pnd_Hold_Cntrct_Ppcn_Nbr);                                                                                        //Natural: ASSIGN #AUDIT-HOLD.#CNTRCT-PPCN-NBR := #HOLD.CNTRCT-PPCN-NBR
            pnd_Audit_Hold_Pnd_Cntrct_Pay_Cde.setValue(pnd_Hold_Cntrct_Payee_Cde);                                                                                        //Natural: ASSIGN #AUDIT-HOLD.#CNTRCT-PAY-CDE := #HOLD.CNTRCT-PAYEE-CDE
            pnd_Hold_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #HOLD-CTR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Name_Address() throws Exception                                                                                                                  //Natural: GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        if (condition(pnd_Name_Eof.getBoolean()))                                                                                                                         //Natural: IF #NAME-EOF
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(gdaIaag700.getPnd_Name_Cntrct_Payee().greater(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee())))                                                     //Natural: IF #NAME.CNTRCT-PAYEE GT #INPUT.#CNTRCT-PAYEE
            {
                //*  7/09 - USE PAYEE 01 NAAD
                //*  7/09 - USE PAYEE 01 NAAD
                //*  7/09
                //*  7/09
                if (condition(pnd_Save_Scnd_Ann_Dod.greater(getZero()) && pnd_Save_First_Ann_Dod.equals(getZero()) && gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr().equals(gdaIaag700.getPnd_Name_01_Cntrct_Nmbr())  //Natural: IF #SAVE-SCND-ANN-DOD GT 0 AND #SAVE-FIRST-ANN-DOD = 0 AND #INPUT.#PPCN-NBR = #NAME-01.CNTRCT-NMBR AND #INPUT.#PAYEE-CDE = 02
                    && gdaIaag700.getPnd_Input_Pnd_Payee_Cde().equals(2)))
                {
                    //*  7/09
                    gdaIaag700.getPnd_Name_Save().setValuesByName(gdaIaag700.getPnd_Hld());                                                                               //Natural: MOVE BY NAME #HLD TO #NAME-SAVE
                    //*  7/09
                    //*  7/09
                    //*  7/09
                    //*  7/09
                    gdaIaag700.getPnd_Hld().setValuesByName(gdaIaag700.getPnd_Name_01());                                                                                 //Natural: MOVE BY NAME #NAME-01 TO #HLD
                    pnd_Use_Payee_01.setValue(true);                                                                                                                      //Natural: ASSIGN #USE-PAYEE-01 := TRUE
                    getReports().write(1, "Contract Payee",gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee(),"will use Payee 01 Address");                                       //Natural: WRITE ( 1 ) 'Contract Payee' #INPUT.#CNTRCT-PAYEE 'will use Payee 01 Address'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  7/09
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ALWAYS SAVE 01 NAAD 11/09
                    if (condition(gdaIaag700.getPnd_Name_Cntrct_Payee_Cde().equals(1)))                                                                                   //Natural: IF #NAME.CNTRCT-PAYEE-CDE = 01
                    {
                        //*                      11/09
                        gdaIaag700.getPnd_Name_01().setValuesByName(gdaIaag700.getPnd_Name());                                                                            //Natural: MOVE BY NAME #NAME TO #NAME-01
                        //*                      11/09
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Error_Code_1.getValue(1).setValue("1");                                                                                                           //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '1'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  7/09
                }                                                                                                                                                         //Natural: END-IF
                //*  ALWAYS SAVE 01 NAAD 11/09
                if (condition(gdaIaag700.getPnd_Name_Cntrct_Payee_Cde().equals(1)))                                                                                       //Natural: IF #NAME.CNTRCT-PAYEE-CDE = 01
                {
                    //*                      11/09
                    gdaIaag700.getPnd_Name_01().setValuesByName(gdaIaag700.getPnd_Name());                                                                                //Natural: MOVE BY NAME #NAME TO #NAME-01
                    //*                      11/09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  7/09
            //*  7/09
            if (condition(gdaIaag700.getPnd_Name_Cntrct_Payee().equals(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee()) || (pnd_Use_Payee_01.getBoolean() &&                   //Natural: IF #NAME.CNTRCT-PAYEE = #INPUT.#CNTRCT-PAYEE OR ( #USE-PAYEE-01 AND #INPUT.#PAYEE-CDE = 02 AND #HLD.CNTRCT-NMBR = #INPUT.#PPCN-NBR )
                gdaIaag700.getPnd_Input_Pnd_Payee_Cde().equals(2) && gdaIaag700.getPnd_Hld_Cntrct_Nmbr().equals(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr()))))
            {
                gdaIaag700.getGtn_Pda_E_Pymnt_Nme_And_Addr_Grp().getValue("*").reset();                                                                                   //Natural: RESET GTN-PDA-E.PYMNT-NME-AND-ADDR-GRP ( * )
                pnd_Error_Code_1.getValue(1).setValue(1);                                                                                                                 //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := 1
                //*  082017
                if (condition(gdaIaag700.getPnd_Hld_Ph_Unque_Id_Nmbr().equals(getZero())))                                                                                //Natural: IF #HLD.PH-UNQUE-ID-NMBR = 0
                {
                    gdaIaag700.getGtn_Pda_E_Cntrct_Unq_Id_Nbr().setValue(999999999999L);                                                                                  //Natural: ASSIGN GTN-PDA-E.CNTRCT-UNQ-ID-NBR := 999999999999
                    //*      #ERROR-CODE-1(3) := '3'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaIaag700.getGtn_Pda_E_Cntrct_Unq_Id_Nbr().setValue(gdaIaag700.getPnd_Hld_Ph_Unque_Id_Nmbr());                                                       //Natural: ASSIGN GTN-PDA-E.CNTRCT-UNQ-ID-NBR := #HLD.PH-UNQUE-ID-NMBR
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet3148 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #HLD.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #HLD.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #HLD.PENDING-PERM-ADDRSS-CHNGE-DTE-K = 0
                if (condition(gdaIaag700.getPnd_Hld_Pending_Addrss_Chnge_Dte_K().equals(getZero()) && gdaIaag700.getPnd_Hld_Pending_Addrss_Restore_Dte_K().equals(getZero()) 
                    && gdaIaag700.getPnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K().equals(getZero())))
                {
                    decideConditionsMet3148++;
                                                                                                                                                                          //Natural: PERFORM ASSIGN-NAME-ADDR
                    sub_Assign_Name_Addr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #HLD.PENDING-ADDRSS-CHNGE-DTE-K = 0 AND #HLD.PENDING-ADDRSS-RESTORE-DTE-K = 0 AND #SAVE-CHECK-DTE GE #HLD.PENDING-PERM-ADDRSS-CHNGE-DTE-K
                else if (condition(gdaIaag700.getPnd_Hld_Pending_Addrss_Chnge_Dte_K().equals(getZero()) && gdaIaag700.getPnd_Hld_Pending_Addrss_Restore_Dte_K().equals(getZero()) 
                    && pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte.greaterOrEqual(gdaIaag700.getPnd_Hld_Pending_Perm_Addrss_Chnge_Dte_K())))
                {
                    decideConditionsMet3148++;
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAC-NAME-ADDR
                    sub_Assign_Vac_Name_Addr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN #HLD.PENDING-ADDRSS-CHNGE-DTE-K NE 0 AND #HLD.PENDING-ADDRSS-RESTORE-DTE-K NE 0 AND ( #SAVE-CHECK-DTE GE #HLD.PENDING-ADDRSS-CHNGE-DTE-K AND #SAVE-CHECK-DTE LE #HLD.PENDING-ADDRSS-RESTORE-DTE-K )
                else if (condition(gdaIaag700.getPnd_Hld_Pending_Addrss_Chnge_Dte_K().notEquals(getZero()) && gdaIaag700.getPnd_Hld_Pending_Addrss_Restore_Dte_K().notEquals(getZero()) 
                    && pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte.greaterOrEqual(gdaIaag700.getPnd_Hld_Pending_Addrss_Chnge_Dte_K()) && pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte.lessOrEqual(gdaIaag700.getPnd_Hld_Pending_Addrss_Restore_Dte_K())))
                {
                    decideConditionsMet3148++;
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAC-NAME-ADDR
                    sub_Assign_Vac_Name_Addr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-NAME-ADDR
                    sub_Assign_Name_Addr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  KN - 09/05/96
                //*  ACTIVE
                //*  BLANK N/A
                if (condition((gdaIaag700.getPnd_Input_Pnd_Status_Cde().notEquals(9) && gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(1).equals(" ") &&                    //Natural: IF ( #INPUT.#STATUS-CDE NE 9 AND GTN-PDA-E.PYMNT-NME ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( 1 ) = ' ' AND GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( 1 ) = ' ' )
                    gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(1).equals(" ") && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(1).equals(" ") 
                    && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(1).equals(" ") && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(1).equals(" ") 
                    && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(1).equals(" ") && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(1).equals(" "))))
                {
                    pnd_Error_Code_1.getValue(1).setValue("1");                                                                                                           //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '1'
                    getReports().write(0, "BLANK NAME AND ADDRESS:",gdaIaag700.getPnd_Hld_Cntrct_Payee());                                                                //Natural: WRITE 'BLANK NAME AND ADDRESS:' #HLD.CNTRCT-PAYEE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                gdaIaag700.getGtn_Pda_E_Global_Country().setValue(gdaIaag700.getPnd_Hld_Global_Country());                                                                //Natural: ASSIGN GTN-PDA-E.GLOBAL-COUNTRY := #HLD.GLOBAL-COUNTRY
                //*  7/09
                //*  7/09
                if (condition(pnd_Use_Payee_01.getBoolean() && gdaIaag700.getPnd_Input_Pnd_Payee_Cde().equals(2) && gdaIaag700.getPnd_Hld_Cntrct_Nmbr().equals(gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr()))) //Natural: IF #USE-PAYEE-01 AND #INPUT.#PAYEE-CDE = 02 AND #HLD.CNTRCT-NMBR = #INPUT.#PPCN-NBR
                {
                    //*  7/09
                    gdaIaag700.getPnd_Hld().setValuesByName(gdaIaag700.getPnd_Name_Save());                                                                               //Natural: MOVE BY NAME #NAME-SAVE TO #HLD
                    //*  7/09
                    if (condition(gdaIaag700.getPnd_Hld_Cntrct_Payee_Cde().equals(1)))                                                                                    //Natural: IF #HLD.CNTRCT-PAYEE-CDE = 01
                    {
                        //*  7/09
                        gdaIaag700.getPnd_Name_01().setValuesByName(gdaIaag700.getPnd_Hld());                                                                             //Natural: MOVE BY NAME #HLD TO #NAME-01
                    }                                                                                                                                                     //Natural: END-IF
                    //*  7/09
                    pnd_Use_Payee_01.reset();                                                                                                                             //Natural: RESET #USE-PAYEE-01
                    //*  7/09
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  7/09
            getWorkFiles().read(3, gdaIaag700.getPnd_Hld());                                                                                                              //Natural: READ WORK 3 ONCE #HLD
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Name_Eof.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #NAME-EOF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  7/09
            gdaIaag700.getPnd_Name().setValuesByName(gdaIaag700.getPnd_Hld());                                                                                            //Natural: MOVE BY NAME #HLD TO #NAME
            //*  ALWAYS SAVE 01 NAAD    7/09
            //*  11/09
            if (condition(gdaIaag700.getPnd_Hld_Cntrct_Payee_Cde().equals(1) && gdaIaag700.getPnd_Hld_Cntrct_Payee().lessOrEqual(gdaIaag700.getPnd_Input_Pnd_Cntrct_Payee()))) //Natural: IF #HLD.CNTRCT-PAYEE-CDE = 01 AND #HLD.CNTRCT-PAYEE LE #INPUT.#CNTRCT-PAYEE
            {
                //*  7/09
                gdaIaag700.getPnd_Name_01().setValuesByName(gdaIaag700.getPnd_Hld());                                                                                     //Natural: MOVE BY NAME #HLD TO #NAME-01
                //*  7/09
            }                                                                                                                                                             //Natural: END-IF
            pnd_Name_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NAME-CTR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    private void sub_Assign_Name_Addr() throws Exception                                                                                                                  //Natural: ASSIGN-NAME-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Indx1.setValue(1);                                                                                                                                            //Natural: ASSIGN #INDX1 := 1
        pnd_Error_Code_1.getValue(1).setValue("0");                                                                                                                       //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '0'
        gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Cntrct_Name_Free_K());                                                     //Natural: ASSIGN GTN-PDA-E.PYMNT-NME ( #INDX1 ) := #HLD.CNTRCT-NAME-FREE-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_1_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-1-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_2_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-2-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_3_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-3-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_4_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-4-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_5_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-5-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_6_K());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-6-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Zip_Cde().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_K().getSubstring(1,9));                        //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-ZIP-CDE ( #INDX1 ) := SUBSTR ( #HLD.ADDRSS-POSTAL-DATA-K,1,9 )
        gdaIaag700.getGtn_Pda_E_Pymnt_Postl_Data().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-POSTL-DATA ( #INDX1 ) := #HLD.ADDRSS-POSTAL-DATA-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Type_Cde_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( #INDX1 ) := #HLD.ADDRSS-TYPE-CDE-K
        //*  7/09
        //*  7/09
        if (condition(gdaIaag700.getPnd_Hld_Addrss_Type_Cde_K().equals("U")))                                                                                             //Natural: IF #HLD.ADDRSS-TYPE-CDE-K = 'U'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("N");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'N'
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("Y");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Dte_K(),"9999999"))))                                                                //Natural: IF #HLD.ADDRSS-LAST-CHNGE-DTE-K NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := 00000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Dte_K());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := #HLD.ADDRSS-LAST-CHNGE-DTE-K
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Time_K(),"9999999"))))                                                               //Natural: IF #HLD.ADDRSS-LAST-CHNGE-TIME-K NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := 0000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Time_K());                             //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := #HLD.ADDRSS-LAST-CHNGE-TIME-K
            //*  7/09
            //*  7/09
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Chg_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Permanent_Addrss_Ind_K());                                        //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-CHG-IND ( #INDX1 ) := #HLD.PERMANENT-ADDRSS-IND-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Acct_Nbr().setValue(gdaIaag700.getPnd_Hld_Ph_Bank_Pymnt_Acct_Nmbr_K());                                                         //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-ACCT-NBR := #HLD.PH-BANK-PYMNT-ACCT-NMBR-K
        //*  7/09
        //*  9/13
        if (condition(gdaIaag700.getPnd_Hld_Bank_Aba_Acct_Nmbr_K().equals(" ") || ! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_Bank_Aba_Acct_Nmbr_K(),                    //Natural: IF #HLD.BANK-ABA-ACCT-NMBR-K = ' ' OR #HLD.BANK-ABA-ACCT-NMBR-K NE MASK ( 999999999 )
            "999999999"))))
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().setValue(0);                                                                                                   //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-TRANSIT-ID := 000000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().setValue(gdaIaag700.getPnd_Hld_Bank_Aba_Acct_Nmbr_N_K());                                                      //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-TRANSIT-ID := #HLD.BANK-ABA-ACCT-NMBR-N-K
            //*  7/09
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Chk_Sav_Ind().setValue(gdaIaag700.getPnd_Hld_Checking_Saving_Cde_K());                                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-CHK-SAV-IND := #HLD.CHECKING-SAVING-CDE-K
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT-INDICATOR
        sub_Get_Payment_Indicator();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(2);                                                                                                                                            //Natural: ASSIGN #INDX1 := 2
        pnd_Error_Code_1.getValue(1).setValue("0");                                                                                                                       //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '0'
        gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Cntrct_Name_Free_R());                                                     //Natural: ASSIGN GTN-PDA-E.PYMNT-NME ( #INDX1 ) := #HLD.CNTRCT-NAME-FREE-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_1_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-1-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_2_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-2-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_3_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-3-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_4_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-4-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_5_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-5-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Lne_6_R());                                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( #INDX1 ) := #HLD.ADDRSS-LNE-6-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Zip_Cde().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_R().getSubstring(1,9));                        //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-ZIP-CDE ( #INDX1 ) := SUBSTR ( #HLD.ADDRSS-POSTAL-DATA-R,1,9 )
        gdaIaag700.getGtn_Pda_E_Pymnt_Postl_Data().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Postal_Data_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-POSTL-DATA ( #INDX1 ) := #HLD.ADDRSS-POSTAL-DATA-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Type_Cde_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( #INDX1 ) := #HLD.ADDRSS-TYPE-CDE-R
        //*  7/09
        if (condition(gdaIaag700.getPnd_Hld_Addrss_Type_Cde_R().equals("U")))                                                                                             //Natural: IF #HLD.ADDRSS-TYPE-CDE-R = 'U'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("N");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("Y");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Dte_R(),"9999999"))))                                                                //Natural: IF #HLD.ADDRSS-LAST-CHNGE-DTE-R NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := 00000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Dte_R());                              //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := #HLD.ADDRSS-LAST-CHNGE-DTE-R
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Time_R(),"9999999"))))                                                               //Natural: IF #HLD.ADDRSS-LAST-CHNGE-TIME-R NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := 0000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_Addrss_Last_Chnge_Time_R());                             //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := #HLD.ADDRSS-LAST-CHNGE-TIME-R
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    //*  7/09
    private void sub_Assign_Vac_Name_Addr() throws Exception                                                                                                              //Natural: ASSIGN-VAC-NAME-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Indx1.setValue(1);                                                                                                                                            //Natural: ASSIGN #INDX1 := 1
        pnd_Error_Code_1.getValue(1).setValue("0");                                                                                                                       //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '0'
        gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Cntrct_Name_Free_K());                                                   //Natural: ASSIGN GTN-PDA-E.PYMNT-NME ( #INDX1 ) := #HLD.V-CNTRCT-NAME-FREE-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_1_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-1-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_2_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-2-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_3_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-3-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_4_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-4-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_5_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-5-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_6_K());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-6-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Zip_Cde().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Postal_Data_K().getSubstring(1,9));                      //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-ZIP-CDE ( #INDX1 ) := SUBSTR ( #HLD.V-ADDRSS-POSTAL-DATA-K,1,9 )
        gdaIaag700.getGtn_Pda_E_Pymnt_Postl_Data().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Postal_Data_K());                                          //Natural: ASSIGN GTN-PDA-E.PYMNT-POSTL-DATA ( #INDX1 ) := #HLD.V-ADDRSS-POSTAL-DATA-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Type_Cde_K());                                          //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( #INDX1 ) := #HLD.V-ADDRSS-TYPE-CDE-K
        //*  7/09
        if (condition(gdaIaag700.getPnd_Hld_V_Addrss_Type_Cde_K().equals("U")))                                                                                           //Natural: IF #HLD.V-ADDRSS-TYPE-CDE-K = 'U'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("N");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("Y");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Dte_K(),"9999999"))))                                                              //Natural: IF #HLD.V-ADDRSS-LAST-CHNGE-DTE-K NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := 00000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Dte_K());                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := #HLD.V-ADDRSS-LAST-CHNGE-DTE-K
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Time_K(),"9999999"))))                                                             //Natural: IF #HLD.V-ADDRSS-LAST-CHNGE-TIME-K NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := 0000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Time_K());                           //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := #HLD.V-ADDRSS-LAST-CHNGE-TIME-K
            //*  7/09
            //*  7/09
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Chg_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Permanent_Addrss_Ind_K());                                      //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-CHG-IND ( #INDX1 ) := #HLD.V-PERMANENT-ADDRSS-IND-K
        gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Acct_Nbr().setValue(gdaIaag700.getPnd_Hld_V_Ph_Bank_Pymnt_Acct_Nmbr_K());                                                       //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-ACCT-NBR := #HLD.V-PH-BANK-PYMNT-ACCT-NMBR-K
        //*  7/09
        //*  9/13
        if (condition(gdaIaag700.getPnd_Hld_V_Bank_Aba_Acct_Nmbr_K().equals(" ") || ! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_V_Bank_Aba_Acct_Nmbr_K(),                //Natural: IF #HLD.V-BANK-ABA-ACCT-NMBR-K = ' ' OR #HLD.V-BANK-ABA-ACCT-NMBR-K NE MASK ( 999999999 )
            "999999999"))))
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().setValue(0);                                                                                                   //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-TRANSIT-ID := 000000000
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().setValue(gdaIaag700.getPnd_Hld_V_Bank_Aba_Acct_Nmbr_N_K());                                                    //Natural: ASSIGN GTN-PDA-E.PYMNT-EFT-TRANSIT-ID := #HLD.V-BANK-ABA-ACCT-NMBR-N-K
            //*  7/09
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Chk_Sav_Ind().setValue(gdaIaag700.getPnd_Hld_V_Checking_Saving_Cde_K());                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-CHK-SAV-IND := #HLD.V-CHECKING-SAVING-CDE-K
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
        //*  7/09
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT-INDICATOR
        sub_Get_Payment_Indicator();
        if (condition(Global.isEscape())) {return;}
        pnd_Indx1.setValue(2);                                                                                                                                            //Natural: ASSIGN #INDX1 := 2
        pnd_Error_Code_1.getValue(1).setValue("0");                                                                                                                       //Natural: ASSIGN #ERROR-CODE-1 ( 1 ) := '0'
        gdaIaag700.getGtn_Pda_E_Pymnt_Nme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Cntrct_Name_Free_R());                                                   //Natural: ASSIGN GTN-PDA-E.PYMNT-NME ( #INDX1 ) := #HLD.V-CNTRCT-NAME-FREE-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line1_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_1_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE1-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-1-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line2_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_2_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE2-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-2-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line3_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_3_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE3-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-3-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line4_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_4_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE4-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-4-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line5_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_5_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE5-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-5-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Line6_Txt().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Lne_6_R());                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LINE6-TXT ( #INDX1 ) := #HLD.V-ADDRSS-LNE-6-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Zip_Cde().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Postal_Data_R().getSubstring(1,9));                      //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-ZIP-CDE ( #INDX1 ) := SUBSTR ( #HLD.V-ADDRSS-POSTAL-DATA-R,1,9 )
        gdaIaag700.getGtn_Pda_E_Pymnt_Postl_Data().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Postal_Data_R());                                          //Natural: ASSIGN GTN-PDA-E.PYMNT-POSTL-DATA ( #INDX1 ) := #HLD.V-ADDRSS-POSTAL-DATA-R
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Type_Cde_R());                                          //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( #INDX1 ) := #HLD.V-ADDRSS-TYPE-CDE-R
        //*  7/09
        if (condition(gdaIaag700.getPnd_Hld_V_Addrss_Type_Cde_R().equals("U")))                                                                                           //Natural: IF #HLD.V-ADDRSS-TYPE-CDE-R = 'U'
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("N");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Foreign_Cde().getValue(pnd_Indx1).setValue("Y");                                                                                //Natural: ASSIGN GTN-PDA-E.PYMNT-FOREIGN-CDE ( #INDX1 ) := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Dte_R(),"9999999"))))                                                              //Natural: IF #HLD.V-ADDRSS-LAST-CHNGE-DTE-R NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := 00000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Dte_R());                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-DTE ( #INDX1 ) := #HLD.V-ADDRSS-LAST-CHNGE-DTE-R
        }                                                                                                                                                                 //Natural: END-IF
        //*  7/09
        if (condition(! (DbsUtil.maskMatches(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Time_R(),"9999999"))))                                                             //Natural: IF #HLD.V-ADDRSS-LAST-CHNGE-TIME-R NE MASK ( 9999999 )
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(0);                                                                            //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := 0000000
            //*  7/09
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Addrss_Last_Chnge_Time_R());                           //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-LAST-CHG-TME ( #INDX1 ) := #HLD.V-ADDRSS-LAST-CHNGE-TIME-R
            //*  7/09
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Chg_Ind().getValue(pnd_Indx1).setValue(gdaIaag700.getPnd_Hld_V_Permanent_Addrss_Ind_R());                                      //Natural: ASSIGN GTN-PDA-E.PYMNT-ADDR-CHG-IND ( #INDX1 ) := #HLD.V-PERMANENT-ADDRSS-IND-R
    }
    private void sub_Get_Payment_Indicator() throws Exception                                                                                                             //Natural: GET-PAYMENT-INDICATOR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        //*  ADDED 5/03
        //*  ROLLOVER
        //*  EFT
        //*  7/09
        //*  GLOBAL
        //*  7/09
        //*  GLOBAL
        //*  7/09
        //*  GLOBAL
        //*  CHECK
        short decideConditionsMet3426 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = '57BT' OR = '57BC'
        if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("IRAC") || 
            gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("03BC") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLT") 
            || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("QPLC") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BT") || gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde().equals("57BC")))
        {
            decideConditionsMet3426++;
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(8);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 8
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-E.PYMNT-EFT-TRANSIT-ID NE 0
        else if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Eft_Transit_Id().notEquals(getZero())))
        {
            decideConditionsMet3426++;
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(2);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 2
        }                                                                                                                                                                 //Natural: WHEN #HLD.GLOBAL-INDICATOR = '3'
        else if (condition(gdaIaag700.getPnd_Hld_Global_Indicator().equals("3")))
        {
            decideConditionsMet3426++;
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(3);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 3
        }                                                                                                                                                                 //Natural: WHEN #HLD.GLOBAL-INDICATOR = '4'
        else if (condition(gdaIaag700.getPnd_Hld_Global_Indicator().equals("4")))
        {
            decideConditionsMet3426++;
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(4);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 4
        }                                                                                                                                                                 //Natural: WHEN #HLD.GLOBAL-INDICATOR = '9'
        else if (condition(gdaIaag700.getPnd_Hld_Global_Indicator().equals("9")))
        {
            decideConditionsMet3426++;
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(9);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 9
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                                 //Natural: ASSIGN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND := 1
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Hold_Code() throws Exception                                                                                                               //Natural: DETERMINE-HOLD-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        pnd_Cntrct_Hold_Cde.reset();                                                                                                                                      //Natural: RESET #CNTRCT-HOLD-CDE
        short decideConditionsMet3447 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN GTN-PDA-E.CNTRCT-HOLD-IND NE ' ' AND GTN-PDA-E.CNTRCT-HOLD-IND NE '0'
        if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind().notEquals(" ") && gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind().notEquals("0")))
        {
            decideConditionsMet3447++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.setValue("A");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-1 := 'A'
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-E.TEMP-PEND-CDE NE ' ' AND GTN-PDA-E.TEMP-PEND-CDE NE '0'
        else if (condition(gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().notEquals(" ") && gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().notEquals("0")))
        {
            decideConditionsMet3447++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.setValue("A");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-1 := 'A'
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND = 1 AND GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( 1 ) = 'F'
        else if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(1) && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(1).equals("F")))
        {
            decideConditionsMet3447++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.setValue("B");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-1 := 'B'
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND = 1 AND GTN-PDA-E.PYMNT-ADDR-TYPE-IND ( 1 ) = 'C'
        else if (condition(gdaIaag700.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().equals(1) && gdaIaag700.getGtn_Pda_E_Pymnt_Addr_Type_Ind().getValue(1).equals("C")))
        {
            decideConditionsMet3447++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.setValue("C");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-1 := 'C'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.setValue("0");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-1 := '0'
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet3460 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN GTN-PDA-E.CNTRCT-HOLD-IND NE ' ' AND GTN-PDA-E.CNTRCT-HOLD-IND NE '0'
        if (condition(gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind().notEquals(" ") && gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind().notEquals("0")))
        {
            decideConditionsMet3460++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2.setValue(gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Ind());                                                                //Natural: ASSIGN #CNTRCT-HOLD-CDE-2 := GTN-PDA-E.CNTRCT-HOLD-IND
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-E.TEMP-PEND-CDE NE ' ' AND GTN-PDA-E.TEMP-PEND-CDE NE '0'
        else if (condition(gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().notEquals(" ") && gdaIaag700.getGtn_Pda_E_Temp_Pend_Cde().notEquals("0")))
        {
            decideConditionsMet3460++;
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2.setValue("M");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-2 := 'M'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2.setValue("0");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-2 := '0'
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_1.equals("A") && pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_2.equals("M")))                                    //Natural: IF #CNTRCT-HOLD-CDE-1 = 'A' AND #CNTRCT-HOLD-CDE-2 = 'M'
        {
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_3.setValue(gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Grp().getSubstring(1,1));                                              //Natural: ASSIGN #CNTRCT-HOLD-CDE-3 := SUBSTR ( GTN-PDA-E.CNTRCT-HOLD-GRP,1,1 )
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_4.setValue(gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Grp().getSubstring(2,1));                                              //Natural: ASSIGN #CNTRCT-HOLD-CDE-4 := SUBSTR ( GTN-PDA-E.CNTRCT-HOLD-GRP,2,1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_3.setValue(" ");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-3 := ' '
            pnd_Cntrct_Hold_Cde_Pnd_Cntrct_Hold_Cde_4.setValue(" ");                                                                                                      //Natural: ASSIGN #CNTRCT-HOLD-CDE-4 := ' '
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Cntrct_Hold_Cde().setValue(pnd_Cntrct_Hold_Cde);                                                                                          //Natural: ASSIGN GTN-PDA-E.CNTRCT-HOLD-CDE := #CNTRCT-HOLD-CDE
    }
    private void sub_Tax_Exempt() throws Exception                                                                                                                        //Natural: TAX-EXEMPT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Issue_Dte.setValue(pnd_Save_Issue_Dte);                                                                                                                    //Natural: ASSIGN #WS-ISSUE-DTE := #SAVE-ISSUE-DTE
        pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Ccyy.nadd(9);                                                                                                                   //Natural: ADD 9 TO #WS-ISSUE-DTE-CCYY
        //*  YEARLY
        if (condition(gdaIaag700.getPnd_Input_Pnd_Mode_Ind().equals(100)))                                                                                                //Natural: IF #MODE-IND = 100
        {
            pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm.nadd(11);                                                                                                                //Natural: ADD 11 TO #WS-ISSUE-DTE-MM
        }                                                                                                                                                                 //Natural: END-IF
        //*  QUARTERLY
        if (condition(gdaIaag700.getPnd_Input_Pnd_Mode_Ind().greater(600) && gdaIaag700.getPnd_Input_Pnd_Mode_Ind().less(604)))                                           //Natural: IF #MODE-IND GT 600 AND #MODE-IND LT 604
        {
            pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm.nadd(9);                                                                                                                 //Natural: ADD 9 TO #WS-ISSUE-DTE-MM
        }                                                                                                                                                                 //Natural: END-IF
        //*  2 TIMES A YEAR
        if (condition(gdaIaag700.getPnd_Input_Pnd_Mode_Ind().greater(700) && gdaIaag700.getPnd_Input_Pnd_Mode_Ind().less(707)))                                           //Natural: IF #MODE-IND GT 700 AND #MODE-IND LT 707
        {
            pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm.nadd(6);                                                                                                                 //Natural: ADD 6 TO #WS-ISSUE-DTE-MM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm.greater(12)))                                                                                                  //Natural: IF #WS-ISSUE-DTE-MM GT 12
        {
            pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Mm.nsubtract(12);                                                                                                           //Natural: SUBTRACT 12 FROM #WS-ISSUE-DTE-MM
            pnd_Ws_Issue_Dte_Pnd_Ws_Issue_Dte_Ccyy.nadd(1);                                                                                                               //Natural: ADD 1 TO #WS-ISSUE-DTE-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK TO SEE IF GREATER
        //*  THAN 10 YEARS
        if (condition(pnd_Ws_Issue_Dte.greater(gdaIaag700.getPnd_Input_Pnd_Cntrct_Fin_Per_Pay_Dte())))                                                                    //Natural: IF #WS-ISSUE-DTE GT #CNTRCT-FIN-PER-PAY-DTE
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Ac_Lt_10yrs().setValue(true);                                                                                                  //Natural: ASSIGN GTN-PDA-E.CNTRCT-AC-LT-10YRS := TRUE
            //*  IF #CNTRCT-EMP-TRMNT-CDE = 'B' OR                     /* 10/10 START
            //*    #SAVE-CNTRCT-ORGN-CDE = '03' OR = '46' OR  /* 11/06
            //*    #SAVE-CNTRCT-ORGN-CDE = '17' OR
            //*    #SAVE-CNTRCT-ORGN-CDE = '18' OR
            //*    #CASH-CDE = 'M' OR
            //*    #CASH-CDE = 'E'
            //*    GTN-PDA-E.PYMNT-TAX-EXEMPT-IND := ' '
            //*  ELSE
            //*    IF #CURR-DIST-CDE = 'CASH' OR
            //*      #CURR-DIST-CDE = ' '
            //*      GTN-PDA-E.PYMNT-TAX-EXEMPT-IND := 'Y'
            //*    ELSE
            //*      GTN-PDA-E.PYMNT-TAX-EXEMPT-IND := 'N'
            //*    END-IF
            //*  END-IF                                                /* 10/10 END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaIaag700.getGtn_Pda_E_Cntrct_Ac_Lt_10yrs().setValue(false);                                                                                                 //Natural: ASSIGN GTN-PDA-E.CNTRCT-AC-LT-10YRS := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************************
        getReports().write(0, Global.getPROGRAM(),"STARTED AT: ",Global.getTIMN());                                                                                       //Natural: WRITE *PROGRAM 'STARTED AT: ' *TIMN
        if (Global.isEscape()) return;
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        getWorkFiles().read(9, pnd_Input_Record_Work_9);                                                                                                                  //Natural: READ WORK 9 ONCE #INPUT-RECORD-WORK-9
        if (condition(pnd_Input_Record_Work_9_Pnd_Call_Type.equals("L") || pnd_Input_Record_Work_9_Pnd_Call_Type.equals("P")))                                            //Natural: IF #CALL-TYPE = 'L' OR = 'P'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "*********************************************");                                                                                       //Natural: WRITE '*********************************************'
            if (Global.isEscape()) return;
            getReports().write(0, " ");                                                                                                                                   //Natural: WRITE ' '
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Record_Work_9_Pnd_Call_Type);                                                                                             //Natural: WRITE '=' #CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "ERROR IN CALL TYPE PARAMETER - MUST BE L OR P");                                                                                       //Natural: WRITE 'ERROR IN CALL TYPE PARAMETER - MUST BE L OR P'
            if (Global.isEscape()) return;
            getReports().write(0, " ");                                                                                                                                   //Natural: WRITE ' '
            if (Global.isEscape()) return;
            getReports().write(0, "*********************************************");                                                                                       //Natural: WRITE '*********************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Fund_Company() throws Exception                                                                                                                  //Natural: GET-FUND-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 80
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(80)); pnd_I.nadd(1))
        {
            if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).equals(gdaIaag700.getPnd_Input_Pnd_Summ_Fund_Cde())))                                //Natural: IF IAAA051Z.#IA-STD-NM-CD ( #I ) = #SUMM-FUND-CDE
            {
                //*  ADDED 6/02 TO FORCE T FOR PA CONTRACTS WITH VAR FNDS
                //*  ADDED 6/02 FOR PA CONTRACTS
                //*  TO CAPTURE IVC AMTS FOR THESE
                if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Cmpny_Cd().getValue(pnd_I).equals(1) || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("U")         //Natural: IF IAAA051Z.#IA-STD-CMPNY-CD ( #I ) = 1 OR #SUMM-CMPNY-CDE = 'U' OR = 'W'
                    || gdaIaag700.getPnd_Input_Pnd_Summ_Cmpny_Cde().equals("W")))
                {
                    pnd_Tiaafslash_Cref.setValue("T");                                                                                                                    //Natural: ASSIGN #TIAA/CREF := 'T'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Cmpny_Cd().getValue(pnd_I).equals(2)))                                                               //Natural: IF IAAA051Z.#IA-STD-CMPNY-CD ( #I ) = 2
                    {
                        pnd_Tiaafslash_Cref.setValue("C");                                                                                                                //Natural: ASSIGN #TIAA/CREF := 'C'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Ivcroll_For_O1iv_Check() throws Exception                                                                                                      //Natural: CHECK-IVCROLL-FOR-O1IV-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        if (condition((((((DbsUtil.maskMatches(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde(),"'IRA'") || DbsUtil.maskMatches(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde(),"'QPL'"))  //Natural: IF ( GTN-PDA-E.CNTRCT-ROLL-DEST-CDE = MASK ( 'IRA' ) OR = MASK ( 'QPL' ) OR = MASK ( '03B' ) OR = MASK ( '57B' ) ) AND #SAVE-RLLVR-IVC-IND = 'Y' AND #SAVE-RLLVR-ELGBLE-IND = 'Y'
            || DbsUtil.maskMatches(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde(),"'03B'")) || DbsUtil.maskMatches(gdaIaag700.getGtn_Pda_E_Cntrct_Roll_Dest_Cde(),"'57B'")) 
            && pnd_Save_Rllvr_Ivc_Ind.equals("Y")) && pnd_Save_Rllvr_Elgble_Ind.equals("Y"))))
        {
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(pnd_Save_Cntrct_Ivc_Amt_Ik);                                                                  //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := #SAVE-CNTRCT-IVC-AMT-IK
            pnd_Save_Cntrct_Ivc_Amt_Ik.setValue(0);                                                                                                                       //Natural: ASSIGN #SAVE-CNTRCT-IVC-AMT-IK := 0
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(0);                                                                                               //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := 0
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(1).nsubtract(pnd_Save_Cntrct_Ivc_Amt_Ik);                                                                  //Natural: SUBTRACT #SAVE-CNTRCT-IVC-AMT-IK FROM GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( 1 )
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(1).compute(new ComputeParameters(false, gdaIaag700.getGtn_Pda_E_Inv_Acct_Settl_Amt().getValue(1)),          //Natural: ASSIGN GTN-PDA-E.INV-ACCT-SETTL-AMT ( 1 ) := GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( 1 ) + GTN-PDA-E.INV-ACCT-DVDND-AMT ( 1 )
            gdaIaag700.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(1).add(gdaIaag700.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(1)));
        gdaIaag700.getGtn_Pda_E_Inv_Acct_Ivc_Amt().getValue(1).setValue(0);                                                                                               //Natural: ASSIGN GTN-PDA-E.INV-ACCT-IVC-AMT ( 1 ) := 0
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"===========================",NEWLINE);                                                                                  //Natural: WRITE NOHDR '===========================' /
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",gdaIaag700.getPnd_Input_Pnd_Ppcn_Nbr(),gdaIaag700.getPnd_Input_Pnd_Payee_Cde(), //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #PPCN-NBR #PAYEE-CDE '=' #SAVE-CNTRCT-PAYEE
            "=",pnd_Save_Cntrct_Payee);
        getReports().write(0, "=",pnd_Core_Ctr,"=",pnd_Name_Ctr,"=",pnd_Cps_Ctr);                                                                                         //Natural: WRITE '=' #CORE-CTR '=' #NAME-CTR '=' #CPS-CTR
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"===========================",NEWLINE);                                                                                  //Natural: WRITE NOHDR '===========================' /
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=0");
        Global.format(2, "LS=133 PS=56");

        getReports().setDisplayColumns(1, gdaIaag700.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),";",
        		gdaIaag700.getGtn_Pda_E_Cntrct_Payee_Cde(),";",
        		gdaIaag700.getGtn_Pda_E_Annt_Soc_Sec_Nbr());
    }
}
