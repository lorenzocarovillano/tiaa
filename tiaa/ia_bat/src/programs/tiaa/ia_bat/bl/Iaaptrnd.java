/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:35 PM
**        * FROM NATURAL PROGRAM : Iaaptrnd
************************************************************
**        * FILE NAME            : Iaaptrnd.java
**        * CLASS NAME           : Iaaptrnd
**        * INSTANCE NAME        : Iaaptrnd
************************************************************
***********************************************************************
* PROGRAM  : IAAPTRND
* SYSTEM   : IAD
* TITLE    :
* CREATED  : 08/03/2007 JUN TINIO
* FUNCTION : CREATE A REPORT OF ALL TRANSCTIONS ON A DAILY BASIS.
*
*
*
* HISTORY
* JUN 2017 J BREMER       PIN EXPANSION - STOW ONLY
* 04/2017  O SOTTO PIN EXPANSION CHANGES MARKED 082017.
***********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaaptrnd extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Record_1;
    private DbsField pnd_Work_Record_1_Pnd_Work_Record_1a;
    private DbsField pnd_Work_Record_1_Pnd_Work_Record_1b;

    private DbsGroup pnd_Work_Record_1__R_Field_1;
    private DbsField pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Payee;
    private DbsField pnd_Work_Record_1_Pnd_W1_Record_Code;
    private DbsField pnd_Work_Record_1_Pnd_W1_Optn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orgn_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Acctng_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Issue_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Crrncy_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Type_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Type;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler;
    private DbsField pnd_Work_Record_1_Pnd_W1_Filler_1;

    private DbsGroup pnd_Work_Record_2;
    private DbsField pnd_Work_Record_2_Pnd_Work_Record_2a;
    private DbsField pnd_Work_Record_2_Pnd_Work_Record_2b;

    private DbsGroup pnd_Work_Record_2__R_Field_2;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Record_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_2_Pnd_W2_Actvty_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cash_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Company_Cd;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rtb_Percent;
    private DbsField pnd_Work_Record_2_Pnd_W2_Mode_Ind;

    private DbsGroup pnd_Work_Record_2__R_Field_3;
    private DbsField pnd_Work_Record_2_Pnd_F1;
    private DbsField pnd_Work_Record_2_Pnd_Mode_Mm;
    private DbsField pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Hold_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Pend_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cmbne_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Srce;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_State_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt;
    private DbsField pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;

    private DataAccessProgramView vw_iaa_Dc_Pmt_Audit;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Id_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Payee_Cde;
    private DbsField iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte;

    private DbsGroup iaa_Dc_Pmt_Audit__R_Field_4;
    private DbsField iaa_Dc_Pmt_Audit_Pnd_Paudit_Pymnt_Dte;
    private DbsField pnd_Cntrct_First_Pymnt_Pd_Dte;

    private DbsGroup pnd_Cntrct_First_Pymnt_Pd_Dte__R_Field_5;
    private DbsField pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Yyyy_Mm;
    private DbsField pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Dd;
    private DbsField pnd_Cntrct_First_Pymnt_Due_Dte;

    private DbsGroup pnd_Cntrct_First_Pymnt_Due_Dte__R_Field_6;
    private DbsField pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Yyyy_Mm;
    private DbsField pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Dd;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField pnd_Prev_Bus_Dte;
    private DbsField pnd_Curr_Bus_Dte;

    private DbsGroup pnd_Curr_Bus_Dte__R_Field_7;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Mm;

    private DbsGroup pnd_Curr_Bus_Dte__R_Field_8;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy_Mm;
    private DbsField pnd_Trans_Dte;

    private DbsGroup pnd_Trans_Dte__R_Field_9;
    private DbsField pnd_Trans_Dte_Pnd_Yyyy;
    private DbsField pnd_Trans_Dte_Pnd_Mm;
    private DbsField pnd_Trans_Dte_Pnd_Dd;
    private DbsField pnd_Trans_Chck_Dte_Key;

    private DbsGroup pnd_Trans_Chck_Dte_Key__R_Field_10;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Chck_Dte_Key_Pnd_Fill;
    private DbsField pnd_B_Dte;

    private DbsGroup pnd_B_Dte__R_Field_11;
    private DbsField pnd_B_Dte_Pnd_B_Dte_Yyyy;
    private DbsField pnd_B_Dte_Pnd_B_Dte_Mm;
    private DbsField pnd_B_Dte_Pnd_B_Dte_Dd;
    private DbsField pnd_P_Dte;

    private DbsGroup pnd_P_Dte__R_Field_12;
    private DbsField pnd_P_Dte_Pnd_P_Dte_Yyyy;
    private DbsField pnd_P_Dte_Pnd_P_Dte_Mm;
    private DbsField pnd_P_Dte_Pnd_P_Dte_Dd;
    private DbsField pnd_E_Dte;

    private DbsGroup pnd_E_Dte__R_Field_13;
    private DbsField pnd_E_Dte_Pnd_E_Dte_Yyyy;
    private DbsField pnd_E_Dte_Pnd_E_Dte_Mm;
    private DbsField pnd_E_Dte_Pnd_E_Dte_Dd;
    private DbsField pnd_Per_Pymnt_Dte;

    private DbsGroup pnd_Per_Pymnt_Dte__R_Field_14;
    private DbsField pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Yyyy;
    private DbsField pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm;
    private DbsField pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Dd;
    private DbsField pnd_Proc_Dtes;
    private DbsField pnd_Trans_Cnt;
    private DbsField pnd_W_Dte;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_D_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Record_1 = localVariables.newGroupInRecord("pnd_Work_Record_1", "#WORK-RECORD-1");
        pnd_Work_Record_1_Pnd_Work_Record_1a = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Record_1a", "#WORK-RECORD-1A", FieldType.STRING, 
            255);
        pnd_Work_Record_1_Pnd_Work_Record_1b = pnd_Work_Record_1.newFieldInGroup("pnd_Work_Record_1_Pnd_Work_Record_1b", "#WORK-RECORD-1B", FieldType.STRING, 
            104);

        pnd_Work_Record_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Record_1__R_Field_1", "REDEFINE", pnd_Work_Record_1);
        pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Ppcn_Nbr", "#W1-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Work_Record_1_Pnd_W1_Payee = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Payee", "#W1-PAYEE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Record_Code = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Record_Code", "#W1-RECORD-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Optn_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Optn_Cde", "#W1-OPTN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Orgn_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orgn_Cde", "#W1-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_1_Pnd_W1_Acctng_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Acctng_Cde", "#W1-ACCTNG-CDE", FieldType.STRING, 
            2);
        pnd_Work_Record_1_Pnd_W1_Issue_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Issue_Dte", "#W1-ISSUE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte", "#W1-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte", "#W1-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_1_Pnd_W1_Crrncy_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Crrncy_Cde", "#W1-CRRNCY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_1_Pnd_W1_Type_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Type_Cde", "#W1-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pymnt_Mthd", "#W1-PYMNT-MTHD", FieldType.STRING, 
            1);
        pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Pnsn_Pln_Cde", "#W1-PNSN-PLN-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Joint_Cnvrt_Rcrcd_Ind", 
            "#W1-JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Orig_Da_Cntrct_Nbr", "#W1-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Rsdncy_At_Issue_Cde", "#W1-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Xref_Ind", "#W1-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dob_Dte", "#W1-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Mrtlty_Yob_Dte", 
            "#W1-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Sex_Cde", "#W1-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Life_Cnt", "#W1-FIRST-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_First_Annt_Dod_Dte", "#W1-FIRST-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Xref_Ind", "#W1-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dob_Dte", "#W1-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Mrtlty_Yob_Dte", 
            "#W1-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Sex_Cde", "#W1-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Dod_Dte", "#W1-SCND-ANNT-DOD-DTE", 
            FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Life_Cnt", "#W1-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Scnd_Annt_Ssn", "#W1-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Payee_Cde", "#W1-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Div_Coll_Cde", "#W1-DIV-COLL-CDE", 
            FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Inst_Iss_Cde", "#W1-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Lst_Trans_Dte", "#W1-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Type = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Type", "#W1-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Rsdncy_At_Iss_Re", 
            "#W1-CNTRCT-RSDNCY-AT-ISS-RE", FieldType.STRING, 3);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte = pnd_Work_Record_1__R_Field_1.newFieldArrayInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fnl_Prm_Dte", 
            "#W1-CNTRCT-FNL-PRM-DTE", FieldType.DATE, new DbsArrayController(1, 5));
        pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Mtch_Ppcn", "#W1-CNTRCT-MTCH-PPCN", 
            FieldType.STRING, 10);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Annty_Strt_Dte", 
            "#W1-CNTRCT-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Issue_Dte_Dd", "#W1-CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd", 
            "#W1-CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd", "#W1-CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_1_Pnd_W1_Filler = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler", "#W1-FILLER", FieldType.STRING, 
            167);
        pnd_Work_Record_1_Pnd_W1_Filler_1 = pnd_Work_Record_1__R_Field_1.newFieldInGroup("pnd_Work_Record_1_Pnd_W1_Filler_1", "#W1-FILLER-1", FieldType.STRING, 
            8);

        pnd_Work_Record_2 = localVariables.newGroupInRecord("pnd_Work_Record_2", "#WORK-RECORD-2");
        pnd_Work_Record_2_Pnd_Work_Record_2a = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_Work_Record_2a", "#WORK-RECORD-2A", FieldType.STRING, 
            255);
        pnd_Work_Record_2_Pnd_Work_Record_2b = pnd_Work_Record_2.newFieldInGroup("pnd_Work_Record_2_Pnd_Work_Record_2b", "#WORK-RECORD-2B", FieldType.STRING, 
            104);

        pnd_Work_Record_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Record_2__R_Field_2", "REDEFINE", pnd_Work_Record_2);
        pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Ppcn_Nbr", "#W2-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Part_Payee_Cde", "#W2-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Record_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Record_Cde", "#W2-RECORD-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Id_Nbr", "#W2-CPR-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Trans_Dte", "#W2-LST-TRANS-DTE", 
            FieldType.TIME);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Ctznshp_Cde", "#W2-PRTCPNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Cde", "#W2-PRTCPNT-RSDNCY-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Rsdncy_Sw", "#W2-PRTCPNT-RSDNCY-SW", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Nbr", "#W2-PRTCPNT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prtcpnt_Tax_Id_Typ", "#W2-PRTCPNT-TAX-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Actvty_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Actvty_Cde", "#W2-ACTVTY-CDE", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Trmnte_Rsn", "#W2-TRMNTE-RSN", FieldType.STRING, 
            2);
        pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rwrttn_Ind", "#W2-RWRTTN-IND", FieldType.NUMERIC, 
            1);
        pnd_Work_Record_2_Pnd_W2_Cash_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cash_Cde", "#W2-CASH-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Emplymnt_Trmnt_Cde", "#W2-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Company_Cd = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Company_Cd", "#W2-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rcvry_Type_Ind", "#W2-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Per_Ivc_Amt", "#W2-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Resdl_Ivc_Amt", "#W2-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Amt = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Amt", "#W2-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Ivc_Used_Amt", "#W2-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Amt = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Amt", "#W2-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Rtb_Percent = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rtb_Percent", "#W2-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5));
        pnd_Work_Record_2_Pnd_W2_Mode_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Mode_Ind", "#W2-MODE-IND", FieldType.NUMERIC, 
            3);

        pnd_Work_Record_2__R_Field_3 = pnd_Work_Record_2__R_Field_2.newGroupInGroup("pnd_Work_Record_2__R_Field_3", "REDEFINE", pnd_Work_Record_2_Pnd_W2_Mode_Ind);
        pnd_Work_Record_2_Pnd_F1 = pnd_Work_Record_2__R_Field_3.newFieldInGroup("pnd_Work_Record_2_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_Mode_Mm = pnd_Work_Record_2__R_Field_3.newFieldInGroup("pnd_Work_Record_2_Pnd_Mode_Mm", "#MODE-MM", FieldType.NUMERIC, 2);
        pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Wthdrwl_Dte", "#W2-WTHDRWL-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Per_Pay_Dte", "#W2-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Final_Pay_Dte", "#W2-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Xref_Ind", "#W2-BNFCRY-XREF-IND", 
            FieldType.STRING, 9);
        pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Bnfcry_Dod_Dte", "#W2-BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Pend_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Cde", "#W2-PEND-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Hold_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Hold_Cde", "#W2-HOLD-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Pend_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Pend_Dte", "#W2-PEND-DTE", FieldType.NUMERIC, 
            6);
        pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Prev_Dist_Cde", "#W2-PREV-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Curr_Dist_Cde", "#W2-CURR-DIST-CDE", 
            FieldType.STRING, 4);
        pnd_Work_Record_2_Pnd_W2_Cmbne_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cmbne_Cde", "#W2-CMBNE-CDE", FieldType.STRING, 
            12);
        pnd_Work_Record_2_Pnd_W2_Spirt_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Cde", "#W2-SPIRT-CDE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Amt = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Amt", "#W2-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Work_Record_2_Pnd_W2_Spirt_Srce = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Srce", "#W2-SPIRT-SRCE", FieldType.STRING, 
            1);
        pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Arr_Dte", "#W2-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Spirt_Prcss_Dte", "#W2-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Fed_Tax_Amt", "#W2-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_2_Pnd_W2_State_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Cde", "#W2-STATE-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_State_Tax_Amt = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_State_Tax_Amt", "#W2-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_2_Pnd_W2_Local_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Cde", "#W2-LOCAL-CDE", FieldType.STRING, 
            3);
        pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Local_Tax_Amt", "#W2-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Lst_Chnge_Dte", "#W2-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Term_Cde", "#W2-CPR-XFR-TERM-CDE", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Lgl_Res_Cde", "#W2-CPR-LGL-RES-CDE", 
            FieldType.STRING, 3);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte", "#W2-CPR-XFR-ISS-DTE", 
            FieldType.DATE);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Cntrct_Nbr", "#W2-RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Ivc_Ind", "#W2-RLLVR-IVC-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Elgble_Ind", "#W2-RLLVR-ELGBLE-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde = pnd_Work_Record_2__R_Field_2.newFieldArrayInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Dstrbtng_Irc_Cde", 
            "#W2-RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Accptng_Irc_Cde", 
            "#W2-RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2);
        pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Rllvr_Pln_Admn_Ind", "#W2-RLLVR-PLN-ADMN-IND", 
            FieldType.STRING, 1);
        pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8 = pnd_Work_Record_2__R_Field_2.newFieldInGroup("pnd_Work_Record_2_Pnd_W2_Cpr_Xfr_Iss_Dte_N8", "#W2-CPR-XFR-ISS-DTE-N8", 
            FieldType.NUMERIC, 8);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        registerRecord(vw_iaa_Trans_Rcrd);

        vw_iaa_Dc_Pmt_Audit = new DataAccessProgramView(new NameInfo("vw_iaa_Dc_Pmt_Audit", "IAA-DC-PMT-AUDIT"), "IAA_DC_PMT_AUDIT", "IA_DEATH_CLAIMS");
        iaa_Dc_Pmt_Audit_Paudit_Id_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Id_Nbr", "PAUDIT-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "PAUDIT_ID_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Tax_Id_Nbr", "PAUDIT-TAX-ID-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PAUDIT_TAX_ID_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Ppcn_Nbr", "PAUDIT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PAUDIT_PPCN_NBR");
        iaa_Dc_Pmt_Audit_Paudit_Payee_Cde = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Payee_Cde", "PAUDIT-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PAUDIT_PAYEE_CDE");
        iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte = vw_iaa_Dc_Pmt_Audit.getRecord().newFieldInGroup("iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte", "PAUDIT-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "PAUDIT_PYMNT_DTE");

        iaa_Dc_Pmt_Audit__R_Field_4 = vw_iaa_Dc_Pmt_Audit.getRecord().newGroupInGroup("iaa_Dc_Pmt_Audit__R_Field_4", "REDEFINE", iaa_Dc_Pmt_Audit_Paudit_Pymnt_Dte);
        iaa_Dc_Pmt_Audit_Pnd_Paudit_Pymnt_Dte = iaa_Dc_Pmt_Audit__R_Field_4.newFieldInGroup("iaa_Dc_Pmt_Audit_Pnd_Paudit_Pymnt_Dte", "#PAUDIT-PYMNT-DTE", 
            FieldType.STRING, 8);
        registerRecord(vw_iaa_Dc_Pmt_Audit);

        pnd_Cntrct_First_Pymnt_Pd_Dte = localVariables.newFieldInRecord("pnd_Cntrct_First_Pymnt_Pd_Dte", "#CNTRCT-FIRST-PYMNT-PD-DTE", FieldType.STRING, 
            8);

        pnd_Cntrct_First_Pymnt_Pd_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_First_Pymnt_Pd_Dte__R_Field_5", "REDEFINE", pnd_Cntrct_First_Pymnt_Pd_Dte);
        pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Yyyy_Mm = pnd_Cntrct_First_Pymnt_Pd_Dte__R_Field_5.newFieldInGroup("pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Yyyy_Mm", 
            "#FP-YYYY-MM", FieldType.NUMERIC, 6);
        pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Dd = pnd_Cntrct_First_Pymnt_Pd_Dte__R_Field_5.newFieldInGroup("pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Dd", 
            "#FP-DD", FieldType.NUMERIC, 2);
        pnd_Cntrct_First_Pymnt_Due_Dte = localVariables.newFieldInRecord("pnd_Cntrct_First_Pymnt_Due_Dte", "#CNTRCT-FIRST-PYMNT-DUE-DTE", FieldType.STRING, 
            8);

        pnd_Cntrct_First_Pymnt_Due_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_First_Pymnt_Due_Dte__R_Field_6", "REDEFINE", pnd_Cntrct_First_Pymnt_Due_Dte);
        pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Yyyy_Mm = pnd_Cntrct_First_Pymnt_Due_Dte__R_Field_6.newFieldInGroup("pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Yyyy_Mm", 
            "#FD-YYYY-MM", FieldType.NUMERIC, 6);
        pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Dd = pnd_Cntrct_First_Pymnt_Due_Dte__R_Field_6.newFieldInGroup("pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Dd", 
            "#FD-DD", FieldType.NUMERIC, 2);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Prev_Bus_Dte = localVariables.newFieldInRecord("pnd_Prev_Bus_Dte", "#PREV-BUS-DTE", FieldType.STRING, 8);
        pnd_Curr_Bus_Dte = localVariables.newFieldInRecord("pnd_Curr_Bus_Dte", "#CURR-BUS-DTE", FieldType.STRING, 8);

        pnd_Curr_Bus_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Curr_Bus_Dte__R_Field_7", "REDEFINE", pnd_Curr_Bus_Dte);
        pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy = pnd_Curr_Bus_Dte__R_Field_7.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy", "#CURR-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Curr_Bus_Dte_Pnd_Curr_Mm = pnd_Curr_Bus_Dte__R_Field_7.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Mm", "#CURR-MM", FieldType.NUMERIC, 2);

        pnd_Curr_Bus_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Bus_Dte__R_Field_8", "REDEFINE", pnd_Curr_Bus_Dte);
        pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy_Mm = pnd_Curr_Bus_Dte__R_Field_8.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy_Mm", "#CURR-YYYY-MM", FieldType.NUMERIC, 
            6);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.STRING, 8);

        pnd_Trans_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Trans_Dte__R_Field_9", "REDEFINE", pnd_Trans_Dte);
        pnd_Trans_Dte_Pnd_Yyyy = pnd_Trans_Dte__R_Field_9.newFieldInGroup("pnd_Trans_Dte_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Trans_Dte_Pnd_Mm = pnd_Trans_Dte__R_Field_9.newFieldInGroup("pnd_Trans_Dte_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Trans_Dte_Pnd_Dd = pnd_Trans_Dte__R_Field_9.newFieldInGroup("pnd_Trans_Dte_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Trans_Chck_Dte_Key = localVariables.newFieldInRecord("pnd_Trans_Chck_Dte_Key", "#TRANS-CHCK-DTE-KEY", FieldType.STRING, 35);

        pnd_Trans_Chck_Dte_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Trans_Chck_Dte_Key__R_Field_10", "REDEFINE", pnd_Trans_Chck_Dte_Key);
        pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte = pnd_Trans_Chck_Dte_Key__R_Field_10.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Trans_Check_Dte", 
            "#TRANS-CHECK-DTE", FieldType.NUMERIC, 8);
        pnd_Trans_Chck_Dte_Key_Pnd_Fill = pnd_Trans_Chck_Dte_Key__R_Field_10.newFieldInGroup("pnd_Trans_Chck_Dte_Key_Pnd_Fill", "#FILL", FieldType.STRING, 
            27);
        pnd_B_Dte = localVariables.newFieldInRecord("pnd_B_Dte", "#B-DTE", FieldType.STRING, 8);

        pnd_B_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_B_Dte__R_Field_11", "REDEFINE", pnd_B_Dte);
        pnd_B_Dte_Pnd_B_Dte_Yyyy = pnd_B_Dte__R_Field_11.newFieldInGroup("pnd_B_Dte_Pnd_B_Dte_Yyyy", "#B-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_B_Dte_Pnd_B_Dte_Mm = pnd_B_Dte__R_Field_11.newFieldInGroup("pnd_B_Dte_Pnd_B_Dte_Mm", "#B-DTE-MM", FieldType.NUMERIC, 2);
        pnd_B_Dte_Pnd_B_Dte_Dd = pnd_B_Dte__R_Field_11.newFieldInGroup("pnd_B_Dte_Pnd_B_Dte_Dd", "#B-DTE-DD", FieldType.NUMERIC, 2);
        pnd_P_Dte = localVariables.newFieldInRecord("pnd_P_Dte", "#P-DTE", FieldType.STRING, 8);

        pnd_P_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_P_Dte__R_Field_12", "REDEFINE", pnd_P_Dte);
        pnd_P_Dte_Pnd_P_Dte_Yyyy = pnd_P_Dte__R_Field_12.newFieldInGroup("pnd_P_Dte_Pnd_P_Dte_Yyyy", "#P-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_P_Dte_Pnd_P_Dte_Mm = pnd_P_Dte__R_Field_12.newFieldInGroup("pnd_P_Dte_Pnd_P_Dte_Mm", "#P-DTE-MM", FieldType.NUMERIC, 2);
        pnd_P_Dte_Pnd_P_Dte_Dd = pnd_P_Dte__R_Field_12.newFieldInGroup("pnd_P_Dte_Pnd_P_Dte_Dd", "#P-DTE-DD", FieldType.NUMERIC, 2);
        pnd_E_Dte = localVariables.newFieldInRecord("pnd_E_Dte", "#E-DTE", FieldType.STRING, 8);

        pnd_E_Dte__R_Field_13 = localVariables.newGroupInRecord("pnd_E_Dte__R_Field_13", "REDEFINE", pnd_E_Dte);
        pnd_E_Dte_Pnd_E_Dte_Yyyy = pnd_E_Dte__R_Field_13.newFieldInGroup("pnd_E_Dte_Pnd_E_Dte_Yyyy", "#E-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_E_Dte_Pnd_E_Dte_Mm = pnd_E_Dte__R_Field_13.newFieldInGroup("pnd_E_Dte_Pnd_E_Dte_Mm", "#E-DTE-MM", FieldType.NUMERIC, 2);
        pnd_E_Dte_Pnd_E_Dte_Dd = pnd_E_Dte__R_Field_13.newFieldInGroup("pnd_E_Dte_Pnd_E_Dte_Dd", "#E-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Per_Pymnt_Dte = localVariables.newFieldInRecord("pnd_Per_Pymnt_Dte", "#PER-PYMNT-DTE", FieldType.STRING, 8);

        pnd_Per_Pymnt_Dte__R_Field_14 = localVariables.newGroupInRecord("pnd_Per_Pymnt_Dte__R_Field_14", "REDEFINE", pnd_Per_Pymnt_Dte);
        pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Yyyy = pnd_Per_Pymnt_Dte__R_Field_14.newFieldInGroup("pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Yyyy", "#PER-PYMNT-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm = pnd_Per_Pymnt_Dte__R_Field_14.newFieldInGroup("pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm", "#PER-PYMNT-MM", FieldType.NUMERIC, 
            2);
        pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Dd = pnd_Per_Pymnt_Dte__R_Field_14.newFieldInGroup("pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Dd", "#PER-PYMNT-DD", FieldType.NUMERIC, 
            2);
        pnd_Proc_Dtes = localVariables.newFieldArrayInRecord("pnd_Proc_Dtes", "#PROC-DTES", FieldType.STRING, 8, new DbsArrayController(1, 31));
        pnd_Trans_Cnt = localVariables.newFieldArrayInRecord("pnd_Trans_Cnt", "#TRANS-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 31));
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.DATE);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 9);
        pnd_D_Cnt = localVariables.newFieldInRecord("pnd_D_Cnt", "#D-CNT", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Dc_Pmt_Audit.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaaptrnd() throws Exception
    {
        super("Iaaptrnd");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 2 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        2
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            if (condition(pnd_Trans_Chck_Dte_Key.equals(" ")))                                                                                                            //Natural: IF #TRANS-CHCK-DTE-KEY = ' '
            {
                pnd_Trans_Chck_Dte_Key.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #TRANS-CHCK-DTE-KEY
                pnd_Curr_Bus_Dte.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #CURR-BUS-DTE
                //*    #CURR-BUS-DTE := '20070806'
                pnd_E_Dte.setValue(pnd_Curr_Bus_Dte);                                                                                                                     //Natural: ASSIGN #E-DTE := #CURR-BUS-DTE
                pnd_Per_Pymnt_Dte.setValue(pnd_Curr_Bus_Dte);                                                                                                             //Natural: ASSIGN #PER-PYMNT-DTE := #CURR-BUS-DTE
                pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Dd.setValue(1);                                                                                                           //Natural: ASSIGN #PER-PYMNT-DD := 01
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prev_Bus_Dte.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                        //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #PREV-BUS-DTE
                //*    #PREV-BUS-DTE := '20070727'
                pnd_B_Dte.setValue(pnd_Prev_Bus_Dte);                                                                                                                     //Natural: ASSIGN #B-DTE := #PREV-BUS-DTE
                pnd_P_Dte.setValue(pnd_Prev_Bus_Dte);                                                                                                                     //Natural: ASSIGN #P-DTE := #PREV-BUS-DTE
                pnd_P_Dte_Pnd_P_Dte_Dd.nadd(1);                                                                                                                           //Natural: ADD 1 TO #P-DTE-DD
                if (condition(DbsUtil.maskMatches(pnd_P_Dte,"YYYYMMDD")))                                                                                                 //Natural: IF #P-DTE = MASK ( YYYYMMDD )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_P_Dte_Pnd_P_Dte_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN #P-DTE-DD := 01
                    pnd_P_Dte_Pnd_P_Dte_Mm.nadd(1);                                                                                                                       //Natural: ADD 1 TO #P-DTE-MM
                    if (condition(pnd_P_Dte_Pnd_P_Dte_Mm.greater(12)))                                                                                                    //Natural: IF #P-DTE-MM GT 12
                    {
                        pnd_P_Dte_Pnd_P_Dte_Mm.setValue(1);                                                                                                               //Natural: ASSIGN #P-DTE-MM := 01
                        pnd_P_Dte_Pnd_P_Dte_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #P-DTE-YYYY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_B_Dte_Pnd_B_Dte_Dd.nadd(1);                                                                                                                               //Natural: ADD 1 TO #B-DTE-DD
            if (condition(DbsUtil.maskMatches(pnd_B_Dte,"YYYYMMDD")))                                                                                                     //Natural: IF #B-DTE = MASK ( YYYYMMDD )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_B_Dte_Pnd_B_Dte_Dd.setValue(1);                                                                                                                       //Natural: ASSIGN #B-DTE-DD := 01
                pnd_B_Dte_Pnd_B_Dte_Mm.nadd(1);                                                                                                                           //Natural: ADD 1 TO #B-DTE-MM
                if (condition(pnd_B_Dte_Pnd_B_Dte_Mm.greater(12)))                                                                                                        //Natural: IF #B-DTE-MM GT 12
                {
                    pnd_B_Dte_Pnd_B_Dte_Mm.setValue(1);                                                                                                                   //Natural: ASSIGN #B-DTE-MM := 01
                    pnd_B_Dte_Pnd_B_Dte_Yyyy.nadd(1);                                                                                                                     //Natural: ADD 1 TO #B-DTE-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_B_Dte);                                                                                           //Natural: MOVE EDITED #B-DTE TO #W-DTE ( EM = YYYYMMDD )
            pnd_Day_Of_Week.setValueEdited(pnd_W_Dte,new ReportEditMask("NNNNNNNNN"));                                                                                    //Natural: MOVE EDITED #W-DTE ( EM = NNNNNNNNN ) TO #DAY-OF-WEEK
            if (condition(pnd_Day_Of_Week.equals("Sunday") && pnd_B_Dte_Pnd_B_Dte_Dd.notEquals(1)))                                                                       //Natural: IF #DAY-OF-WEEK = 'Sunday' AND #B-DTE-DD NE 01
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_B_Dte.greater(pnd_E_Dte)))                                                                                                                  //Natural: IF #B-DTE GT #E-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_D_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #D-CNT
            pnd_Proc_Dtes.getValue(pnd_D_Cnt).setValue(pnd_B_Dte);                                                                                                        //Natural: ASSIGN #PROC-DTES ( #D-CNT ) := #B-DTE
            if (condition(pnd_B_Dte.equals(pnd_E_Dte)))                                                                                                                   //Natural: IF #B-DTE = #E-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  FOR TRANSFER, RESETTLEMENT, AND ADJUSTMENT TRANSACTIONS
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CHCK-DTE-KEY STARTING FROM #TRANS-CHCK-DTE-KEY
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CHCK_DTE_KEY", ">=", pnd_Trans_Chck_Dte_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CHCK_DTE_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            pnd_Trans_Dte.setValueEdited(iaa_Trans_Rcrd_Trans_Dte,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED TRANS-DTE ( EM = YYYYMMDD ) TO #TRANS-DTE
            if (condition(!((pnd_Trans_Dte.equals(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt)) && ((iaa_Trans_Rcrd_Trans_Cde.greaterOrEqual(500) && iaa_Trans_Rcrd_Trans_Cde.lessOrEqual(518))  //Natural: ACCEPT IF #TRANS-DTE = #PROC-DTES ( 1:#D-CNT ) AND ( TRANS-CDE = 500 THRU 518 OR TRANS-CDE = 60 THRU 62 )
                || (iaa_Trans_Rcrd_Trans_Cde.greaterOrEqual(60) && iaa_Trans_Rcrd_Trans_Cde.lessOrEqual(62)))))))
            {
                continue;
            }
            if (condition(iaa_Trans_Rcrd_Trans_Verify_Cde.equals("V") || iaa_Trans_Rcrd_Trans_Actvty_Cde.equals("R")))                                                    //Natural: IF TRANS-VERIFY-CDE = 'V' OR TRANS-ACTVTY-CDE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO #D-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Cnt)); pnd_I.nadd(1))
            {
                if (condition(pnd_Trans_Dte.equals(pnd_Proc_Dtes.getValue(pnd_I))))                                                                                       //Natural: IF #TRANS-DTE = #PROC-DTES ( #I )
                {
                    pnd_Trans_Cnt.getValue(pnd_I).nadd(1);                                                                                                                //Natural: ADD 1 TO #TRANS-CNT ( #I )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  FOR DEATH SETTLEMENTS
        vw_iaa_Dc_Pmt_Audit.startDatabaseRead                                                                                                                             //Natural: READ IAA-DC-PMT-AUDIT BY PAUDIT-PPCN-PYE-KEY STARTING FROM ' '
        (
        "READ03",
        new Wc[] { new Wc("PAUDIT_PPCN_PYE_KEY", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("PAUDIT_PPCN_PYE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Dc_Pmt_Audit.readNextRow("READ03")))
        {
            if (condition(!(iaa_Dc_Pmt_Audit_Pnd_Paudit_Pymnt_Dte.equals(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt)))))                                                      //Natural: ACCEPT IF #PAUDIT-PYMNT-DTE = #PROC-DTES ( 1:#D-CNT )
            {
                continue;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO #D-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Cnt)); pnd_I.nadd(1))
            {
                if (condition(iaa_Dc_Pmt_Audit_Pnd_Paudit_Pymnt_Dte.equals(pnd_Proc_Dtes.getValue(pnd_I))))                                                               //Natural: IF #PAUDIT-PYMNT-DTE = #PROC-DTES ( #I )
                {
                    pnd_Trans_Cnt.getValue(pnd_I).nadd(1);                                                                                                                //Natural: ADD 1 TO #TRANS-CNT ( #I )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  FOR PAYMENTS - PERIODIC AND DAILY
        READWORK04:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RECORD-1
        while (condition(getWorkFiles().read(1, pnd_Work_Record_1)))
        {
            if (condition(!(pnd_Work_Record_1_Pnd_W1_Record_Code.equals(10) || pnd_Work_Record_1_Pnd_W1_Record_Code.equals(20))))                                         //Natural: ACCEPT IF #W1-RECORD-CODE = 10 OR = 20
            {
                continue;
            }
            if (condition(pnd_Work_Record_1_Pnd_W1_Record_Code.equals(10)))                                                                                               //Natural: IF #W1-RECORD-CODE = 10
            {
                pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Yyyy_Mm.setValue(pnd_Work_Record_1_Pnd_W1_First_Pymnt_Pd_Dte);                                                       //Natural: ASSIGN #FP-YYYY-MM := #W1-FIRST-PYMNT-PD-DTE
                pnd_Cntrct_First_Pymnt_Pd_Dte_Pnd_Fp_Dd.setValue(pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Pd_Dte_Dd);                                                           //Natural: ASSIGN #FP-DD := #W1-CNTRCT-FP-PD-DTE-DD
                pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Yyyy_Mm.setValue(pnd_Work_Record_1_Pnd_W1_First_Pymnt_Due_Dte);                                                     //Natural: ASSIGN #FD-YYYY-MM := #W1-FIRST-PYMNT-DUE-DTE
                pnd_Cntrct_First_Pymnt_Due_Dte_Pnd_Fd_Dd.setValue(pnd_Work_Record_1_Pnd_W1_Cntrct_Fp_Due_Dte_Dd);                                                         //Natural: ASSIGN #FD-DD := #W1-CNTRCT-FP-DUE-DTE-DD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Record_2_Pnd_Work_Record_2a.setValue(pnd_Work_Record_1_Pnd_Work_Record_1a);                                                                      //Natural: ASSIGN #WORK-RECORD-2A := #WORK-RECORD-1A
                pnd_Work_Record_2_Pnd_Work_Record_2b.setValue(pnd_Work_Record_1_Pnd_Work_Record_1b);                                                                      //Natural: ASSIGN #WORK-RECORD-2B := #WORK-RECORD-1B
                if (condition(pnd_Work_Record_2_Pnd_W2_Actvty_Cde.equals(9) || ! (pnd_Work_Record_2_Pnd_W2_Pend_Cde.equals("0") || pnd_Work_Record_2_Pnd_W2_Pend_Cde.equals(" ")))) //Natural: IF #W2-ACTVTY-CDE = 9 OR NOT ( #W2-PEND-CDE = '0' OR = ' ' )
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cntrct_First_Pymnt_Pd_Dte.equals(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt))))                                                             //Natural: IF #CNTRCT-FIRST-PYMNT-PD-DTE = #PROC-DTES ( 1:#D-CNT )
                {
                    FOR03:                                                                                                                                                //Natural: FOR #I 1 TO #D-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Cnt)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Cntrct_First_Pymnt_Pd_Dte.equals(pnd_Proc_Dtes.getValue(pnd_I))))                                                               //Natural: IF #CNTRCT-FIRST-PYMNT-PD-DTE = #PROC-DTES ( #I )
                        {
                            pnd_Trans_Cnt.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #I )
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cntrct_First_Pymnt_Due_Dte.equals(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt))))                                                            //Natural: IF #CNTRCT-FIRST-PYMNT-DUE-DTE = #PROC-DTES ( 1:#D-CNT )
                {
                    FOR04:                                                                                                                                                //Natural: FOR #I 1 TO #D-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Cnt)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Cntrct_First_Pymnt_Due_Dte.equals(pnd_Proc_Dtes.getValue(pnd_I))))                                                              //Natural: IF #CNTRCT-FIRST-PYMNT-DUE-DTE = #PROC-DTES ( #I )
                        {
                            pnd_Trans_Cnt.getValue(pnd_I).nadd(1);                                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #I )
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Per_Pymnt_Dte.equals(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt))))                                                                         //Natural: IF #PER-PYMNT-DTE = #PROC-DTES ( 1:#D-CNT )
                {
                    DbsUtil.examine(new ExamineSource(pnd_Proc_Dtes.getValue(1,":",pnd_D_Cnt)), new ExamineSearch(pnd_Per_Pymnt_Dte), new ExamineGivingIndex(pnd_J));     //Natural: EXAMINE #PROC-DTES ( 1:#D-CNT ) FOR #PER-PYMNT-DTE GIVING INDEX #J
                    short decideConditionsMet368 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE #W2-MODE-IND;//Natural: VALUE 100
                    if (condition((pnd_Work_Record_2_Pnd_W2_Mode_Ind.equals(100))))
                    {
                        decideConditionsMet368++;
                        pnd_Trans_Cnt.getValue(pnd_J).nadd(1);                                                                                                            //Natural: ADD 1 TO #TRANS-CNT ( #J )
                    }                                                                                                                                                     //Natural: VALUE 800:812
                    else if (condition(((pnd_Work_Record_2_Pnd_W2_Mode_Ind.greaterOrEqual(800) && pnd_Work_Record_2_Pnd_W2_Mode_Ind.lessOrEqual(812)))))
                    {
                        decideConditionsMet368++;
                        if (condition(pnd_Work_Record_2_Pnd_Mode_Mm.equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm)))                                                          //Natural: IF #MODE-MM = #PER-PYMNT-MM
                        {
                            pnd_Trans_Cnt.getValue(pnd_J).nadd(1);                                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 601:603
                    else if (condition(((pnd_Work_Record_2_Pnd_W2_Mode_Ind.greaterOrEqual(601) && pnd_Work_Record_2_Pnd_W2_Mode_Ind.lessOrEqual(603)))))
                    {
                        decideConditionsMet368++;
                        if (condition(pnd_Work_Record_2_Pnd_Mode_Mm.equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm) || pnd_Work_Record_2_Pnd_Mode_Mm.add(3).equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm)  //Natural: IF #MODE-MM = #PER-PYMNT-MM OR #MODE-MM + 3 = #PER-PYMNT-MM OR #MODE-MM + 6 = #PER-PYMNT-MM OR #MODE-MM + 9 = #PER-PYMNT-MM
                            || pnd_Work_Record_2_Pnd_Mode_Mm.add(6).equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm) || pnd_Work_Record_2_Pnd_Mode_Mm.add(9).equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm)))
                        {
                            pnd_Trans_Cnt.getValue(pnd_J).nadd(1);                                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 701:706
                    else if (condition(((pnd_Work_Record_2_Pnd_W2_Mode_Ind.greaterOrEqual(701) && pnd_Work_Record_2_Pnd_W2_Mode_Ind.lessOrEqual(706)))))
                    {
                        decideConditionsMet368++;
                        if (condition(pnd_Work_Record_2_Pnd_Mode_Mm.equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm) || pnd_Work_Record_2_Pnd_Mode_Mm.add(6).equals(pnd_Per_Pymnt_Dte_Pnd_Per_Pymnt_Mm))) //Natural: IF #MODE-MM = #PER-PYMNT-MM OR #MODE-MM + 6 = #PER-PYMNT-MM
                        {
                            pnd_Trans_Cnt.getValue(pnd_J).nadd(1);                                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK04_Exit:
        if (Global.isEscape()) return;
        //*  PRINT REPORT OF TRANSACTIONS PER DAY.
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 TO #D-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_D_Cnt)); pnd_I.nadd(1))
        {
            pnd_W_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Proc_Dtes.getValue(pnd_I));                                                                       //Natural: MOVE EDITED #PROC-DTES ( #I ) TO #W-DTE ( EM = YYYYMMDD )
            getReports().display(1, new ColumnSpacing(18),"Trans Date",                                                                                                   //Natural: DISPLAY ( 1 ) 18X 'Trans Date' #W-DTE ( EM = MM/DD/YYYY ) 'Trans Count' #TRANS-CNT ( #I ) ( EM = Z,ZZZ,ZZ9 )
            		pnd_W_Dte, new ReportEditMask ("MM/DD/YYYY"),"Trans Count",
            		pnd_Trans_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(5),"Legacy IA Transaction Count Report",new ColumnSpacing(5),Global.getDATX(),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 5X 'Legacy IA Transaction Count Report' 5X *DATX ( EM = MM/DD/YYYY )
                        new ReportEditMask ("MM/DD/YYYY"));
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(8),"For processing date(s):",pnd_P_Dte,"thru",pnd_E_Dte,NEWLINE,NEWLINE);                //Natural: WRITE ( 1 ) 8X 'For processing date(s):' #P-DTE 'thru' #E-DTE //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133");

        getReports().setDisplayColumns(1, new ColumnSpacing(18),"Trans Date",
        		pnd_W_Dte, new ReportEditMask ("MM/DD/YYYY"),"Trans Count",
        		pnd_Trans_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));
    }
}
