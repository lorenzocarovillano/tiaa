/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:32:12 PM
**        * FROM NATURAL PROGRAM : Iaap782
************************************************************
**        * FILE NAME            : Iaap782.java
**        * CLASS NAME           : Iaap782
**        * INSTANCE NAME        : Iaap782
************************************************************
**********************************************************************
* PROGRAM:   IAAP782
* SYSTEM:    IA ADMINISTRATION
* FUNCTION:  ACTUARIAL REPORT
* TITLE:     IA ADMINISTRATION MODE CONTROL FOR FUND ACCOUNTS
*            CONTRACTS ISSUED OR TERMINATED IN THE CURRENT MONTH
* COMMENTS:  THIS PROGRAM READS THE REPORT FILE (WORKFILE2) CREATED
*            BY IAAP780 (REPORT FILE GENERATOR), SORTS THE FILE BY
*            PPCN, PAYEE, AND TRANSACTION DATE (DESCENTDING).
*            IT THEN TESTS SEVERAL TRANSACTION SCENARIOS TO ADD OR
*             SUBTRACT TO THE CONTROL TOTALS TABLE.
*
* WRITTEN BY MIKE OLIVA, 3/12/96 FOR KEITH NICOLL AND JAY MIRANDA
*                                                                    *
* HISTORY                                                            *
*                                                                    *
*  5/19/00                NEW PA SELECT FUNDS                        *
*                         DO SCAN ON 5/00 FOR CHANGES                *
*  3/01 DUANE ROBINSON (DR01) - ALLOW TPA, IPRO, AND P & I           *
* 10/01 ADDED LOGIC FOR TPA,IPRO & P/I REPORTS                       *
*  4/02 CHANGED IAAN0510 TO IAAN051Z FOR NEW OIA SYSTEM              *
*  3/12 RATE BASE EXPANSION                                          *
*  8/17 OS ADJUST REPORT FILE LENGTH TO BE THE SAME AS IN IAAP780.   *
*  9/17 JT ADDED PREFIX IG TO LIST OF VALID PREFIX.                  *
**********************************************************************
* DEFINE DATA LOCAL USING IAAA0500
* DEFINE DATA LOCAL USING IAAA0510 /* ADDED 5/00 NEW PA SELECT
* CHANGED 04/2002

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap782 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Report_File;

    private DbsGroup pnd_Report_File__R_Field_1;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Date;

    private DbsGroup pnd_Report_File__R_Field_2;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Yyyy;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Mm;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Dd;
    private DbsField pnd_Report_File_Pnd_Rep_Ppcn;
    private DbsField pnd_Report_File_Pnd_Rep_Payee;
    private DbsField pnd_Report_File_Pnd_Rep_First_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Second_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Option_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Mode_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_First_Pay_Due_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Before;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_After;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Diff;
    private DbsField pnd_Report_File_Pnd_Rep_Orgn_Cde;
    private DbsField pnd_Report_File_Pnd_Rep_Last_Name;
    private DbsField pnd_Report_File_Pnd_Rep_First_Name;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Pay_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Div_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Report_File_Pnd_Rep_Inst_Iss_Cde;
    private DbsField pnd_Tmrrw_Dte;

    private DbsGroup pnd_Tmrrw_Dte__R_Field_3;
    private DbsField pnd_Tmrrw_Dte_Pnd_Tmrrw_Cc;
    private DbsField pnd_Tmrrw_Dte_Pnd_Tmrrw_Yy;
    private DbsField pnd_Tmrrw_Dte_Pnd_Tmrrw_Mm;
    private DbsField pnd_Tmrrw_Dte_Pnd_Tmrrw_Dd;
    private DbsField pnd_Format_Check_Date;

    private DbsGroup pnd_Format_Check_Date__R_Field_4;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Mm;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Dd;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Yyyy;
    private DbsField pnd_Prev_Dl_Nbr;
    private DbsField pnd_Prev_Dl_Sort_Pfx;
    private DbsField pnd_Fund_Code_2;
    private DbsField pnd_Dl_Fund_Name;
    private DbsField pnd_Prev_Dl_Fund_Name;
    private DbsField pnd_Count;
    private DbsField pnd_Ledger_Num;
    private DbsField pnd_Ledger_Desc;
    private DbsField pnd_Effctv_Dte;

    private DbsGroup pnd_Effctv_Dte__R_Field_5;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy;
    private DbsField pnd_Formatted_Acct_Dte;

    private DbsGroup pnd_Formatted_Acct_Dte__R_Field_6;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy;
    private DbsField pnd_Formatted_Bsns_Dte;

    private DbsGroup pnd_Formatted_Bsns_Dte__R_Field_7;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy;
    private DbsField pnd_Len;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Inx1;
    private DbsField pnd_Inx2;
    private DbsField pnd_End_Of_Data;
    private DbsField pnd_Hold_Ppcn;
    private DbsField pnd_Hold_Payee;
    private DbsField pnd_Hold_Most_Curr_Trans_Code;
    private DbsField pnd_Multiple_Trans_Sw;
    private DbsField pnd_Total_Issues;
    private DbsField pnd_Total_Deaths;
    private DbsField pnd_Total_Lumpsums;
    private DbsField pnd_Total_Finalpay;

    private DbsGroup pnd_Report_Table;

    private DbsGroup pnd_Report_Table_Pnd_Rt_Funds;
    private DbsField pnd_Report_Table_Pnd_Rt_Fund_Code;
    private DbsField pnd_Report_Table_Pnd_Rt_Fund_Desc;

    private DbsGroup pnd_Report_Table_Pnd_Rt_Ppcn_Entry;
    private DbsField pnd_Report_Table_Pnd_Rt_Ppcn_Desc;
    private DbsField pnd_Report_Table_Pnd_Rt_Trans_Type;

    private DbsGroup pnd_Report_Table__R_Field_8;
    private DbsField pnd_Report_Table_Pnd_Rt_Trans_Type_Issues;
    private DbsField pnd_Report_Table_Pnd_Rt_Trans_Type_Deaths;
    private DbsField pnd_Report_Table_Pnd_Rt_Trans_Type_Lump_Sums;
    private DbsField pnd_Report_Table_Pnd_Rt_Trans_Type_Final_Pays;

    private DbsGroup pnd_Fund_Lit_Table;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Grade;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Stand;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Group;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Rea;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Acc;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Stk;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Mma;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Sca;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Glb;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Grw;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Eqx;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Bnd;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Ilb;
    private DbsField pnd_Fund_Code;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_Comp_Code;
    private DbsField pnd_Fund_Desc_35;

    private DbsGroup pnd_Fund_Desc_35__R_Field_9;
    private DbsField pnd_Fund_Desc_35_Pnd_Fund_Desc;
    private DbsField pnd_Ppcn_Template;

    private DbsGroup pnd_Ppcn_Template__R_Field_10;
    private DbsField pnd_Ppcn_Template_Pnd_Ppcn12;
    private DbsField pnd_Ppcn_Template_Pnd_Ppcn34567;
    private DbsField pnd_Ppcn_Template_Pnd_Ppcnrest;
    private DbsField pnd_Frst_Pa_Slct;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_Report_File = localVariables.newFieldInRecord("pnd_Report_File", "#REPORT-FILE", FieldType.STRING, 175);

        pnd_Report_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Report_File__R_Field_1", "REDEFINE", pnd_Report_File);
        pnd_Report_File_Pnd_Rep_Check_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Date", "#REP-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Report_File__R_Field_2 = pnd_Report_File__R_Field_1.newGroupInGroup("pnd_Report_File__R_Field_2", "REDEFINE", pnd_Report_File_Pnd_Rep_Check_Date);
        pnd_Report_File_Pnd_Rep_Check_Yyyy = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Yyyy", "#REP-CHECK-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Report_File_Pnd_Rep_Check_Mm = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Mm", "#REP-CHECK-MM", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Check_Dd = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Dd", "#REP-CHECK-DD", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Ppcn = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Ppcn", "#REP-PPCN", FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Payee = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Payee", "#REP-PAYEE", FieldType.NUMERIC, 2);
        pnd_Report_File_Pnd_Rep_First_Annt_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Annt_Ind", "#REP-FIRST-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Second_Annt_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Second_Annt_Ind", "#REP-SECOND-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Option_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Option_Code", "#REP-OPTION-CODE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Mode_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Mode_Ind", "#REP-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_First_Pay_Due_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Pay_Due_Date", "#REP-FIRST-PAY-DUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Report_File_Pnd_Rep_Trans_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Code", "#REP-TRANS-CODE", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_Trans_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Date", "#REP-TRANS-DATE", FieldType.TIME);
        pnd_Report_File_Pnd_Rep_Fund_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Code", "#REP-FUND-CODE", FieldType.STRING, 
            2);
        pnd_Report_File_Pnd_Rep_Fund_Units_Before = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Before", "#REP-FUND-UNITS-BEFORE", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_After = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_After", "#REP-FUND-UNITS-AFTER", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_Diff = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Diff", "#REP-FUND-UNITS-DIFF", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Orgn_Cde = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Orgn_Cde", "#REP-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Last_Name = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Last_Name", "#REP-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_First_Name = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Name", "#REP-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_Per_Pay_Amt = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Pay_Amt", "#REP-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Per_Div_Amt = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Div_Amt", "#REP-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr", "#REP-RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Inst_Iss_Cde = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Inst_Iss_Cde", "#REP-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Tmrrw_Dte = localVariables.newFieldInRecord("pnd_Tmrrw_Dte", "#TMRRW-DTE", FieldType.NUMERIC, 8);

        pnd_Tmrrw_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Tmrrw_Dte__R_Field_3", "REDEFINE", pnd_Tmrrw_Dte);
        pnd_Tmrrw_Dte_Pnd_Tmrrw_Cc = pnd_Tmrrw_Dte__R_Field_3.newFieldInGroup("pnd_Tmrrw_Dte_Pnd_Tmrrw_Cc", "#TMRRW-CC", FieldType.NUMERIC, 2);
        pnd_Tmrrw_Dte_Pnd_Tmrrw_Yy = pnd_Tmrrw_Dte__R_Field_3.newFieldInGroup("pnd_Tmrrw_Dte_Pnd_Tmrrw_Yy", "#TMRRW-YY", FieldType.NUMERIC, 2);
        pnd_Tmrrw_Dte_Pnd_Tmrrw_Mm = pnd_Tmrrw_Dte__R_Field_3.newFieldInGroup("pnd_Tmrrw_Dte_Pnd_Tmrrw_Mm", "#TMRRW-MM", FieldType.NUMERIC, 2);
        pnd_Tmrrw_Dte_Pnd_Tmrrw_Dd = pnd_Tmrrw_Dte__R_Field_3.newFieldInGroup("pnd_Tmrrw_Dte_Pnd_Tmrrw_Dd", "#TMRRW-DD", FieldType.NUMERIC, 2);
        pnd_Format_Check_Date = localVariables.newFieldInRecord("pnd_Format_Check_Date", "#FORMAT-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Format_Check_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Format_Check_Date__R_Field_4", "REDEFINE", pnd_Format_Check_Date);
        pnd_Format_Check_Date_Pnd_Business_Mm = pnd_Format_Check_Date__R_Field_4.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Mm", "#BUSINESS-MM", 
            FieldType.NUMERIC, 2);
        pnd_Format_Check_Date_Pnd_Business_Dd = pnd_Format_Check_Date__R_Field_4.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Dd", "#BUSINESS-DD", 
            FieldType.NUMERIC, 2);
        pnd_Format_Check_Date_Pnd_Business_Yyyy = pnd_Format_Check_Date__R_Field_4.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Yyyy", "#BUSINESS-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Prev_Dl_Nbr = localVariables.newFieldInRecord("pnd_Prev_Dl_Nbr", "#PREV-DL-NBR", FieldType.STRING, 8);
        pnd_Prev_Dl_Sort_Pfx = localVariables.newFieldInRecord("pnd_Prev_Dl_Sort_Pfx", "#PREV-DL-SORT-PFX", FieldType.STRING, 2);
        pnd_Fund_Code_2 = localVariables.newFieldInRecord("pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 2);
        pnd_Dl_Fund_Name = localVariables.newFieldInRecord("pnd_Dl_Fund_Name", "#DL-FUND-NAME", FieldType.STRING, 4);
        pnd_Prev_Dl_Fund_Name = localVariables.newFieldInRecord("pnd_Prev_Dl_Fund_Name", "#PREV-DL-FUND-NAME", FieldType.STRING, 4);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 10);
        pnd_Ledger_Num = localVariables.newFieldInRecord("pnd_Ledger_Num", "#LEDGER-NUM", FieldType.STRING, 15);
        pnd_Ledger_Desc = localVariables.newFieldInRecord("pnd_Ledger_Desc", "#LEDGER-DESC", FieldType.STRING, 40);
        pnd_Effctv_Dte = localVariables.newFieldInRecord("pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.NUMERIC, 6);

        pnd_Effctv_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Effctv_Dte__R_Field_5", "REDEFINE", pnd_Effctv_Dte);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm", "#EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd", "#EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy", "#EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Formatted_Acct_Dte = localVariables.newFieldInRecord("pnd_Formatted_Acct_Dte", "#FORMATTED-ACCT-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Acct_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Formatted_Acct_Dte__R_Field_6", "REDEFINE", pnd_Formatted_Acct_Dte);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm = pnd_Formatted_Acct_Dte__R_Field_6.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm", 
            "#FORMATTED-ACCT-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd = pnd_Formatted_Acct_Dte__R_Field_6.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd", 
            "#FORMATTED-ACCT-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy = pnd_Formatted_Acct_Dte__R_Field_6.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy", 
            "#FORMATTED-ACCT-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte = localVariables.newFieldInRecord("pnd_Formatted_Bsns_Dte", "#FORMATTED-BSNS-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Bsns_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Formatted_Bsns_Dte__R_Field_7", "REDEFINE", pnd_Formatted_Bsns_Dte);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm = pnd_Formatted_Bsns_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm", 
            "#FORMATTED-BSNS-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd = pnd_Formatted_Bsns_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd", 
            "#FORMATTED-BSNS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy = pnd_Formatted_Bsns_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy", 
            "#FORMATTED-BSNS-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Inx1 = localVariables.newFieldInRecord("pnd_Inx1", "#INX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Inx2 = localVariables.newFieldInRecord("pnd_Inx2", "#INX2", FieldType.PACKED_DECIMAL, 3);
        pnd_End_Of_Data = localVariables.newFieldInRecord("pnd_End_Of_Data", "#END-OF-DATA", FieldType.STRING, 1);
        pnd_Hold_Ppcn = localVariables.newFieldInRecord("pnd_Hold_Ppcn", "#HOLD-PPCN", FieldType.STRING, 10);
        pnd_Hold_Payee = localVariables.newFieldInRecord("pnd_Hold_Payee", "#HOLD-PAYEE", FieldType.NUMERIC, 2);
        pnd_Hold_Most_Curr_Trans_Code = localVariables.newFieldInRecord("pnd_Hold_Most_Curr_Trans_Code", "#HOLD-MOST-CURR-TRANS-CODE", FieldType.NUMERIC, 
            3);
        pnd_Multiple_Trans_Sw = localVariables.newFieldInRecord("pnd_Multiple_Trans_Sw", "#MULTIPLE-TRANS-SW", FieldType.STRING, 1);
        pnd_Total_Issues = localVariables.newFieldInRecord("pnd_Total_Issues", "#TOTAL-ISSUES", FieldType.NUMERIC, 8);
        pnd_Total_Deaths = localVariables.newFieldInRecord("pnd_Total_Deaths", "#TOTAL-DEATHS", FieldType.NUMERIC, 8);
        pnd_Total_Lumpsums = localVariables.newFieldInRecord("pnd_Total_Lumpsums", "#TOTAL-LUMPSUMS", FieldType.NUMERIC, 8);
        pnd_Total_Finalpay = localVariables.newFieldInRecord("pnd_Total_Finalpay", "#TOTAL-FINALPAY", FieldType.NUMERIC, 8);

        pnd_Report_Table = localVariables.newGroupInRecord("pnd_Report_Table", "#REPORT-TABLE");

        pnd_Report_Table_Pnd_Rt_Funds = pnd_Report_Table.newGroupArrayInGroup("pnd_Report_Table_Pnd_Rt_Funds", "#RT-FUNDS", new DbsArrayController(1, 
            93));
        pnd_Report_Table_Pnd_Rt_Fund_Code = pnd_Report_Table_Pnd_Rt_Funds.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Fund_Code", "#RT-FUND-CODE", FieldType.STRING, 
            2);
        pnd_Report_Table_Pnd_Rt_Fund_Desc = pnd_Report_Table_Pnd_Rt_Funds.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Fund_Desc", "#RT-FUND-DESC", FieldType.STRING, 
            17);

        pnd_Report_Table_Pnd_Rt_Ppcn_Entry = pnd_Report_Table_Pnd_Rt_Funds.newGroupArrayInGroup("pnd_Report_Table_Pnd_Rt_Ppcn_Entry", "#RT-PPCN-ENTRY", 
            new DbsArrayController(1, 3));
        pnd_Report_Table_Pnd_Rt_Ppcn_Desc = pnd_Report_Table_Pnd_Rt_Ppcn_Entry.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Ppcn_Desc", "#RT-PPCN-DESC", FieldType.STRING, 
            20);
        pnd_Report_Table_Pnd_Rt_Trans_Type = pnd_Report_Table_Pnd_Rt_Ppcn_Entry.newFieldArrayInGroup("pnd_Report_Table_Pnd_Rt_Trans_Type", "#RT-TRANS-TYPE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 4));

        pnd_Report_Table__R_Field_8 = pnd_Report_Table_Pnd_Rt_Ppcn_Entry.newGroupInGroup("pnd_Report_Table__R_Field_8", "REDEFINE", pnd_Report_Table_Pnd_Rt_Trans_Type);
        pnd_Report_Table_Pnd_Rt_Trans_Type_Issues = pnd_Report_Table__R_Field_8.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Trans_Type_Issues", "#RT-TRANS-TYPE-ISSUES", 
            FieldType.NUMERIC, 8);
        pnd_Report_Table_Pnd_Rt_Trans_Type_Deaths = pnd_Report_Table__R_Field_8.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Trans_Type_Deaths", "#RT-TRANS-TYPE-DEATHS", 
            FieldType.NUMERIC, 8);
        pnd_Report_Table_Pnd_Rt_Trans_Type_Lump_Sums = pnd_Report_Table__R_Field_8.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Trans_Type_Lump_Sums", "#RT-TRANS-TYPE-LUMP-SUMS", 
            FieldType.NUMERIC, 8);
        pnd_Report_Table_Pnd_Rt_Trans_Type_Final_Pays = pnd_Report_Table__R_Field_8.newFieldInGroup("pnd_Report_Table_Pnd_Rt_Trans_Type_Final_Pays", "#RT-TRANS-TYPE-FINAL-PAYS", 
            FieldType.NUMERIC, 8);

        pnd_Fund_Lit_Table = localVariables.newGroupInRecord("pnd_Fund_Lit_Table", "#FUND-LIT-TABLE");
        pnd_Fund_Lit_Table_Pnd_T_Grade = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Grade", "#T-GRADE", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Stand = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Stand", "#T-STAND", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Group = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Group", "#T-GROUP", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Rea = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Rea", "#T-REA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Acc = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Acc", "#T-ACC", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Stk = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Stk", "#T-STK", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Mma = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Mma", "#T-MMA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Sca = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Sca", "#T-SCA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Glb = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Glb", "#T-GLB", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Grw = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Grw", "#T-GRW", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Eqx = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Eqx", "#T-EQX", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Bnd = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Bnd", "#T-BND", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Ilb = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Ilb", "#T-ILB", FieldType.STRING, 19);
        pnd_Fund_Code = localVariables.newFieldArrayInRecord("pnd_Fund_Code", "#FUND-CODE", FieldType.STRING, 2, new DbsArrayController(1, 93));
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_Comp_Code = localVariables.newFieldArrayInRecord("pnd_Comp_Code", "#COMP-CODE", FieldType.STRING, 1, new DbsArrayController(1, 93));
        pnd_Fund_Desc_35 = localVariables.newFieldInRecord("pnd_Fund_Desc_35", "#FUND-DESC-35", FieldType.STRING, 35);

        pnd_Fund_Desc_35__R_Field_9 = localVariables.newGroupInRecord("pnd_Fund_Desc_35__R_Field_9", "REDEFINE", pnd_Fund_Desc_35);
        pnd_Fund_Desc_35_Pnd_Fund_Desc = pnd_Fund_Desc_35__R_Field_9.newFieldInGroup("pnd_Fund_Desc_35_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 
            10);
        pnd_Ppcn_Template = localVariables.newFieldInRecord("pnd_Ppcn_Template", "#PPCN-TEMPLATE", FieldType.STRING, 10);

        pnd_Ppcn_Template__R_Field_10 = localVariables.newGroupInRecord("pnd_Ppcn_Template__R_Field_10", "REDEFINE", pnd_Ppcn_Template);
        pnd_Ppcn_Template_Pnd_Ppcn12 = pnd_Ppcn_Template__R_Field_10.newFieldInGroup("pnd_Ppcn_Template_Pnd_Ppcn12", "#PPCN12", FieldType.STRING, 2);
        pnd_Ppcn_Template_Pnd_Ppcn34567 = pnd_Ppcn_Template__R_Field_10.newFieldInGroup("pnd_Ppcn_Template_Pnd_Ppcn34567", "#PPCN34567", FieldType.STRING, 
            5);
        pnd_Ppcn_Template_Pnd_Ppcnrest = pnd_Ppcn_Template__R_Field_10.newFieldInGroup("pnd_Ppcn_Template_Pnd_Ppcnrest", "#PPCNREST", FieldType.STRING, 
            3);
        pnd_Frst_Pa_Slct = localVariables.newFieldInRecord("pnd_Frst_Pa_Slct", "#FRST-PA-SLCT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Len.setInitialValue(5);
        pnd_Fund_Lit_Table_Pnd_T_Grade.setInitialValue("1GTIAA GRADED      ");
        pnd_Fund_Lit_Table_Pnd_T_Stand.setInitialValue("1STIAA STANDARD    ");
        pnd_Fund_Lit_Table_Pnd_T_Group.setInitialValue("1 TIAA GROUP       ");
        pnd_Fund_Lit_Table_Pnd_T_Rea.setInitialValue("09TIAA REAL ESTATE ");
        pnd_Fund_Lit_Table_Pnd_T_Acc.setInitialValue("11TIAA ACCESS      ");
        pnd_Fund_Lit_Table_Pnd_T_Stk.setInitialValue("02CREF STOCK       ");
        pnd_Fund_Lit_Table_Pnd_T_Mma.setInitialValue("03CREF MMA         ");
        pnd_Fund_Lit_Table_Pnd_T_Sca.setInitialValue("04CREF SOCIAL      ");
        pnd_Fund_Lit_Table_Pnd_T_Glb.setInitialValue("06CREF GLOBAL      ");
        pnd_Fund_Lit_Table_Pnd_T_Grw.setInitialValue("08CREF GROWTH      ");
        pnd_Fund_Lit_Table_Pnd_T_Eqx.setInitialValue("07CREF EQUITY      ");
        pnd_Fund_Lit_Table_Pnd_T_Bnd.setInitialValue("05CREF BOND        ");
        pnd_Fund_Lit_Table_Pnd_T_Ilb.setInitialValue("10CREF ILB         ");
        pnd_Frst_Pa_Slct.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap782() throws Exception
    {
        super("Iaap782");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP782", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Count.reset();                                                                                                                                                //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: RESET #COUNT
        pnd_End_Of_Data.reset();                                                                                                                                          //Natural: RESET #END-OF-DATA
        //*  ADDED 5/00
        //*  ADDED 5/00
        //*  ADDED 5/00
        //*  ADDED 4/01
        //*  ADDED 4/01
        //*  ADDED 4/01
        //*  CHANGED FROM 1:20 TO 1:40  5/00
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Fund_Code.getValue(1,":",40).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Nm_Cd().getValue(1,":",40));                                                         //Natural: ASSIGN #FUND-CODE ( 1:40 ) := IAAA051Z.#IA-RPT-NM-CD ( 1:40 )
        pnd_Comp_Code.getValue(1,":",40).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Rpt_Cmpny_Cd_A().getValue(1,":",40));                                                    //Natural: ASSIGN #COMP-CODE ( 1:40 ) := IAAA051Z.#IA-RPT-CMPNY-CD-A ( 1:40 )
        pnd_Fund_Code.getValue(91,":",91).setValue(91);                                                                                                                   //Natural: ASSIGN #FUND-CODE ( 91:91 ) := 91
        pnd_Fund_Code.getValue(92,":",92).setValue(92);                                                                                                                   //Natural: ASSIGN #FUND-CODE ( 92:92 ) := 92
        pnd_Fund_Code.getValue(93,":",93).setValue(93);                                                                                                                   //Natural: ASSIGN #FUND-CODE ( 93:93 ) := 93
        //*  #COMP-CODE (1:40) := IAAA0510.#IA-RPT-CMPNY-CD-A(1:40)
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            if (condition(pnd_Fund_Code.getValue(pnd_I).notEquals(" ")))                                                                                                  //Natural: IF #FUND-CODE ( #I ) NE ' '
            {
                pnd_Report_Table_Pnd_Rt_Fund_Code.getValue(pnd_I).setValue(pnd_Fund_Code.getValue(pnd_I));                                                                //Natural: MOVE #FUND-CODE ( #I ) TO #RT-FUND-CODE ( #I ) #FUND-CODE-2
                pnd_Fund_Code_2.setValue(pnd_Fund_Code.getValue(pnd_I));
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                sub_Pnd_Get_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_35_Pnd_Fund_Desc));                              //Natural: COMPRESS #COMP-DESC #FUND-DESC INTO #RT-FUND-DESC ( #I )
                //*    COMPRESS  #FUND-DESC INTO #RT-FUND-DESC(#I)
                //*  ADDED FOLLOWING FOR PA SELECT     5/00                                                                                                               //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet220 = 0;                                                                                                                         //Natural: WHEN #FUND-CODE ( #I ) = '41' THRU '60'
                if (condition(pnd_Fund_Code.getValue(pnd_I).greaterOrEqual("41") && pnd_Fund_Code.getValue(pnd_I).lessOrEqual("60")))
                {
                    decideConditionsMet220++;
                    //*  9/17
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("IA thru IG          ");                                                                 //Natural: MOVE 'IA thru IG          ' TO #RT-PPCN-DESC ( #I,1 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("S087000 - S087999   ");                                                                 //Natural: MOVE 'S087000 - S087999   ' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("W022000 - W022999   ");                                                                 //Natural: MOVE 'W022000 - W022999   ' TO #RT-PPCN-DESC ( #I,3 )
                    //*  END OF ADD FOR PA SELECT     5/00
                }                                                                                                                                                         //Natural: WHEN #FUND-CODE ( #I ) = '1G'
                else if (condition(pnd_Fund_Code.getValue(pnd_I).equals("1G")))
                {
                    decideConditionsMet220++;
                    //*  9/17
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("GA, IA-IG, 6M, Y    ");                                                                 //Natural: MOVE 'GA, IA-IG, 6M, Y    ' TO #RT-PPCN-DESC ( #I,1 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("GW, W0 (1-24999), 6L");                                                                 //Natural: MOVE 'GW, W0 (1-24999), 6L' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("                    ");                                                                 //Natural: MOVE '                    ' TO #RT-PPCN-DESC ( #I,3 )
                }                                                                                                                                                         //Natural: WHEN #FUND-CODE ( #I ) = '1S'
                else if (condition(pnd_Fund_Code.getValue(pnd_I).equals("1S")))
                {
                    decideConditionsMet220++;
                    //*  9/17
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("GA, IA-IG, 6M, Y    ");                                                                 //Natural: MOVE 'GA, IA-IG, 6M, Y    ' TO #RT-PPCN-DESC ( #I,1 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("GW, W0 (1-24999), 6L");                                                                 //Natural: MOVE 'GW, W0 (1-24999), 6L' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("S0                  ");                                                                 //Natural: MOVE 'S0                  ' TO #RT-PPCN-DESC ( #I,3 )
                }                                                                                                                                                         //Natural: WHEN #FUND-CODE ( #I ) = '1 '
                else if (condition(pnd_Fund_Code.getValue(pnd_I).equals("1 ")))
                {
                    decideConditionsMet220++;
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("W0 (25000-79999)    ");                                                                 //Natural: MOVE 'W0 (25000-79999)    ' TO #RT-PPCN-DESC ( #I,1 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("W0 (80000-89999)    ");                                                                 //Natural: MOVE 'W0 (80000-89999)    ' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("                    ");                                                                 //Natural: MOVE '                    ' TO #RT-PPCN-DESC ( #I,3 )
                }                                                                                                                                                         //Natural: WHEN #COMP-CODE ( #I ) = 'U'
                else if (condition(pnd_Comp_Code.getValue(pnd_I).equals("U")))
                {
                    decideConditionsMet220++;
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("6L, GW, W0 (1-24999)");                                                                 //Natural: MOVE '6L, GW, W0 (1-24999)' TO #RT-PPCN-DESC ( #I,1 )
                    //*  9/17
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("6M, GA, IA-IG, Y    ");                                                                 //Natural: MOVE '6M, GA, IA-IG, Y    ' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("6N                  ");                                                                 //Natural: MOVE '6N                  ' TO #RT-PPCN-DESC ( #I,3 )
                }                                                                                                                                                         //Natural: WHEN #COMP-CODE ( #I ) = '2'
                else if (condition(pnd_Comp_Code.getValue(pnd_I).equals("2")))
                {
                    decideConditionsMet220++;
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,1).setValue("0L, 6L              ");                                                                 //Natural: MOVE '0L, 6L              ' TO #RT-PPCN-DESC ( #I,1 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,2).setValue("0M, 0T, 0U, 6M, Z   ");                                                                 //Natural: MOVE '0M, 0T, 0U, 6M, Z   ' TO #RT-PPCN-DESC ( #I,2 )
                    pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_I,3).setValue("0N, 6N              ");                                                                 //Natural: MOVE '0N, 6N              ' TO #RT-PPCN-DESC ( #I,3 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA    DR01
        pnd_Report_Table_Pnd_Rt_Fund_Code.getValue(91).setValue("91");                                                                                                    //Natural: MOVE '91' TO #RT-FUND-CODE ( 91 )
        pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(91).setValue("TIAA TPA");                                                                                              //Natural: MOVE 'TIAA TPA' TO #RT-FUND-DESC ( 91 )
        pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(91,1).setValue("IA thru IF,IG,IH,&IJ");                                                                                //Natural: MOVE 'IA thru IF,IG,IH,&IJ' TO #RT-PPCN-DESC ( 91,1 )
        //*  IPRO   DR01
        pnd_Report_Table_Pnd_Rt_Fund_Code.getValue(92).setValue("92");                                                                                                    //Natural: MOVE '92' TO #RT-FUND-CODE ( 92 )
        pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(92).setValue("TIAA IPRO");                                                                                             //Natural: MOVE 'TIAA IPRO' TO #RT-FUND-DESC ( 92 )
        pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(92,1).setValue("IP");                                                                                                  //Natural: MOVE 'IP' TO #RT-PPCN-DESC ( 92,1 )
        //*  P&I    DR01
        pnd_Report_Table_Pnd_Rt_Fund_Code.getValue(93).setValue("93");                                                                                                    //Natural: MOVE '93' TO #RT-FUND-CODE ( 93 )
        pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(93).setValue("TIAA P&I");                                                                                              //Natural: MOVE 'TIAA P&I' TO #RT-FUND-DESC ( 93 )
        pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(93,1).setValue("S0 IA - IF,IG,IH,&IJ");                                                                                //Natural: MOVE 'S0 IA - IF,IG,IH,&IJ' TO #RT-PPCN-DESC ( 93,1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 02 #REPORT-FILE
        while (condition(getWorkFiles().read(2, pnd_Report_File)))
        {
            //*   IF #REP-OPTION-CODE = 28 OR = 30               /* TPA    DR01
            //*     MOVE 91 TO #REP-FUND-CODE
            //*  ELSE
            //*    IF #REP-OPTION-CODE = 25 OR = 27             /* IPRO   DR01
            //*      MOVE 92 TO #REP-FUND-CODE
            //*    ELSE
            //*      IF #REP-OPTION-CODE = 22                   /* P&I    DR01
            //*        MOVE 93 TO #REP-FUND-CODE
            //*      END-IF
            //*    END-IF
            //*  END-IF
            if (condition(!((pnd_Report_File_Pnd_Rep_Trans_Code.notEquals(50) && pnd_Report_File_Pnd_Rep_Trans_Code.notEquals(60)))))                                     //Natural: ACCEPT IF ( #REP-TRANS-CODE NOT = 050 AND #REP-TRANS-CODE NE 060 )
            {
                continue;
            }
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            getSort().writeSortInData(pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date, pnd_Report_File_Pnd_Rep_Check_Date,  //Natural: END-ALL
                pnd_Report_File_Pnd_Rep_Option_Code, pnd_Report_File_Pnd_Rep_First_Annt_Ind, pnd_Report_File_Pnd_Rep_Second_Annt_Ind, pnd_Report_File_Pnd_Rep_Mode_Ind, 
                pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, pnd_Report_File_Pnd_Rep_Trans_Code, pnd_Report_File_Pnd_Rep_Fund_Code, pnd_Report_File_Pnd_Rep_Fund_Units_Before, 
                pnd_Report_File_Pnd_Rep_Fund_Units_After, pnd_Report_File_Pnd_Rep_Fund_Units_Diff, pnd_Report_File_Pnd_Rep_Orgn_Cde);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date, "DESCENDING");                                //Natural: SORT BY #REP-PPCN #REP-PAYEE #REP-TRANS-DATE DESCENDING USING #REP-CHECK-DATE #REP-OPTION-CODE #REP-FIRST-ANNT-IND #REP-SECOND-ANNT-IND #REP-MODE-IND #REP-FIRST-PAY-DUE-DATE #REP-TRANS-CODE #REP-FUND-CODE #REP-FUND-UNITS-BEFORE #REP-FUND-UNITS-AFTER #REP-FUND-UNITS-DIFF #REP-ORGN-CDE
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date, pnd_Report_File_Pnd_Rep_Check_Date, 
            pnd_Report_File_Pnd_Rep_Option_Code, pnd_Report_File_Pnd_Rep_First_Annt_Ind, pnd_Report_File_Pnd_Rep_Second_Annt_Ind, pnd_Report_File_Pnd_Rep_Mode_Ind, 
            pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, pnd_Report_File_Pnd_Rep_Trans_Code, pnd_Report_File_Pnd_Rep_Fund_Code, pnd_Report_File_Pnd_Rep_Fund_Units_Before, 
            pnd_Report_File_Pnd_Rep_Fund_Units_After, pnd_Report_File_Pnd_Rep_Fund_Units_Diff, pnd_Report_File_Pnd_Rep_Orgn_Cde)))
        {
            pnd_Ppcn_Template.setValue(pnd_Report_File_Pnd_Rep_Ppcn);                                                                                                     //Natural: MOVE #REP-PPCN TO #PPCN-TEMPLATE
            pnd_Format_Check_Date_Pnd_Business_Mm.setValue(pnd_Report_File_Pnd_Rep_Check_Mm);                                                                             //Natural: MOVE #REP-CHECK-MM TO #BUSINESS-MM
            pnd_Format_Check_Date_Pnd_Business_Dd.setValue(pnd_Report_File_Pnd_Rep_Check_Dd);                                                                             //Natural: MOVE #REP-CHECK-DD TO #BUSINESS-DD
            pnd_Format_Check_Date_Pnd_Business_Yyyy.setValue(pnd_Report_File_Pnd_Rep_Check_Yyyy);                                                                         //Natural: MOVE #REP-CHECK-YYYY TO #BUSINESS-YYYY
            //*  ---- BYPASS PROCESSING BELOW
            if (condition((pnd_Report_File_Pnd_Rep_Ppcn.equals(pnd_Hold_Ppcn) && pnd_Report_File_Pnd_Rep_Payee.notEquals(pnd_Hold_Payee))))                               //Natural: IF ( #REP-PPCN = #HOLD-PPCN AND #REP-PAYEE NOT = #HOLD-PAYEE )
            {
                if (condition((pnd_Hold_Most_Curr_Trans_Code.equals(40) && pnd_Report_File_Pnd_Rep_Trans_Code.equals(40)) || (pnd_Hold_Most_Curr_Trans_Code.equals(6)     //Natural: IF ( #HOLD-MOST-CURR-TRANS-CODE = 040 AND #REP-TRANS-CODE = 040 ) OR ( #HOLD-MOST-CURR-TRANS-CODE = 006 AND #REP-TRANS-CODE = 006 )
                    && pnd_Report_File_Pnd_Rep_Trans_Code.equals(6))))
                {
                    pnd_Hold_Ppcn.setValue(pnd_Report_File_Pnd_Rep_Ppcn);                                                                                                 //Natural: MOVE #REP-PPCN TO #HOLD-PPCN
                    pnd_Hold_Payee.setValue(pnd_Report_File_Pnd_Rep_Payee);                                                                                               //Natural: MOVE #REP-PAYEE TO #HOLD-PAYEE
                    pnd_Hold_Most_Curr_Trans_Code.setValue(pnd_Report_File_Pnd_Rep_Trans_Code);                                                                           //Natural: MOVE #REP-TRANS-CODE TO #HOLD-MOST-CURR-TRANS-CODE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ---- BYPASS PROCESSING ABOVE
            if (condition((pnd_Report_File_Pnd_Rep_Ppcn.notEquals(pnd_Hold_Ppcn)) || (pnd_Report_File_Pnd_Rep_Payee.notEquals(pnd_Hold_Payee))))                          //Natural: IF ( #REP-PPCN NOT = #HOLD-PPCN ) OR ( #REP-PAYEE NOT = #HOLD-PAYEE )
            {
                pnd_Hold_Ppcn.setValue(pnd_Report_File_Pnd_Rep_Ppcn);                                                                                                     //Natural: MOVE #REP-PPCN TO #HOLD-PPCN
                pnd_Hold_Payee.setValue(pnd_Report_File_Pnd_Rep_Payee);                                                                                                   //Natural: MOVE #REP-PAYEE TO #HOLD-PAYEE
                pnd_Hold_Most_Curr_Trans_Code.setValue(pnd_Report_File_Pnd_Rep_Trans_Code);                                                                               //Natural: MOVE #REP-TRANS-CODE TO #HOLD-MOST-CURR-TRANS-CODE
                                                                                                                                                                          //Natural: PERFORM SET-FUND-AND-PPCN-INDICES
                sub_Set_Fund_And_Ppcn_Indices();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM EVALUATE-MOST-CURR-TRAN-CODE
                sub_Evaluate_Most_Curr_Tran_Code();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  (IF PPCN AND PAYEE ARE EQUAL)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  DEATH CLAIM
                if (condition(pnd_Hold_Most_Curr_Trans_Code.equals(66)))                                                                                                  //Natural: IF #HOLD-MOST-CURR-TRANS-CODE = 066
                {
                    //*  NEW ISSUE
                    if (condition(pnd_Report_File_Pnd_Rep_Trans_Code.equals(31) || pnd_Report_File_Pnd_Rep_Trans_Code.equals(30) || pnd_Report_File_Pnd_Rep_Trans_Code.equals(33))) //Natural: IF #REP-TRANS-CODE = 031 OR = 030 OR = 033
                    {
                                                                                                                                                                          //Natural: PERFORM SET-FUND-AND-PPCN-INDICES
                        sub_Set_Fund_And_Ppcn_Indices();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM EVALUATE-MOST-CURR-TRAN-CODE
                        sub_Evaluate_Most_Curr_Tran_Code();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        //*  ===========================================================
        //*                   END  OF  SORT  LOOP
        //*  ===========================================================
        if (condition(pnd_Count.equals(getZero())))                                                                                                                       //Natural: IF #COUNT = 0
        {
            getReports().write(0, NEWLINE,NEWLINE,"*** NO TRANSACTIONS PROCESSED TODAY ***");                                                                             //Natural: WRITE // '*** NO TRANSACTIONS PROCESSED TODAY ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Number of report file records accepted:",pnd_Count);                                                                                   //Natural: WRITE 'Number of report file records accepted:' #COUNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-TABLE
        sub_Print_Report_Table();
        if (condition(Global.isEscape())) {return;}
        //*  END OF MAINLINE CODE
        //*  BEGINNING OF SUBROUTINES
        //* **********************************
        //*    WRITE '=' #INX1
        //*                       OR = '2' OR = '4')
        //*      ((#REP-FUND-CODE = '09') AND
        //*      ((#REP-FUND-CODE = '02' THRU '09') AND
        //*      ((#REP-FUND-CODE = '09') AND
        //*      ((#REP-FUND-CODE = '02' THRU '09') AND
        //* **********************************
        //* *************************
        //* *************************
        //*  END 0F ADD  10/01
        //* ********************************************************************
        //* ********************************************************************
        //*  OS 082017 START                                                                                                                                              //Natural: AT TOP OF PAGE ( 1 )
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Set_Fund_And_Ppcn_Indices() throws Exception                                                                                                         //Natural: SET-FUND-AND-PPCN-INDICES
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Report_File_Pnd_Rep_Fund_Code.equals("1 ")))                                                                                                    //Natural: IF #REP-FUND-CODE = '1 '
        {
            getReports().write(0, "=",pnd_Ppcn_Template,"=",pnd_Report_File_Pnd_Rep_Payee,"=",pnd_Report_File_Pnd_Rep_Fund_Code);                                         //Natural: WRITE '=' #PPCN-TEMPLATE '=' #REP-PAYEE '=' #REP-FUND-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET THE FUND INDEX
        DbsUtil.examine(new ExamineSource(pnd_Fund_Code.getValue("*")), new ExamineSearch(pnd_Report_File_Pnd_Rep_Fund_Code), new ExamineGivingIndex(pnd_Inx1));          //Natural: EXAMINE #FUND-CODE ( * ) FOR #REP-FUND-CODE GIVING INDEX #INX1
        if (condition(pnd_Inx1.equals(getZero())))                                                                                                                        //Natural: IF #INX1 = 0
        {
            getReports().write(0, "=",pnd_Ppcn_Template,"=",pnd_Report_File_Pnd_Rep_Payee,"=",pnd_Inx1);                                                                  //Natural: WRITE '=' #PPCN-TEMPLATE '=' #REP-PAYEE '=' #INX1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET THE PPCN INDEX
        //*  ADDED 10/01 PA
        short decideConditionsMet407 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #REP-OPTION-CODE = 28 OR = 30
        if (condition(pnd_Report_File_Pnd_Rep_Option_Code.equals(28) || pnd_Report_File_Pnd_Rep_Option_Code.equals(30)))
        {
            decideConditionsMet407++;
            //*  ADDED TPA 10/01
            pnd_Inx1.setValue(91);                                                                                                                                        //Natural: MOVE 91 TO #INX1
            //*  ADDED TPA 10/01
            //*  IPRO   DR01
            pnd_Inx2.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN #REP-OPTION-CODE = 25 OR = 27
        else if (condition(pnd_Report_File_Pnd_Rep_Option_Code.equals(25) || pnd_Report_File_Pnd_Rep_Option_Code.equals(27)))
        {
            decideConditionsMet407++;
            //*  ADDED TPA 10/01
            pnd_Inx1.setValue(92);                                                                                                                                        //Natural: MOVE 92 TO #INX1
            //*  ADDED IPRO 10/01
            //*  P&I    DR01
            pnd_Inx2.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN #REP-OPTION-CODE = 22
        else if (condition(pnd_Report_File_Pnd_Rep_Option_Code.equals(22)))
        {
            decideConditionsMet407++;
            //*  ADDED TPA 10/01
            pnd_Inx1.setValue(93);                                                                                                                                        //Natural: MOVE 93 TO #INX1
            //*  ADDED P&I 10/01
            //*  ADDED PA SELECT 5/00
            //*  ADDED PA SELECT 5/00
            //*  NEW PA ORIGIN
            pnd_Inx2.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN ( #COMP-CODE ( #INX1 ) = 'U' OR = 'W' ) AND ( #REP-ORGN-CDE = 37 OR = 38 OR = 40 OR = 43 OR = 45 OR = 46 )
        else if (condition(((pnd_Comp_Code.getValue(pnd_Inx1).equals("U") || pnd_Comp_Code.getValue(pnd_Inx1).equals("W")) && (((((pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(37) 
            || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(38)) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(40)) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(43)) 
            || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(45)) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(46)))))
        {
            decideConditionsMet407++;
            //*  ADDED PA SELECT 5/00
            //*  NEW PA ORIGIN
            if (condition(pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(38) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(40) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(43)       //Natural: IF #REP-ORGN-CDE = 38 OR = 40 OR = 43 OR = 45 OR = 46
                || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(45) || pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(46)))
            {
                //*  ADDED PA SELECT 5/00
                pnd_Inx2.setValue(1);                                                                                                                                     //Natural: MOVE 1 TO #INX2
                //*  ADDED PA SELECT 5/00
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED PA SELECT 5/00
            if (condition(pnd_Report_File_Pnd_Rep_Orgn_Cde.equals(37)))                                                                                                   //Natural: IF #REP-ORGN-CDE = 37
            {
                if (condition(pnd_Ppcn_Template_Pnd_Ppcn12.equals("W0")))                                                                                                 //Natural: IF #PPCN12 = 'W0'
                {
                    //*  ADDED PA SELECT 5/00
                    pnd_Inx2.setValue(3);                                                                                                                                 //Natural: MOVE 3 TO #INX2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inx2.setValue(2);                                                                                                                                 //Natural: MOVE 2 TO #INX2
                    //*  ADDED PA SELECT 5/00
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED PA SELECT 5/00
                //*  9/17
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( ( #REP-FUND-CODE = '1G' OR = '1S' ) AND ( #PPCN12 = 'GA' OR = '6M' OR ( #PPCN12 = 'IA' THRU 'IG' ) OR ( #PPCN12 = 'Y0' THRU 'Y9' ) ) ) OR ( #REP-FUND-CODE = '1 ' AND #PPCN12 = 'W0' AND #PPCN34567 GE '25000' AND #PPCN34567 LE '79999' ) OR ( ( #COMP-CODE ( #INX1 ) = 'U' ) AND ( #PPCN12 = '6L' OR = 'GW' OR ( #PPCN12 = 'W0' AND #PPCN34567 GE '00001' AND #PPCN34567 LE '24999' ) ) ) OR ( ( #COMP-CODE ( #INX1 ) = '2' ) AND ( #PPCN12 = '0L' OR = '6L' ) )
        else if (condition((((((pnd_Report_File_Pnd_Rep_Fund_Code.equals("1G") || pnd_Report_File_Pnd_Rep_Fund_Code.equals("1S")) && (((pnd_Ppcn_Template_Pnd_Ppcn12.equals("GA") 
            || pnd_Ppcn_Template_Pnd_Ppcn12.equals("6M")) || (pnd_Ppcn_Template_Pnd_Ppcn12.greaterOrEqual("IA") && pnd_Ppcn_Template_Pnd_Ppcn12.lessOrEqual("IG"))) 
            || (pnd_Ppcn_Template_Pnd_Ppcn12.greaterOrEqual("Y0") && pnd_Ppcn_Template_Pnd_Ppcn12.lessOrEqual("Y9")))) || (((pnd_Report_File_Pnd_Rep_Fund_Code.equals("1 ") 
            && pnd_Ppcn_Template_Pnd_Ppcn12.equals("W0")) && pnd_Ppcn_Template_Pnd_Ppcn34567.greaterOrEqual("25000")) && pnd_Ppcn_Template_Pnd_Ppcn34567.lessOrEqual("79999"))) 
            || (pnd_Comp_Code.getValue(pnd_Inx1).equals("U") && ((pnd_Ppcn_Template_Pnd_Ppcn12.equals("6L") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("GW")) 
            || ((pnd_Ppcn_Template_Pnd_Ppcn12.equals("W0") && pnd_Ppcn_Template_Pnd_Ppcn34567.greaterOrEqual("00001")) && pnd_Ppcn_Template_Pnd_Ppcn34567.lessOrEqual("24999"))))) 
            || (pnd_Comp_Code.getValue(pnd_Inx1).equals("2") && (pnd_Ppcn_Template_Pnd_Ppcn12.equals("0L") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("6L"))))))
        {
            decideConditionsMet407++;
            //*  9/17
            pnd_Inx2.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN ( ( #REP-FUND-CODE = '1G' OR = '1S' ) AND ( #PPCN12 = 'GW' OR = '6L' OR ( #PPCN12 = 'W0' AND #PPCN34567 GE '00001' AND #PPCN34567 LE '24999' ) ) ) OR ( #REP-FUND-CODE = '1 ' AND #PPCN12 = 'W0' AND #PPCN34567 GE '80000' AND #PPCN34567 LE '89999' ) OR ( ( #COMP-CODE ( #INX1 ) = 'U' ) AND ( #PPCN12 = '6M' OR = 'GA' OR ( #PPCN12 = 'IA' THRU 'IG' ) OR ( #PPCN12 = 'Y0' THRU 'Y9' ) ) ) OR ( ( #COMP-CODE ( #INX1 ) = '2' ) AND ( ( #PPCN12 = '0M' OR = '0T' OR = '0U' OR = '6M' ) OR ( #PPCN12 = 'Z0' THRU 'Z9' ) ) )
        else if (condition((((((pnd_Report_File_Pnd_Rep_Fund_Code.equals("1G") || pnd_Report_File_Pnd_Rep_Fund_Code.equals("1S")) && ((pnd_Ppcn_Template_Pnd_Ppcn12.equals("GW") 
            || pnd_Ppcn_Template_Pnd_Ppcn12.equals("6L")) || ((pnd_Ppcn_Template_Pnd_Ppcn12.equals("W0") && pnd_Ppcn_Template_Pnd_Ppcn34567.greaterOrEqual("00001")) 
            && pnd_Ppcn_Template_Pnd_Ppcn34567.lessOrEqual("24999")))) || (((pnd_Report_File_Pnd_Rep_Fund_Code.equals("1 ") && pnd_Ppcn_Template_Pnd_Ppcn12.equals("W0")) 
            && pnd_Ppcn_Template_Pnd_Ppcn34567.greaterOrEqual("80000")) && pnd_Ppcn_Template_Pnd_Ppcn34567.lessOrEqual("89999"))) || (pnd_Comp_Code.getValue(pnd_Inx1).equals("U") 
            && (((pnd_Ppcn_Template_Pnd_Ppcn12.equals("6M") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("GA")) || (pnd_Ppcn_Template_Pnd_Ppcn12.greaterOrEqual("IA") 
            && pnd_Ppcn_Template_Pnd_Ppcn12.lessOrEqual("IG"))) || (pnd_Ppcn_Template_Pnd_Ppcn12.greaterOrEqual("Y0") && pnd_Ppcn_Template_Pnd_Ppcn12.lessOrEqual("Y9"))))) 
            || (pnd_Comp_Code.getValue(pnd_Inx1).equals("2") && ((((pnd_Ppcn_Template_Pnd_Ppcn12.equals("0M") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("0T")) 
            || pnd_Ppcn_Template_Pnd_Ppcn12.equals("0U")) || pnd_Ppcn_Template_Pnd_Ppcn12.equals("6M")) || (pnd_Ppcn_Template_Pnd_Ppcn12.greaterOrEqual("Z0") 
            && pnd_Ppcn_Template_Pnd_Ppcn12.lessOrEqual("Z9")))))))
        {
            decideConditionsMet407++;
            pnd_Inx2.setValue(2);                                                                                                                                         //Natural: MOVE 2 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN ( #PPCN12 = 'S0' OR = '6N' OR = '0N' )
        else if (condition(pnd_Ppcn_Template_Pnd_Ppcn12.equals("S0") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("6N") || pnd_Ppcn_Template_Pnd_Ppcn12.equals("0N")))
        {
            decideConditionsMet407++;
            pnd_Inx2.setValue(3);                                                                                                                                         //Natural: MOVE 3 TO #INX2
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Inx2.equals(getZero())))                                                                                                                        //Natural: IF #INX2 = 0
        {
            getReports().write(0, "=",pnd_Ppcn_Template,"=",pnd_Report_File_Pnd_Rep_Payee);                                                                               //Natural: WRITE '=' #PPCN-TEMPLATE '=' #REP-PAYEE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Evaluate_Most_Curr_Tran_Code() throws Exception                                                                                                      //Natural: EVALUATE-MOST-CURR-TRAN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition((((pnd_Report_File_Pnd_Rep_Trans_Code.equals(31) || pnd_Report_File_Pnd_Rep_Trans_Code.equals(30)) || pnd_Report_File_Pnd_Rep_Trans_Code.equals(33))  //Natural: IF ( #REP-TRANS-CODE = 031 OR = 030 OR = 033 ) AND ( #REP-PAYEE = 01 )
            && pnd_Report_File_Pnd_Rep_Payee.equals(1))))
        {
            //*  ISSUES
            pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,1).nadd(1);                                                                                     //Natural: ADD 1 TO #RT-TRANS-TYPE ( #INX1,#INX2,1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Report_File_Pnd_Rep_Trans_Code.equals(66) && (pnd_Report_File_Pnd_Rep_Payee.equals(1) || pnd_Report_File_Pnd_Rep_Payee.equals(2)))))           //Natural: IF ( #REP-TRANS-CODE = 066 ) AND ( #REP-PAYEE = 01 OR = 02 )
        {
            //*  DEATHS
            pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,2).nadd(1);                                                                                     //Natural: ADD 1 TO #RT-TRANS-TYPE ( #INX1,#INX2,2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Report_File_Pnd_Rep_Trans_Code.equals(40))))                                                                                                   //Natural: IF ( #REP-TRANS-CODE = 040 )
        {
            //*  LUMP SUMS
            pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,3).nadd(1);                                                                                     //Natural: ADD 1 TO #RT-TRANS-TYPE ( #INX1,#INX2,3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Report_File_Pnd_Rep_Trans_Code.equals(6))))                                                                                                    //Natural: IF ( #REP-TRANS-CODE = 006 )
        {
            //*  FINAL PAY
            pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,4).nadd(1);                                                                                     //Natural: ADD 1 TO #RT-TRANS-TYPE ( #INX1,#INX2,4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  DR01
    private void sub_Print_Report_Table() throws Exception                                                                                                                //Natural: PRINT-REPORT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #INX1 1 TO 93
        for (pnd_Inx1.setValue(1); condition(pnd_Inx1.lessOrEqual(93)); pnd_Inx1.nadd(1))
        {
            if (condition(pnd_Report_Table_Pnd_Rt_Fund_Code.getValue(pnd_Inx1).equals(" ")))                                                                              //Natural: IF #RT-FUND-CODE ( #INX1 ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    WRITE '=' #INX1  '='  #RT-FUND-DESC (#INX1)
                getReports().write(1, ReportOption.NOTITLE," ");                                                                                                          //Natural: WRITE ( 01 ) ' '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADDED FOLLOWING 10/01 PRINT TPA,IPRO & P/I TOTALS
                if (condition(pnd_Inx1.equals(5)))                                                                                                                        //Natural: IF #INX1 = 5
                {
                    pnd_Inx1.setValue(91);                                                                                                                                //Natural: ASSIGN #INX1 := 91
                    REPEAT01:                                                                                                                                             //Natural: REPEAT
                    while (condition(whileTrue))
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #INX2 1 TO 3
                        for (pnd_Inx2.setValue(1); condition(pnd_Inx2.lessOrEqual(3)); pnd_Inx2.nadd(1))
                        {
                            getReports().write(1, ReportOption.NOTITLE,pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(pnd_Inx1),pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_Inx1,pnd_Inx2),new  //Natural: WRITE ( 01 ) #RT-FUND-DESC ( #INX1 ) #RT-PPCN-DESC ( #INX1,#INX2 ) 12X #RT-TRANS-TYPE ( #INX1,#INX2,1 ) ( EM = Z,ZZZ,ZZ9 ) 11X #RT-TRANS-TYPE ( #INX1,#INX2,2 ) ( EM = Z,ZZZ,ZZ9 ) 12X #RT-TRANS-TYPE ( #INX1,#INX2,3 ) ( EM = Z,ZZZ,ZZ9 ) 15X #RT-TRANS-TYPE ( #INX1,#INX2,4 ) ( EM = Z,ZZZ,ZZ9 )
                                ColumnSpacing(12),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                                ColumnSpacing(11),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,2), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                                ColumnSpacing(12),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,3), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                                ColumnSpacing(15),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,4), new ReportEditMask ("Z,ZZZ,ZZ9"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Total_Issues.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,1));                                                      //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,1 ) TO #TOTAL-ISSUES
                            pnd_Total_Deaths.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,2));                                                      //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,2 ) TO #TOTAL-DEATHS
                            pnd_Total_Lumpsums.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,3));                                                    //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,3 ) TO #TOTAL-LUMPSUMS
                            pnd_Total_Finalpay.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,4));                                                    //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,4 ) TO #TOTAL-FINALPAY
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Inx1.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #INX1
                        if (condition(pnd_Inx1.greater(93)))                                                                                                              //Natural: IF #INX1 GT 93
                        {
                            pnd_Inx1.setValue(5);                                                                                                                         //Natural: ASSIGN #INX1 := 5
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-REPEAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                FOR04:                                                                                                                                                    //Natural: FOR #INX2 1 TO 3
                for (pnd_Inx2.setValue(1); condition(pnd_Inx2.lessOrEqual(3)); pnd_Inx2.nadd(1))
                {
                    getReports().write(1, ReportOption.NOTITLE,pnd_Report_Table_Pnd_Rt_Fund_Desc.getValue(pnd_Inx1),pnd_Report_Table_Pnd_Rt_Ppcn_Desc.getValue(pnd_Inx1,pnd_Inx2),new  //Natural: WRITE ( 01 ) #RT-FUND-DESC ( #INX1 ) #RT-PPCN-DESC ( #INX1,#INX2 ) 12X #RT-TRANS-TYPE ( #INX1,#INX2,1 ) ( EM = Z,ZZZ,ZZ9 ) 11X #RT-TRANS-TYPE ( #INX1,#INX2,2 ) ( EM = Z,ZZZ,ZZ9 ) 12X #RT-TRANS-TYPE ( #INX1,#INX2,3 ) ( EM = Z,ZZZ,ZZ9 ) 15X #RT-TRANS-TYPE ( #INX1,#INX2,4 ) ( EM = Z,ZZZ,ZZ9 )
                        ColumnSpacing(12),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,1), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(11),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,2), 
                        new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,3), new ReportEditMask 
                        ("Z,ZZZ,ZZ9"),new ColumnSpacing(15),pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,4), new ReportEditMask ("Z,ZZZ,ZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Total_Issues.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,1));                                                              //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,1 ) TO #TOTAL-ISSUES
                    pnd_Total_Deaths.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,2));                                                              //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,2 ) TO #TOTAL-DEATHS
                    pnd_Total_Lumpsums.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,3));                                                            //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,3 ) TO #TOTAL-LUMPSUMS
                    pnd_Total_Finalpay.nadd(pnd_Report_Table_Pnd_Rt_Trans_Type.getValue(pnd_Inx1,pnd_Inx2,4));                                                            //Natural: ADD #RT-TRANS-TYPE ( #INX1,#INX2,4 ) TO #TOTAL-FINALPAY
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(6),"TOTAL",new ColumnSpacing(28),new ColumnSpacing(12),pnd_Total_Issues,                     //Natural: WRITE ( 01 ) / 6X 'TOTAL' 28X 12X #TOTAL-ISSUES ( EM = Z,ZZZ,ZZ9 ) 11X #TOTAL-DEATHS ( EM = Z,ZZZ,ZZ9 ) 12X #TOTAL-LUMPSUMS ( EM = Z,ZZZ,ZZ9 ) 15X #TOTAL-FINALPAY ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(11),pnd_Total_Deaths, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Total_Lumpsums, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(15),pnd_Total_Finalpay, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Get_Desc() throws Exception                                                                                                                      //Natural: #GET-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_Fund_Desc_35_Pnd_Fund_Desc.reset();                                                                                                                           //Natural: RESET #FUND-DESC #COMP-DESC
        pnd_Comp_Desc.reset();
        DbsUtil.callnat(Iaan051c.class , getCurrentProcessState(), pnd_Fund_Code_2, pnd_Fund_Desc_35, pnd_Comp_Desc, pnd_Len);                                            //Natural: CALLNAT 'IAAN051C' #FUND-CODE-2 #FUND-DESC-35 #COMP-DESC #LEN
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM   : ",Global.getPROGRAM(),new ColumnSpacing(26),"IA ADMINISTRATIVE MODE CONTROL FOR FUND ACCOUNTS ",pnd_Format_Check_Date,  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'PROGRAM   : ' *PROGRAM 26X 'IA ADMINISTRATIVE MODE CONTROL FOR FUND ACCOUNTS ' #FORMAT-CHECK-DATE ( EM = 99/99/9999 ) 5X 'PAGE :' *PAGE-NUMBER / 'RUN DATE  : ' *DATU 29X 'CONTRACTS ISSUED OR TERMINATED IN THE CURRENT MONTH' /
                        new ReportEditMask ("99/99/9999"),new ColumnSpacing(5),"PAGE :",getReports().getPageNumberDbs(0),NEWLINE,"RUN DATE  : ",Global.getDATU(),new 
                        ColumnSpacing(29),"CONTRACTS ISSUED OR TERMINATED IN THE CURRENT MONTH",NEWLINE);
                    //*      'RUN TIME  : ' *TIMX
                    //*      65X 'Check Date:' #REP-CHECK-DATE
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(49),"  ISSUES  ",new ColumnSpacing(10),"  DEATHS  ",new ColumnSpacing(10)," LUMP SUMS ",new  //Natural: WRITE ( 01 ) 49X '  ISSUES  ' 10X '  DEATHS  ' 10X ' LUMP SUMS ' 10X ' FINAL PAYMENTS'
                        ColumnSpacing(10)," FINAL PAYMENTS");
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(49),"----------",new ColumnSpacing(10),"----------",new ColumnSpacing(10),"-----------",new  //Natural: WRITE ( 01 ) 49X '----------' 10X '----------' 10X '-----------' 10X '----------------'
                        ColumnSpacing(10),"----------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  9/17
        //*  9/17
        //*  9/17
        //*  9/17
        getReports().write(0, "=",pnd_Report_File_Pnd_Rep_Ppcn,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Payee,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Trans_Date,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Check_Date,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Option_Code,NEWLINE,"=",pnd_Report_File_Pnd_Rep_First_Annt_Ind,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Second_Annt_Ind,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Mode_Ind,NEWLINE,"=",pnd_Report_File_Pnd_Rep_First_Pay_Due_Date,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Trans_Code,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Fund_Code,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Fund_Units_Before,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Fund_Units_After,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Fund_Units_Diff,NEWLINE,"=",pnd_Report_File_Pnd_Rep_Orgn_Cde,NEWLINE,"ELIN: ",Global.getERROR_LINE(),NEWLINE,"ERR: ",Global.getERROR_NR(),NEWLINE,"=",pnd_Inx1,  //Natural: WRITE '=' #REP-PPCN / '=' #REP-PAYEE / '=' #REP-TRANS-DATE / '=' #REP-CHECK-DATE / '=' #REP-OPTION-CODE / '=' #REP-FIRST-ANNT-IND / '=' #REP-SECOND-ANNT-IND / '=' #REP-MODE-IND / '=' #REP-FIRST-PAY-DUE-DATE / '=' #REP-TRANS-CODE / '=' #REP-FUND-CODE / '=' #REP-FUND-UNITS-BEFORE / '=' #REP-FUND-UNITS-AFTER / '=' #REP-FUND-UNITS-DIFF / '=' #REP-ORGN-CDE / '=' *ERROR-LINE / '=' *ERROR-NR / '=' #INX1 ( EM = 999 ) / '=' #INX2 ( EM = 999 )
            new ReportEditMask ("999"),NEWLINE,"=",pnd_Inx2, new ReportEditMask ("999"));
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
        //*  OS 082017 END
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
    }
}
