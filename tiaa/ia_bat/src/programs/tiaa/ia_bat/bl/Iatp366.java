/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:37:59 PM
**        * FROM NATURAL PROGRAM : Iatp366
************************************************************
**        * FILE NAME            : Iatp366.java
**        * CLASS NAME           : Iatp366
**        * INSTANCE NAME        : Iatp366
************************************************************
************************************************************************
* PROGRAM  : IATP366
* SYSTEM   : IAS
* TITLE    : IA POST RETIREMENT FLEXIBILITIES
* GENERATED: 08/08/1997
* FUNCTION : THIS PROGRAM PRODUCES THE IA CUMULATIVE SWITCH REPORT USING
*            A WORK FILE, CREATED IN IATP360, AS INPUT.
*    MAPS USED ARE IATM361H - HEADINGS, PAGE COUNT, DATE AND TIME
*                  IATM365I - DETAIL LINE HEADINGS
*                  IATM365D - DETAIL LINES
*                  IATM365S - SUB-TOTAL LINES  (REFER TO HISTORY)
*                  IATM365T - TOTALS PAGE
*
*    WORK FILE 1 :- ALL TRANSFER RECORDS THAT WERE WRITTEN IN PROGRAM
*                   IATP360 TO WORK FILE 4 AND THEN SORTED.
*    WORK FILE 5 :- RUN DATE
*
*
* HISTORY
* OCT  1999     ADDED LOGIC NOT TO REPORT COMPLETED REQUESTS
*                DO SCAN ON 10/99
*
* MAY  1999     ADDED LOGIC TO CALL IATN36X TO PASS UNITS TO REPORT ON
*                REPORTS DONE BY RQST-UNIT-CDE TO REPORT BY WRK-AREA
*                DO SCAN ON 5/99
*
* AUG. 27, 1997 TMCK. SUB TOTALS NOT BEING USED AT THIS TIME.
* 11/97         LEN B ADDED STATUS TO THE REPORT
*                     ADDED READ TO WORK FILE 5 TO RETRIEVE REPORT DATE
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* AUG 2017 R CARREON      PIN EXPANSION. CHANGES IN MAPS AND
*                         PIN FIELD THAT WAS LEFT OUT (#M365D-UNIQUE-ID)
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp366 extends BLNatBase
{
    // Data Areas
    private PdaIata201 pdaIata201;
    private LdaIatl365 ldaIatl365;
    private PdaIata36x pdaIata36x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I1;
    private DbsField pnd_Max_Acct;
    private DbsField pnd_Report_Date_A8;

    private DbsGroup pnd_Report_Date_A8__R_Field_1;
    private DbsField pnd_Report_Date_A8_Pnd_Report_Date_N8;
    private DbsField pnd_Bsns_Date;

    private DbsGroup pnd_Bsns_Date__R_Field_2;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_A;

    private DbsGroup pnd_Bsns_Date__R_Field_3;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Mm;
    private DbsField pnd_Bsns_Date_Pnd_Bsns_Date_Dd;
    private DbsField pnd_Currnt_Dte;

    private DbsGroup pnd_Currnt_Dte__R_Field_4;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_N;

    private DbsGroup pnd_Currnt_Dte__R_Field_5;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm;
    private DbsField pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_6;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_N;

    private DbsGroup pnd_Eff_Date__R_Field_7;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Yyyy;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Mm;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Dd;
    private DbsField pnd_Rqst_Date;

    private DbsGroup pnd_Rqst_Date__R_Field_8;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_N;

    private DbsGroup pnd_Rqst_Date__R_Field_9;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Mm;
    private DbsField pnd_Rqst_Date_Pnd_Rqst_Date_Dd;
    private DbsField pnd_Rqst_Entry_Date;

    private DbsGroup pnd_Rqst_Entry_Date__R_Field_10;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N;

    private DbsGroup pnd_Rqst_Entry_Date__R_Field_11;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm;
    private DbsField pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd;
    private DbsField pnd_Cntct_Mthd;
    private DbsField pnd_Eval_Mthd;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_I4;
    private DbsField pnd_First_Time;
    private DbsField pnd_In_Detail;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_M361h_Busns_Dte;

    private DbsGroup pnd_M361h_Busns_Dte__R_Field_12;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd;
    private DbsField pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy;
    private DbsField pnd_M361h_Page;
    private DbsField pnd_Program;
    private DbsField pnd_M365i_Cntct_Mthd;

    private DbsGroup iatm365d_Dtl;
    private DbsField iatm365d_Dtl_Pnd_M365d_Rqst_Entry_Dte_Ot;
    private DbsField iatm365d_Dtl_Pnd_M365d_Rcvd_Dte_Ot;
    private DbsField iatm365d_Dtl_Pnd_M365d_Frm_Cntrct;
    private DbsField iatm365d_Dtl_Pnd_M365d_Frm_Fnd;
    private DbsField iatm365d_Dtl_Pnd_M365d_Frm_Mthd;
    private DbsField iatm365d_Dtl_Pnd_M365d_Frm_Qunty;
    private DbsField iatm365d_Dtl_Pnd_M365d_Frm_Typ;
    private DbsField iatm365d_Dtl_Pnd_M365d_Prtcpnt_Nme;
    private DbsField iatm365d_Dtl_Pnd_M365d_To_Fnd;
    private DbsField iatm365d_Dtl_Pnd_M365d_To_Mthd;
    private DbsField iatm365d_Dtl_Pnd_M365d_To_Qunty;
    private DbsField iatm365d_Dtl_Pnd_M365d_To_Typ;
    private DbsField iatm365d_Dtl_Pnd_M365d_Unique_Id;
    private DbsField iatm365d_Dtl_Pnd_M365d_Status;
    private DbsField pnd_M365t_Totl_Rqst_Amt;
    private DbsField pnd_M365t_Switch_Amt;
    private DbsField pnd_M365t_Anul_To_Mnth_Amt;
    private DbsField pnd_M365t_Mnth_To_Anul_Amt;
    private DbsField pnd_M365t_Ttl_Prtcpnts_Amt;
    private DbsField pnd_M365t_Cntct_At_Amt;
    private DbsField pnd_M365t_Cntct_Cs_Amt;
    private DbsField pnd_M365t_Cntct_Fx_Amt;
    private DbsField pnd_M365t_Cntct_Ig_Amt;
    private DbsField pnd_M365t_Cntct_In_Amt;
    private DbsField pnd_M365t_Cntct_Ml_Amt;
    private DbsField pnd_M365t_Cntct_Vr_Amt;
    private DbsField pnd_M365t_Cntct_Vs_Amt;
    private DbsField pnd_M365t_Stat_Af_Amt;
    private DbsField pnd_M365t_Stat_De_Amt;
    private DbsField pnd_M365t_Ttl_Rqst_Pcnt_Amt;
    private DbsField pnd_M365t_Ttl_Rqsts_Dlrs_Amt;
    private DbsField pnd_M365t_Ttl_Rqsts_Unts_Amt;
    private DbsField tbl_Cntct_Mthd_Cde;
    private DbsField tbl_Cntct_Mthd_Litrl;
    private DbsField pnd_Cmpr_Pin;
    private DbsField pnd_Page_Cntrl;

    private DbsGroup msg_Info_Sub;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg;

    private DbsGroup msg_Info_Sub__R_Field_13;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Display;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data;

    private DbsGroup msg_Info_Sub__R_Field_14;

    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data_Char;
    private DbsField msg_Info_Sub_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIata201 = new PdaIata201(localVariables);
        ldaIatl365 = new LdaIatl365();
        registerRecord(ldaIatl365);
        pdaIata36x = new PdaIata36x(localVariables);

        // Local Variables
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.INTEGER, 2);
        pnd_Max_Acct = localVariables.newFieldInRecord("pnd_Max_Acct", "#MAX-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Report_Date_A8 = localVariables.newFieldInRecord("pnd_Report_Date_A8", "#REPORT-DATE-A8", FieldType.STRING, 8);

        pnd_Report_Date_A8__R_Field_1 = localVariables.newGroupInRecord("pnd_Report_Date_A8__R_Field_1", "REDEFINE", pnd_Report_Date_A8);
        pnd_Report_Date_A8_Pnd_Report_Date_N8 = pnd_Report_Date_A8__R_Field_1.newFieldInGroup("pnd_Report_Date_A8_Pnd_Report_Date_N8", "#REPORT-DATE-N8", 
            FieldType.NUMERIC, 8);
        pnd_Bsns_Date = localVariables.newFieldInRecord("pnd_Bsns_Date", "#BSNS-DATE", FieldType.NUMERIC, 8);

        pnd_Bsns_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Bsns_Date__R_Field_2", "REDEFINE", pnd_Bsns_Date);
        pnd_Bsns_Date_Pnd_Bsns_Date_A = pnd_Bsns_Date__R_Field_2.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_A", "#BSNS-DATE-A", FieldType.STRING, 8);

        pnd_Bsns_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Bsns_Date__R_Field_3", "REDEFINE", pnd_Bsns_Date);
        pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Yyyy", "#BSNS-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Bsns_Date_Pnd_Bsns_Date_Mm = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Mm", "#BSNS-DATE-MM", FieldType.STRING, 
            2);
        pnd_Bsns_Date_Pnd_Bsns_Date_Dd = pnd_Bsns_Date__R_Field_3.newFieldInGroup("pnd_Bsns_Date_Pnd_Bsns_Date_Dd", "#BSNS-DATE-DD", FieldType.STRING, 
            2);
        pnd_Currnt_Dte = localVariables.newFieldInRecord("pnd_Currnt_Dte", "#CURRNT-DTE", FieldType.STRING, 8);

        pnd_Currnt_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_4", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_N = pnd_Currnt_Dte__R_Field_4.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_N", "#CURRNT-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Currnt_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Currnt_Dte__R_Field_5", "REDEFINE", pnd_Currnt_Dte);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy = pnd_Currnt_Dte__R_Field_5.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy", "#CURRNT-DTE-CY", FieldType.STRING, 
            4);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm = pnd_Currnt_Dte__R_Field_5.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm", "#CURRNT-DTE-MM", FieldType.STRING, 
            2);
        pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd = pnd_Currnt_Dte__R_Field_5.newFieldInGroup("pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd", "#CURRNT-DTE-DD", FieldType.STRING, 
            2);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_6", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_N = pnd_Eff_Date__R_Field_6.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_N", "#EFF-DATE-N", FieldType.NUMERIC, 8);

        pnd_Eff_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_7", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Yyyy = pnd_Eff_Date__R_Field_7.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Yyyy", "#EFF-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Eff_Date_Pnd_Eff_Date_Mm = pnd_Eff_Date__R_Field_7.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Mm", "#EFF-DATE-MM", FieldType.STRING, 2);
        pnd_Eff_Date_Pnd_Eff_Date_Dd = pnd_Eff_Date__R_Field_7.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Dd", "#EFF-DATE-DD", FieldType.STRING, 2);
        pnd_Rqst_Date = localVariables.newFieldInRecord("pnd_Rqst_Date", "#RQST-DATE", FieldType.STRING, 8);

        pnd_Rqst_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_8", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_N = pnd_Rqst_Date__R_Field_8.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_N", "#RQST-DATE-N", FieldType.NUMERIC, 8);

        pnd_Rqst_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Rqst_Date__R_Field_9", "REDEFINE", pnd_Rqst_Date);
        pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy = pnd_Rqst_Date__R_Field_9.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy", "#RQST-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Rqst_Date_Pnd_Rqst_Date_Mm = pnd_Rqst_Date__R_Field_9.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Mm", "#RQST-DATE-MM", FieldType.STRING, 
            2);
        pnd_Rqst_Date_Pnd_Rqst_Date_Dd = pnd_Rqst_Date__R_Field_9.newFieldInGroup("pnd_Rqst_Date_Pnd_Rqst_Date_Dd", "#RQST-DATE-DD", FieldType.STRING, 
            2);
        pnd_Rqst_Entry_Date = localVariables.newFieldInRecord("pnd_Rqst_Entry_Date", "#RQST-ENTRY-DATE", FieldType.STRING, 8);

        pnd_Rqst_Entry_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_Rqst_Entry_Date__R_Field_10", "REDEFINE", pnd_Rqst_Entry_Date);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N = pnd_Rqst_Entry_Date__R_Field_10.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_N", "#RQST-ENTRY-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Rqst_Entry_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Rqst_Entry_Date__R_Field_11", "REDEFINE", pnd_Rqst_Entry_Date);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy = pnd_Rqst_Entry_Date__R_Field_11.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy", 
            "#RQST-ENTRY-DATE-YYYY", FieldType.STRING, 4);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm = pnd_Rqst_Entry_Date__R_Field_11.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm", "#RQST-ENTRY-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd = pnd_Rqst_Entry_Date__R_Field_11.newFieldInGroup("pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd", "#RQST-ENTRY-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Cntct_Mthd = localVariables.newFieldInRecord("pnd_Cntct_Mthd", "#CNTCT-MTHD", FieldType.STRING, 1);
        pnd_Eval_Mthd = localVariables.newFieldInRecord("pnd_Eval_Mthd", "#EVAL-MTHD", FieldType.STRING, 1);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.PACKED_DECIMAL, 3);
        pnd_I4 = localVariables.newFieldInRecord("pnd_I4", "#I4", FieldType.PACKED_DECIMAL, 3);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_In_Detail = localVariables.newFieldInRecord("pnd_In_Detail", "#IN-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 58);
        pnd_M361h_Busns_Dte = localVariables.newFieldInRecord("pnd_M361h_Busns_Dte", "#M361H-BUSNS-DTE", FieldType.NUMERIC, 8);

        pnd_M361h_Busns_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_M361h_Busns_Dte__R_Field_12", "REDEFINE", pnd_M361h_Busns_Dte);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm = pnd_M361h_Busns_Dte__R_Field_12.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm", "#M361H-BUSNS-DTE-MM", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd = pnd_M361h_Busns_Dte__R_Field_12.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd", "#M361H-BUSNS-DTE-DD", 
            FieldType.STRING, 2);
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy = pnd_M361h_Busns_Dte__R_Field_12.newFieldInGroup("pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy", "#M361H-BUSNS-DTE-CY", 
            FieldType.STRING, 4);
        pnd_M361h_Page = localVariables.newFieldInRecord("pnd_M361h_Page", "#M361H-PAGE", FieldType.NUMERIC, 5);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_M365i_Cntct_Mthd = localVariables.newFieldInRecord("pnd_M365i_Cntct_Mthd", "#M365I-CNTCT-MTHD", FieldType.STRING, 20);

        iatm365d_Dtl = localVariables.newGroupInRecord("iatm365d_Dtl", "IATM365D-DTL");
        iatm365d_Dtl_Pnd_M365d_Rqst_Entry_Dte_Ot = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Rqst_Entry_Dte_Ot", "#M365D-RQST-ENTRY-DTE-OT", 
            FieldType.STRING, 10);
        iatm365d_Dtl_Pnd_M365d_Rcvd_Dte_Ot = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Rcvd_Dte_Ot", "#M365D-RCVD-DTE-OT", FieldType.STRING, 
            10);
        iatm365d_Dtl_Pnd_M365d_Frm_Cntrct = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Frm_Cntrct", "#M365D-FRM-CNTRCT", FieldType.STRING, 10);
        iatm365d_Dtl_Pnd_M365d_Frm_Fnd = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Frm_Fnd", "#M365D-FRM-FND", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_Frm_Mthd = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Frm_Mthd", "#M365D-FRM-MTHD", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_Frm_Qunty = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Frm_Qunty", "#M365D-FRM-QUNTY", FieldType.NUMERIC, 11, 
            2);
        iatm365d_Dtl_Pnd_M365d_Frm_Typ = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Frm_Typ", "#M365D-FRM-TYP", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_Prtcpnt_Nme = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Prtcpnt_Nme", "#M365D-PRTCPNT-NME", FieldType.STRING, 
            30);
        iatm365d_Dtl_Pnd_M365d_To_Fnd = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_To_Fnd", "#M365D-TO-FND", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_To_Mthd = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_To_Mthd", "#M365D-TO-MTHD", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_To_Qunty = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_To_Qunty", "#M365D-TO-QUNTY", FieldType.NUMERIC, 11, 2);
        iatm365d_Dtl_Pnd_M365d_To_Typ = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_To_Typ", "#M365D-TO-TYP", FieldType.STRING, 1);
        iatm365d_Dtl_Pnd_M365d_Unique_Id = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Unique_Id", "#M365D-UNIQUE-ID", FieldType.STRING, 12);
        iatm365d_Dtl_Pnd_M365d_Status = iatm365d_Dtl.newFieldInGroup("iatm365d_Dtl_Pnd_M365d_Status", "#M365D-STATUS", FieldType.STRING, 20);
        pnd_M365t_Totl_Rqst_Amt = localVariables.newFieldInRecord("pnd_M365t_Totl_Rqst_Amt", "#M365T-TOTL-RQST-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Switch_Amt = localVariables.newFieldInRecord("pnd_M365t_Switch_Amt", "#M365T-SWITCH-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Anul_To_Mnth_Amt = localVariables.newFieldInRecord("pnd_M365t_Anul_To_Mnth_Amt", "#M365T-ANUL-TO-MNTH-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Mnth_To_Anul_Amt = localVariables.newFieldInRecord("pnd_M365t_Mnth_To_Anul_Amt", "#M365T-MNTH-TO-ANUL-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Prtcpnts_Amt = localVariables.newFieldInRecord("pnd_M365t_Ttl_Prtcpnts_Amt", "#M365T-TTL-PRTCPNTS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_At_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_At_Amt", "#M365T-CNTCT-AT-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Cs_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Cs_Amt", "#M365T-CNTCT-CS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Fx_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Fx_Amt", "#M365T-CNTCT-FX-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Ig_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Ig_Amt", "#M365T-CNTCT-IG-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_In_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_In_Amt", "#M365T-CNTCT-IN-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Ml_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Ml_Amt", "#M365T-CNTCT-ML-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Vr_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Vr_Amt", "#M365T-CNTCT-VR-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Cntct_Vs_Amt = localVariables.newFieldInRecord("pnd_M365t_Cntct_Vs_Amt", "#M365T-CNTCT-VS-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Stat_Af_Amt = localVariables.newFieldInRecord("pnd_M365t_Stat_Af_Amt", "#M365T-STAT-AF-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Stat_De_Amt = localVariables.newFieldInRecord("pnd_M365t_Stat_De_Amt", "#M365T-STAT-DE-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Rqst_Pcnt_Amt = localVariables.newFieldInRecord("pnd_M365t_Ttl_Rqst_Pcnt_Amt", "#M365T-TTL-RQST-PCNT-AMT", FieldType.NUMERIC, 9);
        pnd_M365t_Ttl_Rqsts_Dlrs_Amt = localVariables.newFieldInRecord("pnd_M365t_Ttl_Rqsts_Dlrs_Amt", "#M365T-TTL-RQSTS-DLRS-AMT", FieldType.NUMERIC, 
            9);
        pnd_M365t_Ttl_Rqsts_Unts_Amt = localVariables.newFieldInRecord("pnd_M365t_Ttl_Rqsts_Unts_Amt", "#M365T-TTL-RQSTS-UNTS-AMT", FieldType.NUMERIC, 
            9);
        tbl_Cntct_Mthd_Cde = localVariables.newFieldArrayInRecord("tbl_Cntct_Mthd_Cde", "TBL-CNTCT-MTHD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        tbl_Cntct_Mthd_Litrl = localVariables.newFieldArrayInRecord("tbl_Cntct_Mthd_Litrl", "TBL-CNTCT-MTHD-LITRL", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Cmpr_Pin = localVariables.newFieldInRecord("pnd_Cmpr_Pin", "#CMPR-PIN", FieldType.NUMERIC, 12);
        pnd_Page_Cntrl = localVariables.newFieldInRecord("pnd_Page_Cntrl", "#PAGE-CNTRL", FieldType.PACKED_DECIMAL, 3);

        msg_Info_Sub = localVariables.newGroupInRecord("msg_Info_Sub", "MSG-INFO-SUB");
        msg_Info_Sub_Pnd_Pnd_Msg = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);

        msg_Info_Sub__R_Field_13 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_13", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg);
        msg_Info_Sub_Pnd_Pnd_Msg_Display = msg_Info_Sub__R_Field_13.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Display", "##MSG-DISPLAY", FieldType.STRING, 
            78);
        msg_Info_Sub_Pnd_Pnd_Msg_Nr = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Sub_Pnd_Pnd_Msg_Data = msg_Info_Sub.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));

        msg_Info_Sub__R_Field_14 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_14", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg_Data);

        msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct = msg_Info_Sub__R_Field_14.newGroupArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1, 3));
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Char = msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1, 32));
        msg_Info_Sub_Pnd_Pnd_Return_Code = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Sub_Pnd_Pnd_Error_Field = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index1 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index2 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index3 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIatl365.initializeValues();

        localVariables.reset();
        pnd_Max_Acct.setInitialValue(20);
        pnd_First_Time.setInitialValue(true);
        pnd_In_Detail.setInitialValue(true);
        pnd_Header1.setInitialValue("            POST SETTLEMENT FLEXIBILITIES ");
        pnd_Header2.setInitialValue("       CUMULATIVE - SWITCH REQUEST REPORT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iatp366() throws Exception
    {
        super("Iatp366");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* ======================================================================
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: ASSIGN #PROGRAM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM SET-REPORT-DATE
        sub_Set_Report_Date();
        if (condition(Global.isEscape())) {return;}
        //*  ADDED 5/99 GET WRK-AREA IDS
        DbsUtil.callnat(Iatn36x.class , getCurrentProcessState(), pdaIata36x.getIata36x());                                                                               //Natural: CALLNAT 'IATN36X' IATA36X
        if (condition(Global.isEscape())) return;
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 1 )
        //* *>> ADDED FOLLOWING LINES  5/99
        //* *
        R1:                                                                                                                                                               //Natural: REPEAT
        while (condition(whileTrue))
        {
            pnd_I1.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I1
            if (condition(pnd_I1.greater(pdaIata36x.getIata36x_Iata36x_Nbr_Of_Unit_Ids())))                                                                               //Natural: IF #I1 GT IATA36X-NBR-OF-UNIT-IDS
            {
                if (true) break R1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( R1. )
            }                                                                                                                                                             //Natural: END-IF
            //* *>> END OF ADD 5/99
            //*  PROCESS THE RECORDS IN THE WORK FILE
            //*  READ CUMULATIVE SWITCH RECORDS
            boolean endOfDataReadwork01 = true;                                                                                                                           //Natural: READ WORK FILE 1 IAT-SWITCH-RPT
            boolean firstReadwork01 = true;
            READWORK01:
            while (condition(getWorkFiles().read(1, ldaIatl365.getIat_Switch_Rpt())))
            {
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom()))
                            break;
                        else if (condition(Global.isEscapeBottomImmediate()))
                        {
                            endOfDataReadwork01 = false;
                            break;
                        }
                        else if (condition(Global.isEscapeTop()))
                        continue;
                        else if (condition())
                        return;
                    }
                }
                //* *
                //* *** ADDED FOLLOWING IF TO BYPASS REPORTING COMPLETED REQUESTS 10/99
                pnd_Eff_Date.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED RQST-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DATE
                //*  M = COMPLETE DO REPORT THESE
                if (condition(pnd_Eff_Date_Pnd_Eff_Date_N.less(pnd_Currnt_Dte_Pnd_Currnt_Dte_N) && DbsUtil.maskMatches(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde(),       //Natural: IF #EFF-DATE-N LT #CURRNT-DTE-N AND XFR-STTS-CDE = MASK ( 'M' )
                    "'M'")))
                {
                    //*  AFTER CURRENT DATE PAST EFFECT-DTE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //* * END OF ADD  10/99
                //* *
                //* *
                //* * ADDED FOLLOWING LINES TO SELECT BY UNIT-IDS   5/99
                if (condition(DbsUtil.maskMatches(pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1),"'ALL'")))                                                    //Natural: IF IATA36X-UNIT-CDE ( #I1 ) = MASK ( 'ALL' )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(!pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1).getSubstring(1,pdaIata36x.getIata36x_Iata36x_Lngth().getInt()).equals(ldaIatl365.getIat_Switch_Rpt_Rqst_Unit_Cde().getSubstring(1, //Natural: IF SUBSTR ( IATA36X-UNIT-CDE ( #I1 ) ,1,IATA36X-LNGTH ) NE SUBSTR ( RQST-UNIT-CDE,1,IATA36X-LNGTH )
                        pdaIata36x.getIata36x_Iata36x_Lngth().getInt()))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* * END OF ADD  5/99
                pnd_M365t_Totl_Rqst_Amt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #M365T-TOTL-RQST-AMT
                if (condition(pnd_First_Time.getBoolean()))                                                                                                               //Natural: IF #FIRST-TIME
                {
                    pnd_First_Time.reset();                                                                                                                               //Natural: RESET #FIRST-TIME
                    pnd_Cmpr_Pin.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                                                   //Natural: ASSIGN #CMPR-PIN := IA-UNIQUE-ID
                    pnd_M365t_Ttl_Prtcpnts_Amt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #M365T-TTL-PRTCPNTS-AMT
                    //*  GET CONTACT METHOD FROM TABLE FILE
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MODE
                    sub_Get_Contact_Mode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(msg_Info_Sub_Pnd_Pnd_Return_Code.equals(" ")))                                                                                          //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
                    {
                        pnd_M365i_Cntct_Mthd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaIata201.getIata201_Pnd_Long_Desc()));                            //Natural: COMPRESS #LONG-DESC INTO #M365I-CNTCT-MTHD LEAVING NO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_M365i_Cntct_Mthd.setValue("CNTCT MTHD NOT ON FILE");                                                                                          //Natural: ASSIGN #M365I-CNTCT-MTHD := 'CNTCT MTHD NOT ON FILE'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FIRST TIME READ OF WORK FILE
                }                                                                                                                                                         //Natural: END-IF
                //*                                                                                                                                                       //Natural: AT BREAK OF IAT-SWITCH-RPT.RQST-CNTCT-MDE
                //*  PIN
                if (condition(pnd_Cmpr_Pin.notEquals(ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id())))                                                                       //Natural: IF #CMPR-PIN NE IA-UNIQUE-ID
                {
                    pnd_M365t_Ttl_Prtcpnts_Amt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #M365T-TTL-PRTCPNTS-AMT
                    pnd_Cmpr_Pin.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                                                   //Natural: ASSIGN #CMPR-PIN := IA-UNIQUE-ID
                }                                                                                                                                                         //Natural: END-IF
                //*  PRINT DETAIL LINES
                //*  INSURE THE FULL REQUEST WILL FIT ON THE CURRENT PAGE
                if (condition(pnd_Page_Cntrl.greater(40)))                                                                                                                //Natural: IF #PAGE-CNTRL > 40
                {
                    pnd_Page_Cntrl.reset();                                                                                                                               //Natural: RESET #PAGE-CNTRL
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  SET UP AND PRINT THE FIRST LINE OF THE REQUEST
                iatm365d_Dtl.reset();                                                                                                                                     //Natural: RESET IATM365D-DTL
                pnd_I2.setValue(1);                                                                                                                                       //Natural: ASSIGN #I2 = 1
                                                                                                                                                                          //Natural: PERFORM GET-STATUS-DESC
                sub_Get_Status_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                iatm365d_Dtl_Pnd_M365d_Unique_Id.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Unique_Id());                                                                   //Natural: ASSIGN #M365D-UNIQUE-ID := IA-UNIQUE-ID
                iatm365d_Dtl_Pnd_M365d_Frm_Cntrct.setValue(ldaIatl365.getIat_Switch_Rpt_Ia_Frm_Cntrct());                                                                 //Natural: ASSIGN #M365D-FRM-CNTRCT := IA-FRM-CNTRCT
                //*  RECEIVED DATE AND ENTRY DATE
                pnd_Rqst_Date.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED RQST-RCVD-DTE ( EM = YYYYMMDD ) TO #RQST-DATE
                iatm365d_Dtl_Pnd_M365d_Rcvd_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rqst_Date_Pnd_Rqst_Date_Mm, "/", pnd_Rqst_Date_Pnd_Rqst_Date_Dd,  //Natural: COMPRESS #RQST-DATE-MM '/' #RQST-DATE-DD '/' #RQST-DATE-YYYY INTO #M365D-RCVD-DTE-OT LEAVING NO SPACE
                    "/", pnd_Rqst_Date_Pnd_Rqst_Date_Yyyy));
                pnd_Rqst_Entry_Date.setValueEdited(ldaIatl365.getIat_Switch_Rpt_Rqst_Entry_Dte(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED RQST-ENTRY-DTE ( EM = YYYYMMDD ) TO #RQST-ENTRY-DATE
                iatm365d_Dtl_Pnd_M365d_Rqst_Entry_Dte_Ot.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Mm,             //Natural: COMPRESS #RQST-ENTRY-DATE-MM '/' #RQST-ENTRY-DATE-DD '/' #RQST-ENTRY-DATE-YYYY INTO #M365D-RQST-ENTRY-DTE-OT LEAVING NO
                    "/", pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Dd, "/", pnd_Rqst_Entry_Date_Pnd_Rqst_Entry_Date_Yyyy));
                iatm365d_Dtl_Pnd_M365d_Frm_Fnd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2));                                                //Natural: ASSIGN #M365D-FRM-FND := XFR-FRM-ACCT-CDE ( #I2 )
                iatm365d_Dtl_Pnd_M365d_Frm_Qunty.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(pnd_I2));                                                   //Natural: ASSIGN #M365D-FRM-QUNTY := XFR-FRM-QTY ( #I2 )
                iatm365d_Dtl_Pnd_M365d_Frm_Typ.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(pnd_I2));                                                     //Natural: ASSIGN #M365D-FRM-TYP := XFR-FRM-TYP ( #I2 )
                iatm365d_Dtl_Pnd_M365d_Prtcpnt_Nme.setValue(ldaIatl365.getIat_Switch_Rpt_Prtcpnt_Nme());                                                                  //Natural: ASSIGN #M365D-PRTCPNT-NME := IAT-SWITCH-RPT.PRTCPNT-NME
                iatm365d_Dtl_Pnd_M365d_Frm_Mthd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(pnd_I2));                                               //Natural: ASSIGN #M365D-FRM-MTHD := XFR-FRM-UNIT-TYP ( #I2 )
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm365d.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM365D'
                pnd_Page_Cntrl.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-CNTRL
                                                                                                                                                                          //Natural: PERFORM ADD-TO-TOTALS
                sub_Add_To_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SET UP AND PRINT THE REMAINING LINE(S) OF THE REQUEST
                iatm365d_Dtl.reset();                                                                                                                                     //Natural: RESET IATM365D-DTL
                FOR01:                                                                                                                                                    //Natural: FOR #I2 2 TO #MAX-ACCT
                for (pnd_I2.setValue(2); condition(pnd_I2.lessOrEqual(pnd_Max_Acct)); pnd_I2.nadd(1))
                {
                    if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2).equals(" ")))                                                          //Natural: IF XFR-FRM-ACCT-CDE ( #I2 ) = ' '
                    {
                        pnd_Max_Acct.setValue(21);                                                                                                                        //Natural: ASSIGN #MAX-ACCT := 21
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    iatm365d_Dtl_Pnd_M365d_Frm_Fnd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Acct_Cde().getValue(pnd_I2));                                            //Natural: ASSIGN #M365D-FRM-FND := XFR-FRM-ACCT-CDE ( #I2 )
                    iatm365d_Dtl_Pnd_M365d_Frm_Qunty.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Qty().getValue(pnd_I2));                                               //Natural: ASSIGN #M365D-FRM-QUNTY := XFR-FRM-QTY ( #I2 )
                    iatm365d_Dtl_Pnd_M365d_Frm_Typ.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue(pnd_I2));                                                 //Natural: ASSIGN #M365D-FRM-TYP := XFR-FRM-TYP ( #I2 )
                    iatm365d_Dtl_Pnd_M365d_Frm_Mthd.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue(pnd_I2));                                           //Natural: ASSIGN #M365D-FRM-MTHD := XFR-FRM-UNIT-TYP ( #I2 )
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm365d.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM365D'
                    pnd_Page_Cntrl.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-CNTRL
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-WORK
            READWORK01_Exit:
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01(endOfDataReadwork01);
            }
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM #WRITE-TOTAL-PAGE
            sub_Pnd_Write_Total_Page();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   ADDED FOLLOWING RESET ALL ACCUMULATORS FOR NEXT UNIT REPORT 5/99
            //* *
            //*  ADDED 5/99
            //*  ADDED 5/99
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL #M361H-PAGE
            pnd_M361h_Page.reset();
            //*  ADDED 5/99
            //*  ADDED 5/99
            //*  ADDED 5/99
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_In_Detail.setValue(true);                                                                                                                                 //Natural: ASSIGN #IN-DETAIL := TRUE
            pnd_First_Time.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-TIME := TRUE
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2 #I3 #I4 #M365T-TOTL-RQST-AMT #M365T-SWITCH-AMT #M365T-ANUL-TO-MNTH-AMT #M365T-MNTH-TO-ANUL-AMT #M365T-TTL-PRTCPNTS-AMT #M365T-CNTCT-AT-AMT #M365T-CNTCT-CS-AMT #M365T-CNTCT-FX-AMT #M365T-CNTCT-IG-AMT #M365T-CNTCT-IN-AMT #M365T-CNTCT-ML-AMT #M365T-CNTCT-VR-AMT #M365T-CNTCT-VS-AMT #M365T-STAT-AF-AMT #M365T-STAT-DE-AMT #M365T-TTL-RQST-PCNT-AMT #M365T-TTL-RQSTS-DLRS-AMT #M365T-TTL-RQSTS-UNTS-AMT
            pnd_I3.reset();
            pnd_I4.reset();
            pnd_M365t_Totl_Rqst_Amt.reset();
            pnd_M365t_Switch_Amt.reset();
            pnd_M365t_Anul_To_Mnth_Amt.reset();
            pnd_M365t_Mnth_To_Anul_Amt.reset();
            pnd_M365t_Ttl_Prtcpnts_Amt.reset();
            pnd_M365t_Cntct_At_Amt.reset();
            pnd_M365t_Cntct_Cs_Amt.reset();
            pnd_M365t_Cntct_Fx_Amt.reset();
            pnd_M365t_Cntct_Ig_Amt.reset();
            pnd_M365t_Cntct_In_Amt.reset();
            pnd_M365t_Cntct_Ml_Amt.reset();
            pnd_M365t_Cntct_Vr_Amt.reset();
            pnd_M365t_Cntct_Vs_Amt.reset();
            pnd_M365t_Stat_Af_Amt.reset();
            pnd_M365t_Stat_De_Amt.reset();
            pnd_M365t_Ttl_Rqst_Pcnt_Amt.reset();
            pnd_M365t_Ttl_Rqsts_Dlrs_Amt.reset();
            pnd_M365t_Ttl_Rqsts_Unts_Amt.reset();
            //*  ADDED 5/99  TO REPORT ALL WRK-AREAS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  ============================================================
        //*                     PROGRAM SUBROUTINES
        //*  ============================================================
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-REPORT-DATE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTACT-MODE
        //* **********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATUS-DESC
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-TOTAL-PAGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #WRITE-SUB-HEADING-1
        //* ***********************************************************************
        //*  IATP366
    }
    private void sub_Set_Report_Date() throws Exception                                                                                                                   //Natural: SET-REPORT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  INITIAL DETAIL SETUP
        getWorkFiles().read(5, pnd_Report_Date_A8);                                                                                                                       //Natural: READ WORK FILE 5 ONCE #REPORT-DATE-A8
        if (condition(pnd_Report_Date_A8.equals("99999999")))                                                                                                             //Natural: IF #REPORT-DATE-A8 = '99999999'
        {
            pnd_Currnt_Dte.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #CURRNT-DTE := *DATN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CHECK DATE FORMAT
            if (condition(DbsUtil.maskMatches(pnd_Report_Date_A8,"YYYYMMDD")))                                                                                            //Natural: IF #REPORT-DATE-A8 EQ MASK ( YYYYMMDD )
            {
                pnd_Currnt_Dte.setValue(pnd_Report_Date_A8_Pnd_Report_Date_N8);                                                                                           //Natural: ASSIGN #CURRNT-DTE := #REPORT-DATE-N8
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "* ================ ERROR ======================= *");                                                                              //Natural: WRITE '* ================ ERROR ======================= *'
                if (Global.isEscape()) return;
                getReports().write(0, "* The Requested Date for the Report is           *");                                                                              //Natural: WRITE '* The Requested Date for the Report is           *'
                if (Global.isEscape()) return;
                getReports().write(0, "* not in the format YYYYMMDD.                    *");                                                                              //Natural: WRITE '* not in the format YYYYMMDD.                    *'
                if (Global.isEscape()) return;
                getReports().write(0, "* Request Date = ",pnd_Report_Date_A8);                                                                                            //Natural: WRITE '* Request Date = ' #REPORT-DATE-A8
                if (Global.isEscape()) return;
                getReports().write(0, "*                                                *");                                                                              //Natural: WRITE '*                                                *'
                if (Global.isEscape()) return;
                getReports().write(0, "**************************************************");                                                                              //Natural: WRITE '**************************************************'
                if (Global.isEscape()) return;
                DbsUtil.terminate();  if (true) return;                                                                                                                   //Natural: TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Cy.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Cy);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-CY := #CURRNT-DTE-CY
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Mm.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Mm);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-MM := #CURRNT-DTE-MM
        pnd_M361h_Busns_Dte_Pnd_M361h_Busns_Dte_Dd.setValue(pnd_Currnt_Dte_Pnd_Currnt_Dte_Dd);                                                                            //Natural: ASSIGN #M361H-BUSNS-DTE-DD := #CURRNT-DTE-DD
        //*  SET-REPORT-DATE
    }
    private void sub_Get_Contact_Mode() throws Exception                                                                                                                  //Natural: GET-CONTACT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata201.getIata201_Pnd_Naz_Table_Key().setValue(ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde());                                                                //Natural: ASSIGN #NAZ-TABLE-KEY := IAT-SWITCH-RPT.RQST-CNTCT-MDE
        pdaIata201.getIata201_Pnd_Table_Code().setValue("CMDE");                                                                                                          //Natural: ASSIGN #TABLE-CODE := 'CMDE'
        DbsUtil.callnat(Iatn201.class , getCurrentProcessState(), msg_Info_Sub, pdaIata201.getIata201());                                                                 //Natural: CALLNAT 'IATN201' MSG-INFO-SUB IATA201
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Status_Desc() throws Exception                                                                                                                   //Natural: GET-STATUS-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pdaIata201.getIata201_Pnd_Naz_Table_Key().setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                  //Natural: ASSIGN #NAZ-TABLE-KEY := IAT-SWITCH-RPT.XFR-STTS-CDE
        pdaIata201.getIata201_Pnd_Table_Code().setValue("PSC");                                                                                                           //Natural: ASSIGN #TABLE-CODE := 'PSC'
        DbsUtil.callnat(Iatn201.class , getCurrentProcessState(), msg_Info_Sub, pdaIata201.getIata201());                                                                 //Natural: CALLNAT 'IATN201' MSG-INFO-SUB IATA201
        if (condition(Global.isEscape())) return;
        if (condition(msg_Info_Sub_Pnd_Pnd_Return_Code.equals(" ")))                                                                                                      //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
        {
            iatm365d_Dtl_Pnd_M365d_Status.setValue(pdaIata201.getIata201_Pnd_Short_Desc());                                                                               //Natural: ASSIGN #M365D-STATUS := #SHORT-DESC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde().notEquals(" ")))                                                                                    //Natural: IF IAT-SWITCH-RPT.XFR-STTS-CDE NE ' '
            {
                iatm365d_Dtl_Pnd_M365d_Status.setValue(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde());                                                                      //Natural: ASSIGN #M365D-STATUS := IAT-SWITCH-RPT.XFR-STTS-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iatm365d_Dtl_Pnd_M365d_Status.setValue("NOT ON FILE");                                                                                                    //Natural: ASSIGN #M365D-STATUS := 'NOT ON FILE'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Add_To_Totals() throws Exception                                                                                                                     //Natural: ADD-TO-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  STATUS
        if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde().getSubstring(1,1).equals("F")))                                                                         //Natural: IF SUBSTRING ( XFR-STTS-CDE,1,1 ) = 'F'
        {
            pnd_M365t_Stat_Af_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M365T-STAT-AF-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIatl365.getIat_Switch_Rpt_Xfr_Stts_Cde().getSubstring(1,1).equals("N")))                                                                         //Natural: IF SUBSTRING ( XFR-STTS-CDE,1,1 ) = 'N'
        {
            pnd_M365t_Stat_De_Amt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M365T-STAT-DE-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  REVALUATION METHODS
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue("*")), new ExamineSearch("A"), new ExamineGivingNumber(pnd_I3));       //Natural: EXAMINE XFR-FRM-UNIT-TYP ( * ) FOR 'A' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M365t_Anul_To_Mnth_Amt.nadd(pnd_I3);                                                                                                                      //Natural: ADD #I3 TO #M365T-ANUL-TO-MNTH-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Unit_Typ().getValue("*")), new ExamineSearch("M"), new ExamineGivingNumber(pnd_I3));       //Natural: EXAMINE XFR-FRM-UNIT-TYP ( * ) FOR 'M' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M365t_Mnth_To_Anul_Amt.nadd(pnd_I3);                                                                                                                      //Natural: ADD #I3 TO #M365T-MNTH-TO-ANUL-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTACT METHODS
        short decideConditionsMet581 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF RQST-CNTCT-MDE;//Natural: VALUE 'A'
        if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("A"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_At_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-AT-AMT
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("C"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Cs_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-CS-AMT
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("F"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Fx_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-FX-AMT
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("I"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Ig_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-IG-AMT
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("M"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Ml_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-ML-AMT
        }                                                                                                                                                                 //Natural: VALUE 'V'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("V"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Vr_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-VR-AMT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().equals("X"))))
        {
            decideConditionsMet581++;
            pnd_M365t_Cntct_Vs_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M365T-CNTCT-VS-AMT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  TYPES OF ACCOUNT TRANSFERS
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3 #I4
        pnd_I4.reset();
        //*  REQUESTS BY PERCENT, DOLLARS OR UNITS
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("P"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'P' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M365t_Ttl_Rqst_Pcnt_Amt.nadd(pnd_I3);                                                                                                                     //Natural: ADD #I3 TO #M365T-TTL-RQST-PCNT-AMT
            pnd_M365t_Switch_Amt.nadd(pnd_I3);                                                                                                                            //Natural: ADD #I3 TO #M365T-SWITCH-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("D"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'D' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M365t_Ttl_Rqsts_Dlrs_Amt.nadd(pnd_I3);                                                                                                                    //Natural: ADD #I3 TO #M365T-TTL-RQSTS-DLRS-AMT
            pnd_M365t_Switch_Amt.nadd(pnd_I3);                                                                                                                            //Natural: ADD #I3 TO #M365T-SWITCH-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I3.reset();                                                                                                                                                   //Natural: RESET #I3
        DbsUtil.examine(new ExamineSource(ldaIatl365.getIat_Switch_Rpt_Xfr_Frm_Typ().getValue("*")), new ExamineSearch("U"), new ExamineGivingNumber(pnd_I3));            //Natural: EXAMINE XFR-FRM-TYP ( * ) FOR 'U' GIVING NUMBER IN #I3
        if (condition(pnd_I3.greater(getZero())))                                                                                                                         //Natural: IF #I3 > 0
        {
            pnd_M365t_Ttl_Rqsts_Unts_Amt.nadd(pnd_I3);                                                                                                                    //Natural: ADD #I3 TO #M365T-TTL-RQSTS-UNTS-AMT
            pnd_M365t_Switch_Amt.nadd(pnd_I3);                                                                                                                            //Natural: ADD #I3 TO #M365T-SWITCH-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-TO-TOTALS
    }
    private void sub_Pnd_Write_Total_Page() throws Exception                                                                                                              //Natural: #WRITE-TOTAL-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_In_Detail.reset();                                                                                                                                            //Natural: RESET #IN-DETAIL
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm365t.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM365T'
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"****", Color.white, new FieldAttributes ("AD=I"),new TabSetting(56),"END OF REPORT",               //Natural: WRITE ( 1 ) 050T '****' ( NEI ) 056T 'END OF REPORT' ( NEI ) 071T '****' ( NEI )
            Color.white, new FieldAttributes ("AD=I"),new TabSetting(71),"****", Color.white, new FieldAttributes ("AD=I"));
        if (Global.isEscape()) return;
        if (condition(pnd_M365t_Totl_Rqst_Amt.equals(getZero())))                                                                                                         //Natural: IF #M365T-TOTL-RQST-AMT = 0
        {
            getReports().write(1, ReportOption.NOTITLE," ",NEWLINE,"                    ******************************************      ",NEWLINE,"                    No data was selected for this report today      ", //Natural: WRITE ( 1 ) ' ' / '                    ******************************************      ' / '                    No data was selected for this report today      ' / '                    ******************************************      '
                NEWLINE,"                    ******************************************      ");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #WRITE-TOTAL-PAGE
    }
    private void sub_Pnd_Write_Sub_Heading_1() throws Exception                                                                                                           //Natural: #WRITE-SUB-HEADING-1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, new FieldAttributes ("AD=D"),ReportOption.NOTITLE,new TabSetting(1),"Requests Received by:", Color.white, new FieldAttributes               //Natural: WRITE ( 1 ) ( AD = D ) 001T 'Requests Received by:' ( NEI ) 023T #M365I-CNTCT-MTHD ( AD = OD CD = BL )
            ("AD=I"),new TabSetting(23),pnd_M365i_Cntct_Mthd, new FieldAttributes ("AD=OD"), Color.blue);
        if (Global.isEscape()) return;
        //*  #WRITE-SUB-HEADING-1
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_M361h_Page.nadd(1);                                                                                                                               //Natural: ADD 1 TO #M361H-PAGE
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm361h.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM361H'
                    //* * ADDED FOLLOWING    5/99
                    getReports().write(1, ReportOption.NOTITLE,"REPORT FOR UNIT:",pdaIata36x.getIata36x_Iata36x_Unit_Cde().getValue(pnd_I1));                             //Natural: WRITE ( 1 ) 'REPORT FOR UNIT:' IATA36X-UNIT-CDE ( #I1 )
                    //* * END OF ADD         5/99
                    if (condition(pnd_In_Detail.getBoolean()))                                                                                                            //Natural: IF #IN-DETAIL
                    {
                                                                                                                                                                          //Natural: PERFORM #WRITE-SUB-HEADING-1
                        sub_Pnd_Write_Sub_Heading_1();
                        if (condition(Global.isEscape())) {return;}
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Iatm365i.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'IATM365I'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DO UNTIL END OF ENTRIES PASSED BACK BY IATN36X 5/99
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaIatl365_getIat_Switch_Rpt_Rqst_Cntct_MdeIsBreak = ldaIatl365.getIat_Switch_Rpt_Rqst_Cntct_Mde().isBreak(endOfData);
        if (condition(ldaIatl365_getIat_Switch_Rpt_Rqst_Cntct_MdeIsBreak))
        {
            //*  GET CONTACT METHOD FROM TABLE FILE
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT-MODE
            sub_Get_Contact_Mode();
            if (condition(Global.isEscape())) {return;}
            if (condition(msg_Info_Sub_Pnd_Pnd_Return_Code.equals(" ")))                                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE = ' '
            {
                pnd_M365i_Cntct_Mthd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaIata201.getIata201_Pnd_Long_Desc()));                                    //Natural: COMPRESS #LONG-DESC INTO #M365I-CNTCT-MTHD LEAVING NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_M365i_Cntct_Mthd.setValue("CNTCT MTHD NOT ON FILE");                                                                                                  //Natural: ASSIGN #M365I-CNTCT-MTHD := 'CNTCT MTHD NOT ON FILE'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Page_Cntrl.reset();                                                                                                                                       //Natural: RESET #PAGE-CNTRL
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
