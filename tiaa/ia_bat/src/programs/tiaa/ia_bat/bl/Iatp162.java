/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:36:40 PM
**        * FROM NATURAL PROGRAM : Iatp162
************************************************************
**        * FILE NAME            : Iatp162.java
**        * CLASS NAME           : Iatp162
**        * INSTANCE NAME        : Iatp162
************************************************************
************************************************************************
* PROGRAM  : IATP162
* SYSTEM   : IAS
* TITLE    : IA POST RETIREMENT FLEXIBILITIES
* GENERATED: 09/02/1997
* FUNCTION : THIS PROGRAM CREATES A REPORT OF ACKNOWLEDGMENT LETTERS
*            PULLED BY EXTRACTING IAA-TRNSFR-SW-RQST.
*            THE RECORDS WILL BE SORTED INTERNALLY USING PIN AND
*            CONTRACT NUMBER AS THE SORT CRITERIA.
* JUN 2017 J BREMER       PIN EXPANSION SCAN 06/2017
* 04/2017  O SOTTO  PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iatp162 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_kdo;
    private DbsField kdo_Rcrd_Type_Cde;
    private DbsField kdo_Rqst_Id;
    private DbsField kdo_Rqst_Effctv_Dte;
    private DbsField kdo_Rqst_Entry_Dte;
    private DbsField kdo_Rqst_Entry_Tme;
    private DbsField kdo_Rqst_Lst_Actvty_Dte;
    private DbsField kdo_Rqst_Rcvd_Dte;
    private DbsField kdo_Rqst_Rcvd_Tme;
    private DbsField kdo_Rqst_Rcvd_User_Id;
    private DbsField kdo_Rqst_Entry_User_Id;
    private DbsField kdo_Rqst_Cntct_Mde;
    private DbsField kdo_Rqst_Srce;
    private DbsField kdo_Rqst_Rep_Nme;
    private DbsField kdo_Rqst_Sttmnt_Ind;
    private DbsField kdo_Rqst_Opn_Clsd_Ind;
    private DbsField kdo_Rqst_Rcprcl_Dte;
    private DbsField kdo_Rqst_Ssn;
    private DbsField kdo_Xfr_Work_Prcss_Id;
    private DbsField kdo_Xfr_Mit_Log_Dte_Tme;
    private DbsField kdo_Xfr_Stts_Cde;
    private DbsField kdo_Xfr_Rjctn_Cde;
    private DbsField kdo_Ia_Frm_Cntrct;
    private DbsField kdo_Ia_Frm_Payee;
    private DbsField kdo_Ia_Unique_Id;
    private DbsField kdo_Ia_Hold_Cde;
    private DbsField kdo_Count_Castxfr_Frm_Acct_Dta;

    private DbsGroup kdo_Xfr_Frm_Acct_Dta;
    private DbsField kdo_Xfr_Frm_Acct_Cde;
    private DbsField kdo_Xfr_Frm_Unit_Typ;
    private DbsField kdo_Xfr_Frm_Typ;
    private DbsField kdo_Xfr_Frm_Qty;
    private DbsField kdo_Xfr_Frm_Est_Amt;
    private DbsField kdo_Count_Castxfr_To_Acct_Dta;

    private DbsGroup kdo_Xfr_To_Acct_Dta;
    private DbsField kdo_Xfr_To_Acct_Cde;
    private DbsField kdo_Xfr_To_Unit_Typ;
    private DbsField kdo_Xfr_To_Typ;
    private DbsField kdo_Xfr_To_Qty;
    private DbsField kdo_Xfr_To_Est_Amt;
    private DbsField kdo_Ia_To_Cntrct;
    private DbsField kdo_Ia_To_Payee;
    private DbsField kdo_Ia_Appl_Rcvd_Dte;
    private DbsField kdo_Ia_New_Issue;
    private DbsField kdo_Ia_Rsn_For_Ovrrde;
    private DbsField kdo_Ia_Ovrrde_User_Id;
    private DbsField kdo_Ia_Gnrl_Pend_Cde;
    private DbsField pnd_Annual_Monthly;
    private DbsField pnd_Annual_Total;
    private DbsField pnd_Effective_Date;
    private DbsField pnd_Grand_Total;
    private DbsField pnd_Monthly_Total;
    private DbsField pnd_To_Date;
    private DbsField pnd_Workfile_Date_A;
    private DbsField pnd_Workfile_Date_D;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Annual_MonthlyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_kdo = new DataAccessProgramView(new NameInfo("vw_kdo", "KDO"), "IAA_TRNSFR_SW_RQST", "IA_TRANSFER_KDO", DdmPeriodicGroups.getInstance().getGroups("IAA_TRNSFR_SW_RQST"));
        kdo_Rcrd_Type_Cde = vw_kdo.getRecord().newFieldInGroup("kdo_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        kdo_Rqst_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Id", "RQST-ID", FieldType.STRING, 34, RepeatingFieldStrategy.None, "RQST_ID");
        kdo_Rqst_Effctv_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_EFFCTV_DTE");
        kdo_Rqst_Entry_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "RQST_ENTRY_DTE");
        kdo_Rqst_Entry_Tme = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_TME");
        kdo_Rqst_Lst_Actvty_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Lst_Actvty_Dte", "RQST-LST-ACTVTY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "RQST_LST_ACTVTY_DTE");
        kdo_Rqst_Rcvd_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Rcvd_Dte", "RQST-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "RQST_RCVD_DTE");
        kdo_Rqst_Rcvd_Tme = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Rcvd_Tme", "RQST-RCVD-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RQST_RCVD_TME");
        kdo_Rqst_Rcvd_User_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Rcvd_User_Id", "RQST-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_RCVD_USER_ID");
        kdo_Rqst_Entry_User_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Entry_User_Id", "RQST-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RQST_ENTRY_USER_ID");
        kdo_Rqst_Cntct_Mde = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Cntct_Mde", "RQST-CNTCT-MDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_CNTCT_MDE");
        kdo_Rqst_Srce = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Srce", "RQST-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RQST_SRCE");
        kdo_Rqst_Rep_Nme = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Rep_Nme", "RQST-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RQST_REP_NME");
        kdo_Rqst_Sttmnt_Ind = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Sttmnt_Ind", "RQST-STTMNT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_STTMNT_IND");
        kdo_Rqst_Opn_Clsd_Ind = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Opn_Clsd_Ind", "RQST-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_OPN_CLSD_IND");
        kdo_Rqst_Rcprcl_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Rcprcl_Dte", "RQST-RCPRCL-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RQST_RCPRCL_DTE");
        kdo_Rqst_Ssn = vw_kdo.getRecord().newFieldInGroup("kdo_Rqst_Ssn", "RQST-SSN", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "RQST_SSN");
        kdo_Xfr_Work_Prcss_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Xfr_Work_Prcss_Id", "XFR-WORK-PRCSS-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "XFR_WORK_PRCSS_ID");
        kdo_Xfr_Mit_Log_Dte_Tme = vw_kdo.getRecord().newFieldInGroup("kdo_Xfr_Mit_Log_Dte_Tme", "XFR-MIT-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "XFR_MIT_LOG_DTE_TME");
        kdo_Xfr_Stts_Cde = vw_kdo.getRecord().newFieldInGroup("kdo_Xfr_Stts_Cde", "XFR-STTS-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "XFR_STTS_CDE");
        kdo_Xfr_Rjctn_Cde = vw_kdo.getRecord().newFieldInGroup("kdo_Xfr_Rjctn_Cde", "XFR-RJCTN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "XFR_RJCTN_CDE");
        kdo_Ia_Frm_Cntrct = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Frm_Cntrct", "IA-FRM-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "IA_FRM_CNTRCT");
        kdo_Ia_Frm_Payee = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Frm_Payee", "IA-FRM-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_FRM_PAYEE");
        kdo_Ia_Unique_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Unique_Id", "IA-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "IA_UNIQUE_ID");
        kdo_Ia_Hold_Cde = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Hold_Cde", "IA-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "IA_HOLD_CDE");
        kdo_Count_Castxfr_Frm_Acct_Dta = vw_kdo.getRecord().newFieldInGroup("kdo_Count_Castxfr_Frm_Acct_Dta", "C*XFR-FRM-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");

        kdo_Xfr_Frm_Acct_Dta = vw_kdo.getRecord().newGroupInGroup("kdo_Xfr_Frm_Acct_Dta", "XFR-FRM-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Xfr_Frm_Acct_Cde = kdo_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_Frm_Acct_Cde", "XFR-FRM-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_ACCT_CDE", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Xfr_Frm_Unit_Typ = kdo_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_Frm_Unit_Typ", "XFR-FRM-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_UNIT_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Xfr_Frm_Typ = kdo_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_Frm_Typ", "XFR-FRM-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_TYP", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Xfr_Frm_Qty = kdo_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_Frm_Qty", "XFR-FRM-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_QTY", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Xfr_Frm_Est_Amt = kdo_Xfr_Frm_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_Frm_Est_Amt", "XFR-FRM-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_FRM_EST_AMT", "IA_TRANSFER_KDO_XFR_FRM_ACCT_DTA");
        kdo_Count_Castxfr_To_Acct_Dta = vw_kdo.getRecord().newFieldInGroup("kdo_Count_Castxfr_To_Acct_Dta", "C*XFR-TO-ACCT-DTA", RepeatingFieldStrategy.CAsteriskVariable, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");

        kdo_Xfr_To_Acct_Dta = vw_kdo.getRecord().newGroupInGroup("kdo_Xfr_To_Acct_Dta", "XFR-TO-ACCT-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Xfr_To_Acct_Cde = kdo_Xfr_To_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_To_Acct_Cde", "XFR-TO-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_ACCT_CDE", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Xfr_To_Unit_Typ = kdo_Xfr_To_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_To_Unit_Typ", "XFR-TO-UNIT-TYP", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_UNIT_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Xfr_To_Typ = kdo_Xfr_To_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_To_Typ", "XFR-TO-TYP", FieldType.STRING, 1, new DbsArrayController(1, 20) , 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_TYP", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Xfr_To_Qty = kdo_Xfr_To_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_To_Qty", "XFR-TO-QTY", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_QTY", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Xfr_To_Est_Amt = kdo_Xfr_To_Acct_Dta.newFieldArrayInGroup("kdo_Xfr_To_Est_Amt", "XFR-TO-EST-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "XFR_TO_EST_AMT", "IA_TRANSFER_KDO_XFR_TO_ACCT_DTA");
        kdo_Ia_To_Cntrct = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_To_Cntrct", "IA-TO-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "IA_TO_CNTRCT");
        kdo_Ia_To_Payee = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_To_Payee", "IA-TO-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "IA_TO_PAYEE");
        kdo_Ia_Appl_Rcvd_Dte = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Appl_Rcvd_Dte", "IA-APPL-RCVD-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "IA_APPL_RCVD_DTE");
        kdo_Ia_New_Issue = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_New_Issue", "IA-NEW-ISSUE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "IA_NEW_ISSUE");
        kdo_Ia_Rsn_For_Ovrrde = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Rsn_For_Ovrrde", "IA-RSN-FOR-OVRRDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "IA_RSN_FOR_OVRRDE");
        kdo_Ia_Ovrrde_User_Id = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Ovrrde_User_Id", "IA-OVRRDE-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "IA_OVRRDE_USER_ID");
        kdo_Ia_Gnrl_Pend_Cde = vw_kdo.getRecord().newFieldInGroup("kdo_Ia_Gnrl_Pend_Cde", "IA-GNRL-PEND-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "IA_GNRL_PEND_CDE");
        registerRecord(vw_kdo);

        pnd_Annual_Monthly = localVariables.newFieldInRecord("pnd_Annual_Monthly", "#ANNUAL-MONTHLY", FieldType.STRING, 7);
        pnd_Annual_Total = localVariables.newFieldInRecord("pnd_Annual_Total", "#ANNUAL-TOTAL", FieldType.PACKED_DECIMAL, 10);
        pnd_Effective_Date = localVariables.newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.DATE);
        pnd_Grand_Total = localVariables.newFieldInRecord("pnd_Grand_Total", "#GRAND-TOTAL", FieldType.PACKED_DECIMAL, 10);
        pnd_Monthly_Total = localVariables.newFieldInRecord("pnd_Monthly_Total", "#MONTHLY-TOTAL", FieldType.PACKED_DECIMAL, 10);
        pnd_To_Date = localVariables.newFieldInRecord("pnd_To_Date", "#TO-DATE", FieldType.DATE);
        pnd_Workfile_Date_A = localVariables.newFieldInRecord("pnd_Workfile_Date_A", "#WORKFILE-DATE-A", FieldType.STRING, 8);
        pnd_Workfile_Date_D = localVariables.newFieldInRecord("pnd_Workfile_Date_D", "#WORKFILE-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Annual_MonthlyOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Annual_Monthly_OLD", "Pnd_Annual_Monthly_OLD", FieldType.STRING, 
            7);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_kdo.reset();
        internalLoopRecord.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iatp162() throws Exception
    {
        super("Iatp162");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132 SG = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getWorkFiles().read(5, pnd_Workfile_Date_A);                                                                                                                      //Natural: READ WORK FILE 5 ONCE #WORKFILE-DATE-A
        if (condition(pnd_Workfile_Date_A.equals("99999999")))                                                                                                            //Natural: IF #WORKFILE-DATE-A = '99999999'
        {
            pnd_Workfile_Date_D.setValue(Global.getDATX());                                                                                                               //Natural: MOVE *DATX TO #WORKFILE-DATE-D
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Workfile_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Workfile_Date_A);                                                                       //Natural: MOVE EDITED #WORKFILE-DATE-A TO #WORKFILE-DATE-D ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Effective_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),"19980115");                                                                                     //Natural: MOVE EDITED '19980115' TO #EFFECTIVE-DATE ( EM = YYYYMMDD )
        vw_kdo.startDatabaseRead                                                                                                                                          //Natural: READ KDO
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(vw_kdo.readNextRow("READ01")))
        {
            //*  PULLED
            //*  ACKNOWLEDGEMENT PULLED
            //*  01/15/1998
            if (condition(!(kdo_Rqst_Sttmnt_Ind.equals("P") && kdo_Xfr_Stts_Cde.equals("F6") && kdo_Rqst_Effctv_Dte.greaterOrEqual(pnd_Effective_Date)                    //Natural: ACCEPT IF KDO.RQST-STTMNT-IND = 'P' AND KDO.XFR-STTS-CDE = 'F6' AND KDO.RQST-EFFCTV-DTE >= #EFFECTIVE-DATE AND KDO.RQST-ENTRY-DTE = #WORKFILE-DATE-D
                && kdo_Rqst_Entry_Dte.equals(pnd_Workfile_Date_D))))
            {
                continue;
            }
            if (condition(kdo_Rqst_Lst_Actvty_Dte.greater(pnd_To_Date)))                                                                                                  //Natural: IF KDO.RQST-LST-ACTVTY-DTE > #TO-DATE
            {
                pnd_To_Date.setValue(kdo_Rqst_Lst_Actvty_Dte);                                                                                                            //Natural: ASSIGN #TO-DATE := KDO.RQST-LST-ACTVTY-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(kdo_Xfr_Frm_Unit_Typ.getValue(1).equals("A")))                                                                                                  //Natural: IF KDO.XFR-FRM-UNIT-TYP ( 1 ) = 'A'
            {
                pnd_Annual_Monthly.setValue("ANNUAL ");                                                                                                                   //Natural: ASSIGN #ANNUAL-MONTHLY := 'ANNUAL '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Annual_Monthly.setValue("MONTHLY");                                                                                                                   //Natural: ASSIGN #ANNUAL-MONTHLY := 'MONTHLY'
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pnd_Annual_Monthly, kdo_Ia_Unique_Id, kdo_Ia_Frm_Cntrct, kdo_Rqst_Rcvd_Dte, kdo_Xfr_Frm_Acct_Cde.getValue(1), kdo_Rqst_Entry_User_Id); //Natural: END-ALL
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Annual_Monthly, kdo_Ia_Unique_Id, kdo_Ia_Frm_Cntrct);                                                                                      //Natural: SORT BY #ANNUAL-MONTHLY KDO.IA-UNIQUE-ID KDO.IA-FRM-CNTRCT USING KDO.RQST-RCVD-DTE KDO.XFR-FRM-ACCT-CDE ( 1 ) KDO.RQST-ENTRY-USER-ID
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Annual_Monthly, kdo_Ia_Unique_Id, kdo_Ia_Frm_Cntrct, kdo_Rqst_Rcvd_Dte, kdo_Xfr_Frm_Acct_Cde.getValue(1), 
            kdo_Rqst_Entry_User_Id)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #ANNUAL-MONTHLY
            //*  082017
            //*  082017 END
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),kdo_Ia_Unique_Id, new ReportEditMask ("999999999999"),new TabSetting(22),kdo_Ia_Frm_Cntrct,      //Natural: WRITE ( 1 ) 03T KDO.IA-UNIQUE-ID ( EM = 999999999999 ) 22T KDO.IA-FRM-CNTRCT ( EM = XXXXXXX'-'X ) 39T KDO.XFR-FRM-ACCT-CDE ( 1 ) 46T KDO.RQST-RCVD-DTE ( EM = MM/DD/YYYY ) 62T KDO.RQST-ENTRY-USER-ID 77T '_' ( 45 )
                new ReportEditMask ("XXXXXXX'-'X"),new TabSetting(39),kdo_Xfr_Frm_Acct_Cde.getValue(1),new TabSetting(46),kdo_Rqst_Rcvd_Dte, new ReportEditMask 
                ("MM/DD/YYYY"),new TabSetting(62),kdo_Rqst_Entry_User_Id,new TabSetting(77),"_",new RepeatItem(45));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *  09T KDO.IA-UNIQUE-ID
            //*    50T KDO.RQST-RCVD-DTE (EM=MM/DD/YYYY)
            //*    66T KDO.RQST-ENTRY-USER-ID
            //*    78T '_'(45)
            short decideConditionsMet131 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #ANNUAL-MONTHLY;//Natural: VALUE 'ANNUAL '
            if (condition((pnd_Annual_Monthly.equals("ANNUAL "))))
            {
                decideConditionsMet131++;
                pnd_Annual_Total.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #ANNUAL-TOTAL
            }                                                                                                                                                             //Natural: VALUE 'MONTHLY'
            else if (condition((pnd_Annual_Monthly.equals("MONTHLY"))))
            {
                decideConditionsMet131++;
                pnd_Monthly_Total.nadd(1);                                                                                                                                //Natural: ADD 1 TO #MONTHLY-TOTAL
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet131 > 0))
            {
                pnd_Grand_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #GRAND-TOTAL
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            sort01Pnd_Annual_MonthlyOld.setValue(pnd_Annual_Monthly);                                                                                                     //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(58),"END OF REPORT","*",new RepeatItem(58));                                        //Natural: WRITE ( 1 ) // '*' ( 58 ) 'END OF REPORT' '*' ( 58 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  082017
                    //*  082017
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(59),"DAILY TRANSFERS",new TabSetting(121),"PAGE:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *PROGRAM 059T 'DAILY TRANSFERS' 121T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ9 ) / 001T *DATX ( EM = MM/DD/YYYY ) 034T 'ACKNOWLEDGEMENT LETTERS PULLED REPORT FOR ENTRIES MADE' 089T #TO-DATE ( EM = MM/DD/YYYY ) 121T *TIMX ( EM = HH:II:SS' 'AP ) // 001T 'METHOD:' #ANNUAL-MONTHLY // 005T 'PIN NUMBER' 018T 'FROM CONTRACT' 034T 'FROM FUND' 046T 'RECEIVED DATE' 062T 'ENTRY USER-ID' 077T 'COMMENTS' / 005T '----------' 018T '-------------' 034T '---------' 046T '-------------' 062T '-------------' 077T '-' ( 45 ) /
                        new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(34),"ACKNOWLEDGEMENT LETTERS PULLED REPORT FOR ENTRIES MADE",new 
                        TabSetting(89),pnd_To_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(121),Global.getTIMX(), new ReportEditMask ("HH:II:SS' 'AP"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"METHOD:",pnd_Annual_Monthly,NEWLINE,NEWLINE,new TabSetting(5),"PIN NUMBER",new TabSetting(18),"FROM CONTRACT",new 
                        TabSetting(34),"FROM FUND",new TabSetting(46),"RECEIVED DATE",new TabSetting(62),"ENTRY USER-ID",new TabSetting(77),"COMMENTS",NEWLINE,new 
                        TabSetting(5),"----------",new TabSetting(18),"-------------",new TabSetting(34),"---------",new TabSetting(46),"-------------",new 
                        TabSetting(62),"-------------",new TabSetting(77),"-",new RepeatItem(45),NEWLINE);
                    //* *  093T 'COMMENTS' /
                    //*    078T '-'(45) /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Annual_MonthlyIsBreak = pnd_Annual_Monthly.isBreak(endOfData);
        if (condition(pnd_Annual_MonthlyIsBreak))
        {
            if (condition(sort01Pnd_Annual_MonthlyOld.equals("ANNUAL ")))                                                                                                 //Natural: IF OLD ( #ANNUAL-MONTHLY ) = 'ANNUAL '
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"ANNUAL  TOTAL:",pnd_Annual_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE);              //Natural: WRITE ( 1 ) // 'ANNUAL  TOTAL:' #ANNUAL-TOTAL ( EM = Z,ZZZ,ZZZ,ZZ9 ) /
                if (condition(Global.isEscape())) return;
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"MONTHLY TOTAL:",pnd_Monthly_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),NEWLINE);             //Natural: WRITE ( 1 ) // 'MONTHLY TOTAL:' #MONTHLY-TOTAL ( EM = Z,ZZZ,ZZZ,ZZ9 ) /
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132 SG=OFF");
    }
}
