/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:38 PM
**        * FROM NATURAL PROGRAM : Iaap120
************************************************************
**        * FILE NAME            : Iaap120.java
**        * CLASS NAME           : Iaap120
**        * INSTANCE NAME        : Iaap120
************************************************************
************************************************************************
* PROGRAM : IAAP120
* DATE    : 02/05/2004
*
* PURPOSE : IA PENDED REPORT
*
*
* HISTORY :
*         : 10/26/2009 - ADDED TOTALS BY PEND CODE AS WELL AS ANNUALIZED
*           PAYMENTS. SEE 10/09 FOR CHANGES
*
* 3/12      RATE BASE EXPANSION - PDA CHANGES FOR AIAN026 - SCAN ON 3/12
* 04/2017 OS  RE-STOWED ONLY FOR PIN EXPANSION.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap120 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;
    private LdaIaal050 ldaIaal050;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Code;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Desc;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role__R_Field_1;
    private DbsField iaa_Cntrct_Prtcpnt_Role_F1;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_2;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField pnd_Sort_N;
    private DbsField pnd_Total;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_3;
    private DbsField pnd_Fund_Key_Pnd_Fund_Ppcn;
    private DbsField pnd_Fund_Key_Pnd_Fund_Paye;
    private DbsField pnd_Fund_Key_Pnd_Fund_Cde;

    private DbsGroup work_Rec;
    private DbsField work_Rec_Cntrct_Part_Ppcn_Nbr_W;
    private DbsField work_Rec_Cntrct_Part_Payee_Cde_W;
    private DbsField work_Rec_Cntrct_Pend_Cde_W;
    private DbsField work_Rec_Cntrct_Pend_Dte_W;
    private DbsField work_Rec_Cntrct_Optn_Cde_W;
    private DbsField work_Rec_Cntrct_Crrncy_Cde_W;
    private DbsField work_Rec_Cntrct_First_Pymnt_Pd_Dte_W;
    private DbsField work_Rec_Cntrct_Mode_Ind_W;
    private DbsField work_Rec_Prtcpnt_Rsdncy_Cde_W;

    private DbsGroup work_Rec__R_Field_4;
    private DbsField work_Rec_Prtcpnt_Rsdncy_Cde_W_1;
    private DbsField work_Rec_Prtcpnt_Rsdncy_Cde_W_2;
    private DbsField work_Rec_Pnd_Desc_W;
    private DbsField work_Rec_Cntrct_Final_Per_Pay_Dte_W;
    private DbsField work_Rec_Cntrct_First_Pymnt_Due_Dte_W;
    private DbsField work_Rec_Tiaa_Tot_Per_Amt_W;
    private DbsField work_Rec_Tiaa_Tot_Div_Amt_W;
    private DbsField work_Rec_Total_Ppcn_W;
    private DbsField work_Rec_Pnd_Total_W;
    private DbsField pnd_Cntrct_Type_Corr_Cntrct_Key;
    private DbsField pnd_Fld_Da;
    private DbsField pnd_Date_Dif;
    private DbsField save_Pend_Date;

    private DbsGroup save_Pend_Date__R_Field_5;
    private DbsField save_Pend_Date_Pend_Date;
    private DbsField save_Pend_Date_Fill;
    private DbsField tiaa_Tot_Per_Amt_S;
    private DbsField tiaa_Tot_Div_Amt_S;
    private DbsField gr_Tot_Per_Amt_S;
    private DbsField gr_Tot_Div_Amt_S;
    private DbsField gr_Total_S;
    private DbsField total_Ppcn;
    private DbsField tiaa_Tot_Per_Amt_R;
    private DbsField tiaa_Tot_Div_Amt_R;
    private DbsField gr_Tot_Per_Amt_R;
    private DbsField gr_Tot_Div_Amt_R;
    private DbsField gr_Total_R;
    private DbsField total_Ppcn_R;
    private DbsField total_Ppcn_S;
    private DbsField pnd_Sort_Sw;
    private DbsField pnd_Sve_Rsdncy_Cde;

    private DbsGroup act_Totals;
    private DbsField act_Totals_Fund_Annual_Annlzd_Units;
    private DbsField act_Totals_Fund_Annual_Annlzd_Payts;
    private DbsField act_Totals_Fund_Annual_Units;
    private DbsField act_Totals_Fund_Annual_Payments;
    private DbsField act_Totals_Fund_Monthly_Units;
    private DbsField act_Totals_Fund_Monthly_Payments;

    private DbsGroup other_Totals;
    private DbsField other_Totals_Pnd_Std;
    private DbsField other_Totals_Pnd_Grd;

    private DbsGroup am_Totals;
    private DbsField am_Totals_Pnd_A_Units;
    private DbsField am_Totals_Pnd_A_Pmt;
    private DbsField am_Totals_Pnd_M_Units;
    private DbsField am_Totals_Pnd_M_Pmt;
    private DbsField fund_Code_1;
    private DbsField fund_Code_2;
    private DbsField fund_Desc;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_Cntrl_Cde;

    private DbsGroup pnd_Input_Record_Work_2;
    private DbsField pnd_Input_Record_Work_2_Pnd_Call_Type;
    private DbsField pnd_Parm_Desc;

    private DbsGroup pnd_Parm_Desc__R_Field_6;
    private DbsField pnd_Parm_Desc_Pnd_Parm_Desc_6;
    private DbsField pnd_Cmpny_Desc;
    private DbsField pnd_Parm_Len;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Fctr;
    private DbsField pnd_W_Unit;
    private DbsField pnd_W_Pmt;
    private DbsField pnd_Pymnt_Unit;
    private DbsField pnd_Pymnt_Amt;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Save_Check_Dte_A;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_7;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_8;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Yyyy;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Mm;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Dd;

    private DbsGroup pnd_Save_Check_Dte_A__R_Field_9;
    private DbsField pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm;
    private DbsField pnd_Cntrl_Rcrd_Key;

    private DbsGroup pnd_Cntrl_Rcrd_Key__R_Field_10;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde;
    private DbsField pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte;
    private DbsField pnd_Payment_Due_Dte;

    private DbsGroup pnd_Payment_Due_Dte__R_Field_11;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash1;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd;
    private DbsField pnd_Payment_Due_Dte_Pnd_Slash2;
    private DbsField pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy;
    private DbsField pnd_Curr_Bus_Dte;

    private DbsGroup pnd_Curr_Bus_Dte__R_Field_12;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Mm;
    private DbsField pnd_Curr_Bus_Dte_Pnd_Curr_Dd;
    private DbsField pnd_Pmt_Due;
    private DbsField pnd_Pend_Cdes;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);
        ldaIaal050 = new LdaIaal050();
        registerRecord(ldaIaal050);

        // Local Variables
        pnd_Code = localVariables.newFieldInRecord("pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 20);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");

        iaa_Cntrct_Prtcpnt_Role__R_Field_1 = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role__R_Field_1", "REDEFINE", 
            iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde);
        iaa_Cntrct_Prtcpnt_Role_F1 = iaa_Cntrct_Prtcpnt_Role__R_Field_1.newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_F1", "F1", FieldType.STRING, 1);
        iaa_Cntrct_Prtcpnt_Role_Rsdncy_Cde = iaa_Cntrct_Prtcpnt_Role__R_Field_1.newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rsdncy_Cde", "RSDNCY-CDE", FieldType.STRING, 
            2);
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_2 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_2", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_2.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        pnd_Sort_N = localVariables.newFieldInRecord("pnd_Sort_N", "#SORT-N", FieldType.NUMERIC, 1);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 14, 2);
        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_3", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Fund_Ppcn = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Ppcn", "#FUND-PPCN", FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Fund_Paye = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Paye", "#FUND-PAYE", FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Fund_Cde = pnd_Fund_Key__R_Field_3.newFieldInGroup("pnd_Fund_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 3);

        work_Rec = localVariables.newGroupInRecord("work_Rec", "WORK-REC");
        work_Rec_Cntrct_Part_Ppcn_Nbr_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Part_Ppcn_Nbr_W", "CNTRCT-PART-PPCN-NBR-W", FieldType.STRING, 10);
        work_Rec_Cntrct_Part_Payee_Cde_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Part_Payee_Cde_W", "CNTRCT-PART-PAYEE-CDE-W", FieldType.NUMERIC, 
            2);
        work_Rec_Cntrct_Pend_Cde_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Pend_Cde_W", "CNTRCT-PEND-CDE-W", FieldType.STRING, 1);
        work_Rec_Cntrct_Pend_Dte_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Pend_Dte_W", "CNTRCT-PEND-DTE-W", FieldType.NUMERIC, 6);
        work_Rec_Cntrct_Optn_Cde_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Optn_Cde_W", "CNTRCT-OPTN-CDE-W", FieldType.NUMERIC, 2);
        work_Rec_Cntrct_Crrncy_Cde_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Crrncy_Cde_W", "CNTRCT-CRRNCY-CDE-W", FieldType.STRING, 1);
        work_Rec_Cntrct_First_Pymnt_Pd_Dte_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_First_Pymnt_Pd_Dte_W", "CNTRCT-FIRST-PYMNT-PD-DTE-W", FieldType.NUMERIC, 
            6);
        work_Rec_Cntrct_Mode_Ind_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Mode_Ind_W", "CNTRCT-MODE-IND-W", FieldType.NUMERIC, 3);
        work_Rec_Prtcpnt_Rsdncy_Cde_W = work_Rec.newFieldInGroup("work_Rec_Prtcpnt_Rsdncy_Cde_W", "PRTCPNT-RSDNCY-CDE-W", FieldType.STRING, 3);

        work_Rec__R_Field_4 = work_Rec.newGroupInGroup("work_Rec__R_Field_4", "REDEFINE", work_Rec_Prtcpnt_Rsdncy_Cde_W);
        work_Rec_Prtcpnt_Rsdncy_Cde_W_1 = work_Rec__R_Field_4.newFieldInGroup("work_Rec_Prtcpnt_Rsdncy_Cde_W_1", "PRTCPNT-RSDNCY-CDE-W-1", FieldType.STRING, 
            1);
        work_Rec_Prtcpnt_Rsdncy_Cde_W_2 = work_Rec__R_Field_4.newFieldInGroup("work_Rec_Prtcpnt_Rsdncy_Cde_W_2", "PRTCPNT-RSDNCY-CDE-W-2", FieldType.STRING, 
            2);
        work_Rec_Pnd_Desc_W = work_Rec.newFieldInGroup("work_Rec_Pnd_Desc_W", "#DESC-W", FieldType.STRING, 20);
        work_Rec_Cntrct_Final_Per_Pay_Dte_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_Final_Per_Pay_Dte_W", "CNTRCT-FINAL-PER-PAY-DTE-W", FieldType.NUMERIC, 
            6);
        work_Rec_Cntrct_First_Pymnt_Due_Dte_W = work_Rec.newFieldInGroup("work_Rec_Cntrct_First_Pymnt_Due_Dte_W", "CNTRCT-FIRST-PYMNT-DUE-DTE-W", FieldType.NUMERIC, 
            6);
        work_Rec_Tiaa_Tot_Per_Amt_W = work_Rec.newFieldInGroup("work_Rec_Tiaa_Tot_Per_Amt_W", "TIAA-TOT-PER-AMT-W", FieldType.NUMERIC, 9, 2);
        work_Rec_Tiaa_Tot_Div_Amt_W = work_Rec.newFieldInGroup("work_Rec_Tiaa_Tot_Div_Amt_W", "TIAA-TOT-DIV-AMT-W", FieldType.NUMERIC, 9, 2);
        work_Rec_Total_Ppcn_W = work_Rec.newFieldInGroup("work_Rec_Total_Ppcn_W", "TOTAL-PPCN-W", FieldType.NUMERIC, 5);
        work_Rec_Pnd_Total_W = work_Rec.newFieldInGroup("work_Rec_Pnd_Total_W", "#TOTAL-W", FieldType.NUMERIC, 11, 2);
        pnd_Cntrct_Type_Corr_Cntrct_Key = localVariables.newFieldInRecord("pnd_Cntrct_Type_Corr_Cntrct_Key", "#CNTRCT-TYPE-CORR-CNTRCT-KEY", FieldType.STRING, 
            14);
        pnd_Fld_Da = localVariables.newFieldInRecord("pnd_Fld_Da", "#FLD-DA", FieldType.DATE);
        pnd_Date_Dif = localVariables.newFieldInRecord("pnd_Date_Dif", "#DATE-DIF", FieldType.PACKED_DECIMAL, 8);
        save_Pend_Date = localVariables.newFieldInRecord("save_Pend_Date", "SAVE-PEND-DATE", FieldType.STRING, 8);

        save_Pend_Date__R_Field_5 = localVariables.newGroupInRecord("save_Pend_Date__R_Field_5", "REDEFINE", save_Pend_Date);
        save_Pend_Date_Pend_Date = save_Pend_Date__R_Field_5.newFieldInGroup("save_Pend_Date_Pend_Date", "PEND-DATE", FieldType.NUMERIC, 6);
        save_Pend_Date_Fill = save_Pend_Date__R_Field_5.newFieldInGroup("save_Pend_Date_Fill", "FILL", FieldType.NUMERIC, 2);
        tiaa_Tot_Per_Amt_S = localVariables.newFieldInRecord("tiaa_Tot_Per_Amt_S", "TIAA-TOT-PER-AMT-S", FieldType.NUMERIC, 9, 2);
        tiaa_Tot_Div_Amt_S = localVariables.newFieldInRecord("tiaa_Tot_Div_Amt_S", "TIAA-TOT-DIV-AMT-S", FieldType.NUMERIC, 9, 2);
        gr_Tot_Per_Amt_S = localVariables.newFieldInRecord("gr_Tot_Per_Amt_S", "GR-TOT-PER-AMT-S", FieldType.NUMERIC, 11, 2);
        gr_Tot_Div_Amt_S = localVariables.newFieldInRecord("gr_Tot_Div_Amt_S", "GR-TOT-DIV-AMT-S", FieldType.NUMERIC, 11, 2);
        gr_Total_S = localVariables.newFieldInRecord("gr_Total_S", "GR-TOTAL-S", FieldType.NUMERIC, 11, 2);
        total_Ppcn = localVariables.newFieldInRecord("total_Ppcn", "TOTAL-PPCN", FieldType.NUMERIC, 9);
        tiaa_Tot_Per_Amt_R = localVariables.newFieldInRecord("tiaa_Tot_Per_Amt_R", "TIAA-TOT-PER-AMT-R", FieldType.NUMERIC, 9, 2);
        tiaa_Tot_Div_Amt_R = localVariables.newFieldInRecord("tiaa_Tot_Div_Amt_R", "TIAA-TOT-DIV-AMT-R", FieldType.NUMERIC, 9, 2);
        gr_Tot_Per_Amt_R = localVariables.newFieldInRecord("gr_Tot_Per_Amt_R", "GR-TOT-PER-AMT-R", FieldType.NUMERIC, 11, 2);
        gr_Tot_Div_Amt_R = localVariables.newFieldInRecord("gr_Tot_Div_Amt_R", "GR-TOT-DIV-AMT-R", FieldType.NUMERIC, 11, 2);
        gr_Total_R = localVariables.newFieldInRecord("gr_Total_R", "GR-TOTAL-R", FieldType.NUMERIC, 11, 2);
        total_Ppcn_R = localVariables.newFieldInRecord("total_Ppcn_R", "TOTAL-PPCN-R", FieldType.NUMERIC, 5);
        total_Ppcn_S = localVariables.newFieldInRecord("total_Ppcn_S", "TOTAL-PPCN-S", FieldType.NUMERIC, 5);
        pnd_Sort_Sw = localVariables.newFieldInRecord("pnd_Sort_Sw", "#SORT-SW", FieldType.STRING, 1);
        pnd_Sve_Rsdncy_Cde = localVariables.newFieldInRecord("pnd_Sve_Rsdncy_Cde", "#SVE-RSDNCY-CDE", FieldType.STRING, 3);

        act_Totals = localVariables.newGroupArrayInRecord("act_Totals", "ACT-TOTALS", new DbsArrayController(1, 20, 1, 13));
        act_Totals_Fund_Annual_Annlzd_Units = act_Totals.newFieldInGroup("act_Totals_Fund_Annual_Annlzd_Units", "FUND-ANNUAL-ANNLZD-UNITS", FieldType.NUMERIC, 
            13, 3);
        act_Totals_Fund_Annual_Annlzd_Payts = act_Totals.newFieldInGroup("act_Totals_Fund_Annual_Annlzd_Payts", "FUND-ANNUAL-ANNLZD-PAYTS", FieldType.NUMERIC, 
            13, 2);
        act_Totals_Fund_Annual_Units = act_Totals.newFieldInGroup("act_Totals_Fund_Annual_Units", "FUND-ANNUAL-UNITS", FieldType.NUMERIC, 13, 3);
        act_Totals_Fund_Annual_Payments = act_Totals.newFieldInGroup("act_Totals_Fund_Annual_Payments", "FUND-ANNUAL-PAYMENTS", FieldType.NUMERIC, 13, 
            2);
        act_Totals_Fund_Monthly_Units = act_Totals.newFieldInGroup("act_Totals_Fund_Monthly_Units", "FUND-MONTHLY-UNITS", FieldType.NUMERIC, 13, 3);
        act_Totals_Fund_Monthly_Payments = act_Totals.newFieldInGroup("act_Totals_Fund_Monthly_Payments", "FUND-MONTHLY-PAYMENTS", FieldType.NUMERIC, 
            13, 2);

        other_Totals = localVariables.newGroupArrayInRecord("other_Totals", "OTHER-TOTALS", new DbsArrayController(1, 13));
        other_Totals_Pnd_Std = other_Totals.newFieldInGroup("other_Totals_Pnd_Std", "#STD", FieldType.PACKED_DECIMAL, 9, 2);
        other_Totals_Pnd_Grd = other_Totals.newFieldInGroup("other_Totals_Pnd_Grd", "#GRD", FieldType.PACKED_DECIMAL, 9, 2);

        am_Totals = localVariables.newGroupArrayInRecord("am_Totals", "AM-TOTALS", new DbsArrayController(1, 20, 1, 13));
        am_Totals_Pnd_A_Units = am_Totals.newFieldInGroup("am_Totals_Pnd_A_Units", "#A-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        am_Totals_Pnd_A_Pmt = am_Totals.newFieldInGroup("am_Totals_Pnd_A_Pmt", "#A-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        am_Totals_Pnd_M_Units = am_Totals.newFieldInGroup("am_Totals_Pnd_M_Units", "#M-UNITS", FieldType.PACKED_DECIMAL, 10, 3);
        am_Totals_Pnd_M_Pmt = am_Totals.newFieldInGroup("am_Totals_Pnd_M_Pmt", "#M-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        fund_Code_1 = localVariables.newFieldArrayInRecord("fund_Code_1", "FUND-CODE-1", FieldType.STRING, 1, new DbsArrayController(1, 20));
        fund_Code_2 = localVariables.newFieldArrayInRecord("fund_Code_2", "FUND-CODE-2", FieldType.STRING, 2, new DbsArrayController(1, 20));
        fund_Desc = localVariables.newFieldArrayInRecord("fund_Desc", "FUND-DESC", FieldType.STRING, 6, new DbsArrayController(1, 20));

        vw_iaa_Cntrl_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd", "IAA-CNTRL-RCRD"), "IAA_CNTRL_RCRD", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_Cntrl_Cde = vw_iaa_Cntrl_Rcrd.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd);

        pnd_Input_Record_Work_2 = localVariables.newGroupInRecord("pnd_Input_Record_Work_2", "#INPUT-RECORD-WORK-2");
        pnd_Input_Record_Work_2_Pnd_Call_Type = pnd_Input_Record_Work_2.newFieldInGroup("pnd_Input_Record_Work_2_Pnd_Call_Type", "#CALL-TYPE", FieldType.STRING, 
            1);
        pnd_Parm_Desc = localVariables.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 35);

        pnd_Parm_Desc__R_Field_6 = localVariables.newGroupInRecord("pnd_Parm_Desc__R_Field_6", "REDEFINE", pnd_Parm_Desc);
        pnd_Parm_Desc_Pnd_Parm_Desc_6 = pnd_Parm_Desc__R_Field_6.newFieldInGroup("pnd_Parm_Desc_Pnd_Parm_Desc_6", "#PARM-DESC-6", FieldType.STRING, 6);
        pnd_Cmpny_Desc = localVariables.newFieldInRecord("pnd_Cmpny_Desc", "#CMPNY-DESC", FieldType.STRING, 4);
        pnd_Parm_Len = localVariables.newFieldInRecord("pnd_Parm_Len", "#PARM-LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Fctr = localVariables.newFieldInRecord("pnd_Fctr", "#FCTR", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Unit = localVariables.newFieldInRecord("pnd_W_Unit", "#W-UNIT", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_W_Pmt = localVariables.newFieldInRecord("pnd_W_Pmt", "#W-PMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pymnt_Unit = localVariables.newFieldInRecord("pnd_Pymnt_Unit", "#PYMNT-UNIT", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Pymnt_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Save_Check_Dte_A = localVariables.newFieldInRecord("pnd_Save_Check_Dte_A", "#SAVE-CHECK-DTE-A", FieldType.STRING, 8);

        pnd_Save_Check_Dte_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Save_Check_Dte_A__R_Field_7", "REDEFINE", pnd_Save_Check_Dte_A);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte = pnd_Save_Check_Dte_A__R_Field_7.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte", "#SAVE-CHECK-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Save_Check_Dte_A__R_Field_8 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_8", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Yyyy = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Save_Check_Dte_A_Pnd_Mm = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Save_Check_Dte_A_Pnd_Dd = pnd_Save_Check_Dte_A__R_Field_8.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);

        pnd_Save_Check_Dte_A__R_Field_9 = pnd_Save_Check_Dte_A__R_Field_7.newGroupInGroup("pnd_Save_Check_Dte_A__R_Field_9", "REDEFINE", pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte);
        pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm = pnd_Save_Check_Dte_A__R_Field_9.newFieldInGroup("pnd_Save_Check_Dte_A_Pnd_Save_Check_Dte_Yyyymm", 
            "#SAVE-CHECK-DTE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Cntrl_Rcrd_Key = localVariables.newFieldInRecord("pnd_Cntrl_Rcrd_Key", "#CNTRL-RCRD-KEY", FieldType.STRING, 10);

        pnd_Cntrl_Rcrd_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Cntrl_Rcrd_Key__R_Field_10", "REDEFINE", pnd_Cntrl_Rcrd_Key);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde = pnd_Cntrl_Rcrd_Key__R_Field_10.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde", "#CNTRL-CDE", FieldType.STRING, 
            2);
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte = pnd_Cntrl_Rcrd_Key__R_Field_10.newFieldInGroup("pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Invrse_Dte", "#CNTRL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Payment_Due_Dte = localVariables.newFieldInRecord("pnd_Payment_Due_Dte", "#PAYMENT-DUE-DTE", FieldType.STRING, 10);

        pnd_Payment_Due_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_Payment_Due_Dte__R_Field_11", "REDEFINE", pnd_Payment_Due_Dte);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm", "#PAYMENT-DUE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash1 = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash1", "#SLASH1", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd", "#PAYMENT-DUE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Dte_Pnd_Slash2 = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Slash2", "#SLASH2", FieldType.STRING, 
            1);
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy = pnd_Payment_Due_Dte__R_Field_11.newFieldInGroup("pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy", "#PAYMENT-DUE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Curr_Bus_Dte = localVariables.newFieldInRecord("pnd_Curr_Bus_Dte", "#CURR-BUS-DTE", FieldType.STRING, 8);

        pnd_Curr_Bus_Dte__R_Field_12 = localVariables.newGroupInRecord("pnd_Curr_Bus_Dte__R_Field_12", "REDEFINE", pnd_Curr_Bus_Dte);
        pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy = pnd_Curr_Bus_Dte__R_Field_12.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Yyyy", "#CURR-YYYY", FieldType.STRING, 
            4);
        pnd_Curr_Bus_Dte_Pnd_Curr_Mm = pnd_Curr_Bus_Dte__R_Field_12.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Mm", "#CURR-MM", FieldType.STRING, 2);
        pnd_Curr_Bus_Dte_Pnd_Curr_Dd = pnd_Curr_Bus_Dte__R_Field_12.newFieldInGroup("pnd_Curr_Bus_Dte_Pnd_Curr_Dd", "#CURR-DD", FieldType.NUMERIC, 2);
        pnd_Pmt_Due = localVariables.newFieldInRecord("pnd_Pmt_Due", "#PMT-DUE", FieldType.STRING, 7);
        pnd_Pend_Cdes = localVariables.newFieldArrayInRecord("pnd_Pend_Cdes", "#PEND-CDES", FieldType.STRING, 1, new DbsArrayController(1, 13));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd.reset();

        ldaIaal050.initializeValues();

        localVariables.reset();
        pnd_Parm_Len.setInitialValue(6);
        pnd_Pend_Cdes.getValue(1).setInitialValue("A");
        pnd_Pend_Cdes.getValue(2).setInitialValue("C");
        pnd_Pend_Cdes.getValue(3).setInitialValue("E");
        pnd_Pend_Cdes.getValue(4).setInitialValue("G");
        pnd_Pend_Cdes.getValue(5).setInitialValue("I");
        pnd_Pend_Cdes.getValue(6).setInitialValue("K");
        pnd_Pend_Cdes.getValue(7).setInitialValue("M");
        pnd_Pend_Cdes.getValue(8).setInitialValue("P");
        pnd_Pend_Cdes.getValue(9).setInitialValue("Q");
        pnd_Pend_Cdes.getValue(10).setInitialValue("R");
        pnd_Pend_Cdes.getValue(11).setInitialValue("S");
        pnd_Pend_Cdes.getValue(12).setInitialValue("T");
        pnd_Pend_Cdes.getValue(13).setInitialValue("O");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap120() throws Exception
    {
        super("Iaap120");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP120", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  10/09
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55 LS = 132;//Natural: FORMAT ( 2 ) PS = 55 LS = 132;//Natural: FORMAT ( 3 ) PS = 00 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        getWorkFiles().read(2, pnd_Input_Record_Work_2);                                                                                                                  //Natural: READ WORK 2 ONCE #INPUT-RECORD-WORK-2
        if (condition(! (pnd_Input_Record_Work_2_Pnd_Call_Type.equals("P") || pnd_Input_Record_Work_2_Pnd_Call_Type.equals("L"))))                                        //Natural: IF NOT ( #CALL-TYPE = 'P' OR = 'L' )
        {
            getReports().write(0, "**************************************************");                                                                                  //Natural: WRITE '**************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*           ERROR READING WORK FILE 2");                                                                                               //Natural: WRITE '*           ERROR READING WORK FILE 2'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                                         //Natural: WRITE '*' '=' #CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*  CALL TYPE MUST BE 'L' OR 'P' ");                                                                                                    //Natural: WRITE '*  CALL TYPE MUST BE "L" OR "P" '
            if (Global.isEscape()) return;
            getReports().write(0, " **************************************************");                                                                                 //Natural: WRITE ' **************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED 10/09 END
                                                                                                                                                                          //Natural: PERFORM READ-EXTERN-FILE
        sub_Read_Extern_File();
        if (condition(Global.isEscape())) {return;}
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("AA");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'AA'
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ01")))
        {
            pnd_Save_Check_Dte_A.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #SAVE-CHECK-DTE-A
            pnd_Pmt_Due.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Check_Dte,new ReportEditMask("MM/YYYY"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = MM/YYYY ) TO #PMT-DUE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Mm.setValue(pnd_Save_Check_Dte_A_Pnd_Mm);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-MM := #MM
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Dd.setValue(pnd_Save_Check_Dte_A_Pnd_Dd);                                                                                     //Natural: ASSIGN #PAYMENT-DUE-DD := #DD
        pnd_Payment_Due_Dte_Pnd_Payment_Due_Yyyy.setValue(pnd_Save_Check_Dte_A_Pnd_Yyyy);                                                                                 //Natural: ASSIGN #PAYMENT-DUE-YYYY := #YYYY
        pnd_Payment_Due_Dte_Pnd_Slash1.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH1 := '/'
        pnd_Payment_Due_Dte_Pnd_Slash2.setValue("/");                                                                                                                     //Natural: ASSIGN #SLASH2 := '/'
        pnd_Cntrl_Rcrd_Key_Pnd_Cntrl_Cde.setValue("DC");                                                                                                                  //Natural: ASSIGN #CNTRL-CDE := 'DC'
        vw_iaa_Cntrl_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) IAA-CNTRL-RCRD BY CNTRL-RCRD-KEY STARTING FROM #CNTRL-RCRD-KEY
        (
        "READ02",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", pnd_Cntrl_Rcrd_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ02:
        while (condition(vw_iaa_Cntrl_Rcrd.readNextRow("READ02")))
        {
            pnd_Curr_Bus_Dte.setValueEdited(iaa_Cntrl_Rcrd_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #CURR-BUS-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Curr_Bus_Dte_Pnd_Curr_Dd.greater(20)))                                                                                                          //Natural: IF #CURR-DD GT 20
        {
            pnd_Input_Record_Work_2_Pnd_Call_Type.setValue("L");                                                                                                          //Natural: ASSIGN #CALL-TYPE := 'L'
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Cntrct_Prtcpnt_Role.startDatabaseRead                                                                                                                      //Natural: READ IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY
        (
        "READ03",
        new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Cntrct_Prtcpnt_Role.readNextRow("READ03")))
        {
            //*    STARTING FROM 'GA000000'
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.equals(9) || iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals(" ") || iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("0"))) //Natural: REJECT IF CNTRCT-ACTVTY-CDE = 9 OR CNTRCT-PEND-CDE = ' ' OR CNTRCT-PEND-CDE = '0'
            {
                continue;
            }
            vw_iaa_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = CNTRCT-PART-PPCN-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_iaa_Cntrct.readNextRow("FIND01")))
            {
                vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Code.reset();                                                                                                                                             //Natural: RESET #CODE #DESC
            pnd_Desc.reset();
            pnd_Code.setValue(iaa_Cntrct_Prtcpnt_Role_Rsdncy_Cde);                                                                                                        //Natural: ASSIGN #CODE := RSDNCY-CDE
            DbsUtil.callnat(Iaan031q.class , getCurrentProcessState(), pnd_Code, pnd_Return_Code, pnd_Desc);                                                              //Natural: CALLNAT 'IAAN031Q' #CODE #RETURN-CODE #DESC
            if (condition(Global.isEscape())) return;
            //*  10/09
            DbsUtil.examine(new ExamineSource(pnd_Pend_Cdes.getValue("*")), new ExamineSearch(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde), new ExamineGivingIndex(pnd_J));   //Natural: EXAMINE #PEND-CDES ( * ) FOR CNTRCT-PEND-CDE GIVING INDEX #J
            //*  10/09
                                                                                                                                                                          //Natural: PERFORM CHECK-MODE
            sub_Check_Mode();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-FUND-REC
            sub_Read_Fund_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sort_Sw.reset();                                                                                                                                          //Natural: RESET #SORT-SW
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("R")))                                                                                           //Natural: IF CNTRCT-PEND-CDE = 'R'
            {
                pnd_Sort_N.setValue(1);                                                                                                                                   //Natural: ASSIGN #SORT-N := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sort_N.setValue(2);                                                                                                                                   //Natural: ASSIGN #SORT-N := 2
            }                                                                                                                                                             //Natural: END-IF
            save_Pend_Date_Pend_Date.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte);                                                                                   //Natural: MOVE CNTRCT-PEND-DTE TO PEND-DATE
            save_Pend_Date_Fill.setValue(1);                                                                                                                              //Natural: ASSIGN FILL := 01
            getSort().writeSortInData(pnd_Sort_N, iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr, iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde,  //Natural: END-ALL
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte, iaa_Cntrct_Cntrct_Optn_Cde, iaa_Cntrct_Cntrct_Crrncy_Cde, iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte, 
                iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind, iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, pnd_Desc, iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte, 
                iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte, tiaa_Tot_Per_Amt_S, tiaa_Tot_Div_Amt_S, total_Ppcn, pnd_Total, iaa_Cntrct_Cntrct_Issue_Dte, iaa_Cntrct_Cntrct_Issue_Dte_Dd);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Sort_N);                                                                                                                                   //Natural: SORT BY #SORT-N ASC USING CNTRCT-PART-PPCN-NBR CNTRCT-PART-PAYEE-CDE CNTRCT-PEND-CDE CNTRCT-PEND-DTE CNTRCT-OPTN-CDE CNTRCT-CRRNCY-CDE CNTRCT-FIRST-PYMNT-PD-DTE CNTRCT-MODE-IND PRTCPNT-RSDNCY-CDE #DESC CNTRCT-FINAL-PER-PAY-DTE CNTRCT-FIRST-PYMNT-DUE-DTE TIAA-TOT-PER-AMT-S TIAA-TOT-DIV-AMT-S TOTAL-PPCN #TOTAL CNTRCT-ISSUE-DTE CNTRCT-ISSUE-DTE-DD
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Sort_N, iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr, iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, 
            iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde, iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte, iaa_Cntrct_Cntrct_Optn_Cde, iaa_Cntrct_Cntrct_Crrncy_Cde, 
            iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte, iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind, iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde, pnd_Desc, iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte, 
            iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte, tiaa_Tot_Per_Amt_S, tiaa_Tot_Div_Amt_S, total_Ppcn, pnd_Total, iaa_Cntrct_Cntrct_Issue_Dte, iaa_Cntrct_Cntrct_Issue_Dte_Dd)))
        {
            if (condition(pnd_Sort_Sw.notEquals("Y")))                                                                                                                    //Natural: IF #SORT-SW NE 'Y'
            {
                if (condition(pnd_Sort_N.greater(1)))                                                                                                                     //Natural: IF #SORT-N GT 1
                {
                    pnd_Sort_Sw.setValue("Y");                                                                                                                            //Natural: ASSIGN #SORT-SW := 'Y'
                    getReports().write(1, "TOTAL CONTRACTS",total_Ppcn_R,new ColumnSpacing(64),gr_Tot_Per_Amt_R,gr_Tot_Div_Amt_R,new ColumnSpacing(4),                    //Natural: WRITE ( 1 ) 'TOTAL CONTRACTS' TOTAL-PPCN-R 64X GR-TOT-PER-AMT-R GR-TOT-DIV-AMT-R 4X GR-TOTAL-R
                        gr_Total_R);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "//Cntract",                                                                                                                          //Natural: DISPLAY ( 1 ) '//Cntract' CNTRCT-PART-PPCN-NBR '//Pyee' CNTRCT-PART-PAYEE-CDE ( EM = 99 ) '//Opt' CNTRCT-OPTN-CDE '/Cur/Cde' CNTRCT-CRRNCY-CDE '/Pend/Date' CNTRCT-PEND-DTE ( EM = 9999-99 ) '/Pend/Cde' CNTRCT-PEND-CDE '//Mode' CNTRCT-MODE-IND '/Rsd/Cde' PRTCPNT-RSDNCY-CDE '//Residency-Text' #DESC '/Fnl-Per-Pay/Date' CNTRCT-FINAL-PER-PAY-DTE ( EM = 9999-99 ) '/First-Due/Date' CNTRCT-FIRST-PYMNT-DUE-DTE ( EM = 9999-99 ) '/Guaranteed/Pmt-Amt' TIAA-TOT-PER-AMT-S '/Dividend/Pmt-Amt' TIAA-TOT-DIV-AMT-S '//Total-Amt' #TOTAL
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr,"//Pyee",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"//Opt",
            		iaa_Cntrct_Cntrct_Optn_Cde,"/Cur/Cde",
            		iaa_Cntrct_Cntrct_Crrncy_Cde,"/Pend/Date",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte, new ReportEditMask ("9999-99"),"/Pend/Cde",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde,"//Mode",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind,"/Rsd/Cde",
            		iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde,"//Residency-Text",
            		pnd_Desc,"/Fnl-Per-Pay/Date",
            		iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999-99"),"/First-Due/Date",
            		iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte, new ReportEditMask ("9999-99"),"/Guaranteed/Pmt-Amt",
            		tiaa_Tot_Per_Amt_S,"/Dividend/Pmt-Amt",
            		tiaa_Tot_Div_Amt_S,"//Total-Amt",
            		pnd_Total);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED 10/09 START
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(fund_Code_1.getValue(pnd_I).equals(" ")))                                                                                                       //Natural: IF FUND-CODE-1 ( #I ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(3, ReportOption.NOTITLE,"Fund:",fund_Code_1.getValue(pnd_I),fund_Desc.getValue(pnd_I),NEWLINE,NEWLINE);                                    //Natural: WRITE ( 3 ) 'Fund:' FUND-CODE-1 ( #I ) FUND-DESC ( #I ) //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"      Pmts due in all months                   Payments due",pnd_Pmt_Due);                                        //Natural: WRITE ( 3 ) '      Pmts due in all months                   Payments due' #PMT-DUE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"    -------------------------- -----------------------------------------------------");                           //Natural: WRITE ( 3 ) '    -------------------------- -----------------------------------------------------'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"Pend Annualized   Annualized      Annual        Annual       Monthly      Monthly   ");                           //Natural: WRITE ( 3 ) 'Pend Annualized   Annualized      Annual        Annual       Monthly      Monthly   '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"cde    Units       Payments        Units       Payments       Units       Payments  ");                           //Natural: WRITE ( 3 ) 'cde    Units       Payments        Units       Payments       Units       Payments  '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"--- ------------- ------------ ------------- ------------ ------------- ------------");                           //Natural: WRITE ( 3 ) '--- ------------- ------------ ------------- ------------ ------------- ------------'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #J 1 TO 13
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(13)); pnd_J.nadd(1))
            {
                getReports().write(3, ReportOption.NOTITLE,pnd_Pend_Cdes.getValue(pnd_J),new ColumnSpacing(3),act_Totals_Fund_Annual_Annlzd_Units.getValue(pnd_I,pnd_J),  //Natural: WRITE ( 3 ) #PEND-CDES ( #J ) 3X FUND-ANNUAL-ANNLZD-UNITS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.999 ) FUND-ANNUAL-ANNLZD-PAYTS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.99 ) FUND-ANNUAL-UNITS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.999 ) FUND-ANNUAL-PAYMENTS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.99 ) FUND-MONTHLY-UNITS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.999 ) FUND-MONTHLY-PAYMENTS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.99 )
                    new ReportEditMask ("Z,ZZZ,ZZ9.999"),act_Totals_Fund_Annual_Annlzd_Payts.getValue(pnd_I,pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.99"),act_Totals_Fund_Annual_Units.getValue(pnd_I,pnd_J), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.999"),act_Totals_Fund_Annual_Payments.getValue(pnd_I,pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.99"),act_Totals_Fund_Monthly_Units.getValue(pnd_I,pnd_J), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.999"),act_Totals_Fund_Monthly_Payments.getValue(pnd_I,pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"************************************************************",                //Natural: WRITE ( 3 ) ///// '************************************************************' / '*           Summary by Pend Code - All Contracts           *' / '************************************************************' //
            NEWLINE,"*           Summary by Pend Code - All Contracts           *",NEWLINE,"************************************************************",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #J 1 TO 13
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(13)); pnd_J.nadd(1))
        {
            getReports().write(3, ReportOption.NOTITLE,"Pend Code:",pnd_Pend_Cdes.getValue(pnd_J),NEWLINE);                                                               //Natural: WRITE ( 3 ) 'Pend Code:' #PEND-CDES ( #J ) /
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"         Per Pymnts/    Annual       Monthly      Monthly   ");                                                   //Natural: WRITE ( 3 ) '         Per Pymnts/    Annual       Monthly      Monthly   '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"Fund    Annual Units    Payments      Units       Payments  ");                                                   //Natural: WRITE ( 3 ) 'Fund    Annual Units    Payments      Units       Payments  '
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"------ ------------- ------------ ------------- ------------");                                                   //Natural: WRITE ( 3 ) '------ ------------- ------------ ------------- ------------'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"TRD   ",other_Totals_Pnd_Std.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.99"));                               //Natural: WRITE ( 3 ) 'TRD   ' #STD ( #J ) ( EM = Z,ZZZ,ZZ9.99 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"GRD   ",other_Totals_Pnd_Grd.getValue(pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.99"));                               //Natural: WRITE ( 3 ) 'GRD   ' #GRD ( #J ) ( EM = Z,ZZZ,ZZ9.99 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(fund_Desc.getValue(pnd_I).equals(" ")))                                                                                                     //Natural: IF FUND-DESC ( #I ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOTITLE,fund_Desc.getValue(pnd_I),am_Totals_Pnd_A_Units.getValue(pnd_I,pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.999"),am_Totals_Pnd_A_Pmt.getValue(pnd_I,pnd_J),  //Natural: WRITE ( 3 ) FUND-DESC ( #I ) #A-UNITS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.999 ) #A-PMT ( #I,#J ) ( EM = Z,ZZZ,ZZ9.99 ) #M-UNITS ( #I,#J ) ( EM = Z,ZZZ,ZZ9.999 ) #M-PMT ( #I,#J ) ( EM = Z,ZZZ,ZZ9.99 )
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),am_Totals_Pnd_M_Units.getValue(pnd_I,pnd_J), new ReportEditMask ("Z,ZZZ,ZZ9.999"),am_Totals_Pnd_M_Pmt.getValue(pnd_I,pnd_J), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ADDED 10/09 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  END-READ
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-FUND-REC
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SORT-WORK-FILE
                                                                                                                                                                          //Natural: PERFORM SORT-WORK-FILE
        sub_Sort_Work_File();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, "TOTAL CONTRACTS",total_Ppcn_S,new ColumnSpacing(64),gr_Tot_Per_Amt_S,gr_Tot_Div_Amt_S,new ColumnSpacing(4),gr_Total_S);                    //Natural: WRITE ( 1 ) 'TOTAL CONTRACTS' TOTAL-PPCN-S 64X GR-TOT-PER-AMT-S GR-TOT-DIV-AMT-S 4X GR-TOTAL-S
        if (Global.isEscape()) return;
        total_Ppcn_S.compute(new ComputeParameters(false, total_Ppcn_S), total_Ppcn_R.add(total_Ppcn_S));                                                                 //Natural: ASSIGN TOTAL-PPCN-S := TOTAL-PPCN-R + TOTAL-PPCN-S
        gr_Tot_Per_Amt_S.nadd(gr_Tot_Per_Amt_R);                                                                                                                          //Natural: ASSIGN GR-TOT-PER-AMT-S := GR-TOT-PER-AMT-S + GR-TOT-PER-AMT-R
        gr_Tot_Div_Amt_S.nadd(gr_Tot_Div_Amt_R);                                                                                                                          //Natural: ASSIGN GR-TOT-DIV-AMT-S := GR-TOT-DIV-AMT-S+ GR-TOT-DIV-AMT-R
        gr_Total_S.nadd(gr_Total_R);                                                                                                                                      //Natural: ASSIGN GR-TOTAL-S := GR-TOTAL-S+ GR-TOTAL-R
        getReports().write(2, "TOTAL CONTRACTS",total_Ppcn_S,new ColumnSpacing(62),gr_Tot_Per_Amt_S,gr_Tot_Div_Amt_S,new ColumnSpacing(4),gr_Total_S);                    //Natural: WRITE ( 2 ) 'TOTAL CONTRACTS' TOTAL-PPCN-S 62X GR-TOT-PER-AMT-S GR-TOT-DIV-AMT-S 4X GR-TOTAL-S
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MONTHLY-AUV
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-EXTERN-FILE
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Read_Fund_Rec() throws Exception                                                                                                                     //Natural: READ-FUND-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Total.reset();                                                                                                                                                //Natural: RESET #TOTAL
        tiaa_Tot_Per_Amt_S.reset();                                                                                                                                       //Natural: RESET TIAA-TOT-PER-AMT-S
        tiaa_Tot_Div_Amt_S.reset();                                                                                                                                       //Natural: RESET TIAA-TOT-DIV-AMT-S
        pnd_Fund_Key_Pnd_Fund_Ppcn.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                                //Natural: ASSIGN #FUND-PPCN := CNTRCT-PART-PPCN-NBR
        pnd_Fund_Key_Pnd_Fund_Paye.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                               //Natural: ASSIGN #FUND-PAYE := CNTRCT-PART-PAYEE-CDE
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #FUND-KEY
        (
        "READ04",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ04")))
        {
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr)))                                               //Natural: IF CNTRCT-PART-PPCN-NBR NE TIAA-CNTRCT-PPCN-NBR
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.notEquals(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde)))                                             //Natural: IF CNTRCT-PART-PAYEE-CDE NE TIAA-CNTRCT-PAYEE-CDE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED 10/09 START
            if (condition(DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde,"'T'")))                                                                             //Natural: IF TIAA-CMPNY-FUND-CDE = MASK ( 'T' )
            {
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1G")))                                                                                             //Natural: IF TIAA-FUND-CDE = '1G'
                {
                    other_Totals_Pnd_Grd.getValue(pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                       //Natural: ADD TIAA-TOT-PER-AMT TO #GRD ( #J )
                    other_Totals_Pnd_Grd.getValue(pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                       //Natural: ADD TIAA-TOT-DIV-AMT TO #GRD ( #J )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    other_Totals_Pnd_Std.getValue(pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                       //Natural: ADD TIAA-TOT-PER-AMT TO #STD ( #J )
                    other_Totals_Pnd_Std.getValue(pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                       //Natural: ADD TIAA-TOT-DIV-AMT TO #STD ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt.reset();                                                                                                              //Natural: RESET TIAA-TOT-DIV-AMT
                DbsUtil.examine(new ExamineSource(fund_Code_2.getValue("*")), new ExamineSearch(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde), new ExamineGivingIndex(pnd_I));        //Natural: EXAMINE FUND-CODE-2 ( * ) FOR TIAA-FUND-CDE GIVING INDEX #I
                if (condition(DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde,"'W'") || DbsUtil.maskMatches(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde,            //Natural: IF TIAA-CMPNY-FUND-CDE = MASK ( 'W' ) OR TIAA-CMPNY-FUND-CDE = MASK ( '4' )
                    "'4'")))
                {
                                                                                                                                                                          //Natural: PERFORM GET-MONTHLY-AUV
                    sub_Get_Monthly_Auv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt.compute(new ComputeParameters(true, iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt), iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1).multiply(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned())); //Natural: COMPUTE ROUNDED TIAA-TOT-PER-AMT = TIAA-UNITS-CNT ( 1 ) * AUV-RETURNED
                    am_Totals_Pnd_M_Pmt.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                  //Natural: ADD TIAA-TOT-PER-AMT TO #M-PMT ( #I,#J )
                    am_Totals_Pnd_M_Units.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                                      //Natural: ADD TIAA-UNITS-CNT ( 1 ) TO #M-UNITS ( #I,#J )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        act_Totals_Fund_Monthly_Payments.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                 //Natural: ADD TIAA-TOT-PER-AMT TO FUND-MONTHLY-PAYMENTS ( #I,#J )
                        act_Totals_Fund_Monthly_Units.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                          //Natural: ADD TIAA-UNITS-CNT ( 1 ) TO FUND-MONTHLY-UNITS ( #I,#J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    am_Totals_Pnd_A_Pmt.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                  //Natural: ADD TIAA-TOT-PER-AMT TO #A-PMT ( #I,#J )
                    am_Totals_Pnd_A_Units.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                                      //Natural: ADD TIAA-UNITS-CNT ( 1 ) TO #A-UNITS ( #I,#J )
                    pnd_W_Unit.compute(new ComputeParameters(true, pnd_W_Unit), iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1).multiply(pnd_Fctr));                        //Natural: COMPUTE ROUNDED #W-UNIT = TIAA-UNITS-CNT ( 1 ) * #FCTR
                    pnd_W_Pmt.compute(new ComputeParameters(true, pnd_W_Pmt), iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt.multiply(pnd_Fctr));                                    //Natural: COMPUTE ROUNDED #W-PMT = TIAA-TOT-PER-AMT * #FCTR
                    act_Totals_Fund_Annual_Annlzd_Units.getValue(pnd_I,pnd_J).nadd(pnd_W_Unit);                                                                           //Natural: ADD #W-UNIT TO FUND-ANNUAL-ANNLZD-UNITS ( #I,#J )
                    act_Totals_Fund_Annual_Annlzd_Payts.getValue(pnd_I,pnd_J).nadd(pnd_W_Pmt);                                                                            //Natural: ADD #W-PMT TO FUND-ANNUAL-ANNLZD-PAYTS ( #I,#J )
                    if (condition(pnd_Payment_Due.getBoolean()))                                                                                                          //Natural: IF #PAYMENT-DUE
                    {
                        act_Totals_Fund_Annual_Payments.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                  //Natural: ADD TIAA-TOT-PER-AMT TO FUND-ANNUAL-PAYMENTS ( #I,#J )
                        act_Totals_Fund_Annual_Units.getValue(pnd_I,pnd_J).nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                           //Natural: ADD TIAA-UNITS-CNT ( 1 ) TO FUND-ANNUAL-UNITS ( #I,#J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ADDED 10/09 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("R")))                                                                                           //Natural: IF CNTRCT-PEND-CDE = 'R'
            {
                total_Ppcn_R.nadd(1);                                                                                                                                     //Natural: ADD 1 TO TOTAL-PPCN-R
                tiaa_Tot_Per_Amt_S.compute(new ComputeParameters(false, tiaa_Tot_Per_Amt_S), tiaa_Tot_Per_Amt_R.add(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt));                //Natural: ASSIGN TIAA-TOT-PER-AMT-S := TIAA-TOT-PER-AMT-R + TIAA-TOT-PER-AMT
                pnd_Total.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                                      //Natural: ASSIGN #TOTAL := #TOTAL+ TIAA-TOT-PER-AMT
                gr_Tot_Per_Amt_R.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                               //Natural: ASSIGN GR-TOT-PER-AMT-R := GR-TOT-PER-AMT-R + TIAA-TOT-PER-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                total_Ppcn_S.nadd(1);                                                                                                                                     //Natural: ADD 1 TO TOTAL-PPCN-S
                pnd_Total.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                                      //Natural: ASSIGN #TOTAL := #TOTAL+ TIAA-TOT-PER-AMT
                tiaa_Tot_Per_Amt_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                             //Natural: ASSIGN TIAA-TOT-PER-AMT-S := TIAA-TOT-PER-AMT-S + TIAA-TOT-PER-AMT
                gr_Tot_Per_Amt_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                               //Natural: ASSIGN GR-TOT-PER-AMT-S := GR-TOT-PER-AMT-S + TIAA-TOT-PER-AMT
                gr_Total_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                                     //Natural: ASSIGN GR-TOTAL-S := GR-TOTAL-S + TIAA-TOT-PER-AMT
            }                                                                                                                                                             //Natural: END-IF
            //* *******************8
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1 ") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1G") || iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde.equals("1S"))) //Natural: IF TIAA-FUND-CDE = '1 ' OR = '1G' OR = '1S'
            {
                if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.equals("R")))                                                                                       //Natural: IF CNTRCT-PEND-CDE = 'R'
                {
                    tiaa_Tot_Div_Amt_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                         //Natural: ASSIGN TIAA-TOT-DIV-AMT-S := TIAA-TOT-DIV-AMT-S + TIAA-TOT-DIV-AMT
                    pnd_Total.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                                  //Natural: ASSIGN #TOTAL := #TOTAL+ TIAA-TOT-DIV-AMT
                    gr_Tot_Div_Amt_R.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                           //Natural: ASSIGN GR-TOT-DIV-AMT-R := GR-TOT-DIV-AMT-R + TIAA-TOT-DIV-AMT
                    gr_Total_R.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                                 //Natural: ASSIGN GR-TOTAL-R := GR-TOTAL-R + TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    tiaa_Tot_Div_Amt_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                         //Natural: ASSIGN TIAA-TOT-DIV-AMT-S := TIAA-TOT-DIV-AMT-S + TIAA-TOT-DIV-AMT
                    pnd_Total.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                                  //Natural: ASSIGN #TOTAL := #TOTAL+ TIAA-TOT-DIV-AMT
                    gr_Tot_Div_Amt_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                           //Natural: ASSIGN GR-TOT-DIV-AMT-S := GR-TOT-DIV-AMT-S + TIAA-TOT-DIV-AMT
                    gr_Total_S.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt);                                                                                                 //Natural: ASSIGN GR-TOTAL-S := GR-TOTAL-S + TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        work_Rec_Cntrct_Part_Ppcn_Nbr_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr);                                                                           //Natural: ASSIGN CNTRCT-PART-PPCN-NBR-W := CNTRCT-PART-PPCN-NBR
        work_Rec_Cntrct_Part_Payee_Cde_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde);                                                                         //Natural: ASSIGN CNTRCT-PART-PAYEE-CDE-W := CNTRCT-PART-PAYEE-CDE
        work_Rec_Cntrct_Pend_Cde_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde);                                                                                     //Natural: ASSIGN CNTRCT-PEND-CDE-W := CNTRCT-PEND-CDE
        work_Rec_Cntrct_Pend_Dte_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte);                                                                                     //Natural: ASSIGN CNTRCT-PEND-DTE-W := CNTRCT-PEND-DTE
        work_Rec_Cntrct_Optn_Cde_W.setValue(iaa_Cntrct_Cntrct_Optn_Cde);                                                                                                  //Natural: ASSIGN CNTRCT-OPTN-CDE-W := CNTRCT-OPTN-CDE
        work_Rec_Cntrct_Crrncy_Cde_W.setValue(iaa_Cntrct_Cntrct_Crrncy_Cde);                                                                                              //Natural: ASSIGN CNTRCT-CRRNCY-CDE-W := CNTRCT-CRRNCY-CDE
        work_Rec_Cntrct_First_Pymnt_Pd_Dte_W.setValue(iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte);                                                                              //Natural: ASSIGN CNTRCT-FIRST-PYMNT-PD-DTE-W := CNTRCT-FIRST-PYMNT-PD-DTE
        work_Rec_Cntrct_Mode_Ind_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind);                                                                                     //Natural: ASSIGN CNTRCT-MODE-IND-W := CNTRCT-MODE-IND
        work_Rec_Prtcpnt_Rsdncy_Cde_W.setValue(iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde);                                                                               //Natural: ASSIGN PRTCPNT-RSDNCY-CDE-W := PRTCPNT-RSDNCY-CDE
        work_Rec_Pnd_Desc_W.setValue(pnd_Desc);                                                                                                                           //Natural: ASSIGN #DESC-W := #DESC
        work_Rec_Cntrct_Final_Per_Pay_Dte_W.setValue(iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte);                                                                   //Natural: ASSIGN CNTRCT-FINAL-PER-PAY-DTE-W := CNTRCT-FINAL-PER-PAY-DTE
        work_Rec_Cntrct_First_Pymnt_Due_Dte_W.setValue(iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte);                                                                            //Natural: ASSIGN CNTRCT-FIRST-PYMNT-DUE-DTE-W := CNTRCT-FIRST-PYMNT-DUE-DTE
        work_Rec_Tiaa_Tot_Per_Amt_W.setValue(tiaa_Tot_Per_Amt_S);                                                                                                         //Natural: ASSIGN TIAA-TOT-PER-AMT-W := TIAA-TOT-PER-AMT-S
        work_Rec_Tiaa_Tot_Div_Amt_W.setValue(tiaa_Tot_Div_Amt_S);                                                                                                         //Natural: ASSIGN TIAA-TOT-DIV-AMT-W := TIAA-TOT-DIV-AMT-S
        work_Rec_Total_Ppcn_W.setValue(total_Ppcn_S);                                                                                                                     //Natural: ASSIGN TOTAL-PPCN-W := TOTAL-PPCN-S
        work_Rec_Pnd_Total_W.setValue(pnd_Total);                                                                                                                         //Natural: ASSIGN #TOTAL-W := #TOTAL
        getWorkFiles().write(1, false, work_Rec);                                                                                                                         //Natural: WRITE WORK FILE 1 WORK-REC
    }
    private void sub_Sort_Work_File() throws Exception                                                                                                                    //Natural: SORT-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        READWORK05:                                                                                                                                                       //Natural: READ WORK 1 WORK-REC
        while (condition(getWorkFiles().read(1, work_Rec)))
        {
            getSort().writeSortInData(work_Rec_Prtcpnt_Rsdncy_Cde_W, work_Rec_Cntrct_Part_Ppcn_Nbr_W, work_Rec_Cntrct_Part_Payee_Cde_W, work_Rec_Cntrct_Pend_Cde_W,       //Natural: END-ALL
                work_Rec_Cntrct_Pend_Dte_W, work_Rec_Cntrct_Optn_Cde_W, work_Rec_Cntrct_Crrncy_Cde_W, work_Rec_Cntrct_First_Pymnt_Pd_Dte_W, work_Rec_Cntrct_Mode_Ind_W, 
                work_Rec_Pnd_Desc_W, work_Rec_Cntrct_Final_Per_Pay_Dte_W, work_Rec_Cntrct_First_Pymnt_Due_Dte_W, work_Rec_Tiaa_Tot_Per_Amt_W, work_Rec_Tiaa_Tot_Div_Amt_W, 
                work_Rec_Total_Ppcn_W, work_Rec_Pnd_Total_W);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK05_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(work_Rec_Prtcpnt_Rsdncy_Cde_W);                                                                                                                //Natural: SORT BY PRTCPNT-RSDNCY-CDE-W ASC USING CNTRCT-PART-PPCN-NBR-W CNTRCT-PART-PAYEE-CDE-W CNTRCT-PEND-CDE-W CNTRCT-PEND-DTE-W CNTRCT-OPTN-CDE-W CNTRCT-CRRNCY-CDE-W CNTRCT-FIRST-PYMNT-PD-DTE-W CNTRCT-MODE-IND-W #DESC-W CNTRCT-FINAL-PER-PAY-DTE-W CNTRCT-FIRST-PYMNT-DUE-DTE-W TIAA-TOT-PER-AMT-W TIAA-TOT-DIV-AMT-W TOTAL-PPCN-W #TOTAL-W
        SORT02:
        while (condition(getSort().readSortOutData(work_Rec_Prtcpnt_Rsdncy_Cde_W, work_Rec_Cntrct_Part_Ppcn_Nbr_W, work_Rec_Cntrct_Part_Payee_Cde_W, work_Rec_Cntrct_Pend_Cde_W, 
            work_Rec_Cntrct_Pend_Dte_W, work_Rec_Cntrct_Optn_Cde_W, work_Rec_Cntrct_Crrncy_Cde_W, work_Rec_Cntrct_First_Pymnt_Pd_Dte_W, work_Rec_Cntrct_Mode_Ind_W, 
            work_Rec_Pnd_Desc_W, work_Rec_Cntrct_Final_Per_Pay_Dte_W, work_Rec_Cntrct_First_Pymnt_Due_Dte_W, work_Rec_Tiaa_Tot_Per_Amt_W, work_Rec_Tiaa_Tot_Div_Amt_W, 
            work_Rec_Total_Ppcn_W, work_Rec_Pnd_Total_W)))
        {
            if (condition(work_Rec_Prtcpnt_Rsdncy_Cde_W.notEquals(pnd_Sve_Rsdncy_Cde)))                                                                                   //Natural: IF PRTCPNT-RSDNCY-CDE-W NE #SVE-RSDNCY-CDE
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Sve_Rsdncy_Cde.setValue(work_Rec_Prtcpnt_Rsdncy_Cde_W);                                                                                               //Natural: ASSIGN #SVE-RSDNCY-CDE := PRTCPNT-RSDNCY-CDE-W
            }                                                                                                                                                             //Natural: END-IF
            if (condition(work_Rec_Prtcpnt_Rsdncy_Cde_W_2.equals("GE")))                                                                                                  //Natural: IF PRTCPNT-RSDNCY-CDE-W-2 = 'GE'
            {
                work_Rec_Pnd_Desc_W.setValue("Germany");                                                                                                                  //Natural: ASSIGN #DESC-W := 'Germany'
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(2, "//Cntract",                                                                                                                          //Natural: DISPLAY ( 2 ) '//Cntract' CNTRCT-PART-PPCN-NBR-W '//Pyee' CNTRCT-PART-PAYEE-CDE-W ( EM = 99 ) '//Opt' CNTRCT-OPTN-CDE-W '/Cur/Cde' CNTRCT-CRRNCY-CDE-W '/Pend/Date' CNTRCT-PEND-DTE-W ( EM = 9999-99 ) '/Pend/Cde' CNTRCT-PEND-CDE-W '//Mode' CNTRCT-MODE-IND-W '/Rsd/Cde' PRTCPNT-RSDNCY-CDE-W '//Residency-Text' #DESC-W '/Fnl-Per-Pay/Date' CNTRCT-FINAL-PER-PAY-DTE-W ( EM = 9999-99 ) '/First-Due/Date' CNTRCT-FIRST-PYMNT-DUE-DTE-W ( EM = 9999-99 ) '/Guaranteed/Pmt-Amt' TIAA-TOT-PER-AMT-W '/Dividend/Pmt-Amt' TIAA-TOT-DIV-AMT-W '//Total-Amt' #TOTAL-W
            		work_Rec_Cntrct_Part_Ppcn_Nbr_W,"//Pyee",
            		work_Rec_Cntrct_Part_Payee_Cde_W, new ReportEditMask ("99"),"//Opt",
            		work_Rec_Cntrct_Optn_Cde_W,"/Cur/Cde",
            		work_Rec_Cntrct_Crrncy_Cde_W,"/Pend/Date",
            		work_Rec_Cntrct_Pend_Dte_W, new ReportEditMask ("9999-99"),"/Pend/Cde",
            		work_Rec_Cntrct_Pend_Cde_W,"//Mode",
            		work_Rec_Cntrct_Mode_Ind_W,"/Rsd/Cde",
            		work_Rec_Prtcpnt_Rsdncy_Cde_W,"//Residency-Text",
            		work_Rec_Pnd_Desc_W,"/Fnl-Per-Pay/Date",
            		work_Rec_Cntrct_Final_Per_Pay_Dte_W, new ReportEditMask ("9999-99"),"/First-Due/Date",
            		work_Rec_Cntrct_First_Pymnt_Due_Dte_W, new ReportEditMask ("9999-99"),"/Guaranteed/Pmt-Amt",
            		work_Rec_Tiaa_Tot_Per_Amt_W,"/Dividend/Pmt-Amt",
            		work_Rec_Tiaa_Tot_Div_Amt_W,"//Total-Amt",
            		work_Rec_Pnd_Total_W);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
    }
    //*  ADDED 10/09 START
    private void sub_Check_Mode() throws Exception                                                                                                                        //Natural: CHECK-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Payment_Due.reset();                                                                                                                                          //Natural: RESET #PAYMENT-DUE #FCTR
        pnd_Fctr.reset();
        short decideConditionsMet555 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST CNTRCT-MODE-IND;//Natural: VALUE 100
        if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(100))))
        {
            decideConditionsMet555++;
            pnd_Payment_Due.setValue(true);                                                                                                                               //Natural: ASSIGN #PAYMENT-DUE := TRUE
        }                                                                                                                                                                 //Natural: VALUE 601
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(601))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10))) //Natural: IF #MM = 01 OR = 04 OR = 07 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 602
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(602))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11))) //Natural: IF #MM = 02 OR = 05 OR = 08 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 603
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(603))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12))) //Natural: IF #MM = 03 OR = 06 OR = 09 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 701
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(701))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                //Natural: IF #MM = 01 OR = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 702
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(702))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                //Natural: IF #MM = 02 OR = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 703
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(703))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                //Natural: IF #MM = 03 OR = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 704
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(704))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                               //Natural: IF #MM = 04 OR = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 705
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(705))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                               //Natural: IF #MM = 05 OR = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 706
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(706))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6) || pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                               //Natural: IF #MM = 06 OR = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fctr.setValue(2);                                                                                                                                         //Natural: ASSIGN #FCTR := 2
        }                                                                                                                                                                 //Natural: VALUE 801
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(801))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(1)))                                                                                                         //Natural: IF #MM = 01
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 802
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(802))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(2)))                                                                                                         //Natural: IF #MM = 02
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 803
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(803))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(3)))                                                                                                         //Natural: IF #MM = 03
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 804
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(804))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(4)))                                                                                                         //Natural: IF #MM = 04
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 805
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(805))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(5)))                                                                                                         //Natural: IF #MM = 05
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 806
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(806))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(6)))                                                                                                         //Natural: IF #MM = 06
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 807
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(807))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(7)))                                                                                                         //Natural: IF #MM = 07
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 808
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(808))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(8)))                                                                                                         //Natural: IF #MM = 08
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 809
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(809))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(9)))                                                                                                         //Natural: IF #MM = 09
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 810
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(810))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(10)))                                                                                                        //Natural: IF #MM = 10
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 811
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(811))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(11)))                                                                                                        //Natural: IF #MM = 11
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 812
        else if (condition((iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(812))))
        {
            decideConditionsMet555++;
            if (condition(pnd_Save_Check_Dte_A_Pnd_Mm.equals(12)))                                                                                                        //Natural: IF #MM = 12
            {
                pnd_Payment_Due.setValue(true);                                                                                                                           //Natural: ASSIGN #PAYMENT-DUE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet555 > 0))
        {
            //*  SEMI-ANNUAL
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.greaterOrEqual(701) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.lessOrEqual(706)))                       //Natural: IF CNTRCT-MODE-IND = 701 THRU 706
            {
                pnd_Fctr.setValue(2);                                                                                                                                     //Natural: ASSIGN #FCTR := 2
            }                                                                                                                                                             //Natural: END-IF
            //*  ANNUAL
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.greaterOrEqual(801) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.lessOrEqual(812)))                       //Natural: IF CNTRCT-MODE-IND = 801 THRU 812
            {
                pnd_Fctr.setValue(1);                                                                                                                                     //Natural: ASSIGN #FCTR := 1
            }                                                                                                                                                             //Natural: END-IF
            //*  QUARTERLY
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.greaterOrEqual(601) && iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.lessOrEqual(603)))                       //Natural: IF CNTRCT-MODE-IND = 601 THRU 603
            {
                pnd_Fctr.setValue(4);                                                                                                                                     //Natural: ASSIGN #FCTR := 4
            }                                                                                                                                                             //Natural: END-IF
            //*  MONTHLY
            if (condition(iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.equals(100)))                                                                                           //Natural: IF CNTRCT-MODE-IND = 100
            {
                pnd_Fctr.setValue(12);                                                                                                                                    //Natural: ASSIGN #FCTR := 12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHECK-MODE
    }
    private void sub_Get_Monthly_Auv() throws Exception                                                                                                                   //Natural: GET-MONTHLY-AUV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  L FOR LATEST AVAILABLE FACTOR
        //*  MONTHLY
        ldaIaal050.getIa_Aian026_Linkage_Return_Code().reset();                                                                                                           //Natural: RESET RETURN-CODE AUV-RETURNED AUV-DATE-RETURN
        ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().reset();
        ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return().reset();
        ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type().setValue(pnd_Input_Record_Work_2_Pnd_Call_Type);                                                                  //Natural: ASSIGN IA-CALL-TYPE := #CALL-TYPE
        ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code().setValue(fund_Code_1.getValue(pnd_I));                                                                            //Natural: ASSIGN IA-FUND-CODE := FUND-CODE-1 ( #I )
        ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd().setValue("M");                                                                                                  //Natural: ASSIGN IA-REVAL-METHD := 'M'
        if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals("L")))                                                                                                 //Natural: IF #CALL-TYPE = 'L'
        {
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date().setValue(99999999);                                                                                          //Natural: ASSIGN IA-CHECK-DATE := 99999999
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date().reset();                                                                                                     //Natural: RESET IA-ISSUE-DATE
            //*  "P"= PAYMENT FOR ORIGINAL SETTLEMENT UNITS (FIRST OF MONTH ONLY)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MOVE EDITED CNTRL-CHECK-DTE (EM=YYYYMMDD) TO IA-CHECK-DATE-A
            ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date_A().setValue(pnd_Save_Check_Dte_A);                                                                            //Natural: MOVE #SAVE-CHECK-DTE-A TO IA-CHECK-DATE-A
            ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Ccyymm().setValue(iaa_Cntrct_Cntrct_Issue_Dte);                                                                     //Natural: ASSIGN IA-ISSUE-CCYYMM := CNTRCT-ISSUE-DTE
            if (condition(DbsUtil.maskMatches(iaa_Cntrct_Cntrct_Issue_Dte_Dd,"DD")))                                                                                      //Natural: IF CNTRCT-ISSUE-DTE-DD EQ MASK ( DD )
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd);                                                                 //Natural: ASSIGN IA-ISSUE-DAY := CNTRCT-ISSUE-DTE-DD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Day().setValue(1);                                                                                              //Natural: ASSIGN IA-ISSUE-DAY := 01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FETCH THE ANNUITY UNIT VALUE
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), ldaIaal050.getIa_Aian026_Linkage());                                                                    //Natural: CALLNAT 'AIAN026' IA-AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Return_Code().notEquals(getZero())))                                                                               //Natural: IF RETURN-CODE NE 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                  ERROR                                 *");                                                                          //Natural: WRITE '*                  ERROR                                 *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an RETURN CODE of =",ldaIaal050.getIa_Aian026_Linkage_Return_Code());                                              //Natural: WRITE '* and returned with an RETURN CODE of =' RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal050.getIa_Aian026_Linkage_Auv_Returned().equals(getZero())))                                                                                 //Natural: IF AUV-RETURNED = 0
        {
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "*                 WARNING                                *");                                                                          //Natural: WRITE '*                 WARNING                                *'
            if (Global.isEscape()) return;
            getReports().write(0, "*                                                        *");                                                                          //Natural: WRITE '*                                                        *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Called 'AIAN026' to Retrieve the Annuity unit value    *");                                                                          //Natural: WRITE '* Called "AIAN026" to Retrieve the Annuity unit value    *'
            if (Global.isEscape()) return;
            getReports().write(0, "* and returned with an ANNUITY-UNIT-VALUE of '0'         *");                                                                          //Natural: WRITE '* and returned with an ANNUITY-UNIT-VALUE of "0"         *'
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Call_Type());                                                                               //Natural: WRITE '*' '=' IA-CALL-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Fund_Code());                                                                               //Natural: WRITE '*' '=' IA-FUND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Reval_Methd());                                                                             //Natural: WRITE '*' '=' IA-REVAL-METHD
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Issue_Date());                                                                              //Natural: WRITE '*' '=' IA-ISSUE-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*","=",ldaIaal050.getIa_Aian026_Linkage_Ia_Check_Date());                                                                              //Natural: WRITE '*' '=' IA-CHECK-DATE
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Date Returned               =",ldaIaal050.getIa_Aian026_Linkage_Auv_Date_Return());                                               //Natural: WRITE '*' ' Date Returned               =' AUV-DATE-RETURN
            if (Global.isEscape()) return;
            getReports().write(0, "*"," Returned Annuity unit value =",ldaIaal050.getIa_Aian026_Linkage_Auv_Returned());                                                  //Natural: WRITE '*' ' Returned Annuity unit value =' AUV-RETURNED
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************************************");                                                                          //Natural: WRITE '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RETRIEVE-MONTHLY-ANNUITY-UNIT-VALUE
    }
    private void sub_Read_Extern_File() throws Exception                                                                                                                  //Natural: READ-EXTERN-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET TIAA & CREF ACTIVE PRODUCTS
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_J.setValue(1);                                                                                                                                                //Natural: ASSIGN #J := 1
        pnd_K.setValue(11);                                                                                                                                               //Natural: ASSIGN #K := 11
        FOR05:                                                                                                                                                            //Natural: FOR #I 4 TO 23
        for (pnd_I.setValue(4); condition(pnd_I.lessOrEqual(23)); pnd_I.nadd(1))
        {
            if (condition(DbsUtil.maskMatches(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I),"NN")))                                                          //Natural: IF #IA-STD-NM-CD ( #I ) = MASK ( NN )
            {
                if (condition(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).greaterOrEqual(1) && pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I).lessOrEqual(11))) //Natural: IF VAL ( #IA-STD-NM-CD ( #I ) ) = 01 THRU 11
                {
                    fund_Code_2.getValue(pnd_J).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I));                                                     //Natural: ASSIGN FUND-CODE-2 ( #J ) := #IA-STD-NM-CD ( #I )
                    fund_Code_1.getValue(pnd_J).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                                  //Natural: ASSIGN FUND-CODE-1 ( #J ) := #IA-STD-ALPHA-CD ( #I )
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    fund_Code_2.getValue(pnd_K).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(pnd_I));                                                     //Natural: ASSIGN FUND-CODE-2 ( #K ) := #IA-STD-NM-CD ( #I )
                    fund_Code_1.getValue(pnd_K).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Alpha_Cd().getValue(pnd_I));                                                  //Natural: ASSIGN FUND-CODE-1 ( #K ) := #IA-STD-ALPHA-CD ( #I )
                    pnd_K.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #K
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            DbsUtil.callnat(Iaan051a.class , getCurrentProcessState(), fund_Code_2.getValue(pnd_I), pnd_Parm_Desc, pnd_Cmpny_Desc, pnd_Parm_Len);                         //Natural: CALLNAT 'IAAN051A' FUND-CODE-2 ( #I ) #PARM-DESC #CMPNY-DESC #PARM-LEN
            if (condition(Global.isEscape())) return;
            fund_Desc.getValue(pnd_I).setValue(pnd_Parm_Desc_Pnd_Parm_Desc_6);                                                                                            //Natural: ASSIGN FUND-DESC ( #I ) := #PARM-DESC-6
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(13),"PENDED CONTRACTS REPORT FOR",             //Natural: WRITE ( 1 ) //// 'PROGRAM:' *PROGRAM 13X 'PENDED CONTRACTS REPORT FOR' #PAYMENT-DUE-DTE
                        pnd_Payment_Due_Dte);
                    //*    *DATX (EM=MM/YYYY)
                    //*  10/09
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(13),"PENDED CONTRACTS REPORT IN STATE/COUNTRY ORDER FOR", //Natural: WRITE ( 2 ) //// 'PROGRAM:' *PROGRAM 13X 'PENDED CONTRACTS REPORT IN STATE/COUNTRY ORDER FOR' #PAYMENT-DUE-DTE / 'State/Country:' #DESC-W /
                        pnd_Payment_Due_Dte,NEWLINE,"State/Country:",work_Rec_Pnd_Desc_W,NEWLINE);
                    //*    *DATX (EM=MM/YYYY)
                    //*  ADDED 10/09 START
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(10),"PENDED CONTRACTS BY FUND SUMMARY REPORT",NEWLINE,new  //Natural: WRITE ( 3 ) NOTITLE //// 'PROGRAM:' *PROGRAM 10X 'PENDED CONTRACTS BY FUND SUMMARY REPORT' / 28X '        CHECK DATE:' #PAYMENT-DUE-DTE
                        ColumnSpacing(28),"        CHECK DATE:",pnd_Payment_Due_Dte);
                    if (condition(pnd_Input_Record_Work_2_Pnd_Call_Type.equals("P")))                                                                                     //Natural: IF #CALL-TYPE = 'P'
                    {
                        getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(27),"(Calculation based on Payment Factor)");                                        //Natural: WRITE ( 3 ) 27X '(Calculation based on Payment Factor)'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(23),"(Calculation based on Latest Available Factor)");                               //Natural: WRITE ( 3 ) 23X '(Calculation based on Latest Available Factor)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "ERROR",NEWLINE,"=",iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde,"=",iaa_Cntrct_Cntrct_Ppcn_Nbr,"=",iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, //Natural: WRITE 'ERROR' / '=' CNTRCT-PEND-CDE '=' CNTRCT-PPCN-NBR '=' CNTRCT-PART-PAYEE-CDE / '=' #I '=' #J '=' TIAA-CMPNY-FUND-CDE
            NEWLINE,"=",pnd_I,"=",pnd_J,"=",iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        //*  ADDED 10/09 END
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=132");
        Global.format(2, "PS=55 LS=132");
        Global.format(3, "PS=00 LS=132");

        getReports().setDisplayColumns(1, "//Cntract",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr,"//Pyee",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde, new ReportEditMask ("99"),"//Opt",
        		iaa_Cntrct_Cntrct_Optn_Cde,"/Cur/Cde",
        		iaa_Cntrct_Cntrct_Crrncy_Cde,"/Pend/Date",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte, new ReportEditMask ("9999-99"),"/Pend/Cde",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde,"//Mode",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind,"/Rsd/Cde",
        		iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde,"//Residency-Text",
        		pnd_Desc,"/Fnl-Per-Pay/Date",
        		iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte, new ReportEditMask ("9999-99"),"/First-Due/Date",
        		iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte, new ReportEditMask ("9999-99"),"/Guaranteed/Pmt-Amt",
        		tiaa_Tot_Per_Amt_S,"/Dividend/Pmt-Amt",
        		tiaa_Tot_Div_Amt_S,"//Total-Amt",
        		pnd_Total);
        getReports().setDisplayColumns(2, "//Cntract",
        		work_Rec_Cntrct_Part_Ppcn_Nbr_W,"//Pyee",
        		work_Rec_Cntrct_Part_Payee_Cde_W, new ReportEditMask ("99"),"//Opt",
        		work_Rec_Cntrct_Optn_Cde_W,"/Cur/Cde",
        		work_Rec_Cntrct_Crrncy_Cde_W,"/Pend/Date",
        		work_Rec_Cntrct_Pend_Dte_W, new ReportEditMask ("9999-99"),"/Pend/Cde",
        		work_Rec_Cntrct_Pend_Cde_W,"//Mode",
        		work_Rec_Cntrct_Mode_Ind_W,"/Rsd/Cde",
        		work_Rec_Prtcpnt_Rsdncy_Cde_W,"//Residency-Text",
        		work_Rec_Pnd_Desc_W,"/Fnl-Per-Pay/Date",
        		work_Rec_Cntrct_Final_Per_Pay_Dte_W, new ReportEditMask ("9999-99"),"/First-Due/Date",
        		work_Rec_Cntrct_First_Pymnt_Due_Dte_W, new ReportEditMask ("9999-99"),"/Guaranteed/Pmt-Amt",
        		work_Rec_Tiaa_Tot_Per_Amt_W,"/Dividend/Pmt-Amt",
        		work_Rec_Tiaa_Tot_Div_Amt_W,"//Total-Amt",
        		work_Rec_Pnd_Total_W);
    }
}
