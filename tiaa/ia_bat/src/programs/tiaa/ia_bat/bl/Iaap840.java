/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:56 PM
**        * FROM NATURAL PROGRAM : Iaap840
************************************************************
**        * FILE NAME            : Iaap840.java
**        * CLASS NAME           : Iaap840
**        * INSTANCE NAME        : Iaap840
************************************************************
************************************************************************
*
* PROGRAM:- IAAP840
* DATE   :- 06/15/2000
* AUTHOR :- JEFF BERINGER
*
* HISTORY
* -------
* 04/2106 - COR NAAD REDIRECT TO MDM
* 04/2017 OS PIN EXPANSION CHANGES MARKED 082017.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap840 extends BLNatBase
{
    // Data Areas
    private LdaIaal840 ldaIaal840;
    private PdaMdma101 pdaMdma101;
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;

    private DbsGroup iaa_Cntrct__R_Field_1;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Yyyy;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Mm;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;

    private DbsGroup iaa_Cntrct__R_Field_2;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd_A;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Prtcpnt_Tax_Id_Nbr;
    private DbsField cpr_Cpr_Id_Nbr;
    private DbsField pnd_Type_Call;
    private DbsField pnd_Res;
    private DbsField pnd_Residency;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_3;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde_A;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_4;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_2_3;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField pnd_Date_Record;
    private DbsField pnd_Cntl_Date;

    private DbsGroup pnd_Cntl_Date__R_Field_5;
    private DbsField pnd_Cntl_Date_Pnd_Cntl_Date_Mm;
    private DbsField pnd_Cntl_Date_Pnd_Filler_1;
    private DbsField pnd_Cntl_Date_Pnd_Cntl_Date_Dd;
    private DbsField pnd_Cntl_Date_Pnd_Filler_2;
    private DbsField pnd_Cntl_Date_Pnd_Cntl_Date_Yyyy;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_6;
    private DbsField pnd_Date_Pnd_Date_A;

    private DbsGroup pnd_Date__R_Field_7;
    private DbsField pnd_Date_Pnd_Date_Cc_A;
    private DbsField pnd_Date_Pnd_Date_Yy_A;
    private DbsField pnd_Date_Pnd_Date_Mm_A;
    private DbsField pnd_Date_Pnd_Date_Dd_A;
    private DbsField pnd_Print_Contract;
    private DbsField pnd_Print_Date;
    private DbsField pnd_Fund;
    private DbsField pnd_Total_Records;
    private DbsField pnd_Total_Contracts;
    private DbsField pnd_Total_Tiaa_Per_Pay_Amt;
    private DbsField pnd_Total_Tiaa_Units_Cnt;
    private DbsField pnd_Todays_Date;
    private DbsField pnd_Number_Days;
    private DbsField pnd_Code_State;

    private DbsGroup pnd_Code_State__R_Field_8;
    private DbsField pnd_Code_State_Pnd_Code_N;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Desc;
    private DbsField pnd_Name;
    private DbsField pnd_Cntrct_Ppcn_Nbr_Key;

    private DbsGroup pnd_Cntrct_Ppcn_Nbr_Key__R_Field_9;
    private DbsField pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Prefix;
    private DbsField pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ppcn;
    private DbsField pnd_Payee;

    private DbsGroup pnd_Payee__R_Field_10;
    private DbsField pnd_Payee_Pnd_Payee_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal840 = new LdaIaal840();
        registerRecord(ldaIaal840);
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaAdspda_M = new PdaAdspda_M(localVariables);

        // Local Variables

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");

        iaa_Cntrct__R_Field_1 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct__R_Field_1", "REDEFINE", iaa_Cntrct_Cntrct_Issue_Dte);
        iaa_Cntrct_Cntrct_Issue_Dte_Yyyy = iaa_Cntrct__R_Field_1.newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Yyyy", "CNTRCT-ISSUE-DTE-YYYY", FieldType.STRING, 
            4);
        iaa_Cntrct_Cntrct_Issue_Dte_Mm = iaa_Cntrct__R_Field_1.newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Mm", "CNTRCT-ISSUE-DTE-MM", FieldType.STRING, 
            2);
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");

        iaa_Cntrct__R_Field_2 = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct__R_Field_2", "REDEFINE", iaa_Cntrct_Cntrct_Issue_Dte_Dd);
        iaa_Cntrct_Cntrct_Issue_Dte_Dd_A = iaa_Cntrct__R_Field_2.newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd_A", "CNTRCT-ISSUE-DTE-DD-A", FieldType.STRING, 
            2);
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Prtcpnt_Tax_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PRTCPNT_TAX_ID_NBR");
        cpr_Cpr_Id_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        registerRecord(vw_cpr);

        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_Res = localVariables.newFieldInRecord("pnd_Res", "#RES", FieldType.STRING, 2);
        pnd_Residency = localVariables.newFieldInRecord("pnd_Residency", "#RESIDENCY", FieldType.STRING, 20);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_3 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_3", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde_A = iaa_Tiaa_Fund_Rcrd__R_Field_3.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde_A", "TIAA-CNTRCT-PAYEE-CDE-A", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_4 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_4", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_1 = iaa_Tiaa_Fund_Rcrd__R_Field_4.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_1", "TIAA-CMPNY-FUND-CDE-1", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_2_3 = iaa_Tiaa_Fund_Rcrd__R_Field_4.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_2_3", "TIAA-CMPNY-FUND-CDE-2-3", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        pnd_Date_Record = localVariables.newFieldInRecord("pnd_Date_Record", "#DATE-RECORD", FieldType.DATE);
        pnd_Cntl_Date = localVariables.newFieldInRecord("pnd_Cntl_Date", "#CNTL-DATE", FieldType.STRING, 10);

        pnd_Cntl_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntl_Date__R_Field_5", "REDEFINE", pnd_Cntl_Date);
        pnd_Cntl_Date_Pnd_Cntl_Date_Mm = pnd_Cntl_Date__R_Field_5.newFieldInGroup("pnd_Cntl_Date_Pnd_Cntl_Date_Mm", "#CNTL-DATE-MM", FieldType.STRING, 
            2);
        pnd_Cntl_Date_Pnd_Filler_1 = pnd_Cntl_Date__R_Field_5.newFieldInGroup("pnd_Cntl_Date_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Cntl_Date_Pnd_Cntl_Date_Dd = pnd_Cntl_Date__R_Field_5.newFieldInGroup("pnd_Cntl_Date_Pnd_Cntl_Date_Dd", "#CNTL-DATE-DD", FieldType.STRING, 
            2);
        pnd_Cntl_Date_Pnd_Filler_2 = pnd_Cntl_Date__R_Field_5.newFieldInGroup("pnd_Cntl_Date_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Cntl_Date_Pnd_Cntl_Date_Yyyy = pnd_Cntl_Date__R_Field_5.newFieldInGroup("pnd_Cntl_Date_Pnd_Cntl_Date_Yyyy", "#CNTL-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Date__R_Field_6", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_A = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_A", "#DATE-A", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Date__R_Field_7", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Cc_A = pnd_Date__R_Field_7.newFieldInGroup("pnd_Date_Pnd_Date_Cc_A", "#DATE-CC-A", FieldType.STRING, 2);
        pnd_Date_Pnd_Date_Yy_A = pnd_Date__R_Field_7.newFieldInGroup("pnd_Date_Pnd_Date_Yy_A", "#DATE-YY-A", FieldType.STRING, 2);
        pnd_Date_Pnd_Date_Mm_A = pnd_Date__R_Field_7.newFieldInGroup("pnd_Date_Pnd_Date_Mm_A", "#DATE-MM-A", FieldType.STRING, 2);
        pnd_Date_Pnd_Date_Dd_A = pnd_Date__R_Field_7.newFieldInGroup("pnd_Date_Pnd_Date_Dd_A", "#DATE-DD-A", FieldType.STRING, 2);
        pnd_Print_Contract = localVariables.newFieldInRecord("pnd_Print_Contract", "#PRINT-CONTRACT", FieldType.STRING, 11);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.STRING, 10);
        pnd_Fund = localVariables.newFieldInRecord("pnd_Fund", "#FUND", FieldType.STRING, 6);
        pnd_Total_Records = localVariables.newFieldInRecord("pnd_Total_Records", "#TOTAL-RECORDS", FieldType.NUMERIC, 10);
        pnd_Total_Contracts = localVariables.newFieldInRecord("pnd_Total_Contracts", "#TOTAL-CONTRACTS", FieldType.NUMERIC, 5);
        pnd_Total_Tiaa_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Per_Pay_Amt", "#TOTAL-TIAA-PER-PAY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Total_Tiaa_Units_Cnt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Units_Cnt", "#TOTAL-TIAA-UNITS-CNT", FieldType.NUMERIC, 9, 3);
        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.STRING, 10);
        pnd_Number_Days = localVariables.newFieldInRecord("pnd_Number_Days", "#NUMBER-DAYS", FieldType.NUMERIC, 3);
        pnd_Code_State = localVariables.newFieldInRecord("pnd_Code_State", "#CODE-STATE", FieldType.STRING, 2);

        pnd_Code_State__R_Field_8 = localVariables.newGroupInRecord("pnd_Code_State__R_Field_8", "REDEFINE", pnd_Code_State);
        pnd_Code_State_Pnd_Code_N = pnd_Code_State__R_Field_8.newFieldInGroup("pnd_Code_State_Pnd_Code_N", "#CODE-N", FieldType.NUMERIC, 2);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 20);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_Cntrct_Ppcn_Nbr_Key = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr_Key", "#CNTRCT-PPCN-NBR-KEY", FieldType.STRING, 10);

        pnd_Cntrct_Ppcn_Nbr_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Cntrct_Ppcn_Nbr_Key__R_Field_9", "REDEFINE", pnd_Cntrct_Ppcn_Nbr_Key);
        pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Prefix = pnd_Cntrct_Ppcn_Nbr_Key__R_Field_9.newFieldInGroup("pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Prefix", "#PREFIX", FieldType.STRING, 
            2);
        pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Ppcn_Nbr_Key__R_Field_9.newFieldInGroup("pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Cntrct_Ppcn_Nbr", 
            "#CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Ppcn = localVariables.newFieldInRecord("pnd_Ppcn", "#PPCN", FieldType.STRING, 8);
        pnd_Payee = localVariables.newFieldInRecord("pnd_Payee", "#PAYEE", FieldType.STRING, 2);

        pnd_Payee__R_Field_10 = localVariables.newGroupInRecord("pnd_Payee__R_Field_10", "REDEFINE", pnd_Payee);
        pnd_Payee_Pnd_Payee_N = pnd_Payee__R_Field_10.newFieldInGroup("pnd_Payee_Pnd_Payee_N", "#PAYEE-N", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1_View.reset();
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();

        ldaIaal840.initializeValues();

        localVariables.reset();
        pnd_Type_Call.setInitialValue("R");
        pnd_Return_Code.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap840() throws Exception
    {
        super("Iaap840");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP840", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  RC - OPEN MQ 04/2016                                                                                                                                         //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'DC '
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC ", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ01")))
        {
            getReports().write(0, "TODAYGS DATE ",iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte);                                                                                //Natural: WRITE 'TODAY''S DATE ' CNTRL-TODAYS-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Todays_Date.setValueEdited(iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte,new ReportEditMask("MM/DD/YYYY"));                                                      //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = MM/DD/YYYY ) TO #TODAYS-DATE
            //*  COMPRESS #CNTL-DATE-MM '/' #CNTL-DATE-DD '/20' #CNTL-DATE-YY
            //*    INTO  #TODAYS-DATE LEAVE NO SPACE
            //*  MOVE #CNTL-DATE-MM     TO #DATE-MM-A
            //*  MOVE #CNTL-DATE-DD     TO #DATE-DD-A
            //*  MOVE #CNTL-DATE-YY     TO #DATE-YY-A
            //*  IF #CNTL-DATE-YY < '50'
            //*    MOVE  '20'           TO #DATE-CC-A
            //*  ELSE
            //*    MOVE  '19'           TO #DATE-CC-A
            //*  END-IF
            //*  MOVE EDITED #DATE      TO #CNTL-DATE-D (EM=YYYYMMDD)
            //*  MOVE 'IO' TO #PREFIX
                                                                                                                                                                          //Natural: PERFORM #READ-IAA-CNTRCT-RECORD
            sub_Pnd_Read_Iaa_Cntrct_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Total_Tiaa_Per_Pay_Amt.greater(getZero()) || pnd_Total_Tiaa_Units_Cnt.greater(getZero())))                                                  //Natural: IF #TOTAL-TIAA-PER-PAY-AMT > 0 OR #TOTAL-TIAA-UNITS-CNT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(4),"TOTAL CONTRACT PAYEES ",new ColumnSpacing(2),pnd_Total_Contracts,                //Natural: WRITE ( 1 ) / 04X 'TOTAL CONTRACT PAYEES ' 02X #TOTAL-CONTRACTS ( EM = ZZ,ZZZ ) //
                    new ReportEditMask ("ZZ,ZZZ"),NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE);                                                                                                      //Natural: WRITE ( 1 ) /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE," JOB ENDED SUCCESFULLY                           ",NEWLINE);                                                  //Natural: WRITE ( 1 ) ' JOB ENDED SUCCESFULLY                           ' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE," NO SPIA CONTRACT WITH FREE LOOK PRODUCT         ",NEWLINE);                                                  //Natural: WRITE ( 1 ) ' NO SPIA CONTRACT WITH FREE LOOK PRODUCT         ' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* RC - CLOSE MQ  04/2016
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-IAA-CNTRCT-RECORD
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-IAA-TIAA-FUND-RECORD
        //* *****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #READ-NAME-ADDRESS
        //* *****************************************************************
        //* *#I-PIN             := CPR.CPR-ID-NBR
        //* *CALLNAT 'MDMN100A' #MDMA100
        //* *IF #MDMA100.#O-RETURN-CODE EQ '0000'
        //* ************************  O N   E R R O R  ***************************
        //* *
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Pnd_Read_Iaa_Cntrct_Record() throws Exception                                                                                                        //Natural: #READ-IAA-CNTRCT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM 'IG000000'
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", "IG000000", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.greater("W0249999")))                                                                                                //Natural: IF CNTRCT-PPCN-NBR GT 'W0249999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition((iaa_Cntrct_Cntrct_First_Annt_Dod_Dte.greater(getZero()) && iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte.greater(getZero())) || iaa_Cntrct_Cntrct_Issue_Dte.less(201601)  //Natural: IF ( CNTRCT-FIRST-ANNT-DOD-DTE GT 0 AND CNTRCT-SCND-ANNT-DOD-DTE GT 0 ) OR CNTRCT-ISSUE-DTE LT 201601 OR CNTRCT-ORGN-CDE NE 40
                || iaa_Cntrct_Cntrct_Orgn_Cde.notEquals(40)))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Records.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-RECORDS
            //*   FOR TESTING ONLY TO OBTAIN DATA
            //*  IF  CNTRCT-ORGN-CDE NE 40                    /*  PA-SELECT FUNDS ONLY
            //*   PA-SELECT FUNDS ONLY
            if (condition(iaa_Cntrct_Cntrct_Orgn_Cde.equals(40)))                                                                                                         //Natural: IF CNTRCT-ORGN-CDE = 40
            {
                if (condition(iaa_Cntrct_Cntrct_Issue_Dte_Dd_A.equals("00")))                                                                                             //Natural: IF CNTRCT-ISSUE-DTE-DD-A = '00'
                {
                    iaa_Cntrct_Cntrct_Issue_Dte_Dd_A.setValue("01");                                                                                                      //Natural: MOVE '01' TO CNTRCT-ISSUE-DTE-DD-A
                }                                                                                                                                                         //Natural: END-IF
                pnd_Print_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, iaa_Cntrct_Cntrct_Issue_Dte_Mm, "/", iaa_Cntrct_Cntrct_Issue_Dte_Dd_A,            //Natural: COMPRESS CNTRCT-ISSUE-DTE-MM '/' CNTRCT-ISSUE-DTE-DD-A '/' CNTRCT-ISSUE-DTE-YYYY INTO #PRINT-DATE LEAVING NO SPACE
                    "/", iaa_Cntrct_Cntrct_Issue_Dte_Yyyy));
                pnd_Date_Record.setValueEdited(new ReportEditMask("MM/DD/YYYY"),pnd_Print_Date);                                                                          //Natural: MOVE EDITED #PRINT-DATE TO #DATE-RECORD ( EM = MM/DD/YYYY )
                if (condition(iaa_Cntrct_Cntrct_Issue_Dte_Dd_A.equals("00")))                                                                                             //Natural: IF CNTRCT-ISSUE-DTE-DD-A = '00'
                {
                    pnd_Date_Pnd_Date_Dd_A.setValue(1);                                                                                                                   //Natural: MOVE 01 TO #DATE-DD-A
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Date_Pnd_Date_Dd_A.setValue(iaa_Cntrct_Cntrct_Issue_Dte_Dd_A);                                                                                    //Natural: MOVE CNTRCT-ISSUE-DTE-DD-A TO #DATE-DD-A
                }                                                                                                                                                         //Natural: END-IF
                //*  04/2016 - START
                vw_cpr.startDatabaseRead                                                                                                                                  //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM CNTRCT-PPCN-NBR
                (
                "READ03",
                new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", iaa_Cntrct_Cntrct_Ppcn_Nbr, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
                );
                READ03:
                while (condition(vw_cpr.readNextRow("READ03")))
                {
                    if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cntrct_Cntrct_Ppcn_Nbr)))                                                                        //Natural: IF CNTRCT-PART-PPCN-NBR NE CNTRCT-PPCN-NBR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(!(cpr_Cntrct_Part_Ppcn_Nbr.equals(iaa_Cntrct_Cntrct_Ppcn_Nbr))))                                                                        //Natural: ACCEPT IF CNTRCT-PART-PPCN-NBR = CNTRCT-PPCN-NBR
                    {
                        continue;
                    }
                    if (condition(cpr_Cntrct_Actvty_Cde.notEquals(9)))                                                                                                    //Natural: IF CNTRCT-ACTVTY-CDE NE 9
                    {
                                                                                                                                                                          //Natural: PERFORM #READ-IAA-TIAA-FUND-RECORD
                        sub_Pnd_Read_Iaa_Tiaa_Fund_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  04/2016 - END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_Iaa_Tiaa_Fund_Record() throws Exception                                                                                                     //Natural: #READ-IAA-TIAA-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        vw_iaa_Tiaa_Fund_Rcrd.startDatabaseRead                                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM CNTRCT-PART-PPCN-NBR
        (
        "READ04",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", cpr_Cntrct_Part_Ppcn_Nbr, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        READ04:
        while (condition(vw_iaa_Tiaa_Fund_Rcrd.readNextRow("READ04")))
        {
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.notEquals(cpr_Cntrct_Part_Ppcn_Nbr) || iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.notEquals(cpr_Cntrct_Part_Payee_Cde))) //Natural: IF TIAA-CNTRCT-PPCN-NBR NE CNTRCT-PART-PPCN-NBR OR TIAA-CNTRCT-PAYEE-CDE NE CNTRCT-PART-PAYEE-CDE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR NEXT LINE IS FOR TESTING PURPOSE ONLY
            //*  IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE-2-3 NE '41'
            if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_2_3.equals("41")))                                                                                       //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE-2-3 = '41'
            {
                pnd_Res.setValue(iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde.getSubstring(2,2));                                                                                //Natural: MOVE SUBSTR ( IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE,2,2 ) TO #RES
                pnd_Code_State.setValue(iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde.getSubstring(2,2));                                                                         //Natural: MOVE SUBSTR ( IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE,2,2 ) TO #CODE-STATE
                pnd_Cntrct_Ppcn_Nbr_Key_Pnd_Cntrct_Ppcn_Nbr.setValue(iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr);                                                            //Natural: MOVE IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR TO #CNTRCT-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM #READ-NAME-ADDRESS
                sub_Pnd_Read_Name_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Res, pnd_Residency);                          //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL #RES #RESIDENCY
                if (condition(Global.isEscape())) return;
                //*    IF #CODE NE ' '
                //*      TBLDCODA.#MODE := 'V'
                //*      TBLDCODA.#TABLE-ID := 03
                //*      CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
                //*    END-IF
                if (condition(iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde_1.equals("U")))                                                                                      //Natural: IF TIAA-CMPNY-FUND-CDE-1 = 'U'
                {
                    pnd_Fund.setValue("PAFL-A");                                                                                                                          //Natural: MOVE 'PAFL-A' TO #FUND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fund.setValue("PAFL-M");                                                                                                                          //Natural: MOVE 'PAFL-M' TO #FUND
                }                                                                                                                                                         //Natural: END-IF
                pnd_Number_Days.compute(new ComputeParameters(false, pnd_Number_Days), iaa_Cntrl_Rcrd_1_View_Cntrl_Todays_Dte.subtract(pnd_Date_Record));                 //Natural: SUBTRACT #DATE-RECORD FROM CNTRL-TODAYS-DTE GIVING #NUMBER-DAYS
                pnd_Print_Contract.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr, "-", iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde_A)); //Natural: COMPRESS IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR '-' IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE-A INTO #PRINT-CONTRACT LEAVING NO SPACE
                getReports().display(1, "//Contract",                                                                                                                     //Natural: DISPLAY ( 1 ) '//Contract' #PRINT-CONTRACT '//Name' #NAME '//Fund' #FUND '//Amount' IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT ( EM = Z,ZZZ,ZZZ.99 ) '//Units' IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 ) ( EM = Z,ZZZ.999 ) 2X '/Issue/State' #RESIDENCY 2X '/# of Days/Allowed' #DAYS ( #CODE-N ) 2X '/Issue/Date' #PRINT-DATE '//Days' #NUMBER-DAYS
                		pnd_Print_Contract,"//Name",
                		pnd_Name,"//Fund",
                		pnd_Fund,"//Amount",
                		iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"//Units",
                		iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1), new ReportEditMask ("Z,ZZZ.999"),new ColumnSpacing(2),"/Issue/State",
                		pnd_Residency,new ColumnSpacing(2),"/# of Days/Allowed",
                		ldaIaal840.getPnd_State_Table_Pnd_Days().getValue(pnd_Code_State_Pnd_Code_N),new ColumnSpacing(2),"/Issue/Date",
                		pnd_Print_Date,"//Days",
                		pnd_Number_Days);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Contracts.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-CONTRACTS
                pnd_Total_Tiaa_Per_Pay_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt);                                                                                     //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT TO #TOTAL-TIAA-PER-PAY-AMT
                pnd_Total_Tiaa_Units_Cnt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt.getValue(1));                                                                             //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-UNITS-CNT ( 1 ) TO #TOTAL-TIAA-UNITS-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Read_Name_Address() throws Exception                                                                                                             //Natural: #READ-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(cpr_Cpr_Id_Nbr);                                                                                               //Natural: ASSIGN #I-PIN-N12 := CPR.CPR-ID-NBR
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  082017 END
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            pnd_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name()));                               //Natural: COMPRESS #O-FIRST-NAME #O-LAST-NAME INTO #NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ NA-NAME-ADDRESS BY CNTRCT-TYPE-CORR-CNTRCT-KEY
        //*     STARTING FROM #CNTRCT-PPCN-NBR-KEY
        //* *
        //*   IF NA-NAME-ADDRESS.CNTRCT-NMBR NE IAA-CNTRCT.CNTRCT-PPCN-NBR
        //*     #NAME := 'COULD NOT FIND NAME '
        //*     ESCAPE ROUTINE
        //*   END-IF
        //* *
        //*   IF NA-NAME-ADDRESS.CNTRCT-NMBR = #CNTRCT-PPCN-NBR AND
        //*     NA-NAME-ADDRESS.CNTRCT-PAYEE-CDE =
        //*       IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE
        //*     IF NA-NAME-ADDRESS.CNTRCT-NAME-LAST > ' '
        //*       COMPRESS NA-NAME-ADDRESS.CNTRCT-NAME-FIRST
        //*         NA-NAME-ADDRESS.CNTRCT-NAME-LAST  INTO #NAME
        //*     ELSE
        //*       MOVE NA-NAME-ADDRESS.CNTRCT-NAME-FREE TO #NAME
        //*     END-IF
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*  END-READ
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(4),Global.getPROGRAM(),new ColumnSpacing(22),"                IA ADMINISTRATION SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 04X *PROGRAM 22X '                IA ADMINISTRATION SYSTEM' 40X 'DATE : ' *DATU / 33X '         T/L FREE LOOK FUND REPORT FOR' 01X #TODAYS-DATE 32X 'PAGE :   ' *PAGE-NUMBER ( 1 ) //
                        ColumnSpacing(40),"DATE : ",Global.getDATU(),NEWLINE,new ColumnSpacing(33),"         T/L FREE LOOK FUND REPORT FOR",new ColumnSpacing(1),pnd_Todays_Date,new 
                        ColumnSpacing(32),"PAGE :   ",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE);                                                                            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM /
        getReports().write(0, ReportOption.NOHDR,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE);                                                                           //Natural: WRITE NOHDR 'ERROR NUMBER ' *ERROR-NR /
        getReports().write(0, ReportOption.NOHDR,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE);                                                                         //Natural: WRITE NOHDR 'ERROR LINE   ' *ERROR-LINE /
        getReports().write(0, ReportOption.NOHDR,"==================================",NEWLINE);                                                                           //Natural: WRITE NOHDR '==================================' /
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");

        getReports().setDisplayColumns(1, "//Contract",
        		pnd_Print_Contract,"//Name",
        		pnd_Name,"//Fund",
        		pnd_Fund,"//Amount",
        		iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99"),"//Units",
        		iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt, new ReportEditMask ("Z,ZZZ.999"),new ColumnSpacing(2),"/Issue/State",
        		pnd_Residency,new ColumnSpacing(2),"/# of Days/Allowed",
        		ldaIaal840.getPnd_State_Table_Pnd_Days(),new ColumnSpacing(2),"/Issue/Date",
        		pnd_Print_Date,"//Days",
        		pnd_Number_Days);
    }
}
