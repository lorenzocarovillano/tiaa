/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:33:44 PM
**        * FROM NATURAL PROGRAM : Iaap800
************************************************************
**        * FILE NAME            : Iaap800.java
**        * CLASS NAME           : Iaap800
**        * INSTANCE NAME        : Iaap800
************************************************************
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP800  :  READS VSAM LEGAL OVERRIDE FILE AND    *
*      DATE     -  10/95       CREATES A FLAT FILE OF LEGAL OVERRIDE *
*                                                                    *
*                                                                    *
**********************************************************************
*
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap800 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_legal_Override_Rec;
    private DbsField legal_Override_Rec_Lo_Record_Key;

    private DbsGroup legal_Override_Rec__R_Field_1;
    private DbsField legal_Override_Rec_Lo_Contract_Number;
    private DbsField legal_Override_Rec_Lo_Payee_Code;
    private DbsField legal_Override_Rec_Lo_Sequence_No;

    private DbsGroup legal_Override_Rec_Lo_Common_Data;
    private DbsField legal_Override_Rec_Lo_Status_Code;
    private DbsField legal_Override_Rec_Lo_Effective_Date;
    private DbsField legal_Override_Rec_Lo_Entry_Date;
    private DbsField legal_Override_Rec_Lo_Entry_Operator;
    private DbsField legal_Override_Rec_Lo_Activity_Date;
    private DbsField legal_Override_Rec_Lo_Activity_Operator;
    private DbsField legal_Override_Rec_Lo_State_Code;
    private DbsField legal_Override_Rec_Lo_Ia_State_Code;
    private DbsField legal_Override_Rec_Lo_State_Name;
    private DbsField legal_Override_Rec_Filler_1;

    private DbsGroup legal_Override_Rec_Out;
    private DbsField legal_Override_Rec_Out_Lo_Record_Key;

    private DbsGroup legal_Override_Rec_Out__R_Field_2;
    private DbsField legal_Override_Rec_Out_Lo_Contract_Number;
    private DbsField legal_Override_Rec_Out_Lo_Payee_Code;
    private DbsField legal_Override_Rec_Out_Lo_Sequence_No;

    private DbsGroup legal_Override_Rec_Out_Lo_Common_Data;
    private DbsField legal_Override_Rec_Out_Lo_Status_Code;
    private DbsField legal_Override_Rec_Out_Lo_Effective_Date;
    private DbsField legal_Override_Rec_Out_Lo_Entry_Date;
    private DbsField legal_Override_Rec_Out_Lo_Entry_Operator;
    private DbsField legal_Override_Rec_Out_Lo_Activity_Date;
    private DbsField legal_Override_Rec_Out_Lo_Activity_Operator;

    private DbsGroup legal_Override_Rec_Out__R_Field_3;
    private DbsField legal_Override_Rec_Out__Filler1;
    private DbsField legal_Override_Rec_Out_Pnd_Global_Cntry;
    private DbsField legal_Override_Rec_Out_Lo_State_Code;
    private DbsField legal_Override_Rec_Out_Lo_Ia_State_Code;
    private DbsField legal_Override_Rec_Out_Lo_State_Name;
    private DbsField legal_Override_Rec_Out_Pnd_Global_Ind;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Write_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_legal_Override_Rec = new DataAccessProgramView(new NameInfo("vw_legal_Override_Rec", "LEGAL-OVERRIDE-REC"), "LEGAL_OVERRIDE_REC", "LEGAL_OVERRIDE_REC");
        legal_Override_Rec_Lo_Record_Key = vw_legal_Override_Rec.getRecord().newFieldInGroup("legal_Override_Rec_Lo_Record_Key", "LO-RECORD-KEY", FieldType.STRING, 
            13, RepeatingFieldStrategy.None, "LO_RECORD_KEY");

        legal_Override_Rec__R_Field_1 = vw_legal_Override_Rec.getRecord().newGroupInGroup("legal_Override_Rec__R_Field_1", "REDEFINE", legal_Override_Rec_Lo_Record_Key);
        legal_Override_Rec_Lo_Contract_Number = legal_Override_Rec__R_Field_1.newFieldInGroup("legal_Override_Rec_Lo_Contract_Number", "LO-CONTRACT-NUMBER", 
            FieldType.STRING, 8);
        legal_Override_Rec_Lo_Payee_Code = legal_Override_Rec__R_Field_1.newFieldInGroup("legal_Override_Rec_Lo_Payee_Code", "LO-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        legal_Override_Rec_Lo_Sequence_No = legal_Override_Rec__R_Field_1.newFieldInGroup("legal_Override_Rec_Lo_Sequence_No", "LO-SEQUENCE-NO", FieldType.NUMERIC, 
            3);

        legal_Override_Rec_Lo_Common_Data = vw_legal_Override_Rec.getRecord().newGroupInGroup("LEGAL_OVERRIDE_REC_LO_COMMON_DATA", "LO-COMMON-DATA");
        legal_Override_Rec_Lo_Status_Code = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Status_Code", "LO-STATUS-CODE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "LO_STATUS_CODE");
        legal_Override_Rec_Lo_Effective_Date = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Effective_Date", "LO-EFFECTIVE-DATE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LO_EFFECTIVE_DATE");
        legal_Override_Rec_Lo_Entry_Date = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Entry_Date", "LO-ENTRY-DATE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "LO_ENTRY_DATE");
        legal_Override_Rec_Lo_Entry_Operator = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Entry_Operator", "LO-ENTRY-OPERATOR", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "LO_ENTRY_OPERATOR");
        legal_Override_Rec_Lo_Activity_Date = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Activity_Date", "LO-ACTIVITY-DATE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "LO_ACTIVITY_DATE");
        legal_Override_Rec_Lo_Activity_Operator = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Activity_Operator", "LO-ACTIVITY-OPERATOR", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "LO_ACTIVITY_OPERATOR");
        legal_Override_Rec_Lo_State_Code = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_State_Code", "LO-STATE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "LO_STATE_CODE");
        legal_Override_Rec_Lo_Ia_State_Code = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_Ia_State_Code", "LO-IA-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "LO_IA_STATE_CODE");
        legal_Override_Rec_Lo_State_Name = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Lo_State_Name", "LO-STATE-NAME", FieldType.STRING, 
            17, RepeatingFieldStrategy.None, "LO_STATE_NAME");
        legal_Override_Rec_Filler_1 = legal_Override_Rec_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Filler_1", "FILLER-1", FieldType.STRING, 13, 
            RepeatingFieldStrategy.None, "FILLER_1");
        registerRecord(vw_legal_Override_Rec);

        legal_Override_Rec_Out = localVariables.newGroupInRecord("legal_Override_Rec_Out", "LEGAL-OVERRIDE-REC-OUT");
        legal_Override_Rec_Out_Lo_Record_Key = legal_Override_Rec_Out.newFieldInGroup("legal_Override_Rec_Out_Lo_Record_Key", "LO-RECORD-KEY", FieldType.STRING, 
            13);

        legal_Override_Rec_Out__R_Field_2 = legal_Override_Rec_Out.newGroupInGroup("legal_Override_Rec_Out__R_Field_2", "REDEFINE", legal_Override_Rec_Out_Lo_Record_Key);
        legal_Override_Rec_Out_Lo_Contract_Number = legal_Override_Rec_Out__R_Field_2.newFieldInGroup("legal_Override_Rec_Out_Lo_Contract_Number", "LO-CONTRACT-NUMBER", 
            FieldType.STRING, 8);
        legal_Override_Rec_Out_Lo_Payee_Code = legal_Override_Rec_Out__R_Field_2.newFieldInGroup("legal_Override_Rec_Out_Lo_Payee_Code", "LO-PAYEE-CODE", 
            FieldType.NUMERIC, 2);
        legal_Override_Rec_Out_Lo_Sequence_No = legal_Override_Rec_Out__R_Field_2.newFieldInGroup("legal_Override_Rec_Out_Lo_Sequence_No", "LO-SEQUENCE-NO", 
            FieldType.NUMERIC, 3);

        legal_Override_Rec_Out_Lo_Common_Data = legal_Override_Rec_Out.newGroupInGroup("legal_Override_Rec_Out_Lo_Common_Data", "LO-COMMON-DATA");
        legal_Override_Rec_Out_Lo_Status_Code = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Status_Code", "LO-STATUS-CODE", 
            FieldType.STRING, 1);
        legal_Override_Rec_Out_Lo_Effective_Date = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Effective_Date", "LO-EFFECTIVE-DATE", 
            FieldType.STRING, 8);
        legal_Override_Rec_Out_Lo_Entry_Date = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Entry_Date", "LO-ENTRY-DATE", 
            FieldType.STRING, 8);
        legal_Override_Rec_Out_Lo_Entry_Operator = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Entry_Operator", "LO-ENTRY-OPERATOR", 
            FieldType.STRING, 4);
        legal_Override_Rec_Out_Lo_Activity_Date = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Activity_Date", "LO-ACTIVITY-DATE", 
            FieldType.STRING, 8);
        legal_Override_Rec_Out_Lo_Activity_Operator = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Activity_Operator", 
            "LO-ACTIVITY-OPERATOR", FieldType.STRING, 4);

        legal_Override_Rec_Out__R_Field_3 = legal_Override_Rec_Out_Lo_Common_Data.newGroupInGroup("legal_Override_Rec_Out__R_Field_3", "REDEFINE", legal_Override_Rec_Out_Lo_Activity_Operator);
        legal_Override_Rec_Out__Filler1 = legal_Override_Rec_Out__R_Field_3.newFieldInGroup("legal_Override_Rec_Out__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        legal_Override_Rec_Out_Pnd_Global_Cntry = legal_Override_Rec_Out__R_Field_3.newFieldInGroup("legal_Override_Rec_Out_Pnd_Global_Cntry", "#GLOBAL-CNTRY", 
            FieldType.STRING, 3);
        legal_Override_Rec_Out_Lo_State_Code = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_State_Code", "LO-STATE-CODE", 
            FieldType.STRING, 2);
        legal_Override_Rec_Out_Lo_Ia_State_Code = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_Ia_State_Code", "LO-IA-STATE-CODE", 
            FieldType.STRING, 2);
        legal_Override_Rec_Out_Lo_State_Name = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Lo_State_Name", "LO-STATE-NAME", 
            FieldType.STRING, 17);
        legal_Override_Rec_Out_Pnd_Global_Ind = legal_Override_Rec_Out_Lo_Common_Data.newFieldInGroup("legal_Override_Rec_Out_Pnd_Global_Ind", "#GLOBAL-IND", 
            FieldType.STRING, 1);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Write_Ctr = localVariables.newFieldInRecord("pnd_Write_Ctr", "#WRITE-CTR", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_legal_Override_Rec.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap800() throws Exception
    {
        super("Iaap800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_legal_Override_Rec.startDatabaseRead                                                                                                                           //Natural: READ LEGAL-OVERRIDE-REC BY LO-RECORD-KEY STARTING FROM 'G'
        (
        "READ01",
        new Wc[] { new Wc("LO_RECORD_KEY", ">=", "G", WcType.BY) },
        new Oc[] { new Oc("LO_RECORD_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_legal_Override_Rec.readNextRow("READ01")))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(legal_Override_Rec_Lo_Status_Code.notEquals("A")))                                                                                              //Natural: IF LEGAL-OVERRIDE-REC.LO-STATUS-CODE NE 'A'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(legal_Override_Rec_Lo_Sequence_No.less(99)))                                                                                                //Natural: IF LEGAL-OVERRIDE-REC.LO-SEQUENCE-NO LT 99
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    legal_Override_Rec_Out.reset();                                                                                                                       //Natural: RESET LEGAL-OVERRIDE-REC-OUT
                    legal_Override_Rec_Out.setValuesByName(vw_legal_Override_Rec);                                                                                        //Natural: MOVE BY NAME LEGAL-OVERRIDE-REC TO LEGAL-OVERRIDE-REC-OUT
                                                                                                                                                                          //Natural: PERFORM CHECK-SEQ-NUM
                    sub_Check_Seq_Num();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(1, false, legal_Override_Rec_Out);                                                                                               //Natural: WRITE WORK FILE 1 LEGAL-OVERRIDE-REC-OUT
                    pnd_Write_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WRITE-CTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "PFC RECORDS READ   : ",pnd_Read_Ctr);                                                                                                      //Natural: WRITE 'PFC RECORDS READ   : ' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "PFC RECORDS WRITTEN: ",pnd_Write_Ctr);                                                                                                     //Natural: WRITE 'PFC RECORDS WRITTEN: ' #WRITE-CTR
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SEQ-NUM
    }
    private void sub_Check_Seq_Num() throws Exception                                                                                                                     //Natural: CHECK-SEQ-NUM
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(legal_Override_Rec_Lo_Sequence_No.greaterOrEqual(99) && legal_Override_Rec_Lo_Sequence_No.lessOrEqual(199)))                                        //Natural: IF LEGAL-OVERRIDE-REC.LO-SEQUENCE-NO GE 99 AND LEGAL-OVERRIDE-REC.LO-SEQUENCE-NO LE 199
        {
            legal_Override_Rec_Out_Pnd_Global_Ind.setValue("3");                                                                                                          //Natural: ASSIGN #GLOBAL-IND := '3'
            legal_Override_Rec_Out_Lo_Ia_State_Code.setValue(" ");                                                                                                        //Natural: ASSIGN LEGAL-OVERRIDE-REC-OUT.LO-IA-STATE-CODE := ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(legal_Override_Rec_Lo_Sequence_No.greaterOrEqual(200)))                                                                                             //Natural: IF LEGAL-OVERRIDE-REC.LO-SEQUENCE-NO GE 200
        {
            legal_Override_Rec_Out_Pnd_Global_Ind.setValue(" ");                                                                                                          //Natural: ASSIGN #GLOBAL-IND := ' '
            legal_Override_Rec_Out_Pnd_Global_Cntry.setValue(" ");                                                                                                        //Natural: ASSIGN #GLOBAL-CNTRY := ' '
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
