/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:22:20 PM
**        * FROM NATURAL PROGRAM : Iaap100
************************************************************
**        * FILE NAME            : Iaap100.java
**        * CLASS NAME           : Iaap100
**        * INSTANCE NAME        : Iaap100
************************************************************
************************************************************************
*                                                                      *
*   PROGRAM    -   IAAP100     THIS PROGRAM EXTRACTS FIELDS FROM THE   *
*                              MASTER FILE. IT ELIMINATES NON ACTIVE   *
*                              CONTRACTS AND CERTAIN CONTRACT TYPES.   *
*                              IT PRODUCES A WORK FILE THAT IS USED    *
*                              IN IAAP110A (ANNUAL MODE REPORT)        *
*                                                                      *
*   HISTORY   :- 11/97 LEN B  CHANGED THIS PROGRAM TO WRITE ALL MONTHLY*
*                             FUNDS TO A SECOND WORK FILE THAT IS USED *
*                             IN IAAP110M (MONTHLY MODE REPORT)        *
*                                                                      *
*                                                                      *
*                10/09  MODIFIED LOGIC FOR PA/PA SELECT/TC LIFE
*                       DO SCAN ON 10/09 FOR CHANGES                   *
*                02/01  REMOVED REJECT OF OPTION 22,25,27,28,30 & 23   *
*                09/04  USE OUTPUT-UNITS-CNT TO CAPTURE ORIGIN CODES   *
*                       03 (ISSUE-DATE GT 12/86), 17,18, 37,38 & 40    *
*                       TO REPORT THESE IN IAAP110A                    *
*                       DO SCAN ON 9/04 FOR CHANGES                    *
*                05/17 OS STOWED DUE TO IAAL100 PIN EXP. CHANGES.
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap100 extends BLNatBase
{
    // Data Areas
    private LdaIaal100 ldaIaal100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bypass;
    private DbsField pnd_New_Payee;
    private DbsField pnd_Payment_Due;
    private DbsField pnd_Save_Ppcn_Nbr;
    private DbsField pnd_Save_Cntrct_Actvty_Cde;
    private DbsField pnd_Save_Fund_Ppcn_Nbr;
    private DbsField pnd_Save_Fund_Payee_Cde;
    private DbsField pnd_Ctr;
    private DbsField pnd_Proc_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Other_Ctr;
    private DbsField pnd_Write_Ctr_Annual;
    private DbsField pnd_Write_Ctr_Monthly;
    private DbsField pnd_10_Ctr;
    private DbsField pnd_20_Ctr;
    private DbsField pnd_30_Ctr;
    private DbsField pnd_Head_Trail_Ctr;
    private DbsField return_Code;
    private DbsField pnd_In_Filler_1;

    private DbsGroup pnd_In_Filler_1__R_Field_1;
    private DbsField pnd_In_Filler_1_Pnd_In_Origin_Cde;
    private DbsField pnd_In_Filler_1_Pnd_In_Acctg_Cde;
    private DbsField pnd_Pa_Orgn;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal100 = new LdaIaal100();
        registerRecord(ldaIaal100);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Bypass = localVariables.newFieldInRecord("pnd_Bypass", "#BYPASS", FieldType.BOOLEAN, 1);
        pnd_New_Payee = localVariables.newFieldInRecord("pnd_New_Payee", "#NEW-PAYEE", FieldType.BOOLEAN, 1);
        pnd_Payment_Due = localVariables.newFieldInRecord("pnd_Payment_Due", "#PAYMENT-DUE", FieldType.BOOLEAN, 1);
        pnd_Save_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Ppcn_Nbr", "#SAVE-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Cntrct_Actvty_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Actvty_Cde", "#SAVE-CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1);
        pnd_Save_Fund_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Save_Fund_Ppcn_Nbr", "#SAVE-FUND-PPCN-NBR", FieldType.STRING, 10);
        pnd_Save_Fund_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Fund_Payee_Cde", "#SAVE-FUND-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Proc_Ctr = localVariables.newFieldInRecord("pnd_Proc_Ctr", "#PROC-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Other_Ctr = localVariables.newFieldInRecord("pnd_Other_Ctr", "#OTHER-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Write_Ctr_Annual = localVariables.newFieldInRecord("pnd_Write_Ctr_Annual", "#WRITE-CTR-ANNUAL", FieldType.PACKED_DECIMAL, 9);
        pnd_Write_Ctr_Monthly = localVariables.newFieldInRecord("pnd_Write_Ctr_Monthly", "#WRITE-CTR-MONTHLY", FieldType.PACKED_DECIMAL, 9);
        pnd_10_Ctr = localVariables.newFieldInRecord("pnd_10_Ctr", "#10-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_20_Ctr = localVariables.newFieldInRecord("pnd_20_Ctr", "#20-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_30_Ctr = localVariables.newFieldInRecord("pnd_30_Ctr", "#30-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_Head_Trail_Ctr = localVariables.newFieldInRecord("pnd_Head_Trail_Ctr", "#HEAD-TRAIL-CTR", FieldType.PACKED_DECIMAL, 9);
        return_Code = localVariables.newFieldInRecord("return_Code", "RETURN-CODE", FieldType.NUMERIC, 2);
        pnd_In_Filler_1 = localVariables.newFieldInRecord("pnd_In_Filler_1", "#IN-FILLER-1", FieldType.STRING, 4);

        pnd_In_Filler_1__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Filler_1__R_Field_1", "REDEFINE", pnd_In_Filler_1);
        pnd_In_Filler_1_Pnd_In_Origin_Cde = pnd_In_Filler_1__R_Field_1.newFieldInGroup("pnd_In_Filler_1_Pnd_In_Origin_Cde", "#IN-ORIGIN-CDE", FieldType.STRING, 
            2);
        pnd_In_Filler_1_Pnd_In_Acctg_Cde = pnd_In_Filler_1__R_Field_1.newFieldInGroup("pnd_In_Filler_1_Pnd_In_Acctg_Cde", "#IN-ACCTG-CDE", FieldType.STRING, 
            2);
        pnd_Pa_Orgn = localVariables.newFieldArrayInRecord("pnd_Pa_Orgn", "#PA-ORGN", FieldType.STRING, 2, new DbsArrayController(1, 12));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal100.initializeValues();

        localVariables.reset();
        pnd_Pa_Orgn.getValue(1).setInitialValue("03");
        pnd_Pa_Orgn.getValue(2).setInitialValue("17");
        pnd_Pa_Orgn.getValue(3).setInitialValue("18");
        pnd_Pa_Orgn.getValue(4).setInitialValue("37");
        pnd_Pa_Orgn.getValue(5).setInitialValue("38");
        pnd_Pa_Orgn.getValue(6).setInitialValue("40");
        pnd_Pa_Orgn.getValue(7).setInitialValue("43");
        pnd_Pa_Orgn.getValue(8).setInitialValue("44");
        pnd_Pa_Orgn.getValue(9).setInitialValue("45");
        pnd_Pa_Orgn.getValue(10).setInitialValue("46");
        pnd_Pa_Orgn.getValue(11).setInitialValue("35");
        pnd_Pa_Orgn.getValue(12).setInitialValue("36");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap100() throws Exception
    {
        super("Iaap100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("IAAP100", onError);
        setupReports();
        //*  ZP=OFF SG=OFF
        //*  ZP=OFF SG=OFF                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 56
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, ldaIaal100.getPnd_Input_Record())))
        {
            //*  WRITE '=' #CNTRCT-PPCN-NBR '=' #CNTRCT-PAYEE-CDE '=' #RECORD-CDE
            //*  '=' #TIAA-FUND-CDE
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            if (condition(pnd_Ctr.equals(1)))                                                                                                                             //Natural: IF #CTR = 1
            {
                ldaIaal100.getPnd_W_Rec_3().setValue(ldaIaal100.getPnd_Input_Record_Pnd_W_Check_Date());                                                                  //Natural: MOVE #W-CHECK-DATE TO #W-REC-3
                //*    WRITE '=' #W-CHECK-DATE '=' #CTR
                getWorkFiles().write(3, false, ldaIaal100.getPnd_W_Rec_3());                                                                                              //Natural: WRITE WORK FILE 3 #W-REC-3
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #CTR > 120
            //*   ESCAPE BOTTOM
            //*  END-IF
            if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   CHEADER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   FHEADER")  //Natural: IF #CNTRCT-PPCN-NBR = '   CHEADER' OR = '   FHEADER' OR = '   SHEADER' OR = '99CTRAILER' OR = '99FTRAILER' OR = '99STRAILER'
                || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("   SHEADER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99CTRAILER") 
                || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99FTRAILER") || ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals("99STRAILER")))
            {
                pnd_Head_Trail_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #HEAD-TRAIL-CTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   IF #PROC-CTR < 299
            //*     WRITE #RECORD-CDE
            //*   END-IF
            short decideConditionsMet178 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #RECORD-CDE;//Natural: VALUE 10
            if (condition((ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(10))))
            {
                decideConditionsMet178++;
                pnd_10_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #10-CTR
                pnd_Bypass.reset();                                                                                                                                       //Natural: RESET #BYPASS
                if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Crrncy_Cde().equals(2)))                                                                          //Natural: IF #CNTRCT-CRRNCY-CDE = 2
                {
                    //*        #CNTRCT-OPTN-CDE = 22    OR
                    //*        #CNTRCT-OPTN-CDE = 23    OR
                    //*        #CNTRCT-OPTN-CDE = 25    OR
                    //*        #CNTRCT-OPTN-CDE = 27    OR
                    //*        #CNTRCT-OPTN-CDE = 28    OR
                    //*        #CNTRCT-OPTN-CDE = 30
                    pnd_Bypass.setValue(true);                                                                                                                            //Natural: ASSIGN #BYPASS := TRUE
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                    //*  ADDED 9/04
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Issue_Dte().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Issue_Dte());                             //Natural: ASSIGN #O-CNTRCT-ISSUE-DTE := #CNTRCT-ISSUE-DTE
                    ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Optn_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Optn_Cde());                               //Natural: ASSIGN #O-CNTRCT-OPTN-CDE := #CNTRCT-OPTN-CDE
                    pnd_In_Filler_1.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Filler_1());                                                                              //Natural: ASSIGN #IN-FILLER-1 := #FILLER-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Proc_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #PROC-CTR
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(20))))
            {
                decideConditionsMet178++;
                pnd_20_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #20-CTR
                //*      IF #O-CNTRCT-OPTN-CDE = 25 OR = 27 AND       /* COMMENTED OUT 8/17
                //*        #CNTRCT-PEND-CDE = 'Q'
                //*  WRITE  'IPRO BYPASSED '
                //*         '=' #CNTRCT-PPCN-NBR '=' #CNTRCT-PEND-CDE
                //*        #BYPASS := TRUE
                //*        ADD 1 TO #BYPASS-CTR
                //*      END-IF
                if (condition(pnd_Bypass.getBoolean()))                                                                                                                   //Natural: IF #BYPASS
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Save_Cntrct_Actvty_Cde.reset();                                                                                                                   //Natural: RESET #SAVE-CNTRCT-ACTVTY-CDE
                    if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde().equals(9)))                                                                      //Natural: IF #CNTRCT-ACTVTY-CDE = 9
                    {
                        pnd_Save_Cntrct_Actvty_Cde.setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde());                                                      //Natural: ASSIGN #SAVE-CNTRCT-ACTVTY-CDE := #CNTRCT-ACTVTY-CDE
                        pnd_Bypass_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #BYPASS-CTR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Proc_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #PROC-CTR
                                                                                                                                                                          //Natural: PERFORM SAVE-CPR
                        sub_Save_Cpr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaIaal100.getPnd_Input_Record_Pnd_Record_Cde().equals(30))))
            {
                decideConditionsMet178++;
                pnd_30_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #30-CTR
                //*  ADDED FOLLOWING 9/04
                ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt().reset();                                                                                            //Natural: RESET #O-DDCTN-PER-AMT
                if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1S") || ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1G")))        //Natural: IF #TIAA-FUND-CDE = '1S' OR = '1G'
                {
                    //*        IF #IN-ORIGIN-CDE = '03' OR= '17' OR= '18' OR= '37' OR= '38'
                    //*            OR= '40'                             /* 10/09 START
                    //*          DECIDE ON FIRST #IN-ORIGIN-CDE         /* 10/09
                    //*            VALUE '03'
                    //*              IF  #O-CNTRCT-ISSUE-DTE GT 198612
                    //*                #O-DDCTN-PER-AMT := 3
                    //*              END-IF
                    //*            VALUE '17'
                    //*              #O-DDCTN-PER-AMT := 17
                    //*            VALUE '18'
                    //*              #O-DDCTN-PER-AMT := 18
                    //*            VALUE '37'
                    //*              #O-DDCTN-PER-AMT := 37
                    //*            VALUE '38'
                    //*              #O-DDCTN-PER-AMT := 38
                    //*            VALUE '40'
                    //*              #O-DDCTN-PER-AMT := 40
                    //*            NONE
                    //*              IGNORE
                    //*          END-DECIDE
                    //*  10/09
                    if (condition(pnd_In_Filler_1_Pnd_In_Origin_Cde.equals(pnd_Pa_Orgn.getValue("*"))))                                                                   //Natural: IF #IN-ORIGIN-CDE = #PA-ORGN ( * )
                    {
                        //*  10/09
                        if (condition(pnd_In_Filler_1_Pnd_In_Origin_Cde.equals("03")))                                                                                    //Natural: IF #IN-ORIGIN-CDE = '03'
                        {
                            //*  10/09
                            if (condition(ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Issue_Dte().greater(198612)))                                                      //Natural: IF #O-CNTRCT-ISSUE-DTE GT 198612
                            {
                                ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt().compute(new ComputeParameters(false, ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt()),  //Natural: ASSIGN #O-DDCTN-PER-AMT := VAL ( #IN-ORIGIN-CDE )
                                    pnd_In_Filler_1_Pnd_In_Origin_Cde.val());
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt().compute(new ComputeParameters(false, ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt()),  //Natural: ASSIGN #O-DDCTN-PER-AMT := VAL ( #IN-ORIGIN-CDE )
                                pnd_In_Filler_1_Pnd_In_Origin_Cde.val());
                        }                                                                                                                                                 //Natural: END-IF
                        //*  10/09 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF ADD 9/04
                if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1") || ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1G")           //Natural: IF #TIAA-FUND-CDE = '1' OR = '1G' OR = '1S'
                    || ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1S")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde(), MoveOption.RightJustified);           //Natural: MOVE RIGHT #TIAA-FUND-CDE TO #TIAA-FUND-CDE
                    DbsUtil.examine(new ExamineSource(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde()), new ExamineSearch(" "), new ExamineReplace("0"));              //Natural: EXAMINE #TIAA-FUND-CDE FOR ' ' REPLACE '0'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Bypass.getBoolean() || pnd_Save_Cntrct_Actvty_Cde.equals(9)))                                                                           //Natural: IF #BYPASS OR #SAVE-CNTRCT-ACTVTY-CDE = 9
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SAVE-FUND-SUMM
                    sub_Save_Fund_Summ();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Proc_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PROC-CTR
                }                                                                                                                                                         //Natural: END-IF
                //*     VALUE 40
                //*       IF #BYPASS               OR
                //*          #SAVE-CNTRCT-ACTVTY-CDE = 9
                //*         ADD 1 TO #BYPASS-CTR
                //*       ELSE
                //*         ADD 1 TO #PROC-CTR
                //*         PERFORM SAVE-DEDUCTION
                //*       END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Other_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #OTHER-CTR
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, Global.getPROGRAM(),"FINISHED AT: ",Global.getTIMN());                                                                                      //Natural: WRITE *PROGRAM 'FINISHED AT: ' *TIMN
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ                : ",pnd_Ctr);                                                                                        //Natural: WRITE 'NUMBER OF RECORDS READ                : ' #CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS PROCESSED           : ",pnd_Proc_Ctr);                                                                                   //Natural: WRITE 'NUMBER OF RECORDS PROCESSED           : ' #PROC-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS BYPASSED            : ",pnd_Bypass_Ctr);                                                                                 //Natural: WRITE 'NUMBER OF RECORDS BYPASSED            : ' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "OTHER RECORDS BYPASSED                : ",pnd_Other_Ctr);                                                                                  //Natural: WRITE 'OTHER RECORDS BYPASSED                : ' #OTHER-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "HEADER TRAILER COUNT                  : ",pnd_Head_Trail_Ctr);                                                                             //Natural: WRITE 'HEADER TRAILER COUNT                  : ' #HEAD-TRAIL-CTR
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS WRITTEN ANNUAL      : ",pnd_Write_Ctr_Annual);                                                                           //Natural: WRITE 'NUMBER OF RECORDS WRITTEN ANNUAL      : ' #WRITE-CTR-ANNUAL
        if (Global.isEscape()) return;
        //* LB
        getReports().write(0, "NUMBER OF RECORDS WRITTEN MONTHLY     : ",pnd_Write_Ctr_Monthly);                                                                          //Natural: WRITE 'NUMBER OF RECORDS WRITTEN MONTHLY     : ' #WRITE-CTR-MONTHLY
        if (Global.isEscape()) return;
        //*  WRITE '=' #10-CTR '=' #20-CTR '=' #30-CTR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-CPR
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-FUND-SUMM
        //* ***********************************************************************
        //*  WRITE '=' #TIAA-TOT-PAY-AMT '=' #TIAA-FUND-CDE '=' #CNTRCT-PPCN-NBR
        //*   '=' #CNTRCT-PAYEE-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-DEDUCTION
        //* **********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Save_Cpr() throws Exception                                                                                                                          //Natural: SAVE-CPR
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr());                                           //Natural: ASSIGN #O-CNTRCT-PPCN-NBR := #CNTRCT-PPCN-NBR
        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Mode_Ind().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Mode_Ind());                                           //Natural: ASSIGN #O-CNTRCT-MODE-IND := #CNTRCT-MODE-IND
        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Actvty_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Actvty_Cde());                                       //Natural: ASSIGN #O-CNTRCT-ACTVTY-CDE := #CNTRCT-ACTVTY-CDE
        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Final_Pay_Dte().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Final_Pay_Dte());                                 //Natural: ASSIGN #O-CNTRCT-FINAL-PAY-DTE := #CNTRCT-FINAL-PAY-DTE
        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Curr_Dist_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Curr_Dist_Cde());                                 //Natural: ASSIGN #O-CNTRCT-CURR-DIST-CDE := #CNTRCT-CURR-DIST-CDE
        //*  SAVE-CPR
    }
    private void sub_Save_Fund_Summ() throws Exception                                                                                                                    //Natural: SAVE-FUND-SUMM
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Cmpny_Cde());                                             //Natural: ASSIGN #O-TIAA-CMPNY-CDE := #TIAA-CMPNY-CDE
        if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().equals("1 ")))                                                                                   //Natural: IF #TIAA-FUND-CDE = '1 '
        {
            ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde().setValue("1Z");                                                                                            //Natural: MOVE '1Z' TO #TIAA-FUND-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Fund_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Fund_Cde());                                               //Natural: ASSIGN #O-TIAA-FUND-CDE := #TIAA-FUND-CDE
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Tot_Pay_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Tot_Pay_Amt());                                         //Natural: ASSIGN #O-TIAA-TOT-PAY-AMT := #TIAA-TOT-PAY-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Tot_Div_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Tot_Div_Amt());                                         //Natural: ASSIGN #O-TIAA-TOT-DIV-AMT := #TIAA-TOT-DIV-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Per_Pay_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Pay_Amt());                                         //Natural: ASSIGN #O-TIAA-PER-PAY-AMT := #TIAA-PER-PAY-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Per_Div_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Per_Div_Amt());                                         //Natural: ASSIGN #O-TIAA-PER-DIV-AMT := #TIAA-PER-DIV-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Units_Cnt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Units_Cnt());                                             //Natural: ASSIGN #O-TIAA-UNITS-CNT := #TIAA-UNITS-CNT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Pay_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Pay_Amt());                           //Natural: ASSIGN #O-TIAA-RATE-FINAL-PAY-AMT := #TIAA-RATE-FINAL-PAY-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Rate_Final_Div_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Tiaa_Rate_Final_Div_Amt());                           //Natural: ASSIGN #O-TIAA-RATE-FINAL-DIV-AMT := #TIAA-RATE-FINAL-DIV-AMT
        ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Payee_Cde().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde());                                         //Natural: ASSIGN #O-CNTRCT-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        //* *#SAVE-FUND-PPCN-NBR      := #CNTRCT-PPCN-NBR
        if (condition(ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Fund_Cde().equals("1S") && ldaIaal100.getPnd_Output_Record_Pnd_O_Cntrct_Ppcn_Nbr_1_3().equals("W08")))   //Natural: IF #O-TIAA-FUND-CDE = '1S' AND #O-CNTRCT-PPCN-NBR-1-3 = 'W08'
        {
            ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Fund_Cde().setValue("1Z");                                                                                         //Natural: MOVE '1Z' TO #O-TIAA-FUND-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE #OUTPUT-RECORD
        short decideConditionsMet356 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #O-TIAA-CMPNY-CDE = 'T' OR = 'U' OR = '2'
        if (condition(ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().equals("T") || ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().equals("U") 
            || ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().equals("2")))
        {
            decideConditionsMet356++;
            getWorkFiles().write(2, false, ldaIaal100.getPnd_Output_Record());                                                                                            //Natural: WRITE WORK FILE 2 #OUTPUT-RECORD
            pnd_Write_Ctr_Annual.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WRITE-CTR-ANNUAL
        }                                                                                                                                                                 //Natural: WHEN #O-TIAA-CMPNY-CDE = 'W' OR = '4'
        else if (condition(ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().equals("W") || ldaIaal100.getPnd_Output_Record_Pnd_O_Tiaa_Cmpny_Cde().equals("4")))
        {
            decideConditionsMet356++;
            getWorkFiles().write(4, false, ldaIaal100.getPnd_Output_Record());                                                                                            //Natural: WRITE WORK FILE 4 #OUTPUT-RECORD
            pnd_Write_Ctr_Monthly.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WRITE-CTR-MONTHLY
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  SAVE-FUND-SUMM
    }
    private void sub_Save_Deduction() throws Exception                                                                                                                    //Natural: SAVE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr().equals(pnd_Save_Fund_Ppcn_Nbr) && ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde().equals(pnd_Save_Fund_Payee_Cde))) //Natural: IF #CNTRCT-PPCN-NBR = #SAVE-FUND-PPCN-NBR AND #CNTRCT-PAYEE-CDE = #SAVE-FUND-PAYEE-CDE
        {
            ldaIaal100.getPnd_Output_Record_Pnd_O_Ddctn_Per_Amt().setValue(ldaIaal100.getPnd_Input_Record_Pnd_Ddctn_Per_Amt());                                           //Natural: ASSIGN #O-DDCTN-PER-AMT := #DDCTN-PER-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  SAVE-DEDUCTIONS
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM ERROR IN ",Global.getPROGRAM()," FOR CONTRACT #: ",ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Ppcn_Nbr(),ldaIaal100.getPnd_Input_Record_Pnd_Cntrct_Payee_Cde()); //Natural: WRITE 'PROGRAM ERROR IN ' *PROGRAM ' FOR CONTRACT #: ' #CNTRCT-PPCN-NBR #CNTRCT-PAYEE-CDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
        Global.format(2, "LS=133 PS=56");
    }
}
