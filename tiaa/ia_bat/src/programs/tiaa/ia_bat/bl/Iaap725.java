/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:31:15 PM
**        * FROM NATURAL PROGRAM : Iaap725
************************************************************
**        * FILE NAME            : Iaap725.java
**        * CLASS NAME           : Iaap725
**        * INSTANCE NAME        : Iaap725
************************************************************
************************************************************************
* PROGRAM  : IAAP725
* PURPOSE  : TO GENERATE A RATE BY RATE REPORT FOR TPA AND IPROS BY
*            MODE. THIS WILL BE USED IN THE TPA CONVERSION
*            RECONCILIATION
* DATE     : 06/29/2005
* AUTHOR   : JUN TINIO
*
*          : INCREASED RATE ARRAY TO 99   DO SCAN 4/08
* 3/12     : RATE BASE EXPANSION          DO SCAN 3/12
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap725 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type;

    private DataAccessProgramView vw_cpr;
    private DbsField cpr_Cntrct_Part_Ppcn_Nbr;
    private DbsField cpr_Cntrct_Part_Payee_Cde;
    private DbsField cpr_Cntrct_Actvty_Cde;
    private DbsField cpr_Cntrct_Mode_Ind;

    private DataAccessProgramView vw_fund;
    private DbsField fund_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField fund_Tiaa_Cntrct_Payee_Cde;
    private DbsField fund_Tiaa_Cmpny_Fund_Cde;
    private DbsField fund_Tiaa_Tot_Per_Amt;
    private DbsField fund_Tiaa_Tot_Div_Amt;

    private DbsGroup fund_Tiaa_Rate_Data_Grp;
    private DbsField fund_Tiaa_Rate_Cde;
    private DbsField fund_Tiaa_Rate_Dte;
    private DbsField fund_Tiaa_Per_Pay_Amt;
    private DbsField fund_Tiaa_Per_Div_Amt;
    private DbsField fund_Tiaa_Rate_Final_Pay_Amt;
    private DbsField fund_Tiaa_Rate_Final_Div_Amt;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Todays_Dte;
    private DbsField pnd_Fund_Key;

    private DbsGroup pnd_Fund_Key__R_Field_1;
    private DbsField pnd_Fund_Key_Pnd_Cpr_Key;

    private DbsGroup pnd_Fund_Key__R_Field_2;
    private DbsField pnd_Fund_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Fund_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Fund_Key_Pnd_Cmpny_Fund;

    private DbsGroup pnd_Output_Rec;
    private DbsField pnd_Output_Rec_Pnd_Mode;
    private DbsField pnd_Output_Rec_Pnd_Rate_Cde;
    private DbsField pnd_Output_Rec_Pnd_Guar_Amt;
    private DbsField pnd_Output_Rec_Pnd_Div_Amt;
    private DbsField pnd_Output_Rec_Pnd_Final_Pay;
    private DbsField pnd_Tot_Guar;
    private DbsField pnd_Tot_Div;
    private DbsField pnd_Tot_Final;
    private DbsField pnd_Tot_Pay;
    private DbsField pnd_M_Tot_Guar;
    private DbsField pnd_M_Tot_Div;
    private DbsField pnd_M_Tot_Final;
    private DbsField pnd_M_Tot_Pay;
    private DbsField pnd_G_Tot_Guar;
    private DbsField pnd_G_Tot_Div;
    private DbsField pnd_G_Tot_Final;
    private DbsField pnd_G_Tot_Pay;
    private DbsField pnd_I;
    private DbsField pnd_Tpa_Rte_Cnt;
    private DbsField pnd_Ipro_Rte_Cnt;
    private DbsField pnd_Ipro_Cntrct_Cnt;
    private DbsField pnd_Tpa_Cntrct_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        registerRecord(vw_iaa_Cntrct);

        vw_cpr = new DataAccessProgramView(new NameInfo("vw_cpr", "CPR"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cpr_Cntrct_Part_Ppcn_Nbr = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PPCN_NBR");
        cpr_Cntrct_Part_Payee_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_PART_PAYEE_CDE");
        cpr_Cntrct_Actvty_Cde = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_ACTVTY_CDE");
        cpr_Cntrct_Mode_Ind = vw_cpr.getRecord().newFieldInGroup("cpr_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_IND");
        registerRecord(vw_cpr);

        vw_fund = new DataAccessProgramView(new NameInfo("vw_fund", "FUND"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        fund_Tiaa_Cntrct_Ppcn_Nbr = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CREF_CNTRCT_PPCN_NBR");
        fund_Tiaa_Cntrct_Payee_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        fund_Tiaa_Cmpny_Fund_Cde = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIAA_CMPNY_FUND_CDE");
        fund_Tiaa_Tot_Per_Amt = vw_fund.getRecord().newFieldInGroup("FUND_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "CREF_TOT_PER_AMT");
        fund_Tiaa_Tot_Div_Amt = vw_fund.getRecord().newFieldInGroup("fund_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIAA_TOT_DIV_AMT");

        fund_Tiaa_Rate_Data_Grp = vw_fund.getRecord().newGroupInGroup("fund_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Cde = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_RATE_CDE", "TIAA-RATE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Dte = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("FUND_TIAA_RATE_DTE", "TIAA-RATE-DTE", FieldType.DATE, new DbsArrayController(1, 
            250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Per_Pay_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Per_Div_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Final_Pay_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Rate_Final_Pay_Amt", "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        fund_Tiaa_Rate_Final_Div_Amt = fund_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("fund_Tiaa_Rate_Final_Div_Amt", "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        registerRecord(vw_fund);

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        pnd_Fund_Key = localVariables.newFieldInRecord("pnd_Fund_Key", "#FUND-KEY", FieldType.STRING, 15);

        pnd_Fund_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Fund_Key__R_Field_1", "REDEFINE", pnd_Fund_Key);
        pnd_Fund_Key_Pnd_Cpr_Key = pnd_Fund_Key__R_Field_1.newFieldInGroup("pnd_Fund_Key_Pnd_Cpr_Key", "#CPR-KEY", FieldType.STRING, 12);

        pnd_Fund_Key__R_Field_2 = pnd_Fund_Key__R_Field_1.newGroupInGroup("pnd_Fund_Key__R_Field_2", "REDEFINE", pnd_Fund_Key_Pnd_Cpr_Key);
        pnd_Fund_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Fund_Key__R_Field_2.newFieldInGroup("pnd_Fund_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Fund_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Fund_Key__R_Field_2.newFieldInGroup("pnd_Fund_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Fund_Key_Pnd_Cmpny_Fund = pnd_Fund_Key__R_Field_1.newFieldInGroup("pnd_Fund_Key_Pnd_Cmpny_Fund", "#CMPNY-FUND", FieldType.STRING, 3);

        pnd_Output_Rec = localVariables.newGroupInRecord("pnd_Output_Rec", "#OUTPUT-REC");
        pnd_Output_Rec_Pnd_Mode = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Mode", "#MODE", FieldType.NUMERIC, 3);
        pnd_Output_Rec_Pnd_Rate_Cde = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Rate_Cde", "#RATE-CDE", FieldType.STRING, 2);
        pnd_Output_Rec_Pnd_Guar_Amt = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Guar_Amt", "#GUAR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Output_Rec_Pnd_Div_Amt = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Div_Amt", "#DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Output_Rec_Pnd_Final_Pay = pnd_Output_Rec.newFieldInGroup("pnd_Output_Rec_Pnd_Final_Pay", "#FINAL-PAY", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Guar = localVariables.newFieldInRecord("pnd_Tot_Guar", "#TOT-GUAR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Div = localVariables.newFieldInRecord("pnd_Tot_Div", "#TOT-DIV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Final = localVariables.newFieldInRecord("pnd_Tot_Final", "#TOT-FINAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Pay = localVariables.newFieldInRecord("pnd_Tot_Pay", "#TOT-PAY", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_M_Tot_Guar = localVariables.newFieldInRecord("pnd_M_Tot_Guar", "#M-TOT-GUAR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_M_Tot_Div = localVariables.newFieldInRecord("pnd_M_Tot_Div", "#M-TOT-DIV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_M_Tot_Final = localVariables.newFieldInRecord("pnd_M_Tot_Final", "#M-TOT-FINAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_M_Tot_Pay = localVariables.newFieldInRecord("pnd_M_Tot_Pay", "#M-TOT-PAY", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_G_Tot_Guar = localVariables.newFieldInRecord("pnd_G_Tot_Guar", "#G-TOT-GUAR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_G_Tot_Div = localVariables.newFieldInRecord("pnd_G_Tot_Div", "#G-TOT-DIV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_G_Tot_Final = localVariables.newFieldInRecord("pnd_G_Tot_Final", "#G-TOT-FINAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_G_Tot_Pay = localVariables.newFieldInRecord("pnd_G_Tot_Pay", "#G-TOT-PAY", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Tpa_Rte_Cnt = localVariables.newFieldInRecord("pnd_Tpa_Rte_Cnt", "#TPA-RTE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ipro_Rte_Cnt = localVariables.newFieldInRecord("pnd_Ipro_Rte_Cnt", "#IPRO-RTE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ipro_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Ipro_Cntrct_Cnt", "#IPRO-CNTRCT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tpa_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Tpa_Cntrct_Cnt", "#TPA-CNTRCT-CNT", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();
        vw_cpr.reset();
        vw_fund.reset();
        vw_cntrl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap725() throws Exception
    {
        super("Iaap725");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 66 LS = 133;//Natural: FORMAT ( 2 ) PS = 66 LS = 133
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_iaa_Cntrct.startDatabaseRead                                                                                                                                   //Natural: READ IAA-CNTRCT BY CNTRCT-PPCN-NBR STARTING FROM 'IA'
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", ">=", "IA", WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PPCN_NBR", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Cntrct.readNextRow("READ02")))
        {
            if (condition(iaa_Cntrct_Cntrct_Ppcn_Nbr.greater("IP999999")))                                                                                                //Natural: IF CNTRCT-PPCN-NBR GT 'IP999999'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(iaa_Cntrct_Cntrct_Optn_Cde.equals(25) || iaa_Cntrct_Cntrct_Optn_Cde.equals(27) || iaa_Cntrct_Cntrct_Optn_Cde.equals(28) ||                    //Natural: ACCEPT IF CNTRCT-OPTN-CDE = 25 OR = 27 OR = 28 OR = 30
                iaa_Cntrct_Cntrct_Optn_Cde.equals(30))))
            {
                continue;
            }
            //*    AND (CNTRCT-TYPE = 'F' OR = 'P')
            pnd_Fund_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(iaa_Cntrct_Cntrct_Ppcn_Nbr);                                                                                   //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := CNTRCT-PPCN-NBR
            pnd_Fund_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                           //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 01
            vw_cpr.startDatabaseRead                                                                                                                                      //Natural: READ CPR BY CNTRCT-PAYEE-KEY STARTING FROM #CPR-KEY
            (
            "READ03",
            new Wc[] { new Wc("CNTRCT_PAYEE_KEY", ">=", pnd_Fund_Key_Pnd_Cpr_Key, WcType.BY) },
            new Oc[] { new Oc("CNTRCT_PAYEE_KEY", "ASC") }
            );
            READ03:
            while (condition(vw_cpr.readNextRow("READ03")))
            {
                if (condition(cpr_Cntrct_Part_Ppcn_Nbr.notEquals(iaa_Cntrct_Cntrct_Ppcn_Nbr)))                                                                            //Natural: IF CNTRCT-PART-PPCN-NBR NE CNTRCT-PPCN-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cpr_Cntrct_Actvty_Cde.notEquals(9)))                                                                                                        //Natural: IF CNTRCT-ACTVTY-CDE NE 9
                {
                    pnd_Fund_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(cpr_Cntrct_Part_Payee_Cde);                                                                           //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := CNTRCT-PART-PAYEE-CDE
                    pnd_Fund_Key_Pnd_Cmpny_Fund.setValue("T1S");                                                                                                          //Natural: ASSIGN #CMPNY-FUND := 'T1S'
                                                                                                                                                                          //Natural: PERFORM GET-FUND-DATA
                    sub_Get_Fund_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL TPA  RATES:        ",pnd_Tpa_Rte_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                             //Natural: WRITE 'TOTAL TPA  RATES:        ' #TPA-RTE-CNT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL TPA  CONTRCT PAYEE:",pnd_Tpa_Cntrct_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                          //Natural: WRITE 'TOTAL TPA  CONTRCT PAYEE:' #TPA-CNTRCT-CNT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL IPRO RATES:        ",pnd_Ipro_Rte_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                            //Natural: WRITE 'TOTAL IPRO RATES:        ' #IPRO-RTE-CNT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL IPRO CONTRCT PAYEE:",pnd_Ipro_Cntrct_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                         //Natural: WRITE 'TOTAL IPRO CONTRCT PAYEE:' #IPRO-CNTRCT-CNT ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-DATA
    }
    private void sub_Get_Fund_Data() throws Exception                                                                                                                     //Natural: GET-FUND-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  4/08         /* 3/12
        vw_fund.startDatabaseFind                                                                                                                                         //Natural: FIND FUND WITH TIAA-CNTRCT-FUND-KEY = #FUND-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", pnd_Fund_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_fund.readNextRow("FIND01")))
        {
            vw_fund.setIfNotFoundControlFlag(false);
            pnd_Output_Rec_Pnd_Mode.setValue(cpr_Cntrct_Mode_Ind);                                                                                                        //Natural: ASSIGN #MODE := CNTRCT-MODE-IND
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 250
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(250)); pnd_I.nadd(1))
            {
                if (condition(fund_Tiaa_Rate_Cde.getValue(pnd_I).notEquals(" ")))                                                                                         //Natural: IF TIAA-RATE-CDE ( #I ) NE ' '
                {
                    pnd_Output_Rec_Pnd_Rate_Cde.setValue(fund_Tiaa_Rate_Cde.getValue(pnd_I));                                                                             //Natural: ASSIGN #RATE-CDE := TIAA-RATE-CDE ( #I )
                    pnd_Output_Rec_Pnd_Guar_Amt.setValue(fund_Tiaa_Per_Pay_Amt.getValue(pnd_I));                                                                          //Natural: ASSIGN #GUAR-AMT := TIAA-PER-PAY-AMT ( #I )
                    pnd_Output_Rec_Pnd_Div_Amt.setValue(fund_Tiaa_Per_Div_Amt.getValue(pnd_I));                                                                           //Natural: ASSIGN #DIV-AMT := TIAA-PER-DIV-AMT ( #I )
                    pnd_Output_Rec_Pnd_Final_Pay.setValue(fund_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_I));                                                                  //Natural: ASSIGN #FINAL-PAY := TIAA-RATE-FINAL-PAY-AMT ( #I )
                    if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(28) || iaa_Cntrct_Cntrct_Optn_Cde.equals(30)))                                                        //Natural: IF CNTRCT-OPTN-CDE = 28 OR = 30
                    {
                        getWorkFiles().write(1, false, pnd_Output_Rec);                                                                                                   //Natural: WRITE WORK FILE 1 #OUTPUT-REC
                        pnd_Tpa_Rte_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TPA-RTE-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getWorkFiles().write(2, false, pnd_Output_Rec);                                                                                                   //Natural: WRITE WORK FILE 2 #OUTPUT-REC
                        pnd_Ipro_Rte_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #IPRO-RTE-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(28) || iaa_Cntrct_Cntrct_Optn_Cde.equals(30)))                                                                //Natural: IF CNTRCT-OPTN-CDE = 28 OR = 30
            {
                pnd_Tpa_Cntrct_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TPA-CNTRCT-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ipro_Cntrct_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #IPRO-CNTRCT-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=66 LS=133");
        Global.format(2, "PS=66 LS=133");
    }
}
