/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:10 PM
**        * FROM NATURAL PROGRAM : Nazp723
************************************************************
**        * FILE NAME            : Nazp723.java
**        * CLASS NAME           : Nazp723
**        * INSTANCE NAME        : Nazp723
************************************************************
************************************************************************
* PROGRAM    : NAZP723
* SYSTEM     : ANNUITIZATION
* TITLE      : REPORT FOR ACTUARIAL
* WRITTEN    : FEB, 2006
* FUNCTION :
*
* 02/27/08   O.SOTTO RATE EXPANSION TO 99 OCCURS. SC 022708.
*********************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nazp723 extends BLNatBase
{
    // Data Areas
    private LdaNazldarc ldaNazldarc;
    private LdaNazlprtc ldaNazlprtc;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntrl;
    private DbsField cntrl_Cntrl_Check_Dte;
    private DbsField cntrl_Cntrl_Invrse_Dte;
    private DbsField cntrl_Cntrl_Cde;
    private DbsField cntrl_Cntrl_Todays_Dte;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Contr_Rec_Read;
    private DbsField pnd_Counters_Pnd_Part_Rec_Read;
    private DbsField pnd_Frm_Dte;

    private DbsGroup pnd_Frm_Dte__R_Field_1;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Yyyy;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Mm;
    private DbsField pnd_Frm_Dte_Pnd_Frm_Dd;
    private DbsField pnd_Thru_Dte;

    private DbsGroup pnd_Thru_Dte__R_Field_2;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Yyyy;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Mm;
    private DbsField pnd_Thru_Dte_Pnd_Thru_Dd;
    private DbsField pnd_Date;
    private DbsField pnd_Datea;
    private DbsField pnd_First;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_A;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Ext;

    private DbsGroup pnd_Extract;
    private DbsField pnd_Extract_Pnd_E_Date;
    private DbsField pnd_Extract_Pnd_E_Orig_Cntrct_Cnt;
    private DbsField pnd_Extract_Pnd_E_Orig_Cntrct;
    private DbsField pnd_Extract_Pnd_E_Cntrct;
    private DbsField pnd_Extract_Pnd_E_Cert;
    private DbsField pnd_Extract_Pnd_Fund;

    private DbsGroup pnd_Extract_Pnd_Rte_Info;
    private DbsField pnd_Extract_Pnd_Rte_Cd;
    private DbsField pnd_Extract_Pnd_Rte_Actl_Amt;
    private DbsField pnd_Extract_Pnd_Rte_Rtb_Amt;
    private DbsField pnd_Extract_Pnd_Rte_Grd_Mnth_Amt;
    private DbsField pnd_Extract_Pnd_Rte_Std_Annl_Amt;
    private DbsField pnd_Max_Rates;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaNazldarc = new LdaNazldarc();
        registerRecord(ldaNazldarc);
        registerRecord(ldaNazldarc.getVw_naz_Da_Rslt_Ddm_View());
        ldaNazlprtc = new LdaNazlprtc();
        registerRecord(ldaNazlprtc);
        registerRecord(ldaNazlprtc.getVw_naz_Prtcpnt_Ddm_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntrl = new DataAccessProgramView(new NameInfo("vw_cntrl", "CNTRL"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        cntrl_Cntrl_Check_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_CHECK_DTE");
        cntrl_Cntrl_Invrse_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRL_INVRSE_DTE");
        cntrl_Cntrl_Cde = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRL_CDE");
        cntrl_Cntrl_Todays_Dte = vw_cntrl.getRecord().newFieldInGroup("cntrl_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRL_TODAYS_DTE");
        registerRecord(vw_cntrl);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Contr_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Contr_Rec_Read", "#CONTR-REC-READ", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Part_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Part_Rec_Read", "#PART-REC-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Frm_Dte = localVariables.newFieldInRecord("pnd_Frm_Dte", "#FRM-DTE", FieldType.STRING, 8);

        pnd_Frm_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Frm_Dte__R_Field_1", "REDEFINE", pnd_Frm_Dte);
        pnd_Frm_Dte_Pnd_Frm_Yyyy = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Yyyy", "#FRM-YYYY", FieldType.NUMERIC, 4);
        pnd_Frm_Dte_Pnd_Frm_Mm = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Mm", "#FRM-MM", FieldType.NUMERIC, 2);
        pnd_Frm_Dte_Pnd_Frm_Dd = pnd_Frm_Dte__R_Field_1.newFieldInGroup("pnd_Frm_Dte_Pnd_Frm_Dd", "#FRM-DD", FieldType.NUMERIC, 2);
        pnd_Thru_Dte = localVariables.newFieldInRecord("pnd_Thru_Dte", "#THRU-DTE", FieldType.STRING, 8);

        pnd_Thru_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Thru_Dte__R_Field_2", "REDEFINE", pnd_Thru_Dte);
        pnd_Thru_Dte_Pnd_Thru_Yyyy = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Yyyy", "#THRU-YYYY", FieldType.NUMERIC, 4);
        pnd_Thru_Dte_Pnd_Thru_Mm = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Mm", "#THRU-MM", FieldType.NUMERIC, 2);
        pnd_Thru_Dte_Pnd_Thru_Dd = pnd_Thru_Dte__R_Field_2.newFieldInGroup("pnd_Thru_Dte_Pnd_Thru_Dd", "#THRU-DD", FieldType.NUMERIC, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Datea = localVariables.newFieldInRecord("pnd_Datea", "#DATEA", FieldType.STRING, 8);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 5);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 5);
        pnd_Ext = localVariables.newFieldInRecord("pnd_Ext", "#EXT", FieldType.PACKED_DECIMAL, 7);

        pnd_Extract = localVariables.newGroupInRecord("pnd_Extract", "#EXTRACT");
        pnd_Extract_Pnd_E_Date = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Date", "#E-DATE", FieldType.STRING, 8);
        pnd_Extract_Pnd_E_Orig_Cntrct_Cnt = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Orig_Cntrct_Cnt", "#E-ORIG-CNTRCT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Extract_Pnd_E_Orig_Cntrct = pnd_Extract.newFieldArrayInGroup("pnd_Extract_Pnd_E_Orig_Cntrct", "#E-ORIG-CNTRCT", FieldType.STRING, 10, new 
            DbsArrayController(1, 6));
        pnd_Extract_Pnd_E_Cntrct = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Cntrct", "#E-CNTRCT", FieldType.STRING, 10);
        pnd_Extract_Pnd_E_Cert = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_E_Cert", "#E-CERT", FieldType.STRING, 10);
        pnd_Extract_Pnd_Fund = pnd_Extract.newFieldInGroup("pnd_Extract_Pnd_Fund", "#FUND", FieldType.STRING, 1);

        pnd_Extract_Pnd_Rte_Info = pnd_Extract.newGroupArrayInGroup("pnd_Extract_Pnd_Rte_Info", "#RTE-INFO", new DbsArrayController(1, 99));
        pnd_Extract_Pnd_Rte_Cd = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Cd", "#RTE-CD", FieldType.STRING, 2);
        pnd_Extract_Pnd_Rte_Actl_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Actl_Amt", "#RTE-ACTL-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Extract_Pnd_Rte_Rtb_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Rtb_Amt", "#RTE-RTB-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Extract_Pnd_Rte_Grd_Mnth_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Grd_Mnth_Amt", "#RTE-GRD-MNTH-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Extract_Pnd_Rte_Std_Annl_Amt = pnd_Extract_Pnd_Rte_Info.newFieldInGroup("pnd_Extract_Pnd_Rte_Std_Annl_Amt", "#RTE-STD-ANNL-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrl.reset();

        ldaNazldarc.initializeValues();
        ldaNazlprtc.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Max_Rates.setInitialValue(99);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nazp723() throws Exception
    {
        super("Nazp723");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 133 PS = 55
        vw_cntrl.startDatabaseRead                                                                                                                                        //Natural: AT TOP OF PAGE ( 01 );//Natural: READ CNTRL BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cntrl.readNextRow("READ01")))
        {
            if (condition(cntrl_Cntrl_Cde.notEquals("DC")))                                                                                                               //Natural: IF CNTRL-CDE NE 'DC'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Thru_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #THRU-DTE
                if (condition(pnd_Thru_Dte_Pnd_Thru_Dd.lessOrEqual(20)))                                                                                                  //Natural: IF #THRU-DD LE 20
                {
                    DbsUtil.terminate(0);  if (true) return;                                                                                                              //Natural: TERMINATE 00
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Frm_Dte.setValueEdited(cntrl_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                            //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #FRM-DTE
            if (condition(pnd_Frm_Dte_Pnd_Frm_Mm.notEquals(pnd_Thru_Dte_Pnd_Thru_Mm)))                                                                                    //Natural: IF #FRM-MM NE #THRU-MM
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "FROM DATE:",pnd_Frm_Dte," TO DATE:",pnd_Thru_Dte);                                                                                         //Natural: WRITE 'FROM DATE:' #FRM-DTE ' TO DATE:' #THRU-DTE
        if (Global.isEscape()) return;
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Thru_Dte);                                                                                             //Natural: MOVE EDITED #THRU-DTE TO #DATE ( EM = YYYYMMDD )
        pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Frm_Dte);                                                                                //Natural: MOVE EDITED #FRM-DTE TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
        ldaNazlprtc.getVw_naz_Prtcpnt_Ddm_View().startDatabaseRead                                                                                                        //Natural: READ NAZ-PRTCPNT-DDM-VIEW BY NAP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
        (
        "READ02",
        new Wc[] { new Wc("NAP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
        new Oc[] { new Oc("NAP_LST_ACTVTY_DTE", "ASC") }
        );
        READ02:
        while (condition(ldaNazlprtc.getVw_naz_Prtcpnt_Ddm_View().readNextRow("READ02")))
        {
            if (condition(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Lst_Actvty_Dte().greater(pnd_Date)))                                                                    //Natural: IF NAP-LST-ACTVTY-DTE GT #DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Stts_Cde().getSubstring(1,1).equals("T"))))                                                           //Natural: ACCEPT IF SUBSTR ( NAP-STTS-CDE,1,1 ) = 'T'
            {
                continue;
            }
            pnd_Counters_Pnd_Part_Rec_Read.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PART-REC-READ
                                                                                                                                                                          //Natural: PERFORM READ-DARESULT-FILE
            sub_Read_Daresult_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"  ");                                                                                                                 //Natural: WRITE ( 01 ) '  '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NAZ-PRTCPNT records read     ",pnd_Counters_Pnd_Part_Rec_Read);                                                       //Natural: WRITE ( 01 ) 'NAZ-PRTCPNT records read     ' #PART-REC-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"  ");                                                                                                                 //Natural: WRITE ( 01 ) '  '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NAZ-DARSLT  records read     ",pnd_Counters_Pnd_Contr_Rec_Read);                                                      //Natural: WRITE ( 01 ) 'NAZ-DARSLT  records read     ' #CONTR-REC-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Total Extract Records        ",pnd_Ext);                                                                                                   //Natural: WRITE 'Total Extract Records        ' #EXT
        if (Global.isEscape()) return;
        //* *****************************************************************
        //*      S U B R O U T I N E S
        //* *****************************************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DARESULT-FILE
        //*    'Req ID'    NAZ-PRTCPNT-DDM-VIEW.RQST-ID
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-REA-INFO
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-INFO
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT
        //* ***********************************************************************
    }
    private void sub_Read_Daresult_File() throws Exception                                                                                                                //Natural: READ-DARESULT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Extract.reset();                                                                                                                                              //Natural: RESET #EXTRACT
        ldaNazldarc.getVw_naz_Da_Rslt_Ddm_View().startDatabaseRead                                                                                                        //Natural: READ NAZ-DA-RSLT-DDM-VIEW BY NAZ-DA-RSLT-DDM-VIEW.RQST-ID = NAZ-PRTCPNT-DDM-VIEW.RQST-ID
        (
        "READ03",
        new Wc[] { new Wc("RQST_ID", ">=", ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Rqst_Id(), WcType.BY) },
        new Oc[] { new Oc("RQST_ID", "ASC") }
        );
        READ03:
        while (condition(ldaNazldarc.getVw_naz_Da_Rslt_Ddm_View().readNextRow("READ03")))
        {
            pnd_Counters_Pnd_Contr_Rec_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CONTR-REC-READ
            if (condition(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Rqst_Id().notEquals(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Rqst_Id())))                                        //Natural: IF NAZ-DA-RSLT-DDM-VIEW.RQST-ID NE NAZ-PRTCPNT-DDM-VIEW.RQST-ID
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "IA TIAA/Number ",                                                                                                                    //Natural: DISPLAY ( 01 ) 'IA TIAA/Number ' NAZ-PRTCPNT-DDM-VIEW.NAP-IA-TIAA-NBR 'IA CREF/ Number ' NAZ-PRTCPNT-DDM-VIEW.NAP-IA-CREF-NBR /
            		ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Tiaa_Nbr(),"IA CREF/ Number ",
            		ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Cref_Nbr(),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #X 1 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cde().getValue(pnd_X).equals(" ")))                                                            //Natural: IF NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-CDE ( #X ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,"    Acct",ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cde().getValue(pnd_X),"    Actual Amt",                //Natural: WRITE ( 01 ) '    Acct' NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-CDE ( #X ) '    Actual Amt' NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-ACTL-AMT ( #X ) '    RTB Amt' NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-RTB-ACTL-AMT ( #X ) '    GRD/Amt' NAZ-DA-RSLT-DDM-VIEW.NAD-GRD-MNTHLY-ACTL-AMT ( #X ) '    Std/Amt' NAZ-DA-RSLT-DDM-VIEW.NAD-STNDRD-ANNL-ACTL-AMT ( #X ) //
                    ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt().getValue(pnd_X),"    RTB Amt",ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt().getValue(pnd_X),
                    "    GRD/Amt",ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt().getValue(pnd_X),"    Std/Amt",ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt().getValue(pnd_X),
                    NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Extract_Pnd_Fund.setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cde().getValue(pnd_X));                                                        //Natural: ASSIGN #FUND := NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-CDE ( #X )
                pnd_Extract_Pnd_E_Date.setValueEdited(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED NAP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #E-DATE
                pnd_Extract_Pnd_E_Cntrct.setValue(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Tiaa_Nbr());                                                                 //Natural: ASSIGN #E-CNTRCT := NAZ-PRTCPNT-DDM-VIEW.NAP-IA-TIAA-NBR
                pnd_Extract_Pnd_E_Cert.setValue(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Cref_Nbr());                                                                   //Natural: ASSIGN #E-CERT := NAZ-PRTCPNT-DDM-VIEW.NAP-IA-CREF-NBR
                if (condition(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cde().getValue(pnd_X).equals("T")))                                                            //Natural: IF NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-CDE ( #X ) = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                    sub_Write_Tiaa_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-REA-INFO
                    sub_Write_Cref_Rea_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-CONTRACT-FILE
    }
    private void sub_Write_Cref_Rea_Info() throws Exception                                                                                                               //Natural: WRITE-CREF-REA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Extract_Pnd_Rte_Cd.getValue(1).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Cref_Rate_Cde().getValue(pnd_X));                                        //Natural: ASSIGN #RTE-CD ( 1 ) := NAD-ACCT-CREF-RATE-CDE ( #X )
        pnd_Extract_Pnd_Rte_Actl_Amt.getValue(1).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Actl_Amt().getValue(pnd_X));                                       //Natural: ASSIGN #RTE-ACTL-AMT ( 1 ) := NAD-ACCT-ACTL-AMT ( #X )
        pnd_Extract_Pnd_Rte_Rtb_Amt.getValue(1).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Acct_Rtb_Actl_Amt().getValue(pnd_X));                                    //Natural: ASSIGN #RTE-RTB-AMT ( 1 ) := NAD-ACCT-RTB-ACTL-AMT ( #X )
        pnd_Extract_Pnd_Rte_Grd_Mnth_Amt.getValue(1).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Grd_Mnthly_Actl_Amt().getValue(pnd_X));                             //Natural: ASSIGN #RTE-GRD-MNTH-AMT ( 1 ) := NAD-GRD-MNTHLY-ACTL-AMT ( #X )
        pnd_Extract_Pnd_Rte_Std_Annl_Amt.getValue(1).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Stndrd_Annl_Actl_Amt().getValue(pnd_X));                            //Natural: ASSIGN #RTE-STD-ANNL-AMT ( 1 ) := NAD-STNDRD-ANNL-ACTL-AMT ( #X )
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT
        sub_Write_Extract();
        if (condition(Global.isEscape())) {return;}
    }
    //*  022708
    private void sub_Write_Tiaa_Info() throws Exception                                                                                                                   //Natural: WRITE-TIAA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #Y 1 #MAX-RATES
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_Max_Rates)); pnd_Y.nadd(1))
        {
            if (condition(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde().getValue(pnd_Y).equals(" ")))                                                       //Natural: IF NAZ-DA-RSLT-DDM-VIEW.NAD-DTL-TIAA-RATE-CDE ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Extract_Pnd_Rte_Cd.getValue(pnd_Y).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rate_Cde().getValue(pnd_Y));                                 //Natural: ASSIGN #RTE-CD ( #Y ) := NAZ-DA-RSLT-DDM-VIEW.NAD-DTL-TIAA-RATE-CDE ( #Y )
            pnd_Extract_Pnd_Rte_Actl_Amt.getValue(pnd_Y).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Actl_Amt().getValue(pnd_Y));                           //Natural: ASSIGN #RTE-ACTL-AMT ( #Y ) := NAD-DTL-TIAA-ACTL-AMT ( #Y )
            pnd_Extract_Pnd_Rte_Rtb_Amt.getValue(pnd_Y).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Rtb_Actl_Amt().getValue(pnd_Y));                        //Natural: ASSIGN #RTE-RTB-AMT ( #Y ) := NAD-DTL-TIAA-RTB-ACTL-AMT ( #Y )
            pnd_Extract_Pnd_Rte_Grd_Mnth_Amt.getValue(pnd_Y).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Y));                   //Natural: ASSIGN #RTE-GRD-MNTH-AMT ( #Y ) := NAD-DTL-TIAA-GRD-ACTL-AMT ( #Y )
            pnd_Extract_Pnd_Rte_Std_Annl_Amt.getValue(pnd_Y).setValue(ldaNazldarc.getNaz_Da_Rslt_Ddm_View_Nad_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Y));                //Natural: ASSIGN #RTE-STD-ANNL-AMT ( #Y ) := NAD-DTL-TIAA-STNDRD-ACTL-AMT ( #Y )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT
        sub_Write_Extract();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Extract() throws Exception                                                                                                                     //Natural: WRITE-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #A 1 6
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(6)); pnd_A.nadd(1))
        {
            if (condition(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Cntrcts_In_Rqst().getValue(pnd_A).equals(" ")))                                                         //Natural: IF NAZ-PRTCPNT-DDM-VIEW.NAP-CNTRCTS-IN-RQST ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Extract_Pnd_E_Orig_Cntrct_Cnt.setValue(pnd_A);                                                                                                            //Natural: ASSIGN #E-ORIG-CNTRCT-CNT := #A
            pnd_Extract_Pnd_E_Orig_Cntrct.getValue(pnd_A).setValue(ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Cntrcts_In_Rqst().getValue(pnd_A));                            //Natural: ASSIGN #E-ORIG-CNTRCT ( #A ) := NAZ-PRTCPNT-DDM-VIEW.NAP-CNTRCTS-IN-RQST ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getWorkFiles().write(1, false, pnd_Extract);                                                                                                                      //Natural: WRITE WORK FILE 1 #EXTRACT
        pnd_Extract_Pnd_Rte_Info.getValue("*").reset();                                                                                                                   //Natural: RESET #RTE-INFO ( * )
        pnd_Ext.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #EXT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(52),"TIAA/CREF",NEWLINE,new ColumnSpacing(1),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 52X 'TIAA/CREF' / 1X 'RUN DATE  : ' *DATU 21X 'ADAM Report for Actuarial' 15X 'PROGRAM ID: ' *PROGRAM / 1X 'RUN TIME  : ' *TIMX 61X 'PAGE : ' *PAGE-NUMBER ( 01 ) ///
                        ColumnSpacing(21),"ADAM Report for Actuarial",new ColumnSpacing(15),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(1),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(61),"PAGE : ",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55");

        getReports().setDisplayColumns(1, "IA TIAA/Number ",
        		ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Tiaa_Nbr(),"IA CREF/ Number ",
        		ldaNazlprtc.getNaz_Prtcpnt_Ddm_View_Nap_Ia_Cref_Nbr(),NEWLINE);
    }
}
