/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:29:04 PM
**        * FROM NATURAL PROGRAM : Iaap585r
************************************************************
**        * FILE NAME            : Iaap585r.java
**        * CLASS NAME           : Iaap585r
**        * INSTANCE NAME        : Iaap585r
************************************************************
*****************************************************************
* YR2000 COMPLIANT FIX APPLIED  ---> A.MURTHY 04/15/98          *
*                      LAST PCY STOW       ON 05/12/95 09:28:11 *
*                      REVIEWED  BY  ED    ON 05/11/98          *
*   ORIGINALLY TAGGED/NO CHANGES BY  MN    ON 04/15/98          *
*****************************************************************
**Y2CHAM CP IMNDLP
*
**********************************************************************
*                                                                    *
*   PROGRAM     -  IAAP585R   READ WORK FILE (CREATED IN IAAP585S)   *
*      DATE     -  05/95      AND WRITE OUT A DETAILED DAILY PAYMENT *
*    AUTHOR     -  ARI G.     REPORT. IT WILL REPORT ON CURRENT AND  *
*                             FUTURE PAYMENTS AND GIVE DAILY TOTALS. *
*   HISTORY                                                          *
*              10/12/2000     CREATE NEW REPORT FOR LUMP SUM PAYMENT *
*                                                                    *
**********************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap585r extends BLNatBase
{
    // Data Areas
    private LdaIaal585r ldaIaal585r;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Y2_Parm_Date_Yy;

    private DbsGroup pnd_Y2_Parm_Date_Yy__R_Field_1;
    private DbsField pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N;
    private DbsField pnd_Save_Instllmnt_Typ;
    private DbsField pnd_Tot_Cont_Pay;
    private DbsField pnd_Tot_Lump_Pay;
    private DbsField pnd_First;
    private DbsField pnd_Ctr;
    private DbsField pnd_Sub_Title;
    private DbsField pnd_Tot_Ls_Cr_Gross;
    private DbsField pnd_Tot_Ls_Cr_Dci;
    private DbsField pnd_Tot_Ls_Cr_Ovp;
    private DbsField pnd_Tot_Ls_Past_Due;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal585r = new LdaIaal585r();
        registerRecord(ldaIaal585r);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Y2_Parm_Date_Yy = localVariables.newFieldInRecord("pnd_Y2_Parm_Date_Yy", "#Y2-PARM-DATE-YY", FieldType.STRING, 2);

        pnd_Y2_Parm_Date_Yy__R_Field_1 = localVariables.newGroupInRecord("pnd_Y2_Parm_Date_Yy__R_Field_1", "REDEFINE", pnd_Y2_Parm_Date_Yy);
        pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N = pnd_Y2_Parm_Date_Yy__R_Field_1.newFieldInGroup("pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N", "#Y2-PARM-DATE-YY-N", 
            FieldType.NUMERIC, 2);
        pnd_Save_Instllmnt_Typ = localVariables.newFieldInRecord("pnd_Save_Instllmnt_Typ", "#SAVE-INSTLLMNT-TYP", FieldType.STRING, 4);
        pnd_Tot_Cont_Pay = localVariables.newFieldInRecord("pnd_Tot_Cont_Pay", "#TOT-CONT-PAY", FieldType.NUMERIC, 4);
        pnd_Tot_Lump_Pay = localVariables.newFieldInRecord("pnd_Tot_Lump_Pay", "#TOT-LUMP-PAY", FieldType.NUMERIC, 4);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 9);
        pnd_Sub_Title = localVariables.newFieldInRecord("pnd_Sub_Title", "#SUB-TITLE", FieldType.STRING, 60);
        pnd_Tot_Ls_Cr_Gross = localVariables.newFieldInRecord("pnd_Tot_Ls_Cr_Gross", "#TOT-LS-CR-GROSS", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Ls_Cr_Dci = localVariables.newFieldInRecord("pnd_Tot_Ls_Cr_Dci", "#TOT-LS-CR-DCI", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Ls_Cr_Ovp = localVariables.newFieldInRecord("pnd_Tot_Ls_Cr_Ovp", "#TOT-LS-CR-OVP", FieldType.NUMERIC, 11, 2);
        pnd_Tot_Ls_Past_Due = localVariables.newFieldInRecord("pnd_Tot_Ls_Past_Due", "#TOT-LS-PAST-DUE", FieldType.NUMERIC, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal585r.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaap585r() throws Exception
    {
        super("Iaap585r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 56
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        getReports().write(0, "**********************************",NEWLINE,"  START OF IAAP585R REPORT PROGRAM",NEWLINE,"**********************************");            //Natural: WRITE '**********************************' / '  START OF IAAP585R REPORT PROGRAM' / '**********************************'
        if (Global.isEscape()) return;
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 IAA-PARM-CARD
        while (condition(getWorkFiles().read(2, ldaIaal585r.getIaa_Parm_Card())))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM #CHECK-PARM-CARD
        sub_Pnd_Check_Parm_Card();
        if (condition(Global.isEscape())) {return;}
        boolean endOfDataRw = true;                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-RECORD-1
        boolean firstRw = true;
        RW:
        while (condition(getWorkFiles().read(1, ldaIaal585r.getPnd_Work_Record_1())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRw();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRw = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            ldaIaal585r.getPnd_Tot_Cntrct().nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOT-CNTRCT
            if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Contract().notEquals(" ")))                                                                             //Natural: IF #W1-CONTRACT NE ' '
            {
                ldaIaal585r.getPnd_W_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Contract_1_7(),          //Natural: COMPRESS #W1-CONTRACT-1-7 '-' #W1-CONTRACT-8 INTO #W-CONTRACT LEAVING NO
                    "-", ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Contract_8()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaIaal585r.getPnd_W_Contract().setValue(" ");                                                                                                            //Natural: MOVE ' ' TO #W-CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            //*  10/2000
            //*      IF #W1-INSTLLMNT-TYP NE #SAVE-INSTLLMNT-TYP
            //*                                                                                                                                                           //Natural: AT BREAK OF #W1-INSTLLMNT-TYP
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().equals("PP")))                                                                          //Natural: IF #W1-INSTLLMNT-TYP = 'PP'
            {
                pnd_Tot_Cont_Pay.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-CONT-PAY
                                                                                                                                                                          //Natural: PERFORM WRITE-PP-REPORT
                sub_Write_Pp_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().equals("LS")))                                                                          //Natural: IF #W1-INSTLLMNT-TYP = 'LS'
            {
                pnd_Tot_Lump_Pay.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOT-LUMP-PAY
                                                                                                                                                                          //Natural: PERFORM WRITE-LS-REPORT
                sub_Write_Ls_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Save_Instllmnt_Typ.setValue(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ());                                                                     //Natural: MOVE #W1-INSTLLMNT-TYP TO #SAVE-INSTLLMNT-TYP
                                                                                                                                                                          //Natural: PERFORM RESET-FIELDS
            sub_Reset_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RW_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRw(endOfDataRw);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(ldaIaal585r.getPnd_Tot_Cntrct().notEquals(getZero())))                                                                                          //Natural: IF #TOT-CNTRCT NE 0
            {
                                                                                                                                                                          //Natural: PERFORM #PRINT-TOTALS
                sub_Pnd_Print_Totals();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-COUNTERS
                sub_Write_Counters();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().write(0, "**********************************",NEWLINE,"  END OF IAAP585R REPORT PROGRAM",NEWLINE,"**********************************");              //Natural: WRITE '**********************************' / '  END OF IAAP585R REPORT PROGRAM'/ '**********************************'
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PP-REPORT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-LS-REPORT
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #PRINT-TOTALS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADING
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUBHEADING
        //* *COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #PARM-DATE-YY
        //* ***************************
        //* *  YEAR 2000 FIX END     **
        //* ***************************
        //* *Y2NCMN
        //*          48T 'DETAILED DAILY PAYMENT REPORT (LUMPSUM)'
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-FIELDS
        //* *********************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-COUNTERS
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #CHECK-PARM-CARD
        //* *******************************************************************
    }
    private void sub_Write_Pp_Report() throws Exception                                                                                                                   //Natural: WRITE-PP-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        short decideConditionsMet231 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #W1-CR-GROSS > 0
        if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross().greater(getZero())))
        {
            decideConditionsMet231++;
            ldaIaal585r.getPnd_Tot_Current().nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOT-CURRENT
            ldaIaal585r.getPnd_Tot_Cr_Gross().nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross());                                                                   //Natural: ADD #W1-CR-GROSS TO #TOT-CR-GROSS
            ldaIaal585r.getPnd_Tot_Cr_Dci().nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Dci());                                                                       //Natural: ADD #W1-CR-DCI TO #TOT-CR-DCI
            ldaIaal585r.getPnd_Tot_Cr_Ovp().nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp());                                                                       //Natural: ADD #W1-CR-OVP TO #TOT-CR-OVP
        }                                                                                                                                                                 //Natural: WHEN #W1-FT-GROSS > 0
        if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Gross().greater(getZero())))
        {
            decideConditionsMet231++;
            ldaIaal585r.getPnd_Tot_Future().nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOT-FUTURE
            ldaIaal585r.getPnd_Tot_Ft_Gross().nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Gross());                                                                   //Natural: ADD #W1-FT-GROSS TO #TOT-FT-GROSS
            ldaIaal585r.getPnd_Tot_Ft_Ovp().nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Ovp());                                                                       //Natural: ADD #W1-FT-OVP TO #TOT-FT-OVP
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet231 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Sub_Title.setValue(" DETAILED DAILY PAYMENT REPORT (CONTINUED) ");                                                                                            //Natural: MOVE ' DETAILED DAILY PAYMENT REPORT (CONTINUED) ' TO #SUB-TITLE
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaIaal585r.getPnd_W_Contract(),new TabSetting(17),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Payee_Cde(),new     //Natural: WRITE ( 1 ) / #W-CONTRACT 017T #W1-PAYEE-CDE 028T #W1-CR-GROSS ( EM = ZZZ,ZZZ,ZZ9.99 ) 046T #W1-CR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 ) 064T #W1-CR-DCI ( EM = ZZZ,ZZZ,ZZ9.99 ) 090T #W1-FT-GROSS ( EM = ZZZ,ZZZ,ZZ9.99 ) 108T #W1-FT-OVP ( EM = ZZZ,ZZZ,ZZ9.99 )
            TabSetting(28),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(46),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(64),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Dci(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(90),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Gross(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(108),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Ovp(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Ls_Report() throws Exception                                                                                                                   //Natural: WRITE-LS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        ldaIaal585r.getPnd_Tot_Current().nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-CURRENT
        pnd_Tot_Ls_Cr_Gross.nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross());                                                                                     //Natural: ADD #W1-CR-GROSS TO #TOT-LS-CR-GROSS
        pnd_Tot_Ls_Cr_Dci.nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Dci());                                                                                         //Natural: ADD #W1-CR-DCI TO #TOT-LS-CR-DCI
        pnd_Tot_Ls_Cr_Ovp.nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp());                                                                                         //Natural: ADD #W1-CR-OVP TO #TOT-LS-CR-OVP
        pnd_Tot_Ls_Past_Due.nadd(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Past_Due());                                                                                     //Natural: ADD #W1-PAST-DUE TO #TOT-LS-PAST-DUE
        pnd_Ctr.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #CTR
        pnd_Sub_Title.setValue(" DETAILED DAILY PAYMENT REPORT (LUMP SUM) ");                                                                                             //Natural: MOVE ' DETAILED DAILY PAYMENT REPORT (LUMP SUM) ' TO #SUB-TITLE
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaIaal585r.getPnd_W_Contract(),new TabSetting(17),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Payee_Cde(),new     //Natural: WRITE ( 1 ) / #W-CONTRACT 017T #W1-PAYEE-CDE 021T #W1-PAST-DUE ( EM = Z,ZZZ,ZZ9.99 ) 040T #W1-CR-GROSS ( EM = ZZZ,ZZZ,ZZ9.99 ) 058T #W1-CR-OVP ( EM = ZZZ,ZZZ,ZZ9.99 ) 079T #W1-CR-DCI ( EM = ZZZ,ZZZ,ZZ9.99 )
            TabSetting(21),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Past_Due(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(40),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(58),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(79),ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Dci(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Pnd_Print_Totals() throws Exception                                                                                                                  //Natural: #PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Save_Instllmnt_Typ.equals("PP")))                                                                                                               //Natural: IF #SAVE-INSTLLMNT-TYP = 'PP'
        {
            pnd_Sub_Title.setValue(" DETAILED DAILY PAYMENT REPORT (CONTINUED) ");                                                                                        //Natural: MOVE ' DETAILED DAILY PAYMENT REPORT (CONTINUED) ' TO #SUB-TITLE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Instllmnt_Typ.equals("LS")))                                                                                                               //Natural: IF #SAVE-INSTLLMNT-TYP = 'LS'
        {
            pnd_Sub_Title.setValue(" DETAILED DAILY PAYMENT REPORT (LUMP SUM) ");                                                                                         //Natural: MOVE ' DETAILED DAILY PAYMENT REPORT (LUMP SUM) ' TO #SUB-TITLE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLinesLeft(1).less(17)))                                                                                                          //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 17 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        if (condition(pnd_Save_Instllmnt_Typ.equals("PP")))                                                                                                               //Natural: IF #SAVE-INSTLLMNT-TYP = 'PP'
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(59),"REPORT TOTALS",NEWLINE,new TabSetting(59),"-------------");                    //Natural: WRITE ( 1 ) // 59T 'REPORT TOTALS' / 59T '-------------'
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"NUMBER OF CONTRACTS SETTLED => ",pnd_Tot_Cont_Pay);                                                               //Natural: WRITE ( 1 ) 'NUMBER OF CONTRACTS SETTLED => ' #TOT-CONT-PAY
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(14),"- CURRENT PAYMENTS -",new TabSetting(87),"- FUTURE PAYMENTS -");                       //Natural: WRITE ( 1 ) / 14T '- CURRENT PAYMENTS -' 87T '- FUTURE PAYMENTS -'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"NUMBER OF PAYMENTS ===>",new ColumnSpacing(10),ldaIaal585r.getPnd_Tot_Current(), new ReportEditMask       //Natural: WRITE ( 1 ) / 'NUMBER OF PAYMENTS ===>' 10X #TOT-CURRENT ( EM = ZZZ9 ) 74T 'NUMBER OF PAYMENTS ==>' 10X #TOT-FUTURE ( EM = ZZZ9 )
                ("ZZZ9"),new TabSetting(74),"NUMBER OF PAYMENTS ==>",new ColumnSpacing(10),ldaIaal585r.getPnd_Tot_Future(), new ReportEditMask ("ZZZ9"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"GROSS AMOUNT =========>",ldaIaal585r.getPnd_Tot_Cr_Gross(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new           //Natural: WRITE ( 1 ) 'GROSS AMOUNT =========>' #TOT-CR-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 74T 'GROSS AMOUNT ========>' #TOT-FT-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
                TabSetting(74),"GROSS AMOUNT ========>",ldaIaal585r.getPnd_Tot_Ft_Gross(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"OVERPAYMENT AMOUNT ===>",ldaIaal585r.getPnd_Tot_Cr_Ovp(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new             //Natural: WRITE ( 1 ) 'OVERPAYMENT AMOUNT ===>' #TOT-CR-OVP ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 74T 'OVERPAYMENT AMOUNT ==>' #TOT-FT-OVP ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
                TabSetting(74),"OVERPAYMENT AMOUNT ==>",ldaIaal585r.getPnd_Tot_Ft_Ovp(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"DCI AMOUNT ===========>",ldaIaal585r.getPnd_Tot_Cr_Dci(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));               //Natural: WRITE ( 1 ) 'DCI AMOUNT ===========>' #TOT-CR-DCI ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));              //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Instllmnt_Typ.equals("LS")))                                                                                                               //Natural: IF #SAVE-INSTLLMNT-TYP = 'LS'
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(30),"REPORT TOTALS",NEWLINE,new TabSetting(30),"-------------",NEWLINE,NEWLINE,NEWLINE,"NUMBER OF CONTRACTS SETTLED =>       ",pnd_Tot_Lump_Pay,NEWLINE,new  //Natural: WRITE ( 1 ) // 30T 'REPORT TOTALS' / 30T '-------------' // / 'NUMBER OF CONTRACTS SETTLED =>       ' #TOT-LUMP-PAY / 14T '- CURRENT PAYMENTS -' / 'NUMBER OF PAYMENTS =========>' 10X #TOT-CURRENT ( EM = ZZZ9 ) / 'PAST DUE AMOUNT  ===========>' 5X #TOT-LS-PAST-DUE ( EM = Z,ZZZ,ZZ9.99 ) / 'COMMUTED VALUE AMOUNT ======>' #TOT-LS-CR-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'OVERPAYMENT AMOUNT =========>' #TOT-LS-CR-OVP ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 'DCI AMOUNT =================>' #TOT-LS-CR-DCI ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
                TabSetting(14),"- CURRENT PAYMENTS -",NEWLINE,"NUMBER OF PAYMENTS =========>",new ColumnSpacing(10),ldaIaal585r.getPnd_Tot_Current(), new 
                ReportEditMask ("ZZZ9"),NEWLINE,"PAST DUE AMOUNT  ===========>",new ColumnSpacing(5),pnd_Tot_Ls_Past_Due, new ReportEditMask ("Z,ZZZ,ZZ9.99"),NEWLINE,"COMMUTED VALUE AMOUNT ======>",pnd_Tot_Ls_Cr_Gross, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"OVERPAYMENT AMOUNT =========>",pnd_Tot_Ls_Cr_Ovp, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"DCI AMOUNT =================>",pnd_Tot_Ls_Cr_Dci, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(57),new TabSetting(59),"END OF REPORT",new TabSetting(73),"-",new RepeatItem(59));              //Natural: WRITE ( 1 ) '-' ( 57 ) 59T 'END OF REPORT' 73T '-' ( 59 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Instllmnt_Typ.reset();                                                                                                                                   //Natural: RESET #SAVE-INSTLLMNT-TYP #TOT-CR-OVP #TOT-CR-DCI #TOT-CURRENT #TOT-FT-OVP #TOT-FT-GROSS #TOT-FUTURE
        ldaIaal585r.getPnd_Tot_Cr_Ovp().reset();
        ldaIaal585r.getPnd_Tot_Cr_Dci().reset();
        ldaIaal585r.getPnd_Tot_Current().reset();
        ldaIaal585r.getPnd_Tot_Ft_Ovp().reset();
        ldaIaal585r.getPnd_Tot_Ft_Gross().reset();
        ldaIaal585r.getPnd_Tot_Future().reset();
    }
    private void sub_Write_Heading() throws Exception                                                                                                                     //Natural: WRITE-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) NOTITLE ' '
        if (Global.isEscape()) return;
        ldaIaal585r.getPnd_Page_Ctr().nadd(1);                                                                                                                            //Natural: ADD 1 TO #PAGE-CTR
        getReports().write(1, ReportOption.NOTITLE,"PROGRAM ",Global.getPROGRAM(),new TabSetting(46),"IA ADMINISTRATION - DEATH CLAIMS PROCESSING",new                    //Natural: WRITE ( 1 ) 'PROGRAM ' *PROGRAM 46T 'IA ADMINISTRATION - DEATH CLAIMS PROCESSING' 119T 'PAGE ' #PAGE-CTR
            TabSetting(119),"PAGE ",ldaIaal585r.getPnd_Page_Ctr());
        if (Global.isEscape()) return;
    }
    private void sub_Print_Subheading() throws Exception                                                                                                                  //Natural: PRINT-SUBHEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************
        //*  10/2000
        if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().equals("PP")))                                                                              //Natural: IF #W1-INSTLLMNT-TYP = 'PP'
        {
            //*  ALL RECORDS WITH 'PP' INSTLLMNT-TYP (CONTINUED PAYMENT)
            //* *Y2NCMN
            getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(48),pnd_Sub_Title);                                                     //Natural: WRITE ( 1 ) '   DATE ' *DATU 48T #SUB-TITLE
            if (Global.isEscape()) return;
            //* ***************************
            //* *  YEAR 2000 FIX STARTS  **
            //* ***************************
            //* *Y2CHAM
            pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N.setValue(ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Yy());                                                          //Natural: MOVE #PARM-DATE-YY TO #Y2-PARM-DATE-YY-N
            ldaIaal585r.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),                    //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                "/", ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy));
            //* *Y2NCMN
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(56)," FOR CHECK DATE",new TabSetting(72),ldaIaal585r.getPnd_W_Parm_Date());                         //Natural: WRITE ( 1 ) 56T ' FOR CHECK DATE' 72T #W-PARM-DATE
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(28),"------------------- CURRENT ----------------------",new TabSetting(90),"----------- FUTURE -------------"); //Natural: WRITE ( 1 ) 028T '------------------- CURRENT ----------------------' 090T '----------- FUTURE -------------'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1)," CONTRACT",new TabSetting(16),"PAYEE",new TabSetting(28),"    GROSS     ",new                   //Natural: WRITE ( 1 ) 001T ' CONTRACT' 016T 'PAYEE' 028T '    GROSS     ' 046T '  OVERPAYMENT ' 064T '     DCI      ' 090T '    GROSS     ' 108T '  OVERPAYMENT '
                TabSetting(46),"  OVERPAYMENT ",new TabSetting(64),"     DCI      ",new TabSetting(90),"    GROSS     ",new TabSetting(108),"  OVERPAYMENT ");
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(16),"-----",new TabSetting(28),"--------------",new                   //Natural: WRITE ( 1 ) 001T '---------' 016T '-----' 028T '--------------' 046T '--------------' 064T '--------------' 090T '--------------' 108T '--------------'
                TabSetting(46),"--------------",new TabSetting(64),"--------------",new TabSetting(90),"--------------",new TabSetting(108),"--------------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().equals("LS")))                                                                              //Natural: IF #W1-INSTLLMNT-TYP = 'LS'
        {
            //*  ALL RECORDS WITH 'LS' INSTLLMNT-TYP (LUMP SUM PAYMENT)
            getReports().write(1, ReportOption.NOTITLE,"   DATE ",Global.getDATU(),new TabSetting(48),pnd_Sub_Title);                                                     //Natural: WRITE ( 1 ) '   DATE ' *DATU 48T #SUB-TITLE
            if (Global.isEscape()) return;
            pnd_Y2_Parm_Date_Yy_Pnd_Y2_Parm_Date_Yy_N.setValue(ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Yy());                                                          //Natural: MOVE #PARM-DATE-YY TO #Y2-PARM-DATE-YY-N
            ldaIaal585r.getPnd_W_Parm_Date().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Mm(),                    //Natural: COMPRESS #PARM-DATE-MM '/' #PARM-DATE-DD '/' #Y2-PARM-DATE-YY INTO #W-PARM-DATE LEAVING NO
                "/", ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date_Dd(), "/", pnd_Y2_Parm_Date_Yy));
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(56)," FOR CHECK DATE",new TabSetting(72),ldaIaal585r.getPnd_W_Parm_Date());                         //Natural: WRITE ( 1 ) 56T ' FOR CHECK DATE' 72T #W-PARM-DATE
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(28),"------------------- CURRENT ----------------------",NEWLINE,new TabSetting(1)," CONTRACT ",new //Natural: WRITE ( 1 ) 028T '------------------- CURRENT ----------------------' / 001T ' CONTRACT ' 016T 'PAYEE' 025T ' PAST DUE ' 040T 'COMMUTED VALUE' 060T ' OVERPAYMENT ' 082T '   DCI     '
                TabSetting(16),"PAYEE",new TabSetting(25)," PAST DUE ",new TabSetting(40),"COMMUTED VALUE",new TabSetting(60)," OVERPAYMENT ",new TabSetting(82),
                "   DCI     ");
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"---------",new TabSetting(16),"-----",new TabSetting(25),"----------",new TabSetting(40),"--------------",new  //Natural: WRITE ( 1 ) 001T '---------' 016T '-----' 025T '----------' 040T '--------------' 060T '-------------' 082T '-----------'
                TabSetting(60),"-------------",new TabSetting(82),"-----------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Reset_Fields() throws Exception                                                                                                                      //Natural: RESET-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaIaal585r.getPnd_W_Contract().reset();                                                                                                                          //Natural: RESET #W-CONTRACT #W1-PAYEE-CDE #W1-CR-GROSS #W1-CR-DCI #W1-CR-OVP #W1-PAST-DUE #W1-FT-GROSS #W1-FT-OVP
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Payee_Cde().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Gross().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Dci().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Cr_Ovp().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Past_Due().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Gross().reset();
        ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Ft_Ovp().reset();
    }
    private void sub_Write_Counters() throws Exception                                                                                                                    //Natural: WRITE-COUNTERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        getReports().write(0, "WORK FILE READS =====> ",ldaIaal585r.getPnd_Tot_Cntrct());                                                                                 //Natural: WRITE 'WORK FILE READS =====> ' #TOT-CNTRCT
        if (Global.isEscape()) return;
        getReports().write(0, "CONTINUED PAYMENT ===> ",pnd_Tot_Cont_Pay);                                                                                                //Natural: WRITE 'CONTINUED PAYMENT ===> ' #TOT-CONT-PAY
        if (Global.isEscape()) return;
        getReports().write(0, "LUMP SUM PAYMENT ====> ",pnd_Tot_Lump_Pay);                                                                                                //Natural: WRITE 'LUMP SUM PAYMENT ====> ' #TOT-LUMP-PAY
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Tot_Cont_Pay.reset();                                                                                                                                         //Natural: RESET #TOT-CONT-PAY #TOT-LUMP-PAY
        pnd_Tot_Lump_Pay.reset();
    }
    private void sub_Pnd_Check_Parm_Card() throws Exception                                                                                                               //Natural: #CHECK-PARM-CARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //* *Y2NCMN
        if (condition(DbsUtil.maskMatches(ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date(),"YYYYMMDD")))                                                                      //Natural: IF #PARM-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            getReports().write(0, "          PARM DATE ERROR ");                                                                                                          //Natural: WRITE '          PARM DATE ERROR '
            if (Global.isEscape()) return;
            getReports().write(0, "**********************************");                                                                                                  //Natural: WRITE '**********************************'
            if (Global.isEscape()) return;
            //* *Y2NCMN
            getReports().write(0, NEWLINE,"     PARM DATE ====> ",ldaIaal585r.getIaa_Parm_Card_Pnd_Parm_Date());                                                          //Natural: WRITE / '     PARM DATE ====> ' #PARM-DATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADING
                    sub_Write_Heading();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBHEADING
                    sub_Print_Subheading();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRw() throws Exception {atBreakEventRw(false);}
    private void atBreakEventRw(boolean endOfData) throws Exception
    {
        boolean ldaIaal585r_getPnd_Work_Record_1_Pnd_W1_Instllmnt_TypIsBreak = ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().isBreak(endOfData);
        if (condition(ldaIaal585r_getPnd_Work_Record_1_Pnd_W1_Instllmnt_TypIsBreak))
        {
            if (condition(ldaIaal585r.getPnd_Work_Record_1_Pnd_W1_Instllmnt_Typ().notEquals(pnd_Save_Instllmnt_Typ)))                                                     //Natural: IF #W1-INSTLLMNT-TYP NE #SAVE-INSTLLMNT-TYP
            {
                                                                                                                                                                          //Natural: PERFORM #PRINT-TOTALS
                sub_Pnd_Print_Totals();
                if (condition(Global.isEscape())) {return;}
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=56");
    }
}
