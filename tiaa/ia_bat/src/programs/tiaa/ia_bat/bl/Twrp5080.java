/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:30 PM
**        * FROM NATURAL PROGRAM : Twrp5080
************************************************************
**        * FILE NAME            : Twrp5080.java
**        * CLASS NAME           : Twrp5080
**        * INSTANCE NAME        : Twrp5080
************************************************************
**--------------------------------------------------------------------**
* PROGRAM     : TWRP5080                                               *
* SYSTEM      : TAXWARS - TAX WITHHOLDING AND REPORTING SYSTEM.        *
* FUNCTION    : AUTOMATE IVC MAINTENANCE DATE PARM PROCESSING TO       *
*             : ELIMINATE THE NEED FOR MONTHLY PARM UPDATES AND        *
*             : MIGRATIONS.                                            *
* CREATED     : 10/15/2007                                             *
* AUTHOR      : ALTHEA A. YOUNG                                        *
**--------------------------------------------------------------------**
* HISTORY     :                                                        *
* 10-30-2007  : A. YOUNG     - ADDED 'TWRA0001' AND 'TWRN0001' FOR     *
*             :                ENVIRONMENT IDENTIFICATION.             *
**--------------------------------------------------------------------**
* ERROR CODES : ??? - RETURN CODE FROM TWRN0001 OR SYSTEM.             *
*             : 090 - BLANK 'AP' CONTROL FILE.                         *
*             : 091 - INVALID 'AP' CONTROL SOURCE CODE.                *
*             : 092 - BLANK IVC DATE FILE.                             *
*             : 093 - INVALID IVC DATE FILE.                           *
*             : 094 - DUPLICATE RUN ATTEMPTED.                         *
**--------------------------------------------------------------------**
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5080 extends BLNatBase
{
    // Data Areas
    private PdaTwra0001 pdaTwra0001;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ivc_Prev_Date_Card;

    private DbsGroup pnd_Ivc_Prev_Date_Card__R_Field_1;
    private DbsField pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd;
    private DbsField pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Special_Run;
    private DbsField pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd;

    private DbsGroup pnd_Ap_Control_Record;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_Control_Rec;

    private DbsGroup pnd_Ap_Control_Record__R_Field_2;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_Origin_Code;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd;

    private DbsGroup pnd_Ap_Control_Record__R_Field_3;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_From_Mm;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_From_Dd;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_Filler;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_To_Ccyymmdd;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_Tax_Year_Ccyy;
    private DbsField pnd_Ap_Control_Record_Pnd_Ap_Interface_Ccyymmdd;
    private DbsField pnd_Work_Card;

    private DbsGroup pnd_Work_Card__R_Field_4;
    private DbsField pnd_Work_Card_Pnd_Work_Ccyymmdd;

    private DbsGroup pnd_Work_Card__R_Field_5;
    private DbsField pnd_Work_Card_Pnd_Work_Ccyy;
    private DbsField pnd_Work_Card_Pnd_Work_Mm;
    private DbsField pnd_Work_Card_Pnd_Work_Dd;
    private DbsField pnd_Work_Card_Pnd_Work_Special_Run;
    private DbsField pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd;

    private DbsGroup pnd_Work_Card__R_Field_6;
    private DbsField pnd_Work_Card_Pnd_Wk_Int_Ccyy;
    private DbsField pnd_Work_Card_Pnd_Wk_Int_Mm;
    private DbsField pnd_Work_Card_Pnd_Wk_Int_Dd;
    private DbsField pnd_Return_Code_N;
    private DbsField pnd_System_Date_N;

    private DbsGroup pnd_System_Date_N__R_Field_7;
    private DbsField pnd_System_Date_N_Pnd_System_Date_N_Yyyy;
    private DbsField pnd_System_Date_N_Pnd_System_Date_N_Mm;
    private DbsField pnd_System_Date_N_Pnd_System_Date_N_Dd;

    private DbsGroup pnd_System_Date_N__R_Field_8;
    private DbsField pnd_System_Date_N_Pnd_System_Date_A;
    private DbsField pnd_Datn;
    private DbsField pnd_Dup_Run;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwra0001 = new PdaTwra0001(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        pnd_Ivc_Prev_Date_Card = localVariables.newFieldInRecord("pnd_Ivc_Prev_Date_Card", "#IVC-PREV-DATE-CARD", FieldType.STRING, 17);

        pnd_Ivc_Prev_Date_Card__R_Field_1 = localVariables.newGroupInRecord("pnd_Ivc_Prev_Date_Card__R_Field_1", "REDEFINE", pnd_Ivc_Prev_Date_Card);
        pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd = pnd_Ivc_Prev_Date_Card__R_Field_1.newFieldInGroup("pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd", 
            "#IVC-PREV-DATE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Special_Run = pnd_Ivc_Prev_Date_Card__R_Field_1.newFieldInGroup("pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Special_Run", 
            "#IVC-PREV-SPECIAL-RUN", FieldType.STRING, 1);
        pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd = pnd_Ivc_Prev_Date_Card__R_Field_1.newFieldInGroup("pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd", 
            "#IVC-PREV-INTFCE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ap_Control_Record = localVariables.newGroupInRecord("pnd_Ap_Control_Record", "#AP-CONTROL-RECORD");
        pnd_Ap_Control_Record_Pnd_Ap_Control_Rec = pnd_Ap_Control_Record.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_Control_Rec", "#AP-CONTROL-REC", 
            FieldType.STRING, 31);

        pnd_Ap_Control_Record__R_Field_2 = pnd_Ap_Control_Record.newGroupInGroup("pnd_Ap_Control_Record__R_Field_2", "REDEFINE", pnd_Ap_Control_Record_Pnd_Ap_Control_Rec);
        pnd_Ap_Control_Record_Pnd_Ap_Origin_Code = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_Origin_Code", "#AP-ORIGIN-CODE", 
            FieldType.STRING, 2);
        pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd", "#AP-FROM-CCYYMMDD", 
            FieldType.NUMERIC, 8);

        pnd_Ap_Control_Record__R_Field_3 = pnd_Ap_Control_Record__R_Field_2.newGroupInGroup("pnd_Ap_Control_Record__R_Field_3", "REDEFINE", pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd);
        pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy = pnd_Ap_Control_Record__R_Field_3.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy", "#AP-FROM-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Ap_Control_Record_Pnd_Ap_From_Mm = pnd_Ap_Control_Record__R_Field_3.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_From_Mm", "#AP-FROM-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Control_Record_Pnd_Ap_From_Dd = pnd_Ap_Control_Record__R_Field_3.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_From_Dd", "#AP-FROM-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Control_Record_Pnd_Ap_Filler = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_Filler", "#AP-FILLER", FieldType.STRING, 
            1);
        pnd_Ap_Control_Record_Pnd_Ap_To_Ccyymmdd = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_To_Ccyymmdd", "#AP-TO-CCYYMMDD", 
            FieldType.NUMERIC, 8);
        pnd_Ap_Control_Record_Pnd_Ap_Tax_Year_Ccyy = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_Tax_Year_Ccyy", "#AP-TAX-YEAR-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Ap_Control_Record_Pnd_Ap_Interface_Ccyymmdd = pnd_Ap_Control_Record__R_Field_2.newFieldInGroup("pnd_Ap_Control_Record_Pnd_Ap_Interface_Ccyymmdd", 
            "#AP-INTERFACE-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Work_Card = localVariables.newFieldInRecord("pnd_Work_Card", "#WORK-CARD", FieldType.STRING, 17);

        pnd_Work_Card__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Card__R_Field_4", "REDEFINE", pnd_Work_Card);
        pnd_Work_Card_Pnd_Work_Ccyymmdd = pnd_Work_Card__R_Field_4.newFieldInGroup("pnd_Work_Card_Pnd_Work_Ccyymmdd", "#WORK-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Work_Card__R_Field_5 = pnd_Work_Card__R_Field_4.newGroupInGroup("pnd_Work_Card__R_Field_5", "REDEFINE", pnd_Work_Card_Pnd_Work_Ccyymmdd);
        pnd_Work_Card_Pnd_Work_Ccyy = pnd_Work_Card__R_Field_5.newFieldInGroup("pnd_Work_Card_Pnd_Work_Ccyy", "#WORK-CCYY", FieldType.NUMERIC, 4);
        pnd_Work_Card_Pnd_Work_Mm = pnd_Work_Card__R_Field_5.newFieldInGroup("pnd_Work_Card_Pnd_Work_Mm", "#WORK-MM", FieldType.NUMERIC, 2);
        pnd_Work_Card_Pnd_Work_Dd = pnd_Work_Card__R_Field_5.newFieldInGroup("pnd_Work_Card_Pnd_Work_Dd", "#WORK-DD", FieldType.NUMERIC, 2);
        pnd_Work_Card_Pnd_Work_Special_Run = pnd_Work_Card__R_Field_4.newFieldInGroup("pnd_Work_Card_Pnd_Work_Special_Run", "#WORK-SPECIAL-RUN", FieldType.STRING, 
            1);
        pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd = pnd_Work_Card__R_Field_4.newFieldInGroup("pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd", "#WK-INT-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Work_Card__R_Field_6 = pnd_Work_Card__R_Field_4.newGroupInGroup("pnd_Work_Card__R_Field_6", "REDEFINE", pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd);
        pnd_Work_Card_Pnd_Wk_Int_Ccyy = pnd_Work_Card__R_Field_6.newFieldInGroup("pnd_Work_Card_Pnd_Wk_Int_Ccyy", "#WK-INT-CCYY", FieldType.NUMERIC, 4);
        pnd_Work_Card_Pnd_Wk_Int_Mm = pnd_Work_Card__R_Field_6.newFieldInGroup("pnd_Work_Card_Pnd_Wk_Int_Mm", "#WK-INT-MM", FieldType.NUMERIC, 2);
        pnd_Work_Card_Pnd_Wk_Int_Dd = pnd_Work_Card__R_Field_6.newFieldInGroup("pnd_Work_Card_Pnd_Wk_Int_Dd", "#WK-INT-DD", FieldType.NUMERIC, 2);
        pnd_Return_Code_N = localVariables.newFieldInRecord("pnd_Return_Code_N", "#RETURN-CODE-N", FieldType.NUMERIC, 2);
        pnd_System_Date_N = localVariables.newFieldInRecord("pnd_System_Date_N", "#SYSTEM-DATE-N", FieldType.NUMERIC, 8);

        pnd_System_Date_N__R_Field_7 = localVariables.newGroupInRecord("pnd_System_Date_N__R_Field_7", "REDEFINE", pnd_System_Date_N);
        pnd_System_Date_N_Pnd_System_Date_N_Yyyy = pnd_System_Date_N__R_Field_7.newFieldInGroup("pnd_System_Date_N_Pnd_System_Date_N_Yyyy", "#SYSTEM-DATE-N-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_System_Date_N_Pnd_System_Date_N_Mm = pnd_System_Date_N__R_Field_7.newFieldInGroup("pnd_System_Date_N_Pnd_System_Date_N_Mm", "#SYSTEM-DATE-N-MM", 
            FieldType.NUMERIC, 2);
        pnd_System_Date_N_Pnd_System_Date_N_Dd = pnd_System_Date_N__R_Field_7.newFieldInGroup("pnd_System_Date_N_Pnd_System_Date_N_Dd", "#SYSTEM-DATE-N-DD", 
            FieldType.NUMERIC, 2);

        pnd_System_Date_N__R_Field_8 = localVariables.newGroupInRecord("pnd_System_Date_N__R_Field_8", "REDEFINE", pnd_System_Date_N);
        pnd_System_Date_N_Pnd_System_Date_A = pnd_System_Date_N__R_Field_8.newFieldInGroup("pnd_System_Date_N_Pnd_System_Date_A", "#SYSTEM-DATE-A", FieldType.STRING, 
            8);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.DATE);
        pnd_Dup_Run = localVariables.newFieldInRecord("pnd_Dup_Run", "#DUP-RUN", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_System_Date_N.setInitialValue(0);
        pnd_Dup_Run.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5080() throws Exception
    {
        super("Twrp5080");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp5080|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *===================================================================**
                //*                        M A I N   P R O G R A M                       *
                //* *===================================================================**
                                                                                                                                                                          //Natural: PERFORM ENVIRONMENT-IDENTIFICATION
                sub_Environment_Identification();
                if (condition(Global.isEscape())) {return;}
                if (condition(pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return().equals("00"), INPUT_1))                                                                       //Natural: IF #TWRN0001-RETURN = '00'
                {
                    if (condition(pdaTwra0001.getTwra0001_Pnd_Prod_Test().equals("TEST"), INPUT_1))                                                                       //Natural: IF TWRA0001.#PROD-TEST = 'TEST'
                    {
                        DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_System_Date_N);                                                                            //Natural: INPUT #SYSTEM-DATE-N
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_System_Date_N.equals(getZero())))                                                                                                   //Natural: IF #SYSTEM-DATE-N = 0
                    {
                        pnd_System_Date_N.setValue(Global.getDATN());                                                                                                     //Natural: ASSIGN #SYSTEM-DATE-N := *DATN
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Datn.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_System_Date_N_Pnd_System_Date_A);                                                          //Natural: MOVE EDITED #SYSTEM-DATE-A TO #DATN ( EM = YYYYMMDD )
                    getReports().write(0, "=",pnd_System_Date_N);                                                                                                         //Natural: WRITE ( 00 ) '=' #SYSTEM-DATE-N
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM AP-CONTROL-PROCESSING
                    sub_Ap_Control_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DUPLICATE-RUN-DETECTION
                    sub_Duplicate_Run_Detection();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IVC-CONTROL-PROCESSING
                    sub_Ivc_Control_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Return_Code_N.compute(new ComputeParameters(false, pnd_Return_Code_N), pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return().val());                      //Natural: ASSIGN #RETURN-CODE-N := VAL ( #TWRN0001-RETURN )
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(6),"INVALID RETURN CODE FROM TWRN0001!",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Return Code From Environment Routine is:",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return(),new  //Natural: WRITE ( 00 ) '***' 06T 'INVALID RETURN CODE FROM TWRN0001!' 77T '***' / '***' 06T 'Return Code From Environment Routine is:' #TWRN0001-RETURN 77T '***'
                        TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(pnd_Return_Code_N);  if (true) return;                                                                                              //Natural: TERMINATE #RETURN-CODE-N
                }                                                                                                                                                         //Natural: END-IF
                //* *===================================================================**
                //*                         S U B R O U T I N E S                        *
                //* *===================================================================**
                //* *-----------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AP-CONTROL-PROCESSING
                //* *-------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUPLICATE-RUN-DETECTION
                //* *------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IVC-CONTROL-PROCESSING
                //*  ENVIRONMENT ROUTINE
                //* *----------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ENVIRONMENT-IDENTIFICATION
                //* *----------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Ap_Control_Processing() throws Exception                                                                                                             //Natural: AP-CONTROL-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------**
        //*  LATEST 'AP' CONTROL
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK 01 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"'AP' CONTROL FILE ERROR - 090",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"'AP' Control File is empty!",new  //Natural: WRITE ( 00 ) '***' 06T '"AP" CONTROL FILE ERROR - 090' 77T '***' / '***' 06T '"AP" Control File is empty!' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  BLANK 'AP' CTRL FILE
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AP")))                                                                //Natural: IF #TWRP0600-ORIGIN-CODE = 'AP'
        {
            getReports().write(0, "'AP'  Control BEFORE:      ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd(), //Natural: WRITE ( 00 ) '"AP"  Control BEFORE:      ' #TWRP0600-CONTROL-RECORD
                ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Filler(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),
                ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());
            if (Global.isEscape()) return;
            pnd_Ap_Control_Record_Pnd_Ap_Origin_Code.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code());                                     //Natural: ASSIGN #AP-ORIGIN-CODE := #TWRP0600-ORIGIN-CODE
            pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd.compute(new ComputeParameters(false, pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd), ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().val()); //Natural: ASSIGN #AP-FROM-CCYYMMDD := VAL ( #TWRP0600-FROM-CCYYMMDD )
            pnd_Ap_Control_Record_Pnd_Ap_Filler.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Filler());                                               //Natural: ASSIGN #AP-FILLER := #TWRP0600-FILLER
            pnd_Ap_Control_Record_Pnd_Ap_To_Ccyymmdd.compute(new ComputeParameters(false, pnd_Ap_Control_Record_Pnd_Ap_To_Ccyymmdd), ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().val()); //Natural: ASSIGN #AP-TO-CCYYMMDD := VAL ( #TWRP0600-TO-CCYYMMDD )
            pnd_Ap_Control_Record_Pnd_Ap_Tax_Year_Ccyy.compute(new ComputeParameters(false, pnd_Ap_Control_Record_Pnd_Ap_Tax_Year_Ccyy), ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().val()); //Natural: ASSIGN #AP-TAX-YEAR-CCYY := VAL ( #TWRP0600-TAX-YEAR-CCYY )
            pnd_Ap_Control_Record_Pnd_Ap_Interface_Ccyymmdd.compute(new ComputeParameters(false, pnd_Ap_Control_Record_Pnd_Ap_Interface_Ccyymmdd), ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().val()); //Natural: ASSIGN #AP-INTERFACE-CCYYMMDD := VAL ( #TWRP0600-INTERFACE-CCYYMMDD )
            getReports().write(0, "'AP'  Control AFTER:       ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd(), //Natural: WRITE ( 00 ) '"AP"  Control AFTER:       ' #TWRP0600-CONTROL-RECORD
                ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Filler(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),
                ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,NEWLINE,"'AP'  Control Working Copy:",pnd_Ap_Control_Record_Pnd_Ap_Control_Rec);                                                //Natural: WRITE ( 00 ) // '"AP"  Control Working Copy:' #AP-CONTROL-RECORD
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"'AP' CONTROL FILE ERROR - 091",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid Source Code is 'AP'.  File Source Code is",new  //Natural: WRITE ( 00 ) '***' 06T '"AP" CONTROL FILE ERROR - 091' 77T '***' / '***' 06T 'Valid Source Code is "AP".  File Source Code is' 54T #TWRP0600-ORIGIN-CODE '!' 77T '***'
                TabSetting(54),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),"!",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  INVALID SOURCE CODE
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
        //*  AP-CONTROL-PROCESSING
    }
    private void sub_Duplicate_Run_Detection() throws Exception                                                                                                           //Natural: DUPLICATE-RUN-DETECTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------**
        //*  PREVIOUS IVC DATES
        getWorkFiles().read(2, pnd_Ivc_Prev_Date_Card);                                                                                                                   //Natural: READ WORK 02 ONCE #IVC-PREV-DATE-CARD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( 00 )
            if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"'IVC' PREVIOUS DATE FILE ERROR - 092",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"'IVC' Previous Date File is empty!",new  //Natural: WRITE ( 00 ) '***' 06T '"IVC" PREVIOUS DATE FILE ERROR - 092' 77T '***' / '***' 06T '"IVC" Previous Date File is empty!' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  BLANK IVC DATE CARD
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(DbsUtil.maskMatches(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd,"NNNNNNNN") && DbsUtil.maskMatches(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd, //Natural: IF #IVC-PREV-DATE-CCYYMMDD = MASK ( NNNNNNNN ) AND #IVC-PREV-INTFCE-CCYYMMDD = MASK ( NNNNNNNN )
            "NNNNNNNN")))
        {
            pnd_Work_Card_Pnd_Work_Ccyymmdd.setValue(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd);                                                                  //Natural: ASSIGN #WORK-CCYYMMDD := #IVC-PREV-DATE-CCYYMMDD
            pnd_Work_Card_Pnd_Work_Special_Run.setValue(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Special_Run);                                                                 //Natural: ASSIGN #WORK-SPECIAL-RUN := #IVC-PREV-SPECIAL-RUN
            pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd.setValue(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd);                                                              //Natural: ASSIGN #WK-INT-CCYYMMDD := #IVC-PREV-INTFCE-CCYYMMDD
            getReports().write(0, NEWLINE,NEWLINE,"'IVC' Control Date PREVIOUS:",pnd_Ivc_Prev_Date_Card);                                                                 //Natural: WRITE ( 00 ) // '"IVC" Control Date PREVIOUS:' #IVC-PREV-DATE-CARD
            if (Global.isEscape()) return;
            if (condition(pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd.equals(pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd)))                                          //Natural: IF #IVC-PREV-DATE-CCYYMMDD = #AP-FROM-CCYYMMDD
            {
                //*    TEST IF IVC MAINTENANCE WAS RUN WITHIN THE FIVE DAYS.
                if (condition((pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn)) || (pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn.subtract(1)))  //Natural: IF ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN ) OR ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN - 1 ) OR ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN - 2 ) OR ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN - 3 ) OR ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN - 4 ) OR ( #IVC-PREV-INTFCE-CCYYMMDD = #DATN - 5 )
                    || (pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn.subtract(2))) || (pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn.subtract(3))) 
                    || (pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn.subtract(4))) || (pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Intfce_Ccyymmdd.equals(pnd_Datn.subtract(5)))))
                {
                    //*  JAN. SPECIAL RUN
                    if (condition((pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(1) && pnd_Work_Card_Pnd_Work_Mm.equals(1)) && ((pnd_Work_Card_Pnd_Wk_Int_Mm.equals(12)     //Natural: IF ( #AP-FROM-MM = 01 AND #WORK-MM = 01 ) AND ( ( #WK-INT-MM = 12 AND #WK-INT-DD > 20 ) OR ( #WK-INT-MM = 01 AND #WK-INT-DD < 20 ) )
                        && pnd_Work_Card_Pnd_Wk_Int_Dd.greater(20)) || (pnd_Work_Card_Pnd_Wk_Int_Mm.equals(1) && pnd_Work_Card_Pnd_Wk_Int_Dd.less(20)))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Dup_Run.setValue(true);                                                                                                                       //Natural: ASSIGN #DUP-RUN := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  JAN. SPECIAL RUN
                    //*  JAN. ROUTINE RUN
                    if (condition((pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(1) && pnd_Work_Card_Pnd_Work_Mm.equals(1)) && ((pnd_Work_Card_Pnd_Wk_Int_Mm.equals(12)     //Natural: IF ( #AP-FROM-MM = 01 AND #WORK-MM = 01 ) AND ( ( #WK-INT-MM = 12 AND #WK-INT-DD > 20 ) OR ( #WK-INT-MM = 01 AND #WK-INT-DD < 20 ) ) OR ( ( #AP-FROM-MM = 02 AND #WORK-MM = 02 ) AND ( #WK-INT-MM = 01 AND #WK-INT-DD < 20 ) AND ( #WORK-SPECIAL-RUN = 'Y' ) )
                        && pnd_Work_Card_Pnd_Wk_Int_Dd.greater(20)) || (pnd_Work_Card_Pnd_Wk_Int_Mm.equals(1) && pnd_Work_Card_Pnd_Wk_Int_Dd.less(20))) 
                        || ((pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(2) && pnd_Work_Card_Pnd_Work_Mm.equals(2)) && (pnd_Work_Card_Pnd_Wk_Int_Mm.equals(1) 
                        && pnd_Work_Card_Pnd_Wk_Int_Dd.less(20)) && (pnd_Work_Card_Pnd_Work_Special_Run.equals("Y")))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Dup_Run.setValue(true);                                                                                                                       //Natural: ASSIGN #DUP-RUN := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Dup_Run.getBoolean()))                                                                                                                  //Natural: IF #DUP-RUN
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(6),"DUPLICATE RUN ATTEMPTED!",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Duplicate run attempted for control date",pnd_Ivc_Prev_Date_Card_Pnd_Ivc_Prev_Date_Ccyymmdd,new  //Natural: WRITE ( 00 ) '***' 06T 'DUPLICATE RUN ATTEMPTED!' 77T '***' / '***' 06T 'Duplicate run attempted for control date' #IVC-PREV-DATE-CCYYMMDD 77T '***'
                        TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  DUPLICATE RUN
                    DbsUtil.terminate(94);  if (true) return;                                                                                                             //Natural: TERMINATE 94
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"'IVC' PREVIOUS DATE FILE ERROR - 093",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"'IVC' previous date file format is invalid!",new  //Natural: WRITE ( 00 ) '***' 06T '"IVC" PREVIOUS DATE FILE ERROR - 093' 77T '***' / '***' 06T '"IVC" previous date file format is invalid!' 77T '***' / '***' 06T 'Valid File Format is YYYYMMDDXYYYYMMDD.' 77T '***' / '***' 06T 'File Contains' #IVC-PREV-DATE-CARD '!' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid File Format is YYYYMMDDXYYYYMMDD.",new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(6),"File Contains",pnd_Ivc_Prev_Date_Card,"!",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  INVALID DATE CARD
            DbsUtil.terminate(93);  if (true) return;                                                                                                                     //Natural: TERMINATE 93
        }                                                                                                                                                                 //Natural: END-IF
        //*  DUPLICATE-RUN-DETECTION
    }
    private void sub_Ivc_Control_Processing() throws Exception                                                                                                            //Natural: IVC-CONTROL-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------**
        //*  DECEMBER ROUTINE RUN
        short decideConditionsMet337 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #AP-FROM-MM < #WORK-MM
        if (condition(pnd_Ap_Control_Record_Pnd_Ap_From_Mm.less(pnd_Work_Card_Pnd_Work_Mm)))
        {
            decideConditionsMet337++;
            if (condition((pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy.equals(pnd_Work_Card_Pnd_Work_Ccyy.add(1))) && (pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(1))         //Natural: IF ( #AP-FROM-CCYY = #WORK-CCYY + 1 ) AND ( #AP-FROM-MM = 01 ) AND ( #WORK-MM = 12 )
                && (pnd_Work_Card_Pnd_Work_Mm.equals(12))))
            {
                pnd_Work_Card_Pnd_Work_Ccyymmdd.setValue(pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd);                                                                     //Natural: ASSIGN #WORK-CCYYMMDD := #AP-FROM-CCYYMMDD
                pnd_Work_Card_Pnd_Work_Special_Run.setValue(" ");                                                                                                         //Natural: ASSIGN #WORK-SPECIAL-RUN := ' '
                //*  JANUARY RUNS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #AP-FROM-MM = #WORK-MM
        else if (condition(pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(pnd_Work_Card_Pnd_Work_Mm)))
        {
            decideConditionsMet337++;
            //*  SPECIAL YEAR-END RUN
            if (condition((pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy.equals(pnd_Work_Card_Pnd_Work_Ccyy)) && (pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(1))                //Natural: IF ( #AP-FROM-CCYY = #WORK-CCYY ) AND ( #AP-FROM-MM = 01 ) AND ( #WORK-MM = 01 )
                && (pnd_Work_Card_Pnd_Work_Mm.equals(1))))
            {
                pnd_Work_Card_Pnd_Work_Ccyymmdd.setValue(pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd);                                                                     //Natural: ASSIGN #WORK-CCYYMMDD := #AP-FROM-CCYYMMDD
                pnd_Work_Card_Pnd_Work_Mm.setValue(2);                                                                                                                    //Natural: ASSIGN #WORK-MM := 02
                pnd_Work_Card_Pnd_Work_Special_Run.setValue("Y");                                                                                                         //Natural: ASSIGN #WORK-SPECIAL-RUN := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  JANUARY ROUTINE RUN
            if (condition((pnd_Ap_Control_Record_Pnd_Ap_From_Ccyy.equals(pnd_Work_Card_Pnd_Work_Ccyy)) && (pnd_Ap_Control_Record_Pnd_Ap_From_Mm.equals(2))                //Natural: IF ( #AP-FROM-CCYY = #WORK-CCYY ) AND ( #AP-FROM-MM = 02 ) AND ( #WORK-MM = 02 ) AND ( #WORK-SPECIAL-RUN = 'Y' )
                && (pnd_Work_Card_Pnd_Work_Mm.equals(2)) && (pnd_Work_Card_Pnd_Work_Special_Run.equals("Y"))))
            {
                pnd_Work_Card_Pnd_Work_Ccyymmdd.setValue(pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd);                                                                     //Natural: ASSIGN #WORK-CCYYMMDD := #AP-FROM-CCYYMMDD
                pnd_Work_Card_Pnd_Work_Special_Run.setValue(" ");                                                                                                         //Natural: ASSIGN #WORK-SPECIAL-RUN := ' '
                //*  ROUTINE MONTHLY RUNS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Work_Card_Pnd_Work_Ccyymmdd.setValue(pnd_Ap_Control_Record_Pnd_Ap_From_Ccyymmdd);                                                                         //Natural: ASSIGN #WORK-CCYYMMDD := #AP-FROM-CCYYMMDD
            pnd_Work_Card_Pnd_Work_Special_Run.setValue(" ");                                                                                                             //Natural: ASSIGN #WORK-SPECIAL-RUN := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Work_Card_Pnd_Wk_Int_Ccyymmdd.setValue(pnd_System_Date_N);                                                                                                    //Natural: ASSIGN #WK-INT-CCYYMMDD := #SYSTEM-DATE-N
        getWorkFiles().write(3, false, pnd_Work_Card);                                                                                                                    //Natural: WRITE WORK FILE 03 #WORK-CARD
        getReports().write(0, "'IVC' Control Date CURRENT:",pnd_Work_Card);                                                                                               //Natural: WRITE ( 00 ) '"IVC" Control Date CURRENT:' #WORK-CARD
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        //*  IVC-CONTROL-PROCESSING
    }
    private void sub_Environment_Identification() throws Exception                                                                                                        //Natural: ENVIRONMENT-IDENTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------**
        //*  DETERMINE IF 'PROD' OR 'TEST' ENVIRONMENT.
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"- Calling Sub-Program 'TWRN0001'...");                                                                                 //Natural: WRITE ( 00 ) *PROGRAM '- Calling Sub-Program "TWRN0001"...'
        if (Global.isEscape()) return;
        pdaTwra0001.getTwra0001().reset();                                                                                                                                //Natural: RESET TWRA0001
        DbsUtil.callnat(Twrn0001.class , getCurrentProcessState(), pdaTwra0001.getTwra0001());                                                                            //Natural: CALLNAT 'TWRN0001' TWRA0001
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Prod_Test());                                                                                               //Natural: WRITE ( 00 ) '=' TWRA0001.#PROD-TEST
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return());                                                                                         //Natural: WRITE ( 00 ) '=' TWRA0001.#TWRN0001-RETURN
        if (Global.isEscape()) return;
        //* *----------------------------------------**
        //*  ENVIRONMENT-IDENTIFICATION
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
