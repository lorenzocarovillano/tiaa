/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:32:16 PM
**        * FROM NATURAL PROGRAM : Iaap786
************************************************************
**        * FILE NAME            : Iaap786.java
**        * CLASS NAME           : Iaap786
**        * INSTANCE NAME        : Iaap786
************************************************************
**********************************************************************
* PROGRAM:   IAAP786
* SYSTEM:    IA ADMINISTRATION
*
* FUNCTION:  ACTUARIAL REPORT (REPORT 3)
*
* TITLE:     IA ADMINISTRATION MODE CONTROL FOR FUND ACCOUNTS
*                  CREF REVERSALS AND REINSTATMENT DETAIL
*
* REMARKS:   THIS PROGRAM READS THE REPORT FILE (WORKFILE2) CREATED
*            BY IAAP780 (REPORT FILE GENERATOR), SORTS THE FILE BY
*            PPCN, PAYEE, AND TRANSACTION DATE (ASCENDING).
*            IT THEN TESTS SEVERAL TRANSACTION SCENARIOS TO ADD OR
*             SUBTRACT TO THE CONTROL TOTALS TABLE.
*
*            NOTE: THIS PROGRAM SELECTS ONLY REVERSALS AND
*                  REINSTATEMENT TRANSACTIONS
*                  FROM THE WORKFILE (020 & 035) AND DOES IT FOR
*                  CREF FUNDS ONLY.
*
*
* WRITTEN BY: MIKE OLIVA, 3/15/96 FOR KEITH NICOLL AND JAY MIRANDA
*
* DATE OF CHANGE   PROGRAMMER           DESCRIPTION OF CHANGE
* -------------- ------------- -----------------------------------------
*
*   5/00             RM         NEW PA SELECT FUNDS
*   4/02                        CHANGED IAAN0510 TO IAAN051Z OIA CHANGE
*   3/12                        RATE BASE EXPANSION - SCAN ON 3/12
*   8/17             OS         ADJUST REPORT FILE LENGTH TO BE THE SAME
*                               AS IN IAAP780.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaap786 extends BLNatBase
{
    // Data Areas
    private PdaIaaa051z pdaIaaa051z;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Report_File;

    private DbsGroup pnd_Report_File__R_Field_1;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Date;

    private DbsGroup pnd_Report_File__R_Field_2;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Yyyy;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Mm;
    private DbsField pnd_Report_File_Pnd_Rep_Check_Dd;
    private DbsField pnd_Report_File_Pnd_Rep_Ppcn;
    private DbsField pnd_Report_File_Pnd_Rep_Payee;
    private DbsField pnd_Report_File_Pnd_Rep_First_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Second_Annt_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_Option_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Mode_Ind;
    private DbsField pnd_Report_File_Pnd_Rep_First_Pay_Due_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Trans_Date;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Code;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Before;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_After;
    private DbsField pnd_Report_File_Pnd_Rep_Fund_Units_Diff;
    private DbsField pnd_Report_File_Pnd_Rep_Orgn_Cde;
    private DbsField pnd_Report_File_Pnd_Rep_Last_Name;
    private DbsField pnd_Report_File_Pnd_Rep_First_Name;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Pay_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Per_Div_Amt;
    private DbsField pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Report_File_Pnd_Rep_Inst_Iss_Cde;
    private DbsField pnd_Rep_Trans_Date_A15;

    private DbsGroup pnd_Rep_Trans_Date_A15__R_Field_3;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd;
    private DbsField pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time;
    private DbsField pnd_Format_Date;

    private DbsGroup pnd_Format_Date__R_Field_4;
    private DbsField pnd_Format_Date_Pnd_Format_Date_Mm;
    private DbsField pnd_Format_Date_Pnd_Format_Date_Dd;
    private DbsField pnd_Format_Date_Pnd_Format_Date_Yyyy;
    private DbsField pnd_Format_Check_Date;

    private DbsGroup pnd_Format_Check_Date__R_Field_5;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Mm;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Dd;
    private DbsField pnd_Format_Check_Date_Pnd_Business_Yyyy;
    private DbsField pnd_Prev_Dl_Nbr;
    private DbsField pnd_Prev_Dl_Sort_Pfx;
    private DbsField pnd_Dl_Fund_Name;
    private DbsField pnd_Prev_Dl_Fund_Name;
    private DbsField pnd_Count;
    private DbsField pnd_Ledger_Num;
    private DbsField pnd_Ledger_Desc;
    private DbsField pnd_Effctv_Dte;

    private DbsGroup pnd_Effctv_Dte__R_Field_6;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy;
    private DbsField pnd_Formatted_Acct_Dte;

    private DbsGroup pnd_Formatted_Acct_Dte__R_Field_7;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd;
    private DbsField pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy;
    private DbsField pnd_Formatted_Bsns_Dte;

    private DbsGroup pnd_Formatted_Bsns_Dte__R_Field_8;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Inx;
    private DbsField pnd_Inx1;
    private DbsField pnd_Inx2;
    private DbsField pnd_End_Of_Data;
    private DbsField pnd_Prev_Fund_Code;
    private DbsField pnd_Total_Before_Units;
    private DbsField pnd_Total_After_Units;
    private DbsField pnd_Grand_Before_Units;
    private DbsField pnd_Grand_After_Units;
    private DbsField pnd_Title_Fund_Name;

    private DbsGroup pnd_Fund_Lit_Table;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Grade;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Stand;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Group;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Rea;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Stk;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Mma;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Sca;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Glb;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Grw;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Eqx;
    private DbsField pnd_Fund_Lit_Table_Pnd_T_Bnd;
    private DbsField pnd_Fund_Code_2;
    private DbsField pnd_Fund_Code;
    private DbsField pnd_Fund_Desc;
    private DbsField pnd_Len;
    private DbsField pnd_Comp_Desc;
    private DbsField pnd_Comp_Code;
    private DbsField pnd_Fund_Desc_35;

    private DbsGroup pnd_Fund_Desc_35__R_Field_9;
    private DbsField pnd_Fund_Desc_35_Pnd_Fund_Desc_10;
    private DbsField pnd_Hd_Title;
    private DbsField pnd_Hd_Desc;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Rep_Fund_CodeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIaaa051z = new PdaIaaa051z(localVariables);

        // Local Variables
        pnd_Report_File = localVariables.newFieldInRecord("pnd_Report_File", "#REPORT-FILE", FieldType.STRING, 175);

        pnd_Report_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Report_File__R_Field_1", "REDEFINE", pnd_Report_File);
        pnd_Report_File_Pnd_Rep_Check_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Date", "#REP-CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Report_File__R_Field_2 = pnd_Report_File__R_Field_1.newGroupInGroup("pnd_Report_File__R_Field_2", "REDEFINE", pnd_Report_File_Pnd_Rep_Check_Date);
        pnd_Report_File_Pnd_Rep_Check_Yyyy = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Yyyy", "#REP-CHECK-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Report_File_Pnd_Rep_Check_Mm = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Mm", "#REP-CHECK-MM", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Check_Dd = pnd_Report_File__R_Field_2.newFieldInGroup("pnd_Report_File_Pnd_Rep_Check_Dd", "#REP-CHECK-DD", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Ppcn = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Ppcn", "#REP-PPCN", FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Payee = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Payee", "#REP-PAYEE", FieldType.NUMERIC, 2);
        pnd_Report_File_Pnd_Rep_First_Annt_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Annt_Ind", "#REP-FIRST-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Second_Annt_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Second_Annt_Ind", "#REP-SECOND-ANNT-IND", 
            FieldType.STRING, 9);
        pnd_Report_File_Pnd_Rep_Option_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Option_Code", "#REP-OPTION-CODE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Mode_Ind = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Mode_Ind", "#REP-MODE-IND", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_First_Pay_Due_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Pay_Due_Date", "#REP-FIRST-PAY-DUE-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Report_File_Pnd_Rep_Trans_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Code", "#REP-TRANS-CODE", FieldType.NUMERIC, 
            3);
        pnd_Report_File_Pnd_Rep_Trans_Date = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Trans_Date", "#REP-TRANS-DATE", FieldType.TIME);
        pnd_Report_File_Pnd_Rep_Fund_Code = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Code", "#REP-FUND-CODE", FieldType.STRING, 
            2);
        pnd_Report_File_Pnd_Rep_Fund_Units_Before = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Before", "#REP-FUND-UNITS-BEFORE", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_After = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_After", "#REP-FUND-UNITS-AFTER", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Fund_Units_Diff = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Fund_Units_Diff", "#REP-FUND-UNITS-DIFF", 
            FieldType.NUMERIC, 9, 3);
        pnd_Report_File_Pnd_Rep_Orgn_Cde = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Orgn_Cde", "#REP-ORGN-CDE", FieldType.NUMERIC, 
            2);
        pnd_Report_File_Pnd_Rep_Last_Name = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Last_Name", "#REP-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_First_Name = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_First_Name", "#REP-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Report_File_Pnd_Rep_Per_Pay_Amt = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Pay_Amt", "#REP-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Per_Div_Amt = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Per_Div_Amt", "#REP-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Rllvr_Cntrct_Nbr", "#REP-RLLVR-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Report_File_Pnd_Rep_Inst_Iss_Cde = pnd_Report_File__R_Field_1.newFieldInGroup("pnd_Report_File_Pnd_Rep_Inst_Iss_Cde", "#REP-INST-ISS-CDE", 
            FieldType.STRING, 5);
        pnd_Rep_Trans_Date_A15 = localVariables.newFieldInRecord("pnd_Rep_Trans_Date_A15", "#REP-TRANS-DATE-A15", FieldType.STRING, 15);

        pnd_Rep_Trans_Date_A15__R_Field_3 = localVariables.newGroupInRecord("pnd_Rep_Trans_Date_A15__R_Field_3", "REDEFINE", pnd_Rep_Trans_Date_A15);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy = pnd_Rep_Trans_Date_A15__R_Field_3.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy", "#REP-TRANS-YYYY", 
            FieldType.STRING, 4);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm = pnd_Rep_Trans_Date_A15__R_Field_3.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm", "#REP-TRANS-MM", 
            FieldType.STRING, 2);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd = pnd_Rep_Trans_Date_A15__R_Field_3.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd", "#REP-TRANS-DD", 
            FieldType.STRING, 2);
        pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time = pnd_Rep_Trans_Date_A15__R_Field_3.newFieldInGroup("pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Time", "#REP-TRANS-TIME", 
            FieldType.STRING, 7);
        pnd_Format_Date = localVariables.newFieldInRecord("pnd_Format_Date", "#FORMAT-DATE", FieldType.NUMERIC, 8);

        pnd_Format_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Format_Date__R_Field_4", "REDEFINE", pnd_Format_Date);
        pnd_Format_Date_Pnd_Format_Date_Mm = pnd_Format_Date__R_Field_4.newFieldInGroup("pnd_Format_Date_Pnd_Format_Date_Mm", "#FORMAT-DATE-MM", FieldType.STRING, 
            2);
        pnd_Format_Date_Pnd_Format_Date_Dd = pnd_Format_Date__R_Field_4.newFieldInGroup("pnd_Format_Date_Pnd_Format_Date_Dd", "#FORMAT-DATE-DD", FieldType.STRING, 
            2);
        pnd_Format_Date_Pnd_Format_Date_Yyyy = pnd_Format_Date__R_Field_4.newFieldInGroup("pnd_Format_Date_Pnd_Format_Date_Yyyy", "#FORMAT-DATE-YYYY", 
            FieldType.STRING, 4);
        pnd_Format_Check_Date = localVariables.newFieldInRecord("pnd_Format_Check_Date", "#FORMAT-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Format_Check_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Format_Check_Date__R_Field_5", "REDEFINE", pnd_Format_Check_Date);
        pnd_Format_Check_Date_Pnd_Business_Mm = pnd_Format_Check_Date__R_Field_5.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Mm", "#BUSINESS-MM", 
            FieldType.NUMERIC, 2);
        pnd_Format_Check_Date_Pnd_Business_Dd = pnd_Format_Check_Date__R_Field_5.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Dd", "#BUSINESS-DD", 
            FieldType.NUMERIC, 2);
        pnd_Format_Check_Date_Pnd_Business_Yyyy = pnd_Format_Check_Date__R_Field_5.newFieldInGroup("pnd_Format_Check_Date_Pnd_Business_Yyyy", "#BUSINESS-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Prev_Dl_Nbr = localVariables.newFieldInRecord("pnd_Prev_Dl_Nbr", "#PREV-DL-NBR", FieldType.STRING, 8);
        pnd_Prev_Dl_Sort_Pfx = localVariables.newFieldInRecord("pnd_Prev_Dl_Sort_Pfx", "#PREV-DL-SORT-PFX", FieldType.STRING, 2);
        pnd_Dl_Fund_Name = localVariables.newFieldInRecord("pnd_Dl_Fund_Name", "#DL-FUND-NAME", FieldType.STRING, 4);
        pnd_Prev_Dl_Fund_Name = localVariables.newFieldInRecord("pnd_Prev_Dl_Fund_Name", "#PREV-DL-FUND-NAME", FieldType.STRING, 4);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 10);
        pnd_Ledger_Num = localVariables.newFieldInRecord("pnd_Ledger_Num", "#LEDGER-NUM", FieldType.STRING, 15);
        pnd_Ledger_Desc = localVariables.newFieldInRecord("pnd_Ledger_Desc", "#LEDGER-DESC", FieldType.STRING, 40);
        pnd_Effctv_Dte = localVariables.newFieldInRecord("pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.NUMERIC, 6);

        pnd_Effctv_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Effctv_Dte__R_Field_6", "REDEFINE", pnd_Effctv_Dte);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm = pnd_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Mm", "#EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd = pnd_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Dd", "#EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy = pnd_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dte_Yy", "#EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Formatted_Acct_Dte = localVariables.newFieldInRecord("pnd_Formatted_Acct_Dte", "#FORMATTED-ACCT-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Acct_Dte__R_Field_7 = localVariables.newGroupInRecord("pnd_Formatted_Acct_Dte__R_Field_7", "REDEFINE", pnd_Formatted_Acct_Dte);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm = pnd_Formatted_Acct_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Mm", 
            "#FORMATTED-ACCT-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd = pnd_Formatted_Acct_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Dd", 
            "#FORMATTED-ACCT-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy = pnd_Formatted_Acct_Dte__R_Field_7.newFieldInGroup("pnd_Formatted_Acct_Dte_Pnd_Formatted_Acct_Dte_Yy", 
            "#FORMATTED-ACCT-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte = localVariables.newFieldInRecord("pnd_Formatted_Bsns_Dte", "#FORMATTED-BSNS-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Bsns_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Formatted_Bsns_Dte__R_Field_8", "REDEFINE", pnd_Formatted_Bsns_Dte);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm = pnd_Formatted_Bsns_Dte__R_Field_8.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm", 
            "#FORMATTED-BSNS-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd = pnd_Formatted_Bsns_Dte__R_Field_8.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd", 
            "#FORMATTED-BSNS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy = pnd_Formatted_Bsns_Dte__R_Field_8.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy", 
            "#FORMATTED-BSNS-DTE-YY", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Inx = localVariables.newFieldInRecord("pnd_Inx", "#INX", FieldType.PACKED_DECIMAL, 2);
        pnd_Inx1 = localVariables.newFieldInRecord("pnd_Inx1", "#INX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Inx2 = localVariables.newFieldInRecord("pnd_Inx2", "#INX2", FieldType.PACKED_DECIMAL, 3);
        pnd_End_Of_Data = localVariables.newFieldInRecord("pnd_End_Of_Data", "#END-OF-DATA", FieldType.STRING, 1);
        pnd_Prev_Fund_Code = localVariables.newFieldInRecord("pnd_Prev_Fund_Code", "#PREV-FUND-CODE", FieldType.STRING, 2);
        pnd_Total_Before_Units = localVariables.newFieldInRecord("pnd_Total_Before_Units", "#TOTAL-BEFORE-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Total_After_Units = localVariables.newFieldInRecord("pnd_Total_After_Units", "#TOTAL-AFTER-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Grand_Before_Units = localVariables.newFieldInRecord("pnd_Grand_Before_Units", "#GRAND-BEFORE-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Grand_After_Units = localVariables.newFieldInRecord("pnd_Grand_After_Units", "#GRAND-AFTER-UNITS", FieldType.NUMERIC, 12, 3);
        pnd_Title_Fund_Name = localVariables.newFieldInRecord("pnd_Title_Fund_Name", "#TITLE-FUND-NAME", FieldType.STRING, 17);

        pnd_Fund_Lit_Table = localVariables.newGroupInRecord("pnd_Fund_Lit_Table", "#FUND-LIT-TABLE");
        pnd_Fund_Lit_Table_Pnd_T_Grade = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Grade", "#T-GRADE", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Stand = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Stand", "#T-STAND", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Group = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Group", "#T-GROUP", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Rea = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Rea", "#T-REA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Stk = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Stk", "#T-STK", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Mma = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Mma", "#T-MMA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Sca = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Sca", "#T-SCA", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Glb = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Glb", "#T-GLB", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Grw = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Grw", "#T-GRW", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Eqx = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Eqx", "#T-EQX", FieldType.STRING, 19);
        pnd_Fund_Lit_Table_Pnd_T_Bnd = pnd_Fund_Lit_Table.newFieldInGroup("pnd_Fund_Lit_Table_Pnd_T_Bnd", "#T-BND", FieldType.STRING, 19);
        pnd_Fund_Code_2 = localVariables.newFieldInRecord("pnd_Fund_Code_2", "#FUND-CODE-2", FieldType.STRING, 2);
        pnd_Fund_Code = localVariables.newFieldArrayInRecord("pnd_Fund_Code", "#FUND-CODE", FieldType.STRING, 2, new DbsArrayController(1, 40));
        pnd_Fund_Desc = localVariables.newFieldArrayInRecord("pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 17, new DbsArrayController(1, 40));
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Comp_Desc = localVariables.newFieldInRecord("pnd_Comp_Desc", "#COMP-DESC", FieldType.STRING, 4);
        pnd_Comp_Code = localVariables.newFieldArrayInRecord("pnd_Comp_Code", "#COMP-CODE", FieldType.STRING, 1, new DbsArrayController(1, 40));
        pnd_Fund_Desc_35 = localVariables.newFieldInRecord("pnd_Fund_Desc_35", "#FUND-DESC-35", FieldType.STRING, 35);

        pnd_Fund_Desc_35__R_Field_9 = localVariables.newGroupInRecord("pnd_Fund_Desc_35__R_Field_9", "REDEFINE", pnd_Fund_Desc_35);
        pnd_Fund_Desc_35_Pnd_Fund_Desc_10 = pnd_Fund_Desc_35__R_Field_9.newFieldInGroup("pnd_Fund_Desc_35_Pnd_Fund_Desc_10", "#FUND-DESC-10", FieldType.STRING, 
            10);
        pnd_Hd_Title = localVariables.newFieldInRecord("pnd_Hd_Title", "#HD-TITLE", FieldType.STRING, 65);
        pnd_Hd_Desc = localVariables.newFieldInRecord("pnd_Hd_Desc", "#HD-DESC", FieldType.STRING, 47);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Rep_Fund_CodeOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Rep_Fund_Code_OLD", "Pnd_Rep_Fund_Code_OLD", FieldType.STRING, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Fund_Lit_Table_Pnd_T_Grade.setInitialValue("1GTIAA GRADED      ");
        pnd_Fund_Lit_Table_Pnd_T_Stand.setInitialValue("1STIAA STANDARD    ");
        pnd_Fund_Lit_Table_Pnd_T_Group.setInitialValue("1 TIAA GROUP       ");
        pnd_Fund_Lit_Table_Pnd_T_Rea.setInitialValue("09TIAA REAL ESTATE ");
        pnd_Fund_Lit_Table_Pnd_T_Stk.setInitialValue("02CREF STOCK       ");
        pnd_Fund_Lit_Table_Pnd_T_Mma.setInitialValue("03CREF MMA         ");
        pnd_Fund_Lit_Table_Pnd_T_Sca.setInitialValue("04CREF SOCIAL      ");
        pnd_Fund_Lit_Table_Pnd_T_Glb.setInitialValue("06CREF GLOBAL      ");
        pnd_Fund_Lit_Table_Pnd_T_Grw.setInitialValue("08CREF GROWTH      ");
        pnd_Fund_Lit_Table_Pnd_T_Eqx.setInitialValue("07CREF EQUITY      ");
        pnd_Fund_Lit_Table_Pnd_T_Bnd.setInitialValue("05CREF BOND        ");
        pnd_Len.setInitialValue(5);
        pnd_Hd_Desc.setInitialValue("CREF REVERSALS AND REINSTATEMENT DETAIL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Iaap786() throws Exception
    {
        super("Iaap786");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Count.reset();                                                                                                                                                //Natural: FORMAT ( 03 ) LS = 132 PS = 55;//Natural: RESET #COUNT
        pnd_End_Of_Data.reset();                                                                                                                                          //Natural: RESET #END-OF-DATA
        //*  CALLNAT 'IAAN0500' IAAA0500
        //*  #FUND-CODE(1:20) := IAAA0500.#IA-STD-NM-CD(1:20)
        //*  #COMP-CODE(1:20) := IAAA0500.#IA-STD-CMPNY-CD-A(1:20)
        //*  NEW OIA CHNAGE 04/02
        //*  PA SELECT  5/00 CHANGED FROM 20 TO 40
        DbsUtil.callnat(Iaan051z.class , getCurrentProcessState(), pdaIaaa051z.getIaaa051z());                                                                            //Natural: CALLNAT 'IAAN051Z' IAAA051Z
        if (condition(Global.isEscape())) return;
        pnd_Fund_Code.getValue(1,":",40).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Nm_Cd().getValue(1,":",40));                                                         //Natural: ASSIGN #FUND-CODE ( 1:40 ) := IAAA051Z.#IA-STD-NM-CD ( 1:40 )
        pnd_Comp_Code.getValue(1,":",40).setValue(pdaIaaa051z.getIaaa051z_Pnd_Ia_Std_Cmpny_Cd_A().getValue(1,":",40));                                                    //Natural: ASSIGN #COMP-CODE ( 1:40 ) := IAAA051Z.#IA-STD-CMPNY-CD-A ( 1:40 )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 40
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
        {
            if (condition(pnd_Fund_Code.getValue(pnd_I).notEquals(" ")))                                                                                                  //Natural: IF #FUND-CODE ( #I ) NE ' '
            {
                pnd_Fund_Code_2.setValue(pnd_Fund_Code.getValue(pnd_I));                                                                                                  //Natural: MOVE #FUND-CODE ( #I ) TO #FUND-CODE-2
                                                                                                                                                                          //Natural: PERFORM #GET-DESC
                sub_Pnd_Get_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Fund_Desc.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Comp_Desc, pnd_Fund_Desc_35_Pnd_Fund_Desc_10));                                               //Natural: COMPRESS #COMP-DESC #FUND-DESC-10 INTO #FUND-DESC ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 02 #REPORT-FILE
        while (condition(getWorkFiles().read(2, pnd_Report_File)))
        {
            DbsUtil.examine(new ExamineSource(pnd_Fund_Code.getValue("*")), new ExamineSearch(pnd_Report_File_Pnd_Rep_Fund_Code), new ExamineGivingIndex(pnd_Inx1));      //Natural: EXAMINE #FUND-CODE ( * ) FOR #REP-FUND-CODE GIVING INDEX #INX1
            if (condition(!((((pnd_Report_File_Pnd_Rep_Trans_Code.equals(20) || pnd_Report_File_Pnd_Rep_Trans_Code.equals(35)) && (pnd_Comp_Code.getValue(pnd_Inx1).equals("2")  //Natural: ACCEPT IF ( #REP-TRANS-CODE = 020 OR = 035 ) AND #COMP-CODE ( #INX1 ) = '2' OR = 'U' AND ( #REP-PAYEE = 01 )
                || pnd_Comp_Code.getValue(pnd_Inx1).equals("U"))) && pnd_Report_File_Pnd_Rep_Payee.equals(1)))))
            {
                continue;
            }
            //*             (#REP-FUND-CODE = '02' OR = '03' OR = '04' OR = '05' OR
            //*                             = '06' OR = '07' OR = '08' OR = '09' OR
            //*                             = '10' OR = '11' OR = '12' OR = '13' OR
            //*                             = '14' OR = '15' OR = '16' OR = '17')
            //*                            = '1S' OR = '1G' OR = '16' OR = '17')
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            getSort().writeSortInData(pnd_Report_File_Pnd_Rep_Fund_Code, pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date, //Natural: END-ALL
                pnd_Report_File_Pnd_Rep_Check_Date, pnd_Report_File_Pnd_Rep_Option_Code, pnd_Report_File_Pnd_Rep_First_Annt_Ind, pnd_Report_File_Pnd_Rep_Second_Annt_Ind, 
                pnd_Report_File_Pnd_Rep_Mode_Ind, pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, pnd_Report_File_Pnd_Rep_Trans_Code, pnd_Report_File_Pnd_Rep_Fund_Units_Before, 
                pnd_Report_File_Pnd_Rep_Fund_Units_After, pnd_Report_File_Pnd_Rep_Fund_Units_Diff);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Report_File_Pnd_Rep_Fund_Code, pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date);           //Natural: SORT BY #REP-FUND-CODE #REP-PPCN #REP-PAYEE #REP-TRANS-DATE ASCENDING USING #REP-CHECK-DATE #REP-OPTION-CODE #REP-FIRST-ANNT-IND #REP-SECOND-ANNT-IND #REP-MODE-IND #REP-FIRST-PAY-DUE-DATE #REP-TRANS-CODE #REP-FUND-UNITS-BEFORE #REP-FUND-UNITS-AFTER #REP-FUND-UNITS-DIFF
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Report_File_Pnd_Rep_Fund_Code, pnd_Report_File_Pnd_Rep_Ppcn, pnd_Report_File_Pnd_Rep_Payee, pnd_Report_File_Pnd_Rep_Trans_Date, 
            pnd_Report_File_Pnd_Rep_Check_Date, pnd_Report_File_Pnd_Rep_Option_Code, pnd_Report_File_Pnd_Rep_First_Annt_Ind, pnd_Report_File_Pnd_Rep_Second_Annt_Ind, 
            pnd_Report_File_Pnd_Rep_Mode_Ind, pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, pnd_Report_File_Pnd_Rep_Trans_Code, pnd_Report_File_Pnd_Rep_Fund_Units_Before, 
            pnd_Report_File_Pnd_Rep_Fund_Units_After, pnd_Report_File_Pnd_Rep_Fund_Units_Diff)))
        {
            CheckAtStartofData239();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Rep_Trans_Date_A15.setValueEdited(pnd_Report_File_Pnd_Rep_Trans_Date,new ReportEditMask("YYYYMMDDHHIISST"));                                              //Natural: MOVE EDITED #REP-TRANS-DATE ( EM = YYYYMMDDHHIISST ) TO #REP-TRANS-DATE-A15
            pnd_Format_Date_Pnd_Format_Date_Mm.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Mm);                                                                         //Natural: MOVE #REP-TRANS-MM TO #FORMAT-DATE-MM
            pnd_Format_Date_Pnd_Format_Date_Dd.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Dd);                                                                         //Natural: MOVE #REP-TRANS-DD TO #FORMAT-DATE-DD
            pnd_Format_Date_Pnd_Format_Date_Yyyy.setValue(pnd_Rep_Trans_Date_A15_Pnd_Rep_Trans_Yyyy);                                                                     //Natural: MOVE #REP-TRANS-YYYY TO #FORMAT-DATE-YYYY
            pnd_Format_Check_Date_Pnd_Business_Yyyy.setValue(pnd_Report_File_Pnd_Rep_Check_Yyyy);                                                                         //Natural: MOVE #REP-CHECK-YYYY TO #BUSINESS-YYYY
            pnd_Format_Check_Date_Pnd_Business_Mm.setValue(pnd_Report_File_Pnd_Rep_Check_Mm);                                                                             //Natural: MOVE #REP-CHECK-MM TO #BUSINESS-MM
            pnd_Format_Check_Date_Pnd_Business_Dd.setValue(pnd_Report_File_Pnd_Rep_Check_Dd);                                                                             //Natural: MOVE #REP-CHECK-DD TO #BUSINESS-DD
            getReports().display(3, "TRANSACTION/DATE",                                                                                                                   //Natural: DISPLAY ( 03 ) 'TRANSACTION/DATE' #FORMAT-DATE ( EM = 99/99/9999 ) 'CONTRACT/NUMBER' #REP-PPCN '/PAYEE' #REP-PAYEE ( EM = 99 ) '---NAME---/LAST   FM' #REP-FIRST-ANNT-IND 'PAYMENT/MODE/' #REP-MODE-IND 'ISSUE/  DATE  /' #REP-FIRST-PAY-DUE-DATE ( EM = 9999/99 ) '/  OPTION     /' #REP-OPTION-CODE 'TRANS/CODE/' #REP-TRANS-CODE 'PERIODIC/UNITS/' #REP-FUND-UNITS-AFTER
            		pnd_Format_Date, new ReportEditMask ("99/99/9999"),"CONTRACT/NUMBER",
            		pnd_Report_File_Pnd_Rep_Ppcn,"/PAYEE",
            		pnd_Report_File_Pnd_Rep_Payee, new ReportEditMask ("99"),"---NAME---/LAST   FM",
            		pnd_Report_File_Pnd_Rep_First_Annt_Ind,"PAYMENT/MODE/",
            		pnd_Report_File_Pnd_Rep_Mode_Ind,"ISSUE/  DATE  /",
            		pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, new ReportEditMask ("9999/99"),"/  OPTION     /",
            		pnd_Report_File_Pnd_Rep_Option_Code,"TRANS/CODE/",
            		pnd_Report_File_Pnd_Rep_Trans_Code,"PERIODIC/UNITS/",
            		pnd_Report_File_Pnd_Rep_Fund_Units_After);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DISPLAY      'TRANSACTION/DATE'     #FORMAT-DATE (EM=99/99/9999)
            //*                                                                                                                                                           //Natural: AT START OF DATA
            pnd_Total_After_Units.nadd(pnd_Report_File_Pnd_Rep_Fund_Units_After);                                                                                         //Natural: ADD #REP-FUND-UNITS-AFTER TO #TOTAL-AFTER-UNITS
            pnd_Grand_After_Units.nadd(pnd_Report_File_Pnd_Rep_Fund_Units_After);                                                                                         //Natural: ADD #REP-FUND-UNITS-AFTER TO #GRAND-AFTER-UNITS
            //*                                                                                                                                                           //Natural: AT BREAK OF #REP-FUND-CODE
            //*                                                                                                                                                           //Natural: AT END OF DATA
            sort01Pnd_Rep_Fund_CodeOld.setValue(pnd_Report_File_Pnd_Rep_Fund_Code);                                                                                       //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //*  ===========================================================
        //*                   END  OF  SORT  LOOP
        //*  ===========================================================
        if (condition(pnd_Count.equals(getZero())))                                                                                                                       //Natural: IF #COUNT = 0
        {
            getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"*** NO TRANSACTIONS PROCESSED TODAY ***");                                                                     //Natural: WRITE ( 03 ) /// '*** NO TRANSACTIONS PROCESSED TODAY ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"GRAND TOTAL UNITS:",new ColumnSpacing(33),pnd_Grand_After_Units, new ReportEditMask ("ZZ,ZZZ,ZZ9.9999"));      //Natural: WRITE ( 03 ) /// 'GRAND TOTAL UNITS:' 33X #GRAND-AFTER-UNITS ( EM = ZZZ,ZZZ,ZZ9.9999 )
            if (Global.isEscape()) return;
            //*  WRITE      /// 'GRAND TOTAL UNITS:' 33X #GRAND-AFTER-UNITS
            getReports().write(0, "Number of report file records accepted:",pnd_Count);                                                                                   //Natural: WRITE 'Number of report file records accepted:' #COUNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF MAINLINE CODE
        //* ********************************************************************
        //* ********************************************************************
        //*  AT TOP OF PAGE
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT / 'PROGRAM   : ' *PROGRAM 26X 'IA ADMINISTRATIVE MODE CONTROL FOR FUND ACCOUNTS ' #FORMAT-CHECK-DATE ( EM = 99/99/9999 ) 5X 'PAGE :' *PAGE-NUMBER ( 03 ) / 'RUN DATE  : ' *DATU 34X #HD-TITLE / 67X #TITLE-FUND-NAME //
        //*      34X 'CREF REVERSALS AND REINSTATEMENT DETAIL' /
        //*      64X 'Check Date:' #REP-CHECK-DATE  /
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #GET-DESC
        //* *****************************************************************
        //*  END-TOPPAGE
    }
    private void sub_Pnd_Get_Desc() throws Exception                                                                                                                      //Natural: #GET-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Fund_Desc_35.reset();                                                                                                                                         //Natural: RESET #FUND-DESC-35 #COMP-DESC
        pnd_Comp_Desc.reset();
        //*   CALLNAT 'IAAN050C' #FUND-CODE-2 #FUND-DESC-35 #COMP-DESC #LEN
        //*  5/00
        DbsUtil.callnat(Iaan051c.class , getCurrentProcessState(), pnd_Fund_Code_2, pnd_Fund_Desc_35, pnd_Comp_Desc, pnd_Len);                                            //Natural: CALLNAT 'IAAN051C' #FUND-CODE-2 #FUND-DESC-35 #COMP-DESC #LEN
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Report_File_Pnd_Rep_Fund_CodeIsBreak = pnd_Report_File_Pnd_Rep_Fund_Code.isBreak(endOfData);
        if (condition(pnd_Report_File_Pnd_Rep_Fund_CodeIsBreak))
        {
            DbsUtil.examine(new ExamineSource(pnd_Fund_Code.getValue("*")), new ExamineSearch(pnd_Prev_Fund_Code), new ExamineGivingIndex(pnd_I));                        //Natural: EXAMINE #FUND-CODE ( * ) FOR #PREV-FUND-CODE GIVING INDEX #I
            getReports().write(3, NEWLINE,"TOTAL FOR FUND:  ",pnd_Fund_Desc.getValue(pnd_I),new ColumnSpacing(17),pnd_Total_After_Units);                                 //Natural: WRITE ( 03 ) / 'TOTAL FOR FUND:  ' #FUND-DESC ( #I ) 17X #TOTAL-AFTER-UNITS
            if (condition(Global.isEscape())) return;
            pnd_Total_After_Units.reset();                                                                                                                                //Natural: RESET #TOTAL-AFTER-UNITS
            DbsUtil.examine(new ExamineSource(pnd_Fund_Code.getValue("*")), new ExamineSearch(pnd_Report_File_Pnd_Rep_Fund_Code), new ExamineGivingIndex(pnd_I));         //Natural: EXAMINE #FUND-CODE ( * ) FOR #REP-FUND-CODE GIVING INDEX #I
            pnd_Title_Fund_Name.setValue(pnd_Fund_Desc.getValue(pnd_I));                                                                                                  //Natural: MOVE #FUND-DESC ( #I ) TO #TITLE-FUND-NAME
            //*  ADDED 5/00
            pnd_Hd_Title.setValue(DbsUtil.compress(pnd_Title_Fund_Name, pnd_Hd_Desc));                                                                                    //Natural: COMPRESS #TITLE-FUND-NAME #HD-DESC INTO #HD-TITLE
            if (condition(sort01Pnd_Rep_Fund_CodeOld.notEquals(pnd_Report_File_Pnd_Rep_Fund_Code)))                                                                       //Natural: IF OLD ( #REP-FUND-CODE ) NOT = #REP-FUND-CODE
            {
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 03 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Fund_Code.setValue(pnd_Report_File_Pnd_Rep_Fund_Code);                                                                                               //Natural: MOVE #REP-FUND-CODE TO #PREV-FUND-CODE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(3, "LS=132 PS=55");

        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,"PROGRAM   : ",Global.getPROGRAM(),new ColumnSpacing(26),"IA ADMINISTRATIVE MODE CONTROL FOR FUND ACCOUNTS ",pnd_Format_Check_Date, 
            new ReportEditMask ("99/99/9999"),new ColumnSpacing(5),"PAGE :",getReports().getPageNumberDbs(3),NEWLINE,"RUN DATE  : ",Global.getDATU(),new 
            ColumnSpacing(34),pnd_Hd_Title,NEWLINE,new ColumnSpacing(67),pnd_Title_Fund_Name,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(3, "TRANSACTION/DATE",
        		pnd_Format_Date, new ReportEditMask ("99/99/9999"),"CONTRACT/NUMBER",
        		pnd_Report_File_Pnd_Rep_Ppcn,"/PAYEE",
        		pnd_Report_File_Pnd_Rep_Payee, new ReportEditMask ("99"),"---NAME---/LAST   FM",
        		pnd_Report_File_Pnd_Rep_First_Annt_Ind,"PAYMENT/MODE/",
        		pnd_Report_File_Pnd_Rep_Mode_Ind,"ISSUE/  DATE  /",
        		pnd_Report_File_Pnd_Rep_First_Pay_Due_Date, new ReportEditMask ("9999/99"),"/  OPTION     /",
        		pnd_Report_File_Pnd_Rep_Option_Code,"TRANS/CODE/",
        		pnd_Report_File_Pnd_Rep_Trans_Code,"PERIODIC/UNITS/",
        		pnd_Report_File_Pnd_Rep_Fund_Units_After);
    }
    private void CheckAtStartofData239() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Prev_Fund_Code.setValue(pnd_Report_File_Pnd_Rep_Fund_Code);                                                                                               //Natural: MOVE #REP-FUND-CODE TO #PREV-FUND-CODE
            DbsUtil.examine(new ExamineSource(pnd_Fund_Code.getValue("*")), new ExamineSearch(pnd_Report_File_Pnd_Rep_Fund_Code), new ExamineGivingIndex(pnd_I));         //Natural: EXAMINE #FUND-CODE ( * ) FOR #REP-FUND-CODE GIVING INDEX #I
            pnd_Title_Fund_Name.setValue(pnd_Fund_Desc.getValue(pnd_I));                                                                                                  //Natural: MOVE #FUND-DESC ( #I ) TO #TITLE-FUND-NAME
            //*  ADDED 5/00
            pnd_Hd_Title.setValue(DbsUtil.compress(pnd_Title_Fund_Name, pnd_Hd_Desc));                                                                                    //Natural: COMPRESS #TITLE-FUND-NAME #HD-DESC INTO #HD-TITLE
        }                                                                                                                                                                 //Natural: END-START
    }
}
