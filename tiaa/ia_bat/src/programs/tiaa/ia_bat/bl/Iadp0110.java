/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:53 PM
**        * FROM NATURAL PROGRAM : Iadp0110
************************************************************
**        * FILE NAME            : Iadp0110.java
**        * CLASS NAME           : Iadp0110
**        * INSTANCE NAME        : Iadp0110
************************************************************
************************************************************************
* PROGRAM  : IADP0110
* SYSTEM   : IAS
* TITLE    : IADP0110
* GENERATED: JAN 19,95 AT 02:03 PM
* FUNCTION : THIS PROGRAM ... READ SORTED EXT FILE SORTED IN PIN-NBR
*                             CONTRACT-NBR SEQ
*                             CREATE FILE WITH LOWEST-CONTRACT NBR &
*                             CONTRACT-NBR COMBINING
*                             IA AUTO COMBINE CHECK PROCESSING
*
**SAG PRIMARY-FILE: COR-EXT RECORD
*
* HISTORY
*          CHANGED 8/97    ADDED NEW PREFIXES Y & Z
*                          DO SCAN ON 8/97 FOR CHANGE
*          CHANGED 2/13    TNG CHANGES - FIX THE TIAA/CREF PREFIX
*          JUN F. TINIO    IDENTIFICATION. SCAN ON 2/13 FOR CHANGES
*          04/2017 OS      PIN EXPANSION CHANGES MARKED 082017.
*          05/26/2017 RC   FIX COMBINED CONTRACT PROCESS RC082017
*                          HAD TO FIX DEFINITION FOR PIN EXPANION ON
*                          INPUT ETL FILE
*                          ETL IS PASSING A FILE WITH EXPANDED PIN. BUT
*                          ADDITIONAL 5 BYTES IS POSITIONED AFTER THE
*                          ORIGINAL 7-BYTE PIN.
************************************************************************

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iadp0110 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Cmb_Cntrct_Tbl;

    private DbsGroup pnd_Cor_Ext_Rec;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr;

    private DbsGroup pnd_Cor_Ext_Rec__R_Field_1;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Pfx;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_In_Nbr;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_Payee_Cde;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind;
    private DbsField pnd_Cor_Ext_Rec_Pnd_Cor_Fil;

    private DbsGroup pnd_Cmb_Out_Rec;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_No;

    private DbsGroup pnd_Cmb_Out_Rec__R_Field_2;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_Pfx;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Nbr;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Addr_Ind;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_No;

    private DbsGroup pnd_Cmb_Out_Rec__R_Field_3;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx;
    private DbsField pnd_Cmb_Out_Rec_Pnd_Cmb_Fil;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_Tot_Rec_Out;
    private DbsField pnd_Tot_Tiaa_Contr;
    private DbsField pnd_Tot_Cref_Contr;
    private DbsField pnd_Save_Low_Contr;
    private DbsField pnd_Save_Payee_Cde;
    private DbsField pnd_Cntrct_Cnt;
    private DbsField pnd_Sve_Pin_Nbr;
    private DbsField pnd_First_Tme;
    private DbsField pnd_Save_Mode_Ind;
    private DbsField pnd_Save_Low_Contract;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Cmb_Cntrct_Tbl = localVariables.newFieldArrayInRecord("pnd_Cmb_Cntrct_Tbl", "#CMB-CNTRCT-TBL", FieldType.STRING, 8, new DbsArrayController(1, 
            200));

        pnd_Cor_Ext_Rec = localVariables.newGroupInRecord("pnd_Cor_Ext_Rec", "#COR-EXT-REC");
        pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr = pnd_Cor_Ext_Rec.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr", "#COR-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr = pnd_Cor_Ext_Rec.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr", "#COR-IN-CNTRCT-NBR", FieldType.STRING, 
            8);

        pnd_Cor_Ext_Rec__R_Field_1 = pnd_Cor_Ext_Rec.newGroupInGroup("pnd_Cor_Ext_Rec__R_Field_1", "REDEFINE", pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr);
        pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Pfx = pnd_Cor_Ext_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Pfx", "#COR-IN-CNTRCT-PFX", 
            FieldType.STRING, 2);
        pnd_Cor_Ext_Rec_Pnd_Cor_In_Nbr = pnd_Cor_Ext_Rec__R_Field_1.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_In_Nbr", "#COR-IN-NBR", FieldType.NUMERIC, 
            6);
        pnd_Cor_Ext_Rec_Pnd_Cor_Payee_Cde = pnd_Cor_Ext_Rec.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_Payee_Cde", "#COR-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind = pnd_Cor_Ext_Rec.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind", "#COR-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Cor_Ext_Rec_Pnd_Cor_Fil = pnd_Cor_Ext_Rec.newFieldInGroup("pnd_Cor_Ext_Rec_Pnd_Cor_Fil", "#COR-FIL", FieldType.STRING, 1);

        pnd_Cmb_Out_Rec = localVariables.newGroupInRecord("pnd_Cmb_Out_Rec", "#CMB-OUT-REC");
        pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_No = pnd_Cmb_Out_Rec.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_No", "#CMB-LOW-CONTRACT-NO", FieldType.STRING, 
            8);

        pnd_Cmb_Out_Rec__R_Field_2 = pnd_Cmb_Out_Rec.newGroupInGroup("pnd_Cmb_Out_Rec__R_Field_2", "REDEFINE", pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_No);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_Pfx = pnd_Cmb_Out_Rec__R_Field_2.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_Pfx", "#CMB-LOW-CONTRACT-PFX", 
            FieldType.STRING, 2);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Nbr = pnd_Cmb_Out_Rec__R_Field_2.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Nbr", "#CMB-LOW-NBR", FieldType.NUMERIC, 
            6);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Addr_Ind = pnd_Cmb_Out_Rec.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Addr_Ind", "#CMB-ADDR-IND", FieldType.STRING, 1);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_No = pnd_Cmb_Out_Rec.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_No", "#CMB-NXT-CONTRACT-NO", FieldType.STRING, 
            8);

        pnd_Cmb_Out_Rec__R_Field_3 = pnd_Cmb_Out_Rec.newGroupInGroup("pnd_Cmb_Out_Rec__R_Field_3", "REDEFINE", pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_No);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx = pnd_Cmb_Out_Rec__R_Field_3.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx", "#CMB-NXT-CONTRACT-PFX", 
            FieldType.STRING, 2);
        pnd_Cmb_Out_Rec_Pnd_Cmb_Fil = pnd_Cmb_Out_Rec.newFieldInGroup("pnd_Cmb_Out_Rec_Pnd_Cmb_Fil", "#CMB-FIL", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 4);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.NUMERIC, 4);
        pnd_Tot_Rec_Out = localVariables.newFieldInRecord("pnd_Tot_Rec_Out", "#TOT-REC-OUT", FieldType.NUMERIC, 9);
        pnd_Tot_Tiaa_Contr = localVariables.newFieldInRecord("pnd_Tot_Tiaa_Contr", "#TOT-TIAA-CONTR", FieldType.NUMERIC, 7);
        pnd_Tot_Cref_Contr = localVariables.newFieldInRecord("pnd_Tot_Cref_Contr", "#TOT-CREF-CONTR", FieldType.NUMERIC, 7);
        pnd_Save_Low_Contr = localVariables.newFieldInRecord("pnd_Save_Low_Contr", "#SAVE-LOW-CONTR", FieldType.STRING, 8);
        pnd_Save_Payee_Cde = localVariables.newFieldInRecord("pnd_Save_Payee_Cde", "#SAVE-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.NUMERIC, 4);
        pnd_Sve_Pin_Nbr = localVariables.newFieldInRecord("pnd_Sve_Pin_Nbr", "#SVE-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_First_Tme = localVariables.newFieldInRecord("pnd_First_Tme", "#FIRST-TME", FieldType.BOOLEAN, 1);
        pnd_Save_Mode_Ind = localVariables.newFieldInRecord("pnd_Save_Mode_Ind", "#SAVE-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Save_Low_Contract = localVariables.newFieldInRecord("pnd_Save_Low_Contract", "#SAVE-LOW-CONTRACT", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_Cmb_Cntrct_Tbl.getValue(1).setInitialValue(" ");
        pnd_I.setInitialValue(0);
        pnd_I2.setInitialValue(0);
        pnd_Tot_Rec_Out.setInitialValue(0);
        pnd_Tot_Tiaa_Contr.setInitialValue(0);
        pnd_Tot_Cref_Contr.setInitialValue(0);
        pnd_Save_Low_Contr.setInitialValue("99999999");
        pnd_Save_Payee_Cde.setInitialValue("00");
        pnd_Cntrct_Cnt.setInitialValue(0);
        pnd_Sve_Pin_Nbr.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iadp0110() throws Exception
    {
        super("Iadp0110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 56
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        pnd_First_Tme.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #FIRST-TME
        READ_PRIME:                                                                                                                                                       //Natural: READ WORK 1 #COR-EXT-REC
        while (condition(getWorkFiles().read(1, pnd_Cor_Ext_Rec)))
        {
            //*  EQUAL TRUE 1ST TIME THRU
            if (condition(pnd_First_Tme.getBoolean()))                                                                                                                    //Natural: IF #FIRST-TME
            {
                pnd_Sve_Pin_Nbr.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr);                                                                                                //Natural: ASSIGN #SVE-PIN-NBR := #COR-PIN-NBR
                //*  RC082017
                //*  RC082017
                pnd_First_Tme.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #FIRST-TME
                pnd_Save_Mode_Ind.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind);                                                                                             //Natural: ASSIGN #SAVE-MODE-IND := #COR-MODE-IND
                pnd_Save_Low_Contract.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr);                                                                                    //Natural: ASSIGN #SAVE-LOW-CONTRACT := #COR-IN-CNTRCT-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*   IF #SVE-PIN-NBR  NE #COR-PIN-NBR
            //*      #SVE-PIN-NBR  := #COR-PIN-NBR
            //*      PERFORM WRITE-CONTRACT-RECS-FOR-PIN
            //*      RESET INITIAL #I2 #CMB-CNTRCT-TBL(*)
            //*      #SAVE-LOW-CONTR :=  '999999999'
            //*   END-IF
            //*  RC082017
            //*  RC082017
            //*  RC082017
            if (condition(pnd_Sve_Pin_Nbr.equals(pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr) && pnd_Save_Mode_Ind.equals(pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind)))                         //Natural: IF #SVE-PIN-NBR EQ #COR-PIN-NBR AND #SAVE-MODE-IND EQ #COR-MODE-IND
            {
                ignore();
                //*  RC082017
                //*  RC082017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sve_Pin_Nbr.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_Pin_Nbr);                                                                                                //Natural: ASSIGN #SVE-PIN-NBR := #COR-PIN-NBR
                pnd_Save_Mode_Ind.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_Mode_Ind);                                                                                             //Natural: ASSIGN #SAVE-MODE-IND := #COR-MODE-IND
                pnd_Save_Low_Contract.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr);                                                                                    //Natural: ASSIGN #SAVE-LOW-CONTRACT := #COR-IN-CNTRCT-NBR
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTRACT-RECS-FOR-PIN
                sub_Write_Contract_Recs_For_Pin();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_I2.resetInitial();                                                                                                                                    //Natural: RESET INITIAL #I2 #CMB-CNTRCT-TBL ( * )
                pnd_Cmb_Cntrct_Tbl.getValue("*").resetInitial();
                pnd_Save_Low_Contr.setValue("999999999");                                                                                                                 //Natural: ASSIGN #SAVE-LOW-CONTR := '999999999'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Save_Low_Contr.greater(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr)))                                                                             //Natural: IF #SAVE-LOW-CONTR GT #COR-IN-CNTRCT-NBR
            {
                pnd_Save_Low_Contr.setValue(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr);                                                                                       //Natural: ASSIGN #SAVE-LOW-CONTR := #COR-IN-CNTRCT-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_I2.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #I2
            pnd_Cmb_Cntrct_Tbl.getValue(pnd_I2).setValue(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr);                                                                          //Natural: ASSIGN #CMB-CNTRCT-TBL ( #I2 ) := #COR-IN-CNTRCT-NBR
            //*  ADD Z 8/97
            if (condition(DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'0L'") || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'0M'")          //Natural: IF #COR-IN-CNTRCT-NBR = MASK ( '0L' ) OR = MASK ( '0M' ) OR = MASK ( '0N' ) OR = MASK ( '0U' ) OR = MASK ( '0T' ) OR = MASK ( 'Z' )
                || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'0N'") || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'0U'") 
                || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'0T'") || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'Z'")))
            {
                //*         OR  = MASK('6L') OR = MASK('6M') OR = MASK ('6N') /* 2/13
                pnd_Tot_Cref_Contr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-CREF-CONTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Tiaa_Contr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-TIAA-CONTR
            }                                                                                                                                                             //Natural: END-IF
            //*  2/13 - START
            if (condition(DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6L'") || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6M'")          //Natural: IF #COR-IN-CNTRCT-NBR = MASK ( '6L' ) OR = MASK ( '6M' ) OR = MASK ( '6N' )
                || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6N'")))
            {
                //*  REA
                if (condition(DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6L7'") || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6M7'")    //Natural: IF #COR-IN-CNTRCT-NBR = MASK ( '6L7' ) OR = MASK ( '6M7' ) OR = MASK ( '6N7' )
                    || DbsUtil.maskMatches(pnd_Cor_Ext_Rec_Pnd_Cor_In_Cntrct_Nbr,"'6N7'")))
                {
                    pnd_Tot_Tiaa_Contr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOT-TIAA-CONTR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tot_Cref_Contr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOT-CREF-CONTR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  2/13 - END
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-WORK
        READ_PRIME_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL RECORDS OUT ..........",pnd_Tot_Rec_Out);                                                              //Natural: WRITE ( 1 ) / ' TOTAL RECORDS OUT ..........' #TOT-REC-OUT
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL TIAA CONTRACTS........",pnd_Tot_Tiaa_Contr);                                                           //Natural: WRITE ( 1 ) / ' TOTAL TIAA CONTRACTS........' #TOT-TIAA-CONTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE," TOTAL CREF CONTRACTS........",pnd_Tot_Cref_Contr);                                                           //Natural: WRITE ( 1 ) / ' TOTAL CREF CONTRACTS........' #TOT-CREF-CONTR
        if (Global.isEscape()) return;
        //*  -----------------------------------------------------
        //*  FORMAT OUTPUT RECORDS
        //*  -----------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTRACT-RECS-FOR-PIN
        //* *
    }
    private void sub_Write_Contract_Recs_For_Pin() throws Exception                                                                                                       //Natural: WRITE-CONTRACT-RECS-FOR-PIN
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            if (condition(pnd_Cmb_Cntrct_Tbl.getValue(pnd_I).equals("        ")))                                                                                         //Natural: IF #CMB-CNTRCT-TBL ( #I ) = '        '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cmb_Out_Rec_Pnd_Cmb_Low_Contract_No.setValue(pnd_Save_Low_Contr);                                                                                         //Natural: ASSIGN #CMB-LOW-CONTRACT-NO := #SAVE-LOW-CONTR
            pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_No.setValue(pnd_Cmb_Cntrct_Tbl.getValue(pnd_I));                                                                         //Natural: ASSIGN #CMB-NXT-CONTRACT-NO := #CMB-CNTRCT-TBL ( #I )
            //*  ADDED  Z 8/97
            if (condition(pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0L") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0M") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0N")  //Natural: IF #CMB-NXT-CONTRACT-PFX = '0L' OR = '0M' OR = '0N' OR = '0T' OR = '0U' OR = '0T' OR = MASK ( 'Z' ) OR = '6L' OR = '6M' OR = '6N'
                || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0T") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0U") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("0T") 
                || DbsUtil.maskMatches(pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx,"'Z'") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("6L") || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("6M") 
                || pnd_Cmb_Out_Rec_Pnd_Cmb_Nxt_Contract_Pfx.equals("6N")))
            {
                pnd_Cmb_Out_Rec_Pnd_Cmb_Addr_Ind.setValue("1");                                                                                                           //Natural: ASSIGN #CMB-ADDR-IND := '1'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cmb_Out_Rec_Pnd_Cmb_Addr_Ind.setValue("3");                                                                                                           //Natural: ASSIGN #CMB-ADDR-IND := '3'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cmb_Out_Rec_Pnd_Cmb_Fil.setValue(0);                                                                                                                      //Natural: ASSIGN #CMB-FIL := 0
            pnd_Tot_Rec_Out.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT-REC-OUT
            getWorkFiles().write(2, false, pnd_Cmb_Out_Rec);                                                                                                              //Natural: WRITE WORK FILE 2 #CMB-OUT-REC
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(22),"IA OUT IN LOW-CONTRACT & CONTRACT COMBINING DEQ",new               //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 22T 'IA OUT IN LOW-CONTRACT & CONTRACT COMBINING DEQ' 72T 'Page' *PAGE-NUMBER ( 1 ) ( NL = 3 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 21T 'NEW ISSUES 304 COMBINE CHECK PROCESSING' 72T *TIMX ( EM = HH':'II' 'AP )
                        TabSetting(72),"Page",getReports().getPageNumberDbs(1), new NumericLength (3), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), 
                        new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(21),"NEW ISSUES 304 COMBINE CHECK PROCESSING",new TabSetting(72),Global.getTIMX(), 
                        new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=56");
    }
}
