/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:09 PM
**        * FROM NATURAL PROGRAM : Iaapgrpc
************************************************************
**        * FILE NAME            : Iaapgrpc.java
**        * CLASS NAME           : Iaapgrpc
**        * INSTANCE NAME        : Iaapgrpc
************************************************************
************************************************************************
*
* PROGRAM : IAAPGRPC
* DATE    : 12/03/2007
* PURPOSE : CREATE GROUP CONTRACTS FROM A BATCH FEED. BASED ON IAAPGRP
* HISTORY :
*
* 08/12/15 JT COR/NAAD SUNSET. SCAN 08/15 FOR CHANGES.
* 04/2017  OS PIN EXPANSION CHANGES MARKED 082017.
* 01/2018  RC ADD VOLUNTARY DEDUCTIONS. SCAN 1/2018 FOR CHANGES.
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.ia_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapgrpc extends BLNatBase
{
    // Data Areas
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup ti_Record_Input;
    private DbsField ti_Record_Input_Pnd_Ti1_Contract;
    private DbsField ti_Record_Input_Pnd_Ti1_Payee;
    private DbsField ti_Record_Input_Pnd_Ti1_Prod;
    private DbsField ti_Record_Input_Pnd_Ti1_Curr;
    private DbsField ti_Record_Input_Pnd_Ti1_Mode;
    private DbsField ti_Record_Input_Pnd_Ti1_Opt;
    private DbsField ti_Record_Input_Pnd_Ti1_Org;
    private DbsField ti_Record_Input_Pnd_Ti1_Iss_Dte;
    private DbsField ti_Record_Input_Pnd_Ti1_Fst_Due_Dte;
    private DbsField ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte;
    private DbsField ti_Record_Input_Pnd_Ti1_Fin_Per_Dte;

    private DbsGroup ti_Record_Input__R_Field_1;
    private DbsField ti_Record_Input_Pnd_Ti1_Fin_Per_Dten;
    private DbsField ti_Record_Input_Pnd_Ti1_Fin_Pmt_Dte;
    private DbsField ti_Record_Input_Pnd_Ti1_Citizen;
    private DbsField ti_Record_Input_Pnd_Ti1_Iss_St;
    private DbsField ti_Record_Input_Pnd_Ti1_Res_St;
    private DbsField ti_Record_Input_Pnd_Ti1_Iss_Coll;
    private DbsField ti_Record_Input_Pnd_Filler;
    private DbsField ti_Record_Input_Pnd_Ti1_Ssn;

    private DbsGroup ti_Record_Input__R_Field_2;
    private DbsField ti_Record_Input_Pnd_Ti1_Ssn_A;
    private DbsField ti_Record_Input_Pnd_Ti2_Fst_Cross;
    private DbsField ti_Record_Input_Pnd_Ti2_Fst_Sex;
    private DbsField ti_Record_Input_Pnd_Ti2_Fst_Dob;
    private DbsField ti_Record_Input_Pnd_Ti2_Fst_Mort_Yob;
    private DbsField ti_Record_Input_Pnd_Ti2_Fst_Life_Cnt;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Cross;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Sex;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Dob;

    private DbsGroup ti_Record_Input__R_Field_3;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Dobn;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Mort_Yob;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Life_Cnt;
    private DbsField ti_Record_Input_Pnd_Filler2;
    private DbsField ti_Record_Input_Pnd_Ti2_Sec_Snn;
    private DbsField ti_Record_Input_Pnd_Ti5_Rate1;
    private DbsField ti_Record_Input_Pnd_Ti5_Per_Pmt1_A;

    private DbsGroup ti_Record_Input__R_Field_4;
    private DbsField ti_Record_Input_Pnd_Ti5_Per_Pmt1;
    private DbsField ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A;

    private DbsGroup ti_Record_Input__R_Field_5;
    private DbsField ti_Record_Input_Pnd_Ti5_Fin_Pmt1;
    private DbsField ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt_A;

    private DbsGroup ti_Record_Input__R_Field_6;
    private DbsField ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt;
    private DbsField ti_Record_Input_Pnd_Ddctn_Info;

    private DbsGroup ti_Record_Input__R_Field_7;

    private DbsGroup ti_Record_Input_Pnd_Ddctn_Info_2;
    private DbsField ti_Record_Input_Pnd_Ddctn_Cde;
    private DbsField ti_Record_Input_Pnd_Ddctn_Amt;
    private DbsField ti_Record_Input_Pnd_Cola_Eligibility_Ind;
    private DbsField ti_Record_Input_Pnd_2k_Db_Eligibility_Ind;
    private DbsField ti_Record_Input_Pnd_Hrvrd_Emp_Id;
    private DbsField pnd_D;
    private DbsField pnd_I;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;

    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;

    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;

    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;

    private DbsGroup iaa_Tiaa_Fund_Rcrd__R_Field_8;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tckr_Symbl;

    private DataAccessProgramView vw_iaa_Deduction;
    private DbsField iaa_Deduction_Lst_Trans_Dte;
    private DbsField iaa_Deduction_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee_Cde;
    private DbsField iaa_Deduction_Ddctn_Id_Nbr;
    private DbsField iaa_Deduction_Ddctn_Cde;
    private DbsField iaa_Deduction_Ddctn_Seq_Nbr;
    private DbsField iaa_Deduction_Ddctn_Payee;
    private DbsField iaa_Deduction_Ddctn_Per_Amt;
    private DbsField iaa_Deduction_Ddctn_Ytd_Amt;
    private DbsField iaa_Deduction_Ddctn_Pd_To_Dte;
    private DbsField iaa_Deduction_Ddctn_Tot_Amt;
    private DbsField iaa_Deduction_Ddctn_Intent_Cde;
    private DbsField iaa_Deduction_Ddctn_Strt_Dte;
    private DbsField iaa_Deduction_Ddctn_Stp_Dte;
    private DbsField iaa_Deduction_Ddctn_Final_Dte;

    private DataAccessProgramView vw_iaa_Ddctn_Trans;
    private DbsField iaa_Ddctn_Trans_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Lst_Trans_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Id_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Cde;

    private DbsGroup iaa_Ddctn_Trans__R_Field_9;
    private DbsField iaa_Ddctn_Trans_Ddctn_Seq_Nbr;
    private DbsField iaa_Ddctn_Trans_Ddctn_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Payee;
    private DbsField iaa_Ddctn_Trans_Ddctn_Per_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Ytd_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Pd_To_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Tot_Amt;
    private DbsField iaa_Ddctn_Trans_Ddctn_Intent_Cde;
    private DbsField iaa_Ddctn_Trans_Ddctn_Strt_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Stp_Dte;
    private DbsField iaa_Ddctn_Trans_Ddctn_Final_Dte;
    private DbsField iaa_Ddctn_Trans_Trans_Check_Dte;
    private DbsField iaa_Ddctn_Trans_Bfre_Imge_Id;
    private DbsField iaa_Ddctn_Trans_Aftr_Imge_Id;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key;

    private DbsGroup pnd_Cntrct_Payee_Ddctn_Key__R_Field_10;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr;
    private DbsField pnd_Cntrct_Payee_Ddctn_Key__Filler1;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DbsField pnd_Invrse_Dte;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Check_Dte;

    private DbsGroup pnd_Check_Dte__R_Field_11;
    private DbsField pnd_Check_Dte_Pnd_Check_Dte_A;
    private DbsField pnd_Todays_Dte;
    private DbsField pnd_Date;
    private DbsField pnd_S_Invrse_Dte;
    private DbsField pnd_Per_Ivc_Amt;
    private DbsField pnd_Ivc_Used_Amt;
    private DbsField pnd_Cntrct_Cnt;
    private DbsField pnd_Tot_Per_Amt;

    private DbsGroup pnd_Ded;
    private DbsField pnd_Ded_Pnd_D_Cde;
    private DbsField pnd_Ded_Pnd_D_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        ti_Record_Input = localVariables.newGroupInRecord("ti_Record_Input", "TI-RECORD-INPUT");
        ti_Record_Input_Pnd_Ti1_Contract = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Contract", "#TI1-CONTRACT", FieldType.STRING, 8);
        ti_Record_Input_Pnd_Ti1_Payee = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Payee", "#TI1-PAYEE", FieldType.NUMERIC, 2);
        ti_Record_Input_Pnd_Ti1_Prod = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Prod", "#TI1-PROD", FieldType.STRING, 1);
        ti_Record_Input_Pnd_Ti1_Curr = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Curr", "#TI1-CURR", FieldType.NUMERIC, 1);
        ti_Record_Input_Pnd_Ti1_Mode = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Mode", "#TI1-MODE", FieldType.NUMERIC, 3);
        ti_Record_Input_Pnd_Ti1_Opt = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Opt", "#TI1-OPT", FieldType.NUMERIC, 2);
        ti_Record_Input_Pnd_Ti1_Org = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Org", "#TI1-ORG", FieldType.NUMERIC, 2);
        ti_Record_Input_Pnd_Ti1_Iss_Dte = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Iss_Dte", "#TI1-ISS-DTE", FieldType.NUMERIC, 6);
        ti_Record_Input_Pnd_Ti1_Fst_Due_Dte = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Fst_Due_Dte", "#TI1-FST-DUE-DTE", FieldType.NUMERIC, 
            6);
        ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte", "#TI1-FST-PD-DTE", FieldType.NUMERIC, 
            6);
        ti_Record_Input_Pnd_Ti1_Fin_Per_Dte = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Fin_Per_Dte", "#TI1-FIN-PER-DTE", FieldType.STRING, 
            6);

        ti_Record_Input__R_Field_1 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_1", "REDEFINE", ti_Record_Input_Pnd_Ti1_Fin_Per_Dte);
        ti_Record_Input_Pnd_Ti1_Fin_Per_Dten = ti_Record_Input__R_Field_1.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Fin_Per_Dten", "#TI1-FIN-PER-DTEN", 
            FieldType.NUMERIC, 6);
        ti_Record_Input_Pnd_Ti1_Fin_Pmt_Dte = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Fin_Pmt_Dte", "#TI1-FIN-PMT-DTE", FieldType.STRING, 
            8);
        ti_Record_Input_Pnd_Ti1_Citizen = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Citizen", "#TI1-CITIZEN", FieldType.NUMERIC, 3);
        ti_Record_Input_Pnd_Ti1_Iss_St = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Iss_St", "#TI1-ISS-ST", FieldType.STRING, 3);
        ti_Record_Input_Pnd_Ti1_Res_St = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Res_St", "#TI1-RES-ST", FieldType.STRING, 3);
        ti_Record_Input_Pnd_Ti1_Iss_Coll = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Iss_Coll", "#TI1-ISS-COLL", FieldType.STRING, 5);
        ti_Record_Input_Pnd_Filler = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Filler", "#FILLER", FieldType.STRING, 9);
        ti_Record_Input_Pnd_Ti1_Ssn = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Ssn", "#TI1-SSN", FieldType.NUMERIC, 9);

        ti_Record_Input__R_Field_2 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_2", "REDEFINE", ti_Record_Input_Pnd_Ti1_Ssn);
        ti_Record_Input_Pnd_Ti1_Ssn_A = ti_Record_Input__R_Field_2.newFieldInGroup("ti_Record_Input_Pnd_Ti1_Ssn_A", "#TI1-SSN-A", FieldType.STRING, 9);
        ti_Record_Input_Pnd_Ti2_Fst_Cross = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Fst_Cross", "#TI2-FST-CROSS", FieldType.STRING, 9);
        ti_Record_Input_Pnd_Ti2_Fst_Sex = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Fst_Sex", "#TI2-FST-SEX", FieldType.STRING, 1);
        ti_Record_Input_Pnd_Ti2_Fst_Dob = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Fst_Dob", "#TI2-FST-DOB", FieldType.NUMERIC, 8);
        ti_Record_Input_Pnd_Ti2_Fst_Mort_Yob = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Fst_Mort_Yob", "#TI2-FST-MORT-YOB", FieldType.NUMERIC, 
            4);
        ti_Record_Input_Pnd_Ti2_Fst_Life_Cnt = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Fst_Life_Cnt", "#TI2-FST-LIFE-CNT", FieldType.NUMERIC, 
            1);
        ti_Record_Input_Pnd_Ti2_Sec_Cross = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Cross", "#TI2-SEC-CROSS", FieldType.STRING, 9);
        ti_Record_Input_Pnd_Ti2_Sec_Sex = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Sex", "#TI2-SEC-SEX", FieldType.STRING, 1);
        ti_Record_Input_Pnd_Ti2_Sec_Dob = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Dob", "#TI2-SEC-DOB", FieldType.STRING, 8);

        ti_Record_Input__R_Field_3 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_3", "REDEFINE", ti_Record_Input_Pnd_Ti2_Sec_Dob);
        ti_Record_Input_Pnd_Ti2_Sec_Dobn = ti_Record_Input__R_Field_3.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Dobn", "#TI2-SEC-DOBN", FieldType.NUMERIC, 
            8);
        ti_Record_Input_Pnd_Ti2_Sec_Mort_Yob = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Mort_Yob", "#TI2-SEC-MORT-YOB", FieldType.STRING, 
            4);
        ti_Record_Input_Pnd_Ti2_Sec_Life_Cnt = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Life_Cnt", "#TI2-SEC-LIFE-CNT", FieldType.STRING, 
            1);
        ti_Record_Input_Pnd_Filler2 = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Filler2", "#FILLER2", FieldType.STRING, 8);
        ti_Record_Input_Pnd_Ti2_Sec_Snn = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti2_Sec_Snn", "#TI2-SEC-SNN", FieldType.STRING, 9);
        ti_Record_Input_Pnd_Ti5_Rate1 = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Rate1", "#TI5-RATE1", FieldType.NUMERIC, 2);
        ti_Record_Input_Pnd_Ti5_Per_Pmt1_A = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Per_Pmt1_A", "#TI5-PER-PMT1-A", FieldType.STRING, 
            7);

        ti_Record_Input__R_Field_4 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_4", "REDEFINE", ti_Record_Input_Pnd_Ti5_Per_Pmt1_A);
        ti_Record_Input_Pnd_Ti5_Per_Pmt1 = ti_Record_Input__R_Field_4.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Per_Pmt1", "#TI5-PER-PMT1", FieldType.NUMERIC, 
            7, 2);
        ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A", "#TI5-FIN-PMT1-A", FieldType.STRING, 
            9);

        ti_Record_Input__R_Field_5 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_5", "REDEFINE", ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A);
        ti_Record_Input_Pnd_Ti5_Fin_Pmt1 = ti_Record_Input__R_Field_5.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Fin_Pmt1", "#TI5-FIN-PMT1", FieldType.NUMERIC, 
            9, 2);
        ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt_A = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt_A", "#TI5-CNTRCT-IVC-AMT-A", 
            FieldType.STRING, 9);

        ti_Record_Input__R_Field_6 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_6", "REDEFINE", ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt_A);
        ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt = ti_Record_Input__R_Field_6.newFieldInGroup("ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt", "#TI5-CNTRCT-IVC-AMT", 
            FieldType.NUMERIC, 9, 2);
        ti_Record_Input_Pnd_Ddctn_Info = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Ddctn_Info", "#DDCTN-INFO", FieldType.STRING, 60);

        ti_Record_Input__R_Field_7 = ti_Record_Input.newGroupInGroup("ti_Record_Input__R_Field_7", "REDEFINE", ti_Record_Input_Pnd_Ddctn_Info);

        ti_Record_Input_Pnd_Ddctn_Info_2 = ti_Record_Input__R_Field_7.newGroupArrayInGroup("ti_Record_Input_Pnd_Ddctn_Info_2", "#DDCTN-INFO-2", new DbsArrayController(1, 
            6));
        ti_Record_Input_Pnd_Ddctn_Cde = ti_Record_Input_Pnd_Ddctn_Info_2.newFieldInGroup("ti_Record_Input_Pnd_Ddctn_Cde", "#DDCTN-CDE", FieldType.NUMERIC, 
            3);
        ti_Record_Input_Pnd_Ddctn_Amt = ti_Record_Input_Pnd_Ddctn_Info_2.newFieldInGroup("ti_Record_Input_Pnd_Ddctn_Amt", "#DDCTN-AMT", FieldType.NUMERIC, 
            7, 2);
        ti_Record_Input_Pnd_Cola_Eligibility_Ind = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Cola_Eligibility_Ind", "#COLA-ELIGIBILITY-IND", 
            FieldType.STRING, 1);
        ti_Record_Input_Pnd_2k_Db_Eligibility_Ind = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_2k_Db_Eligibility_Ind", "#2K-DB-ELIGIBILITY-IND", 
            FieldType.STRING, 1);
        ti_Record_Input_Pnd_Hrvrd_Emp_Id = ti_Record_Input.newFieldInGroup("ti_Record_Input_Pnd_Hrvrd_Emp_Id", "#HRVRD-EMP-ID", FieldType.STRING, 8);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("IAA_CNTRCT_CNTRCT_FNL_PRM_DTEMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        registerRecord(vw_iaa_Cntrct);

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");

        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("IAA_CNTRCT_PRTCPNT_ROLE_RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1, 4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        registerRecord(vw_iaa_Cntrct_Prtcpnt_Role);

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_CNTRCT_PPCN_NBR", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        iaa_Tiaa_Fund_Rcrd__R_Field_8 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd__R_Field_8", "REDEFINE", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_8.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", FieldType.STRING, 
            1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd__R_Field_8.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", FieldType.STRING, 
            2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_TOT_PER_AMT", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_PER_AMT", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_OLD_DIV_AMT", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");

        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_CDE", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_RATE_DTE", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("IAA_TIAA_FUND_RCRD_TIAA_UNITS_CNT", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 90) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_IN_DTE", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("IAA_TIAA_FUND_RCRD_TIAA_LST_XFR_OUT_DTE", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_Tckr_Symbl = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        registerRecord(vw_iaa_Tiaa_Fund_Rcrd);

        vw_iaa_Deduction = new DataAccessProgramView(new NameInfo("vw_iaa_Deduction", "IAA-DEDUCTION"), "IAA_DEDUCTION", "IA_CONTRACT_PART");
        iaa_Deduction_Lst_Trans_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Deduction_Ddctn_Ppcn_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Deduction_Ddctn_Payee_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Deduction_Ddctn_Id_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Deduction_Ddctn_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DDCTN_CDE");
        iaa_Deduction_Ddctn_Seq_Nbr = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "DDCTN_SEQ_NBR");
        iaa_Deduction_Ddctn_Payee = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "DDCTN_PAYEE");
        iaa_Deduction_Ddctn_Per_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Deduction_Ddctn_Ytd_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Deduction_Ddctn_Pd_To_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Deduction_Ddctn_Tot_Amt = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Deduction_Ddctn_Intent_Cde = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Deduction_Ddctn_Strt_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Deduction_Ddctn_Stp_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Deduction_Ddctn_Final_Dte = vw_iaa_Deduction.getRecord().newFieldInGroup("iaa_Deduction_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        registerRecord(vw_iaa_Deduction);

        vw_iaa_Ddctn_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Ddctn_Trans", "IAA-DDCTN-TRANS"), "IAA_DDCTN_TRANS", "IA_TRANS_FILE");
        iaa_Ddctn_Trans_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Ddctn_Trans_Invrse_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Ddctn_Trans_Lst_Trans_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ppcn_Nbr", "DDCTN-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "DDCTN_PPCN_NBR");
        iaa_Ddctn_Trans_Ddctn_Payee_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee_Cde", "DDCTN-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "DDCTN_PAYEE_CDE");
        iaa_Ddctn_Trans_Ddctn_Id_Nbr = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Id_Nbr", "DDCTN-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "DDCTN_ID_NBR");
        iaa_Ddctn_Trans_Ddctn_Seq_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Cde", "DDCTN-SEQ-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "DDCTN_SEQ_CDE");

        iaa_Ddctn_Trans__R_Field_9 = vw_iaa_Ddctn_Trans.getRecord().newGroupInGroup("iaa_Ddctn_Trans__R_Field_9", "REDEFINE", iaa_Ddctn_Trans_Ddctn_Seq_Cde);
        iaa_Ddctn_Trans_Ddctn_Seq_Nbr = iaa_Ddctn_Trans__R_Field_9.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Seq_Nbr", "DDCTN-SEQ-NBR", FieldType.NUMERIC, 
            3);
        iaa_Ddctn_Trans_Ddctn_Cde = iaa_Ddctn_Trans__R_Field_9.newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Cde", "DDCTN-CDE", FieldType.STRING, 3);
        iaa_Ddctn_Trans_Ddctn_Payee = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Payee", "DDCTN-PAYEE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "DDCTN_PAYEE");
        iaa_Ddctn_Trans_Ddctn_Per_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Per_Amt", "DDCTN-PER-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "DDCTN_PER_AMT");
        iaa_Ddctn_Trans_Ddctn_Ytd_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Ytd_Amt", "DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_YTD_AMT");
        iaa_Ddctn_Trans_Ddctn_Pd_To_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Pd_To_Dte", "DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_PD_TO_DTE");
        iaa_Ddctn_Trans_Ddctn_Tot_Amt = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Tot_Amt", "DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "DDCTN_TOT_AMT");
        iaa_Ddctn_Trans_Ddctn_Intent_Cde = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Intent_Cde", "DDCTN-INTENT-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "DDCTN_INTENT_CDE");
        iaa_Ddctn_Trans_Ddctn_Strt_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Strt_Dte", "DDCTN-STRT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STRT_DTE");
        iaa_Ddctn_Trans_Ddctn_Stp_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Stp_Dte", "DDCTN-STP-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_STP_DTE");
        iaa_Ddctn_Trans_Ddctn_Final_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Ddctn_Final_Dte", "DDCTN-FINAL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "DDCTN_FINAL_DTE");
        iaa_Ddctn_Trans_Trans_Check_Dte = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Ddctn_Trans_Bfre_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Ddctn_Trans_Aftr_Imge_Id = vw_iaa_Ddctn_Trans.getRecord().newFieldInGroup("iaa_Ddctn_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        registerRecord(vw_iaa_Ddctn_Trans);

        pnd_Cntrct_Payee_Ddctn_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Ddctn_Key", "#CNTRCT-PAYEE-DDCTN-KEY", FieldType.STRING, 18);

        pnd_Cntrct_Payee_Ddctn_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Ddctn_Key__R_Field_10", "REDEFINE", pnd_Cntrct_Payee_Ddctn_Key);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr = pnd_Cntrct_Payee_Ddctn_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr", 
            "#DDCTN-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde = pnd_Cntrct_Payee_Ddctn_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde", 
            "#DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr = pnd_Cntrct_Payee_Ddctn_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr", 
            "#DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Cntrct_Payee_Ddctn_Key__Filler1 = pnd_Cntrct_Payee_Ddctn_Key__R_Field_10.newFieldInGroup("pnd_Cntrct_Payee_Ddctn_Key__Filler1", "_FILLER1", 
            FieldType.STRING, 3);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Invrse_Dte = localVariables.newFieldInRecord("pnd_Invrse_Dte", "#INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.NUMERIC, 8);

        pnd_Check_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_Check_Dte__R_Field_11", "REDEFINE", pnd_Check_Dte);
        pnd_Check_Dte_Pnd_Check_Dte_A = pnd_Check_Dte__R_Field_11.newFieldInGroup("pnd_Check_Dte_Pnd_Check_Dte_A", "#CHECK-DTE-A", FieldType.STRING, 8);
        pnd_Todays_Dte = localVariables.newFieldInRecord("pnd_Todays_Dte", "#TODAYS-DTE", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_S_Invrse_Dte = localVariables.newFieldInRecord("pnd_S_Invrse_Dte", "#S-INVRSE-DTE", FieldType.PACKED_DECIMAL, 12);
        pnd_Per_Ivc_Amt = localVariables.newFieldInRecord("pnd_Per_Ivc_Amt", "#PER-IVC-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ivc_Used_Amt = localVariables.newFieldInRecord("pnd_Ivc_Used_Amt", "#IVC-USED-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Cntrct_Cnt = localVariables.newFieldInRecord("pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Per_Amt = localVariables.newFieldInRecord("pnd_Tot_Per_Amt", "#TOT-PER-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ded = localVariables.newGroupArrayInRecord("pnd_Ded", "#DED", new DbsArrayController(1, 6));
        pnd_Ded_Pnd_D_Cde = pnd_Ded.newFieldInGroup("pnd_Ded_Pnd_D_Cde", "#D-CDE", FieldType.STRING, 3);
        pnd_Ded_Pnd_D_Amt = pnd_Ded.newFieldInGroup("pnd_Ded_Pnd_D_Amt", "#D-AMT", FieldType.NUMERIC, 7, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Deduction.reset();
        vw_iaa_Ddctn_Trans.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapgrpc() throws Exception
    {
        super("Iaapgrpc");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  1/2018
        //*                                                                                                                                                               //Natural: FORMAT PS = 66 LS = 133;//Natural: FORMAT ( 1 ) PS = 66 LS = 250 ZP = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("READ01")))
        {
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Check_Dte.compute(new ComputeParameters(false, pnd_Check_Dte), pnd_Date.val());                                                                           //Natural: ASSIGN #CHECK-DTE := VAL ( #DATE )
            pnd_Date.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED CNTRL-TODAYS-DTE ( EM = YYYYMMDD ) TO #DATE
            pnd_Todays_Dte.compute(new ComputeParameters(false, pnd_Todays_Dte), pnd_Date.val());                                                                         //Natural: ASSIGN #TODAYS-DTE := VAL ( #DATE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  08/15 - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //* *#REC-TYPE := 01       /* 08/15
        READWORK02:                                                                                                                                                       //Natural: READ WORK 1 TI-RECORD-INPUT
        while (condition(getWorkFiles().read(1, ti_Record_Input)))
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-CONTRACT-RECORD
            sub_Create_Contract_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-RECORD
            sub_Create_Cpr_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-FUND-RECORD
            sub_Create_Fund_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-DDCTN-RECORD
            sub_Create_Ddctn_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RECORD
            sub_Create_Trans_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  07/2004
            pnd_Cntrct_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNTRCT-CNT
            vw_iaa_Cntrct.reset();                                                                                                                                        //Natural: RESET IAA-CNTRCT IAA-CNTRCT-PRTCPNT-ROLE IAA-TIAA-FUND-RCRD IAA-DEDUCTION #PER-IVC-AMT
            vw_iaa_Cntrct_Prtcpnt_Role.reset();
            vw_iaa_Tiaa_Fund_Rcrd.reset();
            vw_iaa_Deduction.reset();
            pnd_Per_Ivc_Amt.reset();
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  08/15 - CLOSE MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL CONTRACTS ADDED--->",pnd_Cntrct_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                       //Natural: WRITE ( 1 ) 'TOTAL CONTRACTS ADDED--->' #CNTRCT-CNT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL PAYMENTS  ADDED--->",pnd_Tot_Per_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));                                   //Natural: WRITE ( 1 ) 'TOTAL PAYMENTS  ADDED--->' #TOT-PER-AMT ( EM = ZZZ,ZZZ,ZZ9.99 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CONTRACT-RECORD
        //* ***********************************************************************
        //* *IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := #TI2-FST-SEX
        //*  052020 - END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPR-RECORD
        //* *IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := #CNTRCT-STTS-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FUND-RECORD
        //* *IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #TI5-PER-PMT1
        //* *IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT(1) := #TI5-PER-PMT1
        //* *IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT(1) := #TI5-FIN-PMT1
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PIN
        //*  #SSN-NBR := #TI1-SSN
        //*  IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR := #O-PIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DDCTN-RECORD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RECORD
        //*  1/2018 - START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PER-IVC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  1/2018 - END
    }
    private void sub_Create_Contract_Record() throws Exception                                                                                                            //Natural: CREATE-CONTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                         //Natural: ASSIGN #TRANS-DTE := *TIMX
        //*  DO NOT CREATE ANOTHER CONTRACT RECORD WHEN THE PAYEE IS NOT 01
        //*  - WILL CAUSE UNIQUENESS PROBLEM.
        if (condition(ti_Record_Input_Pnd_Ti1_Payee.notEquals(1)))                                                                                                        //Natural: IF #TI1-PAYEE NE 01
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Cntrct_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-PPCN-NBR := #TI1-CONTRACT
        iaa_Cntrct_Cntrct_Optn_Cde.setValue(ti_Record_Input_Pnd_Ti1_Opt);                                                                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-OPTN-CDE := #TI1-OPT
        iaa_Cntrct_Cntrct_Orgn_Cde.setValue(ti_Record_Input_Pnd_Ti1_Org);                                                                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #TI1-ORG
        iaa_Cntrct_Cntrct_Issue_Dte.setValue(ti_Record_Input_Pnd_Ti1_Iss_Dte);                                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE := #TI1-ISS-DTE
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte.setValue(ti_Record_Input_Pnd_Ti1_Fst_Due_Dte);                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE := #TI1-FST-DUE-DTE
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte.setValue(ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte);                                                                                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE := #TI1-FST-PD-DTE
        iaa_Cntrct_Cntrct_Crrncy_Cde.setValue(ti_Record_Input_Pnd_Ti1_Curr);                                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-CRRNCY-CDE := #TI1-CURR
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde.setValue(ti_Record_Input_Pnd_Ti1_Iss_St, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #TI1-ISS-ST TO IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        //* *IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE := #TI1-ISS-ST
        iaa_Cntrct_Cntrct_Inst_Iss_Cde.setValue(ti_Record_Input_Pnd_Ti1_Iss_Coll, MoveOption.RightJustified);                                                             //Natural: MOVE RIGHT #TI1-ISS-COLL TO IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        iaa_Cntrct_Cntrct_Inst_Iss_Cde.setValue(ti_Record_Input_Pnd_Ti1_Iss_Coll);                                                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-INST-ISS-CDE := #TI1-ISS-COLL
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind.setValue(ti_Record_Input_Pnd_Ti2_Fst_Cross);                                                                                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND := #TI2-FST-CROSS
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte.setValue(ti_Record_Input_Pnd_Ti2_Fst_Dob);                                                                                   //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE := #TI2-FST-DOB
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte.setValue(ti_Record_Input_Pnd_Ti2_Fst_Mort_Yob);                                                                       //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE := #TI2-FST-MORT-YOB
        if (condition(ti_Record_Input_Pnd_Ti2_Fst_Sex.equals("M")))                                                                                                       //Natural: IF #TI2-FST-SEX = 'M'
        {
            iaa_Cntrct_Cntrct_First_Annt_Sex_Cde.setValue(1);                                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ti_Record_Input_Pnd_Ti2_Fst_Sex.equals("F")))                                                                                                   //Natural: IF #TI2-FST-SEX = 'F'
            {
                iaa_Cntrct_Cntrct_First_Annt_Sex_Cde.setValue(2);                                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := 2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt.setValue(ti_Record_Input_Pnd_Ti2_Fst_Life_Cnt);                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := #TI2-FST-LIFE-CNT
        if (condition(iaa_Cntrct_Cntrct_Optn_Cde.equals(21)))                                                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt.setValue(0);                                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := 0
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind.setValue(ti_Record_Input_Pnd_Ti2_Sec_Cross);                                                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND := #TI2-SEC-CROSS
        //* *IF #TI2-SEC-DOB   NE ' 'AND
        //*    #TI2-SEC-DOB   NE '00000000'
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti2_Sec_Dob.getText(),"N8")))                                                                                        //Natural: IF #TI2-SEC-DOB IS ( N8 )
        {
            iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte), ti_Record_Input_Pnd_Ti2_Sec_Dob.val());        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE := VAL ( #TI2-SEC-DOB )
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #TI2-SEC-MORT-YOB NE ' ' AND
        //*    #TI2-SEC-MORT-YOB NE '0000'
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti2_Sec_Mort_Yob.getText(),"N4")))                                                                                   //Natural: IF #TI2-SEC-MORT-YOB IS ( N4 )
        {
            iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte), ti_Record_Input_Pnd_Ti2_Sec_Mort_Yob.val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE := VAL ( #TI2-SEC-MORT-YOB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ti_Record_Input_Pnd_Ti2_Sec_Sex.equals("M")))                                                                                                       //Natural: IF #TI2-SEC-SEX = 'M'
        {
            iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde.setValue(1);                                                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ti_Record_Input_Pnd_Ti2_Sec_Sex.equals("F")))                                                                                                   //Natural: IF #TI2-SEC-SEX = 'F'
            {
                iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde.setValue(2);                                                                                                          //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := 2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #TI2-SEC-SEX NE ' '
        //* *  IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := VAL(#TI2-SEC-SEX)
        //* *END-IF
        //* *IF #TI2-SEC-LIFE-CNT NE ' '
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti2_Sec_Life_Cnt.getText(),"N1")))                                                                                   //Natural: IF #TI2-SEC-LIFE-CNT IS ( N1 )
        {
            iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt.compute(new ComputeParameters(false, iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt), ti_Record_Input_Pnd_Ti2_Sec_Life_Cnt.val()); //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT := VAL ( #TI2-SEC-LIFE-CNT )
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #TI2-SEC-SNN NE ' '
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti2_Sec_Snn.getText(),"N9")))                                                                                        //Natural: IF #TI2-SEC-SNN IS ( N9 )
        {
            iaa_Cntrct_Cntrct_Scnd_Annt_Ssn.compute(new ComputeParameters(false, iaa_Cntrct_Cntrct_Scnd_Annt_Ssn), ti_Record_Input_Pnd_Ti2_Sec_Snn.val());                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN := VAL ( #TI2-SEC-SNN )
        }                                                                                                                                                                 //Natural: END-IF
        //* *IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOD-DTE := #FRST-ANNT-DOD-DTE
        //* *IAA-CNTRCT.CNTRCT-SCND-ANNT-DOD-DTE := #SCND-ANNT-DOD-DTE
        //*  052020 - START
        if (condition(ti_Record_Input_Pnd_2k_Db_Eligibility_Ind.equals("Y")))                                                                                             //Natural: IF #2K-DB-ELIGIBILITY-IND = 'Y'
        {
            iaa_Cntrct_Cntrct_Type.setValue("D");                                                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE := 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ti_Record_Input_Pnd_Cola_Eligibility_Ind.equals("Y")))                                                                                              //Natural: IF #COLA-ELIGIBILITY-IND = 'Y'
        {
            iaa_Cntrct_Cntrct_Type.setValue("C");                                                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ti_Record_Input_Pnd_2k_Db_Eligibility_Ind.equals("Y") && ti_Record_Input_Pnd_Cola_Eligibility_Ind.equals("Y")))                                     //Natural: IF #2K-DB-ELIGIBILITY-IND = 'Y' AND #COLA-ELIGIBILITY-IND = 'Y'
        {
            iaa_Cntrct_Cntrct_Type.setValue("B");                                                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE := 'B'
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                                 //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TRANS-DTE
        //* *MOVE EDITED '20050501' TO
        iaa_Cntrct_Cntrct_Annty_Strt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Check_Dte_Pnd_Check_Dte_A);                                                    //Natural: MOVE EDITED #CHECK-DTE-A TO IAA-CNTRCT.CNTRCT-ANNTY-STRT-DTE ( EM = YYYYMMDD )
        iaa_Cntrct_Cntrct_Issue_Dte_Dd.setValue(1);                                                                                                                       //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD := 01
        if (condition(iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte.greater(getZero())))                                                                                          //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE GT 0
        {
            iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd.setValue(1);                                                                                                                  //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-DUE-DTE-DD := 01
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte.greater(getZero())))                                                                                           //Natural: IF IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE GT 0
        {
            iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-PD-DTE-DD := 01
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Cntrct.insertDBRow();                                                                                                                                      //Natural: STORE IAA-CNTRCT
    }
    private void sub_Create_Cpr_Record() throws Exception                                                                                                                 //Natural: CREATE-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM GET-PIN
        sub_Get_Pin();
        if (condition(Global.isEscape())) {return;}
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde.setValue(1);                                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 1
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd.getValue(1).setValue("T");                                                                                              //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) := 'T'
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                    //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE := #TRANS-DTE
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                          //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR := #TI1-CONTRACT
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde.setValue(ti_Record_Input_Pnd_Ti1_Payee);                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE := #TI1-PAYEE
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind.setValue(ti_Record_Input_Pnd_Ti1_Mode);                                                                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND := #TI1-MODE
        //* *IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE := #TI1-FIN-PER-DTE
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti1_Fin_Per_Dte.getText(),"N6")))                                                                                    //Natural: IF #TI1-FIN-PER-DTE IS ( N6 )
        {
            iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte), ti_Record_Input_Pnd_Ti1_Fin_Per_Dte.val()); //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE := VAL ( #TI1-FIN-PER-DTE )
        }                                                                                                                                                                 //Natural: END-IF
        //* *IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE := #TI1-FIN-PMT-DTE
        if (condition(DbsUtil.is(ti_Record_Input_Pnd_Ti1_Fin_Pmt_Dte.getText(),"N8")))                                                                                    //Natural: IF #TI1-FIN-PMT-DTE IS ( N8 )
        {
            iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte.compute(new ComputeParameters(false, iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte), ti_Record_Input_Pnd_Ti1_Fin_Pmt_Dte.val()); //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE := VAL ( #TI1-FIN-PMT-DTE )
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde.setValue(ti_Record_Input_Pnd_Ti1_Citizen);                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE := #TI1-CITIZEN
        //* *IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE := #TI1-RES-ST
        //*  052020
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde.setValue(ti_Record_Input_Pnd_Ti1_Res_St, MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT #TI1-RES-ST TO IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Ssn);                                                                                 //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR := #TI1-SSN
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind.getValue(1).setValue(2);                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( 1 ) := 2
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde.setValue("0");                                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE := '0'
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde.setValue("0");                                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE := '0'
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr.setValue(ti_Record_Input_Pnd_Hrvrd_Emp_Id);                                                                              //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-CNTRCT-NBR := #HRVRD-EMP-ID
        //*  07/2004 - ADDED FOR PER-IVC CALCULATION
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt.getValue(1).reset();                                                                                                       //Natural: RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 1 )
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(1).reset();
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(1).reset();
        if (condition(ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A.notEquals(" ")))                                                                                                 //Natural: IF #TI5-FIN-PMT1-A NE ' '
        {
            DbsUtil.examine(new ExamineSource(ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                                 //Natural: EXAMINE FULL #TI5-FIN-PMT1-A FOR ' ' REPLACE '0'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt.greater(getZero())))                                                                                         //Natural: IF #TI5-CNTRCT-IVC-AMT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PER-IVC
            sub_Calculate_Per_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        vw_iaa_Cntrct_Prtcpnt_Role.insertDBRow();                                                                                                                         //Natural: STORE IAA-CNTRCT-PRTCPNT-ROLE
    }
    private void sub_Create_Fund_Record() throws Exception                                                                                                                //Natural: CREATE-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ti_Record_Input_Pnd_Ti5_Per_Pmt1_A.notEquals(" ")))                                                                                                 //Natural: IF #TI5-PER-PMT1-A NE ' '
        {
            DbsUtil.examine(new ExamineSource(ti_Record_Input_Pnd_Ti5_Per_Pmt1_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                                 //Natural: EXAMINE FULL #TI5-PER-PMT1-A FOR ' ' REPLACE '0'
            iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt.setValue(ti_Record_Input_Pnd_Ti5_Per_Pmt1);                                                                               //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := #TI5-PER-PMT1
            iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(1).setValue(ti_Record_Input_Pnd_Ti5_Per_Pmt1);                                                                   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) := #TI5-PER-PMT1
            pnd_Tot_Per_Amt.nadd(iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt.getValue(1));                                                                                        //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1 ) TO #TOT-PER-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A.notEquals(" ")))                                                                                                 //Natural: IF #TI5-FIN-PMT1-A NE ' '
        {
            DbsUtil.examine(new ExamineSource(ti_Record_Input_Pnd_Ti5_Fin_Pmt1_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                                 //Natural: EXAMINE FULL #TI5-FIN-PMT1-A FOR ' ' REPLACE '0'
            iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt.getValue(1).setValue(ti_Record_Input_Pnd_Ti5_Fin_Pmt1);                                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( 1 ) := #TI5-FIN-PMT1
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                               //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := #TI1-CONTRACT
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde.setValue(ti_Record_Input_Pnd_Ti1_Payee);                                                                                 //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := #TI1-PAYEE
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde.setValue("T1 ");                                                                                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1 '
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde.getValue(1).setValue(ti_Record_Input_Pnd_Ti5_Rate1);                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1 ) := #TI5-RATE1
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
        vw_iaa_Tiaa_Fund_Rcrd.insertDBRow();                                                                                                                              //Natural: STORE IAA-TIAA-FUND-RCRD
    }
    private void sub_Get_Pin() throws Exception                                                                                                                           //Natural: GET-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  08/15
        if (condition(ti_Record_Input_Pnd_Ti1_Payee.equals(1)))                                                                                                           //Natural: IF #TI1-PAYEE = 01
        {
            pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(ti_Record_Input_Pnd_Ti1_Ssn);                                                                                  //Natural: ASSIGN #I-SSN := #TI1-SSN
            //* *ELSE
            //*   IF #FRST-ANNT-DOD-DTE GT 0
            //*     IF #TI2-SEC-SNN = MASK(NNNNNNNNN)
            //*       #SSN-NBR := VAL(#TI2-SEC-SNN)
            //*     END-IF
            //*   ELSE
            //*     #SSN-NBR := #TI1-SSN
            //*   END-IF
        }                                                                                                                                                                 //Natural: END-IF
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr.reset();                                                                                                                       //Natural: RESET IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR
        //*  08/15 - START
        //* *CALLNAT 'MDMN100A' #MDMA100                      /* 082017
        //*  082017
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  082017
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #O-RETURN-CODE = '0000'
        {
            iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                       //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR := #O-PIN-N12
        }                                                                                                                                                                 //Natural: END-IF
        //* *FIND COR-XREF-PH WITH COR-SUPER-SSN-RCDTYPE = #SSN-KEY
        //*   IF COR-XREF-PH.PH-UNIQUE-ID-NBR NE 9999999
        //*     IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR := COR-XREF-PH.PH-UNIQUE-ID-NBR
        //*   END-IF
        //* *END-FIND
        //*  08/15 - END
    }
    private void sub_Create_Ddctn_Record() throws Exception                                                                                                               //Natural: CREATE-DDCTN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1/2018 - START
        //* *ESCAPE ROUTINE
        pnd_D.reset();                                                                                                                                                    //Natural: RESET #D #DED ( * )
        pnd_Ded.getValue("*").reset();
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(ti_Record_Input_Pnd_Ddctn_Amt.getValue(pnd_I).equals(getZero()) || ti_Record_Input_Pnd_Ddctn_Cde.getValue(pnd_I).equals(getZero())              //Natural: IF #DDCTN-AMT ( #I ) = 0 OR ( #DDCTN-CDE ( #I ) = 0 OR = MASK ( '   ' ) ) OR #DDCTN-AMT ( #I ) = MASK ( AAAAAAA )
                || DbsUtil.maskMatches(ti_Record_Input_Pnd_Ddctn_Cde.getValue(pnd_I),"'   '") || DbsUtil.maskMatches(ti_Record_Input_Pnd_Ddctn_Amt.getValue(pnd_I),
                "AAAAAAA")))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_D.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #D
            iaa_Deduction_Lst_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                          //Natural: ASSIGN IAA-DEDUCTION.LST-TRANS-DTE := #TRANS-DTE
            iaa_Deduction_Ddctn_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                                      //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PPCN-NBR := #TI1-CONTRACT
            iaa_Deduction_Ddctn_Payee_Cde.setValue(ti_Record_Input_Pnd_Ti1_Payee);                                                                                        //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PAYEE-CDE := #TI1-PAYEE
            iaa_Deduction_Ddctn_Cde.setValueEdited(ti_Record_Input_Pnd_Ddctn_Cde.getValue(pnd_I),new ReportEditMask("999"));                                              //Natural: MOVE EDITED #DDCTN-CDE ( #I ) ( EM = 999 ) TO IAA-DEDUCTION.DDCTN-CDE
            iaa_Deduction_Ddctn_Seq_Nbr.setValue(pnd_I);                                                                                                                  //Natural: ASSIGN IAA-DEDUCTION.DDCTN-SEQ-NBR := #I
            iaa_Deduction_Ddctn_Payee.setValue(ti_Record_Input_Pnd_Ti1_Iss_Coll);                                                                                         //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PAYEE := #TI1-ISS-COLL
            iaa_Deduction_Ddctn_Per_Amt.setValue(ti_Record_Input_Pnd_Ddctn_Amt.getValue(pnd_I));                                                                          //Natural: ASSIGN IAA-DEDUCTION.DDCTN-PER-AMT := #DDCTN-AMT ( #I )
            iaa_Deduction_Ddctn_Strt_Dte.setValue(pnd_Check_Dte);                                                                                                         //Natural: ASSIGN IAA-DEDUCTION.DDCTN-STRT-DTE := #CHECK-DTE
            iaa_Deduction_Ddctn_Final_Dte.setValue(99999999);                                                                                                             //Natural: ASSIGN IAA-DEDUCTION.DDCTN-FINAL-DTE := 99999999
            iaa_Deduction_Ddctn_Id_Nbr.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                               //Natural: ASSIGN IAA-DEDUCTION.DDCTN-ID-NBR := #O-PIN-N12
            pnd_Ded_Pnd_D_Cde.getValue(pnd_D).setValue(ti_Record_Input_Pnd_Ddctn_Cde.getValue(pnd_I));                                                                    //Natural: ASSIGN #D-CDE ( #D ) := #DDCTN-CDE ( #I )
            pnd_Ded_Pnd_D_Amt.getValue(pnd_D).setValue(ti_Record_Input_Pnd_Ddctn_Amt.getValue(pnd_I));                                                                    //Natural: ASSIGN #D-AMT ( #D ) := #DDCTN-AMT ( #I )
            vw_iaa_Deduction.insertDBRow();                                                                                                                               //Natural: STORE IAA-DEDUCTION
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  1/2018 - END
    }
    private void sub_Create_Trans_Record() throws Exception                                                                                                               //Natural: CREATE-TRANS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                                    //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
        if (condition(pnd_Invrse_Dte.equals(pnd_S_Invrse_Dte)))                                                                                                           //Natural: IF #INVRSE-DTE = #S-INVRSE-DTE
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                 //Natural: ASSIGN #TRANS-DTE := *TIMX
                pnd_Invrse_Dte.compute(new ComputeParameters(false, pnd_Invrse_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                            //Natural: COMPUTE #INVRSE-DTE = 1000000000000 - #TRANS-DTE
                if (condition(pnd_Invrse_Dte.notEquals(pnd_S_Invrse_Dte)))                                                                                                //Natural: IF #INVRSE-DTE NE #S-INVRSE-DTE
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_S_Invrse_Dte.setValue(pnd_Invrse_Dte);                                                                                                                        //Natural: ASSIGN #S-INVRSE-DTE := #INVRSE-DTE
        iaa_Trans_Rcrd_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-DTE
        iaa_Trans_Rcrd_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        iaa_Trans_Rcrd_Lst_Trans_Dte.setValue(iaa_Trans_Rcrd_Trans_Dte);                                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-TRANS-RCRD.TRANS-DTE
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #TI1-CONTRACT
        iaa_Trans_Rcrd_Trans_Payee_Cde.setValue(ti_Record_Input_Pnd_Ti1_Payee);                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #TI1-PAYEE
        iaa_Trans_Rcrd_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                                           //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := #CHECK-DTE
        iaa_Trans_Rcrd_Trans_Actvty_Cde.setValue("A");                                                                                                                    //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-ACTVTY-CDE := 'A'
        iaa_Trans_Rcrd_Trans_Todays_Dte.setValue(pnd_Todays_Dte);                                                                                                         //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := #TODAYS-DTE
        iaa_Trans_Rcrd_Trans_User_Area.setValue("BATCH");                                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'BATCH'
        iaa_Trans_Rcrd_Trans_User_Id.setValue(Global.getPROGRAM());                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := *PROGRAM
        iaa_Trans_Rcrd_Trans_Cde.setValue(33);                                                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 033
        vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
        //*  07/2004 - ADDED FOR IVC
        if (condition(ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt.greater(getZero())))                                                                                         //Natural: IF #TI5-CNTRCT-IVC-AMT GT 0
        {
            iaa_Trans_Rcrd_Trans_Cde.setValue(724);                                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 724
            vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                              //Natural: STORE IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(ti_Record_Input_Pnd_Ddctn_Amt.getValue(pnd_I).greater(getZero())))                                                                              //Natural: IF #DDCTN-AMT ( #I ) GT 0
            {
                iaa_Trans_Rcrd_Trans_Cde.setValue(906);                                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 906
                if (condition(pnd_I.greater(1)))                                                                                                                          //Natural: IF #I GT 1
                {
                    iaa_Trans_Rcrd_Invrse_Trans_Dte.nadd(1);                                                                                                              //Natural: ADD 1 TO IAA-TRANS-RCRD.INVRSE-TRANS-DTE
                }                                                                                                                                                         //Natural: END-IF
                vw_iaa_Trans_Rcrd.insertDBRow();                                                                                                                          //Natural: STORE IAA-TRANS-RCRD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr.setValue(ti_Record_Input_Pnd_Ti1_Contract);                                                                         //Natural: ASSIGN #DDCTN-PPCN-NBR := #TI1-CONTRACT
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde.setValue(ti_Record_Input_Pnd_Ti1_Payee);                                                                           //Natural: ASSIGN #DDCTN-PAYEE-CDE := #TI1-PAYEE
        pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Seq_Nbr.reset();                                                                                                             //Natural: RESET #DDCTN-SEQ-NBR
        vw_iaa_Deduction.startDatabaseRead                                                                                                                                //Natural: READ IAA-DEDUCTION BY CNTRCT-PAYEE-DDCTN-KEY STARTING FROM #CNTRCT-PAYEE-DDCTN-KEY
        (
        "READ03",
        new Wc[] { new Wc("CNTRCT_PAYEE_DDCTN_KEY", ">=", pnd_Cntrct_Payee_Ddctn_Key, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_PAYEE_DDCTN_KEY", "ASC") }
        );
        READ03:
        while (condition(vw_iaa_Deduction.readNextRow("READ03")))
        {
            if (condition(iaa_Deduction_Ddctn_Ppcn_Nbr.notEquals(pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Ppcn_Nbr) || iaa_Deduction_Ddctn_Payee_Cde.notEquals(pnd_Cntrct_Payee_Ddctn_Key_Pnd_Ddctn_Payee_Cde))) //Natural: IF DDCTN-PPCN-NBR NE #DDCTN-PPCN-NBR OR DDCTN-PAYEE-CDE NE #DDCTN-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            vw_iaa_Ddctn_Trans.setValuesByName(vw_iaa_Deduction);                                                                                                         //Natural: MOVE BY NAME IAA-DEDUCTION TO IAA-DDCTN-TRANS
            iaa_Ddctn_Trans_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                            //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-DTE := #TRANS-DTE
            iaa_Ddctn_Trans_Invrse_Trans_Dte.setValue(pnd_Invrse_Dte);                                                                                                    //Natural: ASSIGN IAA-DDCTN-TRANS.INVRSE-TRANS-DTE := #INVRSE-DTE
            iaa_Ddctn_Trans_Trans_Check_Dte.setValue(pnd_Check_Dte);                                                                                                      //Natural: ASSIGN IAA-DDCTN-TRANS.TRANS-CHECK-DTE := #CHECK-DTE
            iaa_Ddctn_Trans_Bfre_Imge_Id.setValue(" ");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.BFRE-IMGE-ID := ' '
            iaa_Ddctn_Trans_Aftr_Imge_Id.setValue("2");                                                                                                                   //Natural: ASSIGN IAA-DDCTN-TRANS.AFTR-IMGE-ID := '2'
            vw_iaa_Ddctn_Trans.insertDBRow();                                                                                                                             //Natural: STORE IAA-DDCTN-TRANS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  1/2018 - END
        //*  CREATE AFTER IMAGES
        DbsUtil.callnat(Iaan060h.class , getCurrentProcessState(), iaa_Trans_Rcrd_Trans_Ppcn_Nbr, iaa_Trans_Rcrd_Trans_Payee_Cde, pnd_Invrse_Dte, pnd_Trans_Dte,          //Natural: CALLNAT 'IAAN060H' IAA-TRANS-RCRD.TRANS-PPCN-NBR IAA-TRANS-RCRD.TRANS-PAYEE-CDE #INVRSE-DTE #TRANS-DTE #CHECK-DTE
            pnd_Check_Dte);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Calculate_Per_Ivc() throws Exception                                                                                                                 //Natural: CALCULATE-PER-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Iaangrp.class , getCurrentProcessState(), ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt, ti_Record_Input_Pnd_Ti1_Mode, ti_Record_Input_Pnd_Ti1_Opt,      //Natural: CALLNAT 'IAANGRP' #TI5-CNTRCT-IVC-AMT #TI1-MODE #TI1-OPT #TI1-ORG #TI1-ISS-DTE #TI1-FST-DUE-DTE #TI1-FIN-PER-DTEN #CHECK-DTE #TI2-FST-DOB #TI2-SEC-DOBN #TI5-PER-PMT1 #TI5-FIN-PMT1 #PER-IVC-AMT #IVC-USED-AMT
            ti_Record_Input_Pnd_Ti1_Org, ti_Record_Input_Pnd_Ti1_Iss_Dte, ti_Record_Input_Pnd_Ti1_Fst_Due_Dte, ti_Record_Input_Pnd_Ti1_Fin_Per_Dten, pnd_Check_Dte, 
            ti_Record_Input_Pnd_Ti2_Fst_Dob, ti_Record_Input_Pnd_Ti2_Sec_Dobn, ti_Record_Input_Pnd_Ti5_Per_Pmt1, ti_Record_Input_Pnd_Ti5_Fin_Pmt1, pnd_Per_Ivc_Amt, 
            pnd_Ivc_Used_Amt);
        if (condition(Global.isEscape())) return;
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt.getValue(1).setValue(ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt);                                                              //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 ) := #TI5-CNTRCT-IVC-AMT
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt.getValue(1).setValue(pnd_Per_Ivc_Amt);                                                                                 //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 ) := #PER-IVC-AMT
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt.getValue(1).setValue(pnd_Ivc_Used_Amt);                                                                               //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 1 ) := #IVC-USED-AMT
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1/2018 - START
        if (condition(pnd_D.equals(getZero())))                                                                                                                           //Natural: IF #D = 0
        {
            pnd_D.setValue(1);                                                                                                                                            //Natural: ASSIGN #D := 1
            //*  082017
            //*  1/2018
            //*  1/2018
            //*  052020
            //*  052020
            //*  052020
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "/Contract",                                                                                                                              //Natural: DISPLAY ( 1 ) '/Contract' #TI1-CONTRACT '/Py' #TI1-PAYEE ( EM = 99 ) 'Iss/St' #TI1-ISS-ST 'Res/Cde' #TI1-RES-ST 'Issue/Dte' #TI1-ISS-DTE 'PPG/Cde' #TI1-ISS-COLL '/Mode' #TI1-MODE '/Opt' #TI1-OPT '/Orgn' #TI1-ORG '/Rte' #TI5-RATE1 '/Per Amt' #TI5-PER-PMT1 ( EM = ZZ,ZZ9.99 ) '/PIN' IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR ( NL = 7 ) '1ST ANNT/SSN' #TI1-SSN-A '2nd Annt/SSN' #TI2-SEC-SNN '/1st DOB' #TI2-FST-DOB '/2nd DOB' #TI2-SEC-DOB '1st Pmt/Due Date' #TI1-FST-DUE-DTE '1st Pmt/Pd Date' #TI1-FST-PD-DTE 'IVC Amount' #TI5-CNTRCT-IVC-AMT ( EM = ZZZZZZ9.99 ) 'Per IVC' #PER-IVC-AMT ( EM = ZZZZZZ9.99 ) 'Ded/Code' #D-CDE ( 1:#D ) 'Ded/Amt' #D-AMT ( 1:#D ) ( EM = ZZZZ9.99 ZP = OFF ) 'COLA' #COLA-ELIGIBILITY-IND '2k DB' #2K-DB-ELIGIBILITY-IND 'Harvard Id' #HRVRD-EMP-ID
        		ti_Record_Input_Pnd_Ti1_Contract,"/Py",
        		ti_Record_Input_Pnd_Ti1_Payee, new ReportEditMask ("99"),"Iss/St",
        		ti_Record_Input_Pnd_Ti1_Iss_St,"Res/Cde",
        		ti_Record_Input_Pnd_Ti1_Res_St,"Issue/Dte",
        		ti_Record_Input_Pnd_Ti1_Iss_Dte,"PPG/Cde",
        		ti_Record_Input_Pnd_Ti1_Iss_Coll,"/Mode",
        		ti_Record_Input_Pnd_Ti1_Mode,"/Opt",
        		ti_Record_Input_Pnd_Ti1_Opt,"/Orgn",
        		ti_Record_Input_Pnd_Ti1_Org,"/Rte",
        		ti_Record_Input_Pnd_Ti5_Rate1,"/Per Amt",
        		ti_Record_Input_Pnd_Ti5_Per_Pmt1, new ReportEditMask ("ZZ,ZZ9.99"),"/PIN",
        		iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr, new NumericLength (7),"1ST ANNT/SSN",
        		ti_Record_Input_Pnd_Ti1_Ssn_A,"2nd Annt/SSN",
        		ti_Record_Input_Pnd_Ti2_Sec_Snn,"/1st DOB",
        		ti_Record_Input_Pnd_Ti2_Fst_Dob,"/2nd DOB",
        		ti_Record_Input_Pnd_Ti2_Sec_Dob,"1st Pmt/Due Date",
        		ti_Record_Input_Pnd_Ti1_Fst_Due_Dte,"1st Pmt/Pd Date",
        		ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte,"IVC Amount",
        		ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt, new ReportEditMask ("ZZZZZZ9.99"),"Per IVC",
        		pnd_Per_Ivc_Amt, new ReportEditMask ("ZZZZZZ9.99"),"Ded/Code",
        		pnd_Ded_Pnd_D_Cde.getValue(1,":",pnd_D),"Ded/Amt",
        		pnd_Ded_Pnd_D_Amt.getValue(1,":",pnd_D), new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"COLA",
        		ti_Record_Input_Pnd_Cola_Eligibility_Ind,"2k DB",
        		ti_Record_Input_Pnd_2k_Db_Eligibility_Ind,"Harvard Id",
        		ti_Record_Input_Pnd_Hrvrd_Emp_Id);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new ColumnSpacing(40),"IA CONTRACT LOAD REPORT",new                 //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 40X 'IA CONTRACT LOAD REPORT' 40X / *DATX ( EM = MM/DD/YYYY ) 106X 'Page' *PAGE-NUMBER ( 1 )
                        ColumnSpacing(40),NEWLINE,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(106),"Page",getReports().getPageNumberDbs(1));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=66 LS=133");
        Global.format(1, "PS=66 LS=250 ZP=OFF");

        getReports().setDisplayColumns(1, "/Contract",
        		ti_Record_Input_Pnd_Ti1_Contract,"/Py",
        		ti_Record_Input_Pnd_Ti1_Payee, new ReportEditMask ("99"),"Iss/St",
        		ti_Record_Input_Pnd_Ti1_Iss_St,"Res/Cde",
        		ti_Record_Input_Pnd_Ti1_Res_St,"Issue/Dte",
        		ti_Record_Input_Pnd_Ti1_Iss_Dte,"PPG/Cde",
        		ti_Record_Input_Pnd_Ti1_Iss_Coll,"/Mode",
        		ti_Record_Input_Pnd_Ti1_Mode,"/Opt",
        		ti_Record_Input_Pnd_Ti1_Opt,"/Orgn",
        		ti_Record_Input_Pnd_Ti1_Org,"/Rte",
        		ti_Record_Input_Pnd_Ti5_Rate1,"/Per Amt",
        		ti_Record_Input_Pnd_Ti5_Per_Pmt1, new ReportEditMask ("ZZ,ZZ9.99"),"/PIN",
        		iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr, new NumericLength (7),"1ST ANNT/SSN",
        		ti_Record_Input_Pnd_Ti1_Ssn_A,"2nd Annt/SSN",
        		ti_Record_Input_Pnd_Ti2_Sec_Snn,"/1st DOB",
        		ti_Record_Input_Pnd_Ti2_Fst_Dob,"/2nd DOB",
        		ti_Record_Input_Pnd_Ti2_Sec_Dob,"1st Pmt/Due Date",
        		ti_Record_Input_Pnd_Ti1_Fst_Due_Dte,"1st Pmt/Pd Date",
        		ti_Record_Input_Pnd_Ti1_Fst_Pd_Dte,"IVC Amount",
        		ti_Record_Input_Pnd_Ti5_Cntrct_Ivc_Amt, new ReportEditMask ("ZZZZZZ9.99"),"Per IVC",
        		pnd_Per_Ivc_Amt, new ReportEditMask ("ZZZZZZ9.99"),"Ded/Code",
        		pnd_Ded_Pnd_D_Cde,"Ded/Amt",
        		pnd_Ded_Pnd_D_Amt, new ReportEditMask ("ZZZZ9.99"), new ReportZeroPrint (false),"COLA",
        		ti_Record_Input_Pnd_Cola_Eligibility_Ind,"2k DB",
        		ti_Record_Input_Pnd_2k_Db_Eligibility_Ind,"Harvard Id",
        		ti_Record_Input_Pnd_Hrvrd_Emp_Id);
    }
}
